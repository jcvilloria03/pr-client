/**
 * COMMON WEBPACK CONFIGURATION
 */

const path = require( 'path' );
const webpack = require( 'webpack' );

module.exports = ( options ) => ({
    entry: options.entry,
    output: Object.assign({ // Compile into js/build.js
        path: path.resolve( process.cwd(), 'build' ),
        publicPath: '/payroll/'
    }, options.output ), // Merge with env dependent settings
    module: {
        loaders: [{
            test: /\.js$/, // Transform all .js files required somewhere with Babel
            loader: 'babel-loader',
            exclude: /node_modules/,
            query: options.babelQuery
        }, {
      // Do not transform vendor's CSS with CSS-modules
      // The point is that they remain in global scope.
      // Since we require these CSS files in our JS or CSS files,
      // they will be a part of our compilation either way.
      // So, no need for ExtractTextPlugin here.
            test: /\.css$/,
            include: /node_modules/,
            loaders: [ 'style-loader', 'css-loader' ]
        }, {
            test: /\.(eot|svg|ttf|woff|woff2)$/,
            loader: 'file-loader'
        }, {
            test: /\.(jpg|png|gif)$/,
            loaders: [
                'file-loader',
                {
                    loader: 'image-webpack-loader',
                    query: {
                        progressive: true,
                        optimizationLevel: 7,
                        interlaced: false,
                        pngquant: {
                            quality: '65-90',
                            speed: 4
                        }
                    }
                }
            ]
        }, {
            test: /\.html$/,
            loader: 'html-loader'
        }, {
            test: /\.json$/,
            loader: 'json-loader'
        }, {
            test: /\.(mp4|webm)$/,
            loader: 'url-loader',
            query: {
                limit: 10000
            }
        }]
    },
    plugins: options.plugins.concat([
        new webpack.ProvidePlugin({
      // make fetch available
            fetch: 'exports-loader?self.fetch!whatwg-fetch'
        }),

    // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
    // inside your code for any environment checks; UglifyJS will automatically
    // drop any unreachable code.
        new webpack.DefinePlugin({
            'process.env': {
                APP_ENV: JSON.stringify( process.env.APP_ENV, 'production' ),
                API_DOMAIN: JSON.stringify( process.env.API_DOMAIN ),
                NODE_ENV: JSON.stringify( process.env.NODE_ENV ),
                AUTH_DOMAIN: JSON.stringify( process.env.AUTH_DOMAIN ),
                AUTH_CLIENT_ID: JSON.stringify( process.env.AUTH_CLIENT_ID ),
                SOCKETS_URL: JSON.stringify( process.env.SOCKETS_URL ),
                SALPAY_BANK_ID: JSON.stringify( process.env.SALPAY_BANK_ID ),
                SESSION_TIMEOUT_COUNTDOWN_SECONDS: JSON.stringify( process.env.SESSION_TIMEOUT_COUNTDOWN_SECONDS ),
                SESSION_IDLE_TIME_MS: JSON.stringify( process.env.SESSION_IDLE_TIME_MS ),
                RECAPTCHA_SITE_KEY: JSON.stringify( process.env.RECAPTCHA_SITE_KEY ),
                OPEN_SIGNUP_URL: JSON.stringify( process.env.OPEN_SIGNUP_URL ),
                TRACKING_ID: JSON.stringify( process.env.TRACKING_ID ),
                GTMID: JSON.stringify( process.env.GTMID ),
                INLINEMANUAL_API_KEY: JSON.stringify( process.env.INLINEMANUAL_API_KEY ),
                STRIPE_PUB_KEY: JSON.stringify( process.env.STRIPE_PUB_KEY ),
                INTERCOM_APP_ID: JSON.stringify( process.env.INTERCOM_APP_ID ),
                USERFLOW_TOKEN: JSON.stringify( process.env.USERFLOW_TOKEN ),
                ZOHO_WIDGET_CODE: JSON.stringify( process.env.ZOHO_WIDGET_CODE ),
                AUTHN_API_BASE_URI: JSON.stringify( process.env.AUTHN_API_BASE_URI )
            }
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.ContextReplacementPlugin( /\.\/locale$/, null, false, /js$/ )
    ]),
    resolve: {
        modules: [ 'app', 'node_modules' ],
        extensions: [
            '.js',
            '.jsx',
            '.react.js'
        ],
        mainFields: [
            'browser',
            'jsnext:main',
            'main'
        ],
        alias: {
            moment$: 'moment/moment.js'
        }
    },
    devtool: options.devtool,
    target: 'web', // Make web variables accessible to webpack, e.g. window
    performance: options.performance || {}
});
