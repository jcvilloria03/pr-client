// needed for regenerator-runtime
// (ES7 generator support is required by redux-saga)
import 'babel-polyfill';

/**
 * Mock localStorage Service to Jest testing
 */
class LocalStorageMock {
    /**
     * service constructor
     */
    constructor() {
        this.store = {};
    }

    /**
     * this clears the mocked storage
     */
    clear() {
        this.store = {};
    }

    /**
     * this simulates the getter for localstorage
     */
    getItem( key ) {
        return this.store[ key ];
    }

    /**
     * this simulates the setter for localstorage
     */
    setItem( key, value ) {
        this.store[ key ] = value.toString();
    }

    /**
     * this simulates the remove method of localstorage
     */
    removeItem( key ) {
        delete this.store[ key ];
    }
}

global.localStorage = new LocalStorageMock();
