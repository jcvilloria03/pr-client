{
    "name": "salarium-v3",
    "version": "0.1.0",
    "description": "A showcase of prototypes for upcoming Salarium features",
    "repository": {
        "type": "git",
        "url": "git://github.com/SalariumPH/salarium.git"
    },
    "engines": {
        "npm": ">=3",
        "node": ">=5"
    },
    "author": "Salarium",
    "license": "UNLICENSED",
    "private": true,
    "scripts": {
        "set:env": "node setenv.js > .env",
        "analyze:clean": "rimraf stats.json",
        "preanalyze": "npm run analyze:clean",
        "analyze": "node ./internals/scripts/analyze.js",
        "extract-intl": "babel-node --presets latest,stage-0 -- ./internals/scripts/extract-intl.js",
        "npmcheckversion": "node ./internals/scripts/npmcheckversion.js",
        "preinstall": "npm run npmcheckversion",
        "postinstall": "npm run build:dll",
        "prebuild": "npm run build:clean",
        "build": "cross-env NODE_ENV=production env-cmd .env webpack --config internals/webpack/webpack.prod.babel.js  --color -p --progress && mkdir -p build/.well-known && cp apple-developer-merchantid-domain-association build/.well-known/",
        "build:clean": "npm run test:clean && rimraf ./build",
        "build:dll": "node ./internals/scripts/dependencies.js",
        "start": "cross-env NODE_ENV=development env-cmd .env node server",
        "start:tunnel": "cross-env NODE_ENV=development env-cmd .env ENABLE_TUNNEL=true node server",
        "start:production": "npm run test && npm run build && npm run start:prod",
        "start:prod": "cross-env NODE_ENV=production node server",
        "presetup": "npm i chalk shelljs",
        "setup": "node ./internals/scripts/setup.js",
        "postsetup": "npm run build:dll",
        "clean": "shjs ./internals/scripts/clean.js",
        "clean:all": "npm run analyze:clean && npm run test:clean && npm run build:clean",
        "generate": "plop --plopfile internals/generators/index.js",
        "lint": "npm run lint:js",
        "lint:eslint": "eslint --ignore-path .gitignore --ignore-pattern internals/scripts",
        "lint:js": "npm run lint:eslint -- . ",
        "lint:staged": "lint-staged",
        "pretest": "npm run test:clean && npm run lint",
        "test:clean": "rimraf ./coverage",
        "test": "cross-env NODE_ENV=test env-cmd .env jest",
        "test:ci": "cross-env NODE_ENV=test jest",
        "test:watch": "cross-env NODE_ENV=test env-cmd .env jest --watchAll",
        "coveralls": "cat ./coverage/lcov.info | coveralls"
    },
    "lint-staged": {
        "*.js": "lint:eslint"
    },
    "pre-commit": "lint:staged",
    "babel": {
        "presets": [
            [
                "latest",
                {
                    "es2015": {
                        "modules": false
                    }
                }
            ],
            "react",
            "stage-0"
        ],
        "env": {
            "production": {
                "only": [
                    "app"
                ],
                "plugins": [
                    "transform-react-remove-prop-types",
                    "transform-react-constant-elements",
                    "transform-react-inline-elements"
                ]
            },
            "test": {
                "plugins": [
                    "transform-es2015-modules-commonjs",
                    "dynamic-import-node"
                ]
            }
        }
    },
    "eslintConfig": {
        "parser": "babel-eslint",
        "extends": "airbnb",
        "env": {
            "browser": true,
            "node": true,
            "jest": true,
            "es6": true
        },
        "plugins": [
            "redux-saga",
            "react",
            "jsx-a11y"
        ],
        "parserOptions": {
            "ecmaVersion": 6,
            "sourceType": "module",
            "ecmaFeatures": {
                "jsx": true
            }
        },
        "rules": {
            "array-bracket-spacing": [
                "error",
                "always",
                {
                    "singleValue": false,
                    "objectsInArrays": false,
                    "arraysInArrays": false
                }
            ],
            "arrow-parens": [
                "error",
                "always"
            ],
            "arrow-body-style": [
                2,
                "as-needed"
            ],
            "class-methods-use-this": 0,
            "comma-dangle": [
                2,
                "never"
            ],
            "comma-spacing": [
                "error",
                {
                    "before": false,
                    "after": true
                }
            ],
            "comma-style": [
                "error",
                "last"
            ],
            "computed-property-spacing": [
                "error",
                "always"
            ],
            "import/imports-first": 0,
            "import/newline-after-import": 0,
            "import/no-extraneous-dependencies": 0,
            "import/no-named-as-default": 0,
            "import/no-unresolved": 2,
            "import/prefer-default-export": 0,
            "indent": [
                2,
                4,
                {
                    "SwitchCase": 1
                }
            ],
            "jsx-a11y/aria-props": 2,
            "jsx-a11y/heading-has-content": 0,
            "jsx-a11y/href-no-hash": 2,
            "jsx-a11y/label-has-for": 2,
            "jsx-a11y/mouse-events-have-key-events": 2,
            "jsx-a11y/role-has-required-aria-props": 2,
            "jsx-a11y/role-supports-aria-props": 2,
            "max-len": 0,
            "newline-per-chained-call": 0,
            "no-console": 1,
            "no-duplicate-imports": "error",
            "no-extra-semi": "error",
            "no-lonely-if": "error",
            "no-mixed-operators": [
                0,
                {
                    "groups": [
                        [
                            "+",
                            "-",
                            "*",
                            "/",
                            "%",
                            "**"
                        ],
                        [
                            "&",
                            "|",
                            "^",
                            "~",
                            "<<",
                            ">>",
                            ">>>"
                        ],
                        [
                            "==",
                            "!=",
                            "===",
                            "!==",
                            ">",
                            ">=",
                            "<",
                            "<="
                        ],
                        [
                            "&&",
                            "||"
                        ],
                        [
                            "in",
                            "instanceof"
                        ]
                    ],
                    "allowSamePrecedence": true
                }
            ],
            "no-multiple-empty-lines": [
                "error",
                {
                    "max": 1
                }
            ],
            "no-restricted-syntax": [
                "error",
                "ForInStatement",
                "LabeledStatement",
                "WithStatement"
            ],
            "no-trailing-spaces": "error",
            "no-underscore-dangle": 0,
            "no-unneeded-ternary": "error",
            "no-nested-ternary": 0,
            "no-unused-expressions": [
                2,
                {
                    "allowShortCircuit": true,
                    "allowTernary": true
                }
            ],
            "no-use-before-define": 0,
            "no-useless-constructor": "error",
            "no-var": "error",
            "object-curly-spacing": [
                "error",
                "always",
                {
                    "arraysInObjects": false,
                    "objectsInObjects": false
                }
            ],
            "prefer-arrow-callback": "error",
            "prefer-const": "error",
            "prefer-template": 2,
            "react/forbid-prop-types": 0,
            "react/jsx-curly-spacing": [
                2,
                "always"
            ],
            "react/jsx-first-prop-new-line": [
                2,
                "multiline"
            ],
            "react/jsx-indent": [
                2,
                4
            ],
            "react/jsx-indent-props": [
                2,
                4
            ],
            "react/jsx-filename-extension": 0,
            "react/jsx-no-target-blank": 0,
            "react/require-extension": 0,
            "react/self-closing-comp": 0,
            "require-yield": 0,
            "require-jsdoc": [
                "error",
                {
                    "require": {
                        "FunctionDeclaration": true,
                        "MethodDefinition": false,
                        "ClassDeclaration": true
                    }
                }
            ],
            "semi-spacing": [
                "error",
                {
                    "before": false,
                    "after": true
                }
            ],
            "space-in-parens": [
                "error",
                "always",
                {
                    "exceptions": [
                        "{}",
                        "[]",
                        "empty"
                    ]
                }
            ]
        },
        "settings": {
            "import/resolver": {
                "webpack": {
                    "config": "./internals/webpack/webpack.prod.babel.js"
                }
            }
        }
    },
    "dllPlugin": {
        "path": "node_modules/react-boilerplate-dlls",
        "exclude": [
            "chalk",
            "compression",
            "cross-env",
            "express",
            "ip",
            "minimist",
            "sanitize.css"
        ],
        "include": [
            "core-js",
            "lodash",
            "eventsource-polyfill"
        ]
    },
    "jest": {
        "collectCoverageFrom": [
            "app/**/*.{js,jsx}",
            "!app/containers/**/*.{js,jsx}",
            "!app/components/Table/*.{js,jsx}",
            "!app/components/AsyncTable/*.{js,jsx}",
            "!app/components/DatePicker/*.{js,jsx}",
            "!app/**/*.test.{js,jsx}",
            "!app/*/RbGenerated*/*.{js,jsx}",
            "!app/app.js",
            "!app/global-styles.js",
            "!app/routes.js",
            "!app/utils/*.js",
            "!app/utils/AnnualEarnings/*.js"
        ],
        "coverageThreshold": {
            "global": {
                "statements": 70,
                "branches": 78,
                "functions": 82,
                "lines": 80
            }
        },
        "moduleDirectories": [
            "node_modules",
            "app"
        ],
        "moduleNameMapper": {
            ".*\\.(css|less|styl|scss|sass)$": "<rootDir>/internals/mocks/cssModule.js",
            ".*\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/internals/mocks/image.js"
        },
        "setupTestFrameworkScriptFile": "<rootDir>/internals/testing/test-bundler.js",
        "testRegex": "tests/.*\\.test\\.js$"
    },
    "dependencies": {
        "@babel/runtime": "^7.14.5",
        "@iconify/icons-fluent": "^1.2.17",
        "auth0-js": "8.2.0",
        "axios": "0.15.3",
        "babel-polyfill": "6.20.0",
        "chalk": "1.1.3",
        "color": "1.0.2",
        "compression": "1.6.2",
        "countries-and-timezones": "^3.5.1",
        "cross-env": "3.1.3",
        "express": "4.14.0",
        "fontfaceobserver": "2.0.7",
        "fullcalendar": "^3.10.2",
        "imagemin-pngquant": "5.0.1",
        "immutable": "3.8.1",
        "intl": "1.2.5",
        "invariant": "2.2.2",
        "ip": "1.1.4",
        "jquery": "^3.6.0",
        "js-big-decimal": "^1.1.7",
        "js-file-download": "^0.4.1",
        "jwt-decode": "2.1.0",
        "laravel-echo": "^1.4.0",
        "lodash": "4.17.2",
        "minimist": "1.2.0",
        "moment": "^2.20.1",
        "moment-range": "^4.0.1",
        "moment-recur": "^1.0.7",
        "ngrok": "^2.2.4",
        "popper.js": "1.12.9",
        "rc-checkbox": "1.4.3",
        "rc-pagination": "1.6.5",
        "rc-switch": "1.4.3",
        "react": "^15.6.2",
        "react-addons-css-transition-group": "15.4.1",
        "react-addons-transition-group": "15.4.1",
        "react-async-script": "0.11.1",
        "react-bootstrap-typeahead": "^3.0.0-alpha.1",
        "react-confirm-bootstrap": "5.3.0",
        "react-copy-to-clipboard": "4.2.3",
        "react-day-picker": "7.4.10",
        "react-device-detect": "2.2.2",
        "react-dom": "^15.6.2",
        "react-dropzone": "^3.12.2",
        "react-flatpickr": "3.6.3",
        "react-ga": "^3.3.0",
        "react-geocode": "^0.1.2",
        "react-google-maps": "^9.4.5",
        "react-google-recaptcha": "0.11.1",
        "react-gtm-module": "^2.0.11",
        "react-helmet": "3.2.2",
        "react-idle-timer": "4.6.4",
        "react-inlinesvg": "0.7.5",
        "react-input-autosize": "1.1.0",
        "react-intl": "2.7.1",
        "react-lazyload": "^2.3.0",
        "react-media": "^1.8.0",
        "react-redux": "4.4.6",
        "react-router": "3.0.0",
        "react-router-redux": "4.0.6",
        "react-router-scroll": "0.4.1",
        "react-sanfona": "0.1.0",
        "react-scrollspy": "^3.3.5",
        "react-select": "^1.0.0-rc.10",
        "react-slick": "0.28.1",
        "react-stripe-elements": "^6.1.2",
        "react-table": "5.5.1",
        "react-tagsinput": "3.19.0",
        "reactstrap": "3.9.3",
        "redux": "3.6.0",
        "redux-immutable": "3.0.8",
        "redux-saga": "0.14.0",
        "reselect": "2.5.4",
        "sanitize.css": "4.1.0",
        "slick-carousel": "1.8.1",
        "socket.io-client": "^2.1.1",
        "sortablejs": "^1.6.1",
        "styled-components": "2.2.2",
        "try-thread-sleep": "^2.0.0",
        "userflow.js": "^2.8.0",
        "warning": "3.0.0",
        "whatwg-fetch": "2.0.1",
        "yup": "0.24.1"
    },
    "devDependencies": {
        "@iconify/icons-akar-icons": "^1.2.10",
        "@iconify/react": "^1.1.4",
        "aws-code-deploy": "^1.0.8",
        "babel-cli": "6.18.0",
        "babel-core": "6.21.0",
        "babel-eslint": "7.1.1",
        "babel-loader": "6.2.10",
        "babel-plugin-dynamic-import-node": "1.0.0",
        "babel-plugin-react-intl": "2.2.0",
        "babel-plugin-react-transform": "2.0.2",
        "babel-plugin-transform-es2015-modules-commonjs": "6.18.0",
        "babel-plugin-transform-react-constant-elements": "6.9.1",
        "babel-plugin-transform-react-inline-elements": "6.8.0",
        "babel-plugin-transform-react-remove-prop-types": "0.2.11",
        "babel-preset-latest": "6.16.0",
        "babel-preset-react": "6.16.0",
        "babel-preset-react-hmre": "1.1.1",
        "babel-preset-stage-0": "6.16.0",
        "cheerio": "0.22.0",
        "circular-dependency-plugin": "2.0.0",
        "coveralls": "2.11.15",
        "css-loader": "0.26.1",
        "env-cmd": "4.0.0",
        "enzyme": "2.6.0",
        "eslint": "^3.19.0",
        "eslint-config-airbnb": "13.0.0",
        "eslint-config-airbnb-base": "10.0.1",
        "eslint-import-resolver-webpack": "0.8.0",
        "eslint-plugin-import": "2.2.0",
        "eslint-plugin-jsx-a11y": "2.2.3",
        "eslint-plugin-react": "6.7.1",
        "eslint-plugin-redux-saga": "0.1.5",
        "eventsource-polyfill": "0.9.6",
        "exports-loader": "0.6.3",
        "faker": "3.1.0",
        "file-loader": "0.9.0",
        "html-loader": "0.4.4",
        "html-webpack-plugin": "2.24.1",
        "image-webpack-loader": "2.0.0",
        "imports-loader": "0.6.5",
        "jest-cli": "18.0.0",
        "lint-staged": "3.2.1",
        "ngrok": "^2.2.4",
        "node-plop": "0.5.4",
        "null-loader": "0.1.1",
        "offline-plugin": "4.5.2",
        "plop": "1.7.3",
        "pre-commit": "1.1.3",
        "react-addons-test-utils": "15.4.1",
        "rimraf": "2.5.4",
        "shelljs": "0.7.5",
        "sinon": "2.0.0-pre",
        "style-loader": "0.13.1",
        "url-loader": "0.5.7",
        "webpack": "2.3.1",
        "webpack-dev-middleware": "1.9.0",
        "webpack-hot-middleware": "2.15.0"
    },
    "resolutions": {
        "moment-timezone/moment": "2.20.1"
    }
}
