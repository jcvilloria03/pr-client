// employess module
import Employees from './utils/modules/Employees';
import ViewEmployee from './utils/modules/ViewEmployee';
import TerminateEmployee from './utils/modules/TerminateEmployee';
import EmployeeLeaves from './utils/modules/EmployeeLeaves';
import EmployeeLoans from './utils/modules/EmployeeLoans';
import EmployeeDeductions from './utils/modules/EmployeeDeductions';
import EmployeeAdjustments from './utils/modules/EmployeeAdjustments';
import EmployeeAllowances from './utils/modules/EmployeeAllowances';
import EmployeeAnnualEarnings from './utils/modules/EmployeeAnnualEarnings';
import EmployeeWorkflows from './utils/modules/EmployeeWorkflows';
import EmployeeBasicPay from './utils/modules/EmployeeBasicPay';
import EmployeeGovernmentForms from './utils/modules/EmployeeGovernmentForms';

// payroll module
import PayrollSummary from './utils/modules/PayrollSummary';
import PayrollPayslip from './utils/modules/PayrollPayslip';
import PayrollGovernmentForms from './utils/modules/PayrollGovernmentForms';
import PayrollBonuses from './utils/modules/PayrollBonuses';
import PayrollCommissions from './utils/modules/PayrollCommissions';
import GenerateRegularPayroll from './utils/modules/GenerateRegularPayroll';
import GenerateSpecialPayrollFinalPayRun from './utils/modules/GenerateSpecialPayrollFinalPayRun';

// control panel module
import SubscriptionSummary from './utils/modules/SubscriptionSummary';
import SubscriptionLicenses from './utils/modules/SubscriptionLicenses';
import SubscriptionBillingInformation from './utils/modules/SubscriptionBillingInformation';
import SubscriptionInvoices from './utils/modules/SubscriptionInvoices';
import ViewSubscriptionInvoice from './utils/modules/ViewSubscriptionInvoice';
import SubscriptionReceipts from './utils/modules/SubscriptionReceipts';
import AccountDeletion from './utils/modules/SubscriptionAccountDeletion';
import SalpayIntegrationSummary from './utils/modules/SalpayIntegrationSummary';
import SalpayIntegrationPayees from './utils/modules/SalpayIntegrationPayees';
import SalpayIntegrationAdministrators from './utils/modules/SalpayIntegrationAdministrators';
import CompanyPayrollDisbursements from './utils/modules/CompanyPayrollDisbursements';
import CompanyPayrollLoanTypeSettings from './utils/modules/CompanyPayrollLoanTypeSettings';
import ControlPanelUsers from './utils/modules/ControlPanelUsers';
import ControlPanelRoles from './utils/modules/ControlPanelRoles';
import ControlPanelAuditTrail from './utils/modules/ControlPanelAuditTrail';
import ControlPanelDeviceManagement from './utils/modules/ControlPanelDeviceManagement';
import ControlPanelCompanies from './utils/modules/ControlPanelCompanies';

// company settings module
import CompanyDetails from './utils/modules/CompanySettingsCompanyDetails';
import Locations from './utils/modules/CompanySettingsLocations';
import OrganizationalChart from './utils/modules/CompanySettingsOrganizationalChart';
import Departments from './utils/modules/CompanySettingsDepartments';
import Positions from './utils/modules/CompanySettingsPositions';
import CostCenter from './utils/modules/CompanySettingsCostCenter';
import Ranks from './utils/modules/CompanySettingsRanks';
import Teams from './utils/modules/CompanySettingsTeams';
import EmploymentTypes from './utils/modules/CompanySettingsEmploymentTypes';
import Projects from './utils/modules/CompanySettingsProjects';
import DayHourRates from './utils/modules/CompanySettingsDayHourRates';
import MaximumClockOutRules from './utils/modules/CompanySettingsMaximumClockOutRules';
import PayrollGroups from './utils/modules/CompanyPayrollPayrollGroups';
import BonusTypes from './utils/modules/CompanyPayrollBonusTypes';
import CommissionTypes from './utils/modules/CompanyPayrollCommissionTypes';
import AllowanceTypes from './utils/modules/CompanyPayrollAllowanceTypes';
import DeductionTypes from './utils/modules/CompanyPayrollDeductionTypes';
import LeaveTypes from './utils/modules/CompanySettingsLeaveTypes';
import LeaveEntitlements from './utils/modules/CompanySettingsLeaveEntitlements';
import DefaultSchedule from './utils/modules/CompanySettingsDefaultSchedule';
import TardinessRules from './utils/modules/CompanySettingsTardinessRules';
import NightShiftSettings from './utils/modules/CompanySettingsNightShiftSettings';
import Holidays from './utils/modules/CompanySettingsHolidays';
import UserManagement from './utils/modules/CompanySettingsUserManagement';
import CompanyRoles from './utils/modules/CompanySettingsCompanyRoles';
import WorkflowSettings from './utils/modules/CompanySettingsWorkflowSettings';

// time and attendance module
import TimeAttendanceSchedules from './utils/modules/TimeAttendanceSchedules';
import TimeAttendanceShifts from './utils/modules/TimeAttendanceShifts';
import TimeAttendanceApprovalsList from './utils/modules/TimeAttendanceApprovalsList';
import TimeAttendanceComputation from './utils/modules/TimeAttendanceComputation';

// dashboard modules and others
import Downloads from './utils/modules/Downloads';
import MainPageDashboard from './utils/modules/MainPageDashboard';
import MainPageAnnouncements from './utils/modules/MainPageAnnouncements';
import MainPageProfileInformation from './utils/modules/MainPageProfileInformation';

export default {
    '/dashboard': MainPageDashboard,
    '/time/company/announcements/received': MainPageAnnouncements,
    '/time/company/announcements/sent': MainPageAnnouncements,
    '/profile': MainPageProfileInformation,
    '/time/schedules/calendar': TimeAttendanceSchedules.calendar,
    '/time/schedules/table': TimeAttendanceSchedules.calendar,
    '/time/schedules/add/batch': TimeAttendanceSchedules.add.batch,
    '/time/schedules/add/manual': TimeAttendanceSchedules.add.manual,
    '/time/schedules/edit/batch': TimeAttendanceSchedules.update.batch,
    '/time/schedules/edit/:id': TimeAttendanceSchedules.update.manual,
    '/time/shifts/calendar': TimeAttendanceShifts.calendar,
    '/time/shifts/edit-shift/:id': TimeAttendanceShifts.shift.update.manual,
    '/time/shifts/edit-rest-day/:id':
        TimeAttendanceShifts.restday.update.manual,
    '/time/shifts/assign/batch': TimeAttendanceShifts.assign.batch,
    '/time/shifts/assign/manual': TimeAttendanceShifts.assign.manual,
    '/time/shifts/assign': TimeAttendanceShifts.assign.manual,
    '/company-settings/company-structure/company-details': CompanyDetails,
    '/company-settings/company-structure/locations': Locations,
    '/company-settings/company-structure/locations/add': Locations,
    '/company-settings/company-structure/locations/:id/edit': Locations,
    '/company-settings/company-structure/departments': Departments,
    '/company-settings/company-structure/departments/add': Departments,
    '/company-settings/company-structure/departments/:departmentId/edit': Departments,
    '/company-settings/company-structure/positions': Positions,
    '/company-settings/company-structure/positions/add': Positions,
    '/company-settings/company-structure/positions/:positionId/edit': Positions,
    '/company-settings/company-structure/organizational-chart':
        OrganizationalChart,
    '/company-settings/company-structure/organizational-chart/departments/add':
        OrganizationalChart,
    '/company-settings/company-structure/organizational-chart/departments/:id/edit':
        OrganizationalChart,
    '/company-settings/company-structure/organizational-chart/positions/add':
        OrganizationalChart,
    '/company-settings/company-structure/organizational-chart/positions/:id/edit':
        OrganizationalChart,
    '/company-settings/company-structure/cost-centers': CostCenter,
    '/company-settings/company-structure/cost-centers/add': CostCenter,
    '/company-settings/company-structure/ranks': Ranks,
    '/company-settings/company-structure/ranks/add': Ranks,
    '/company-settings/company-structure/teams': Teams,
    '/company-settings/company-structure/teams/add': Teams,
    '/company-settings/company-structure/teams/:id/edit': Teams,
    '/company-settings/company-structure/employment-types': EmploymentTypes,
    '/company-settings/company-structure/employment-types/add': EmploymentTypes,
    '/company-settings/company-structure/projects': Projects,
    '/company-settings/company-structure/projects/add': Projects,
    '/company-settings/time-attendance/day-hour-rates': DayHourRates,
    '/company-settings/time-attendance/maximum-clock-out-rules': MaximumClockOutRules,
    '/company-settings/time-attendance/maximum-clock-out-rules/add': MaximumClockOutRules,
    '/company-settings/payroll/payroll-groups': PayrollGroups,
    '/company-settings/payroll/payroll-groups/add/manual': PayrollGroups,
    '/company-settings/payroll/payroll-groups/add/upload': PayrollGroups,
    '/company-settings/payroll/payroll-groups/:id/edit/manual': PayrollGroups,
    '/company-settings/payroll/bonus-types': BonusTypes,
    '/company-settings/payroll/bonus-types/add': BonusTypes,
    '/company-settings/payroll/bonus-types/:id/edit': BonusTypes,
    '/company-settings/payroll/commission-types': CommissionTypes,
    '/company-settings/payroll/commission-types/add': CommissionTypes,
    '/company-settings/payroll/allowance-types': AllowanceTypes,
    '/company-settings/payroll/allowance-types/add': AllowanceTypes,
    '/company-settings/payroll/allowance-types/:id/edit': AllowanceTypes,
    '/company-settings/payroll/deduction-types': DeductionTypes,
    '/company-settings/payroll/deduction-types/add': DeductionTypes,
    '/company-settings/leave-settings/leave-types': LeaveTypes,
    '/company-settings/leave-settings/leave-types/add': LeaveTypes,
    '/company-settings/leave-settings/leave-types/:id/edit': LeaveTypes,
    '/company-settings/leave-settings/leave-entitlement': LeaveEntitlements,
    '/company-settings/leave-settings/leave-entitlement/add': LeaveEntitlements,
    '/company-settings/leave-settings/leave-entitlement/:id/edit':
        LeaveEntitlements,
    '/company-settings/schedule-settings/default-schedule': DefaultSchedule,
    '/company-settings/schedule-settings/tardiness-rules': TardinessRules,
    '/company-settings/schedule-settings/tardiness-rules/add': TardinessRules,
    '/company-settings/schedule-settings/tardiness-rules/:id/edit':
        TardinessRules,
    '/company-settings/schedule-settings/night-shift': NightShiftSettings,
    '/company-settings/schedule-settings/holidays': Holidays,
    '/company-settings/schedule-settings/holidays/add': Holidays,
    '/company-settings/schedule-settings/holidays/:id/edit': Holidays,
    '/company-settings/users/management': UserManagement,
    '/company-settings/users/create': UserManagement,
    '/company-settings/users/:userId': UserManagement,
    '/company-settings/users/:userId/edit': UserManagement,
    '/company-settings/roles/company-roles': CompanyRoles,
    '/company-settings/roles/company-roles/create': CompanyRoles,
    '/company-settings/roles/:roleId/role-edit': CompanyRoles,
    '/company-settings/workflow-automation/workflows': WorkflowSettings,
    '/company-settings/workflow-automation/workflows/add': WorkflowSettings,
    '/company-settings/workflow-automation/workflows/:id/edit': WorkflowSettings,
    '/time/company/approvals/list': TimeAttendanceApprovalsList,
    '/time/attendance-computations/': TimeAttendanceComputation,
    '/time/attendance-computations': TimeAttendanceComputation,
    '/time/attendance-computations/add/batch/': TimeAttendanceComputation,
    '/time/attendance-computations/add/batch': TimeAttendanceComputation,
    '/employees': Employees,
    '/people': Employees,
    '/employees/add': Employees,
    '/employees/batch-add': Employees,
    '/employees/batch-update': Employees,
    '/employee/:id': ViewEmployee,
    '/employee/:id/basic-pay-adjustments/add': EmployeeBasicPay,
    '/employee/:id/basic-pay-adjustments/:adjustmentId/detail': EmployeeBasicPay,
    '/employee/:id/basic-pay-adjustments/:adjustmentId/edit': EmployeeBasicPay,
    '/employee/:id/terminate': TerminateEmployee,
    '/employee/:id/termination/:terminationId/edit': TerminateEmployee,
    '/employee/:id/termination/:terminationId/checklist': TerminateEmployee,
    '/employee/:id/termination/:terminationId/computation': TerminateEmployee,

    '/payroll/loans': EmployeeLoans,
    '/payroll/loans/add': EmployeeLoans,
    '/payroll/loans/add/edit-amortization': EmployeeLoans,
    '/payroll/loans/:id/edit': EmployeeLoans,
    '/payroll/loans/:id/edit-amortization': EmployeeLoans,

    '/payroll/deductions': EmployeeDeductions,
    '/payroll/deductions/add': EmployeeDeductions,
    '/payroll/deductions/:id/edit': EmployeeDeductions,

    '/payroll/adjustments': EmployeeAdjustments,
    '/payroll/adjustments/add': EmployeeAdjustments,
    '/payroll/adjustments/:id/edit': EmployeeAdjustments,

    '/payroll/allowances': EmployeeAllowances,
    '/payroll/allowances/add': EmployeeAllowances,
    '/payroll/allowances/:id/edit': EmployeeAllowances,

    '/payroll/annual-earnings': EmployeeAnnualEarnings,
    '/payroll/annual-earnings/batch-upload': EmployeeAnnualEarnings,
    '/payroll/annual-earnings/manual-entry': EmployeeAnnualEarnings,
    '/payroll/annual-earnings/:id/detail': EmployeeAnnualEarnings,
    '/payroll/annual-earnings/:id/edit': EmployeeAnnualEarnings,

    '/payroll/payroll': PayrollSummary,
    '/payroll/payroll/:id': PayrollSummary,
    '/payroll/payroll/:id/transaction': PayrollSummary,
    '/payroll/payroll/:id/transaction/edit-amortization': PayrollSummary,

    '/payroll/payslip': PayrollPayslip,

    '/payroll/forms/SSS/LCL/:id': PayrollGovernmentForms,
    '/payroll/forms/SSS/Remittance/:id': PayrollGovernmentForms,
    '/payroll/forms/HDMF/MSRF/:id': PayrollGovernmentForms,
    '/payroll/forms/HDMF/STLRF/:id': PayrollGovernmentForms,
    '/payroll/forms/PhilHealth/MCL/:id': PayrollGovernmentForms,
    '/payroll/forms/BIR/1601C/:id': PayrollGovernmentForms,
    '/payroll/forms/BIR/1601E': PayrollGovernmentForms,
    '/payroll/forms/BIR/1601F': PayrollGovernmentForms,
    '/payroll/forms/BIR/1604C/:id': PayrollGovernmentForms,
    '/payroll/forms': PayrollGovernmentForms,
    '/payroll/forms/generate': PayrollGovernmentForms,

    '/payroll/bonuses': PayrollBonuses,
    '/payroll/bonuses/add': PayrollBonuses,
    '/payroll/bonuses/:id/edit': PayrollBonuses,
    '/payroll/bonuses/:id/detail': PayrollBonuses,

    '/payroll/commissions': PayrollCommissions,
    '/payroll/commissions/add': PayrollCommissions,
    '/payroll/commissions/:id/edit': PayrollCommissions,

    '/payroll/payroll/generate': GenerateRegularPayroll,

    '/payroll/special-pay-run': GenerateSpecialPayrollFinalPayRun,
    '/payroll/special-pay-run/add': GenerateSpecialPayrollFinalPayRun,

    '/payroll/final-pay-run': GenerateSpecialPayrollFinalPayRun,
    '/payroll/final-pay-run/add': GenerateSpecialPayrollFinalPayRun,

    '/time/leaves': EmployeeLeaves,
    '/time/leave-credits/add': EmployeeLeaves,
    '/time/filed-leaves/add': EmployeeLeaves,
    '/time/filed-leaves/:id': EmployeeLeaves,
    '/time/filed-leaves/:id/edit': EmployeeLeaves,
    '/time/leaves/leave-credits/:id/edit': EmployeeLeaves,

    '/time/workflow-entitlements': EmployeeWorkflows,
    '/time/workflow-entitlements/add': EmployeeWorkflows,

    '/employee-government-forms': EmployeeGovernmentForms,
    '/employee-government-forms/generate': EmployeeGovernmentForms,
    '/employee/:employeeId/government-forms/BIR/2316/:id': EmployeeGovernmentForms,

    '/control-panel/subscriptions/summary': SubscriptionSummary,
    '/control-panel/subscriptions/licenses': SubscriptionLicenses,
    '/control-panel/subscriptions/billing-information': SubscriptionBillingInformation,
    '/control-panel/subscriptions/invoices': SubscriptionInvoices,
    '/control-panel/subscriptions/invoices/:id': ViewSubscriptionInvoice,
    '/control-panel/subscriptions/receipts': SubscriptionReceipts,
    '/control-panel/subscriptions/receipts/:id': SubscriptionReceipts,
    '/control-panel/subscriptions/account-deletion': AccountDeletion,
    '/control-panel/companies': ControlPanelCompanies.index,
    '/control-panel/companies/add': ControlPanelCompanies.add,
    '/control-panel/companies/:id/edit': ControlPanelCompanies.edit,
    '/control-panel/device-management/users': ControlPanelDeviceManagement,
    '/control-panel/device-management/users/:userId': ControlPanelDeviceManagement,
    '/control-panel/device-management/unlinked-users': ControlPanelDeviceManagement,
    '/control-panel/users/management': ControlPanelUsers,
    '/control-panel/users/create': ControlPanelUsers,
    '/control-panel/users/:userId': ControlPanelUsers,
    '/control-panel/users/:userId/edit': ControlPanelUsers,
    '/control-panel/roles/user-roles': ControlPanelRoles,
    '/control-panel/roles/role-create': ControlPanelRoles,
    '/control-panel/roles/:roleId/role-edit': ControlPanelRoles,
    '/control-panel/audit-trail': ControlPanelAuditTrail,
    '/control-panel/salpay-integration/summary': SalpayIntegrationSummary,
    '/control-panel/salpay-integration/companies': SalpayIntegrationSummary,
    '/control-panel/salpay-integration/companies/:id/link': SalpayIntegrationSummary,
    '/control-panel/salpay-integration/companies/:id/details': SalpayIntegrationSummary,
    '/control-panel/salpay-integration/payees': SalpayIntegrationPayees,
    '/control-panel/salpay-integration/administrators': SalpayIntegrationAdministrators,
    '/control-panel/audit-trail-filters': ControlPanelAuditTrail,

    '/payroll/disbursements': CompanyPayrollDisbursements,
    '/payroll/disbursements/add': CompanyPayrollDisbursements,

    '/payroll/loan-types': CompanyPayrollLoanTypeSettings,
    '/payroll/loan-types/add': CompanyPayrollLoanTypeSettings,

    '/payroll/download(/:module/:id)': Downloads,
    '/employees/download(/:module/:id)': Employees
};
