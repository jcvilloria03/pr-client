import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  * {
    text-rendering: optimizeLegibility;
  }

  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    padding-top: 75px;
    font-family: 'Source Sans Pro', sans-serif !important;
    &.sl-is-subnav-visible {
      padding-top: 125px;
    }
  }

  label {
    font-weight: 400;
  }

  a {
    color: #00A5E5;
    transition: all .2s ease-in-out;
    text-decoration: none;

    &:hover {
      cursor: pointer;
    }
  }

  #app {
    min-width: 100%;
    color: #4e4e4e;

    // @media (min-width: 480px) {
    //   padding-bottom: 7rem;
    // }
  }

  .modal-dialog {
    margin: 20vh auto !important;
  }

  .modal-center {
    position: relative;
    top: 50%;
    margin: 0 auto !important;
    transform: translateY(-50%) !important;
  }

  .modal-header {
    border-bottom-color: #f0f4f6;
  }

  .modal-title {
    font-size: 16px;
  }

  .modal-footer {
    border-top: none;
    display: flex;
    flex-direction: row-reverse;
    justify-content: center;

    .btn {
      &:last-child {
          margin-right: 10px;
      }

      &.btn-default {
        display: inline-block;
      }
    }
  }

  input[type=number]::-webkit-outer-spin-button,
  input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  .btn.btn-default[type="button"] {
    display: none;
  }

  .modal-footer .btn.btn-default {
    display: inline-block;
  }

  select::-ms-expand {
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
    filter: alpha(opacity=0);
    opacity:0;
  }

  input[type=number] {
    -moz-appearance:textfield;
  }

  .form-control,
  .Select-control {
    border-radius: 0 !important;
  }

  .container {
    width: 80vw;
  }

  .btn {
    border-radius: 2rem;
  }

  .btn-default {
    color: #474747;
    background-color: #ffffff;
    border-color: #9fdc74;
  }

  .btn-primary {
    background-color: #9fdc74;
    border-color: #9fdc74;

    &:hover,
    &:focus,
    &:active,
    &:active:focus {
      background-color: #9fdc74;
      border-color: #9fdc74;
    }
  }

  .sl-u-text--right {
    text-align: right;
  }

  .sl-u-gap-top--lg {
    margin-top: 2rem;
  }

  .sl-u-gap-bottom {
    margin-bottom: 1rem;
  }

  .sl-u-gap-bottom--md {
    margin-bottom: 1.5rem;
  }

  .sl-u-gap-bottom--lg {
    margin-bottom: 2rem;
  }

  .sl-u-gap-bottom--xlg {
    margin-bottom: 4rem;
  }

  .sl-u-gap-left--sm {
    margin-left: .5rem;
  }

  .sl-c-circle {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 2rem;
    height: 2rem;
    color: #cccccc;
    font-size: 16px;
    border-radius: 50%;
    border: 1px solid #cccccc;
  }

  .sl-c-section {
    margin-bottom: 2rem;
  }

  .sl-c-section__header {
    margin-bottom: 1rem;
  }

  .sl-c-section-title {
    display: flex;
    align-items: center;
    margin-bottom: 1rem;
    font-size: 18px;
    color: #00A5E5;

    .sl-c-circle {
      margin-right: 1rem;
      color: #00A5E5;
      border: 1px solid #00A5E5;
    }
  }

  .sl-c-upload-area {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 28px 56px;
    border-radius: 1rem;
    border: 1px dashed #333;
  }

  .sl-c-form-group {
      margin-bottom: 1rem;
  }

  .row {
    margin-bottom: 1rem;
  }

  .input-group-addon {
    .isvg {
      svg {
        display: inline-block;
        width: 16px;
        height: 16px;
        vertical-align: middle;
      }
    }
  }

  .dayContainer {
    .flatpickr-day.selected, .flatpickr-day.selected:hover {
      border-color: #569ff7 !important;
      background: #569ff7 !important;
    }
  }


  .dayContainer.style_date_picker {

    span.flatpickr-day.selected::before {
      display: block !important;
      content: '';
      height: 40px;
      width: 1000px;
      background: #569ff7;
      position: absolute;
      z-index: -1;
      transform: translateX(-50%);
      top: -1px;
      bottom: 0;
    }
    .flatpickr-day.selected, .flatpickr-day.selected:hover {
      color: #ffffff !important;
      background: #569ff7 !important;
      border-color: #569ff7 !important;
    }
  }
  .flatpickr-weekdays{
    background: transparent !important;
  }
  .flatpickr-months .flatpickr-month{
    background: transparent !important;
    color: black;
  }

  .flatpickr-current-month .flatpickr-monthDropdown-months:hover{
    color: black;
  }

  .flatpickr-months .flatpickr-prev-month, .flatpickr-months .flatpickr-next-month{
    color: #000 !important;
  }


  .flatpickr-months .flatpickr-prev-month:hover svg, .flatpickr-months .flatpickr-next-month:hover svg {
    fill: #000 !important;
    color: black !important;
  }

  .flatpickr-months .flatpickr-prev-month:hover svg, .flatpickr-months .flatpickr-next-month:hover svg {
    fill: #000 !important;
    color: black !important;
  }

  .flatpickr-months .flatpickr-month {
    background: white !important;
    color: black !important;
    fill: black !important;

  }
  .flatpickr-weekdaycontainer span.flatpickr-weekday {
    background: transparent;
  }

  .flatpickr-current-month .flatpickr-monthDropdown-months{
    background: transparent !important;
  }
  .flatpickr-current-month .flatpickr-monthDropdown-months .flatpickr-monthDropdown-month{
    background: transparent !important;
  }
  .flatpickr-calendar:before, .flatpickr-calendar:after{
    border: none !important;
  }
  .flatpickr-current-month .numInputWrapper span.arrowDown:after {
    border-top-color: #000 !important;
  }
  .flatpickr-current-month .numInputWrapper span.arrowUp:after {
    border-bottom-color: #000 !important;
  }

  .width_of_modal{
    width: 450px !important;

    .modal-body{
      padding:14px !important;
    }

    .modal-footer{
      padding:0px !important;
    }
  }


  .sl-c-btn--primary.sl-c-btn--ghost {
    color: #474747;
    border: 1px solid #83D24B;
    background-color: #ffffff;
  }

  .modal-md {
    width: 450px;
  }

  .modal-commission-type {
    .modal-body {
      padding: 15px;
      margin-bottom: 0px;

      p {
        margin-bottom: 0px;
      }
    }
  }
`;
