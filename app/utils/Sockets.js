import Echo from 'laravel-echo';

window.io = require( 'socket.io-client' );

/**
 * Sockets class for websocket.
 */
export default class Sockets {
  /**
   * Setup broadcaster, socket host and auth.
   */
    init() {
        window.Echo = new Echo({
            broadcaster: 'socket.io',
            host: process.env.SOCKETS_URL,
            path: '/socket.io',
            auth: {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem( 'access_token' )}`
                }
            }
        });
    }

    disconnect() {
        if ( window.Echo && window.Echo.socket ) {
            window.Echo.socket.disconnect();
        }
    }
}

export const sockets = new Sockets();
