/**
 * Tag function for parsing strings with key inputs
 * @param {Array} strings string values
 * @param {Array} keys arguments related to the expressions
 */
export function template( strings, ...keys ) {
    return ( ( ...values ) => {
        const dict = values[ values.length - 1 ] || {};
        const result = [strings[ 0 ]];
        keys.forEach( ( key, i ) => {
            const value = Number.isInteger( key ) ? values[ key ] : dict[ key ];
            result.push( value, strings[ i + 1 ]);
        });
        return result.join( '' );
    });
}
