/* eslint-disable no-param-reassign */
/* eslint-disable require-jsdoc */
import decimal from 'js-big-decimal';
import moment from 'moment';
import { NEW_RECORD_SAVED_MESSAGE, DATE_FORMATS, TIME_FORMATS } from './constants';

/**
* Formats number into currency
* @param value
*/
function formatCurrency( value ) {
    if ( ( value || value === 0 ) && String( value ).length ) {
        const number = stripNonDigit( value );

        if ( !isNaN( Number( number ) ) ) {
            return decimal.getPrettyValue( decimal.round( number, 2 ) );
        }
        return value;
    }
    return '';
}

/**
 * Returns the employee's full name
 * @param {*} employee
 */
function getEmployeeFullName( employee ) {
    const firstName = employee.first_name ? `${employee.first_name} ` : '';
    const middleName = employee.middle_name ? `${employee.middle_name} ` : '';
    const lastName = employee.last_name ? employee.last_name : '';

    return `${firstName}${middleName}${lastName}`;
}

/**
 * Removes non-digit character to string except dot.
 * @param value
 */
function stripNonDigit( value ) {
    if ( value && value.toString().length ) {
        return value.toString().replace( /[^0-9.-]/g, '' );
    }
    return '';
}

/**
* Formats number into currency with only decimals ( no commas )
* @param value
*/
function formatCurrencyToDecimalNotation( value ) {
    if ( value && String( value ).length ) {
        const number = stripNonDigit( value );
        if ( !isNaN( Number( number ) ) ) {
            return decimal.round( number, 2 );
        }
        return value;
    }
    return '';
}

/**
* Formats the pagination label
* @param tableProps
*/
function formatPaginationLabel( tableProps ) {
    const page = tableProps.page + 1;
    const size = tableProps.pageSize;
    const dataLength = tableProps.dataLength || tableProps.data && tableProps.data.length;

    return `Showing ${dataLength ? ( ( page * size ) + 1 ) - size : 0} - ${( page * size ) > dataLength ? dataLength : ( page * size )} of ${dataLength} entr${dataLength > 1 || dataLength === 0 ? 'ies' : 'y'}`;
}

/**
* Formats the delete label
* @param selectionLength
*/
function formatDeleteLabel( selectionLength = 0 ) {
    return selectionLength > 0 ? `${selectionLength} entr${selectionLength > 1 ? 'ies' : 'y'} selected` : '';
}

/**
 * removes non-numeric character
 * @param value
 */
function stripNonNumeric( value ) {
    if ( value && value.toString().length ) {
        return value.toString().replace( /\D/g, '' );
    }
    return '';
}

/**
 * Removes non-alphanumberic character in string.
 * @param {String} value
 * @returns {String}
 */
function stripNonAlphaNum( value ) {
    if ( value && value.toString().length ) {
        return value.toString().replace( /[^A-Za-z0-9]/g, '' );
    }
    return '';
}

/**
 * Removes non-alphanumberic and noncharacter in string.
 * @param {String} value
 * @returns {String}
 */
function stripNonAlphaNumHypenUnderscore( value ) {
    if ( value && value.toString().length ) {
        return value.toString().replace( /[^A-Za-z0-9-_]/g, '' );
    }
    return '';
}
/**
 * transform value into TIN number format
 * @param value
 */
function formatTIN( value ) {
    if ( value ) {
        const tin = value.replace( /\D/g, '' ).split( '-' ).join( '' ).substring( 0, 12 );
        const set = [];
        set[ 0 ] = tin.substr( 0, 3 );
        if ( tin.length > 3 ) set[ 1 ] = tin.substr( 3, 3 );
        if ( tin.length > 6 ) set[ 2 ] = tin.substr( 6, 3 );
        if ( tin.length > 9 ) set[ 3 ] = tin.substr( 9, 3 );
        return set.join( '-' );
    }
    return value;
}

/**
 * Transforms value into SSS number format
 * @param {String} value
 * @returns {String}
 */
function formatSSS( value ) {
    const clean = stripNonNumeric( value ).split( '-' ).join( '' ).substring( 0, 10 );
    if ( clean ) {
        const set = [];
        set[ 0 ] = clean.substr( 0, 2 );
        if ( clean.length > 2 ) set[ 1 ] = clean.substr( 2, 7 );
        if ( clean.length > 9 ) set[ 2 ] = clean.substr( 9, 1 );
        return set.join( '-' );
    }
    return clean;
}

/**
 * Transforms value into HDMF number format
 * @param {String} value
 * @returns {String}
 */
function formatHDMF( value ) {
    const clean = stripNonNumeric( value ).split( '-' ).join( '' ).substring( 0, 12 );
    if ( clean ) {
        const set = [];
        set[ 0 ] = clean.substr( 0, 4 );
        if ( clean.length > 4 ) set[ 1 ] = clean.substr( 4, 4 );
        if ( clean.length > 8 ) set[ 2 ] = clean.substr( 8, 4 );
        return set.join( '-' );
    }
    return clean;
}

/**
 * Transforms value into PhilHealth number format
 * @param {String} value
 * @returns {String}
 */
function formatPhilHealth( value ) {
    const clean = stripNonNumeric( value ).split( '-' ).join( '' ).substring( 0, 12 );
    if ( clean ) {
        const set = [];
        set[ 0 ] = clean.substr( 0, 2 );
        if ( clean.length > 2 ) set[ 1 ] = clean.substr( 2, 9 );
        if ( clean.length > 11 ) set[ 2 ] = clean.substr( 11, 1 );
        return set.join( '-' );
    }
    return clean;
}

/**
 * Transforms value into RDO number format
 * @param {String} value
 * @returns {String}
 */
function formatRDO( value ) {
    const clean = stripNonAlphaNum( value );
    if ( clean ) {
        return clean.toUpperCase();
    }
    return clean;
}

/**
 * Formats dates.
 * @param {Date} date
 * @returns {Object}
 */
function formatDate( date, format ) {
    if ( !date ) return null;

    const momentDate = moment( date, [ DATE_FORMATS.API, DATE_FORMATS.DISPLAY, DATE_FORMATS.FULL_TIME, DATE_FORMATS.MMDDYYYY ]);
    return momentDate.isValid() ? momentDate.format( format ) : null;
}

/**
 * Format MM/DD/YYYY date
 */
function formatInputDate( value ) {
    const clean = stripNonNumeric( value ).split( '/' ).join( '' ).substring( 0, 8 );
    if ( clean ) {
        const set = [];
        set[ 0 ] = clean.substr( 0, 2 );
        if ( clean.length > 2 ) set[ 1 ] = clean.substr( 2, 2 );
        if ( clean.length > 4 ) set[ 2 ] = clean.substr( 4, 4 );
        return set.join( '/' );
    }
    return clean;
}

/**
 * Different Two Time Calculation
 */
function calculateTimeDifference( startTime, endTime ) {
    return moment.utc( moment( endTime, TIME_FORMATS.DEFAULT )
        .diff( moment( startTime, TIME_FORMATS.DEFAULT ) ) )
        .format( TIME_FORMATS.DEFAULT );
}

function calculateTotalTime( times ) {
    const totalTime = moment( '00:00', TIME_FORMATS.DEFAULT );

    times && times.forEach( ( value ) => {
        const values = value && value.split( ':' );

        totalTime.add({
            hours: +values[ 0 ],
            minutes: +values[ 1 ]
        });
    });

    return totalTime;
}
/**
 * Gets ISO weekday: Monday - 1, Sunday - 7
 * @param {String} date
 *
 * @returns {Number}
 */
function getISOWeekday( date ) {
    return moment( date ).isoWeekday();
}

/**
 * Get current year
 * @returns {Number}
 */
function getCurrentYear() {
    return moment().year();
}

/**
 * Checks if date is valid
 * @param {Date} date
 * @param {String} format
 * @returns {Boolean}
 */
function isValid( date, format ) {
    return date && moment( date, format, true ).isValid();
}

/**
 * Compare two dates
 * @param {Date} date1
 * @param {Date} date2
 * @param {Array} formats
 * @returns {Boolean}
 */
function isSameOrAfter( date1, date2, formats ) {
    return moment( date1, formats ).isSameOrAfter( date2, formats );
}

/**
 * Compare two dates
 * @param {Date} date1
 * @param {Date} date2
 * @param {Array} formats
 * @returns {Boolean}
 */
function isAfter( date1, date2, formats ) {
    return moment( date1, formats ).isAfter( date2, formats );
}

/**
 * Check if date is before today
 * @param {Date} date
 * @param {String} format
 * @returns {Boolean}
 */
function isBeforeToday( date, format ) {
    return moment( date, format ).endOf( 'day' ).isBefore( moment().endOf( 'day' ) );
}

/**
 * Check if two dates are on the same day
 * @param {Date} date1
 * @param {Date} date2
 * @param {String} format1
 * @param {String} format2
 * @returns {Boolean}
 */
function areSame( date1, date2, format1, format2 ) {
    return moment( date1, format1 ).isSame( moment( date2, format2 ), 'day' );
}

/**
 * Get data where dates are formatted
 * @param {Object} data
 * @param {String} inputFormat
 * @param {String} resultingFormat
 * @returns {Object}
 */
function getFormDataWithFormattedDates( data, inputFormat, resultingFormat ) {
    const formattedData = { ...data };
    Object.keys( formattedData ).forEach( ( key ) => {
        if ( isValid( formattedData[ key ], inputFormat ) ) {
            formattedData[ key ] = formatDate( formattedData[ key ], resultingFormat );
        }
    });

    return formattedData;
}

/**
 * Get data where currencies are formatted to numbers
 * @param {Object} data
 * @param {Array} fields
 * @returns {Object}
 */
function getFormDataWithFormattedCurrency( data, fields ) {
    const formattedData = { ...data };
    fields.forEach( ( field ) => {
        if ( Number( stripNonDigit( formattedData[ field ]) ) || formattedData[ field ] === '0.00' ) {
            formattedData[ field ] = Number( stripNonDigit( formattedData[ field ]) );
        }
    });

    return formattedData;
}

/**
 * Remove null/undefined/empty keys from data
 * @param {Object} data
 */
function transformData( payload ) {
    const data = { ...payload };
    Object.keys( data ).forEach( ( key ) => {
        if ( data[ key ] === null || data[ key ] === undefined || data[ key ] === '' ) {
            delete data[ key ];
        }
    });

    return data;
}

/**
 * Checks if any employee belongs to multiple groups
 * @param {Object} recipientsIds
 * @param {Array} employees
 * @returns {Boolean}
 */
function checkIfAnyEmployeesBelongToMultipleGroups({ employeesIds, departmentsIds, payrollGroupsIds }, employees ) {
    let isAnyEmployeeInMultipleGroups = false;

    for ( let i = 0; i < employeesIds.length; i += 1 ) {
        const employee = employees.find( ( e ) => e.id === employeesIds[ i ]);

        if ( payrollGroupsIds.includes( employee.payroll.payroll_group_id ) || departmentsIds.includes( employee.department_id ) ) {
            isAnyEmployeeInMultipleGroups = true;
            break;
        }
    }

    // we don't need to keep checking if we already found a truthy case
    if ( isAnyEmployeeInMultipleGroups ) {
        return true;
    }

    for ( let i = 0; i < departmentsIds.length; i += 1 ) {
        if ( isAnyEmployeeInMultipleGroups ) {
            break;
        }

        const employeesInDepartment = employees.filter( ( employee ) => employee.department_id === departmentsIds[ i ]);

        for ( let j = 0; j < employeesInDepartment.length; j += 1 ) {
            if ( payrollGroupsIds.includes( employeesInDepartment[ j ].payroll.payroll_group_id ) ) {
                isAnyEmployeeInMultipleGroups = true;
                break;
            }
        }
    }

    return isAnyEmployeeInMultipleGroups;
}

/**
 * Strip time in 24:00
 * @param {string} value
 */
function stripTime( value ) {
    const clean = stripNonNumeric( value );

    if ( !clean ) {
        return '';
    }

    let formatted = '';

    const time = clean.split( '' );

    if ( time[ 0 ] && parseInt( time[ 0 ], 10 ) > -1 && parseInt( time[ 0 ], 10 ) < 3 ) {
        formatted += time[ 0 ];
    }

    if ( time[ 1 ] && parseInt( time.slice( 0, 2 ).join( '' ), 10 ) > -1 && parseInt( time.slice( 0, 2 ).join( '' ), 10 ) < 24 ) {
        formatted += time[ 1 ];
    }

    if ( time[ 2 ] && parseInt( time[ 2 ], 10 ) > -1 && parseInt( time[ 2 ], 10 ) < 6 ) {
        formatted += `:${time[ 2 ]}`;
    }

    if ( time[ 3 ]) {
        formatted += time[ 3 ];
    }

    return formatted;
}

/**
 * Truncate number to a specified number of decimals
 */
function truncate( number, decimalPlaces = 2 ) {
    return Math.trunc( number * ( 10 ** decimalPlaces ) ) / ( 10 ** decimalPlaces );
}

/**
 * @param {Number} start
 * @param {Number} end
 * @returns {Array}
 */
function getRange( start, end ) {
    return Array( end - start + 1 ).fill().map( ( _, index ) => start + index );
}

/**
 * @param {String} type
 * @param {String} title
 * @param {String} message
 * @param {Boolean} show
 */
function formatFeedbackMessage( show = true, type = 'success', title = show ? NEW_RECORD_SAVED_MESSAGE : ' ', message = ' ' ) {
    return { show, title, message, type };
}

/**
 * Validation for SSS number format
 * @param {String} value
 * @returns {Boolean}
 */
function checkSSS( value ) {
    let valid = false;

    const stripped = value.replace( /\D/g, '' ).split( '-' ).join( '' );
    valid = stripped.length === 10;

    return valid;
}

/**
 * Validation for HDMF number format
 * @param {String} value
 * @returns {Boolean}
 */
function checkHDMF( value ) {
    let valid = false;

    const stripped = value.replace( /\D/g, '' ).split( '-' ).join( '' );
    valid = stripped.length === 12;

    return valid;
}

/**
 * Validation for PhilHealth number format
 * @param {String} value
 * @returns {Boolean}
 */
function checkPhilHealth( value ) {
    let valid = false;

    const stripped = value.replace( /\D/g, '' ).split( '-' ).join( '' );
    valid = stripped.length === 12;

    return valid;
}

/**
 * Validation for TIN number format
 * @param {String} value
 * @returns {Boolean}
 */
function checkTIN( value ) {
    let valid = false;

    const stripped = value.replace( /\D/g, '' ).split( '-' ).join( '' );
    valid = stripped.length === 12 || stripped.length === 9;

    return valid;
}

/**
 * Validation for RDO format
 * @param {String} value
 * @returns {Boolean}
 */
function checkRDO( value ) {
    const regexp = new RegExp( /^\d{2}(\d|[AB])[AB]?$/ );
    return regexp.test( value );
}

/**
 * Changes mobile number to desired format
 * @param {String} value - Mobile number to format
 * @returns {String}
 */
function formatMobileNumber( value ) {
    const clean = stripNonNumeric( value );
    let formatted = '';
    if ( clean.length === 0 ) {
        formatted = '';
    } else if ( clean.length > 2 ) {
        formatted += `(${clean.substring( 0, 2 )})${clean.substring( 2, 12 )}`;
    } else {
        formatted += `(${clean}`;
    }

    return formatted;
}

/**
 * Formats number into currency
 * @param {Number|String} value
 * @returns {String}
 */
function formatNumber( value, group = true, round = 2 ) {
    if ( value && String( value ).length ) {
        const number = stripNonDigit( value );

        if ( !isNaN( Number( number ) ) ) {
            if ( group ) {
                return decimal.getPrettyValue( decimal.round( number, round ) );
            }
            return decimal.round( number, round );
        }
        return value;
    }
    return '';
}

/**
 * Formats full name
 * @param {Object} name
 * @param {Boolean} bySurname - If set to `true`, formats to `Surname, First Name (Middle Name)`. Otherwise `First Name (Middle Name) Surname`
 * @returns {String} Formatted full name
 */
function formatFullName({ first_name: firstName = '', middle_name: middleName = '', last_name: lastName = '' }, bySurname = true ) {
    if ( bySurname ) {
        return `${lastName ? `${lastName}, ` : ''}${firstName}${middleName ? ` ${middleName}` : ''}`;
    }

    return `${firstName}${middleName ? ` ${middleName}` : ''}${lastName ? ` ${lastName}` : ''}`;
}

/**
 * Format date to display
 * @param {string} time
 * @param {string} format
 * @returns {string}
 */
function formatDateToDisplay( date, format ) {
    return moment( date ).format( format );
}

export {
    formatCurrencyToDecimalNotation,
    stripNonDigit,
    formatCurrency,
    formatPaginationLabel,
    formatDeleteLabel,
    getEmployeeFullName,
    stripNonNumeric,
    stripNonAlphaNum,
    stripNonAlphaNumHypenUnderscore,
    formatTIN,
    formatSSS,
    formatHDMF,
    formatPhilHealth,
    formatRDO,
    formatDate,
    formatInputDate,
    getCurrentYear,
    getRange,
    isValid,
    isAfter,
    isSameOrAfter,
    areSame,
    isBeforeToday,
    getFormDataWithFormattedDates,
    getFormDataWithFormattedCurrency,
    transformData,
    checkIfAnyEmployeesBelongToMultipleGroups,
    stripTime,
    truncate,
    formatFeedbackMessage,
    getISOWeekday,
    checkSSS,
    checkHDMF,
    checkPhilHealth,
    checkTIN,
    checkRDO,
    formatMobileNumber,
    formatNumber,
    formatFullName,
    calculateTimeDifference,
    formatDateToDisplay,
    calculateTotalTime
};
