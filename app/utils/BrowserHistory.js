import { browserHistory as routerBrowserHistory } from 'react-router';
import { BASE_PATH_NAME } from '../constants';

/**
 * React router browserHistory wrapper
 */
export default class BrowserHistory {
    push( path, withoutBasePath = false ) {
        const basePathName = withoutBasePath ? '' : BASE_PATH_NAME;
        return routerBrowserHistory.push( `${basePathName}${path}` );
    }

    pushWithParam( path, withoutBasePath = false, params = {}) {
        const basePathName = withoutBasePath ? '' : BASE_PATH_NAME;
        return routerBrowserHistory.push({ pathname: `${basePathName}${path}`, state: params });
    }

    replace( path, withoutBasePath = false, params = {}) {
        const basePathName = withoutBasePath ? '' : BASE_PATH_NAME;
        return routerBrowserHistory.replace({ pathname: `${basePathName}${path}`, state: params });
    }

    getCurrentLocation() {
        return routerBrowserHistory.getCurrentLocation();
    }

    goBack() {
        return routerBrowserHistory.goBack();
    }
}

export const browserHistory = new BrowserHistory();
