
import cloneDeep from 'lodash/cloneDeep';
import values from 'lodash/values';
import clone from 'lodash/clone';
import isEmpty from 'lodash/isEmpty';
import groupBy from 'lodash/groupBy';
import each from 'lodash/each';

import moment from 'moment';
import 'moment-recur';

import { DATE_FORMATS } from './constants';

/**
 * Rest Day Service
 */
export default class RestDayService {

    setDates( startDate, endDate ) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    handleRestDaysDateRange( response ) {
        const restDays = this.processRestDays( response, {
            start: moment( this.startDate ).format( DATE_FORMATS.API ),
            end: moment( this.endDate ).format( DATE_FORMATS.API )
        });

        return this.parseEmployeeRestDays( restDays );
    }

    processRestDays = ( restDays, interval ) => {
        let restDaysList = [];

        each( restDays, ( restDay ) => {
            restDaysList = restDaysList.concat( this.processRestDay( restDay, interval ) );
        });

        return restDaysList;
    }

    processRestDay( restDay, interval ) {
        restDay.original = cloneDeep( restDay ); // eslint-disable-line

        if ( !restDay.repeat ) {
          // If it's non-repeating rest day we need to check if it's inside the viewed
          // interval so that we don't give it to be rendered by mistake.
            if ( !this.isRestDayStartDateInsideInterval( restDay, interval ) ) {
                return [];
            }

            return [this.processSingleEvent( restDay )];
        }

        // No need to process further restDay if it's not before the end of interval.
        if ( moment( restDay.start_date ).isAfter( interval.end ) ) {
            return [];
        }

        const intervalEnd = this.getIntervalEnd( restDay, interval.end );

        return this.processRepeated( restDay, interval.start, intervalEnd );
    }

    isRestDayStartDateInsideInterval( restDay, interval ) {
        const startDate = moment( restDay.start_date );

        return startDate.isBetween( interval.start, interval.end, 'day' ) ||
            startDate.isSame( interval.start, 'day' ) ||
            startDate.isSame( interval.end, 'day' );
    }

    processSingleEvent( restDay ) {
        restDay.start = restDay.start_date; // eslint-disable-line
        restDay.end = this.getEventEnd( restDay ); // eslint-disable-line

        return restDay;
    }

    getEventEnd( restDay ) {
        return `${restDay.start_date} 23:59:59`;
    }

    getIntervalEnd( restDay, intervalEnd ) {
        if ( moment( restDay.end_date ).isBefore( moment( intervalEnd ) ) ) {
            return restDay.end_date;
        }

        return intervalEnd;
    }

    processRepeated( restDay, intervalStart, intervalEnd ) {
        if ( restDay && !restDay.repeat ) return;

        // Define recurrence
        const recurrence = moment( restDay.start_date, '' ).recur( intervalEnd )
            .daysOfWeek( restDay.repeat.rest_days );

        const dates = this.getWeeklyOccurrenceDates( restDay, recurrence, intervalStart );
        // Make restDay for every occurrence.
        return this.mapDatesToRestDays( dates, restDay );  // eslint-disable-line consistent-return
    }

    getWeeklyOccurrenceDates( restDay, recurrence, intervalStart ) {
        const groupedDates = values( groupBy( recurrence.all(), ( date ) =>
            date.format( 'YYYY' ) + date.week() ) );

        let allDates = [];

        // Get only every nth week
        groupedDates.forEach( ( dates, index ) => {
            if ( ( index % restDay.repeat.repeat_every ) === 0 ) {
                allDates = allDates.concat( dates );
            }
        });

        return this.getRequestedNumberOfOccurrencesAfterIntervalStart( allDates, restDay, intervalStart );
    }

    getRequestedNumberOfOccurrencesAfterIntervalStart( allDates, restDay, intervalStart ) {
        // Get only requested number of occurrences
        if ( restDay.repeat.end_after ) {
            allDates = allDates.splice( 0, restDay.repeat.end_after ); // eslint-disable-line no-param-reassign
        }

        // Get only dates after inside interval
        return allDates.filter( ( date ) => date.isSameOrAfter( intervalStart ) )
            .map( ( date ) => date.format( 'YYYY-MM-DD' ) );
    }

    mapDatesToRestDays( dates, restDay ) {
        return dates.map( ( date ) => {
            const event = clone( restDay );

            event.start_date = date;

            return this.processSingleEvent( event );
        });
    }

    parseEmployeeRestDays( restDays ) {
        const parsedRestDays = {};

        restDays.forEach( ( restDay ) => {
            if ( isEmpty( restDay ) || !this.isShiftDateInDateRange( restDay.start_date ) ) return;

            const parsedRestDay = {
                date: restDay.start_date,
                isAssigned: true,
                name: 'Rest Day',
                rest_day_id: restDay.id,
                isRestDay: true,
                schedule: {
                    name: 'Rest Day'
                }
            };

            parsedRestDays[ restDay.start_date ] = parsedRestDay;
        });

        return parsedRestDays;
    }

    isShiftDateInDateRange( date ) {
        const rangeStart = moment( this.startDate ).format( DATE_FORMATS.API );
        const rangeEnd = moment( this.endDate ).format( DATE_FORMATS.API );

        return moment( date, DATE_FORMATS.API ).isBetween( rangeStart, rangeEnd, null, []);
    }
}

export const restDayService = new RestDayService();
