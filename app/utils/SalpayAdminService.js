
import { auth } from 'utils/AuthService';

/**
 * Salpay Admin helper
 */
export default class SalpayAdminService {

    isSalpayAdmin( companyId ) {
        const user = JSON.parse( localStorage.getItem( 'user' ) );

        if ( user && user.salpay_settings ) {
            const salpayCompany = user.salpay_settings.find( ( e ) => e.company_id === companyId );

            return Boolean( salpayCompany && salpayCompany.is_salpay_admin );
        }
        return false;
    }

    isAccountOwner() {
        const user = auth.getUser();

        return user && user.user_type === 'owner';
    }

    isAllowedToInvite( companyId ) {
        const user = JSON.parse( localStorage.getItem( 'user' ) );

        if ( user && user.salpay_settings ) {
            const salpayCompany = user.salpay_settings.find( ( e ) => e.company_id === companyId );

            return Boolean( salpayCompany && salpayCompany.salarium_admin_settings
                && salpayCompany.salarium_admin_settings.payee_invite );
        }

        return false;
    }
}

export const salpayAdminService = new SalpayAdminService();
