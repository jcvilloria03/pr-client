export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/announcements$/,
            modules: [
                'main_page.announcements'
            ]
        },
        {
            endpoint: '/user/informations',
            modules: [
                'main_page.announcements'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/affected_employees\/search$/,
            modules: [
                'main_page.announcements'
            ]
        },
        {
            endpoint: /^\/announcement\/(\d+)$/,
            modules: [
                'main_page.announcements'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/announcements\/download$/,
            modules: [
                'main_page.announcements'
            ]
        },
        {
            endpoint: /^\/announcement\/(\d+)\/reply$/,
            modules: [
                'main_page.announcements'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/inactive_employees$/,
            modules: [
                'main_page.announcements'
            ]
        },
        {
            endpoint: '/announcement',
            modules: [
                'main_page.announcements'
            ]
        },
        {
            endpoint: /\/company\/(\d+)\/announcement\/recipients\/download/,
            modules: [
                'main_page.announcements'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/announcement\/(\d+)\/recipient_seen$/,
            modules: [
                'main_page.announcements'
            ]
        }
    ]
};
