export default {
    GET: [
        {
            endpoint: /^\/account$/,
            modules: [
                'control_panel.subscriptions.invoices_tab'
            ]
        },
        {
            endpoint: /^\/countries$/,
            modules: [
                'control_panel.subscriptions.invoices_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/invoices$/,
            modules: [
                'control_panel.subscriptions.invoices_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/invoices\/(\d+)$/,
            modules: [
                'control_panel.subscriptions.invoices_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/billing_information$/,
            modules: [
                'control_panel.subscriptions.invoices_tab',
                'control_panel.subscriptions.invoices_tab.billing_information'
            ]
        },
        {
            endpoint: /^\/subscription-payment\/invoice\/payment\/status$/,
            modules: [
                'control_panel.subscriptions.invoices_tab'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/subscriptions\/billing_information$/,
            modules: [
                'control_panel.subscriptions.invoices_tab.billing_information'
            ]
        }
    ]
};
