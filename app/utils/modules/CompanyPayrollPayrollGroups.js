export default {
    GET: [
        {
            endpoint: /^\/philippine\/company\/(\d+)\/payroll_groups$/,
            modules: [
                'company_settings.company_payroll',
                'company_settings.company_payroll.payroll_groups'
            ]
        },
        {
            endpoint: /^\/philippine\/payroll_group\/(\d+)$/,
            modules: [
                'company_settings.company_payroll',
                'company_settings.company_payroll.payroll_groups'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payrolls$/,
            modules: [
                'company_settings.company_payroll',
                'company_settings.company_payroll.payroll_groups'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/philippine\/payroll_group$/,
            modules: [
                'company_settings.company_payroll',
                'company_settings.company_payroll.payroll_groups'
            ]
        },
        {
            endpoint: /^\/payroll_group\/upload\/save$/,
            modules: [
                'company_settings.company_payroll',
                'company_settings.company_payroll.payroll_groups'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payroll_group\/is_name_available$/,
            modules: [
                'company_settings.company_payroll',
                'company_settings.company_payroll.payroll_groups'
            ]
        },
        {
            endpoint: /^\/payroll_group\/bulk_delete$/,
            modules: [
                'company_settings.company_payroll',
                'company_settings.company_payroll.payroll_groups'
            ]
        },
        {
            endpoint: /^\/payroll_group\/upload$/,
            modules: [
                'company_settings.company_payroll',
                'company_settings.company_payroll.payroll_groups'
            ]
        },
        {
            endpoint: '/company/payroll_group/is_in_use',
            modules: [
                'company_settings.company_payroll',
                'company_settings.company_payroll.payroll_groups'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/philippine\/payroll_group\/(\d+)$/,
            modules: [
                'company_settings.company_payroll',
                'company_settings.company_payroll.payroll_groups'
            ]
        },
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_payroll.payroll_groups'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/payroll_group\/bulk_delete$/,
            modules: [
                'company_settings.company_payroll',
                'company_settings.company_payroll.payroll_groups'
            ]
        }
    ]
};
