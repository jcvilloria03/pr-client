export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/deduction_type$/,
            modules: [
                'employees.deductions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees$/,
            modules: [
                'employees.deductions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees\/search$/,
            modules: [
                'employees.deductions'
            ]
        },
        {
            endpoint: /^\/account\/payroll_groups$/,
            modules: [
                'employees.deductions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/deduction\/upload\/status$/,
            modules: [
                'employees.deductions'
            ]
        },
        {
            endpoint: /^\/other_income\/(\d+)$/,
            modules: [
                'employees.deductions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/deduction_type$/,
            modules: [
                'employees.deductions'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/locations$/,
            modules: [
                'employees.deductions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'employees.deductions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'employees.deductions'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/philippine\/company\/(\d+)\/deduction\/bulk_create$/,
            modules: [
                'employees.deductions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/deduction\/upload$/,
            modules: [
                'employees.deductions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/deduction_type\/download$/,
            modules: [
                'employees.deductions'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/philippine\/deduction\/(\d+)$/,
            modules: [
                'employees.deductions'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/company\/(\d+)\/other_income$/,
            modules: [
                'employees.deductions'
            ]
        }
    ]
};
