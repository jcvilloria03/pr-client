export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/day_hour_rates$/,
            modules: [
                'company_settings.day_hour_rates'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/day_hour_rate\/(\d+)$/,
            modules: [
                'company_settings.day_hour_rates'
            ]
        },
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.day_hour_rates'
            ]
        }
    ]
};
