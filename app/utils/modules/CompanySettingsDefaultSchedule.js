export default {
    GET: [
        {
            endpoint: /^\/default_schedule$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.default_schedule'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/default_schedule\/bulk_create$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.default_schedule'
            ]
        },
        {
            endpoint: /^\/default_schedule\/related_requests_to_updated_days_of_week$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.default_schedule'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.schedule_settings.default_schedule'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/default_schedule\/bulk_update$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.default_schedule'
            ]
        }
    ]
};
