export default {
    index: {
        GET: [
            {
                endpoint: /^\/account\/(\S+)\/companies$/,
                modules: [
                    'control_panel.companies'
                ]
            }
        ],
        POST: [
            {
                endpoint: '/company/company/is_in_use',
                modules: [
                    'control_panel.companies'
                ]
            }
        ],
        DELETE: [
            {
                endpoint: '/company/bulk_delete',
                modules: [
                    'control_panel.companies'
                ]
            }
        ]
    },
    add: {
        POST: [
            {
                endpoint: '/philippine/company',
                modules: [
                    'control_panel.companies'
                ]
            }
        ]
    },
    edit: {
        GET: [
            {
                endpoint: /^\/philippine\/company\/(\d+)$/,
                modules: [
                    'control_panel.companies.company_information',
                    'control_panel.companies.government_issued_id_number',
                    'control_panel.companies.contact_information'
                ]
            }
        ],
        POST: [
            {
                endpoint: /^\/company\/(\d+)$/,
                modules: [
                    'control_panel.companies',
                    'control_panel.companies.company_information',
                    'control_panel.companies.government_issued_id_number',
                    'control_panel.companies.contact_information'
                ]
            },
            {
                endpoint: '/philippine/company',
                modules: [
                    'control_panel.companies.company_information',
                    'control_panel.companies.government_issued_id_number',
                    'control_panel.companies.contact_information'
                ]
            }
        ]
    }
};
