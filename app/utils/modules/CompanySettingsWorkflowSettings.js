export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/workflows$/,
            modules: [
                'company_settings.workflow_automation'
            ]
        },
        {
            endpoint: /^\/workflow\/(\d+)$/,
            modules: [
                'company_settings.workflow_automation'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/affected_entities\/search$/,
            modules: [
                'company_settings.workflow_automation'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/workflow\/is_name_available$/,
            modules: [
                'company_settings.workflow_automation'
            ]
        },
        {
            endpoint: /^\/workflow$/,
            modules: [
                'company_settings.workflow_automation'
            ]
        },
        {
            endpoint: /^\/workflow\/bulk_delete$/,
            modules: [
                'company_settings.workflow_automation'
            ]
        },
        {
            endpoint: '/workflow/check_in_use',
            modules: [
                'company_settings.workflow_automation'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/workflow\/(\d+)$/,
            modules: [
                'company_settings.workflow_automation'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.workflow_automation'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/workflow\/bulk_delete$/,
            modules: [
                'company_settings.workflow_automation'
            ]
        }
    ]
};
