export default {
    GET: [
        {
            endpoint: /^\/account$/,
            modules: [
                'control_panel.subscriptions.invoices_tab'
            ]
        },
        {
            endpoint: /^\/countries$/,
            modules: [
                'control_panel.subscriptions.invoices_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/invoices\/(\d+)$/,
            modules: [
                'control_panel.subscriptions.invoices_tab',
                'control_panel.subscriptions.invoices_tab.invoice_history'
            ]
        },
        {
            endpoint: /^\/subscriptions\/invoices\/(\d+)\/billed_users$/,
            modules: [
                'control_panel.subscriptions.invoices_tab',
                'control_panel.subscriptions.invoices_tab.invoice_history'
            ]
        },
        {
            endpoint: /^\/subscriptions\/invoices\/(\d+)\/payments\/generate_paynamics$/,
            modules: [
                'control_panel.subscriptions.invoices_tab.invoice_history'
            ]
        },
        {
            endpoint: /^\/subscriptions\/billing_information$/,
            modules: [
                'control_panel.subscriptions.invoices_tab',
                'control_panel.subscriptions.invoices_tab.billing_information'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/subscriptions\/invoices\/(\d+)\/payments\/paypal\/orders$/,
            modules: [
                'control_panel.subscriptions.invoices_tab.invoice_history'
            ]
        },
        {
            endpoint: /^\/subscriptions\/invoices\/(\d+)\/payments\/paypal\/capture\/(.*)$/,
            modules: [
                'control_panel.subscriptions.invoices_tab.invoice_history'
            ]
        },
        {
            endpoint: /^\/subscription-payment\/invoice\/(\d+)\/payment-intent$/,
            modules: [
                'control_panel.subscriptions.invoices_tab.invoice_history'
            ]
        }
    ]
};
