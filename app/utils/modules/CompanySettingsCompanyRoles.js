export default {
    GET: [
        {
            endpoint: /^\/account\/role\/(\d+)\/assigned_users$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: '/role_templates',
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/roles$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: /^\/account\/role\/(\d+)$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: '/account/roles/tasks',
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: /^\/companies\/(\d+)\/roles$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: /^\/companies\/(\d+)\/roles\/(\d+)$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: /^\/accounts\/(\d+)\/companies\/(\d+)\/essential_data$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: '/account/philippine/companies',
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/account\/role\/is_name_available$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: /^\/account\/role$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: /^\/companies\/(\d+)\/roles$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: /^\/account\/roles\/bulk_delete$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/account\/role\/(\d+)$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/companies\/(\d+)\/roles\/(\d+)$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/companies\/(\d+)\/roles\/(\d+)$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        },
        {
            endpoint: /^\/account\/roles\/bulk_delete$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.company_roles'
            ]
        }
    ]
};
