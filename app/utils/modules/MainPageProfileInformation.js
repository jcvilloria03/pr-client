export default {
    GET: [{
        endpoint: /^\/user\/(\d+)$/,
        modules: [
            'main_page.profile_information'
        ]
    }],
    POST: [{
        endpoint: '/user/informations',
        modules: [
            'main_page.profile_information'
        ]
    }]
};
