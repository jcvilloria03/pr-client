export default {
    GET: [
        {
            endpoint: /^\/other_income_type\/(\d+)$/,
            modules: [
                'company_settings.company_payroll.bonus_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/bonus_type$/,
            modules: [
                'company_settings.company_payroll.bonus_types'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/philippine\/company\/(\d+)\/bonus_type\/bulk_create$/,
            modules: [
                'company_settings.company_payroll.bonus_types'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/bonus_type\/is_name_available$/,
            modules: [
                'company_settings.company_payroll.bonus_types'
            ]
        },
        {
            endpoint: /^\/other_income_type\/is_edit_available$/,
            modules: [
                'company_settings.company_payroll.bonus_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_type$/,
            modules: [
                'company_settings.company_payroll.bonus_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_type\/is_delete_available$/,
            modules: [
                'company_settings.company_payroll.bonus_types'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/philippine\/bonus_type\/(\d+)$/,
            modules: [
                'company_settings.company_payroll.bonus_types'
            ]
        },
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_payroll.bonus_types'
            ]
        }
    ]
};
