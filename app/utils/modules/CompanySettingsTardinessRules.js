export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/tardiness_rules$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.tardiness_rules'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/affected_employees\/search$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.tardiness_rules'
            ]
        },
        {
            endpoint: /^\/tardiness_rule\/(\d+)$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.tardiness_rules'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/tardiness_rule\/is_name_available$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.tardiness_rules'
            ]
        },
        {
            endpoint: /^\/tardiness_rule$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.tardiness_rules'
            ]
        },
        {
            endpoint: /^\/tardiness_rule\/check_in_use$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.tardiness_rules'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.tardiness_rules'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/tardiness_rule\/(\d+)$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.tardiness_rules'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/tardiness_rule\/bulk_delete$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.tardiness_rules'
            ]
        }
    ]
};
