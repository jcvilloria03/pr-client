export default {
    GET: [
        {
            endpoint: /^\/account\/(\S+)\/companies$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/account\/payroll_groups$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/account\/roles\/tasks$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/account\/role\/(\d+)\/assigned_users$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/account\/role\/(\d+)$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/role_templates$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/account\/roles\/all$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/accounts\/(\d+)\/roles$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/accounts\/(\d+)\/roles\/(\d+)$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/accounts\/(\d+)\/essential_data$/,
            modules: [
                'control_panel.roles'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/account\/role\/is_name_available$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/account\/role$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/accounts\/(\d+)\/roles$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/account\/roles\/bulk_delete$/,
            modules: [
                'control_panel.roles'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/account\/role\/(\d+)$/,
            modules: [
                'control_panel.roles'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/accounts\/(\d+)\/roles\/(\d+)$/,
            modules: [
                'control_panel.roles'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/account\/roles\/bulk_delete$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/accounts\/(\d+)\/roles\/(\d+)$/,
            modules: [
                'control_panel.roles'
            ]
        }
    ]
};
