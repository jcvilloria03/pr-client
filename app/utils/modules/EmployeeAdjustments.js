export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/adjustment_type$/,
            modules: [
                'employees.adjustments'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees$/,
            modules: [
                'employees.adjustments'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/adjustment\/upload\/status$/,
            modules: [
                'employees.adjustments'
            ]
        },
        {
            endpoint: /^\/other_income\/(\d+)$/,
            modules: [
                'employees.adjustments'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/adjustment_type$/,
            modules: [
                'employees.adjustments'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/locations$/,
            modules: [
                'employees.adjustments'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'employees.adjustments'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'employees.adjustments'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/philippine\/company\/(\d+)\/adjustment\/bulk_create$/,
            modules: [
                'employees.adjustments'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/adjustment\/upload$/,
            modules: [
                'employees.adjustments'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/adjustment_type\/download$/,
            modules: [
                'employees.adjustments'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/philippine\/adjustment\/(\d+)$/,
            modules: [
                'employees.adjustments'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/company\/(\d+)\/other_income$/,
            modules: [
                'employees.adjustments'
            ]
        }
    ]
};
