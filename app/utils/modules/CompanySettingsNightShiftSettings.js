export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/night_shift$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.night_shift'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.schedule_settings.night_shift'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/night_shift\/(\d+)$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.night_shift'
            ]
        }
    ]
};
