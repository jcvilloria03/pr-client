export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/allowance_type$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/allowance_type$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/allowance\/upload\/status$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/company\/other_income\/allowance\/upload\/preview$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/other_income\/(\d+)$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/allowance_type$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/locations$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'employees.allowances'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/philippine\/company\/(\d+)\/allowance\/bulk_create$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/allowance\/upload$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/allowance_type\/download$/,
            modules: [
                'employees.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/allowance\/upload\/save$/,
            modules: [
                'employees.allowances'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/philippine\/allowance\/(\d+)$/,
            modules: [
                'employees.allowances'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/company\/(\d+)\/other_income$/,
            modules: [
                'employees.allowances'
            ]
        }
    ]
};
