export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/payslips$/,
            modules: [
                'payroll.payslips'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payslips\/authorized_payroll_groups$/,
            modules: [
                'payroll.payslips'
            ]
        },
        {
            endpoint: /^\/payslip\/(\d+)$/,
            modules: [
                'payroll.payslips'
            ]
        },
        {
            endpoint: /^\/payslip\/zipped\/(\S+)\/url$/,
            modules: [
                'payroll.payslips'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/payroll\/(\d+)\/send_single_payslip$/,
            modules: [
                'payroll.payslips'
            ]
        },
        {
            endpoint: /^\/payslip\/download_multiple$/,
            modules: [
                'payroll.payslips'
            ]
        }
    ]
};
