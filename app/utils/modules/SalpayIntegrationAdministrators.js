export default {
    GET: [
        {
            endpoint: /^\/account$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/users\/(\d+)\/companies$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/employees$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/salpay-business\/administrators$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/salpay-account\/administrators$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/salpay-settings\/(\d+)$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/salpay-account\/administrators$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/salpay-settings$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/salpay-account\/administrators\/(\d+)$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        }
    ]
};
