export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/commission_type$/,
            modules: [
                'payroll.commissions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees$/,
            modules: [
                'payroll.commissions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'payroll.commissions'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/payroll_groups$/,
            modules: [
                'payroll.commissions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/commission\/upload\/status$/,
            modules: [
                'payroll.commissions'
            ]
        },
        {
            endpoint: /^\/other_income\/(\d+)$/,
            modules: [
                'payroll.commissions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/commission_type$/,
            modules: [
                'payroll.commissions'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/philippine\/company\/(\d+)\/commission\/bulk_create$/,
            modules: [
                'payroll.commissions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/commission\/upload$/,
            modules: [
                'payroll.commissions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/is_delete_available$/,
            modules: [
                'payroll.commissions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/commission_type\/download$/,
            modules: [
                'payroll.commissions'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/philippine\/commission\/(\d+)$/,
            modules: [
                'payroll.commissions'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/company\/(\d+)\/other_income$/,
            modules: [
                'payroll.commissions'
            ]
        }
    ]
};
