export default {
    GET: [
        {
            endpoint: /^\/account$/,
            modules: [
                'control_panel.subscriptions.subscriptions_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/(\d+)\/stats$/,
            modules: [
                'control_panel.subscriptions.subscriptions_tab'
            ]
        },
        {
            endpoint: /^\/subscription-payment\/payment-method\/current$/,
            modules: [
                'control_panel.subscriptions.subscriptions_tab'
            ]
        },
        {
            endpoint: /^\/subscription-payment\/invoice\/payment-status$/,
            modules: [
                'control_panel.subscriptions.subscriptions_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/billing_information$/,
            modules: [
                'control_panel.subscriptions.invoices_tab',
                'control_panel.subscriptions.invoices_tab.billing_information'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/subscription-payment\/payment-method\/setup-intent$/,
            modules: [
                'control_panel.subscriptions.subscriptions_tab'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/subscriptions\/(\d+)\/plan\/update$/,
            modules: [
                'control_panel.subscriptions.licenses_tab'
            ]
        }
    ]
};
