export default {
    GET: [
        {
            endpoint: /^\/subscriptions\/(\d+)\/stats$/,
            modules: [
                'control_panel.users'
            ]
        },
        {
            endpoint: /^\/account$/,
            modules: [
                'control_panel.users'
            ]
        },
        {
            endpoint: /^\/account\/users$/,
            modules: [
                'control_panel.users'
            ]
        },
        {
            endpoint: /^\/available_product_seats$/,
            modules: [
                'control_panel.users'
            ]
        },
        {
            endpoint: /^\/account\/(\S+)\/companies$/,
            modules: [
                'control_panel.users'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/roles$/,
            modules: [
                'control_panel.roles'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees\/search$/,
            modules: [
                'control_panel.users'
            ]
        },
        {
            endpoint: /^\/user\/(\d+)$/,
            modules: [
                'control_panel.users'
            ]
        },
        {
            endpoint: /^\/accounts\/(\d+)\/roles$/,
            modules: [
                'control_panel.roles'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/user\/(\d+)\/resend_verification$/,
            modules: [
                'control_panel.users'
            ]
        },
        {
            endpoint: /^\/user$/,
            modules: [
                'control_panel.users'
            ]
        },
        {
            endpoint: /^\/user\/bulk_delete$/,
            modules: [
                'control_panel.users'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/user\/(\d+)$/,
            modules: [
                'control_panel.users'
            ]
        },
        {
            endpoint: /^\/user\/(\d+)\/set_status$/,
            modules: [
                'control_panel.users'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/user\/bulk_delete$/,
            modules: [
                'control_panel.users'
            ]
        }
    ]
};
