export default {
    calendar: {
        GET: [
            {
                endpoint: /^\/company\/(\d+)\/shifts_data$/,
                modules: [
                    'time_and_attendance.shifts'
                ]
            },
            {
                endpoint: '/default_schedule',
                modules: [
                    'time_and_attendance.shifts'
                ]
            },
            {
                endpoint: /^\/company\/(\d+)\/schedules$/,
                modules: [
                    'time_and_attendance.shifts'
                ]
            },
            {
                endpoint: /^\/company\/(\d+)\/time_attendance_locations$/,
                modules: [
                    'time_and_attendance.shifts'
                ]
            },
            {
                endpoint: /^\/company\/(\d+)\/departments$/,
                modules: [
                    'time_and_attendance.shifts'
                ]
            },
            {
                endpoint: /^\/company\/(\d+)\/positions$/,
                modules: [
                    'time_and_attendance.shifts'
                ]
            }
        ],
        POST: [
            {
                endpoint: /^\/company\/(\d+)\/shifts\/generate_csv$/,
                modules: [
                    'time_and_attendance.shifts'
                ]
            },
            {
                endpoint: '/shift/overlapping_requests',
                modules: [
                    'time_and_attendance.shifts'
                ]
            },
            {
                endpoint: /^\/rest_day\/unassign\/(\d+)$/,
                modules: [
                    'time_and_attendance.shifts.assign_rest_day'
                ]
            },
            {
                endpoint: '/shift',
                modules: [
                    'time_and_attendance.shifts.assign_shift'
                ]
            },
            {
                endpoint: '/rest_day',
                modules: [
                    'time_and_attendance.shifts.assign_rest_day'
                ]
            }
        ],
        PUT: [
            {
                endpoint: /^\/shift\/unassign\/(\d+)$/,
                modules: [
                    'time_and_attendance.shifts'
                ]
            }
        ]
    },
    shift: {
        update: {
            manual: {
                GET: [
                    {
                        endpoint: /^\/shift\/(\d+)$/,
                        modules: [
                            'time_and_attendance.shifts.assign_shift'
                        ]
                    }
                ],
                PUT: [
                    {
                        endpoint: /^\/shift\/(\d+)$/,
                        modules: [
                            'time_and_attendance.shifts.assign_shift'
                        ]
                    }
                ]
            }
        }
    },
    restday: {
        update: {
            manual: {
                GET: [
                    {
                        endpoint: /^\/rest_day\/(\d+)$/,
                        modules: [
                            'time_and_attendance.shifts.assign_rest_day'
                        ]
                    }
                ],
                PUT: [
                    {
                        endpoint: /^\/rest_day\/(\d+)$/,
                        modules: [
                            'time_and_attendance.shifts.assign_rest_day'
                        ]
                    }
                ]
            }
        }
    },
    assign: {
        batch: {
            GET: [
                {
                    endpoint: /^\/company\/(\d+)\/batch_assign_shifts\/status$/,
                    modules: [
                        'time_and_attendance.shifts.assign_shift'
                    ]
                }
            ],
            POST: [
                {
                    endpoint: /^\/company\/(\d+)\/batch_assign_shifts\/validate$/,
                    modules: [
                        'time_and_attendance.shifts.assign_shift'
                    ]
                },
                {
                    endpoint: /^\/company\/(\d+)\/batch_assign_shifts\/save$/,
                    modules: [
                        'time_and_attendance.shifts.assign_shift'
                    ]
                }
            ]
        },
        manual: {
            GET: [
                {
                    endpoint: /^\/company\/(\d+)\/schedules$/,
                    modules: [
                        'time_and_attendance.shifts.assign_shift'
                    ]
                },
                {
                    endpoint: /^\/company\/(\d+)\/affected_employees\/search$/,
                    modules: [
                        'time_and_attendance.shifts.assign_shift'
                    ]
                }
            ],
            POST: [
                {
                    endpoint: '/shift/bulk_create',
                    modules: [
                        'time_and_attendance.shifts.assign_shift'
                    ]
                },
                {
                    endpoint: /^\/shift\/(\d+)\/assign_shifts\/status$/,
                    modules: [
                        'time_and_attendance.shifts.assign_shift'
                    ]
                }
            ]
        }
    }
};
