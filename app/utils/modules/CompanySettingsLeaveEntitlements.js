export default {
    GET: [
        {
            endpoint: /^\/leave_entitlement\/(\d+)$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_entitlements'

            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_entitlements$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_entitlements'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_types\/search$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_entitlements'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/affected_employees\/search$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_entitlements'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/leave_entitlement\/is_name_available$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_entitlements'
            ]
        },
        {
            endpoint: /^\/leave_entitlement$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_entitlements'
            ]
        },
        {
            endpoint: /^\/leave_entitlement\/check_in_use$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_entitlements'
            ]
        },
        {
            endpoint: /^\/leave_entitlement\/bulk_delete$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_entitlements'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.leave_settings.leave_entitlements'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/leave_entitlement\/(\d+)$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_entitlements'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/leave_entitlement\/bulk_delete$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_entitlements'
            ]
        }
    ]
};
