export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/projects$/,
            modules: [
                'company_settings.company_structure.projects'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/project\/is_name_available$/,
            modules: [
                'company_settings.company_structure.projects'
            ]
        },
        {
            endpoint: '/project/bulk_create',
            modules: [
                'company_settings.company_structure.projects'
            ]
        },
        {
            endpoint: '/project/bulk_delete',
            modules: [
                'company_settings.company_structure.projects'
            ]
        },
        {
            endpoint: /^\/company\/(\S+)\/is_in_use$/,
            modules: [
                'company_settings.company_structure.projects'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/project\/(\d+)$/,
            modules: [
                'company_settings.company_structure.projects'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_structure'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: '/project/bulk_delete',
            modules: [
                'company_settings.company_structure.projects'
            ]
        }
    ]
};
