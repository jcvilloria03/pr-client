export default {
    GET: [
        {
            endpoint: '/default_schedule',
            modules: [
                'time_and_attendance.approvals_list.request_details'
            ]
        },
        {
            endpoint: /^\/undertime_request\/(\d+)$/,
            modules: [
                'time_and_attendance.approvals_list.request_details'
            ]
        },
        {
            endpoint: /^\/leave_request\/(\d+)$/,
            modules: [
                'time_and_attendance.approvals_list.request_details'
            ]
        },
        {
            endpoint: /^\/overtime_request\/(\d+)$/,
            modules: [
                'time_and_attendance.approvals_list.request_details'
            ]
        },
        {
            endpoint: /^\/shift_change_request\/(\d+)$/,
            modules: [
                'time_and_attendance.approvals_list.request_details'
            ]
        },
        {
            endpoint: /^\/time_dispute_request\/(\d+)$/,
            modules: [
                'time_and_attendance.approvals_list.request_details'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/affected_employees\/search$/,
            modules: [
                'time_and_attendance.shifts.assign_shift'
            ]
        }
    ],
    POST: [
        {
            endpoint: '/user/approvals',
            modules: [
                'time_and_attendance.approvals_list'
            ]
        },
        {
            endpoint: '/request/bulk_approve',
            modules: [
                'time_and_attendance.approvals_list',
                'time_and_attendance.approvals_list.request_details'
            ]
        },
        {
            endpoint: '/request/bulk_decline',
            modules: [
                'time_and_attendance.approvals_list',
                'time_and_attendance.approvals_list.request_details'
            ]
        },
        {
            endpoint: '/request/bulk_cancel',
            modules: [
                'time_and_attendance.approvals_list',
                'time_and_attendance.approvals_list.request_details'
            ]
        },
        {
            endpoint: '/request/send_message',
            modules: [
                'time_and_attendance.approvals_list.request_details'
            ]
        }
    ]
};
