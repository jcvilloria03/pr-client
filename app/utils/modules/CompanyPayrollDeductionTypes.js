export default {
    GET: [
        {
            endpoint: /^\/other_income_type\/(\d+)$/,
            modules: [
                'company_settings.company_payroll.deduction_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/deduction_type$/,
            modules: [
                'company_settings.company_payroll.deduction_types'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/philippine\/company\/(\d+)\/deduction_type\/bulk_create$/,
            modules: [
                'company_settings.company_payroll.deduction_types'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/deduction_type\/is_name_available$/,
            modules: [
                'company_settings.company_payroll.deduction_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_type$/,
            modules: [
                'company_settings.company_payroll.deduction_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_type\/is_delete_available$/,
            modules: [
                'company_settings.company_payroll.deduction_types'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/philippine\/deduction_type\/(\d+)$/,
            modules: [
                'company_settings.company_payroll.deduction_types'
            ]
        },
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_payroll.deduction_types'
            ]
        }
    ]
};
