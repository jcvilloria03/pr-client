export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/announcements$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/active_employees_count$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/attendance_stats$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/events$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/holidays$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/dashboard\/attendance$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/default_schedule$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/attendance\/job\/(\S+)$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/attendance\/job\/(\S+)\/errors$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/affected_employees\/search$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: '/time',
            modules: [
                'main_page.dashboard'
            ]
        }
    ],
    POST: [
        {
            endpoint: '/admin_dashboard/calendar_data',
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: '/admin_dashboard/calendar_data_counts',
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/ta_employees\/id$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: '/user/informations',
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: '/attendance/calculate/bulk',
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/inactive_employees$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/scorecard\/download$/,
            modules: [
                'main_page.dashboard'
            ]
        },
        {
            endpoint: '/announcement',
            modules: [
                'main_page.dashboard'
            ]
        }
    ]
};
