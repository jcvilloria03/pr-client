export default {
    GET: [
        {
            endpoint: /^\/account$/,
            modules: [
                'control_panel.subscriptions.licenses_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/plans$/,
            modules: [
                'control_panel.subscriptions.licenses_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/(\d+)\/stats$/,
            modules: [
                'control_panel.subscriptions.licenses_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/(\d+)\/draft$/,
            modules: [
                'control_panel.subscriptions.licenses_tab'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/subscriptions\/(\d+)\/plan\/update$/,
            modules: [
                'control_panel.subscriptions.licenses_tab'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/subscriptions\/(\d+)\/draft$/,
            modules: [
                'control_panel.subscriptions.licenses_tab'
            ]
        }
    ]
};
