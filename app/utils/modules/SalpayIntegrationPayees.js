export default {
    GET: [
        {
            endpoint: /^\/account$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/invite-shipping-details$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/employees$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/card-designs$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/users\/(\d+)\/companies$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/salpay-settings\/(\d+)$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/salpay\/link\/company-authorizations$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/employee-invitations$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/withdrawn-employee-invitations$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/employee-invitations$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/employee-unlink-invitation$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/employee-reinvitations$/,
            modules: [
                'control_panel.salpay_integration'
            ]
        }
    ]
};
