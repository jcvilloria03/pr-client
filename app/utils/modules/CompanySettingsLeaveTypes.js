export default {
    GET: [
        {
            endpoint: /^\/leave_type\/(\d+)$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_types'

            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_types$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_types'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/leave_type\/is_name_available$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_types'
            ]
        },
        {
            endpoint: /^\/leave_type$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_types'
            ]
        },
        {
            endpoint: /^\/leave_type\/check_in_use$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_types'
            ]
        },
        {
            endpoint: /^\/leave_type\/bulk_delete$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_types'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.leave_settings.leave_types'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/leave_type\/(\d+)$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_types'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/leave_type\/bulk_delete$/,
            modules: [
                'company_settings.leave_settings',
                'company_settings.leave_settings.leave_types'
            ]
        }
    ]
};
