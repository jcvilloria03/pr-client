export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.department',
                'company_settings.company_structure.organizational_chart.position'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.position'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/department\/bulk_create$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.department'
            ]
        },
        {
            endpoint: /^\/position\/bulk_create$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.position'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/department\/is_name_available$/,
            modules: [
                'company_settings.company_structure.organizational_chart.department'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/position\/is_name_available$/,
            modules: [
                'company_settings.company_structure.organizational_chart.position'
            ]
        },
        {
            endpoint: '/company/department/is_in_use',
            modules: [
                'company_settings.company_structure.organizational_chart.department'
            ]
        },
        {
            endpoint: '/company/position/is_in_use',
            modules: [
                'company_settings.company_structure.organizational_chart.position'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/position\/bulk_update$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.position'
            ]
        },
        {
            endpoint: /^\/department\/bulk_update$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.department'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_structure'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/department\/(\d+)$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.department'
            ]
        },
        {
            endpoint: /^\/position\/(\d+)$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.position'
            ]
        }
    ]
};
