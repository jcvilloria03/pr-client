export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.position'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/position\/bulk_create$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.position'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/position\/is_name_available$/,
            modules: [
                'company_settings.company_structure.organizational_chart.position'
            ]
        },
        {
            endpoint: '/company/position/is_in_use',
            modules: [
                'company_settings.company_structure.organizational_chart.position'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/position\/bulk_update$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.position'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/position\/(\d+)$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.position'
            ]
        }
    ]
};
