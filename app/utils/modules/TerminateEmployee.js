export default {
    GET: [
        {
            endpoint: /^\/employee\/(\d+)\/termination_information$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/termination_informations$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/termination_informations\/(\d+)$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/final_pay\/available_items$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/termination_information\/(\d+)$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/calculate\/status$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)$/,
            modules: [
                'employees.people.termination_information'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/termination_informations$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/final_pay$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/calculate$/,
            modules: [
                'employees.people.termination_information'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/final_pay\/(\d+)$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/termination_informations\/(\d+)$/,
            modules: [
                'employees.people.termination_information'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/termination_informations\/(\d+)$/,
            modules: [
                'employees.people.termination_information'
            ]
        }
    ]
};
