export default {
    GET: [
        {
            endpoint: /^\/employee\/(\d+)$/,
            modules: [
                'employees.people.basic_pay_adjustments'
            ]
        },
        {
            endpoint: /^\/basic_pay_adjustment\/(\d+)$/,
            modules: [
                'employees.people.basic_pay_adjustments'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/basic_pay_adjustments$/,
            modules: [
                'employees.people.basic_pay_adjustments'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/employee\/(\d+)\/basic_pay_adjustment$/,
            modules: [
                'employees.people.basic_pay_adjustments'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/employee\/(\d+)\/basic_pay_adjustment\/(\d+)\/update$/,
            modules: [
                'employees.people.basic_pay_adjustments'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/employee\/(\d+)\/basic_pay_adjustment\/delete$/,
            modules: [
                'employees.people.basic_pay_adjustments'
            ]
        }
    ]
};
