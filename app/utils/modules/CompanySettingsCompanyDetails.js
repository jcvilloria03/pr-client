export default {
    GET: [
        {
            endpoint: /^\/account\/(\S+)\/companies$/,
            modules: [
                'company_settings.company_structure',
                'company_settings.company_structure.company_details',
                'company_settings.company_structure.company_details.company_information',
                'company_settings.company_structure.company_details.government_issued_id_number',
                'company_settings.company_structure.company_details.contact_information'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)$/,
            modules: [
                'company_settings.company_structure',
                'company_settings.company_structure.company_details',
                'company_settings.company_structure.company_details.company_information',
                'company_settings.company_structure.company_details.government_issued_id_number',
                'company_settings.company_structure.company_details.contact_information'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_structure'
            ]
        }
    ]
};
