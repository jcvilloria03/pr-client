export default {
    GET: [
        {
            endpoint: '/account',
            modules: [
                'control_panel.audit_trail'
            ]
        },
        {
            endpoint: '/account/philippine/companies',
            modules: [
                'control_panel.audit_trail'
            ]
        },
        {
            endpoint: '/account/allusers',
            modules: [
                'control_panel.audit_trail'
            ]
        },
        {
            endpoint: '/audit-trail/modules',
            modules: [
                'control_panel.audit_trail'
            ]
        }
    ],
    POST: [
        {
            endpoint: '/audit-trail/logs',
            modules: [
                'control_panel.audit_trail'
            ]
        },
        {
            endpoint: '/audit-trail/logs/export',
            modules: [
                'control_panel.audit_trail'
            ]
        }
    ]
};
