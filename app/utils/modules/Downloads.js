export default {
    GET: [
        {
            endpoint: /^\/payslip\/zipped\/(\S+)\/url$/,
            modules: [
                'payroll.payslips'
            ]
        },
        {
            endpoint: /^\/download\/(\S+)\/(\S+)/,
            modules: [
                'payroll.payroll_summary',
            ]
        }
    ]
};
