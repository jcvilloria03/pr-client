export default {
    GET: [
        {
            endpoint: /^\/subscriptions\/account_deletion$/,
            modules: [
                'control_panel.subscriptions.subscriptions_tab'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/subscriptions\/account_deletion$/,
            modules: [
                'control_panel.subscriptions.subscriptions_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/account_deletion\/(\d+)\/confirm_otp$/,
            modules: [
                'control_panel.subscriptions.subscriptions_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/account_deletion\/(\d+)\/resend_otp$/,
            modules: [
                'control_panel.subscriptions.subscriptions_tab'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/subscriptions\/account_deletion\/(\d+)\/confirm$/,
            modules: [
                'control_panel.subscriptions.subscriptions_tab'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/subscriptions\/account_deletion\/(\d+)\/cancel$/,
            modules: [
                'control_panel.subscriptions.subscriptions_tab'
            ]
        }
    ]
};
