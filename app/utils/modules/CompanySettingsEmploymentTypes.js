export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/employment_types$/,
            modules: [
                'company_settings.company_structure.employment_types'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/employment_type\/is_name_available$/,
            modules: [
                'company_settings.company_structure.employment_types'
            ]
        },
        {
            endpoint: '/employment_type/bulk_create',
            modules: [
                'company_settings.company_structure.employment_types'
            ]
        },
        {
            endpoint: '/employment_type/bulk_delete',
            modules: [
                'company_settings.company_structure.employment_types'
            ]
        },
        {
            endpoint: /^\/company\/(\S+)\/is_in_use$/,
            modules: [
                'company_settings.company_structure.employment_types'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/employment_type\/(\d+)$/,
            modules: [
                'company_settings.company_structure.employment_types'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_structure'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: '/employment_type/bulk_delete',
            modules: [
                'company_settings.company_structure.employment_types'
            ]
        }
    ]
};
