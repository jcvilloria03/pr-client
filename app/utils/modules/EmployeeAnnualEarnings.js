export default {
    GET: [
        {
            endpoint: /^\/annual_earning\/upload\/status$/,
            modules: [
                'employees.annual_earnings'
            ]
        },
        {
            endpoint: /^\/annual_earning\/upload\/preview$/,
            modules: [
                'employees.annual_earnings'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/annual_earning\/(\d+)$/,
            modules: [
                'employees.annual_earnings'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees$/,
            modules: [
                'employees.annual_earnings'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/annual_earning\/group_by_year$/,
            modules: [
                'employees.annual_earnings'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/annual_earning\/upload$/,
            modules: [
                'employees.annual_earnings'
            ]
        },
        {
            endpoint: /^\/annual_earning\/upload\/save$/,
            modules: [
                'employees.annual_earnings'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/annual_earning\/set$/,
            modules: [
                'employees.annual_earnings'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/annual_earning\/download$/,
            modules: [
                'employees.annual_earnings'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/annual_earning\/set$/,
            modules: [
                'employees.annual_earnings'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/annual_earning\/bulk_delete$/,
            modules: [
                'employees.annual_earnings'
            ]
        }
    ]
};
