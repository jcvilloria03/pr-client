export default {
    GET: [
        {
            endpoint: /^\/employee\/(\d+)\/government_forms\/bir_2316\/(\d+)$/,
            modules: [
                'employees.government_forms'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/government_forms\/bir_2316\/employees$/,
            modules: [
                'employees.government_forms'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/locations$/,
            modules: [
                'employees.government_forms'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'employees.government_forms'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/payroll_groups$/,
            modules: [
                'employees.government_forms'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees$/,
            modules: [
                'employees.government_forms'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/government_forms\/bir_2316$/,
            modules: [
                'employees.government_forms'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/government_form_periods$/,
            modules: [
                'employees.government_forms'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/government_forms\/bir_2316\/regenerate$/,
            modules: [
                'employees.government_forms'
            ]
        },
        {
            endpoint: /^\/employee\/government_forms\/bir_2316\/bulk\/generate$/,
            modules: [
                'employees.government_forms'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/government_forms\/bir_2316\/download$/,
            modules: [
                'employees.government_forms'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/employee\/(\d+)\/government_forms\/bir_2316\/(\d+)$/,
            modules: [
                'employees.government_forms'
            ]
        }
    ]
};
