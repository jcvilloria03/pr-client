export default {
    GET: [
        {
            endpoint: /^\/account$/,
            modules: [
                'control_panel.subscriptions.receipts_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/receipts$/,
            modules: [
                'control_panel.subscriptions.receipts_tab'
            ]
        }
    ]
};
