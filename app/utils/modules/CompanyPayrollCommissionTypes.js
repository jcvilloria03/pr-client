export default {
    GET: [
        {
            endpoint: /^\/other_income_type\/(\d+)$/,
            modules: [
                'company_settings.company_payroll.commission_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/commission_type$/,
            modules: [
                'company_settings.company_payroll.commission_types'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/philippine\/company\/(\d+)\/commission_type\/bulk_create$/,
            modules: [
                'company_settings.company_payroll.commission_types'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/commission_type\/is_name_available$/,
            modules: [
                'company_settings.company_payroll.commission_types'
            ]
        },
        {
            endpoint: /^\/other_income_type\/is_edit_available$/,
            modules: [
                'company_settings.company_payroll.commission_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_type$/,
            modules: [
                'company_settings.company_payroll.commission_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_type\/is_delete_available$/,
            modules: [
                'company_settings.company_payroll.commission_types'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/philippine\/commission_type\/(\d+)$/,
            modules: [
                'company_settings.company_payroll.commission_types'
            ]
        },
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_payroll.commission_types'
            ]
        }
    ]
};
