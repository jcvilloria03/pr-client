export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/time_attendance_locations$/,
            modules: [
                'company_settings.company_structure.locations',
                'company_settings.company_structure.locations.location',
                'company_settings.company_structure.locations.ip_address'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/location\/is_name_available$/,
            modules: [
                'company_settings.company_structure.locations'
            ]
        },
        {
            endpoint: '/time_attendance_locations',
            modules: [
                'company_settings.company_structure.locations'
            ]
        },
        {
            endpoint: /^\/company\/(\S+)\/is_in_use$/,
            modules: [
                'company_settings.company_structure.locations'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/time_attendance_locations\/(\d+)$/,
            modules: [
                'company_settings.company_structure.locations',
                'company_settings.company_structure.locations.location',
                'company_settings.company_structure.locations.ip_address'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_structure'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/time_attendance_locations\/bulk_delete$/,
            modules: [
                'company_settings.company_structure.locations'
            ]
        }
    ]
};
