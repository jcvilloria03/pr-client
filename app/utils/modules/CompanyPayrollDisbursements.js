export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/banks$/,
            modules: [
                'company_settings.company_payroll.disbursements'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/bank$/,
            modules: [
                'company_settings.company_payroll.disbursements'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/bank$/,
            modules: [
                'employees.people.payment_methods'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/company\/(\d+)\/bank$/,
            modules: [
                'company_settings.company_payroll.disbursements'
            ]
        }
    ]
};
