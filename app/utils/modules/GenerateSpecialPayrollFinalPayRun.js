export default {
    GET: [
        {
            endpoint: /^\/philippine\/company\/(\d+)\/locations$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/teams$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees\/final_pay$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payroll\/special\/available_items$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/payroll\/final$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/special$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/attendance$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/calculate$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/as-api\/attendance$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/payroll\/upload\/attendance$/,
            modules: [
                'payroll.payroll_summary.generate_special_payroll_final_pay_run',
                'payroll.payroll_summary'
            ]
        }
    ]
};
