export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/payroll_loan_types$/,
            modules: [
                'company_settings.company_payroll.loan_type_settings'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/payroll_loan_type$/,
            modules: [
                'company_settings.company_payroll.loan_type_settings'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payroll_loan_types\/is_name_available$/,
            modules: [
                'company_settings.company_payroll.loan_type_settings'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/payroll_loan_type\/(\d+)$/,
            modules: [
                'company_settings.company_payroll.loan_type_settings'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/payroll_loan_type\/bulk_delete$/,
            modules: [
                'company_settings.company_payroll.loan_type_settings'
            ]
        }
    ]
};
