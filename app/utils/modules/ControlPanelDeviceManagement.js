export default {
    GET: [
        {
            endpoint: /^\/account\/users$/,
            modules: [
                'control_panel.device_management'
            ]
        },
        {
            endpoint: /^\/user\/(\d+)$/,
            modules: [
                'control_panel.device_management'
            ]
        },
        {
            endpoint: /^\/api\/face_pass\/ra08t\/devices$/,
            modules: [
                'control_panel.device_management'
            ]
        },
        {
            endpoint: /^\/api\/face_pass\/ra08t\/users\/(\d+)$/,
            modules: [
                'control_panel.device_management'
            ]
        },
        {
            endpoint: /^\/api\/face_pass\/ra08t\/accounts\/unlinked_device_users$/,
            modules: [
                'control_panel.device_management'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/api\/face_pass\/ra08t\/users$/,
            modules: [
                'control_panel.device_management'
            ]
        },
        {
            endpoint: /^\/api\/face_pass\/ra08t\/users\/(\d+)\/sync$/,
            modules: [
                'control_panel.device_management'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/api\/face_pass\/ra08t\/accounts\/unlinked_device_users$/,
            modules: [
                'control_panel.device_management'
            ]
        },
        {
            endpoint: /^\/api\/face_pass\/ra08t\/users\/(\d+)\/devices$/,
            modules: [
                'control_panel.device_management'
            ]
        }
    ]
};
