export default {
    GET: [
        {
            endpoint: /^\/subscriptions\/(\d+)\/stats$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/users$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: /^\/available_product_seats$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: /^\/account\/(\S+)\/companies$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/users$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/roles$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: /^\/companies\/(\d+)\/roles$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees\/search$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: /^\/user\/(\d+)$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: '/account',
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/user\/(\d+)\/resend_verification$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: /^\/user$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/user\/(\d+)\/set_status$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: /^\/user\/(\d+)$/,
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        },
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.users_and_roles',
                'company_settings.users_and_roles.user_management'
            ]
        }
    ]
};
