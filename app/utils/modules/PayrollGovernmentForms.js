export default {
    GET: [
        {
            endpoint: /^\/government_forms\/(\d+)$/,
            modules: [
                'payroll.government_forms'
            ]
        },
        {
            endpoint: /^\/download\/govt_form\/(\d+)$/,
            modules: [
                'payroll.government_forms'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/government_form_periods$/,
            modules: [
                'payroll.government_forms'
            ]
        },
        {
            endpoint: /^\/government_forms\/1604c\/available_years$/,
            modules: [
                'payroll.government_forms'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/months_with_payroll_loans\/sss$/i,
            modules: [
                'payroll.government_forms'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/months_with_payroll_loans\/pag-ibig$/i,
            modules: [
                'payroll.government_forms'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/philippine\/government_forms\/bir\/1601c_generate$/,
            modules: [
                'payroll.government_forms'
            ]
        },
        {
            endpoint: /^\/government_forms$/,
            modules: [
                'payroll.government_forms'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/government_forms\/(\d+)$/,
            modules: [
                'payroll.government_forms'
            ]
        }
    ]
};
