export default {
    calendar: {
        GET: [
            {
                endpoint: /^\/company\/(\d+)\/schedules$/,
                modules: [
                    'time_and_attendance.schedules'
                ]
            },
            {
                endpoint: /^\/company\/(\d+)\/schedules\/download\/(\S+)$/,
                modules: [
                    'time_and_attendance.schedules'
                ]
            },
            {
                endpoint: /^\/company\/(\d+)\/time_attendance_locations$/,
                modules: [
                    'time_and_attendance.schedules'
                ]
            },
            {
                endpoint: /^\/company\/(\d+)\/departments$/,
                modules: [
                    'time_and_attendance.schedules'
                ]
            },
            {
                endpoint: /^\/company\/(\d+)\/positions$/,
                modules: [
                    'time_and_attendance.schedules'
                ]
            }
        ],
        POST: [
            {
                endpoint: '/schedule/check_in_use',
                modules: [
                    'time_and_attendance.schedules'
                ]
            },
            {
                endpoint: /^\/company\/(\d+)\/schedules\/generate_csv$/,
                modules: [
                    'time_and_attendance.schedules'
                ]
            }
        ],
        DELETE: [
            {
                endpoint: '/schedule/bulk_delete',
                modules: [
                    'time_and_attendance.schedules'
                ]
            }
        ]
    },
    add: {
        batch: {
            GET: [
                {
                    endpoint: /^\/company\/(\d+)\/schedules$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: /^\/schedule\/(\d+)$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: '/schedule/upload/preview',
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: '/schedule/upload/status',
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: /^\/company\/(\d_)\/departments$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                }
            ],
            POST: [
                {
                    endpoint: '/schedule/upload',
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: '/schedule/upload/save',
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                }
            ]
        },
        manual: {
            GET: [
                {
                    endpoint: /^\/company\/(\d+)\/tags$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: /^\/company\/(\d+)\/affected_employees\/search$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                }
            ],
            POST: [
                {
                    endpoint: '/schedule',
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: /^\/company\/(\d+)\/schedule\/is_name_available$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                }
            ]
        }
    },
    update: {
        batch: {
            GET: [
                {
                    endpoint: /^\/company\/(\d+)\/schedules$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: /^\/schedule\/(\d+)$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: '/schedule/upload/preview',
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: '/schedule/upload/status',
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                }
            ],
            POST: [
                {
                    endpoint: '/schedule/upload',
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: '/schedule/upload/save',
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                }
            ]
        },
        manual: {
            GET: [
                {
                    endpoint: /^\/schedule\/(\d+)$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: /^\/company\/(\d+)\/tags$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: /^\/company\/(\d+)\/affected_employees\/search$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                }
            ],
            POST: [
                {
                    endpoint: '/schedule',
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                },
                {
                    endpoint: /^\/company\/(\d+)\/schedule\/is_name_available$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                }
            ],
            PUT: [
                {
                    endpoint: /^\/schedule\/(\d+)$/,
                    modules: [
                        'time_and_attendance.schedules'
                    ]
                }
            ]
        }
    }
};
