export default {
    GET: [
        {
            endpoint: /^\/subscriptions\/(\d+)\/stats$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/ta_employees$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/locations$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/ranks$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employment_types$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/teams$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/cost_centers$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/payroll_groups$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/philippine\/employee\/(\d+)$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/view\/employee\/user$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_add\/status$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_update\/status$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_add\/preview$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_update\/preview$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/account$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees\/generate_masterfile\/status$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/download\/masterfile\/(\S+)$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/companies\/(\d+)\/roles$/,
            modules: [
                'employees.people'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/people$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/has_usermatch$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_update\/personal_info$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_update\/time_attendance_info$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_update\/payroll_info$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_add\/personal_info$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_add\/payroll_info$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_add\/time_attendance_info$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_add\/save$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_add\/ta_save$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_update\/save$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees\/generate_masterfile$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_add\/people$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_add\/time_attendance_info$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_add\/payroll_info$/,
            modules: [
                'employees.people'
            ]
        },
        {
            endpoint: /^\/employee\/batch_update\/people$/,
            modules: [
                'employees.people'
            ]
        }
    ]
};
