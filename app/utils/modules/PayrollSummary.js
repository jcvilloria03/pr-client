export default {
    GET: [
        {
            endpoint: /^\/payroll\/(\d+)\/payslips\/generate$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payrolls\/jobs\/status$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payroll_data_counts$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payrolls$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/download\/payroll_registers\/(.*)$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/disbursement_summary$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/philippine\/payroll_group\/(\d+)$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll_register\/(\d+)\/has_payroll_register$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/payrolls\/(\d+)\/employees$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/remaining-balance$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/payrolls\/(\d+)\/disbursements$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/has_gap_loan_candidates$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/has_payslips$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/job\/payslip$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/download\/payroll_register\/(\d+)$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/employee_disbursement_method$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/payrolls\/(\d+)\/employees$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/remaining-balance$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/payrolls\/(\d+)\/disbursements$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/get_gap_loan_candidates$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/account\/payroll_groups$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\d+)\/initial_preview$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/payroll_groups$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payrolls\/delete_payroll_job\/(\S+)$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/regular_payroll_job\/(\S+)$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/payrolls\/(\d+)\/disbursements$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/regular_payroll_job$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/calculate$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/open$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/recalculate$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/payrolls\/(\d+)\/otp\/disbursement\/verify$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/payrolls\/(\d+)\/otp\/disbursement\/resend$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/send_payslips$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\S+)\/update_amortization_preview$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/create\/(\S+)$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\d+)$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll_loan$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\d+)\/update_loan_preview$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/activate_gap_loans$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll_register\/multiple$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/(.*)$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/close$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/open$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/recalculate$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/allowance$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/bonus$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/commission$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/deduction$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/attendance$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/calculate$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/as-api\/attendance$/,
            modules: [
                'payroll.payroll_summary'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/salpay\/companies\/(\d+)\/payrolls\/(\d+)\/disbursements\/(\d+)$/,
            modules: [
                'payroll.payroll_summary'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/payroll\/(\d+)$/,
            modules: [
                'payroll.payroll_summary'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/payrolls$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)$/,
            modules: [
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payrolls\/delete_payroll_job$/,
            modules: [
                'payroll.payroll_summary'
            ]
        }
    ]
};
