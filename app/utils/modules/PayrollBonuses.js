export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/bonus_type$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/payroll_groups$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/other_income\/(\d+)$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/bonus\/upload\/status$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/account\/payroll_groups$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/payroll_groups$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/bonus_type$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/locations$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'payroll.bonuses'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/philippine\/company\/(\d+)\/bonus\/bulk_create$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/bonus\/upload$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income\/is_delete_available$/,
            modules: [
                'payroll.bonuses'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/bonus_type\/download$/,
            modules: [
                'payroll.bonuses'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/philippine\/bonus\/(\d+)$/,
            modules: [
                'payroll.bonuses'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/company\/(\d+)\/other_income$/,
            modules: [
                'payroll.bonuses'
            ]
        }
    ]
};
