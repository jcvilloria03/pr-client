export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/max_clock_outs$/,
            modules: [
                'company_settings.max_clock_outs'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/max_clock_out$/,
            modules: [
                'company_settings.max_clock_outs'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/max_clock_out\/(\d+)$/,
            modules: [
                'company_settings.max_clock_outs'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/max_clock_out\/bulk_delete$/,
            modules: [
                'company_settings.max_clock_outs'
            ]
        }
    ]
};
