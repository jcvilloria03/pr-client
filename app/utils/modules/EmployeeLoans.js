export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/payroll_loan_types$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan_type\/subtypes\/SSS$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan_type\/subtypes\/Pag-ibig$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/account\/payroll_groups$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\S+)\/initial_preview$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/upload\/status$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/upload\/preview$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payroll_loans$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/locations$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'employees.loans'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/payroll_loan$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\S+)$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/create\/(\S+)$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payroll_loans\/generate_csv$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\S+)\/update_amortization_preview$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\S+)\/update_loan_preview$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/upload$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/upload\/save$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payroll_loans\/download$/,
            modules: [
                'employees.loans'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payroll_loans\/download\/status$/,
            modules: [
                'employees.loans'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/payroll_loan\/bulk_delete$/,
            modules: [
                'employees.loans'
            ]
        }
    ]
};
