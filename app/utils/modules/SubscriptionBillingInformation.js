export default {
    GET: [
        {
            endpoint: /^\/account$/,
            modules: [
                'control_panel.subscriptions.invoices_tab'
            ]
        },
        {
            endpoint: /^\/countries$/,
            modules: [
                'control_panel.subscriptions.invoices_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/invoices$/,
            modules: [
                'control_panel.subscriptions.invoices_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/invoices\/(\d+)$/,
            modules: [
                'control_panel.subscriptions.invoices_tab'
            ]
        },
        {
            endpoint: /^\/subscriptions\/billing_information$/,
            modules: [
                'control_panel.subscriptions.invoices_tab',
                'control_panel.subscriptions.invoices_tab.billing_information'
            ]
        },
        {
            endpoint: /^\/subscription-payment\/payment-method\/current$/,
            modules: [
                'control_panel.subscriptions.invoices_tab.billing_information'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/subscriptions\/billing_information$/,
            modules: [
                'control_panel.subscriptions.invoices_tab.billing_information'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/subscription-payment\/payment-method\/setup-intent$/,
            modules: [
                'control_panel.subscriptions.invoices_tab.billing_information'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/subscription-payment\/payment-method\/current\/remove$/,
            modules: [
                'control_panel.subscriptions.invoices_tab.billing_information'
            ]
        }
    ]
};
