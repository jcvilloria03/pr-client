export default {
    GET: [
        {
            endpoint: /^\/employee\/(\d+)$/,
            modules: [
                'employees.people.basic_information',
                'employees.people.employment_information',
                'employees.people.termination_information',
                'employees.people.payroll_information',
                'employees.people.payment_methods',
                'employees.people.basic_pay_adjustments',
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees$/,
            modules: [
                'employees.people.deductions',
                'employees.people.loans',
                'employees.people.adjustments',
                'employees.people.allowances',
                'employees.people.commissions',
                'employees.people.bonuses',
                'employees.people.leave_credits',
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/account\/payroll_groups$/,
            modules: [
                'employees.people.deductions',
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/other_income\/(\d+)$/,
            modules: [
                'employees.people.deductions',
                'employees.people.adjustments',
                'employees.people.allowances',
                'employees.people.commissions',
                'employees.people.bonuses'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/deduction_type$/,
            modules: [
                'employees.people.deductions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/(\d+)\/other_incomes\/deduction_type$/,
            modules: [
                'employees.people.deductions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/(\d+)\/payroll_loans$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payroll_loan_types$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan_type\/subtypes\/SSS$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan_type\/subtypes\/Pag-ibig$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\d+)\/initial_preview$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/adjustment_type$/,
            modules: [
                'employees.people.adjustments'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/(\d+)\/other_incomes\/adjustment_type$/,
            modules: [
                'employees.people.adjustments'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/allowance_type$/,
            modules: [
                'employees.people.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/(\d+)\/other_incomes\/allowance_type$/,
            modules: [
                'employees.people.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/commission_type$/,
            modules: [
                'employees.people.commissions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'employees.people.commissions',
                'employees.people.bonuses',
                'employees.people.employment_information'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/payroll_groups$/,
            modules: [
                'employees.people.commissions',
                'employees.people.bonuses',
                'employees.people.payroll_information'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/(\d+)\/other_incomes\/commission_type$/,
            modules: [
                'employees.people.commissions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/bonus_type$/,
            modules: [
                'employees.people.bonuses'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/(\d+)\/other_incomes\/bonus_type$/,
            modules: [
                'employees.people.bonuses'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/leave_entitlement$/,
            modules: [
                'employees.people.leave_credits'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_types$/,
            modules: [
                'employees.people.leave_credits',
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/schedules$/,
            modules: [
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/(\d+)\/leave_types$/,
            modules: [
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/(\d+)\/shifts$/,
            modules: [
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/rest_days$/,
            modules: [
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/leave_request\/(\d+)$/,
            modules: [
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_entitlements$/,
            modules: [
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/default_schedule$/,
            modules: [
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/ranks$/,
            modules: [
                'employees.people.employment_information'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employment_types$/,
            modules: [
                'employees.people.employment_information'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'employees.people.employment_information'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/teams$/,
            modules: [
                'employees.people.employment_information'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/locations$/,
            modules: [
                'employees.people.employment_information'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/termination_information$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/termination_informations$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/termination_informations\/(\d+)$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/final_pay\/available_items$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/termination_information\/(\d+)$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/calculate\/status$/,
            modules: [
                'employees.people.termination_information'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/(\d+)\/annual_earning$/,
            modules: [
                'employees.people.annual_earnings'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/workflow_entitlements$/,
            modules: [
                'employees.people.approval_groups'
            ]
        },
        {
            endpoint: /^\/employee_request_module$/,
            modules: [
                'employees.people.approval_groups'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/workflows$/,
            modules: [
                'employees.people.approval_groups'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/banks$/,
            modules: [
                'employees.people.payment_methods'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/payment_methods$/,
            modules: [
                'employees.people.payment_methods'
            ]
        },
        {
            endpoint: /^\/basic_pay_adjustment\/(\d+)$/,
            modules: [
                'employees.people.basic_pay_adjustments'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/basic_pay_adjustments$/,
            modules: [
                'employees.people.basic_pay_adjustments'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/cost_centers$/,
            modules: [
                'employees.people.employment_information'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/employees\/export$/,
            modules: [
                'employees.people.basic_information'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/deduction\/bulk_create$/,
            modules: [
                'employees.people.deductions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/deduction_type\/download$/,
            modules: [
                'employees.people.deductions'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/create\/(\d+)$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\S+)\/update_amortization_preview$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\d+)\/update_loan_preview$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payroll_loans\/generate_csv$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/(\d+)$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/adjustment\/bulk_create$/,
            modules: [
                'employees.people.adjustments'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/adjustment_type\/download$/,
            modules: [
                'employees.people.adjustments'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/allowance\/bulk_create$/,
            modules: [
                'employees.people.allowances'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/allowance_type\/download$/,
            modules: [
                'employees.people.allowances'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/commission\/bulk_create$/,
            modules: [
                'employees.people.commissions'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/commission_type\/download$/,
            modules: [
                'employees.people.commissions'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/bonus\/bulk_create$/,
            modules: [
                'employees.people.bonuses'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_incomes\/bonus_type\/download$/,
            modules: [
                'employees.people.bonuses'
            ]
        },
        {
            endpoint: /^\/leave_credit$/,
            modules: [
                'employees.people.leave_credits'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_credits$/,
            modules: [
                'employees.people.leave_credits'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/leave_credits\/download$/,
            modules: [
                'employees.people.leave_credits'
            ]
        },
        {
            endpoint: /^\/leave_request\/admin$/,
            modules: [
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/leave_requests$/,
            modules: [
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/leave_requests\/download$/,
            modules: [
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/leave_request\/admin\/calculate_leaves_total_value$/,
            modules: [
                'employees.people.filed_leaves'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/workflow_entitlement\/has_pending_requests$/,
            modules: [
                'employees.people.approval_groups'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/workflow_entitlement\/create_or_update$/,
            modules: [
                'employees.people.approval_groups'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/bank$/,
            modules: [
                'employees.people.payment_methods'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/basic_pay_adjustment$/,
            modules: [
                'employees.people.basic_pay_adjustments'
            ]
        },
        {
            endpoint: /^\/employee\/upload\/picture\/(\d+)$/,
            modules: [
                'employees.people.basic_information'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/philippine\/deduction\/(\d+)$/,
            modules: [
                'employees.people.deductions'
            ]
        },
        {
            endpoint: /^\/philippine\/adjustment\/(\d+)$/,
            modules: [
                'employees.people.adjustments'
            ]
        },
        {
            endpoint: /^\/philippine\/allowance\/(\d+)$/,
            modules: [
                'employees.people.allowances'
            ]
        },
        {
            endpoint: /^\/philippine\/commission\/(\d+)$/,
            modules: [
                'employees.people.commissions'
            ]
        },
        {
            endpoint: /^\/philippine\/bonus\/(\d+)$/,
            modules: [
                'employees.people.bonuses'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/payment_methods\/(\d+)$/,
            modules: [
                'employees.people.payment_methods'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/basic_pay_adjustment\/(\d+)\/update$/,
            modules: [
                'employees.people.basic_pay_adjustments'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/employee\/(\d+)$/,
            modules: [
                'employees.people.basic_information',
                'employees.people.employment_information',
                'employees.people.payroll_information'
            ]
        },
        {
            endpoint: /^\/leave_credit\/(\d+)$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/company\/(\d+)\/other_income$/,
            modules: [
                'employees.people.deductions',
                'employees.people.adjustments',
                'employees.people.allowances',
                'employees.people.commissions',
                'employees.people.bonuses'
            ]
        },
        {
            endpoint: /^\/payroll_loan\/bulk_delete$/,
            modules: [
                'employees.people.loans'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/payment_method$/,
            modules: [
                'employees.people.payment_methods'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/basic_pay_adjustment\/delete$/,
            modules: [
                'employees.people.basic_pay_adjustments'
            ]
        },
        {
            endpoint: /^\/termination_informations\/(\d+)$/,
            modules: [
                'employees.people.termination_information'
            ]
        }
    ]
};
