export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.department',
                'company_settings.company_structure.organizational_chart.position'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/department\/bulk_create$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.department'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/department\/is_name_available$/,
            modules: [
                'company_settings.company_structure.organizational_chart.department'
            ]
        },
        {
            endpoint: '/company/department/is_in_use',
            modules: [
                'company_settings.company_structure.organizational_chart.department'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/department\/bulk_update$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.department'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/department\/(\d+)$/,
            modules: [
                'company_settings.company_structure.organizational_chart',
                'company_settings.company_structure.organizational_chart.department'
            ]
        }
    ]
};
