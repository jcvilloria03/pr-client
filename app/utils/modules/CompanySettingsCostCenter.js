export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/cost_centers$/,
            modules: [
                'company_settings.company_structure.cost_centers'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/cost_center\/is_name_available$/,
            modules: [
                'company_settings.company_structure.cost_centers'
            ]
        },
        {
            endpoint: /^\/cost_center$/,
            modules: [
                'company_settings.company_structure.cost_centers'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/cost_center\/(\d+)$/,
            modules: [
                'company_settings.company_structure.cost_centers'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_structure'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/cost_center\/bulk_delete$/,
            modules: [
                'company_settings.company_structure.cost_centers'
            ]
        }
    ]
};
