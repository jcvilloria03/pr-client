export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/leave_types$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees$/,
            modules: [
                'employees.leaves.leave_credits',
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/leave_entitlement$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/leave_credit\/upload\/status$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/leave_credit\/upload\/preview$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/leave_credit\/(\d+)$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/locations$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/schedules$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/(\d+)\/leave_types$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/default_schedule$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employee\/(\d+)\/shifts$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/employee\/(\d+)\/rest_days$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/leave_request\/upload\/status$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/leave_request\/upload\/preview$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/leave_request\/(\d+)$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_entitlements$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/leave_credit$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/leave_credit\/upload$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/leave_credit\/upload\/save$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_credits$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_credits\/download$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_credits\/download\/status$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/leave_request\/admin$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/leave_request\/upload$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/leave_request\/upload\/save$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/leave_request\/admin\/calculate_leaves_total_value$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_requests$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_requests\/download$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_requests\/download\/status$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/leave_credit\/(\d+)$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/leave_request\/(\d+)\/admin$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/leave_credit\/bulk_delete$/,
            modules: [
                'employees.leaves.leave_credits'
            ]
        },
        {
            endpoint: /^\/leave_request\/bulk_delete$/,
            modules: [
                'employees.leaves.filed_leave'
            ]
        }
    ]
};
