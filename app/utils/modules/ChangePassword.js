export default {
    PATCH: [
        {
            endpoint: /^\/auth\/user\/change_password$/,
            modules: [
                'main_page.profile_information.change_password'
            ]
        }
    ]
};
