export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/ranks$/,
            modules: [
                'company_settings.company_structure.ranks'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/rank\/is_name_available$/,
            modules: [
                'company_settings.company_structure.ranks'
            ]
        },
        {
            endpoint: '/rank/bulk_create',
            modules: [
                'company_settings.company_structure.ranks'
            ]
        },
        {
            endpoint: /^\/company\/(\S+)\/is_in_use$/,
            modules: [
                'company_settings.company_structure.ranks'
            ]
        },
        {
            endpoint: '/rank/bulk_delete',
            modules: [
                'company_settings.company_structure.ranks'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/rank\/(\d+)$/,
            modules: [
                'company_settings.company_structure.ranks'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_structure'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/cost_center\/bulk_delete$/,
            modules: [
                'company_settings.company_structure.ranks'
            ]
        }
    ]
};
