export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/holidays$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.holidays'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/time_attendance_locations/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.holidays'
            ]
        },
        {
            endpoint: /^\/holiday\/(\d+)$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.holidays'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/holiday\/is_name_available$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.holidays'
            ]
        },
        {
            endpoint: /^\/holiday$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.holidays'
            ]
        },
        {
            endpoint: /^\/holiday\/check_in_use$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.holidays'
            ]
        },
        {
            endpoint: /^\/holiday\/bulk_delete$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.holidays'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.schedule_settings.holidays'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/holiday\/(\d+)$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.holidays'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/holiday\/bulk_delete$/,
            modules: [
                'company_settings.schedule_settings',
                'company_settings.schedule_settings.holidays'
            ]
        }
    ]
};
