export default {
    GET: [
        {
            endpoint: /^\/team\/(\d+)$/,
            modules: [
                'company_settings.company_structure.teams'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/ta_employees\/search$/,
            modules: [
                'company_settings.company_structure.teams'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/teams$/,
            modules: [
                'company_settings.company_structure.teams'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/company\/(\d+)\/team\/is_name_available$/,
            modules: [
                'company_settings.company_structure.teams'
            ]
        },
        {
            endpoint: '/time_attendance_team',
            modules: [
                'company_settings.company_structure.teams'
            ]
        },
        {
            endpoint: '/team/bulk_delete',
            modules: [
                'company_settings.company_structure.teams'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /^\/team\/(\d+)$/,
            modules: [
                'company_settings.company_structure.teams'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_structure'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: '/team/bulk_delete',
            modules: [
                'company_settings.company_structure.teams'
            ]
        }
    ]
};
