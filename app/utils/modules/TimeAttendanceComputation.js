export default {
    GET: [
        {
            endpoint: /^\/view\/(\d+)\/attendance\/user$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: /^\/attendance\/job\/(\S+)\/errors$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: /^\/attendance\/job\/(\S+)$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/leave_types\/search$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance.leaves'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/time_attendance_locations$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/departments$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/positions$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/ta_employees$/,
            modules: [
                'time_and_attendance.attendance_computation'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/shifts$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: /^\/attendance\/records\/export\/job\/(\S+)$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
    ],
    POST: [
        {
            endpoint: '/attendance/calculate/bulk',
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: '/attendance/records',
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: '/attendance/records/upload',
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: '/attendance/records/export',
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: '/attendance/lock/bulk',
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: '/attendance/unlock/bulk',
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees_by_ids$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/time_records\/bulk_create_or_delete$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance.time_records'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/hours_worked\/leaves\/bulk_create_or_update_or_delete$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance.leaves'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/hours_worked\/leaves$/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance.leaves'
            ]
        }
    ],
    PUT: [
        {
            endpoint: /\/attendance\/(\d+)\/(\S+)\/edit/,
            modules: [
                'time_and_attendance.attendance_computation',
                'time_and_attendance.attendance_computation.attendance.computed_attendance'
            ]
        }
    ]
};
