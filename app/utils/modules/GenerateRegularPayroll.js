export default {
    GET: [
        {
            endpoint: /^\/company\/(\d+)\/payroll_group_periods$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\S+)$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll_group\/(\d+)\/periods$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/regular_payroll_job\/(\S+)$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/payroll\/special\/available_items$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/employees\/final_pay$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll_group\/(\d+)\/pay_dates$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/calculate\/status$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/allowance\/status$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/bonus\/status$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/commission\/status$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/deduction\/status$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/attendance\/status$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/download\/allowance\/(\S+)$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/download\/bonus\/(\S+)$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/download\/commission\/(\S+)$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/download\/deduction\/(\S+)$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/download\/attendance\/(\S+)$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/employee\/employees_for_payroll_validation\/(\S+)$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/payroll\/(\d+)\/calculate$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/final$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/regular_payroll_job$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/special_payroll_job$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/allowance$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/bonus$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/commission$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/deduction$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/attendance$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/calculate$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/(\d+)\/as-api\/attendance$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/employee\/employees_for_payroll_validation$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        }
    ],
    DELETE: [
        {
            endpoint: /^\/payroll\/upload\/allowance$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/bonus$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/commission$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/deduction$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        },
        {
            endpoint: /^\/payroll\/upload\/attendance$/,
            modules: [
                'payroll.payroll_summary.generate_regular_payroll',
                'payroll.payroll_summary'
            ]
        }
    ]
};
