export default {
    GET: [
        {
            endpoint: /^\/other_income_type\/(\d+)$/,
            modules: [
                'company_settings.company_payroll.allowance_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_types\/allowance_type$/,
            modules: [
                'company_settings.company_payroll.allowance_types'
            ]
        },
        {
            endpoint: /^\/other_income_type\/tax_option_choices\/allowance_type$/,
            modules: [
                'company_settings.company_payroll.allowance_types'
            ]
        }
    ],
    POST: [
        {
            endpoint: /^\/philippine\/company\/(\d+)\/allowance_type\/bulk_create$/,
            modules: [
                'company_settings.company_payroll.allowance_types'
            ]
        },
        {
            endpoint: /^\/philippine\/company\/(\d+)\/allowance_type\/is_name_available$/,
            modules: [
                'company_settings.company_payroll.allowance_types'
            ]
        },
        {
            endpoint: /^\/other_income_type\/is_edit_available$/,
            modules: [
                'company_settings.company_payroll.allowance_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_type$/,
            modules: [
                'company_settings.company_payroll.allowance_types'
            ]
        },
        {
            endpoint: /^\/company\/(\d+)\/other_income_type\/is_delete_available$/,
            modules: [
                'company_settings.company_payroll.allowance_types'
            ]
        }
    ],
    PATCH: [
        {
            endpoint: /^\/philippine\/allowance_type\/(\d+)$/,
            modules: [
                'company_settings.company_payroll.allowance_types'
            ]
        },
        {
            endpoint: '/account/progress',
            modules: [
                'company_settings.company_payroll.allowance_types'
            ]
        }
    ]
};
