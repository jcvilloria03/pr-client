import forIn from 'lodash/forIn';
import has from 'lodash/has';
import startCase from 'lodash/startCase';

import { VALIDATION_ERRORS, UPLOAD_FILE_HEADERS } from './constants';

const {
    ID,
    FIRST_NAME,
    MIDDLE_NAME,
    LAST_NAME,
    TIMESHEET_DATE,
    SCHEDULED_HOURS,
    REGULAR,
    BONUS_TYPE,
    ALLOWANCE_TYPE,
    COMMISSION_TYPE,
    DEDUCTION_TYPE,
    AMOUNT,
    TAXABLE
} = UPLOAD_FILE_HEADERS;

const PARAMETERS_WITH_EMPLOYEE = [
    FIRST_NAME,
    LAST_NAME,
    TIMESHEET_DATE,
    SCHEDULED_HOURS,
    REGULAR
];

const REQUIRED_PARAMETERS = [
    ID,
    FIRST_NAME,
    LAST_NAME,
    TIMESHEET_DATE,
    SCHEDULED_HOURS,
    REGULAR,
    BONUS_TYPE,
    ALLOWANCE_TYPE,
    COMMISSION_TYPE,
    DEDUCTION_TYPE,
    AMOUNT,
    TAXABLE
];

/**
 * Processes errors from the upload validation
 * @param {Object|Array} data - Error object
 * @returns {Object}      Processed error object
 */
export function processValidationErrors( data ) {
    const processed = {};

    if ( Array.isArray( data ) ) {
        data.forEach( ( error, row ) => {
            processed[ row ] = [error];
        });
    } else {
        forIn( data, ( error, row ) => {
            processed[ row ] = createErrorMessagesArray( error, row );
        });
    }

    return processed;
}

/**
 * @param {Object[]} array - Array of objects to group
 * @returns {Object}
 */
function groupSameKeys( array ) {
    return array.reduce( ( grouped, entry ) => {
        const keys = Object.keys( entry );

        keys.forEach( ( key ) => {
            // eslint-disable-next-line no-param-reassign
            grouped[ key ] = has( grouped, key )
                ? grouped[ key ].concat( entry[ key ])
                : entry[ key ];
        });

        return grouped;
    }, {});
}

/**
 * Processes errors from the Get Attendance validation
 * @param {Object[]} data - Array of error objects
 * @returns {Object}      Processed error object
 */
export function processGetAttendanceErrors( data ) {
    const processed = {};

    const groupedData = groupSameKeys( data.map( ( errorObject ) => errorObject.attributes.description ) );

    forIn( groupedData, ( error, row ) => {
        processed[ row ] = createErrorMessagesArray( error, row, { is_get_attendance: true });
    });

    return processed;
}

/**
 * Creates descriptive error message
 *
 * @param {Array} errors   - Array of error objects
 * @param {String} key     - Error key string
 * @param {Object} options - Custom options for processing
 * @returns {Array}          Array of error messages
 */
function createErrorMessagesArray( errors, key, options ) {
    const {
        MISSING_PARAMETERS,
        INVALID_FORMAT,
        INVALID_VALUE,
        DUPLICATE_DATA,
        MISSING_HEADER,
        INVALID_HEADER,
        NON_EXISTENT_ID,
        INFORMATION_MISMATCH,
        INVALID_PARAMETER_VALUE,
        PROCESSING_ERROR
    } = VALIDATION_ERRORS.CODE;

    return errors.reduce( ( errorList, error ) => {
        let parsedErrors = [];
        switch ( error.code ) {
            case MISSING_PARAMETERS:
                parsedErrors = parseMissingParametersError( error );
                break;
            case INVALID_FORMAT:
                parsedErrors = parseInvalidFormatError( error );
                break;
            case INVALID_VALUE:
                parsedErrors = parseInvalidValueError( error );
                break;
            case DUPLICATE_DATA:
                parsedErrors = parseDuplicateDataError( error );
                break;
            case MISSING_HEADER:
            case INVALID_HEADER:
                parsedErrors = parseHeadersError( error );
                break;
            case NON_EXISTENT_ID:
                parsedErrors = parseNonExistentIdError( error );
                break;
            case INFORMATION_MISMATCH:
                parsedErrors = parseInformationMismatchError( error );
                break;
            case INVALID_PARAMETER_VALUE:
                errorList.push( error.name );
                break;
            case PROCESSING_ERROR:
                parsedErrors = parseProcessingError( error, key, options );
                break;
            default:
                errorList.push( error.name );
        }

        return errorList.concat( parsedErrors );
    }, []);
}

/**
 * Parses Missing Parameters Error
 * @param {Object} error - Error Object
 * @returns {Array}
 */
function parseMissingParametersError( error ) {
    const errorList = [];

    if ( has( error, 'details' ) ) {
        error.details.forEach( ( detail ) => {
            detail.parameters.forEach( ( parameter ) => (
                errorList.push( `${formatParameterName( parameter )} is required.` )
            ) );
        });
    } else {
        errorList.push( error.name );
    }

    return errorList;
}

/**
 * Parses Invalid Format Error
 * @param {Object} error - Error Object
 * @returns {Array}
 */
function parseInvalidFormatError( error ) {
    const errorList = [];

    if ( has( error, 'details' ) ) {
        error.details.forEach( ({ parameters, expectedFormat }) => {
            parameters.forEach( ( parameter ) => {
                if ( parameter === AMOUNT ) {
                    errorList.push( 'Invalid value for field Amount. This field can only accept numeric values' );
                } else {
                    errorList.push( `Invalid ${parameter} format. ${startCase( getUnitForExpectedFormat( expectedFormat ) )} must be in ${expectedFormat} format.` );
                }
            });
        });
    } else {
        errorList.push( error.name );
    }

    return errorList;
}

/**
 * Parses Invalid Value Error
 * @param {Object} error - Error Object
 * @returns {Array}
 */
function parseInvalidValueError( error ) {
    const errorList = [];

    if ( has( error, 'details' ) ) {
        error.details.forEach( ({ parameters, expectedValue, expectedFormat }) => {
            parameters.forEach( ( parameter ) => {
                switch ( true ) {
                    case parameter === TIMESHEET_DATE:
                        errorList.push( 'Invalid timesheet date. Timesheet date must be covered by the selected payroll period.' );
                        break;
                    case parameter === TAXABLE:
                        errorList.push( `Invalid value for field ${parameter}. Possible values are Yes or No.` );
                        break;
                    case has( expectedValue, 'min', 'max' ) && expectedFormat:
                        errorList.push( `Invalid ${parameter} format. ${startCase( getUnitForExpectedFormat( expectedFormat ) )} must be in ${expectedFormat} format. Acceptable values range from ${expectedValue.min} - ${expectedValue.max}` );
                        break;
                    default:
                }
            });
        });
    } else {
        errorList.push( error.name );
    }

    return errorList;
}

/**
 * Parses Duplicate Data Error
 * @param {Object} error - Error Object
 * @returns {Array}
 */
function parseDuplicateDataError( error ) {
    const errorList = [];

    if ( has( error, 'details' ) && has( error.details, 'Employee Id' ) ) {
        errorList.push( `Duplicate timesheet date found for Employee ID ${error.details[ 'Employee Id' ]}` );
    } else {
        errorList.push( error.name );
    }

    return errorList;
}

/**
 * Parses Missing Header and Invalid Header Error
 * @param {Object} error - Error Object
 * @returns {Array}
 */
function parseHeadersError( error ) {
    const errorList = [];

    if ( has( error, 'details' ) ) {
        error.details.forEach( ( detail ) => {
            detail.parameters.forEach( ( parameter ) => {
                if ( REQUIRED_PARAMETERS.includes( parameter ) ) {
                    errorList.push( `${formatParameterName( parameter )} is required.` );
                    return;
                }

                if ( parameter === '' ) {
                    errorList.push( 'Unknown field header value' );
                    return;
                }

                errorList.push( `${parameter} is not a valid header name.` );
            });
        });
    } else {
        errorList.push( error.name );
    }

    return errorList;
}

/**
 * Parses Non-existent ID Error
 * @param {Object} error - Error Object
 * @returns {Array}
 */
function parseNonExistentIdError( error ) {
    const errorList = [];

    if ( has( error, 'details' ) && has( error.details, 'Employee Id', FIRST_NAME, MIDDLE_NAME, LAST_NAME ) ) {
        const idAndFullName = `${error.details[ 'Employee Id' ]} ${error.details[ FIRST_NAME ]} ${error.details[ MIDDLE_NAME ]} ${error.details[ LAST_NAME ]}`;
        errorList.push( `Employee ${idAndFullName} does not belong to this Payroll Group.` );
    } else {
        errorList.push( 'Employee ID does not exist.' );
    }

    return errorList;
}

/**
 * Parses Information Mismatch Error
 * @param {Object} error - Error Object
 * @returns {Array}
 */
function parseInformationMismatchError( error ) {
    const errorList = [];

    if ( has( error, 'details' ) && has( error.details, 'Employee Id', FIRST_NAME, MIDDLE_NAME, LAST_NAME ) ) {
        const idAndFullName = `${error.details[ 'Employee Id' ]} ${error.details[ FIRST_NAME ]} ${error.details[ MIDDLE_NAME ]} ${error.details[ LAST_NAME ]}`;
        errorList.push( `Employee ${idAndFullName} employee information does not exist. Please check if this person is included on this Payroll.` );
    } else {
        errorList.push( error.name );
    }

    return errorList;
}

/**
 * Parses Processing Error Error
 * @param {Object} error - Error Object
 * @param {String} key - Index/key of error
 * @param {Object} options - Custom options for processing
 * @returns {Array}
 */
function parseProcessingError( error, key, options = {}) {
    const {
        MISSING_ATTENDANCE_DATA,
        MISSING_EMPLOYEE,
        INACTIVE_EMPLOYEE
    } = VALIDATION_ERRORS.KEY;

    const { is_get_attendance: isGetAttendance } = options;

    const errorList = [];

    if ( key === INACTIVE_EMPLOYEE ) {
        errorList.push( 'There are inactive employees on your uploaded attendance data. Please review your template and re-upload again.' );
    } else if ( has( error, 'details' ) ) {
        error.details.forEach( ({ description, details, employeeId, fullName }) => {
            switch ( key ) {
                case MISSING_ATTENDANCE_DATA:
                    if ( details && employeeId && fullName ) {
                        if ( isGetAttendance ) {
                            errorList.push( `Cannot find generated attendance for Employee ${employeeId}, ${fullName} for the selected payroll period.` );
                        } else {
                            details.forEach( ( date ) => {
                                errorList.push( `Missing timesheet data for Employee ${employeeId}, ${fullName} on Date ${date}.` );
                            });
                        }
                    } else {
                        errorList.push( description );
                    }

                    break;
                case MISSING_EMPLOYEE:
                    if ( details ) {
                        details.forEach( ({ employeeId: _employeeId, fullName: _fullName }) => {
                            errorList.push( `Employee ${_employeeId}, ${_fullName} is part of the selected payroll period but does not exist in Payroll Attendance Upload.` );
                        });
                    } else {
                        errorList.push( description );
                    }

                    break;
                default:
                    errorList.push( description );
            }
        });
    } else {
        errorList.push( error.name );
    }

    return errorList;
}

/**
 * Formats parameter name
 *
 * @param {String} name
 * @returns {String}
 */
function formatParameterName( name ) {
    return `${PARAMETERS_WITH_EMPLOYEE.includes( name ) ? 'Employee ' : ''}${name}`;
}

/**
 * Get unit for expected format
 * @param {String} format - Expected format of parameter
 * @returns {String}
 */
function getUnitForExpectedFormat( format ) {
    let unit = '';
    switch ( format ) {
        case 'HH:MM':
            unit = 'time';
            break;
        case 'MM/DD/YYYY':
            unit = 'date';
            break;
        default:
    }

    return unit;
}
