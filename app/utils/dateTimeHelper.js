/* eslint-disable no-restricted-properties */
/* eslint-disable radix */
/* eslint-disable require-jsdoc */
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import { forEach } from 'lodash-es';
import { DATE_FORMATS } from 'constants';

const moment = extendMoment( Moment );
export const TIME_FORMATS = 'HH:mm';

export function calculateTimeDifference( startTime, endTime ) {
    return moment.utc( moment( endTime, TIME_FORMATS )
    .diff( moment( startTime, TIME_FORMATS ) ) )
    .format( TIME_FORMATS );
}

export function calculateTotalTime( times ) {
    const time = !times ? [] : times.filter( Boolean );
    const totalTime = moment( '00:00', TIME_FORMATS );

    time && time.forEach( ( value ) => {
        const values = value.split( ':' );

        totalTime.add({
            hours: +values[ 0 ],
            minutes: +values[ 1 ]
        });
    });

    return totalTime;
}

export function isTimeWithinTimeInterval( value, intervalItem ) {
    const time = moment( value, TIME_FORMATS );
    const interval = convertTimeInterval( intervalItem );

    if ( interval.start.isSame( interval.end ) ) return false;

    if ( interval.start.isBefore( interval.end ) ) {
        if ( time.isBefore( interval.start ) || time.isAfter( interval.end ) ) return false;

        return true;
    }

    if ( time.isSameOrAfter( interval.start ) || time.isSameOrBefore( interval.end ) ) return true;

    return false;
}

export function isValidSubintervalWithinTimeInterval( sub, main ) {
    const subInterval = convertTimeInterval( sub );
    const interval = convertTimeInterval( main );

  // if subinterval has same start and end time
    if ( subInterval.start.isSame( subInterval.end ) ) return false;

  // if its overnight schedule
    if ( interval.start.isAfter( interval.end ) ) {
    // if its subinterval is before midnight, e.g (22-23)
        if ( subInterval.start.isSameOrAfter( interval.start ) &&
      subInterval.end.isSameOrAfter( interval.start ) &&
      subInterval.start.isBefore( subInterval.end ) ) {
            return true;
        }
    // if its subinterval is after midnight, e.g (01-03)
        if ( subInterval.start.isSameOrBefore( interval.end ) &&
      subInterval.end.isSameOrBefore( interval.end ) &&
      subInterval.start.isBefore( subInterval.end ) ) {
            return true;
        }
    // if its subinterval is over midnight, e.g (23-03)
        if ( subInterval.start.isSameOrAfter( interval.start ) &&
      subInterval.end.isSameOrBefore( interval.end ) &&
      subInterval.start.isAfter( subInterval.end ) ) {
            return true;
        }
        return false;
    }

    if ( subInterval.start.isSameOrAfter( subInterval.end ) ) return false;

    return true;
}

export function isTimeOutsideTimeIntervals( time, intervals ) {
    let flag = true;

    // eslint-disable-next-line consistent-return
    forEach( intervals, ( interval ) => {
        if ( isTimeWithinTimeInterval( time, interval ) ) flag = false;
    });

    return flag;
}

/**
 * Check if date interval contains any date instance from dates
 *
 * @param {Object} interval Date interval with start and end props in 'YYYY-MM-DD' format
 * @param {Array} dates Array of dates in 'YYYY-MM-DD' format
 * @return {Boolean}
 */
export function isDateIntervalOverlapsDates( intervalItem, dates ) {
    const interval = convertDateInterval( intervalItem );

    for ( let date of dates ) {
        date = moment( date, DATE_FORMATS.DEFAULT );
    // check if date interval contain current date
        if ( interval.start.isSameOrBefore( date ) && interval.end.isSameOrAfter( date ) ) {
            return true;
        }
    }

    return false;
}

/**
 * Convert date interval with start and end props in 'YYYY-MM-DD' format to moment objects
 *
 * @param {Object} interval Date interval with start and end props in 'YYYY-MM-DD' format
 * @return {Object} Moment object of date interval
 */
export function convertDateInterval( interval ) {
    return {
        start: moment( interval.start, DATE_FORMATS.DEFAULT ),
        end: moment( interval.end, DATE_FORMATS.DEFAULT )
    };
}

export function convertTimeInterval( interval ) {
    return {
        start: moment( interval.start, TIME_FORMATS ),
        end: moment( interval.end, TIME_FORMATS )
    };
}

/**
 * Convert time string to integer. The first segment is presumed to be hours.
 * Accepts time in these formats: HH, HH:mm, HH:mm:ss
 * .
 * @param {String} time
 * @return {Number}
 */
export function convertTimeStringToNumber( time ) {
    return time.split( ':' ).reduce( ( sum, segment, index ) => {
        const value = parseInt( segment );

        return sum + ( value === 0 ? value : value / Math.pow( 60, index ) );
    }, 0 );
}

/**
 * Convert time string to number of minutes. First segment is presumed to be hours.
 * Accepts time in these formats: HH, HH:mm, HH:mm:ss
 *
 * @param {String} time
 * @return {Number}
 */
export function convertTimeStringToMinutes( time ) {
    return parseInt( convertTimeStringToNumber( time ) * 60 );
}

/**
 * Convert number of minutes to time string.
 *
 * @param {Number} minutes
 * @return {String} HH:mm
 */
export function minutesToTimeString( minutes ) {
    const hours = `${Math.floor( minutes / 60 )}`;
    const minutesLeft = `${minutes % 60}`;

    return `${hours.padStart( 2, '0' )}:${minutesLeft.padStart( 2, '0' )}`;
}

export function convertDatetimeInterval( interval ) {
    const startTime = moment( interval.start, TIME_FORMATS );
    const endTime = moment( interval.end, TIME_FORMATS );

    const startDate = moment( new Date() );
    const endDate = moment( new Date() );

    if ( startTime.isAfter( endTime ) ) {
        endDate.add( 1, 'days' );
    }

    return {
        start: moment( `${startDate.format( 'YYYY-MM-DD' )} ${interval.start}`, 'YYYY-MM-DD HH:mm' ),
        end: moment( `${endDate.format( 'YYYY-MM-DD' )} ${interval.end}`, 'YYYY-MM-DD HH:mm' )
    };
}

export function isNotOverlapping( datetime, start, end, type = null ) {
    if ( type === 'end' ) {
        if ( datetime.isSame( end ) ) return false;
    }

    if ( type === 'start' ) {
        if ( datetime.isSame( start ) ) return false;
    }

    if ( datetime.isBetween( start, end ) ) return false;

    if ( datetime.isAfter( start ) && datetime.isBefore( end ) ) return false;

    if ( datetime.isBefore( start ) && datetime.isAfter( end ) ) return false;

    return true;
}

export function isTimeIntervalsHasNoOverLap( sub, main ) {
    const subInterval = convertDatetimeInterval( sub );
    const interval = convertDatetimeInterval( main );

  // if subinterval has same start and end time
    if ( subInterval.start.isSame( subInterval.end ) ) return false;

    if ( subInterval.start.isSame( interval.start ) && subInterval.end.isSame( interval.end ) ) return false;

  // convert to simple array
    const subIntervalValues = Object.values( subInterval );
    const intervalValues = Object.values( interval );

    const subIntervalRange = moment.range( subIntervalValues );
    const intervalRange = moment.range( intervalValues );

  // has overlapping
    if ( subIntervalRange.overlaps( intervalRange ) ) {
        return false;
    }

    return true;
}

export function isTimeOutsideOvertimeIntervals( value, intervals ) {
    let flag = true;
    // eslint-disable-next-line consistent-return
    forEach( intervals, ( interval ) => {
        if ( isTimeWithinTimeInterval( value, interval ) ) flag = false;
    });

    return flag;
}

export function isTimeOutsideTimeInterval( time, intervalItem ) {
    const interval = convertDatetimeInterval( intervalItem );
    const bufferInHours = 4;

    const date = moment( new Date() );
    const datetime = moment( `${date.format( 'YYYY-MM-DD' )} ${time}`, 'YYYY-MM-DD HH:mm' );

  // get shift end to given time total hours
    const duration = moment.duration( interval.end.diff( datetime ) );
    const hoursDiff = duration.asHours();

  // get shift total hours
    const shiftHours = moment.duration( interval.end.diff( interval.start ) );
    const shiftHourssDiff = shiftHours.asHours();

  // subtract results, if total is greater than the buffer then given time date is set to next day
    const totalHoursDiff = hoursDiff - shiftHourssDiff;
    if ( totalHoursDiff > bufferInHours ) {
        datetime.add( 1, 'days' );
    }

    return isNotOverlapping( datetime, interval.start, interval.end );
}
