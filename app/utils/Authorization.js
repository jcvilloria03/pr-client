/**
 * Checks whether user has authorization with given task(s).
 * Returns a boolean value for single task.
 * Return a list of boolean results for multiple tasks.
 * @param task = task to check
 */
export function isAuthorized( tasks, callback ) {
    const authorization = {};

    tasks.forEach( ( task ) => {
        authorization[ task ] = true;
    });

    callback( authorization );
}
