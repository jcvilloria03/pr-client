import countriesAndTimezones from 'countries-and-timezones';

const ctz = countriesAndTimezones.getAllTimezones();
export const timezones = Object.keys( ctz ).map( ( tz ) => {
    const { name, utcOffsetStr, utcOffset, countries } = ctz[ tz ];
    return {
        label: `UTC${utcOffsetStr} ${name.replace( /_/g, ' ' )}`,
        value: name,
        utcOffset,
        countries
    };
}).sort( ( a, b ) => a.utcOffset - b.utcOffset ).filter( ({ countries }) => countries.length );

export const defaultTimezone = timezones.find( ( tz ) => tz.value === 'Asia/Manila' );
