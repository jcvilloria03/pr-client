import uniq from 'lodash/uniq';

const requiredSteps = {
    hris: [],
    payroll: [
        'company', 'user', 'department', 'rank', 'cost_center', 'location',
        'payroll_group', 'employee'
    ],
    'time and attendance': [
        'company', 'user', 'department', 'position', 'employment_type', 'rank',
        'team', 'project', 'default_schedule', 'location', 'employee'
    ],
    'time and attendance plus payroll': [
        'company', 'user', 'department', 'position', 'employment_type', 'rank',
        'cost_center', 'team', 'project', 'default_schedule', 'location',
        'payroll_group', 'employee'
    ]
};

/**
 * Onboading helper
 * @type {OnboardingService}
 */
export default class OnboardingService {
    /**
     * Check completed steps depending on the subscribed products.
     * @param {Array} products
     * @param {Object} progress
     * @return {Boolean}
     */
    completedFor( products, progress ) {
        // Get steps needed for subscribed products
        const steps = uniq( products.reduce( ( accumulator, current ) =>
            accumulator.concat( requiredSteps[ current ] || []), []) );

        // With no subscription there are no steps required
        if ( !steps.length ) {
            return false;
        }

        let completed = true;
        // If any is not completed we return false
        steps.forEach( ( step ) => {
            if ( !progress[ step ]) {
                completed = false;
            }
        });

        return completed;
    }
}

export const onboarding = new OnboardingService();
