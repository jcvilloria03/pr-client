import decode from 'jwt-decode';
/**
 * Determines Expiration Date based on passed token.
 */
export function getTokenExpirationDate( token ) {
    const decoded = decode( token );
    if ( !decoded.exp ) {
        return null;
    }

    const date = new Date( 0 ); // The 0 here is the key, which sets the date to the epoch
    date.setUTCSeconds( decoded.exp );
    return date;
}

/**
 * Checks if the saved token is still valid.
 */
export function isTokenExpired( token ) {
    try {
        const date = getTokenExpirationDate( token );
        if ( date === null ) {
            return false;
        }
        return !( date.valueOf() > new Date().valueOf() );
    } catch ( error ) {
        return true;
    }
}

/**
 * Checks if user hav verified email after login
 */
export function isEmailVerified() {
    let profile = localStorage.getItem( 'user' );
    profile = profile ? JSON.parse( profile ) : null;
    if ( !( profile && profile.email_verified_at ) ) {
        return null;
    }
    return !!profile.email_verified_at;
}

/**
 * Gets user's user_id
 */
export function getUserID( token ) {
    const decoded = decode( token );
    if ( !decoded.sub ) {
        return null;
    }
    return decoded.sub;
}

/**
 * Gets user's nickname
 */
export function getName( profile ) {
    if ( !profile && !profile.first_name ) {
        return null;
    }
    return profile.first_name;
}
