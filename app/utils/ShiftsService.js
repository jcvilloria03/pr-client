/* eslint-disable no-param-reassign */

import cloneDeep from 'lodash/cloneDeep';
import values from 'lodash/values';
import clone from 'lodash/clone';
import isEmpty from 'lodash/isEmpty';
import groupBy from 'lodash/groupBy';
import each from 'lodash/each';
import find from 'lodash/find';
import sortBy from 'lodash/sortBy';
import startsWith from 'lodash/startsWith';

import moment from 'moment';
import 'moment-recur';

import {
    DEFAULT_SHIFT_DATETIME_FORMAT,
    DATE_FORMAT,
    NOT_AVAILABLE,
    REST_DAY,
    REST_DAY_TYPE
} from './constants';

/**
 * Shift service class
 */
export default class ShiftsService {

    /**
     * Parse shifts
     * @param {Array} shifts
     *
     * @return {Object} parsed shifts
     */
    parseShifts( shifts, selected, expanded ) {
        const parsedShifts = {};

        shifts.forEach( ( shift ) => {
            shift.dates.forEach( ( date ) => {
                if ( !parsedShifts[ date ]) {
                    parsedShifts[ date ] = [];
                }

                const shiftHasSchedule = Object.prototype.hasOwnProperty.call( shift, 'schedule' );
                const found = shiftHasSchedule && find( parsedShifts[ date ], ({ name }) => name === shift.schedule.name );
                if ( !found ) {
                    parsedShifts[ date ].push(
                        shiftHasSchedule ?
                        {
                            schedule: shift.schedule,
                            shift_id: shift.id,
                            date,
                            start: shift.schedule.start_time,
                            end: shift.schedule.end_time,
                            selected,
                            expanded
                        }
                        :
                        {
                            day_type: REST_DAY_TYPE,
                            schedule: {
                                name: REST_DAY,
                                start_time: NOT_AVAILABLE,
                                end_time: NOT_AVAILABLE,
                                id: null
                            },
                            shift_id: null,
                            date,
                            start: NOT_AVAILABLE,
                            end: NOT_AVAILABLE,
                            expanded: false,
                            selected: false
                        }
                    );
                }
            });
        });

        return parsedShifts;
    }

    /**
     * Parses Default Schedule for Display
     *
     * @param {Object} defaultSchedule
     * @param {Object} date
     *
     * @returns {Object}
     */
    parseDefaultScheduleForDisplay( defaultSchedule, date, expanded = false, selected = false ) {
        const start = moment( defaultSchedule.work_start, 'HH:mm:ss' ).format( 'HH:mm' );
        const end = moment( defaultSchedule.work_end, 'HH:mm:ss' ).format( 'HH:mm' );
        const breakStart = moment( defaultSchedule.work_break_start, 'HH:mm:ss' ).format( 'HH:mm' );
        const breakEnd = moment( defaultSchedule.work_break_end, 'HH:mm:ss' ).format( 'HH:mm' );
        const shiftId = `ds_${defaultSchedule.id}`;

        // Determine the shift id. if day type is rest day,
        // the shift id should be 'rd' otherwise, check if there's a
        // prefix ds_ if not present, concat the string ds_ as prefix
        // otherwise, use the current defaultSchedule.id.
        // By history, this is the current implementation of both BE and FE
        // due to id conflicts of default schedules tbl and shifts table

        const schedule = {
            day_type: defaultSchedule.day_type,
            schedule: {
                name: this.getDefaultScheduleNameForDayType( defaultSchedule.day_type ),
                start_time: start,
                end_time: end,
                id: null,
                breaks: [{
                    start: breakStart,
                    end: breakEnd,
                    type: 'fixed'
                }]
            },
            shift_id: shiftId,
            date,
            start,
            end,
            expanded,
            selected
        };

        return schedule;
    }

    /**
     * Gets default schedule's name depending of day type.
     *
     * @param {String} dayType regular|rest_day
     *
     * @returns {String}
     */
    getDefaultScheduleNameForDayType( dayType ) {
        const defaultScheduleName = 'Default Schedule';

        return dayType === 'regular' ? defaultScheduleName : `${defaultScheduleName} - Rest Day`;
    }

    /**
     * Check if it's night shift.
     *
     * @param {String} startTime
     * @param {String} endTime
     * @return {Boolean}
     */
    isNightShift( startTime, endTime ) {
        return moment( startTime, 'HH:mm' ).isSameOrAfter( moment( endTime, 'HH:mm' ) );
    }

    /**
     * Computes the hours per day on a default schedule.
     *
     * @param {Object} leave The default schedule assigned to an emplyoee.
     * @param {Object} duration A time duration in hours or minutes.
     * @param {Object} unit The time unit to expect for calculation. Possible values:
     *  asHours - Hours calculation
     *  asMinutes - Minutes calculation
     * @returns {Number}
     */
    deductBreaksFromSchedule( shift, duration, unit = 'asHours' ) {
        let totalBreakTime = 0;
        let shiftStart = null;
        let shiftEnd = null;

        if ( isEmpty( shift ) || isEmpty( shift.schedule ) ) {
            return duration;
        }

        if ( !isEmpty( shift.start ) && !isEmpty( shift.end ) ) {
            shiftStart = moment( shift.start, 'HH:mm' );
            shiftEnd = moment( shift.end, 'HH:mm' );
        } else if ( !isEmpty( shift.schedule.start_time ) && !isEmpty( shift.schedule.end_time ) ) {
            shiftStart = moment( shift.schedule.start_time, 'HH:mm' );
            shiftEnd = moment( shift.schedule.end_time, 'HH:mm' );
        }

        // if shiftEnd is less than shiftStart
        // this means shift end is next day
        if ( shiftEnd.isBefore( shiftStart ) ) {
            shiftEnd.add( 1, 'days' );
        }

        // Handle breaks for custom defined shift. A custom shift
        // should always have a `breaks` array that contains the
        // start and end time of breaks
        if ( !isEmpty( shift.schedule.breaks ) ) {
            shift.schedule.breaks.forEach( ( item ) => {
                // Deduct when the break is unpaid. Otherwise, skip it
                if ( item.is_paid === true ) {
                    return;
                }

                if ( item.type === 'floating' || item.type === 'flexi' ) {
                    totalBreakTime += moment.duration( item.break_hours )[ unit ]();
                } else {
                    const shiftStartDate = shiftStart.format( 'YYYY-MM-DD' );
                    const shiftEndDate = shiftEnd.format( 'YYYY-MM-DD' );

                    let breakStart = moment( `${shiftStartDate} ${item.start}`, 'YYYY-MM-DD HH:mm' );
                    let breakEnd = moment( `${shiftStartDate} ${item.end}`, 'YYYY-MM-DD HH:mm' );

                    if ( breakEnd.isBefore( breakStart ) ) {
                        breakEnd.add( 1, 'days' );
                    }

                    // Determine the date of the breaks
                    if ( shiftStart > breakStart ) {
                        breakStart = moment( `${shiftEndDate} ${item.start}`, 'YYYY-MM-DD HH:mm' );
                        breakEnd = moment( `${shiftEndDate} ${item.end}`, 'YYYY-MM-DD HH:mm' );
                    }

                    // If the break hours is still within the shift start and end, deduct the breaks
                    if ( breakStart.isBetween( shiftStart, shiftEnd ) && breakEnd.isBetween( shiftStart, shiftEnd ) ) {
                        totalBreakTime += moment.duration( breakEnd.diff( breakStart ) )[ unit ]();
                    }
                }
            });
        } else if ( !isEmpty( shift.schedule.work_break_start ) && !isEmpty( shift.schedule.work_break_end ) ) {
            // Handle breaks of a default schedule. A default schedule
            // Should have a schedule work_break_start and work_break_end
            // key value pairs.
            const breakStart = moment( shift.schedule.work_break_start, 'HH:mm:ss' );
            const breakEnd = moment( shift.schedule.work_break_end, 'HH:mm:ss' );

            if ( breakEnd.isBefore( breakStart ) ) {
                breakEnd.add( 1, 'days' );
            }

            // If the break hours is still within the shift start and end, deduct the breaks
            if ( breakStart.isBetween( shiftStart, shiftEnd ) && breakEnd.isBetween( shiftStart, shiftEnd ) ) {
                totalBreakTime += moment.duration( breakEnd.diff( breakStart ) )[ unit ]();
            }

            totalBreakTime += moment.duration( breakEnd.diff( breakStart ) )[ unit ]();
        }

        return duration - totalBreakTime;
    }

    /**
     * Deduct unpaid breaks from clock pairs duration.
     *
     * @param {Object} shift The schedule assigned to an emplyoee.
     * @param {Object} clockPairs clockPairs rendered by employee.
     * @param {Object} duration A time duration in hours or minutes.
     * @param {string} unit The time unit to expect for calculation. Possible values:
     *  asHours - Hours calculation
     *  asMinutes - Minutes calculation
     * @returns {Number}
     */
    deductBreaksFromClockpairsDuration( shift, clockPairs, duration, unit = 'asHours' ) {
        let totalBreakTime = 0;
        let shiftStart = null;
        let shiftEnd = null;
        const shiftStartDate = moment( shift.date );
        const shiftEndDate = moment( shift.date );
        const shiftStartDateTime = moment( `${shift.date} ${shift.start || shift.schedule.start_time}`, DEFAULT_SHIFT_DATETIME_FORMAT );

        if ( isEmpty( shift ) || isEmpty( shift.schedule ) ) {
            return duration;
        }

        if ( !isEmpty( shift.start ) && !isEmpty( shift.end ) ) {
            shiftStart = moment( shift.start, 'HH:mm' );
            shiftEnd = moment( shift.end, 'HH:mm' );
        } else if ( !isEmpty( shift.schedule.start_time ) && !isEmpty( shift.schedule.end_time ) ) {
            shiftStart = moment( shift.schedule.start_time, 'HH:mm' );
            shiftEnd = moment( shift.schedule.end_time, 'HH:mm' );
        }

        // if shiftEnd is less than shiftStart
        // this means shift end is next day
        if ( shiftEnd.isBefore( shiftStart ) ) {
            shiftEnd.add( 1, 'days' );
            shiftEndDate.add( 1, 'days' );
        }

        // Handle breaks for custom defined shift. A custom shift
        // should always have a `breaks` array that contains the
        // start and end time of breaks
        if ( !isEmpty( shift.schedule.breaks ) ) {
            shift.schedule.breaks.forEach( ( item ) => {
                // Deduct when the break is unpaid. Otherwise, skip it
                if ( item.is_paid === true ) {
                    return;
                }
                let breakStart = moment( `${shiftStartDate.format( 'YYYY-MM-DD' )} ${item.start}`, 'YYYY-MM-DD HH:mm' );
                let breakEnd = moment( `${shiftStartDate.format( 'YYYY-MM-DD' )} ${item.end}`, 'YYYY-MM-DD HH:mm' );

                if ( breakEnd.isBefore( breakStart ) ) {
                    breakEnd.add( 1, 'days' );
                }

                // Determine the date of the breaks
                if ( shiftStartDateTime > breakStart ) {
                    breakStart = moment( `${shiftEndDate.format( 'YYYY-MM-DD' )} ${item.start}`, 'YYYY-MM-DD HH:mm' );
                    breakEnd = moment( `${shiftEndDate.format( 'YYYY-MM-DD' )} ${item.end}`, 'YYYY-MM-DD HH:mm' );
                }

                if ( item.type === 'floating' ) {
                    if ( clockPairs.length === 1 ) {
                        totalBreakTime += moment.duration( item.break_hours )[ unit ]();
                    } else {
                        // get the total break taken by employee within the time range
                        // if the taken break is less than break_hours then deduct the remaining
                        // break time from the employee's work duration since this is an unpaid break.
                        const breakTaken = this.getBreakTakenDuringTimeRange( clockPairs, this.getShiftRange( shift ), 'asMinutes' );
                        if ( breakTaken < moment.duration( item.break_hours )[ unit ]() ) {
                            totalBreakTime += moment.duration( item.break_hours )[ unit ]() - breakTaken;
                        }
                    }
                } else if ( item.type === 'flexi' ) {
                    if ( clockPairs.length === 1 ) {
                        totalBreakTime += moment.duration( item.break_hours )[ unit ]();
                    } else if ( clockPairs.length > 1 ) {
                        const breakTaken = this.getBreakTakenDuringTimeRange( clockPairs, {
                            start: breakStart,
                            end: breakEnd
                        }, 'asMinutes' );
                        if ( breakTaken < moment.duration( item.break_hours )[ unit ]() ) {
                            totalBreakTime += moment.duration( item.break_hours )[ unit ]() - breakTaken;
                        }
                    }
                } else {
                    // If the break hours is still within the shift start and end, deduct the breaks
                    clockPairs.forEach( ( clockPair ) => {
                        let clockIn = moment( clockPair.clockIn.datetime, DEFAULT_SHIFT_DATETIME_FORMAT );
                        let clockOut = moment( clockPair.clockOut.datetime, DEFAULT_SHIFT_DATETIME_FORMAT );
                        if ( clockPair.clockIn.offset ) {
                            clockIn = clockIn.utcOffset( clockPair.clockIn.offset );
                        }

                        if ( clockPair.clockOut.offset ) {
                            clockOut = clockOut.utcOffset( clockPair.clockOut.offset );
                        }
                        if ( breakStart.isBetween( clockIn, clockOut ) && breakEnd.isBetween( clockIn, clockOut ) ) {
                            totalBreakTime += moment.duration( breakEnd.diff( breakStart ) )[ unit ]();
                        } else if ( breakStart.isBetween( clockIn, clockOut ) ) {
                            totalBreakTime += moment.duration( clockOut.diff( breakStart ) )[ unit ]();
                        } else if ( breakEnd.isBetween( clockIn, clockOut ) ) {
                            totalBreakTime += moment.duration( breakEnd.diff( clockIn ) )[ unit ]();
                        }
                    });
                }
            });
        } else if ( !isEmpty( shift.schedule.work_break_start ) && !isEmpty( shift.schedule.work_break_end ) ) {
            // Handle breaks of a default schedule. A default schedule
            // Should have a schedule work_break_start and work_break_end
            // key value pairs.
            const breakStart = moment( `${shiftStartDate.format( 'YYYY-MM-DD' )} ${shift.schedule.work_break_start}`, 'YYYY-MM-DD HH:mm' );
            const breakEnd = moment( `${shiftEndDate.format( 'YYYY-MM-DD' )} ${shift.schedule.work_break_end}`, 'YYYY-MM-DD HH:mm' );

            if ( breakEnd.isBefore( breakStart ) ) {
                breakEnd.add( 1, 'days' );
            }

            // If the break hours is still within the shift start and end, deduct the breaks
            clockPairs.forEach( ( clockPair ) => {
                let clockIn = moment( clockPair.clockIn.datetime, DEFAULT_SHIFT_DATETIME_FORMAT );
                let clockOut = moment( clockPair.clockOut.datetime, DEFAULT_SHIFT_DATETIME_FORMAT );
                if ( clockPair.clockIn.offset ) {
                    clockIn = clockIn.utcOffset( clockPair.clockIn.offset );
                }

                if ( clockPair.clockOut.offset ) {
                    clockOut = clockOut.utcOffset( clockPair.clockOut.offset );
                }
                if ( breakStart.isBetween( clockIn, clockOut ) && breakEnd.isBetween( clockIn, clockOut ) ) {
                    totalBreakTime += moment.duration( breakEnd.diff( breakStart ) )[ unit ]();
                } else if ( breakStart.isBetween( clockIn, clockOut ) ) {
                    totalBreakTime += moment.duration( clockOut.diff( breakStart ) )[ unit ]();
                } else if ( breakEnd.isBetween( clockIn, clockOut ) ) {
                    totalBreakTime += moment.duration( breakEnd.diff( clockIn ) )[ unit ]();
                }
            });
        }
        return duration - totalBreakTime;
    }

    /**
     * Return total break taken within the time range
     *
     * @param {Object} clockPairs
     * @param {Object} timeRange
     * timeRange = {start, end}
     * @param {string} unit
     */
    getBreakTakenDuringTimeRange( clockPairs, timeRange, unit ) {
        let totalBreakTaken = 0;
        if ( clockPairs.length === 1 ) {
            return totalBreakTaken;
        }
        for ( let i = 0; i < clockPairs.length - 1; i += 1 ) {
            let firstClockOut = moment( clockPairs[ i ].clockOut.datetime, DEFAULT_SHIFT_DATETIME_FORMAT );
            let nextClockIn = moment( clockPairs[ i + 1 ].clockIn.datetime, DEFAULT_SHIFT_DATETIME_FORMAT );
            if ( clockPairs[ i ].clockOut.offset ) {
                firstClockOut = firstClockOut.utcOffset( clockPairs[ i ].clockOut.offset );
            }

            if ( clockPairs[ i + 1 ].clockIn.offset ) {
                nextClockIn = nextClockIn.utcOffset( clockPairs[ i + 1 ].clockIn.offset );
            }
            if ( firstClockOut.isBetween( timeRange.start, timeRange.end, null, '[]' ) && nextClockIn.isBetween( timeRange.start, timeRange.end, null, '[]' ) ) {
                totalBreakTaken += moment.duration( nextClockIn.diff( firstClockOut ) )[ unit ]();
            }
        }
        return totalBreakTaken;
    }

    /**
     * Return shift range
     *
     * @param {Object} shift
     * @param {Object}
     */
    getShiftRange( shift ) {
        let start = null;
        let end = null;

        // Terminate this method if there are no shift or schedule
        // present.
        if ( isEmpty( shift ) || isEmpty( shift.schedule ) ) {
            return null;
        }

        // Get data from a custom shift if the schedule start time/ end time is
        // present. Those fields exists only in a custom shift otherwise, default
        // to a default shift data.
        if ( !isEmpty( shift.schedule.start_time ) && !isEmpty( shift.schedule.end_time ) ) {
            start = moment(
                `${shift.date} ${shift.schedule.start_time}`, DEFAULT_SHIFT_DATETIME_FORMAT
            );
            end = moment(
                `${shift.date} ${shift.schedule.end_time}`, DEFAULT_SHIFT_DATETIME_FORMAT
            );
        } else if ( !isEmpty( shift.schedule.work_start ) && !isEmpty( shift.schedule.work_end ) ) {
            // Using default schedule at this point
            start = moment(
                `${shift.date} ${shift.schedule.work_start}`, DEFAULT_SHIFT_DATETIME_FORMAT
            );
            end = moment(
                `${shift.date} ${shift.schedule.work_end}`, DEFAULT_SHIFT_DATETIME_FORMAT
            );
        }

        if ( start && end && end.isBefore( start ) ) {
            end.add( 1, 'days' );
        }

        return { start, end };
    }

    /**
     * Process rest days and return repeated rest days for date range.
     * @param {Array} restDays
     * @param {Object} interval
     * @return {Array}
     */
    processRestDays( restDays, interval ) {
        let restDaysList = [];

        each( restDays, ( restDay ) => {
            restDaysList = restDaysList.concat( this.processRestDay( restDay, interval ) );
        });

        return restDaysList;
    }

    /**
     * Process one rest day and return repeated events.
     * @param {Object} restDay
     * @param {Object} interval
     * @return {Array}
     */
    processRestDay( restDay, interval ) {
        restDay.original = cloneDeep( restDay );

        if ( !restDay.repeat ) {
          // If it's non-repeating rest day we need to check if it's inside the viewed
          // interval so that we don't give it to be rendered by mistake.
            if ( !this.isRestDayStartDateInsideInterval( restDay, interval ) ) {
                return [];
            }

            return [this.processSingleEvent( restDay )];
        }

        // No need to process further restDay if it's not before the end of interval.
        if ( moment( restDay.start_date ).isAfter( interval.end ) ) {
            return [];
        }

        const intervalEnd = this.getIntervalEnd( restDay, interval.end );

        return this.processRepeated( restDay, interval.start, intervalEnd );
    }

    /**
     * Get lower limit from interval end and rest day end date
     * @param {Object} restDay
     * @param {String} intervalEnd
     * @return {String}
     */
    getIntervalEnd( restDay, intervalEnd ) {
        if ( moment( restDay.end_date ).isBefore( moment( intervalEnd ) ) ) {
            return restDay.end_date;
        }

        return intervalEnd;
    }

    /**
     * Process restDay with "weekly" repeat type.
     * @param {Object} restDay
     * @param {Moment} intervalStart
     * @param {Moment} intervalEnd
     * @return {Array}
     */
    processRepeated( restDay, intervalStart, intervalEnd ) {
        if ( restDay && !restDay.repeat ) return;

        // Define recurrence
        const recurrence = moment( restDay.start_date, '' ).recur( intervalEnd )
            .daysOfWeek( restDay.repeat.rest_days );

        const dates = this.getWeeklyOccurrenceDates( restDay, recurrence, intervalStart );
        // Make restDay for every occurrence.
        return this.mapDatesToRestDays( dates, restDay );  // eslint-disable-line consistent-return
    }
    /**
     * Limit occurrences and map dates to single event.
     * @param {Array} dates
     * @param {Object} restDay
     * @return {Array}
     */
    mapDatesToRestDays( dates, restDay ) {
        return dates.map( ( date ) => {
            const event = clone( restDay );

            event.start_date = date;

            return this.processSingleEvent( event );
        });
    }

    /**
     * Process single event to set start, end and title.
     * @param {Object} restDay
     * @return {Object}
     */
    processSingleEvent( restDay ) {
        restDay.start = restDay.start_date;
        restDay.end = this.getEventEnd( restDay );

        return restDay;
    }

    /**
     * Calculate event end.
     * If the end time is before start time, that means we need date for tomorrow.
     * @param {Object} restDay
     * @return {String}
     */
    getEventEnd( restDay ) {
        return `${restDay.start_date} 23:59:59`;
    }

    /**
     * Check if rest day start date is inside given date interval.
     * @param {Object} restDay
     * @param {Object} interval
     * @return {Boolean}
    */
    isRestDayStartDateInsideInterval( restDay, interval ) {
        const startDate = moment( restDay.start_date );

        return startDate.isBetween( interval.start, interval.end, 'day' ) ||
            startDate.isSame( interval.start, 'day' ) ||
            startDate.isSame( interval.end, 'day' );
    }

    /**
     * Get dates of monthly occurrences having in mind "end after" value.
     * @param {Object} restDay
     * @param {Object} recurrence
     * @param {Moment} intervalStart
     * @return {Array}
     */
    getWeeklyOccurrenceDates( restDay, recurrence, intervalStart ) {
        const groupedDates = values( groupBy( recurrence.all(), ( date ) =>
            date.format( 'YYYY' ) + date.week() ) );

        let allDates = [];

        // Get only every nth week
        groupedDates.forEach( ( dates, index ) => {
            if ( ( index % restDay.repeat.repeat_every ) === 0 ) {
                allDates = allDates.concat( dates );
            }
        });

        return this.getRequestedNumberOfOccurrencesAfterIntervalStart( allDates, restDay, intervalStart );
    }

    /**
     * Get requested number of occurrences and after interval start.
     * @param {Array} allDates
     * @param {Object} restDay
     * @param {Moment} intervalStart
     * @return {Array}
     */
    getRequestedNumberOfOccurrencesAfterIntervalStart( allDates, restDay, intervalStart ) {
        // Get only requested number of occurrences
        if ( restDay.repeat.end_after ) {
            allDates = allDates.splice( 0, restDay.repeat.end_after ); // eslint-disable-line no-param-reassign
        }

        // Get only dates after inside interval
        return allDates.filter( ( date ) => date.isSameOrAfter( intervalStart ) )
            .map( ( date ) => date.format( 'YYYY-MM-DD' ) );
    }

    /**
     * Gets parsedShift id and property name.
     * @param {Object} shift
     * @returns {Object} containing id and property name based on t
     */
    getIdPropertyByType( shift ) {
        if ( shift.shift_id ) {
            return { shift_id: shift.shift_id };
        }
        if ( shift.schedule_id ) {
            return { schedule_id: shift.schedule_id };
        }

        return { rest_day_id: shift.rest_day_id };
    }

    /**
     * Checks are shifts or schedule dates and ids same.
     * @param {Object} dayShift
     * @param {Object} employeShift
     * @param {Boolean} isSchedule false by default
     * @returns {Boolean}
     */
    areShiftsSame( dayShift, employeeShift ) {
        const dayShiftDate = moment( dayShift.date ).format( DATE_FORMAT );
        const employeeShiftDate = moment( employeeShift.date ).format( DATE_FORMAT );

        return this.areIdsSame( dayShift, employeeShift ) ||
            this.areShiftsDatesSame( dayShiftDate, employeeShiftDate );
    }

    /**
     * Checks are shifts or schedule ids same.
     * @param {Object} dayShift
     * @param {Object} employeShift
     * @returns {Boolean}
     */
    areIdsSame( dayShift, employeeShift ) {
        const dayShiftId = this.getIdByShiftType( dayShift );
        const employeeShiftId = this.getIdByShiftType( employeeShift );

        if ( dayShift.schedule && dayShift.schedule.id ) {
            return dayShiftId === employeeShiftId ||
                dayShift.schedule.id === employeeShiftId;
        }

        return dayShiftId === employeeShiftId;
    }
    /**
     * Compares dates of day shift and employeeShift.
     * @param {String} dayShiftDate
     * @param {String} employeeShiftDate
     * @returns {Boolean}
     */
    areShiftsDatesSame( dayShiftDate, employeeShiftDate ) {
        return moment( dayShiftDate ).isSame( employeeShiftDate, 'day' );
    }

    /**
     * Gets id by shift type.
     * @param {Object} shiftType
     * @returns {Number};
     */
    getIdByShiftType( shiftType ) {
        return shiftType.shift_id || shiftType.schedule_id || shiftType.rest_day_id;
    }

    /**
     * Gets shifts date time.
     * @param {String} date
     * @param {String} time
     * @returns {Object}
     */
    getShiftDateTime( date, time ) {
        return moment( `${date} ${time}` );
    }
    /**
     * Checks is shift overlaping with first next day shift.
     * @param {Object} shiftEnd moment object
     * @param {Array} nextDayShifts
     * @returns {Boolean}
     */
    isShiftBeforeNextDayShift( shiftEnd, nextDayShift ) {
        if ( this.isEventOrNextDayRestDayEventDefined( shiftEnd, nextDayShift ) ) {
            return true;
        }

        const nightShiftEnd = moment( shiftEnd, DEFAULT_SHIFT_DATETIME_FORMAT );
        const shiftStart = this.getShiftDateTime( nextDayShift.date, nextDayShift.start );
        const formatedShiftStart = shiftStart.format( DEFAULT_SHIFT_DATETIME_FORMAT );

        if ( !this.areDatesValidMomentObjects( formatedShiftStart, nightShiftEnd ) ) {
            return true;
        }

        return moment( nightShiftEnd ).isSameOrBefore( moment( formatedShiftStart ) );
    }

    /**
     * Checks are dates valid moment objects.
     * @param {Object} formatedShiftStart
     * @param {Object} nightShiftEnd
     * @returns {Boolean}
     */
    areDatesValidMomentObjects( formatedShiftStart, nightShiftEnd ) {
        return moment( formatedShiftStart ).isValid() && moment( nightShiftEnd ).isValid();
    }

    /**
     * Checks is next day without events assigned.
     * @param {String} shiftEnd
     * @param {Object} nextDayShift
     * @returns {Boolean}
     */
    isEventOrNextDayRestDayEventDefined( shiftEnd, nextDayShift ) {
        return !shiftEnd || !nextDayShift || nextDayShift.rest_day_id;
    }

    /**
     * Orders employeeDayEvents by event start in asc order.
     * @param {Array} dayEvents
     * @returns {Array}
     */
    sortDayEvents( dayEvents ) {
        return sortBy( dayEvents, ( event ) => event.start );
    }

    /**
     * Gets defaultSchedule for date.
     *
     * @param {Object} payload
     * @param {String} date
     *
     * @returns {object}
     */
    getDefaultScheduleForDate( payload, date ) {
        return find( payload.defaultScheduleData, ( defaultSchedule ) => (
            defaultSchedule.day_of_week === moment( date ).isoWeekday() )
        );
    }

    /**
     * Sorts object keys.
     *
     * @param {Object} unsortedObject
     *
     * @returns {Object} sorted.
     */
    sortObjectByKey( unsortedObject ) {
        const sorted = {};
        each( Object.keys( unsortedObject ).sort(), ( key ) => {
            sorted[ key ] = unsortedObject[ key ];
        });

        return sorted;
    }

    /**
     * This method determines the expected hours of shift either
     * default shift or a user defined shift. Please note that
     * all calculations done in this method uses minutes as the
     * main unit of computation.
     *
     * @param {Object} shift
     *
     * @returns {Object} sorted.
     */
    getExpectedHours( shift ) {
        let expectedHours = 0;

        // The passed parameter chould be null or blank so,
        if ( isEmpty( shift ) ) {
            return '00:00';
        }

        // The current shift data is either a Rest Day, Default Schedule or User defined fixed schedule.
        if ( startsWith( shift.shift_id, 'ds_' ) || shift.schedule.type === 'fixed' ) {
            // Calculate the time duration of shift from its start time and end time in minutes.
            expectedHours = this.calculateDuration( shift.schedule.start_time, shift.schedule.end_time, 'asMinutes' );

            // Deduct breaks from expected hours in minutes.
            expectedHours = this.deductBreaksFromSchedule( shift, expectedHours, 'asMinutes' );
        } else if ( shift.schedule.type === 'flexi' ) {
            // Get total hours from a FIXED shift
            return shift.schedule.total_hours;
        } else if ( shift.day_type === 'rest_day' ) {
            // There are no expected hours for a rest day.
            return 'N/A';
        }

        // For the final result, return the computed time in minutes to a human readable hour time.
        return this.formatMinutesToTime( expectedHours );
    }

    /**
     * This method computes the time difference or duration
     * of a given start time and end time
     *
     * @param {Object} startTime
     * @param {Object} endTime
     *
     * @returns {Object} sorted.
     */
    calculateDuration( startTime, endTime, unit = 'asHours' ) {
        return moment.duration( moment( endTime, 'HH:mm' )
            .diff( moment( startTime, 'HH:mm' ) ) )[ unit ]();
    }

    /**
     * Format number of minutes into time string.
     * @param  {Number} minutes
     * @return {String} HH:mm
     */
    formatMinutesToTime( minutes ) {
        let h = parseInt( Math.floor( minutes / 60 ), 10 );
        let m = parseInt( minutes % 60, 10 );

        h = h < 10 ? `0${h}` : h;
        m = m < 10 ? `0${m}` : m;

        return `${h}:${m}`;
    }

    getScheduleForShift( defaultSchedules, shifts, shift, date ) {
        if ( shift.shift_id === 'rd' || startsWith( shift.shift_id, 'ds_' ) ) {
            const defaultSchedule = this.parseDefaultScheduleForDisplay(
                this.getDefaultScheduleForDate({ defaultScheduleData: defaultSchedules }, date ),
                date
            );

            return defaultSchedule && defaultSchedule.schedule;
        }

        return this.findShiftsScheduleById( shifts, shift );
    }

    findShiftsScheduleById( shifts, shiftData ) {
        const foundShift = find( shifts, ( shift ) => ( shift.id === shiftData.shift_id ) );

        return isEmpty( foundShift ) ? {} : foundShift.schedule;
    }
}

export const shiftsService = new ShiftsService();
