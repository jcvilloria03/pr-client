/* eslint-disable require-jsdoc */
/* eslint-disable no-param-reassign */
import moment from 'moment';

export function makeRange( startDate, endDate, format ) {
    const range = [];
    while ( startDate < endDate ) {
        range.push( startDate.format( format ) );
        startDate = moment( startDate ).add( 1, 'd' );
    }

    return range;
}

export function getIsoWeekday( date ) {
    return moment( date ).isoWeekday();
}
