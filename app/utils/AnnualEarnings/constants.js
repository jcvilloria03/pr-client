import React from 'react';

import { DATE_FORMATS } from 'utils/constants';

export const CURRENT_EMPLOYER_TAXABLE_INCOME_FIELDS = [
    'current_taxable_basic_salary',
    'current_taxable_13_month_pay_other_benefits',
    'current_taxable_salaries_other_forms_compensation'
];

export const PREVIOUS_EMPLOYER_TAXABLE_INCOME_FIELDS = [
    'prev_taxable_basic_salary',
    'prev_taxable_13_month_pay_other_benefits',
    'prev_taxable_salaries_other_forms_compensation'
];

export const CURRENT_EMPLOYER_NON_TAXABLE_INCOME_FIELDS = [
    'current_non_taxable_de_minimis',
    'current_non_taxable_salaries_other_forms_compensation',
    'current_non_taxable_13_month_pay_other_benefits'
];

export const PREVIOUS_EMPLOYER_NON_TAXABLE_INCOME_FIELDS = [
    'prev_non_taxable_de_minimis',
    'prev_non_taxable_salaries_other_forms_compensation',
    'prev_non_taxable_13_month_pay_other_benefits'
];

export const CURRENT_EMPLOYER_EMPLOYEE_CONTRIBUTION_FIELDS = [
    'current_sss_employee_share',
    'current_philhealth_employee_share',
    'current_pagibig_employee_share',
    'current_total_employee_contributions'
];

export const PREVIOUS_EMPLOYER_EMPLOYEE_CONTRIBUTION_FIELDS = [
    'prev_sss_employee_share',
    'prev_philhealth_employee_share',
    'prev_pagibig_employee_share',
    'prev_total_employee_contributions'
];

export const PREVIOUS_EMPLOYER_EMPLOYER_CONTRIBUTION_FIELDS = [
    'prev_sss_employer_share',
    'prev_philhealth_employer_share',
    'prev_pagibig_employer_share'
];

export const TOTAL_TAXABLE_INCOME_FIELDS = [
    ...CURRENT_EMPLOYER_TAXABLE_INCOME_FIELDS,
    ...PREVIOUS_EMPLOYER_TAXABLE_INCOME_FIELDS
];

export const TOTAL_NON_TAXABLE_INCOME_FIELDS = [
    ...CURRENT_EMPLOYER_NON_TAXABLE_INCOME_FIELDS,
    ...PREVIOUS_EMPLOYER_NON_TAXABLE_INCOME_FIELDS
];

export const CURRENT_EMPLOYER_GROSS_INCOME_FIELDS = [
    ...CURRENT_EMPLOYER_TAXABLE_INCOME_FIELDS,
    ...CURRENT_EMPLOYER_NON_TAXABLE_INCOME_FIELDS
];

export const PREVIOUS_EMPLOYER_GROSS_INCOME_FIELDS = [
    ...PREVIOUS_EMPLOYER_TAXABLE_INCOME_FIELDS,
    ...PREVIOUS_EMPLOYER_NON_TAXABLE_INCOME_FIELDS
];

export const TOTAL_GROSS_INCOME_FIELDS = [
    ...CURRENT_EMPLOYER_GROSS_INCOME_FIELDS,
    ...PREVIOUS_EMPLOYER_GROSS_INCOME_FIELDS
];

export const EMPLOYEE_CONTRIBUTION_FIELDS = [
    ...CURRENT_EMPLOYER_EMPLOYEE_CONTRIBUTION_FIELDS,
    ...PREVIOUS_EMPLOYER_EMPLOYEE_CONTRIBUTION_FIELDS
];

export const CURRENT_EMPLOYER_TOTAL_WITHHOLDING_TAX_FIELD = 'current_employer_withholding_tax';
export const PREVIOUS_EMPLOYER_TOTAL_WITHHOLDING_TAX_FIELD = 'prev_employer_withholding_tax';

export const TOTAL_WITHHOLDING_TAX_FIELDS = [
    CURRENT_EMPLOYER_TOTAL_WITHHOLDING_TAX_FIELD,
    PREVIOUS_EMPLOYER_TOTAL_WITHHOLDING_TAX_FIELD
];

export const PREVIOUS_EMPLOYER_FIELDS = [
    ...PREVIOUS_EMPLOYER_TAXABLE_INCOME_FIELDS,
    ...PREVIOUS_EMPLOYER_NON_TAXABLE_INCOME_FIELDS,
    ...PREVIOUS_EMPLOYER_EMPLOYEE_CONTRIBUTION_FIELDS,
    ...PREVIOUS_EMPLOYER_EMPLOYER_CONTRIBUTION_FIELDS,
    PREVIOUS_EMPLOYER_TOTAL_WITHHOLDING_TAX_FIELD
];

export const FORM_FIELDS = {
    CURRENT_EMPLOYER_SECTIONS: [
        {
            title: 'Non-Taxable Income',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_basic_salary_or_statutory_minimum_wage',
                        label: 'Basic Salary/Statutory Minimum Wage (MWE)',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_holiday_pay_mwe',
                        label: 'Holiday Pay (MWE)',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_overtime_pay_mwe',
                        label: 'Overtime Pay (MWE)',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_nt_differential_mwe',
                        label: 'Night Shift Differential (MWE)',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_de_minimis_benefits',
                        label: 'De Minimis Benefits',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_non_taxable_salaries_and_other_forms_of_compensation',
                        label: 'Salaries & Other Forms of Compensation',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    }
                ]
            ]
        },
        {
            title: 'Taxable Income',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_basic_salary',
                        label: 'Basic Salary',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_taxable_ot_pay',
                        label: 'Overtime Pay',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_taxable_commission',
                        label: 'Commission',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_others_regular',
                        label: 'Others Regular',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_others_supplementary',
                        label: 'Others Supplementary',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    }
                ]
            ]
        },
        {
            title: 'Others',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_13th_month_pay_and_other_benefits',
                        label: '13th Month Pay and Other Benefits',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_premium_paid_on_health_and_hospital_insurance',
                        label: 'Premium Paid on Health & Hospital Insurance',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_tax_withheld',
                        label: 'Tax Withheld',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_employee_sss_contribution',
                        label: 'Employee SSS Contribution',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_employee_philhealth_contribution',
                        label: 'Employee PhilHealth Contribution',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'current_employee_pagibig_contribution',
                        label: 'Employee Pag-IBIG Contribution',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    }
                ]
            ]
        }
    ],
    PREVIOUS_EMPLOYER_SECTIONS: [
        {
            title: 'Previous Employer Information',
            rows: [
                [
                    {
                        field_type: 'date-picker',
                        class_name: 'col-xs-4 date',
                        id: 'prev_date_from',
                        label: <span><span className="item-number">2A</span> For The Period From</span>,
                        placeholder: 'Select date',
                        validations: {
                            required: false
                        },
                        display_format: DATE_FORMATS.MMDDYYYY
                    },
                    {
                        field_type: 'date-picker',
                        class_name: 'col-xs-4 date',
                        id: 'prev_date_to',
                        label: <span><span className="item-number">2B</span> For The Period To</span>,
                        placeholder: 'Select date',
                        validations: {
                            required: false
                        },
                        display_format: DATE_FORMATS.MMDDYYYY
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-8',
                        id: 'prev_employer_business_name',
                        label: <span><span className="item-number">16</span> Registered Name</span>,
                        placeholder: 'Enter Previous Employer',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_employer_tin',
                        label: <span><span className="item-number">15</span> Tax Identification Number (TIN)</span>,
                        placeholder: 'Enter TIN Number',
                        validations: {
                            required: false
                        },
                        type: 'tin'
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-8',
                        id: 'prev_employer_business_address',
                        label: <span><span className="item-number">17</span> Registered Address</span>,
                        placeholder: 'Enter Address',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_employer_zip_code',
                        label: <span><span className="item-number">17A</span> ZIP Code</span>,
                        placeholder: 'Enter ZIP Code',
                        validations: {
                            required: false
                        }
                    }
                ]
            ]
        },
        {
            title: 'Non-Taxable Income',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_basic_salary_or_statutory_minimum_wage',
                        label: <span><span className="item-number">32</span> Basic Salary/Statutory Minimum Wage (MWE)</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_holiday_pay_mwe',
                        label: <span><span className="item-number">33</span> Holiday Pay (MWE)</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_overtime_pay_mwe',
                        label: <span><span className="item-number">34</span> Overtime Pay (MWE)</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_nt_differential_mwe',
                        label: <span><span className="item-number">35</span> Night Shift Differential (MWE)</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_non_taxable_13th_month_pay_and_other_benefits',
                        label: <span><span className="item-number">37</span> 13th Month Pay and Other Benefits</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_de_minimis_benefits',
                        label: <span><span className="item-number">38</span> De Minimis Benefits</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_sss_gsis_phic_pagibig_contribution',
                        label: <span><span className="item-number">39</span> SSS, GSIS, PHIC &amp; Pag-IBIG Contributions</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_non_taxable_salaries_and_other_forms_of_compensation',
                        label: <span><span className="item-number">40</span> Salaries &amp; Other Forms of Compensation</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_total_non_taxable_compensation_income',
                        label: <span><span className="item-number">41</span> Total Non-Taxable Compensation Income</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    }
                ]
            ]
        },
        {
            title: 'Taxable Income',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_taxable_salaries_and_other_forms_of_compensation',
                        label: 'Salaries & Other Forms of Compensation',
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_taxable_13th_month_pay_and_other_benefits',
                        label: <span><span className="item-number">51</span> 13th Month Pay and Other Benefits</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_total_taxable_compensation_income',
                        label: <span><span className="item-number">55</span> Total Taxable Compensation Income</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    }
                ]
            ]
        },
        {
            title: 'Others',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_gross_compensation_income',
                        label: <span><span className="item-number">21</span> Gross Compensation Income</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_premium_paid_on_health_and_hospital_insurance',
                        label: <span><span className="item-number">27</span> Premium Paid on Health &amp; Hospital Insurance</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'prev_tax_withheld',
                        label: <span><span className="item-number">31</span> Total Amount of Taxes Withheld as Adjusted</span>,
                        placeholder: 'Enter Amount',
                        validations: {
                            required: false
                        }
                    }
                ]
            ]
        }
    ]
};

export const PREVIOUS_EMPLOYER_INFO_FIELDS = [
    'prev_employer_business_name',
    'prev_employer_tin',
    'prev_employer_business_address',
    'prev_employer_zip_code'
];

export const CURRENCY_FIELDS = {
    CURRENT_EMPLOYER: [
        'current_basic_salary_or_statutory_minimum_wage',
        'current_holiday_pay_mwe',
        'current_overtime_pay_mwe',
        'current_nt_differential_mwe',
        'current_de_minimis_benefits',
        'current_non_taxable_salaries_and_other_forms_of_compensation',
        'current_basic_salary',
        'current_taxable_ot_pay',
        'current_taxable_commission',
        'current_others_regular',
        'current_others_supplementary',
        'current_13th_month_pay_and_other_benefits',
        'current_premium_paid_on_health_and_hospital_insurance',
        'current_tax_withheld',
        'current_employee_sss_contribution',
        'current_employee_philhealth_contribution',
        'current_employee_pagibig_contribution'
    ],
    PREVIOUS_EMPLOYER: [
        'prev_basic_salary_or_statutory_minimum_wage',
        'prev_holiday_pay_mwe',
        'prev_overtime_pay_mwe',
        'prev_nt_differential_mwe',
        'prev_non_taxable_13th_month_pay_and_other_benefits',
        'prev_de_minimis_benefits',
        'prev_sss_gsis_phic_pagibig_contribution',
        'prev_non_taxable_salaries_and_other_forms_of_compensation',
        'prev_total_non_taxable_compensation_income',
        'prev_taxable_salaries_and_other_forms_of_compensation',
        'prev_taxable_13th_month_pay_and_other_benefits',
        'prev_total_taxable_compensation_income',
        'prev_gross_compensation_income',
        'prev_premium_paid_on_health_and_hospital_insurance',
        'prev_tax_withheld'
    ]
};
