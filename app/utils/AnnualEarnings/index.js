import {
    stripNonDigit
} from '../functions';

const BRACKET_ONE_UPPER_LIMIT = 250000;
const BRACKET_TWO_UPPER_LIMIT = 400000;
const BRACKET_THREE_UPPER_LIMIT = 800000;
const BRACKET_FOUR_UPPER_LIMIT = 2000000;
const BRACKET_FIVE_UPPER_LIMIT = 8000000;

const BRACKET_ONE_BASE = 0;
const BRACKET_TWO_BASE = 0;
const BRACKET_THREE_BASE = 30000;
const BRACKET_FOUR_BASE = 130000;
const BRACKET_FIVE_BASE = 490000;
const BRACKET_SIX_BASE = 2410000;

const BRACKET_TWO_COEFFICIENT = 0.20;
const BRACKET_THREE_COEFFICIENT = 0.25;
const BRACKET_FOUR_COEFFICIENT = 0.30;
const BRACKET_FIVE_COEFFICIENT = 0.32;
const BRACKET_SIX_COEFFICIENT = 0.35;

/**
 * Class for annual earnings computations
 */
export default class AnnualEarningsService {
    calculateNetIncome( data, fields, employer = '' ) {
        let sum = 0;

        sum = this.calculateIncome( data, fields, employer )
            - this.calculateSummaryWithholdingTax( data, employer )
            - this.readTotalEmployeeContributions( data, employer );

        return sum;
    }

    calculateIncome = ( data, fields, employer = '' ) => {
        let sum = 0;
        fields.forEach( ( field ) => {
            if ( data[ field ] && field.includes( employer ) ) {
                sum += parseFloat( stripNonDigit( data[ field ]) );
            }
        });
        return sum;
    }

    areContributionFieldsFilled = ( data, employer ) => {
        const fields = [
            `${employer}_sss_employee_share`,
            `${employer}_philhealth_employee_share`,
            `${employer}_pagibig_employee_share`
        ];

        return !data[ `${employer}_total_employee_contributions` ] && this.isAnyValueNonNull( data, fields );
    }

    isAnyValueNonNull( data, fields ) {
        return fields.some( ( field ) => (
            !!data[ field ]
        ) );
    }

    calculateSummaryWithholdingTax( data, employer = '' ) {
        const currentEmployerWithholdingTax = employer === '' || employer === 'current' ? parseFloat( stripNonDigit( data.current_employer_withholding_tax ) ) || 0 : 0;
        const previousEmployerWithholdingTax = employer === '' || employer === 'prev' ? parseFloat( stripNonDigit( data.prev_employer_withholding_tax ) ) || 0 : 0;

        return currentEmployerWithholdingTax || previousEmployerWithholdingTax ? currentEmployerWithholdingTax + previousEmployerWithholdingTax : 0;
    }

    getTotalEmployeeContributions = ( data, employer ) => {
        const sssEmployeeContribution = parseFloat( stripNonDigit( data[ `${employer}_sss_employee_share` ]) ) || 0;
        const pagibigEmployeeContribution = parseFloat( stripNonDigit( data[ `${employer}_pagibig_employee_share` ]) ) || 0;
        const philhealthEmployeeContribution = parseFloat( stripNonDigit( data[ `${employer}_philhealth_employee_share` ]) ) || 0;

        const amount = sssEmployeeContribution + pagibigEmployeeContribution + philhealthEmployeeContribution || 0;

        return amount;
    }

    readTotalEmployeeContributions( data, employer = '' ) {
        const currentTotalEmployeeContributions = this.areContributionFieldsFilled( data, 'current' )
            ? this.getTotalEmployeeContributions( data, 'current' )
            : parseFloat( stripNonDigit( data.current_total_employee_contributions ) ) || 0;

        const previousTotalEmployeeContributions = this.areContributionFieldsFilled( data, 'prev' )
            ? this.getTotalEmployeeContributions( data, 'prev' )
            : parseFloat( stripNonDigit( data.prev_total_employee_contributions ) ) || 0;

        if ( currentTotalEmployeeContributions || previousTotalEmployeeContributions ) {
            if ( employer === 'current' ) return currentTotalEmployeeContributions || 0;
            if ( employer === 'prev' ) return previousTotalEmployeeContributions || 0;

            return currentTotalEmployeeContributions + previousTotalEmployeeContributions || 0;
        }

        return 0;
    }

    getSummaryContribution = ( data, type ) => {
        const currentContribution = this.getTotalContribution( data, 'current', type );
        const previousContribution = this.getTotalContribution( data, 'prev', type );

        return currentContribution || previousContribution ? currentContribution + previousContribution : 0;
    }

    getTotalContribution = ( data, employer, type ) => {
        const employeeContribution = parseFloat( stripNonDigit( data[ `${employer}_${type}_employee_share` ]) ) || 0;
        const employerContribution = parseFloat( stripNonDigit( data[ `${employer}_${type}_employer_share` ]) ) || 0;

        return employeeContribution || employerContribution ? employeeContribution + employerContribution : 0;
    }

    getTotalEmployeeContributions = ( data, employer ) => {
        const sssEmployeeContribution = parseFloat( stripNonDigit( data[ `${employer}_sss_employee_share` ]) ) || 0;
        const pagibigEmployeeContribution = parseFloat( stripNonDigit( data[ `${employer}_pagibig_employee_share` ]) ) || 0;
        const philhealthEmployeeContribution = parseFloat( stripNonDigit( data[ `${employer}_philhealth_employee_share` ]) ) || 0;

        return ( sssEmployeeContribution + pagibigEmployeeContribution + philhealthEmployeeContribution ) || 0;
    }

    calculateWithholdingTax = ( data, fields, employer ) => {
        const taxableIncome = this.calculateIncome( data, fields, employer );
        let sum = BRACKET_ONE_BASE;

        if ( taxableIncome > BRACKET_ONE_UPPER_LIMIT && taxableIncome <= BRACKET_TWO_UPPER_LIMIT ) {
            sum += BRACKET_TWO_BASE + ( taxableIncome - BRACKET_ONE_UPPER_LIMIT ) * BRACKET_TWO_COEFFICIENT;
        } else if ( taxableIncome > BRACKET_TWO_UPPER_LIMIT && taxableIncome <= BRACKET_THREE_UPPER_LIMIT ) {
            sum += BRACKET_THREE_BASE + ( taxableIncome - BRACKET_TWO_UPPER_LIMIT ) * BRACKET_THREE_COEFFICIENT;
        } else if ( taxableIncome > BRACKET_THREE_UPPER_LIMIT && taxableIncome <= BRACKET_FOUR_UPPER_LIMIT ) {
            sum += BRACKET_FOUR_BASE + ( taxableIncome - BRACKET_THREE_UPPER_LIMIT ) * BRACKET_FOUR_COEFFICIENT;
        } else if ( taxableIncome > BRACKET_FOUR_UPPER_LIMIT && taxableIncome <= BRACKET_FIVE_UPPER_LIMIT ) {
            sum += BRACKET_FIVE_BASE + ( taxableIncome - BRACKET_FOUR_UPPER_LIMIT ) * BRACKET_FIVE_COEFFICIENT;
        } else if ( taxableIncome > BRACKET_FIVE_UPPER_LIMIT ) {
            sum += BRACKET_SIX_BASE + ( taxableIncome - BRACKET_FIVE_UPPER_LIMIT ) * BRACKET_SIX_COEFFICIENT;
        }

        return sum;
    }

}

export const annualEarningsService = new AnnualEarningsService();
