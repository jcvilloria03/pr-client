import { auth } from 'utils/AuthService';
import { Fetch } from 'utils/request';
import { PRODUCTS, FREE_TRIAL_SUBSCRIPTIONS_SUBHEADER_ITEMS, SUBSCRIPTIONS_SUBHEADER_ITEMS } from './constants';

/**
 * Subscription helper
 */
export default class SubscriptionService {
    isSubscribedToPayroll( products ) {
        let productSeats = products;

        if ( !productSeats ) {
            productSeats = auth.getProducts();
        }

        return this.isSubscribedToPayrollOnly( productSeats ) || this.isSubscribedToBothPayrollAndTA( productSeats );
    }

    isSubscribedToTA( products ) {
        let productSeats = products;

        if ( !productSeats ) {
            productSeats = auth.getProducts();
        }

        return this.isSubscribedToTAOnly( productSeats ) || this.isSubscribedToBothPayrollAndTA( productSeats );
    }

    isSubscribedToPayrollOnly( products ) {
        let productSeats = products;

        if ( !productSeats ) {
            productSeats = auth.getProducts();
        }

        return productSeats && ( productSeats.includes( PRODUCTS.PAYROLL ) ) && !this.isSubscribedToBothPayrollAndTA( productSeats );
    }

    isSubscribedToTAOnly( products ) {
        let productSeats = products;

        if ( !productSeats ) {
            productSeats = auth.getProducts();
        }

        return productSeats && ( productSeats.includes( PRODUCTS.TIME_AND_ATTENDANCE ) ) && !this.isSubscribedToBothPayrollAndTA( productSeats );
    }

    isSubscribedToBothPayrollAndTA( products ) {
        let productSeats = products;

        if ( !productSeats ) {
            productSeats = auth.getProducts();
        }

        return productSeats && productSeats.includes( PRODUCTS.BOTH );
    }

    isSubscribedToAnyProduct( products ) {
        let productSeats = products;

        if ( !productSeats ) {
            productSeats = auth.getProducts();
        }

        return this.isSubscribedToPayroll( productSeats ) || this.isSubscribedToTA( productSeats );
    }

    isTrial() {
        return auth.isTrial();
    }

    isExpired() {
        return auth.isExpired();
    }

    updateSubscription( subscription ) {
        const profile = auth.getUser();
        if ( profile ) {
            auth.setUserData( Object.assign({}, profile, { subscription }) );
        }
    }

    getTabs() {
        let items = this.isTrial()
            ? FREE_TRIAL_SUBSCRIPTIONS_SUBHEADER_ITEMS
            : SUBSCRIPTIONS_SUBHEADER_ITEMS;

        if ( !auth.isOwner() ) {
            items = items.filter( ( item ) => item.title !== 'Account Deletion' );
        }

        return items;
    }

    /**
     * Fetches Subscription ID
     * @returns {Number} Subscription ID
     */
    async getSubscriptionId() {
        const profile = auth.getUser();
        if ( profile && profile.subscription ) {
            return profile.subscription.id;
        }

        try {
            const account = await Fetch( '/account', { method: 'GET' });
            const subscription = account.subscriptions[ 0 ];
            profile.subscription = subscription;
            auth.setUserData( profile );

            return subscription.id;
        } catch ( error ) {
            throw error;
        }
    }

    async createPayPalOrder( invoiceId ) {
        const { data } = await Fetch( `/subscriptions/invoices/${invoiceId}/payments/paypal/orders`, { method: 'POST' });

        return data;
    }

    async capturePayPalOrder( invoiceId, orderId ) {
        const { data } = await Fetch( `/subscriptions/invoices/${invoiceId}/payments/paypal/capture/${orderId}`, { method: 'POST' });

        return data;
    }
}

export const subscriptionService = new SubscriptionService();
