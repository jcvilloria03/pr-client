import auth0 from 'auth0-js';
import 'whatwg-fetch';
import axios from 'axios';
import userflow from 'userflow.js';

import { EventEmitter } from 'events';
import { sockets } from 'utils/Sockets';
import { browserHistory } from './BrowserHistory';
import { isTokenExpired } from './jwtHelper';
import { Fetch, getUserPermissions } from './request';
import { LOGIN_PAGE_SNACKBAR_MESSAGE } from '../constants';

const AUTH_DOMAIN = process.env.AUTH_DOMAIN;
const AUTH_CLIENT_ID = process.env.AUTH_CLIENT_ID;
const HOST = window.location.origin || `${window.location.protocol}//${window.location.host}`;

/**
 * Authentication Service
 */
export default class AuthService extends EventEmitter {
    /**
     * Class constructor
     */
    constructor() {
        super();
        // Configure Auth0
        this.auth0 = new auth0.WebAuth({
            clientID: AUTH_CLIENT_ID,
            domain: AUTH_DOMAIN,
            responseType: 'token access_token',
            redirectUri: `${HOST}/login`
        });

        this.login = this.login.bind( this );
        this.signup = this.signup.bind( this );
        this.resendVerification = this.resendVerification.bind( this );
    }

    /**
     * Logs the user in.
     *
     * @param username
     * @param password
     */
    async login( username, password ) {
        this.clearStoredCredentials();

        try {
            const { data: { data }} = await axios({
                url: '/auth/login',
                baseURL: process.env.AUTHN_API_BASE_URI,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    email: username,
                    password
                }
            });

            if ( data && data.for_reset_password ) {
                localStorage.setItem( 'authn-forgot-password-email', username );
                return null;
            }

            const { user, token } = data;

            this.setToken( token.access_token );

            const companyId = user.last_active_company_id ? user.last_active_company_id : user.company_id;

            const userData = Object.assign( user, {
                email_verified: Boolean( user.email_verified_at ),
                user_id: user.user_id,
                employee_id: user.employee_id,
                company_id: user.company_id,
                last_active_company_id: user.company_id,
                account_id: user.account_id,
                authn_id: user.id,
                for_change_password: user.for_change_password
            });

            this.setUserData( userData );

            // Set as authz default header
            axios.defaults.headers.common[ 'X-Authz-Company-Id' ] = companyId;

            await this.getUserDetails( user.user_id, user.company_id );

            await getUserPermissions();
        } catch ( loginError ) {
            const { response } = loginError;

            if ( response.status === 309 ) {
                if ( response.data.action_code === 1 ) {
                    window.location.replace( '/ess/payslips' );
                } else if ( response.data.action_code === 2 ) {
                    const message = {
                        title: 'Error',
                        message: response.data.message || '',
                        type: 'error'
                    };

                    localStorage.clear();
                    localStorage.setItem( LOGIN_PAGE_SNACKBAR_MESSAGE, JSON.stringify( message ) );
                    browserHistory.replace( '/login', true );
                }
            }

            let message = response.data.error_description || response.data.message;
            if ( response && response.status >= 500 ) {
                message = 'The system encountered a problem while processing your request. Please try again after 5 minutes. If the issue still persists, please contact support@salarium.com.';
            }
            this.clearStoredCredentials();

            return { title: response.statusText, message };
        }

        return null;
    }

    /**
     * Check and get email for migration
     * @return string
     */
    isForAuthnMigration() {
        return localStorage.getItem( 'authn-forgot-password-email' );
    }

    async getUserDetails( userId, companyId ) {
        const { data: { data }} = await axios({
            url: `/user/${userId}/initial-data`,
            method: 'GET',
            headers: {
                'X-Authz-Entities': 'root.admin',
                'X-Authz-Company-Id': companyId,
                Authorization: `Bearer ${localStorage.getItem( 'access_token' )}`
            }
        });

        const userData = JSON.parse( localStorage.getItem( 'user' ) );

        const user = Object.assign( userData, {
            ...data,
            is_employee: Boolean( data.employee )
        });
        localStorage.setItem( 'user', JSON.stringify( user ) );

        return true;
    }

    /**
     * Sign up
     * @param {Object} formData - sign up details
     * @returns {Promise}
     */
    signup( formData ) {
        const options = {
            method: 'POST',
            data: formData
        };

        return Fetch( '/account/sign_up', options )
            .then( ({ data }) => {
                if ( data ) {
                    const {
                        user: {
                            access_token
                        }
                    } = data;

                    this.setToken( access_token );
                    browserHistory.replace( '/verify', true );
                }
            })
            .catch( ( error ) => {
                throw error;
            });
    }

    /**
     * this resends a verification email to the user id provided
     */
    resendVerification( callback ) {
        Fetch( '/auth/user/verify_resend', { method: 'POST', headers: { Authorization: `Bearer ${localStorage.getItem( 'access_token' )}` }})
            .then( ({ data }) => {
                if ( data ) {
                    callback && callback( false );
                }
            })
            .catch( () => {
                callback && callback( true );
            });
    }

    async fetchSalpaySetting() {
        if ( !this.getUser() ) {
            return false;
        }

        const userId = this.getUser().id;
        const companies = this.getUser().companies;

        try {
            const companyIds = companies.map( ( company ) => company.id );
            const paramCompanyIds = companyIds.join();
            const options = {
                method: 'GET',
                authzModules: 'control_panel.salpay_integration',
                authzCompanyId: this.getUser().last_active_company_id ? this.getUser().last_active_company_id : this.getUser().company_id
            };

            const salpaySettings = await Fetch( `/salpay/companies/salpay-settings/${userId}?company_ids=${paramCompanyIds}`, options );

            let userLocal = JSON.parse( localStorage.getItem( 'user' ) );
            userLocal = Object.assign({}, userLocal, { salpay_settings: salpaySettings.data });
            localStorage.setItem( 'user', JSON.stringify( userLocal ) );
        } catch ( error ) {
            // Do Nothing
        }

        return true;
    }

    /**
     * Change Password
     * @param {String} currentPassword
     * @param {String} newPassword
     * @param {String} newPassConfirm
     * @returns {Promise}
     */
    changePassword( currentPassword, newPassword, newPassConfirm ) {
        const user = JSON.parse( localStorage.getItem( 'user' ) );
        return axios({
            url: '/auth/change-password',
            baseURL: process.env.AUTHN_API_BASE_URI,
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem( 'access_token' )}`
            },
            data: {
                email: user && user.email,
                old_password: currentPassword,
                new_password: newPassword,
                password_confirmation: newPassConfirm
            }
        });
    }

    /**
     * Request Reset Password
     * @param {String} email - Email to request reset password
     * @returns {Promise}
     */
    requestResetPassword( email ) {
        return axios({
            url: '/auth/forgot-password',
            baseURL: process.env.AUTHN_API_BASE_URI,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                email
            }
        });
    }

    /**
     * Reset Password
     * @param {String} token          - Token from redirect URL
     * @param {String} newPassword    - Password
     * @param {String} newPassConfirm - Confirm password
     */
    resetPassword({ newPassword, newPassConfirm }) {
        const query = localStorage.getItem( 'reset_password_query' );
        return axios({
            url: `/auth/reset-password${query}`,
            baseURL: process.env.AUTHN_API_BASE_URI,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                password: newPassword,
                password_confirmation: newPassConfirm
            }
        });
    }

    /**
     * This method looks for an access token in the URL hash when the user is redirected back to the application.
     * If token found, it will be saved into local storage and the user is redirected to the dashboard route.
     */
    parseHash( hash ) {
        this.auth0.parseHash({ hash }, ( err, authResult ) => {
            if ( authResult && authResult.accessToken ) {
                this.setToken( authResult.accessToken );
            }
        });
    }

    /**
     * Checks if there is a saved token and it's still valid
     * @returns {boolean}
     */
    loggedIn() {
        const token = this.getToken();
        const user = this.getUser();

        return !!user && !!token && !isTokenExpired( token );
    }

    /**
     * checks if the user has verified email after login
     */
    emailVerified() {
        const user = this.getUser();
        return user && user.email_verified_at;
    }

    /**
     * Saves user access token and ID token into local storage
     * @param accessToken
     */
    setToken( accessToken ) {
        localStorage.setItem( 'access_token', accessToken );
    }

    /**
     * Saves profile data to localStorage and Triggers profile_updated event to update the UI
     * @param profile
     */
    setProfile( profile ) {
        localStorage.setItem( 'profile', JSON.stringify( profile ) );
        this.emit( 'profile_updated', profile );
    }

    /**
     * Retrieves the profile data from localStorage
     */
    getProfile() {
        const profile = localStorage.getItem( 'profile' );

        return profile ? JSON.parse( localStorage.profile ) : {};
    }

    /**
     * Retrieves the user token from localStorage
     */
    getToken() {
        return localStorage.getItem( 'access_token' );
    }

    /**
     * Retrieves the user's nickname
     */
    getNickname() {
        return this.getUser().first_name || null;
    }

    /**
     * Clear user token and profile data from localStorage
     */
    logout() {
        axios({
            url: '/auth/logout',
            baseURL: process.env.AUTHN_API_BASE_URI,
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem( 'access_token' )}`
            }
        });

        localStorage.clear();
        userflow.reset();

        sockets.disconnect();
        browserHistory.push( '/login', true );
    }

    /**
     * Check if logged in user is non admin.
     * @return {Boolean}
     */
    isNonAdmin() {
        const user = this.getUser();

        return user && user.user_type === 'employee';
    }

    isAdmin() {
        const user = this.getUser();
        return user && user.user_type !== 'employee';
    }

    /**
     * Check if logged in user is an employee.
     * @return {Boolean}
     */
    isEmployee() {
        const user = this.getUser();

        return ( user && user.employee && user.employee.id );
    }

    /**
     * Check if logged in user subscription is expired.
     * @return {Boolean}
     */
    isExpired() {
        const user = this.getUser();
        return user && user.subscription && user.subscription.is_expired;
    }

    /**
     * Check if logged in user subscription is trial.
     * @return {Boolean}
     */
    isTrial() {
        const user = this.getUser();
        return user && user.subscription && user.subscription.is_trial;
    }

    isFreshAccount() {
        const user = this.getUser();

        return user && user.for_change_password;
    }

    /**
     * Get user subscription products
     */
    getProducts() {
        const user = this.getUser();
        return user && user.product_seats && user.product_seats.map( ( productSeat ) => productSeat.name );
    }

    /**
     * Get user permissions
     */
    getAuthzPermissions() {
        return JSON.parse( localStorage.getItem( 'authz_permissions' ) ) || [];
    }

    /**
     * Get user data
     * @return {Object}
     */
    getUser() {
        return JSON.parse( localStorage.getItem( 'user' ) );
    }

    /**
     * Set user data to local storage.
     * @param {Object} user
     */
    setUserData( user ) {
        localStorage.setItem( 'user', JSON.stringify( user ) );
    }

    /**
     * Set salpay integration status
     */
    setSalpayIntegrationStatus( status ) {
        let user = JSON.parse( localStorage.getItem( 'user' ) );

        user = Object.assign({}, user, { salpay_integration_status: status });
        localStorage.setItem( 'user', JSON.stringify( user ) );
    }

    /**
     * Get salpay integration status
     */
    getSalpayIntegrationStatus() {
        const user = JSON.parse( localStorage.getItem( 'user' ) );

        if ( 'salpay_integration_status' in user ) {
            return user.salpay_integration_status;
        }

        return false;
    }

    isOwner() {
        const user = this.getUser();
        return user && user.user_type === 'owner';
    }

    setAccountDeleteRequestData( deleteRequest = null ) {
        const profile = this.getUser();
        if ( profile ) {
            this.setUserData( Object.assign({}, profile, { account_deletion_request: deleteRequest }) );
        }
    }

    getAccountDeleteRequestData() {
        const profile = JSON.parse( localStorage.getItem( 'user' ) );

        if ( profile && 'account_deletion_request' in profile ) {
            return profile.account_deletion_request;
        }

        return null;
    }

    /**
     * Remove the query string from path and store in local storage
     */
    storeAndRemoveResetPasswordQueryString( nextState, replace ) {
        if ( this.loggedIn() ) {
            replace( '/' );
        }

        if ( nextState.location.search ) {
            localStorage.setItem( 'reset_password_query', nextState.location.search );
            replace({ pathname: '/reset-password', search: '', query: {}});
        }
    }

    /**
     * Clears stored credentials
     */
    clearStoredCredentials() {
        localStorage.removeItem( 'access_token' );
        localStorage.removeItem( 'profile' );
        localStorage.removeItem( 'user' );
        localStorage.removeItem( 'authz_permissions' );
        localStorage.removeItem( 'companies' );
    }
}

export const auth = new AuthService();
