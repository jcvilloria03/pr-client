import React from 'react';
import Input from 'components/Input';
import DatePicker from 'components/DatePicker';
import Select from 'components/Select';
import Switch from 'components/Switch';
import Switch2 from 'components/Switch2';
import GovernmentNumbersInput from 'components/GovernmentNumbersInput';
import RadioGroup from 'components/RadioGroup';
import Radio from 'components/Radio';

/**
 * Form utility
 */
function renderField( config ) {
    const {
        field_type: fieldType,
        id,
        label,
        placeholder,
        validations,
        display_format: displayFormat,
        options,
        ...rest
    } = config;

    switch ( fieldType ) {
        case 'select':
            return (
                <Select
                    id={ `${id}_select` }
                    label={ label }
                    placeholder={ placeholder }
                    required={ validations.required }
                    data={ options }
                    { ...rest }
                />
            );
        case 'switch':
        case 'switch2': {
            const SwitchComponent = fieldType === 'switch' ? Switch : Switch2;

            return (
                <div>
                    { label && (
                        <div className="switch-label">{ label }</div>
                    ) }
                    <SwitchComponent
                        id={ `${id}_switch` }
                        { ...rest }
                    />
                    { !label && placeholder && (
                        <span>{ placeholder }</span>
                    ) }
                </div>
            );
        }
        case 'date-picker':
            return (
                <DatePicker
                    id={ `${id}_datepicker` }
                    placeholder={ placeholder }
                    label={ label }
                    dayFormat={ displayFormat }
                    required={ validations.required }
                    { ...rest }
                />
            );
        case 'radio-group':
            return (
                <div>
                    <label htmlFor={ `${id}_radio_group` }>
                        { label }
                    </label>
                    <RadioGroup
                        id={ `${id}_radio_group` }
                        { ...rest }
                    >
                        { options.map( ( option ) => (
                            <Radio key={ option.value } value={ option.value }>
                                { option.label }
                            </Radio>
                        ) ) }
                    </RadioGroup>
                </div>
            );
        case 'input':
        default:
            if ([ 'tenure_date', 'btn_terminate' ].includes( config.id ) ) {
                return '';
            }
            if ([ 'rdo', 'sss', 'tin', 'hdmf', 'philhealth' ].includes( config.type ) ) {
                return (
                    <GovernmentNumbersInput
                        id={ `${id}_input` }
                        label={ label }
                        placeholder={ placeholder }
                        required={ validations.required }
                        { ...rest }
                    />
                );
            }

            return (
                <Input
                    id={ `${id}_input` }
                    label={ label }
                    type="text"
                    placeholder={ placeholder }
                    required={ validations.required }
                    minNumber={ validations.minNumber }
                    maxNumber={ validations.maxNumber }
                    { ...rest }
                />
            );
    }
}

export { renderField };
