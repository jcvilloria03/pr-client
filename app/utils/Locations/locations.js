import * as countries from "./data/country.json";
import * as states from "./data/state.json";
import * as cities from "./data/city.json";
import _ from 'lodash';

export const getCountries = () =>
    countries.map((country) => ({
        value: country.isoCode,
        label: country.name,
    }));

export const getStates = (countryCode) => {
    if (!countryCode) return [];
    return states
        .filter((value) => value.countryCode === countryCode)
        .sort(compare)
        .map((state) => ({ value: state.isoCode, label: state.name }));
};

export const getCities = (countryCode, stateCode) => {
    let cityList = convertArrayToObject(KEYS, cities);
    if (!stateCode) {
        if (!countryCode) return [];
        const cities = cityList.filter(
            (value) => value.countryCode === countryCode
        );
        cityList = cities
            .sort(compare)
            .map((city) => ({ value: city.name, label: city.name }));

        return _.uniqBy(cityList, 'value');
    }
    cityList = cityList.filter(
            (value) =>
                value.countryCode === countryCode &&
                value.stateCode === stateCode
        )
        .sort(compare)
        .map((city) => ({ value: city.name, label: city.name }));

    return _.uniqBy(cityList, 'value');
};

export const DEFAULT_NO_OPTION = { value: "N/A", label: "N/A" };
export const DEFAULT_COUNTRY_OPTION = { value: "PH", label: "Philippines" };

const KEYS = ["name", "countryCode", "stateCode", "latitude", "longitude"];

const compare = (a, b) => {
    if (a.name < b.name) return -1;
    if (a.name > b.name) return 1;
    return 0;
};

const convertArrayToObject = (keys, arr) => {
    const result = arr.map((subArr) => {
        return Object.fromEntries(
            keys.map((key, index) => [key, subArr[index]])
        );
    });
    return result;
};
