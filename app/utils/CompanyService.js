import { auth } from 'utils/AuthService';
import { AUTHZ } from './constants';
import { Fetch } from './request';

/**
 * Last active company helper.
 */
export default class CompanyService {
    /**
     * Get company id from localStorage
     * @return {Number} Company ID
     */
    getLastActiveCompanyId() {
        const user = JSON.parse( localStorage.getItem( 'user' ) );
        return user && user.last_active_company_id;
    }

    /**
     * Set company id to localStorage
     * @param {Number} id Company ID
     */
    setLastActiveCompanyId( id ) {
        let user = JSON.parse( localStorage.getItem( 'user' ) );

        user = Object.assign({}, user, { last_active_company_id: id });

        localStorage.setItem( 'user', JSON.stringify( user ) );
    }

    /**
     * Return last_active_company object after filtering from localstorage (user.companies)
     * @return {object} Company
     */
    getLastActiveCompany() {
        const lastActiveCompanyId = this.getLastActiveCompanyId();

        if ( !auth.getUser() ) {
            return null;
        }

        const companiesList = auth.getUser().companies;
        const lastActiveCompany = companiesList && companiesList.filter( ( company ) => company.id === lastActiveCompanyId )[ 0 ];
        return lastActiveCompany;
    }

    isDemoCompany() {
        if ( !auth.loggedIn() ) {
            return null;
        }

        const lastActiveCompany = this.getLastActiveCompany();
        return Boolean( lastActiveCompany && lastActiveCompany.isdemo );
    }

    async setToDemoCompany() {
        const user = auth.getUser();

        if ( user && user.companies ) {
            const companies = user.companies;
            const demoCompany = companies.filter( ( company ) => company.isdemo )[ 0 ];
            if ( auth.isOwner() && demoCompany ) {
                this.setLastActiveCompanyId( demoCompany.id );
                const response = await Fetch( '/user/last_active_company', {
                    method: 'PATCH',
                    data: { company_id: demoCompany.id },
                    authzModules: AUTHZ.ROOT_ADMIN,
                    headers: {
                        'X-Authz-Company-Id': demoCompany.id
                    }
                });

                return response.last_active_company_id;
            }
        }

        return null;
    }
}

export const company = new CompanyService();
