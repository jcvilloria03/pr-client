import 'whatwg-fetch';
import axios from 'axios';
import set from 'lodash/set';
import get from 'lodash/get';
import matchRoutes from 'react-router/lib/matchRoutes';
import { browserHistory } from 'utils/BrowserHistory';
import { LOGIN_PAGE_SNACKBAR_MESSAGE, BASE_PATH_NAME, AUTHZ_PERMISSIONS } from '../constants';
import modulesMap from '../modules-map';
import { setShouldResetTimer } from '../components/IdleTimer/actions';

axios.defaults.baseURL = process.env.API_DOMAIN;
axios.defaults.headers.common[ 'X-Requested-With' ] = 'XMLHttpRequest';
axios.defaults.headers.post[ 'Content-Type' ] = 'application/json';

let store;
export const injectStore = ( _store ) => {
    store = _store;
};

axios.interceptors.request.use( ( config ) => {
    // Reset idle timer each time an API request is made
    store.dispatch( setShouldResetTimer( true ) );
    return config;
}, ( error ) =>
     Promise.reject( error ) );

let routes = [];

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON( response ) {
    return response.json();
}

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseDATA( response ) {
    return response.data;
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus( response ) {
    if ( response.status >= 200 && response.status < 300 ) {
        return response;
    }
    const error = new Error( response.statusText );
    error.response = response;
    throw error;
}

/**
 * Get mapping by path and request method
 *
 * @param  {String} path
 * @param  {String} method
 * @param  {String} url
 * @return {Array}
 */
function findModuleMapEntry( path, method, url ) {
    if ( !modulesMap[ path ]) {
        return null;
    }

    const endpoints = modulesMap[ path ][ method.toUpperCase() ];

    if ( !endpoints ) {
        return null;
    }

    return endpoints.find( ({ endpoint }) => url.match( endpoint ) );
}

/**
 *
 * @param {String} url url to be cleaned
 * @return {String} cleaned url
 */
function trimQueryParams( url ) {
    return url.indexOf( '?' ) <= 0
        ? url
        : url.substring( 0, url.indexOf( '?' ) );
}

/**
 * Match route for the current location
 *
 * @return {Promise}
 */
export function getMatchedRoutes() {
    return new Promise( ( resolve, reject ) => {
        matchRoutes( routes, location, ( error, result ) => {
            if ( error ) {
                reject( error );
            }

            resolve( result );
        });
    });
}

/**
 * Get user permissions
 *
 * @return {Object}
 */
export async function getUserPermissions() {
    if ( localStorage.getItem( 'access_token' ) === null ) {
        return [];
    }

    let userPermissions = localStorage.getItem( AUTHZ_PERMISSIONS );

    if ( userPermissions !== null ) {
        userPermissions = JSON.parse( userPermissions );
    }

    if ( userPermissions === null || userPermissions.length === 0 ) {
        const { data: { data }} = await axios.get( '/users/permissions', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem( 'access_token' )}`
            }
        });

        userPermissions = Object.keys( data );

        localStorage.setItem( AUTHZ_PERMISSIONS, JSON.stringify( userPermissions ) );
    }

    return userPermissions;
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export default function request( url, options ) {
    return fetch( url, options )
    .then( checkStatus )
    .then( parseJSON );
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "axios"
 *
 * @return {object}           The response data
 */
export async function Fetch( url, options = {}) {
    const newOptions = {
        // Default method if not specified
        method: 'GET',
        ...options
    };

    const { authzModules, authzCompanyId } = options;

    if ( !( 'headers' in newOptions ) ) {
        newOptions.headers = {};
    }

    if ( localStorage.getItem( 'access_token' ) !== null ) {
        newOptions.headers[ 'Authorization' ] = `Bearer ${localStorage.getItem( 'access_token' )}`; // eslint-disable-line dot-notation
    }

    if ( authzModules === undefined ) {
        const { routes: matchedRoutes } = await getMatchedRoutes();

        let currentRoute = null;

        if ( matchedRoutes.length > 0 ) {
            currentRoute = matchedRoutes[ 0 ];
        }

        if ( currentRoute ) {
            const { path } = currentRoute;
            const { method } = newOptions;

            const entry = findModuleMapEntry( path, method, trimQueryParams( url ) );
            const userPermissions = await getUserPermissions();

            if ( entry ) {
                const filteredModules = entry.modules.filter( ( moduleName ) => userPermissions.includes( moduleName ) );

                newOptions.headers[ 'X-Authz-Entities' ] = filteredModules.join( ', ' );
            }
        }
    } else {
        newOptions.headers[ 'X-Authz-Entities' ] = Array.isArray( authzModules )
            ? authzModules.join( ', ' )
            : authzModules;
    }

    if ( authzCompanyId !== undefined ) {
        newOptions.headers[ 'X-Authz-Company-Id' ] = authzCompanyId;
    }

    return axios( url, newOptions )
    .then( checkStatus )
    .then( parseDATA )
    .catch( ( error ) => {
        const location = browserHistory.getCurrentLocation();
        const { status, data } = error.response;
        const errorMessage = get( data, 'message', data );

        if ( status === 309 ) {
            if ( data.action_code === 1 ) {
                window.location.replace( '/ess/payslips' );
            } else if ( data.action_code === 2 ) {
                const message = {
                    title: 'Error',
                    message: data.message || '',
                    type: 'error'
                };

                localStorage.clear();
                localStorage.setItem( LOGIN_PAGE_SNACKBAR_MESSAGE, JSON.stringify( message ) );
                browserHistory.replace( '/login', true );
            }
        } else if ( status === 418 && !location.pathname.match( new RegExp( '/control-panel/subscriptions', 'gi' ) ) && !location.pathname.match( new RegExp( `${BASE_PATH_NAME}/unauthorized`, 'gi' ) ) ) {
            let profile = JSON.parse( localStorage.getItem( 'user' ) );
            profile = Object.assign({}, profile, { is_expired: true });
            localStorage.setItem( 'user', JSON.stringify( profile ) );

            if ( !location.pathname.match( new RegExp( `${BASE_PATH_NAME}/guides/subscriptions/update`, 'gi' ) ) ) {
                browserHistory.replace( '/control-panel/subscriptions', true );
            }
        } else if ( status >= 500 ) {
            const errorClone = Object.assign({}, error );

            set(
                errorClone,
                'response.data.message',
                'The system encountered a problem while processing your request. Please try again after 5 minutes. If the issue still persists, please contact support@salarium.com.'
            );

            throw errorClone;
        } else if ( status === 401 && errorMessage.includes( 'Unauthenticated.' ) ) {
            const message = {
                title: 'Unauthenticated',
                message: 'Your session has expired or revoked.',
                type: 'error'
            };

            localStorage.clear();
            localStorage.setItem( LOGIN_PAGE_SNACKBAR_MESSAGE, JSON.stringify( message ) );
            window.location.replace( '/login' );
        }

        throw error;
    });
}

/**
 * Set root routes for module mapping
 *
 * @param {Array} options.childRoutes
 */
export function setRootRoutes({ childRoutes }) {
    routes = childRoutes;
}
