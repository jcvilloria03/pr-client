import { auth } from 'utils/AuthService';

/**
 * Generates object for company settings sidebar items
 *
 * @param {Object} subscriptions - Collection of subscriptions to determine if some links will be hidden
 * @returns {Array}
 */
export function getCompanySettingsSidebarLinks({ isSubscribedToPayroll = false, isSubscribedToTA = false }) {
    const isSubscribedToPayrollOnly = isSubscribedToPayroll && !isSubscribedToTA;
    const isSubscribedToTAOnly = !isSubscribedToPayroll && isSubscribedToTA;

    return [
        {
            label: 'Company Structure',
            icon: 'hierarchy',
            link: '/company-settings/company-structure',
            sublinks: [
                {
                    label: 'Company Details',
                    link: '/company-settings/company-structure/company-details',
                    native: true
                },
                {
                    label: 'Locations',
                    link: '/company-settings/company-structure/locations',
                    native: true
                },
                {
                    label: 'Department',
                    link: '/company-settings/company-structure/departments',
                    native: true
                },
                {
                    label: 'Position',
                    link: '/company-settings/company-structure/positions',
                    native: true
                },
                {
                    label: 'Cost Center',
                    link: '/company-settings/company-structure/cost-centers',
                    native: true
                },
                {
                    label: 'Ranks',
                    link: '/company-settings/company-structure/ranks',
                    native: true
                },
                {
                    label: 'Teams',
                    link: '/company-settings/company-structure/teams',
                    native: true
                },
                {
                    label: 'Employment Types',
                    link: '/company-settings/company-structure/employment-types',
                    native: true
                },
                {
                    label: 'Projects',
                    link: '/company-settings/company-structure/projects',
                    hide: isSubscribedToPayrollOnly,
                    native: true
                }
            ]
        },
        {
            label: 'Time and Attendance',
            icon: 'time',
            link: '/company-settings/time-attendance',
            sublinks: [
                {
                    label: 'Day/hour Rates',
                    link: '/company-settings/time-attendance/day-hour-rates',
                    native: true
                },
                {
                    label: 'Maximum Clock-out',
                    link: '/company-settings/time-attendance/maximum-clock-out-rules',
                    hide: isSubscribedToPayrollOnly,
                    native: true
                }
            ]
        },
        {
            label: 'Company Payroll',
            icon: 'dollarSign',
            link: 'company-settings/payroll',
            hide: isSubscribedToTAOnly,
            sublinks: [
                {
                    label: 'Payroll Groups',
                    link: '/company-settings/payroll/payroll-groups',
                    native: true
                },
                {
                    label: 'Bonus Types',
                    link: '/company-settings/payroll/bonus-types',
                    native: true
                },
                {
                    label: 'Commission Types',
                    link: '/company-settings/payroll/commission-types',
                    native: true
                },
                {
                    label: 'Allowance Types',
                    link: '/company-settings/payroll/allowance-types',
                    native: true
                },
                {
                    label: 'Deduction Types',
                    link: '/company-settings/payroll/deduction-types',
                    native: true
                },
                {
                    label: 'Loan Type Settings',
                    link: '/payroll/loan-types',
                    native: true
                },
                {
                    label: 'Payment Methods',
                    link: '/payroll/disbursements',
                    native: true
                }
            ]
        },
        {
            label: 'Leave Settings',
            icon: 'calendar',
            link: '/company-settings/leave-settings',
            hide: isSubscribedToPayrollOnly,
            sublinks: [
                {
                    label: 'Leave Types',
                    link: '/company-settings/leave-settings/leave-types',
                    native: true
                },
                {
                    label: 'Leave Entitlements',
                    link: '/company-settings/leave-settings/leave-entitlement',
                    native: true
                }
            ]
        },
        {
            label: 'Schedule Settings',
            icon: 'calendarO',
            link: '/company-settings/schedule-settings',
            sublinks: [
                {
                    label: 'Default Schedule',
                    link: '/company-settings/schedule-settings/default-schedule',
                    hide: isSubscribedToPayrollOnly,
                    native: true
                },
                {
                    label: 'Tardiness Rules',
                    link: '/company-settings/schedule-settings/tardiness-rules',
                    hide: isSubscribedToPayrollOnly,
                    native: true
                },
                {
                    label: 'Night Shift',
                    link: '/company-settings/schedule-settings/night-shift',
                    hide: isSubscribedToPayrollOnly,
                    native: true
                },
                {
                    label: 'Holidays',
                    link: '/company-settings/schedule-settings/holidays',
                    native: true
                }
            ]
        },
        {
            label: 'Workflow Automation',
            icon: 'cogs',
            link: '/company-settings/workflow-automation',
            hide: isSubscribedToPayrollOnly,
            sublinks: [
                {
                    label: 'Workflows',
                    link: '/company-settings/workflow-automation/workflows',
                    native: true
                }
            ]
        }
    ];
}

/**
 * Generates object for control panel sidebar items
 *
 * @param {Object} [config] - Additional config for sidebar
 * @returns {Array}
 */
export function getControlPanelSidebarLinks( config = {}) {
    const {
        accountViewPermission = false,
        salpayViewPermission = false,
        isSubscribedToPayroll = false
    } = config;

    const expiredSubscription = auth.isExpired();

    return [
        {
            label: 'Subscriptions',
            icon: 'users',
            link: '/control-panel/subscriptions',
            sublinks: [],
            hide: !accountViewPermission,
            native: true
        },
        {
            label: 'Companies',
            icon: 'building',
            link: '/control-panel/companies',
            sublinks: [],
            hide: expiredSubscription,
            native: true
        },
        {
            label: 'Device Management',
            icon: 'deviceManagement',
            link: '/control-panel/device-management/users',
            sublinks: [],
            hide: !accountViewPermission || expiredSubscription,
            native: true
        },
        {
            label: 'Users',
            icon: 'users',
            link: '/control-panel/users/management',
            sublinks: [],
            hide: expiredSubscription,
            native: true
        },
        {
            label: 'SALPay Integration',
            icon: 'dollarSign',
            link: '/control-panel/salpay-integration',
            hide: expiredSubscription || !( salpayViewPermission && isSubscribedToPayroll ),
            sublinks: [],
            native: true
        },
        {
            label: 'Roles',
            icon: 'imageSymbol',
            link: '/control-panel/roles',
            sublinks: [],
            hide: expiredSubscription,
            native: true
        },
        {
            label: 'Audit Trail',
            icon: 'list',
            link: '/control-panel/audit-trail',
            sublinks: [],
            hide: expiredSubscription,
            native: false
        }
    ];
}
