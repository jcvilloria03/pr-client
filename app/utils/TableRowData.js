/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable require-jsdoc */
import React from 'react';

class TableRowData extends React.PureComponent {
    constructor( props ) {
        super( props );

        this.state = {
            item: this.truncateString( this.props.data, 150 ),
            expanded: false
        };
        this.showMore = this.showMore.bind( this );
        this.showLess = this.showLess.bind( this );
    }

    truncateString( str, length ) {
        if ( str.length > length ) {
            return `${str.slice( 0, length )}...`;
        }
        return str;
    }

    showMore() {
        this.setState({ item: this.props.data, expanded: true });
    }

    showLess() {
        this.setState({ item: this.truncateString( this.props.data, 150 ), expanded: false });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div>
                        {this.state.item}
                    </div>
                    { this.state.item.length > 150 &&
                        <div>
                            { this.state.expanded ?
                                <span className="less_sec" onClick={ () => this.showLess() }>Show less</span> :
                                <span className="more_sec" onClick={ () => this.showMore() }>Show more</span>
                            }
                        </div>
                    }
                </div>
            </div>
        );
    }
}

export default TableRowData;
