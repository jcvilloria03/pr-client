import get from 'lodash/get';
import keys from 'lodash/keys';

import { template } from './taggedStringTemplate';

export const REQUEST_TYPE_PLACEHOLDERS = {
    leave_request: 'Leave',
    loan_request: 'Loan',
    overtime_request: 'Overtime',
    shift_change_request: 'Shift Change',
    time_dispute_request: 'Time Correction',
    undertime_request: 'Undertime'
};

export const ANNOUNCEMENT_NOTIFICATIONS = {
    REPLY_SENT: 'announcement_reply_sent',
    PUBLISHED: 'announcement_published'
};

/**
 * Get request name
 * @param {Object} activity
 * @return {String}
 */
function getRequestName( activity ) {
    const requestType = get( activity, 'params.request_type' );

    if ( requestType === 'leave_request' ) {
        return get( activity, 'params.leave_type.name' );
    }

    return REQUEST_TYPE_PLACEHOLDERS[ requestType ];
}

/**
 * Dinamicaly parses routes objects.
 */
function parseDefaultRoutes() {
    const routes = {};

    keys( REQUEST_TYPE_PLACEHOLDERS ).forEach( ( type ) => {
        routes[ type ] = {
            name: type === 'time_dispute_request' ? 'time-correction-request' : `${type.replace( /_/g, '-' )}`,
            params: {
                requestId: 'params.request_id'
            }
        };
    });

    return routes;
}

export default {
    request_pending_approval: {
        own: {
            messageTemplate: template`You filed a ${0} request.`,
            templateTags: [getRequestName],
            defaultValues: ['request'],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0} filed a ${1} request.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'request' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        }
    },

    message_sent: {
        own: {
            messageTemplate: template`${0} sent a message concerning your ${1} request.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Approver', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0} sent a message concerning ${1}s ${2} request.`,
            templateTags: [ 'params.employee_name', 'params.owner_name', getRequestName ],
            defaultValues: [ 'Approver', 'employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        }
    },

    request_approval_rejected: {
        own: {
            messageTemplate: template`${0} declined your ${1} request.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Approver', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0} decilined ${1}s ${2} request.`,
            templateTags: [ 'params.employee_name', 'params.owner_name', getRequestName ],
            defaultValues: [ 'Approver', 'employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        }
    },

    request_approval_approved: {
        own: {
            messageTemplate: template`${0} approved your ${1} request.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Approver', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0} approved ${1}s ${2} request.`,
            templateTags: [ 'params.employee_name', 'params.owner_name', getRequestName ],
            defaultValues: [ 'Approver', 'employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        }
    },

    request_approved: {
        own: {
            messageTemplate: template`Your ${0} request has been approved.`,
            templateTags: [getRequestName],
            defaultValues: ['filed'],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0}s ${1} request has been approved.`,
            templateTags: [ 'params.owner_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        }
    },

    request_rejected: {
        own: {
            messageTemplate: template`Your ${0} request has been declined.`,
            templateTags: [getRequestName],
            defaultValues: ['filed'],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0}s ${1} request has been declined.`,
            templateTags: [ 'params.owner_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        }
    },

    request_auto_rejected: {
        own: {
            messageTemplate: template`Your ${0} has been auto-rejected due to shift change.`,
            templateTags: [getRequestName],
            defaultValues: ['request'],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0}'s ${1} request has been auto-rejected.`,
            templateTags: [ 'params.owner_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },

    request_cancelled: {
        own: {
            messageTemplate: template`You cancelled your ${0} request`,
            templateTags: [getRequestName],
            defaultValues: ['filed'],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0} cancelled their ${1} request`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },
    request_created_and_approved: {
        own: {
            messageTemplate: template`${0} has filed a ${1} on your behalf. No further action is required.
                If you did not intend to take this leave please contact ${0} directly.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`${0} has filed a ${1} on your behalf. No further action is required.
                If you did not intend to take this leave please contact ${0} directly.`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'filed' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },
    request_updated: {
        own: {
            messageTemplate: template`${0} updated your ${1}`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'updated' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes()
        },
        other: {
            messageTemplate: template`You updated ${1}`,
            templateTags: [ 'params.employee_name', getRequestName ],
            defaultValues: [ 'Employee', 'updated' ],
            link: true,
            multipleRoutes: true,
            routeTypeParam: 'params.request_type',
            routes: parseDefaultRoutes( true )
        }
    },
    'subscriptions.trials.will_expire_soon': {
        own: {
            messageTemplate: 'Subscription trial will expire in five days',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        },
        other: {
            messageTemplate: 'Subscription trial will expire in five days',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        }
    },
    'subscriptions.trials.will_expire_24hrs': {
        own: {
            messageTemplate: 'Subscription trial will expire in 24 hours (last day of trial)',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        },
        other: {
            messageTemplate: 'Subscription trial will expire in 24 hours (last day of trial)',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        }
    },
    'subscriptions.trials.expired': {
        own: {
            messageTemplate: 'Subscription trial has expired',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        },
        other: {
            messageTemplate: 'Subscription trial has expired',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        }
    },
    'subscriptions.renewal_ahead': {
        own: {
            messageTemplate: 'Subscription renewal ahead',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        },
        other: {
            messageTemplate: 'Subscription renewal ahead',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        }
    },
    'invoices.unpaid_invoice': {
        own: {
            messageTemplate: 'Subscription license due for payment',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        },
        other: {
            messageTemplate: 'Subscription license due for payment',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        }
    },
    'invoices.due_date_changed': {
        other: {
            messageTemplate: 'Your invoice due date has been updated',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions/invoices'
                }
            }
        }
    },
    'subscriptions.suspension_reminder': {
        own: {
            messageTemplate: 'Account suspension reminder before invoice due date',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        },
        other: {
            messageTemplate: 'Account suspension reminder before invoice due date',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        }
    },
    'subscriptions.suspension_notification': {
        own: {
            messageTemplate: 'Account suspension notification when unpaid after invoice due date',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        },
        other: {
            messageTemplate: 'Account suspension notification when unpaid after invoice due date',
            templateTags: [],
            defaultValues: [],
            link: true,
            multipleRoutes: false,
            isSubscription: true,
            routes: {
                subscription: {
                    name: '/control-panel/subscriptions'
                }
            }
        }
    },
    'payslips.new': {
        data: {
            messageTemplate: template`Your payslip for ${0} is ready for viewing via ESS page.`,
            templateTags: ['params.payrollDate'],
            defaultValues: ['the previous payroll'],
            link: false,
            multipleRoutes: false,
            routes: parseDefaultRoutes( false )
        }
    }
};
