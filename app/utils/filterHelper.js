import map from 'lodash/map';
/**
 * Filter Helper
 */
export default class FilterHelper {
    filter( data, filters, type ) {
        const typeFilters = this.getTypeFilters( filters, type );
        const allOtherFilters = this.getAllOtherFilters( filters, type );

        return data.reduce( ( acc, val ) => {
            if ( this.isNotType( typeFilters, val.type_id ) ) {
                return acc;
            }

            if ( !this.passesAnyOtherFilter( allOtherFilters, val ) ) {
                return acc;
            }

            acc.push( val );

            return acc;
        }, []);
    }

    /**
     * Maps parsed data by param ( 'id' by default ).
     * @param {Array} dataArray
     * @param {String} param
     * @returns {Array}
     */
    mapDataByParam( dataArray, param = 'value' ) {
        return map( dataArray, ( item ) => item[ param ]);
    }

    /**
     * Maps filterDataIds.
     * @param {Object} filterData
     */
    mapLeavesFilterDataIds( filterData ) {
        return {
            leave_types_ids: this.mapDataByParam( filterData.leaveTypes ),
            position_ids: this.mapDataByParam( filterData.positions ),
            departments_ids: this.mapDataByParam( filterData.departments ),
            locations_ids: this.mapDataByParam( filterData.locations ),
            employees_ids: this.mapDataByParam( filterData.employees ),
            request_types_ids: this.mapDataByParam( filterData.requestTypes ),
            workflows_ids: this.mapDataByParam( filterData.workflows )
        };
    }

    getTypeFilters( filters, type ) {
        return filters.filter( ( filter ) => filter.type === type );
    }

    getAllOtherFilters( filters, type ) {
        return filters.filter( ( filter ) => filter.type !== type );
    }

    isNotType( typeFilters, typeId ) {
        return typeFilters && typeFilters.length && !typeFilters.find( ( filterType ) => filterType.value === typeId );
    }

    passesAnyOtherFilter( otherFilters, data ) {
        let hasPassed = !otherFilters.length;

        otherFilters.forEach( ( filter ) => {
            if ( data[ `employee_${filter.type}_id` ] && data[ `employee_${filter.type}_id` ] === filter.value ) {
                hasPassed = true;
            }
        });

        return hasPassed;
    }
}

export const filterHelper = new FilterHelper();
