/* eslint-disable quote-props */
import { browserHistory } from './BrowserHistory';

export const EMPLOYEE_SUBHEADER_ITEMS = [
    {
        title: 'People',
        links: [ 'employees', 'views' ],
        onClick: () => browserHistory.push( '/employees', true )
    },
    {
        title: 'Loans',
        links: ['loans'],
        onClick: () => browserHistory.push( '/loans' ),
        product: 'payroll'
    },
    {
        title: 'Deductions',
        links: ['deductions'],
        onClick: () => browserHistory.push( '/deductions' ),
        product: 'payroll'
    },
    {
        title: 'Adjustments',
        links: ['adjustments'],
        onClick: () => browserHistory.push( '/adjustments' ),
        product: 'payroll'
    },
    {
        title: 'Allowances',
        links: ['allowances'],
        onClick: () => browserHistory.push( '/allowances' ),
        product: 'payroll'
    },
    {
        title: 'Leaves',
        links: [ 'leaves', 'leave-credits' ],
        onClick: () => browserHistory.push( '/time/leaves', true ),
        product: 'time and attendance'
    },
    {
        title: 'Annual Earnings',
        links: ['annual-earnings'],
        onClick: () => browserHistory.push( '/annual-earnings' ),
        product: 'payroll'
    },
    {
        title: 'Workflows',
        links: ['workflow-entitlements'],
        onClick: () => browserHistory.push( '/time/workflow-entitlements', true ),
        product: 'time and attendance'
    },
    {
        title: 'Government Forms',
        links: [ 'employee-government-forms', '/government-forms' ],
        onClick: () => browserHistory.push( '/employee-government-forms', true )
    }
];

export const TIME_AND_ATTENDANCE_SUBHEADER_ITEMS = [
    {
        title: 'Attendance Computation',
        links: ['attendance-computations'],
        onClick: () =>
            browserHistory.push( '/time/attendance-computations', true )
    },
    {
        title: 'Shifts',
        links: ['shifts'],
        onClick: () => browserHistory.push( '/time/shifts/calendar', true )
    },
    {
        title: 'Schedules',
        links: [ 'schedules', 'views' ],
        onClick: () => browserHistory.push( '/time/schedules', true )
    }
];

export const PAYROLL_SUBHEADER_ITEMS = [
    {
        title: 'Payroll',
        links: [ 'payroll/payroll', 'special-pay-run', 'final-pay-run' ],
        onClick: () => browserHistory.push( '/payroll' )
    },
    {
        title: 'Payslips',
        links: ['payslip'],
        onClick: () => browserHistory.push( '/payslip' )
    },
    {
        title: 'Government Forms',
        links: ['forms'],
        onClick: () => {
            localStorage.removeItem( 'government-form-setting' );
            browserHistory.push( '/forms' );
        }
    },
    {
        title: 'Bonuses',
        links: ['bonuses'],
        onClick: () => browserHistory.push( '/bonuses' )
    },
    {
        title: 'Commissions',
        links: ['commissions'],
        onClick: () => browserHistory.push( '/commissions' )
    }
];

export const FREE_TRIAL_SUBSCRIPTIONS_SUBHEADER_ITEMS = [
    {
        title: 'Subscriptions',
        links: ['summary'],
        onClick: () =>
            browserHistory.push( '/control-panel/subscriptions/summary', true )
    },
    {
        title: 'Licenses',
        links: ['licenses'],
        onClick: () =>
            browserHistory.push( '/control-panel/subscriptions/licenses', true )
    },
    {
        title: 'Billing Information',
        links: ['billing-information'],
        onClick: () =>
            browserHistory.push(
                '/control-panel/subscriptions/billing-information',
                true,
            )
    },
    {
        title: 'Invoices',
        links: ['invoices'],
        onClick: () =>
            browserHistory.push( '/control-panel/subscriptions/invoices', true )
    },
    {
        title: 'Account Deletion',
        links: ['account-deletion'],
        onClick: () =>
            browserHistory.push( '/control-panel/subscriptions/account-deletion', true )
    }
];

export const SUBSCRIPTIONS_SUBHEADER_ITEMS = [
    {
        title: 'Subscriptions',
        links: ['summary'],
        onClick: () =>
            browserHistory.push( '/control-panel/subscriptions/summary', true )
    },
    {
        title: 'Licenses',
        links: ['licenses'],
        onClick: () =>
            browserHistory.push( '/control-panel/subscriptions/licenses', true )
    },
    {
        title: 'Billing Information',
        links: ['billing-information'],
        onClick: () =>
            browserHistory.push(
                '/control-panel/subscriptions/billing-information',
                true,
            )
    },
    {
        title: 'Invoices',
        links: ['invoices'],
        onClick: () =>
            browserHistory.push( '/control-panel/subscriptions/invoices', true )
    },
    {
        title: 'Receipts',
        links: ['receipts'],
        onClick: () =>
            browserHistory.push( '/control-panel/subscriptions/receipts', true )
    },
    {
        title: 'Account Deletion',
        links: ['account-deletion'],
        onClick: () =>
            browserHistory.push( '/control-panel/subscriptions/account-deletion', true )
    }
];

export const DEVICE_MANAGEMENT_SUBHEADER_ITEMS = [
    {
        title: 'Manage Users',
        links: ['device-management/users'],
        onClick: () =>
            browserHistory.push( '/control-panel/device-management/users', true )
    },
    {
        title: 'Manage unlink Users',
        links: ['device-management/unlinked-users'],
        onClick: () =>
            browserHistory.push(
                '/control-panel/device-management/unlinked-users',
                true,
            )
    }
];

export const NEW_RECORD_SAVED_MESSAGE = 'New record saved!';
export const RECORD_UPDATED_MESSAGE = 'Changes successfully saved!';
export const RECORD_DELETE_MESSAGE = 'Record successfully deleted!';

export const PRODUCTS = {
    TIME_AND_ATTENDANCE: 'time and attendance',
    PAYROLL: 'payroll',
    BOTH: 'time and attendance plus payroll'
};

export const DATE_FORMATS = {
    API: 'YYYY-MM-DD',
    API_2: 'MMM-DD-YYYY',
    DISPLAY: 'MMMM DD, YYYY',
    TIME: 'HH:mm',
    DEFAULT_SHIFT_DATETIME: 'YYYY-MM-DD HH:mm',
    FULL_TIME: 'HH:mm:ss',
    MMDDYYYY: 'MM/DD/YYYY',
    DATE_TIME: 'MMMM DD, YYYY hh:mm',
    SCHEDULES_DISPLAY: 'MM/DD/YYYY'
};

export const TIME_FORMATS = {
    DEFAULT: 'HH:mm'
};

export const FILED_LEAVE_UNITS_SINGULAR = {
    days: 'day',
    hours: 'hour'
};

export const DAY_TYPES = {
    REST_DAY: 'rest_day',
    REGULAR_DAY: 'regular_day'
};

export const VALIDATION_ERRORS = {
    CODE: {
        MISSING_PARAMETERS: 'VE-100',
        INVALID_FORMAT: 'VE-101',
        INVALID_VALUE: 'VE-102',
        DUPLICATE_DATA: 'VE-103',
        MISSING_HEADER: 'VE-110',
        INVALID_HEADER: 'VE-111',
        NON_EXISTENT_ID: 'VE-200',
        INFORMATION_MISMATCH: 'VE-201',
        INVALID_PARAMETER_VALUE: 'VE-202',
        PROCESSING_ERROR: 'VE-203'
    },
    KEY: {
        MISSING_ATTENDANCE_DATA: 'incomplete_attendance',
        MISSING_EMPLOYEE: 'missing_employees',
        INACTIVE_EMPLOYEE: 'inactive_employees'
    }
};

export const UPLOAD_FILE_HEADERS = {
    ID: 'Employee ID',
    FIRST_NAME: 'First Name',
    MIDDLE_NAME: 'Middle Name',
    LAST_NAME: 'Last Name',
    TIMESHEET_DATE: 'Timesheet Date',
    SCHEDULED_HOURS: 'Scheduled Hours',
    REGULAR: 'Regular',
    BONUS_TYPE: 'Bonus Type',
    ALLOWANCE_TYPE: 'Allowance Type',
    COMMISSION_TYPE: 'Commission Type',
    DEDUCTION_TYPE: 'Deduction Type',
    AMOUNT: 'Amount',
    TAXABLE: 'Taxable'
};

export const AUTHZ = {
    ROOT_ADMIN: 'root.admin'
};

export const SALPAY_INTEGRATION_SUBHEADER_ITEMS = [
    {
        title: 'Summary',
        links: ['summary'],
        onClick: () =>
            browserHistory.push(
                '/control-panel/salpay-integration/summary',
                true,
            )
    },
    {
        title: 'Payees',
        links: ['payees'],
        onClick: () =>
            browserHistory.push(
                '/control-panel/salpay-integration/payees',
                true,
            )
    },
    {
        title: 'Administrators',
        links: ['administrators'],
        onClick: () =>
            browserHistory.push(
                '/control-panel/salpay-integration/administrators',
                true,
            )
    }
];

export const EMPLOYEE_STATUS = [
    {
        value: 'active',
        label: 'Active',
        disabled: false
    },
    {
        value: 'semi-active',
        label: 'Semi-active',
        disabled: false
    },
    {
        value: 'inactive',
        label: 'Inactive',
        disabled: false
    }
];

export const REPEAT_WEEKLY_VALUE = {
    value: 'weekly',
    label: 'Weekly'
};

export const REPEAT_MONTHLY_VALUE = {
    value: 'monthly',
    label: 'Monthly'
};

export const REPEAT_EVERY_LABELS = {
    daily: 'days',
    weekly: 'weeks',
    monthly: 'months',
    yearly: 'years'
};

export const REPEAT_EVERY_VALUES = [ 'Daily', 'Weekly', 'Monthly', 'Yearly' ];

export const NEVER_OPTION = 'never';
export const AFTER_OPTION = 'after';
export const ON_OPTION = 'on';

export const REPEAT_BY_WEEK = 'week';
export const REPEAT_BY_MONTH = 'month';

export const END_AFTER_DEFAULT_VALUE = 35;

export const REPEAT_VALUES = [
    {
        value: 'daily',
        label: 'Daily'
    },
    {
        value: 'every_weekday',
        label: 'Every weekday (Monday to Friday)',
        summary: 'Weekly on weekdays'
    },
    {
        value: 'every_monday_wednesday_friday',
        label: 'Every Monday, Wednesday and Friday',
        summary: 'Every Monday, Wednesday, Friday'
    },
    {
        value: 'every_tuesday_thursday',
        label: 'Every Tuesday and Thursday',
        summary: 'Every Tuesday, Thursday'

    },
    REPEAT_WEEKLY_VALUE,
    REPEAT_MONTHLY_VALUE,
    {
        value: 'yearly',
        label: 'Yearly'
    }
];

export const REPEAT_DAYS = [
    {
        label: 'Sunday',
        value: 'sunday',
        selected: false
    },
    {
        label: 'Monday',
        value: 'monday',
        selected: false
    },
    {
        label: 'Tuesday',
        value: 'tuesday',
        selected: false
    },
    {
        label: 'Wednesday',
        value: 'wednesday',
        selected: false
    },
    {
        label: 'Thursday',
        value: 'thursday',
        selected: false
    },
    {
        label: 'Friday',
        value: 'friday',
        selected: false
    },
    {
        label: 'Saturday',
        value: 'saturday',
        selected: false
    }
];

export const TIME_METHODS_ALLOWED_VALUES = [
    {
        selected: true,
        name: 'Bundy App'
    },
    {
        selected: true,
        name: 'Time Sheet'
    },
    {
        selected: true,
        name: 'Web Bundy'
    },
    {
        selected: true,
        name: 'Biometric'
    }
];

export const MORE_ACTION = {
    action: [
        { label: 'Approve', name: 'approve' },
        { label: 'Decline', name: 'decline' }
    ]
};
export const PAY_FREQUENCY_VALUES = {
    monthly: 'MONTHLY',
    semiMonthly: 'SEMI_MONTHLY',
    fortnightly: 'FORTNIGHTLY',
    weekly: 'WEEKLY'
};

export const PAY_FREQUENCY = [
    { label: 'Monthly', value: 'MONTHLY', disabled: false },
    { label: 'Semi-monthly', value: 'SEMI_MONTHLY', disabled: false },
    { label: 'Fortnightly', value: 'FORTNIGHTLY', disabled: false },
    { label: 'Weekly', value: 'WEEKLY', disabled: false }
];

export const PAYRUN_NON_WORKING_DAYS = [
    {
        label: 'Move payday after non-working days',
        value: 'AFTER',
        disabled: false
    },
    {
        label: 'Move payday before non-working days',
        value: 'BEFORE',
        disabled: false
    },
    {
        label: 'Schedule payday on non-working days',
        value: 'ON',
        disabled: false
    }
];

export const PAYMENT_SCHEDULE = [
    { label: 'First Pay', value: 'FIRST_PAY', disabled: false },
    { label: 'Last Pay', value: 'LAST_PAY', disabled: false },
    { label: 'Every Pay', value: 'EVERY_PAY', disabled: false }
];

export const PREVIEW_SHIFTS_AND_METHODS_TEXT = {
    FIRST_PAY: 'First Pay',
    LAST_PAY: 'Last Pay',
    EVERY_PAY: 'Every pay',
    BASED_ON_ACTUAL: 'Based on Actual',
    EQUALLY_SPLIT: 'Equally Split'
};

export const PAY_FREQUENCY_VIEW = {
    MONTHLY: 'Monthly',
    SEMI_MONTHLY: 'Semi-monthly',
    FORTNIGHTLY: 'Fortnightly',
    WEEKLY: 'Weekly'
};

export const NON_WORKING_DAY_OPTION_VALUES = {
    AFTER: 'Move payday after non-working days',
    BEFORE: 'Move payday before non-working days',
    ON: 'Schedule payday on non-working days'
};

export const PAYROLL_GROUP_VALUES = {
    SCHEDULES_AND_METHODS: PREVIEW_SHIFTS_AND_METHODS_TEXT,
    FREQUENCY: PAY_FREQUENCY_VIEW,
    NON_WORKING_DAYS: NON_WORKING_DAY_OPTION_VALUES
};

export const UNITDATA = [
    { value: 'Days', label: 'Days' },
    { value: 'Hours', label: 'Hours' }
];
export const ACCRUEPERIODDATA = [
    { value: 'Once-Off', label: 'Once-Off' },
    { value: 'Monthly', label: 'Monthly' },
    { value: 'Semi-Monthly', label: 'Semi-Monthly' },
    { value: 'Weekly', label: 'Weekly' }
];
export const DATA1 = [
    { value: 'Months', label: 'Months' },
    { value: 'Half Months', label: 'Half Months' },
    { value: 'Weeks', label: 'Weeks' }
];
export const RUNDATA = [
    { value: 'Annual', label: 'Annual' },
    { value: 'Anniversary', label: 'Anniversary' },
    { value: 'Specific date', label: 'Specific date' }
];
export const TYPEDATA = [
    {
        value: 'Carry Over the Next Period',
        label: 'Carry Over the Next Period'
    },
    { value: 'Forfeit', label: 'Forfeit' }
];
export const TERMINATIONDATA = [{ value: 'Forfeit', label: 'Forfeit' }];

export const SELECT_ALL_EMPLOYEES_OPTIONS = [
    {
        id: null,
        value: null,
        type: 'department',
        label: 'All departments'
    },
    {
        id: null,
        value: null,
        type: 'position',
        label: 'All positions'
    },
    {
        id: null,
        value: null,
        type: 'location',
        label: 'All locations'
    },
    {
        id: null,
        value: null,
        type: 'employee',
        label: 'All employee'
    }
];

export const WEEKDAYS = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday' ];

export const DEFAULT_SHIFT_DATETIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';
export const DATE_FORMAT = 'MMMM DD, YYYY';
export const NOT_AVAILABLE = 'N/A';
export const REST_DAY = 'Rest Day';
export const REST_DAY_TYPE = 'rest_day';

export const PHT_UTC_OFFSET = '+0800';
