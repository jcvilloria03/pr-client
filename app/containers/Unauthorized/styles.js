import styled from 'styled-components';

export const Wrapper = styled.div`
    color: #666;
    position: relative;
    height: 100vh;
    text-align: center;
    font-size: 16px;

    .wrapper {
        width: 100%;
        padding-top: 64px;
        margin-top: 9.5rem;
        padding-bottom: 64px;
        display: flex;
        flex-direction: row;
    }

    h1 {
        color: #00A5E5;
        font-size: 36px;
        font-weight: bold;
        margin-bottom: 1rem;
        margin-top: 5px;
    }

    h3 {
        font-size: 26px;
        font-weight: bold;
        margin-bottom: 2rem;
    }

    h5 {
        font-size: 18px;
        font-weight: 400;
    }

    .left {
        position: relative;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-start;
        flex-grow: 1;
    }
`;
