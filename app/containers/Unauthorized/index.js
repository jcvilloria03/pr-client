/* eslint-disable react/no-unescaped-entities */
/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import Icon from 'components/Icon';

import { Wrapper } from './styles';
/**
 * Component for 404 pages.
 */
export default class Unauthorized extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        router: React.PropTypes.object
    }
    /**
    * Component render method
    */
    render() {
        return (
            <div>
                <Wrapper className="container" style={ { overflow: 'hidden', paddingTop: '70px' } }>
                    <div className="wrapper">
                        <div className="left">
                            <Icon name="unauthorized" />
                            <h1>
                                Unauthorized Access
                            </h1>
                            <h3> The page or resource you're trying to access is forbidden. </h3>
                            <h5> If you think this is incorrect, please contact your company administrator <br></br>
                                to provide you with permission to access.
                            </h5>
                        </div>
                    </div>
                </Wrapper>
            </div>
        );
    }
}
