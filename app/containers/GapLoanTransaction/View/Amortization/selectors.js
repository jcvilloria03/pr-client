import { createSelector } from 'reselect';

const selectGapLoanCandidatesDomain = () => ( state ) => state.get( 'gapLoanCandidates' );

const makeSelectAmortizationSubmitted = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'amortizationSubmitted' )
);

const makeSelectNotification = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLoan = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'loan' ).toJS()
);

export {
    makeSelectAmortizationSubmitted,
    makeSelectNotification,
    makeSelectLoan
};
