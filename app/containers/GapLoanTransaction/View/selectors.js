import { createSelector } from 'reselect';

const selectGapLoanCandidatesDomain = () => ( state ) => state.get( 'gapLoanCandidates' );

const makeSelectNotification = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLoan = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'loan' ).toJS()
);

const makeSelectGapLoanCadidates = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'gapLoanCandidates' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectEmployee = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'employee' ).toJS()
);

const makeSelectSubmitError = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'submitError' )
);

export {
    selectGapLoanCandidatesDomain,
    makeSelectNotification,
    makeSelectLoan,
    makeSelectGapLoanCadidates,
    makeSelectSubmitted,
    makeSelectEmployee,
    makeSelectSubmitError
};
