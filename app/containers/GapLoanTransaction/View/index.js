import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import SnackBar from '../../../components/SnackBar';
import SubHeader from '../../SubHeader';
import GapLoanCandidatesView from './templates/List';
import InitialView from './templates/InitialView';
import GapLoanView from './templates/GapLoanView';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { PAYROLL_SUBHEADER_ITEMS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import * as viewActions from './actions';
import {
    makeSelectNotification,
    makeSelectLoan,
    makeSelectGapLoanCadidates,
    makeSelectSubmitted,
    makeSelectEmployee,
    makeSelectSubmitError
} from './selectors';
import { PageWrapper } from './styles';

/**
 * Preview employees that are gap loan candidates
 */
export class View extends React.Component {
    static propTypes = {
        gapLoanCandidates: React.PropTypes.object,
        resetStore: React.PropTypes.func,
        getEmployeeData: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        routeParams: React.PropTypes.object,
        loan: React.PropTypes.object,
        submitted: React.PropTypes.bool,
        employee: React.PropTypes.object,
        products: React.PropTypes.array
    }

    constructor( props ) {
        super( props );
        this.state = {
            showInitialView: true,
            selectedIndex: -1,
            finishedEditing: false,
            collapsed: false
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.payroll_loans'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        if ( this.props.loan.uid !== null ) {
            this.setState({ showInitialView: false });
        }
        this.setState({ finishedEditing: !this.hasUnfinishedGapLoans() });
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.submitted && !nextProps.submitted && !nextProps.submitError ) {
            this.goToNextEmployee( this.props.employee );
        }
        this.setState({ finishedEditing: !this.hasUnfinishedGapLoans() });
    }

     /**
     * Reset store
     */
    componentWillUnmount() {
        if ( localStorage.getItem( 'addLoanEditingAmortization' ) === null ) {
            this.props.resetStore();
        }
    }

    selectEmployee = ( employee, index ) => {
        this.setState({ showInitialView: false, selectedIndex: index });
        this.props.getEmployeeData( employee );
    }

    hasUnfinishedGapLoans = () => (
        !!this.props.gapLoanCandidates.data.filter( ( candidate ) => !candidate.gap_loan_id ).length
    )

    goToNextEmployee( employee ) {
        const index = this.props.gapLoanCandidates.data.findIndex( ( e ) => e.id === employee.data.id );
        let nextIndex = this.props.gapLoanCandidates.data.findIndex( ( candidate, i ) => i > index && !candidate.gap_loan_id );
        if ( nextIndex === -1 ) {
            nextIndex = this.props.gapLoanCandidates.data.findIndex( ( candidate, i ) => i < index && !candidate.gap_loan_id );
        }
        if ( nextIndex === -1 ) {
            this.setState({ showInitialView: true, finishedEditing: true, selectedIndex: -1 });
            return;
        }
        this.setState({ selectedIndex: nextIndex }, () => {
            this.props.getEmployeeData( this.props.gapLoanCandidates.data[ this.state.selectedIndex ]);
        });
    }

    toggleCollapse = () => {
        this.setState({ collapsed: !this.state.collapsed });
    }

    render() {
        const payrollId = this.props.routeParams.id;

        return (
            <div>
                <Helmet
                    title="View Payroll"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <PageWrapper>
                    <div className="content">
                        <GapLoanCandidatesView
                            ref={ ( ref ) => { this.gapLoanCandidatesList = ref; } }
                            onSelect={ this.selectEmployee }
                            payrollId={ payrollId }
                            selectedIndex={ this.state.selectedIndex }
                            onCollapse={ this.toggleCollapse }
                            collapsed={ this.state.collapsed }
                        />
                        { this.state.showInitialView && !this.props.loan.uid ?
                            <InitialView
                                finishedEditing={ this.state.finishedEditing }
                            /> : <GapLoanView
                                payrollId={ payrollId }
                                employeeListCollapsed={ this.state.collapsed }
                            />
                        }
                    </div>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    loan: makeSelectLoan(),
    gapLoanCandidates: makeSelectGapLoanCadidates(),
    submitted: makeSelectSubmitted(),
    employee: makeSelectEmployee(),
    submitError: makeSelectSubmitError()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        viewActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
