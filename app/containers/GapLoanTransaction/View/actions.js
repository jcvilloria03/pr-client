import {
    GET_GAP_LOAN_CANDIDATES,
    NOTIFICATION,
    GET_EMPLOYEE,
    UPDATE_AMORTIZATION_PREVIEW,
    GENERATE_PAYMENT_SCHEDULE_DETAILS,
    SUBMIT_GAP_LOAN,
    RESET_STORE,
    EDIT_LOAN,
    EDIT_PAYMENT_SCHEDULE_DETAILS,
    FINISH_GAP_LOAN_TRANSACTION,
    SET_FINISHING
} from './constants';

/**
 * Fetch list of gap loan candidates
 */
export function getGapLoanCandidates( payrollId ) {
    return {
        type: GET_GAP_LOAN_CANDIDATES,
        payload: {
            payrollId
        }
    };
}

/**
 * Fetch list of gap loan candidates
 */
export function getEmployeeData( employee ) {
    return {
        type: GET_EMPLOYEE,
        payload: {
            employee
        }
    };
}

/**
 * Update loan preview
 */
export function editPaymentScheduleDetails( payload ) {
    return {
        type: EDIT_PAYMENT_SCHEDULE_DETAILS,
        payload
    };
}

/**
 * Update loan preview
 */
export function generatePaymentScheduleDetails( payload ) {
    return {
        type: GENERATE_PAYMENT_SCHEDULE_DETAILS,
        payload
    };
}

/**
 * Submit a new payroll loan
 */
export function submitGapLoan( payload ) {
    return {
        type: SUBMIT_GAP_LOAN,
        payload
    };
}

/**
 * Update loan preview
 */
export function editLoan( payload ) {
    return {
        type: EDIT_LOAN,
        payload
    };
}

/**
* Update amortization preview
*/
export function updateAmortizationPreview( payload ) {
    return {
        type: UPDATE_AMORTIZATION_PREVIEW,
        payload
    };
}

/**
 * Sends request to finish the gap loan transaction
 * @param {Integer} id - Payroll ID
 * @returns {Object}
 */
export function finishGapLoanTransaction( id ) {
    return {
        type: FINISH_GAP_LOAN_TRANSACTION,
        payload: id
    };
}

/**
 * Sets finishing status for the Finish Button
 * @param {Boolean} payload - Finishing status
 * @returns {Object}
 */
export function setFinishing( id ) {
    return {
        type: SET_FINISHING,
        payload: id
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
