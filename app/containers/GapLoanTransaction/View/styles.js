import styled from 'styled-components';

export const PageWrapper = styled.div`
    display: flex;
    flex-direction: column;
    padding-top: 0;
    height: calc( 100vh - 127px );

    .break-word {
        word-break: break-all;
    }

    > .heading {
        padding: 10px 20px;
        position: fixed;
        background: #f0f4f6;
        z-index: 1;
        width: 100%;
        top: 76px;
    }

    > .content {
        display: flex;
        position: relative;
        height: 100%;

        > .list-container {
            position: fixed;
            height: 100%;
            z-index: 2;

            @media (max-width: 1280px) {
                width: 70px;
            }

            &.collapsed {
                width: 70px;
            }
        }
    }

    .date-picker {
        .DayPickerInput {
            width: 100%;
        }
        span {
            display: block;
        }
        input {
            width: 100%;
            padding-top: 0px !important;
        }
    }
`;
