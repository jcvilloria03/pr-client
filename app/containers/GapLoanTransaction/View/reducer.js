import { fromJS } from 'immutable';
import {
    NOTIFICATION_SAGA,
    SET_GAP_LOAN_CANDIDATES,
    SET_GAP_LOAN_CANDIDATES_LOADING,
    SET_EMPLOYEE_LOADING,
    SET_EMPLOYEE_ERRORS,
    SET_EMPLOYEE,
    SET_FORM_OPTIONS,
    SET_AMORTIZATION_SUBMITTED,
    GENERATING,
    SUBMITTED,
    SET_ERRORS,
    SET_GENERATED_PAYMENT_SCHEDULE_DETAILS,
    SET_LOAN,
    SET_FINISHING,
    SUBMIT_ERROR
} from './constants';

const initialState = fromJS({
    gapLoanCandidates: {
        data: [],
        loading: true
    },
    amortizationSubmitted: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    employee: {
        data: {},
        loading: true,
        saving: false,
        errors: null
    },
    formOptions: {
        payrollGroups: []
    },
    generating: false,
    submitted: false,
    amortizations: [],
    errors: {},
    loan: {
        id: null,
        uid: null,
        type_id: '',
        total_amount: '',
        subtype: '',
        reference_no: '',
        payment_start_date: null,
        payment_end_date: null,
        payment_scheme: '',
        monthly_amortization: '',
        employee_id_no: '',
        employee_id: '',
        created_date: '',
        company_id: '',
        term: ''
    },
    submitError: false,
    finishing: false
});

/**
 *
 * View reducer
 *
 */
function viewReducer( state = initialState, action ) {
    switch ( action.type ) {
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_GAP_LOAN_CANDIDATES:
            return state.setIn([ 'gapLoanCandidates', 'data' ], fromJS( action.payload ) );
        case SET_GAP_LOAN_CANDIDATES_LOADING:
            return state.setIn([ 'gapLoanCandidates', 'loading' ], action.payload );
        case SET_EMPLOYEE_LOADING:
            return state.setIn([ 'employee', 'loading' ], action.payload );
        case SET_EMPLOYEE_ERRORS:
            return state.setIn([ 'employee', 'errors' ], fromJS( action.payload ) );
        case SET_EMPLOYEE:
            return state.setIn([ 'employee', 'data' ], fromJS( action.payload ) );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_AMORTIZATION_SUBMITTED:
            return state.set( 'amortizationSubmitted', action.payload );
        case SET_LOAN:
            return state.set( 'loan', fromJS( action.payload ) );
        case GENERATING:
            return state.set( 'generating', action.payload );
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case SET_FINISHING:
            return state.set( 'finishing', action.payload );
        case SET_GENERATED_PAYMENT_SCHEDULE_DETAILS:
            return state.set( 'amortizations', fromJS( action.payload ) );
        case SUBMIT_ERROR:
            return state.set( 'submitError', action.payload );
        default:
            return state;
    }
}

export default viewReducer;
