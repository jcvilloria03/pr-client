/*
 *
 * View constants
 *
 */

const namespace = 'app/GapLoanTransaction';

export const NOTIFICATION_SAGA = `${namespace}/View/NOTIFICATION_SAGA`;
export const NOTIFICATION = `${namespace}/View/NOTIFICATION`;

export const GET_GAP_LOAN_CANDIDATES = `${namespace}/View/GET_GAP_LOAN_CANDIDATES`;
export const SET_GAP_LOAN_CANDIDATES = `${namespace}/View/SET_GAP_LOAN_CANDIDATES`;
export const SET_GAP_LOAN_CANDIDATES_LOADING = `${namespace}/View/SET_GAP_LOAN_CANDIDATES_LOADING`;

export const SET_FORM_OPTIONS = `${namespace}/View/SET_FORM_OPTIONS`;

export const GET_EMPLOYEE = `${namespace}/View/GET_EMPLOYEE`;
export const SET_EMPLOYEE = `${namespace}/View/SET_EMPLOYEE`;
export const SET_EMPLOYEE_LOADING = `${namespace}/View/SET_EMPLOYEE_LOADING`;
export const SET_EMPLOYEE_ERRORS = `${namespace}/View/SET_EMPLOYEE_ERRORS`;

export const SET_LOAN = `${namespace}/View/SET_LOAN`;

export const SET_AMORTIZATION_SUBMITTED = `${namespace}/View/SET_AMORTIZATION_SUBMITTED`;
export const UPDATE_AMORTIZATION_PREVIEW = `${namespace}/View/UPDATE_AMORTIZATION_PREVIEW`;
export const GENERATE_PAYMENT_SCHEDULE_DETAILS = `${namespace}/View/GENERATE_PAYMENT_SCHEDULE_DETAILS`;
export const SET_GENERATED_PAYMENT_SCHEDULE_DETAILS = `${namespace}/View/SET_GENERATED_PAYMENT_SCHEDULE_DETAILS`;
export const GENERATING = `${namespace}/View/GENERATING`;
export const SUBMITTED = `${namespace}/View/SUBMITTED`;
export const SUBMIT_GAP_LOAN = `${namespace}/View/SUBMIT_GAP_LOAN`;
export const EDIT_LOAN = `${namespace}/View/EDIT_LOAN`;
export const EDIT_PAYMENT_SCHEDULE_DETAILS = `${namespace}/View/EDIT_PAYMENT_SCHEDULE_DETAILS`;
export const SUBMIT_ERROR = `${namespace}/View/SUBMIT_ERROR`;

export const SET_ERRORS = `${namespace}/View/SET_ERRORS`;

export const RESET_STORE = `${namespace}/View/RESET_STORE`;

export const SET_FINISHING = `${namespace}/View/SET_FINISHING`;
export const FINISH_GAP_LOAN_TRANSACTION = `${namespace}/View/FINISH_GAP_LOAN_TRANSACTION`;
