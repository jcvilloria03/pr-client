import styled from 'styled-components';

export const MessageWrapper = styled.div`
    display: flex;
    justify-content: center;
    height: calc(100% - 90px);
    width: 100%;
    margin-bottom: 0 !important;

    > div {
        display: flex;
        flex-direction: column
        justify-content: center;

        i {
            font-size: 7em;
            align-self: center;
        }

        .description {
            padding-top: 40px;
            text-align: center;

            h3 {
                font-weight: 600;
            }
        }
    }
`;

export const ListWrapper = styled.div`
    display: flex;
    flex-direction: column;
    background-color: #fff;
    border-right: 1px solid #e5e5e5;
    padding-top: 40px;
    min-width: 500px;
    height: calc(100vh - 124px);
    left: 0;
    bottom: 0;
    top: 0;

    @media (max-width: 1440px) {
        min-width: 500px;
        position: absolute;

        > div {
            padding-left: 15px;
            padding-right: 15px;
        }
    }

    @media (max-width: 1280px) {
        position: absolute;
        z-index: 10;
        min-width: 400px;

        > div {
            padding-left: 40px;
            padding-right: 40px;
        }
    }

    &.collapsed {
        display: none;
    }

    .actions {
        display: flex;
        flex-direction: column;
        padding: 0 45px 25px;

        .bottom {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding-top: 30px;

            h5 {
                padding-bottom: 0;
                font-weight: bold;
                font-size: 1.2rem;
            }

            .button-collapse {
                border: none;
                outline: none;
                margin-left: 10px;
                cursor: pointer;

                i {
                    color: #adadad;
                    font-size: 1.8em;
                }
            }
        }
    }

    .list {
        height: 100%;
        overflow-y: auto;
        padding: 0 45px 25px;

        ul {
            list-style: none;
            display: flex;
            flex-direction: column;
            padding: 0 0 25px;
        }
    }
`;

export const ListItemWrapper = styled.li`
    display: flex;
    justify-content: space-between;
    padding: 15px 20px;
    border-bottom: 1px solid #e5e5e5;
    cursor: pointer;
    border-left: 4px solid #fff;

    .icon-section {
        margin-left: 30px;
        align-items: flex-end;
        width: 30px;
        height: 100%;

        .icon svg {
            width: 25px;
            height: 100%;
            color: #9fdc74;
        }
    }

    div:first-child {
        width: 35%;
    }

    &:hover {
        background-color: rgba(0, 165, 229, 0.01);
    }

    &.active,
    &:hover.active {
        border-left: 4px solid #00a5e5;
        font-weight: 700;
        background-color: #F5FCEF;
    }

    &.disabled,
    &:hover.disabled {
        cursor: not-allowed;
    }
`;

export const ListCollapsedWrapper = styled.div`
    top: 100px;
    position: absolute;
    background-color: transparent;
    z-index: 10;

    &.collapsed {
        display: none;

        @media (max-width: 1280px) {
            display: block;
        }
    }

    button {
        border: 1px solid #e5e5e5;
        border-left: none;
        border-radius: 10px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        background-color: #fff;
        outline: none;
        cursor: pointer;
        padding: 6px 11px;
        font-size: 13px;

        i {
            color: #adadad;
            font-size: 1.8em;
        }
    }
`;

export const InitialViewWrapper = styled.div`
    padding: 40px 5%;
    width: 100%;
    height: 100%;
    max-height: 70vh;

    @media (max-width: 1440px) {
        padding: 40px 15px 40px 50px;
    }

    @media (max-width: 1280px) {
        padding: 40px 25px 40px 50px;
    }

    .offset-right-4 {
        margin-right: 33.333333%;
    }

    div {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        height: 100%;
        padding: 0;
        margin: 0;
    }
`;

export const MainWrapper = styled.div`
    padding-top: 76px;
    background: #fff;
    margin-right: ${( props ) => ( !props.isEmployeeListVisible ? '50px' : '0' )};
    width: ${( props ) => ( !props.isEmployeeListVisible ? 'calc(100vw - 600px)' : '100vw' )};
    margin-left: ${( props ) => ( !props.isEmployeeListVisible ? '550px' : '0' )};

    @media (max-width: 1280px) {
        margin-right: 0;
        width: 100vw;
        margin-left: 0;
    }

    div > .footer {
        padding-bottom: 30px;

        > .container {
            display: flex;
            flex-direction: row-reverse;
            width: 100%;
        }
    }

    .tableAction button {
        width: 130px;
    }

    .title {
        display: flex;
        align-items: center;
        margin-bottom: 20px;

        h5 {
            margin: 0;
            margin-right: 20px;
            font-weight: 600;
            font-size: 22px;
        }

        .search-wrapper {
            flex-grow: 1;

            .search {
                width: 300px;
                border: 1px solid #333;
                border-radius: 30px;

                input {
                    border: none;
                }
            }

            p {
                display: none;
            }

            .input-group,
            .form-control {
                background-color: transparent;
            }

            .input-group-addon {
                background-color: transparent;
                border: none;
            }

            .isvg {
                display: inline-block;
                width: 1rem;
            }
        }
    }

    .nav {
        padding: 10px 20px;
        background: #f0f4f6;
        margin-bottom: 50px;
    }

    .loader {
        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .money {
        input {
            text-align: right;
        }
    }

    .stepper {
        padding: 20px 10vw;
    }

    .radiogroup {
        padding: 0;
        & > span {
            padding: 0 15px;
            color: #5b5b5b;
            font-size: 14px;
            margin-bottom: 4px;
            margin-right: 1rem;
            font-weight: 400;
        }
    }

    .basic, .employment, .payroll {
        padding: 20px 10vw;

        h4 {
            font-weight: 600;
        }
    }

    .employment .DayPickerInput-Overlay {
        bottom: 47px;
    }

    .date {
        & > div > span {
            display: block;
        }
        .DayPickerInput {
            display: block;

            input {
                width: 100%;
            }
        }
    }

    .heading {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 0 auto 30px auto;

        h3 {
            font-weight: 600;
        }

        p {
            text-align: center;
            max-width: 800px;
        }
    }

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        button {
            margin-left: 12px;
            min-width: 120px;
        }
    }

    .hide {
        display: none;
    }

    .selected .fill {
      fill: #fff;
    }

    .selected .stroke {
      stroke: #fff;
    }

    .fill {
      fill: rgb(0,165,226);
    }

    .stroke {
      stroke: rgb(0,165,226);
    }

    .sl-u-special-select {
        margin-top: 67px;

        @media (min-width: 1080px) {
            margin-top: 46px;
        }

        @media (min-width: 1480px) {
            margin-top: 25px;
        }
    }
`;

export const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;

    h3 {
        font-weight: 600;
    }

    p {
        text-align: center;
        max-width: 800px;
    }
`;

