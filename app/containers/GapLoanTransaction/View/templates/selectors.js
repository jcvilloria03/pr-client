import { createSelector } from 'reselect';

const selectGapLoanCandidatesDomain = () => ( state ) => state.get( 'gapLoanCandidates' );

const makeSelectGenerating = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'generating' )
);

const makeSelectFormOptions = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectEmployee = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'employee' ).toJS()
);

const makeSelectAmortizations = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'amortizations' ).toJS()
);

const makeSelectGapLoanCadidates = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'gapLoanCandidates' ).toJS()
);

const makeSelectLoan = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'loan' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectFinishing = () => createSelector(
    selectGapLoanCandidatesDomain(),
    ( substate ) => substate.get( 'finishing' )
);

export {
    makeSelectGenerating,
    makeSelectFormOptions,
    makeSelectEmployee,
    makeSelectAmortizations,
    makeSelectGapLoanCadidates,
    makeSelectLoan,
    makeSelectSubmitted,
    makeSelectFinishing
};
