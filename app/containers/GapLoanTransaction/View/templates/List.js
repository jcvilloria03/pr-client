import React from 'react';
import isEqual from 'lodash/isEqual';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import { H3 } from 'components/Typography';
import SalConfirm from 'components/SalConfirm';
import Button from 'components/Button';
import Icon from 'components/Icon';
import Loader from 'components/Loader';

import * as actions from '../actions';

import {
    makeSelectGapLoanCadidates,
    makeSelectLoan,
    makeSelectSubmitted,
    makeSelectGenerating,
    makeSelectFormOptions,
    makeSelectFinishing
} from './selectors';

import {
    ListWrapper,
    ListItemWrapper,
    MessageWrapper,
    ListCollapsedWrapper
} from './styles';

/**
 * EmployeeListView component
 */
class EmployeeListView extends React.PureComponent {
    static propTypes = {
        finishGapLoanTransaction: React.PropTypes.func,
        getGapLoanCandidates: React.PropTypes.func,
        gapLoanCandidates: React.PropTypes.shape({
            data: React.PropTypes.arrayOf(
                React.PropTypes.object
            ),
            loading: React.PropTypes.bool
        }),
        onSelect: React.PropTypes.func,
        payrollId: React.PropTypes.string,
        loan: React.PropTypes.object,
        selectedIndex: React.PropTypes.number,
        generating: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        collapsed: React.PropTypes.bool,
        onCollapse: React.PropTypes.func,
        finishing: React.PropTypes.bool
    }

    /**
     * component's constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            gapLoanCandidates: [],
            activeEmployeeIndex: this.props.selectedIndex,
            showCloseConfirm: false
        };
    }

    componentWillMount() {
        if ( this.props.loan.uid === null ) {
            this.props.getGapLoanCandidates( this.props.payrollId );
        } else {
            this.setInitialStateFromProps( this.props );
        }
    }

    componentWillReceiveProps( nextProps ) {
        !isEqual( nextProps.gapLoanCandidates.data, this.props.gapLoanCandidates.data )
        && this.setState({
            gapLoanCandidates: nextProps.gapLoanCandidates.data
        });
        nextProps.selectedIndex !== this.state.activeEmployeeIndex && this.setActiveEmployee( nextProps.selectedIndex );
    }

    setInitialStateFromProps( props ) {
        this.setState({ gapLoanCandidates: props.gapLoanCandidates.data }, () => {
            const index = this.state.gapLoanCandidates.findIndex( ( employee ) => employee.id === parseInt( this.props.loan.employee_id, 10 ) );
            const nextActiveEmployee = this.state.gapLoanCandidates[ index ];
            if ( nextActiveEmployee ) {
                nextActiveEmployee.active = true;
                this.setState({ activeEmployeeIndex: index });
            }
            this.setState({ collapse: false });
        });
    }

    setActiveEmployee( nextActiveIndex ) {
        if ( this.props.generating || this.props.submitted ) {
            return;
        }
        const gapLoanCandidates = this.state.gapLoanCandidates;

        const activeEmployee = gapLoanCandidates[ this.state.activeEmployeeIndex ];
        if ( activeEmployee ) {
            activeEmployee.active = false;
        }

        const nextActiveEmployee = gapLoanCandidates[ nextActiveIndex ];
        if ( nextActiveEmployee ) {
            this.props.onSelect( nextActiveEmployee, nextActiveIndex );
            nextActiveEmployee.active = true;

            this.setState({
                gapLoanCandidates,
                activeEmployeeIndex: nextActiveIndex
            });
        }
    }

    handleListItemStyle = ( employee ) => ( `${this.props.generating || this.props.submitted ? 'disabled' : ''} ${employee.active ? 'active' : ''}` );

    hasCandidatesWithoutGapLoans = () => ( !!this.state.gapLoanCandidates.filter( ( candidate ) => candidate.gap_loan_id === null ).length )

    finishGapLoanTransaction = () => {
        this.props.finishGapLoanTransaction( this.props.payrollId );
    }

    /**
     * renders a loading screen
     */
    renderLoadingScreen() {
        return (
            <MessageWrapper style={ { backgroundColor: '#fff' } }>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" aria-hidden="true" />
                    <div className="description">
                        <H3>Loading employee list...</H3>
                    </div>
                </div>
            </MessageWrapper>
        );
    }

    renderEmployeeList = () => (
        <div className="list">
            <ul>
                {
                    this.state.gapLoanCandidates.map( ( employee, index ) =>
                        <ListItemWrapper
                            key={ index }
                            className={ this.handleListItemStyle( employee ) }
                            onClick={ () => this.setActiveEmployee( index ) }
                        >
                            <div>{ employee.employee_id }</div>
                            <div className="break-word">{ employee.first_name } { employee.last_name }</div>
                            <div className="icon-section">
                                { employee.gap_loan_id ? <Icon name="checkCircle" className="icon" /> : null }
                            </div>
                        </ListItemWrapper>
                    )
                }
            </ul>
        </div>
    );

    renderConfirmModal = () => (
        <SalConfirm
            onConfirm={ () => { this.finishGapLoanTransaction(); } }
            body={
                <div style={ { display: 'flex', padding: '0 20px', flexDirection: 'column', alignItems: 'center' } }>
                    <div style={ { display: 'flex', alignSelf: 'center' } } >
                        You are about to close the gap loan page. Please be informed that your payroll will re-calculate
                        and all created gap loans will be credited to employees. Please click the confirm button if you
                        wish to proceed.
                    </div>
                </div>
            }
            title="Are you sure you want to proceed?"
            visible={ this.state.showCloseConfirm }
            buttonStyle="warning"
        />
    )

    /**
     * render's component to DOM
     */
    render() {
        const collapsed = this.props.collapsed ? 'collapsed' : '';
        return (
            <div className={ `list-container ${collapsed}` }>
                <ListCollapsedWrapper className={ this.props.collapsed ? '' : 'collapsed' }>
                    <button className="button-collapse" onClick={ this.props.onCollapse } >
                        <i className="fa fa-bars" aria-hidden="true"></i>
                    </button>
                </ListCollapsedWrapper>
                { this.renderConfirmModal() }
                <ListWrapper className={ collapsed }>
                    <div className="actions">
                        <Button
                            label={ this.props.finishing ? <Loader /> : 'Finish and Re-calculate Payroll' }
                            size="large"
                            type="action"
                            disabled={ this.props.gapLoanCandidates.loading || this.props.finishing }
                            onClick={ () => {
                                this.setState({ showCloseConfirm: false }, () => {
                                    this.setState({ showCloseConfirm: true });
                                });
                            } }
                        />
                        <div className="bottom">
                            <h5>Employee list { this.state.gapLoanCandidates.length ? `(${this.state.gapLoanCandidates.length})` : '' }</h5>
                            <button className="button-collapse" onClick={ this.props.onCollapse }>
                                <i className="fa fa-bars" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    {
                        this.props.gapLoanCandidates.loading
                            ? this.renderLoadingScreen()
                            : this.renderEmployeeList()
                    }
                </ListWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    gapLoanCandidates: makeSelectGapLoanCadidates(),
    loan: makeSelectLoan(),
    submitted: makeSelectSubmitted(),
    generating: makeSelectGenerating(),
    formOptions: makeSelectFormOptions(),
    finishing: makeSelectFinishing()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EmployeeListView );
