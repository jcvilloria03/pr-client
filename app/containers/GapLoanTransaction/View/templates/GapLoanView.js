import React from 'react';
import { Container } from 'reactstrap';
import decimal from 'js-big-decimal';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import Input from '../../../../components/Input';
import { H3, H5 } from '../../../../components/Typography';
import Button from '../../../../components/Button';
import DatePicker from '../../../../components/DatePicker';
import SalSelect from '../../../../components/Select';
import Table from '../../../../components/Table';
import Icon from '../../../../components/Icon';
import Loader from '../../../../components/Loader';

import { formatCurrencyToDecimalNotation, formatPaginationLabel, formatDate } from '../../../../utils/functions';
import { DATE_FORMATS } from '../../../../utils/constants';

import { PAYMENT_SCHEME_OPTIONS } from '../../../Loans/constants';

import * as actions from '../actions';

import {
    makeSelectGenerating,
    makeSelectFormOptions,
    makeSelectEmployee,
    makeSelectAmortizations,
    makeSelectLoan,
    makeSelectSubmitted
} from './selectors';

import { MainWrapper, HeadingWrapper, MessageWrapper } from './styles';

/**
 * Gap loan for a single employee
 */
class GapLoanView extends React.Component {
    static propTypes = {
        editLoan: React.PropTypes.func,
        submitGapLoan: React.PropTypes.func,
        generatePaymentScheduleDetails: React.PropTypes.func,
        editPaymentScheduleDetails: React.PropTypes.func,
        payrollId: React.PropTypes.string,
        generating: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        formOptions: React.PropTypes.object,
        employee: React.PropTypes.object,
        amortizations: React.PropTypes.array,
        loan: React.PropTypes.object,
        employeeListCollapsed: React.PropTypes.bool
    }

    constructor( props ) {
        super( props );
        this.state = {
            loanData: this.props.loan,
            amortizations: this.props.amortizations,
            label: 'Showing 0-0 of 0 entries'
        };
    }

    componentWillMount() {
        if ( this.props.loan.uid !== null ) {
            this.setInitialStateFromProps( this.props );
        }
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.loan.payment_end_date !== this.state.loanData.payment_end_date && this.setState({ loanData: nextProps.loan });

        nextProps.amortizations !== this.props.amortizations && this.setState({ amortizations: nextProps.amortizations }, () => {
            this.handleTableChanges();
        });
    }

    setInitialStateFromProps( props ) {
        this.setState({ loanData: props.loan }, () => {
            this.handleTableChanges();
        });
    }

    getPaymentSchemeOptions = () => {
        const payrollGroup = this.props.formOptions.payrollGroups.find( ( group ) => group.id === this.props.employee.data.payroll_group_id );
        return PAYMENT_SCHEME_OPTIONS[ payrollGroup.payroll_frequency ];
    }

    submitLoan = () => {
        const { loanData } = this.state;
        const payload = this.adjustPayloadNumberValues( loanData );

        Object.assign(
            payload,
            { created_date: payload.payment_start_date },
            { employee_id: this.props.employee.data.id },
            { type_id: 3 },
            { subtype: null },
            { payroll_id: this.props.payrollId },
            { active: false }
        );

        if ( this.validateForm() ) {
            this.props.employee.data.gap_loan_id ? this.props.editLoan( payload ) : this.props.submitGapLoan( payload );
        }
    }

    /**
     * Updates the state with the form inputs
     */
    updateState( key, value ) {
        if ( typeof value === 'undefined' ) {
            return;
        }
        const dataClone = Object.assign({}, this.state.loanData );
        dataClone[ key ] = value !== '' && value !== null ? value : '';
        this.setState({ loanData: Object.assign( this.state.loanData, dataClone ) });
    }

    /**
     * handles filters and search inputs
     */
    handleSearch = () => {
        let searchQuery = null;

        let dataToDisplay = this.props.amortizations;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery ) {
            dataToDisplay = dataToDisplay.filter( ( amortization ) => {
                let match = false;
                const { due_date } = amortization;
                if ( formatDate( due_date, DATE_FORMATS.DISPLAY ).toLowerCase().indexOf( searchQuery ) >= 0 ) {
                    match = true;
                }

                return match;
            });
        }

        this.setState({ amortizations: dataToDisplay }, () => {
            this.handleTableChanges();
        });
    }

        /**
     * handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = () => {
        if ( !this.amortizationsTable ) {
            return;
        }

        this.setState({
            label: formatPaginationLabel( this.amortizationsTable.tableComponent.state )
        });
    }

    handleLoanTermCalculation = () => {
        const totalAmount = parseFloat( this.state.loanData.total_amount );
        const monthlyAmortization = parseFloat( this.state.loanData.monthly_amortization );
        if ( totalAmount && monthlyAmortization && ( totalAmount >= monthlyAmortization ) ) {
            const precision = 5;
            const numberOfPayments = decimal.divide( totalAmount, monthlyAmortization, precision );
            this.updateState( 'term', decimal.ceil( numberOfPayments ) );
        } else {
            this.updateState( 'term', '' );
        }
    }

    handleMonthlyDeductionCalculation = () => {
        const totalAmount = parseFloat( this.state.loanData.total_amount );
        const loanTerm = parseInt( this.state.loanData.term, 10 );
        if ( totalAmount && loanTerm ) {
            let monthlyDeduction = decimal.divide( totalAmount, loanTerm );
            if ( loanTerm * decimal.round( monthlyDeduction.toString(), 2 ) < totalAmount ) {
                monthlyDeduction += 0.01;
            }
            this.updateState( 'monthly_amortization', decimal.round( monthlyDeduction.toString(), 2 ) );
        } else {
            this.updateState( 'monthly_amortization', '' );
        }
    }

    handleCreatedDateChange = ( value ) => {
        const selectedDay = formatDate( value, DATE_FORMATS.API );
        this.state.loanData.payment_start_date = selectedDay;
    }

    validateMonthlyAmortizations = () => {
        if ( parseFloat( this.monthly_amortization.state.value ) > parseFloat( this.total_amount.state.value ) ) {
            const errorMessage = 'Monthly amortization must be less or equal to total amount.';
            this.monthly_amortization.setState({ error: true, errorMessage });
        }
    }

    /**
     * Returns new payload object with adjusted numbers
     */
    adjustPayloadNumberValues = ( payload ) => ({
        ...payload,
        total_amount: decimal.round( payload.total_amount, 2 ),
        monthly_amortization: decimal.round( payload.monthly_amortization, 2 )
    });

    validateForm = () => {
        const { loanData } = this.state;

        let valid = true;

        if ( this.payment_start_date.checkRequired() ) {
            valid = false;
        }

        if ( this.term._validate( this.term.state.value ) ) {
            valid = false;
        }

        if ( this.total_amount._validate( this.total_amount.state.value ) ) {
            valid = false;
        }

        if ( this.monthly_amortization._validate( this.monthly_amortization.state.value ) ) {
            valid = false;
        }

        if ( !this.payment_scheme._checkRequire( this.payment_scheme.state.value ) ) {
            valid = false;
        }

        if ( parseFloat( loanData.monthly_amortization ) > parseFloat( loanData.total_amount ) ) {
            const errorMessage = 'Monthly amortization must be less or equal to total amount.';
            this.monthly_amortization.setState({ error: true, errorMessage });

            valid = false;
        }

        return valid;
    };

    /**
     * Sends api request for updating loan preview
     */
    generatePaymentScheduleDetails = () => {
        const { loanData } = this.state;
        const payload = this.adjustPayloadNumberValues( loanData );

        Object.assign(
            payload,
            { created_date: payload.payment_start_date },
            { employee_id: this.props.employee.data.id },
            { type_id: 3 },
            { subtype: null },
            { payroll_id: this.props.payrollId }
        );

        const valid = this.validateForm();

        if ( valid ) {
            this.props.employee.data.gap_loan_id ? this.props.editPaymentScheduleDetails( payload ) : this.props.generatePaymentScheduleDetails( payload );
        }
    };

       /**
     * Render payment schedule details
     */
    renderPaymentScheduleDetails() {
        return (
            <div>
                <h2 className="sl-c-section-title">
                    <span className="sl-c-circle">2</span>
                    Payment Schedule Details
                </h2>
                <Button
                    label={ this.props.generating ? <Loader /> : 'Generate Payment Schedule Details' }
                    type="neutral"
                    size="large"
                    onClick={ () => { this.generatePaymentScheduleDetails(); } }
                    ref={ ( ref ) => { this.submitButton = ref; } }
                    disabled={ this.props.generating || this.props.submitted }
                />
                <br />
                <br />
                { this.props.amortizations.length ? this.renderPaymentScheduleTable() : null }
            </div>
        );
    }

    /**
     * Render payment schedule table
     */
    renderPaymentScheduleTable() {
        const { payrollId } = this.props;

        const tableColumns = [
            {
                header: 'Payment Date',
                accessor: 'due_date',
                minWidth: 200,
                render: ({ row }) => <div>{formatDate( row.due_date, DATE_FORMATS.DISPLAY )}</div>
            },
            {
                header: 'Amount Due',
                accessor: 'amount_due',
                minWidth: 150,
                render: ({ row }) => <div>P { row.amount_due }</div>
            },
            {
                header: 'Amount Deducted',
                accessor: 'amount_collected',
                minWidth: 200,
                render: ({ row }) => <div>P { row.amount_collected }</div>
            },
            {
                header: 'Remaining Balance',
                accessor: 'amount_remaining',
                minWidth: 200,
                render: ({ row }) => <div>P { row.amount_remaining }</div>
            },
            {
                header: 'Status',
                accessor: 'is_collected',
                minWidth: 150,
                render: ({ row }) => <div>{row.is_collected}</div>
            },
            {
                header: 'Employee Remarks',
                accessor: 'employer_remarks',
                minWidth: 200,
                render: ({ row }) => <div>{row.employer_remarks}</div>
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => {
                    const totalAmount = this.state.amortizations.reduce(
                        ( sum, amortization ) => sum + parseFloat( formatCurrencyToDecimalNotation( amortization.amount_due ) ), 0
                    );

                    return (
                        <Button
                            label="Edit"
                            type="grey"
                            to={ `/payroll/${payrollId}/transaction/edit-amortization` }
                            onClick={ () => {
                                localStorage.setItem(
                                    'gapLoanEditingAmortization',
                                    JSON.stringify({ ...row, total_loan_amount: totalAmount })
                                );
                            } }
                        />
                    );
                }
            }
        ];

        const { amortizations } = this.state;

        const dataForDisplay = amortizations.map( ( amortization ) => ({
            ...amortization,
            is_collected: amortization.is_collected ? 'Deducted' : 'Undeducted',
            employer_remarks: amortization.employer_remarks ? amortization.employer_remarks : ' '
        }) );

        return (
            <div>
                <div className="title">
                    <H5>Payment Schedule Details</H5>
                    <div className="search-wrapper">
                        <Input
                            className="search"
                            id="search"
                            ref={ ( ref ) => { this.searchInput = ref; } }
                            onChange={ this.handleSearch }
                            addon={ {
                                content: <Icon name="search" />,
                                placement: 'right'
                            } }
                        />
                    </div>
                    <span>
                        { this.state.label }
                    </span>
                </div>
                <Table
                    data={ dataForDisplay }
                    columns={ tableColumns }
                    pagination
                    onDataChange={ this.handleTableChanges }
                    ref={ ( ref ) => { this.amortizationsTable = ref; } }
                />
            </div>
        );
    }

    /**
     * renders a loading screen
     */
    renderLoadingScreen( saving = false ) {
        return (
            <MessageWrapper>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        <H3>{ saving ? 'Saving. Please wait...' : 'Loading...' }</H3>
                    </div>
                </div>
            </MessageWrapper>
        );
    }

    renderGapLoanForm() {
        const { employee } = this.props;

        return (
            <MainWrapper isEmployeeListVisible={ this.props.employeeListCollapsed }>
                <Container>
                    <div>
                        <HeadingWrapper>
                            <H3>Update Gap Loan</H3>
                            <p>
                                Gap loan is a feature which will allow you to lend money to
                                employee to meet the minimum take home pay requirement. The
                                amount will be deducted to the employee&apos;s payroll based
                                on the repayment start date.
                            </p>
                        </HeadingWrapper>
                        <h2 className="sl-c-section-title">
                            <span className="sl-c-circle">1</span>
                            Choose a loan type
                        </h2>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="employee_name"
                                    label="Employee name"
                                    value={ `${employee.data.last_name} ${employee.data.first_name} ${employee.data.employee_id}` }
                                    required
                                    disabled
                                />
                            </div>
                        </div>
                        <div>
                            <div className="row">
                                <div className="col-xs-4">
                                    <Input
                                        id="total_amount"
                                        label="Total amount payable"
                                        required
                                        type="number"
                                        key="total_amount"
                                        ref={ ( ref ) => { this.total_amount = ref; } }
                                        onChange={ ( value ) => {
                                            this.updateState( 'total_amount', value );
                                            this.handleLoanTermCalculation();
                                        } }
                                        onBlur={ ( value ) => this.updateState( 'total_amount', formatCurrencyToDecimalNotation( value ) ) }
                                        value={ this.state.loanData.total_amount }
                                        minNumber={ employee.data.gap_loan_total_amount }
                                    />
                                </div>
                                <div className="col-xs-4">
                                    <Input
                                        id="monthly_amortization"
                                        label="Monthly deductions"
                                        required
                                        type="number"
                                        value={ this.state.loanData.monthly_amortization }
                                        ref={ ( ref ) => { this.monthly_amortization = ref; } }
                                        onChange={ ( value ) => {
                                            this.updateState( 'monthly_amortization', value );
                                            this.handleLoanTermCalculation();
                                            this.validateMonthlyAmortizations();
                                        } }
                                        onBlur={ ( value ) => {
                                            this.updateState( 'monthly_amortization', formatCurrencyToDecimalNotation( value ) );
                                            this.validateMonthlyAmortizations();
                                        } }
                                        minNumber={ 1 }
                                    />
                                </div>
                                <div className="col-xs-4">
                                    <SalSelect
                                        id="payment_scheme"
                                        label="Payment scheme"
                                        data={ this.getPaymentSchemeOptions() }
                                        value={ this.state.loanData.payment_scheme }
                                        required
                                        placeholder="Choose a payment scheme"
                                        ref={ ( ref ) => { this.payment_scheme = ref; } }
                                        onChange={ ({ value }) => { this.updateState( 'payment_scheme', value ); } }
                                    />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-4">
                                    <Input
                                        id="term"
                                        label="Loan terms (Months)"
                                        type="number"
                                        required
                                        maxNumber={ 500 }
                                        minNumber={ 1 }
                                        ref={ ( ref ) => { this.term = ref; } }
                                        value={ this.state.loanData.term }
                                        onChange={ ( value ) => {
                                            this.updateState( 'term', value );
                                            this.handleMonthlyDeductionCalculation();
                                            this.validateMonthlyAmortizations();
                                        } }
                                        onBlur={ ( value ) => {
                                            this.updateState(
                                                'term',
                                                value === '' ? '' : Math.floor( value ).toString()
                                            );
                                        } }
                                    />
                                </div>
                                <div className="col-xs-4 date-picker">
                                    <DatePicker
                                        label="Repayment start date"
                                        dayFormat={ DATE_FORMATS.DISPLAY }
                                        required
                                        selectedDay={ this.state.loanData.payment_start_date }
                                        ref={ ( ref ) => { this.payment_start_date = ref; } }
                                        onChange={ ( value ) => {
                                            this.handleCreatedDateChange( value );
                                        } }
                                    />
                                </div>
                                <div className="col-xs-4 date-picker">
                                    <DatePicker
                                        label="Repayment end date"
                                        dayFormat={ DATE_FORMATS.DISPLAY }
                                        disabled
                                        placeholder=""
                                        selectedDay={ this.state.loanData.payment_end_date }
                                        ref={ ( ref ) => { this.payment_end_date = ref; } }
                                    />
                                </div>
                            </div>
                        </div>
                        <div>
                            { this.renderPaymentScheduleDetails() }
                            <br />
                        </div>
                    </div>
                    <div className="footer">
                        <Container>
                            <Button
                                label={ this.props.submitted ? <Loader /> : 'Finish and Go To Next' }
                                type="neutral"
                                size="large"
                                disabled={ this.props.submitted || this.props.generating || this.props.amortizations.length === 0 }
                                onClick={ () => { this.submitLoan(); } }
                                ref={ ( ref ) => { this.submitButton = ref; } }
                            />
                        </Container>
                    </div>
                </Container>
            </MainWrapper>
        );
    }

    render() {
        const { loading, saving } = this.props.employee;

        return loading || saving ? this.renderLoadingScreen( saving ) : this.renderGapLoanForm();
    }
}

const mapStateToProps = createStructuredSelector({
    formOptions: makeSelectFormOptions(),
    generating: makeSelectGenerating(),
    employee: makeSelectEmployee(),
    amortizations: makeSelectAmortizations(),
    loan: makeSelectLoan(),
    submitted: makeSelectSubmitted()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( GapLoanView );
