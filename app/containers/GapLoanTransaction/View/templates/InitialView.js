import React from 'react';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import { H1, H3 } from '../../../../components/Typography';

import * as actions from '../actions';

import { makeSelectGapLoanCadidates } from '../selectors';

import { InitialViewWrapper, MessageWrapper } from './styles';

/**
 * Initial message
 */
class InitialView extends React.Component {
    static propTypes = {
        finishedEditing: React.PropTypes.bool,
        gapLoanCandidates: React.PropTypes.object
    }

    renderLoadingScreen = () => (
        <MessageWrapper>
            <div>
                <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                <div className="description">
                    <H3>Loading...</H3>
                </div>
            </div>
        </MessageWrapper>
    )

    renderInitialView = () => (
        <InitialViewWrapper>
            <div>
                <H1>Gap Loan Transactions</H1>
                { !this.props.finishedEditing ? (
                    <ol>
                        <li>Get started by selecting an employee</li>
                        <li>On the loan details page, specify the total amounts payable, monthly deductions, payment scheme, loan terms in onths and repayment start date.</li>
                        <li>Click the &quot;Generate Payment Schedule Details&quot; to compute the loan amortization schedule</li>
                        <li>Review the loan amortization schedule and if everything is correct, you may click the &quot;Finish and go to next&quot; button to process a gap loan for another employee</li>
                        <li>After creating loans to all gap loan candidates, click the &quot;Finish and Regenerate Payroll&quot; to reprocess the payroll</li>
                    </ol>
                ) : (
                    <p>All gap loan candidates have been resolved. You may now click the finish button to re-calculate your payroll.</p>
                ) }
            </div>
        </InitialViewWrapper>
    )

    render() {
        if ( this.props.gapLoanCandidates.loading ) {
            return this.renderLoadingScreen();
        }
        return this.renderInitialView();
    }
}

const mapStateToProps = createStructuredSelector({
    gapLoanCandidates: makeSelectGapLoanCadidates()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( InitialView );
