import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import moment from 'moment';

import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { browserHistory } from '../../../utils/BrowserHistory';
import { formatCurrencyToDecimalNotation } from '../../../utils/functions';
import { DATE_FORMATS } from '../../../utils/constants';

import {
    NOTIFICATION_SAGA,
    NOTIFICATION,
    SET_GAP_LOAN_CANDIDATES,
    GET_GAP_LOAN_CANDIDATES,
    SET_GAP_LOAN_CANDIDATES_LOADING,
    SET_EMPLOYEE,
    SET_EMPLOYEE_ERRORS,
    SET_EMPLOYEE_LOADING,
    GET_EMPLOYEE,
    SET_FORM_OPTIONS,
    SET_AMORTIZATION_SUBMITTED,
    UPDATE_AMORTIZATION_PREVIEW,
    SET_GENERATED_PAYMENT_SCHEDULE_DETAILS,
    GENERATE_PAYMENT_SCHEDULE_DETAILS,
    SET_LOAN,
    GENERATING,
    SUBMITTED,
    SET_ERRORS,
    SUBMIT_GAP_LOAN,
    EDIT_LOAN,
    EDIT_PAYMENT_SCHEDULE_DETAILS,
    FINISH_GAP_LOAN_TRANSACTION,
    SET_FINISHING,
    SUBMIT_ERROR
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Fetch gap loan candidates from the server
 * @param payrollId
 */
export function* getGapLoanCandidates({ payload }) {
    try {
        yield put({
            type: SET_GAP_LOAN_CANDIDATES_LOADING,
            payload: true
        });

        const payroll = yield call( Fetch, `/payroll/${payload.payrollId}`, { method: 'GET' });
        const gapLoanCandidates = yield call( Fetch, `/payroll/${payload.payrollId}/get_gap_loan_candidates`, { method: 'GET' });
        yield put({
            type: SET_GAP_LOAN_CANDIDATES,
            payload: gapLoanCandidates
        });

        const formOptions = {};
        const payrollGroups = yield call( Fetch, '/account/payroll_groups', { method: 'GET' });

        formOptions.payrollGroups = payrollGroups.data;
        formOptions.enforceGapLoans = payrollGroups.data.find( ( group ) => group.id === payroll.payroll_group_id ).enforce_gap_loans;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_GAP_LOAN_CANDIDATES_LOADING,
            payload: false
        });
    }
}

/**
 * Fetch employee profile by id
 */
export function* getEmployeeData({ payload }) {
    try {
        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                title: ' ',
                message: ' ',
                show: false,
                type: payload.type
            }
        });

        const { employee } = payload;

        yield put({ type: SET_EMPLOYEE_LOADING, payload: true });
        yield put({ type: SET_EMPLOYEE_ERRORS, payload: null });

        if ( employee.gap_loan_id ) {
            const loan = yield call( Fetch, `/payroll_loan/${employee.gap_loan_id}/initial_preview` );
            yield [
                put({ type: SET_LOAN, payload: loan }),
                put({ type: SET_GENERATED_PAYMENT_SCHEDULE_DETAILS, payload: loan.amortizations })
            ];
        } else {
            yield [
                put({ type: SET_LOAN,
                    payload: {
                        total_amount: formatCurrencyToDecimalNotation( employee.gap_loan_total_amount ),
                        payment_start_date: moment().add( 1, 'day' ).format( DATE_FORMATS.API )
                    }
                }),
                put({ type: SET_GENERATED_PAYMENT_SCHEDULE_DETAILS, payload: []})
            ];
        }

        yield put({
            type: SET_EMPLOYEE,
            payload: employee
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING,
            payload: false
        });
    }
}

/**
 * Update Amortization Preview
 */
export function* updateAmortizationPreview({ payload }) {
    try {
        yield put({
            type: SET_AMORTIZATION_SUBMITTED,
            payload: true
        });

        const id = payload.loanUid ? payload.loanUid : payload.loan_id;
        const payrollId = payload.payrollId;
        const companyId = company.getLastActiveCompanyId();
        const payloadWithCompanyId = { ...payload, company_id: companyId };

        const amortizations = yield call(
            Fetch,
            `/payroll_loan/${id}/update_amortization_preview`,
            { method: 'POST', data: payloadWithCompanyId }
        );
        yield put({ type: SET_GENERATED_PAYMENT_SCHEDULE_DETAILS, payload: amortizations });
        yield call( browserHistory.push, `/payroll/${payrollId}/transaction` );
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_AMORTIZATION_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Submit gap loan
 * @param payload
 */
export function* submitGapLoan({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];
        // Transform data to remove null keys
        const data = { ...payload };
        const keys = Object.keys( payload );

        keys.forEach( ( key ) => {
            if ( payload[ key ] === null ) {
                delete data[ key ];
            }
        });

        data.company_id = company.getLastActiveCompanyId();

        yield call( Fetch, `/payroll_loan/create/${data.uid}`, { method: 'POST', data });
        yield call( getGapLoanCandidates, { payload: { payrollId: payload.payroll_id }});
        yield put({ type: SUBMIT_ERROR, payload: false });
        yield call( delay, 500 );
    } catch ( error ) {
        yield put({ type: SUBMIT_ERROR, payload: true });
        if ( error.response && error.response.status === 406 && !error.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Edit loan
 */
export function* editLoan({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        // Transform data to remove null keys
        const data = { ...payload };
        const keys = Object.keys( payload );

        keys.forEach( ( key ) => {
            if ( payload[ key ] === null ) {
                delete data[ key ];
            }
        });

        const loanId = data.id;
        const companyId = company.getLastActiveCompanyId();
        const payloadWithCompanyId = { ...data, company_id: companyId };

        yield call( Fetch, `/payroll_loan/${loanId}`, { method: 'POST', data: payloadWithCompanyId });
        yield call( resetStore );
        yield put({ type: SUBMIT_ERROR, payload: false });
    } catch ( error ) {
        yield put({ type: SUBMIT_ERROR, payload: true });
        if ( error.response && error.response.status === 406 && !error.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Generate payment schedule details
 * @param payload
 */
export function* generatePaymentScheduleDetails({ payload }) {
    try {
        yield [
            put({ type: GENERATING, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];
        // Transform data to remove null keys
        const data = { ...payload };
        const keys = Object.keys( payload );

        keys.forEach( ( key ) => {
            if ( payload[ key ] === null ) {
                delete data[ key ];
            }
        });

        data.company_id = company.getLastActiveCompanyId();

        const loan = yield call( Fetch, '/payroll_loan', { method: 'POST', data });
        Object.assign( loan, { id: data.id });

        yield [
            put({ type: SET_GENERATED_PAYMENT_SCHEDULE_DETAILS, payload: loan.amortizations }),
            put({ type: SET_LOAN, payload: loan })
        ];
    } catch ( error ) {
        if ( error.response && error.response.data && error.response.data.message ) {
            yield call( notifyUser, error );
        } else {
            yield call( setErrors, error.response.data );
        }
    } finally {
        yield put({
            type: GENERATING,
            payload: false
        });
    }
}

/**
 * Edit payment schedule details
 */
export function* editPaymentScheduleDetails({ payload }) {
    try {
        yield [
            put({ type: GENERATING, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        // Transform data to ronemove null keys
        const data = { ...payload };
        const keys = Object.keys( payload );

        keys.forEach( ( key ) => {
            if ( payload[ key ] === null ) {
                delete data[ key ];
            }
        });

        const companyId = company.getLastActiveCompanyId();
        const loanId = payload.id;
        const payloadWithCompanyId = { ...data, company_id: companyId };

        const loan = yield call( Fetch, `/payroll_loan/${loanId}/update_loan_preview`, { method: 'POST', data: payloadWithCompanyId });
        yield [
            put({ type: SET_GENERATED_PAYMENT_SCHEDULE_DETAILS, payload: loan.amortizations }),
            put({ type: SET_LOAN, payload: loan })
        ];
    } catch ( error ) {
        if ( error.response && error.response.data && error.response.data.message ) {
            yield call( notifyUser, error );
        } else {
            yield call( setErrors, error.response.data );
        }
    } finally {
        yield put({
            type: GENERATING,
            payload: false
        });
    }
}

/**
 * Sends request to Finish Gap Loan Transaction
 * @param {Integer} payload - Payroll ID
 */
export function* finishGapLoan({ payload }) {
    try {
        yield put({
            type: SET_FINISHING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, `/company/${companyId}/activate_gap_loans`, {
            method: 'POST',
            data: {
                payroll_id: payload
            }
        });

        yield call( delay, 2000 );

        yield call( Fetch, `/payroll/${payload}/recalculate`, {
            method: 'POST',
            data: {
                saveResultToParent: true
            }
        });

        yield call( delay, 1000 );
        yield call( browserHistory.push, `/payroll/${payload}` );
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_FINISHING,
            payload: false
        });
    }
}

/**
 * converts api error to notification payload
 */
function toErrorPayload( error ) {
    return {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getGapLoanCandidates );
}

/**
 * Individual exports for testing
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for edit loan
 */
export function* watchForEditLoan() {
    const watcher = yield takeLatest( EDIT_LOAN, editLoan );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetGapLoanCandidates() {
    const watcher = yield takeEvery( GET_GAP_LOAN_CANDIDATES, getGapLoanCandidates );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE
 */
export function* watchForGetEmployeeData() {
    yield takeLatest( GET_EMPLOYEE, getEmployeeData );
}

/**
 * Watch for UPDATE_AMORTIZATION_PREVIEW
 */
export function* watchForUpdateAmortizationPreview() {
    const watcher = yield takeEvery( UPDATE_AMORTIZATION_PREVIEW, updateAmortizationPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for update loan preview
 */
export function* watchForGeneratePaymentScheduleDetails() {
    const watcher = yield takeLatest( GENERATE_PAYMENT_SCHEDULE_DETAILS, generatePaymentScheduleDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for update loan preview
 */
export function* watchForEditPaymentScheduleDetails() {
    const watcher = yield takeLatest( EDIT_PAYMENT_SCHEDULE_DETAILS, editPaymentScheduleDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitGapLoan() {
    const watcher = yield takeEvery( SUBMIT_GAP_LOAN, submitGapLoan );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for FINISH_GAP_LOAN_TRANSACTION
 */
export function* watchForFinishGapLoan() {
    const watcher = yield takeEvery( FINISH_GAP_LOAN_TRANSACTION, finishGapLoan );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForGetGapLoanCandidates,
    watchForUpdateAmortizationPreview,
    watchForGetEmployeeData,
    watchForGeneratePaymentScheduleDetails,
    watchForSubmitGapLoan,
    watchForEditLoan,
    watchForEditPaymentScheduleDetails,
    watchForFinishGapLoan
];
