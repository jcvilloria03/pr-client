import { createSelector } from 'reselect';

/**
 * Direct selector to the userProfile state domain
 */
const selectUserProfileDomain = () => ( state ) => state.get( 'userProfile' );

const makeSelectLoading = () => createSelector(
    selectUserProfileDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectUserProfile = () => createSelector(
    selectUserProfileDomain(),
  ( substate ) => substate.get( 'userDetails' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectUserProfileDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectUserProfile,
  makeSelectNotification
};
