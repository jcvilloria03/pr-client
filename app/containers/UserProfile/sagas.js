/* eslint-disable require-jsdoc */
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { call, cancel, put, take } from 'redux-saga/effects';
import get from 'lodash/get';

import { Fetch } from 'utils/request';

import { REINITIALIZE_PAGE } from '../App/constants';
import { resetStore } from '../App/sagas';

import {
  setLoading,
  setUserDetails,
  setNotification
} from './actions';

import {
  NOTIFICATION,
  GET_USER_DETAILS
} from './constants';

/**
 * get user details
 *
 */
export function* getUserDetails() {
    try {
        yield put( setLoading( true ) );

        const userInfo = yield call( Fetch, '/user/informations', { method: 'POST' });
        const userData = yield call( Fetch, `/user/${userInfo.id}`, { method: 'GET' });

        yield put( setUserDetails( userData ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getUserDetails );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetUserDetails() {
    const watcher = yield takeLatest( GET_USER_DETAILS, getUserDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForGetUserDetails,
    watchForReinitializePage
];
