import { fromJS } from 'immutable';
import {
  SET_LOADING,
  SET_NOTIFICATION,
  SET_USER_DETAILS
} from './constants';

const initialState = fromJS({
    loading: false,
    userDetails: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * UserProfile reducer
 *
 */
function userProfileReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_USER_DETAILS:
            return state.set( 'userDetails', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default userProfileReducer;
