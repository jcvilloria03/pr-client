/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/prop-types */
import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { Row, Col } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import Button from 'components/Button';
import SnackBar from 'components/SnackBar';
import { H2, H3 } from 'components/Typography';

import * as userProfileActions from './actions';

import { PRODUCT_SEATS_LABELS } from './constants';

import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectUserProfile
} from './selectors';

import {
    UserTitle,
    HeaderTitle,
    SectionTitle,
    LoadingStyles,
    HeaderSubTitle,
    StyledContainer,
    SectionSubTitle
} from './styles';

/**  ,
 *
 * UserProfile
 *
 */
export class UserProfile extends React.Component {
    static propTypes = {
        profile: React.PropTypes.object,
        getUserDetails: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    componentDidMount() {
        this.props.getUserDetails();
    }

    /**
       * Formats company seats names.
       *
       * @param {Array} companySeats
       * @returns {String}
       */
    formatCompanySeatsNames = ( companySeats ) => companySeats && companySeats
        .map( ( seat ) => PRODUCT_SEATS_LABELS[ seat.name ])
        .join( ', ' )

    render() {
        const {
          loading,
          notification,
          profile
        } = this.props;

        return (
            <div>
                <Helmet
                    title="User Profile"
                    meta={ [
                        { name: 'description', content: 'Description of User Profile' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <StyledContainer>
                    { loading
                      ? (
                          <div className="loader">
                              <LoadingStyles>
                                  <H2>Loading User Profile</H2>
                                  <br />
                                  <H3>Please wait...</H3>
                              </LoadingStyles>
                          </div>
                        )
                      : ( <div>

                          <div>
                              <HeaderTitle>User Details</HeaderTitle>
                              <HeaderSubTitle>You may review the user's details in this page.</HeaderSubTitle>
                          </div>

                          <div
                              style={ {
                                  display: 'flex',
                                  justifyContent: 'space-between',
                                  alignItems: 'center'
                              } }
                          >
                              <div>
                                  <UserTitle>{ profile.full_name }</UserTitle>
                              </div>
                              <div
                                  style={ {
                                      display: 'flex',
                                      alignItems: 'center'
                                  } }
                              >
                                  <Button
                                      label="Change Password"
                                      type="neutral"
                                      onClick={ () =>
                                        this.props.router.push(
                                            '/change-password'
                                        )
                                    }
                                  />
                              </div>

                          </div>
                          <div>
                              <Row>
                                  <Col xs={ 4 }>
                                      <SectionTitle>User Details</SectionTitle>
                                  </Col>
                              </Row>
                              <Row>
                                  <Col xs={ 4 }>
                                      <SectionSubTitle>User Role:</SectionSubTitle>
                                      <SectionSubTitle>{ profile.authz_role && profile.authz_role.name }</SectionSubTitle>
                                  </Col>
                                  <Col xs={ 4 }>
                                      <SectionSubTitle>Email:</SectionSubTitle>
                                      <SectionSubTitle>{ profile.email }</SectionSubTitle>
                                  </Col>
                              </Row>
                              <Row>
                                  <Col xs={ 4 }>
                                      <SectionTitle>Assigned Subscription License</SectionTitle>
                                  </Col>
                              </Row>
                              <Row>
                                  <Col xs={ 4 }>
                                      <SectionSubTitle>{ this.formatCompanySeatsNames( profile.product_seats ) }</SectionSubTitle>
                                  </Col>
                              </Row>
                              <Row>
                                  <Col xs={ 4 }>
                                      <SectionTitle>Linked Employee ID</SectionTitle>
                                  </Col>
                              </Row>
                              <Row>
                                  <Col xs={ 4 }>
                                      <SectionSubTitle>Employee ID:</SectionSubTitle>
                                      <SectionSubTitle>{ profile.employee && profile.employee.employee_id }</SectionSubTitle>
                                  </Col>
                                  <Col xs={ 4 }>
                                      <SectionSubTitle>Company Name:</SectionSubTitle>
                                      <SectionSubTitle>{ profile.employee && profile.employee.company_name }</SectionSubTitle>
                                  </Col>
                              </Row>
                          </div>
                      </div>
                    )}
                </StyledContainer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    profile: makeSelectUserProfile()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        userProfileActions,
        dispatch
    );
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( UserProfile );
