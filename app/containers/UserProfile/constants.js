/*
 *
 * UserProfile constants
 *
 */
const namespace = '/app/UserProfile';

export const LOADING = `${namespace}/LOADING`;
export const SET_LOADING = `${namespace}/SET_LOADING`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const GET_USER_DETAILS = `${namespace}/GET_USER_DETAILS`;
export const SET_USER_DETAILS = `${namespace}/SET_USER_DETAILS`;

export const PRODUCT_SEATS_IDS = {
    HRIS: 1,
    TIME_AND_ATTENDANCE: 2,
    PAYROLL: 3,
    TIME_AND_ATTENDANCE_PLUS_PAYROLL: 4
};

export const PRODUCT_SEATS = {
    HRIS: 'hris',
    TIME_AND_ATTENDANCE: 'time and attendance',
    PAYROLL: 'payroll',
    TIME_AND_ATTENDANCE_PLUS_PAYROLL: 'time and attendance plus payroll'
};

export const PRODUCT_SEATS_LABELS = {
    [ PRODUCT_SEATS.HRIS ]: 'HRIS',
    [ PRODUCT_SEATS.TIME_AND_ATTENDANCE ]: 'Time & Attendance',
    [ PRODUCT_SEATS.PAYROLL ]: 'Payroll',
    [ PRODUCT_SEATS.TIME_AND_ATTENDANCE_PLUS_PAYROLL ]: 'Time & Attendance + Payroll'
};
