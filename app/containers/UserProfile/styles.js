import styled from 'styled-components';
import { Container } from 'reactstrap';

export const StyledContainer = styled( Container )`
    min-height: calc(100vh - 80px);
    background: #fff;
    padding: 120px 0 20px 0;
`;

export const ActionContainer = styled( Container )`
    height: 100%;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    padding: 0;
`;

export const HeaderTitle = styled.h1`
    font-size: 36px;
    font-weight: bold;
    margin: 6px auto;
    text-align: center;
`;

export const HeaderSubTitle = styled.p`
    font-size: 14px;
    text-align: center;
    margin-top: 0;
    margin-bottom: 60px;
`;

export const UserTitle = styled.h4`
    font-size: 20px;
    font-weight: bold;
    text-align: left;
    text-transform: uppercase;
`;

export const SectionTitle = styled.h6`
    font-size: 16px;
    text-align: left;
    margin-top: 30px;
    font-weight: 600;
`;

export const SectionSubTitle = styled.p`
    font-size: 14px;
    text-align: left;
    margin-top: 0;
    margin-bottom: 0;
    padding-left: 8px;
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;
