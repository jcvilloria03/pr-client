/* eslint-disable require-jsdoc */
import {
  SET_LOADING,
  SET_NOTIFICATION,
  GET_USER_DETAILS,
  SET_USER_DETAILS
} from './constants';

/**
 *
 * User Profile actions
 *
 */

/**
 * Sets page loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object}
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets notification
 * @param {Object} payload - Notification config
 * @returns {Object}
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

export function getUserDetails() {
    return {
        type: GET_USER_DETAILS
    };
}

export function setUserDetails( payload ) {
    return {
        type: SET_USER_DETAILS,
        payload
    };
}
