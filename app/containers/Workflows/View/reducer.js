import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, DELETE_LOADING, LOADING, SET_NOTIFICATION, SET_PAGINATION, SET_WORKFLOWS
} from './constants';

const initialState = fromJS({
    workflow: {},
    loading: true,
    deleteLoading: false,
    pagination: {
        count: 0,
        current_page: 0,
        links: [],
        per_page: 0,
        total: 0,
        total_pages: 0
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Workflows reducer
 *
 */
function workflowsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_WORKFLOWS:
            return state.set( 'workflow', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        case DELETE_LOADING:
            return state.set( 'deleteLoading', action.payload );
        default:
            return state;
    }
}

export default workflowsReducer;
