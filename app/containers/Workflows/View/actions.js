import {
    DEFAULT_ACTION, DELETE_WORKFLOWS, GET_WORKFLOWS
} from './constants';

/**
 *
 * Workflows actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
/**
 *
 * Get workflows rules actions
 *
 */
export function getWorkflows( payload ) {
    return {
        type: GET_WORKFLOWS,
        payload
    };
}
/**
 *
 * delete workflows actions
 */
export function deleteWorkflows( payload ) {
    return {
        type: DELETE_WORKFLOWS,
        payload
    };
}
