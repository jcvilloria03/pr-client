import { get } from 'lodash';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery } from 'redux-saga';
import { call, cancel, put, take } from 'redux-saga/effects';

import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { DELETE_LOADING, DELETE_WORKFLOWS, GET_WORKFLOWS, LOADING, NOTIFICATION, SET_NOTIFICATION, SET_PAGINATION, SET_WORKFLOWS } from './constants';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}
/**
 *  get Workflows
 */
export function* getWorkflows({ payload }) {
    try {
        yield put({
            type: DELETE_LOADING,
            payload: true
        });
        const { page, perPage, sortBy, sortOrder, term } = payload;
        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/workflows?term=${term}&page=${page}&per_page=${perPage}&sort_by=${sortBy}&sort_order=${sortOrder}`, { method: 'GET' });
        yield put({ type: SET_WORKFLOWS, payload: res });
        yield put({ type: SET_PAGINATION, payload: res.meta.pagination });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: DELETE_LOADING,
            payload: false
        });
        yield put({
            type: LOADING,
            payload: false
        });
    }
}
/**
 *  delete workflows
 */
export function* deleteWorkflows({ payload }) {
    try {
        const isUse = yield call( Fetch, '/workflow/check_in_use', { method: 'POST', data: payload });
        if ( isUse && isUse.in_use === 0 ) {
            const formData = new FormData();

            formData.append( '_method', 'DELETE' );
            formData.append( 'company_id', payload.company_id );
            payload.workflow_ids.forEach( ( id ) => {
                formData.append( 'workflows_ids[]', id );
            });
            yield call( Fetch, '/workflow/bulk_delete', {
                method: 'POST',
                data: formData
            });
            yield call( notifyUser, {
                title: 'Success',
                message: 'Record successfully deleted',
                show: true,
                type: 'success'
            });
        } else {
            yield call( notifyError, {
                show: true,
                title: 'Error',
                message: "Cannot read properties of undefined reading 'status'",
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
    }
}
/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}
/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}
/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for Announcement
 */
export function* watchForWorkflows() {
    const watcher = yield takeEvery( GET_WORKFLOWS, getWorkflows );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for Announcement
 */
export function* watchFordeleteWorkflows() {
    const watcher = yield takeEvery( DELETE_WORKFLOWS, deleteWorkflows );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
// All sagas to be loaded
export default [
    defaultSaga,
    watchForWorkflows,
    watchFordeleteWorkflows,
    watchNotify
];
