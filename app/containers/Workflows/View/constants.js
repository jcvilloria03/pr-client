/*
 *
 * Workflows constants
 *
 */
const namespace = 'app/Workflows/View';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;

export const SET_WORKFLOWS = `${namespace}/SET_WORKFLOWS`;
export const GET_WORKFLOWS = `${namespace}/GET_WORKFLOWS`;

export const SET_PAGINATION = `${namespace}/SET_PAGINATION`;

export const DELETE_WORKFLOWS = `${namespace}/DELETE_WORKFLOWS`;

export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const LOADING = `${namespace}/LOADING`;
export const DELETE_LOADING = `${namespace}/DELETE_LOADING`;
