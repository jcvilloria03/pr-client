import { debounce } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { LoadingStyles, PageWrapper } from './styles';
import Sidebar from '../../../components/Sidebar';
import AsyncTable from '../../../components/AsyncTable';
import Button from '../../../components/Button';
import SalDropdown from '../../../components/SalDropdown';
import Modal from '../../../components/Modal';
import Search from '../../../components/Search';
import { getIdsOfSelectedRows } from '../../../components/Table/helpers';
import SnackBar from '../../../components/SnackBar';
import { H2, H3, H1, H5, P } from '../../../components/Typography';
import FooterTablePaginationV2 from '../../../components/FooterTablePagination/FooterTablePaginationV2';

import { company } from '../../../utils/CompanyService';
import { formatDeleteLabel, formatPaginationLabel } from '../../../utils/functions';
import { getCompanySettingsSidebarLinks } from '../../../utils/sidebarHelper';
import { browserHistory } from '../../../utils/BrowserHistory';

import makeSelectWorkflows, {
    makeSelectGetWorkflows,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectPagination,
    makeSelectTableLoading
} from './selectors';
import * as workflowsAction from './actions';

/**
 *
 * Workflows
 *
 */
export class Workflows extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        pagination: React.PropTypes.object,
        workflow: React.PropTypes.object,
        getWorkflows: React.PropTypes.func,
        deleteWorkflows: React.PropTypes.func,
        loading: React.PropTypes.bool,
        tableLoad: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    static defaultProps = {
        loading: true
    };
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            selectLabel: '',
            showLabel: false,
            sortBy: 'name',
            workflowsList: [],
            sortOrder: 'desc',
            term: '',
            page: 0
        };
        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.handleSearch = this.handleSearch.bind( this );
    }
    componentDidMount() {
        this.props.getWorkflows({
            page: 1,
            perPage: 10,
            sortBy: this.state.sortBy,
            sortOrder: this.state.sortOrder,
            term: this.state.term
        });
    }
    componentWillReceiveProps( nextProps ) {
        if ( nextProps.workflow.data !== this.props.workflow.data ) {
            this.setState({
                workflowsList: nextProps.workflow.data,
                label: formatPaginationLabel({
                    page: nextProps.pagination.current_page - 1,
                    pageSize: nextProps.pagination.per_page,
                    dataLength: nextProps.pagination.total
                })
            });
        }
    }
    onPageChange( data ) {
        this.props.getWorkflows({
            page: data,
            perPage: this.props.pagination.per_page,
            sortBy: this.state.sortBy,
            sortOrder: this.state.sortOrder,
            term: this.state.term
        });
        this.workflowsTable.setState({ selected: [], selectedRows: []});
        this.setState({ showLabel: false }, );
        setTimeout( () => this.handleTableChanges(), 2000 );
    }
    onPageSizeChange( data ) {
        if ( !data ) {
            return;
        }
        this.props.getWorkflows({
            page: 1,
            perPage: data,
            sortBy: this.state.sortBy,
            sortOrder: this.state.sortOrder,
            term: this.state.term
        });
        this.workflowsTable.setState({ selected: [], selectedRows: []});
        this.setState({ showLabel: false }, );
        setTimeout( () => this.handleTableChanges(), 2000 );
    }
    getDropdownItems() {
        return [
            {
                children: <div>Delete</div>,
                onClick: () => this.DeleteModel.toggle()
            }
        ];
    }
    handleTableChanges( tableProps = this.workflowsTable.tableComponent.state ) {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });
        this.setState({
            label: formatPaginationLabel( tableProps ),
            selectLabel: formatDeleteLabel()
        });
    }
    AffectedEntityListdata = ( row ) => {
        if ( !row || row === '' ) {
            return '';
        }
        if ( row.length === 1 ) {
            return row[ 0 ];
        }
        return row.length > 2 ? `${row[ 0 ]} and ${row.length - 1} more` : `${row[ 0 ]} and ${row[ 1 ]}`;
    }
    handleDelete = () => {
        const ids = getIdsOfSelectedRows( this.workflowsTable );
        this.setState({
            showLabel: false
        });
        this.workflowsTable.setState({
            selected: [], selectedRows: []
        });
        this.props.deleteWorkflows( ({ workflow_ids: ids, company_id: company.getLastActiveCompanyId() }) );
        this.DeleteModel.toggle();
        setTimeout( async () => {
            await this.props.getWorkflows({
                page: 1,
                perPage: 10,
                sortBy: this.state.sortBy,
                sortOrder: this.state.sortOrder,
                term: this.state.term
            });
        }, 5000 );
    }
    handleSearch =async ( term ) => {
        if ( term !== this.state.term ) {
            this.setState({ term });
            this.props.getWorkflows({
                page: 1,
                perPage: 10,
                sortBy: this.state.sortBy,
                sortOrder: this.state.sortOrder,
                term
            });
        } else {
            this.setState({ term });
            this.props.getWorkflows({
                page: 1,
                perPage: 10,
                sortBy: this.state.sortBy,
                sortOrder: this.state.sortOrder,
                term: this.state.term
            });
        }
    }

    tableColumns() {
        const colStyles = {
            fontSize: '15px', display: 'flex', alignItems: 'center', flex: 1, justifyContent: 'flex-start'
        };

        return [
            {
                id: 'name',
                header: 'Workflow Name ',
                width: 180,
                style: { ...colStyles },
                sortable: false,
                render: ({ row }) => ( <P noBottomMargin>{row.name}</P> )
            },
            {
                id: 'levels',
                header: 'Approval Levels ',
                width: 150,
                style: { ...colStyles },
                sortable: false,
                render: ({ row }) => ( <P noBottomMargin>
                    { row.levels && row.levels.map( ( val ) => ( val ) ).length === 1 ? `${row.levels.length} level` : `${row.levels.length} levels` }
                </P> )
            },
            {
                id: 'approvers',
                header: 'Approvers',
                minWidth: 250,
                style: { ...colStyles },
                sortable: false,
                render: ({ row }) => ( <P noBottomMargin>
                    { this.showAssignedEmployees( row.levels ) }
                </P> )
            },
            {
                id: 'id',
                header: ' ',
                minWidth: 70,
                accessor: 'actions',
                sortable: false,
                style: { ...colStyles, justifyContent: 'flex-end' },
                render: ({ row }) => (
                    <Button
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => browserHistory.push( `/company-settings/workflow-automation/workflows/${row.id}/edit`, true ) }
                    />
                )
            }
        ];
    }
    formatList( list ) {
        if ( !list || list === undefined ) return '';

        return this.AffectedEntityListdata( list.map( ( item ) => ( item ? item.name : '' ) ) );
    }
    showAssignedEmployees( levels ) {
        const approvers = levels.reduce( ( approverss, level ) => {
            approverss.push(
              ...level.approvers.map( ( approver ) => approver.affected_entity )
            );
            return approverss;
        }, []);
        return this.formatList( approvers );
    }
    /**
     *
     * Workflows render method
     *
     */
    render() {
        const { workflowsList, label, selectLabel, showLabel } = this.state;
        const { loading, tableLoad, pagination } = this.props;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const footerTablePagination = {
            total: pagination.total || 0,
            per_page: pagination.per_page || 10,
            current_page: pagination.current_page || 1,
            last_page: pagination.total_pages || 1,
            to: 0
        };

        return (
            <div>
                <Helmet
                    title="Workflows"
                    meta={ [
                        { name: 'description', content: 'Description of Workflows' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Confirm Your Action"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.DeleteModel = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.DeleteModel.toggle()
                        },
                        {
                            type: 'darkRed',
                            label: 'Yes',
                            onClick: this.handleDelete
                        }
                    ] }
                />
                <PageWrapper>
                    <div className="loader" style={ { display: loading ? '' : 'none' } }>
                        <LoadingStyles>
                            <H2>Loading Workflows.</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    </div>
                    <Container style={ { display: loading ? 'none' : '' } }>
                        <div className="content">
                            <div className="contentData">
                                <H1 noBottomMargin>Workflows</H1>
                            </div>
                            <div>
                                <div className="workflows_section">
                                    <div className="workflows_input">
                                        <H5 noBottomMargin>Workflows List</H5>

                                        <Search
                                            ref={ ( ref ) => { this.search = ref; } }
                                            handleSearch={ debounce( this.handleSearch, 1000 ) }
                                        />
                                    </div>
                                    <div className="actions-wrapper">
                                        <P noBottomMargin>
                                            {showLabel ? selectLabel : label}
                                        </P>

                                        <span style={ { marginLeft: '14px' } }>
                                            {showLabel ? (
                                                <SalDropdown dropdownItems={ this.getDropdownItems() } />
                                            ) : (
                                                <Button
                                                    label="Add Workflow"
                                                    type="action"
                                                    onClick={ () => browserHistory.push( '/company-settings/workflow-automation/workflows/add', true ) }
                                                />
                                            )}
                                        </span>
                                    </div>
                                </div>

                                <div>
                                    <AsyncTable
                                        data={ workflowsList }
                                        columns={ this.tableColumns() }
                                        selectable
                                        loading={ tableLoad }
                                        ref={ ( ref ) => { this.workflowsTable = ref; } }
                                        onDataChange={ this.handleTableChanges }
                                        onPageChange={ ( data ) => {
                                            this.onPageChange( data );
                                        } }
                                        onPageSizeChange={ ( data ) => {
                                            this.onPageSizeChange( data );
                                        } }
                                        sizeOptions={ [ 10, 25, 50, 100 ] }
                                        tablePage={ this.props.pagination.current_page - 1 || 0 }
                                        pageSize={ this.props.pagination.per_page || 10 }
                                        pages={ this.props.pagination.total_pages }
                                        onSelectionChange={ ({ selected }) => {
                                            const selectionLength = selected.filter( ( row ) => row ).length;
                                            this.setState({
                                                showLabel: selectionLength > 0,
                                                selectLabel: formatDeleteLabel( selectionLength )
                                            });
                                        } }
                                    />

                                    <FooterTablePaginationV2
                                        page={ this.props.pagination.current_page || 1 }
                                        pageSize={ this.props.pagination.per_page || 10 }
                                        pagination={ footerTablePagination }
                                        onPageChange={ ( data ) => {
                                            this.onPageChange( data );
                                        } }
                                        onPageSizeChange={ ( data ) => {
                                            this.onPageSizeChange( data );
                                        } }
                                        paginationLabel={ this.state.label }
                                        fluid
                                    />
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    Workflows: makeSelectWorkflows(),
    workflow: makeSelectGetWorkflows(),
    pagination: makeSelectPagination(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    tableLoad: makeSelectTableLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        workflowsAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Workflows );
