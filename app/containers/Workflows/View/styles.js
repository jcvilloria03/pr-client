import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh ;
    padding: 0;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
    overflow: hidden;
`;

export const PageWrapper = styled.div`
    padding-left: 91px;
    display: flex;
    flex-direction: column;
    align-items: center;

    .content {
        margin: 120px 0;
        width: 80vw;

        .contentData {
            text-align: center;
            color: #474747;
            margin-bottom: 4rem;
            margin-top: -1.5rem;

            h1 {
                font-weight: 700;
                font-size: 36px;
                line-height: 57.6px;
            }
        }

        .workflow_createtitle {
            text-align: center;

            .workflow_img {
                width: 9rem;
                height: 9rem;
                color: #fff;
                background-color: #00a5e5;
                border-radius: 50%;
                margin:auto;

                span {
                    display: flex;
                    align-items: center;
                    height: 100%;
                    justify-content: center;

                    svg {
                        width: 4rem;
                        height: 4rem;
                    }
                }
            }

            h1 {
                font-size: 18px;
                font-weight: 700;
                margin-bottom: 1rem;
                margin-top:0px;
            }

            button {
                padding: 0.5rem 1.5rem;
            }
        }

        .workflows_section {
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin-bottom: 1rem;

            .workflows_input {
                display: flex;
                align-items: center;
                gap: 30px;

                h5 {
                    margin: 0;
                    font-weight: 600;
                    font-size: 18px;
                    line-height: 28.8px;
                }

                .search-wrapper {
                    .search {
                        width:auto;

                        .input-group {
                            height: 42px;

                            input {
                                min-width: 168px;
                                width:168px;
                                height: 42px;
                                padding: 0.5rem;
                            }
                        }
                    }
                }
            }

            .actions-wrapper {
                display: flex;
                flex-grow: 1;
                align-items: center;
                justify-content: flex-end;

                .dropdown,
                .dropdown-item {
                    font-size: 14px;
                }

                .dropdown-menu {
                    min-width: 122px;
                }

                .dropdown {
                    span {
                        border-radius: 2px;
                    }
                }
            }
        }
    }

    .ReactTable .rt-tbody .rt-td {
        padding: 14px;
        font-size: 14px;
    }

    .ReactTable .rt-tr-group {
        &:nth-child(even) {
            background-color: #FFFFFF !important;
        }
    }

    .ReactTable {
        .rt-table {
            .selected {
                background-color: rgba(131,210,75,.15) !important;
            }

            .rt-th {
                height: 64px !important;
            }

            .rt-tbody {
                .rt-td {
                    height: 64px !important;
                }
            }
        }
    }
`;
