import { createSelector } from 'reselect';

/**
 * Direct selector to the workflows state domain
 */
const selectWorkflowsDomain = () => ( state ) => state.get( 'workflows' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by Workflows
 */

const makeSelectWorkflows = () => createSelector(
  selectWorkflowsDomain(),
  ( substate ) => substate.toJS()
);
const makeSelectGetWorkflows = () => createSelector(
  selectWorkflowsDomain(),
  ( substate ) => substate.get( 'workflow' ).toJS()
);
const makeSelectPagination = () => createSelector(
  selectWorkflowsDomain(),
( substate ) => substate.get( 'pagination' ).toJS()
);
const makeSelectLoading = () => createSelector(
  selectWorkflowsDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectNotification = () => createSelector(
  selectWorkflowsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);
const makeSelectTableLoading = () => createSelector(
  selectWorkflowsDomain(),
  ( substate ) => substate.get( 'deleteLoading' )
);
export default makeSelectWorkflows;
export {
  selectWorkflowsDomain,
  makeSelectGetWorkflows,
  makeSelectPagination,
  makeSelectNotification,
  makeSelectLoading,
  makeSelectTableLoading
};
