import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, LOADING, NOTIFICATION_SAGA, SET_EDIT, SET_EDIT_WORKFLOWS
} from './constants';

const initialState = fromJS({
    isEdit: {},
    editWorkflows: '',
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Editworkflows reducer
 *
 */
function editworkflowsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_EDIT:
            return state.set( 'isEdit', fromJS( action.payload ) );
        case SET_EDIT_WORKFLOWS:
            return state.set( 'editWorkflows', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default editworkflowsReducer;
