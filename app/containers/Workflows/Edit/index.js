/* eslint-disable no-param-reassign */
/* eslint-disable react/no-unused-prop-types */

import _ from 'lodash';
import React from 'react';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { browserHistory } from 'utils/BrowserHistory';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';

import A from 'components/A';
import SalSelect from 'components/Select';
import Button from 'components/Button';
import Modal from 'components/Modal';
import Input from 'components/Input';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import { H2, H3, H1 } from 'components/Typography';
import MultiSelect from 'components/MultiSelect';
import Icon from 'components/Icon';
import Loader from 'components/Loader';

import makeSelectEditworkflows, { makeSelecteditWorkflows, makeSelectgetEditWorkflows, makeSelectLoading, makeSelectNotification } from './selectors';
import * as WorkflowsEditAction from './actions';
import { Footer, Header, LoadingStyles, PageWrapper } from './styles';
/**
 *
 * Editworkflows
 *
 */
export class Editworkflows extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        params: React.PropTypes.object,
        getEditWorkflows: React.PropTypes.func,
        isEdit: React.PropTypes.object,
        editWorkflows: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    static defaultProps = {
        loading: false
    };
    constructor( props ) {
        super( props );

        this.state = {
            formData: {},
            validEditName: 'name',
            validApprover: 'Approver',
            validexistName: false,
            addLevelFlag: false
        };
        this.discardModal = null;
        this.saveModel = null;
    }
    componentDidMount() {
        this.props.getEditWorkflows( this.props.params.id );
    }
    componentWillReceiveProps( nextProps ) {
        const Data = nextProps.isEdit.levels && nextProps.isEdit.levels.map( ( level ) => {
            const newApprovers = level.approvers.map( ( approvers ) => {
                const approverOne = {
                    value: approvers.affected_entity.id,
                    label: approvers.affected_entity.name,
                    type: approvers.affected_entity.type
                };
                return { ...approvers, affected_entity: approverOne };
            });
            return { ...level, approvers: newApprovers };
        }
        );
        this.setState({
            formData: { ...nextProps.isEdit, levels: Data }
        });
    }
    getAffectedEntity = ( keyword, callback ) => {
        const companyId = company.getLastActiveCompanyId();
        Fetch( `/company/${companyId}/affected_entities/search?term=${keyword}&limit=10&include_admin_users=true`, { method: 'GET' })
          .then( ( result ) => {
              const workflows = result.map( ( data ) => ({
                  value: data.id,
                  label: data.name,
                  type: data.type
              }) );
              callback( null, { options: workflows });
          })
          .catch( ( error ) => callback( error, null ) );
    }
    addLevels() {
        this.state.formData.levels && this.state.formData.levels.push({
            position: _.last( this.state.formData.levels ).position + 1,
            approvers: [{
                affected_entity: null
            }]
        });
        this.setState({ addLevelFlag: this.state.formData.levels.length >= 8 });
    }
    handleRemoveLevel = ( level ) => {
        this.state.formData.levels.splice( this.state.formData.levels.indexOf( level ), 1 );
        this.setState( ( state ) => ({ ...state, addLevelFlag: !( this.state.formData.levels.length <= 8 ) }) );
    }
    handleAddApprover=( level ) => {
        level.approvers.push({
            affected_entity: null
        });
        this.setState( ( state ) => ({ state }) );
    }
    handleRemoveApprover = ( level, i, approver ) => {
        level.approvers.splice( level.approvers.indexOf( approver ), 1 );
        this.setState( ( state ) => ({ ...state }) );
    }
    handleTreeApprover = ( approver, index, level, i ) => (
        <li key={ index }>
            <div className="flow-nodetree-wrap">
                <div className="flow-form-group">
                    {( index === 0 && i === 0 ) ?
                        <MultiSelect
                            id={ `announcement${index}${i}` }
                            async
                            multi={ false }
                            clientError="This field is required"
                            required={ !!( ( index === 0 && i === 0 ) ) }
                            label={ <span>Approver {index + 1} </span> }
                            ref={ ( ref ) => { this.affectedEntityOne = ref; } }
                            loadOptions={ this.getAffectedEntity }
                            value={ approver.affected_entity }
                            onChange={ ( value ) => { approver.affected_entity = value; } }
                            placeholder="Select option"
                        /> : <SalSelect
                            id={ `announcement${index}${i}` }
                            async
                            ref={ ( ref ) => { this.affectedEntity = ref; } }
                            loadOptions={ this.getAffectedEntity }
                            label={ <span>Approver {index + 1}
                                {index >= 1 &&
                                <A
                                    href
                                    onClick={ ( e ) => {
                                        e.preventDefault();
                                        this.handleRemoveApprover( level, index, approver );
                                    } }
                                >
                                    <Icon name="minusCircle" className="removeTree" />
                                </A>}
                            </span> }
                            value={ approver.affected_entity }
                            onChange={ ( value ) => { approver.affected_entity = value; } }
                            placeholder="Select option"
                        />}
                </div>
            </div>
            {level.approvers.length < 3 &&
                <A
                    href
                    onClick={ ( e ) => {
                        e.preventDefault();
                        this.handleAddApprover( level, i );
                    }
                    }
                    className="addTree"
                >
                    <Icon name="plusCircle" />
                </A>}
        </li>
        )
    handleTreeView = ( level, i ) => (
        <li key={ i }>
            <div className="flow-nodetree-wrap">
                <span className="action-trigger">
                    <span className="flow-tree-dept"><Icon name="levels" /> Level { i + 1 }</span>
                    <span className="flow-action-pop">
                        {
                        level.position > 1 &&
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                this.handleRemoveLevel( level, i );
                            } }
                        >
                            <Icon name="minusCircle" className="removeTree" />
                        </A>
                    }
                    </span>
                </span>
                <ul className="flow-tree-horizontal">
                    { level.approvers.length && level.approvers.map( ( approver, index ) => this.handleTreeApprover( approver, index, level, i ) )}
                </ul>
            </div>
        </li>
        )
    prepareLevels() {
        this.state.formData.levels.forEach( ( level, index ) => {
            level.position = index + 1;
            level.approvers.forEach( ( approver ) => {
                approver.approver_id = approver.affected_entity.value;
                approver.approver_type = approver.affected_entity.type;
            });
        });
    }
    clearEmptyLevelsAndApprovers() {
        this.state.formData.levels.forEach( ( level ) => {
            level.approvers = level.approvers.filter( ( approver, index ) => approver.affected_entity || ( level.position === 1 && index === 0 ) );
        });

        this.state.formData.levels = this.state.formData.levels.filter( ( level ) => level.approvers.length );
        this.setState( ( state ) => state );

        this.state.formData.levels.map( ( level, Li ) => (
            level.approvers.map( ( approver, Ai ) => ( ( Li === 0 && Ai === 0 ) && ( approver.affected_entity === null ) ? this.affectedEntityOne.setState({ error: true }) : '' ) )
        ) );
    }
    submitEditForm= async () => {
        await this.clearEmptyLevelsAndApprovers();
        const companyId = company.getLastActiveCompanyId();
        if ( this.state.formData.name === '' ) {
            this.setState({
                validEditName: ''
            });
        }
        this.prepareLevels();
        const dataList = this.state.formData.levels && this.state.formData.levels.map( ( level ) => {
            const newApprovers = level.approvers.map( ( approvers ) => {
                const approverOne = {
                    id: approvers.affected_entity.value,
                    name: approvers.affected_entity.label,
                    type: approvers.affected_entity.type
                };
                return { ...approvers, affected_entity: approverOne };
            }); return { ...level, approvers: newApprovers };
        });
        if ( this.state.formData.levels[ 0 ].approvers[ 0 ].affected_entity !== null && this.state.formData.name !== '' ) {
            const data = {
                name: this.state.formData.name,
                company_id: companyId,
                id: parseFloat( this.props.params.id ),
                workflow_id: parseFloat( this.props.params.id ),
                levels: dataList
            };
            await Fetch( `/company/${companyId}/workflow/is_name_available`, { method: 'POST', data })
        .then( ( response ) => {
            if ( response.available ) {
                this.props.editWorkflows( data );
            } else { this.setState({ validexistName: true }); }
        });
        }
    }
    /**
     *
     * Editworkflows render method
     *
     */
    render() {
        const { loading } = this.props;
        const { formData, validEditName, validexistName } = this.state;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        return (
            <div>
                <Helmet
                    title="Edit Workflow"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Workflow' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Header>
                    {!loading ?
                        <div className="nav">
                            <Container>
                                <A
                                    href
                                    onClick={ ( e ) => {
                                        e.preventDefault();
                                        browserHistory.push( '/company-settings/workflow-automation/workflows', true );
                                    } }
                                >
                                    <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Workflows</span>
                                </A>
                            </Container>
                        </div> : '' }
                </Header>
                <PageWrapper>
                    <Container>
                        { loading ?
                            <div className="loader" style={ { display: loading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Loading Edit Workflows.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div> : ''}
                        {!loading ?
                            <div className="main_section">
                                <div className="heading">
                                    <H1 noBottomMargin>Edit Workflow</H1>
                                </div>
                                <div className="row">
                                    <div className={ validEditName === '' || validexistName ? 'col-md-4 error-show' : 'col-md-4 workflow-Field' }>
                                        <Input
                                            id="tardiness_Rule_Name"
                                            label="* Workflow name"
                                            type="text"
                                            value={ formData.name }
                                            onChange={ ( value ) => this.setState({
                                                formData: { ...formData, name: value },
                                                validEditName: value,
                                                validexistName: false
                                            }) }
                                        />
                                        {validEditName === '' ? <p>This field is required</p> : ''}
                                        {validexistName ? <p>Record name already exists</p> : ''}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <span className="flow-title">* Approval Workflow</span>
                                        <ul className="flow-node-tree">
                                            {
                                            this.state.formData.levels && this.state.formData.levels.map( ( level, i ) => this.handleTreeView( level, i ) )
                                        }
                                        </ul>
                                        { !this.state.addLevelFlag &&
                                        <Button
                                            label={ <span><Icon name="plus" />Add Level</span> }
                                            type="neutral"
                                            size="large"
                                            onClick={ () => this.addLevels() }
                                            className="btn-addlevel"
                                        /> }
                                    </div>
                                </div>
                            </div> : '' }
                    </Container>
                </PageWrapper>
                {!loading ?
                    <Footer>
                        <div className="from_footer">
                            <Container>
                                <div className="footer_button">
                                    <Button className="btn_cancel" label="Cancel" onClick={ () => this.discardModal.toggle() } />
                                    <Button type="action" label={ loading ? <Loader /> : 'Submit' } onClick={ () => this.saveModel.toggle() } />
                                </div>
                            </Container>
                        </div>
                    </Footer> : ''}
                <Modal
                    title="Discard Changes"
                    className="modal-md modal-commission-type"
                    body={
                        <p>
                           Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?
                        </p>
                    }
                    buttons={ [

                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.discardModal.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/company-settings/workflow-automation/workflows', true );
                                this.discardModal.toggle();
                            }
                        }
                    ] }
                    center
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                />
                <Modal
                    title="Confirm Your Action"
                    className="modal-md modal-commission-type"
                    body={
                        <p>
                           Updating workflow will reset pending requests that use it and their approval process will start over.
                        </p>
                    }
                    buttons={ [

                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Cancel',
                            onClick: () => {
                                this.saveModel.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'darkRed',
                            label: 'Continue',
                            onClick: () => {
                                this.submitEditForm();
                                this.saveModel.toggle();
                            }
                        }
                    ] }
                    center
                    showClose={ false }
                    ref={ ( ref ) => { this.saveModel = ref; } }
                />
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    Editworkflows: makeSelectEditworkflows(),
    isEdit: makeSelectgetEditWorkflows(),
    editWorkflows: makeSelecteditWorkflows(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        WorkflowsEditAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Editworkflows );
