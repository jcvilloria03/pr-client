import styled from 'styled-components';

export const Header = styled.div`
    .nav {
        padding:10px 10px;
        background:#f0f4f6;
        margin-bottom:10px;
        position: fixed;
        top: 76px;
        right: 0;
        left: 0;
        width: 100%;
        z-index:9;
    }

    .icon-arrow {
        width: 14px;
        font-size: 14px;
        display: inline-block;
        margin-right: 4px;

        > i {
            align-self: center;
        }
    }

    .back-text {
        font-size: 14px;
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh ;
    padding: 0;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    height:100%;
    
    .container {
        height:100%;

        .loader {
            height:100vh;
        }
    }

    .main_section {
        padding-bottom: 200px;
        margin: auto;
        margin-top: 150px;

        .heading {
            margin-top: 20px;
            margin-bottom: 50px;

            h1 {
                text-align: center;
                font-size: 36px;
                font-weight: 700;
                line-height: 57.6px;
                margin-bottom: 7px;
            }

            p {
                text-align: center;
                font-size: 14px;
                margin-bottom: 0px;
            }
        }

        .flow-title {
            font-size: 14px;
        }

        .workflow-Field {
            .required {
                color:#5b5b5b;
            }
        }
        
        .error-show {
            label,p,.required {
                color: #eb7575 !important;
                font-size: 13px;
            }

            input {
                border-color: #eb7575;
            }
        }

        ul.flow-node-tree {
            list-style: none;
            padding: 0;
            position: relative;

            &:before {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                z-index: 1;
                width: 2rem;
                height: 1rem;
                background-color: #fff;
            }

            li {
                padding-left: 4rem;
                position: relative;
                padding-top: 1rem;

                &:before {
                    position: absolute;
                    top: 3rem;
                    left: 1.5rem;
                    height: 1px;
                    width: 3rem;
                    margin: auto;
                    border-bottom: 1px dotted #ccc;
                    content: "";
                }

                &:after {
                    position: absolute;
                    top: 0;
                    bottom: 0;
                    left: 1.5rem;
                    width: 1px;
                    height: 100%;
                    border-left: 1px dotted #ccc;
                    content: "";
                }

                &:last-child:after {
                    height: 3rem;
                }

                .flow-nodetree-wrap {
                    position: relative;
                    display: inline-block;

                    .action-trigger {
                        &:hover {
                            .removeTree {
                                opacity:1;
                            }
                        }

                        .flow-tree-dept {
                            position: relative;
                            display: inline-flex;
                            align-items: center;
                            padding: .5rem 1rem;
                            min-width: 9rem;
                            max-width: 15rem;
                            white-space: nowrap;
                            overflow: hidden;
                            text-overflow: ellipsis;
                            background-color: #e5f6fc;
                            border: 1px solid #adadad;
                            border-radius: 0.25rem;

                            svg {
                                width: 14px;
                                height: 14px;
                                margin-right: 1rem;
                            }
                        }

                        .removeTree {
                            margin-left: 15px;
                            opacity:0;

                            svg {
                                color: #eb7575;
                                width: 14px;
                            }
                        }
                    }

                    .flow-tree-horizontal {
                        list-style: none;
                        margin: 0;
                        padding: 0;

                        li {
                            display: inline-block;

                            &:before {
                                top: 4.5rem;
                            }

                            &:after {
                                height: 70%;
                            }

                            .addTree {
                                position: absolute;
                                top: 4rem;
                                right: -30px;

                                svg {
                                    fill: #83d24b;
                                    width: 14px
                                }
                            }

                            .flow-nodetree-wrap {
                                .flow-form-group {
                                    width: 22rem;

                                    .is-error {
                                        .Select-control {
                                            border-color: #eb7575;
                                        }
                                    }

                                    .is-error + label,.is-error ~ p {
                                        color:#eb7575;
                                    }

                                    &:hover {
                                        .removeTree {
                                            opacity:1;
                                        }
                                    }

                                    label {
                                        min-height: 2rem;

                                        .required {
                                            display:none;
                                        }

                                        .removeTree {
                                            margin-left: 15px;
                                            opacity:0;

                                            svg {
                                                color: #eb7575;
                                                width: 14px;
                                            }
                                        }
                                    }

                                    .Select {
                                        min-height: 3rem;
                                    }
                                }
                            }
                        }

                        li:not(:first-child) {
                            padding-left: 1rem;
                        }

                        li:not(:first-child):before {
                            left: 0;
                            width: 5rem;
                        }

                        li:not(:first-child):after {
                            display:none;
                        }
                    }
                }
            }
        }

        .btn-addlevel {
            padding: 14px 27px;
            border-radius: 2rem;

            svg {
                width: 14px;
                margin-right: 3px;
            }
        }

        .flow-node-tree>li {
            margin-left: 0;
            padding-left: 0 !important;
        }
    }
`;

export const Footer = styled.div`
    .from_footer {
        margin: 14px 0px 0px;
        padding: 14px 0px;
        background: #F0F4F6;
        margin-top:280px;
        position: fixed;
        bottom: 0;
        right: 0;
        left: 0;
    }

    .from_footer .footer_button {
        width: 100%;
        text-align: end;
    }

    .from_footer .footer_button button {
        padding: 14px 28px;
        border-radius: 2rem;

        &:focus {
            outline:none;
        }

        cursor:pointer
    }

    .from_footer .footer_button .btn_cancel {
        color: #474747;
        border: 1px solid #83D24B;
        background-color: #ffffff;
        margin-right: 5px;
    }
`;
