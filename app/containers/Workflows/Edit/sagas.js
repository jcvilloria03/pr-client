import { get } from 'lodash';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { call, cancel, put, take } from 'redux-saga/effects';
import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { RECORD_UPDATED_MESSAGE } from '../../../utils/constants';
import { formatFeedbackMessage } from '../../../utils/functions';
import { Fetch } from '../../../utils/request';
import { REINITIALIZE_PAGE } from '../../App/constants';

import { GET_EDIT, GET_EDIT_WORKFLOWS, LOADING, NOTIFICATION, NOTIFICATION_SAGA, SET_EDIT } from './constants';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}
/**
 * get EditWorkflows
 * @param payload
 */
export function* getEditWorkflows({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const getData = yield call( Fetch, `/workflow/${payload}`, { method: 'GET' });
        yield put({ type: SET_EDIT, payload: getData });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}
/**
 * edit Workflows
 * @param payload
 */
export function* editWorkflows({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const data = {
            company_id: companyId,
            id: payload.id,
            ...payload
        };
        yield call( Fetch, `/workflow/${payload.id}`, { method: 'PUT', data });
        yield call( showSuccessMessage );
        browserHistory.push( '/company-settings/workflow-automation/workflows', true );
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message || `Request failed with statuscode ${error.response.status}` : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}
/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}
 /**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}
/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });
}
/**
* reinitialize
*/
export function* reinitializePage() {
    yield call( getEditWorkflows );
}
/**
* watchForReinitializePage
*/
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
* watchForNotifyUser
*/
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Individual exports for testing
 */
export function* watchForGetEditWorkflows() {
    const watcher = yield takeEvery( GET_EDIT, getEditWorkflows );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for edit workflows
 */
export function* watchForeditWorkflows() {
    const watcher = yield takeEvery( GET_EDIT_WORKFLOWS, editWorkflows );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
// All sagas to be loaded
export default [
    defaultSaga,
    watchForNotifyUser,
    watchForGetEditWorkflows,
    watchForReinitializePage,
    watchForeditWorkflows
];
