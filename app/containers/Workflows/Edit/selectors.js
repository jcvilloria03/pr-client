import { createSelector } from 'reselect';

/**
 * Direct selector to the editworkflows state domain
 */
const selectEditworkflowsDomain = () => ( state ) => state.get( 'workflowsEdit' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by Editworkflows
 */

const makeSelectEditworkflows = () => createSelector(
  selectEditworkflowsDomain(),
  ( substate ) => substate.toJS()
);
const makeSelectgetEditWorkflows = () => createSelector(
  selectEditworkflowsDomain(),
  ( substate ) => substate.get( 'isEdit' ).toJS()
);
const makeSelecteditWorkflows = () => createSelector(
  selectEditworkflowsDomain(),
  ( substate ) => substate.get( 'editWorkflows' )
);
const makeSelectLoading = () => createSelector(
  selectEditworkflowsDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectNotification = () => createSelector(
  selectEditworkflowsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);
export default makeSelectEditworkflows;
export {
  selectEditworkflowsDomain,
  makeSelectgetEditWorkflows,
  makeSelecteditWorkflows,
  makeSelectLoading,
  makeSelectNotification
};
