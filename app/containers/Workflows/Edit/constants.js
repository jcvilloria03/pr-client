/*
 *
 * Editworkflows constants
 *
 */
const namespace = 'app/Workflows/Edit';
export const DEFAULT_ACTION = 'app/Editworkflows/DEFAULT_ACTION';

export const GET_EDIT = `${namespace}/GET_EDIT`;
export const SET_EDIT = `${namespace}/SET_EDIT`;

export const GET_EDIT_WORKFLOWS = `${namespace}/GET_EDIT_WORKFLOWS`;
export const SET_EDIT_WORKFLOWS = `${namespace}/SET_EDIT_WORKFLOWS`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;

export const LOADING = `${namespace}/LOADING`;
