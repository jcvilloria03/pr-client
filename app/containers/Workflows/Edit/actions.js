import {
    DEFAULT_ACTION, GET_EDIT, GET_EDIT_WORKFLOWS
} from './constants';

/**
 *
 * Editworkflows actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
/**
 * get Workflows action
 * @param {object} payload
 */
export function getEditWorkflows( payload ) {
    return {
        type: GET_EDIT,
        payload
    };
}
/**
 *
 * Edit TardinessRules action
 *
 */
export function editWorkflows( payload ) {
    return {
        type: GET_EDIT_WORKFLOWS,
        payload
    };
}
