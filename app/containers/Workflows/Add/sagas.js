import { get } from 'lodash';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery } from 'redux-saga';
import { call, cancel, put, take } from 'redux-saga/effects';
import { Fetch } from 'utils/request';
import { NEW_RECORD_SAVED_MESSAGE } from 'utils/constants';
import { formatFeedbackMessage } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import { ADD_APPROVER, BTN_LOADING, NOTIFICATION, SET_NOTIFICATION } from './constants';

/**
 * Get Approver Data
 */
export function* addApprover({ payload }) {
    try {
        yield put({ type: BTN_LOADING, payload: true });
        yield call( Fetch, '/workflow', { method: 'POST', data: payload });
        yield call( showSuccessMessage );
        browserHistory.push( '/company-settings/workflow-automation/workflows', true );
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({ type: BTN_LOADING, payload: false });
    }
}

 /**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( true, 'success', NEW_RECORD_SAVED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForaddApprover() {
    const watcher = yield takeEvery( ADD_APPROVER, addApprover );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForaddApprover
];
