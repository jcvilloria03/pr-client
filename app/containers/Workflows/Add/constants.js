/*
 *
 * Workflows constants
 *
 */

const namespace = 'app/Workflows';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;

export const LOADING = `${namespace}/LOADING`;

export const ADD_APPROVER = `${namespace}/ADD_APPROVER`;
export const SET_APPROVER = `${namespace}/SET_APPROVER`;

export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const BTN_LOADING = `${namespace}/BTN_LOADING`;
