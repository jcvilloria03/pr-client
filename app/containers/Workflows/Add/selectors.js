import { createSelector } from 'reselect';

/**
 * Direct selector to the workflows state domain
 */
const selectWorkflowsDomain = () => ( state ) => state.get( 'workflowsAdd' );

/**
 * Other specific selectors
 */
const makeSelectGetApprover = () => createSelector(
  selectWorkflowsDomain(),
  ( substate ) => substate.get( 'approverData' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectWorkflowsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBTNLoading = () => createSelector(
  selectWorkflowsDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

/**
 * Default selector used by Workflows
 */
const makeSelectWorkflows = () => createSelector(
  selectWorkflowsDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectWorkflows,
  selectWorkflowsDomain,
  makeSelectNotification,
  makeSelectGetApprover,
  makeSelectBTNLoading
};
