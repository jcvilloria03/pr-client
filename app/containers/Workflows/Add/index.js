/* eslint-disable no-param-reassign */
import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';
import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';

import A from 'components/A';
import Modal from 'components/Modal';
import Icon from 'components/Icon';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Input from 'components/Input';
import SnackBar from 'components/SnackBar';
import MultiSelect from 'components/MultiSelect';
import Loader from 'components/Loader';
import { H1 } from 'components/Typography';

import { makeSelectBTNLoading, makeSelectGetApprover, makeSelectNotification, makeSelectWorkflows } from './selectors';
import * as workflowsAction from './actions';
import { Footer, Header, PageWrapper } from './styles';

/**
 *
 * Workflows
 *
 */
export class Workflows extends React.Component { // eslint-disable-line react/prefer-stateless-function

    static propTypes={
        addApprover: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        btnLoading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );
        this.state = {
            profile: null,
            names: '',
            nameFlag: false,
            levels: [{
                position: 1,
                approvers: [{
                    affected_entity: null
                }]
            }],
            addBTNFlag: false
        };
        this.confirmCancelModal = null;
    }

    componentWillMount() {
        this.props.resetStore();
        const profile = auth.getUser();
        this.setState({ profile });
    }
    handleAddLevel=() => {
        this.state.levels.push({
            position: _.last( this.state.levels ).position + 1,
            approvers: [{
                affected_entity: null
            }]
        });
        this.setState({ addBTNFlag: this.state.levels.length >= 8 });
    }

    handleRemoveLevel=( level ) => {
        this.state.levels.splice( this.state.levels.indexOf( level ), 1 );
        this.setState( ( state ) => ({ ...state, addBTNFlag: !( this.state.levels.length <= 8 ) }) );
    }

    handleAddApprover=( level ) => {
        level.approvers.push({
            affected_entity: null
        });
        this.setState( ( state ) => ({ state }) );
    }

    handleRemoveApprover=( level, i, approver ) => {
        level.approvers.splice( level.approvers.indexOf( approver ), 1 );
        this.setState( ( state ) => ({ state }) );
    }

    handleClearEmptyAddApprover=() => {
        this.setState( ( state ) => {
            state.levels.map( ( level ) => ( level.approvers = level.approvers.filter( ( approver, index ) => approver.affected_entity || ( level.position === 1 && index === 0 ) ) ) );
            const newLevels = state.levels.filter( ( level ) => level.approvers.length );
            return ({ ...state, levels: newLevels });
        });

        const { levels, names } = this.state;
        levels.map( ( level, Li ) => (
            level.approvers.map( ( approver, Ai ) => ( ( Li === 0 && Ai === 0 ) && ( approver.affected_entity === null ) ? this.workflowsOne.setState({ error: true }) : '' ) )
        ) );

        if ( names === '' ) {
            this.workflowsName.setState({ error: true, errorMessage: 'This field is required' });
            this.setState({ nameFlag: true });
        }
    }

    handlePrepareLevels=() => {
        const { levels } = this.state;
        const payload = levels.map( ( level, Li ) => {
            const newApprovers = level.approvers.map( ( approver ) => ({ ...approver, approver_id: approver.affected_entity.value, approver_type: approver.affected_entity.type }) );
            return ({ position: Li + 1, approvers: newApprovers });
        });

        return payload;
    }

    handleSubmitBTN=async () => {
        await this.handleClearEmptyAddApprover();
        const { names, levels } = this.state;
        if ( levels[ 0 ].approvers[ 0 ].affected_entity !== null && names !== '' ) {
            const updatedPayload = await this.handlePrepareLevels();
            const payload = { name: this.state.names, levels: this.state.levels };
            Fetch( `/company/${this.state.profile.last_active_company_id}/workflow/is_name_available`, { method: 'POST', data: payload })
            .then( ( res ) => {
                if ( res.available ) {
                    const postPayload = { name: this.state.names, company_id: this.state.profile.last_active_company_id, levels: updatedPayload };
                    this.props.addApprover( postPayload );
                } else {
                    this.workflowsName.setState({ error: true, errorMessage: 'Record name already exists' });
                    this.setState({ nameFlag: true });
                }
            });
        }
    }

    handleTreeViewApprover=( approver, inedx, level, levelIndex ) => (
        <li key={ inedx }>
            <div className="flow-nodetree-wrap">
                <div className="flow-form-group">
                    { ( inedx === 0 && levelIndex === 0 ) ?
                        <MultiSelect
                            id={ `announcement${inedx}${levelIndex}` }
                            async
                            multi={ false }
                            clientError="This field is required"
                            required={ !!( ( inedx === 0 && levelIndex === 0 ) ) }
                            label={ <span>Approver {inedx + 1} </span> }
                            loadOptions={ this.loadApproverList }
                            ref={ ( ref ) => { this.workflowsOne = ref; } }
                            value={ approver.affected_entity }
                            onChange={ ( value ) => {
                                approver.affected_entity = value;
                            } }
                            placeholder="Select option"
                        />
                    : <MultiSelect
                        id={ `announcement${inedx}${levelIndex}` }
                        async
                        multi={ false }
                        clientError="This field is required"
                        required={ !!( ( inedx === 0 && levelIndex === 0 ) ) }
                        label={ <span>Approver {inedx + 1}
                            {inedx >= 1 &&
                                <A
                                    href
                                    onClick={ ( e ) => {
                                        e.preventDefault();
                                        this.handleRemoveApprover( level, inedx, approver );
                                    } }
                                >
                                    <Icon name="minusCircle" className="removeTree" />
                                </A>}
                        </span> }
                        loadOptions={ this.loadApproverList }
                        ref={ ( ref ) => { this.workflows = ref; } }
                        value={ approver.affected_entity }
                        onChange={ ( value ) => {
                            approver.affected_entity = value;
                        } }
                        placeholder="Select option"
                    />
                    }
                </div>
            </div>
            {level.approvers.length < 3 &&
                <A
                    href
                    onClick={ ( e ) => {
                        e.preventDefault();
                        this.handleAddApprover( level, levelIndex );
                    }
                    }
                    className="addTree"
                >
                    <Icon name="plusCircle" />
                </A>}
        </li>
        )

    handleTreeView=( level, i ) => (
        <li key={ i }>
            <div className="flow-nodetree-wrap">
                <span className="action-trigger">
                    <span className="flow-tree-dept"><Icon name="levels" /> Level {i + 1} </span>
                    <span className="flow-action-pop">
                        {level.position > 1 &&
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                this.handleRemoveLevel( level, i );
                            } }
                        >
                            <Icon name="minusCircle" className="removeTree" />
                        </A>}
                    </span>
                </span>
                <ul className="flow-tree-horizontal">
                    {level.approvers.length && level.approvers.map( ( approver, inedx ) => this.handleTreeViewApprover( approver, inedx, level, i ) )}
                </ul>
            </div>
        </li>
        )

    loadApproverList = ( keyword, callback ) => {
        Fetch( `/company/${this.state.profile.last_active_company_id}/affected_entities/search?term=${keyword}&limit=10&include_admin_users=true`, { method: 'GET' })
          .then( ( result ) => {
              const list = result.map( ({ id, name, type }) => ({
                  type,
                  value: id,
                  label: name
              }) );
              callback( null, { options: list });
          })
          .catch( ( error ) => callback( error, null ) );
    }
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { btnLoading } = this.props;
        return (
            <div>
                <Helmet
                    title="Add Workflow"
                    meta={ [
                        { name: 'description', content: 'Description of Add Workflow' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Discard Changes"
                    className="modal-md modal-commission-type"
                    body={
                        <div>
                            <p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonCancel',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => this.confirmCancelModal.toggle()
                        },
                        {
                            id: 'buttonProceed',
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: () => browserHistory.push( '/company-settings/workflow-automation/workflows/', true )
                        }
                    ] }
                    center
                    showClose={ false }
                    backdrop="static"
                    ref={ ( ref ) => { this.confirmCancelModal = ref; } }
                />
                <Header>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/workflow-automation/workflows', true );
                                } }
                            >
                                <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Workflows</span>
                            </A>
                        </Container>
                    </div>
                </Header>
                <PageWrapper>
                    <Container>
                        <div className="main_section">
                            <div className="heading">
                                <H1 noBottomMargin>Add Workflow</H1>
                            </div>
                            <div className="row">
                                <div className={ this.state.nameFlag ? 'col-md-4 error-show' : 'col-md-4 workflow-Field' }>
                                    <Input
                                        id="workflow-name"
                                        label="Workflow name"
                                        required
                                        type="text"
                                        clientError="This field is required"
                                        value={ this.state.names }
                                        ref={ ( ref ) => { this.workflowsName = ref; } }
                                        onChange={ ( value ) => this.setState({ names: value, nameFlag: value.length <= 0 }) }
                                        onBlur={ ( value ) => this.setState({ nameFlag: value.length <= 0 }) }
                                    />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <span className="flow-title">* Approval Workflow</span>
                                    <ul className="flow-node-tree">
                                        {
                                            this.state.levels.map( ( level, i ) => this.handleTreeView( level, i ) )
                                        }
                                    </ul>
                                    {!this.state.addBTNFlag &&
                                    <Button
                                        label={ <span><Icon name="plus" />Add Level</span> }
                                        type="neutral"
                                        size="large"
                                        onClick={ () => this.handleAddLevel() }
                                        className="btn-addlevel"
                                    />}
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <Footer>
                    <div className="from_footer">
                        <Container>
                            <div className="footer_button">
                                <Button
                                    label="Cancel"
                                    type="neutral"
                                    size="large"
                                    onClick={ () => this.confirmCancelModal.toggle() }
                                />
                                <Button
                                    label={ btnLoading ? <Loader /> : 'Submit' }
                                    type="action"
                                    size="large"
                                    disabled={ !!btnLoading }
                                    onClick={ () => this.handleSubmitBTN() }
                                />
                            </div>
                        </Container>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    Workflows: makeSelectWorkflows(),
    Approver: makeSelectGetApprover(),
    notification: makeSelectNotification(),
    btnLoading: makeSelectBTNLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        workflowsAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Workflows );
