import { RESET_STORE } from '../../App/constants';
import {
    ADD_APPROVER
} from './constants';

/**
 *
 * Workflows actions
 *
 */

/**
 * payroll Request Type
 * @param {object} payload
 */
export function addApprover( payload ) {
    return {
        type: ADD_APPROVER,
        payload
    };
}

/**
 * payroll Request Type
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
