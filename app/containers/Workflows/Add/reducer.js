import { fromJS } from 'immutable';
import { RESET_STORE } from '../../App/constants';
import {
    BTN_LOADING,
    SET_APPROVER, SET_NOTIFICATION
} from './constants';

const initialState = fromJS({
    approverData: [],
    btnLoading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Workflows reducer
 *
 */
function workflowsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_APPROVER:
            return state.set( 'approverData', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default workflowsReducer;
