/* eslint-disable react/sort-comp */
import React from 'react';
import moment from 'moment';
import LazyLoad from 'react-lazyload';
import get from 'lodash/get';
import capitalize from 'lodash/capitalize';
import startCase from 'lodash/startCase';
import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';
import toLower from 'lodash/toLower';
import toUpper from 'lodash/toUpper';
import flatten from 'lodash/flatten';
import snakeCase from 'lodash/snakeCase';
import has from 'lodash/has';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { H2, H3 } from 'components/Typography';
import Button from 'components/Button';
import Input from 'components/Input';
import SalSelect from 'components/Select';
import DatePicker from 'components/DatePicker';
import SalConfirm from 'components/SalConfirm';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { DATE_FORMATS } from 'utils/constants';
import {
    formatDate,
    stripNonDigit,
    stripNonNumeric,
    formatMobileNumber,
    formatRDO,
    formatSSS,
    formatTIN,
    formatHDMF,
    formatPhilHealth,
    formatNumber
} from 'utils/functions';
import { subscriptionService } from 'utils/SubscriptionService';
import { renderField } from 'utils/form';

import * as viewActions from '../actions';
import {
    makeSelectEmployee,
    makeSelectFormOptions,
    makeSelectRequestTypes,
    makeSelectAnnualEarningsFormOptions,
    makeSelectProductsState,
    makeSelectUserInformation
} from '../selectors';
import {
    CONSULTANT,
    FIXED,
    INITIAL_PERMISSIONS,
    EMPLOYEE_SECTIONS,
    PROFILE_SECTION_IDS,
    OPTIONS,
    PAYROLL_FIELDS,
    TA_FIELDS,
    TA_PLUS_PAYROLL_FIELDS,
    CUSTOM_ERROR_MESSAGES
} from '../constants';

import EmployeeLoans from './sections/loans';
import EmployeeBonuses from './sections/bonuses';
import EmployeeCommissions from './sections/commissions';
import EmployeeDeductions from './sections/deductions';
import EmployeeAllowances from './sections/allowances';
import EmployeeLeaveCredits from './sections/leaveCredits';
import EmployeeAdjustments from './sections/adjustments';
import EmployeeFiledLeaves from './sections/filedLeaves';
import EmployeeApprovalGroups from './sections/approvalGroups';
import EmployeeHistoricalEarnings from './sections/historicalEarnings';
import EmployeeAnnualEarnings from './sections/annualEarnings';
import EmployeeEarnings from './sections/earnings';
import EmployeeBasicPayAdjustments from './sections/basicPayAdjustments';
import EmployeeTerminationInformation, { FINAL_PAY_STATUSES } from './sections/terminationInformation';
import EmployeePaymentMethods from './sections/paymentMethod';

import {
    ProfileWrapper,
    ProfileSectionWrapper,
    MessageWrapper,
    ConfirmBodyWrapperStyle,
    LoadingStyles
} from './styles';
import CompanyService, { company } from '../../../../utils/CompanyService';

const formatBasePay = ( value ) =>
    Intl.NumberFormat(
        'en-PH',
        {
            minimumFractionDigits: 2,
            maximumFractionDigits: 4
        }
    ).format( value );

/**
 *
 * EmployeeProfileView component
 *
 */
class EmployeeProfileView extends React.Component {
    static propTypes = {
        saveEmployeeProfile: React.PropTypes.func,
        getEmployeeLeaveCredits: React.PropTypes.func,
        getEmployeeFiledLeaves: React.PropTypes.func,
        updateEmployeeLeaveCredits: React.PropTypes.func,
        exportLeaveCredits: React.PropTypes.func,
        exportFiledLeaves: React.PropTypes.func,
        canLoadEmployeeProfile: React.PropTypes.func,
        exportLoans: React.PropTypes.func,
        deleteLoans: React.PropTypes.func,
        exportOtherIncomes: React.PropTypes.func,
        deleteOtherIncomes: React.PropTypes.func,
        deleteBasicPayAdjustments: React.PropTypes.func,
        deleteTerminationInformation: React.PropTypes.func,
        exportHistoricalEarnings: React.PropTypes.func,
        employee: React.PropTypes.shape({
            data: React.PropTypes.object,
            loading: React.PropTypes.bool,
            saving: React.PropTypes.bool,
            errors: React.PropTypes.object,
            loadingEmploymentInformation: React.PropTypes.bool,
            loadingPayrollInformation: React.PropTypes.bool,
            loadingTimeAttendanceInformation: React.PropTypes.bool,
            loadingBasicInformation: React.PropTypes.bool,
            loadingTerminationInformation: React.PropTypes.bool,
            loadingApprovalGroups: React.PropTypes.bool,
            terminationInformation: React.PropTypes.object
        }),
        formOptions: React.PropTypes.shape({
            constants: React.PropTypes.shape({
                base_pay_unit: React.PropTypes.arrayOf( React.PropTypes.object ),
                tax_type: React.PropTypes.arrayOf( React.PropTypes.object ),
                tax_status: React.PropTypes.arrayOf( React.PropTypes.object ),
                sss_basis: React.PropTypes.arrayOf( React.PropTypes.object ),
                hdmf_basis: React.PropTypes.arrayOf( React.PropTypes.object ),
                philhealth_basis: React.PropTypes.arrayOf( React.PropTypes.object )
            }),
            costCenters: React.PropTypes.arrayOf( React.PropTypes.object ),
            departments: React.PropTypes.arrayOf( React.PropTypes.object ),
            ranks: React.PropTypes.arrayOf( React.PropTypes.object ),
            locations: React.PropTypes.arrayOf( React.PropTypes.object ),
            payrollGroups: React.PropTypes.arrayOf( React.PropTypes.object ),
            workflows: React.PropTypes.arrayOf( React.PropTypes.object ),
            employmentTypes: React.PropTypes.arrayOf( React.PropTypes.object ),
            employment_status: React.PropTypes.arrayOf( React.PropTypes.object )
        }),
        requestTypes: React.PropTypes.array,
        updateEmployeeApprovalGroups: React.PropTypes.func,
        approvalCheckPendingRequests: React.PropTypes.func,
        getEmployeeAnnualEarnings: React.PropTypes.func,
        setShouldShowDiscardConfirm: React.PropTypes.func,
        getEmployeeOtherIncome: React.PropTypes.func,
        getEmployeeLoans: React.PropTypes.func,
        getEmployeeHistoricalEarnings: React.PropTypes.func,
        getEmployeeEarnings: React.PropTypes.func,
        getDataNecessaryForEdit: React.PropTypes.func,
        getEmployeeApprovalGroups: React.PropTypes.func,
        getEmployeePaymentMethods: React.PropTypes.func,
        getEmployeeBasicPayAdjustments: React.PropTypes.func,
        deleteEmployeePaymentMethods: React.PropTypes.func,
        assignEmployeePaymentMethod: React.PropTypes.func,
        products: React.PropTypes.array,
        userInformation: React.PropTypes.object,
        currentSection: React.PropTypes.string,
        scrollToSection: React.PropTypes.func
    }

    /**
     * components' constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            edit: {
                basic: false,
                employment: false,
                payroll: false,
                time_attendance: false
            },
            employee: {},
            employeeEdit: {},
            employeeNameChanged: false,
            authorized: false,
            loanPermission: INITIAL_PERMISSIONS,
            bonusPermission: INITIAL_PERMISSIONS,
            commissionPermission: INITIAL_PERMISSIONS,
            deductionPermission: INITIAL_PERMISSIONS,
            allowancePermission: INITIAL_PERMISSIONS,
            adjustmentPermission: INITIAL_PERMISSIONS,
            leaveCreditPermission: INITIAL_PERMISSIONS,
            filedLeavesPermission: INITIAL_PERMISSIONS,
            workflowEntitlementPermission: INITIAL_PERMISSIONS,
            basicPayAdjustmentsPermission: INITIAL_PERMISSIONS,
            workflowPermission: INITIAL_PERMISSIONS,
            historicalEarningsPermission: INITIAL_PERMISSIONS,
            annualEarningsPermission: INITIAL_PERMISSIONS,
            earningsPermission: INITIAL_PERMISSIONS,
            terminationInformationPermission: INITIAL_PERMISSIONS,
            showDeleteConfirmationModal: false,
            is_subscribed_to_payroll: false,
            is_subscribed_to_ta: false,
            is_subscribed_to_payroll_only: false,
            is_subscribed_to_ta_only: false,
            is_subscribed_to_ta_plus_payroll: false,
            is_employee_subscribed_to_payroll: false,
            showUpdateEmailConfirm: false,
            showContributionErrorOnTerminateEmployee: false
        };

        this.renderBasicInformation = this.renderBasicInformation.bind( this );
        this.renderEmploymentDetails = this.renderEmploymentDetails.bind( this );
        this.renderPayrollInformation = this.renderPayrollInformation.bind( this );
        this.renderEmployeeApprovalGroups = this.renderEmployeeApprovalGroups.bind( this );
        this.renderEmployeeAnnualEarnings = this.renderEmployeeAnnualEarnings.bind( this );
        this.renderEmployeeBasicPayAdjustments = this.renderEmployeeBasicPayAdjustments.bind( this );
        this.renderEmployeeLeaveCredits = this.renderEmployeeLeaveCredits.bind( this );
        this.renderEmployeeFiledLeaves = this.renderEmployeeFiledLeaves.bind( this );
        this.renderEmployeeLoans = this.renderEmployeeLoans.bind( this );
        this.renderEmployeeDeductions = this.renderEmployeeDeductions.bind( this );
        this.renderEmployeeAdjustments = this.renderEmployeeAdjustments.bind( this );
        this.renderEmployeeAllowances = this.renderEmployeeAllowances.bind( this );
        this.renderEmployeeBonuses = this.renderEmployeeBonuses.bind( this );
        this.renderEmployeeCommissions = this.renderEmployeeCommissions.bind( this );
        this.renderEmployeePaymentMethods = this.renderEmployeePaymentMethods.bind( this );
        this.renderEmployeeTerminationInformation = this.renderEmployeeTerminationInformation.bind( this );
    }

    /**
      * authorization check for editing employee data
      */
    componentWillMount() {
        isAuthorized([
            'edit.employee'
        ], ( authorized ) => {
            this.setState({ authorized });
        });

        isAuthorized([
            'view.payroll_loans',
            'create.payroll_loans',
            'delete.payroll_loans',
            'edit.payroll_loans',
            'view.leave_credit',
            'create.leave_credit',
            'edit.leave_credit',
            'view.leave_request',
            'create.leave_request',
            'edit.leave_request',
            'view.workflow_entitlement',
            'create.workflow_entitlement',
            'view.workflow',
            'view.basic_pay_adjustment',
            'create.basic_pay_adjustment',
            'edit.basic_pay_adjustment',
            'view.earnings',
            'create.earnings',
            'edit.earnings'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.payroll_loans' ];

            if ( authorized ) {
                this.setState({
                    loanPermission: {
                        view: authorization[ 'view.payroll_loans' ],
                        create: authorization[ 'create.payroll_loans' ],
                        delete: authorization[ 'delete.payroll_loans' ],
                        edit: authorization[ 'edit.payroll_loans' ]
                    },
                    leaveCreditPermission: {
                        view: authorization[ 'view.leave_credit' ],
                        create: authorization[ 'create.leave_credit' ],
                        edit: authorization[ 'edit.leave_credit' ]
                    },
                    filedLeavesPermission: {
                        view: authorization[ 'view.leave_request' ],
                        create: authorization[ 'create.leave_request' ],
                        edit: authorization[ 'edit.leave_request' ]
                    },
                    workflowEntitlementPermission: {
                        view: authorization[ 'view.workflow_entitlement' ],
                        create: authorization[ 'create.workflow_entitlement' ]
                    },
                    workflowPermission: {
                        view: authorization[ 'view.workflow' ]
                    },
                    earningsPermission: {
                        view: authorization[ 'view.earnings' ],
                        create: authorization[ 'create.earnings' ],
                        edit: authorization[ 'edit.earnings' ]
                    },
                    basicPayAdjustments: {
                        view: authorization[ 'view.basic_pay_adjustment' ],
                        create: authorization[ 'create.basic_pay_adjustment' ],
                        edit: authorization[ 'edit.basic_pay_adjustment' ]
                    },
                    paymentMethodPermission: { // TODO: temporary until RBAC is implemented
                        view: true,
                        create: true,
                        delete: true,
                        edit: true
                    }
                });
            }
        });
    }

    componentDidMount() {
        // eslint-disable-next-line react/no-did-mount-set-state
        this.updateSubscriptionState();
    }

    componentDidUpdate( prevProps ) {
        /**
         * Update state that determines if user has payroll subscription.
         *
         * If the state isn't updated, the content sections
         * won't be displayed as the value might remain to
         * be always `false`.
         */
        if ( prevProps.products !== this.props.products ) {
            this.updateSubscriptionState();
        }
    }

    updateSubscriptionState() {
        this.setState({
            is_subscribed_to_payroll: Boolean(
                    this.props.products
                        && subscriptionService.isSubscribedToPayroll( this.props.products )
                ),
            is_subscribed_to_ta: Boolean(
                    this.props.products &&
                        subscriptionService.isSubscribedToTA( this.props.products )
                ),
            is_subscribed_to_payroll_only: Boolean(
                    this.props.products &&
                        subscriptionService.isSubscribedToPayrollOnly( this.props.products )
                ),
            is_subscribed_to_ta_only: Boolean(
                    this.props.products &&
                        subscriptionService.isSubscribedToTAOnly( this.props.products )
                ),
            is_subscribed_to_ta_plus_payroll: Boolean(
                    this.props.products &&
                        subscriptionService.isSubscribedToBothPayrollAndTA( this.props.products )
                )
        });
    }

    /**
     * Handle actions when component receive new props
     */
    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.employee.data, this.props.employee.data ) ) {
            this.setState({
                employee: nextProps.employee.data,
                employeeEdit: {},
                edit: { basic: false, employment: false, payroll: false },
                is_employee_subscribed_to_payroll: nextProps.employee.data.subscription_license.product
                    && subscriptionService.isSubscribedToPayroll([
                        nextProps.employee.data.subscription_license.product
                    ])
            });
        }

        if ( nextProps.employee.loading !== this.props.employee.loading ) {
            this.enableActionButtons( nextProps );
        }

        if ( nextProps.employee.saving !== this.props.employee.saving ) {
            this.enableActionButtons( nextProps );
        }

        if ( !isEqual( nextProps.employee.errors, this.props.employee.errors ) ) {
            this.handleApiErrors( nextProps.employee.errors );
        }

        if ( nextProps.currentSection && !isEqual( nextProps.currentSection, this.props.currentSection ) ) {
            if ( !nextProps.employee.saving && !nextProps.employee.laoding ) {
                this.setState({
                    edit: {
                        basic: false,
                        employment: false,
                        payroll: false,
                        time_attendance: false
                    }
                });
            }
        }
    }

    /**
     * Sets the section edit mode
     */
    setEditMode( section, edit ) {
        edit && this.fetchDataIfNeeded( section.toUpperCase() );

        const setMode = () => {
            const editClone = Object.assign({}, this.state.edit, { [ section ]: edit });
            this.setState({
                edit: editClone,
                employeeEdit: {},
                employee: this.props.employee.data
            }, () => {
                this.enableActionButtons( this.props );
            });
        };

        if ( !edit && !this.props.canLoadEmployeeProfile( setMode ) ) {
            return;
        }

        setMode();
    }

    /**
     * formats form options value to display
     */
    getFormOptionLabel( formOptions, value ) {
        if ( formOptions && value ) {
            const formOption = formOptions.find( ( item ) => item.value === value.toUpperCase() );
            return formOption.label;
        }
        return value;
    }

    /**
     * Returns the value for field
     */
    getEmployeeValueForField( field ) {
        const { employee, employeeEdit } = this.state;

        /**
         * Field is edited
         */
        if ( has( employeeEdit, field ) ) {
            return employeeEdit[ field ];
        }

        if ( has( employee, field ) ) {
            return employee[ field ];
        }

        if ( has( employee, `payroll.${field}` ) ) {
            return employee.payroll[ field ];
        }

        if ( has( employee, `time_attendance.${field}` ) ) {
            return employee.time_attendance[ field ];
        }

        return '';
    }

    /**
     * Gets input types depending on the license
     *
     * @param {Object} fields - config for the specific section
     * @returns {Object}
     */
    getFieldTypes( fields ) {
        const inputTypes = fields.sections.reduce( ( inputs, section ) => {
            flatten( section.rows ).forEach( ( field ) => {
                if ( field.field_type !== '' ) {
                    const key = snakeCase( field.field_type );

                    if ( inputs[ key ]) {
                        inputs[ key ].push( field.id );
                    }
                }
            });

            return inputs;
        }, {
            input: [],
            select: [],
            date_picker: [],
            switch_2: []
        });

        return inputTypes;
    }

    /**
     * Gets onChange handler for form fields
     *
     * @param {String} id - ID of field
     * @param {String} type - Field Type
     * @returns {Function}
     */
    getOnChangeHandler( id, type ) {
        if ( type === 'select' ) {
            switch ( id ) {
                case 'payroll_group_id':
                    return ( value ) => {
                        this.updateEmployee({
                            payroll_group_id: value.value,
                            payroll_group_name: value.label
                        });
                    };
                case 'rank_id':
                    return ( value ) => {
                        this.updateEmployee({
                            rank_id: value ? value.value : null,
                            rank_name: value ? value.label : ''
                        });
                    };
                case 'primary_location_id':
                    return ( value ) => {
                        this.updateEmployee({
                            primary_location_id: value.value,
                            primary_location_name: value.label
                        });
                    };
                case 'employment_type_id':
                    return ( value ) => {
                        this.updateEmployee({
                            employment_type_id: value ? value.value : null,
                            employment_type_name: value ? value.label : ''
                        });
                    };
                case 'team_id':
                    return ( value ) => {
                        this.updateEmployee({
                            team_id: value ? value.value : null,
                            team_name: value ? value.label : ''
                        });
                    };
                case 'secondary_location_id':
                    return ( value ) => {
                        this.updateEmployee({
                            secondary_location_id: value.value,
                            secondary_location_name: value.label
                        });
                    };
                case 'position_id':
                    return ( value ) => {
                        this.updateEmployee({
                            position_id: value ? value.value : null,
                            position_name: value ? value.label : ''
                        });
                    };
                case 'department_id':
                    return ( value ) => {
                        this.updateEmployee({
                            department_id: value ? value.value : null,
                            department_name: value ? value.label : ''
                        });
                    };
                case 'cost_center_id':
                    return ( value ) => {
                        this.updateEmployee({
                            cost_center_id: !Array.isArray( value ) ? value.value : null,
                            cost_center_name: !Array.isArray( value ) ? value.label : ''
                        });
                    };
                case 'tax_type':
                    return ({ value }) => {
                        if ( value !== CONSULTANT && this.consultant_tax_rate ) {
                            this.consultant_tax_rate.setState({
                                value: '',
                                error: false
                            }, () => this.updateEmployee({ consultant_tax_rate: 0 }) );
                        }

                        this.updateEmployee({ [ id ]: value });
                    };
                case 'sss_basis':
                    return ({ value }) => {
                        if ( value !== FIXED ) {
                            this.sss_amount.setState({
                                value: '',
                                error: ''
                            }, () =>
                                this.updateEmployee({ sss_amount: 0 })
                            );
                        }

                        this.updateEmployee({ [ id ]: value });
                    };
                case 'philhealth_basis':
                    return ({ value }) => {
                        if ( value !== FIXED ) {
                            this.philhealth_amount.setState({
                                value: '',
                                error: ''
                            }, () =>
                                this.updateEmployee({ philhealth_amount: 0 })
                            );
                        }

                        this.updateEmployee({ [ id ]: value });
                    };
                case 'hdmf_basis':
                    return ({ value }) => {
                        if ( value !== FIXED ) {
                            this.hdmf_amount.setState({
                                value: '',
                                error: ''
                            }, () =>
                                this.updateEmployee({ hdmf_amount: 0 })
                            );
                        }

                        this.updateEmployee({ [ id ]: value });
                    };
                default:
                    return ( value ) => {
                        this.updateEmployee({ [ id ]: value ? value.value : null });
                    };
            }
        }

        switch ( id ) {
            case 'birth_date':
            case 'date_hired':
            case 'date_ended':
                return ( value ) => {
                    const formatted = formatDate( value, id === 'birth_date' ? 'MM/DD/YYYY' : DATE_FORMATS.API_2 );
                    this.updateEmployee({ [ id ]: formatted });
                };
            case 'mobile_number':
                return ( value ) => {
                    const formatted = formatMobileNumber( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateEmployee({ [ id ]: formatted })
                    );
                };
            case 'telephone_number':
                return ( value ) => {
                    const clean = stripNonNumeric( value );
                    const formatted = clean.substring( 0, 7 );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateEmployee({ [ id ]: formatted })
                    );
                };
            case 'zip_code':
                return ( value ) => {
                    const formatted = stripNonNumeric( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateEmployee({ [ id ]: formatted })
                    );
                };
            case 'sss_amount':
            case 'philhealth_amount':
            case 'hdmf_amount':
            case 'hours_per_day':
            case 'base_pay':
                return ( value ) => {
                    const clean = stripNonDigit( value );
                    let formatted = clean;

                    if ( id === 'hours_per_day' && !isNaN( clean ) ) {
                        formatted = Number( clean ) > 24 ? '24' : clean;
                    }

                    this[ id ].setState({ value: formatted }, () =>
                        this.updateEmployee({ [ id ]: formatted })
                    );
                };
            case 'timesheet_required':
                return ( value ) => {
                    this.updateEmployee({ [ id ]: value }, () => {
                        if ( !value ) {
                            this.updateEmployee({
                                regular_holiday_pay: true,
                                special_holiday_pay: true
                            });
                        }
                    });
                };
            case 'rdo':
                return ( value ) => {
                    const formatted = formatRDO( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateEmployee({ [ id ]: formatted })
                    );
                };
            case 'sss_number':
                return ( value ) => {
                    const formatted = formatSSS( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateEmployee({ [ id ]: formatted })
                    );
                };
            case 'tin':
                return ( value ) => {
                    const formatted = formatTIN( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateEmployee({ [ id ]: formatted })
                    );
                };
            case 'hdmf_number':
                return ( value ) => {
                    const formatted = formatHDMF( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateEmployee({ [ id ]: formatted })
                    );
                };
            case 'philhealth_number':
                return ( value ) => {
                    const formatted = formatPhilHealth( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateEmployee({ [ id ]: formatted })
                    );
                };
            case 'consultant_tax_rate':
                return ( value ) => {
                    let formatted = formatNumber( value, false, 0 );
                    formatted = Number( formatted ) > 100 ? '100' : formatted;
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateEmployee({ [ id ]: formatted })
                    );
                };
            case 'active':
                return ( value ) => this.updateEmployee({ [ id ]: value, auth0_active: value });
            default:
                return ( value ) => this.updateEmployee({ [ id ]: value });
        }
    }

    /**
     * Computes duration of date hired and last working date
     * @param {string} start - Date start
     * @param {string} end - Date end
     * @returns {object}
     */
    getDurationOfService( start, end ) {
        const dateHired = moment( start, DATE_FORMATS.API );
        const lastWorkedDayDate = moment( end, DATE_FORMATS.API );

        const months = lastWorkedDayDate.diff( dateHired, 'months', true );

        const year = Math.floor( months / 12 );
        const month = Math.floor( months ) % 12;
        const day = Math.round( ( months % 1 ) * dateHired.daysInMonth(), 0 );

        return { year, month, day };
    }

    fetchDataIfNeeded = ( section ) => {
        if ( section === EMPLOYEE_SECTIONS.EMPLOYMENT ) {
            if ( ( this.state.is_subscribed_to_payroll && this.props.formOptions.costCenters.length > 0 )
                    || ( this.state.is_subscribed_to_ta && this.props.formOptions.ranks.length > 0 ) ) {
                return;
            }
        }
        if ( section === EMPLOYEE_SECTIONS.PAYROLL && this.props.formOptions.payrollGroups.length ) return;
        if ( section === EMPLOYEE_SECTIONS.TIME_ATTENDANCE && this.props.formOptions.employmentTypes.length ) return;
        this.props.getDataNecessaryForEdit( section );
    }

    /**
     * Enable or disable action buttons
     */
    enableActionButtons( props ) {
        const { edit } = this.state;
        const disabled = edit.basic || edit.employment || edit.payroll || edit.time_attendance || props.employee.loading || props.employee.saving;

        this.buttonPrevious && this.buttonPrevious.setState({ disabled });
        this.buttonNext && this.buttonNext.setState({ disabled });
        this.buttonCopy && this.buttonCopy.setState({ disabled });
        this.buttonExport && this.buttonExport.setState({ disabled });
    }

    /**
     * updates the employee state with the form inputs
     * and stores edited values in store
     */
    updateEmployee( fields, callback ) {
        this.setState( ( prevState ) => ({
            employeeEdit: Object.assign({}, prevState.employeeEdit, fields )
        }), () => {
            callback && callback();
        });
    }

    /**
     * Check government ids length
     */
    checkIdLength( value, length ) {
        return value && stripNonNumeric( value ).length === length;
    }

    /**
     * Validates basic section entries
     */
    validateBasicSection() {
        let valid = true;
        if ( this.first_name._validate( this.first_name.state.value ) ) {
            valid = false;
        }

        if ( this.middle_name._validate( this.middle_name.state.value ) ) {
            valid = false;
        }

        if ( this.last_name._validate( this.last_name.state.value ) ) {
            valid = false;
        }

        if ( this.status && !this.status._checkRequire( typeof this.status.state.value === 'object' && this.status.state.value !== null ? this.status.state.value.value : this.status.state.value ) ) {
            valid = false;
        }

        if ( this.email._validate( this.email.state.value ) ) {
            valid = false;
        }

        if ( this.telephone_number._validate( this.telephone_number.state.value ) ) {
            valid = false;
        }

        if ( this.address_line_1._validate( this.address_line_1.state.value ) ) {
            valid = false;
        }

        return valid;
    }

    /**
     * Validates employment section entries
     */
    validateEmploymentSection() {
        const {
            is_subscribed_to_ta_only: isSubscribedToTAOnly,
            is_subscribed_to_payroll_only: isSubscribedToPayrollOnly
        } = this.state;

        const fields = isSubscribedToTAOnly ? TA_FIELDS.EMPLOYMENT_DETAILS : (
            isSubscribedToPayrollOnly ? PAYROLL_FIELDS.EMPLOYMENT_DETAILS : (
                TA_PLUS_PAYROLL_FIELDS.EMPLOYMENT_DETAILS
            )
        );

        const fieldTypes = this.getFieldTypes( fields );

        let valid = true;

        fieldTypes.input.forEach( ( input ) => {
            const field = this[ input ];
            if ( field._validate( field.state.value ) ) {
                valid = false;
            }
        });

        fieldTypes.select.forEach( ( select ) => {
            const field = this[ select ];
            if ( !field._checkRequire( typeof field.state.value === 'object' && field.state.value !== null ? field.state.value.value : field.state.value ) ) {
                valid = false;
            }
        });

        fieldTypes.date_picker.forEach( ( datePicker ) => {
            const field = this[ datePicker ];
            if ( field && field.checkRequired() ) {
                valid = false;
            }
        });

        return valid;
    }

    /**
     * Validates payroll section entries
     */
    validatePayrollSection() {
        const fieldTypes = this.getFieldTypes( PAYROLL_FIELDS.PAYROLL_INFORMATION );

        let valid = true;

        fieldTypes.input.forEach( ( fieldName ) => {
            const field = this[ fieldName ];

            if ( field._validate( field.state.value ) ) {
                valid = false;
            }
        });

        fieldTypes.select.forEach( ( fieldName ) => {
            const field = this[ fieldName ];
            const fieldValue = get( field, 'state.value.value' ) || get( field, 'state.value' );

            const validationResult = field._checkRequire(
                fieldValue,
                CUSTOM_ERROR_MESSAGES[ fieldName ]
            );

            if ( !validationResult ) {
                valid = false;
            }
        });

        fieldTypes.date_picker.forEach( ( fieldName ) => {
            const field = this[ fieldName ];

            if ( field.checkRequired() ) {
                valid = false;
            }
        });

        return valid;
    }

    /**
     * Validate entries
     */
    validate() {
        switch ( true ) {
            case this.state.edit.basic:
                return this.validateBasicSection();
            case this.state.edit.employment:
                return this.validateEmploymentSection();
            case this.state.edit.payroll:
                return this.validatePayrollSection();
            default:
                return false;
        }
    }

    /**
     * Check email input
     */
    checkEmail( section ) {
        const {
            employeeEdit
        } = this.state;

        if ( has( employeeEdit, 'email' ) ) {
            this.setState({ showUpdateEmailConfirm: false }, () => {
                this.setState({ showUpdateEmailConfirm: true });
            });
        } else {
            this.saveSection( section );
        }
    }

    /**
     * Saves changes of a section
     */
    saveSection( section ) {
        if ( this.validate() ) {
            const {
                employeeEdit,
                edit,
                is_subscribed_to_ta: isSubscribedToTA
            } = this.state;

            const product_seat_id = Math.max( ...this.props.userInformation.product_seats.map( ( seat ) => seat.id ) );
            const employee = Object.assign({}, employeeEdit, { product_seat_id });

            employee.position_name = this.getEmployeeValueForField( 'position_name' );
            employee.active = this.getEmployeeValueForField( 'active' );

            if ( has( employeeEdit, 'date_ended' ) && !has( employeeEdit, 'date_hired' ) ) {
                employee.date_hired = formatDate( this.getEmployeeValueForField( 'date_hired' ), DATE_FORMATS.API_2 );
            }

            if ( section === 'payroll' ) {
                employee.base_pay_unit = this.getEmployeeValueForField( 'base_pay_unit' );
                employee.consultant_tax_rate = this.getEmployeeValueForField( 'consultant_tax_rate' );
                employee.hdmf_amount = this.getEmployeeValueForField( 'hdmf_amount' );
                employee.hdmf_basis = this.getEmployeeValueForField( 'hdmf_basis' );
                employee.hours_per_day = this.getEmployeeValueForField( 'hours_per_day' );
                employee.payroll_group_name = this.getEmployeeValueForField( 'payroll_group_name' );
                employee.philhealth_amount = this.getEmployeeValueForField( 'philhealth_amount' );
                employee.philhealth_basis = this.getEmployeeValueForField( 'philhealth_basis' );
                employee.sss_amount = this.getEmployeeValueForField( 'sss_amount' );
                employee.sss_basis = this.getEmployeeValueForField( 'sss_basis' );
                employee.tax_status = this.getEmployeeValueForField( 'tax_status' );
                employee.tax_type = this.getEmployeeValueForField( 'tax_type' );
            }

            switch ( true ) {
                case edit.employment:
                    if ( isSubscribedToTA ) {
                        employee.rank_id && delete employee.rank_id;
                        employee.primary_location_id && delete employee.primary_location_id;
                        employee.employment_type_id && delete employee.employment_type_id;
                        employee.team_id && delete employee.team_id;
                        employee.secondary_location_id && delete employee.secondary_location_id;
                        employee.position_id && delete employee.position_id;
                        employee.department_id && delete employee.department_id;
                    } else {
                        employee.location_id && delete employee.location_id;
                        employee.department_id && delete employee.department_id;
                        employee.rank_id && delete employee.rank_id;
                        employee.cost_center_id && delete employee.cost_center_id;
                    }
                    break;
                case edit.payroll:
                    employee.payroll_group_id && delete employee.payroll_group_id;
                    employee.tax_type && employee.tax_type !== CONSULTANT && delete employee.consultant_tax_rate;
                    break;
                default:
                    break;
            }

            if ( employee.payroll_group_name || employee.base_pay_unit ) {
                employee.base_pay = this.getEmployeeValueForField( 'base_pay' );
            }

            this.props.saveEmployeeProfile( this.state.employee.id, employee, section.toUpperCase() );
        }
    }

    /**
     * handles display of errors from API
     */
    handleApiErrors( errors ) {
        let fields;

        switch ( true ) {
            case this.state.edit.basic:
                fields = {
                    first_name: 'first_name',
                    middle_name: 'middle_name',
                    last_name: 'last_name',
                    email: 'email',
                    gender: 'gender',
                    birth_date: 'birth_date',
                    telephone_number: 'telephone_number',
                    mobile_number: 'mobile_number',
                    address_line_1: 'address_line_1',
                    address_line_2: 'address_line_2',
                    city: 'city',
                    zip_code: 'zip_code',
                    country: 'country',
                    status: 'status'
                };
                break;
            case this.state.edit.employment:
                fields = {
                    employee_id: 'employee_id',
                    active: 'active',
                    hours_per_day: 'hours_per_day',
                    department_name: 'department_name',
                    rank_name: 'rank_name',
                    cost_center_id: 'cost_center_name',
                    tin: 'tin',
                    sss_number: 'sss_number',
                    philhealth_number: 'philhealth_number',
                    hdmf_number: 'hdmf_number',
                    rdo: 'rdo',
                    date_hired: 'date_hired',
                    date_ended: 'date_ended',
                    employment_type: 'employment_type',
                    position: 'position',
                    team: 'team_name',
                    team_role: 'team_role',
                    primary_location: 'primary_location',
                    secondary_location: 'secondary_location'
                };
                break;
            case this.state.edit.payroll:
                fields = {
                    payroll_group_id: 'payroll_group_name',
                    base_pay: 'base_pay',
                    base_pay_unit: 'base_pay_unit',
                    hours_per_day: 'hours_per_day',
                    tax_status: 'tax_status',
                    tax_type: 'tax_type',
                    consultant_tax_rate: 'consultant_tax_rate',
                    sss_basis: 'sss_basis',
                    philhealth_basis: 'philhealth_basis',
                    hdmf_basis: 'hdmf_basis',
                    sss_amount: 'sss_amount',
                    philhealth_amount: 'philhealth_amount',
                    hdmf_amount: 'hdmf_amount'
                };
                break;
            default:
                return;
        }

        Object.keys( fields ).forEach( ( field ) => {
            if ( this[ field ]) {
                const errorKey = fields[ field ];
                const message = errors[ errorKey ] ? errors[ errorKey ][ 0 ] : null;

                switch ( field ) {
                    case 'date_hired':
                    case 'date_ended':
                        if ( message ) {
                            this[ field ].setState({ error: true, message });
                        } else {
                            this[ field ].setState({ error: false, message: '' });
                        }
                        break;
                    default:
                        if ( message ) {
                            this[ field ].setState({ error: true, errorMessage: message });
                        } else {
                            this[ field ].setState({ error: false, errorMessage: '' });
                        }
                        break;
                }
            }
        });
    }

    /**
     * Redirect to add single employee page for copying current employe profile
     */
    duplicateEmployee() {
        localStorage.setItem( 'clone_employee', this.state.employee.id );
        browserHistory.push( '/employees/add', true );
    }

    scrollTo = ( sectionId ) => {
        const section = document.getElementById( sectionId );

        if ( this.element && section ) {
            const top = section.offsetTop - this.element.scrollTop - 44;

            this.element.scrollBy({ top, left: 0, behavior: 'smooth' });
        }
    }

    /**
     * Builds additional config for input field provided
     *
     * @param {Object} field - Details of field to be rendered
     * @returns {Object}
     */
    buildAdditionalInputConfig( field ) {
        const { id } = field;

        let value = this.getEmployeeValueForField( id );
        let extraConfig = {};
        let isFixedBasis;
        let isConsultant;

        switch ( id ) {
            case 'base_pay':
                extraConfig = {
                    onFocus: () => this[ id ].setState({ value: stripNonDigit( value ) }),
                    onBlur: ( _value ) => {
                        const formattedValue = formatBasePay( _value );

                        this[ id ].setState(
                            { value: formattedValue },
                            () => this.updateEmployee({ [ id ]: formattedValue })
                        );
                    },
                    validations: { required: this.requireTaxTypeRelatedField() && CUSTOM_ERROR_MESSAGES.base_pay }
                };

                break;
            case 'consultant_tax_rate':
                isConsultant = this.getEmployeeValueForField( 'tax_type' ) === CONSULTANT;
                value = isConsultant ? formatNumber( value, false, 0 ) : '';
                extraConfig = {
                    hide: !isConsultant,
                    disabled: !isConsultant,
                    validations: {
                        required: isConsultant
                    }
                };

                break;
            case 'sss_amount':
                isFixedBasis = this.getEmployeeValueForField( 'sss_basis' ) === FIXED;
                value = isFixedBasis ? stripNonDigit( value ) : '';
                extraConfig = {
                    disabled: !isFixedBasis,
                    onFocus: () => this[ id ].setState({ value: formatNumber( value, false, 2 ) }),
                    onBlur: ( _value ) => this[ id ].setState({ value: formatNumber( _value ) }, () =>
                        this.updateEmployee({ [ id ]: _value })
                    ),
                    validations: {
                        required: isFixedBasis
                    }
                };

                break;
            case 'philhealth_amount':
                isFixedBasis = this.getEmployeeValueForField( 'philhealth_basis' ) === FIXED;
                value = isFixedBasis ? stripNonDigit( value ) : '';
                extraConfig = {
                    disabled: !isFixedBasis,
                    onFocus: () => this[ id ].setState({ value: formatNumber( value, false, 2 ) }),
                    onBlur: ( _value ) => this[ id ].setState({ value: formatNumber( _value ) }, () =>
                        this.updateEmployee({ [ id ]: _value })
                    ),
                    validations: {
                        required: isFixedBasis
                    }
                };

                break;
            case 'hdmf_amount':
                isFixedBasis = this.getEmployeeValueForField( 'hdmf_basis' ) === FIXED;
                value = isFixedBasis ? stripNonDigit( value ) : '';
                extraConfig = {
                    disabled: !isFixedBasis,
                    onFocus: () => this[ id ].setState({ value: formatNumber( value, false, 2 ) }),
                    onBlur: ( _value ) => this[ id ].setState({ value: formatNumber( _value ) }, () =>
                        this.updateEmployee({ [ id ]: _value })
                    ),
                    validations: {
                        required: isFixedBasis
                    }
                };

                break;
            default:
        }

        return {
            value,
            max: get( field, 'validations.max', null ),
            ...extraConfig
        };
    }

    requireContributionBasisFields = () => {
        const hdmfBasis = this.getEmployeeValueForField( 'hdmf_basis' );
        const philhealthBasis = this.getEmployeeValueForField( 'philhealth_basis' );
        const sssBasis = this.getEmployeeValueForField( 'sss_basis' );

        return hdmfBasis || philhealthBasis || sssBasis;
    }

    buildAdditionalSelectConfig( field ) {
        const { formOptions } = this.props;

        const { options } = field;

        const config = {
            value: field.id === 'base_pay_unit'
                ? toUpper( this.getEmployeeValueForField( field.id ) )
                : this.getEmployeeValueForField( field.id ),
            data: Array.isArray( options )
                ? options
                : get( formOptions, options, get( formOptions, `constants.${options}`, []) )
        };

        const requireTaxTypeRelatedField = [
            'payroll_group_id',
            'base_pay_unit',
            'tax_type'
        ].includes( field.id ) && this.requireTaxTypeRelatedField();

        const requireContributionBasisFields = [
            'hdmf_basis',
            'philhealth_basis',
            'sss_basis'
        ].includes( field.id ) && this.requireContributionBasisFields();

        if ( requireTaxTypeRelatedField || requireContributionBasisFields ) {
            config.validations = {
                required: true
            };
        }

        return config;
    }

    /**
     * Builds additional config for date picker field provided
     *
     * @param {Object} field - Details of field to be rendered
     * @returns {Object}
     */
    buildAdditionalDatePickerConfig( field ) {
        const { terminationInformation } = this.props.employee;

        let disabledDays = [];
        let selectedDay = this.getEmployeeValueForField( field.id );
        let disabled = false;

        switch ( field.id ) {
            case 'birth_date':
                disabledDays = [{ after: new Date() }];
                break;
            case 'date_ended':
                disabledDays = [{ before: new Date( this.getEmployeeValueForField( 'date_hired' ) ) }];
                selectedDay = selectedDay || get( terminationInformation, 'last_date', '' );
                disabled = !isEmpty( terminationInformation ) && !this.state.is_subscribed_to_ta_only;
                break;
            default:
        }

        return {
            disabledDays,
            selectedDay,
            disabled
        };
    }

    requireTaxTypeRelatedField = () => {
        const taxType = this.getEmployeeValueForField( 'tax_type' );
        const payrollGroupId = this.getEmployeeValueForField( 'payroll_group_id' );
        const basePay = this.getEmployeeValueForField( 'base_pay' );
        const basePayUnit = this.getEmployeeValueForField( 'base_pay_unit' );

        return Boolean( taxType || payrollGroupId || basePay || basePayUnit );
    }

    /**
     * Converts duration of tenure into human-readable string
     * @param {object} duration       - Duration of tenure
     * @param {number} duration.year  - Years of tenure
     * @param {number} duration.month - Months of tenure, less years
     * @param {number} duration.day   - Days of tenure, less years and months
     * @returns {string}
     */
    formatDurationToString({ year, month, day }) {
        if ( year === 0 && month === 0 && day === 0 ) {
            return '0 days';
        }

        let yearString = '';
        if ( year > 0 ) {
            yearString = `${year} ${year > 1 ? 'years' : 'year'}`;
        }

        let monthString = '';
        if ( month > 0 ) {
            monthString = `${year ? ' ' : ''}${month} ${month > 1 ? 'months' : 'month'}`;
        }

        let dayString = '';
        if ( day > 0 ) {
            dayString = `${( year || month ) ? ' ' : ''}${day} ${day > 1 ? 'days' : 'day'}`;
        }

        return `${yearString}${monthString}${dayString}`;
    }

    /**
     * Renders field according to config provided
     *
     * @param {Object} fieldConfig - Details of field to be rendered
     * @returns {Object}
     */
    renderFieldForView( fieldConfig ) {
        const { constants } = this.props.formOptions;
        const {
            id,
            class_name: className,
            label,
            placeholder,
            field_type: fieldType
        } = fieldConfig;

        let _id = id;
        if ( fieldType === 'select' ) {
            _id = id.replace( new RegExp( /id$/ ), 'name' );
        }

        let value;
        let govContributionId;
        let hideField;
        switch ( _id ) {
            case 'active':
                value = startCase( this.getEmployeeValueForField( _id ) );
                break;
            case 'hours_per_day': {
                const fieldValue = this.getEmployeeValueForField( _id );
                value = fieldValue ? `${formatNumber( fieldValue, false )} Hour${fieldValue > 1 ? 's' : ''}` : '-';
            }
                break;
            case 'team_role':
                value = capitalize( this.getEmployeeValueForField( _id ) );
                break;
            case 'sss_amount':
            case 'philhealth_amount':
            case 'hdmf_amount':
                govContributionId = _id.split( '_' )[ 0 ];
                value = this.getEmployeeValueForField( `${govContributionId}_basis` ) === FIXED ? `Php ${formatNumber( this.getEmployeeValueForField( _id ) )}` : 'System Calculated';
                break;
            case 'timesheet_required':
            case 'rest_day_pay':
            case 'regular_holiday_pay':
            case 'holiday_premium_pay':
            case 'special_holiday_pay':
            case 'differential':
            case 'overtime':
            case 'entitled_deminimis':
                value = this.getEmployeeValueForField( _id ) ? 'Yes' : 'No';
                break;
            case 'base_pay':
                value = formatBasePay( parseFloat( stripNonDigit( this.getEmployeeValueForField( _id ) ) ) ) || '';
                break;
            case 'cost_center':
            case 'base_pay_unit':
            case 'tax_type':
            case 'sss_basis':
            case 'philhealth_basis':
            case 'hdmf_basis':
                value = this.getFormOptionLabel( constants[ _id ], this.getEmployeeValueForField( _id ) );
                break;
            case 'tax_status':
                value = `${this.getEmployeeValueForField( _id )} - ${this.getFormOptionLabel( constants[ _id ], this.getEmployeeValueForField( _id ) )}`;
                break;
            case 'consultant_tax_rate':
                value = formatNumber( this.getEmployeeValueForField( _id ), false, 0 );
                hideField = this.getEmployeeValueForField( 'tax_type' ).toUpperCase() !== CONSULTANT;
                break;
            case 'tenure_date': {
                const { terminationInformation } = this.props.employee;
                const employeeDateEnded = this.getEmployeeValueForField( 'date_ended' ) || get( terminationInformation, 'last_date', '' );

                const dateHired = this.getEmployeeValueForField( 'date_hired' );
                const durationOfService = this.getDurationOfService( dateHired, employeeDateEnded || moment().format( DATE_FORMATS.API ) );
                value = this.formatDurationToString( durationOfService );
                break;
            }
            case 'date_hired':
                value = formatDate( this.getEmployeeValueForField( _id ), DATE_FORMATS.DISPLAY );
                break;
            case 'btn_terminate': {
                hideField = this.props.employee.data.termination_information;
                value = (
                    <div className="terminate-button">
                        <Button
                            label="Add Termination Information"
                            type="danger"
                            size="small"
                            alt
                            onClick={ () => {
                                this.verifyFinalPayContributionBasis();
                            } }
                        />
                    </div>
                );
                break;
            }
            case 'date_ended': {
                const { terminationInformation } = this.props.employee;
                const lastDate = get( terminationInformation, 'last_date', _id );
                const hasTerminationInformation = !isEmpty( terminationInformation );

                if ( this.state.is_subscribed_to_ta_only ) {
                    hideField = false;
                } else {
                    hideField = !hasTerminationInformation;
                }

                value = formatDate( lastDate, DATE_FORMATS.DISPLAY );
            }
                break;
            default:
                value = this.getEmployeeValueForField( _id );
        }

        const fieldWrapperClass = className.split( ' ' )[ 0 ];
        return (
            <div className={ `${fieldWrapperClass} ${hideField ? 'hide' : ''}` } key={ `${id}_view` }>
                { fieldType && <div className={ 'field-label' }>{ `${label || placeholder}:` }</div> }
                { fieldType && <div className={ 'field-value' }>{ value || '' }</div> }
            </div>
        );
    }

    /**
     * Renders field according to config provided
     *
     * @param {Object} field - Details of field to be rendered
     * @returns {Object}
     */
    renderFieldForEdit( field ) {
        const isDemo = company.isDemoCompany();
        const {
            class_name: className,
            is_demo_field: isDemoField,
            ...rest
        } = field;

        let fieldConfig = {};
        switch ( field.field_type ) {
            case 'input':
                fieldConfig = this.buildAdditionalInputConfig( field );
                break;
            case 'select':
                fieldConfig = this.buildAdditionalSelectConfig( field );
                break;
            case 'date-picker':
                fieldConfig = this.buildAdditionalDatePickerConfig( field );
                break;
            case 'switch2':
                fieldConfig = {
                    checked: Boolean( this.getEmployeeValueForField( field.id ) ),
                    disabled: [ 'regular_holiday_pay', 'special_holiday_pay' ].includes( field.id ) && !this.getEmployeeValueForField( 'timesheet_required' )
                };
                break;
            default:
        }

        const config = {
            ...rest,
            ...fieldConfig,
            disabled: isDemo && isDemoField,
            onChange: this.getOnChangeHandler( field.id, field.field_type ),
            ref: ( ref ) => { this[ field.id ] = ref; }
        };

        const { terminationInformation } = this.props.employee;
        const hasTerminationInformation = !isEmpty( terminationInformation );

        if ( field.id === 'date_ended' && !hasTerminationInformation && !this.state.is_subscribed_to_ta_only ) {
            return false;
        }

        if ( field.id === 'btn_terminate' && !hasTerminationInformation ) {
            const disableButton = this.props.employee.data.termination_information;
            const fieldWrapperClass = className.split( ' ' )[ 0 ];
            const value = (
                <div className="terminate-button">
                    <Button
                        label="Add Termination Information"
                        type="danger"
                        size="small"
                        alt
                        disableButton={ disableButton }
                        onClick={ () => {
                            this.verifyFinalPayContributionBasis();
                        } }
                    />
                </div>
            );

            return (
                <div className={ `${fieldWrapperClass} field-value` }>
                    <div>{ `${field.label}:` }</div>
                    <div>{ value }</div>
                </div>
            );
        }

        return (
            <div className={ `${className} ${config.hide ? 'hide' : 'field-value'}` } key={ `${field.id}_edit` }>
                { field.field_type && renderField( config ) }
            </div>
        );
    }

    verifyFinalPayContributionBasis() {
        const employeeId = get( this.props.employee.data, 'id', null );
        const payroll = get( this.props.employee.data, 'payroll', null );
        const hdmfBasis = get( payroll, 'hdmf_basis', null );
        const philhealthBasis = get( payroll, 'philhealth_basis', null );
        const sssBasis = get( payroll, 'sss_basis', null );

        const hasBasis = ( hdmfBasis && philhealthBasis && sssBasis );

        if ( isEmpty( payroll ) || !hasBasis ) {
            this.setState({ showContributionErrorOnTerminateEmployee: false }, () => {
                this.setState({ showContributionErrorOnTerminateEmployee: true });
            });
        } else {
            browserHistory.push( `/employee/${employeeId}/terminate`, true );
        }
    }

    renderLoader = ( title, key, height ) => (
        <ProfileSectionWrapper key={ key }>
            <LoadingStyles minHeight={ height }>
                <H2>{ title }</H2>
                <br />
                <H3>Please wait...</H3>
            </LoadingStyles>
        </ProfileSectionWrapper>
    )

    renderCurrentSection( section ) {
        let currentSectionId = section;

        if ( !currentSectionId ) {
            currentSectionId = PROFILE_SECTION_IDS.BASIC_INFORMATION;
        }

        const sectionRenderMap = {
            [ PROFILE_SECTION_IDS.BASIC_INFORMATION ]: this.renderBasicInformation,
            [ PROFILE_SECTION_IDS.EMPLOYMENT_DETAILS ]: this.renderEmploymentDetails,
            [ PROFILE_SECTION_IDS.PAYROLL_INFORMATION ]: this.renderPayrollInformation,
            [ PROFILE_SECTION_IDS.APPROVAL_GROUPS ]: this.renderEmployeeApprovalGroups,
            [ PROFILE_SECTION_IDS.ANNUAL_EARNINGS ]: this.renderEmployeeAnnualEarnings,
            [ PROFILE_SECTION_IDS.BASIC_PAY_ADJUSTMENTS ]: this.renderEmployeeBasicPayAdjustments,
            [ PROFILE_SECTION_IDS.LEAVE_CREDITS ]: this.renderEmployeeLeaveCredits,
            [ PROFILE_SECTION_IDS.FILED_LEAVES ]: this.renderEmployeeFiledLeaves,
            [ PROFILE_SECTION_IDS.LOANS ]: this.renderEmployeeLoans,
            [ PROFILE_SECTION_IDS.DEDUCTIONS ]: this.renderEmployeeDeductions,
            [ PROFILE_SECTION_IDS.ADJUSTMENTS ]: this.renderEmployeeAdjustments,
            [ PROFILE_SECTION_IDS.ALLOWANCES ]: this.renderEmployeeAllowances,
            [ PROFILE_SECTION_IDS.BONUSES ]: this.renderEmployeeBonuses,
            [ PROFILE_SECTION_IDS.COMMISSIONS ]: this.renderEmployeeCommissions,
            [ PROFILE_SECTION_IDS.PAYMENT_METHODS ]: this.renderEmployeePaymentMethods,
            [ PROFILE_SECTION_IDS.TERMINATION_INFORMATION ]: this.renderEmployeeTerminationInformation
        };

        let renderedSection = '';
        const renderSection = sectionRenderMap[ currentSectionId ];
        if ( typeof renderSection === 'function' ) {
            renderedSection = renderSection();
        }

        return renderedSection;
    }

    /**
     * renders Personal Information section
     */
    renderBasicInformation() {
        const {
            edit,
            employee,
            authorized
        } = this.state;

        if ( edit.basic ) {
            if ( !this.props.employee.loadingBasicInformation ) {
                return this.renderEditBasicInformation();
            }
        }

        const renderLoading = ( edit.basic && this.props.employee.loadingBasicInformation );

        return (
            <div key="basic-information" id={ PROFILE_SECTION_IDS.BASIC_INFORMATION }>
                <ProfileSectionWrapper>
                    <div className="employee-header">
                        <div className="section-title">
                            <span>Personal Information</span>
                        </div>

                        <div>
                            {
                            authorized ? (
                                <Button
                                    label="Edit"
                                    type="action"
                                    alt
                                    disabled={ edit.employment || edit.payroll || edit.time_attendance || renderLoading }
                                    onClick={ () => this.setEditMode( 'basic', true ) }
                                />
                            ) : false
                        }
                        </div>
                    </div>
                    {
                        renderLoading
                        ? (
                            <LoadingStyles minHeight={ '530px' }>
                                <H2>{ 'Loading Personal Information' }</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        )
                        : (
                            <div>
                                <div className="section-heading mt-40">
                                    <span>Personal Details</span>
                                </div>
                                <div className="section-body">
                                    <div className="row">
                                        <div className="section-column col-xs-3">
                                            <div className="field-label">Name</div>
                                            <div className="field-value">{ startCase( toLower( this.getEmployeeValueForField( 'name' ) ) ) || '' }</div>
                                        </div>
                                        <div className="section-column col-xs-3">
                                            <div className="field-label">Gender</div>
                                            <div className="field-value">{ startCase( toLower( this.getEmployeeValueForField( 'gender' ) ) ) || '' }</div>
                                        </div>
                                        <div className="section-column col-xs-3">
                                            <div className="field-label">Marital Status</div>
                                            <div className="field-value">{ startCase( toLower( this.getEmployeeValueForField( 'status' ) ) ) || '' }</div>
                                        </div>
                                        <div className="section-column col-xs-3">
                                            <div className="field-label">Date of Birth</div>
                                            <div className="field-value">{ formatDate( this.getEmployeeValueForField( 'birth_date' ), DATE_FORMATS.DISPLAY ) || '' }</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="section-heading mt-5-percent">
                                    <span>Contact Details</span>
                                </div>
                                <div className="section-body">
                                    <div className="row">
                                        <div className="col-xs-3">
                                            <div className="field-label">Email Address:</div>
                                            <div className="field-value">{ this.getEmployeeValueForField( 'email' ) || '' }</div>
                                        </div>
                                        <div className="col-xs-3">
                                            <div className="field-label">Mobile Number</div>
                                            <div className="field-value">{ this.getEmployeeValueForField( 'mobile_number' ) || '' }</div>
                                        </div>
                                        <div className="col-xs-3">
                                            <div className="field-label">Telephone Number</div>
                                            <div className="field-value">{ this.getEmployeeValueForField( 'telephone_number' ) || '' }</div>
                                        </div>
                                        <div className="col-xs-12 mt-2-percent">
                                            <div className="field-label">Address:</div>
                                            <div className="field-value">
                                                {
                                                    employee.address_line_1 ? `${employee.address_line_1} ` : false
                                                }
                                                {
                                                    employee.formatted_address_line_2 ? `${employee.formatted_address_line_2} ` : false
                                                }
                                                {
                                                    employee.country ? `${employee.country}` : false
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                </ProfileSectionWrapper>
            </div>
        );
    }

    /**
     * renders Employment Details section
     */
    renderEmploymentDetails() {
        const {
            edit,
            authorized,
            is_subscribed_to_payroll_only: isSubscribedToPayrollOnly,
            is_subscribed_to_ta_only: isSubscribedToTAOnly,
            is_subscribed_to_ta_plus_payroll: isSubscribedToTAPlusPayroll
        } = this.state;

        if ( edit.employment ) {
            if ( !this.props.employee.loadingEmploymentInformation ) {
                return this.renderEditEmploymentDetails();
            }
        }

        const fields = isSubscribedToTAOnly ? TA_FIELDS : (
            isSubscribedToPayrollOnly ? PAYROLL_FIELDS : (
                isSubscribedToTAPlusPayroll ? TA_PLUS_PAYROLL_FIELDS
                    : null
            )
        );

        const renderLoading = ( edit.employment && this.props.employee.loadingEmploymentInformation );

        return (
            <ProfileSectionWrapper key="employment-details" id={ PROFILE_SECTION_IDS.EMPLOYMENT_DETAILS }>
                <div className="employee-header mb-40">
                    <div className="section-title">
                        <span>Employment Information</span>
                    </div>
                    {
                        authorized ? (
                            <Button
                                label="Edit"
                                type="action"
                                alt
                                disabled={ renderLoading || edit.employment || edit.payroll || edit.time_attendance }
                                onClick={ () => this.setEditMode( 'employment', true ) }
                            />
                        ) : false
                    }
                </div>

                {
                    renderLoading
                    ? (
                        <LoadingStyles minHeight={ '630px' }>
                            <H2>{ 'Loading Employment Information' }</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    )
                    : fields && fields.EMPLOYMENT_DETAILS.sections.map( ({ title, rows }) => (
                        <div key={ title }>
                            <div className="section-heading">
                                <span>{ title }</span>
                            </div>

                            <div className="section-body mb-2-percent">
                                { rows.map( ( row, i ) => (
                                    <div className="section-row row" key={ `${title}_${row.id}_${i}` }>
                                        { row.map( ( field ) => this.renderFieldForView( field ) ) }
                                    </div>
                                    ) ) }
                            </div>
                            <br />
                        </div>
                    ) )
                }

            </ProfileSectionWrapper>
        );
    }

    /**
     * renders Payroll Information section
     */
    renderPayrollInformation() {
        if ( !this.state.is_subscribed_to_payroll ) return '';

        const {
            edit,
            authorized,
            is_subscribed_to_ta: TASubscribed
        } = this.state;

        if ( edit.payroll ) {
            return this.renderEditPayrollInformation( this.props.employee.loadingPayrollInformation );
        }

        let fields;
        if ( TASubscribed ) {
            fields = TA_PLUS_PAYROLL_FIELDS;
        } else {
            fields = PAYROLL_FIELDS;
        }

        return (
            <ProfileSectionWrapper key="payroll-information" id={ PROFILE_SECTION_IDS.PAYROLL_INFORMATION }>
                <div className="employee-header mb-40">
                    <div className="section-title">
                        <span>Payroll Information</span>
                    </div>
                    {
                        authorized ? (
                            <Button
                                label="Edit"
                                type="action"
                                alt
                                disabled={ edit.basic || edit.employment || edit.time_attendance }
                                onClick={ () => this.setEditMode( 'payroll', true ) }
                            />
                        ) : false
                    }
                </div>
                { fields && fields.PAYROLL_INFORMATION.sections.map( ({ title, rows }) => (
                    <div key={ title }>
                        <div className="section-heading">
                            <span>{ title }</span>
                        </div>

                        <div className="section-body mb-2-percent">
                            { rows.map( ( row, i ) => (
                                <div className="section-row row" key={ `${title}_${i}` }>
                                    { row.map( ( field ) => this.renderFieldForView( field ) ) }
                                </div>
                            ) ) }
                        </div>
                        <br />
                    </div>
                ) ) }
            </ProfileSectionWrapper>
        );
    }

    /**
     * renders Edit Personal Information section
     */
    renderEditBasicInformation() {
        const isDemo = company.isDemoCompany();

        const { formOptions } = this.props;
        const valueFormat = "[^a-zA-Z-\\.'\\s]";

        return (
            <ProfileSectionWrapper key="basic-information" id={ PROFILE_SECTION_IDS.BASIC_INFORMATION }>
                <div className="employee-header mb-40">
                    <div className="section-title">
                        <span>Personal Information</span>
                    </div>

                    <div>
                        <Button
                            label="Cancel"
                            type="action"
                            alt
                            onClick={ () => this.setEditMode( 'basic', false ) }
                        />
                        <Button
                            label="Save"
                            type="action"
                            disabled={ this.state.employeeEdit === null || isEmpty( this.state.employeeEdit ) }
                            ref={ ( ref ) => { this.buttonSaveBasic = ref; } }
                            onClick={ () => this.checkEmail( 'basic' ) }
                        />
                    </div>
                </div>
                <div className="section-heading">
                    <span>Personal Details</span>
                </div>
                <div className="section-body">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="row edit">
                                <div className="col-xs-4 field-value">
                                    <Input
                                        id="textLastname"
                                        label="Last Name"
                                        required
                                        value={ this.getEmployeeValueForField( 'last_name' ) }
                                        valueFormat={ valueFormat }
                                        onChange={ ( value ) => this.updateEmployee({ last_name: value }) }
                                        ref={ ( ref ) => { this.last_name = ref; } }
                                        disabled={ isDemo }
                                    />
                                </div>
                                <div className="col-xs-4 field-value">
                                    <Input
                                        id="textFirstname"
                                        label="First Name"
                                        required
                                        value={ this.getEmployeeValueForField( 'first_name' ) }
                                        valueFormat={ valueFormat }
                                        onChange={ ( value ) => this.updateEmployee({ first_name: value }) }
                                        ref={ ( ref ) => { this.first_name = ref; } }
                                        disabled={ isDemo }
                                    />
                                </div>
                                <div className="col-xs-4 field-value">
                                    <Input
                                        id="textMiddleName"
                                        label="Middle Name"
                                        value={ this.getEmployeeValueForField( 'middle_name' ) }
                                        valueFormat={ valueFormat }
                                        onChange={ ( value ) => this.updateEmployee({ middle_name: value }) }
                                        ref={ ( ref ) => { this.middle_name = ref; } }
                                        placeholder="Enter middle name"
                                    />
                                </div>
                                <div className="col-xs-4 field-value">
                                    <SalSelect
                                        id="gender"
                                        label="Gender"
                                        placeholder="Select gender"
                                        data={ OPTIONS.GENDER }
                                        value={ this.getEmployeeValueForField( 'gender' ) }
                                        ref={ ( ref ) => { this.gender = ref; } }
                                        onChange={ ({ value }) => { this.updateEmployee({ gender: value }); } }
                                    />
                                </div>
                                <div className="col-xs-4 field-value">
                                    <SalSelect
                                        id="selectStatus"
                                        label="Marital Status"
                                        autosize
                                        placeholder="Select marital status"
                                        value={ toLower( this.getEmployeeValueForField( 'status' ) ) }
                                        data={ OPTIONS.MARITAL_STATUS }
                                        ref={ ( ref ) => { this.status = ref; } }
                                        onChange={ ({ value }) => this.updateEmployee({ status: value }) }
                                    />
                                </div>
                                <div className="col-xs-4 field-value" style={ { paddingTop: '4px' } }>
                                    <DatePicker
                                        label="Date of Birth"
                                        placeholder="Choose a date (mm/dd/yyyy)"
                                        dayFormat={ DATE_FORMATS.DISPLAY }
                                        ref={ ( ref ) => { this.birth_date = ref; } }
                                        selectedDay={ this.getEmployeeValueForField( 'birth_date' ) }
                                        onChange={ ( value ) => {
                                            const selectedDay = formatDate( value, DATE_FORMATS.API );
                                            this.updateEmployee({ birth_date: selectedDay });
                                        } }
                                    />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div className="section-heading">
                    <span>Contact Details</span>
                </div>
                <div className="section-body mb-2-percent">
                    <div className="row edit">
                        <Input
                            id="textEmail"
                            className="col-xs-4"
                            label="Email Address"
                            required
                            type="email"
                            value={ this.getEmployeeValueForField( 'email' ) }
                            onChange={ ( value ) => this.updateEmployee({ email: value }) }
                            ref={ ( ref ) => { this.email = ref; } }
                            disabled={ isDemo }
                        />
                        <Input
                            id="textMobileNumber"
                            className="col-xs-4"
                            label="Mobile Number"
                            value={ this.getEmployeeValueForField( 'mobile_number' ) }
                            placeholder="Enter mobile number"
                            ref={ ( ref ) => { this.mobile_number = ref; } }
                            onChange={ ( value ) => {
                                const clean = stripNonNumeric( value );
                                let formatted = '';
                                if ( clean.length === 0 ) {
                                    formatted = '';
                                } else if ( clean.length > 2 ) {
                                    formatted += `(${clean.substring( 0, 2 )})${clean.substring( 2, 12 )}`;
                                } else {
                                    formatted += `(${clean}`;
                                }
                                this.mobile_number.setState({ value: formatted }, () => {
                                    this.updateEmployee({ mobile_number: formatted });
                                });
                            } }
                        />
                        <Input
                            id="textTelephoneNumber"
                            className="col-xs-4"
                            label="Telephone Number"
                            placeholder="Enter telephone number"
                            value={ stripNonNumeric( this.getEmployeeValueForField( 'telephone_number' ) ) }
                            onChange={ ( value ) => {
                                const stripped = stripNonNumeric( value );
                                this.telephone_number.setState({ value: stripped }, () => {
                                    this.updateEmployee({ telephone_number: stripped });
                                });
                            } }
                            ref={ ( ref ) => { this.telephone_number = ref; } }
                            max={ 7 }
                        />
                    </div>
                    <div className="row edit">
                        <Input
                            id="textAddressLine1"
                            className="col-xs-12"
                            label="Address Line 1"
                            placeholder="Enter address"
                            value={ this.getEmployeeValueForField( 'address_line_1' ) }
                            onChange={ ( value ) => this.updateEmployee({ address_line_1: value }) }
                            ref={ ( ref ) => { this.address_line_1 = ref; } }
                        />
                    </div>
                    <div className="row edit">
                        <Input
                            id="textAddressLine2"
                            className="col-xs-12"
                            label="Address Line 2"
                            placeholder="Enter address"
                            value={ this.getEmployeeValueForField( 'address_line_2' ) }
                            onChange={ ( value ) => this.updateEmployee({ address_line_2: value }) }
                            ref={ ( ref ) => { this.address_line_2 = ref; } }
                        />
                    </div>
                    <div className="row edit">
                        <Input
                            id="textCity"
                            className="col-xs-4"
                            label="City"
                            placeholder="Enter city"
                            value={ this.getEmployeeValueForField( 'city' ) }
                            valueFormat={ valueFormat }
                            onChange={ ( value ) => this.updateEmployee({ city: value }) }
                            ref={ ( ref ) => { this.city = ref; } }
                        />
                        <div className="col-xs-4">
                            <SalSelect
                                id="selectCountry"
                                label="Country"
                                placeholder="Select country"
                                data={ formOptions.countries }
                                value={ ( this.getEmployeeValueForField( 'country' ) ) }
                                ref={ ( ref ) => { this.country = ref; } }
                                onChange={ ({ value }) => this.updateEmployee({ country: value }) }
                            />
                        </div>
                        <Input
                            id="textZipCode"
                            className="col-xs-4"
                            label="Zip Code"
                            placeholder="Enter zip code"
                            value={ this.getEmployeeValueForField( 'zip_code' ) }
                            onChange={ ( value ) => {
                                const stripped = stripNonNumeric( value );
                                this.zip_code.setState({ value: stripped }, () => {
                                    this.updateEmployee({ zip_code: stripped });
                                });
                            } }
                            ref={ ( ref ) => { this.zip_code = ref; } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }

    /**
     * renders Edit Employment Details section
     */
    renderEditEmploymentDetails() {
        const {
            is_subscribed_to_ta_only: isSubscribedToTAOnly,
            is_subscribed_to_payroll_only: isSubscribedToPayrollOnly,
            is_subscribed_to_ta_plus_payroll: isSubscribedToTAPlusPayroll
        } = this.state;

        const fields = isSubscribedToTAOnly ? TA_FIELDS : (
            isSubscribedToPayrollOnly ? PAYROLL_FIELDS : (
                isSubscribedToTAPlusPayroll ? TA_PLUS_PAYROLL_FIELDS
                    : null
            )
        );

        return (
            <ProfileSectionWrapper key="employment-details" id={ PROFILE_SECTION_IDS.EMPLOYMENT_DETAILS }>
                <div className="employee-header mb-40">
                    <div className="section-title">
                        <span>Employment Information</span>
                    </div>
                    <div>
                        <Button
                            label="Cancel"
                            type="action"
                            alt
                            onClick={ () => this.setEditMode( 'employment', false ) }
                        />
                        <Button
                            label="Update"
                            type="action"
                            disabled={ this.state.employeeEdit === null || isEmpty( this.state.employeeEdit ) }
                            ref={ ( ref ) => { this.buttonSaveEmployment = ref; } }
                            onClick={ () => this.saveSection( 'employment' ) }
                        />
                    </div>
                </div>
                { fields && fields.EMPLOYMENT_DETAILS.sections.map( ({ title, rows }) => (
                    <div key={ title }>
                        <div className="section-heading">
                            <span>{ title }</span>
                        </div>

                        <div className="section-body mb-2-percent">
                            { rows.map( ( row, i ) => (
                                <div className="row edit" key={ `${title}_${row.id}}_${i}` }>
                                    { row.map( ( field ) => this.renderFieldForEdit( field ) ) }
                                </div>
                            ) ) }
                        </div>

                        <br />
                    </div>
                ) ) }
            </ProfileSectionWrapper>
        );
    }

    /**
     * renders Edit Payroll Information section
     */
    renderEditPayrollInformation( isLoadingInProgress = false ) {
        const { is_subscribed_to_ta: TASubscribed } = this.state;

        let fields;
        if ( TASubscribed ) {
            fields = TA_PLUS_PAYROLL_FIELDS;
        } else {
            fields = PAYROLL_FIELDS;
        }

        return (
            <ProfileSectionWrapper
                key="payroll-information"
                id={ PROFILE_SECTION_IDS.PAYROLL_INFORMATION }
            >
                <div className="employee-header mb-40">
                    <div className="section-title">
                        <span>Payroll Information</span>
                    </div>
                    <div>
                        <Button
                            label="Cancel"
                            type="action"
                            alt
                            disabled={ isLoadingInProgress }
                            onClick={ () => this.setEditMode( 'payroll', false ) }
                        />
                        <Button
                            label="Update"
                            type="action"
                            disabled={ isLoadingInProgress || this.state.employeeEdit === null || isEmpty( this.state.employeeEdit ) }
                            ref={ ( ref ) => { this.buttonSavePayroll = ref; } }
                            onClick={ () => this.saveSection( 'payroll' ) }
                        />
                    </div>
                </div>
                {
                    isLoadingInProgress && (
                        <LoadingStyles minHeight="710px">
                            <H2>{ 'Loading Payroll Information' }</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    )
                }
                { !isLoadingInProgress && fields.PAYROLL_INFORMATION.sections.map( ({ title, rows }) => (
                    <div key={ title }>
                        <div className="section-heading">
                            <span>{ title }</span>
                        </div>
                        <div className="section-body">
                            { rows.map( ( row, i ) => (
                                <div className="row edit" key={ `${title}_${i}` }>
                                    { row.map( ( field ) => this.renderFieldForEdit( field ) ) }
                                </div>
                            ) ) }
                        </div>
                        <br />
                    </div>
                ) ) }
            </ProfileSectionWrapper>
        );
    }

    /**
     * Renders historical earnings for employee
     */
    renderEmployeeTerminationInformation() {
        if ( !this.props.employee ) return '';

        const { employee } = this.props;

        return [
            <EmployeeTerminationInformation
                id={ PROFILE_SECTION_IDS.TERMINATION_INFORMATION }
                employee={ employee.data }
                loading={ employee.loadingTerminationInformation }
                terminationInformation={ employee.terminationInformation }
                permission={ this.state.terminationInformationPermission }
                deleteConfirmationModal={ () => {
                    this.setState({ showDeleteConfirmationModal: false }, () => {
                        this.setState({ showDeleteConfirmationModal: true });
                    });
                } }
            />
        ];
    }

    /**
     * Renders basic pay adjustments for employee
     */
    renderEmployeeBasicPayAdjustments() {
        if ( !this.state.is_subscribed_to_payroll ) return '';

        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.BASIC_PAY_ADJUSTMENTS );

        return (
            <LazyLoad key="basic-pay-adjustments" height={ 280 } offset={ 100 } once placeholder={ placeholder } overflow>
                <EmployeeBasicPayAdjustments
                    id={ PROFILE_SECTION_IDS.BASIC_PAY_ADJUSTMENTS }
                    employee={ employee.data }
                    formOptions={ this.props.formOptions }
                    loading={ employee.loadingBasicPayAdjustments }
                    basicPayAdjustments={ employee.basicPayAdjustments }
                    permission={ this.state.basicPayAdjustmentsPermission }
                    deleteBasicPayAdjustments={ this.props.deleteBasicPayAdjustments }
                    getEmployeeBasicPayAdjustments={ this.props.getEmployeeBasicPayAdjustments }
                />
            </LazyLoad>
        );
    }

    /**
     * Renders approval groups for employee
     */
    renderEmployeeApprovalGroups() {
        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.APPROVAL_GROUPS );

        return (
            <LazyLoad key="approval-groups" height={ 280 } offset={ 100 } once placeholder={ placeholder } overflow>
                <EmployeeApprovalGroups
                    id={ PROFILE_SECTION_IDS.APPROVAL_GROUPS }
                    setShouldShowDiscardConfirm={ this.props.setShouldShowDiscardConfirm }
                    employee={ employee.data }
                    permission={ this.state.workflowEntitlementPermission }
                    loading={ employee.loadingApprovalGroups }
                    approvalGroups={ employee.approvalGroups }
                    hasPendingRequests={ employee.approvalHasPendingRequests }
                    checkPendingRequests={ this.props.approvalCheckPendingRequests }
                    requestTypes={ this.props.requestTypes }
                    formOptions={ this.props.formOptions }
                    updateEmployeeApprovalGroups={ this.props.updateEmployeeApprovalGroups }
                    getEmployeeApprovalGroups={ this.props.getEmployeeApprovalGroups }
                    products={ this.props.products }
                />
            </LazyLoad>
        );
    }

    /**
     * Renders historical earnings for employee
     */
    renderEmployeeHistoricalEarnings() {
        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.HISTORICAL_EARNINGS );

        return employee.data.payroll ? (
            <LazyLoad key="historical-earnings" height={ 280 } offset={ 100 } once placeholder={ placeholder } overflow>
                <EmployeeHistoricalEarnings
                    id={ PROFILE_SECTION_IDS.HISTORICAL_EARNINGS }
                    loading={ employee.loadingHistoricalEarnings }
                    historicalEarnings={ employee.historicalEarnings }
                    employee={ employee.data }
                    permission={ this.state.historicalEarningsPermission }
                    downloading={ employee.downloadingHistoricalEarnings }
                    exportHistoricalEarnings={ this.props.exportHistoricalEarnings }
                    getEmployeeHistoricalEarnings={ this.props.getEmployeeHistoricalEarnings }
                />
            </LazyLoad>
        ) : null;
    }

    /**
     * Renders annual earnings for employee
     */
    renderEmployeeAnnualEarnings() {
        if ( !this.state.is_subscribed_to_payroll ) return '';

        const { employee, getEmployeeAnnualEarnings } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.ANNUAL_EARNINGS );

        return employee.data.payroll && this.state.annualEarningsPermission.view && (
            <LazyLoad key="annual-earnings" height={ 280 } offset={ 100 } once placeholder={ placeholder } overflow>
                <EmployeeAnnualEarnings
                    id={ PROFILE_SECTION_IDS.ANNUAL_EARNINGS }
                    loading={ employee.loadingAnnualEarnings }
                    annualEarnings={ employee.annualEarnings }
                    employee={ employee.data }
                    getEmployeeAnnualEarnings={ getEmployeeAnnualEarnings }
                />
            </LazyLoad>
        );
    }

    /**
     * Renders earnings for employee
     */
    renderEmployeeEarnings() {
        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.EARNINGS );

        return employee.data.payroll ? (
            <LazyLoad key="earnings" height={ 200 } offset={ 100 } once placeholder={ placeholder } overflow>
                <EmployeeEarnings
                    id={ PROFILE_SECTION_IDS.EARNINGS }
                    loading={ employee.loadingEarnings }
                    employee={ employee.data }
                    earnings={ employee.earnings }
                    permission={ this.state.earningsPermission }
                    getEmployeeEarnings={ this.props.getEmployeeEarnings }
                />
            </LazyLoad>
        ) : null;
    }

    /**
     * Renders leave credits for employee
     */
    renderEmployeeLeaveCredits() {
        if ( !this.state.is_subscribed_to_ta ) return '';

        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.LEAVE_CREDITS );

        return employee.data.time_attendance ? (
            <LazyLoad key="leave-credits" height={ 280 } offset={ 100 } once placeholder={ placeholder } overflow>
                <EmployeeLeaveCredits
                    id={ PROFILE_SECTION_IDS.LEAVE_CREDITS }
                    leaveCredits={ employee.leaveCredits }
                    leaveCreditsPagination={ employee.leaveCreditsPagination }
                    updateEmployeeLeaveCredits={ this.props.updateEmployeeLeaveCredits }
                    employee={ employee.data }
                    permission={ this.state.leaveCreditPermission }
                    downloading={ employee.downloadingLeaveCredits }
                    loading={ employee.loadingLeaveCredits }
                    exportLeaveCredits={ this.props.exportLeaveCredits }
                    getEmployeeLeaveCredits={ this.props.getEmployeeLeaveCredits }
                />
            </LazyLoad>
        ) : null;
    }

    /**
     * Renders filed leaves for employee
     */
    renderEmployeeFiledLeaves() {
        if ( !this.state.is_subscribed_to_ta ) return '';

        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.FILED_LEAVES );

        return employee.data.time_attendance ? (
            <LazyLoad key="filed-leaves" height={ 280 } offset={ 100 } once placeholder={ placeholder } overflow>
                <EmployeeFiledLeaves
                    id={ PROFILE_SECTION_IDS.FILED_LEAVES }
                    filedLeaves={ employee.filedLeaves }
                    filedLeavesPagination={ employee.filedLeavesPagination }
                    employee={ employee.data }
                    permission={ this.state.filedLeavesPermission }
                    loading={ employee.loadingFiledLeaves }
                    getEmployeeFiledLeaves={ this.props.getEmployeeFiledLeaves }
                    exportFiledLeaves={ this.props.exportFiledLeaves }
                />
            </LazyLoad>
        ) : null;
    }

    /**
     * Renders loans for employee
     */
    renderEmployeeLoans() {
        if ( !this.state.is_subscribed_to_payroll ) return '';

        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.LOANS );

        return employee.data.payroll ? (
            <LazyLoad key="loans" height={ 280 } offset={ 100 } once placeholder={ placeholder } overflow>
                <EmployeeLoans
                    id={ PROFILE_SECTION_IDS.LOANS }
                    loading={ employee.loadingLoans }
                    loans={ employee.loans }
                    employee={ employee.data }
                    permission={ this.state.loanPermission }
                    downloading={ employee.downloadingLoans }
                    exportLoans={ this.props.exportLoans }
                    deleteLoans={ this.props.deleteLoans }
                    getEmployeeLoans={ this.props.getEmployeeLoans }
                />
            </LazyLoad>
        ) : null;
    }

    /**
     * Renders bonuses for employee
     */
    renderEmployeeBonuses() {
        if ( !this.state.is_subscribed_to_payroll ) return '';

        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.BONUSES );

        return employee.data.payroll ? (
            <LazyLoad key="bonuses" height={ 280 } offset={ 100 } once placeholder={ placeholder } overflow>
                <EmployeeBonuses
                    id={ PROFILE_SECTION_IDS.BONUSES }
                    loading={ employee.loadingBonuses }
                    bonuses={ employee.bonuses }
                    employee={ employee.data }
                    permission={ this.state.bonusPermission }
                    downloading={ employee.downloadingBonuses }
                    exportBonuses={ this.props.exportOtherIncomes }
                    deleteBonuses={ this.props.deleteOtherIncomes }
                    getEmployeeOtherIncome={ this.props.getEmployeeOtherIncome }
                />
            </LazyLoad>
        ) : null;
    }

    /**
     * Renders commissions for employee
     */
    renderEmployeeCommissions() {
        if ( !this.state.is_subscribed_to_payroll ) return '';

        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.COMMISSIONS );

        return employee.data.payroll ? (
            <LazyLoad key="commissions" height={ 280 } offset={ 100 } once placeholder={ placeholder } overflow>
                <EmployeeCommissions
                    id={ PROFILE_SECTION_IDS.COMMISSIONS }
                    loading={ employee.loadingCommissions }
                    commissions={ employee.commissions }
                    employee={ employee.data }
                    permission={ this.state.commissionPermission }
                    downloading={ employee.downloadingCommissions }
                    exportCommissions={ this.props.exportOtherIncomes }
                    deleteCommissions={ this.props.deleteOtherIncomes }
                    getEmployeeOtherIncome={ this.props.getEmployeeOtherIncome }
                />
            </LazyLoad>
        ) : null;
    }

    /**
     * Renders deductions for employee
     */
    renderEmployeeDeductions() {
        if ( !this.state.is_subscribed_to_payroll ) return '';

        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.DEDUCTIONS );

        return employee.data.payroll ? (
            <LazyLoad key="deductions" height={ 280 } offset={ 100 } once placeholder={ placeholder }>
                <EmployeeDeductions
                    id={ PROFILE_SECTION_IDS.DEDUCTIONS }
                    loading={ employee.loadingDeductions }
                    deductions={ employee.deductions }
                    employee={ employee.data }
                    permission={ this.state.deductionPermission }
                    downloading={ employee.downloadingDeductions }
                    exportDeductions={ this.props.exportOtherIncomes }
                    deleteDeductions={ this.props.deleteOtherIncomes }
                    getEmployeeOtherIncome={ this.props.getEmployeeOtherIncome }
                />
            </LazyLoad>
        ) : null;
    }

    /**
     * Renders allowances for employee
     */
    renderEmployeeAllowances() {
        if ( !this.state.is_subscribed_to_payroll ) return '';

        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.ALLOWANCES );

        return employee.data.payroll ? (
            <LazyLoad key="allowances" height={ 280 } offset={ 100 } once placeholder={ placeholder }>
                <EmployeeAllowances
                    id={ PROFILE_SECTION_IDS.ALLOWANCES }
                    loading={ employee.loadingAllowances }
                    allowances={ employee.allowances }
                    employee={ employee.data }
                    permission={ this.state.allowancePermission }
                    downloading={ employee.downloadingAllowances }
                    exportAllowances={ this.props.exportOtherIncomes }
                    deleteAllowances={ this.props.deleteOtherIncomes }
                    getEmployeeOtherIncome={ this.props.getEmployeeOtherIncome }
                />
            </LazyLoad>
        ) : null;
    }

    /**
     * Renders adjustments for employee
     */
    renderEmployeeAdjustments() {
        if ( !this.state.is_subscribed_to_payroll ) return '';

        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.ADJUSTMENTS );

        return employee.data.payroll ? (
            <LazyLoad key="adjustments" height={ 280 } offset={ 100 } once placeholder={ placeholder }>
                <EmployeeAdjustments
                    id={ PROFILE_SECTION_IDS.ADJUSTMENTS }
                    loading={ employee.loadingAdjustments }
                    adjustments={ employee.adjustments }
                    employee={ employee.data }
                    permission={ this.state.adjustmentPermission }
                    downloading={ employee.downloadingAdjustments }
                    exportAdjustments={ this.props.exportOtherIncomes }
                    deleteAdjustments={ this.props.deleteOtherIncomes }
                    getEmployeeOtherIncome={ this.props.getEmployeeOtherIncome }
                />
            </LazyLoad>
        ) : null;
    }

    /**
     * Renders payment methods for employee
     */
    renderEmployeePaymentMethods() {
        if ( !this.state.is_subscribed_to_payroll ) return '';

        const { employee } = this.props;
        const placeholder = this.renderLazyloadPlaceholder( PROFILE_SECTION_IDS.PAYMENT_METHODS );

        return (
            <LazyLoad key="payment-methods" height={ 280 } offset={ 100 } once placeholder={ placeholder }>
                <EmployeePaymentMethods
                    id={ PROFILE_SECTION_IDS.PAYMENT_METHODS }
                    loading={ employee.loadingPaymentMethods }
                    employee={ employee.data }
                    permission={ this.state.paymentMethodPermission }
                    paymentMethods={ employee.paymentMethods }
                    getEmployeePaymentMethods={ this.props.getEmployeePaymentMethods }
                    deleteEmployeePaymentMethods={ this.props.deleteEmployeePaymentMethods }
                    assignEmployeePaymentMethod={ this.props.assignEmployeePaymentMethod }
                />
            </LazyLoad>
        );
    }

    renderLazyloadPlaceholder( elementId ) {
        return <div className="lazyload-placeholder" style={ { height: '280px' } } id={ elementId } />;
    }

    /**
     * renders a loading screen
     */
    renderLoadingScreen() {
        return (
            <MessageWrapper>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        <H3>Loading employee profile...</H3>
                    </div>
                </div>
            </MessageWrapper>
        );
    }

    /**
     * render's component to DOM
     */
    render() {
        const { loading, terminationInformation } = this.props.employee;
        const {
            showDeleteConfirmationModal
        } = this.state;

        return (
            <ProfileWrapper innerRef={ ( ref ) => { this.element = ref; } } id="employee-profile-wrapper">
                <SalConfirm
                    onConfirm={ () => { this.saveSection( 'basic' ); } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                You are about to update a verified email address. Once updated, the employee will receive a new verification email on his new email address. <br /><br /> Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    confirmText="Proceed"
                    cancelText="Cancel"
                    title="Update Employee Email"
                    buttonStyle="success"
                    visible={ this.state.showUpdateEmailConfirm }
                />
                <SalConfirm
                    onConfirm={ () => { this.props.deleteTerminationInformation({ terminationId: terminationInformation.id, id: this.state.employee.id }); } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                You are about to delete termination information for the selected employee.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ showDeleteConfirmationModal }
                />
                <SalConfirm
                    onConfirm={ () => {
                        this.setState({ showContributionErrorOnTerminateEmployee: false });
                        this.props.scrollToSection( PROFILE_SECTION_IDS.PAYROLL_INFORMATION );
                    } }
                    onCancel={ () => {
                        this.setState({ showContributionErrorOnTerminateEmployee: false });
                    } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                No contribution basis set for this employee, please check the settings under Payroll Information.
                                <br /><br />
                                Proceed to Payroll Information section?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    confirmText="Proceed"
                    cancelText="Cancel"
                    title="Final Pay Contribution Basis Error"
                    buttonStyle="danger"
                    visible={ this.state.showContributionErrorOnTerminateEmployee }
                />
                {
                    terminationInformation && terminationInformation.final_pay_status === FINAL_PAY_STATUSES.CLOSED ? (
                        <div className="final-pay-info">
                            You have Closed Final Pay
                        </div>
                    ) : null
                }
                {
                    loading ? this.renderLoadingScreen()
                        : this.renderCurrentSection( this.props.currentSection )
                }
            </ProfileWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    employee: makeSelectEmployee(),
    formOptions: makeSelectFormOptions(),
    requestTypes: makeSelectRequestTypes(),
    annualEarningsFormOptions: makeSelectAnnualEarningsFormOptions(),
    products: makeSelectProductsState(),
    userInformation: makeSelectUserInformation()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        viewActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps, null, { withRef: true })( EmployeeProfileView );
