import React from 'react';

import Button from 'components/Button';

/**
 * Section heading for employee records table
 */
const SectionHeading = ({ moreActionsVisible, permission, heading, onAdd, showMore, createButtonLabel, id, cancelEdit, cancelButtonLabel, disabled }) => (
    <div>
        <div className="employee-header">
            <div className="section-title">
                <span>{heading}</span>
            </div>
            {
                moreActionsVisible ? (
                    <div className="edit-actions">
                        { cancelEdit && cancelButtonLabel && (
                            <Button
                                id={ `${id}-cancel-edit` }
                                className="cancel"
                                label={ cancelButtonLabel }
                                size="default"
                                type="neutral"
                                alt
                                onClick={ cancelEdit }
                                disabled={ disabled }
                            />
                        ) }
                        <Button
                            className={ permission.create ? '' : 'hide' }
                            label={ createButtonLabel }
                            size="default"
                            type="action"
                            onClick={ onAdd }
                            disabled={ disabled }
                        />
                    </div>
                ) : (
                    <Button
                        label="Edit"
                        type="action"
                        alt
                        disabled={ moreActionsVisible || disabled }
                        onClick={ showMore }
                    />
                )
            }
        </div>
        <div className="section-heading mt-1-percent">
        </div>
    </div>
);

SectionHeading.propTypes = {
    moreActionsVisible: React.PropTypes.bool,
    permission: React.PropTypes.object,
    heading: React.PropTypes.string,
    onAdd: React.PropTypes.func,
    showMore: React.PropTypes.func,
    createButtonLabel: React.PropTypes.string,
    cancelEdit: React.PropTypes.func,
    cancelButtonLabel: React.PropTypes.string,
    id: React.PropTypes.string
};

export default SectionHeading;
