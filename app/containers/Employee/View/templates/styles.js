import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: '200px';
    min-height: ${( props ) => ( props.minHeight ? props.minHeight : '200px' )};
    max-height: ${( props ) => ( props.maxHeight ? props.maxHeight : '200px' )};
    justify-content: center;
    padding: 140px 0;
`;

export const ValueWrapper = styled.div`
    padding-top: 15px;
    padding-left: 12.5px;
    padding-bottom: 30px;

    .Select-control {
        border: none;
        color: #373a3c !important;
        background: #ffffff !important;
        cursor: default !important;
        vertical-align: top;
    }

    .Select.is-disabled {
        .Select-value-label {
            color: #373a3c !important;
        }
    }

    .Select-arrow-zone {
        display: none;
    }
`;

export const LoansWrapper = styled.div`
    .content {
        margin-top: 40px;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .main {
            .btn {
                min-width: 140px;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;

            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 600;
                font-size: 22px;
            }

            .search-wrapper {
                flex-grow: 1;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 30px;

                    input {
                        border: none;
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }
        }
    }

    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }
`;

export const MessageWrapper = styled.div`
    display: flex;
    justify-content: center;
    height: calc(100% - 90px);
    width: 100%;
    margin-bottom: 0 !important;

    > div {
        display: flex;
        flex-direction: column;
        justify-content: center;

        i {
            font-size: 7em;
            align-self: center;
        }

        .description {
            padding-top: 40px;
            text-align: center;

            h3 {
                font-weight: 600;
            }
        }
    }
`;

export const ProfileWrapper = styled.div`
    overflow-y: auto;
    padding: 20px 2%;
    width: 100%;
    height: 100%;
    flex: 1;

    .final-pay-info {
        width: 100%;
        height: 55px;
        background-color: rgba(184, 233, 134, 0.5);
        text-align: end;
        line-height: 55px;
        padding-right: 10px;
        font-weight: bold;
    }

    .summary-panel {
        font-size: 14px;
        display: flex;
        padding: 25px;
        background-color: rgba(229, 246, 252, 1);
        border-radius: 5px;
        margin-bottom: 70px;

        &__column {
            flex-grow: 1;
            flex-basis: 0;
            padding: 10px;

            &:not(:last-child) {
                border-right: solid 2px rgb(216, 216, 216);
                margin-right: 40px
            }

            p {
                margin: 0;
            }
        }
    }

    .section-title {
        font-weight: 600;
        font-size: 24px;

        .emp-id {
            font-weight: 400;
            color: #00a5e5;
            margin-right: 4px;
        }

        .brace {
            color: #4e4e4e;
        }

        .semi-active {
            font-size: 24px;
            color: #F21108;
        }
    }

    .mb-40 {
        margin-bottom: 40px;
    }

    .mt-40 {
        margin-top: 40px;
    }

    .employee-header {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .terminate-button {
        border-color: #F21108;
    }

    @media (max-width: 1440px) {
        padding: 40px 15px 40px 50px;
    }

    @media (max-width: 1280px) {
        padding: 40px 25px 40px 50px;
    }

    .col-xs-3,
    .col-xs-4,
    .col-xs-6 {
        .DayPickerInput,
        .DayPickerInput input {
            width: 100%;
        }
    }

    .offset-right-4 {
        margin-right: 33.333333%;
    }


    > div {
        margin-bottom: 20px;

        &.actions {
            display: flex;
            margin-bottom: 25px;

            button {
                text-transform: none;
                color: inherit;
                padding: 12px 30px;

                &:first-child {
                    margin-right: 15px;
                }

                &:nth-last-child(2) {
                    margin-left: auto;
                    margin-right: 5px;
                }
            }
        }
    }
`;

export const ProfileSectionWrapper = styled.div`
    background-color: #fff;

    .red-font {
        color: red !important;
    }

    .section-heading {
        display: flex;
        align-items: center;
        justify-content: space-between;
        border-bottom: 1px solid #e5e5e5;

        font-weight: 600;
        font-size: 20px;

        button {
            margin-bottom: 20px;
        }

        a {
            border-bottom: none;

            button {
                background-color: #4aba4a;
                border-radius: 50%;
                width: 30px;
                height: 30px;
                border: none;
                outline: none;
                cursor: pointer;

                &:disabled {
                    cursor: not-allowed;
                    opacity: .65;
                }

                i {
                    color: #fff;
                }
            }
        }

        & .edit-actions {
            display: flex;
            flex-direction: row;

            & .cancel {
                margin-right: 10px;
            }
        }
    }

    .section-sub-heading {
        font-weight: 600;
        font-size: 16px;
    }

    .mb-2-percent {
        margin-bottom: 2%;
    }

    .mb-5-percent {
        margin-bottom: 5%;
    }

    .mt-10-percent {
        margin-top: 10%;
    }

    .mt-5-percent {
        margin-top: 5%;
    }

    .mt-2-percent {
        margin-top: 2%;
    }

    .mt-1-percent {
        margin-top: 1%;
    }

    .field-value {
        white-space: pre-wrap;
        font-size: 14px;
        margin-top: 10px;
        margin-bottom: 15px;
        word-wrap: break-word
    }

    .field-label {
        font-size: 14px;
        color: #626262;
        font-weight: 550;
    }

    .pt-25 {
        padding-top: 25px;
    }

    .section-body {
        font-size: 14px;
        padding-top: 25px;

        i.fa {
            color: #00a5e5;
            font-size: 1.12em;
            width: 35px;
            display: inline-block;
        }

        > div {
            > div {
                .section-row {
                    display: flex;
                    flex-wrap: wrap;
                    margin-bottom: 15px;

                    > div {
                        .radio-group {
                            > div:first-child{
                                padding-left: 0 !important;
                            }
                        }
                    }

                    &.edit {
                        > div:not(:last-child) {
                            margin-bottom: 0;
                        }
                    }
                }
            }
        }

        .content {
            .heading {
                display: flex;
                align-items: center;
                flex-direction: column;
                margin: 0 auto 50px auto;

                h3 {
                    font-weight: 600;
                }

                p {
                    text-align: center;
                    max-width: 800px;
                }
            }

            .tableAction button {
                width: 130px;
            }

            .title {
                display: flex;
                align-items: center;
                margin-bottom: 20px;

                h5 {
                    margin: 0;
                    margin-right: 20px;
                    font-weight: 600;
                    font-size: 20px;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }

            .no-cursor-pointer {
                .rc-switch, .rc-switch:after {
                    cursor: default;
                }
            }
        }
        .search-wrapper {
            flex-grow: 1;

            .search {
                width: 300px;
                border: 1px solid #333;
                border-radius: 30px;

                input {
                    border: none;
                }
            }

            p {
                display: none;
            }

            .input-group,
            .form-control {
                background-color: transparent;
            }

            .input-group-addon {
                background-color: transparent;
                border: none;
            }
        }

        .row {
            .employee-id {
                margin-left: 11px;
            }

            .switch {
                & > div, .switch-label {
                    font-size: 14px;
                    margin-bottom: 20px;
                }

                span {
                    margin-left: 5px;
                }

                .rc-switch-checked.rc-switch-disabled::after {
                    background-color: #87b865;
                }
            }

            .sub-switch {
                padding-left: 40px;
            }
        }
    }

    .section-footer {
        display: flex;
        justify-content: flex-end;
        border-top: 1px solid #e5e5e5;
        padding: 25px 0 0 0;

        button {
            text-transform: none;
            padding: 12px 45px;

            &:first-child {
                color: inherit;
                margin-right: 15px;
            }
        }
    }

    .hide {
        display: none;
    }

    .annual-earnings > div {
        padding-left: 15px;

        .row-title {
            font-style: italic;
        }

        .row {
            padding-left: 10px;
        }
    }
`;

export const ListWrapper = styled.div`
    display: flex;
    flex-direction: column;
    background-color: #fff;
    border-right: 1px solid #e5e5e5;
    width: 350px;
    height: calc(100vh - 120px);

    @media (max-width: 1280px) {
        width: 300px;
    }

    @media (max-width: 1024px) {
        width: 250px;
    }

    .hide {
        display: none;
    }

    .collapsed & {
        display: flex;

        @media (max-width: 1280px) {
            display: none;
        }
    }

    .mt-10 {
        margin-top: 10px;
    }

    .mt-20 {
        margin-top: 20px;
    }

    .mb-10 {
        margin-bottom: 10px;
    }

    .mb-20 {
        margin-bottom: 20px;
    }

    .mb-40 {
        margin-bottom: 40px;
    }

    .menu-btn {
        padding: 5px 15px;
        font-weight: 600;
        font-size: 14px;
        margin-top: 5px;
    }

    .menu-btn i {
        text-align: right;
        font-size: 12px;
    }

    .menu-btn:hover {
        width: 100%;
        text-align: left;
        color: #33adff;
    }

    .menu-btn:focus {
        border: none;
        outline: 0;
        outline-style: none;
        outline-width: 0;
    }

    .actions {
        display: flex;
        flex-direction: row-reverse;
        padding: 0 45px 25px;

        .input-search-group {
            width: 100%;
            display: flex;
            position: relative;

            > div {
                width: 100%;
                input {
                    border-radius: 50px;
                    padding-right: 45px;
                }

                p {
                    display: none;
                }
            }

            .button-search {
                border: none;
                outline: none;
                position: absolute;
                right: 0;
                cursor: pointer;
                padding-right: 15px;
                align-self: center;

                @media (-ms-high-contrast: active), (-ms-high-contrast: none) {
                    margin-top: 10px;
                }

                i {
                    font-size: 1.5em;
                }
            }
        }

        .button-collapse {
            border: none;
            outline: none;
            margin-left: 10px;
            cursor: pointer;

            i {
                color: #adadad;
                font-size: 1.8em;
            }
        }
    }

    .list {
        height: 100%;
        overflow-y: auto;
        font-size: 14px;

        ul {
            list-style: none;
            display: flex;
            flex-direction: column;
            padding: 0 0 25px;
        }

        li:hover {
            text-align: left;
            color: #33adff;
        }
    }
`;

export const ListItemWrapper = styled.li`
    display: flex;
    padding: 5px 30px;
    cursor: pointer;
    border-left: 4px solid #fff;

    div:first-child {
        width: 35%;
    }

    &:hover {
        background-color: rgba(0, 165, 229, 0.01);
    }

    &.active,
    &:hover.active {
        font-weight: 600;
        background-color: #e4edf3;
        color: #33adff
    }
`;

export const ListCollapsedWrapper = styled.div`
    top: 40px;
    display: block;
    position: absolute;
    background-color: transparent;
    z-index: 10;

    @media (max-width: 1280px) {
        display: none;
    }

    .collapsed & {
        display: none;

        @media (max-width: 1280px) {
            display: block;
        }
    }

    button {
        border: 1px solid #e5e5e5;
        border-left: none;
        border-radius: 10px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        background-color: #fff;
        outline: none;
        cursor: pointer;
        padding: 6px 11px;
        font-size: 13px;

        i {
            color: #adadad;
            font-size: 1.8em;
        }
    }

`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;

export const BreakdownWrapper = styled.section`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    flex-wrap: wrap;
    margin-top: 32px;

    .column {
        flex: 1;
        display: flex;
        flex-direction: column;
        min-height: 300px;
    }

    .column:not(:first-child) {
        margin-left: 10px;
    }

    .items {
        padding-left: 10px;
        padding-right: 24px;
    }

    .items:not( .net-pay ) {
        flex: 1;
        padding-bottom: 12px;
    }

    .item {
        display: flex;
        align-items: center;
        justify-content: space-between;
        line-height: 1.5;
        margin: 8px 0;
    }

    .item-name {
        margin-right: 8px;
    }

    .item-value {
        margin-left: 8px;
        white-space: nowrap;
    }

    .items, .column-title {
        border: 1px solid rgba( 0, 0, 0, 0.1 );
    }

    .column-title {
        margin-bottom: 0;
        padding: 10px 12px;
        border-bottom: 0;
        font-size: 14px;
        font-weight: 600;
    }
`;

