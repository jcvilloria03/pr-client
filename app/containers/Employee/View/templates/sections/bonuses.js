import React from 'react';

import Table from 'components/Table';
import Button from 'components/Button';

import {
    getSortedData,
    getIdsOfSelectedRows
} from 'components/Table/helpers';

import { browserHistory } from 'utils/BrowserHistory';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate
} from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';

import { ProfileSectionWrapper } from '../styles';

const BASIS = {
    CURRENT_BASIC_SALARY: 'Current salary',
    AVERAGE_OF_BASIC_SALARY: 'Average salary',
    GROSS_SALARY: 'Gross salary',
    BASIC_PAY: 'Basic Pay'
};

/**
 * Employee bonuses component
 */
export default class EmployeeBonuses extends React.Component {
    static propTypes = {
        bonuses: React.PropTypes.array,
        employee: React.PropTypes.object,
        downloading: React.PropTypes.bool,
        permission: React.PropTypes.object,
        exportBonuses: React.PropTypes.func,
        deleteBonuses: React.PropTypes.func,
        loading: React.PropTypes.bool,
        getEmployeeOtherIncome: React.PropTypes.func,
        id: React.PropTypes.string
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: [],
            moreActionsVisible: false
        };
    }

    componentDidMount() {
        this.props.getEmployeeOtherIncome({
            employeeId: this.props.employee.id,
            otherIncomeType: 'BONUS'
        });
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.bonuses !== this.props.bonuses ) {
            const displayedData = this.getDataForDisplay( nextProps.bonuses );
            this.setState({ displayedData }, () => {
                this.handleSearch();
            });
        }
    }

    getTableColumns = () => {
        const allColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'type_name',
                header: 'Bonus Type',
                accessor: ( row ) => ( row.type ? ( row.type.name === '13TH_MONTH_PAY' ? '13th Month Pay' : row.type.name ) : '' ),
                minWidth: 260
            },
            {
                id: 'amount_basis',
                header: 'Amount/Basis',
                accessor: ( row ) => ( row.amount ? formatCurrency( row.amount ) : BASIS[ row.basis ]),
                minWidth: 260
            },
            {
                id: 'release_date',
                header: 'Release Date',
                accessor: ( row ) => formatDate( row.schedule_of_release, DATE_FORMATS.DISPLAY ),
                minWidth: 200
            },
            {
                id: 'status',
                header: 'Status',
                accessor: ( row ) => ( row.status ? 'Released' : 'Not Yet Released' ),
                minWidth: 150
            },
            {
                id: 'schedule_of_release',
                header: 'Special Pay Run?',
                accessor: ( row ) => row.special_pay_run,
                minWidth: 200
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => ( !row.status ? (
                    <Button
                        className={ this.props.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => {
                            localStorage.setItem( 'employeeIdForEditBonus', this.props.employee.id );
                            browserHistory.push( `/bonuses/${row.id}/edit` );
                        } }
                    /> ) : null
                )
            }
        ];

        const visibleColumns = this.state.moreActionsVisible
            ? [ 'type_name', 'amount_basis', 'release_date', 'status', 'schedule_of_release', 'actions' ]
            : [ 'type_name', 'amount_basis', 'release_date', 'status', 'schedule_of_release' ];

        return allColumns.filter( ( column ) => (
            visibleColumns.includes( column.id ? column.id : column.accessor )
        ) );
    }

    getDataForDisplay = ( bonuses ) => (
        bonuses.map( ( data ) => {
            const lastReleaseDetails = data.release_details.find( ( releaseDetails ) => !releaseDetails.status ) || data.release_details[ 0 ];
            return {
                ...data,
                schedule_of_release: lastReleaseDetails.date,
                status: lastReleaseDetails.status,
                bonus_per_recipient: data.type.basis === 'FIXED' ? `Fixed Bonus ${formatCurrency( data.amount )}` : `Salary Based ${data.percentage}%`,
                total_amount: data.type.basis === 'FIXED' ? `Php ${formatCurrency( data.amount )}` : `${data.percentage}%`,
                special_pay_run: lastReleaseDetails.disburse_through_special_pay_run === false ? 'No' : 'Yes'
            };
        })
    )

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.bonusesTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    /**
     * Send request to export bonuses
     */
    exportBonuses = () => {
        const sortedBonuses = getSortedData( this.bonusesTable );
        this.props.exportBonuses({ ids: sortedBonuses.map( ( bonus ) => bonus.id ), type: 'BONUS' });
    };

    /**
     * Send a request to delete bonuses
     */
    deleteBonuses = () => {
        const ids = getIdsOfSelectedRows( this.bonusesTable );
        this.props.deleteBonuses({ employeeId: this.props.employee.id, ids, type: 'BONUS' });
    }

    handleSearch = ( term = '' ) => {
        const dataForDisplay = this.getDataForDisplay( this.props.bonuses );
        const matchingRows = this.bonusesTable.getFilteredData( dataForDisplay, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <SectionHeading
                    moreActionsVisible={ this.state.moreActionsVisible }
                    permission={ this.props.permission }
                    heading="Bonuses"
                    id="tooltipBonuses"
                    createButtonLabel="Add Bonus"
                    showMore={ () => {
                        this.setState({ moreActionsVisible: true }, () => {
                            this.bonusesTable.initColumns();
                        });
                    } }
                    onAdd={ () => {
                        localStorage.setItem( 'prepopulatedEmployeeForAddBonus', JSON.stringify( this.props.employee ) );
                        browserHistory.push( '/bonuses/add' );
                    } }
                />
                <div className="section-body">
                    <div className="content">
                        <SectionBody
                            table={ this.bonusesTable }
                            permission={ this.props.permission }
                            deleteLabel={ this.state.deleteLabel }
                            label={ this.state.label }
                            deleteModalHeading="Delete Bonuses"
                            downloading={ this.props.downloading }
                            moreActionsVisible={ this.state.moreActionsVisible }
                            onDelete={ this.deleteBonuses }
                            onSearch={ this.handleSearch }
                            onExport={ this.exportBonuses }
                        />
                        <Table
                            data={ this.state.displayedData }
                            columns={ this.getTableColumns() }
                            pagination
                            selectable={ this.state.moreActionsVisible }
                            loading={ this.props.loading }
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => { this.bonusesTable = ref; } }
                            onSelectionChange={ ({ selected }) => {
                                const selectionLength = selected.filter( ( row ) => row ).length;

                                this.setState({
                                    deleteLabel: formatDeleteLabel( selectionLength )
                                });
                            } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }
}
