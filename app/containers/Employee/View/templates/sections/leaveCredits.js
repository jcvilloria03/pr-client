import React from 'react';
import first from 'lodash/first';
import isEmpty from 'lodash/isEmpty';
import clone from 'lodash/clone';

import AsyncTable from 'components/AsyncTable';
import Button from 'components/Button';
import SalDropdown from 'components/SalDropdown';
import Input from 'components/Input';
import Loader from 'components/Loader';
import { FILED_LEAVE_UNITS_SINGULAR } from 'utils/constants';
import { browserHistory } from 'utils/BrowserHistory';
import {
    formatPaginationLabel,
    stripNonDigit
} from 'utils/functions';

import {
    ProfileSectionWrapper
} from '../styles';

import { UNLIMITED, NOT_AVAILABLE } from '../../constants';

/**
 * Employee leaveCredits component
 */
class EmployeeLeaveCredits extends React.Component {
    static propTypes = {
        leaveCredits: React.PropTypes.array,
        getEmployeeLeaveCredits: React.PropTypes.func,
        updateEmployeeLeaveCredits: React.PropTypes.func,
        employee: React.PropTypes.object,
        permission: React.PropTypes.object,
        loading: React.PropTypes.bool,
        downloading: React.PropTypes.bool,
        exportLeaveCredits: React.PropTypes.func,
        id: React.PropTypes.string
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            term: '',
            moreActionsVisible: false,
            edited: null,
            sorting: {
                by: 'leave_type_name',
                order: 'asc'
            },
            pagination: {
                page: 0,
                pageSize: 10,
                totalPages: 0,
                dataLength: 0
            },
            selectedValues: []
        };
    }

    componentDidMount() {
        this.getEmployeeLeaveCredits();
    }

    /**
     * Handle new props
     */
    componentWillReceiveProps( newProps ) {
        if ( !isEmpty( newProps.leaveCreditsPagination ) ) {
            this.setLeaveCreditsPagination( newProps.leaveCreditsPagination );
        }
    }

    /**
     * Get employee leave credits.
     * @param {Object} data
     */
    getEmployeeLeaveCredits = () => {
        this.props.getEmployeeLeaveCredits( this.props.employee.id, {
            term: this.state.term,
            page: this.state.pagination.page,
            per_page: this.state.pagination.pageSize,
            sort_by: this.state.sorting.by,
            sort_order: this.state.sorting.order
        });
    }

    /**
     * Sets leave credits Pagination
     */
    setLeaveCreditsPagination = ( leaveCreditsPagination ) => {
        if ( !this.leaveCreditsTable || !this.leaveCreditsTable.tableComponent ) {
            return;
        }

        const pagination = {
            page: leaveCreditsPagination.current_page,
            pageSize: leaveCreditsPagination.per_page,
            totalPages: leaveCreditsPagination.total_pages,
            dataLength: leaveCreditsPagination.total
        };

        this.setState({
            pagination,
            label: formatPaginationLabel({
                page: leaveCreditsPagination.current_page - 1,
                pageSize: leaveCreditsPagination.per_page,
                dataLength: leaveCreditsPagination.total
            })
        });
    }

    /**
     * Get table columns information.
     * @return {Array}
     */
    getTableColumns() {
        const allColumns = [
            {
                header: 'Leave Type',
                accessor: 'leave_type_name',
                minWidth: 150,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.leave_type.name}
                    </div>
                )
            },
            {
                header: 'Earned',
                accessor: 'earned',
                minWidth: 150,
                sortable: true,
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) ? (
                        <Input
                            id="earned"
                            type="text"
                            placeholder=""
                            required
                            value={ this.state.edited.value }
                            ref={ ( ref ) => { this.earnedInput = ref; } }
                            onChange={ ( value ) => {
                                this.updateEditedRowState( 'value', value );
                            } }
                        />
                    ) : (
                        <div>
                            { row.leave_type.leave_credit_required ? `${row.earned} ${row.unit}` : NOT_AVAILABLE }
                        </div>
                    )
                )
            },
            {
                header: 'Used',
                accessor: 'used',
                minWidth: 150,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { row.used } { row.unit }
                    </div>
                )
            },
            {
                header: 'Remaining',
                accessor: 'remaining',
                minWidth: 260,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { row.leave_type.leave_credit_required
                            ? `${row.value} ${parseFloat( row.value ) === 1
                                ? FILED_LEAVE_UNITS_SINGULAR[ row.unit ]
                                : row.unit
                            }`
                            : UNLIMITED
                        }
                    </div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 150,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) ? (
                        <div>
                            <Button
                                label={ <span>Cancel</span> }
                                type="grey"
                                size="small"
                                onClick={ this.stopEditing }
                            />
                            <Button
                                label={ <span>Save</span> }
                                type="grey"
                                size="small"
                                onClick={ this.saveEdited }
                            />
                        </div>
                    ) : (
                        <Button
                            className={ this.props.permission.edit ? '' : 'hide' }
                            label={ <span>Edit</span> }
                            type="grey"
                            size="small"
                            onClick={ () => {
                                this.startEditingRow( row );
                            } }
                            disabled={ !row.leave_type.leave_credit_required }
                        />
                    )
                )
            }
        ];

        const visibleColumns = this.state.moreActionsVisible
            ? [ 'leave_type_name', 'earned', 'used', 'remaining', 'actions' ]
            : [ 'leave_type_name', 'earned', 'used', 'remaining' ];

        return allColumns.filter( ( column ) => visibleColumns.includes( column.accessor ) );
    }

    /**
     * Gets ids of selected values from table.
     * Sets valueIds state.
     * @returns {Array}
     */
    setSelectedValues = () => {
        const selectedValues = [];

        this.leaveCreditsTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedValues.push( this.leaveCreditsTable.props.data[ index ]);
            }
        });

        this.setState({ selectedValues });

        return this.state.selectedValues.map( ( selectedValue ) => selectedValue.id );
    }

    /**
     * Gets selected values ids.
     * @returns {Array}
     */
    getSelectedValuesIds() {
        return this.state.selectedValues.map( ( item ) => item.id );
    }

    /**
     * Checks is leave credits table rendered.
     * @returns {Boolean}
     */
    leaveCreditsTableIsNotRendered() {
        return !this.leaveCreditsTable ||
            !this.leaveCreditsTable.tableComponent ||
            !this.leaveCreditsTable.tableComponent.state;
    }

    /**
     * handles filters and search inputs.
     */
    handleSearch = ( term ) => {
        if ( term !== this.state.term ) {
            this.setState({ term }, () => {
                this.getEmployeeLeaveCredits();
            });
        }
    }

    /**
     * Handle changes to table data.
     * @param {Object} tableProps
     */
    handleDataChange = ( tableProps ) => {
        if ( !tableProps.sorting || !tableProps.sorting.length ) return;

        const sortCondition = first( tableProps.sorting );

        this.setState({
            sorting: {
                by: sortCondition.id,
                order: sortCondition.desc ? 'desc' : 'asc'
            }
        }, () => {
            this.getEmployeeLeaveCredits();
        });
    }

    /**
     * Handle fetching table data.
     * @param {Oject} data
     */
    handlePageChange = ( page ) => {
        this.setState({
            pagination: {
                ...this.state.pagination,
                page
            }
        }, () => {
            this.getEmployeeLeaveCredits();
        });
    }

    /**
     * Handles page size change and calls getEmployeeLeaveCredits on change.
     * @param {Number} pageSize
     */
    handlePageSizeChange = ( pageSize ) => {
        if ( !pageSize ) {
            return;
        }

        this.setState({
            pagination: {
                ...this.state.pagination,
                pageSize
            }
        }, () => {
            this.getEmployeeLeaveCredits();
        });
    }

    /**
     * Handle click on add leave credits button.
     */
    handleAddClick = () => {
        browserHistory.push( `/time/leave-credits/add?employee_id=${this.props.employee.id}`, true );
    }

    /**
     * Show more options for employee leave credits.
     */
    showMore = () => {
        this.setState({ moreActionsVisible: true }, () => {
            this.leaveCreditsTable.initColumns();
        });
    }

    /**
     * Start editing one row.
     * @param {Object} row
     */
    startEditingRow( row ) {
        this.setState({
            edited: clone( row )
        });
    }

    /**
     * Stop editing row.
     */
    stopEditing = ( callback ) => {
        this.setState({
            edited: null
        }, callback );
    }

    /**
     * Validate that row being edited contains required information.
     * @return {Boolean}
     */
    validateInlineEdit() {
        return this.earnedInput._validate( this.earnedInput.state.value );
    }

    /**
     * Save edited row.
     */
    saveEdited = () => {
        if ( this.validateInlineEdit() ) return;

        this.props.updateEmployeeLeaveCredits( this.state.edited, () => {
            this.stopEditing( this.getEmployeeLeaveCredits );
        });
    }

    /**
     * Check if given row is currently being edited.
     * @param {Object} row
     * @return {Boolean}
     */
    isCurrentlyEdited( row ) {
        return this.state.edited && this.state.edited.id === row.id;
    }

    /**
     * Update edited row state.
     * @param {String} property
     * @param {Number} value
     */
    updateEditedRowState( property, value ) {
        this.earnedInput.setState({ value: stripNonDigit( value ) });

        this.setState({
            edited: {
                ...this.state.edited,
                [ property ]: stripNonDigit( value )
            }
        });
    }

    /**
     * Export leave credits.
     */
    exportLeaveCredits = () => {
        if ( this.props.downloading ) return;

        const payload = {
            employee: this.props.employee,
            term: this.state.term
        };

        const ids = this.getSelectedValuesIds();

        if ( this.leaveCreditsTable.headerSelect.checked || !ids.length ) {
            payload.download_all = true;
        } else {
            payload.leave_credits_ids = ids;
        }

        this.props.exportLeaveCredits( payload );
    }

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <div className="employee-header">
                    <div className="section-title">
                        <span>Leave Credits</span>
                    </div>
                    {
                        this.state.moreActionsVisible ? (
                            <Button
                                className={ this.props.permission.create ? '' : 'hide' }
                                label="Add Leave Credits"
                                type="action"
                                onClick={ this.handleAddClick }
                            />
                        ) : (
                            <Button
                                label="Edit"
                                type="action"
                                alt
                                disabled={ this.state.moreActionsVisible }
                                onClick={ this.showMore }
                            />
                        )
                    }
                </div>
                <div className="section-heading mt-1-percent">
                </div>
                <div className="section-body">
                    <div className="content">
                        {
                            this.state.moreActionsVisible && (
                                <div className="title">
                                    <div className="search-wrapper">
                                    </div>
                                    <span>
                                        { this.state.label }
                                    </span>
                                    <span className="sl-u-gap-left--sm">
                                        <SalDropdown
                                            dropdownItems={ [
                                                {
                                                    label: 'Download',
                                                    children: this.props.downloading ? <Loader /> : <div>Download</div>,
                                                    onClick: this.exportLeaveCredits
                                                }
                                            ] }
                                        />
                                    </span>
                                </div>
                            )
                        }

                        <AsyncTable
                            data={ this.props.leaveCredits }
                            columns={ this.getTableColumns() }
                            onDataChange={ this.handleDataChange }
                            onPageChange={ this.handlePageChange }
                            ref={ ( ref ) => { this.leaveCreditsTable = ref; } }
                            onRowClick={ this.goToLeaveCreditDetails }
                            pagination
                            loading={ this.props.loading }
                            pages={ this.state.pagination.totalPages }
                            onPageSizeChange={ this.handlePageSizeChange }
                            pageSize={ this.state.pagination.pageSize }
                            defaultSorted={ [{ id: 'id', desc: false }] }
                            selectable={ this.state.moreActionsVisible }
                            onSelectionChange={ () => {
                                this.setSelectedValues();
                            } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }
}

export default EmployeeLeaveCredits;
