import React from 'react';

import Table from 'components/Table';
import Switch from 'components/Switch2';
import { getIdsOfSelectedRows } from 'components/Table/helpers';

import {
    formatPaginationLabel,
    formatDeleteLabel
} from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';

import { ProfileSectionWrapper } from '../styles';

/**
 * Employee Payment Methods component
 */
export default class EmployeePaymentMethods extends React.PureComponent {
    static propTypes = {
        employee: React.PropTypes.object,
        permission: React.PropTypes.object,
        loading: React.PropTypes.bool,
        id: React.PropTypes.string,
        paymentMethods: React.PropTypes.array,
        getEmployeePaymentMethods: React.PropTypes.func,
        deleteEmployeePaymentMethods: React.PropTypes.func,
        assignEmployeePaymentMethod: React.PropTypes.func
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayed_data: [],
            moreActionsVisible: false,
            credit_to: ''
        };
    }

    componentDidMount() {
        this.props.getEmployeePaymentMethods( this.props.employee.id );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.paymentMethods !== this.props.paymentMethods ) {
            const currentPaymentMethod = nextProps.paymentMethods.find( ( paymentMethod ) => paymentMethod.credit_to );
            const cashPaymentMethod = nextProps.paymentMethods.find( ( paymentMethod ) => paymentMethod.account_type === 'Cash' );

            this.setState({
                displayed_data: nextProps.paymentMethods,
                credit_to: currentPaymentMethod ? currentPaymentMethod.id : '',
                cash_id: cashPaymentMethod ? cashPaymentMethod.id : ''
            }, () => {
                this.handleSearch();
            });
        }
    }

    getTableColumns() {
        return [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'type',
                accessor: 'type',
                show: false
            },
            {
                id: 'account_type',
                header: 'Account Type',
                accessor: ( row ) => row.account_type || '-',
                minWidth: 170,
                maxWidth: 190
            },
            {
                id: 'account_details',
                header: 'Account Details',
                accessor: ( row ) => row.account_details || '-',
                minWidth: 260
            },
            {
                id: 'account_number',
                header: 'Account Number',
                accessor: ( row ) => row.account_number || '-',
                minWidth: 180
            },
            {
                header: 'Default Payment Method',
                accessor: 'credit_to',
                sortable: false,
                minWidth: 180,
                render: ({ row }) => (
                    <div className={ this.state.moreActionsVisible ? '' : 'no-cursor-pointer' }>
                        <Switch
                            id={ `payment-method-toggle-${row.id}` }
                            checked={ this.state.credit_to === row.id }
                            onChange={ ( value ) => this.state.moreActionsVisible && this.handleToggleChanges( value, row.id ) }
                            ref={ ( ref ) => { this[ row.id ] = ref; } }
                        />
                    </div>
                )
            }
        ];
    }

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.paymentMethodTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    /**
     * Handles changes to the assigned payment method
     * @param {Boolean} value
     * @param {String|Integer} id - ID of the payment method
     */
    handleToggleChanges = ( value, id ) => {
        const {
            cash_id: cashId,
            credit_to: creditTo
        } = this.state;

        const creditToCash = cashId === id && creditTo === id;

        if ( !creditToCash ) {
            this.setState({ credit_to: value ? id : cashId }, () =>
                this.props.assignEmployeePaymentMethod( this.props.employee.id, this.state.credit_to )
            );
        }
    }

    /**
     * Send a request to delete payment methods
     */
    deletePaymentMethods = () => {
        const ids = getIdsOfSelectedRows( this.paymentMethodTable );
        if ( ids ) {
            this.props.deleteEmployeePaymentMethods( this.props.employee.id, ids );
        }
    }

    handleSearch = ( term = '' ) => {
        const matchingRows = this.paymentMethodTable.getFilteredData( this.props.paymentMethods, term );

        this.setState({ displayed_data: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <SectionHeading
                    moreActionsVisible={ this.state.moreActionsVisible }
                    permission={ this.props.permission }
                    heading="Payment Methods"
                    id="tooltipPaymentMethods"
                    createButtonLabel="Add Payment Method"
                    showMore={ () => {
                        this.setState({ moreActionsVisible: true }, () => {
                            this.paymentMethodTable.initColumns();
                        });
                    } }
                    onAdd={ () => {
                        localStorage.setItem( 'prepopulatedEmployeeForAddPaymentMethod', JSON.stringify( this.props.employee ) );
                        browserHistory.push( '/disbursements/add' );
                    } }
                />
                <div className="section-body">
                    <div className="content">
                        <SectionBody
                            table={ this.paymentMethodTable }
                            permission={ this.props.permission }
                            deleteLabel={ this.state.deleteLabel }
                            label={ this.state.label }
                            deleteModalHeading="Delete Payment Methods"
                            moreActionsVisible={ this.state.moreActionsVisible }
                            onSearch={ this.handleSearch }
                            onDelete={ this.deletePaymentMethods }
                        />
                        <Table
                            data={ this.state.displayed_data }
                            columns={ this.getTableColumns() }
                            pagination
                            selectable={ this.state.moreActionsVisible }
                            loading={ this.props.loading }
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => { this.paymentMethodTable = ref; } }
                            onSelectionChange={ ({ selected }) => {
                                const selectionLength = selected.filter( ( row ) => row ).length;

                                this.setState({
                                    deleteLabel: formatDeleteLabel( selectionLength )
                                });
                            } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }
}
