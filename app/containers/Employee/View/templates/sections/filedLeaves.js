import React from 'react';
import first from 'lodash/first';
import isEmpty from 'lodash/isEmpty';
import each from 'lodash/each';

import AsyncTable from 'components/AsyncTable';
import Button from 'components/Button';
import SalDropdown from 'components/SalDropdown';
import Loader from 'components/Loader';

import {
    formatPaginationLabel,
    formatDate
} from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import { DATE_FORMATS } from 'utils/constants';

import {
    ProfileSectionWrapper
} from './../styles';
/**
 * Employee Filed Leaves component
 */
class EmployeeFiledLeaves extends React.Component {

    static propTypes = {
        filedLeaves: React.PropTypes.array,
        getEmployeeFiledLeaves: React.PropTypes.func,
        employee: React.PropTypes.object,
        permission: React.PropTypes.object,
        loading: React.PropTypes.bool,
        downloading: React.PropTypes.bool,
        exportFiledLeaves: React.PropTypes.func,
        id: React.PropTypes.string
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            term: '',
            moreActionsVisible: false,
            edited: null,
            sorting: {
                by: 'leave_type_name',
                order: 'asc'
            },
            pagination: {
                page: 0,
                pageSize: 10,
                totalPages: 0,
                dataLength: 0
            },
            selectedValues: []
        };
    }

    componentDidMount() {
        this.getEmployeeFiledLeaves();
    }

    /**
     * Handle new props
     */
    componentWillReceiveProps( newProps ) {
        if ( !isEmpty( newProps.filedLeavesPagination ) ) {
            this.setFiledLeavesPagination( newProps.filedLeavesPagination );
        }
    }

    /**
     * Get employee filed leaves.
     * @param {Object} data
     */
    getEmployeeFiledLeaves = () => {
        this.props.getEmployeeFiledLeaves( this.props.employee.id, {
            term: this.state.term,
            page: this.state.pagination.page,
            per_page: this.state.pagination.pageSize,
            sort_by: this.state.sorting.by,
            sort_order: this.state.sorting.order
        });
    }

    /**
     * Sets Filed Leaves Pagination
     */
    setFiledLeavesPagination = ( filedLeavesPagination ) => {
        if ( !this.filedLeavesTable || !this.filedLeavesTable.tableComponent ) {
            return;
        }

        const pagination = {
            page: filedLeavesPagination.current_page,
            pageSize: filedLeavesPagination.per_page,
            totalPages: filedLeavesPagination.total_pages,
            dataLength: filedLeavesPagination.total
        };

        this.setState({
            pagination,
            label: formatPaginationLabel({
                page: filedLeavesPagination.current_page - 1,
                pageSize: filedLeavesPagination.per_page,
                dataLength: filedLeavesPagination.total
            })
        });
    }

    /**
     * Get table columns information.
     * @return {Array}
     */
    getTableColumns() {
        const allColumns = [
            {
                header: 'Leave Type',
                accessor: 'leave_type_name',
                minWidth: 70,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { row.leave_type.name }
                    </div>
                )
            },
            {
                header: 'Start Date',
                accessor: 'start_date',
                minWidth: 70,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { formatDate( row.start_date, DATE_FORMATS.DISPLAY ) }
                    </div>
                )
            },
            {
                header: 'End Date',
                accessor: 'end_date',
                minWidth: 70,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { formatDate( row.end_date, DATE_FORMATS.DISPLAY ) }
                    </div>
                )
            },
            {
                header: 'Quatity',
                accessor: 'total_value',
                minWidth: 150,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { `${row.total_value} ${row.unit}` }
                    </div>
                )
            },
            {
                header: 'Status',
                accessor: 'status',
                minWidth: 50,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { row.status }
                    </div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 30,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.props.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => {
                            this.handleEditClick( row );
                        } }
                    />
                )
            }
        ];

        const visibleColumns = this.state.moreActionsVisible
            ? [ 'leave_type_name', 'start_date', 'end_date', 'quantity', 'status', 'actions' ]
            : [ 'leave_type_name', 'start_date', 'end_date', 'quantity', 'status' ];

        return allColumns.filter( ( column ) => visibleColumns.includes( column.accessor ) );
    }

    /**
     * Gets schedule names.
     * @param {Object} row
     * @return {String} schedules
     */
    getScheduleNames( row ) {
        let scheduleNames = '';

        each( row.leaves, ( leave, i ) => {
            scheduleNames += leave.schedule ? leave.schedule.name : 'Default Schedule';
            scheduleNames += ( i !== row.leaves.length - 1 ) ? ', ' : '';
        });

        return scheduleNames;
    }

    /**
     * Gets ids of selected values from table.
     * Sets valueIds state.
     * @returns {Array}
     */
    setSelectedValues = () => {
        const selectedValues = [];

        this.filedLeavesTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedValues.push( this.filedLeavesTable.props.data[ index ]);
            }
        });

        this.setState({ selectedValues });

        return this.state.selectedValues.map( ( selectedValue ) => selectedValue.id );
    }

    /**
     * Checks is filed leaves table rendered.
     * @returns {Boolean}
     */
    filedLeavesTableIsNotRendered() {
        return !this.filedLeavesTable ||
            !this.filedLeavesTable.tableComponent ||
            !this.filedLeavesTable.tableComponent.state;
    }

    /**
     * Handle changes to table data.
     * @param {Object} tableProps
     */
    handleDataChange = ( tableProps ) => {
        if ( !tableProps.sorting || !tableProps.sorting.length ) return;

        const sortCondition = first( tableProps.sorting );

        this.setState({
            sorting: {
                by: sortCondition.id,
                order: sortCondition.desc ? 'desc' : 'asc'
            }
        }, () => {
            this.getEmployeeFiledLeaves();
        });
    }

    /**
     * Handle fetching table data.
     * @param {Oject} data
     */
    handlePageChange = ( page ) => {
        this.setState({
            pagination: {
                ...this.state.pagination,
                page
            }
        }, () => {
            this.getEmployeeFiledLeaves();
        });
    }

    /**
     * Handles page size change and calls getEmployeeFiledLeaves on change.
     * @param {Number} pageSize
     */
    handlePageSizeChange = ( pageSize ) => {
        if ( !pageSize ) {
            return;
        }

        this.setState({
            pagination: {
                ...this.state.pagination,
                pageSize
            }
        }, () => {
            this.getEmployeeFiledLeaves();
        });
    }

    /**
     * Handle click on add filed leaves button.
     */
    handleAddClick = () => {
        browserHistory.push( `/time/filed-leaves/add?employee_id=${this.props.employee.id}`, true );
    }

    /**
     * Handles search inputs.
     */
    handleSearch = ( term ) => {
        if ( term !== this.state.term ) {
            this.setState({ term }, () => {
                this.getEmployeeFiledLeaves();
            });
        }
    }

    /**
     * Show more options for employee filed leaves.
     */
    showMore = () => {
        this.setState({ moreActionsVisible: true }, () => {
            this.filedLeavesTable.initColumns();
        });
    }

    /**
     * Handle click on edit filed leaves buttion.
     */
    handleEditClick = ( row ) => {
        browserHistory.push( `/time/filed-leaves/${row.id}/edit?is_profile=true`, true );
    }

    /**
     * Export filed leaves.
     */
    exportFiledLeaves = () => {
        if ( this.props.downloading ) return;

        const payload = {
            employee: this.props.employee,
            term: this.state.term
        };

        const ids = this.state.selectedValues.map( ( item ) => item.id );

        if ( this.filedLeavesTable.headerSelect.checked || !ids.length ) {
            payload.download_all = true;
        } else {
            payload.leave_requests_ids = ids;
        }

        this.props.exportFiledLeaves( payload );
    }

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <div className="employee-header">
                    <div className="section-title">
                        <span>Filed Leaves</span>
                    </div>
                    {
                        this.state.moreActionsVisible ? (
                            <Button
                                className={ this.props.permission.create ? '' : 'hide' }
                                label="Add Filed Leave"
                                type="action"
                                onClick={ this.handleAddClick }
                            />
                        ) : (
                            <Button
                                label="Edit"
                                type="action"
                                alt
                                disabled={ this.state.moreActionsVisible }
                                onClick={ this.showMore }
                            />
                        )
                    }
                </div>
                <div className="section-heading mt-1-percent">
                </div>
                <div className="section-body">
                    <div className="content">
                        {
                            this.state.moreActionsVisible && (
                                <div className="title">
                                    <div className="search-wrapper">
                                    </div>
                                    <span>
                                        { this.state.label }
                                    </span>
                                    <span className="sl-u-gap-left--sm">
                                        <SalDropdown
                                            dropdownItems={ [
                                                {
                                                    label: 'Download',
                                                    children: this.props.downloading ? <Loader /> : <div>Download</div>,
                                                    onClick: this.exportFiledLeaves
                                                }
                                            ] }
                                        />
                                    </span>
                                </div>
                            )
                        }

                        <AsyncTable
                            data={ this.props.filedLeaves }
                            columns={ this.getTableColumns() }
                            onDataChange={ this.handleDataChange }
                            onPageChange={ this.handlePageChange }
                            ref={ ( ref ) => { this.filedLeavesTable = ref; } }
                            onRowClick={ this.goToFiledLeavesDetails }
                            pagination
                            loading={ this.props.loading }
                            pages={ this.state.pagination.totalPages }
                            onPageSizeChange={ this.handlePageSizeChange }
                            pageSize={ this.state.pagination.pageSize }
                            defaultSorted={ [{ id: 'id', desc: false }] }
                            selectable={ this.state.moreActionsVisible }
                            onSelectionChange={ () => {
                                this.setSelectedValues();
                            } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }

}

export default EmployeeFiledLeaves;
