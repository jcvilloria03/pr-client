/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
import React from 'react';
import startCase from 'lodash/startCase';
import get from 'lodash/get';

import { browserHistory } from 'utils/BrowserHistory';
import { formatDate, formatCurrency } from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import { H2, H3 } from 'components/Typography';
import Button from 'components/Button';
import Table from 'components/Table';
import Loader from 'components/Loader';

import {
    ProfileSectionWrapper,
    LoadingStyles,
    BreakdownWrapper
} from '../styles';

export const FINAL_PAY_STATUSES = {
    OPENED: 'OPENED',
    CLOSED: 'CLOSED',
    DISBURSED: 'DISBURSED'
};

/**
 * Employee Approval Groups component
 */
class EmployeeTerminationInformation extends React.PureComponent {
    static propTypes = {
        employee: React.PropTypes.object,
        terminationInformation: React.PropTypes.object,
        permission: React.PropTypes.object,
        loading: React.PropTypes.bool,
        id: React.PropTypes.string,
        deleteConfirmationModal: React.PropTypes.func
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            editing: false,
            terminationInformation: {}
        };
    }
    getTableColumns() {
        return [
            {
                header: 'Gross Income',
                accessor: 'gross_income',
                sortable: false,
                minWidth: 140,
                render: ({ value }) => this.formatValueForDisplay( value ),
                className: 'align-left',
                headerClassName: 'align-left'
            },
            {
                header: 'Non-Taxable Income',
                accessor: 'non_taxable_income',
                sortable: false,
                minWidth: 185,
                render: ({ value }) => this.formatValueForDisplay( value ),
                className: 'align-left',
                headerClassName: 'align-left'
            },
            {
                header: 'Taxable Income',
                accessor: 'taxable_income',
                sortable: false,
                minWidth: 150,
                render: ({ value }) => this.formatValueForDisplay( value ),
                className: 'align-left',
                headerClassName: 'align-left'
            },
            {
                header: 'Withholding Tax as Adjusted',
                accessor: 'withholding_tax_as_adjusted',
                sortable: false,
                minWidth: 250,
                render: ({ value }) => (
                    <div className={ Number( value ) < 0 ? 'red-font' : '' }>
                        { this.formatValueForDisplay( value ) }
                    </div>
                ),
                className: 'align-left',
                headerClassName: 'align-left'
            },
            {
                header: 'Net Pay',
                accessor: 'net_pay',
                sortable: false,
                minWidth: 100,
                render: ({ value }) => this.formatValueForDisplay( value ),
                className: 'align-left',
                headerClassName: 'align-left'
            }
        ];
    }

    formatValueForDisplay( value ) {
        const isNegative = Number( value ) < 0;
        const formatted = formatCurrency( Math.abs( value ) );
        return isNegative ? `( ${formatted} )` : formatted;
    }

    deleteConfirmationModal() {
        this.props.deleteConfirmationModal();
    }

    renderItems( items, additionalClass = '' ) {
        return (
            <div className={ `items ${additionalClass}` }>
                { items.map( ( item, index ) => (
                    <div className="item" key={ index }>
                        <div className="item-name">{ item.label }</div>
                        <div className="item-value">{ this.formatValueForDisplay( item.amount ) }</div>
                    </div>
                ) ) }
            </div>
        );
    }

    render() {
        const { terminationInformation, employee } = this.props;

        const payrollStatus = get( terminationInformation, 'payroll.status', '' );
        const isFinalPayStatusClosedOrDisbursed = payrollStatus === FINAL_PAY_STATUSES.CLOSED || payrollStatus === FINAL_PAY_STATUSES.DISBURSED;
        const hasUnsavedProjectedFinalPay = Boolean( get( terminationInformation, 'final_pay_projected' ) ) && employee.active === 'active';
        const showCancelTerminationButton = !get( terminationInformation, 'payroll.id', 0 );

        const finalProjectedPay = get( terminationInformation, 'final_pay_projected' );
        const taxableIncomeItems = get( finalProjectedPay, 'taxable_incomes', []);
        const nonTaxableIncomeItems = get( finalProjectedPay, 'non_taxable_incomes', []);
        const deductionItems = get( finalProjectedPay, 'deductions', []);

        const tableData = [finalProjectedPay];

        const editButtonRoute = hasUnsavedProjectedFinalPay
            ? 'computation'
            : ( isFinalPayStatusClosedOrDisbursed
                ? 'checklist'
                : 'edit'
            );

        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <div className="employee-header">
                    <div className="section-title">
                        <span>Termination Information</span>
                    </div>
                    <div>
                        { showCancelTerminationButton && (
                            <Button
                                label={ this.props.loading ? ( <Loader /> ) : 'Cancel Termination Details' }
                                type="action"
                                alt
                                disabled={ this.props.loading }
                                onClick={ () => { this.deleteConfirmationModal(); } }
                            />
                        )}
                        {
                            !this.props.loading && this.props.permission.create && (
                            <Button
                                label={ isFinalPayStatusClosedOrDisbursed ? 'View' : 'Edit' }
                                type="action"
                                alt
                                onClick={ () => browserHistory.push(
                                        `/employee/${this.props.employee.id}/termination/${terminationInformation.id}/${editButtonRoute}`,
                                        true
                                    ) }
                            />
                            )
                        }
                    </div>
                </div>
                <div className="section-heading mt-40">
                    <span>Termination Details</span>
                </div>

                { this.props.loading ? (
                    <LoadingStyles maxHeight="100px">
                        <H2>Loading Termination Information.</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoadingStyles>
                ) : (
                    <div>
                        <div className="section-body">
                            <div className="section-row row">
                                <div className="col-xs-3">
                                    <div className="field-label">Final Pay Attendance Start Date:</div>
                                    <div className="field-value">{ formatDate( terminationInformation.start_date_basis, DATE_FORMATS.DISPLAY ) }</div>
                                </div>
                                <div className="col-xs-3">
                                    <div className="field-label">Employee Last Day:</div>
                                    <div className="field-value">{ formatDate( terminationInformation.last_date, DATE_FORMATS.DISPLAY ) }</div>
                                </div>
                                <div className="col-xs-3">
                                    <div className="field-label">Proposed Release Date:</div>
                                    <div className="field-value">{ formatDate( terminationInformation.proposed_release_date, DATE_FORMATS.DISPLAY ) }</div>
                                </div>
                                <div className="col-xs-3">
                                    <div className="field-label">Status:</div>
                                    <div className="field-value">{ isFinalPayStatusClosedOrDisbursed ? 'Processed' : 'Awaiting Processing' }</div>
                                </div>
                            </div>
                            <div className="section-row row" style={ { marginTop: '20px' } }>
                                <div className="col-xs-12">
                                    <div className="field-label">Reason for Leaving:</div>
                                    <div className="field-value">{ terminationInformation.reason_for_leaving ? startCase( terminationInformation.reason_for_leaving.toLowerCase() ) : '' }</div>
                                </div>
                            </div>
                        </div>

                        <div className="section-heading mt-40">
                            <span>Projected Final Pay</span>
                        </div>

                        <div className="section-body">
                            {
                                finalProjectedPay && (
                                    <div>
                                        <Table
                                            data={ tableData }
                                            columns={ this.getTableColumns() }
                                        />

                                        <BreakdownWrapper>
                                            <div className="column">
                                                <span className="column-title">Taxable Income</span>
                                                { this.renderItems( taxableIncomeItems ) }
                                            </div>
                                            <div className="column">
                                                <span className="column-title">Non-Taxable Income</span>
                                                { this.renderItems( nonTaxableIncomeItems ) }
                                            </div>
                                            <div className="column">
                                                <span className="column-title">Deductions</span>
                                                { this.renderItems( deductionItems ) }
                                            </div>
                                            <div className="column">
                                                <span className="column-title">Net Pay</span>
                                                <div className="items">
                                                    <div className="item">
                                                        <div className="item-name"></div>
                                                        <div className="item-value">{ this.formatValueForDisplay( finalProjectedPay.net_pay ) }</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </BreakdownWrapper>
                                    </div>
                                )
                            }
                        </div>
                    </div>
                ) }
            </ProfileSectionWrapper>
        );
    }
}

export default EmployeeTerminationInformation;
