import React from 'react';

import { H2, H3 } from 'components/Typography';

import { formatCurrency, formatDate } from 'utils/functions';
import { CURRENCY_FIELDS, PREVIOUS_EMPLOYER_INFO_FIELDS } from 'utils/AnnualEarnings/constants';
import { DATE_FORMATS } from 'utils/constants';

import {
    ProfileSectionWrapper,
    LoadingStyles
} from '../styles';

/**
 * Employee Annual Earnings component
 */
class EmployeeAnnualEarnings extends React.PureComponent {
    static propTypes = {
        employee: React.PropTypes.object,
        annualEarnings: React.PropTypes.object,
        loading: React.PropTypes.bool,
        getEmployeeAnnualEarnings: React.PropTypes.func,
        id: React.PropTypes.string
    };

    constructor( props ) {
        super( props );

        this.state = {
            has_previous_employer_data: false
        };
    }

    componentDidMount() {
        this.props.getEmployeeAnnualEarnings( this.props.employee.id );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.annualEarnings !== this.props.annualEarnings ) {
            const hasPrevEmployeeInfo = PREVIOUS_EMPLOYER_INFO_FIELDS.some( ( field ) => nextProps.annualEarnings[ field ]);
            const hasPrevEmployeeIncomes = CURRENCY_FIELDS.PREVIOUS_EMPLOYER.some( ( field ) => Number( nextProps.annualEarnings[ field ]) );

            this.setState({ has_previous_employer_data: hasPrevEmployeeInfo || hasPrevEmployeeIncomes });
        }
    }

    render() {
        const { annualEarnings } = this.props;

        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <div className="employee-header">
                    <div className="section-title">
                        <span>Annual Earnings</span>
                    </div>
                </div>
                <div className="section-body">
                    { this.props.loading ? (
                        <LoadingStyles>
                            <H2>Loading Annual Earnings.</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    ) : (
                        <div>
                            <div className="section-heading">
                                <span>Current Employer</span>
                            </div>
                            <div className="section-body">
                                <div className="row">
                                    <div className="section-sub-heading col-xs-12">
                                        <span>Non-Taxable Income</span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Basic Salary/Statutory Minimum Wage (MWE)</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_basic_salary_or_statutory_minimum_wage || 0 ) }</div>
                                    </div>
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Holiday Pay (MWE)</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_holiday_pay_mwe || 0 ) }</div>
                                    </div>
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Overtime Pay (MWE)</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_overtime_pay_mwe || 0 ) }</div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Night Shift Differential (MWE)</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_nt_differential_mwe || 0 ) }</div>
                                    </div>
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">De Minimis Benefits</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_de_minimis_benefits || 0 ) }</div>
                                    </div>
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Salaries &amp; Other Forms of Compensation</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_non_taxable_salaries_and_other_forms_of_compensation || 0 ) }</div>
                                    </div>
                                </div>
                            </div>

                            <div className="section-body">
                                <div className="row">
                                    <div className="section-sub-heading col-xs-12">
                                        <span>Taxable Income</span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Basic Salary</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_basic_salary || 0 ) }</div>
                                    </div>
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Overtime Pay</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_taxable_ot_pay || 0 ) }</div>
                                    </div>
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Commission</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_taxable_commission || 0 ) }</div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Others Regular</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_others_regular || 0 ) }</div>
                                    </div>
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Others Supplementary</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_others_supplementary || 0 ) }</div>
                                    </div>
                                </div>
                            </div>

                            <div className="section-body">
                                <div className="row">
                                    <div className="section-sub-heading col-xs-12">
                                        <span>Others</span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">13th Month Pay &amp; Other Benefits</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_13th_month_pay_and_other_benefits || 0 ) }</div>
                                    </div>
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Premium Paid on Health &amp; Hospital Insurance</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_premium_paid_on_health_and_hospital_insurance || 0 ) }</div>
                                    </div>
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Tax Withheld</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_tax_withheld || 0 ) }</div>
                                    </div>
                                </div>
                                <div className="row mb-5-percent">
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Employee SSS Contribution</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_employee_sss_contribution || 0 ) }</div>
                                    </div>
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Employee PhilHealth Contribution</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_employee_philhealth_contribution || 0 ) }</div>
                                    </div>
                                    <div className="section-column col-xs-4">
                                        <div className="field-label">Employee Pag-IBIG Contribution</div>
                                        <div className="field-value">{ formatCurrency( annualEarnings.current_employee_pagibig_contribution || 0 ) }</div>
                                    </div>
                                </div>
                            </div>

                            { this.state.has_previous_employer_data && (
                            <div>
                                <div className="section-heading">
                                    <span>Previous Employer</span>
                                </div>

                                <div className="section-body">
                                    <div className="row">
                                        <div className="section-sub-heading col-xs-12">
                                            <span>Previous Employer Details</span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">For The Period From</div>
                                            <div className="field-value">{ formatDate( annualEarnings.prev_date_from, DATE_FORMATS.DISPLAY ) || '-' }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">For The Period To</div>
                                            <div className="field-value">{ formatDate( annualEarnings.prev_date_to, DATE_FORMATS.DISPLAY ) || '-' }</div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Registered Name</div>
                                            <div className="field-value">{ annualEarnings.prev_employer_business_name || '-' }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Tax Identification Number (TIN)</div>
                                            <div className="field-value">{ annualEarnings.prev_employer_business_address || '-' }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">ZIP code</div>
                                            <div className="field-value">{ annualEarnings.prev_employer_zip_code || '-' }</div>
                                        </div>
                                    </div>
                                </div>

                                <div className="section-body">
                                    <div className="row">
                                        <div className="section-sub-heading col-xs-12">
                                            <span>Non-Taxable Income</span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Basic Salary/Statutory Minimum Wage (MWE)</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_basic_salary_or_statutory_minimum_wage || 0 ) }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Holiday Pay (MWE)</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_holiday_pay_mwe || 0 ) }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Overtime Pay (MWE)</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_overtime_pay_mwe || 0 ) }</div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Night Shift Differential (MWE)</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_nt_differential_mwe || 0 ) }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">13th Month Pay and Other Benefits</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_non_taxable_13th_month_pay_and_other_benefits || 0 ) }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">De Minimis Benefits</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_de_minimis_benefits || 0 ) }</div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">SSS, GSIS, PHIC &amp; Pag-IBIG Contributions</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_sss_gsis_phic_pagibig_contribution || 0 ) }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Salaries &amp; Other Forms of Compensation</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_non_taxable_salaries_and_other_forms_of_compensation || 0 ) }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Total Non-Taxable Compensation Income</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_total_non_taxable_compensation_income || 0 ) }</div>
                                        </div>
                                    </div>
                                </div>

                                <div className="section-body">
                                    <div className="row">
                                        <div className="section-sub-heading col-xs-12">
                                            <span>Taxable Income</span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Salaries & Other Forms of Compensation</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_taxable_salaries_and_other_forms_of_compensation || 0 ) }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">13th Month Pay and Other Benefits</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_taxable_13th_month_pay_and_other_benefits || 0 ) }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Total Taxable Compensation Income</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_total_taxable_compensation_income || 0 ) }</div>
                                        </div>
                                    </div>
                                </div>

                                <div className="section-body">
                                    <div className="row">
                                        <div className="section-sub-heading col-xs-12">
                                            <span>Others</span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Gross Compensation Income</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_gross_compensation_income || 0 ) }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Premium Paid on Health &amp; Hospital Insurance</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_premium_paid_on_health_and_hospital_insurance || 0 ) }</div>
                                        </div>
                                        <div className="section-column col-xs-4">
                                            <div className="field-label">Total Amount of Taxes Withheld as Adjusted</div>
                                            <div className="field-value">{ formatCurrency( annualEarnings.prev_tax_withheld || 0 ) }</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            ) }
                        </div>
                    ) }
                </div>
            </ProfileSectionWrapper>
        );
    }
}

export default EmployeeAnnualEarnings;
