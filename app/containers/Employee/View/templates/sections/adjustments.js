/* eslint-disable import/first */
import React from 'react';
import startCase from 'lodash/startCase';

import Table from 'components/Table';
import Button from 'components/Button';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';

import { ProfileSectionWrapper } from '../styles';

import { browserHistory } from 'utils/BrowserHistory';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate
} from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import {
    getSortedData,
    getIdsOfSelectedRows
} from 'components/Table/helpers';

/**
 * Employee adjustments component
 */
export default class EmployeeAdjustments extends React.Component {
    static propTypes = {
        adjustments: React.PropTypes.array,
        employee: React.PropTypes.object,
        downloading: React.PropTypes.bool,
        permission: React.PropTypes.object,
        getEmployeeOtherIncome: React.PropTypes.func,
        exportAdjustments: React.PropTypes.func,
        deleteAdjustments: React.PropTypes.func,
        loading: React.PropTypes.bool,
        id: React.PropTypes.string
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: [],
            moreActionsVisible: false
        };
    }

    componentDidMount() {
        this.props.getEmployeeOtherIncome({
            employeeId: this.props.employee.id,
            otherIncomeType: 'ADJUSTMENT'
        });
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.adjustments !== this.props.adjustments ) {
            this.setState({ displayedData: nextProps.adjustments }, () => {
                this.handleSearch();
            });
        }
    }

    getTableColumns = () => {
        const allColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'type_name',
                accessor: ( row ) => startCase( row.type.name.toLowerCase() ),
                header: 'Adjustment Type',
                minWidth: 220
            },
            {
                id: 'amount',
                header: 'Amount',
                accessor: ( row ) => formatCurrency( row.amount ),
                minWidth: 150
            },
            {
                id: 'payroll_date',
                header: 'Payroll Date',
                accessor: ( row ) => ( row.release_details.length > 0 && row.release_details[ 0 ].date ? formatDate( row.release_details[ 0 ].date, DATE_FORMATS.DISPLAY ) : 'N/A' ),
                minWidth: 200
            },
            {
                id: 'disburse_through_special_pay_run',
                header: 'Special Pay Run',
                accessor: ( row ) => ( row.release_details.length > 0 && row.release_details[ 0 ].disburse_through_special_pay_run ? 'Yes' : 'No' ),
                minWidth: 180
            },
            {
                id: 'reason',
                header: 'Reason',
                accessor: ( row ) => row.reason,
                minWidth: 200
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 150,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    row.release_details.length > 0 && !row.release_details[ 0 ].status ? (
                        <Button
                            className={ this.props.permission.edit ? '' : 'hide' }
                            label={ <span>Edit</span> }
                            type="grey"
                            size="small"
                            onClick={ () => {
                                localStorage.setItem( 'employeeIdForEditAdjustment', this.props.employee.id );
                                browserHistory.push( `/adjustments/${row.id}/edit` );
                            } }
                        />
                    ) : null
                )
            }
        ];

        const visibleColumns = this.state.moreActionsVisible
            ? [ 'type_name', 'amount', 'payroll_date', 'disburse_through_special_pay_run', 'reason', 'actions' ]
            : [ 'type_name', 'amount', 'payroll_date', 'disburse_through_special_pay_run', 'reason' ];

        return allColumns.filter( ( column ) => (
            visibleColumns.includes( column.id ? column.id : column.accessor )
        ) );
    }

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.adjustmentsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    /**
     * Send request to export adjustments
     */
    exportAdjustments = () => {
        const sortedAdjustments = getSortedData( this.adjustmentsTable );
        this.props.exportAdjustments({ ids: sortedAdjustments.map( ( adjustment ) => adjustment.id ), type: 'ADJUSTMENT' });
    };

    /**
     * Send a request to delete adjustments
     */
    deleteAdjustments = () => {
        const ids = getIdsOfSelectedRows( this.adjustmentsTable );
        this.props.deleteAdjustments({ employeeId: this.props.employee.id, ids, type: 'ADJUSTMENT' });
    }

    handleSearch = ( term = '' ) => {
        const dataForDisplay = this.props.adjustments;
        const matchingRows = this.adjustmentsTable.getFilteredData( dataForDisplay, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <SectionHeading
                    moreActionsVisible={ this.state.moreActionsVisible }
                    permission={ this.props.permission }
                    heading="Adjustments"
                    id="tooltipAdjustments"
                    createButtonLabel="Add Adjustments"
                    showMore={ () => {
                        this.setState({ moreActionsVisible: true }, () => {
                            this.adjustmentsTable.initColumns();
                        });
                    } }
                    onAdd={ () => {
                        localStorage.setItem( 'prepopulatedEmployeeForAddAdjustment', JSON.stringify( this.props.employee ) );
                        browserHistory.push( '/adjustments/add' );
                    } }
                />
                <div className="section-body">
                    <div className="content">
                        <SectionBody
                            table={ this.adjustmentsTable }
                            permission={ this.props.permission }
                            deleteLabel={ this.state.deleteLabel }
                            label={ this.state.label }
                            deleteModalHeading="Delete Adjustments"
                            downloading={ this.props.downloading }
                            moreActionsVisible={ this.state.moreActionsVisible }
                            onDelete={ this.deleteAdjustments }
                            onSearch={ this.handleSearch }
                            onExport={ this.exportAdjustments }
                        />
                        <Table
                            data={ this.state.displayedData }
                            columns={ this.getTableColumns() }
                            pagination
                            selectable={ this.state.moreActionsVisible }
                            loading={ this.props.loading }
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => { this.adjustmentsTable = ref; } }
                            onSelectionChange={ ({ selected }) => {
                                const selectionLength = selected.filter( ( row ) => row ).length;

                                this.setState({
                                    deleteLabel: formatDeleteLabel( selectionLength )
                                });
                            } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }
}
