import React from 'react';

import Table from 'components/Table';
import Button from 'components/Button';
import {
    getSortedData,
    getIdsOfSelectedRows
} from 'components/Table/helpers';

import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate
} from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import { DATE_FORMATS } from 'utils/constants';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';

import { ProfileSectionWrapper } from '../styles';

import { COMMISSION_TYPE_BASIS } from '../../../../Commissions/constants';

/**
 * Employee commissions component
 */
export default class EmployeeCommissions extends React.Component {
    static propTypes = {
        commissions: React.PropTypes.array,
        employee: React.PropTypes.object,
        downloading: React.PropTypes.bool,
        permission: React.PropTypes.object,
        exportCommissions: React.PropTypes.func,
        deleteCommissions: React.PropTypes.func,
        loading: React.PropTypes.bool,
        getEmployeeOtherIncome: React.PropTypes.func,
        id: React.PropTypes.string
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: [],
            moreActionsVisible: false
        };
    }

    componentDidMount() {
        this.props.getEmployeeOtherIncome({
            employeeId: this.props.employee.id,
            otherIncomeType: 'COMMISSION'
        });
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.commissions !== this.props.commissions ) {
            const displayedData = this.getDataForDisplay( nextProps.commissions );
            this.setState({ displayedData }, () => {
                this.handleSearch();
            });
        }
    }

    getTableColumns = () => {
        const allColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'type_name',
                header: 'Commission Type',
                accessor: ( row ) => row.type_name,
                minWidth: 150
            },
            {
                id: 'amount',
                header: 'Amount',
                accessor: ( row ) => ( row.type.other_income_type_subtype.basis === COMMISSION_TYPE_BASIS.FIXED
                    ? `${formatCurrency( row.amount )}`
                    : `${formatCurrency( row.percentage )} %`
                ),
                minWidth: 150
            },
            {
                id: 'release_date',
                header: 'Release Date',
                accessor: ( row ) => row.release_date,
                minWidth: 150
            },
            {
                id: 'special_pay_run',
                header: 'Special Pay Run',
                accessor: ( row ) => row.special_pay_run,
                minWidth: 150
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => ( !row.release_details[ 0 ].status ? (
                    <Button
                        className={ this.props.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => {
                            localStorage.setItem( 'employeeIdForEditCommission', this.props.employee.id );
                            browserHistory.push( `/commissions/${row.id}/edit` );
                        } }
                    /> ) : null
                )
            }
        ];

        const visibleColumns = this.state.moreActionsVisible
            ? [ 'type_name', 'amount', 'release_date', 'special_pay_run', 'actions' ]
            : [ 'type_name', 'amount', 'release_date', 'special_pay_run' ];

        return allColumns.filter( ( column ) => (
            visibleColumns.includes( column.id ? column.id : column.accessor )
        ) );
    }

    getDataForDisplay = ( commissions ) => (
        commissions.map( ( data ) => ({
            ...data,
            type_name: data.type.name,
            release_date: formatDate( data.release_details[ 0 ].date, DATE_FORMATS.DISPLAY ),
            status: data.release_details[ 0 ].status ? 'Released' : 'Not yet released',
            special_pay_run: data.release_details[ 0 ].disburse_through_special_pay_run ? 'Yes' : 'No'
        }) )
    )

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.commissionsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    /**
     * Send request to export commissions
     */
    exportCommissions = () => {
        const sortedCommissions = getSortedData( this.commissionsTable );
        this.props.exportCommissions({ ids: sortedCommissions.map( ( commission ) => commission.id ), type: 'COMMISSION' });
    };

    /**
     * Send a request to delete commissions
     */
    deleteCommissions = () => {
        const ids = getIdsOfSelectedRows( this.commissionsTable );
        this.props.deleteCommissions({ employeeId: this.props.employee.id, ids, type: 'COMMISSION' });
    }

    handleSearch = ( term = '' ) => {
        const dataForDisplay = this.getDataForDisplay( this.props.commissions );
        const matchingRows = this.commissionsTable.getFilteredData( dataForDisplay, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <SectionHeading
                    moreActionsVisible={ this.state.moreActionsVisible }
                    permission={ this.props.permission }
                    heading="Commissions"
                    id="tooltipCommissions"
                    createButtonLabel="Add Commission"
                    showMore={ () => {
                        this.setState({ moreActionsVisible: true }, () => {
                            this.commissionsTable.initColumns();
                        });
                    } }
                    onAdd={ () => {
                        localStorage.setItem( 'prepopulatedEmployeeForAddCommission', JSON.stringify( this.props.employee ) );
                        browserHistory.push( '/commissions/add' );
                    } }
                />
                <div className="section-body">
                    <div className="content">
                        <SectionBody
                            table={ this.commissionsTable }
                            permission={ this.props.permission }
                            deleteLabel={ this.state.deleteLabel }
                            label={ this.state.label }
                            deleteModalHeading="Delete Commissions"
                            downloading={ this.props.downloading }
                            moreActionsVisible={ this.state.moreActionsVisible }
                            onDelete={ this.deleteCommissions }
                            onSearch={ this.handleSearch }
                            onExport={ this.exportCommissions }
                        />
                        <Table
                            data={ this.state.displayedData }
                            columns={ this.getTableColumns() }
                            pagination
                            selectable={ this.state.moreActionsVisible }
                            loading={ this.props.loading }
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => { this.commissionsTable = ref; } }
                            onSelectionChange={ ({ selected }) => {
                                const selectionLength = selected.filter( ( row ) => row ).length;

                                this.setState({
                                    deleteLabel: formatDeleteLabel( selectionLength )
                                });
                            } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }
}
