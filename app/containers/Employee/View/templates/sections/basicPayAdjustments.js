import React from 'react';
import moment from 'moment';

import Table from 'components/Table';
import Button from 'components/Button';
import {
    getIdsOfSelectedRows
} from 'components/Table/helpers';

import { browserHistory } from 'utils/BrowserHistory';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatDate
} from 'utils/functions';

import { DATE_FORMATS } from 'utils/constants';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';

import { ProfileSectionWrapper } from '../styles';
import { company } from '../../../../../utils/CompanyService';

/**
 * Basic pay adjustments component
 */
export default class BasicPayAdjustments extends React.Component {
    static propTypes = {
        employee: React.PropTypes.object,
        permission: React.PropTypes.object,
        basicPayAdjustments: React.PropTypes.array,
        loading: React.PropTypes.bool,
        deleteBasicPayAdjustments: React.PropTypes.func,
        getEmployeeBasicPayAdjustments: React.PropTypes.func,
        id: React.PropTypes.string
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: [],
            moreActionsVisible: false
        };
    }

    componentDidMount() {
        this.props.getEmployeeBasicPayAdjustments({
            employeeId: this.props.employee.id
        });
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.basicPayAdjustments !== this.props.basicPayAdjustments ) {
            this.setState({
                displayedData: nextProps.basicPayAdjustments.map( this.addNoSelectProperty )
            }, () => {
                this.handleSearch();
            });
        }
    }

    getTableColumns = () => ([
        {
            header: 'id',
            accessor: 'id',
            show: false
        },
        {
            id: 'basic_pay',
            header: 'Basic Pay',
            accessor: ( row ) => row.basic_pay,
            minWidth: 200
        },
        {
            id: 'effective_date',
            header: 'Effective Date',
            accessor: ( row ) => ( row.effective_date ? formatDate( row.effective_date, DATE_FORMATS.DISPLAY ) : 'N/A' ),
            minWidth: 200
        },
        {
            id: 'reason_for_adjustment',
            header: 'Reason for Adjustment',
            accessor: ( row ) => ( row.reason ? row.reason : '-' ),
            minWidth: 200
        },
        {
            header: ' ',
            accessor: 'actions',
            minWidth: 150,
            sortable: false,
            style: { justifyContent: 'end' },
            render: ({ row }) => (
                this.state.moreActionsVisible ? (
                    <Button
                        className={ this.props.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => {
                            browserHistory.push( `/employee/${this.props.employee.id}/basic-pay-adjustments/${row.id}/edit`, true );
                        } }
                    />
                ) : null
            )
        }
    ])

    isEarliestCreatedDate = ( date ) => {
        let isEarliestCreatedDate = true;

        this.props.basicPayAdjustments.forEach( ( adjustment ) => {
            if ( moment( adjustment.created_at ).isBefore( moment( date ) ) ) {
                isEarliestCreatedDate = false;
            }
        });

        return isEarliestCreatedDate;
    };

    addNoSelectProperty = ( data ) => ({
        ...data,
        noselect: this.isEarliestCreatedDate( data.created_at )
    });

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.basicPayAdjustmentsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    handleSearch = ( term = '' ) => {
        const dataForDisplay = this.props.basicPayAdjustments;
        const matchingRows = this.basicPayAdjustmentsTable.getFilteredData( dataForDisplay, term );

        this.setState({
            displayedData: matchingRows.map( this.addNoSelectProperty )
        }, () => {
            this.handleTableChanges();
        });
    }

    /**
     * Send a request to delete adjustments
     */
    deleteBasicPayAdjustments = () => {
        const ids = getIdsOfSelectedRows( this.basicPayAdjustmentsTable );

        this.props.deleteBasicPayAdjustments({
            ids,
            employeeId: this.props.employee.id
        });
    }

    goToBasicPayAdjustmentDetail = ( state, rowInfo, column ) => ({
        onDoubleClick: () => {
            if ( column.id !== 'select' ) {
                browserHistory.push( `/employee/${this.props.employee.id}/basic-pay-adjustments/${rowInfo.row.id}/detail`, true );
            }
        },
        style: {
            cursor: column.id !== 'select' ? 'pointer' : 'default'
        }
    })

    render() {
        const isDemo = company.isDemoCompany();

        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <SectionHeading
                    moreActionsVisible={ this.state.moreActionsVisible }
                    permission={ this.props.permission }
                    heading="Basic Pay Adjustments"
                    id="tooltipBasicPayAdjustments"
                    createButtonLabel="Add Basic Pay Adjustment"
                    showMore={ () => {
                        this.setState({
                            moreActionsVisible: true
                        }, () => {
                            this.basicPayAdjustmentsTable.initColumns();
                        });
                    } }
                    onAdd={ () => {
                        browserHistory.push( `/employee/${this.props.employee.id}/basic-pay-adjustments/add`, true );
                    } }
                    disabled={ isDemo }
                />
                <div className="section-body">
                    <div className="content">
                        <SectionBody
                            table={ this.basicPayAdjustmentsTable }
                            permission={ this.props.permission }
                            deleteLabel={ this.state.deleteLabel }
                            label={ this.state.label }
                            deleteModalHeading="Delete Basic Pay Adjustments"
                            moreActionsVisible={ this.state.moreActionsVisible }
                            onSearch={ this.handleSearch }
                            onDelete={ this.deleteBasicPayAdjustments }
                        />
                        <Table
                            data={ this.state.displayedData }
                            columns={ this.getTableColumns() }
                            pagination
                            selectable={ this.state.moreActionsVisible }
                            loading={ this.props.loading }
                            onRowClick={ this.goToBasicPayAdjustmentDetail }
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => {
                                this.basicPayAdjustmentsTable = ref;
                            } }
                            onSelectionChange={ ({ selected }) => {
                                const selectionLength = selected.filter( ( row ) => row ).length;

                                this.setState({
                                    deleteLabel: formatDeleteLabel( selectionLength )
                                });
                            } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }
}
