import React from 'react';

import Table from 'components/Table';
import Button from 'components/Button';
import {
    getSortedData,
    getIdsOfSelectedRows
} from 'components/Table/helpers';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency
} from 'utils/functions';

import { browserHistory } from 'utils/BrowserHistory';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';

import { ProfileSectionWrapper } from '../styles';

import { CREDIT_FREQUENCY } from '../../../../Allowances/constants';

/**
 * Employee allowances component
 */
export default class EmployeeAllowances extends React.Component {
    static propTypes = {
        allowances: React.PropTypes.array,
        employee: React.PropTypes.object,
        downloading: React.PropTypes.bool,
        permission: React.PropTypes.object,
        exportAllowances: React.PropTypes.func,
        deleteAllowances: React.PropTypes.func,
        loading: React.PropTypes.bool,
        getEmployeeOtherIncome: React.PropTypes.func,
        id: React.PropTypes.string
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: [],
            moreActionsVisible: false
        };
    }

    componentDidMount() {
        this.props.getEmployeeOtherIncome({
            employeeId: this.props.employee.id,
            otherIncomeType: 'ALLOWANCE'
        });
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.allowances !== this.props.allowances ) {
            const dataForDisplay = this.getDataForDisplay( nextProps.allowances );
            this.setState({ displayedData: dataForDisplay }, () => {
                this.handleSearch();
            });
        }
    }

    getTableColumns = () => {
        const allColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'type_name',
                header: 'Allowance Type',
                accessor: ( row ) => row.type_name,
                minWidth: 220
            },
            {
                id: 'amount',
                header: 'Amount',
                accessor: ( row ) => formatCurrency( row.amount ),
                minWidth: 120
            },
            {
                id: 'credit_frequency',
                header: 'Crediting Frequency',
                accessor: ( row ) => row.credit_frequency,
                minWidth: 170
            },
            {
                id: 'recurring',
                header: 'Recurring?',
                accessor: ( row ) => row.recurring,
                minWidth: 150
            },
            {
                id: 'prorated',
                header: 'Prorated?',
                accessor: ( row ) => row.prorated,
                minWidth: 150
            },
            {
                id: 'special_pay_run',
                header: 'Special Pay Run?',
                accessor: ( row ) => row.special_pay_run,
                minWidth: 180
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'end' },
                render: ({ row }) => (
                    <Button
                        className={ this.props.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => {
                            localStorage.setItem( 'employeeIdForEditAllowance', this.props.employee.id );
                            browserHistory.push( `/allowances/${row.id}/edit` );
                        } }
                    />
                )
            }
        ];

        const visibleColumns = this.state.moreActionsVisible
            ? [ 'amount', 'credit_frequency', 'recurring', 'type_name', 'prorated', 'special_pay_run', 'actions' ]
            : [ 'amount', 'credit_frequency', 'recurring', 'type_name', 'prorated', 'special_pay_run' ];

        return allColumns.filter( ( column ) => (
            visibleColumns.includes( column.id ? column.id : column.accessor )
        ) );
    }

    getDataForDisplay = ( allowances ) => (
        allowances.map( ( data ) => {
            const creditFrequency = CREDIT_FREQUENCY.find( ( f ) => f.value === data.credit_frequency );
            return {
                ...data,
                recurring: data.recurring ? 'Yes' : 'No',
                credit_frequency: creditFrequency ? creditFrequency.label : '-',
                type_name: data.type.name,
                prorated: data.prorated ? 'Yes' : 'No',
                special_pay_run: data.release_details[ 0 ].disburse_through_special_pay_run ? 'Yes' : 'No'
            };
        })
    )

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.allowancesTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    /**
     * Send request to export allowances
     */
    exportAllowances = () => {
        const sortedAllowances = getSortedData( this.allowancesTable );
        this.props.exportAllowances({ ids: sortedAllowances.map( ( allowance ) => allowance.id ), type: 'ALLOWANCE' });
    };

    /**
     * Send a request to delete allowances
     */
    deleteAllowances = () => {
        const ids = getIdsOfSelectedRows( this.allowancesTable );
        this.props.deleteAllowances({ employeeId: this.props.employee.id, ids, type: 'ALLOWANCE' });
    }

    handleSearch = ( term = '' ) => {
        const dataForDisplay = this.getDataForDisplay( this.props.allowances );
        const matchingRows = this.allowancesTable.getFilteredData( dataForDisplay, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <SectionHeading
                    moreActionsVisible={ this.state.moreActionsVisible }
                    permission={ this.props.permission }
                    heading="Allowances"
                    id="tooltipAllowances"
                    createButtonLabel="Add Allowances"
                    showMore={ () => {
                        this.setState({ moreActionsVisible: true }, () => {
                            this.allowancesTable.initColumns();
                        });
                    } }
                    onAdd={ () => {
                        localStorage.setItem( 'prepopulatedEmployeeForAddAllowance', JSON.stringify( this.props.employee ) );
                        browserHistory.push( '/allowances/add' );
                    } }
                />
                <div className="section-body">
                    <div className="content">
                        <SectionBody
                            table={ this.allowancesTable }
                            permission={ this.props.permission }
                            deleteLabel={ this.state.deleteLabel }
                            label={ this.state.label }
                            deleteModalHeading="Delete Allowances"
                            downloading={ this.props.downloading }
                            moreActionsVisible={ this.state.moreActionsVisible }
                            onDelete={ this.deleteAllowances }
                            onSearch={ this.handleSearch }
                            onExport={ this.exportAllowances }
                        />
                        <Table
                            data={ this.state.displayedData }
                            columns={ this.getTableColumns() }
                            pagination
                            selectable={ this.state.moreActionsVisible }
                            loading={ this.props.loading }
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => { this.allowancesTable = ref; } }
                            onSelectionChange={ ({ selected }) => {
                                const selectionLength = selected.filter( ( row ) => row ).length;

                                this.setState({
                                    deleteLabel: formatDeleteLabel( selectionLength )
                                });
                            } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }
}
