import React from 'react';

import Table from 'components/Table';
import Button from 'components/Button';

import {
    getSortedData
} from 'components/Table/helpers';

import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency
} from 'utils/functions';

import { browserHistory } from 'utils/BrowserHistory';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';

import { ProfileSectionWrapper } from '../styles';

/**
 * Employee Historical Earnings component
 */
export default class EmployeeHistoricalEarnings extends React.Component {
    static propTypes = {
        historicalEarnings: React.PropTypes.array,
        employee: React.PropTypes.object,
        downloading: React.PropTypes.bool,
        permission: React.PropTypes.object,
        exportHistoricalEarnings: React.PropTypes.func,
        loading: React.PropTypes.bool,
        getEmployeeHistoricalEarnings: React.PropTypes.func,
        id: React.PropTypes.string
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: [],
            moreActionsVisible: false
        };
    }

    componentDidMount() {
        this.props.getEmployeeHistoricalEarnings( this.props.employee.id );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.historicalEarnings !== this.props.historicalEarnings ) {
            this.setState({ displayedData: nextProps.historicalEarnings }, () => {
                this.handleSearch();
            });
        }
    }

    getTableColumns = () => {
        const allColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'year',
                header: 'Year',
                accessor: ( row ) => row.year,
                minWidth: 100
            },
            {
                id: 'gross_income',
                header: 'Gross Income',
                accessor: ( row ) => ( row.gross_income ? `Php ${formatCurrency( row.gross_income )}` : 'N/A' ),
                minWidth: 200
            },
            {
                id: 'taxable_income',
                header: 'Taxable Income',
                accessor: ( row ) => ( row.taxable_income ? `Php ${formatCurrency( row.taxable_income )}` : 'N/A' ),
                minWidth: 200
            },
            {
                id: 'w_tax',
                header: 'Withholding Tax',
                accessor: ( row ) => ( row.total_withholding_tax ? `Php ${formatCurrency( row.total_withholding_tax )}` : 'N/A' ),
                minWidth: 200
            },
            {
                id: 'net_income',
                header: 'Net Pay',
                accessor: ( row ) => ( row.net_income ? `Php ${formatCurrency( row.net_income )}` : 'N/A' ),
                minWidth: 200
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 150,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.props.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => {
                            localStorage.setItem( 'employeeIdForEditHistoricalEarnings', this.props.employee.id );
                            browserHistory.push( `/annual-earnings/${row.id}/edit` );
                        } }
                    />
                )
            }
        ];

        const visibleColumns = this.state.moreActionsVisible
            ? [ 'year', 'gross_income', 'taxable_income', 'w_tax', 'net_income', 'actions' ]
            : [ 'year', 'gross_income', 'taxable_income', 'w_tax', 'net_income' ];

        return allColumns.filter( ( column ) => (
            visibleColumns.includes( column.id ? column.id : column.accessor )
         ) );
    }

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.historicalEarningsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    /**
     * Send request to export Historical Earnings
     */
    exportHistoricalEarnings = () => {
        const sortedHistoricalEarnings = getSortedData( this.historicalEarningsTable );
        this.props.exportHistoricalEarnings({ ids: sortedHistoricalEarnings.map( ( historicalEarnings ) => historicalEarnings.id ), type: 'ADJUSTMENT' });
    };

    handleSearch = ( term = '' ) => {
        const dataForDisplay = this.props.historicalEarnings;
        const matchingRows = this.historicalEarningsTable.getFilteredData( dataForDisplay, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    goToDetailsPage = ( _, rowInfo, column ) => ({
        onClick: () => {
            if ( column.id !== 'select' && column.id !== 'actions' ) {
                localStorage.setItem( 'employeeIdForAnnualEarningsDetail', this.props.employee.id );
                browserHistory.push( `/annual-earnings/${rowInfo.row.id}/detail` );
            } else if ( column.id === 'actions' ) {
                localStorage.setItem( 'employeeIdForEditHistoricalEarnings', this.props.employee.id );
                browserHistory.push( `/annual-earnings/${rowInfo.row.id}/edit` );
            }
        },
        style: {
            cursor: column.id !== 'select' ? 'pointer' : 'default'
        }
    })

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <SectionHeading
                    moreActionsVisible={ this.state.moreActionsVisible }
                    permission={ this.props.permission }
                    heading="Historical Earnings"
                    id="tooltipHistoricalEarnings"
                    createButtonLabel="Add Historical Earnings"
                    showMore={ () => {
                        this.setState({ moreActionsVisible: true }, () => {
                            this.historicalEarningsTable.initColumns();
                        });
                    } }
                    onAdd={ () => {
                        localStorage.setItem( 'prepopulatedEmployeeForAddAnnualEarnings', JSON.stringify( this.props.employee ) );
                        browserHistory.push( '/annual-earnings/manual-entry' );
                    } }
                />
                <div className="section-body">
                    <div className="content">
                        <SectionBody
                            table={ this.historicalEarningsTable }
                            permission={ this.props.permission }
                            deleteLabel={ this.state.deleteLabel }
                            label={ this.state.label }
                            downloading={ this.props.downloading }
                            moreActionsVisible={ this.state.moreActionsVisible }
                            onSearch={ this.handleSearch }
                            onExport={ this.exportHistoricalEarnings }
                        />
                        <Table
                            data={ this.state.displayedData }
                            columns={ this.getTableColumns() }
                            pagination
                            selectable={ this.state.moreActionsVisible }
                            loading={ this.props.loading }
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => { this.historicalEarningsTable = ref; } }
                            onRowClick={ this.goToDetailsPage }
                            onSelectionChange={ ({ selected }) => {
                                const selectionLength = selected.filter( ( row ) => row ).length;

                                this.setState({
                                    deleteLabel: formatDeleteLabel( selectionLength )
                                });
                            } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }
}
