import React from 'react';
import find from 'lodash/find';
import get from 'lodash/get';
import values from 'lodash/values';
import keys from 'lodash/keys';
import isEmpty from 'lodash/isEmpty';
import { debounce } from 'lodash';

import { H3 } from 'components/Typography';
import Button from 'components/Button';
import Select from 'components/Select';
import SalConfirm from 'components/SalConfirm';
import Loader from 'components/Loader';

import { Fetch } from 'utils/request';
import { subscriptionService } from 'utils/SubscriptionService';

import {
    ProfileSectionWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from '../styles';

import { REQUEST_TYPES } from '../../constants';

/**
 * Employee Approval Workflows component
 */
class EmployeeApprovalGroups extends React.Component {
    static propTypes = {
        approvalGroups: React.PropTypes.array,
        requestTypes: React.PropTypes.array,
        employee: React.PropTypes.object,
        permission: React.PropTypes.object,
        loading: React.PropTypes.bool,
        updateEmployeeApprovalGroups: React.PropTypes.func,
        hasPendingRequests: React.PropTypes.bool,
        checkPendingRequests: React.PropTypes.func,
        setShouldShowDiscardConfirm: React.PropTypes.func,
        getEmployeeApprovalGroups: React.PropTypes.func,
        products: React.PropTypes.array,
        id: React.PropTypes.string
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            editing: false,
            approvalGroups: [],
            form: {},
            workflowLoading: false,
            isSaving: false,
            allDataFetched: false
        };
    }

    componentDidMount() {
        this.props.getEmployeeApprovalGroups( this.props.employee.id );
        this.getWorkflows();
    }

    /**
     * Handle new props
     */
    componentWillReceiveProps( newProps ) {
        if ( this.props.loading && !newProps.loading ) {
            this.setState({ editing: false, isSaving: false, form: {}});
        }
    }

    /**
     * Get request type based on name. We need this mostly for the request type id.
     * @param {String} type
     * @return {Object}
     */
    getRequestType( type ) {
        return find( this.props.requestTypes, { name: type });
    }

    /**
     * Get workflow entitlement by request type ID.
     * @param {Number} requestTypeId
     * @return {Object}
     */
    getEntitlementByRequestTypeId( requestTypeId ) {
        return find( this.props.approvalGroups, { request_type_id: requestTypeId });
    }

    /**
     * Get workflow entitlement by request type name.
     * @param {String} type
     * @return {Object}
     */
    getEntitlementForRequestType( type ) {
        const requestType = this.getRequestType( type );

        if ( !requestType ) {
            return null;
        }

        return this.getEntitlementByRequestTypeId( requestType.id );
    }

    getRequestWorkflowName( id, type ) {
        const workflow = this.state.approvalGroups.filter( ( item ) => item.id === id );
        let workflowName = workflow.length > 0 ? this.formatWorkflow( workflow[ 0 ]) : '';

        if ( workflowName === '' ) {
            workflowName = this.getRequestWorkflow( type );
        }

        return workflowName;
    }

    /**
     * Get formated name of chosen workflow for given request type.
     * @param {String} type
     * @return {String}
     */
    getRequestWorkflow( type ) {
        const entitlement = this.getEntitlementForRequestType( type );

        if ( !entitlement ) {
            return '';
        }

        return this.formatWorkflow( entitlement.workflow );
    }

    /**
     * Get form data from employees workflow entitlements (Approval Workflows).
     * @return {Object}
     */
    getFormData() {
        const form = {};

        values( REQUEST_TYPES ).forEach( ( type ) => {
            if ( type === REQUEST_TYPES.LOAN && !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
                return;
            }
            form[ type ] = get( this.getEntitlementForRequestType( type ), 'workflow.id', null );
        });

        return form;
    }

    async fetchWorkflows( companyId, page = 1, perPage = 20, keyword = '' ) {
        const queryParams = {
            per_page: perPage,
            page: page,
            term: keyword,
            response_size: 'simple'
        };
    
        try {
            const result = await Fetch(`/company/${companyId}/workflows`, { method: 'GET', params: queryParams });
            return result;
        } catch ( error ) {
            this.setState({ workflowLoading: false });
            return null;
        }
    }
    
    async getWorkflows() {
        this.setState({ workflowLoading: true });
        const { company_id: companyId } = this.props.employee;
    
        try {
            const initialResult = await this.fetchWorkflows(companyId);
            if ( initialResult.data ) {
                this.setState({ approvalGroups: initialResult.data });
            }

            if ( initialResult.meta && initialResult.meta.pagination ) {
                let totalPages = initialResult.meta.pagination.total_pages;
                let currentPage = initialResult.meta.pagination.current_page + 1;

                while ( currentPage <= totalPages ) {
                    const workflows = await this.fetchWorkflows(companyId, currentPage, 20);
                    if ( workflows && workflows.data ) {
                        this.setState(prevState => ({
                            approvalGroups: [...prevState.approvalGroups, ...workflows.data]
                        }));
                    } else {
                        break;
                    }
                    currentPage += 1;
                }
            }
    
            this.setState({ allDataFetched: true, workflowLoading: false });
        } catch ( error ) {
            this.setState({ workflowLoading: false });
        }
    }
    

    getValue( type ) {
        const id = this.state.form[ type ];
        return {
            disabled: false,
            label: this.getRequestWorkflowName( id, type ),
            value: id
        };
    }

    /**
     * Format workflow name with number of levels.
     * @param {Object} workflow
     * @return {String}
     */
    formatWorkflow( workflow ) {
        let workflowLevels = workflow.levels_count;
        if ( !workflowLevels ) {
            workflowLevels = workflow.levels.length;
        }
        const levels = workflowLevels > 1 ? 'levels' : 'level';

        return `${workflowLevels} approval ${levels} - ${workflow.name}`;
    }

    /**
     * Switch to editing mode.
     */
    startEditing = () => {
        this.setState({ editing: true, form: this.getFormData() }, () => {
            this.props.setShouldShowDiscardConfirm( true );
        });
    }

    loadWorkflows = async ( keyword, callback ) => {
        try {
            if ( isEmpty( keyword ) && this.state.allDataFetched ) {
                const workflows = this.state.approvalGroups.map( ({ id, name }) => ({
                    value: id,
                    label: name,
                    disabled: false
                }));
        
                callback( null, { options: workflows } );
            } else if ( this.state.allDataFetched ) {
                const filteredWorkflows = this.state.approvalGroups.filter( workflow =>
                    workflow.name.toLowerCase().includes(keyword.toLowerCase())
                );
        
                const workflowOptions = filteredWorkflows.map( ({ id, name }) => ({
                    value: id,
                    label: name,
                    disabled: false
                }));
        
                callback( null, { options: workflowOptions } );
            } else {
                const workflows = await this.fetchWorkflows( companyId, 1, 20, keyword );
    
                if ( workflows && workflows.data ) {
                    const newWorkflows = workflows.data.map( ({ id, name }) => ({
                        value: id,
                        label: name,
                        disabled: false
                    }));
    
                    callback( null, { options: newWorkflows } );
                } else {
                    callback( null, { options: [] } );
                }
            }
        } catch ( error ) {
            callback( null, { options: [] } );
        }
    }

    /**
     * Stop editing.
     */
    stopEditing = () => {
        this.setState({ isSaving: false, editing: false, form: {}}, () => {
            this.props.setShouldShowDiscardConfirm( false );
        });
    }

    /**
     * Update the form with new value for request type.
     * @param {String} type
     * @param {Object} selected
     */
    updateForm( type, selected ) {
        this.setState({
            form: {
                ...this.state.form,
                [ type ]: selected.value
            }
        });
    }

    /**
     * Map form data to prepare it for submission.
     * @return {Array}
     */
    prepareFormDataForSubmission() {
        return keys( this.state.form ).map( ( requestType ) => ({
            request_type: requestType,
            workflow_id: this.state.form[ requestType ]
        }) );
    }

    /**
     * Check if there are pending requests.
     */
    checkPendingRequests = () => {
        this.setState({ isSaving: true });
        const data = this.prepareFormDataForSubmission();
        data.employeeId = this.props.employee.id;

        this.props.checkPendingRequests( data, this.submit );
    }

    /**
     * Submit the form to update the employee Approval Workflows.
     */
    submit = () => {
        const data = this.prepareFormDataForSubmission();
        this.props.updateEmployeeApprovalGroups( this.props.employee.id, data );
    }

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <SalConfirm
                    onConfirm={ this.submit }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Saving changes. Pending requests in this changed workflows will be reset.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ this.props.hasPendingRequests }
                />
                <div className="employee-header mb-40">
                    <div className="section-title">
                        <span>Approval Workflows</span>
                    </div>
                    {
                        !this.props.loading && !this.state.editing && this.props.permission.create && (
                            <Button
                                label={ this.state.workflowLoading ? ( <Loader /> ) : 'Edit' }
                                type="action"
                                alt
                                disabled={ this.state.editing || this.state.workflowLoading }
                                onClick={ this.startEditing }
                            />
                        )
                    }
                    { !this.state.loading && this.state.editing && (
                        <div>
                            <Button
                                label={ 'Cancel' }
                                type="action"
                                alt
                                onClick={ this.stopEditing }
                                disabled={ this.state.isSaving || this.props.loading }
                            />
                            <Button
                                label={ this.state.isSaving ? ( <Loader /> ) : 'Update' }
                                type="action"
                                disabled={ this.state.isSaving || this.props.loading }
                                onClick={ this.checkPendingRequests }
                            />
                        </div>
                    ) }
                </div>
                <div className="section-heading">
                    <span>Approval Details</span>
                </div>
                <div className="section-body">
                    { this.props.loading && !this.state.isSaving ? (
                        <LoadingStyles>
                            <h4>Loading Approval Workflows.</h4>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    ) : (
                        <div>
                            <div className="row">
                                { this.state.editing ? (
                                    <div className="col-xs-3 field-value">
                                        <Select
                                            async
                                            id="selectTimeDisputeWorkflow"
                                            placeholder="Select Approval Group"
                                            loadOptions={ debounce( this.loadWorkflows, 600 ) }
                                            label="Time Correction"
                                            onChange={ ( value ) => {
                                                this.updateForm( REQUEST_TYPES.TIME_DISPUTE, value );
                                            } }
                                            value={ this.getValue( REQUEST_TYPES.TIME_DISPUTE ) }
                                            ref={ ( ref ) => { this.timeDisputeWorkflowSelect = ref; } }
                                        />
                                    </div>
                                ) : (
                                    <div className="col-xs-3">
                                        <div className="field-label">Time Correction:</div>
                                        <div className="field-value">{this.getRequestWorkflow( REQUEST_TYPES.TIME_DISPUTE )}</div>
                                    </div>
                                ) }
                                { this.state.editing ? (
                                    <div className="col-xs-3 field-value">
                                        <Select
                                            async
                                            loadOptions={ debounce( this.loadWorkflows, 600 ) }
                                            value={ this.getValue( REQUEST_TYPES.LEAVES ) }
                                            id="selectLeavesWorkflow"
                                            label="Leaves"
                                            placeholder="Select Group"
                                            onChange={ ( value ) => {
                                                this.updateForm( REQUEST_TYPES.LEAVES, value );
                                            } }
                                            ref={ ( ref ) => { this.leaveWorkflowSelect = ref; } }
                                        />
                                    </div>
                                ) : (
                                    <div className="col-xs-3">
                                        <div className="field-label">Leaves:</div>
                                        <div className="field-value">{this.getRequestWorkflow( REQUEST_TYPES.LEAVES )}</div>
                                    </div>
                                ) }
                                { this.state.editing ? (
                                    <div className="col-xs-3 field-value">
                                        <Select
                                            async
                                            loadOptions={ debounce( this.loadWorkflows, 600 ) }
                                            value={ this.getValue( REQUEST_TYPES.OVERTIME ) }
                                            id="selectOvertimeWorkflow"
                                            label="Overtime"
                                            placeholder="Select Group"
                                            onChange={ ( value ) => {
                                                this.updateForm( REQUEST_TYPES.OVERTIME, value );
                                            } }
                                            ref={ ( ref ) => { this.overtimeWorkflowSelect = ref; } }
                                        />
                                    </div>
                                ) : (
                                    <div className="col-xs-3">
                                        <div className="field-label">Overtime:</div>
                                        <div className="field-value">{this.getRequestWorkflow( REQUEST_TYPES.OVERTIME )}</div>
                                    </div>
                                ) }
                                { this.state.editing ? (
                                    <div className="col-xs-3 field-value">
                                        <Select
                                            async
                                            loadOptions={ debounce( this.loadWorkflows, 600 ) }
                                            value={ this.getValue( REQUEST_TYPES.SHIFT_CHANGE ) }
                                            id="selectShiftChangeWorkflow"
                                            label="Shift Change"
                                            autoSize
                                            placeholder="Select Group"
                                            onChange={ ( value ) => {
                                                this.updateForm( REQUEST_TYPES.SHIFT_CHANGE, value );
                                            } }
                                            ref={ ( ref ) => { this.shiftChangeWorkflowSelect = ref; } }
                                        />
                                    </div>
                                ) : (
                                    <div className="col-xs-3">
                                        <div className="field-label">Shift Change:</div>
                                        <div className="field-value">{this.getRequestWorkflow( REQUEST_TYPES.SHIFT_CHANGE )}</div>
                                    </div>
                                ) }
                            </div>
                            <div className="row">
                                { this.state.editing ? (
                                    <div className="col-xs-3 field-value">
                                        <Select
                                            async
                                            loadOptions={ debounce( this.loadWorkflows, 600 ) }
                                            value={ this.getValue( REQUEST_TYPES.PAID_UNDERTIME ) }
                                            id="selectPaidUndertimeWorkflow"
                                            label="Undertime"
                                            autoSize
                                            placeholder="Select Group"
                                            onChange={ ( value ) => {
                                                this.updateForm( REQUEST_TYPES.PAID_UNDERTIME, value );
                                            } }
                                            ref={ ( ref ) => { this.paidUndertimeWorkflowSelect = ref; } }
                                        />
                                    </div>
                                ) : (
                                    <div className="col-xs-3">
                                        <div className="field-label">Undertime:</div>
                                        <div className="field-value">{this.getRequestWorkflow( REQUEST_TYPES.PAID_UNDERTIME )}</div>
                                    </div>
                                ) }
                            </div>
                        </div>
                    ) }
                </div>
            </ProfileSectionWrapper>
        );
    }
}

export default EmployeeApprovalGroups;
