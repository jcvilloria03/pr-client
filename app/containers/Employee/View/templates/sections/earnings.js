import React from 'react';

import { H2, H3 } from 'components/Typography';
import ToolTip from 'components/ToolTip';

import { formatCurrency } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';

import { WITHHOLDING_TAX_TYPE } from '../../../../Earnings/constants';
import { getLabelForWithholdingTaxType } from '../../../../Earnings/utils';

import {
    ProfileSectionWrapper,
    LoadingStyles,
    ValueWrapper
} from '../styles';

/**
 * Employee Earnings component
 */
class EmployeeEarnings extends React.Component {
    static propTypes = {
        employee: React.PropTypes.object,
        earnings: React.PropTypes.object,
        permission: React.PropTypes.object,
        loading: React.PropTypes.bool,
        getEmployeeEarnings: React.PropTypes.func,
        id: React.PropTypes.string
    };

    componentDidMount() {
        this.props.getEmployeeEarnings( this.props.employee.id );
    }

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <div className="section-heading">
                    <span>Earnings</span>
                    {
                        !this.props.loading && this.props.permission.create && (
                            <ToolTip
                                id="tooltipEarnings"
                                target={
                                    <button
                                        id="tooltipEarnings"
                                        className="edit"
                                        onClick={ () => {
                                            browserHistory.push( `/earnings/${this.props.earnings.id}/edit` );
                                        } }
                                    >
                                        <i className="fa fa-pencil edit" />
                                    </button>
                                }
                                placement="top"
                            >
                                Edit this section
                            </ToolTip>
                        )
                    }
                </div>

                <div className="section-body" style={ { minHeight: '200px' } }>
                    { this.props.loading ? (
                        <LoadingStyles>
                            <H2>Loading Earnings.</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    ) : (
                        <div>
                            <div>
                                <div className="row" style={ { marginTop: '20px' } }>
                                    <div className="col-xs-3">
                                        <div>Basic Pay:</div>
                                        <ValueWrapper>
                                            { `Php ${formatCurrency( this.props.employee.payroll.base_pay || 0 )}` }
                                        </ValueWrapper>
                                    </div>
                                    <div className="col-xs-3">
                                        <div>Withholding Tax Type:</div>
                                        <ValueWrapper>
                                            { getLabelForWithholdingTaxType( this.props.earnings.with_holding_tax_type ) }
                                        </ValueWrapper>
                                    </div>
                                    { this.props.earnings.with_holding_tax_type === WITHHOLDING_TAX_TYPE.EXPANDED
                                        ? (
                                            <div className="col-xs-3">
                                                <div>Expanded Withholding Tax Rate:</div>
                                                <ValueWrapper>
                                                    { `${this.props.earnings.expanded_with_holding_tax_rate}%` }
                                                </ValueWrapper>
                                            </div>
                                        )
                                        : null
                                    }
                                </div>
                            </div>
                            <div>
                                <div className="row" style={ { marginTop: '20px' } }>
                                    <div className="col-xs-3">
                                        <div>Attendance Data Required:</div>
                                        <ValueWrapper>
                                            { this.props.earnings.attendance_data_required ? 'Yes' : 'No' }
                                        </ValueWrapper>
                                    </div>
                                    <div className="col-xs-3">
                                        <div>Entitled Unworked Regular Holiday Pay:</div>
                                        <ValueWrapper>
                                            { this.props.earnings.entitled_unworked_regular_holiday_pay ? 'Yes' : 'No' }
                                        </ValueWrapper>
                                    </div>
                                    <div className="col-xs-3">
                                        <div>Entitled Overtime Pay:</div>
                                        <ValueWrapper>
                                            { this.props.earnings.entitled_overtime ? 'Yes' : 'No' }
                                        </ValueWrapper>
                                    </div>
                                    <div className="col-xs-3">
                                        <div>Entitled Rest Day Pay:</div>
                                        <ValueWrapper>
                                            { this.props.earnings.entitled_rest_day_pay ? 'Yes' : 'No' }
                                        </ValueWrapper>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="row" style={ { marginTop: '20px' } }>
                                    <div className="col-xs-3">
                                        <div>Entitled Night Differential:</div>
                                        <ValueWrapper>
                                            { this.props.earnings.entitled_night_differential ? 'Yes' : 'No' }
                                        </ValueWrapper>
                                    </div>
                                    <div className="col-xs-3">
                                        <div>Entitled Unworked Special Holiday Pay:</div>
                                        <ValueWrapper>
                                            { this.props.earnings.entitled_unworked_special_holiday_pay ? 'Yes' : 'No' }
                                        </ValueWrapper>
                                    </div>
                                    <div className="col-xs-3">
                                        <div>Entitled Holiday Premium Pay:</div>
                                        <ValueWrapper>
                                            { this.props.earnings.entitled_holiday_premium_pay ? 'Yes' : 'No' }
                                        </ValueWrapper>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ) }
                </div>
            </ProfileSectionWrapper>
        );
    }
}

export default EmployeeEarnings;
