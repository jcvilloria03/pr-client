import React from 'react';

import Table from 'components/Table';
import Button from 'components/Button';

import {
    getSortedData,
    getIdsOfSelectedRows
} from 'components/Table/helpers';

import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate
} from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import { DATE_FORMATS } from 'utils/constants';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';

import { ProfileSectionWrapper } from '../styles';

import { PAY_FREQUENCIES } from '../../../../Deductions/View/constants';

/**
 * Employee deductions component
 */
export default class EmployeeDeductions extends React.Component {
    static propTypes = {
        deductions: React.PropTypes.array,
        employee: React.PropTypes.object,
        downloading: React.PropTypes.bool,
        permission: React.PropTypes.object,
        exportDeductions: React.PropTypes.func,
        deleteDeductions: React.PropTypes.func,
        loading: React.PropTypes.bool,
        getEmployeeOtherIncome: React.PropTypes.func,
        id: React.PropTypes.string
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: [],
            moreActionsVisible: false
        };
    }

    componentDidMount() {
        this.props.getEmployeeOtherIncome({
            employeeId: this.props.employee.id,
            otherIncomeType: 'DEDUCTION'
        });
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.deductions !== this.props.deductions ) {
            this.setState({ displayedData: nextProps.deductions }, () => {
                this.handleSearch();
            });
        }
    }

    getTableColumns = () => {
        const allColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'type_name',
                header: 'Deduction Type',
                accessor: ( row ) => row.type.name,
                minWidth: 260
            },
            {
                id: 'total_amount',
                header: 'Amount',
                accessor: ( row ) => formatCurrency( row.amount ),
                minWidth: 260
            },
            {
                id: 'frequency',
                header: 'Frequency',
                accessor: ( row ) => ( PAY_FREQUENCIES[ row.frequency ] ? PAY_FREQUENCIES[ row.frequency ] : 'N/A' ),
                minWidth: 300
            },
            {
                id: 'valid_from',
                header: 'Start Date',
                accessor: ( row ) => formatDate( row.valid_from, DATE_FORMATS.DISPLAY ),
                minWidth: 200
            },
            {
                id: 'valid_to',
                header: 'End Date',
                accessor: ( row ) => ( row.valid_to ? formatDate( row.valid_to, DATE_FORMATS.DISPLAY ) : 'N/A' ),
                minWidth: 200
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.props.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => {
                            localStorage.setItem( 'employeeIdForEditDeduction', this.props.employee.id );
                            browserHistory.push( `/deductions/${row.id}/edit` );
                        } }
                    />
                )
            }
        ];

        const visibleColumns = this.state.moreActionsVisible
            ? [ 'type_name', 'total_amount', 'frequency', 'valid_from', 'valid_to', 'actions' ]
            : [ 'type_name', 'total_amount', 'frequency', 'valid_from', 'valid_to' ];

        return allColumns.filter( ( column ) => (
            visibleColumns.includes( column.id ? column.id : column.accessor )
        ) );
    }

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.deductionsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    /**
     * Send request to export deductions
     */
    exportDeductions = () => {
        const sortedDeductions = getSortedData( this.deductionsTable );
        this.props.exportDeductions({ ids: sortedDeductions.map( ( deduction ) => deduction.id ), type: 'DEDUCTION' });
    };

    /**
     * Send a request to delete deductions
     */
    deleteDeductions = () => {
        const ids = getIdsOfSelectedRows( this.deductionsTable );
        this.props.deleteDeductions({ employeeId: this.props.employee.id, ids, type: 'DEDUCTION' });
    }

    handleSearch = ( term = '' ) => {
        const dataForDisplay = this.props.deductions;
        const matchingRows = this.deductionsTable.getFilteredData( dataForDisplay, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <SectionHeading
                    moreActionsVisible={ this.state.moreActionsVisible }
                    permission={ this.props.permission }
                    heading="Deductions"
                    id="tooltipDeductions"
                    createButtonLabel="Add Deduction"
                    showMore={ () => {
                        this.setState({ moreActionsVisible: true }, () => {
                            this.deductionsTable.initColumns();
                        });
                    } }
                    onAdd={ () => {
                        localStorage.setItem( 'prepopulatedEmployeeForAddDeduction', JSON.stringify( this.props.employee ) );
                        browserHistory.push( '/deductions/add' );
                    } }
                />
                <div className="section-body">
                    <div className="content">
                        <SectionBody
                            table={ this.deductionsTable }
                            permission={ this.props.permission }
                            deleteLabel={ this.state.deleteLabel }
                            label={ this.state.label }
                            deleteModalHeading="Delete Deductions"
                            downloading={ this.props.downloading }
                            moreActionsVisible={ this.state.moreActionsVisible }
                            onDelete={ this.deleteDeductions }
                            onSearch={ this.handleSearch }
                            onExport={ this.exportDeductions }
                        />
                        <Table
                            data={ this.state.displayedData }
                            columns={ this.getTableColumns() }
                            pagination
                            selectable={ this.state.moreActionsVisible }
                            loading={ this.props.loading }
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => { this.deductionsTable = ref; } }
                            onSelectionChange={ ({ selected }) => {
                                const selectionLength = selected.filter( ( row ) => row ).length;

                                this.setState({
                                    deleteLabel: formatDeleteLabel( selectionLength )
                                });
                            } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }
}
