import React from 'react';

import Table from 'components/Table';
import Button from 'components/Button';
import {
    getSortedData,
    getIdsOfSelectedRows
} from 'components/Table/helpers';

import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency
} from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';

import { ProfileSectionWrapper } from '../styles';

import { PAYMENT_SCHEME_LABELS } from '../../../../Loans/constants';

/**
 * Employee loans component
 */
export default class EmployeeLoans extends React.Component {
    static propTypes = {
        loans: React.PropTypes.array,
        employee: React.PropTypes.object,
        downloading: React.PropTypes.bool,
        permission: React.PropTypes.object,
        exportLoans: React.PropTypes.func,
        deleteLoans: React.PropTypes.func,
        loading: React.PropTypes.bool,
        getEmployeeLoans: React.PropTypes.func,
        id: React.PropTypes.string
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: [],
            moreActionsVisible: false
        };
    }

    componentDidMount() {
        this.props.getEmployeeLoans( this.props.employee.id );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.loans !== this.props.loans ) {
            const displayedData = this.getDataForDisplay( nextProps.loans );
            this.setState({ displayedData }, () => {
                this.handleSearch();
            });
        }
    }

    getTableColumns = () => {
        const allColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'type_name',
                header: 'Loan Type',
                accessor: ( row ) => row.type_name,
                minWidth: 150
            },
            {
                id: 'payment_scheme',
                header: 'Payment Scheme',
                accessor: ( row ) => row.payment_scheme,
                minWidth: 260
            },
            {
                id: 'principal_amount',
                header: 'Principal Amount',
                accessor: ( row ) => ( formatCurrency( row.total_amount ) ),
                minWidth: 200
            },
            {
                id: 'collected_to_date',
                header: 'Total Amount Paid',
                accessor: ( row ) => ( row.is_paid ? 'Paid' : formatCurrency( row.collected_to_date ) ),
                minWidth: 200,
                render: ({ row }) => (
                    row.is_paid
                        ? <div><span className="bullet-green">&#x25cf;</span> Paid</div>
                        : <div>Php {formatCurrency( row.collected_to_date )}</div>
                )
            },
            {
                id: 'parsed_remaining_balance',
                header: 'Remaning Balance',
                accessor: ( row ) => ( row.is_paid ? '' : formatCurrency( row.remaining_balance ) ),
                minWidth: 200,
                render: ({ row }) => (
                    row.is_paid
                        ? <div></div>
                        : <div><span className="bullet-red">&#x25cf;</span> {formatCurrency( row.remaining_balance )}</div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.props.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => {
                            localStorage.setItem( 'employeeIdForEditLoan', this.props.employee.id );
                            browserHistory.push( `/loans/${row.id}/edit` );
                        } }
                    />
                )
            }
        ];

        const visibleColumns = this.state.moreActionsVisible
            ? [ 'type_name', 'payment_scheme', 'principal_amount', 'collected_to_date', 'parsed_remaining_balance', 'actions' ]
            : [ 'type_name', 'payment_scheme', 'principal_amount', 'collected_to_date', 'parsed_remaining_balance' ];

        return allColumns.filter( ( column ) => (
            visibleColumns.includes( column.id ? column.id : column.accessor )
        ) );
    }

    getDataForDisplay = ( loans ) => (
        loans.map( ( data ) => ({
            ...data,
            payment_scheme: PAYMENT_SCHEME_LABELS[ data.payment_scheme ],
            type_name: data.type.name,
            parsed_remaining_balance: formatCurrency( data.amount_remaining )
        }) )
    )

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.loansTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    /**
     * Send request to export loans
     */
    exportLoans = () => {
        const sortedLoans = getSortedData( this.loansTable );
        this.props.exportLoans( sortedLoans.map( ( loan ) => loan.id ) );
    };

    /**
     * Send a request to delete loans
     */
    deleteLoans = () => {
        const loansIds = getIdsOfSelectedRows( this.loansTable );
        this.props.deleteLoans({ employeeId: this.props.employee.id, loansIds });
    }

    handleSearch = ( term = '' ) => {
        const dataForDisplay = this.getDataForDisplay( this.props.loans );
        const matchingRows = this.loansTable.getFilteredData( dataForDisplay, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <ProfileSectionWrapper id={ this.props.id }>
                <SectionHeading
                    moreActionsVisible={ this.state.moreActionsVisible }
                    permission={ this.props.permission }
                    heading="Loans"
                    id="tooltipLoans"
                    createButtonLabel="Add Loan"
                    showMore={ () => {
                        this.setState({ moreActionsVisible: true }, () => {
                            this.loansTable.initColumns();
                        });
                    } }
                    onAdd={ () => {
                        localStorage.setItem( 'prepopulatedEmployeeForAddLoan', JSON.stringify( this.props.employee ) );
                        browserHistory.push( '/loans/add' );
                    } }
                />
                <div className="section-body">
                    <div className="content">
                        <SectionBody
                            table={ this.loansTable }
                            permission={ this.props.permission }
                            deleteLabel={ this.state.deleteLabel }
                            label={ this.state.label }
                            deleteModalHeading="Delete Loans"
                            downloading={ this.props.downloading }
                            moreActionsVisible={ this.state.moreActionsVisible }
                            onDelete={ this.deleteLoans }
                            onSearch={ this.handleSearch }
                            onExport={ this.exportLoans }
                        />
                        <Table
                            data={ this.state.displayedData }
                            columns={ this.getTableColumns() }
                            pagination
                            selectable={ this.state.moreActionsVisible }
                            loading={ this.props.loading }
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => { this.loansTable = ref; } }
                            onSelectionChange={ ({ selected }) => {
                                const selectionLength = selected.filter( ( row ) => row ).length;

                                this.setState({
                                    deleteLabel: formatDeleteLabel( selectionLength )
                                });
                            } }
                        />
                    </div>
                </div>
            </ProfileSectionWrapper>
        );
    }
}
