import React from 'react';

import SalDropdown from '../../../../components/SalDropdown';
import SalConfirm from '../../../../components/SalConfirm';
import Loader from '../../../../components/Loader';

import { isAnyRowSelected } from '../../../../components/Table/helpers';

import { ConfirmBodyWrapperStyle } from './styles';

/**
 * Section body for employee records table
 */
export default class SectionBody extends React.Component {
    static propTypes = {
        table: React.PropTypes.object,
        permission: React.PropTypes.object,
        deleteLabel: React.PropTypes.string,
        deleteModalHeading: React.PropTypes.string,
        deleteModalBody: React.PropTypes.string,
        label: React.PropTypes.string,
        downloading: React.PropTypes.bool,
        moreActionsVisible: React.PropTypes.bool,
        onDelete: React.PropTypes.func,
        onExport: React.PropTypes.func
    };

    static defaultProps = {
        deleteModalBody: 'Are you sure you want to delete the selected items?'
    }

    constructor( props ) {
        super( props );

        this.state = {
            showModal: false,
            dropdownItems: []
        };
    }

    componentWillMount() {
        const dropdownItems = [];

        if ( this.props.onExport ) {
            dropdownItems.push({
                label: 'Download',
                children: this.props.downloading ? <Loader /> : <div>Download</div>,
                onClick: this.props.onExport
            });
        }

        if ( this.props.onDelete ) {
            dropdownItems.push({
                label: 'Delete',
                children: this.props.permission.delete ? <div>Delete</div> : null,
                onClick: () => {
                    this.setState({ showModal: false }, () => {
                        this.setState({ showModal: true });
                    });
                }
            });
        }

        this.setState({ dropdownItems });
    }

    render() {
        return (
            <div>
                { this.props.deleteModalHeading ? (
                    <SalConfirm
                        onConfirm={ this.props.onDelete }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    { this.props.deleteModalBody }
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title={ this.props.deleteModalHeading }
                        buttonStyle="danger"
                        showCancel
                        confirmText="Delete"
                        cancelText="Cancel"
                        visible={ this.state.showModal }
                    /> ) : null
                }
                {
                    this.props.moreActionsVisible && (
                        <div className="title">
                            <div className="search-wrapper">
                            </div>
                            <span>
                                { isAnyRowSelected( this.props.table ) ? this.props.deleteLabel : this.props.label }
                            </span>
                            <span className="sl-u-gap-left--sm">
                                { isAnyRowSelected( this.props.table ) ? (
                                    <SalDropdown
                                        dropdownItems={ this.state.dropdownItems }
                                    /> ) : null
                                }
                            </span>
                        </div>
                    )
                }
            </div>
        );
    }
}
