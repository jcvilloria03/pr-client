import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';
import fileDownload from 'js-file-download';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';
import { auth } from 'utils/AuthService';
import {
    getCurrentYear,
    getRange
} from 'utils/functions';

import {
    INITIAL_DATA,
    EMPLOYEE_SECTIONS,
    GET_EMPLOYEE,
    SET_EMPLOYEE,
    SET_EMPLOYEE_LOANS,
    GET_EMPLOYEE_OTHER_INCOME,
    GET_EMPLOYEE_LOANS,
    GET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS,
    GET_EMPLOYEE_HISTORICAL_EARNINGS,
    GET_DATA_NECESSARY_FOR_EDIT,
    GET_EMPLOYEE_APPROVAL_GROUPS,
    GET_EMPLOYEE_LEAVE_CREDITS,
    SET_EMPLOYEE_LEAVE_CREDITS,
    SET_EMPLOYEE_LEAVE_CREDITS_PAGINATION,
    GET_EMPLOYEE_FILED_LEAVES,
    SET_EMPLOYEE_FILED_LEAVES,
    SET_EMPLOYEE_FILED_LEAVES_PAGINATION,
    SET_EMPLOYEE_TERMINATION_INFORMATION,
    SET_EMPLOYEE_LOADING,
    SET_EMPLOYEE_LOADING_LOANS,
    SET_EMPLOYEE_LOADING_LEAVE_CREDITS,
    SET_EMPLOYEE_LOADING_FILED_LEAVES,
    SET_EMPLOYEE_LOADING_APPROVAL_GROUPS,
    SET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS,
    GET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS,
    SET_EMPLOYEE_LOADING_HISTORICAL_EARNINGS,
    SET_EMPLOYEE_LOADING_ANNUAL_EARNINGS,
    SET_EMPLOYEE_LOADING_TERMINATION_INFORMATION,
    SET_EMPLOYEE_LOADING_EMPLOYMENT,
    SET_EMPLOYEE_LOADING_BASIC_PAY_ADJUSTMENTS,
    SET_EMPLOYEE_LOADING_PAYROLL,
    SET_EMPLOYEE_LOADING_BASIC,
    SET_EMPLOYEE_APPROVAL_GROUPS,
    SET_EMPLOYEE_HISTORICAL_EARNINGS,
    SET_EMPLOYEE_ANNUAL_EARNINGS,
    GET_EMPLOYEE_ANNUAL_EARNINGS,
    SET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS,
    SET_ANNUAL_EARNINGS_FORM_OPTIONS,
    UPDATE_EMPLOYEE_ANNUAL_EARNINGS,
    UPDATE_EMPLOYEE_APPROVAL_GROUPS,
    UPDATE_EMPLOYEE_LEAVE_CREDITS,
    SET_EMPLOYEE_DOWNLOADING_LOANS,
    SET_EMPLOYEE_DOWNLOADING_PROFILE,
    SET_EMPLOYEE_DOWNLOADING_LEAVE_CREDITS,
    SET_EMPLOYEE_DOWNLOADING_FILED_LEAVES,
    SET_EMPLOYEE_DOWNLOADING_HISTORICAL_EARNINGS,
    EXPORT_LOANS,
    EXPORT_BONUSES,
    EXPORT_ALLOWANCES,
    EXPORT_COMMISSIONS,
    EXPORT_DEDUCTIONS,
    EXPORT_LEAVE_CREDITS,
    EXPORT_FILED_LEAVES,
    EXPORT_ADJUSTMENTS,
    EXPORT_HISTORICAL_EARNINGS,
    EXPORT_EMPLOYEE_PROFILE,
    DELETE_LOANS,
    DELETE_BONUSES,
    DELETE_ALLOWANCES,
    DELETE_COMMISSIONS,
    DELETE_DEDUCTIONS,
    DELETE_ADJUSTMENTS,
    DELETE_TERMINATION_INFORMATION,
    DELETE_BASIC_PAY_ADJUSTMENTS,
    SET_EMPLOYEES_LOADING,
    SAVE_EMPLOYEE,
    SAVE_PHILIPPINE_EMPLOYEE,
    SET_EMPLOYEE_SAVING,
    SET_EMPLOYEE_ERRORS,
    SET_EMPLOYEES,
    SET_COMPANY_DATA,
    SET_FORM_CONSTANTS,
    SET_COST_CENTERS,
    SET_RANKS,
    SET_EMPLOYMENT_TYPES,
    SET_POSITIONS,
    SET_TEAMS,
    SET_DEPARTMENTS,
    SET_LOCATIONS,
    SET_PAYROLL_GROUPS,
    SET_REQUEST_TYPES,
    SET_WORKFLOWS,
    SET_COUNTRIES,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    NAMESPACE,
    TYPE_PLURALS,
    SET_EMPLOYEE_EARNINGS,
    SET_EMPLOYEE_LOADING_EARNINGS,
    GET_EMPLOYEE_EARNINGS,
    GET_EMPLOYEE_PAYMENT_METHODS,
    SET_EMPLOYEE_LOADING_PAYMENT_METHODS,
    SET_EMPLOYEE_PAYMENT_METHODS,
    DELETE_EMPLOYEE_PAYMENT_METHODS,
    ASSIGN_EMPLOYEE_PAYMENT_METHOD,
    UPLOAD_PROFILE_PICTURE,
    SET_PROFILE_PICTURE,
    SET_EMPLOYEE_LOADING_PICTURE
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Gets initial data
 */
export function* initialData({ payload }) {
    try {
        const { id } = payload;

        const companyId = company.getLastActiveCompanyId();
        yield put({
            type: SET_COMPANY_DATA,
            payload: companyId
        });

        yield [
            call( getFormOptions ),
            call( getRequestTypes ),
            call( getCountries )
        ];

        yield [
            call( getEmployeeProfile, { payload: id, isInitialLoad: true })
        ];
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Fetch employee profile by id
 */
export function* getEmployeeProfile({ payload, isInitialLoad }) {
    try {
        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                title: ' ',
                message: ' ',
                show: false,
                type: payload.type
            }
        });

        yield put({ type: SET_EMPLOYEE_LOADING, payload: true });
        yield put({ type: SET_EMPLOYEE_ERRORS, payload: null });

        const employee = yield call( Fetch, `/employee/${payload}`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE,
            payload: {
                ...employee,
                cost_center_id: employee.payroll ? employee.payroll.cost_center_id : ''
            }
        });

        yield put({
            type: SET_PROFILE_PICTURE,
            payload: employee.picture
        });

        yield put({
            type: SET_EMPLOYEE_LOADING,
            payload: false
        });

        yield call( getEmployeeTerminationInformation, { employeeId: payload });

        if ( !isInitialLoad ) {
            browserHistory.push( `/employee/${payload}`, true );
        }
    } catch ( error ) {
        if ( error.response.status === 401 || error.response.status === 404 ) {
            yield call( browserHistory.push, '/employees', true );
        } else {
            yield call( notifyUser, toErrorPayload( error ) );
        }
    }
}

/**
 * Fetch employee earnings
 */
export function* getEmployeeEarnings({ payload }) {
    const { employeeId } = payload;

    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_EARNINGS,
            payload: true
        });

        yield put({
            type: SET_EMPLOYEE_EARNINGS,
            payload: {}
        });

        const earnings = yield call( Fetch, `/employee/${employeeId}/earning`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE_EARNINGS,
            payload: earnings
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_EARNINGS,
            payload: false
        });
    }
}

/**
 * Fetch employee historical earnings
 */
export function* getEmployeeHistoricalEarnings({ payload }) {
    const { employeeId } = payload;
    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_HISTORICAL_EARNINGS,
            payload: true
        });

        yield put({
            type: SET_EMPLOYEE_LOADING_ANNUAL_EARNINGS,
            payload: true
        });

        yield put({
            type: SET_EMPLOYEE_HISTORICAL_EARNINGS,
            payload: []
        });

        const formOptions = {};
        const companyId = company.getLastActiveCompanyId();
        const historicalEarnings = yield call( Fetch, `/company/${companyId}/employee/${employeeId}/annual_earning`, { method: 'GET' });

        formOptions.years = getRange( 1900, getCurrentYear() )
            .reverse()
            .map( ( year ) => ({ label: year.toString(), value: year }) );

        yield put({
            type: SET_ANNUAL_EARNINGS_FORM_OPTIONS,
            payload: formOptions
        });

        yield put({
            type: SET_EMPLOYEE_HISTORICAL_EARNINGS,
            payload: historicalEarnings.data
        });

        yield put({
            type: SET_EMPLOYEE_ANNUAL_EARNINGS,
            payload: historicalEarnings.data.find( ( annualEarning ) => annualEarning.year === getCurrentYear() )
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_HISTORICAL_EARNINGS,
            payload: false
        });

        yield put({
            type: SET_EMPLOYEE_LOADING_ANNUAL_EARNINGS,
            payload: false
        });
    }
}

/**
 * Gets employee's annual earnings
 * @param {Number} payload.employeeId - Employee ID
 */
export function* getEmployeeAnnualEarnings({ payload }) {
    const { employee_id: employeeId } = payload;

    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_ANNUAL_EARNINGS,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const { data } = yield call( Fetch, `/company/${companyId}/employee/${employeeId}/annual_earning`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE_ANNUAL_EARNINGS,
            payload: data.find( ( annualEarning ) => annualEarning.year === getCurrentYear() ) || {}
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_ANNUAL_EARNINGS,
            payload: false
        });
    }
}

/**
 * Fetch loans by employee
 */
export function* getEmployeeLoans({ payload }) {
    const { employeeId } = payload;
    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_LOANS,
            payload: true
        });
        yield put({
            type: SET_EMPLOYEE_LOANS,
            payload: []
        });

        const companyId = company.getLastActiveCompanyId();
        const loans = yield call( Fetch, `/company/${companyId}/employee/${employeeId}/payroll_loans`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE_LOANS,
            payload: loans.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_LOANS,
            payload: false
        });
    }
}

/**
 * Fetch payment methods for employee
 *
 * @param {Number|String} payload - Employee ID
 */
export function* getEmployeePaymentMethods({ payload }) {
    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_PAYMENT_METHODS,
            payload: true
        });

        const { data } = yield call( Fetch, `/employee/${payload}/payment_methods`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE_PAYMENT_METHODS,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_PAYMENT_METHODS,
            payload: false
        });
    }
}

/**
 * Delete payment methods of employee
 *
 * @param {String|Number} payload.employeeId - Employee ID
 * @param {Array} payload.ids - Payment method IDs
 */
export function* deleteEmployeePaymentMethods({ payload: { employeeId, ids }}) {
    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_PAYMENT_METHODS,
            payload: true
        });

        if ( employeeId && ids ) {
            yield call( Fetch, `/employee/${employeeId}/payment_method`, {
                method: 'DELETE',
                data: {
                    data: {
                        type: 'employee-payment-method',
                        id: ids
                    }
                }
            });

            yield call( getEmployeePaymentMethods, { payload: employeeId });
            yield call( notifyUser, {
                title: 'Updating employee payment methods success!',
                message: 'Your changes has been saved.',
                show: true,
                type: 'success'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
        yield put({
            type: SET_EMPLOYEE_LOADING_PAYMENT_METHODS,
            payload: false
        });
    }
}

/**
 * Assign payment method of employee
 *
 * @param {String|Number} payload.employeeId - Employee ID
 * @param {String|Number} payload.paymentMethodId - Payment method ID
 */
export function* assignEmployeePaymentMethod({ payload: { employeeId, paymentMethodId }}) {
    try {
        yield call( Fetch, `/employee/${employeeId}/payment_methods/${paymentMethodId}`, {
            method: 'PATCH',
            data: {
                data: {
                    type: 'employee-payment-method',
                    attributes: {
                        active: true
                    }
                }
            }
        });
    } catch ( error ) {
        yield [
            call( getEmployeePaymentMethods, { payload: employeeId }),
            call( notifyUser, toErrorPayload( error ) )
        ];
    }
}

/**
 * Fetch basic pay adjustments by employee
 */
export function* getEmployeeBasicPayAdjustments({ payload }) {
    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_BASIC_PAY_ADJUSTMENTS,
            payload: true
        });

        yield put({
            type: SET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS,
            payload: []
        });

        const basicPayAdjustments = yield call( Fetch, `/employee/${payload.employeeId}/basic_pay_adjustments`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS,
            payload: basicPayAdjustments.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_BASIC_PAY_ADJUSTMENTS,
            payload: false
        });
    }
}

/**
 * Fetch leave credits by employee
 */
export function* getEmployeeLeaveCredits({ employeeId, data = {}}) {
    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_LEAVE_CREDITS,
            payload: true
        });
        yield put({
            type: SET_EMPLOYEE_LEAVE_CREDITS,
            payload: []
        });
        yield put({
            type: SET_EMPLOYEE_LEAVE_CREDITS_PAGINATION,
            payload: {}
        });

        const companyId = company.getLastActiveCompanyId();
        const leaveCreditsResponse = yield call( Fetch, `/company/${companyId}/leave_credits`, {
            method: 'POST',
            data: {
                ...data,
                employees_ids: [employeeId]
            }
        });

        yield put({
            type: SET_EMPLOYEE_LEAVE_CREDITS,
            payload: leaveCreditsResponse.data
        });
        yield put({
            type: SET_EMPLOYEE_LEAVE_CREDITS_PAGINATION,
            payload: leaveCreditsResponse.meta.pagination
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_LEAVE_CREDITS,
            payload: false
        });
    }
}

/**
 * Update employee leave credits.
 */
export function* updateEmployeeLeaveCredits({ payload, callback }) {
    let leaveCredits;

    const { id, value, company_id } = payload;

    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_LEAVE_CREDITS,
            payload: true
        });

        leaveCredits = yield call( Fetch, `/leave_credit/${payload.id}`, {
            method: 'PUT',
            data: { id, value, company_id }
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_LEAVE_CREDITS,
            payload: false
        });

        if ( callback ) {
            yield call( callback, leaveCredits );
        }
    }
}

/**
 * Fetch filed leaves by employee
 */
export function* getEmployeeFiledLeaves({ employeeId, data = {}}) {
    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_FILED_LEAVES,
            payload: true
        });
        yield put({
            type: SET_EMPLOYEE_FILED_LEAVES,
            payload: []
        });
        yield put({
            type: SET_EMPLOYEE_FILED_LEAVES_PAGINATION,
            payload: {}
        });

        const filedLeavesResponse = yield call( Fetch, `/employee/${employeeId}/leave_requests`, {
            method: 'POST',
            data
        });

        yield put({
            type: SET_EMPLOYEE_FILED_LEAVES,
            payload: filedLeavesResponse.data
        });
        yield put({
            type: SET_EMPLOYEE_FILED_LEAVES_PAGINATION,
            payload: filedLeavesResponse.meta.pagination
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_FILED_LEAVES,
            payload: false
        });
    }
}

/**
 * Fetch other incomes by employee
 */
export function* getEmployeeOtherIncome({ payload }) {
    const { employeeId, otherIncomeType } = payload;
    try {
        yield put({ type: `${NAMESPACE}SET_EMPLOYEE_LOADING_${TYPE_PLURALS[ otherIncomeType ]}`, payload: true });
        yield put({ type: `${NAMESPACE}SET_EMPLOYEE_${TYPE_PLURALS[ otherIncomeType ]}`, payload: []});

        const companyId = company.getLastActiveCompanyId();
        const otherIncome = yield call( Fetch, `/company/${companyId}/employee/${employeeId}/other_incomes/${otherIncomeType.toLowerCase()}_type`, { method: 'GET' });

        yield put({ type: `${NAMESPACE}SET_EMPLOYEE_${TYPE_PLURALS[ otherIncomeType ]}`, payload: otherIncome.data });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({ type: `${NAMESPACE}SET_EMPLOYEE_LOADING_${TYPE_PLURALS[ otherIncomeType ]}`, payload: false });
    }
}

/**
 * Fetch data necessary for editing certain sections
 */
export function* getDataNecessaryForEdit({ payload }) {
    const { section } = payload;

    try {
        yield put({ type: `${NAMESPACE}SET_EMPLOYEE_LOADING_${section}`, payload: true });

        const companyId = company.getLastActiveCompanyId();
        if ( section === EMPLOYEE_SECTIONS.EMPLOYMENT ) {
            yield [
                call( getCostCenters, companyId ),
                call( getLocations, companyId ),
                call( getDepartments, companyId ),
                call( getRanks, companyId ),
                call( getEmploymentTypes, companyId ),
                call( getTeams, companyId ),
                call( getPositions, companyId )

            ];
        } else if ( section === EMPLOYEE_SECTIONS.PAYROLL ) {
            yield call( getPayrollGroups, companyId );
            yield call( getCostCenters, companyId );
        }
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({ type: `${NAMESPACE}SET_EMPLOYEE_LOADING_${section}`, payload: false });
    }
}

/**
 * Fetch employee approval groups (workflow entitlements)
 */
export function* getEmployeeApprovalGroups({ employeeId }) {
    try {
        yield put({ type: SET_EMPLOYEE_LOADING_APPROVAL_GROUPS, payload: true });
        yield put({ type: SET_EMPLOYEE_APPROVAL_GROUPS, payload: []});

        const workflowEntitlements = yield call( Fetch, `/employee/${employeeId}/workflow_entitlements`, {
            method: 'GET'
        });

        yield put({ type: SET_EMPLOYEE_APPROVAL_GROUPS, payload: workflowEntitlements.data });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({ type: SET_EMPLOYEE_LOADING_APPROVAL_GROUPS, payload: false });
    }
}

/**
 * Fetch employee termination information
 */
export function* getEmployeeTerminationInformation({ employeeId }) { // eslint-disable-line
    try {
        yield put({ type: SET_EMPLOYEE_LOADING_TERMINATION_INFORMATION, payload: true });
        yield put({ type: SET_EMPLOYEE_TERMINATION_INFORMATION, payload: {}});

        const { data } = yield call( Fetch, `/employee/${employeeId}/termination_informations`, {
            method: 'GET',
            params: {
                include_final_pay: 1
            }
        });

        yield put({ type: SET_EMPLOYEE_TERMINATION_INFORMATION, payload: data });
    } catch ( error ) {
        if ( error.response.status === 404 ) {
            yield put({ type: SET_EMPLOYEE_TERMINATION_INFORMATION, payload: {}});
        } else {
            yield call( notifyUser, toErrorPayload( error ) );
        }
    } finally {
        yield put({ type: SET_EMPLOYEE_LOADING_TERMINATION_INFORMATION, payload: false });
    }
}

/**
 * Update employee approval groups (workflow entitlements)
 */
export function* updateEmployeeApprovalGroups({ employeeId, approvalGroups }) {
    try {
        yield put({ type: SET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS, payload: false });
        yield put({ type: SET_EMPLOYEE_LOADING_APPROVAL_GROUPS, payload: true });

        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, `/employee/${employeeId}/workflow_entitlement/create_or_update`, {
            method: 'POST',
            data: {
                company_id: companyId,
                workflow_entitlements: {
                    ...approvalGroups
                }
            }
        });

        yield call( getEmployeeApprovalGroups, { employeeId });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({ type: SET_EMPLOYEE_LOADING_APPROVAL_GROUPS, payload: false });
    }
}

/**
 * Check if there are pending requests for the workflows that are about to be changed.
 */
export function* approvalCheckPendingRequests({ payload, callback }) {
    try {
        yield put({ type: SET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS, payload: false });

        const response = yield call( Fetch, `/employee/${payload.employeeId}/workflow_entitlement/has_pending_requests`, {
            method: 'POST',
            data: {
                company_id: company.getLastActiveCompanyId(),
                workflow_entitlements: payload
            }
        });

        if ( response.has_pending_requests ) {
            yield put({
                type: SET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS,
                payload: true
            });
        } else {
            yield call( callback );
        }
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Update employee approval groups (workflow entitlements)
 */
export function* updateEmployeeAnnualEarnings({ payload }) {
    const data = { ...payload.data };
    Object.keys( data ).forEach( ( key ) => {
        if ( data[ key ] === '' ) {
            data[ key ] = null;
        }
    });

    try {
        yield put({ type: SET_EMPLOYEE_LOADING_ANNUAL_EARNINGS, payload: true });

        const companyId = company.getLastActiveCompanyId();
        yield call( Fetch, `/company/${companyId}/annual_earning/set`, { method: 'POST', data });

        yield call( getEmployeeHistoricalEarnings, { payload: { employeeId: payload.employeeId }});
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({ type: SET_EMPLOYEE_LOADING_ANNUAL_EARNINGS, payload: false });
    }
}

/**
 * Export loans
 */
export function* exportLoans({ payload }) {
    try {
        yield put({
            type: SET_EMPLOYEE_DOWNLOADING_LOANS,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/payroll_loans/generate_csv`, {
            method: 'POST',
            data: {
                loan_ids: payload,
                company_id: companyId
            }
        });

        fileDownload( response, 'loans.csv' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EMPLOYEE_DOWNLOADING_LOANS,
            payload: false
        });
    }
}

/**
 * Export historical earnings
 */
export function* exportHistoricalEarnings({ payload }) {
    try {
        yield put({
            type: SET_EMPLOYEE_DOWNLOADING_HISTORICAL_EARNINGS,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/annual_earning/download`, {
            method: 'POST',
            data: {
                ids: payload.ids,
                id: companyId
            }
        });

        fileDownload( response, 'historical-earnings.csv' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EMPLOYEE_DOWNLOADING_HISTORICAL_EARNINGS,
            payload: false
        });
    }
}

/**
 * Export loans
 */
export function* exportLeaveCredits({ payload }) {
    try {
        yield put({
            type: SET_EMPLOYEE_DOWNLOADING_LEAVE_CREDITS,
            payload: true
        });

        const response = yield call( Fetch, `/employee/${payload.employee.id}/leave_credits/download`, {
            method: 'POST',
            data: {
                ...payload,
                company_id: company.getLastActiveCompanyId()
            }
        });

        fileDownload( response, `${payload.employee.employee_id}-leave_credits.csv` );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EMPLOYEE_DOWNLOADING_LEAVE_CREDITS,
            payload: false
        });
    }
}

/**
 * Export Filed Leaves
 */
export function* exportFiledLeaves({ payload }) {
    try {
        yield put({
            type: SET_EMPLOYEE_DOWNLOADING_FILED_LEAVES,
            payload: true
        });

        const response = yield call( Fetch, `/employee/${payload.employee.id}/leave_requests/download`, {
            method: 'POST',
            data: payload
        });

        fileDownload( response, `${payload.employee.employee_id}-filed_leaves.csv` );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EMPLOYEE_DOWNLOADING_FILED_LEAVES,
            payload: false
        });
    }
}

/**
 * Export other incomes
 */
export function* exportOtherIncomes({ payload }) {
    const { type } = payload;
    try {
        yield put({ type: `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_${TYPE_PLURALS[ type ]}`, payload: true });

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_incomes/${type.toLowerCase()}_type/download`, {
            method: 'POST',
            data: { ids: payload.ids }
        });

        fileDownload( response, `${TYPE_PLURALS[ type ].toLowerCase()}.csv` );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({ type: `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_${TYPE_PLURALS[ type ]}`, payload: false });
    }
}

/**
 * Export employee profile
 */
export function* exportEmployeeProfile({ payload }) {
    try {
        yield put({
            type: SET_EMPLOYEE_DOWNLOADING_PROFILE,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/employees/export`, {
            method: 'POST',
            data: {
                employees: [payload]
            }
        });

        const [key] = Object.keys( response.data );
        const link = document.createElement( 'a' );

        link.href = response.data[ key ];
        link.setAttribute( 'download', '.csv' );

        document.body.appendChild( link );
        link.click();

        document.body.removeChild( link );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EMPLOYEE_DOWNLOADING_PROFILE,
            payload: false
        });
    }
}

/**
 * Batch delete other incomes
 */
export function* deleteOtherIncomes({ payload }) {
    try {
        const { employeeId, ids } = payload;

        yield put({ type: `${NAMESPACE}SET_EMPLOYEE_LOADING_${TYPE_PLURALS[ payload.type ]}`, payload: true });

        yield call( Fetch, `/company/${company.getLastActiveCompanyId()}/other_income`, {
            method: 'DELETE',
            data: { ids }
        });

        yield call( getEmployeeOtherIncome, { payload: { employeeId, otherIncomeType: payload.type }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({ type: `${NAMESPACE}SET_EMPLOYEE_LOADING_${TYPE_PLURALS[ payload.type ]}`, payload: false });
    }
}

/**
 * Batch delete basic pay adjustments
 */
export function* deleteBasicPayAdjustments({ payload }) {
    try {
        const { employeeId, ids } = payload;

        yield put({
            type: SET_EMPLOYEE_LOADING_BASIC_PAY_ADJUSTMENTS,
            payload: true
        });

        yield call( Fetch, `/employee/${employeeId}/basic_pay_adjustment/delete`, {
            method: 'DELETE',
            data: { ids }
        });

        yield call( getEmployeeBasicPayAdjustments, { payload: { employeeId }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.ids : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_BASIC_PAY_ADJUSTMENTS,
            payload: false
        });
    }
}

/**
 * Batch delete loans
 */
export function* deleteLoans({ payload }) {
    try {
        const { employeeId, loansIds } = payload;

        yield put({
            type: SET_EMPLOYEE_LOADING_LOANS,
            payload: true
        });

        yield call( Fetch, '/payroll_loan/bulk_delete', {
            method: 'DELETE',
            data: {
                payroll_loans_ids: loansIds,
                company_id: company.getLastActiveCompanyId()
            }
        });

        yield call( getEmployeeLoans, { payload: { employeeId }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_LOANS,
            payload: false
        });
    }
}

/**
 * Delete termination information
 */
export function* deleteTerminationInformation({ payload }) {
    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_TERMINATION_INFORMATION,
            payload: true
        });

        yield call( Fetch, `/termination_informations/${payload.terminationId}`, { method: 'DELETE' });

        yield call( notifyUser, {
            title: 'Cancelation of termination details success!',
            message: 'Your changes has been saved.',
            show: true,
            type: 'success'
        });
        yield call( getEmployeeProfile, { payload: payload.id, isInitialLoad: false });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_TERMINATION_INFORMATION,
            payload: false
        });
    }
}

/**
 * Saves philippine employee profile changes
 */
export function* savePhilippineEmployeeProfile({ payload }) {
    let loading = '';
    switch ( payload.section ) {
        case EMPLOYEE_SECTIONS.EMPLOYMENT:
            loading = SET_EMPLOYEE_LOADING_EMPLOYMENT;
            break;
        case EMPLOYEE_SECTIONS.PAYROLL:
            loading = SET_EMPLOYEE_LOADING_PAYROLL;
            break;
        case EMPLOYEE_SECTIONS.BASIC:
            loading = SET_EMPLOYEE_LOADING_BASIC;
            break;
        default:
            break;
    }

    yield put({
        type: SET_EMPLOYEE_ERRORS,
        payload: {}
    });

    let response = null;
    try {
        const { id, employee } = payload;

        yield put({ type: loading, payload: true });
        yield put({ type: SET_EMPLOYEE_SAVING, payload: true });

        const data = Object.assign({}, employee );
        data.entitled_deminimis = data.entitled_deminimis ? 'Yes' : 'No';

        response = yield call( Fetch, `/philippine/employee/${id}`, { method: 'PATCH', data });
        yield put({
            type: SET_EMPLOYEE,
            payload: {
                ...response,
                cost_center_id: response.payroll ? response.payroll.cost_center_id : ''
            }
        });

        yield put({ type: SET_EMPLOYEE_SAVING, payload: false });
        yield put({ type: loading, payload: false });

        if ( response ) {
            yield call( notifyUser, {
                show: true,
                title: 'Updating employee profile success!',
                message: 'Your changes has been saved.',
                type: 'success'
            });
        }

        if ( payload.section === EMPLOYEE_SECTIONS.PAYROLL ) {
            yield call( getEmployeeBasicPayAdjustments, { payload: { employeeId: id }});
        }
    } catch ( error ) {
        yield put({ type: SET_EMPLOYEE_SAVING, payload: false });
        yield put({ type: loading, payload: false });

        if ( error.response && error.response.status === 406 ) {
            yield put({
                type: SET_EMPLOYEE_ERRORS,
                payload: error.response.data
            });
            yield call( notifyUser, {
                show: true,
                title: 'Error saving employee profile',
                message: 'Please check and correct your entries',
                type: 'error'
            });
        } else {
            yield call( notifyUser, toErrorPayload( error ) );
        }
    }
}

/**
 * Saves employee profile changes
 */
export function* saveEmployeeProfile({ payload }) {
    let loading = '';
    switch ( payload.section ) {
        case EMPLOYEE_SECTIONS.EMPLOYMENT:
            loading = SET_EMPLOYEE_LOADING_EMPLOYMENT;
            break;
        case EMPLOYEE_SECTIONS.BASIC:
            loading = SET_EMPLOYEE_LOADING_BASIC;
            break;
        case EMPLOYEE_SECTIONS.PAYROLL:
            loading = SET_EMPLOYEE_LOADING_PAYROLL;
            break;
        default:
    }

    yield put({
        type: SET_EMPLOYEE_ERRORS,
        payload: {}
    });

    try {
        const { id, employee } = payload;

        const data = Object.assign({}, employee );
        const keys = Object.keys( data );
        const user = auth.getUser();

        // Transform boolean to 'Yes/No'
        keys.forEach( ( key ) => {
            const convertBoolToYesNo = [
                'timesheet_required',
                'rest_day_pay',
                'regular_holiday_pay',
                'holiday_premium_pay',
                'special_holiday_pay',
                'differential',
                'overtime',
                'entitled_deminimis'
            ].includes( key );

            if ( convertBoolToYesNo ) {
                data[ key ] = data[ key ] ? 'Yes' : 'No';
            }
        });

        yield [
            put({ type: loading, payload: true }),
            put({ type: SET_EMPLOYEE_SAVING, payload: true })
        ];

        const response = yield call( Fetch, `/employee/${id}`, { method: 'PUT', data });

        yield put({
            type: SET_EMPLOYEE,
            payload: response
        });

        yield [
            put({ type: SET_EMPLOYEE_SAVING, payload: false }),
            put({ type: loading, payload: false }),
            call( notifyUser, {
                show: true,
                title: 'Updating employee profile success!',
                message: 'Your changes has been saved.',
                type: 'success'
            })
        ];

        if ( user.id === response.user_id && response.update_email === true ) {
            localStorage.clear();
            browserHistory.push( '/login', true );
        }

        if ( payload.section === EMPLOYEE_SECTIONS.PAYROLL ) {
            yield call( getEmployeeBasicPayAdjustments, { payload: { employeeId: id }});
        }
    } catch ( error ) {
        yield [
            put({ type: SET_EMPLOYEE_SAVING, payload: false }),
            put({ type: loading, payload: false })
        ];

        if ( error.response && error.response.status === 406 ) {
            yield put({
                type: SET_EMPLOYEE_ERRORS,
                payload: error.response.data
            });
            yield call( notifyUser, {
                show: true,
                title: 'Error saving employee profile',
                message: error.response.data.message,
                type: 'error'
            });
        } else {
            yield call( notifyUser, toErrorPayload( error ) );
        }
    }
}

/**
 * Fetch employee list
 */
export function* getEmployeeList( companyId ) {
    try {
        yield put({
            type: SET_EMPLOYEES_LOADING,
            payload: true
        });

        const response = yield call( Fetch, `/company/${companyId}/employees`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEES,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEES_LOADING,
            payload: false
        });
    }
}

/**
 * Get employee form options
 */
export function* getFormOptions() {
    try {
        const response = yield call( Fetch, '/philippine/employee/form_options', { method: 'GET' });
        yield put({
            type: SET_FORM_CONSTANTS,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get cost centers
 */
export function* getCostCenters( companyId ) {
    try {
        const response = yield call( Fetch, `/company/${companyId}/cost_centers`, { method: 'GET' });
        yield put({
            type: SET_COST_CENTERS,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get departments
 */
export function* getDepartments( companyId ) {
    try {
        const response = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });
        yield put({
            type: SET_DEPARTMENTS,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get ranks
 */
export function* getRanks( companyId ) {
    try {
        const response = yield call( Fetch, `/company/${companyId}/ranks`, { method: 'GET' });
        yield put({
            type: SET_RANKS,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get employment types
 */
export function* getEmploymentTypes( companyId ) {
    try {
        const response = yield call( Fetch, `/company/${companyId}/employment_types`, { method: 'GET' });
        yield put({
            type: SET_EMPLOYMENT_TYPES,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get positions
 */
export function* getPositions( companyId ) {
    try {
        const response = yield call( Fetch, `/company/${companyId}/positions`, { method: 'GET' });
        yield put({
            type: SET_POSITIONS,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get teams
 */
export function* getTeams( companyId ) {
    try {
        const response = yield call( Fetch, `/company/${companyId}/teams`, { method: 'GET' });
        yield put({
            type: SET_TEAMS,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get locations
 */
export function* getLocations( companyId ) {
    try {
        const response = yield call( Fetch, `/philippine/company/${companyId}/locations`, { method: 'GET' });
        yield put({
            type: SET_LOCATIONS,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get payroll groups
 */
export function* getPayrollGroups( companyId ) {
    try {
        const response = yield call( Fetch, `/philippine/company/${companyId}/payroll_groups`, { method: 'GET' });
        yield put({
            type: SET_PAYROLL_GROUPS,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get employee request types
 */
export function* getRequestTypes() {
    try {
        const response = yield call( Fetch, '/employee_request_module', { method: 'GET' });

        yield put({
            type: SET_REQUEST_TYPES,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get company workflows
 */
export function* getWorkflows( companyId ) {
    try {
        const response = yield call( Fetch,
            `/company/${companyId}/workflows`,
            {
                method: 'GET'
            }
        );
        yield put({
            type: SET_WORKFLOWS,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get countries
 */
export function* getCountries() {
    try {
        const { data } = yield call( Fetch, '/countries', { method: 'GET' });
        yield put({
            type: SET_COUNTRIES,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Upload employee picture
 */
export function* uploadProfilePicture({ employeeId, file }) {
    try {
        yield put({
            type: SET_EMPLOYEE_LOADING_PICTURE,
            payload: true
        });

        const data = new FormData();
        data.append( 'picture', file );

        const response = yield call( Fetch, `/employee/upload/picture/${employeeId}`, {
            method: 'POST',
            data
        });
        yield put({
            type: SET_PROFILE_PICTURE,
            payload: response.picture
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_EMPLOYEE_LOADING_PICTURE,
            payload: false
        });
    }
}

/**
 * Watcher for EXPORT_LOANS
 */
export function* watchForExportLoans() {
    const watcher = yield takeEvery( EXPORT_LOANS, exportLoans );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for EXPORT_LOANS
 */
export function* watchForExportHistoricalEarnings() {
    const watcher = yield takeEvery( EXPORT_HISTORICAL_EARNINGS, exportHistoricalEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for EXPORT_LEAVE_CREDITS
 */
export function* watchForExportLeaveCredits() {
    const watcher = yield takeEvery( EXPORT_LEAVE_CREDITS, exportLeaveCredits );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for EXPORT_FILED_LEAVES
 */
export function* watchForExportFiledLeaves() {
    const watcher = yield takeEvery( EXPORT_FILED_LEAVES, exportFiledLeaves );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for EXPORT_EMPLOYEE_PROFILE
 */
export function* watchForExportEmployeeProfile() {
    const watcher = yield takeEvery( EXPORT_EMPLOYEE_PROFILE, exportEmployeeProfile );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher from exproting other incomes
 */
export function* watchForOtherIncomesExport() {
    const tasks = yield [
        takeEvery( EXPORT_BONUSES, exportOtherIncomes ),
        takeEvery( EXPORT_ALLOWANCES, exportOtherIncomes ),
        takeEvery( EXPORT_COMMISSIONS, exportOtherIncomes ),
        takeEvery( EXPORT_DEDUCTIONS, exportOtherIncomes ),
        takeEvery( EXPORT_ADJUSTMENTS, exportOtherIncomes )
    ];

    yield take( LOCATION_CHANGE );
    yield tasks.map( ( task ) => cancel( task ) );
}

/**
 * Watcher from deleting other incomes
 */
export function* watchForOtherIncomesDelete() {
    const tasks = yield [
        takeEvery( DELETE_BONUSES, deleteOtherIncomes ),
        takeEvery( DELETE_ALLOWANCES, deleteOtherIncomes ),
        takeEvery( DELETE_COMMISSIONS, deleteOtherIncomes ),
        takeEvery( DELETE_DEDUCTIONS, deleteOtherIncomes ),
        takeEvery( DELETE_ADJUSTMENTS, deleteOtherIncomes )
    ];

    yield take( LOCATION_CHANGE );
    yield tasks.map( ( task ) => cancel( task ) );
}

/**
 * Watcher for DELETE_LOANS
 */
export function* watchForDeleteLoans() {
    const watcher = yield takeEvery( DELETE_LOANS, deleteLoans );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE_TERMINATION_INFORMATION
 */
export function* watchForDeleteTerminationInformation() {
    const watcher = yield takeEvery( DELETE_TERMINATION_INFORMATION, deleteTerminationInformation );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE_BASIC_PAY_ADJUSTMENTS
 */
export function* watchForDeleteBasicPayAdjustments() {
    const watcher = yield takeEvery( DELETE_BASIC_PAY_ADJUSTMENTS, deleteBasicPayAdjustments );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * converts api error to notification payload
 */
function toErrorPayload( error ) {
    return {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initialData );
}

/**
 * Watcher for INITIAL_DATA
 */
export function* watchForInitialData() {
    const watcher = yield takeEvery( INITIAL_DATA, initialData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE
 */
export function* watchForGetEmployeeProfile() {
    const watcher = yield takeLatest( GET_EMPLOYEE, getEmployeeProfile );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SAVE_EMPLOYEE
 */
export function* watchForSaveEmployeeProfile() {
    const watcher = yield takeEvery( SAVE_EMPLOYEE, saveEmployeeProfile );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SAVE_PHILIPPINE_EMPLOYEE
 */
export function* watchForSavePhilippineEmployeeProfile() {
    const watcher = yield takeEvery( SAVE_PHILIPPINE_EMPLOYEE, savePhilippineEmployeeProfile );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_OTHER_INCOME
 */
export function* watchForGetEmployeeOtherIncome() {
    const watcher = yield takeEvery( GET_EMPLOYEE_OTHER_INCOME, getEmployeeOtherIncome );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_APPROVAL_GROUPS
 */
export function* watchForGetEmployeeApprovalGroups() {
    const watcher = yield takeEvery( GET_EMPLOYEE_APPROVAL_GROUPS, getEmployeeApprovalGroups );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_LOANS
 */
export function* watchForGetEmployeeLoans() {
    const watcher = yield takeEvery( GET_EMPLOYEE_LOANS, getEmployeeLoans );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS
 */
export function* watchForGetEmployeeBasicPayAdjustments() {
    const watcher = yield takeEvery( GET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS, getEmployeeBasicPayAdjustments );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_PAYMENT_METHODS
 */
export function* watchForGetEmployeePaymentMethods() {
    const watcher = yield takeEvery( GET_EMPLOYEE_PAYMENT_METHODS, getEmployeePaymentMethods );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE_EMPLOYEE_PAYMENT_METHODS
 */
export function* watchForDeletePaymentMethods() {
    const watcher = yield takeEvery( DELETE_EMPLOYEE_PAYMENT_METHODS, deleteEmployeePaymentMethods );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for ASSIGN_EMPLOYEE_PAYMENT_METHOD
 */
export function* watchForAssignPaymentMethod() {
    const watcher = yield takeEvery( ASSIGN_EMPLOYEE_PAYMENT_METHOD, assignEmployeePaymentMethod );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_HISTORICAL_EARNINGS
 */
export function* watchForGetEmployeeHistoricalEarnings() {
    const watcher = yield takeEvery( GET_EMPLOYEE_HISTORICAL_EARNINGS, getEmployeeHistoricalEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_ANNUAL_EARNINGS
 */
export function* watchForGetEmployeeAnnualEarnings() {
    const watcher = yield takeEvery( GET_EMPLOYEE_ANNUAL_EARNINGS, getEmployeeAnnualEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_EARNINGS
 */
export function* watchForGetEmployeeEarnings() {
    const watcher = yield takeEvery( GET_EMPLOYEE_EARNINGS, getEmployeeEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_DATA_NECESSARY_FOR_EDIT
 */
export function* watchForGetDataNecessaryForEdit() {
    const watcher = yield takeEvery( GET_DATA_NECESSARY_FOR_EDIT, getDataNecessaryForEdit );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_LEAVE_CREDITS
 */
export function* watchForGetEmployeeLeaveCredits() {
    const watcher = yield takeEvery( GET_EMPLOYEE_LEAVE_CREDITS, getEmployeeLeaveCredits );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPDATE_EMPLOYEE_LEAVE_CREDITS
 */
export function* watchForUpdateEmployeeLeaveCredits() {
    const watcher = yield takeEvery( UPDATE_EMPLOYEE_LEAVE_CREDITS, updateEmployeeLeaveCredits );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_FILED_LEAVES
 */
export function* watchForGetEmployeeFiledLeaves() {
    const watcher = yield takeEvery( GET_EMPLOYEE_FILED_LEAVES, getEmployeeFiledLeaves );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPDATE_EMPLOYEE_APPROVAL_GROUPS
 */
export function* watchForUpdateEmployeeApprovalGroups() {
    const watcher = yield takeEvery( UPDATE_EMPLOYEE_APPROVAL_GROUPS, updateEmployeeApprovalGroups );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS
 */
export function* watchForApprovalCheckPendingRequests() {
    const watcher = yield takeEvery( GET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS, approvalCheckPendingRequests );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPDATE_EMPLOYEE_ANNUAL_EARNINGS
 */
export function* watchForUpdateEmployeeAnnualEarnings() {
    const watcher = yield takeEvery( UPDATE_EMPLOYEE_ANNUAL_EARNINGS, updateEmployeeAnnualEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for UPLOAD_PROFILE_PICTURE
 */
export function* watchForUploadProfilePicture() {
    const watcher = yield takeEvery( UPLOAD_PROFILE_PICTURE, uploadProfilePicture );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitialData,
    watchForGetEmployeeProfile,
    watchForSaveEmployeeProfile,
    watchForSavePhilippineEmployeeProfile,
    watchForNotifyUser,
    watchForExportLoans,
    watchForDeleteLoans,
    watchForDeleteTerminationInformation,
    watchForReinitializePage,
    watchForOtherIncomesExport,
    watchForOtherIncomesDelete,
    watchForGetEmployeeLeaveCredits,
    watchForUpdateEmployeeLeaveCredits,
    watchForExportLeaveCredits,
    watchForGetEmployeeFiledLeaves,
    watchForUpdateEmployeeApprovalGroups,
    watchForApprovalCheckPendingRequests,
    watchForExportFiledLeaves,
    watchForExportHistoricalEarnings,
    watchForExportEmployeeProfile,
    watchForUpdateEmployeeAnnualEarnings,
    watchForGetEmployeeOtherIncome,
    watchForGetEmployeeLoans,
    watchForGetEmployeeHistoricalEarnings,
    watchForGetEmployeeEarnings,
    watchForGetDataNecessaryForEdit,
    watchForGetEmployeeApprovalGroups,
    watchForGetEmployeePaymentMethods,
    watchForGetEmployeeBasicPayAdjustments,
    watchForDeleteBasicPayAdjustments,
    watchForDeletePaymentMethods,
    watchForAssignPaymentMethod,
    watchForGetEmployeeAnnualEarnings,
    watchForUploadProfilePicture
];
