
/* eslint-disable no-return-assign */
import React from 'react';
import startCase from 'lodash/startCase';
import get from 'lodash/get';

import { subscriptionService } from 'utils/SubscriptionService';

import UploadPhoto from '../UploadPhoto';

import {
    ListWrapper,
    ListItemWrapper,
    MessageWrapper
} from '../templates/styles';

import { PROFILE_SECTION_IDS, PROFILE_PICTURE } from '../constants';

import { EmployeeAvatar, EmployeeAvatarInfo, EmployeeAvatarInfoContainer } from './styles';

/**
 *
 * EmployeeProfileSectionNavigation component
 *
 */
class EmployeeProfileSectionNavigation extends React.PureComponent {
    static propTypes = {
        employee: React.PropTypes.object,
        loading: React.PropTypes.bool,
        onClick: React.PropTypes.func,
        products: React.PropTypes.array,
        hasEmployeeTerminationDetails: React.PropTypes.bool,
        employeePicture: React.PropTypes.string,
        loadingProfilePicture: React.PropTypes.bool,
        uploadProfilePicture: React.PropTypes.func,
        currentSection: React.PropTypes.string
    }

    /**
     * component's constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
        };

        this.toggleSectionDropDown = this.toggleSectionDropDown.bind( this );
        this.menuHeaders = {};
    }

    /**
     * handles route changes
     */
    componentWillReceiveProps( nextProps ) {
        if ( nextProps.currentSection !== this.props.currentSection ) {
            this.handleActiveHover( nextProps.currentSection );
        }
    }

    onItemClick( item ) {
        return ( e ) => {
            e.preventDefault();

            this.handleActiveHover( item.id );

            if ( !this.props.loading ) {
                this.props.onClick( item.id );
            }
        };
    }

    getEmployeeName() {
        const { employee: { data }} = this.props;

        if ( data && data.first_name ) {
            const firstName = data.first_name ? startCase( data.first_name.toLowerCase() ) : '';
            const middleName = data.middle_name ? startCase( data.middle_name[ 0 ].toLowerCase() ) : null;
            const lastName = data.first_name ? startCase( data.last_name.toLowerCase() ) : '';

            const fullName = `${firstName} ${middleName ? `${middleName}. ` : ''}${lastName}`;

            return fullName;
        }

        return 'loading...';
    }

    getProfilePictureUrl() {
        return this.props.employeePicture ? this.props.employeePicture : PROFILE_PICTURE;
    }

    getSections() {
        const sections = {};

        sections.general = {
            order: 1,
            label: 'GENERAL',
            items: this.generalSection()
        };

        if ( this.isSubscribedToTA() ) {
            sections.time_and_attendance = {
                order: 2,
                label: 'TIME AND ATTENDANCE',
                items: this.timeAttendanceSection()
            };
        }

        if ( this.isSubscribedToPayroll() ) {
            sections.payroll = {
                order: 3,
                label: 'PAYROLL',
                items: this.payrollSection()
            };
        }

        const sectionHeaders = {};

        Object.keys( sections ).map( ( key ) => sectionHeaders[ key ] = true );
        this.menuHeaders = sectionHeaders;

        return sections;
    }

    generalSection() {
        return [
            {
                id: PROFILE_SECTION_IDS.BASIC_INFORMATION,
                label: 'Personal Information',
                visible: true
            },
            {
                id: PROFILE_SECTION_IDS.EMPLOYMENT_DETAILS,
                label: 'Employment Information',
                visible: true
            },
            {
                id: PROFILE_SECTION_IDS.APPROVAL_GROUPS,
                label: 'Approval Workflows',
                visible: !this.isSubscribedToPayrollOnly()
            },
            {
                id: PROFILE_SECTION_IDS.TERMINATION_INFORMATION,
                label: 'Termination Information',
                visible: this.props.hasEmployeeTerminationDetails
            }
        ].filter( ( section ) => section.visible );
    }

    timeAttendanceSection() {
        const isSubscribedToTA = this.isSubscribedToTA();

        return [
            {
                id: PROFILE_SECTION_IDS.LEAVE_CREDITS,
                label: 'Leave Credits',
                visible: isSubscribedToTA
            },
            {
                id: PROFILE_SECTION_IDS.FILED_LEAVES,
                label: 'Filed Leaves',
                visible: isSubscribedToTA
            }
        ].filter( ( section ) => section.visible );
    }

    payrollSection() {
        const isSubscribedToPayroll = this.isSubscribedToPayroll();

        return [
            {
                id: PROFILE_SECTION_IDS.PAYROLL_INFORMATION,
                label: 'Payroll Information',
                visible: isSubscribedToPayroll
            },
            {
                id: PROFILE_SECTION_IDS.BASIC_PAY_ADJUSTMENTS,
                label: 'Basic Pay Adjustments',
                visible: isSubscribedToPayroll
            },
            {
                id: PROFILE_SECTION_IDS.PAYMENT_METHODS,
                label: 'Payment Methods',
                visible: isSubscribedToPayroll
            },
            {
                id: PROFILE_SECTION_IDS.ALLOWANCES,
                label: 'Allowances',
                visible: isSubscribedToPayroll
            },
            {
                id: PROFILE_SECTION_IDS.BONUSES,
                label: 'Bonuses',
                visible: isSubscribedToPayroll
            },
            {
                id: PROFILE_SECTION_IDS.COMMISSIONS,
                label: 'Commissions',
                visible: isSubscribedToPayroll
            },
            {
                id: PROFILE_SECTION_IDS.ADJUSTMENTS,
                label: 'Adjustments',
                visible: isSubscribedToPayroll
            },
            {
                id: PROFILE_SECTION_IDS.LOANS,
                label: 'Loans',
                visible: isSubscribedToPayroll
            },
            {
                id: PROFILE_SECTION_IDS.DEDUCTIONS,
                label: 'Deductions',
                visible: isSubscribedToPayroll
            },
            {
                id: PROFILE_SECTION_IDS.ANNUAL_EARNINGS,
                label: 'Annual Earnings',
                visible: isSubscribedToPayroll
            }
        ].filter( ( section ) => section.visible );
    }

    uploadProfilePicture( file ) {
        this.props.uploadProfilePicture( this.props.employee.data.id, file );
    }

    toggleSectionDropDown( key ) {
        this.menuHeaders[ key ] = !this.menuHeaders[ key ];

        const sectionListId = `${key}-section-list`;
        const sectionIconId = `${key}-section-icon`;

        const sectionListRef = document.getElementById( sectionListId );
        const sectionIconRef = document.getElementById( sectionIconId );

        if ( this.menuHeaders[ key ]) {
            sectionListRef.classList.remove( 'hide' );
            sectionIconRef.classList.remove( 'fa-caret-up' );
            sectionIconRef.classList.add( 'fa-caret-down' );
        } else {
            sectionListRef.classList.add( 'hide' );
            sectionIconRef.classList.remove( 'fa-caret-down' );
            sectionIconRef.classList.add( 'fa-caret-up' );
        }
    }

    isSubscribedToTA() {
        return this.props.products && subscriptionService.isSubscribedToTA( this.props.products );
    }

    isSubscribedToPayroll() {
        return this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products );
    }

    isSubscribedToPayrollOnly() {
        return this.props.products && subscriptionService.isSubscribedToPayrollOnly( this.props.products );
    }

    handleActiveHover( sectionId ) {
        const sectionIds = Object.values( PROFILE_SECTION_IDS ).filter( ( item ) => item !== sectionId );
        sectionIds.forEach( ( element ) => {
            const elementId = `${element}-list-item`;
            const elementRef = document.getElementById( elementId );

            if ( elementRef ) {
                elementRef.classList.remove( 'active' );
            }
        });

        const activeElementId = `${sectionId}-list-item`;
        const activeElementRef = document.getElementById( activeElementId );

        if ( activeElementRef ) {
            activeElementRef.classList.add( 'active' );
        }
    }

    /**
     * renders a loading screen
     */
    renderLoadingScreen() {
        return (
            <MessageWrapper style={ { backgroundColor: '#fff' } }>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" aria-hidden="true" />
                    <div className="description">
                        Loading employee list...
                    </div>
                </div>
            </MessageWrapper>
        );
    }

    renderItems() {
        const sections = this.getSections();
        const sectionsArray = Object.keys( sections );

        return (
            <div className="list">
                { sectionsArray.map( ( key ) => (
                    <div key={ `${key}-parent` } >
                        <button className="menu-btn" onClick={ () => this.toggleSectionDropDown( key ) }>
                            {sections[ key ].label} &nbsp;
                            <i id={ `${key}-section-icon` } className="fa fa-caret-down"></i>
                        </button>

                        <div id={ `${key}-section-list` }>
                            {sections[ key ].items.map( ( item ) => (
                                <ListItemWrapper
                                    id={ `${item.id}-list-item` }
                                    onClick={ this.onItemClick( item ) }
                                    key={ item.id }
                                    className={ item.id === PROFILE_SECTION_IDS.BASIC_INFORMATION ? 'active' : '' }
                                >
                                    { item.label }
                                </ListItemWrapper>
                        ) )}
                        </div>
                    </div>
                ) ) }
            </div>
        );
    }

    /**
     * render's component to DOM
     */
    render() {
        const { employee: { data }} = this.props;
        const name = get( data, 'name' );

        return (
            <ListWrapper>
                <EmployeeAvatar>
                    <UploadPhoto
                        loadingProfilePicture={ this.props.loadingProfilePicture }
                        uploadPicture={ ( file ) => this.uploadProfilePicture( file ) }
                        employeePicture={ this.props.employeePicture }
                    />

                    { name && (
                        <EmployeeAvatarInfoContainer>
                            <EmployeeAvatarInfo>
                                <strong>{ data && this.getEmployeeName() }</strong>
                            </EmployeeAvatarInfo>
                            <EmployeeAvatarInfo>
                                { data && data.position_name }
                            </EmployeeAvatarInfo>
                        </EmployeeAvatarInfoContainer>
                    ) }

                </EmployeeAvatar>

                { this.renderItems() }
            </ListWrapper>
        );
    }
}

export default EmployeeProfileSectionNavigation;
