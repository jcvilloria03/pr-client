import styled from 'styled-components';

export const EmployeeAvatar = styled.div`
    display: flex;
    padding: 16px;
    border-bottom: 1px solid #e5e5e5;
    flex-direction: column;
    align-items: center;
    gap: 8px;
`;

export const EmployeeAvatarInfoContainer = styled.div`
    text-align: center;
`;

export const EmployeeAvatarInfo = styled.span`
    align-items: center;
    display: flex;
    justify-content: center;
    text-transform: capitalize;
    font-size: 14px;
`;
