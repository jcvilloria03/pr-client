import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';

import A from 'components/A';
import SalConfirm from 'components/SalConfirm';
import SnackBar from 'components/SnackBar';

import EmployeeProfileSectionNavigation from './EmployeeProfileSectionNavigation';

import EmployeeProfileView from './templates/profile';

import * as viewActions from './actions';

import {
    makeSelectCompanyId,
    makeSelectEmployee,
    makeSelectNotification,
    makeSelectProductsState,
    makeSelectEmployeePicture,
    makeSelectLoadingProfilePicture
} from './selectors';

import {
    PageWrapper,
    ConfirmBodyWrapperStyle,
    EmployeeProfileWrapper
} from './styles';

import { PROFILE_SECTION_IDS } from './constants';

/**
 *
 * EmployeeView component
 *
 */
export class EmployeeView extends React.Component {
    static propTypes = {
        initializeData: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        companyId: React.PropTypes.oneOfType([
            React.PropTypes.number,
            React.PropTypes.object
        ]),
        employee: React.PropTypes.shape({
            data: React.PropTypes.object,
            loading: React.PropTypes.bool,
            saving: React.PropTypes.bool,
            errors: React.PropTypes.object,
            terminationInformation: React.PropTypes.object
        }),
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        routeParams: React.PropTypes.object,
        products: React.PropTypes.array,
        employeePicture: React.PropTypes.string,
        loadingProfilePicture: React.PropTypes.bool,
        uploadProfilePicture: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        const { id } = this.props.routeParams;

        this.state = {
            activeEmployee: Number( id || 0 ),
            showDiscardConfirm: false,
            discardCallback: null,
            shouldShowDiscardConfirm: false,
            currentSectionId: PROFILE_SECTION_IDS.BASIC_INFORMATION
        };

        this.canLoadEmployeeProfile = this.canLoadEmployeeProfile.bind( this );
    }

    /**
     * Handle logic after component is rendered to DOM
     */
    componentDidMount() {
        this.state.activeEmployee && this.props.initializeData({
            companyId: this.props.companyId,
            id: this.state.activeEmployee
        });
        isAuthorized(['view.employee'], ( authorized ) => { !authorized && browserHistory.replace( '/unauthorized' ); });
    }

    /**
     * handles route changes when changing active employee
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.routeParams.id !== this.props.routeParams.id && this.setState({ activeEmployee: Number( nextProps.routeParams.id ) });

        if ( !isEqual( nextProps.employee, this.props.employee ) ) {
            this.disableMenuOnEmployeeSaving( nextProps.employee.saving );
        }

        if ( this.state.currentSectionId === PROFILE_SECTION_IDS.TERMINATION_INFORMATION
            && this.props.employee.terminationInformation && isEmpty( nextProps.employee.terminationInformation ) ) {
            this.scrollToSection( PROFILE_SECTION_IDS.BASIC_INFORMATION );
        }
    }

    /**
     * triggers when component is to be removed from DOM
     */
    componentWillUnmount() {
        this.props.resetStore();
    }

    setShouldShowDiscardConfirm = ( value ) => {
        this.setState({ shouldShowDiscardConfirm: value });
    }

    /**
     * Navigate to employees page
     */
    gotoEmployeesPage() {
        const gotoEmployees = () => browserHistory.push( '/employees', true );
        if ( this.canLoadEmployeeProfile( gotoEmployees ) ) {
            gotoEmployees();
        }
    }

    /**
     * Checks if employee is updating or currently in edit
     */
    canLoadEmployeeProfile( discardCallback ) {
        const { employee } = this.props;
        if ( this.employeeProfile ) {
            const { employeeEdit } = this.employeeProfile.refs.wrappedInstance.state;

            const hasUnsavedChanges = Object.keys( employeeEdit ).some( ( key ) => {
                const savedValue = Object.prototype.hasOwnProperty.call( employee.data, key )
                ? employee.data[ key ]
                : get( employee.data, `time_attendance.${key}` );
                return savedValue !== employeeEdit[ key ];
            });

            if ( this.props.employee.saving ) {
                return false;
            } else if ( !isEmpty( employeeEdit ) && hasUnsavedChanges ) {
                this.setState({
                    showDiscardConfirm: false,
                    discardCallback
                }, () => this.setState( ( prevState ) => ({ showDiscardConfirm: prevState.shouldShowDiscardConfirm || hasUnsavedChanges }), () => {
                    if ( !this.state.showDiscardConfirm ) {
                        discardCallback();
                    }
                }) );
                return false;
            }
        }
        return true;
    }

    disableMenuOnEmployeeSaving( status ) {
        const sectionIds = Object.values( PROFILE_SECTION_IDS );
        sectionIds.forEach( ( element ) => {
            const elementId = `${element}-item`;
            const elementRef = document.getElementById( elementId );
            if ( elementRef ) {
                if ( status ) {
                    elementRef.classList.add( 'disabled' );
                } else {
                    elementRef.classList.remove( 'disabled' );
                }
            }
        });
    }

    scrollToSection = ( sectionId ) => {
        if ( this.employeeProfile ) {
            this.setState({ currentSectionId: sectionId });
            this.employeeProfile.refs.wrappedInstance.scrollTo( sectionId );
        }
    }

    /**
     * renders component to DOM
     */
    render() {
        return (
            <EmployeeProfileWrapper>
                <Helmet
                    title="Employee"
                    meta={ [
                        { name: 'description', content: 'Description of Employee View' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ () => { this.state.discardCallback && this.state.discardCallback(); } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Clicking Discard will undo all the changes you made on this page.
                                <br /><br />
                                Are you sure you want to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    confirmText="Discard"
                    cancelText="Stay on this page"
                    title="Discard Changes"
                    buttonStyle="danger"
                    visible={ this.state.showDiscardConfirm }
                />
                <PageWrapper>
                    <div className="heading">
                        <A
                            href
                            onClick={ ( e ) => { e.preventDefault(); this.gotoEmployeesPage(); } }
                        >
                            &#8592; Back to Employees
                        </A>
                    </div>

                    <div className="content">
                        <EmployeeProfileSectionNavigation
                            ref={ ( ref ) => { this.employeeList = ref; } }
                            employee={ this.props.employee }
                            loading={ this.props.employee.loading }
                            onClick={ this.scrollToSection }
                            products={ this.props.products }
                            hasEmployeeTerminationDetails={ Object.keys( this.props.employee.terminationInformation ).length > 0 }
                            employeePicture={ this.props.employeePicture }
                            loadingProfilePicture={ this.props.loadingProfilePicture }
                            currentSection={ this.state.currentSectionId }
                            uploadProfilePicture={ this.props.uploadProfilePicture }
                        />
                        <EmployeeProfileView
                            ref={ ( ref ) => { this.employeeProfile = ref; } }
                            setShouldShowDiscardConfirm={ this.setShouldShowDiscardConfirm }
                            canLoadEmployeeProfile={ this.canLoadEmployeeProfile }
                            currentSection={ this.state.currentSectionId }
                            scrollToSection={ this.scrollToSection }
                        />
                    </div>
                </PageWrapper>
            </EmployeeProfileWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    companyId: makeSelectCompanyId(),
    employee: makeSelectEmployee(),
    notification: makeSelectNotification(),
    products: makeSelectProductsState(),
    employeePicture: makeSelectEmployeePicture(),
    loadingProfilePicture: makeSelectLoadingProfilePicture()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        viewActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EmployeeView );
