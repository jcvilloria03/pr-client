import {
    INITIAL_DATA,
    GET_EMPLOYEE,
    GET_EMPLOYEE_LEAVE_CREDITS,
    GET_EMPLOYEE_FILED_LEAVES,
    GET_EMPLOYEE_OTHER_INCOME,
    GET_EMPLOYEE_LOANS,
    GET_EMPLOYEE_HISTORICAL_EARNINGS,
    GET_EMPLOYEE_EARNINGS,
    GET_EMPLOYEE_PAYMENT_METHODS,
    GET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS,
    GET_DATA_NECESSARY_FOR_EDIT,
    GET_EMPLOYEE_APPROVAL_GROUPS,
    UPDATE_EMPLOYEE_LEAVE_CREDITS,
    UPDATE_EMPLOYEE_APPROVAL_GROUPS,
    GET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS,
    UPDATE_EMPLOYEE_ANNUAL_EARNINGS,
    SAVE_EMPLOYEE,
    EXPORT_EMPLOYEE_PROFILE,
    EXPORT_LOANS,
    EXPORT_LEAVE_CREDITS,
    EXPORT_FILED_LEAVES,
    EXPORT_HISTORICAL_EARNINGS,
    DELETE_LOANS,
    DELETE_TERMINATION_INFORMATION,
    DELETE_BASIC_PAY_ADJUSTMENTS,
    SAVE_PHILIPPINE_EMPLOYEE,
    NAMESPACE,
    TYPE_PLURALS,
    DELETE_EMPLOYEE_PAYMENT_METHODS,
    ASSIGN_EMPLOYEE_PAYMENT_METHOD,
    GET_EMPLOYEE_ANNUAL_EARNINGS,
    SET_PROFILE_PICTURE,
    UPLOAD_PROFILE_PICTURE
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data for payslip page
 */
export function initializeData( payload ) {
    return {
        type: INITIAL_DATA,
        payload
    };
}

/**
 * Gets employee profile by id
 */
export function getEmployeeProfile( payload ) {
    return {
        type: GET_EMPLOYEE,
        payload
    };
}

/**
 * Gets Employee's Payment Methods
 */
export function getEmployeePaymentMethods( payload ) {
    return {
        type: GET_EMPLOYEE_PAYMENT_METHODS,
        payload
    };
}

/**
 * Gets employee basic pay adjustments
 */
export function getEmployeeBasicPayAdjustments( payload ) {
    return {
        type: GET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS,
        payload
    };
}

/**
 * Gets employee other income
 */
export function getEmployeeOtherIncome({ employeeId, otherIncomeType }) {
    return {
        type: GET_EMPLOYEE_OTHER_INCOME,
        payload: {
            employeeId,
            otherIncomeType
        }
    };
}

/**
 * Gets employee loans
 */
export function getEmployeeLoans( employeeId ) {
    return {
        type: GET_EMPLOYEE_LOANS,
        payload: {
            employeeId
        }
    };
}

/**
 * Gets employee loans
 */
export function getEmployeeHistoricalEarnings( employeeId ) {
    return {
        type: GET_EMPLOYEE_HISTORICAL_EARNINGS,
        payload: {
            employeeId
        }
    };
}

/**
 * Gets employee's annual earnings
 * @param {Number} employeeId - Employee ID
 * @returns {Object} action
 */
export function getEmployeeAnnualEarnings( employeeId ) {
    return {
        type: GET_EMPLOYEE_ANNUAL_EARNINGS,
        payload: {
            employee_id: employeeId
        }
    };
}

/**
 * Gets employee earnings
 */
export function getEmployeeEarnings( employeeId ) {
    return {
        type: GET_EMPLOYEE_EARNINGS,
        payload: {
            employeeId
        }
    };
}

/**
 * Gets employee approval groups
 */
export function getEmployeeApprovalGroups( employeeId ) {
    return {
        type: GET_EMPLOYEE_APPROVAL_GROUPS,
        employeeId
    };
}

/**
 * Gets data necessary for editing certain sections
 */
export function getDataNecessaryForEdit( section, employeeId ) {
    return {
        type: GET_DATA_NECESSARY_FOR_EDIT,
        payload: {
            section,
            employeeId
        }
    };
}

/**
 * Gets employee leave credits
 */
export function getEmployeeLeaveCredits( employeeId, data ) {
    return {
        type: GET_EMPLOYEE_LEAVE_CREDITS,
        employeeId,
        data
    };
}

/**
 * Update employee leave credits
 */
export function updateEmployeeLeaveCredits( leaveCredits, callback = null ) {
    return {
        type: UPDATE_EMPLOYEE_LEAVE_CREDITS,
        payload: leaveCredits,
        callback
    };
}

/**
 * Gets employee leave credits
 */
export function getEmployeeFiledLeaves( employeeId, data ) {
    return {
        type: GET_EMPLOYEE_FILED_LEAVES,
        employeeId,
        data
    };
}

/**
 * Update employee approval groups
 */
export function updateEmployeeApprovalGroups( employeeId, approvalGroups ) {
    return {
        type: UPDATE_EMPLOYEE_APPROVAL_GROUPS,
        employeeId,
        approvalGroups
    };
}

/**
 * Save employee profile changes
 */
export function saveEmployeeProfile( id, employee, section ) {
    return {
        type: SAVE_EMPLOYEE,
        payload: {
            id,
            employee,
            section
        }
    };
}

/**
 * Export loans
 */
export function exportLoans( ids ) {
    return {
        type: EXPORT_LOANS,
        payload: ids
    };
}

/**
 * Export historical earnings
 */
export function exportHistoricalEarnings( ids ) {
    return {
        type: EXPORT_HISTORICAL_EARNINGS,
        payload: ids
    };
}

/**
 * Update employee annual earnings
 */
export function updateEmployeeAnnualEarnings({ employeeId, data }) {
    return {
        type: UPDATE_EMPLOYEE_ANNUAL_EARNINGS,
        payload: {
            employeeId,
            data
        }
    };
}

/**
 * Export Employee profile
 */
export function exportEmployeeProfile( payload ) {
    return {
        type: EXPORT_EMPLOYEE_PROFILE,
        payload
    };
}

/**
 * Export leave credits
 */
export function exportLeaveCredits( payload ) {
    return {
        type: EXPORT_LEAVE_CREDITS,
        payload
    };
}

/**
 * Export filed leaves
 */
export function exportFiledLeaves( payload ) {
    return {
        type: EXPORT_FILED_LEAVES,
        payload
    };
}

/**
 * Export other incomes
 */
export function exportOtherIncomes( payload ) {
    return {
        type: `${NAMESPACE}EXPORT_${TYPE_PLURALS[ payload.type ]}`,
        payload
    };
}

/**
 * Delete loans
 */
export function deleteLoans( payload ) {
    return {
        type: DELETE_LOANS,
        payload
    };
}

/**
 * Delete termination information
 */
export function deleteTerminationInformation( payload ) {
    return {
        type: DELETE_TERMINATION_INFORMATION,
        payload
    };
}

/**
 * Delete basic pay adjustments
 */
export function deleteBasicPayAdjustments( payload ) {
    return {
        type: DELETE_BASIC_PAY_ADJUSTMENTS,
        payload
    };
}

/**
 * Delete other incomes
 */
export function deleteOtherIncomes( payload ) {
    return {
        type: `${NAMESPACE}DELETE_${TYPE_PLURALS[ payload.type ]}`,
        payload
    };
}

/**
 * Delete employee payment methods
 * @param {String|Number} employeeId - Employee ID
 * @param {Array} ids - Employee payment method IDs
 * @returns {Object}
 */
export function deleteEmployeePaymentMethods( employeeId, ids ) {
    return {
        type: DELETE_EMPLOYEE_PAYMENT_METHODS,
        payload: {
            employeeId,
            ids
        }
    };
}

/**
 * Assign employee payment method
 * @param {String|Number} employeeId - Employee ID
 * @param {String|Number} paymentMethodId - Payment method ID
 * @returns {Object}
 */
export function assignEmployeePaymentMethod( employeeId, paymentMethodId ) {
    return {
        type: ASSIGN_EMPLOYEE_PAYMENT_METHOD,
        payload: {
            employeeId,
            paymentMethodId
        }
    };
}

/**
 * Save philippine employee profile changes
 */
export function savePhilippineEmployeeProfile( id, employee, section ) {
    return {
        type: SAVE_PHILIPPINE_EMPLOYEE,
        payload: {
            id,
            employee,
            section
        }
    };
}

/**
 * Check if there are pending requests for workflows that are being changed
 */
export function approvalCheckPendingRequests( payload, callback ) {
    return {
        type: GET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS,
        payload,
        callback
    };
}

/**
 * Upload employee picture
 */
export function uploadProfilePicture( employeeId, file ) {
    return {
        type: UPLOAD_PROFILE_PICTURE,
        employeeId,
        file
    };
}

/**
 * Set profile picture
 */
export function setProfilePicture( payload ) {
    return {
        type: SET_PROFILE_PICTURE,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
