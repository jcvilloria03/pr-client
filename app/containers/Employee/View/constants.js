import { DATE_FORMATS } from 'utils/constants';

/*
 *
 * Employee View constants
 *
 */
export const NAMESPACE = 'app/Employee/View/';

export const INITIAL_DATA = `${NAMESPACE}INITIAL_DATA`;

export const GET_DATA_NECESSARY_FOR_EDIT = `${NAMESPACE}GET_DATA_NECESSARY_FOR_EDIT`;

export const GET_EMPLOYEE = `${NAMESPACE}GET_EMPLOYEE`;
export const SET_EMPLOYEE = `${NAMESPACE}SET_EMPLOYEE`;
export const SET_EMPLOYEE_LOANS = `${NAMESPACE}SET_EMPLOYEE_LOANS`;
export const SET_EMPLOYEE_LOADING = `${NAMESPACE}SET_EMPLOYEE_LOADING`;
export const SET_EMPLOYEE_LOADING_LOANS = `${NAMESPACE}SET_EMPLOYEE_LOADING_LOANS`;
export const SET_EMPLOYEE_DOWNLOADING_LOANS = `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_LOANS`;

export const GET_EMPLOYEE_LOANS = `${NAMESPACE}GET_EMPLOYEE_LOANS`;
export const EXPORT_LOANS = `${NAMESPACE}EXPORT_LOANS`;
export const DELETE_LOANS = `${NAMESPACE}DELETE_LOANS`;

export const DELETE_TERMINATION_INFORMATION = `${NAMESPACE}DELETE_TERMINATION_INFORMATION`;

export const SAVE_EMPLOYEE = `${NAMESPACE}SAVE_EMPLOYEE`;
export const SAVE_PHILIPPINE_EMPLOYEE = `${NAMESPACE}SAVE_PHILIPPINE_EMPLOYEE`;
export const SET_EMPLOYEE_SAVING = `${NAMESPACE}SET_EMPLOYEE_SAVING`;
export const SET_EMPLOYEE_ERRORS = `${NAMESPACE}SET_EMPLOYEE_ERRORS`;

export const SET_EMPLOYEES = `${NAMESPACE}SET_EMPLOYEES`;
export const SET_EMPLOYEES_LOADING = `${NAMESPACE}GET_EMPLOYEES_LOADING`;

export const SET_FORM_CONSTANTS = `${NAMESPACE}SET_FORM_OPTIONS`;
export const SET_COST_CENTERS = `${NAMESPACE}SET_COST_CENTERS`;
export const SET_RANKS = `${NAMESPACE}SET_RANKS`;
export const SET_EMPLOYMENT_TYPES = `${NAMESPACE}SET_EMPLOYMENT_TYPES`;
export const SET_POSITIONS = `${NAMESPACE}SET_POSITIONS`;
export const SET_TEAMS = `${NAMESPACE}SET_TEAMS`;
export const SET_DEPARTMENTS = `${NAMESPACE}SET_DEPARTMENTS`;
export const SET_LOCATIONS = `${NAMESPACE}SET_LOCATIONS`;
export const SET_PAYROLL_GROUPS = `${NAMESPACE}SET_PAYROLL_GROUPS`;
export const SET_REQUEST_TYPES = `${NAMESPACE}SET_REQUEST_TYPES`;
export const SET_WORKFLOWS = `${NAMESPACE}SET_WORKFLOWS`;
export const SET_COUNTRIES = `${NAMESPACE}SET_COUNTRIES`;

export const SET_COMPANY_DATA = `${NAMESPACE}SET_COMPANY_DATA`;

export const NOTIFICATION_SAGA = `${NAMESPACE}NOTIFICATION_SAGA`;
export const NOTIFICATION = `${NAMESPACE}NOTIFICATION`;

export const FIXED = 'FIXED';
export const CONSULTANT = 'CONSULTANT';

export const GET_EMPLOYEE_OTHER_INCOME = `${NAMESPACE}GET_EMPLOYEE_OTHER_INCOME`;
export const GET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS = `${NAMESPACE}GET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS`;

export const SET_EMPLOYEE_LOADING_BONUSES = `${NAMESPACE}SET_EMPLOYEE_LOADING_BONUSES`;
export const SET_EMPLOYEE_LOADING_ALLOWANCES = `${NAMESPACE}SET_EMPLOYEE_LOADING_ALLOWANCES`;
export const SET_EMPLOYEE_LOADING_COMMISSIONS = `${NAMESPACE}SET_EMPLOYEE_LOADING_COMMISSIONS`;
export const SET_EMPLOYEE_LOADING_DEDUCTIONS = `${NAMESPACE}SET_EMPLOYEE_LOADING_DEDUCTIONS`;
export const SET_EMPLOYEE_LOADING_PAYMENT_METHODS = `${NAMESPACE}SET_EMPLOYEE_LOADING_PAYMENT_METHODS`;
export const SET_EMPLOYEE_LOADING_ADJUSTMENTS = `${NAMESPACE}SET_EMPLOYEE_LOADING_ADJUSTMENTS`;
export const SET_EMPLOYEE_LOADING_BASIC_PAY_ADJUSTMENTS = `${NAMESPACE}SET_EMPLOYEE_LOADING_BASIC_PAY_ADJUSTMENTS`;
export const SET_EMPLOYEE_LOADING_LEAVE_CREDITS = `${NAMESPACE}SET_EMPLOYEE_LOADING_LEAVE_CREDITS`;
export const SET_EMPLOYEE_LOADING_FILED_LEAVES = `${NAMESPACE}SET_EMPLOYEE_LOADING_FILED_LEAVES`;
export const SET_EMPLOYEE_LOADING_APPROVAL_GROUPS = `${NAMESPACE}SET_EMPLOYEE_LOADING_APPROVAL_GROUPS`;
export const SET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS = `${NAMESPACE}SET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS`;
export const GET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS = `${NAMESPACE}GET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS`;
export const SET_EMPLOYEE_LOADING_HISTORICAL_EARNINGS = `${NAMESPACE}SET_EMPLOYEE_LOADING_HISTORICAL_EARNINGS`;
export const SET_EMPLOYEE_LOADING_ANNUAL_EARNINGS = `${NAMESPACE}SET_EMPLOYEE_LOADING_ANNUAL_EARNINGS`;
export const SET_EMPLOYEE_LOADING_EARNINGS = `${NAMESPACE}SET_EMPLOYEE_LOADING_EARNINGS`;
export const SET_EMPLOYEE_LOADING_TERMINATION_INFORMATION = `${NAMESPACE}SET_EMPLOYEE_LOADING_TERMINATION_INFORMATION`;

export const SET_EMPLOYEE_LOADING_EMPLOYMENT = `${NAMESPACE}SET_EMPLOYEE_LOADING_EMPLOYMENT`;
export const SET_EMPLOYEE_LOADING_PAYROLL = `${NAMESPACE}SET_EMPLOYEE_LOADING_PAYROLL`;
export const SET_EMPLOYEE_LOADING_BASIC = `${NAMESPACE}SET_EMPLOYEE_LOADING_BASIC`;

export const SET_EMPLOYEE_BONUSES = `${NAMESPACE}SET_EMPLOYEE_BONUSES`;
export const SET_EMPLOYEE_ALLOWANCES = `${NAMESPACE}SET_EMPLOYEE_ALLOWANCES`;
export const SET_EMPLOYEE_COMMISSIONS = `${NAMESPACE}SET_EMPLOYEE_COMMISSIONS`;
export const SET_EMPLOYEE_DEDUCTIONS = `${NAMESPACE}SET_EMPLOYEE_DEDUCTIONS`;
export const SET_EMPLOYEE_ADJUSTMENTS = `${NAMESPACE}SET_EMPLOYEE_ADJUSTMENTS`;

export const GET_EMPLOYEE_HISTORICAL_EARNINGS = `${NAMESPACE}GET_EMPLOYEE_HISTORICAL_EARNINGS`;
export const SET_EMPLOYEE_HISTORICAL_EARNINGS = `${NAMESPACE}SET_EMPLOYEE_HISTORICAL_EARNINGS`;

export const GET_EMPLOYEE_ANNUAL_EARNINGS = `${NAMESPACE}GET_EMPLOYEE_ANNUAL_EARNINGS`;
export const SET_EMPLOYEE_ANNUAL_EARNINGS = `${NAMESPACE}SET_EMPLOYEE_ANNUAL_EARNINGS`;
export const GET_EMPLOYEE_EARNINGS = `${NAMESPACE}GET_EMPLOYEE_EARNINGS`;
export const SET_EMPLOYEE_EARNINGS = `${NAMESPACE}SET_EMPLOYEE_EARNINGS`;

export const SET_EMPLOYEE_TERMINATION_INFORMATION = `${NAMESPACE}_SET_EMPLOYEE_TERMINATION_INFORMATION`;
export const SET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS = `${NAMESPACE}SET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS`;

export const GET_EMPLOYEE_LEAVE_CREDITS = `${NAMESPACE}GET_EMPLOYEE_LEAVE_CREDITS`;
export const SET_EMPLOYEE_LEAVE_CREDITS = `${NAMESPACE}SET_EMPLOYEE_LEAVE_CREDITS`;
export const SET_EMPLOYEE_LEAVE_CREDITS_PAGINATION = `${NAMESPACE}SET_EMPLOYEE_LEAVE_CREDITS_PAGINATION`;
export const UPDATE_EMPLOYEE_LEAVE_CREDITS = `${NAMESPACE}UPDATE_EMPLOYEE_LEAVE_CREDITS`;
export const GET_EMPLOYEE_FILED_LEAVES = `${NAMESPACE}GET_EMPLOYEE_FILED_LEAVES`;
export const SET_EMPLOYEE_FILED_LEAVES = `${NAMESPACE}SET_EMPLOYEE_FILED_LEAVES`;
export const SET_EMPLOYEE_FILED_LEAVES_PAGINATION = `${NAMESPACE}SET_EMPLOYEE_FILED_LEAVES_PAGINATION`;
export const GET_EMPLOYEE_APPROVAL_GROUPS = `${NAMESPACE}GET_EMPLOYEE_APPROVAL_GROUPS`;
export const SET_EMPLOYEE_APPROVAL_GROUPS = `${NAMESPACE}SET_EMPLOYEE_APPROVAL_GROUPS`;
export const UPDATE_EMPLOYEE_APPROVAL_GROUPS = `${NAMESPACE}UPDATE_EMPLOYEE_APPROVAL_GROUPS`;
export const GET_EMPLOYEE_PAYMENT_METHODS = `${NAMESPACE}GET_EMPLOYEE_PAYMENT_METHODS`;
export const SET_EMPLOYEE_PAYMENT_METHODS = `${NAMESPACE}SET_EMPLOYEE_PAYMENT_METHODS`;

export const DELETE_BONUSES = `${NAMESPACE}DELETE_BONUSES`;
export const DELETE_ALLOWANCES = `${NAMESPACE}DELETE_ALLOWANCES`;
export const DELETE_COMMISSIONS = `${NAMESPACE}DELETE_COMMISSIONS`;
export const DELETE_DEDUCTIONS = `${NAMESPACE}DELETE_DEDUCTIONS`;
export const DELETE_ADJUSTMENTS = `${NAMESPACE}DELETE_ADJUSTMENTS`;
export const DELETE_BASIC_PAY_ADJUSTMENTS = `${NAMESPACE}DELETE_BASIC_PAY_ADJUSTMENTS`;
export const DELETE_EMPLOYEE_PAYMENT_METHODS = `${NAMESPACE}DELETE_EMPLOYEE_PAYMENT_METHODS`;
export const ASSIGN_EMPLOYEE_PAYMENT_METHOD = `${NAMESPACE}ASSIGN_EMPLOYEE_PAYMENT_METHOD`;

export const EXPORT_EMPLOYEE_PROFILE = `${NAMESPACE}EXPORT_EMPLOYEE_PROFILE`;
export const EXPORT_BONUSES = `${NAMESPACE}EXPORT_BONUSES`;
export const EXPORT_ALLOWANCES = `${NAMESPACE}EXPORT_ALLOWANCES`;
export const EXPORT_COMMISSIONS = `${NAMESPACE}EXPORT_COMMISSIONS`;
export const EXPORT_DEDUCTIONS = `${NAMESPACE}EXPORT_DEDUCTIONS`;
export const EXPORT_ADJUSTMENTS = `${NAMESPACE}EXPORT_ADJUSTMENTS`;
export const EXPORT_LEAVE_CREDITS = `${NAMESPACE}EXPORT_LEAVE_CREDITS`;
export const EXPORT_FILED_LEAVES = `${NAMESPACE}EXPORT_FILED_LEAVES`;
export const EXPORT_HISTORICAL_EARNINGS = `${NAMESPACE}EXPORT_HISTORICAL_EARNINGS`;

export const SET_EMPLOYEE_DOWNLOADING_PROFILE = `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_PROFILE`;
export const SET_EMPLOYEE_DOWNLOADING_BONUSES = `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_BONUSES`;
export const SET_EMPLOYEE_DOWNLOADING_ALLOWANCES = `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_ALLOWANCES`;
export const SET_EMPLOYEE_DOWNLOADING_COMMISSIONS = `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_COMMISSIONS`;
export const SET_EMPLOYEE_DOWNLOADING_DEDUCTIONS = `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_DEDUCTIONS`;
export const SET_EMPLOYEE_DOWNLOADING_ADJUSTMENTS = `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_ADJUSTMENTS`;
export const SET_EMPLOYEE_DOWNLOADING_LEAVE_CREDITS = `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_LEAVE_CREDITS`;
export const SET_EMPLOYEE_DOWNLOADING_FILED_LEAVES = `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_FILED_LEAVES`;
export const SET_EMPLOYEE_DOWNLOADING_HISTORICAL_EARNINGS = `${NAMESPACE}SET_EMPLOYEE_DOWNLOADING_HISTORICAL_EARNINGS`;

export const SET_ANNUAL_EARNINGS_FORM_OPTIONS = `${NAMESPACE}SET_ANNUAL_EARNINGS_FORM_OPTIONS`;
export const UPDATE_EMPLOYEE_ANNUAL_EARNINGS = `${NAMESPACE}UPDATE_EMPLOYEE_ANNUAL_EARNINGS`;

export const SET_PROFILE_PICTURE = `${NAMESPACE}_SET_PROFILE_PICTURE`;
export const UPLOAD_PROFILE_PICTURE = `${NAMESPACE}_UPLOAD_PROFILE_PICTURE`;
export const SET_EMPLOYEE_LOADING_PICTURE = `${NAMESPACE}_SET_EMPLOYEE_LOADING_PICTURE`;

export const TYPE_PLURALS = {
    BONUS: 'BONUSES',
    ALLOWANCE: 'ALLOWANCES',
    COMMISSION: 'COMMISSIONS',
    DEDUCTION: 'DEDUCTIONS',
    ADJUSTMENT: 'ADJUSTMENTS'
};

export const INITIAL_PERMISSIONS = {
    view: true,
    edit: true,
    create: true,
    delete: true
};

export const REQUEST_TYPES = {
    LEAVES: 'Leave',
    TIME_DISPUTE: 'Time record',
    SHIFT_CHANGE: 'Shift change',
    PAID_UNDERTIME: 'Undertime',
    OVERTIME: 'Overtime',
    LOAN: 'Loan'
};

export const EMPLOYEE_SECTIONS = {
    EMPLOYMENT: 'EMPLOYMENT',
    TIME_ATTENDANCE: 'TIME_ATTENDANCE',
    PAYROLL: 'PAYROLL',
    BASIC: 'BASIC'
};

export const OPTIONS = {
    GENDER: [
        { label: 'Male', value: 'male' },
        { label: 'Female', value: 'female' }
    ],
    MARITAL_STATUS: [
        { label: 'Single', value: 'single' },
        { label: 'Married', value: 'married' },
        { label: 'Separated', value: 'separated' },
        { label: 'Widowed', value: 'widowed' }
    ],
    TEAM_ROLE: [
        { label: 'Leader', value: 'leader' },
        { label: 'Member', value: 'member' }
    ]
};

export const INACTIVE = 'inactive';
export const STATUS_TYPES = [
    { label: 'Active', value: 'active' },
    { label: 'Inactive', value: 'inactive' },
    { label: 'Semi-Active', value: 'semi-active' }
];

export const UNLIMITED = 'Unlimited';
export const NOT_AVAILABLE = 'N/A';

export const PROFILE_SECTION_IDS = {
    TERMINATION_INFORMATION: 'employee-profile-termination-information',
    BASIC_INFORMATION: 'employee-profile-basic-information',
    EMPLOYMENT_DETAILS: 'employee-profile-employement-details',
    PAYROLL_INFORMATION: 'employee-profile-payroll-information',
    TIME_ATTENDANCE_INFORMATION: 'employee-profile-time-attendance-information',
    APPROVAL_GROUPS: 'employee-profile-approval-groups',
    ANNUAL_EARNINGS: 'employee-profile-annual-earnings',
    EARNINGS: 'employee-profile-earnings',
    BASIC_PAY_ADJUSTMENTS: 'employee-profile-basic-pay-adjustments',
    LEAVE_CREDITS: 'employee-profile-leave-credits',
    HISTORICAL_EARNINGS: 'employee-profile-historical-earnings',
    FILED_LEAVES: 'employee-profile-filed-leaves',
    LOANS: 'employee-profile-loans',
    DEDUCTIONS: 'employee-profile-deductions',
    ADJUSTMENTS: 'employee-profile-adjustments',
    ALLOWANCES: 'employee-profile-allowances',
    BONUSES: 'employee-profile-bonuses',
    COMMISSIONS: 'employee-profile-commissions',
    PAYMENT_METHODS: 'employee-payment-methods',
    SCHEDULES: 'employee-profile-schedules'
};

export const TA_FIELDS = {
    EMPLOYMENT_DETAILS: {
        sections: [
            {
                title: 'Employment Details',
                icon_class: 'fa fa-user-circle-o',
                rows: [
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'employee_id',
                            label: 'Employee ID',
                            placeholder: 'Employee ID',
                            validations: {
                                required: true
                            },
                            type: 'employee_id',
                            is_demo_field: true
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'active',
                            label: 'Employee Status',
                            placeholder: 'Select Status',
                            validations: {
                                required: true
                            },
                            options: STATUS_TYPES
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'location_id',
                            label: 'Primary Work Location',
                            placeholder: 'Select priamry location',
                            validations: {
                                required: false
                            },
                            options: 'locations'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'secondary_location_id',
                            label: 'Secondary Work Location',
                            placeholder: 'Select secondary location',
                            validations: {
                                required: false
                            },
                            options: 'locations'
                        }
                    ],
                    [

                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'employment_type_id',
                            label: 'Employment Type',
                            placeholder: 'Select employment type',
                            validations: {
                                required: false
                            },
                            options: 'employmentTypes'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'department_id',
                            label: 'Department',
                            placeholder: 'Select department',
                            validations: {
                                required: false
                            },
                            options: 'departments'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'position_id',
                            label: 'Position',
                            placeholder: 'Select position',
                            validations: {
                                required: false
                            },
                            options: 'positions'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'rank_id',
                            label: 'Rank',
                            placeholder: 'Select rank',
                            validations: {
                                required: false
                            },
                            options: 'ranks'
                        }
                    ],
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'team_id',
                            label: 'Team',
                            placeholder: 'Select team',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'teams'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'team_role',
                            label: 'Team Role',
                            placeholder: 'Select role',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: OPTIONS.TEAM_ROLE
                        }
                    ]
                ]
            },
            {
                title: 'Hiring Details',
                rows: [
                    [
                        {
                            field_type: 'date-picker',
                            class_name: 'col-xs-3 date',
                            id: 'date_hired',
                            label: 'Date Hired',
                            placeholder: 'Select a date',
                            validations: {
                                required: false
                            },
                            display_format: DATE_FORMATS.DISPLAY,
                            api_format: DATE_FORMATS.API
                        },
                        {
                            field_type: 'date-picker',
                            class_name: 'col-xs-3 date',
                            id: 'date_ended',
                            label: 'Date Separated',
                            placeholder: 'Select a date',
                            validations: {
                                required: false
                            },
                            display_format: DATE_FORMATS.DISPLAY,
                            api_format: DATE_FORMATS.API
                        },
                        {
                            field_type: 'text',
                            class_name: 'col-xs-3 date',
                            id: 'tenure_date',
                            label: 'Tenure',
                            placeholder: 'Tenure',
                            validations: {
                                required: false
                            }
                        }
                    ]
                ]
            },
            {
                title: 'Entitlements',
                icon_class: 'fa fa-certificate',
                rows: [
                    [
                        {
                            field_type: 'switch2',
                            label: 'Time Sheet Required',
                            class_name: 'col-xs-3 switch',
                            id: 'timesheet_required',
                            placeholder: 'Time Sheet Required'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Unworked Regular Holiday Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'regular_holiday_pay',
                            placeholder: 'Unworked Regular Holiday Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Unworked Special Holiday Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'special_holiday_pay',
                            placeholder: 'Unworked Special Holiday Pay'
                        }
                    ],
                    [
                        {
                            field_type: 'switch2',
                            label: 'Rest Day Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'rest_day_pay',
                            placeholder: 'Rest Day Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Holiday Premium Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'holiday_premium_pay',
                            placeholder: 'Holiday Premium Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Night Differential Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'differential',
                            placeholder: 'Night Differential Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Overtime Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'overtime',
                            placeholder: 'Overtime Pay'
                        }
                    ]
                ]
            }
        ]
    }
};

export const PAYROLL_FIELDS = {
    EMPLOYMENT_DETAILS: {
        sections: [
            {
                title: 'Employment Details',
                icon_class: 'fa fa-user-circle-o',
                rows: [
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'employee_id',
                            label: 'Employee ID',
                            placeholder: 'Employee ID',
                            validations: {
                                required: true
                            },
                            is_demo_field: true
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'active',
                            label: 'Employee Status',
                            placeholder: 'Select Status',
                            validations: {
                                required: true
                            },
                            options: STATUS_TYPES
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'location_id',
                            label: 'Primary Work Location',
                            placeholder: 'Select priamry location',
                            validations: {
                                required: false
                            },
                            options: 'locations'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'secondary_location_id',
                            label: 'Secondary Work Location',
                            placeholder: 'Select secondary location',
                            validations: {
                                required: false
                            },
                            options: 'locations'
                        }
                    ],
                    [

                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'employment_type_id',
                            label: 'Employment Type',
                            placeholder: 'Select employment type',
                            validations: {
                                required: false
                            },
                            options: 'employmentTypes'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'department_id',
                            label: 'Department',
                            placeholder: 'Select department',
                            validations: {
                                required: false
                            },
                            options: 'departments'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'position_id',
                            label: 'Position',
                            placeholder: 'Select position',
                            validations: {
                                required: false
                            },
                            options: 'positions'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'rank_id',
                            label: 'Rank',
                            placeholder: 'Select rank',
                            validations: {
                                required: false
                            },
                            options: 'ranks'
                        }
                    ],
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'team_id',
                            label: 'Team',
                            placeholder: 'Select team',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'teams'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'team_role',
                            label: 'Team Role',
                            placeholder: 'Select role',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: OPTIONS.TEAM_ROLE
                        }
                    ]
                ]
            },
            {
                title: 'Hiring Details',
                rows: [
                    [
                        {
                            field_type: 'date-picker',
                            class_name: 'col-xs-3 date',
                            id: 'date_hired',
                            label: 'Date Hired',
                            placeholder: 'Select a date',
                            validations: {
                                required: false
                            },
                            display_format: DATE_FORMATS.DISPLAY,
                            api_format: DATE_FORMATS.API
                        },
                        {
                            field_type: 'date-picker',
                            class_name: 'col-xs-3 date',
                            id: 'date_ended',
                            label: 'Date Separated',
                            placeholder: 'Select a date',
                            validations: {
                                required: false
                            },
                            display_format: DATE_FORMATS.DISPLAY,
                            api_format: DATE_FORMATS.API
                        },
                        {
                            field_type: 'button',
                            class_name: 'col-xs-3',
                            id: 'btn_terminate',
                            label: 'Date Separated',
                            placeholder: 'Add Termination Information',
                            validations: {
                                required: false
                            }
                        },
                        {
                            field_type: 'text',
                            class_name: 'col-xs-3 date',
                            id: 'tenure_date',
                            label: 'Tenure',
                            placeholder: 'Tenure',
                            validations: {
                                required: false
                            }
                        }
                    ]
                ]
            },
            {
                title: 'Entitlements',
                icon_class: 'fa fa-certificate',
                rows: [
                    [
                        {
                            field_type: 'switch2',
                            label: 'Time Sheet Required',
                            class_name: 'col-xs-3 switch',
                            id: 'timesheet_required',
                            placeholder: 'Time Sheet Required'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Unworked Regular Holiday Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'regular_holiday_pay',
                            placeholder: 'Unworked Regular Holiday Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Unworked Special Holiday Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'special_holiday_pay',
                            placeholder: 'Unworked Special Holiday Pay'
                        }
                    ],
                    [
                        {
                            field_type: 'switch2',
                            label: 'Rest Day Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'rest_day_pay',
                            placeholder: 'Rest Day Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Holiday Premium Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'holiday_premium_pay',
                            placeholder: 'Holiday Premium Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Night Differential Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'differential',
                            placeholder: 'Night Differential Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Overtime Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'overtime',
                            placeholder: 'Overtime Pay'
                        }
                    ]
                ]
            }
        ]
    },
    PAYROLL_INFORMATION: {
        sections: [
            {
                title: 'Payroll Details',
                icon_class: 'fa fa-credit-card-alt',
                rows: [
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'payroll_group_id',
                            label: 'Payroll Group',
                            placeholder: 'Select payroll group',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'payrollGroups'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'base_pay',
                            label: 'Base Pay',
                            placeholder: 'Enter basic pay',
                            validations: {
                                required: false
                            },
                            is_demo_field: true
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'base_pay_unit',
                            label: 'Base Pay Unit',
                            placeholder: 'Select base pay unit',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'base_pay_unit'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'hours_per_day',
                            label: 'Hours per Day',
                            placeholder: 'Enter amount',
                            validations: {
                                required: false
                            }
                        }
                    ],
                    [

                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'tax_type',
                            label: 'Tax Type',
                            placeholder: 'Select tax type',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'tax_type'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'tax_status',
                            label: 'Tax Status',
                            placeholder: 'Select tax status',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'tax_status'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'cost_center_id',
                            label: 'Cost Center',
                            placeholder: 'Select cost center',
                            validations: {
                                required: false
                            },
                            options: 'costCenters'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'rdo',
                            label: 'RDO',
                            placeholder: 'RDO',
                            validations: {
                                required: false
                            },
                            type: 'rdo'
                        }
                    ],
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'consultant_tax_rate',
                            label: 'Consultant Tax Rate (%)',
                            placeholder: 'Enter rate',
                            validations: {
                                required: false,
                                max: 3
                            }
                        }
                    ]
                ]
            },
            {
                title: 'Government Contributions',
                icon_class: 'fa fa-building',
                rows: [
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'sss_basis',
                            label: 'SSS Basis',
                            placeholder: 'Select contribution basis',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'sss_basis'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'sss_amount',
                            label: 'SSS Amount',
                            placeholder: 'Enter amount',
                            validations: {
                                required: true
                            }
                        }
                    ],
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'philhealth_basis',
                            label: 'PhilHealth Basis',
                            placeholder: 'Select contribution basis',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'philhealth_basis'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'philhealth_amount',
                            label: 'PhilHealth Amount',
                            placeholder: 'Enter amount',
                            validations: {
                                required: false
                            }
                        }
                    ],
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'hdmf_basis',
                            label: 'HDMF Basis',
                            placeholder: 'Select contribution basis',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'hdmf_basis'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'hdmf_amount',
                            label: 'HDMF Amount',
                            placeholder: 'Enter amount',
                            validations: {
                                required: false
                            }
                        }
                    ]
                ]
            },
            {
                title: 'Government Numbers',
                icon_class: 'fa fa-user-circle-o',
                rows: [
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'tin',
                            label: 'Tax Identification Number (TIN)',
                            placeholder: '000-000-000-000',
                            validations: {
                                required: false
                            },
                            type: 'tin'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'sss_number',
                            label: 'Social Security System (SSS)',
                            placeholder: '00-0000000-0',
                            validations: {
                                required: false
                            },
                            type: 'sss'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'philhealth_number',
                            label: 'PhilHealth Number',
                            placeholder: '00-000000000-0',
                            validations: {
                                required: false
                            },
                            type: 'philhealth'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'hdmf_number',
                            label: 'HDMF Number',
                            placeholder: '0000-0000-0000',
                            validations: {
                                required: false
                            },
                            type: 'hdmf'
                        }
                    ]
                ]
            }
        ]
    }
};

export const TA_PLUS_PAYROLL_FIELDS = {
    EMPLOYMENT_DETAILS: {
        sections: [
            {
                title: 'Employment Details',
                icon_class: 'fa fa-user-circle-o',
                rows: [
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'employee_id',
                            label: 'Employee ID',
                            placeholder: 'Employee ID',
                            validations: {
                                required: true
                            },
                            type: 'employee_id'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'active',
                            label: 'Employee Status',
                            placeholder: 'Select Status',
                            validations: {
                                required: true
                            },
                            options: STATUS_TYPES
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'location_id',
                            label: 'Primary Work Location',
                            placeholder: 'Select priamry location',
                            validations: {
                                required: false
                            },
                            options: 'locations'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'secondary_location_id',
                            label: 'Secondary Work Location',
                            placeholder: 'Select secondary location',
                            validations: {
                                required: false
                            },
                            options: 'locations'
                        }
                    ],
                    [

                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'employment_type_id',
                            label: 'Employment Type',
                            placeholder: 'Select employment type',
                            validations: {
                                required: false
                            },
                            options: 'employmentTypes',
                            clearable: true
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'department_id',
                            label: 'Department',
                            placeholder: 'Select department',
                            validations: {
                                required: false
                            },
                            options: 'departments',
                            clearable: true
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'position_id',
                            label: 'Position',
                            placeholder: 'Select position',
                            validations: {
                                required: false
                            },
                            options: 'positions',
                            clearable: true
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'rank_id',
                            label: 'Rank',
                            placeholder: 'Select rank',
                            validations: {
                                required: false
                            },
                            options: 'ranks',
                            clearable: true
                        }
                    ],
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'team_id',
                            label: 'Team',
                            placeholder: 'Select team',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'teams',
                            clearable: true
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'team_role',
                            label: 'Team Role',
                            placeholder: 'Select role',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: OPTIONS.TEAM_ROLE,
                            clearable: true
                        }
                    ]
                ]
            },
            {
                title: 'Hiring Details',
                rows: [
                    [
                        {
                            field_type: 'date-picker',
                            class_name: 'col-xs-3 date',
                            id: 'date_hired',
                            label: 'Date Hired',
                            placeholder: 'Select a date',
                            validations: {
                                required: false
                            },
                            display_format: DATE_FORMATS.DISPLAY,
                            api_format: DATE_FORMATS.API
                        },
                        {
                            field_type: 'date-picker',
                            class_name: 'col-xs-3 date',
                            id: 'date_ended',
                            label: 'Date Separated',
                            placeholder: 'Select a date',
                            validations: {
                                required: false
                            },
                            display_format: DATE_FORMATS.DISPLAY,
                            api_format: DATE_FORMATS.API
                        },
                        {
                            field_type: 'button',
                            class_name: 'col-xs-3',
                            id: 'btn_terminate',
                            label: 'Date Separated',
                            placeholder: 'Add Termination Information',
                            validations: {
                                required: false
                            }
                        },
                        {
                            field_type: 'text',
                            class_name: 'col-xs-3 date',
                            id: 'tenure_date',
                            label: 'Tenure',
                            placeholder: 'Tenure',
                            validations: {
                                required: false
                            }
                        }
                    ]
                ]
            },
            {
                title: 'Entitlements',
                icon_class: 'fa fa-certificate',
                rows: [
                    [
                        {
                            field_type: 'switch2',
                            label: 'Time Sheet Required',
                            class_name: 'col-xs-3 switch',
                            id: 'timesheet_required',
                            placeholder: 'Time Sheet Required'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Unworked Regular Holiday Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'regular_holiday_pay',
                            placeholder: 'Unworked Regular Holiday Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Unworked Special Holiday Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'special_holiday_pay',
                            placeholder: 'Unworked Special Holiday Pay'
                        }
                    ],
                    [
                        {
                            field_type: 'switch2',
                            label: 'Rest Day Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'rest_day_pay',
                            placeholder: 'Rest Day Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Holiday Premium Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'holiday_premium_pay',
                            placeholder: 'Holiday Premium Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Night Differential Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'differential',
                            placeholder: 'Night Differential Pay'
                        },
                        {
                            field_type: 'switch2',
                            label: 'Overtime Pay',
                            class_name: 'col-xs-3 switch',
                            id: 'overtime',
                            placeholder: 'Overtime Pay'
                        }
                    ]
                ]
            }
        ]
    },
    PAYROLL_INFORMATION: {
        sections: [
            {
                title: 'Payroll Details',
                icon_class: 'fa fa-credit-card-alt',
                rows: [
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'payroll_group_id',
                            label: 'Payroll Group',
                            placeholder: 'Select payroll group',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'payrollGroups'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'base_pay',
                            label: 'Base Pay',
                            placeholder: 'Enter basic pay',
                            validations: {
                                required: false
                            },
                            is_demo_field: true
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'base_pay_unit',
                            label: 'Base Pay Unit',
                            placeholder: 'Select base pay unit',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'base_pay_unit'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'hours_per_day',
                            label: 'Hours per Day',
                            placeholder: 'Enter amount',
                            validations: {
                                required: false
                            }
                        }
                    ],
                    [

                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'tax_type',
                            label: 'Tax Type',
                            placeholder: 'Select tax type',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'tax_type'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'tax_status',
                            label: 'Tax Status',
                            placeholder: 'Select tax status',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'tax_status'
                        },
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'cost_center_id',
                            label: 'Cost Center',
                            placeholder: 'Select cost center',
                            validations: {
                                required: false
                            },
                            options: 'costCenters'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'rdo',
                            label: 'RDO',
                            placeholder: 'RDO',
                            validations: {
                                required: false
                            },
                            type: 'rdo'
                        }
                    ],
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'consultant_tax_rate',
                            label: 'Consultant Tax Rate (%)',
                            placeholder: 'Enter rate',
                            validations: {
                                required: false,
                                max: 3
                            }
                        }
                    ]
                ]
            },
            {
                title: 'Government Contributions',
                icon_class: 'fa fa-building',
                rows: [
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'sss_basis',
                            label: 'SSS Basis',
                            placeholder: 'Select contribution basis',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'sss_basis'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'sss_amount',
                            label: 'SSS Amount',
                            placeholder: 'Enter amount',
                            validations: {
                                required: true
                            }
                        }
                    ],
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'philhealth_basis',
                            label: 'PhilHealth Basis',
                            placeholder: 'Select contribution basis',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'philhealth_basis'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'philhealth_amount',
                            label: 'PhilHealth Amount',
                            placeholder: 'Enter amount',
                            validations: {
                                required: false
                            }
                        }
                    ],
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-3',
                            id: 'hdmf_basis',
                            label: 'HDMF Basis',
                            placeholder: 'Select contribution basis',
                            validations: {
                                required: false
                            },
                            autoSize: true,
                            options: 'hdmf_basis'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'hdmf_amount',
                            label: 'HDMF Amount',
                            placeholder: 'Enter amount',
                            validations: {
                                required: false
                            }
                        }
                    ]
                ]
            },
            {
                title: 'Government Numbers',
                icon_class: 'fa fa-user-circle-o',
                rows: [
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'tin',
                            label: 'Tax Identification Number (TIN)',
                            placeholder: '000-000-000-000',
                            validations: {
                                required: false
                            },
                            type: 'tin'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'sss_number',
                            label: 'Social Security System (SSS)',
                            placeholder: '00-0000000-0',
                            validations: {
                                required: false
                            },
                            type: 'sss'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'philhealth_number',
                            label: 'PhilHealth Number',
                            placeholder: '00-000000000-0',
                            validations: {
                                required: false
                            },
                            type: 'philhealth'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-3',
                            id: 'hdmf_number',
                            label: 'HDMF Number',
                            placeholder: '0000-0000-0000',
                            validations: {
                                required: false
                            },
                            type: 'hdmf'
                        }
                    ]
                ]
            }
        ]
    }
};

export const CUSTOM_ERROR_MESSAGES = {
    tax_type: 'Tax type is required when payroll group, base pay, or base pay unit is present',
    payroll_group_id: 'Payroll group is required when tax type, base pay, or base pay unit is present',
    base_pay_unit: 'Base pay unit is required when tax type, payroll group, or base pay is present',
    base_pay: 'Base pay is required when tax type, payroll group, or base pay unit is present',
    sss_basis: 'Please select a contribution basis for SSS',
    philhealth_basis: 'Please select a contribution basis for PhilHealth',
    hdmf_basis: 'Please select a contribution basis for HDMF'
};

export const PROFILE_PICTURE = 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/placeholder.png';
