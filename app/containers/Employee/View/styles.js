import styled from 'styled-components';

export const PageWrapper = styled.div`
    display: flex;
    flex-direction: column;
    padding-top: 76px;
    height: 100vh;

    .break-word {
        word-break: break-all;
    }

    > .heading {
        padding: 10px 20px;
        position: fixed;
        background: #f0f4f6;
        z-index: 1;
        width: 100%;
        top: 76px;

        a {
            left: 0
        }
    }

    .hide {
        display: none;
    }

    .disabled {
        cursor: not-allowed;
        opacity: .65;
    }

    > .content {
        display: flex;
        position: relative;
        overflow: hidden;
        height: 100%;
        margin-top: 44px;

        @media (max-width: '960px') {
            > .list-container {
                display: none;
            }
        }
    }
`;

export const EmployeeProfileWrapper = styled.div`
    overflow: 'hidden';
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;
