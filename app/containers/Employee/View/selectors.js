import { createSelector } from 'reselect';

const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Direct selector to the employee profile state domain
 */
const selectEmployeeProfileDomain = () => ( state ) => state.get( 'employee_profile' );

/**
 * Other specific selectors
 */
const makeSelectEmployee = () => createSelector(
    selectEmployeeProfileDomain(),
    ( substate ) => substate.get( 'employee' ).toJS()
);

const makeSelectEmployees = () => createSelector(
    selectEmployeeProfileDomain(),
    ( substate ) => substate.get( 'employees' ).toJS()
);

const makeSelectCompanyId = () => createSelector(
    selectEmployeeProfileDomain(),
    ( substate ) => substate.get( 'company_id' )
);

const makeSelectFormOptions = () => createSelector(
    selectEmployeeProfileDomain(),
    ( substate ) => substate.get( 'form_options' ).toJS()
);

const makeSelectAnnualEarningsFormOptions = () => createSelector(
    selectEmployeeProfileDomain(),
    ( substate ) => substate.get( 'annualEarningsFormOptions' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectEmployeeProfileDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectRequestTypes = () => createSelector(
    selectEmployeeProfileDomain(),
    ( substate ) => substate.get( 'request_types' ).toJS()
);

const makeSelectProductsState = () => createSelector(
    selectAppDomain(),
    ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

const makeSelectEmployeePicture = () => createSelector(
    selectEmployeeProfileDomain(),
    ( substate ) => substate.get( 'picture' )
);

const makeSelectLoadingProfilePicture = () => createSelector(
    selectEmployeeProfileDomain(),
    ( substate ) => substate.get( 'loadingProfilePicture' )
);

const makeSelectUserInformation = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'userInformation' ).toJS()
);

export {
    selectEmployeeProfileDomain,
    makeSelectCompanyId,
    makeSelectEmployee,
    makeSelectEmployees,
    makeSelectFormOptions,
    makeSelectNotification,
    makeSelectRequestTypes,
    makeSelectAnnualEarningsFormOptions,
    makeSelectProductsState,
    makeSelectUserInformation,
    makeSelectEmployeePicture,
    makeSelectLoadingProfilePicture
};
