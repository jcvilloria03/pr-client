import styled from 'styled-components';

export const UploadButtonContainer = styled.div`
    transition: .5s ease;
    opacity: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    text-align: center;
`;

export const UploadImage = styled.img`
    display: block;
    position: relative;
    width: 100%;
    height: 100%;
    object-fit: contain;
    opacity: 1;
    transition: .5s ease;
    backface-visibility: hidden;
`;

export const UploadPhotoWrapper = styled.div`
    input[type=file] {
        display: none;
    }

    label {
        cursor: pointer;
    }

    .left-align {
        text-align: left;
    }

    .error-messages {
        color: red;
        font-size: 14px;
        font-weight: 400;
        text-align: center;
    }

    button:focus {
        border: none !important;
        outline: 0 !important;
        outline-style: none !important;
        outline-width: 0 !important;
    }
`;

export const UploadContainer = styled.div`
    position: relative;
    width: 100px;
    height: 100px;
    border: dashed 2px #1c1b1ba6;
    overflow: hidden;
    margin: 0;

    .transparent-btn {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        color: white;
        font-size: 12px;
        cursor: pointer;
        border-radius: 3px;
        text-align: center;
        width: auto;
        background-color: #69c843;
        padding: 4px 12px;
        transition: .5s ease;

        :focus {
            border: none;
            outline: 0;
            outline-style: none;
            outline-width: 0;
        }
    }

    .btn {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        color: white;
        font-size: 12px;
        cursor: pointer;
        border-radius: 3px;
        text-align: center;
        border: none;
        width: auto;
        transition: .5s ease;
    }

    .cancel-picture-btn {
        position: absolute;
        background: #cf393996;
        color: white;
        top: 2px;
        right: 2px;
        border-radius: 50%;
        font-size: initial;
        box-shadow: 1px 1px 2px #8c8c8c;
        font-weight: 500;
        width: 24px;
        height: 24px;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
        transition: .5s ease;
    }

    :hover {
        .btn {
            background-color: #33adff;
        }

        .cancel-picture-btn {
            background: #cf3939;
        }

        ${UploadImage} {
            opacity: 0.3;
        }

        ${UploadButtonContainer} {
            opacity: 1;
        }
    }
`;

export const LoaderContainer = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    -ms-transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    font-size: 12px;
    cursor: pointer;
    border-radius: 3px;
    text-align: center;
    color: #fdffff;
    background-color: #5cc6316e;
    width: 100px;
    height: 31px;
    font-weight: 600;
    padding-top: 5px;
`;
