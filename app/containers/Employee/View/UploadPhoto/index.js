/* eslint-disable no-return-assign */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-lonely-if */
/* eslint-disable no-plusplus */
import React from 'react';

import Loader from 'components/Loader';
import SalConfirm from 'components/SalConfirm';

import { ConfirmBodyWrapperStyle } from '../templates/styles';

import {
    UploadPhotoWrapper,
    UploadContainer,
    UploadButtonContainer,
    UploadImage,
    LoaderContainer
} from './styles';

import { PROFILE_PICTURE } from '../constants';

/**
 *
 * UploadPhoto component
 *
 */
export default class UploadPhoto extends React.Component {
    static propTypes = {
        uploadPicture: React.PropTypes.func,
        loadingProfilePicture: React.PropTypes.bool,
        employeePicture: React.PropTypes.string
    }

    constructor( props ) {
        super( props );

        this.state = {
            picture: false,
            src: false,
            errorModal: false,
            errors: []
        };

        window.picDetails;
        this.handlePictureSelected = this.handlePictureSelected.bind( this );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.employeePicture !== this.props.employeePicture ) {
            this.reset();
        }
    }

    async getImageDetails( imageUrl ) {
        window.picDetails = await this.checkImageResolution( imageUrl );
    }

    async handlePictureSelected( event ) {
        this.reset();
        const errors = [];
        let canvas;
        let maxSize;
        let width;
        let height;
        let dataurl;
        let arr;
        let mime;
        let bstr;
        let n;
        let u8arr;
        let picture = event.target.files[ 0 ];
        const fileName = picture.name;
        let src = URL.createObjectURL( picture );

        const pictureSize = picture.size / 1024 / 1024;
        const validPictureSize = ( pictureSize <= 2 ); // 2mb max size

        const acceptableFormats = [ 'image/png', 'image/jpg', 'image/jpeg' ];
        const validPictureFormat = acceptableFormats.includes( picture.type );

        this.getImageDetails( src );
        await this.sleep( 100 );

        if ( window.picDetails.width > 640 ) {
            canvas = document.createElement( 'canvas' );
            maxSize = 640;
            width = window.picDetails.width;
            height = window.picDetails.height;

            if ( width > height ) {
                if ( width > maxSize ) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if ( height > maxSize ) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext( '2d' ).drawImage( window.picDetails.image, 0, 0, width, height );
            dataurl = canvas.toDataURL( picture.type );
            arr = dataurl.split( ',' );
            mime = arr[ 0 ].match( /:(.*?);/ )[ 1 ];
            bstr = atob( arr[ 1 ]);
            n = bstr.length;
            u8arr = new Uint8Array( n );

            while ( n-- ) {
                u8arr[ n ] = bstr.charCodeAt( n );
            }

            picture = new File([u8arr], fileName, { type: mime });
            src = URL.createObjectURL( picture );
        }

        if ( !validPictureSize ) errors.push( 'Picture size should not be greater than 2MB.' );
        if ( !validPictureFormat ) errors.push( 'Picture format type should be JPG, JPEG or PNG.' );

        if ( picture && errors.length === 0 ) {
            this.setState({
                picture,
                src
            });
        } else {
            this.setState({ errors, errorModal: false }, () => {
                this.setState({ errorModal: true });
            });
        }
    }

    sleep( ms ) {
        return new Promise( ( resolve ) => setTimeout( resolve, ms ) );
    }

    checkImageResolution( src ) {
        return new Promise( ( resolve, reject ) => {
            const image = new Image();
            image.onload = () => resolve({ image, src, height: image.height, width: image.width });
            image.onerror = reject;
            image.src = src;
        });
    }

    reset() {
        const picture = false;
        const src = false;
        const errors = [];

        this.setState({
            picture,
            src,
            errors,
            errorModal: false
        });
    }

    triggerInputFile = () => {
        this.fileInput.click();
    }

    replacePicture = () => {
        this.props.uploadPicture( this.state.picture );
    }

    renderPreview() {
        const profilePicture = this.props.employeePicture
            ? this.props.employeePicture
            : PROFILE_PICTURE;

        return (
            <UploadContainer
                className="upload-container"
                title="Accepted file types are JPG, JPEG, PNG. Max file size is 2M"
            >
                <UploadImage
                    src={ this.state.src ? this.state.src : profilePicture }
                    alt="Accepted file types are JPG, JPEG, PNG. Max file size is 2M"
                />

                <button
                    title="Cancel selected photo"
                    onClick={ () => this.reset() }
                    className={ !this.state.picture || this.props.loadingProfilePicture
                        ? 'hide'
                        : 'cancel-picture-btn'
                    }
                >
                    <i className="fa fa-times" />
                </button>

                <UploadButtonContainer>
                    <button
                        onClick={ this.triggerInputFile }
                        className={ !this.state.picture ? 'btn' : 'hide' }
                    >
                        Replace
                    </button>

                    <button
                        onClick={ this.replacePicture }
                        className={ this.state.picture && !this.props.loadingProfilePicture
                            ? 'transparent-btn'
                            : 'hide'
                        }
                    >
                        Confirm
                    </button>

                    <input
                        className="hide"
                        type="file"
                        ref={ ( fileInput ) => this.fileInput = fileInput }
                        onChange={ this.handlePictureSelected }

                    />
                </UploadButtonContainer>

                { this.props.loadingProfilePicture && (
                    <LoaderContainer>
                        <Loader />
                    </LoaderContainer>
                ) }
            </UploadContainer>
        );
    }

    render() {
        return (
            <UploadPhotoWrapper>
                { this.renderPreview() }

                <SalConfirm
                    onConfirm={ () => { this.reset(); } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            {
                                this.state.errors.map( ( error, i ) => (
                                    <div className="error-messages" key={ i }>
                                        <p>
                                            { error }
                                        </p>
                                    </div>
                                ) )
                            }
                        </ConfirmBodyWrapperStyle>
                    }
                    showCancel={ false }
                    backdrop={ false }
                    keyboard={ false }
                    confirmText="Confirm"
                    title="Invalid Selected Photo"
                    buttonStyle="danger"
                    visible={ this.state.errorModal }
                />
            </UploadPhotoWrapper>
        );
    }
}
