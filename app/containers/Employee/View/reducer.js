import { fromJS } from 'immutable';

import {
    SET_EMPLOYEE,
    SET_EMPLOYEE_LOANS,
    SET_EMPLOYEE_BONUSES,
    SET_EMPLOYEE_ALLOWANCES,
    SET_EMPLOYEE_COMMISSIONS,
    SET_EMPLOYEE_DEDUCTIONS,
    SET_EMPLOYEE_ADJUSTMENTS,
    SET_EMPLOYEE_LEAVE_CREDITS,
    SET_EMPLOYEE_LEAVE_CREDITS_PAGINATION,
    SET_EMPLOYEE_FILED_LEAVES,
    SET_EMPLOYEE_FILED_LEAVES_PAGINATION,
    SET_EMPLOYEE_APPROVAL_GROUPS,
    SET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS,
    SET_EMPLOYEE_HISTORICAL_EARNINGS,
    SET_EMPLOYEE_ANNUAL_EARNINGS,
    SET_EMPLOYEE_EARNINGS,
    SET_EMPLOYEE_TERMINATION_INFORMATION,
    SET_EMPLOYEE_LOADING,
    SET_EMPLOYEE_LOADING_LOANS,
    SET_EMPLOYEE_LOADING_BONUSES,
    SET_EMPLOYEE_LOADING_ALLOWANCES,
    SET_EMPLOYEE_LOADING_COMMISSIONS,
    SET_EMPLOYEE_LOADING_DEDUCTIONS,
    SET_EMPLOYEE_LOADING_ADJUSTMENTS,
    SET_EMPLOYEE_LOADING_LEAVE_CREDITS,
    SET_EMPLOYEE_LOADING_FILED_LEAVES,
    SET_EMPLOYEE_LOADING_APPROVAL_GROUPS,
    SET_EMPLOYEE_LOADING_HISTORICAL_EARNINGS,
    SET_EMPLOYEE_LOADING_ANNUAL_EARNINGS,
    SET_EMPLOYEE_LOADING_EARNINGS,
    SET_EMPLOYEE_LOADING_TERMINATION_INFORMATION,
    SET_EMPLOYEE_LOADING_EMPLOYMENT,
    SET_EMPLOYEE_LOADING_PAYROLL,
    SET_EMPLOYEE_LOADING_BASIC,
    SET_ANNUAL_EARNINGS_FORM_OPTIONS,
    SET_EMPLOYEE_DOWNLOADING_LOANS,
    SET_EMPLOYEE_DOWNLOADING_BONUSES,
    SET_EMPLOYEE_DOWNLOADING_ALLOWANCES,
    SET_EMPLOYEE_DOWNLOADING_COMMISSIONS,
    SET_EMPLOYEE_DOWNLOADING_DEDUCTIONS,
    SET_EMPLOYEE_DOWNLOADING_ADJUSTMENTS,
    SET_EMPLOYEE_DOWNLOADING_LEAVE_CREDITS,
    SET_EMPLOYEE_DOWNLOADING_FILED_LEAVES,
    SET_EMPLOYEE_DOWNLOADING_HISTORICAL_EARNINGS,
    SET_EMPLOYEE_DOWNLOADING_PROFILE,
    SET_EMPLOYEES,
    SET_EMPLOYEES_LOADING,
    SET_EMPLOYEE_SAVING,
    SET_EMPLOYEE_ERRORS,
    SET_COMPANY_DATA,
    SET_FORM_CONSTANTS,
    SET_COST_CENTERS,
    SET_RANKS,
    SET_EMPLOYMENT_TYPES,
    SET_POSITIONS,
    SET_TEAMS,
    SET_DEPARTMENTS,
    SET_LOCATIONS,
    SET_PAYROLL_GROUPS,
    SET_REQUEST_TYPES,
    SET_WORKFLOWS,
    NOTIFICATION_SAGA,
    SET_EMPLOYEE_LOADING_PAYMENT_METHODS,
    SET_EMPLOYEE_PAYMENT_METHODS,
    SET_EMPLOYEE_LOADING_BASIC_PAY_ADJUSTMENTS,
    SET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS,
    SET_COUNTRIES,
    SET_PROFILE_PICTURE,
    SET_EMPLOYEE_LOADING_PICTURE
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    employee: {
        data: {},
        loading: true,
        saving: false,
        errors: null,
        loans: [],
        bonuses: [],
        allowances: [],
        commissions: [],
        deductions: [],
        adjustments: [],
        paymentMethods: [],
        basicPayAdjustments: [],
        leaveCredits: [],
        leaveCreditsPagination: {},
        filedLeaves: [],
        historicalEarnings: [],
        annualEarnings: {},
        earnings: {},
        terminationInformation: {},
        filedLeavesPagination: {},
        loadingLoans: true,
        loadingBonuses: true,
        loadingAllowances: true,
        loadingCommissions: true,
        loadingDeductions: true,
        loadingAdjustments: true,
        loadingPaymentMethods: true,
        loadingBasicPayAdjustments: true,
        loadingLeaveCredits: true,
        loadingFiledLeaves: true,
        loadingHistoricalEarnings: true,
        loadingAnnualEarnings: true,
        loadingEarnings: true,
        loadingTerminationInformation: true,
        loadingEmploymentInformation: true,
        loadingTimeAttendanceInformation: true,
        loadingBasicInformation: true,
        loadingPayrollInformation: true,
        loadingProfilePicture: true,
        downloadingLoans: false,
        downloadingBonuses: false,
        downloadingAllowances: false,
        downloadingCommissions: false,
        downloadingDeductions: false,
        downloadingAdjustments: false,
        downloadingLeaveCredits: false,
        downloadingFiledLeaves: false,
        downloadingEmployeeProfile: false,
        downloadingHistoricalEarnings: false,
        approvalHasPendingRequests: false,
        picture: false
    },
    employees: {
        data: [],
        loading: true
    },
    company_id: null,
    form_options: {
        constants: {
            base_pay_unit: [],
            tax_type: [],
            tax_status: [],
            sss_basis: [],
            hdmf_basis: [],
            philhealth_basis: [],
            statuses: [],
            team_roles: []
        },
        costCenters: [],
        departments: [],
        ranks: [],
        employmentTypes: [],
        positions: [],
        teams: [],
        locations: [],
        payrollGroups: [],
        workflows: [],
        countries: []
    },
    annualEarningsFormOptions: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    request_types: [],
    employeeEdit: {}
});

/**
 * transforms data from API to accomodate structure in app
 */
function prepareEmployeeData( data ) {
    const formattedData = data;
    const { address } = formattedData;
    const middleName = data.middle_name ? `${data.middle_name.charAt( 0 ).toUpperCase()}. ` : '';

    formattedData.name = `${data.first_name} ${middleName}${data.last_name}`;

    if ( address ) {
        let addressLine2 = address.address_line_2 ? `${address.address_line_2}, ` : '';
        addressLine2 += address.city ? `${address.city}, ` : '';
        addressLine2 += address.zip_code || '';

        formattedData.formatted_address_line_2 = addressLine2;

        formattedData.address_line_1 = address.address_line_1;
        formattedData.address_line_2 = address.address_line_2;
        formattedData.city = address.city;
        formattedData.zip_code = address.zip_code;
        formattedData.country = address.country;
    }

    if ( data.payroll ) {
        formattedData.tax_type = data.payroll.tax_type ? data.payroll.tax_type.toUpperCase() : '';
        formattedData.tax_status = data.payroll.tax_status ? data.payroll.tax_status.toUpperCase() : '';
        formattedData.sss_basis = data.payroll.sss_basis ? data.payroll.sss_basis.toUpperCase() : '';
        formattedData.philhealth_basis = data.payroll.philhealth_basis ? data.payroll.philhealth_basis.toUpperCase() : '';
        formattedData.hdmf_basis = data.payroll.hdmf_basis ? data.payroll.hdmf_basis.toUpperCase() : '';

        formattedData.sss_amount = data.payroll.sss_amount ? data.payroll.sss_amount : '';
        formattedData.philhealth_amount = data.payroll.philhealth_amount ? data.payroll.philhealth_amount : '';
        formattedData.hdmf_amount = data.payroll.hdmf_amount ? data.payroll.hdmf_amount : '';

        formattedData.sss_number = data.payroll.sss_number ? data.payroll.sss_number : '';
        formattedData.tin = data.payroll.tin ? data.payroll.tin : '';
        formattedData.hdmf_number = data.payroll.hdmf_number ? data.payroll.hdmf_number : '';
        formattedData.philhealth_number = data.payroll.philhealth_number ? data.payroll.philhealth_number : '';
        formattedData.rdo = data.payroll.rdo ? data.payroll.rdo : '';

        const consultantTaxRate = data.payroll.consultant_tax_rate;
        formattedData.consultant_tax_rate = consultantTaxRate && !isNaN( Number( consultantTaxRate ) )
            ? ( Number( consultantTaxRate ) * 100 )
            : consultantTaxRate;
    }

    return formattedData;
}

/**
 * transforms data from API to accomodate structure in app
 */
function prepareEmployeesData( data ) {
    const formattedData = data.map( ( employee, index ) => ({
        index,
        id: employee.id,
        employee_id: employee.employee_id,
        name: `${employee.first_name} ${employee.last_name}`
    }) );

    return formattedData;
}

/**
 * transforms data from API to accomodate structure in app
 */
function prepareCommonOptionData( data ) {
    const formattedData = data.map( ( item ) => ({
        value: item.id,
        label: item.name
    }) );

    return formattedData;
}

/**
 * transforms data from API to accomodate structure in app
 */
function prepareCountryOptions( data ) {
    const formattedData = data.map( ( item ) => ({
        value: item.name,
        label: item.name
    }) );

    return formattedData;
}

/**
 * Transforms workflows from API to accomodate structure in app
 */
function prepareWorkflowOptions( data ) {
    return data.map( ( workflow ) => {
        const levels = workflow.levels.length > 1 ? 'levels' : 'level';

        return {
            value: workflow.id,
            label: `${workflow.levels.length} ${levels} approval - ${workflow.name}`
        };
    });
}

/**
 * Transforms payment methods from API to accommodated structure in application
 * @param {Array} data - Employee payment methods
 */
function preparePaymentMethods( data ) {
    return data.map( ({ id, type, attributes }) => ({
        id,
        type,
        account_type: attributes.disbursementMethod,
        account_details: attributes.details || '-',
        account_number: attributes.accountNumber || '-',
        credit_to: attributes.active,
        noselect: [ 'Cash', 'Cheque' ].includes( attributes.disbursementMethod ) || attributes.details === 'SALPAY'
    }) );
}

/**
 * Transforms termination details from API to accommodated structure in application
 * @param {Object} data - Employee termination details
 */
function prepareTerminationInformation({ id, type, attributes }) {
    return id && type
        ? { id, type, ...attributes }
        : {};
}

/**
 * EmployeeProfileReducer
 */
function EmployeeProfileReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_EMPLOYEE:
            return state.setIn([ 'employee', 'data' ], fromJS( prepareEmployeeData( action.payload ) ) );
        case SET_EMPLOYEE_LOANS:
            return state.setIn([ 'employee', 'loans' ], action.payload );
        case SET_EMPLOYEE_BONUSES:
            return state.setIn([ 'employee', 'bonuses' ], action.payload );
        case SET_EMPLOYEE_ALLOWANCES:
            return state.setIn([ 'employee', 'allowances' ], action.payload );
        case SET_EMPLOYEE_COMMISSIONS:
            return state.setIn([ 'employee', 'commissions' ], action.payload );
        case SET_EMPLOYEE_DEDUCTIONS:
            return state.setIn([ 'employee', 'deductions' ], action.payload );
        case SET_EMPLOYEE_ADJUSTMENTS:
            return state.setIn([ 'employee', 'adjustments' ], action.payload );
        case SET_EMPLOYEE_PAYMENT_METHODS:
            return state.setIn([ 'employee', 'paymentMethods' ], preparePaymentMethods( action.payload ) );
        case SET_EMPLOYEE_BASIC_PAY_ADJUSTMENTS:
            return state.setIn([ 'employee', 'basicPayAdjustments' ], action.payload );
        case SET_EMPLOYEE_LEAVE_CREDITS:
            return state.setIn([ 'employee', 'leaveCredits' ], action.payload );
        case SET_EMPLOYEE_LEAVE_CREDITS_PAGINATION:
            return state.setIn([ 'employee', 'leaveCreditsPagination' ], action.payload );
        case SET_EMPLOYEE_FILED_LEAVES:
            return state.setIn([ 'employee', 'filedLeaves' ], action.payload );
        case SET_EMPLOYEE_FILED_LEAVES_PAGINATION:
            return state.setIn([ 'employee', 'filedLeavesPagination' ], action.payload );
        case SET_EMPLOYEE_APPROVAL_GROUPS:
            return state.setIn([ 'employee', 'approvalGroups' ], action.payload );
        case SET_EMPLOYEE_APPROVAL_HAS_PENDING_REQUESTS:
            return state.setIn([ 'employee', 'approvalHasPendingRequests' ], action.payload );
        case SET_EMPLOYEE_HISTORICAL_EARNINGS:
            return state.setIn([ 'employee', 'historicalEarnings' ], action.payload );
        case SET_EMPLOYEE_ANNUAL_EARNINGS:
            return state.setIn([ 'employee', 'annualEarnings' ], action.payload );
        case SET_EMPLOYEE_EARNINGS:
            return state.setIn([ 'employee', 'earnings' ], action.payload );
        case SET_ANNUAL_EARNINGS_FORM_OPTIONS:
            return state.set( 'annualEarningsFormOptions', fromJS( action.payload ) );
        case SET_EMPLOYEE_TERMINATION_INFORMATION:
            return state.setIn([ 'employee', 'terminationInformation' ], prepareTerminationInformation( action.payload ) );
        case SET_EMPLOYEE_LOADING:
            return state.setIn([ 'employee', 'loading' ], action.payload );
        case SET_EMPLOYEE_LOADING_LOANS:
            return state.setIn([ 'employee', 'loadingLoans' ], action.payload );
        case SET_EMPLOYEE_LOADING_BONUSES:
            return state.setIn([ 'employee', 'loadingBonuses' ], action.payload );
        case SET_EMPLOYEE_LOADING_ALLOWANCES:
            return state.setIn([ 'employee', 'loadingAllowances' ], action.payload );
        case SET_EMPLOYEE_LOADING_COMMISSIONS:
            return state.setIn([ 'employee', 'loadingCommissions' ], action.payload );
        case SET_EMPLOYEE_LOADING_DEDUCTIONS:
            return state.setIn([ 'employee', 'loadingDeductions' ], action.payload );
        case SET_EMPLOYEE_LOADING_ADJUSTMENTS:
            return state.setIn([ 'employee', 'loadingAdjustments' ], action.payload );
        case SET_EMPLOYEE_LOADING_PAYMENT_METHODS:
            return state.setIn([ 'employee', 'loadingPaymentMethods' ], action.payload );
        case SET_EMPLOYEE_LOADING_BASIC_PAY_ADJUSTMENTS:
            return state.setIn([ 'employee', 'loadingBasicPayAdjustments' ], action.payload );
        case SET_EMPLOYEE_LOADING_LEAVE_CREDITS:
            return state.setIn([ 'employee', 'loadingLeaveCredits' ], action.payload );
        case SET_EMPLOYEE_LOADING_FILED_LEAVES:
            return state.setIn([ 'employee', 'loadingFiledLeaves' ], action.payload );
        case SET_EMPLOYEE_LOADING_APPROVAL_GROUPS:
            return state.setIn([ 'employee', 'loadingApprovalGroups' ], action.payload );
        case SET_EMPLOYEE_LOADING_HISTORICAL_EARNINGS:
            return state.setIn([ 'employee', 'loadingHistoricalEarnings' ], action.payload );
        case SET_EMPLOYEE_LOADING_ANNUAL_EARNINGS:
            return state.setIn([ 'employee', 'loadingAnnualEarnings' ], action.payload );
        case SET_EMPLOYEE_LOADING_EARNINGS:
            return state.setIn([ 'employee', 'loadingEarnings' ], action.payload );
        case SET_EMPLOYEE_LOADING_TERMINATION_INFORMATION:
            return state.setIn([ 'employee', 'loadingTerminationInformation' ], action.payload );
        case SET_EMPLOYEE_LOADING_EMPLOYMENT:
            return state.setIn([ 'employee', 'loadingEmploymentInformation' ], action.payload );
        case SET_EMPLOYEE_LOADING_BASIC:
            return state.setIn([ 'employee', 'loadingBasicInformation' ], action.payload );
        case SET_EMPLOYEE_LOADING_PAYROLL:
            return state.setIn([ 'employee', 'loadingPayrollInformation' ], action.payload );
        case SET_EMPLOYEE_DOWNLOADING_LOANS:
            return state.setIn([ 'employee', 'downloadingLoans' ], action.payload );
        case SET_EMPLOYEE_DOWNLOADING_BONUSES:
            return state.setIn([ 'employee', 'downloadingBonuses' ], action.payload );
        case SET_EMPLOYEE_DOWNLOADING_ALLOWANCES:
            return state.setIn([ 'employee', 'downloadingAllowances' ], action.payload );
        case SET_EMPLOYEE_DOWNLOADING_COMMISSIONS:
            return state.setIn([ 'employee', 'downloadingCommissions' ], action.payload );
        case SET_EMPLOYEE_DOWNLOADING_DEDUCTIONS:
            return state.setIn([ 'employee', 'downloadingDeductions' ], action.payload );
        case SET_EMPLOYEE_DOWNLOADING_ADJUSTMENTS:
            return state.setIn([ 'employee', 'downloadingAdjustments' ], action.payload );
        case SET_EMPLOYEE_DOWNLOADING_LEAVE_CREDITS:
            return state.setIn([ 'employee', 'downloadingLeaveCredits' ], action.payload );
        case SET_EMPLOYEE_DOWNLOADING_FILED_LEAVES:
            return state.setIn([ 'employee', 'downloadingFiledLeaves' ], action.payload );
        case SET_EMPLOYEE_DOWNLOADING_PROFILE:
            return state.setIn([ 'employee', 'downloadingEmployeeProfile' ], action.payload );
        case SET_EMPLOYEE_DOWNLOADING_HISTORICAL_EARNINGS:
            return state.setIn([ 'employee', 'donwloadingHistoricalEarnings' ], action.payload );
        case SET_EMPLOYEE_SAVING:
            return state.setIn([ 'employee', 'saving' ], action.payload );
        case SET_EMPLOYEE_ERRORS:
            return state.setIn([ 'employee', 'errors' ], fromJS( action.payload ) );
        case SET_EMPLOYEES:
            return state.setIn([ 'employees', 'data' ], fromJS( prepareEmployeesData( action.payload ) ) );
        case SET_EMPLOYEES_LOADING:
            return state.setIn([ 'employees', 'loading' ], action.payload );
        case SET_COMPANY_DATA:
            return state.set( 'company_id', action.payload );
        case SET_FORM_CONSTANTS:
            return state.setIn([ 'form_options', 'constants' ], fromJS( action.payload ) );
        case SET_COST_CENTERS:
            return state.setIn([ 'form_options', 'costCenters' ], fromJS( prepareCommonOptionData( action.payload ) ) );
        case SET_DEPARTMENTS:
            return state.setIn([ 'form_options', 'departments' ], fromJS( prepareCommonOptionData( action.payload ) ) );
        case SET_RANKS:
            return state.setIn([ 'form_options', 'ranks' ], fromJS( prepareCommonOptionData( action.payload ) ) );
        case SET_EMPLOYMENT_TYPES:
            return state.setIn([ 'form_options', 'employmentTypes' ], fromJS( prepareCommonOptionData( action.payload ) ) );
        case SET_POSITIONS:
            return state.setIn([ 'form_options', 'positions' ], fromJS( prepareCommonOptionData( action.payload ) ) );
        case SET_TEAMS:
            return state.setIn([ 'form_options', 'teams' ], fromJS( prepareCommonOptionData( action.payload ) ) );
        case SET_LOCATIONS:
            return state.setIn([ 'form_options', 'locations' ], fromJS( prepareCommonOptionData( action.payload ) ) );
        case SET_PAYROLL_GROUPS:
            return state.setIn([ 'form_options', 'payrollGroups' ], fromJS( prepareCommonOptionData( action.payload ) ) );
        case SET_WORKFLOWS:
            return state.setIn([ 'form_options', 'workflows' ], fromJS( prepareWorkflowOptions( action.payload ) ) );
        case SET_COUNTRIES:
            return state.setIn([ 'form_options', 'countries' ], fromJS( prepareCountryOptions( action.payload ) ) );
        case SET_REQUEST_TYPES:
            return state.set( 'request_types', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_PROFILE_PICTURE:
            return state.set( 'picture', action.payload );
        case SET_EMPLOYEE_LOADING_PICTURE:
            return state.set( 'loadingProfilePicture', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default EmployeeProfileReducer;
