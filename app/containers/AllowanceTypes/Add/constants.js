/*
 *
 * AllowanceType constants
 *
 */
const namespace = 'app/containers/AllowanceTypes/Add';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const REINITIALIZE_PAGE = `${namespace}/REINITIALIZE_PAGE`;

export const SET_SUBMITTED = `${namespace}/SET_SUBMITTED`;
export const SUBMITTED = `${namespace}/SUBMITTED`;
