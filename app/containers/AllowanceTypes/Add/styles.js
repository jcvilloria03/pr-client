import styled from 'styled-components';

export const Header = styled.div`
.nav{
    padding:10px 10px;
    background:#f0f4f6;
    margin-bottom:10px;
    position: fixed;
    top: 76px;
    right: 0;
    left: 0;
    width: 100%;
    z-index:9;
    }
`;

export const Footer = styled.div`
    .from_footer{
        margin: 14px 0px 0px;
        padding: 14px 0px;
        background: #F0F4F6;
        margin-top:280px;
        position: fixed;
        bottom: 0;
        right: 0;
        left: 0;
        text-align: end;
    }

    .from_footer .footer_button{
        width: 105%;
        text-align: end;

        .animation {
            color: #2d2d2d;
        }
    }

    .from_footer .footer_button button {
        border-radius: 2px;
        cursor: pointer;
        line-height: 1;
        height: 45px;
        width: 130px;
        &:focus{
            outline:none;
        }
        &:disabled{
            cursor: not-allowed;
        }
    }
`;

export const PageWrapper = styled.div`
    padding-left: 91px;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 120px;
    .loader {
        padding: 140px 0;
    }

    .main_section{
        width: 85%;
        padding-bottom: 80px;
        margin: auto;

        .heading{
            margin-top: 20px;
            margin-bottom: 50px;
            h1{
                text-align: center;
                font-size: 24px;
                font-weight: 600;
                margin-bottom: 5px;
            }
            p{
                text-align: center;
                font-size: 14px;
                margin-bottom: 0px;
            }
        }

        .add-inputs-wrapper {
            background-color: #fdfdfd;

            .block {
                display: block;
                width: 100%;
                cursor: pointer;
                text-align: center;
            }

            .tax-option-wrapper {
                margin-top: -3px;
            }

            button {
                border-radius: 2px;
                width: 100%;
                height: 45px;
                margin-top: 25px;
            }

            .center {
                justify-content: center;
                align-items: center;
            }
        }

        .add-table-wrapper {
            width: 100%;

            button {
                border-radius: 2px;
            }

            .add-table .rt-thead {
                background-color: #00a5e5;
            }

        }
    }
`;

export const ModalBody = styled.div`
font-size:14px;
margin-bottom:20px;
`;
