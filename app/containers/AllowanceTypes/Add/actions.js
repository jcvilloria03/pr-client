
import {
    DEFAULT_ACTION,
    SET_SUBMITTED,
    NOTIFICATION
} from './constants';

/**
 *
 * AllowanceType actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * Submit Allowance Type
 * @param {object} payload
 */
export function submitAllowanceType( payload ) {
    return {
        type: SET_SUBMITTED,
        payload
    };
}

/**
 * Set Notification
 * @param {object} payload
 */
export function notifyUser( payload ) {
    return {
        type: NOTIFICATION,
        payload
    };
}
