import { createSelector } from 'reselect';

/**
 * Direct selector to the top level domain
 */
const selectAllowanceTypeDomain = () => ( state ) => state.get( 'AllowanceTypes' );

/**
 * Direct selector to the view state domain
 */
const selectAllowanceTypeAddDomain = () => ( state ) => state.get( 'AllowanceTypeAdd' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
  selectAllowanceTypeAddDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectAllowanceTypeAddDomain(),
  ( substate ) => substate.get( 'addNotification' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
  selectAllowanceTypeAddDomain(),
  ( substate ) => substate.get( 'submitted' )
);

const makeSelectTaxOptions = () => createSelector(
  selectAllowanceTypeDomain(),
  ( substate ) => substate.get( 'taxOptions' ).toJS()
);

const makeSelectIsNameAvailable = () => createSelector(
  selectAllowanceTypeDomain(),
  ( substate ) => substate.get( 'isNameAvailable' ).toJS()
);

const makeSelectViewNotification = () => createSelector(
  selectAllowanceTypeDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

/**
 * Default selector used by AllowanceType
 */
const makeSelectAllowanceTypes = () => createSelector(
  selectAllowanceTypeAddDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectAllowanceTypes,
  selectAllowanceTypeAddDomain,
  selectAllowanceTypeDomain,
  makeSelectLoading,
  makeSelectNotification,
  makeSelectSubmitted,
  makeSelectTaxOptions,
  makeSelectIsNameAvailable,
  makeSelectViewNotification
};
