import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { cancel, put, call, take } from 'redux-saga/effects';
import { Fetch } from 'utils/request';
import { formatFeedbackMessage } from 'utils/functions';

import { company } from 'utils/CompanyService';

import {
    NOTIFICATION,
    REINITIALIZE_PAGE,
    SET_NOTIFICATION,
    SET_SUBMITTED,
    LOADING
} from './constants';

import { resetStore } from '../../App/sagas';
import { browserHistory } from '../../../utils/BrowserHistory';

export const selectAllowanceTypeList = ( state ) => state.get( 'AllowanceTypes' ).get( 'allowanceTypes' ).toJS();

/**
 * Submit Allowance Types
 * @param payload
 */
export function* submitAllowanceType({ payload }) {
    try {
        yield put({ type: LOADING, payload: true });
        const companyId = company.getLastActiveCompanyId();

        const data = payload.map( ( item ) => ({
            ...item,
            company_id: companyId
        }) );

        const result = yield call( Fetch, `/philippine/company/${companyId}/allowance_type/bulk_create`, { method: 'POST', data });

        const allowanceTypes = [];
        result && result.data.forEach( ( item ) => {
            allowanceTypes.push({
                company_id: item.company_id,
                id: item.id,
                max_non_taxable: item.max_non_taxable,
                name: item.name,
                tax_option: item.tax_option
            });
        });

        yield call( notifyUser, 'success', 'New record saved!', '', 2000 );
        yield call( browserHistory.pushWithParam, '/company-settings/payroll/allowance-types', true, {
            allowanceTypes
        });
    } catch ( error ) {
        const title = error.response ? error.response.statusText : 'Error';
        const message = error.response ? error.response.data.message || `Request failed with statuscode ${error.response.status}` : error.statusText;
        yield call( notifyUser, 'error', title, message );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Display a notification to user
 */
export function* notifyUser( type = 'error', title, message = ' ', delayCount = 4000 ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( true, type, title, message )
    });

    yield call( delay, delayCount );
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * submitAllowanceType
 */
export function* watchForSubmitAllowanceType() {
    const watcher = yield takeLatest( SET_SUBMITTED, submitAllowanceType );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitAllowanceType,
    watchForReinitializePage,
    watchNotify
];
