import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    SET_NOTIFICATION,
    SUBMITTED
} from './constants';

const initialState = fromJS({
    loading: false,
    submitted: '',
    addNotification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * AllowanceType reducer
 *
 */
function allowanceTypeReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'addNotification', fromJS( action.payload ) );
        case SUBMITTED:
            return state.set( 'submitted', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default allowanceTypeReducer;

