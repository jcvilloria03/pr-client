/* eslint-disable no-constant-condition */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import isEqual from 'lodash/isEqual';
import startCase from 'lodash/startCase';

import A from 'components/A';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';

import Button from 'components/Button';
import Loader from 'components/Loader';
import SnackBar from 'components/SnackBar';
import Sidebar from 'components/Sidebar';
import Input from 'components/Input';
import SalSelect from 'components/Select';
import Table from 'components/Table';
import Modal from 'components/Modal/index';

import { browserHistory } from 'utils/BrowserHistory';
import { formatCurrency, stripNonDigit } from 'utils/functions';

import * as allowanceTypeAddAction from './actions';
import {
    checkTypeNameAvailability,
    getTaxOptions
} from '../View/actions';

import {
    makeSelectTaxOptions,
    makeSelectAllowanceTypes,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectIsNameAvailable,
    makeSelectViewNotification
} from './selectors';

import { Footer, Header, ModalBody, PageWrapper } from './styles';

/**
 *
 * allowanceType
 *
 */
export class AllowanceType extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        submitAllowanceType: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        viewNotification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool,
        getTaxOptions: React.PropTypes.func,
        taxOptions: React.PropTypes.array,
        checkTypeNameAvailability: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        let options = [];
        if ( props.taxOptions.length ) {
            options = props.taxOptions.map( ({ text, value }) => ({
                value,
                label: text,
                disabled: false
            }) );
        }

        this.state = {
            typeName: '',
            taxOption: '',
            maxNonTaxable: '',
            taxOptions: options,
            newTypes: [],
            notification: {
                message: '',
                show: false,
                title: '',
                type: ''
            },
            addNewTypeItemLoading: false
        };
        this.showModal = null;
        this.discardModal = null;
    }

    componentDidMount() {
        if ( !this.props.taxOptions.length ) {
            this.props.getTaxOptions();
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( !this.props.taxOptions.length && nextProps.taxOptions.length ) {
            const options = nextProps.taxOptions.map( ({ text, value }) => ({
                value,
                label: text,
                disabled: false
            }) );
            this.setState({ taxOptions: options });
        }

        if ( this.state.typeName && nextProps.isNameAvailable.available
            && isEqual( nextProps.isNameAvailable.name, this.state.typeName ) ) {
            this.addItem();
        }

        if ( !isEqual( nextProps.notification, this.props.notification ) ) {
            this.setNotification( nextProps.notification );
        }

        if ( !isEqual( nextProps.viewNotification, this.props.viewNotification ) ) {
            this.setNotification( nextProps.viewNotification );
        }
    }

    setNotification( notification ) {
        this.setState({ notification, addNewTypeItemLoading: false }, () => {
            setTimeout( () => {
                this.resetNotification();
            }, 5000 );
        });
    }

    addItem() {
        const newType = {
            max_non_taxable: this.state.maxNonTaxable,
            name: this.state.typeName,
            tax_option: this.state.taxOption
        };

        this.setState( ( prevState ) => ({ newTypes: [ ...prevState.newTypes, newType ]}), () => {
            this.setState({
                maxNonTaxable: '',
                typeName: '',
                taxOption: '',
                addNewTypeItemLoading: false
            });
        });
    }

    resetNotification() {
        this.setState({
            notification: {
                message: '',
                show: false,
                title: '',
                type: ''
            },
            addNewTypeItemLoading: false
        });
    }

    checkTypeName() {
        this.setState({ addNewTypeItemLoading: true });
        const typeName = this.state.typeName;
        const nameExists = this.state.newTypes.some( ( item ) => item.name.toLowerCase() === typeName.toLowerCase() );

        if ( nameExists ) {
            this.setNotification({
                title: 'Invalid Type Name',
                message: 'Allowance type name is already existing.',
                show: true,
                type: 'error'
            });
        } else {
            this.props.checkTypeNameAvailability({
                name: this.state.typeName
            });
        }
    }

    formatTaxOptionName( value ) {
        const name = value.replaceAll( '_', ' ' );
        return startCase( name );
    }

    removeItem = ( name ) => {
        this.setState( ( prevState ) => ({
            newTypes: prevState.newTypes.filter( ( item ) => item.name !== name )
        }) );
    }

    editItem = ( name ) => {
        const itemType = this.state.newTypes.find( ( item ) => item.name === name );

        this.setState( () => ({
            maxNonTaxable: itemType.max_non_taxable,
            typeName: itemType.name,
            taxOption: itemType.tax_option
        }), () => {
            this.setState( ( prevState ) => ({
                newTypes: prevState.newTypes.filter( ( item ) => item.name !== name )
            }) );
        });
    }

    /**
     * Render new allowance types
     */
    renderNewTypesTable() {
        const tableColumns = [
            {
                header: 'Allowance Type Name ',
                accessor: 'name',
                sortable: false,
                style: { whiteSpace: 'inherit', wordBreak: 'break-word' },
                render: ({ row }) => <div>{ row.name }</div>
            },
            {
                header: 'Tax Option',
                accessor: 'tax_option',
                sortable: false,
                style: { whiteSpace: 'inherit', wordBreak: 'break-word' },
                render: ({ row }) => <div>{ this.formatTaxOptionName( row.tax_option ) }</div>
            },
            {
                header: 'Partially Non Taxable Ceiling Amount (Per Annum)',
                accessor: 'max_non_taxable',
                sortable: false,
                width: 400,
                render: ({ row }) => (
                    <div>
                        { row.max_non_taxable ? formatCurrency( row.max_non_taxable ) : '' }
                    </div>
                )
            },
            {
                header: '',
                accessor: 'actions',
                width: 200,
                sortable: false,
                style: { justifyContent: 'center', whiteSpace: 'inherit', wordBreak: 'break-word' },
                render: ({ row }) => (
                    <div>
                        <Button
                            label={ <span>Remove</span> }
                            alt
                            disabled={ this.props.loading }
                            type="danger"
                            size="small"
                            onClick={ () => {
                                this.removeItem( row.name );
                            } }
                        />
                        <Button
                            label={ <span>Edit</span> }
                            alt
                            disabled={ this.props.loading }
                            type="action"
                            size="small"
                            onClick={ () => {
                                this.editItem( row.name );
                            } }
                        />
                    </div>
                )
            }
        ];

        const { newTypes } = this.state;

        return (
            <div>
                <Table
                    data={ newTypes }
                    columns={ tableColumns }
                    noDataText="No Allowance Type Data Added"
                    className="add-table"
                    ref={ ( ref ) => { this.newTypesTable = ref; } }
                />
            </div>
        );
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { notification } = this.state;
        const { loading } = this.props;

        let disableAddButton = false;

        if ( !this.state.typeName || !this.state.taxOption || this.state.addNewTypeItemLoading || loading
            || ( this.state.taxOption === 'partially_non_taxable' && !this.state.maxNonTaxable ) ) {
            disableAddButton = true;
        }

        return (
            <div>
                <Helmet
                    title="Add Allowance Types"
                    meta={ [
                        { name: 'description', content: 'Description of Add Allowance Types' }
                    ] }
                />
                <SnackBar
                    message={ notification && notification.message }
                    title={ notification && notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification && notification.show }
                    delay={ 5000 }
                    type={ notification && notification.type }
                />
                <Header>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/payroll/allowance-types', true );
                                } }
                            >
                                &#8592; Back to Allowance Types
                            </A>
                        </Container>
                    </div>
                </Header>
                <PageWrapper>
                    <Sidebar
                        items={ sidebarLinks }
                    />
                    <br />
                    <div className="main_section">
                        <div className="heading">
                            <h1>Add Allowance Type</h1>
                            <p>View and update allowance types. You can add, edit, or delete allowance types.</p>
                        </div>
                        <div className="add-inputs-wrapper">
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="row">
                                        <div className="col-xs-3">
                                            <Input
                                                id="typeName"
                                                label="Allowance Type Name"
                                                required
                                                onChange={ ( value ) => {
                                                    this.setState({ typeName: value });
                                                } }
                                                value={ this.state.typeName }
                                                ref={ ( ref ) => { this.typeName = ref; } }
                                            />
                                        </div>
                                        <div className="col-xs-3 tax-option-wrapper">
                                            <SalSelect
                                                id="taxOption"
                                                label="Tax Option"
                                                required
                                                data={ this.state.taxOptions }
                                                placeholder="Select tax option"
                                                value={ this.state.taxOption }
                                                onChange={ ( item ) => {
                                                    this.setState({ taxOption: item.value });
                                                    if ( item.value !== 'partially_non_taxable' ) {
                                                        this.maxNonTaxable.setState({ error: false, errorMessage: '' });
                                                    }
                                                } }
                                                ref={ ( ref ) => { this.taxOption = ref; } }
                                            />
                                        </div>
                                        <div className="col-xs-4">
                                            <Input
                                                id="maxNonTaxable"
                                                type="number"
                                                minNumber={ 0 }
                                                label="Partially Non Taxable Ceiling Amount (Per Annum)"
                                                disabled={ this.state.taxOption !== 'partially_non_taxable' }
                                                required={ this.state.taxOption === 'partially_non_taxable' }
                                                onChange={ ( value ) => {
                                                    this.setState({ maxNonTaxable: stripNonDigit( value ) });
                                                } }
                                                value={ this.state.maxNonTaxable }
                                                ref={ ( ref ) => { this.maxNonTaxable = ref; } }
                                            />
                                        </div>
                                        <div className="col-xs-2">
                                            <Button
                                                label={ this.state.addNewTypeItemLoading ? <Loader /> : 'Add' }
                                                type="action"
                                                alt
                                                className="block"
                                                disabled={ disableAddButton }
                                                onClick={ () => {
                                                    this.checkTypeName();
                                                } }
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div className="row">
                            <div className="col-xs-12 add-table-wrapper">
                                { this.renderNewTypesTable() }
                            </div>
                        </div>
                    </div>
                </PageWrapper>
                <Footer>
                    <div className="from_footer">
                        <Container>
                            <div className="footer_button">
                                <Button
                                    label="Cancel"
                                    type="neutral"
                                    size="large"
                                    onClick={ () => {
                                        const withChanges = ( this.state.newTypes && this.state.newTypes.length );
                                        if ( withChanges ) {
                                            this.discardModal.toggle();
                                        } else {
                                            browserHistory.push( '/company-settings/payroll/allowance-types', true );
                                        }
                                    } }
                                />
                                <Button
                                    label={ loading ? <Loader /> : 'Submit' }
                                    type="action"
                                    size="large"
                                    disabled={ !( this.state.newTypes && this.state.newTypes.length ) || this.state.addNewTypeItemLoading || loading }
                                    onClick={ () => {
                                        this.props.submitAllowanceType( this.state.newTypes );
                                    } }
                                />
                            </div>
                        </Container>
                    </div>
                </Footer>
                <Modal
                    title="Discard Changes"
                    body={
                        <ModalBody>
                        Clicking Discard will not save all the changes you made on this page. Are you sure you want to proceed?
                        </ModalBody>
                    }
                    buttons={ [
                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.discardModal.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'danger',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/company-settings/payroll/allowance-types', true );
                                this.discardModal.toggle();
                            }
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                />
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    allowanceType: makeSelectAllowanceTypes(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    taxOptions: makeSelectTaxOptions(),
    isNameAvailable: makeSelectIsNameAvailable(),
    viewNotification: makeSelectViewNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        Object.assign(
            {},
            allowanceTypeAddAction,
            {
                checkTypeNameAvailability,
                getTaxOptions
            }
        ),
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AllowanceType );
