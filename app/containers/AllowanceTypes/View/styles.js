import styled from 'styled-components';

export const PageWrapper = styled.div`
    height:100%;
    overflow-y: auto;

    button {
        border-radius: 2px !important;
    }

    .container{
        height: calc(100vh - 120px);
        .loader{
            margin-top: 150px;
        }
    }

    .content {
        margin-top: 110px;
        padding-bottom: 100px;

        .selectPage{
            p{
                text-align: end;
                background-color: #e5f6fc;
                margin: 0;
                padding: 12px 10px;
                font-size: 14px;
                font-weight: 600;
                a{
                    color: #00a5e5;
                    padding: 0;
                }
            }
        }

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 600;
                margin-bottom: 5px;
            }

            p {
                text-align: center;
                max-width: 800px;
                font-size:14px;
                margin-bottom: 0px;
            }
        }

        .main {
            .btn {
                min-width: 140px;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .selected {
            border-left-color: #00a5e5;
            background-color: #e5f6fc;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
            justify-content: space-between;

            .title-content {
                display: flex;
                align-items: center;
                p,.dropdown span,span{
                    font-size: 14px;
                }
            }

            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 700;
                font-size: 18px;
            }

            .search-wrapper {
                flex-grow: 1;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 2px;
                    height: 40px;

                    input {
                        border: none;
                        height: 40px;
                    }
                }

                p {
                    display: none;
                }

                .input-group {
                    background-color: transparent;
                }

                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                    padding-bottom: 30px;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }
        }
    }

    .hide {
        display: none;
    }

    .ReactTable {
        .rt-table {
            overflow: visible !important;
        }

        .rt-td {
            overflow: visible !important;

            .row-input {
                width: 100%;
            }

            .row-input input.error {
                margin-top: 25px;
            }
        }

        .Select-control{
            .Select-menu-outer{
                z-index: 9 !important;
            }
        }
    }
`;
export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    height: 100%;
    justify-content: center;
    padding: 140px 0;
`;
