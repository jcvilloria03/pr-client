/* eslint-disable no-confusing-arrow */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-param-reassign */

import { get } from 'lodash';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { cancel, put, call, take, select } from 'redux-saga/effects';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { RECORD_DELETE_MESSAGE, RECORD_UPDATED_MESSAGE } from 'utils/constants';
import { formatFeedbackMessage } from 'utils/functions';
import { resetStore } from '../../App/sagas';
import {
    DELETE_ALLOWANCE_TYPE,
    UPDATE_ALLOWANCE_TYPE,
    GET_ALLOWANCE_TYPE_LIST,
    LOADING,
    NOTIFICATION,
    REINITIALIZE_PAGE,
    SET_ALLOWANCE_TYPE_LIST,
    SET_NOTIFICATION,
    GET_TAX_OPTIONS,
    SET_TAX_OPTIONS,
    SET_PAGINATION,
    SET_IS_NAME_AVAILABLE,
    CHECK_TYPE_NAME_AVAILABILITY,
    ACTION_LOADING
} from './constants';

export const selectAllowanceTypeList = ( state ) => state.get( 'AllowanceTypes' ).get( 'allowanceTypes' ).toJS();

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Allowance Type fetch data
 */
export function* getAllowanceTypes() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const allowanceTypes = yield call( Fetch, `/company/${companyId}/other_income_types/allowance_type?exclude_trashed=1`, { method: 'GET' });

        let allowanceTypesData = [];
        if ( allowanceTypes && allowanceTypes.data ) {
            allowanceTypesData = allowanceTypes.data.sort( ( a, b ) => b.id - a.id );
        }

        yield put({ type: SET_ALLOWANCE_TYPE_LIST, payload: allowanceTypesData });

        const totalItems = allowanceTypes && allowanceTypes.data.length;
        const limitPerPage = 10;
        const totalPages = Math.ceil( totalItems / limitPerPage );

        const paginationData = {
            total: totalItems,
            current_page: 1,
            last_page: totalPages,
            per_page: limitPerPage
        };

        yield put({
            type: SET_PAGINATION,
            payload: paginationData
        });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Allowance Type fetch Tax Options
 */
export function* getTaxOptions() {
    try {
        const taxOptions = yield call( Fetch, '/other_income_type/tax_option_choices/allowance_type', { method: 'GET' });
        yield put({ type: SET_TAX_OPTIONS, payload: taxOptions });
    } catch ( error ) {
        yield put({ type: SET_TAX_OPTIONS, payload: []});
    }
}

/**
 * Check if name is available
 */
export function* checkTypeNameAvailability({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();
        const data = {
            name: payload.name
        };

        const result = yield call( Fetch, `/philippine/company/${companyId}/allowance_type/is_name_available`, { method: 'POST', data });

        yield put({ type: SET_IS_NAME_AVAILABLE, payload: { name: payload.name, available: result.available }});

        if ( result.available === false ) {
            yield call( notifyUser, {
                show: true,
                title: 'Invalid Type Name',
                message: 'Allowance type name is already taken.',
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message || `Request failed with statuscode ${error.response.status}` : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Delete Allowance Type
 */
export function* deleteAllowanceType({ payload }) {
    try {
        yield put({ type: ACTION_LOADING, payload: true });
        const isAvailable = yield call( Fetch, `/company/${payload.company_id}/other_income_type/is_delete_available`, { method: 'POST', data: payload });

        if ( isAvailable && isAvailable.available === true ) {
            // preparation for backend delete
            const formData = new FormData();

            formData.append( '_method', 'DELETE' );
            formData.append( 'company_id', payload.company_id );
            payload.ids.forEach( ( id ) => {
                formData.append( 'ids[]', id );
            });

            yield call( Fetch, `/company/${payload.company_id}/other_income_type`, {
                method: 'POST',
                data: formData
            });

            // remove deleted records from current allowance type list
            const allowanceTypes = yield select( selectAllowanceTypeList );
            const newAllowanceTypes = allowanceTypes.filter( ( item ) => !payload.ids.some( ( id ) => item.id === id ) );

            yield put({ type: SET_ALLOWANCE_TYPE_LIST, payload: newAllowanceTypes });

            // set pagination data
            const totalItems = newAllowanceTypes && newAllowanceTypes.length;
            const limitPerPage = 10;
            const totalPages = Math.ceil( totalItems / limitPerPage );

            const paginationData = {
                total: totalItems,
                current_page: 1,
                last_page: totalPages,
                per_page: limitPerPage
            };

            yield put({
                type: SET_PAGINATION,
                payload: paginationData
            });

            yield call( showSuccessMessage, RECORD_DELETE_MESSAGE );
        }

        if ( isAvailable && isAvailable.available === false ) {
            yield put({ type: ACTION_LOADING, payload: false });
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: 'Selected allowance types are not available to delete.',
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
    } finally {
        yield put({ type: ACTION_LOADING, payload: false });
    }
}

/**
 * Update Allowance Type
 */
export function* updateAllowanceType({ payload }) {
    yield put({ type: ACTION_LOADING, payload: true });
    try {
        const companyId = company.getLastActiveCompanyId();
        const data = { name: payload.name, other_income_type_id: payload.id };
        const isAvailable = yield call( Fetch, `/philippine/company/${companyId}/allowance_type/is_name_available`, { method: 'POST', data });

        if ( isAvailable && isAvailable.available === true ) {
            const parameter = {
                ...payload,
                max_non_taxable: payload.tax_option !== 'partially_non_taxable' ? '' : payload.max_non_taxable,
                company_id: companyId

            };

            yield call( Fetch, `/philippine/allowance_type/${payload.id}`, {
                method: 'PATCH',
                data: parameter
            });

            // update edited record in the current list
            const allowanceTypes = yield select( selectAllowanceTypeList );
            const newAllowanceTypes = allowanceTypes.map( ( x ) => ( x.id === parameter.id ) ? parameter : x );

            yield put({ type: SET_ALLOWANCE_TYPE_LIST, payload: newAllowanceTypes });
            yield call( showSuccessMessage, RECORD_UPDATED_MESSAGE );
        }

        if ( isAvailable && isAvailable.available === false ) {
            yield put({ type: ACTION_LOADING, payload: false });
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: 'Allowance type name is already taken.',
                type: 'error'
            });
        }
    } catch ( error ) {
        const errorKeys = [ 'tax_option', 'name', 'max_non_taxable' ];

        let errorMessage = error.response ? error.response.data.message : error.response.statusText;
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            const key = Object.keys( error.response.data )[ 0 ];
            const message = error.response.data[ key ];

            if ( errorKeys.includes( key ) ) {
                errorMessage = `${key}: ${message}`;
            }
        }

        yield call( notifyUser, {
            show: true,
            title: 'Error',
            message: errorMessage,
            type: 'error'
        });
    } finally {
        yield put({ type: ACTION_LOADING, payload: false });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getAllowanceTypes );
}

 /**
 * Display a notification to user
 */
export function* showSuccessMessage( message ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( true, 'success', message )
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

/**
 * Watch NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch GET_ALLOWANCE_TYPE_LIST
 */
export function* watchForGetAllowanceType() {
    const watcher = yield takeEvery( GET_ALLOWANCE_TYPE_LIST, getAllowanceTypes );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch GET_TAX_OPTIONS
 */
export function* watchForGetTaxOptions() {
    const watcher = yield takeLatest( GET_TAX_OPTIONS, getTaxOptions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch CHECK_TYPE_NAME_AVAILABILITY
 */
export function* watchForCheckTypeNameAvailability() {
    const watcher = yield takeLatest( CHECK_TYPE_NAME_AVAILABILITY, checkTypeNameAvailability );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch DELETE_ALLOWANCE_TYPE
 */
export function* watchForDeleteAllowanceType() {
    const watcher = yield takeEvery( DELETE_ALLOWANCE_TYPE, deleteAllowanceType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch UPDATE_ALLOWANCE_TYPE
 */
export function* watchForUpdateAllowanceType() {
    const watcher = yield takeEvery( UPDATE_ALLOWANCE_TYPE, updateAllowanceType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetAllowanceType,
    watchForGetTaxOptions,
    watchForCheckTypeNameAvailability,
    watchForReinitializePage,
    watchForDeleteAllowanceType,
    watchForUpdateAllowanceType
];
