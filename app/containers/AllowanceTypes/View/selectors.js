import { createSelector } from 'reselect';

/**
 * Direct selector to the App state domain
 */
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Direct selector to the allowanceType state domain
 */
const selectAllowanceTypeDomain = () => ( state ) => state.get( 'AllowanceTypes' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
  selectAllowanceTypeDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectActionLoading = () => createSelector(
  selectAllowanceTypeDomain(),
  ( substate ) => substate.get( 'actionLoading' )
);

const makeSelectPagination = () => createSelector(
  selectAllowanceTypeDomain(),
  ( substate ) => substate.get( 'pagination' )
);

const makeSelectAllowanceTypeList = () => createSelector(
  selectAllowanceTypeDomain(),
  ( substate ) => substate.get( 'allowanceTypes' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectAllowanceTypeDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectTaxOptions = () => createSelector(
  selectAllowanceTypeDomain(),
  ( substate ) => substate.get( 'taxOptions' ).toJS()
);

const makeSelectCurrentCompanyState = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'currentCompany' ).toJS()
);

/**
 * Default selector used by AllowanceType
 */
const makeSelectAllowanceTypes = () => createSelector(
  selectAllowanceTypeDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectAllowanceTypes,
  selectAllowanceTypeDomain,
  makeSelectLoading,
  makeSelectPagination,
  makeSelectAllowanceTypeList,
  makeSelectNotification,
  makeSelectActionLoading,
  makeSelectTaxOptions,
  makeSelectCurrentCompanyState
};
