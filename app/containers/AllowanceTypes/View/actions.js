import {
    GET_ALLOWANCE_TYPE_LIST,
    DELETE_ALLOWANCE_TYPE,
    UPDATE_ALLOWANCE_TYPE,
    GET_TAX_OPTIONS,
    DEFAULT_ACTION,
    CHECK_TYPE_NAME_AVAILABILITY,
    SET_ALLOWANCE_TYPE_LIST,
    REINITIALIZE_PAGE
} from './constants';

/**
 *
 * AllowanceType actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * Get Allowance Types
 * @param {object} payload
 */
export function getAllowanceTypes() {
    return {
        type: GET_ALLOWANCE_TYPE_LIST
    };
}

/**
 * Delete Allowance Type
 * @param {object} payload
 */
export function deleteAllowanceType( payload ) {
    return {
        type: DELETE_ALLOWANCE_TYPE,
        payload
    };
}

/**
 * Update Allowance Type
 * @param {object} payload
 */
export function updateAllowanceType( payload ) {
    return {
        type: UPDATE_ALLOWANCE_TYPE,
        payload
    };
}

/**
 * Get Allowance Type Tax Options
 * @param {object} payload
 */
export function getTaxOptions() {
    return {
        type: GET_TAX_OPTIONS
    };
}

/**
 * Check given allowance type name availability
 * @param {object} payload
 */
export function checkTypeNameAvailability( payload ) {
    return {
        type: CHECK_TYPE_NAME_AVAILABILITY,
        payload
    };
}

/**
 * Set Allowance Types
 * @param {object} payload
 */
export function setAllowanceTypes( payload ) {
    return {
        type: SET_ALLOWANCE_TYPE_LIST,
        payload
    };
}

/**
 * Reinitialize
 * @param {object} payload
 */
export function reinitializePage() {
    return {
        type: REINITIALIZE_PAGE
    };
}
