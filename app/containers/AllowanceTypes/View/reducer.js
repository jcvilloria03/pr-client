import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    SET_ALLOWANCE_TYPE_LIST,
    SET_NOTIFICATION,
    SET_PAGINATION,
    SET_TAX_OPTIONS,
    SET_IS_NAME_AVAILABLE,
    ACTION_LOADING
} from './constants';

const initialState = fromJS({
    allowanceTypes: [],
    taxOptions: [],
    loading: false,
    actionLoading: false,
    pagination: {
        total: 1,
        current_page: 1,
        last_page: 1,
        per_page: 10
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    isNameAvailable: {
        name: '',
        available: true
    }
});

/**
 *
 * AllowanceType reducer
 *
 */
function allowanceTypeReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case ACTION_LOADING:
            return state.set( 'actionLoading', action.payload );
        case SET_ALLOWANCE_TYPE_LIST:
            return state.set( 'allowanceTypes', fromJS( action.payload ) );
        case SET_TAX_OPTIONS:
            return state.set( 'taxOptions', fromJS( action.payload ) );
        case SET_IS_NAME_AVAILABLE:
            return state.set( 'isNameAvailable', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', action.payload );
        default:
            return state;
    }
}

export default allowanceTypeReducer;

