/*
 *
 * AllowanceType constants
 *
 */
const namespace = 'app/containers/AllowanceTypes';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const REINITIALIZE_PAGE = `${namespace}/REINITIALIZE_PAGE`;
export const GET_ALLOWANCE_TYPE_LIST = `${namespace}/GET_ALLOWANCE_TYPE_LIST`;
export const SET_ALLOWANCE_TYPE_LIST = `${namespace}/SET_ALLOWANCE_TYPE_LIST`;
export const SET_PAGINATION = `${namespace}/SET_PAGINATION`;
export const DELETE_ALLOWANCE_TYPE = `${namespace}/DELETE_ALLOWANCE_TYPE`;
export const UPDATE_ALLOWANCE_TYPE = `${namespace}/UPDATE_ALLOWANCE_TYPE`;

export const SET_TAX_OPTIONS = `${namespace}/SET_TAX_OPTIONS`;
export const GET_TAX_OPTIONS = `${namespace}/GET_TAX_OPTIONS`;

export const CHECK_TYPE_NAME_AVAILABILITY = `${namespace}/CHECK_TYPE_NAME_AVAILABILITY`;
export const SET_IS_NAME_AVAILABLE = `${namespace}/SET_IS_NAME_AVAILABLE`;
export const ACTION_LOADING = `${namespace}/ACTION_LOADING`;
