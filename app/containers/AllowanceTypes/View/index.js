/* eslint-disable no-restricted-syntax */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';
import startCase from 'lodash/startCase';
import isEqual from 'lodash/isEqual';
import get from 'lodash/get';
import clone from 'lodash/clone';

import SnackBar from 'components/SnackBar';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Modal from 'components/Modal';
import Table from 'components/Table';
import Icon from 'components/Icon';
import Input from 'components/Input';
import Loader from 'components/Loader';
import SalSelect from 'components/Select';
import FooterTablePagination from 'components/FooterTablePagination';
import { H2, H3 } from 'components/Typography';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';

import { formatDeleteLabel, formatPaginationLabel, formatCurrency, stripNonDigit } from 'utils/functions';
import { getIdsOfSelectedRows, isAnyRowSelected } from 'components/Table/helpers';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';
import * as allowanceTypeAction from './actions';
import {
    makeSelectAllowanceTypeList,
    makeSelectNotification,
    makeSelectLoading,
    makeSelectPagination,
    makeSelectTaxOptions,
    makeSelectActionLoading,
    makeSelectCurrentCompanyState } from './selectors';
import { LoadingStyles, PageWrapper } from './styles';

/**
 *
 * AllowanceType
 *
 */
export class AllowanceType extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes={
        getAllowanceTypes: React.PropTypes.func,
        allowanceTypes: React.PropTypes.array,
        loading: React.PropTypes.bool,
        actionLoading: React.PropTypes.bool,
        pagination: React.PropTypes.object,
        deleteAllowanceType: React.PropTypes.func,
        getTaxOptions: React.PropTypes.func,
        taxOptions: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        location: React.PropTypes.object,
        setAllowanceTypes: React.PropTypes.func,
        updateAllowanceType: React.PropTypes.func,
        currentCompany: React.PropTypes.string,
        reinitializePage: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        const displayedAllowanceTypes = this.getNewSetofAllowanceTypes();

        this.state = {
            permissions: {
                create: false,
                edit: true
            },
            label: 'Showing 0-0 of 0 entries',
            displayedData: displayedAllowanceTypes,
            selectionLabel: '',
            page: 0,
            flag: true,
            selectPage: false,
            pagination: this.setPagination( displayedAllowanceTypes ),
            editableCell: false,
            edited: null,
            taxOptions: []
        };
        this.confirmSaveModal = null;
        this.searchInput = null;

        this.handleSearch = this.handleSearch.bind( this );
        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
    }

    componentDidMount() {
        if ( !this.props.allowanceTypes.length ) {
            this.props.getAllowanceTypes();
        }

        if ( !this.props.taxOptions.length ) {
            this.props.getTaxOptions();
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.currentCompany, this.props.currentCompany ) ) {
            this.props.reinitializePage();
        }

        // Tax option will be loaded only once since this one is not always updated
        if ( !this.props.taxOptions.length && nextProps.taxOptions.length ) {
            this.setTaxOptions( nextProps.taxOptions );
        }

        // display handling for allowance type list first load
        if ( !this.props.allowanceTypes.length && !isEqual( nextProps.allowanceTypes.length, this.props.allowanceTypes.length ) ) {
            this.setState({ displayedData: nextProps.allowanceTypes });
            this.setTaxOptions( this.props.taxOptions );
        }

        // display handling for allowance type list after deleting an allowance type
        if ( this.props.allowanceTypes.length && !isEqual( nextProps.allowanceTypes.length, this.props.allowanceTypes.length ) ) {
            const displayedData = this.filteredDisplay( nextProps.allowanceTypes );
            this.setState({ displayedData });
            this.setTaxOptions( this.props.taxOptions );
        }

        // display handling for pagination update
        if ( !isEqual( nextProps.pagination, this.props.pagination ) ) {
            const { current_page: currentPage, per_page: perPage } = nextProps.pagination;

            const displayedData = this.filteredDisplay( this.props.allowanceTypes );

            const total = displayedData.length ? displayedData.length : 0;
            const lastPage = Math.ceil( total / perPage );
            const tablePageNumber = currentPage - 1;

            if ( this.allowanceTypesTable ) {
                // for delete action where table is already mounted
                this.allowanceTypesTable.tableComponent.setState({
                    page: tablePageNumber, pageSize: perPage, dataLength: displayedData.length
                }, () => {
                    this.setState({
                        pagination: {
                            per_page: perPage,
                            current_page: currentPage,
                            last_page: lastPage,
                            total: displayedData.length
                        },
                        page: tablePageNumber
                    });

                    this.handleTableChanges();
                });
            } else {
                // for first load where table is not yet mounted
                this.setState({
                    pagination: {
                        per_page: perPage,
                        current_page: currentPage,
                        last_page: lastPage,
                        total: displayedData.length
                    },
                    page: tablePageNumber
                }, () => {
                    this.allowanceTypesTable && this.allowanceTypesTable.tableComponent.setState({
                        page: tablePageNumber, pageSize: perPage, dataLength: displayedData.length
                    });
                    this.allowanceTypesTable && this.handleTableChanges();
                });
            }
        }

        // handling after allowance type update
        if ( this.props.allowanceTypes.length && !isEqual( nextProps.allowanceTypes, this.props.allowanceTypes ) ) {
            const displayedData = this.filteredDisplay( nextProps.allowanceTypes );
            this.setState({ displayedData }, () => {
                this.stopEditing();
            });
        }
    }

    onPageChange( page ) {
        const tablePageNumber = page > 0 ? ( page - 1 ) : this.state.page;

        this.allowanceTypesTable && this.allowanceTypesTable.tableComponent.setState({
            page: tablePageNumber
        }, () => {
            this.setState({
                pagination: {
                    ...this.state.pagination,
                    current_page: page
                },
                page: tablePageNumber
            });

            this.handleTableChanges();
        });
    }

    onPageSizeChange( pageSize ) {
        if ( !pageSize || pageSize === this.state.pagination.per_page ) return;

        const totalPages = Math.ceil( this.state.pagination.total / pageSize );
        // reset current pages to 1
        const currentPage = 1;
        const tablePageNumber = currentPage - 1;

        this.allowanceTypesTable && this.allowanceTypesTable.tableComponent.setState({
            pageSize, page: tablePageNumber
        }, () => {
            this.setState({
                pagination: {
                    ...this.state.pagination,
                    per_page: pageSize,
                    current_page: currentPage,
                    last_page: totalPages
                },
                page: tablePageNumber
            });

            this.handleTableChanges();
        });
    }

    setTaxOptions( taxOptions ) {
        const options = taxOptions.map( ({ text, value }) => ({
            value,
            label: text,
            disabled: false
        }) );
        this.setState({ taxOptions: options });
    }

    /**
     * Pagination handling after adding new types
     */
    setPagination( allowanceTypes ) {
        const currentPage = 1;
        const perPage = 10;
        let total = 1;
        let lastPage = 1;

        if ( allowanceTypes.length ) {
            total = allowanceTypes.length;
            lastPage = Math.ceil( allowanceTypes.length / perPage );
        }

        return {
            total,
            last_page: lastPage,
            per_page: perPage,
            current_page: currentPage
        };
    }

    /**
     * Display Allowance Types handling after adding new types
     */
    getNewSetofAllowanceTypes() {
        // manually add new items to avoid calling the get enpoint
        // get newly added passed items from location state, and iclude it to the current list
        const { location, allowanceTypes } = this.props;
        const newlyAddedAllowanceTypes = get( location.state, 'allowanceTypes', null );

        let newAllowanceTypes = allowanceTypes;
        if ( newlyAddedAllowanceTypes ) {
            const ids = new Set( allowanceTypes.map( ( d ) => d.id ) );
            newAllowanceTypes = [ ...allowanceTypes, ...newlyAddedAllowanceTypes.filter( ( d ) => !ids.has( d.id ) ) ];
            newAllowanceTypes = newAllowanceTypes.sort( ( a, b ) => b.id - a.id );

            // remove from state once added
            window.history.replaceState({}, document.title );

            this.props.setAllowanceTypes( newAllowanceTypes );
        }

        return newAllowanceTypes;
    }

    getTableColumns() {
        return [
            {
                id: 'name',
                header: 'Name',
                accessor: 'name',
                style: { whiteSpace: 'inherit', wordBreak: 'break-word' },
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) && this.state.editableCell === true ? (
                        <div className="row-input">
                            <Input
                                id="name"
                                type="text"
                                placeholder=""
                                required
                                value={ this.state.edited.name }
                                ref={ ( ref ) => { this.typeName = ref; } }
                                onChange={ ( value ) => {
                                    this.editedRowState( 'name', value );
                                } }
                            />
                        </div>
                    ) : (
                        <div> { row.name } </div>
                    )
                )
            },
            {
                id: 'tax_option',
                header: 'Tax Option',
                accessor: 'tax_option',
                style: { whiteSpace: 'inherit', wordBreak: 'break-word' },
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) && this.state.editableCell === true ? (
                        <div className="row-input">
                            <SalSelect
                                id="tax_option"
                                required
                                data={ this.state.taxOptions }
                                placeholder="Select a tax option"
                                value={ this.state.edited.tax_option }
                                onChange={ ( item ) => {
                                    if ( item.value !== 'partially_non_taxable' ) {
                                        this.maxNonTaxable.setState({ error: false, errorMessage: '', property: '' });
                                    }
                                    this.editedRowState( 'tax_option', item.value );
                                } }
                                ref={ ( ref ) => { this.taxOption = ref; } }
                            />
                        </div>
                    ) : (
                        <div> { row.tax_option ? this.formatTaxOptionName( row.tax_option ) : '' } </div>
                    )
                )
            },
            {
                id: 'max_non_taxable',
                header: 'Partially Non Taxable Ceiling Amount (Per Annum)',
                accessor: 'max_non_taxable',
                style: { whiteSpace: 'inherit', wordBreak: 'break-word' },
                width: 400,
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) && this.state.editableCell === true ? (
                        <div className="row-input">
                            <Input
                                id="max_non_taxable"
                                type="number"
                                minNumber={ 0 }
                                placeholder=""
                                disabled={ this.state.edited.tax_option !== 'partially_non_taxable' }
                                required={ this.state.edited.tax_option === 'partially_non_taxable' }
                                value={ this.state.edited.max_non_taxable }
                                ref={ ( ref ) => { this.maxNonTaxable = ref; } }
                                onChange={ ( value ) => {
                                    this.editedRowState( 'max_non_taxable', stripNonDigit( value ) );
                                } }
                            />
                        </div>
                    ) : (
                        <div> { row.max_non_taxable ? formatCurrency( row.max_non_taxable ) : '' } </div>
                    )
                )
            },
            {
                header: '',
                sortable: false,
                width: 200,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) ? (
                        <div>
                            <Button
                                label={ <span>Cancel</span> }
                                type="grey"
                                size="small"
                                onClick={ this.stopEditing }
                                disabled={ this.props.actionLoading }
                            />
                            <Button
                                label={ this.props.actionLoading ? <Loader /> : 'Save' }
                                type="action"
                                size="small"
                                disabled={ this.props.actionLoading || ( this.state.edited
                                    && ( !this.state.edited.name || !this.state.edited.tax_option
                                        || ( this.state.edited.tax_option === 'partially_non_taxable'
                                            && !this.state.edited.max_non_taxable ) ) ) }
                                onClick={ () => this.saveEdited() }
                            />
                        </div>
                    ) : (
                        row.name === 'De Minimis Benefits'
                            ? null
                            : <Button
                                label={ <span>Edit</span> }
                                type="grey"
                                size="small"
                                onClick={ () => {
                                    this.setState({ editableCell: false }, () => {
                                        this.setState({ editableCell: true }, () => {
                                            this.startEditingRow( row );
                                        });
                                    });
                                } }
                                disabled={ this.props.actionLoading }
                            />
                    )
                )
            }
        ];
    }

    saveEdited() {
        this.setState({ editableCell: false },
        () => {
            this.props.updateAllowanceType({
                id: this.state.edited.id,
                max_non_taxable: this.state.edited.max_non_taxable,
                name: this.state.edited.name,
                tax_option: this.state.edited.tax_option
            });
        });
    }

    /**
     * Stop editing row.
     */
    stopEditing = ( callback ) => {
        this.setState({
            edited: null
        }, callback );
    }

    /**
     * Start editing one row.
     * @param {Object} row
     */
    startEditingRow( row ) {
        this.setState({
            edited: clone( row )
        });
    }

    editedRowState( property, value ) {
        this.typeName.setState({ property: value });
        this.taxOption.setState({ property: value });
        this.maxNonTaxable.setState({ property: stripNonDigit( value ) });

        let newValue = value;
        if ( property === 'max_non_taxable' ) {
            newValue = !value ? '' : value;
        }

        this.setState({
            edited: {
                ...this.state.edited,
                [ property ]: newValue
            }
        });
    }

    /**
     * Check if given row is currently being edited.
     * @param {Object} row
     * @return {Boolean}
     */
    isCurrentlyEdited( row ) {
        return this.state.edited && this.state.edited.id === row.id;
    }

    filteredDisplay( allowanceTypes ) {
        let searchQuery = null;
        let dataToDisplay = allowanceTypes;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery ) {
            dataToDisplay = dataToDisplay.filter( ( allowanceType ) => ( allowanceType && allowanceType.name.toLowerCase().includes( searchQuery ) ) );
        }

        return dataToDisplay;
    }

    handleSearch = ( allowanceTypes = this.props.allowanceTypes ) => {
        const displayedData = this.filteredDisplay( allowanceTypes );
        const currentPage = 1;
        const totalAllowanceTypes = displayedData.length;
        const lastPage = Math.ceil( totalAllowanceTypes / this.state.pagination.per_page );
        const tablePageNumber = currentPage - 1;

        this.allowanceTypesTable && this.allowanceTypesTable.tableComponent.setState({
            page: tablePageNumber, dataLength: displayedData.length
        }, () => {
            this.setState({
                displayedData,
                pagination: {
                    ...this.state.pagination,
                    current_page: currentPage,
                    last_page: lastPage,
                    total: displayedData.length
                },
                page: tablePageNumber
            }, () => {
                this.handleTableChanges();
            });
        });
    }

    formatTaxOptionName( value ) {
        const name = value.replaceAll( '_', ' ' );
        return startCase( name );
    }

    modalDeleteClick=() => {
        const ids = getIdsOfSelectedRows( this.allowanceTypesTable );
        const companyId = company.getLastActiveCompanyId();
        const payload = ({ company_id: companyId, ids });

        this.props.deleteAllowanceType( payload );
        this.confirmSaveModal.toggle();
    }

    checkBoxchanged=() => {
        this.allowanceTypesTable.state.selected = this.allowanceTypesTable.state.selected.map( ( value ) => {
            this.setState({ flag: false });
            return ( value || !value );
        });
        const selectionLength = this.allowanceTypesTable.state.selected.length;
        this.setState({ selectPage: false, selectionLabel: formatDeleteLabel( selectionLength ) });
    }

    /**
    * Handles table changes
    */
    handleTableChanges = ( tableProps = this.allowanceTypesTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            selection_Label: formatDeleteLabel()
        });

        if ( this.allowanceTypesTable ) {
            this.setState({ selectPage: this.pageSelectOne( this.allowanceTypesTable.state.pageSelected ) || false });
        }
    }

    pageSelectOne=( pageObj ) => {
        let isTrue;
        for ( const key in pageObj ) {
            if ( ( this.allowanceTypesTable.tableComponent.state.page === +key ) && ( pageObj[ key ] === true ) ) {
                isTrue = pageObj[ key ];
            }
        }
        return isTrue;
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { loading, allowanceTypes } = this.props;
        const { selectionLabel } = this.state;
        return (
            <div style={ { height: '100vh' } }>
                <Helmet
                    title="Allowance Types"
                    meta={ [
                        { name: 'description', content: 'Description of Allowance Types' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Confirm Your Action"
                    body={
                        <div>
                            <p>Are you sure you want to delete the selected records?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonCancel',
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.confirmSaveModal.toggle()
                        },
                        {
                            id: 'buttonProceed',
                            type: 'danger',
                            label: 'Yes',
                            onClick: () => this.modalDeleteClick()
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.confirmSaveModal = ref; } }
                />
                <PageWrapper>
                    <Container>
                        <div>
                            {this.props.loading === true &&
                            <div className="loader" style={ { display: loading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Loading Company Allowance Types.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                            }
                            {this.props.loading === false &&
                            <div className="content" style={ { display: loading ? 'none' : '' } }>
                                <div className="heading">
                                    <H3>Allowance Types</H3>
                                </div>
                                <div className="title">
                                    <div className="search-wrapper">
                                        <Input
                                            className="search"
                                            id="search"
                                            ref={ ( ref ) => { this.searchInput = ref; } }
                                            onChange={ () => { this.handleSearch(); } }
                                            addon={ {
                                                content: <Icon name="search" />,
                                                placement: 'left'
                                            } }
                                        />
                                    </div>
                                    <span className="title-content">
                                        {isAnyRowSelected( this.allowanceTypesTable ) ? <p className="mb-0 mr-1">{selectionLabel}</p> :
                                        <div>
                                            <Button
                                                label="Add Allowances"
                                                type="action"
                                                alt
                                                onClick={ () => browserHistory.push( '/payroll/allowances/add', true ) }
                                            />
                                            <Button
                                                label="Add Allowance Type"
                                                type="action"
                                                onClick={ () => browserHistory.push( '/company-settings/payroll/allowance-types/add', true ) }
                                            />
                                        </div>
                                        }
                                        { isAnyRowSelected( this.allowanceTypesTable ) && (
                                            <Button
                                                alt
                                                label={ this.props.actionLoading ? <Loader /> : 'Delete' }
                                                type="danger"
                                                disabled={ this.props.actionLoading }
                                                onClick={ () => { this.confirmSaveModal.toggle(); } }
                                            />
                                        ) }
                                    </span>
                                </div>
                                <Table
                                    data={ this.state.displayedData }
                                    columns={ this.getTableColumns() }
                                    selectable
                                    loading={ this.props.loading }
                                    onDataChange={ this.handleTableChanges }
                                    ref={ ( ref ) => { this.allowanceTypesTable = ref; } }
                                    onSelectionChange={ ({ selected }) => {
                                        const selectionLength = selected.filter( ( row ) => row ).length;
                                        const isSelected = allowanceTypes.length === selectionLength ? 'true' : 'false';
                                        this.setState({
                                            totalSelected: selectionLength,
                                            selectPage: this.pageSelectOne( this.allowanceTypesTable.state.pageSelected ),
                                            flag: isSelected,
                                            selectionLabel: formatDeleteLabel( selectionLength )
                                        });
                                    } }
                                    page={ this.state.page }
                                    pageSize={ this.state.pagination.per_page }
                                />
                            </div>
                            }
                        </div>
                    </Container>
                </PageWrapper>

                <div
                    className="loader"
                    style={ { display: this.props.loading ? 'none' : 'block', position: 'fixed', bottom: '0', background: '#fff' } }
                >
                    <FooterTablePagination
                        page={ this.state.pagination.current_page }
                        pageSize={ this.state.pagination.per_page }
                        pagination={ this.state.pagination }
                        onPageChange={ this.onPageChange }
                        onPageSizeChange={ this.onPageSizeChange }
                        paginationLabel={ this.state.label }
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    allowanceTypes: makeSelectAllowanceTypeList(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    pagination: makeSelectPagination(),
    taxOptions: makeSelectTaxOptions(),
    actionLoading: makeSelectActionLoading(),
    currentCompany: makeSelectCurrentCompanyState()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        allowanceTypeAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AllowanceType );
