import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../../components/A';
import Modal from '../../../../components/Modal';
import Table from '../../../../components/Table';
import Button from '../../../../components/Button';
import Clipboard from '../../../../components/Clipboard';
import FileInput from '../../../../components/FileInput';
import { H4, P } from '../../../../components/Typography';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { formatPaginationLabel, formatCurrency, formatCurrencyToDecimalNotation, formatDate } from '../../../../utils/functions';
import { DATE_FORMATS } from '../../../../utils/constants';

import { BASE_PATH_NAME } from '../../../../constants';

import * as actions from '../actions';

import {
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectLoansPreview,
    makeSelectPrepopulatedEmployeeId,
    makeSelectSaving
} from '../selectors';

import { MainWrapper, StyledLoader, ModalWrapper } from './styles';

/**
 * Loans Batch Upload Component
 */
class BatchUpload extends React.Component {
    static propTypes = {
        batchUploadJobId: React.PropTypes.string,
        uploadLoans: React.PropTypes.func,
        batchUploadStatus: React.PropTypes.string,
        batchUploadErrors: React.PropTypes.oneOfType([
            React.PropTypes.object,
            React.PropTypes.array
        ]),
        setPrepopulatedEmployeeId: React.PropTypes.func,
        prepopulatedEmployeeId: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.number
        ]),
        loansPreview: React.PropTypes.shape({
            status: React.PropTypes.string,
            data: React.PropTypes.array
        }),
        saving: React.PropTypes.shape({
            status: React.PropTypes.string,
            errors: React.PropTypes.object
        }),
        saveLoans: React.PropTypes.func
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            files: [],
            status: null,
            errors: {},
            clipboard: '',
            submit: false,
            loansTableLabel: 'Showing 0-0 of 0 entries',
            errorsTableLabel: 'Showing 0-0 of 0 entries',
            loansPreview: {
                data: [],
                status: 'ready'
            },
            savingStatus: '',
            savingErrors: {}
        };
    }

    componentWillMount() {
        const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddLoan' ) );
        this.props.setPrepopulatedEmployeeId( prepopulatedEmployee ? prepopulatedEmployee.id : '' );
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.batchUploadStatus !== this.props.batchUploadStatus && this.setState({
            status: nextProps.batchUploadStatus
        }, () => {
            this.validateButton.setState({
                disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.batchUploadStatus )
            });
        });

        nextProps.batchUploadErrors !== this.props.batchUploadErrors && this.setState({
            errors: nextProps.batchUploadErrors
        }, () => {
            let clipboard = '';
            const rows = Object.keys( nextProps.batchUploadErrors );

            if ( rows.length > 0 ) {
                rows.forEach( ( row ) => {
                    Object.values( nextProps.batchUploadErrors[ row ]).forEach( ( value ) => {
                        clipboard += `Row ${row}, ${value}\n`;
                    });
                });
            }

            this.handleErrorsTableChanges();
            this.setState({ clipboard });
        });

        nextProps.loansPreview.data !== this.props.loansPreview.data && this.setState({
            loansPreview: { ...this.state.loansPreview, data: nextProps.loansPreview.data }
        }, () => {
            this.handleLoansTableChanges();
        });

        nextProps.loansPreview.status !== this.props.loansPreview.status && this.setState({
            loansPreview: { ...this.state.loansPreview, status: nextProps.loansPreview.status }
        });

        nextProps.saving.status !== this.props.saving.status && this.setState({
            savingStatus: nextProps.saving.status
        }, () => {
            this.submitButton.setState({ disabled: nextProps.saving.status !== 'save_failed' });
        });
    }

    formatToCurrency = ( value ) => (
        value ? `P ${formatCurrency( value )}` : 'P 0.00'
    );

    handleErrorsTableChanges = () => {
        if ( !this.errorsTable ) {
            return;
        }

        this.setState({
            errorsTableLabel: formatPaginationLabel( this.errorsTable.tableComponent.state )
        });
    };

    handleLoansTableChanges = () => {
        if ( !this.loansTable ) {
            return;
        }

        this.setState({
            loansTableLabel: formatPaginationLabel( this.loansTable.tableComponent.state )
        });
    };

    renderModalBody = () => (
        <ModalWrapper>
            <div>Select the template for which loan type you want to upload</div>
            <div className="modal-buttons">
                <A
                    onClick={ () => { this.modal.toggle(); } }
                    href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/guides/user_defined_loans.csv"
                    download
                >
                    User-defined Loans
                </A>
                <A
                    onClick={ () => { this.modal.toggle(); } }
                    href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/guides/system_defined_loans.csv"
                    download
                >
                    Government Loans
                </A>
            </div>
        </ModalWrapper>
    )

    renderLoansSection = () => {
        const tableColumns = [
            {
                header: 'employee_id',
                accessor: 'employee_id',
                show: false
            },
            {
                header: 'Employee Name',
                accessor: 'employee_name',
                minWidth: 200,
                render: ({ row }) => <div>{row.employee_name}</div>
            },
            {
                header: 'Loan Type',
                accessor: 'loan_type',
                minWidth: 150,
                render: ({ row }) => <div>{row.loan_type}</div>
            },
            {
                header: 'Subtype',
                accessor: 'subtype',
                minWidth: 150,
                render: ({ row }) => <div>{row.subtype}</div>
            },
            {
                header: 'Payment Scheme',
                accessor: 'payment_scheme',
                minWidth: 200,
                render: ({ row }) => <div>{row.payment_scheme}</div>
            },
            {
                header: 'Date Approved',
                accessor: 'created_date',
                minWidth: 200,
                render: ({ row }) => <div>{row.created_date}</div>
            },
            {
                header: 'Salary Deduction Starts On',
                accessor: 'payment_start_date',
                minWidth: 200,
                render: ({ row }) => <div>{row.payment_start_date}</div>
            },
            {
                header: 'Total Amount',
                accessor: 'total_amount',
                minWidth: 200,
                render: ({ row }) => (
                    <div>{this.formatToCurrency( row.total_amount )}</div>
                )
            },
            {
                header: 'Monthly Amortization',
                accessor: 'monthly_amortization',
                minWidth: 200,
                render: ({ row }) => (
                    <div>{this.formatToCurrency( row.monthly_amortization )}</div>
                )
            }
        ];

        const dataForDisplay = this.state.loansPreview.data.map( ( data ) => ({
            ...data,
            payment_scheme: data.payment_scheme,
            created_date: formatDate( data.created_date, DATE_FORMATS.DISPLAY ),
            payment_start_date: formatDate( data.payment_start_date, DATE_FORMATS.DISPLAY ),
            total_amount: parseFloat( formatCurrencyToDecimalNotation( data.total_amount ), 10 ),
            monthly_amortization: parseFloat( formatCurrencyToDecimalNotation( data.monthly_amortization ), 10 )
        }) );

        return this.state.loansPreview.data.length === 0
            ? null
            : (
                <div>
                    <H4>Employee Loans List:</H4>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.loansTableLabel }
                        </span>
                    </div>
                    <Table
                        columns={ tableColumns }
                        data={ dataForDisplay }
                        pagination
                        ref={ ( ref ) => { this.loansTable = ref; } }
                        onDataChange={ this.handleLoansTableChanges }
                    />
                </div>
            );
    };

    renderErrorsSection = () => {
        const errorDisplay = [];
        const errorList = this.state.errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'Row Number',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'Error Type',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            const keys = Object.keys( errorList );

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row {key}</H4>,
                    error: <ul>{Object.values( errorList[ key ]).map( ( value ) => <li key={ value } style={ { margin: '0' } } >{value}</li> )}</ul>
                });
            });

            return (
                <div className="errors">
                    <H4>Data field errors</H4>
                    <p>There seems to be an error in the file you uploaded. Review the list below, correct the errors and upload the file again.</p>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.errorsTableLabel }
                        </span>
                        <Clipboard value={ this.state.clipboard } />
                    </div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                        ref={ ( ref ) => { this.errorsTable = ref; } }
                        onDataChange={ this.handleErrorsTableChanges }
                    />
                </div>
            );
        }

        return null;
    }

    render() {
        return (
            <MainWrapper>
                <Container>
                    <p className="sl-u-gap-top--lg sl-u-gap-bottom--lg">
                        Follow the steps below for adding loans via batch upload. A template will be available for you to fill out.
                    </p>
                    <Modal
                        ref={ ( ref ) => { this.modal = ref; } }
                        title="Download Template"
                        body={ this.renderModalBody() }
                        buttons={ [] }
                        size="sm"
                    />

                    <div className="steps">
                        <div className="step">
                            <div className="template">
                                <H4>Step 1:</H4>
                                <P>Download and fill-out the Employee Payroll Information Template.</P>
                                <P><A target="_blank" href={ `${BASE_PATH_NAME}/guides/loans/batch-upload` }>Click here to view the upload guide.</A></P>
                                <Button
                                    className="sl-c-btn--wide"
                                    label="Download Template"
                                    type="neutral"
                                    onClick={ () => { this.modal.toggle(); } }
                                    download
                                />
                            </div>
                        </div>
                        <div className="step">
                            <div className="upload">
                                <H4>Step 2:</H4>
                                <P>After completely filling out the template, choose and upload it here.</P>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'none' : 'block' } } >
                                    <FileInput
                                        accept=".csv"
                                        onDrop={ ( files ) => {
                                            const { acceptedFiles } = files;

                                            this.setState({
                                                files: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null,
                                                errors: {},
                                                status: null
                                            }, () => {
                                                this.validateButton.setState({ disabled: acceptedFiles.length <= 0 });
                                            });
                                        } }
                                        ref={ ( ref ) => { this.fileInput = ref; } }
                                    />
                                </div>
                                <div style={ { display: this.state.status === 'validated' ? 'block' : 'none' } }>
                                    <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                                </div>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'validated' ? 'none' : 'block' } } >
                                    <Button
                                        label={
                                            this.state.submit ? (
                                                <StyledLoader className="animation">
                                                    Uploading <div className="anim3"></div>
                                                </StyledLoader>
                                            ) : (
                                                'Upload'
                                            )
                                        }
                                        disabled={ this.state.files ? this.state.files.length <= 0 : true }
                                        type="neutral"
                                        ref={ ( ref ) => { this.validateButton = ref; } }
                                        onClick={ () => {
                                            this.setState({
                                                errors: {},
                                                status: null,
                                                submit: true
                                            }, () => {
                                                this.validateButton.setState({ disabled: true }, () => {
                                                    this.props.uploadLoans({ file: this.state.files });

                                                    setTimeout( () => {
                                                        this.validateButton.setState({ disabled: false });
                                                        this.setState({ submit: false });
                                                    }, 5000 );
                                                });
                                            });
                                        } }
                                    />
                                </div>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'block' : 'none' } } >
                                    <StyledLoader className="animation">
                                        <H4 style={ { margin: '0' } }>{ this.state.status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                    </StyledLoader>
                                </div>
                            </div>
                        </div>
                    </div>
                    { this.renderLoansSection() }
                    { this.renderErrorsSection() }
                </Container>

                <div className="foot">
                    <Container>
                        <Button
                            label="Cancel"
                            type="neutral"
                            size="large"
                            onClick={ () => {
                                this.props.prepopulatedEmployeeId === ''
                                    ? browserHistory.push( '/loans' )
                                    : browserHistory.push( `/employee/${this.props.prepopulatedEmployeeId}`, true );
                            } }
                        />
                        <Button
                            ref={ ( ref ) => { this.submitButton = ref; } }
                            type="action"
                            disabled={ this.state.status !== 'validated' }
                            label={
                                this.state.savingStatus === 'save_queued' || this.state.savingStatus === 'saving' ?
                                    (
                                        <StyledLoader className="animation">
                                            Saving <div className="anim3"></div>
                                        </StyledLoader>
                                    ) : 'Submit'
                            }
                            size="large"
                            onClick={ () => {
                                this.submitButton.setState({ disabled: true });
                                this.setState({ savingStatus: 'save_queued' });
                                this.props.saveLoans({ jobId: this.props.batchUploadJobId });
                            } }
                        />
                    </Container>
                </div>
            </MainWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    batchUploadJobId: makeSelectBatchUploadJobId(),
    batchUploadStatus: makeSelectBatchUploadStatus(),
    batchUploadErrors: makeSelectBatchUploadErrors(),
    prepopulatedEmployeeId: makeSelectPrepopulatedEmployeeId(),
    loansPreview: makeSelectLoansPreview(),
    saving: makeSelectSaving()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( BatchUpload );
