import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { formatFeedbackMessage } from '../../../utils/functions';
import { PAYMENT_SCHEME_OPTIONS } from '../constants';

import {
    INITIALIZE,
    GET_COMPANY_EMPLOYEES,
    LOADING,
    GENERATING,
    SUBMIT_FORM,
    SUBMITTED,
    AMORTIZATION_SUBMITTED,
    GENERATE_PAYMENT_SCHEDULE_DETAILS,
    UPDATE_AMORTIZATION_PREVIEW,
    UPLOAD_LOANS,
    SET_LOAN,
    SET_GENERATED_PAYMENT_SCHEDULE_DETAILS,
    SET_ERRORS,
    SET_FORM_OPTIONS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    SET_SAVING_STATUS,
    SAVE_LOANS,
    GET_LOANS_PREVIEW,
    SET_LOANS_PREVIEW_STATUS,
    SET_LOANS_PREVIEW_DATA,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { setNotification } from './actions';

/**
 * Initialize data
 */
export function* initializeData() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formOptions = {};

        const loanTypes = yield call( Fetch, `/company/${companyId}/payroll_loan_types`, { method: 'GET' });
        const sssSubtypes = yield call( Fetch, '/payroll_loan_type/subtypes/SSS', { method: 'GET' });
        const pagIbigSubtypes = yield call( Fetch, '/payroll_loan_type/subtypes/Pag-ibig', { method: 'GET' });

        const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddLoan' ) );
        if ( prepopulatedEmployee ) {
            const filterEmployeeIdsQueryString = `&filter[employee_ids][]=${prepopulatedEmployee.id}`;
            const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL${filterEmployeeIdsQueryString}`, { method: 'GET' });
            formOptions.employees = employees.data;
        }

        formOptions.loanTypes = loanTypes.data.map( ( value ) => ({ label: value.name, value: value.id, companyId: value.company_id }) );
        formOptions.loanTypes = formOptions.loanTypes.filter( ( loanType ) => loanType.label !== 'Gap' );
        formOptions.sssSubtypes = sssSubtypes.map( ( value ) => {
            const label = ( value.charAt( 0 ) + value.slice( 1 ).toLowerCase() ).replace( '_', ' ' );
            return { label, value };
        });
        formOptions.pagIbigSubtypes = pagIbigSubtypes.map( ( value ) => {
            const label = ( value.charAt( 0 ) + value.slice( 1 ).toLowerCase() ).replace( '_', ' ' );
            return { label, value };
        });
        formOptions.paymentScheme = PAYMENT_SCHEME_OPTIONS;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Get Company Employees
 */
export function* getCompanyEmployees({ payload }) {
    try {
        const keyword = payload.keyword ? payload.keyword : '';
        const companyId = company.getLastActiveCompanyId();
        const formOptions = payload.formOptions;
        let filterEmployeeIdsQueryString = '';

        if ( payload.filterEmployeeIds ) {
            const filterEmployeeIds = payload.filterEmployeeIds;
            if ( filterEmployeeIds.length > 0 ) {
                for ( const employeeId of filterEmployeeIds ) {
                    filterEmployeeIdsQueryString += `&filter[employee_ids][]=${employeeId}`;
                }
            }
        }

        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL&keyword=${keyword}${filterEmployeeIdsQueryString}`, { method: 'GET' });

        formOptions.employees = employees.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        // Transform data to remove null keys
        const data = { ...payload };
        const keys = Object.keys( payload );

        keys.forEach( ( key ) => {
            if ( !payload[ key ]) {
                delete data[ key ];
            }
        });

        data.company_id = company.getLastActiveCompanyId();

        yield call( Fetch, `/payroll_loan/create/${data.uid}`, { method: 'POST', data });
        yield call( showSuccessMessage );
        yield call( delay, 500 );

        payload.previousRouteName ? yield call( browserHistory.goBack ) : yield call( browserHistory.push, '/loans' );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyError, error );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Generate payment schedule details
 * @param payload
 */
export function* generatePaymentScheduleDetails({ payload }) {
    try {
        yield [
            put({ type: GENERATING, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];
        // Transform data to remove null keys
        const data = { ...payload };
        const keys = Object.keys( payload );

        keys.forEach( ( key ) => {
            if ( payload[ key ] === null ) {
                delete data[ key ];
            }
        });

        data.company_id = company.getLastActiveCompanyId();

        const loan = yield call( Fetch, '/payroll_loan', { method: 'POST', data });

        yield [
            put({ type: SET_GENERATED_PAYMENT_SCHEDULE_DETAILS, payload: loan.amortizations }),
            put({ type: SET_LOAN, payload: loan })
        ];
    } catch ( error ) {
        if ( error.response && error.response.data && error.response.data.message ) {
            yield call( notifyError, error );
        } else {
            yield call( setErrors, error.response.data );
        }
    } finally {
        yield put({
            type: GENERATING,
            payload: false
        });
    }
}

/**
 * Update Amortization Preview
 */
export function* updateAmortizationPreview({ payload }) {
    try {
        yield put({
            type: AMORTIZATION_SUBMITTED,
            payload: true
        });

        const loanUid = payload.loanUid;
        const companyId = company.getLastActiveCompanyId();
        const payloadWithCompanyId = { ...payload, company_id: companyId };

        const amortizations = yield call(
            Fetch,
            `/payroll_loan/${loanUid}/update_amortization_preview`,
            { method: 'POST', data: payloadWithCompanyId }
        );

        yield put({ type: SET_GENERATED_PAYMENT_SCHEDULE_DETAILS, payload: amortizations });
        yield call( browserHistory.push, '/loans/add' );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: AMORTIZATION_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage()
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Uploads the CSV of loans to add and starts the validation process
 */
export function* uploadLoans({ payload }) {
    try {
        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: {}
        });

        const data = new FormData();

        data.append( 'company_id', company.getLastActiveCompanyId() );
        data.append( 'loan_type', 'SYSTEM_DEFINED' ); // this should be removed when backend part is implemented
        data.append( 'file', payload.file );

        const upload = yield call( Fetch, '/payroll_loan/upload', {
            method: 'POST',
            data
        });

        if ( upload.id ) {
            yield put({
                type: SET_BATCH_UPLOAD_JOB_ID,
                payload: upload.id
            });

            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: 'validation_queued'
            });

            yield call( checkValidation, { payload: { jobId: upload.id }});
        }
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const check = yield call(
            Fetch,
            `/payroll_loan/upload/status?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}`,
            { method: 'GET' }
        );

        if ( check.status === 'validation_failed' ) {
            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: check.status
            });

            yield put({
                type: SET_BATCH_UPLOAD_ERRORS,
                payload: check.errors
            });
        } else if ( check.status === 'saved' ) {
            yield [
                put({
                    type: SET_SAVING_STATUS,
                    payload: check.status
                }),
                call( notifyUser, {
                    show: true,
                    title: 'Success',
                    message: 'Loans successfully saved',
                    type: 'success'
                })
            ];

            browserHistory.push( '/loans' );
        } else {
            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: check.status
            });

            yield call( delay, 2000 );
            yield call( checkValidation, { payload: { jobId: payload.jobId }});
        }
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Fetches preview for uploaded loans
 */
export function* getLoansPreview({ payload }) {
    try {
        yield put({
            type: SET_LOANS_PREVIEW_DATA,
            payload: []
        });

        yield put({
            type: SET_LOANS_PREVIEW_STATUS,
            payload: 'busy'
        });

        const preview = yield call(
            Fetch,
            `/payroll_loan/upload/preview?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}`,
            { method: 'GET' }
        );

        yield put({
            type: SET_LOANS_PREVIEW_DATA,
            payload: preview
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_LOANS_PREVIEW_STATUS,
            payload: 'ready'
        });
    }
}

/**
 * Saves validated loans
 */
export function* saveLoans({ payload }) {
    try {
        const upload = yield call( Fetch, '/payroll_loan/upload/save', {
            method: 'POST',
            data: {
                company_id: company.getLastActiveCompanyId(),
                job_id: payload.jobId
            }
        });

        if ( upload.id ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: 'save_queued'
            });

            yield call( checkValidation, { payload: {
                step: 'save',
                jobId: payload.jobId
            }});
        }
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForGetCompanyEmployees() {
    const watcher = yield takeEvery( GET_COMPANY_EMPLOYEES, getCompanyEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GENERATE_PAYMENT_SCHEDULE_DETAILS
 */
export function* watchForGeneratePaymentScheduleDetails() {
    const watcher = yield takeEvery( GENERATE_PAYMENT_SCHEDULE_DETAILS, generatePaymentScheduleDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPDATE_AMORTIZATION_PREVIEW
 */
export function* watchForUpdateAmortizationPreview() {
    const watcher = yield takeEvery( UPDATE_AMORTIZATION_PREVIEW, updateAmortizationPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPLOAD_LOANS
 */
export function* watchForUploadLoans() {
    const watcher = yield takeEvery( UPLOAD_LOANS, uploadLoans );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_LOANS_PREVIEW
 */
export function* watchForGetLoansPreview() {
    const watcher = yield takeEvery( GET_LOANS_PREVIEW, getLoansPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SAVE_LOANS
 */
export function* watchForSaveLoans() {
    const watcher = yield takeEvery( SAVE_LOANS, saveLoans );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForGeneratePaymentScheduleDetails,
    watchForUpdateAmortizationPreview,
    watchForGetLoansPreview,
    watchForUploadLoans,
    watchForInitializeData,
    watchForGetCompanyEmployees,
    watchForReinitializePage,
    watchForSaveLoans,
    watchNotify
];
