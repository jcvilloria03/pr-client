import { fromJS } from 'immutable';
import {
    LOADING,
    GENERATING,
    SET_FORM_OPTIONS,
    SET_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    RESET_LOANS_PREVIEW,
    SET_LOANS_PREVIEW_STATUS,
    SET_LOANS_PREVIEW_DATA,
    SET_SAVING_STATUS,
    SET_SAVING_ERRORS,
    SET_PREPOPULATED_EMPLOYEE_ID,
    SUBMITTED,
    AMORTIZATION_SUBMITTED,
    SET_GENERATED_PAYMENT_SCHEDULE_DETAILS,
    SET_LOAN,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    errors: {},
    generating: false,
    submitted: false,
    amortizationSubmitted: false,
    amortizations: [],
    batchUploadJobId: null,
    batchUploadStatus: '',
    batchUploadErrors: {},
    loansPreview: {
        status: 'ready',
        data: []
    },
    saving: {
        status: '',
        errors: {}
    },
    prepopulatedEmployeeId: '',
    loan: {
        uid: null,
        type_id: null,
        total_amount: null,
        subtype: null,
        reference_no: null,
        payment_start_date: null,
        payment_end_date: null,
        payment_scheme: null,
        monthly_amortization: null,
        employee_id_no: null,
        employee_id: null,
        created_date: null,
        company_id: null,
        term: null
    },
    formOptions: {
        loanTypes: [],
        sssSubtypes: [],
        pagIbigSubtypes: [],
        paymentScheme: [],
        employees: []
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Manual entry loan reducer
 *
 */
function manualEntryLoan( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case GENERATING:
            return state.set( 'generating', action.payload );
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case AMORTIZATION_SUBMITTED:
            return state.set( 'amortizationSubmitted', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_GENERATED_PAYMENT_SCHEDULE_DETAILS:
            return state.set( 'amortizations', fromJS( action.payload ) );
        case SET_LOAN:
            return state.set( 'loan', fromJS( action.payload ) );
        case SET_BATCH_UPLOAD_JOB_ID:
            return state.set( 'batchUploadJobId', action.payload );
        case SET_PREPOPULATED_EMPLOYEE_ID:
            return state.set( 'prepopulatedEmployeeId', action.payload );
        case SET_BATCH_UPLOAD_STATUS:
            return state.set( 'batchUploadStatus', action.payload );
        case SET_BATCH_UPLOAD_ERRORS:
            return state.set( 'batchUploadErrors', action.payload );
        case SET_LOANS_PREVIEW_STATUS:
            return state.setIn([ 'loansPreview', 'status' ], action.payload );
        case SET_LOANS_PREVIEW_DATA:
            return state.setIn([ 'loansPreview', 'data' ], fromJS( action.payload ) );
        case SET_SAVING_STATUS:
            return state.setIn([ 'saving', 'status' ], action.payload );
        case SET_SAVING_ERRORS:
            return state.setIn([ 'saving', 'errors' ], action.payload );
        case RESET_LOANS_PREVIEW:
            return state.set( 'preview', fromJS({
                status: 'ready',
                data: []
            }) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default manualEntryLoan;
