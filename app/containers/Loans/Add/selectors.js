import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectAddLoanDomain = () => ( state ) => state.get( 'addLoan' );

const makeSelectAmortizations = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'amortizations' ).toJS()
);

const makeSelectLoan = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'loan' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectGenerating = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'generating' )
);

const makeSelectFormOptions = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectAmortizationSubmitted = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'amortizationSubmitted' )
);

const makeSelectNotification = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectLoansPreview = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'loansPreview' ).toJS()
);

const makeSelectSaving = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectPrepopulatedEmployeeId = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => substate.get( 'prepopulatedEmployeeId' )
);

const makeSelectErrors = () => createSelector(
    selectAddLoanDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectAmortizations,
    makeSelectLoan,
    makeSelectSubmitted,
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectAmortizationSubmitted,
    makeSelectPrepopulatedEmployeeId,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectLoansPreview,
    makeSelectSaving,
    makeSelectGenerating,
    makeSelectErrors,
    makeSelectNotification
};
