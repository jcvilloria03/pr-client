import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment';
import Helmet from 'react-helmet';
import decimal from 'js-big-decimal';
import get from 'lodash/get';

import Typeahead from 'components/Typeahead';
import SnackBar from 'components/SnackBar';
import SalConfirm from 'components/SalConfirm';
import Loader from 'components/Loader';
import Input from 'components/Input';
import Switch from 'components/Switch';
import DatePicker from 'components/DatePicker';
import SalSelect from 'components/Select';
import Button from 'components/Button';
import Table from 'components/Table';
import { H5 } from 'components/Typography';
import Icon from 'components/Icon';

import {
    formatPaginationLabel,
    formatCurrency,
    formatCurrencyToDecimalNotation,
    formatDate,
    isSameOrAfter
} from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { DATE_FORMATS } from 'utils/constants';

import {
    MainWrapper,
    ConfirmBodyWrapperStyle
} from './styles';

import {
    makeSelectAmortizations,
    makeSelectLoan,
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectLoading,
    makeSelectPrepopulatedEmployeeId,
    makeSelectGenerating
} from '../selectors';

import * as createLoanActions from '../actions';

import { USER_DEFINED, SYSTEM_DEFINED } from '../../constants';

const inputTypes = {
    input: [ 'reference_no', 'subtype', 'total_amount', 'monthly_amortization', 'term' ],
    select: [ 'payment_scheme', 'loan_type' ],
    datePicker: [ 'created_date', 'payment_end_date', 'payment_start_date' ]
};

/**
 * ManualEntry Container
 */
export class ManualEntry extends React.Component {
    static propTypes = {
        initializeData: React.PropTypes.func,
        getCompanyEmployees: React.PropTypes.func,
        resetAmortizations: React.PropTypes.func,
        generatePaymentScheduleDetails: React.PropTypes.func,
        formOptions: React.PropTypes.object,
        loan: React.PropTypes.object,
        amortizations: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitPayrollLoan: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        errors: React.PropTypes.object,
        loading: React.PropTypes.bool,
        generating: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        payroll_group_id: React.PropTypes.string,
        prepopulatedEmployeeId: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.number
        ]),
        setPrepopulatedEmployeeId: React.PropTypes.func,
        previousRoute: React.PropTypes.object
    }

    static defaultProps = {
        loading: true,
        errors: {}
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddLoan' ) );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            amortizations: this.props.amortizations,
            loanData: {
                employee_id: prepopulatedEmployee !== null ? prepopulatedEmployee.id : '',
                type_id: '',
                subtype: '',
                created_date: null,
                payment_end_date: null,
                payment_start_date: null,
                total_amount: '',
                monthly_amortization: '',
                payment_scheme: '',
                reference_no: '',
                term: '',
                active: true
            },
            showModal: false,
            showBatchModal: false,
            errors: {},
            typeaheadTimeout: null,
            closeLoan: false
        };

        this.searchInput = null;
    }

    /**
     * Page Authorization check
     */
    componentWillMount() {
        isAuthorized(['create.payroll_loans'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        if ( this.props.loan.uid === null ) {
            this.props.initializeData();
            this.props.setPrepopulatedEmployeeId( this.state.loanData.employee_id );
        } else {
            this.props.getCompanyEmployees({
                filterEmployeeIds: [this.state.loanData.employee_id],
                formOptions: this.props.formOptions
            });
            this.setInitialStateFromProps( this.props );
        }
    }

    /**
     * Display API errors in fields
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });

        nextProps.submitted !== this.props.submitted && this.submitButton.setState({ disabled: nextProps.submitted });
        nextProps.amortizations !== this.props.amortizations && this.setState({ amortizations: nextProps.amortizations }, () => {
            this.handleTableChanges();
        });

        nextProps.loan !== this.props.loan && this.updateState( 'payment_end_date', nextProps.loan.payment_end_date );
        nextProps.payroll_group_id !== this.props.payroll_group_id && this.setState({ payroll_group_id: nextProps.payroll_group_id });
    }

    componentDidUpdate() {
        if ( this.payment_scheme && this.state.payroll_group_id === null ) {
            this.payment_scheme.setState({
                error: true,
                errorMessage: 'Please add payroll group to this employee.'
            });
        }
    }

    /**
     * Reset store
     */
    componentWillUnmount() {
        if ( localStorage.getItem( 'addLoanEditingAmortization' ) === null ) {
            this.props.resetStore();
        }
    }

    getLoanType() {
        if ( !this.state.loanData.type_id ) {
            return null;
        }

        const loanTypes = get( this.props.formOptions, 'loanTypes', []);

        const selectedLoanType = loanTypes.find( ( loanType ) => loanType.value === parseInt( this.state.loanData.type_id, 10 ) );
        if ( selectedLoanType && selectedLoanType.companyId === 0 ) {
            return SYSTEM_DEFINED;
        }

        return USER_DEFINED;
    }

    getEmployeeFullName = () => {
        if ( this.props.formOptions.employees ) {
            const employee = this.props.formOptions.employees.find( ( e ) => e.id === parseInt( this.state.loanData.employee_id, 10 ) );

            return employee ? `${employee.first_name} ${employee.last_name} ${employee.employee_id}` : '';
        }

        return '';
    }

    setInitialStateFromProps = ( props ) => {
        const updatedLoanData = {};

        Object.keys( props.loan ).forEach( ( key ) => {
            if ( this.state.loanData[ key ] === undefined ) {
                return;
            }
            updatedLoanData[ key ] = props.loan[ key ];
        });

        this.setState({
            loanData: updatedLoanData
        }, () => {
            const { loanData } = this.state;
            const { formOptions } = this.props;

            const loanType = this.getLoanType();
            const employee = this.props.formOptions.employees.find( ( e ) => e.id === parseInt( this.state.loanData.employee_id, 10 ) );

            if ( loanType === SYSTEM_DEFINED ) {
                this.created_date.setState({
                    selectedDay: formatDate( loanData.created_date, DATE_FORMATS.DISPLAY )
                });
            }

            this.payment_start_date.setState({
                selectedDay: formatDate( loanData.payment_start_date, DATE_FORMATS.DISPLAY )
            });

            this.type_id.setState({ value: formOptions.loanTypes.filter( ( type ) => `${type.value}` === loanData.type_id )[ 0 ] });
            this.employee_id.setState({ value: [employee]});
            this.handleTableChanges();
        });
    }

    /**
     * Display errors from API
     */
    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.input.includes( key ) || inputTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, label: this.state.errors[ key ][ 0 ] });
                } else if ( inputTypes.datePicker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    /**
     * Updates the state with the form inputs
     */
    updateState( key, value ) {
        if ( typeof value === 'undefined' ) {
            return;
        }
        const dataClone = Object.assign({}, this.state.loanData );
        dataClone[ key ] = value !== '' && value !== null ? value : '';
        this.setState({ loanData: Object.assign( this.state.loanData, dataClone ) });
    }

    /**
     * handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = () => {
        if ( !this.amortizationsTable ) {
            return;
        }
        this.setState({
            label: formatPaginationLabel( this.amortizationsTable.tableComponent.state )
        });
    }

    /**
     * handles filters and search inputs
     */
    handleSearch = () => {
        let searchQuery = null;

        let dataToDisplay = this.props.amortizations;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery ) {
            dataToDisplay = dataToDisplay.filter( ( amortization ) => {
                let match = false;
                const { due_date } = amortization;
                if ( formatDate( due_date, DATE_FORMATS.DISPLAY ).toLowerCase().indexOf( searchQuery ) >= 0 ) {
                    match = true;
                }

                return match;
            });
        }

        this.setState({ amortizations: dataToDisplay }, () => {
            this.handleTableChanges();
        });
    }

    handleCreatedDateChange = ( value ) => {
        const selectedDay = formatDate( value, DATE_FORMATS.API );
        this.state.loanData.payment_start_date = selectedDay;
    }

    resetAmortizations = () => {
        this.props.resetAmortizations();
    }

    resetFields = () => {
        Object.keys( this.state.loanData ).forEach( ( key ) => {
            if ( key === 'payment_start_date' ) {
                this.state.loanData[ key ] = null;
            } else if ( key !== 'employee_id' && key !== 'type_id' ) {
                this.state.loanData[ key ] = '';
            }
        });
        this.resetAmortizations();
    }

    resetPaymentEndDate = () => {
        if ( this.state.loanData.payment_end_date ) {
            this.updateState( 'payment_end_date', null );
            this.payment_end_date.setState({ selectedDay: '' });
        }
    }

    handleLoanTermCalculation = () => {
        const totalAmount = parseFloat( this.state.loanData.total_amount );
        const monthlyAmortization = parseFloat( this.state.loanData.monthly_amortization );
        if ( totalAmount && monthlyAmortization && ( totalAmount >= monthlyAmortization ) ) {
            const precision = 5;
            const numberOfPayments = decimal.divide( totalAmount, monthlyAmortization, precision );
            this.updateState( 'term', decimal.ceil( numberOfPayments ) );
        } else {
            this.updateState( 'term', '' );
        }
    }

    handleMonthlyDeductionCalculation = () => {
        const totalAmount = parseFloat( this.state.loanData.total_amount );
        const loanTerm = parseInt( this.state.loanData.term, 10 );
        if ( totalAmount && loanTerm ) {
            let monthlyDeduction = decimal.divide( totalAmount, loanTerm );
            if ( loanTerm * decimal.round( monthlyDeduction.toString(), 2 ) < totalAmount ) {
                monthlyDeduction += 0.01;
            }
            this.updateState( 'monthly_amortization', decimal.round( monthlyDeduction.toString(), 2 ) );
        } else {
            this.updateState( 'monthly_amortization', '' );
        }
    }

    /**
     * Returns the currency format for a value
     */
    formatToCurrency = ( value ) => (
        value ? `P ${formatCurrency( value )}` : 'P 0.00'
    );

    validateForm() {
        let valid = true;
        const loanType = this.getLoanType();

        if ( loanType === SYSTEM_DEFINED ) {
            if ( this.reference_no._validate( this.reference_no.state.value ) ) {
                valid = false;
            }

            if ( !this.subtype._checkRequire( typeof this.subtype.state.value === 'object' && this.subtype.state.value !== null ? this.subtype.state.value.value : this.subtype.state.value ) ) {
                valid = false;
            }

            if ( this.created_date.checkRequired() ) {
                valid = false;
            }

            if ( this.payment_start_date.checkRequired() ) {
                valid = false;
            }

            if ( this.state.loanData.payment_start_date
                && this.state.loanData.created_date
                && !isSameOrAfter( this.state.loanData.payment_start_date, this.state.loanData.created_date, [DATE_FORMATS.API])
            ) {
                const message = 'The payment start date must be a date after or equal to created date.';
                this.payment_start_date.setState({ error: true, message });

                valid = false;
            } else if ( this.payment_start_date.checkRequired() ) {
                valid = false;
            }
        }

        if ( loanType !== SYSTEM_DEFINED ) {
            if ( this.payment_start_date.checkRequired() ) {
                valid = false;
            }
        }

        if ( this.employee_id._validate( this.employee_id.state.value ) ) {
            valid = false;
        }

        if ( !this.type_id._checkRequire( typeof this.type_id.state.value === 'object' && this.type_id.state.value !== null ? this.type_id.state.value.value : this.type_id.state.value ) ) {
            valid = false;
        }

        if ( this.total_amount._validate( this.total_amount.state.value ) ) {
            valid = false;
        }

        if ( this.monthly_amortization._validate( this.monthly_amortization.state.value ) ) {
            valid = false;
        }

        if ( !this.payment_scheme._checkRequire( typeof this.payment_scheme.state.value === 'object' && this.payment_scheme.state.value !== null ? this.payment_scheme.state.value.value : this.payment_scheme.state.value ) ) {
            valid = false;
        }

        if ( this.term._validate( this.term.state.value ) ) {
            valid = false;
        }

        if ( parseFloat( this.monthly_amortization.state.value ) > parseFloat( this.total_amount.state.value ) ) {
            const errorMessage = 'Monthly amortization must be less or equal to total amount.';
            this.monthly_amortization.setState({ error: true, errorMessage });

            valid = false;
        }

        return valid;
    }

    validateMonthlyAmortizations = () => {
        if ( parseFloat( this.monthly_amortization.state.value ) > parseFloat( this.total_amount.state.value ) ) {
            const errorMessage = 'Monthly amortization must be less or equal to total amount.';
            this.monthly_amortization.setState({ error: true, errorMessage });
        }
    }

    /**
     * Submits loan
     */
    submitPayrollLoan = () => {
        if ( this.validateForm( this.state.step ) ) {
            this.props.submitPayrollLoan({
                ...this.state.loanData,
                uid: this.props.loan.uid,
                employeeId: this.props.prepopulatedEmployeeId,
                previousRouteName: this.props.previousRoute.name
            });
        }
    }

    /**
     * Render system defined loan form
     */
    renderSystemDefinedLoanForm() {
        const employeeId = parseInt( this.state.loanData.employee_id, 10 );
        const loanType = get( this.props.formOptions, 'loanTypes', []).filter( ( t ) => `${t.value}` === `${this.state.loanData.type_id}` )[ 0 ];
        const employee = get( this.props.formOptions, 'employees', []).find( ( e ) => e.id === employeeId );

        const payrollFrequency = employee.payroll_group.payroll_frequency;

        return (
            <div>
                <h2 className="sl-c-section-title">
                    <span className="sl-c-circle">2</span>
                    Loan Details
                </h2>
                <div className="row">
                    <div className="col-xs-4">
                        <Input
                            id="reference_no"
                            label="Reference number"
                            placeholder="Enter reference number"
                            required
                            key="reference_no"
                            ref={ ( ref ) => { this.reference_no = ref; } }
                            onChange={ ( value ) => { this.updateState( 'reference_no', value ); } }
                            value={ this.state.loanData.reference_no }
                        />
                    </div>
                    <div className="col-xs-4 date-picker">
                        <DatePicker
                            label="Date issued"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            required
                            ref={ ( ref ) => { this.created_date = ref; } }
                            selectedDay={ this.state.loanData.created_date }
                            onChange={ ( value ) => {
                                const selectedDay = formatDate( value, DATE_FORMATS.API );
                                this.state.loanData.created_date = selectedDay;
                            } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <SalSelect
                            id="subtype"
                            label={ loanType.label === 'SSS' ? 'SSS loan type' : 'Pag-ibig loan type' }
                            required
                            key="subtype"
                            data={ loanType.label === 'SSS'
                                ? this.props.formOptions.sssSubtypes
                                : this.props.formOptions.pagIbigSubtypes
                            }
                            value={ this.state.loanData.subtype }
                            placeholder="Choose a loan subtype"
                            ref={ ( ref ) => { this.subtype = ref; } }
                            onChange={ ({ value }) => { this.updateState( 'subtype', value ); } }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4 date-picker">
                        <DatePicker
                            label="Repayment start date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            required
                            key="system_defined_start_date"
                            selectedDay={ this.state.loanData.payment_start_date }
                            ref={ ( ref ) => { this.payment_start_date = ref; } }
                            disabledDays={ [{ before: new Date( moment().subtract( 5, 'years' ) ) }, { after: new Date( moment().add( 5, 'years' ) ) }] }
                            onChange={ ( value ) => { this.handleCreatedDateChange( value ); } }
                        />
                    </div>
                    <div className="col-xs-4 date-picker">
                        <DatePicker
                            label="Repayment end date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            disabled
                            selectedDay={ this.state.loanData.payment_end_date }
                            ref={ ( ref ) => { this.payment_end_date = ref; } }
                            value={ this.state.loanData.payment_end_date }
                        />
                    </div>
                    <div className="col-xs-4">
                        <SalSelect
                            id="payment_scheme"
                            label="Payment scheme"
                            key="payment_scheme"
                            required
                            data={ this.props.formOptions.paymentScheme[ payrollFrequency ] }
                            value={ this.state.loanData.payment_scheme }
                            placeholder="Choose a payment scheme"
                            ref={ ( ref ) => { this.payment_scheme = ref; } }
                            onChange={ ({ value }) => { this.updateState( 'payment_scheme', value ); } }
                        />
                    </div>
                </div>
                <div className="row sl-u-gap-bottom--md">
                    <div className="col-xs-4">
                        <Input
                            id="total_amount"
                            label="Amount of loan"
                            placeholder="Enter total amount"
                            type="number"
                            required
                            minNumber={ 1 }
                            ref={ ( ref ) => { this.total_amount = ref; } }
                            onChange={ ( value ) => {
                                this.updateState( 'total_amount', value );
                                this.handleLoanTermCalculation();
                            } }
                            onBlur={ ( value ) => this.updateState( 'total_amount', formatCurrencyToDecimalNotation( value ) ) }
                            value={ this.state.loanData.total_amount }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            id="monthly_amortization"
                            label="Monthly amortization"
                            placeholder="Enter monthly amortization amount"
                            type="number"
                            required
                            minNumber={ 1 }
                            ref={ ( ref ) => { this.monthly_amortization = ref; } }
                            onChange={ ( value ) => {
                                this.updateState( 'monthly_amortization', value );
                                this.handleLoanTermCalculation();
                                this.validateMonthlyAmortizations();
                            } }
                            onBlur={ ( value ) => {
                                this.updateState( 'monthly_amortization', formatCurrencyToDecimalNotation( value ) );
                                this.validateMonthlyAmortizations();
                            } }
                            value={ this.state.loanData.monthly_amortization }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            id="term"
                            label="Loan terms (Months)"
                            type="number"
                            required
                            minNumber={ 1 }
                            maxNumber={ 500 }
                            ref={ ( ref ) => { this.term = ref; } }
                            value={ this.state.loanData.term }
                            onChange={ ( value ) => {
                                this.updateState( 'term', value );
                                this.handleMonthlyDeductionCalculation();
                                this.validateMonthlyAmortizations();
                            } }
                            onBlur={ ( value ) => {
                                this.updateState(
                                    'term',
                                    value === '' ? '' : Math.floor( value ).toString()
                                );
                            } }
                        />
                    </div>
                    <div className="col-xs-4" style={ { paddingBottom: '20px', paddingTop: '25px' } }>
                        <Switch
                            checked={ this.state.closeLoan }
                            onChange={ ( value ) => {
                                this.updateState(
                                    'active',
                                    !value
                                );
                                this.setState({
                                    closeLoan: value
                                });
                            } }
                        />
                        &nbsp; Close this loan?
                    </div>
                </div>
                { this.renderPaymentScheduleDetails() }
            </div>
        );
    }

    /**
     * Render user difiend loan form
     */
    renderUserDefinedLoanForm() {
        const employeeId = parseInt( this.state.loanData.employee_id, 10 );
        const employee = get( this.props.formOptions, 'employees', []).find( ( e ) => e.id === employeeId );

        const payrollFrequency = employee.payroll_group.payroll_frequency;

        return (
            <div>
                <h2 className="sl-c-section-title">
                    <span className="sl-c-circle">2</span>
                    Loan Details
                </h2>
                <div className="row">
                    <div className="col-xs-4">
                        <Input
                            id="total_amount"
                            label="Total amount payable"
                            required
                            type="number"
                            key="total_amount"
                            ref={ ( ref ) => { this.total_amount = ref; } }
                            onChange={ ( value ) => {
                                this.updateState( 'total_amount', value );
                                this.handleLoanTermCalculation();
                            } }
                            onBlur={ ( value ) => this.updateState( 'total_amount', formatCurrencyToDecimalNotation( value ) ) }
                            value={ this.state.loanData.total_amount }
                            minNumber={ 1 }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            id="monthly_amortization"
                            label="Monthly deductions"
                            required
                            type="number"
                            value={ this.state.loanData.monthly_amortization }
                            ref={ ( ref ) => { this.monthly_amortization = ref; } }
                            onChange={ ( value ) => {
                                this.updateState( 'monthly_amortization', value );
                                this.handleLoanTermCalculation();
                                this.validateMonthlyAmortizations();
                            } }
                            onBlur={ ( value ) => {
                                this.updateState( 'monthly_amortization', formatCurrencyToDecimalNotation( value ) );
                                this.validateMonthlyAmortizations();
                            } }
                            minNumber={ 1 }
                        />
                    </div>
                    <div className="col-xs-4">
                        <SalSelect
                            id="payment_scheme"
                            label="Payment scheme"
                            data={ this.props.formOptions.paymentScheme[ payrollFrequency ] }
                            value={ this.state.loanData.payment_scheme }
                            required
                            placeholder="Choose a payment scheme"
                            ref={ ( ref ) => { this.payment_scheme = ref; } }
                            onChange={ ({ value }) => { this.updateState( 'payment_scheme', value ); } }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        <Input
                            id="term"
                            label="Loan terms (Months)"
                            type="number"
                            required
                            minNumber={ 1 }
                            maxNumber={ 500 }
                            ref={ ( ref ) => { this.term = ref; } }
                            value={ this.state.loanData.term }
                            onChange={ ( value ) => {
                                this.updateState( 'term', value );
                                this.handleMonthlyDeductionCalculation();
                                this.validateMonthlyAmortizations();
                            } }
                            onBlur={ ( value ) => {
                                this.updateState(
                                    'term',
                                    value === '' ? '' : Math.floor( value ).toString()
                                );
                            } }
                        />
                    </div>
                    <div className="col-xs-4 date-picker">
                        <DatePicker
                            label="Repayment start date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            required
                            key="user_defined_start_date"
                            selectedDay={ this.state.loanData.payment_start_date }
                            ref={ ( ref ) => { this.payment_start_date = ref; } }
                            onChange={ ( value ) => { this.handleCreatedDateChange( value ); } }
                        />
                    </div>
                    <div className="col-xs-4 date-picker">
                        <DatePicker
                            label="Repayment end date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            disabled
                            placeholder=""
                            selectedDay={ this.state.loanData.payment_end_date }
                            ref={ ( ref ) => { this.payment_end_date = ref; } }
                        />
                    </div>
                    <div className="col-xs-4" style={ { paddingBottom: '20px' } }>
                        <Switch
                            checked={ this.state.closeLoan }
                            onChange={ ( value ) => {
                                this.updateState(
                                    'active',
                                    !value
                                );
                                this.setState({
                                    closeLoan: value
                                });
                            } }
                        />
                        &nbsp; Close this loan?
                    </div>
                </div>
                { this.renderPaymentScheduleDetails() }
            </div>
        );
    }

    /**
     * Render payment schedule details
     */
    renderPaymentScheduleDetails() {
        return (
            <div>
                <h2 className="sl-c-section-title">
                    <span className="sl-c-circle">3</span>
                    Payment Schedule Details
                </h2>
                <Button
                    className=""
                    label={ this.props.generating ? <Loader /> : 'Generate Payment Schedule Details' }
                    type="neutral"
                    size="large"
                    onClick={ () => {
                        const valid = this.validateForm( this.state.step );
                        if ( this.getLoanType() === USER_DEFINED ) {
                            this.setState({
                                loanData: Object.assign( this.state.loanData, { created_date: this.state.loanData.payment_start_date })
                            });
                        }
                        valid && this.props.generatePaymentScheduleDetails( Object.assign({}, this.state.loanData ) );
                    } }
                    ref={ ( ref ) => { this.submitButton = ref; } }
                />
                <br />
                <br />
                { this.props.amortizations.length ? this.renderPaymentScheduleTable() : null }
            </div>
        );
    }

    /**
     * Render payment schedule table
     */
    renderPaymentScheduleTable() {
        const tableColumns = [
            {
                header: 'Payment Date',
                accessor: 'due_date',
                minWidth: 200,
                render: ({ row }) => <div>{formatDate( row.due_date, DATE_FORMATS.DISPLAY )}</div>
            },
            {
                header: 'Amount Due',
                accessor: 'amount_due',
                minWidth: 150,
                render: ({ row }) => <div>P { row.amount_due }</div>
            },
            {
                header: 'Amount Deducted',
                accessor: 'amount_collected',
                minWidth: 200,
                render: ({ row }) => <div>P { row.amount_collected }</div>
            },
            {
                header: 'Remaining Balance',
                accessor: 'amount_remaining',
                minWidth: 200,
                render: ({ row }) => <div>P { row.amount_remaining }</div>
            },
            {
                header: 'Status',
                accessor: 'is_collected',
                minWidth: 150,
                render: ({ row }) => <div>{row.is_collected}</div>
            },
            {
                header: 'Employee Remarks',
                accessor: 'employer_remarks',
                minWidth: 200,
                render: ({ row }) => <div>{row.employer_remarks}</div>
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => {
                    const totalAmount = this.state.amortizations.reduce(
                        ( sum, amortization ) => decimal.add( sum, formatCurrencyToDecimalNotation( amortization.amount_due ) ), 0
                    );

                    return (
                        <Button
                            label={ <span>Edit</span> }
                            type="grey"
                            to="/loans/add/edit-amortization"
                            onClick={ () => {
                                localStorage.setItem(
                                    'addLoanEditingAmortization',
                                    JSON.stringify({ ...row, total_loan_amount: totalAmount })
                                );
                            } }
                        />
                    );
                }
            }
        ];

        const { amortizations } = this.state;

        const dataForDisplay = amortizations.map( ( amortization ) => ({
            ...amortization,
            is_collected: amortization.is_collected ? 'Deducted' : 'Undeducted',
            employer_remarks: amortization.employer_remarks ? amortization.employer_remarks : ' '
        }) );

        return (
            <div>
                <div className="title">
                    <H5>Payment Schedule Details</H5>
                    <div className="search-wrapper">
                        <Input
                            className="search"
                            id="search"
                            ref={ ( ref ) => { this.searchInput = ref; } }
                            onChange={ this.handleSearch }
                            addon={ {
                                content: <Icon name="search" />,
                                placement: 'right'
                            } }
                        />
                    </div>
                    <span>
                        { this.state.label }
                    </span>
                </div>
                <Table
                    data={ dataForDisplay }
                    columns={ tableColumns }
                    pagination
                    onDataChange={ this.handleTableChanges }
                    defaultSorting={ [
                        { id: 'due_date', desc: false }
                    ] }
                    ref={ ( ref ) => { this.amortizationsTable = ref; } }
                />
            </div>
        );
    }

    /**
     * Component Render Method
     */
    render() {
        const loanType = this.getLoanType();
        const formForSelectedLoanType = this.state.loanData.employee_id && loanType === SYSTEM_DEFINED
            ? this.renderSystemDefinedLoanForm()
            : this.state.loanData.employee_id && loanType === USER_DEFINED
                ? this.renderUserDefinedLoanForm()
                : '';

        return (
            <div>
                <Helmet
                    title="Add Loan"
                    meta={ [
                        { name: 'description', content: 'Create a new Loan' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <MainWrapper>
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/loans' ); } }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showModal }
                    />
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/loans/batch-add' ); } }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showBatchModal }
                    />

                    <div className={ `loader ${this.props.loading ? '' : 'hide'}` }>
                        <Loader />
                    </div>

                    <div className={ this.props.loading ? 'hide' : '' }>
                        <Container>
                            <h2 className="sl-c-section-title">
                                <span className="sl-c-circle">1</span>
                                Choose a Loan type
                            </h2>
                            <div className="row">
                                <div className="col-xs-4">
                                    {this.props.loading ? null : (
                                        <Typeahead
                                            id="employee_id"
                                            defaultValue={ this.getEmployeeFullName() }
                                            label="Employee name"
                                            required
                                            disabled={ this.props.prepopulatedEmployeeId !== '' }
                                            labelKey={ ( option ) => `${option.first_name} ${option.last_name} ${option.employee_id}` }
                                            filterBy={ [ 'first_name', 'last_name', 'employee_id' ] }
                                            placeholder="Start typing to search for an employee"
                                            ref={ ( ref ) => {
                                                // TODO: refactor this

                                                if ( ref === null ) {
                                                    return;
                                                }

                                                this.employee_id = ref;

                                                if ( this.state.loanData.employee_id !== '' ) {
                                                    const employee = this.props.formOptions.employees.find(
                                                        ( e ) => e.id === parseInt( this.state.loanData.employee_id, 10 )
                                                    );
                                                    this.employee_id.setState({
                                                        value: [employee]
                                                    });
                                                }
                                            } }
                                            options={ this.props.formOptions.employees }
                                            onInputChange={ ( value ) => {
                                                if ( value.trim() === '' ) {
                                                    return false;
                                                }

                                                if ( this.state.typeaheadTimeout ) {
                                                    clearTimeout( this.state.typeaheadTimeout );
                                                }

                                                this.state.typeaheadTimeout = setTimeout( () => {
                                                    this.props.getCompanyEmployees({
                                                        keyword: value,
                                                        formOptions: this.props.formOptions
                                                    });
                                                }, 1000 );

                                                return true;
                                            } }
                                            onChange={ ( value ) => {
                                                value !== this.state.loanData.employee_id && this.resetFields();
                                                this.updateState( 'employee_id', value[ 0 ] ? value[ 0 ].id : null );
                                                this.setState({ payroll_group_id: value[ 0 ] ? value[ 0 ].payroll.payroll_group_id : null });
                                                if ( value[ 0 ] && value[ 0 ].payroll.payroll_group_id === null ) {
                                                    this.employee_id.setState({
                                                        error: true,
                                                        errorMessage: 'Please add payroll group to this employee.'
                                                    });
                                                }
                                            } }
                                        />
                                    ) }
                                </div>
                                <div className="col-xs-4">
                                    <SalSelect
                                        id="loan_type"
                                        label="Loan type"
                                        required
                                        data={ this.props.formOptions.loanTypes }
                                        value={ this.state.loanData.type_id }
                                        placeholder="Choose a loan type"
                                        ref={ ( ref ) => { this.type_id = ref; } }
                                        onChange={ ({ value }) => {
                                            this.resetPaymentEndDate();
                                            value !== this.state.loanData.type_id && this.resetFields();
                                            this.updateState( 'type_id', value );
                                        } }
                                    />
                                </div>
                            </div>
                            { formForSelectedLoanType }
                        </Container>
                        <div className="foot">
                            <Button
                                label="Cancel"
                                type="neutral"
                                size="large"
                                onClick={ () => {
                                    this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/loans' );
                                } }
                            />
                            <Button
                                label={ this.props.submitted ? <Loader /> : 'Submit' }
                                type="action"
                                size="large"
                                disabled={ this.props.amortizations.length === 0 }
                                onClick={ this.submitPayrollLoan }
                                ref={ ( ref ) => { this.submitButton = ref; } }
                            />
                        </div>
                    </div>
                </MainWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    amortizations: makeSelectAmortizations(),
    loan: makeSelectLoan(),
    formOptions: makeSelectFormOptions(),
    submitted: makeSelectSubmitted(),
    errors: makeSelectErrors(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    generating: makeSelectGenerating(),
    prepopulatedEmployeeId: makeSelectPrepopulatedEmployeeId()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createLoanActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ManualEntry );
