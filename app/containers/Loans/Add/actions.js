import {
    INITIALIZE,
    GET_COMPANY_EMPLOYEES,
    SUBMIT_FORM,
    GENERATE_PAYMENT_SCHEDULE_DETAILS,
    UPDATE_AMORTIZATION_PREVIEW,
    UPLOAD_LOANS,
    SAVE_LOANS,
    NOTIFICATION,
    SET_GENERATED_PAYMENT_SCHEDULE_DETAILS,
    SET_PREPOPULATED_EMPLOYEE_ID,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData() {
    return {
        type: INITIALIZE
    };
}

/**
 * Get Company Employees
 */
export function getCompanyEmployees( payload ) {
    return {
        type: GET_COMPANY_EMPLOYEES,
        payload
    };
}

/**
 * Reset Amortizations
 */
export function resetAmortizations() {
    return {
        type: SET_GENERATED_PAYMENT_SCHEDULE_DETAILS,
        payload: []
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: NOTIFICATION_SAGA,
        payload
    };
}

/**
 * Submit a new payroll loan
 */
export function submitPayrollLoan( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}

/**
 * Generate payment schedule details
 */
export function generatePaymentScheduleDetails( payload ) {
    return {
        type: GENERATE_PAYMENT_SCHEDULE_DETAILS,
        payload
    };
}

/**
 * Update amortization preview
 */
export function updateAmortizationPreview( payload ) {
    return {
        type: UPDATE_AMORTIZATION_PREVIEW,
        payload
    };
}

/**
 * Upload loans
 */
export function uploadLoans( payload ) {
    return {
        type: UPLOAD_LOANS,
        payload
    };
}

/**
 * Set prepopulated employee id
 */
export function setPrepopulatedEmployeeId( employeeId ) {
    return {
        type: SET_PREPOPULATED_EMPLOYEE_ID,
        payload: employeeId
    };
}

/**
 * Save loans
 */
export function saveLoans( payload ) {
    return {
        type: SAVE_LOANS,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
