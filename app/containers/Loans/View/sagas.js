import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import { isEmpty } from 'lodash';

import {
    SET_LOADING,
    GET_PAGINATED_LOANS,
    SET_PAGINATION,
    GET_LOANS,
    SET_LOANS,
    SET_FILTER_DATA,
    DELETE_LOANS,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    EXPORT_LOANS,
    SET_DOWNLOADING,
    SET_DOWNLOAD_URL,
    SET_JOB_ID,
    GET_DOWNLOAD_URL

} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

/**
 * Get loans for table
 */
export function* getLoans() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        yield call( getPaginatedLoans, { payload: {
            page: 1,
            perPage: 10,
            orderBy: 'created_at',
            orderDir: 'desc'
        }});

        const [ loanTypes, locations, departments, positions ] = yield [
            call( Fetch, `/company/${companyId}/payroll_loan_types`, { method: 'GET' }),
            call( Fetch, `/philippine/company/${companyId}/locations`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/departments`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/positions`, { method: 'GET' })
        ];

        const filterData = {
            loanTypes: loanTypes.data,
            locations: locations.data,
            departments: departments.data,
            positions: positions.data
        };

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * get paginated loans
 */
export function* getPaginatedLoans({ payload }) {
    try {
        // yield put({
        //     type: SET_LOADING,
        //     payload: true
        // });

        const { page, perPage, orderBy = 'created_at', orderDir = 'desc' } = payload;
        const companyId = company.getLastActiveCompanyId();
        const params = `payroll_loans?page=${page}&per_page=${perPage}&order_by=${orderBy}&order_dir=${orderDir}`;
        const response = yield call(
                            Fetch,
                            `/company/${companyId}/${params}`,
                            { method: 'GET' }
                        );

        const { data, ...others } = response;

        yield put({
            type: SET_PAGINATION,
            payload: others
        });

        yield put({
            type: SET_LOANS,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Batch delete loans
 */
export function* deleteLoans({ payload }) {
    try {
        yield call( Fetch, '/payroll_loan/bulk_delete', {
            method: 'DELETE',
            data: {
                payroll_loans_ids: payload,
                company_id: company.getLastActiveCompanyId()
            }
        });
        yield [
            call( notifyUser, {
                show: true,
                title: 'Success',
                message: 'Record successfully deleted',
                type: 'success'
            }),
            call( getLoans )
        ];
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Batch export loans
 */
export function* exportLoans({ payload }) {
    try {
        yield put({
            type: SET_DOWNLOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();

        const response = yield call( Fetch, `/company/${companyId}/payroll_loans/download`, {
            method: 'POST',
            data: {
                ...payload,
                company_id: company.getLastActiveCompanyId()
            }
        });

        yield put({
            type: SET_JOB_ID,
            payload: response.data.job_id
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DOWNLOADING,
            payload: false
        });
    }
}

/**
 * Batch export loans
 */
export function* getDownloadUrl({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        let downloadUrl = '';
        // Poll to fetch download url
        while ( isEmpty( downloadUrl ) ) {
            const response = yield call( Fetch, `/company/${companyId}/payroll_loans/download/status`, {
                method: 'POST',
                data: {
                    job_id: payload.jobId
                }
            });
            downloadUrl = response.download_url;
            if ( !isEmpty( downloadUrl ) ) {
                yield put({
                    type: SET_DOWNLOAD_URL,
                    payload: response.download_url
                });
                yield put({
                    type: SET_JOB_ID,
                    payload: ''
                });
            }
            yield call( delay, 1500 );
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getLoans );
}

/**
 * Watcher for GET loans
 */
export function* watchForGetLoans() {
    const watcher = yield takeEvery( GET_LOANS, getLoans );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Paginated loans
 */
export function* watchForPaginatedLoans() {
    const watcher = yield takeEvery( GET_PAGINATED_LOANS, getPaginatedLoans );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE loans
 */
export function* watchForDeleteLoans() {
    const watcher = yield takeEvery( DELETE_LOANS, deleteLoans );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for EXPORT loans
 */
export function* watchForExportLoans() {
    const watcher = yield takeEvery( EXPORT_LOANS, exportLoans );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for get download url
 */
export function* watchForGetDownloadUrl() {
    const watcher = yield takeEvery( GET_DOWNLOAD_URL, getDownloadUrl );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetLoans,
    watchForDeleteLoans,
    watchForExportLoans,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForPaginatedLoans,
    watchForGetDownloadUrl
];
