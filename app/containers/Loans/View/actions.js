import {
    GET_LOANS,
    GET_PAGINATED_LOANS,
    DELETE_LOANS,
    NOTIFICATION,
    EXPORT_LOANS,
    GET_DOWNLOAD_URL

} from './constants';

/**
 * Fetch list of loans
 */
export function getLoans() {
    return {
        type: GET_LOANS
    };
}

/**
 * Fetch list of loans
 */
export function getPaginatedLoans( payload ) {
    return {
        type: GET_PAGINATED_LOANS,
        payload
    };
}

/**
 * Delete loans
 */
export function deleteLoans( IDs ) {
    return {
        type: DELETE_LOANS,
        payload: IDs
    };
}

/**
 * Export loans
 */
export function exportLoans( IDs ) {
    return {
        type: EXPORT_LOANS,
        payload: IDs
    };
}
/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Get Download Url.
 * @param {Object} data
 * @returns {Object}
 */
export function getDownloadUrl( data ) {
    return {
        type: GET_DOWNLOAD_URL,
        payload: data
    };
}
