import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectViewLoansDomain = () => ( state ) => state.get( 'loans' );

const makeSelectLoading = () => createSelector(
    selectViewLoansDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectDownloading = () => createSelector(
    selectViewLoansDomain(),
    ( substate ) => substate.get( 'downloading' )
);

const makeSelectLoans = () => createSelector(
    selectViewLoansDomain(),
  ( substate ) => substate.get( 'loans' ).toJS()
);

const makeSelectFilterData = () => createSelector(
    selectViewLoansDomain(),
  ( substate ) => substate.get( 'filterData' ).toJS()
);

const makeSelectPagination = () => createSelector(
    selectViewLoansDomain(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewLoansDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectDownloadUrl = () => createSelector(
    selectViewLoansDomain(),
    ( substate ) => substate.get( 'downloadUrl' )
);

const makeSelectJobId = () => createSelector(
    selectViewLoansDomain(),
    ( substate ) => substate.get( 'jobId' )
);

export {
  makeSelectLoading,
  makeSelectDownloading,
  makeSelectLoans,
  makeSelectFilterData,
  makeSelectNotification,
  makeSelectPagination,
  makeSelectDownloadUrl,
  makeSelectJobId
};
