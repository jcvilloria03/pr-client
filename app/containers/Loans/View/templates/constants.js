export const FILTER_TYPES = {
    LOAN_TYPE: 'loan_type',
    LOCATION: 'location',
    POSITION: 'position',
    DEPARTMENT: 'department'
};
