import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_LOANS,
    SET_PAGINATION,
    SET_FILTER_DATA,
    NOTIFICATION_SAGA,
    SET_DOWNLOADING,
    SET_JOB_ID,
    SET_DOWNLOAD_URL
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: false,
    downloading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    loans: [],
    filterData: {},
    pagination: {
        current_page: 0,
        from: 0,
        last_page: 0,
        per_page: 0,
        to: 0,
        total: 0
    },
    downloadUrl: '',
    jobId: ''
});

/**
 * Prepare list of loans fetched from API to accomodate FE needs
 * @param {*array} data array of loans
 */
function prepareLoansData( data ) {
    const newData = [].concat( data );

    return newData;
}

/**
 * Lonas view reducer
 */
function loansReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_DOWNLOADING:
            return state.set( 'downloading', action.payload );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_LOANS:
            return state.set( 'loans', fromJS( prepareLoansData( action.payload ) ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_DOWNLOAD_URL:
            return state.set( 'downloadUrl', fromJS( action.payload ) );
        case SET_JOB_ID:
            return state.set( 'jobId', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default loansReducer;
