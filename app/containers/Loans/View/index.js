import React from 'react';
import orderBy from 'lodash/orderBy';
import isEqual from 'lodash/isEqual';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import isEmpty from 'lodash/isEmpty';
import A from '../../../components/A';
import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { filterHelper } from '../../../utils/filterHelper';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';
import {
    formatPaginationLabel,
    formatCurrencyToDecimalNotation,
    formatDeleteLabel,
    formatDate
} from '../../../utils/functions';
import { subscriptionService } from '../../../utils/SubscriptionService';

import {
    makeSelectLoading,
    makeSelectLoans,
    makeSelectFilterData,
    makeSelectNotification,
    makeSelectDownloading,
    makeSelectPagination,
    makeSelectDownloadUrl,
    makeSelectJobId
} from './selectors';
import * as loansActions from './actions';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';

import SubHeader from '../../SubHeader';
import Loader from '../../../components/Loader';
import SnackBar from '../../../components/SnackBar';
import Table from '../../../components/Table';
import { H2, H3, H5 } from '../../../components/Typography';
import Button from '../../../components/Button';
import Input from '../../../components/Input';
import SalConfirm from '../../../components/SalConfirm';
import Icon from '../../../components/Icon';
import Filter from './templates/filter';

import { PAYMENT_SCHEME_LABELS } from '../constants';

/**
 * View Loans Component
 */
export class View extends React.Component {
    static propTypes = {
        getLoans: React.PropTypes.func,
        getPaginatedLoans: React.PropTypes.func,
        deleteLoans: React.PropTypes.func,
        exportLoans: React.PropTypes.func,
        loans: React.PropTypes.array,
        pagination: React.PropTypes.object,
        filterData: React.PropTypes.shape({
            loanTypes: React.PropTypes.array,
            positions: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        loading: React.PropTypes.bool,
        downloading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        getDownloadUrl: React.PropTypes.func,
        downloadUrl: React.PropTypes.string,
        jobId: React.PropTypes.string
    };

    static defaultProps = {
        loading: true,
        downloading: false,
        errors: {}
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: this.props.loans,
            showDelete: false,
            deleteLabel: '',
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            showModal: false,
            showFilter: false,
            hasFiltersApplied: false,
            orderBy: 'created_at',
            orderDir: 'desc',
            filterData: {},
            downloadUrl: '',
            jobId: ''
        };

        this.searchInput = null;
        this.handleSearch = this.handleSearch.bind( this );
        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.deleteLoans = this.deleteLoans.bind( this );
        this.onSortingChange = this.onSortingChange.bind( this );
    }

    /**
     * Page authorization check
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.payroll_loans',
            'create.payroll_loans',
            'delete.payroll_loans',
            'edit.payroll_loans'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.payroll_loans' ];

            if ( authorized ) {
                this.setState({ permission: {
                    view: authorization[ 'view.payroll_loans' ],
                    create: authorization[ 'create.payroll_loans' ],
                    delete: authorization[ 'delete.payroll_loans' ],
                    edit: authorization[ 'edit.payroll_loans' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    /**
     * Fetch loan list
     */
    componentDidMount() {
        this.props.getLoans();
    }

    /**
     * Handle new props
     */
    componentWillReceiveProps( nextProps ) {
        if ( !this.props.loans.length || !isEqual( nextProps.loans, this.props.loans ) ) {
            this.handleSearch( nextProps.loans );
        }
        if ( !isEmpty( nextProps.jobId ) ) {
            if ( !isEmpty( nextProps.downloadUrl ) && nextProps.downloadUrl !== this.props.downloadUrl ) {
                this.downloadFile( nextProps.downloadUrl );
            } else {
                this.getDownloadUrl( nextProps.jobId );
            }
        }
    }

    /**
     * Handle sorting change
     */
    onSortingChange( column, additive ) {
        // clone current sorting sorting
        let newSorting = JSON.parse( JSON.stringify( this.loansTable.state.sorting, ( key, value ) => {
            if ( typeof value === 'function' ) {
                return value.toString();
            }
            return value;
        }) );

        // check if already sorted by same column
        const existingIndex = newSorting.findIndex( ( d ) => d.id === column.id );

        let isDesc = false;
        if ( existingIndex > -1 ) {
            const existing = newSorting[ existingIndex ];
            if ( existing.desc ) {
                if ( additive ) {
                    newSorting.splice( existingIndex, 1 );
                } else {
                    existing.desc = false;
                    newSorting = [existing];
                }
            } else {
                existing.desc = true;
                isDesc = true;
                if ( !additive ) {
                    newSorting = [existing];
                }
            }
        } else if ( additive ) {
            newSorting.push({
                id: column.id,
                desc: false
            });
        } else {
            newSorting = [{
                id: column.id,
                desc: false
            }];
        }

        const orderDir = isDesc ? 'desc' : 'asc';

        this.props.getPaginatedLoans(
            {
                page: 1,
                perPage: this.props.pagination.per_page,
                orderBy: column.id,
                orderDir

            }
        );

        // set new sorting states
        this.setState({
            orderBy: column.id,
            orderDir
        });
        this.loansTable.setState({
            sorting: newSorting
        });
        this.loansTable.tableComponent.setState({
            sorting: newSorting
        });
    }

    /**
     * Get download url
     */
    getDownloadUrl = ( jobId ) => {
        this.props.getDownloadUrl({ jobId });
    };

     /**
     * Downloads file to user
     */
    downloadFile = ( downloadUrl = this.props.downloadUrl ) => {
        if ( downloadUrl ) {
            const linkElement = document.getElementById( 'downloadLink' );
            linkElement.href = downloadUrl;
            linkElement.click();
            linkElement.href = '';
        } else {
            this.getDownloadUrl( this.props.jobId );
        }
    }

    /**
     * handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges( tableProps = this.loansTable.tableComponent.state ) {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });

        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    /**
     * handles filters and search inputs
     */
    handleSearch( loans = this.props.loans ) {
        let searchQuery = null;

        let dataToDisplay = loans;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery ) {
            dataToDisplay = dataToDisplay.filter( ( loan ) => {
                let match = false;
                const { employee_name } = loan;
                if ( employee_name.toLowerCase().indexOf( searchQuery ) >= 0 ) {
                    match = true;
                }

                return match;
            });
        }

        this.setState({ displayedData: dataToDisplay }, () => {
            this.handleTableChanges();
        });
    }

    /**
     * send a request to delete loans
     */
    deleteLoans() {
        // Get role IDs of selected rows
        const loanIDs = [];
        this.loansTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                loanIDs.push( this.loansTable.props.data[ index ].id );
            }
        });
        // dispatch an action to send a request to API
        this.props.deleteLoans( loanIDs );
        this.setState({ showDelete: false });
    }

    /**
     * send a request to export loans
     */
    exportLoans = () => {
        // Get loan IDs of selected rows
        const selectedLoans = [];

        const sortingOptions = this.loansTable.state.sorting;

        this.loansTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedLoans.push( this.loansTable.props.data[ index ]);
            }
        });

        const sortedLoans = orderBy(
            selectedLoans,
            sortingOptions.map( ( o ) => o.id ),
            sortingOptions.map( ( o ) => ( o.desc ? 'desc' : 'asc' ) )
        );

        if ( !sortedLoans.length ) {
            return;
        }

        const payload = {
            filter: this.state.filters !== undefined ? this.state.filters : []
        };

        if ( this.loansTable.headerSelect.checked ) {
            payload.download_all = true;
        } else {
            payload.loan_ids = sortedLoans.map( ( item ) => item.id );
        }

        // dispatch an action to send a request to API
        this.setState({
            downloadUrl: '',
            jobId: ''
        }, () => {
            this.props.exportLoans( payload );
        });
    };

    toggleFilter = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    applyFilters = ( filters ) => {
        const filteredData = filterHelper.filter( this.props.loans, filters, 'loan_type' );

        this.setState({
            filters,
            displayedData: filteredData,
            hasFiltersApplied: !!filters.length
        }, () => {
            this.handleTableChanges();
        });
    }

    resetFilters = () => {
        this.setState({
            showFilter: false,
            displayedData: this.props.loans,
            hasFiltersApplied: false
        });
    }

    /**
     * Component Render Method
     */
    render() {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'employee_name',
                header: 'Employee Name',
                accessor: 'employee_name',
                sortable: true,
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.employee_name}
                    </div>
                )
            },
            {
                id: 'type_id',
                header: 'Loan Type',
                accessor: 'type_name',
                sortable: true,
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.type_name}
                    </div>
                )
            },
            {
                header: 'Payment Scheme',
                accessor: 'payment_scheme',
                sortable: false,
                minWidth: 260,
                render: ({ row }) => (
                    <div>
                        {row.payment_scheme}
                    </div>
                )
            },
            {
                id: 'payment_start_date',
                header: 'Date Approved',
                accessor: 'payment_start_date',
                sortable: true,
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        { formatDate( row.payment_start_date, DATE_FORMATS.DISPLAY )}
                    </div>
                )
            },
            {
                header: 'Total Amount Paid',
                accessor: 'collected_to_date',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.is_paid
                        ? <div><span className="bullet-green">&#x25cf;</span> Paid</div>
                        : <div>P {row.collected_to_date}</div>
                )
            },
            {
                header: 'Remaning Balance',
                accessor: 'parsed_remaining_balance',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.is_paid
                        ? <div></div>
                        : <div><span className="bullet-red">&#x25cf;</span> P {row.remaining_balance}</div>
                )
            },
            {
                header: 'Loan Terms',
                accessor: 'term',
                minWidth: 150,
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.term} { row.term > 1 ? 'months' : 'month' }
                    </div>
                )
            },
            {
                header: 'Status',
                accessor: 'active',
                minWidth: 150,
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.active ? 'Open' : 'Closed'}
                    </div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.state.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        to={ `/loans/${row.id}/edit` }
                    />
                )
            }
        ];

        const dataForDisplay = this.state.displayedData.map( ( data ) => ({
            ...data,
            payment_scheme: PAYMENT_SCHEME_LABELS[ data.payment_scheme ],
            type_name: data.type.name,
            parsed_remaining_balance: parseFloat( formatCurrencyToDecimalNotation( data.amount_remaining ), 10 )
        }) );

        return (
            <div>
                <Helmet
                    title="View Loans"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.deleteLoans }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Deleting the loan will erase all details related to it. Do you want to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Loans"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Loans.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Loans</H3>
                                <p>You may add employee loans and loan types through this page. Loans can be set for both Government or User Defined Loans.</p>
                                <div style={ { textAlign: 'center' } } >
                                    <div className="main">
                                        <Button
                                            className={ this.state.permission.create ? '' : 'hide' }
                                            label="Add Loans"
                                            size="large"
                                            type="action"
                                            to="/loans/add"
                                        />
                                        <Button
                                            className={ this.state.permission.create ? '' : 'hide' }
                                            label="Add Loan Types"
                                            size="large"
                                            type="neutral"
                                            to="/loan-types/add"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="title">
                                <H5>Loans List</H5>
                                <div className="search-wrapper">
                                    <Input
                                        className="search"
                                        id="search"
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                        onChange={ () => { this.handleSearch(); } }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                    />
                                </div>
                                <span>
                                    {
                                        this.state.showDelete ?
                                            (
                                                <div>
                                                    { this.state.deleteLabel }
                                                    &nbsp;

                                                    <Button
                                                        className={ this.state.permission.view ? '' : 'hide' }
                                                        label={ this.props.downloading ? <Loader /> : <span>Download</span> }
                                                        type="neutral"
                                                        onClick={ this.exportLoans }
                                                    />
                                                    <Button
                                                        className={ this.state.permission.delete ? '' : 'hide' }
                                                        label={ <span>Delete</span> }
                                                        type="danger"
                                                        onClick={ () => {
                                                            this.setState({ showModal: false }, () => {
                                                                this.setState({ showModal: true });
                                                            });
                                                        } }
                                                    />
                                                </div>
                                            )
                                            :
                                            (
                                                <div>
                                                    { this.state.label }
                                                    &nbsp;
                                                    <Button
                                                        label={ <span><Icon className="filter-icon" name="filter" /> Filter</span> }
                                                        type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                                        onClick={ this.toggleFilter }
                                                    />
                                                </div>

                                            )
                                    }
                                </span>
                            </div>
                            <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                                <Filter
                                    filterData={ this.props.filterData }
                                    onCancel={ () => { this.resetFilters(); } }
                                    onApply={ ( values ) => { this.applyFilters( values ); } }
                                />
                            </div>
                            <Table
                                data={ dataForDisplay }
                                columns={ tableColumns }
                                pagination
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.loansTable = ref; } }
                                selectable
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;

                                    this.setState({
                                        showDelete: selectionLength > 0,
                                        deleteLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                                onPageChange={ ( data ) => this.props.getPaginatedLoans({
                                    page: data + 1,
                                    perPage: this.props.pagination.per_page,
                                    orderBy: this.state.orderBy,
                                    orderDir: this.state.orderDir
                                }) }
                                onPageSizeChange={ ( data ) => this.props.getPaginatedLoans({
                                    page: 1,
                                    perPage: data,
                                    orderBy: this.state.orderBy,
                                    orderDir: this.state.orderDir
                                }) }
                                page={ this.props.pagination.current_page - 1 }
                                pageSize={ this.props.pagination.per_page }
                                pages={ this.props.pagination.last_page }
                                onSortingChange={ this.onSortingChange }
                                manual
                            />
                            <A download id="downloadLink"></A>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    downloading: makeSelectDownloading(),
    loans: makeSelectLoans(),
    filterData: makeSelectFilterData(),
    notification: makeSelectNotification(),
    pagination: makeSelectPagination(),
    downloadUrl: makeSelectDownloadUrl(),
    jobId: makeSelectJobId()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        loansActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
