import { fromJS } from 'immutable';

import {
    SET_LOADING,
    SET_UPDATING,
    SET_SUBMITTED,
    SET_API_ERRORS,
    SET_AMORTIZATION_SUBMITTED,
    SET_FORM_OPTIONS,
    SET_LOAN_PREVIEW,
    SET_EMPLOYEE_ID,
    SET_AMORTIZATIONS_PREVIEW,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    updating: false,
    submitted: false,
    amortizationSubmitted: false,
    formOptions: {
        loanType: [],
        sssSubtype: [],
        pagIbigSubtype: [],
        paymentScheme: []
    },
    employeeId: null,
    loanPreview: {},
    amortizationsPreview: [],
    apiErrors: {
        editLoan: false
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 * Loan edit reducer
 */
const loanEditReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_UPDATING:
            return state.set( 'updating', action.payload );
        case SET_SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_EMPLOYEE_ID:
            return state.set( 'employeeId', action.payload );
        case SET_API_ERRORS:
            return state.set( 'apiErrors', fromJS( action.payload ) );
        case SET_AMORTIZATION_SUBMITTED:
            return state.set( 'amortizationSubmitted', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_LOAN_PREVIEW:
            return state.set( 'loanPreview', fromJS( action.payload ) );
        case SET_AMORTIZATIONS_PREVIEW:
            return state.set( 'amortizationsPreview', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
};

export default loanEditReducer;
