import { take, call, put, cancel } from 'redux-saga/effects';
import { takeLatest, takeEvery, delay } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { browserHistory } from '../../../utils/BrowserHistory';
import { formatFeedbackMessage } from '../../../utils/functions';
import { RECORD_UPDATED_MESSAGE } from '../../../utils/constants';

import {
    INITIALIZE,
    SET_LOADING,
    SET_UPDATING,
    SET_SUBMITTED,
    SET_API_ERRORS,
    SET_AMORTIZATION_SUBMITTED,
    SET_FORM_OPTIONS,
    SET_LOAN_PREVIEW,
    SET_AMORTIZATIONS_PREVIEW,
    UPDATE_LOAN_PREVIEW,
    UPDATE_AMORTIZATION_PREVIEW,
    EDIT_LOAN,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';

import { SUBTYPE, PAYMENT_SCHEME_OPTIONS } from '../constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();

        const [ initialPreview, loanType, sssSubtype, pagIbigSubtype ] = yield [
            call( Fetch, `/payroll_loan/${payload.id}/initial_preview`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/payroll_loan_types`, { method: 'GET' }),
            call( Fetch, '/payroll_loan_type/subtypes/SSS', { method: 'GET' }),
            call( Fetch, '/payroll_loan_type/subtypes/Pag-ibig', { method: 'GET' })
        ];

        const employee = yield call( Fetch, `/employee/${initialPreview.employee_id}`, { method: 'GET' });

        const formOptions = {
            loanType: loanType.data.map( ( value ) => ({ label: value.name, value: value.id, companyId: value.company_id }) ),
            sssSubtype: sssSubtype.map( ( value ) => ({ label: SUBTYPE[ value ], value }) ),
            pagIbigSubtype: pagIbigSubtype.map( ( value ) => ({ label: SUBTYPE[ value ], value }) ),
            paymentScheme: PAYMENT_SCHEME_OPTIONS[ employee.payroll_group.payroll_frequency ]
        };

        yield put({
            type: SET_LOAN_PREVIEW,
            payload: { ...initialPreview, employee_unique_id: employee.employee_id }
        });

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });

        yield put({
            type: SET_AMORTIZATIONS_PREVIEW,
            payload: initialPreview.amortizations
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Update Loan Preview
 */
export function* updateLoanPreview({ payload }) {
    try {
        yield put({
            type: SET_UPDATING,
            payload: true
        });

        yield put({
            type: SET_API_ERRORS,
            payload: {
                editLoan: false
            }
        });

        const loanId = payload.id;
        const companyId = company.getLastActiveCompanyId();
        const payloadWithCompanyId = { ...payload, company_id: companyId };

        const loanPreview = yield call( Fetch, `/payroll_loan/${loanId}/update_loan_preview`, { method: 'POST', data: payloadWithCompanyId });

        yield put({
            type: SET_LOAN_PREVIEW,
            payload: loanPreview
        });

        yield put({
            type: SET_AMORTIZATIONS_PREVIEW,
            payload: loanPreview.amortizations
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_UPDATING,
            payload: false
        });
    }
}

/**
 * Update Amortization Preview
 */
export function* updateAmortizationPreview({ payload }) {
    try {
        yield put({
            type: SET_AMORTIZATION_SUBMITTED,
            payload: true
        });

        const loanId = payload.loan_id;
        const companyId = company.getLastActiveCompanyId();
        const payloadWithCompanyId = { ...payload, company_id: companyId };

        const amortizations = yield call( Fetch, `/payroll_loan/${loanId}/update_amortization_preview`, { method: 'POST', data: payloadWithCompanyId });

        yield put({
            type: SET_AMORTIZATIONS_PREVIEW,
            payload: amortizations
        });

        yield call( browserHistory.push, `/loans/${loanId}/edit` );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_AMORTIZATION_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Edit loan
 */
export function* editLoan({ payload }) {
    try {
        yield put({
            type: SET_SUBMITTED,
            payload: true
        });

        yield put({
            type: SET_API_ERRORS,
            payload: {
                editLoan: false
            }
        });

        const loanId = payload.id;
        const companyId = company.getLastActiveCompanyId();
        const payloadWithCompanyId = { ...payload, company_id: companyId };
        const { employeeId } = payload;

        yield call( Fetch, `/payroll_loan/${loanId}`, { method: 'POST', data: payloadWithCompanyId });

        yield call( showSuccessMessage );
        yield call( delay, 500 );
        yield call( resetStore );

        employeeId
            ? yield call( browserHistory.push, `/employee/${employeeId}`, true )
            : yield call( browserHistory.push, '/loans' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });

        yield put({
            type: SET_API_ERRORS,
            payload: {
                editLoan: true
            }
        });
    } finally {
        yield put({
            type: SET_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    yield call( resetStore );
    yield call( initializeData );
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Watcher for INITIALIZE
 */
export function* watcherForInitializeData() {
    const watcher = yield takeLatest( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for update loan preview
 */
export function* watchForUpdateLoanPreview() {
    const watcher = yield takeLatest( UPDATE_LOAN_PREVIEW, updateLoanPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for update amortization preview
 */
export function* watchForUpdateAmortizationPreview() {
    const watcher = yield takeLatest( UPDATE_AMORTIZATION_PREVIEW, updateAmortizationPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for edit loan
 */
export function* watchForEditLoan() {
    const watcher = yield takeLatest( EDIT_LOAN, editLoan );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for reinitialize page
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export default [
    watcherForInitializeData,
    watchForUpdateLoanPreview,
    watchForUpdateAmortizationPreview,
    watchForEditLoan,
    watchForNotifyUser,
    watchForReinitializePage
];
