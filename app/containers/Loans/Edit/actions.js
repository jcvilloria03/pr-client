import {
    INITIALIZE,
    SET_AMORTIZATIONS_PREVIEW,
    UPDATE_LOAN_PREVIEW,
    UPDATE_AMORTIZATION_PREVIEW,
    SET_EMPLOYEE_ID,
    EDIT_LOAN,
    NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData( id ) {
    return {
        type: INITIALIZE,
        payload: {
            id
        }
    };
}

/**
 * Update loan preview
 */
export function updateLoanPreview( payload ) {
    return {
        type: UPDATE_LOAN_PREVIEW,
        payload
    };
}

/**
 * Update amortization preview
 */
export function updateAmortizationPreview( payload ) {
    return {
        type: UPDATE_AMORTIZATION_PREVIEW,
        payload
    };
}

/**
 * Set employee id
 */
export function setEmployeeId( employeeId ) {
    return {
        type: SET_EMPLOYEE_ID,
        payload: employeeId
    };
}

/**
 * Set amortizations preview
 */
export function setAmortizationsPreview( payload ) {
    return {
        type: SET_AMORTIZATIONS_PREVIEW,
        payload
    };
}

/**
 * Update loan preview
 */
export function editLoan( payload ) {
    return {
        type: EDIT_LOAN,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
