import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectEditLoanDomain = () => ( state ) => state.get( 'editLoan' );

const makeSelectLoading = () => createSelector(
    selectEditLoanDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectUpdating = () => createSelector(
    selectEditLoanDomain(),
    ( substate ) => substate.get( 'updating' )
);

const makeSelectSubmitted = () => createSelector(
    selectEditLoanDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectApiErrors = () => createSelector(
    selectEditLoanDomain(),
    ( substate ) => substate.get( 'apiErrors' ).toJS()
);

const makeSelectFormOptions = () => createSelector(
    selectEditLoanDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectLoanPreview = () => createSelector(
    selectEditLoanDomain(),
    ( substate ) => substate.get( 'loanPreview' ).toJS()
);

const makeSelectAmortizationsPreview = () => createSelector(
    selectEditLoanDomain(),
    ( substate ) => substate.get( 'amortizationsPreview' ).toJS()
);

const makeSelectEmployeeId = () => createSelector(
    selectEditLoanDomain(),
    ( substate ) => substate.get( 'employeeId' )
);

const makeSelectNotification = () => createSelector(
    selectEditLoanDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectUpdating,
    makeSelectSubmitted,
    makeSelectApiErrors,
    makeSelectFormOptions,
    makeSelectLoanPreview,
    makeSelectAmortizationsPreview,
    makeSelectEmployeeId,
    makeSelectNotification
};
