export const INITIALIZE = 'app/Loans/Edit/INITIALIZE';
export const SET_LOADING = 'app/Loans/Edit/SET_LOADING';
export const SET_UPDATING = 'app/Loans/Edit/SET_UPDATING';
export const SET_SUBMITTED = 'app/Loans/Edit/SET_SUBMITTED';
export const SET_API_ERRORS = 'app/Loans/Edit/SET_API_ERRORS';
export const SET_AMORTIZATION_SUBMITTED = 'app/Loans/Edit/SET_AMORTIZATION_SUBMITTED';
export const SET_FORM_OPTIONS = 'app/Loans/Edit/SET_FORM_OPTIONS';
export const SET_LOAN_PREVIEW = 'app/Loans/Edit/SET_LOAN_PREVIEW';
export const SET_AMORTIZATIONS_PREVIEW = 'app/Loans/Edit/SET_AMORTIZATIONS_PREVIEW';
export const UPDATE_LOAN_PREVIEW = 'app/Loans/Edit/UPDATE_LOAN_PREVIEW';
export const UPDATE_AMORTIZATION_PREVIEW = 'app/Loans/Edit/UPDATE_AMORTIZATION_PREVIEW';
export const EDIT_LOAN = 'app/Loans/Edit/EDIT_LOAN';
export const SET_EMPLOYEE_ID = 'app/Loans/Edit/SET_EMPLOYEE_ID';
export const NOTIFICATION = 'app/Loans/Edit/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/Loans/Edit/NOTIFICATION_SAGA';
