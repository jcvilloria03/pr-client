import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectEditLoanDomain = () => ( state ) => state.get( 'editLoan' );

const makeSelectAmortizationSubmitted = () => createSelector(
    selectEditLoanDomain(),
    ( substate ) => substate.get( 'amortizationSubmitted' )
);

const makeSelectNotification = () => createSelector(
    selectEditLoanDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectAmortizationSubmitted,
    makeSelectNotification
};
