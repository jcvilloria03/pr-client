
import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import decimal from 'js-big-decimal';

import A from '../../../../components/A';
import Icon from '../../../../components/Icon';
import Radio from '../../../../components/Radio';
import Input from '../../../../components/Input';
import Loader from '../../../../components/Loader';
import Button from '../../../../components/Button';
import SnackBar from '../../../../components/SnackBar';
import DatePicker from '../../../../components/DatePicker';
import RadioGroup from '../../../../components/RadioGroup';

import SubHeader from '../../../../containers/SubHeader';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { isAuthorized } from '../../../../utils/Authorization';
import { formatCurrencyToDecimalNotation, formatDate } from '../../../../utils/functions';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../../utils/constants';

import {
    makeSelectAmortizationSubmitted,
    makeSelectNotification
} from './selectors';

import * as actions from '../actions';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    AlignRight
} from './styles';

/**
 * Edit Amortization Component
 */
class Edit extends Component {
    static propTypes = {
        params: PropTypes.object,
        amortizationSubmitted: PropTypes.bool,
        updateAmortizationPreview: PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            amortizationPayload: {
                loan_id: null,
                uid: null,
                amount_due: null,
                amount_collected: null,
                is_collected: null,
                due_date: null,
                employer_remarks: null,
                total_loan_amount: null
            },
            shouldRender: false,
            permission: {
                edit: false
            }
        };
    }

    componentWillMount() {
        isAuthorized([
            'view.payroll_loans',
            'edit.payroll_loans'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.payroll_loans' ];

            if ( authorized ) {
                this.setState({ permission: {
                    edit: authorization[ 'edit.payroll_loans' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });

        const editingAmortization = JSON.parse( localStorage.getItem( 'editingAmortization' ) );
        localStorage.removeItem( 'editingAmortization' );

        const loanId = this.props.params.id;

        editingAmortization === null
            ? browserHistory.push( `/loans/${loanId}/edit` )
            : this.setInitialAmortizationPayload( editingAmortization );
    }

    /**
     * Sets initial state for payload
     */
    setInitialAmortizationPayload = ( editingAmortization ) => {
        const { amortizationPayload } = this.state;
        const initialAmortizationPayload = {};

        Object.keys( amortizationPayload ).forEach( ( key ) => {
            initialAmortizationPayload[ key ] =
                key === 'amount_collected' || key === 'amount_due'
                    ? decimal.round( `${editingAmortization[ key ]}`.replace( /[,\s]/g, '' ), 2 )
                    : editingAmortization[ key ];
        });

        this.setState({
            amortizationPayload: initialAmortizationPayload,
            shouldRender: true
        });
    }

    /**
     * Updates the amortization payload based on form values
     */
    updateAmortizationPayload = ( key, value ) => {
        const { amortizationPayload } = this.state;
        const updatedAmortizationPayload = { ...amortizationPayload, [ key ]: value };

        this.setState({
            amortizationPayload: updatedAmortizationPayload
        });
    };

    /**
     * Sends api request for updating amortization preview
     */
    updateAmortizationPreview = () => {
        const { amortizationPayload } = this.state;

        const valid = this.validateForm();

        if ( valid ) {
            this.props.updateAmortizationPreview( amortizationPayload );
        }
    };

    /**
     * Validates form
     */
    validateForm = () => {
        let valid = true;

        if ( this.amount_collected._validate( this.amount_collected.state.value ) ) {
            valid = false;
        }

        return valid;
    }

    render() {
        const { amortizationPayload, shouldRender } = this.state;

        return shouldRender ? (
            <div>
                <Helmet
                    title="Edit Amortization"
                    meta={ [
                        { name: 'description', content: 'Edit an amortization' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <Container>
                    <NavWrapper>
                        <A
                            href
                            onClick={ ( e ) => { e.preventDefault(); browserHistory.push( `/loans/${amortizationPayload.loan_id}/edit` ); } }
                        >
                            <Icon name="arrow" />
                            Back to Employee Loan
                        </A>
                    </NavWrapper>
                    <HeadingWrapper>
                        <h3>Edit Payment Schedule Details</h3>
                        <p>
                            You may edit and manage the employee loan payment through this page.
                        </p>
                    </HeadingWrapper>
                    <PageWrapper>
                        <div className="row">
                            <div className="col-xs-4 date-picker">
                                <DatePicker
                                    id="due_date"
                                    label="Payment date"
                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                    selectedDay={ amortizationPayload.due_date }
                                    disabled
                                    required
                                    onChange={ ( value ) => {
                                        const selectedDay = formatDate( value, DATE_FORMATS.API );

                                        if ( selectedDay !== amortizationPayload.due_date ) {
                                            this.updateAmortizationPayload( 'due_date', selectedDay );
                                        }
                                    } }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="amount_due"
                                    label="Amount due"
                                    type="number"
                                    value={ amortizationPayload.amount_due }
                                    onChange={ ( value ) => this.updateAmortizationPayload( 'amount_due', value ) }
                                    disabled
                                    required
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="amount_collected"
                                    label="Amount paid"
                                    type="number"
                                    value={ amortizationPayload.amount_collected }
                                    ref={ ( ref ) => { this.amount_collected = ref; } }
                                    onChange={ ( value ) => this.updateAmortizationPayload( 'amount_collected', value ) }
                                    onBlur={ ( value ) => this.updateAmortizationPayload( 'amount_collected', formatCurrencyToDecimalNotation( value ) ) }
                                    required
                                    minNumber={ 0 }
                                    maxNumber={ parseFloat( amortizationPayload.total_loan_amount ) }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4 radio-group">
                                <span>* Status</span>
                                <RadioGroup
                                    horizontal
                                    value={ `${amortizationPayload.is_collected == true}` } // eslint-disable-line eqeqeq
                                >
                                    <Radio value="true" disabled>Deducted</Radio>
                                    <Radio value="false" disabled>Undeducted</Radio>
                                </RadioGroup>
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="employer_remarks"
                                    label="Employer remarks"
                                    placeholder="Enter employer remarks"
                                    value={ amortizationPayload.employer_remarks || '' }
                                    onChange={ ( value ) => this.updateAmortizationPayload( 'employer_remarks', value ) }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <AlignRight>
                                <Button
                                    label="Cancel"
                                    type="neutral"
                                    size="large"
                                    to={ `/loans/${amortizationPayload.loan_id}/edit` }
                                />
                                <Button
                                    className={ this.state.permission.edit ? '' : 'hide' }
                                    label={ this.props.amortizationSubmitted ? <Loader /> : 'Submit' }
                                    type="action"
                                    size="large"
                                    onClick={ this.updateAmortizationPreview }
                                />
                            </AlignRight>
                        </div>
                    </PageWrapper>
                </Container>
            </div>
        ) : null;
    }
}

const mapStateToProps = createStructuredSelector({
    amortizationSubmitted: makeSelectAmortizationSubmitted(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Edit );
