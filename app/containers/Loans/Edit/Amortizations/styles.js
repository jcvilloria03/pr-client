import styled from 'styled-components';

const PageWrapper = styled.div`
    .date-picker {
        .DayPickerInput {
            width: 100%;
        }
        span {
            display: block;
        }
        input {
            width: 100%;
            padding-top: 0px !important;
        }
    }
    .radio-group {
        span {
            color: #5b5b5b;
            font-size: 14px;
            margin-bottom: 4px;
            font-weight: 600;
        }
    }
`;

const NavWrapper = styled.div`
    padding: 10px 0;
    svg {
        height: 15px;
        margin-right: 10px;
    }
`;

const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;
    h3 {
        font-weight: 600;
    }
    p {
        text-align: center;
        max-width: 800px;
    }
`;

const LoaderWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

const AlignRight = styled.div`
    text-align: right;
`;

export {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    LoaderWrapper,
    AlignRight
};
