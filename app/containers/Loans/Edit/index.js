import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import BigDecimal from 'js-big-decimal';
import get from 'lodash/get';

import A from '../../../components/A';
import Icon from '../../../components/Icon';
import Input from '../../../components/Input';
import Switch from '../../../components/Switch';
import Table from '../../../components/Table';
import Loader from '../../../components/Loader';
import Button from '../../../components/Button';
import SalSelect from '../../../components/Select';
import SnackBar from '../../../components/SnackBar';
import DatePicker from '../../../components/DatePicker';
import { H2, H3, H5 } from '../../../components/Typography';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';
import {
    formatPaginationLabel,
    formatCurrency,
    formatCurrencyToDecimalNotation,
    isSameOrAfter,
    formatDate
} from '../../../utils/functions';
import { subscriptionService } from '../../../utils/SubscriptionService';

import SubHeader from '../../../containers/SubHeader';

import {
    makeSelectLoading,
    makeSelectUpdating,
    makeSelectSubmitted,
    makeSelectApiErrors,
    makeSelectFormOptions,
    makeSelectLoanPreview,
    makeSelectAmortizationsPreview,
    makeSelectEmployeeId,
    makeSelectNotification
} from './selectors';

import * as actions from './actions';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    LoaderWrapper,
    AlignRight
} from './styles';

import { USER_DEFINED, SYSTEM_DEFINED, GAP_LOAN } from '../constants';

/**
 * Edit Loan Component
 */
class Edit extends Component {
    static propTypes = {
        params: React.PropTypes.object,
        loading: React.PropTypes.bool,
        updating: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        apiErrors: React.PropTypes.object,
        formOptions: React.PropTypes.object,
        loanPreview: React.PropTypes.object,
        employeeId: React.PropTypes.number,
        amortizationsPreview: React.PropTypes.array,
        initializeData: React.PropTypes.func,
        updateLoanPreview: React.PropTypes.func,
        editLoan: React.PropTypes.func,
        setEmployeeId: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        resetStore: React.PropTypes.func
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            loanPayload: {
                id: null,
                type_id: null,
                employee_id: null,
                subtype: null,
                created_date: null,
                payment_start_date: null,
                total_amount: null,
                monthly_amortization: null,
                reference_no: null,
                payment_scheme: null,
                term: null,
                payment_end_date: null,
                payroll_id: null,
                active: null,
                final_pay_id: null
            },
            table: {
                label: 'Showing 0-0 of 0 entries',
                displayedData: []
            },
            permission: {
                edit: false
            },
            closeLoan: false,
            areFieldsDisabled: false
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.payroll_loans',
            'edit.payroll_loans'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.payroll_loans' ];

            if ( authorized ) {
                this.setState({ permission: {
                    edit: authorization[ 'edit.payroll_loans' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });

        const { loanPreview } = this.props;

        // send request to server for data if we don't have it, else load data from props
        if ( Object.keys( loanPreview ).length === 0 ) {
            this.props.initializeData( this.props.params.id );
            const employeeId = localStorage.getItem( 'employeeIdForEditLoan' );
            localStorage.removeItem( 'employeeIdForEditLoan' );

            if ( employeeId ) {
                this.props.setEmployeeId( parseInt( employeeId, 10 ) );
            }
        } else {
            this.getInitialStateFromProps( this.props );
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.updating || nextProps.submitted ) {
            return;
        }

        if ( nextProps.amortizationsPreview.length !== 0 ) {
            this.getInitialStateFromProps( nextProps );
        }
    }

    componentWillUnmount() {
        if ( localStorage.getItem( 'editingAmortization' ) === null ) {
            this.props.resetStore();
        }
    }

    getLoanType() {
        if ( !this.state.loanPayload.type_id || !this.props.formOptions.loanType.length ) {
            return null;
        }

        const loanTypes = this.props.formOptions.loanType;

        if ( parseInt( this.state.loanPayload.type_id, 10 ) === loanTypes.find( ( loanType ) => loanType.label === 'Gap' ).value ) {
            return GAP_LOAN;
        }

        if ( !loanTypes.find( ( loanType ) => loanType.value === parseInt( this.state.loanPayload.type_id, 10 ) ).companyId ) {
            return SYSTEM_DEFINED;
        }

        return USER_DEFINED;
    }

    /**
     * Extract initial state from props
     */
    getInitialStateFromProps = ({ loanPreview, amortizationsPreview }) => {
        const { editLoan: editLoanApiErrors } = this.props.apiErrors;

        if ( editLoanApiErrors ) {
            return;
        }

        const { loanPayload, table } = this.state;
        const initialLoanPayload = {};

        Object.keys( loanPayload ).forEach( ( key ) => {
            initialLoanPayload[ key ] =
                key === 'total_amount' || key === 'monthly_amortization'
                    ? formatCurrencyToDecimalNotation( loanPreview[ key ])
                    : loanPreview[ key ];
        });

        this.setState({
            loanPayload: initialLoanPayload,
            closeLoan: !( loanPayload.active === '1' ),
            table: { ...table, displayedData: amortizationsPreview }
        }, () => {
            this.handleTableChanges();
        });

        this.fieldsDisabled();
    }

    resetPaymentEndDate = () => {
        if ( this.state.loanPayload.payment_end_date ) {
            this.state.loanPayload.payment_end_date = null;
            this.payment_end_date.setState({ selectedDay: '' });
        }
    }

    handleCreatedDateChange = ( value ) => {
        const selectedDay = formatDate( value, DATE_FORMATS.API );
        this.state.loanPayload.payment_start_date = selectedDay;
        this.state.loanPayload.payment_end_date = null;
    }

    handleLoanTermCalculation = () => {
        const totalAmount = parseFloat( this.state.loanPayload.total_amount );
        const monthlyAmortization = parseFloat( this.state.loanPayload.monthly_amortization );
        if ( totalAmount && monthlyAmortization && ( totalAmount >= monthlyAmortization ) ) {
            const precision = 5;
            const numberOfPayments = BigDecimal.divide( totalAmount, monthlyAmortization, precision );
            this.updateLoanPayload( 'term', BigDecimal.ceil( numberOfPayments ) );
        } else {
            this.updateLoanPayload( 'term', '' );
        }
    }

    handleMonthlyDeductionCalculation = () => {
        const totalAmount = parseFloat( this.state.loanPayload.total_amount );
        const loanTerm = parseInt( this.state.loanPayload.term, 10 );
        if ( totalAmount && loanTerm ) {
            let monthlyDeduction = BigDecimal.divide( totalAmount, loanTerm );
            if ( loanTerm * BigDecimal.round( monthlyDeduction.toString(), 2 ) < totalAmount ) {
                monthlyDeduction += 0.01;
            }
            this.updateLoanPayload( 'monthly_amortization', BigDecimal.round( monthlyDeduction.toString(), 2 ) );
        } else {
            this.updateLoanPayload( 'monthly_amortization', '' );
        }
    }

    validateMonthlyAmortizations = () => {
        if ( parseFloat( this.monthly_amortization.state.value ) > parseFloat( this.total_amount.state.value ) ) {
            const errorMessage = 'Monthly amortization must be less or equal to total amount.';
            this.monthly_amortization.setState({ error: true, errorMessage });
        } else {
            this.monthly_amortization.setState({ error: false });
        }
    }

    validateLoanTerm = () => {
        if ( parseInt( this.state.loanPayload.term, 10 ) > 2000 ) {
            this.term.setState({ error: true, errorMessage: 'This field\'s maximum value is 2000' });
        } else {
            this.term.setState({ error: false, errorMessage: '' });
        }
    }

    /**
     * Updates the loan payload based on form values
     */
    updateLoanPayload = ( key, value ) => {
        if ( typeof value === 'undefined' ) {
            return;
        }

        const dataClone = Object.assign({}, this.state.loanData );
        dataClone[ key ] = value !== '' && value !== null ? value : '';
        this.setState({ loanPayload: Object.assign( this.state.loanPayload, dataClone ) });
    };

    /**
     * Sends api request for updating loan preview
     */
    updateLoanPreview = () => {
        const { loanPayload } = this.state;
        const payload = this.adjustPayloadNumberValues( loanPayload );
        const loanType = this.getLoanType();

        if ( loanType === USER_DEFINED || loanType === GAP_LOAN ) {
            Object.assign( payload, { created_date: payload.payment_start_date });
        }

        const valid = this.validateForm();

        if ( valid ) {
            this.props.updateLoanPreview( payload );
        }
    };

    /**
     * Sends api request for editing loan
     */
    editLoan = () => {
        const { loanPayload } = this.state;
        const payload = this.adjustPayloadNumberValues( loanPayload );
        const loanType = this.getLoanType();
        const valid = this.validateForm();

        if ( loanType === USER_DEFINED || loanType === GAP_LOAN ) {
            Object.assign( payload, { created_date: payload.payment_start_date });
        }

        if ( valid ) {
            this.props.editLoan({ ...payload, employeeId: this.props.employeeId });
        }
    };

    /**
     * Returns new payload object with adjusted numbers
     */
    adjustPayloadNumberValues = ( payload ) => ({
        ...payload,
        total_amount: BigDecimal.round( payload.total_amount, 2 ),
        monthly_amortization: BigDecimal.round( payload.monthly_amortization, 2 ),
        final_pay_id: ( !payload.final_pay_id ? null : payload.final_pay_id )
    });

    /**
     * Handles action when user types in search bar
     */
    handleSearch = () => {
        const { amortizationsPreview } = this.props;
        const { table } = this.state;

        let searchQuery = null;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        const displayedData = searchQuery !== null
            ? amortizationsPreview.filter( ( a ) => formatDate( a.due_date, DATE_FORMATS.DISPLAY ).includes( searchQuery ) )
            : amortizationsPreview;

        this.setState({
            table: { ...table, displayedData }
        }, () => {
            this.handleTableChanges();
        });
    }

    /**
     * Handles table data changes
     */
    handleTableChanges = () => {
        if ( !this.amortizationsTable ) {
            return;
        }

        const { table } = this.state;
        const updatedTable = { ...table, label: formatPaginationLabel( this.amortizationsTable.tableComponent.state ) };

        this.setState({
            table: updatedTable
        });
    }

    /**
     * Validates form
     */
    validateForm = () => {
        const { loanPayload } = this.state;
        const loanType = this.getLoanType();

        let valid = true;

        if ( loanType === SYSTEM_DEFINED ) {
            if ( this.reference_no._validate( this.reference_no.state.value ) ) {
                valid = false;
            }

            if ( !this.subtype._checkRequire( typeof this.subtype.state.value === 'object' && this.subtype.state.value !== null ? this.subtype.state.value.value : this.subtype.state.value ) ) {
                valid = false;
            }

            if ( this.created_date.checkRequired() ) {
                valid = false;
            }

            if ( loanPayload.payment_start_date
                && loanPayload.created_date
                && !isSameOrAfter( loanPayload.payment_start_date, loanPayload.created_date, [DATE_FORMATS.API])
            ) {
                const message = 'The payment start date must be a date after or equal to created date.';
                this.payment_start_date.setState({ error: true, message });

                valid = false;
            } else if ( this.payment_start_date.checkRequired() ) {
                valid = false;
            }
        }

        if ( loanType !== SYSTEM_DEFINED ) {
            if ( this.payment_start_date.checkRequired() ) {
                valid = false;
            }
        }

        if ( this.term._validate( this.term.state.value ) ) {
            valid = false;
        }

        if ( this.total_amount._validate( this.total_amount.state.value ) ) {
            valid = false;
        }

        if ( this.monthly_amortization._validate( this.monthly_amortization.state.value ) ) {
            valid = false;
        }

        if ( !this.payment_scheme._checkRequire( this.payment_scheme.state.value ) ) {
            valid = false;
        }

        if ( parseFloat( loanPayload.monthly_amortization ) > parseFloat( loanPayload.total_amount ) ) {
            const errorMessage = 'Monthly amortization must be less or equal to total amount.';
            this.monthly_amortization.setState({ error: true, errorMessage });

            valid = false;
        }

        return valid;
    };

    /**
     * Returns the currency format for a value
     */
    formatToCurrency = ( value ) => (
        value ? `P ${formatCurrency( value )}` : 'P 0.00'
    );

    /**
     * check if fields are to be disabled based on status or included in final pay run
     */
    fieldsDisabled = () => {
        let areFieldsDisabled = Boolean( Number( get( this.props.loanPreview, 'final_pay_id', null ) ) );

        if ( !get( this.props.loanPreview, 'active', 0 ) ) {
            areFieldsDisabled = true;
        }
        this.setState({
            areFieldsDisabled
        });
    };

    /**
     * Renders payment schedule details section
     */
    renderPaymentScheduleDetailsSection = () => {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Payment Date',
                accessor: 'due_date',
                minWidth: 150,
                render: ({ row }) => <div>{formatDate( row.due_date, DATE_FORMATS.DISPLAY )}</div>
            },
            {
                header: 'Amount Due',
                accessor: 'amount_due',
                minWidth: 150,
                render: ({ row }) => <div>P { row.amount_due }</div>
            },
            {
                header: 'Amount Deducted',
                accessor: 'amount_collected',
                minWidth: 150,
                render: ({ row }) => <div>P { row.amount_collected }</div>
            },
            {
                header: 'Remaining balance',
                accessor: 'amount_remaining',
                minWidth: 150,
                render: ({ row }) => <div>P { row.amount_remaining }</div>
            },
            {
                header: 'Status',
                accessor: 'is_collected',
                render: ({ row }) => <div>{row.is_collected}</div>
            },
            {
                header: 'Employer Remarks',
                accessor: 'employer_remarks',
                minWidth: 150,
                render: ({ row }) => <div>{row.employer_remarks}</div>
            },
            {
                header: ' ',
                accessor: 'actions',
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.state.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        to={ `/loans/${row.loan_id}/edit-amortization` }
                        onClick={ () => {
                            localStorage.setItem(
                                'editingAmortization',
                                JSON.stringify({ ...row, total_loan_amount: this.props.loanPreview.total_amount })
                            );
                        } }
                        disabled={ Boolean( get( row, 'final_pay_id' ) ) }
                    />
                )
            }
        ];

        const { table } = this.state;

        const dataForDisplay = table.displayedData.map( ( d ) => ({
            ...d,
            is_collected: d.is_collected ? 'Deducted' : 'Undeducted',
            employer_remarks: d.employer_remarks ? d.employer_remarks : ' '
        }) );

        return (
            <div>
                <div className="title">
                    <H5>Payment Schedule Details</H5>
                    <div className="search-wrapper">
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    className="search"
                                    id="search"
                                    onChange={ this.handleSearch }
                                    ref={ ( ref ) => { this.searchInput = ref; } }
                                    addon={ {
                                        content: <Icon name="search" />,
                                        placement: 'right'
                                    } }
                                />
                            </div>
                            <div className="col-xs-4">
                            </div>
                            <div className="col-xs-4 sl-u-text--right">
                                <span>
                                    { table.label }
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <Table
                    data={ dataForDisplay }
                    columns={ tableColumns }
                    onDataChange={ this.handleTableChanges }
                    ref={ ( ref ) => { this.amortizationsTable = ref; } }
                    pagination
                />
            </div>
        );
    };

    renderUserDefinedLoanForm() {
        const includedInFinalPayRun = this.state.areFieldsDisabled;

        return (
            <div>
                <div className="row">
                    <div className="col-xs-4">
                        <Input
                            id="total_amount"
                            label="Total amount payable"
                            required
                            type="number"
                            key="total_amount"
                            ref={ ( ref ) => { this.total_amount = ref; } }
                            onChange={ ( value ) => {
                                this.updateLoanPayload( 'total_amount', value );
                                this.handleLoanTermCalculation();
                                this.validateMonthlyAmortizations();
                                this.validateLoanTerm();
                            } }
                            onBlur={ ( value ) => this.updateLoanPayload( 'total_amount', formatCurrencyToDecimalNotation( value ) ) }
                            value={ this.state.loanPayload.total_amount }
                            minNumber={ this.props.loanPreview.gap_loan_total_amount ? this.props.loanPreview.gap_loan_total_amount : 1 }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            id="monthly_amortization"
                            label="Monthly deductions"
                            required
                            type="number"
                            value={ this.state.loanPayload.monthly_amortization }
                            ref={ ( ref ) => { this.monthly_amortization = ref; } }
                            onChange={ ( value ) => {
                                this.updateLoanPayload( 'monthly_amortization', value );
                                this.handleLoanTermCalculation();
                                this.validateMonthlyAmortizations();
                                this.validateLoanTerm();
                            } }
                            onBlur={ ( value ) => {
                                this.updateLoanPayload( 'monthly_amortization', formatCurrencyToDecimalNotation( value ) );
                                this.validateMonthlyAmortizations();
                            } }
                            minNumber={ 1 }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                    <div className="col-xs-4">
                        <SalSelect
                            id="payment_scheme"
                            label="Payment scheme"
                            data={ this.props.formOptions.paymentScheme }
                            value={ this.state.loanPayload.payment_scheme }
                            required
                            placeholder="Choose a payment scheme"
                            ref={ ( ref ) => { this.payment_scheme = ref; } }
                            onChange={ ({ value }) => { this.updateLoanPayload( 'payment_scheme', value ); } }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        <Input
                            id="term"
                            label="Loan terms (Months)"
                            type="number"
                            required
                            maxNumber={ 500 }
                            ref={ ( ref ) => { this.term = ref; } }
                            value={ this.state.loanPayload.term }
                            onChange={ ( value ) => {
                                this.updateLoanPayload( 'term', value );
                                this.handleMonthlyDeductionCalculation();
                                this.validateMonthlyAmortizations();
                            } }
                            onBlur={ ( value ) => {
                                this.updateLoanPayload(
                                    'term',
                                    value === '' ? '' : Math.floor( value ).toString()
                                );
                            } }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                    <div className="col-xs-4 date-picker">
                        <DatePicker
                            label="Repayment start date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            required
                            selectedDay={ this.state.loanPayload.payment_start_date }
                            ref={ ( ref ) => { this.payment_start_date = ref; } }
                            onChange={ ( value ) => {
                                this.handleCreatedDateChange( value );
                                this.resetPaymentEndDate();
                            } }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                    <div className="col-xs-4 date-picker">
                        <DatePicker
                            label="Repayment end date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            disabled
                            placeholder=""
                            selectedDay={ this.state.loanPayload.payment_end_date }
                            ref={ ( ref ) => { this.payment_end_date = ref; } }
                            value={ this.state.loanPayload.payment_end_date }
                        />
                    </div>
                    <div className="col-xs-4" style={ { paddingBottom: '20px' } }>
                        <Switch
                            checked={ this.state.closeLoan }
                            onChange={ ( value ) => {
                                this.setState({
                                    loanPayload: {
                                        ...this.state.loanPayload,
                                        active: !value
                                    },
                                    closeLoan: value
                                });
                            } }
                        />
                        &nbsp; Close this loan?
                    </div>
                </div>
            </div>
        );
    }

    renderSystemDefinedLoanForm() {
        const { loanPayload } = this.state;
        const { formOptions, loanPreview } = this.props;

        const loanType = formOptions.loanType.filter( ( t ) => `${t.value}` === loanPreview.type_id )[ 0 ];
        const includedInFinalPayRun = this.state.areFieldsDisabled;

        return (
            <div>
                <div className="row">
                    <div className="col-xs-4">
                        <Input
                            id="reference_no"
                            placeholder="Enter reference number"
                            label="Reference Number"
                            value={ loanPayload.reference_no }
                            ref={ ( ref ) => { this.reference_no = ref; } }
                            required
                            onChange={ ( value ) => this.updateLoanPayload( 'reference_no', value ) }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                    <div className="col-xs-4 date-picker">
                        <DatePicker
                            id="created_date"
                            label="Date issued"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            selectedDay={ loanPayload.created_date }
                            ref={ ( ref ) => { this.created_date = ref; } }
                            required
                            onChange={ ( value ) => {
                                const selectedDay = formatDate( value, DATE_FORMATS.API );

                                if ( selectedDay !== loanPayload.created_date ) {
                                    this.updateLoanPayload( 'created_date', selectedDay );
                                }
                            } }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                    <div className="col-xs-4">
                        { loanType.value !== 3
                            ? (
                                <SalSelect
                                    id="subtype"
                                    label={ `${loanType.label} loan type` }
                                    data={ loanType.value === 1
                                        ? formOptions.sssSubtype
                                        : formOptions.pagIbigSubtype }
                                    value={ loanPayload.subtype }
                                    ref={ ( ref ) => { this.subtype = ref; } }
                                    required
                                    onChange={ ( subtype ) => this.updateLoanPayload( 'subtype', subtype.value ) }
                                    disabled={ includedInFinalPayRun }
                                />
                            ) : null }
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4 date-picker">
                        <DatePicker
                            label="Repayment start date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            required
                            selectedDay={ this.state.loanPayload.payment_start_date }
                            ref={ ( ref ) => { this.payment_start_date = ref; } }
                            onChange={ ( value ) => {
                                this.handleCreatedDateChange( value );
                                this.resetPaymentEndDate();
                            } }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                    <div className="col-xs-4 date-picker">
                        <DatePicker
                            label="Repayment end date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            disabled
                            placeholder=""
                            selectedDay={ this.state.loanPayload.payment_end_date }
                            ref={ ( ref ) => { this.payment_end_date = ref; } }
                            value={ this.state.loanPayload.payment_end_date }
                        />
                    </div>
                    <div className="col-xs-4">
                        <SalSelect
                            id="payment_scheme"
                            label="Payment scheme"
                            data={ formOptions.paymentScheme }
                            value={ loanPayload.payment_scheme }
                            ref={ ( ref ) => { this.payment_scheme = ref; } }
                            required
                            onChange={ ( scheme ) => this.updateLoanPayload( 'payment_scheme', scheme.value ) }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        <Input
                            id="total_amount"
                            label="Amount of loan"
                            type="number"
                            required
                            minNumber={ 1 }
                            value={ loanPayload.total_amount }
                            ref={ ( ref ) => { this.total_amount = ref; } }
                            onChange={ ( value ) => {
                                this.updateLoanPayload( 'total_amount', value );
                                this.handleLoanTermCalculation();
                                this.validateMonthlyAmortizations();
                                this.validateLoanTerm();
                            } }
                            onBlur={ ( value ) => this.updateLoanPayload( 'total_amount', formatCurrencyToDecimalNotation( value ) ) }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            id="monthly_amortization"
                            label="Monthly amortization"
                            type="number"
                            required
                            minNumber={ 1 }
                            value={ loanPayload.monthly_amortization }
                            ref={ ( ref ) => { this.monthly_amortization = ref; } }
                            onChange={ ( value ) => {
                                this.updateLoanPayload( 'monthly_amortization', value );
                                this.handleLoanTermCalculation();
                                this.validateMonthlyAmortizations();
                                this.validateLoanTerm();
                            } }
                            onBlur={ ( value ) => {
                                this.updateLoanPayload( 'monthly_amortization', formatCurrencyToDecimalNotation( value ) );
                                this.validateMonthlyAmortizations();
                            } }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            id="term"
                            label="Loan terms (Months)"
                            type="number"
                            required
                            maxNumber={ 500 }
                            ref={ ( ref ) => { this.term = ref; } }
                            value={ this.state.loanPayload.term }
                            onChange={ ( value ) => {
                                this.updateLoanPayload( 'term', value );
                                this.handleMonthlyDeductionCalculation();
                                this.validateMonthlyAmortizations();
                            } }
                            onBlur={ ( value ) => {
                                this.updateLoanPayload(
                                    'term',
                                    value === '' ? '' : Math.floor( value ).toString()
                                );
                            } }
                            disabled={ includedInFinalPayRun }
                        />
                    </div>
                    <div className="col-xs-4" style={ { paddingBottom: '20px', paddingTop: '25px' } }>
                        <Switch
                            checked={ this.state.closeLoan }
                            onChange={ ( value ) => {
                                this.setState({
                                    loanPayload: {
                                        ...this.state.loanPayload,
                                        active: !value
                                    },
                                    closeLoan: value
                                });
                            } }
                        />
                        &nbsp; Close this loan?
                    </div>
                </div>
            </div>
        );
    }

    renderUserAndSystemDefinedSection = () => {
        const { formOptions, loanPreview } = this.props;
        const loanType = formOptions.loanType.filter( ( t ) => `${t.value}` === loanPreview.type_id )[ 0 ];

        return (
            <div className="col-xs-4">
                <SalSelect
                    id="type"
                    label="Loan type"
                    data={ formOptions.loanType }
                    value={ loanType }
                    required
                    disabled
                />
            </div>
        );
    }

    /**
     * Component render method
     */
    render() {
        const { loading, updating, loanPreview } = this.props;
        const loanType = this.getLoanType();
        return (
            <PageWrapper>
                <Helmet
                    title="Edit Loan"
                    meta={ [
                        { name: 'description', content: 'Edit a Loan' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();

                                this.props.employeeId
                                    ? browserHistory.push( `/employee/${this.props.employeeId}`, true )
                                    : browserHistory.push( '/loans' );
                            } }
                        >
                            <Icon name="arrow" />
                            Back to {this.props.employeeId ? 'Employee' : 'Loans'}
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    { loading
                        ? (
                            <LoaderWrapper>
                                <H2>Loading</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoaderWrapper>
                        ) : (
                            <div>
                                <HeadingWrapper>
                                    <h3>Edit Loan</h3>
                                    <p>
                                        You may edit and manage the employee loan through this page.
                                    </p>
                                </HeadingWrapper>
                                <h2 className="sl-c-section-title">
                                    <span className="sl-c-circle">1</span>
                                    Choose a loan type
                                </h2>
                                <div className="row">
                                    <div className="col-xs-4">
                                        <Input
                                            id="employee_name"
                                            label="Employee name"
                                            value={ `${loanPreview.employee_last_name} ${loanPreview.employee_first_name} ${loanPreview.employee_unique_id}` }
                                            required
                                            disabled
                                        />
                                    </div>
                                    { loanType !== GAP_LOAN ? this.renderUserAndSystemDefinedSection() : null }
                                </div>
                                <h2 className="sl-c-section-title">
                                    <span className="sl-c-circle">2</span>
                                    Loan Details
                                </h2>
                                <br />
                                { this.getLoanType() === SYSTEM_DEFINED ? this.renderSystemDefinedLoanForm() : this.renderUserDefinedLoanForm() }
                                <h2 className="sl-c-section-title">
                                    <span className="sl-c-circle">3</span>
                                    Payment Schedule Details
                                </h2>
                                Click the generate payment schedule details button to view the loan deduction schedule of this loan.
                                <br />
                                <br />
                                <Button
                                    className={ this.state.permission.edit ? '' : 'hide' }
                                    label={ this.props.updating ? <Loader /> : 'Generate Payment Schedule Details' }
                                    size="large"
                                    type="neutral"
                                    onClick={ this.updateLoanPreview }
                                    disabled={ this.props.updating || this.props.submitted }
                                />
                                <br />
                                <br />
                                { updating
                                    ? (
                                        <LoaderWrapper>
                                            <H2>Generating payment schedule details</H2>
                                            <br />
                                            <H3>Please wait...</H3>
                                        </LoaderWrapper>
                                    ) : this.renderPaymentScheduleDetailsSection() }
                                <AlignRight>
                                    <Button
                                        label="Cancel"
                                        type="neutral"
                                        size="large"
                                        onClick={ () => {
                                            this.props.employeeId
                                                ? browserHistory.push( `/employee/${this.props.employeeId}`, true )
                                                : browserHistory.push( '/loans' );
                                        } }
                                    />
                                    <Button
                                        className={ this.state.permission.edit ? '' : 'hide' }
                                        label={ this.props.submitted ? <Loader /> : 'Submit' }
                                        type="action"
                                        size="large"
                                        onClick={ this.editLoan }
                                        disabled={ this.props.updating || this.props.submitted }
                                    />
                                </AlignRight>
                            </div>
                        )
                    }
                </Container>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    updating: makeSelectUpdating(),
    submitted: makeSelectSubmitted(),
    apiErrors: makeSelectApiErrors(),
    formOptions: makeSelectFormOptions(),
    loanPreview: makeSelectLoanPreview(),
    amortizationsPreview: makeSelectAmortizationsPreview(),
    employeeId: makeSelectEmployeeId(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Edit );
