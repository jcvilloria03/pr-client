import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { Fetch } from '../../utils/request';

import {
    UPLOAD_PERSONAL_INFO,
    UPLOAD_PAYROLL_INFO,
    BATCH_UPLOAD_STATUS,
    GET_PREVIEW,
    SAVE_EMPLOYEES,
    RESET_PERSONAL_INFO,
    RESET_PAYROLL_INFO,
    RESET_PREVIEW,
    LOADING_EMPLOYEES,
    SET_JOB_ID,
    PERSONAL_INFO_STATUS,
    PERSONAL_INFO_ERRORS,
    TIME_ATTENDANCE_INFO_STATUS,
    TIME_ATTENDANCE_INFO_ERRORS,
    PAYROLL_INFO_STATUS,
    PAYROLL_INFO_ERRORS,
    PREVIEW_STATUS,
    SAVING_STATUS,
    SAVING_ERRORS,
    EMPLOYEE_PREVIEW,
    UPDATE_STORE_EMPLOYEES,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    UPLOAD_TIME_ATTENDANCE_INFO
} from './constants';

import { REINITIALIZE_PAGE } from '../App/constants';
import { resetStore } from '../App/sagas';

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* getEmployees( companyId, subscriptions ) {
    const urlSuffix = `${subscriptions.ta
        ? 'ta_employees' : 'employees'}${subscriptions.payroll
        ? '?include=payroll' : ''}`;

    try {
        yield put({
            type: LOADING_EMPLOYEES,
            payload: true
        });

        const response = yield call( Fetch, `/company/${companyId}/${urlSuffix}`, { method: 'GET' });

        if ( response.data.length > 0 ) {
            yield put({
                type: UPDATE_STORE_EMPLOYEES,
                payload: response.data
            });
        }
        yield put({
            type: LOADING_EMPLOYEES,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* uploadPersonalInfo({ payload }) {
    try {
        const data = new FormData();
        data.append( 'company_id', payload.company_id );
        data.append( 'file', payload.file );
        const upload = yield call( Fetch, '/employee/batch_add/personal_info', {
            method: 'POST',
            data
        });
        if ( upload.id ) {
            yield put({
                type: SET_JOB_ID,
                payload: upload.id
            });
            yield put({
                type: PERSONAL_INFO_STATUS,
                payload: 'validation_queued'
            });
        }

        yield call( checkValidation, { payload: { step: 'personal', company_id: payload.company_id, job_id: upload.id }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* uploadPayrollInfo({ payload }) {
    try {
        const data = new FormData();
        data.append( 'company_id', payload.company_id );
        data.append( 'file', payload.file );
        data.append( 'job_id', payload.job_id );
        const upload = yield call( Fetch, '/employee/batch_add/payroll_info', {
            method: 'POST',
            data
        });
        if ( upload.id ) {
            yield put({
                type: PAYROLL_INFO_STATUS,
                payload: 'validation_queued'
            });
            yield call( checkValidation, { payload: { step: 'payroll', company_id: payload.company_id, job_id: payload.job_id }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* uploadTimeAttendanceInfo({ payload }) {
    try {
        const data = new FormData();
        data.append( 'company_id', payload.company_id );
        data.append( 'file', payload.file );
        data.append( 'job_id', payload.job_id );
        const upload = yield call( Fetch, '/employee/batch_add/time_attendance_info', {
            method: 'POST',
            data
        });
        if ( upload.id ) {
            yield put({
                type: TIME_ATTENDANCE_INFO_STATUS,
                payload: 'validation_queued'
            });
            yield call( checkValidation, { payload: { step: 'time_attendance', company_id: payload.company_id, job_id: payload.job_id }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* saveEmployees({ payload }) {
    let step = payload.step;

    if ( !step ) {
        const isPayrollAndTaSubscribed = payload.subscriptions.payroll && payload.subscriptions.ta;
        const isOnlyPayrollSubscribed = payload.subscriptions.payroll && !payload.subscriptions.ta;

        if ( isPayrollAndTaSubscribed ) {
            step = 'save';
        } else {
            step = isOnlyPayrollSubscribed ? 'save' : 'ta_save';
        }
    }

    try {
        const upload = yield call( Fetch, `/employee/batch_add/${step}`, {
            method: 'POST',
            data: {
                company_id: payload.company_id,
                job_id: payload.job_id
            }
        });
        if ( upload.id ) {
            yield put({
                type: SAVING_STATUS,
                payload: 'save_queued'
            });

            yield call( checkValidation, { payload: {
                step,
                company_id: payload.company_id,
                job_id: payload.job_id,
                subscriptions: payload.subscriptions
            }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* getPreview({ payload }) {
    try {
        yield put({
            type: PREVIEW_STATUS,
            payload: 'busy'
        });
        const preview = yield call( Fetch, `/employee/batch_add/preview?company_id=${payload.company_id}&job_id=${payload.job_id}`, {
            method: 'GET'
        });
        yield put({
            type: EMPLOYEE_PREVIEW,
            payload: preview
        });
        yield put({
            type: PREVIEW_STATUS,
            payload: 'ready'
        });
    } catch ( error ) {
        yield put({
            type: PREVIEW_STATUS,
            payload: 'ready'
        });
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const check = yield call( Fetch, `/employee/batch_add/status?company_id=${payload.company_id}&job_id=${payload.job_id}&step=${payload.step}`, {
            method: 'GET'
        });
        if ( payload.step === 'save' ) {
            // checks if job is done with saving
            if ( check.status === 'save_failed' ) {
                yield put({
                    type: SAVING_STATUS,
                    payload: check.status
                });
                yield put({
                    type: SAVING_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'saved' ) {
                if ( payload.subscriptions && ( payload.subscriptions.payroll && payload.subscriptions.ta ) ) {
                    // calling save ta employees after payroll is saved if there are both products
                    yield call( saveEmployees, { payload: {
                        company_id: payload.company_id,
                        job_id: payload.job_id,
                        subscriptions: payload.subscriptions,
                        step: 'ta_save'
                    }});
                } else {
                    yield put({
                        type: SAVING_STATUS,
                        payload: check.status
                    });
                    yield call( getEmployees, payload.company_id, payload.subscriptions );
                }
            } else {
                // wait for 2 secs and check the status again
                yield call( delay, 2000 );
                yield call( checkValidation, { payload });
            }
        } else if ( payload.step === 'ta_save' ) {
            // checks if job is done with saving
            if ( check.status === 'save_failed' ) {
                yield put({
                    type: SAVING_STATUS,
                    payload: check.status
                });
                yield put({
                    type: SAVING_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'saved' ) {
                yield put({
                    type: SAVING_STATUS,
                    payload: check.status
                });
                yield call( getEmployees, payload.company_id, payload.subscriptions );
            } else {
                // wait for 2 secs and check the status again
                yield call( delay, 2000 );
                yield call( checkValidation, { payload });
            }
        } else if ( payload.step === 'personal' ) {
            // checks if job is done with validation
            if ( check.status === 'validation_failed' ) {
                yield put({
                    type: PERSONAL_INFO_STATUS,
                    payload: check.status
                });
                yield put({
                    type: PERSONAL_INFO_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'validated' ) {
                yield put({
                    type: PERSONAL_INFO_STATUS,
                    payload: check.status
                });
                yield put({
                    type: PERSONAL_INFO_ERRORS,
                    payload: {}
                });
            } else {
                yield put({
                    type: PERSONAL_INFO_STATUS,
                    payload: check.status
                });
                // wait for 2 secs and check the status again
                yield call( delay, 2000 );
                yield call( checkValidation, { payload: { step: payload.step, company_id: payload.company_id, job_id: payload.job_id }});
            }
        } else if ( payload.step === 'payroll' ) {
            // checks if job is done with validation
            if ( check.status === 'validation_failed' ) {
                yield put({
                    type: PAYROLL_INFO_STATUS,
                    payload: check.status
                });
                yield put({
                    type: PAYROLL_INFO_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'validated' ) {
                yield put({
                    type: PAYROLL_INFO_STATUS,
                    payload: check.status
                });
                yield put({
                    type: PAYROLL_INFO_ERRORS,
                    payload: {}
                });
            } else {
                yield put({
                    type: PAYROLL_INFO_STATUS,
                    payload: check.status
                });
                // wait for 2 secs and check the status again
                yield call( delay, 2000 );
                yield call( checkValidation, { payload: { step: payload.step, company_id: payload.company_id, job_id: payload.job_id }});
            }
        } else if ( payload.step === 'time_attendance' ) {
            // checks if job is done with validation
            if ( check.status === 'validation_failed' ) {
                yield put({
                    type: TIME_ATTENDANCE_INFO_STATUS,
                    payload: check.status
                });
                yield put({
                    type: TIME_ATTENDANCE_INFO_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'validated' ) {
                yield put({
                    type: TIME_ATTENDANCE_INFO_STATUS,
                    payload: check.status
                });
                yield put({
                    type: TIME_ATTENDANCE_INFO_ERRORS,
                    payload: {}
                });
            } else {
                yield put({
                    type: TIME_ATTENDANCE_INFO_STATUS,
                    payload: check.status
                });
                // wait for 2 secs and check the status again
                yield call( delay, 2000 );
                yield call( checkValidation, { payload: { step: payload.step, company_id: payload.company_id, job_id: payload.job_id }});
            }
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * resets the personal info store
 */
export function* resetPersonalInfo() {
    yield put({
        type: RESET_PERSONAL_INFO
    });
}

/**
 * resets the personal info store
 */
export function* resetPayrollInfo() {
    yield put({
        type: RESET_PAYROLL_INFO
    });
}

/**
 * resets the payroll info store
 */
export function* resetPreview() {
    yield put({
        type: RESET_PREVIEW
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Individual exports for testing
 */
export function* watchForPersonalInfoUpload() {
    const watcher = yield takeLatest( UPLOAD_PERSONAL_INFO, uploadPersonalInfo );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForPayrollInfoUpload() {
    const watcher = yield takeLatest( UPLOAD_PAYROLL_INFO, uploadPayrollInfo );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForTimeAttendanceInfoUpload() {
    const watcher = yield takeLatest( UPLOAD_TIME_ATTENDANCE_INFO, uploadTimeAttendanceInfo );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForEmployeeUploadStatus() {
    const watcher = yield takeLatest( BATCH_UPLOAD_STATUS, checkValidation );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetPreview() {
    const watcher = yield takeEvery( GET_PREVIEW, getPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForSaveEmployees() {
    const watcher = yield takeEvery( SAVE_EMPLOYEES, saveEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForResetPersonalInfo() {
    const watcher = yield takeEvery( RESET_PERSONAL_INFO, resetPersonalInfo );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForResetPayrollInfo() {
    const watcher = yield takeEvery( RESET_PAYROLL_INFO, resetPayrollInfo );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForResetPreview() {
    const watcher = yield takeEvery( RESET_PREVIEW, resetPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForPersonalInfoUpload,
    watchForPayrollInfoUpload,
    watchForTimeAttendanceInfoUpload,
    watchForEmployeeUploadStatus,
    watchForGetPreview,
    watchForSaveEmployees,
    watchForResetPersonalInfo,
    watchForResetPayrollInfo,
    watchForResetPreview,
    watchForNotifyUser,
    watchForReinitializePage
];
