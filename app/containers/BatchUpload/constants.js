/*
 *
 * Setup constants
 *
 */
export const GET_EMPLOYEES = 'app/BatchUpload/GET_EMPLOYEES';
export const LOADING_EMPLOYEES = 'app/BatchUpload/LOADING_EMPLOYEES';
export const UPDATE_STORE_EMPLOYEES = 'app/BatchUpload/UPDATE_STORE_EMPLOYEES';
export const UPDATE_EMPLOYEES_STATUS = 'app/BatchUpload/UPDATE_EMPLOYEES_STATUS';

export const UPLOAD_PERSONAL_INFO = 'app/BatchUpload/UPLOAD_PERSONAL_INFO';
export const UPLOAD_PAYROLL_INFO = 'app/BatchUpload/UPLOAD_PAYROLL_INFO';
export const UPLOAD_TIME_ATTENDANCE_INFO = 'app/BatchUpload/UPLOAD_TIME_ATTENDANCE_INFO';
export const SAVE_EMPLOYEES = 'app/BatchUpload/SAVE_EMPLOYEES';
export const BATCH_UPLOAD_STATUS = 'app/BatchUpload/BATCH_UPLOAD_STATUS';
export const SET_JOB_ID = 'app/BatchUpload/SET_JOB_ID';

export const PERSONAL_INFO_STATUS = 'app/BatchUpload/PERSONAL_INFO_STATUS';
export const PERSONAL_INFO_ERRORS = 'app/BatchUpload/PERSONAL_INFO_ERRORS';
export const RESET_PERSONAL_INFO = 'app/BatchUpload/RESET_PERSONAL_INFO';

export const TIME_ATTENDANCE_INFO_STATUS = 'app/BatchUpload/TIME_ATTENDANCE_INFO_STATUS';
export const TIME_ATTENDANCE_INFO_ERRORS = 'app/BatchUpload/TIME_ATTENDANCE_INFO_ERRORS';
export const RESET_TIME_ATTENDANCE_INFO = 'app/BatchUpload/RESET_TIME_ATTENDANCE_INFO';

export const PAYROLL_INFO_STATUS = 'app/BatchUpload/PAYROLL_INFO_STATUS';
export const PAYROLL_INFO_ERRORS = 'app/BatchUpload/PAYROLL_INFO_ERRORS';
export const RESET_PAYROLL_INFO = 'app/BatchUpload/RESET_PAYROLL_INFO';

export const GET_PREVIEW = 'app/BatchUpload/GET_PREVIEW';
export const PREVIEW_STATUS = 'app/BatchUpload/PREVIEW_STATUS';
export const EMPLOYEE_PREVIEW = 'app/BatchUpload/EMPLOYEE_PREVIEW';
export const RESET_PREVIEW = 'app/BatchUpload/RESET_PREVIEW';

export const SAVING_STATUS = 'app/BatchUpload/SAVING_STATUS';
export const SAVING_ERRORS = 'app/BatchUpload/SAVING_ERRORS';

export const NOTIFICATION = 'app/BatchUpload/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/BatchUpload/NOTIFICATION_SAGA';
