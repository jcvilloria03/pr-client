import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import SubHeader from '../SubHeader';
import SnackBar from '../../components/SnackBar';
import A from '../../components/A';
import Toggle from '../../components/Toggle';
import Table from '../../components/Table';
import Clipboard from '../../components/Clipboard';
import { H4, H6, P } from '../../components/Typography';
import Stepper from '../../components/Stepper';
import FileInput from '../../components/FileInput';
import Button from '../../components/Button';

import { browserHistory } from '../../utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../utils/constants';
import { subscriptionService } from '../../utils/SubscriptionService';
import { formatDate } from '../../utils/functions';

import * as batchUploadActions from './actions';
import {
    PageWrapper,
    StyledLoader
} from './styles';
import {
    makeSelectEmployees,
    makeSelectNotification,
    makeSelectProductsState
} from './selectors';

/**
 * EmployeeBatchUpload Container
 */
export class EmployeeBatchUpload extends React.Component {
    static propTypes = {
        uploadPersonalInfo: React.PropTypes.func,
        uploadTimeAttendanceInfo: React.PropTypes.func,
        uploadPayrollInfo: React.PropTypes.func,
        getPreview: React.PropTypes.func,
        saveEmployees: React.PropTypes.func,
        employees: React.PropTypes.shape({
            data: React.PropTypes.arrayOf(
                React.PropTypes.object
            ),
            job_id: React.PropTypes.string,
            personal_info: React.PropTypes.shape({
                status: React.PropTypes.string,
                errors: React.PropTypes.object
            }).isRequired,
            payroll_info: React.PropTypes.shape({
                status: React.PropTypes.string,
                errors: React.PropTypes.object
            }).isRequired,
            time_attendance_info: React.PropTypes.shape({
                status: React.PropTypes.string,
                errors: React.PropTypes.object
            }).isRequired,
            preview: React.PropTypes.shape({
                status: React.PropTypes.string,
                data: React.PropTypes.array
            }),
            saving: React.PropTypes.shape({
                status: React.PropTypes.string,
                errors: React.PropTypes.object
            }).isRequired
        }).isRequired,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    }

    /**
     * component's constructor
     */
    constructor( props ) {
        super( props );

        const salariumProfile = JSON.parse( localStorage.getItem( 'user' ) );

        this.state = {
            step: 0,

            form: {
                company_id: salariumProfile.last_active_company_id,
                job_id: '',
                personal_info: null,
                time_attendance_info: null,
                payroll_info: null
            },

            personal_info_status: null,
            personal_info_errors: {},
            personal_info_clipboard: '',
            personal_info_submit: false,

            time_attendance_info_status: null,
            time_attendance_info_errors: {},
            time_attendance_info_clipboard: '',
            time_attendance_info_submit: false,

            payroll_info_status: null,
            payroll_info_errors: {},
            payroll_info_clipboard: '',
            payroll_info_submit: false,

            saving_status: null,
            saving_errors: {},
            saving_clipboard: '',
            saving_submit: false,

            preview: {
                data: [],
                status: 'ready'
            }
        };

        this.handleStepper = this.handleStepper.bind( this );
        this.getPersonalInfoSection = this.getPersonalInfoSection.bind( this );
        this.getTimeAttendanceInfoSection = this.getTimeAttendanceInfoSection.bind( this );
        this.getPayrollInfoSection = this.getPayrollInfoSection.bind( this );
        this.getPreview = this.getPreview.bind( this );
        this.formatCurrency = this.formatCurrency.bind( this );
    }

    /**
     * this displays all existing locations onload
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.employees.personal_info.status !== this.props.employees.personal_info.status && this.setState({
            personal_info_status: nextProps.employees.personal_info.status
        }, () => {
            this.personalInfoNext.setState({ disabled: nextProps.employees.personal_info.status !== 'validated' });
            this.validatePersonalInfo.setState({ disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.employees.personal_info.status ) });
        });
        nextProps.employees.personal_info.errors !== this.props.employees.personal_info.errors && this.setState({
            personal_info_errors: nextProps.employees.personal_info.errors
        }, () => {
            let clipboard = '';
            const rows = Object.keys( nextProps.employees.personal_info.errors );
            if ( rows.length ) {
                rows.forEach( ( row ) => {
                    nextProps.employees.personal_info.errors[ row ].forEach( ( error ) => {
                        clipboard += `Row #${Number( row ) + 1}, ${error}\n`;
                    });
                });
            }

            this.setState({ personal_info_clipboard: clipboard });
        });

        nextProps.employees.payroll_info.status !== this.props.employees.payroll_info.status && this.setState({
            payroll_info_status: nextProps.employees.payroll_info.status
        }, () => {
            this.payrollInfoNext.setState({ disabled: nextProps.employees.payroll_info.status !== 'validated' });
            this.validatePayrollInfo.setState({ disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.employees.payroll_info.status ) });
        });
        nextProps.employees.payroll_info.errors !== this.props.employees.payroll_info.errors && this.setState({
            payroll_info_errors: nextProps.employees.payroll_info.errors
        }, () => {
            let clipboard = '';
            const rows = Object.keys( nextProps.employees.payroll_info.errors );
            if ( rows.length ) {
                rows.forEach( ( row ) => {
                    nextProps.employees.payroll_info.errors[ row ].forEach( ( error ) => {
                        clipboard += `Row #${Number( row ) + 1}, ${error}\n`;
                    });
                });
            }

            this.setState({ payroll_info_clipboard: clipboard });
        });

        nextProps.employees.time_attendance_info.status !== this.props.employees.time_attendance_info.status && this.setState({
            time_attendance_info_status: nextProps.employees.time_attendance_info.status
        }, () => {
            this.timeAttendanceInfoNext.setState({ disabled: nextProps.employees.time_attendance_info.status !== 'validated' });
            this.validateTimeAttendanceInfo.setState({ disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.employees.time_attendance_info.status ) });
        });
        nextProps.employees.time_attendance_info.errors !== this.props.employees.time_attendance_info.errors && this.setState({
            time_attendance_info_errors: nextProps.employees.time_attendance_info.errors
        }, () => {
            let clipboard = '';
            const rows = Object.keys( nextProps.employees.time_attendance_info.errors );
            if ( rows.length ) {
                rows.forEach( ( row ) => {
                    const errorArrays = Object.values( nextProps.employees.time_attendance_info.errors[ row ]);
                    const errors = errorArrays.flat();

                    errors.forEach( ( error ) => {
                        clipboard += `Row #${Number( row ) + 1}, ${error}\n`;
                    });
                });
            }

            this.setState({ time_attendance_info_clipboard: clipboard });
        });

        nextProps.employees.job_id !== this.props.employees.job_id && this.setState({
            form: Object.assign( this.state.form, { job_id: nextProps.employees.job_id })
        });

        nextProps.employees.preview.data !== this.props.employees.preview.data && this.setState({
            preview: Object.assign( this.state.preview, { data: nextProps.employees.preview.data })
        });

        nextProps.employees.preview.status !== this.props.employees.preview.status && this.setState({
            preview: Object.assign( this.state.preview, { status: nextProps.employees.preview.status })
        });

        nextProps.employees.saving.status !== this.props.employees.saving.status && this.setState({
            saving_status: nextProps.employees.saving.status
        }, () => {
            this.saveSubmit.setState({ disabled: nextProps.employees.saving.status !== 'save_failed' }, () => {
                nextProps.employees.saving.status === 'saved' && browserHistory.push( '/employees', true );
            });
        });
    }

    /**
     * this renders the personal infomation upload section of the add employees
     */
    getPersonalInfoSection() {
        let errorDisplay = [];

        const errorList = this.state.personal_info_errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'INDEX',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'ERROR',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            const keys = Object.keys( errorList );

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row #{ Number( key ) + 1 }</H4>,
                    error: <ul>{errorList[ key ].map( ( error, index ) => <li key={ index } style={ { margin: '0' } } >{error}</li> )}</ul>
                });
            });

            errorDisplay = (
                <div className="errors">
                    <H4>ERROR OCCURED:</H4>
                    <div style={ { textAlign: 'right' } }><Clipboard value={ this.state.personal_info_clipboard } /></div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                    />
                </div>
            );
        }
        return (
            <div className="personal" style={ { display: this.checkPersonalInfoVisibility() ? '' : 'none' } }>
                <div className="row">
                    <div className="col-xs-12">
                        <H4>Add Personal Information</H4>
                        <P>Provide your employees&#39; personal information here.</P>
                    </div>
                </div>
                <div className="row steps">
                    <div className="step col-sm-6">
                        <div className="template">
                            <H6>Step 1:</H6>
                            <p>Download and fill-out the Employee Personal Information Template.</p>
                            <P><A target="_blank" href="/payroll/guides/employees/batch-upload#personal-guide">Click here to view the upload guide.</A></P>
                            <A href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/employee_personal_info_template.csv" download>Download Template</A>
                        </div>
                    </div>
                    <div className="step col-sm-6">
                        <div className="upload">
                            <H6>Step 2:</H6>
                            <p>After completely filling out the template, choose and upload it here.</p>
                            <div style={ { display: this.state.personal_info_status === 'validating' || this.state.personal_info_status === 'validation_queued' ? 'none' : 'block' } } >
                                <FileInput
                                    accept=".csv"
                                    onDrop={ ( files ) => {
                                        const { acceptedFiles } = files;
                                        this.personalInfoNext.setState({ disabled: true });
                                        this.setState({
                                            form: Object.assign( this.state.form, {
                                                personal_info: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null
                                            }),
                                            personal_info_errors: {},
                                            personal_info_status: null
                                        }, () => {
                                            this.validatePersonalInfo.setState({ disabled: acceptedFiles.length <= 0 });
                                        });
                                    } }
                                    ref={ ( ref ) => { this.personalInput = ref; } }
                                />
                            </div>
                            <div style={ { display: this.state.personal_info_status === 'validated' ? 'block' : 'none' } }>
                                <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                            </div>
                            <div style={ { display: this.state.personal_info_status === 'validating' || this.state.personal_info_status === 'validation_queued' || this.state.personal_info_status === 'validated' ? 'none' : 'block' } } >
                                <Button
                                    label={
                                        this.state.personal_info_submit ? (
                                            <StyledLoader className="animation">
                                                UPLOADING <div className="anim3"></div>
                                            </StyledLoader>
                                        ) : (
                                            'UPLOAD'
                                        )
                                    }
                                    size="large"
                                    disabled={ this.state.form.personal_info ? this.state.form.personal_info.length <= 0 : true }
                                    type="action"
                                    ref={ ( ref ) => { this.validatePersonalInfo = ref; } }
                                    onClick={ () => {
                                        this.setState({
                                            personal_info_errors: {},
                                            personal_info_status: null,
                                            personal_info_submit: true
                                        }, () => {
                                            this.validatePersonalInfo.setState({ disabled: true }, () => {
                                                this.props.uploadPersonalInfo( this.state.form.personal_info, this.state.form.company_id );
                                                setTimeout( () => {
                                                    this.setState({ personal_info_submit: false });
                                                }, 5000 );
                                            });
                                        });
                                    } }
                                />
                            </div>
                            <div style={ { display: this.state.personal_info_status === 'validating' || this.state.personal_info_status === 'validation_queued' ? 'block' : 'none' } } >
                                <StyledLoader className="animation">
                                    <H4 style={ { margin: '0' } }>{ this.state.personal_info_status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                </StyledLoader>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="action_button">
                    <Button
                        disabled={ this.state.personal_info_status !== 'validated' }
                        label="Next"
                        size="large"
                        type="action"
                        ref={ ( ref ) => { this.personalInfoNext = ref; } }
                        onClick={ () => {
                            this.setState({ step: this.state.step + 1 });
                        } }
                    />
                </div>
                { errorDisplay }
            </div>
        );
    }

    /**
     * this renders the time and attendance infomation upload section of the add employees
     */
    getTimeAttendanceInfoSection() {
        let errorDisplay = [];

        const errorList = this.state.time_attendance_info_errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'INDEX',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'ERROR',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            const keys = Object.keys( errorList );

            keys.forEach( ( key ) => {
                const errorArrays = Object.values( errorList[ key ]);
                const errors = errorArrays.flat();

                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row #{ Number( key ) + 1 }</H4>,
                    error: <ul>{errors.map( ( error, index ) => <li key={ index } style={ { margin: '0' } } >{error}</li> )}</ul>
                });
            });

            errorDisplay = (
                <div className="errors">
                    <H4>ERROR OCCURED:</H4>
                    <div style={ { textAlign: 'right' } }><Clipboard value={ this.state.time_attendance_info_clipboard } /></div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                    />
                </div>
            );
        }
        return (
            <div className="payroll" style={ { display: this.checkTimeAttendanceVisibility() ? '' : 'none' } }>
                <div className="row">
                    <div className="col-xs-12">
                        <H4>Add Time and Attendance Information</H4>
                        <P>Provide your employees&#39; time and attendance data.</P>
                    </div>
                </div>
                <div className="row steps">
                    <div className="step col-sm-6">
                        <div className="template">
                            <H4>Step 1:</H4>
                            <P>Download and fill-out the Employee Time and Attendance Information Template.</P>
                            <P><A target="_blank" href="/payroll/guides/employees/batch-upload#time-attendance-guide">Click here to view the upload guide.</A></P>
                            <A href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/employee_time_attendance_info_template.csv" download>Download Template</A>
                        </div>
                    </div>
                    <div className="step col-sm-6">
                        <div className="upload">
                            <H4>Step 2:</H4>
                            <P>After completely filling out the template, choose and upload it here.</P>
                            <div style={ { display: this.state.time_attendance_info_status === 'validating' || this.state.time_attendance_info_status === 'validation_queued' ? 'none' : 'block' } } >
                                <FileInput
                                    accept=".csv"
                                    onDrop={ ( files ) => {
                                        const { acceptedFiles } = files;
                                        this.timeAttendanceInfoNext.setState({ disabled: true });
                                        this.setState({
                                            form: Object.assign( this.state.form, {
                                                time_attendance_info: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null
                                            }),
                                            time_attendance_info_errors: {},
                                            time_attendance_info_status: null
                                        }, () => {
                                            this.validateTimeAttendanceInfo.setState({ disabled: acceptedFiles.length <= 0 });
                                        });
                                    } }
                                    ref={ ( ref ) => { this.timeAttendanceInput = ref; } }
                                />
                            </div>
                            <div style={ { display: this.state.time_attendance_info_status === 'validated' ? 'block' : 'none' } }>
                                <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                            </div>
                            <div style={ { display: this.state.time_attendance_info_status === 'validating' || this.state.time_attendance_info_status === 'validation_queued' || this.state.time_attendance_info_status === 'validated' ? 'none' : 'block' } } >
                                <Button
                                    label={
                                        this.state.time_attendance_info_submit ? (
                                            <StyledLoader className="animation">
                                                UPLOADING <div className="anim3"></div>
                                            </StyledLoader>
                                        ) : (
                                            'UPLOAD'
                                        )
                                    }
                                    size="large"
                                    disabled={ this.state.form.time_attendance_info ? this.state.form.time_attendance_info.length <= 0 : true }
                                    type="action"
                                    ref={ ( ref ) => { this.validateTimeAttendanceInfo = ref; } }
                                    onClick={ () => {
                                        this.setState({
                                            time_attendance_info_errors: {},
                                            time_attendance_info_status: null,
                                            time_attendance_info_submit: true
                                        }, () => {
                                            this.validateTimeAttendanceInfo.setState({ disabled: true }, () => {
                                                this.props.uploadTimeAttendanceInfo( this.state.form.time_attendance_info, this.state.form.company_id, this.state.form.job_id );
                                                setTimeout( () => {
                                                    this.validateTimeAttendanceInfo.setState({ disabled: false });
                                                    this.setState({ time_attendance_info_submit: false });
                                                }, 5000 );
                                            });
                                        });
                                    } }
                                />
                            </div>
                            <div style={ { display: this.state.time_attendance_info_status === 'validating' || this.state.time_attendance_info_status === 'validation_queued' ? 'block' : 'none' } } >
                                <StyledLoader className="animation">
                                    <H4 style={ { margin: '0' } }>{ this.state.time_attendance_info_status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                </StyledLoader>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="action_button">
                    <Button
                        disabled={ this.state.time_attendance_info_status !== 'validated' }
                        label="Next"
                        size="large"
                        type="action"
                        ref={ ( ref ) => { this.timeAttendanceInfoNext = ref; } }
                        onClick={ () => {
                            if ( this.isStepBeforeUploadPreview() ) {
                                this.props.getPreview( this.state.form.company_id, this.state.form.job_id );
                            }
                            this.setState({ step: this.state.step + 1 });
                        } }
                    />
                </div>
                { errorDisplay }
            </div>
        );
    }

    /**
     * this renders the payroll infomation upload section of the add employees
     */
    getPayrollInfoSection() {
        let errorDisplay = [];

        const errorList = this.state.payroll_info_errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'INDEX',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'ERROR',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            const keys = Object.keys( errorList );

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row #{ Number( key ) + 1 }</H4>,
                    error: <ul>{errorList[ key ].map( ( error, index ) => <li key={ index } style={ { margin: '0' } } >{error}</li> )}</ul>
                });
            });

            errorDisplay = (
                <div className="errors">
                    <H4>ERROR OCCURED:</H4>
                    <div style={ { textAlign: 'right' } }><Clipboard value={ this.state.payroll_info_clipboard } /></div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                    />
                </div>
            );
        }
        return (
            <div className="payroll" style={ { display: this.checkPayrollVisibility() ? '' : 'none' } }>
                <div className="row">
                    <div className="col-xs-12">
                        <H4>Add Payroll Information</H4>
                        <P>Provide details like your employees&#39; basic salaries, tax statuses, payroll groups, and government numbers.</P>
                    </div>
                </div>
                <div className="row steps">
                    <div className="step col-sm-6">
                        <div className="template">
                            <H4>Step 1:</H4>
                            <P>Download and fill-out the Employee Payroll Information Template.</P>
                            <P><A target="_blank" href="/payroll/guides/employees/batch-upload#payroll-guide">Click here to view the upload guide.</A></P>
                            <A href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/employee_payroll_info_template.csv" download>Download Template</A>
                        </div>
                    </div>
                    <div className="step col-sm-6">
                        <div className="upload">
                            <H4>Step 2:</H4>
                            <P>After completely filling out the template, choose and upload it here.</P>
                            <div style={ { display: this.state.payroll_info_status === 'validating' || this.state.payroll_info_status === 'validation_queued' ? 'none' : 'block' } } >
                                <FileInput
                                    accept=".csv"
                                    onDrop={ ( files ) => {
                                        const { acceptedFiles } = files;
                                        this.payrollInfoNext.setState({ disabled: true });
                                        this.setState({
                                            form: Object.assign( this.state.form, {
                                                payroll_info: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null
                                            }),
                                            payroll_info_errors: {},
                                            payroll_info_status: null
                                        }, () => {
                                            this.validatePayrollInfo.setState({ disabled: acceptedFiles.length <= 0 });
                                        });
                                    } }
                                    ref={ ( ref ) => { this.payrollInput = ref; } }
                                />
                            </div>
                            <div style={ { display: this.state.payroll_info_status === 'validated' ? 'block' : 'none' } }>
                                <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                            </div>
                            <div style={ { display: this.state.payroll_info_status === 'validating' || this.state.payroll_info_status === 'validation_queued' || this.state.payroll_info_status === 'validated' ? 'none' : 'block' } } >
                                <Button
                                    label={
                                        this.state.payroll_info_submit ? (
                                            <StyledLoader className="animation">
                                                UPLOADING <div className="anim3"></div>
                                            </StyledLoader>
                                        ) : (
                                            'UPLOAD'
                                        )
                                    }
                                    size="large"
                                    disabled={ this.state.form.payroll_info ? this.state.form.payroll_info.length <= 0 : true }
                                    type="action"
                                    ref={ ( ref ) => { this.validatePayrollInfo = ref; } }
                                    onClick={ () => {
                                        this.setState({
                                            payroll_info_errors: {},
                                            payroll_info_status: null,
                                            payroll_info_submit: true
                                        }, () => {
                                            this.validatePayrollInfo.setState({ disabled: true }, () => {
                                                this.props.uploadPayrollInfo( this.state.form.payroll_info, this.state.form.company_id, this.state.form.job_id );
                                                setTimeout( () => {
                                                    this.validatePayrollInfo.setState({ disabled: false });
                                                    this.setState({ payroll_info_submit: false });
                                                }, 5000 );
                                            });
                                        });
                                    } }
                                />
                            </div>
                            <div style={ { display: this.state.payroll_info_status === 'validating' || this.state.payroll_info_status === 'validation_queued' ? 'block' : 'none' } } >
                                <StyledLoader className="animation">
                                    <H4 style={ { margin: '0' } }>{ this.state.payroll_info_status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                </StyledLoader>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="action_button">
                    <Button
                        disabled={ this.state.payroll_info_status !== 'validated' }
                        label="Next"
                        size="large"
                        type="action"
                        ref={ ( ref ) => { this.payrollInfoNext = ref; } }
                        onClick={ () => {
                            if ( this.isStepBeforeUploadPreview() ) {
                                this.props.getPreview( this.state.form.company_id, this.state.form.job_id );
                            }
                            this.setState({ step: this.state.step + 1 });
                        } }
                    />
                </div>
                { errorDisplay }
            </div>
        );
    }

    /**
     * Table Preview of validated data.
     */
    getPreview() {
        const previewData = [];
        const previewList = this.state.preview.data;
        const columns = [
            {
                header: 'EMPLOYEE ID',
                accessor: 'employee_id',
                minWidth: 150
            },
            {
                header: 'LAST NAME',
                accessor: 'last_name',
                minWidth: 200
            },
            {
                header: 'FIRST NAME',
                accessor: 'first_name',
                minWidth: 200
            },
            {
                header: 'MIDDLE NAME',
                accessor: 'middle_name',
                minWidth: 200
            },
            {
                header: 'EMAIL',
                accessor: 'email',
                minWidth: 300
            },
            {
                header: 'TEL. NO.',
                accessor: 'telephone_number',
                minWidth: 110
            },
            {
                header: 'MOBILE NO.',
                accessor: 'mobile_number',
                minWidth: 140
            },
            {
                header: 'ADDRESS LINE 1',
                accessor: 'address_line_1',
                minWidth: 250
            },
            {
                header: 'ADDRESS LINE 2',
                accessor: 'address_line_2',
                minWidth: 250
            },
            {
                header: 'CITY',
                accessor: 'city',
                minWidth: 120
            },
            {
                header: 'ZIP CODE',
                accessor: 'zip_code',
                minWidth: 120
            },
            {
                header: 'COUNTRY',
                accessor: 'country',
                minWidth: 120
            },
            {
                header: 'BIRTHDATE',
                accessor: 'birth_date',
                minWidth: 150
            },
            {
                header: 'GENDER',
                accessor: 'gender',
                minWidth: 120
            }
        ];

        if ( this.isTaSubscribed() || this.isPayrollSubscribed() ) {
            columns.push( ...[
                {
                    header: 'DATE HIRED',
                    accessor: 'date_hired',
                    minWidth: 150
                },
                {
                    header: 'DATE ENDED',
                    accessor: 'date_ended',
                    minWidth: 150
                },
                {
                    header: 'HOURS PER DAY',
                    accessor: 'hours_per_day',
                    minWidth: 150
                },
                {
                    header: 'DEPARTMENT',
                    accessor: 'department_name',
                    minWidth: 150
                },
                {
                    header: 'RANK',
                    accessor: 'rank_name',
                    minWidth: 150
                }
            ]);
        }

        if ( this.isTaSubscribed() ) {
            columns.push( ...[
                {
                    header: 'STATUS',
                    accessor: 'status',
                    minWidth: 150
                },
                {
                    header: 'EMPLOYMENT TYPE',
                    accessor: 'employment_type_name',
                    minWidth: 150
                },
                {
                    header: 'POSITION',
                    accessor: 'position_name',
                    minWidth: 150
                },
                {
                    header: 'TEAM',
                    accessor: 'team_name',
                    minWidth: 150
                },
                {
                    header: 'TEAM ROLE',
                    accessor: 'team_role',
                    minWidth: 150
                },
                {
                    header: 'PRIMARY LOCATION',
                    accessor: 'primary_location_name',
                    minWidth: 150
                },
                {
                    header: 'SECONDARY LOCATION',
                    accessor: 'secondary_location_name',
                    minWidth: 150
                },
                {
                    header: 'TIMESHEET REQUIRED?',
                    accessor: 'timesheet_required',
                    minWidth: 150
                },
                {
                    header: 'ENTITLED TO OVERTIME?',
                    accessor: 'overtime',
                    minWidth: 150
                },
                {
                    header: 'ENTITLED NIGHT DIFFERENTIAL?',
                    accessor: 'differential',
                    minWidth: 150
                },
                {
                    header: 'ENTITLED UNWORKED REGULAR HOLIDAY PAY?',
                    accessor: 'regular_holiday_pay',
                    minWidth: 150
                },
                {
                    header: 'ENTITLED UNWORKED SPECIAL HOLIDAY PAY?',
                    accessor: 'special_holiday_pay',
                    minWidth: 150
                },
                {
                    header: 'ENTITLED HOLIDAY PREMIUM PAY?',
                    accessor: 'holiday_premium_pay',
                    minWidth: 150
                },
                {
                    header: 'ENTITLED REST DAY PAY?',
                    accessor: 'rest_day_pay',
                    minWidth: 150
                }
            ]);
        }

        if ( this.isPayrollSubscribed() ) {
            columns.push( ...[
                {
                    header: 'BASE PAY',
                    accessor: 'base_pay',
                    minWidth: 180,
                    style: { justifyContent: 'flex-end', paddingRight: '10px' }
                },
                {
                    header: 'BASE PAY UNIT',
                    accessor: 'base_pay_unit',
                    minWidth: 180
                },
                {
                    header: 'TAX TYPE',
                    accessor: 'tax_type',
                    minWidth: 150
                },
                {
                    header: 'CONSULTANT TAX RATE',
                    accessor: 'consultant_tax_rate',
                    minWidth: 220
                },
                {
                    header: 'TAX STATUS',
                    accessor: 'tax_status',
                    minWidth: 150
                },
                {
                    header: 'PAYROLL GROUP',
                    accessor: 'payroll_group_name',
                    minWidth: 150
                },
                {
                    header: 'COST CENTER',
                    accessor: 'cost_center_name',
                    minWidth: 150
                },
                {
                    header: 'TIN',
                    accessor: 'tin',
                    minWidth: 150
                },
                {
                    header: 'SSS NO.',
                    accessor: 'sss_number',
                    minWidth: 150
                },
                {
                    header: 'HDMF NO.',
                    accessor: 'hdmf_number',
                    minWidth: 150
                },
                {
                    header: 'PHILHEALTH NO.',
                    accessor: 'philhealth_number',
                    minWidth: 150
                },
                {
                    header: 'SSS BASIS',
                    accessor: 'sss_basis',
                    minWidth: 150
                },
                {
                    header: 'SSS AMOUNT',
                    accessor: 'sss_amount',
                    minWidth: 150,
                    style: { justifyContent: 'flex-end', paddingRight: '10px' },
                    render: ( row ) => (
                        this.formatCurrency( row.value )
                    )
                },
                {
                    header: 'HDMF BASIS',
                    accessor: 'hdmf_basis',
                    minWidth: 150
                },
                {
                    header: 'HDMF AMOUNT',
                    accessor: 'hdmf_amount',
                    minWidth: 150,
                    style: { justifyContent: 'flex-end', paddingRight: '10px' },
                    render: ( row ) => (
                        this.formatCurrency( row.value )
                    )
                },
                {
                    header: 'PHILHEALTH BASIS',
                    accessor: 'philhealth_basis',
                    minWidth: 150
                },
                {
                    header: 'PHILHEALTH AMOUNT',
                    accessor: 'philhealth_amount',
                    minWidth: 150,
                    style: { justifyContent: 'flex-end', paddingRight: '10px' },
                    render: ( row ) => (
                        this.formatCurrency( row.value )
                    )
                },
                {
                    header: 'WORK LOCATION',
                    accessor: 'work_location',
                    minWidth: 150
                }
            ]);
        }

        if ( previewList.length ) {
            previewList.forEach( ( employee ) => {
                previewData.push({
                    employee_id: employee.employee_id,
                    last_name: employee.last_name,
                    first_name: employee.first_name,
                    middle_name: employee.middle_name,
                    email: employee.email,
                    telephone_number: employee.telephone_number,
                    mobile_number: employee.mobile_number,
                    address_line_1: employee.address_line_1,
                    address_line_2: employee.address_line_2,
                    city: employee.city,
                    zip_code: employee.zip_code,
                    birth_date: employee.birth_date ? formatDate( employee.birth_date, DATE_FORMATS.DISPLAY ) : '',
                    gender: employee.gender,
                    country: employee.country,
                    date_hired: employee.date_hired ? formatDate( employee.date_hired, DATE_FORMATS.DISPLAY ) : '',
                    date_ended: employee.date_ended ? formatDate( employee.date_ended, DATE_FORMATS.DISPLAY ) : '',
                    hours_per_day: employee.hours_per_day,
                    department_name: employee.department_name,
                    rank_name: employee.rank_name,
                    status: employee.status,
                    employment_type_name: employee.employment_type_name,
                    position_name: employee.position_name,
                    team_name: employee.team_name,
                    team_role: employee.team_role,
                    primary_location_name: employee.primary_location_name,
                    secondary_location_name: employee.secondary_location_name,
                    timesheet_required: employee.timesheet_required ? 'Yes' : 'No',
                    overtime: employee.overtime ? 'Yes' : 'No',
                    differential: employee.differential ? 'Yes' : 'No',
                    regular_holiday_pay: employee.regular_holiday_pay ? 'Yes' : 'No',
                    special_holiday_pay: employee.special_holiday_pay ? 'Yes' : 'No',
                    holiday_premium_pay: employee.holiday_premium_pay ? 'Yes' : 'No',
                    rest_day_pay: employee.rest_day_pay ? 'Yes' : 'No',
                    base_pay: employee.base_pay,
                    base_pay_unit: employee.base_pay_unit,
                    tax_type: employee.tax_type,
                    consultant_tax_rate: employee.consultant_tax_rate ? employee.consultant_tax_rate : '',
                    tax_status: employee.tax_status,
                    payroll_group_name: employee.payroll_group_name,
                    cost_center_name: employee.cost_center_name,
                    tin: employee.tin,
                    sss_number: employee.sss_number,
                    hdmf_number: employee.hdmf_number,
                    philhealth_number: employee.philhealth_number,
                    sss_basis: employee.sss_basis,
                    sss_amount: employee.sss_amount,
                    hdmf_basis: employee.hdmf_basis,
                    hdmf_amount: employee.hdmf_amount,
                    philhealth_basis: employee.philhealth_basis,
                    philhealth_amount: employee.philhealth_amount,
                    work_location: employee.work_location
                });
            });
        }
        return this.state.preview.status === 'ready' ? (
            <div className="preview" style={ { display: this.checkPreviewVisibility() ? '' : 'none' } }>
                <br />
                <H4>Data Preview:</H4>
                <Table
                    columns={ columns }
                    data={ previewData }
                    pagination={ previewData.length > 20 }
                    sizeOptions={ previewData.length > 20 ? [ 10, 20, 50, 100, 200 ] : [previewData.length] }
                    pageSize={ previewData.length > 20 ? 20 : previewData.length }
                />
                <div className="previewActions" style={ { textAlign: 'center', margin: '20px' } }>
                    <P>**NOTE: Clicking change will take you back to <strong>Personal Information</strong> section and previously validated data will be lost.</P>
                    <Button
                        label="CHANGE"
                        alt
                        size="large"
                        onClick={ () => {
                            this.setState({
                                step: 0,
                                form: {
                                    company_id: this.state.form.company_id,
                                    job_id: '',
                                    personal_info: null,
                                    time_attendance_info: null,
                                    payroll_info: null
                                },
                                personal_info_status: null,
                                personal_info_errors: {},
                                personal_info_submit: false,

                                time_attendance_info_status: null,
                                time_attendance_info_errors: {},
                                time_attendance_info_submit: false,

                                payroll_info_status: null,
                                payroll_info_errors: {},
                                payroll_info_submit: false,

                                preview: {
                                    data: [],
                                    status: 'ready'
                                }
                            }, () => {
                                const changes = {
                                    label: 'Try dropping some files',
                                    files: []
                                };
                                this.personalInfoNext.setState({ disabled: true });
                                this.timeAttendanceInfoNext.setState({ disabled: true });
                                this.payrollInfoNext.setState({ disabled: true });
                                this.personalInput.setState( changes );
                                this.timeAttendanceInput.setState( changes );
                                this.payrollInput.setState( changes );
                            });
                        } }
                    />
                    <Button
                        ref={ ( ref ) => { this.saveSubmit = ref; } }
                        label={
                            this.state.saving_status === 'save_queued' || this.state.saving_status === 'saving' ?
                                (
                                    <StyledLoader className="animation">
                                        SAVING <div className="anim3"></div>
                                    </StyledLoader>
                                ) : 'SAVE'
                        }
                        size="large"
                        onClick={ () => {
                            this.saveSubmit.setState({ disabled: true });
                            this.setState({ saving_status: 'save_queued' });
                            this.props.saveEmployees( this.state.form.company_id, this.state.form.job_id, {
                                payroll: this.isPayrollSubscribed(),
                                ta: this.isTaSubscribed()
                            });
                        } }
                    />
                </div>
            </div>
        ) : (
            <div style={ { textAlign: 'center', margin: '20px' } }>
                <i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" />
                <br />
                <H4>Loading Data. Please Wait...</H4>
            </div>
        );
    }

    isStepBeforeUploadPreview = () => {
        if ( this.isPayrollAndTaSubscribed() ) {
            return this.state.step === 2;
        }
        return this.state.step === 1;
    }

    isTaSubscribed = () => (
        this.props.products && subscriptionService.isSubscribedToTA( this.props.products )
    )

    isPayrollSubscribed = () => (
        this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products )
    )

    isPayrollAndTaSubscribed = () => ( this.isTaSubscribed() && this.isPayrollSubscribed() )

    checkPersonalInfoVisibility = () => ( this.state.step === 0 )

    checkTimeAttendanceVisibility = () => ( this.state.step === ( this.isTaSubscribed() ? 1 : -1 ) )

    checkPayrollVisibility = () => ( this.state.step === ( ( this.isPayrollAndTaSubscribed() ) ? 2 : this.isPayrollSubscribed() ? 1 : -1 ) )

    checkPreviewVisibility = () => ( this.state.step === ( ( this.isPayrollAndTaSubscribed() ) ? 3 : 2 ) )

    /**
     * sets the step
     */
    handleStepper( step ) {
        this.state.step >= step && this.setState({ step });
    }

    /**
     *  converts a number to currency format.
     * @param value = value to format
     * @returns {string} value in currency format
     */
    formatCurrency( value = 0 ) {
        const convertedValue = Number( value );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `PHP ${Number( convertedValue ).toFixed( 2 ).replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    /**
     * gets the batch upload icon
     */
    batchUploadIcon() {
        return (
            <svg width="76px" height="65px" viewBox="0 0 76 65" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <title>icon</title>
                <defs>
                    <rect id="path-1" x="43" y="37" width="8" height="8"></rect>
                    <rect id="path-2" x="55" y="31" width="7" height="8"></rect>
                </defs>
                <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g id="toggle-2" transform="translate(-127.000000, -29.000000)">
                        <g id="toggle">
                            <g id="batch-upload" transform="translate(127.000000, 29.000000)">
                                <g id="icon">
                                    <path d="M66.9858827,9 L59.4823127,9 L59.1997977,8.75349372 C53.5043225,3.78395107 46.2226186,1 38.5069517,1 C25.2906045,1 13.619215,9.21842448 9.03094067,21.3830945 L8.78687155,22.0301831 L5,22.0301831 L5,27 L3,27 L3,53 L14.5907049,53 L14.8892649,53.3382642 C20.832035,60.0713373 29.3509176,64 38.5069517,64 C49.3788719,64 59.2998897,58.4474133 65.0528567,49.4560635 C68.2666016,44.4332805 70,38.5961784 70,32.5 C70,27.8540722 68.9943856,23.3524733 67.0790999,19.2330331 L66.9858827,19.0325394 L66.9858827,9 Z M7.4113622,20.0301831 L8.09528391,20.0301831 L7.15962714,20.6772718 C7.24143209,20.4603863 7.32535406,20.2446804 7.41136529,20.0301754 Z" id="Combined-Shape" className="stroke" strokeWidth="2"></path>
                                    <path d="M70.0652174,15.004039 L70.0652174,15 L39.9347826,15 L39.9347826,15.004039 C36.6367785,15.1209394 34,17.7606896 34,21 C34,24.2393104 36.6367785,26.8790606 39.9347826,26.995961 L39.9347826,27 L70.0652174,27 L70.0652174,26.995961 C73.3632215,26.8790606 76,24.2393104 76,21 C76,17.7606896 73.3632215,15.1209394 70.0652174,15.004039 Z" id="Combined-Shape" fillOpacity="0.1" className="fill"></path>
                                    <path d="M36,47 L6.00000958,47 C2.6862915,47 0,44.3137085 0,41 C0,37.6862915 2.6862915,35 6,35 L36,35 C39.3137085,35 42,37.6862915 42,41 C42,44.3137085 39.3137085,47 36,47 Z" id="Combined-Shape" fillOpacity="0.1" className="fill"></path>
                                    <rect id="Rectangle-9" className="fill" x="2" y="26" width="2" height="28"></rect>
                                    <rect id="Rectangle-9" className="fill" x="43" y="26" width="2" height="28"></rect>
                                    <rect id="Rectangle-10" className="fill" x="3" y="52" width="41" height="2"></rect>
                                    <rect id="Rectangle-10" className="fill" x="3" y="26" width="41" height="2"></rect>
                                    <rect id="Rectangle-11" className="fill" x="4" y="23.0027119" width="2" height="2.99728814"></rect>
                                    <rect id="Rectangle-12" className="fill" x="4" y="21" width="22" height="2"></rect>
                                    <rect id="Rectangle-11" className="fill" x="24" y="22" width="2" height="4"></rect>
                                    <rect id="Rectangle-15" className="fill" x="34" y="26" width="17" height="2"></rect>
                                    <rect id="Rectangle-15" className="fill" x="34" y="20" width="17" height="2"></rect>
                                    <rect id="Rectangle-15" className="fill" x="43" y="32" width="8" height="2"></rect>
                                    <g id="Rectangle-16">
                                        <use fill="#E5F6FC" fillRule="evenodd"></use>
                                        <rect className="stroke" strokeWidth="2" x="44" y="38" width="6" height="6"></rect>
                                    </g>
                                    <rect id="Rectangle-14" className="fill" x="44" y="48" width="11" height="2"></rect>
                                    <rect id="Rectangle-14" className="fill" x="29" y="14" width="28" height="2"></rect>
                                    <rect id="Rectangle-13" className="fill" x="28" y="14" width="2" height="12"></rect>
                                    <rect id="Rectangle-13" className="fill" x="55" y="14" width="2" height="36"></rect>
                                    <rect id="Rectangle-15" className="fill" x="56" y="20" width="6" height="2"></rect>
                                    <rect id="Rectangle-15" className="fill" x="56" y="14" width="6" height="2"></rect>
                                    <rect id="Rectangle-15" className="fill" x="55" y="26" width="7" height="2"></rect>
                                    <g id="Rectangle-16">
                                        <use fill="#E5F6FC" fillRule="evenodd"></use>
                                        <rect className="stroke" strokeWidth="2" x="56" y="32" width="5" height="6"></rect>
                                    </g>
                                    <rect id="Rectangle-14" className="fill" x="55" y="42" width="11" height="2"></rect>
                                    <rect id="Rectangle-14" className="fill" x="40" y="8" width="28" height="2"></rect>
                                    <rect id="Rectangle-13" className="fill" x="39" y="8" width="2" height="8"></rect>
                                    <rect id="Rectangle-13" className="fill" x="66" y="8" width="2" height="36"></rect>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        );
    }

    /**
     * gets the manual entry icon
     */
    manualEntryIcon() {
        return (
            <svg width="76px" height="67px" viewBox="0 0 76 67" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <title>icon</title>
                <defs>
                    <polygon id="path-1" points="23 33 54 33 54 42 48.1835938 42 23 42"></polygon>
                    <rect id="path-2" x="23" y="45" width="14" height="9"></rect>
                    <rect id="path-3" x="40" y="45" width="14" height="9"></rect>
                </defs>
                <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g id="toggle-2" transform="translate(-709.000000, -26.000000)">
                        <g id="toggle">
                            <g id="single-entry" transform="translate(709.000000, 26.000000)">
                                <g id="icon">
                                    <path d="M61.0147622,1 L16.0147622,1 L16.0147622,12.4041844 L15.7380825,12.6940097 C10.1752704,18.5211221 7.01476217,26.2593234 7.01476217,34.5 C7.01476217,51.8980709 21.0472996,66 38.3548387,66 C49.1728227,66 59.0452408,60.4484779 64.770653,51.4578674 C67.9694808,46.4347505 69.6949153,40.5969834 69.6949153,34.5 C69.6949153,26.4173522 66.6551525,18.8159246 61.2816409,13.0234716 L61.0147622,12.7357859 L61.0147622,1 Z" id="Combined-Shape" className="stroke" strokeWidth="2"></path>
                                    <rect id="Rectangle-3" className="fill" transform="translate(38.500000, 1.000000) rotate(90.000000) translate(-38.500000, -1.000000) " x="37.5" y="-22.5" width="2" height="47"></rect>
                                    <rect id="Rectangle-3" className="fill" x="15" y="2" width="2" height="54"></rect>
                                    <rect id="Rectangle-3" className="fill" x="60" y="2" width="2" height="54"></rect>
                                    <rect id="Rectangle-3" className="fill" transform="translate(38.500000, 1.000000) rotate(90.000000) translate(-38.500000, -1.000000) " x="37.5" y="-22.5" width="2" height="47"></rect>
                                    <rect id="Rectangle-3" className="fill" x="15" y="2" width="2" height="54"></rect>
                                    <rect id="Rectangle-3" className="fill" x="60" y="2" width="2" height="54"></rect>
                                    <path d="M70.0652174,17.004039 L70.0652174,17 L39.9347826,17 L39.9347826,17.004039 C36.6367785,17.1209394 34,19.7606896 34,23 C34,26.2393104 36.6367785,28.8790606 39.9347826,28.995961 L39.9347826,29 L70.0652174,29 L70.0652174,28.995961 C73.3632215,28.8790606 76,26.2393104 76,23 C76,19.7606896 73.3632215,17.1209394 70.0652174,17.004039 Z" id="Combined-Shape" fillOpacity="0.1" className="fill"></path>
                                    <path d="M36.0652174,37.004039 L36.0652174,37 L5.93478261,37 L5.93478261,37.004039 C2.63677847,37.1209394 0,39.7606896 0,43 C0,46.2393104 2.63677847,48.8790606 5.93478261,48.995961 L5.93478261,49 L36.0652174,49 L36.0652174,48.995961 C39.3632215,48.8790606 42,46.2393104 42,43 C42,39.7606896 39.3632215,37.1209394 36.0652174,37.004039 Z" id="Combined-Shape" fillOpacity="0.1" className="fill"></path>
                                    <rect id="Rectangle-2" className="fill" x="23" y="10" width="31" height="2"></rect>
                                    <rect id="Rectangle-2" className="fill" x="23" y="20.5" width="31" height="2"></rect>
                                    <rect id="Rectangle-2" className="fill" x="23" y="26.5" width="31" height="2"></rect>
                                    <rect id="Rectangle-2" className="fill" x="23" y="15" width="14" height="2"></rect>
                                    <rect id="Rectangle-2" className="fill" x="40" y="15" width="14" height="2"></rect>
                                    <g id="Rectangle-8">
                                        <use fillOpacity="0.2" className="fill" fillRule="evenodd"></use>
                                        <path className="stroke" strokeWidth="2" d="M24,34 L24,41 L53,41 L53,34 L24,34 Z"></path>
                                    </g>
                                    <g id="Rectangle-8">
                                        <use fillOpacity="0.2" className="fill" fillRule="evenodd"></use>
                                        <rect className="stroke" strokeWidth="2" x="24" y="46" width="12" height="7"></rect>
                                    </g>
                                    <g id="Rectangle-8">
                                        <use fillOpacity="0.2" className="fill" fillRule="evenodd"></use>
                                        <rect className="stroke" strokeWidth="2" x="41" y="46" width="12" height="7"></rect>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        );
    }

    /**
     * renders component to DOM
     */
    render() {
        const steps = [
            {
                label: 'Personal Information'
            }
        ];

        if ( this.isTaSubscribed() ) {
            steps.push({
                label: 'Time and Attendance Information'
            });
        }

        if ( this.isPayrollSubscribed() ) {
            steps.push({
                label: 'Payroll Information'
            });
        }

        steps.push({
            label: 'Preview'
        });

        return (
            <div>
                <Helmet
                    title="Batch Upload"
                    meta={ [
                        { name: 'description', content: 'Employees Batch Upload' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => { e.preventDefault(); browserHistory.push( '/employees', true ); } }
                            >
                                &#8592; Back to Employees
                            </A>
                        </Container>
                    </div>
                    <Container>
                        <div className="heading">
                            <h3>Add Employees</h3>
                            <p>Add employees along with their personal and payroll information to your existing records. You can add multiple entries at once through Batch Upload or create one entry after another through Manual Entry.</p>
                        </div>
                        <div className="toggle">
                            <Toggle
                                options={ [
                                    { title: 'Batch Upload', subtext: 'Upload multiple entries at once using template.', value: 'batch', icon: this.batchUploadIcon() },
                                    { title: 'Manual Entry', subtext: 'Create entries one by one.', value: 'single', icon: this.manualEntryIcon() }
                                ] }
                                defaultSelected="batch"
                                onChange={ ( value ) => { if ( value === 'single' ) { browserHistory.push( '/employees/add', true ); } } }
                                ref={ ( ref ) => { this.toggle = ref; } }
                            />
                        </div>
                        <div className="employees-form">
                            <div>
                                <div style={ { display: this.state.step === 3 ? 'none' : 'block' } } >
                                    <Stepper steps={ steps } activeStep={ this.state.step } />
                                </div>
                                { this.getPersonalInfoSection() }
                                { this.getTimeAttendanceInfoSection() }
                                { this.getPayrollInfoSection() }
                                { this.getPreview() }
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    employees: makeSelectEmployees(),
    products: makeSelectProductsState()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        batchUploadActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EmployeeBatchUpload );
