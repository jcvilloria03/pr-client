import {
    UPLOAD_PERSONAL_INFO,
    UPLOAD_TIME_ATTENDANCE_INFO,
    UPLOAD_PAYROLL_INFO,
    GET_PREVIEW,
    SAVE_EMPLOYEES,
    RESET_PERSONAL_INFO,
    RESET_PAYROLL_INFO,
    RESET_PREVIEW,
    NOTIFICATION
} from './constants';

/**
 * upload personal info
 */
export function uploadPersonalInfo( file, companyId ) {
    return {
        type: UPLOAD_PERSONAL_INFO,
        payload: {
            company_id: companyId,
            file
        }
    };
}

/**
 * upload payroll info
 */
export function uploadPayrollInfo( file, companyId, jobId ) {
    return {
        type: UPLOAD_PAYROLL_INFO,
        payload: {
            company_id: companyId,
            job_id: jobId,
            file
        }
    };
}

/**
 * upload time and attendance info
 */
export function uploadTimeAttendanceInfo( file, companyId, jobId ) {
    return {
        type: UPLOAD_TIME_ATTENDANCE_INFO,
        payload: {
            company_id: companyId,
            job_id: jobId,
            file
        }
    };
}

/**
 * upload payroll info
 */
export function getPreview( companyId, jobId ) {
    return {
        type: GET_PREVIEW,
        payload: {
            company_id: companyId,
            job_id: jobId
        }
    };
}

/**
 * upload payroll info
 */
export function saveEmployees( companyId, jobId, subscriptions ) {
    return {
        type: SAVE_EMPLOYEES,
        payload: {
            company_id: companyId,
            job_id: jobId,
            subscriptions
        }
    };
}

/**
 * reset personal info
 */
export function resetPersonalInfo() {
    return {
        type: RESET_PERSONAL_INFO
    };
}

/**
 * upload payroll info
 */
export function resetPayrollInfo() {
    return {
        type: RESET_PAYROLL_INFO
    };
}

/**
 * upload payroll info
 */
export function resetPreview() {
    return {
        type: RESET_PREVIEW
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
