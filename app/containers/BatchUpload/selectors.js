import { createSelector } from 'reselect';

/**
 * Direct selector to the setup state domain
 */
const selectSetupDomain = () => ( state ) => state.get( 'batch_add' );

const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */
const makeSelectEmployees = () => createSelector(
    selectSetupDomain(),
    ( substate ) => substate.get( 'employees' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectSetupDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectProductsState = () => createSelector(
    selectAppDomain(),
    ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

export {
    selectSetupDomain,
    makeSelectEmployees,
    makeSelectNotification,
    makeSelectProductsState
};

