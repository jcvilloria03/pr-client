import { fromJS } from 'immutable';

import {
    UPDATE_STORE_EMPLOYEES,
    SET_JOB_ID,
    PERSONAL_INFO_STATUS,
    PERSONAL_INFO_ERRORS,
    TIME_ATTENDANCE_INFO_STATUS,
    TIME_ATTENDANCE_INFO_ERRORS,
    PAYROLL_INFO_STATUS,
    PAYROLL_INFO_ERRORS,
    PREVIEW_STATUS,
    EMPLOYEE_PREVIEW,
    SAVING_STATUS,
    SAVING_ERRORS,
    RESET_PERSONAL_INFO,
    RESET_PAYROLL_INFO,
    RESET_PREVIEW,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../App/constants';

const initialState = fromJS({
    employees: {
        data: [],
        job_id: null,
        personal_info: {
            status: '',
            errors: {}
        },
        payroll_info: {
            status: '',
            errors: {}
        },
        time_attendance_info: {
            status: '',
            errors: {}
        },
        preview: {
            status: 'ready',
            data: []
        },
        saving: {
            status: '',
            errors: {}
        }
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Batch Upload reducer
 *
 */
function batchUploadReducer( state = initialState, action ) {
    switch ( action.type ) {
        case UPDATE_STORE_EMPLOYEES:
            return state.setIn([ 'employees', 'data' ], action.payload );
        case SET_JOB_ID:
            return state.setIn([ 'employees', 'job_id' ], action.payload );
        case PERSONAL_INFO_STATUS:
            return state.setIn([ 'employees', 'personal_info', 'status' ], action.payload );
        case PERSONAL_INFO_ERRORS:
            return state.setIn([ 'employees', 'personal_info', 'errors' ], action.payload );
        case TIME_ATTENDANCE_INFO_STATUS:
            return state.setIn([ 'employees', 'time_attendance_info', 'status' ], action.payload );
        case TIME_ATTENDANCE_INFO_ERRORS:
            return state.setIn([ 'employees', 'time_attendance_info', 'errors' ], action.payload );
        case PAYROLL_INFO_STATUS:
            return state.setIn([ 'employees', 'payroll_info', 'status' ], action.payload );
        case PAYROLL_INFO_ERRORS:
            return state.setIn([ 'employees', 'payroll_info', 'errors' ], action.payload );
        case PREVIEW_STATUS:
            return state.setIn([ 'employees', 'preview', 'status' ], action.payload );
        case EMPLOYEE_PREVIEW:
            return state.setIn([ 'employees', 'preview', 'data' ], action.payload );
        case SAVING_STATUS:
            return state.setIn([ 'employees', 'saving', 'status' ], action.payload );
        case SAVING_ERRORS:
            return state.setIn([ 'employees', 'saving', 'errors' ], action.payload );
        case RESET_PERSONAL_INFO:
            return state.setIn([ 'employees', 'personal_info' ], fromJS({
                status: '',
                errors: {}
            }) );
        case RESET_PAYROLL_INFO:
            return state.setIn([ 'employees', 'payroll_info' ], fromJS({
                status: '',
                errors: {}
            }) );
        case RESET_PREVIEW:
            return state.setIn([ 'employees', 'preview' ], fromJS({
                status: 'ready',
                data: []
            }) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default batchUploadReducer;

