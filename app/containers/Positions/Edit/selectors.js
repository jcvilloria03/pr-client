import { createSelector } from 'reselect';

const selectEditPositionDomain = () => ( state ) => state.get( 'positionsEdit' );

const makeSelectNotification = () => createSelector(
  selectEditPositionDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectPositions = () => createSelector(
  selectEditPositionDomain(),
  ( substate ) => substate.get( 'positions' ).toJS()
);

const makeSelectBtnLoad = () => createSelector(
  selectEditPositionDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

export {
  makeSelectPositions,
  makeSelectNotification,
  makeSelectBtnLoad
};
