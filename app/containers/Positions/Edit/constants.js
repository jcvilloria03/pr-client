export const namespace = 'app/containers/Positions/Edit';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const BTN_LOADING = `${namespace}/BTN_LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;

export const UPDATE_POSITION = `${namespace}/UPDATE_POSITION`;
export const GET_POSITIONS = `${namespace}/GET_POSITIONS`;
export const SET_POSITIONS = `${namespace}/SET_POSITIONS`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const SET_ERRORS = `${namespace}/SET_ERRORS`;