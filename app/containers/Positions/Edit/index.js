/* eslint-disable react/no-did-update-set-state */
/* eslint-disable require-jsdoc */
/* eslint-disable consistent-return */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import _ from 'lodash';
import Sidebar from 'components/Sidebar';
import { H3 } from 'components/Typography';
import SnackBar from 'components/SnackBar';
import Button from 'components/Button';
import Input from 'components/Input';
import A from 'components/A';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import Loader from 'components/Loader';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
import * as editPositionAction from './actions';
import {
    makeSelectBtnLoad,
    makeSelectNotification,
    makeSelectPositions
} from './selectors';
import { PageWrapper, Footer, ModalAction, Department } from './styles';
import { LoadingStyles } from '../View/styles';
import { H2 } from '../../../components/Typography';

export class EditPosition extends React.Component {
    static propTypes = {
        btnLoading: React.PropTypes.bool,
        resetStore: React.PropTypes.func,
        getPositions: React.PropTypes.func,
        updatePosition: React.PropTypes.func,
        positions: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        routeParams: React.PropTypes.object
    }
    constructor( props ) {
        super( props );
        this.state = {
            position: [],
            isLoading: true
        };
        this.discardModal = null;
    }

    componentDidMount() {
        this.props.getPositions();
    }

    componentDidUpdate( prevProps ) {
        if ( this.props.positions !== prevProps.positions ) {
            const { positionId } = this.props.routeParams;
            const position = this.props.positions.find( ( item ) => String( item.id ) === positionId );
            if ( position ) {
                this.setState({
                    position: {
                        ...position,
                        ...this.state.position
                    },
                    isLoading: false
                });
            }
        }
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    handleChange( value ) {
        const { position } = this.state;
        position.currentValue = value;
        this.setState({ position });
    }

    handleSubmit() {
        const { position } = this.state;
        const error = this.validatePosition( position.currentValue );

        if ( error.hasError ) {
            position.hasError = true;
            position.errorMessage = error.errorMessage;
            this.setState({ position });
        } else {
            position.hasError = false;
            position.errorMessage = '';
            this.setState({ position });
            this.updatePosition();
        }
    }

    updatePosition() {
        const { position } = this.state;
        const companyId = company.getLastActiveCompanyId();
        this.props.updatePosition({
            company_id: companyId,
            data: [
                {
                    id: position.id,
                    name: position.currentValue,
                    company_id: companyId
                }
            ]
        });
    }

    validatePosition = async ( value ) => {
        if ( !value || value.length < 3 ) {
            return {
                hasError: true,
                errorMessage: 'Please input valid Position name.'
            };
        }
        const companyId = company.getLastActiveCompanyId();
        const isNameAvailable = await Fetch( `/company/${companyId}/position/is_name_available`, { method: 'POST', data: { name: value }});
        const isNameExists = _.find( this.state.positions, { value });
        if ( !isNameAvailable || isNameExists ) {
            return {
                hasError: true,
                errorMessage: 'Position name already exists'
            };
        }
        return {
            hasError: false,
            errorMessage: ''
        };
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { btnLoading } = this.props;
        const { position } = this.state;
        return (
            <div>
                <Helmet
                    title="Edit Positions"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Positions' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p></ModalAction> }
                    buttons={ [
                        {
                            label: 'Stay on this page',
                            type: 'grey',
                            onClick: () => this.discardModal.toggle()
                        },
                        {
                            label: 'Discard',
                            type: 'darkRed',
                            onClick: () => browserHistory.push( '/company-settings/company-structure/positions', true )
                        }
                    ] }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <PageWrapper>
                    <Container className="add-dpt-container">
                        <div className="loader" style={ { display: this.state.isLoading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Positions.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        { !this.state.isLoading ? <div className="nav">
                            <Container>
                                <A
                                    href
                                    onClick={ ( e ) => {
                                        e.preventDefault();
                                        browserHistory.push( '/company-settings/company-structure/positions', true );
                                    } }
                                >
                                    <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Positions</span>
                                </A>
                            </Container>
                        </div> : null }
                        { !this.state.isLoading ? <div className="main_container">
                            <section className="content">
                                <header className="heading">
                                    <H3>Edit Positions</H3>
                                    <p>A position should have a lead position to report to. In this manner, a position will also be tagged with a department so that organizational hierarchy structure will be determined.</p>
                                </header>
                            </section>
                            <section className="content">
                                <Department>
                                    <div className="department-label">
                                        <span></span>
                                        <span>Position Name</span>
                                        <span></span>
                                    </div>
                                    <div className="department-number">
                                        <span>Position 1</span>
                                    </div>
                                    <div className={ `department-name-input ${position.hasError && 'department-name-input--error'}` }>
                                        <Input
                                            value={ position.currentValue || position.name }
                                            onChange={ ( value ) => this.handleChange( value ) }
                                        />
                                        { position.hasError && <span className="department-name-input--error-message">{ position.errorMessage}</span> }
                                    </div>
                                    <div className="department-btns"></div>
                                </Department>
                            </section>
                        </div> : null }
                    </Container>
                </PageWrapper>
                { !this.state.isLoading ? <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                type="action"
                                size="large"
                                label={ btnLoading ? <Loader /> : 'Submit' }
                                disabled={ btnLoading || this.state.isAddLoader }
                                onClick={ () => this.handleSubmit() }
                            />
                        </div>
                    </div>
                </Footer> : null }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    btnLoading: makeSelectBtnLoad(),
    positions: makeSelectPositions()
});

function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { ...editPositionAction },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EditPosition );
