/* eslint-disable require-jsdoc */
import { RESET_STORE } from '../../App/constants';
import {
    DEFAULT_ACTION,
    NOTIFICATION,
    GET_POSITIONS,
    UPDATE_POSITION
} from './constants';

export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function updatePosition( payload ) {
    return {
        type: UPDATE_POSITION,
        payload
    };
}

export function getPositions() {
    return {
        type: GET_POSITIONS
    };
}

export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

export function resetStore() {
    return {
        type: RESET_STORE
    };
}
