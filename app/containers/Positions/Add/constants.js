export const namespace = 'app/containers/Departments/Add';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const BTN_LOADING = `${namespace}/BTN_LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;

export const CREATE_POSITIONS = `${namespace}/CREATE_POSITIONS`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const SET_ERRORS = `${namespace}/SET_ERRORS`;

export const DEFAULT_POSITION = {
    value: '',
    currentValue: '',
    hasError: false,
    errorMessage: '',
    isNew: true,
    isEditable: false,
    isEditing: false
};
