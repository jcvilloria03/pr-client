import { createSelector } from 'reselect';

const selectAddPositionDomain = () => ( state ) => state.get( 'positionsAdd' );

const makeSelectNotification = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBtnLoad = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

const makeSelectAddPosition = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectAddPosition,
  makeSelectNotification,
  makeSelectBtnLoad
};
