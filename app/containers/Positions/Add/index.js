/* eslint-disable require-jsdoc */
/* eslint-disable consistent-return */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import _ from 'lodash';
import Sidebar from 'components/Sidebar';
import { H3 } from 'components/Typography';
import SnackBar from 'components/SnackBar';
import Button from 'components/Button';
import Input from 'components/Input';
import A from 'components/A';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import Loader from 'components/Loader';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
import * as addPositionAction from './actions';
import { DEFAULT_POSITION } from './constants';
import {
    makeSelectBtnLoad,
    makeSelectNotification
} from './selectors';
import { PageWrapper, Footer, ModalAction, Department } from './styles';

export class AddPosition extends React.Component {
    static propTypes = {
        btnLoading: React.PropTypes.bool,
        resetStore: React.PropTypes.func,
        createPositions: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    constructor( props ) {
        super( props );
        this.state = {
            positions: [
                { ...DEFAULT_POSITION }
            ],
            positionToDelete: null,
            isAddLoader: false
        };
        this.confirmOpenModal = null;
        this.successModal = null;
        this.cancelOption = '';
        this.discardModal = null;
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    async handleAddPosition( value, index ) {
        const { positions } = this.state;
        const error = await this.validatePosition( value );
        positions[ index ].hasError = error.hasError;
        positions[ index ].errorMessage = error.errorMessage;

        if ( !error.hasError ) {
            positions[ index ].value = value;
            positions[ index ].isNew = false;
            positions[ index ].isEditable = true;
            positions.push({ ...DEFAULT_POSITION });
        }
        this.setState({ positions });
    }

    handleEditPosition( position, index ) {
        const { positions } = this.state;
        positions[ index ].isEditable = false;
        positions[ index ].isEditing = true;
        positions[ index ].currentValue = position.value;
        this.setState({ positions });
    }

    handleSavePosition( position, index ) {
        const { positions } = this.state;
        positions[ index ].isEditing = false;
        positions[ index ].isEditable = true;
        positions[ index ].value = position.currentValue;
        this.setState({ positions });
    }

    handleCancelEditingPosition( index ) {
        const { positions } = this.state;
        positions[ index ].isEditing = false;
        positions[ index ].isEditable = true;
        positions[ index ].currentValue = positions[ index ].value;
        this.setState({ positions });
    }

    handleDeletePosition( index ) {
        this.setState({ positionToDelete: index });
        this.confirmOpenModal.toggle();
    }

    handleConfirmDeletePosition() {
        const { positions, positionToDelete } = this.state;
        positions.splice( positionToDelete, 1 );
        this.setState({ positions, positionToDelete: null });
        this.confirmOpenModal.toggle();
    }

    handleCancelDeletePosition() {
        this.setState({ position: null });
        this.confirmOpenModal.toggle();
    }

    handleChange( value, index ) {
        const { positions } = this.state;
        positions[ index ].currentValue = value;
        this.setState({ positions });
    }

    handleSubmit() {
        const companyId = company.getLastActiveCompanyId();
        const data = this.state.positions.map( ( position ) => ({
            name: position.value,
            parent: null
        }) ).filter( ( position ) => position.name );
        this.props.createPositions({
            company_id: companyId,
            data
        });
    }

    validatePosition = async ( value ) => {
        if ( !value || value.length < 3 ) {
            return {
                hasError: true,
                errorMessage: 'Please input valid Position name.'
            };
        }
        const companyId = company.getLastActiveCompanyId();
        const isNameAvailable = await Fetch( `/company/${companyId}/position/is_name_available`, { method: 'POST', data: { name: value }});
        const isNameExists = _.find( this.state.positions, { value });
        if ( !isNameAvailable || isNameExists ) {
            return {
                hasError: true,
                errorMessage: 'Position name already exists'
            };
        }
        return {
            hasError: false,
            errorMessage: ''
        };
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { btnLoading } = this.props;
        return (
            <div>
                <Helmet
                    title="Add Positions"
                    meta={ [
                        { name: 'description', content: 'Description of Add Positions' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p></ModalAction> }
                    buttons={ [
                        {
                            label: 'Stay on this page',
                            type: 'grey',
                            onClick: () => this.discardModal.toggle()
                        },
                        {
                            label: 'Discard',
                            type: 'darkRed',
                            onClick: () => browserHistory.push( '/company-settings/company-structure/positions', true )
                        }
                    ] }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Modal
                    size="md"
                    title="Confirm Your Action"
                    body={ <ModalAction>
                        Do you wish to remove this record?
                    </ModalAction> }
                    buttons={ [
                        {
                            label: 'No',
                            type: 'grey',
                            onClick: () => this.handleCancelDeletePosition()
                        },
                        {
                            label: 'Yes',
                            type: 'action',
                            onClick: () => this.handleConfirmDeletePosition()
                        }
                    ] }
                    ref={ ( ref ) => { this.confirmOpenModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Record successfully removed.</p></ModalAction> }
                    buttons={ [
                        {
                            label: 'Okay',
                            type: 'action',
                            onClick: () => this.successModal.toggle()
                        }
                    ] }
                    ref={ ( ref ) => { this.successModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <PageWrapper>
                    <Container className="add-dpt-container">
                        <div className="nav">
                            <Container>
                                <A
                                    href
                                    onClick={ ( e ) => {
                                        e.preventDefault();
                                        browserHistory.push( '/company-settings/company-structure/positions', true );
                                    } }
                                >
                                    <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Positions</span>
                                </A>
                            </Container>
                        </div>
                        <div className="main_container">
                            <section className="content">
                                <header className="heading">
                                    <H3>Add Positions</H3>
                                    <p>A position should have a lead position to report to. In this manner, a position will also be tagged with a department so that organizational hierarchy structure will be determined.</p>
                                </header>
                            </section>
                            <section className="content">
                                { this.state.positions.map( ( position, index ) => (
                                    <Department key={ `department_${index}` }>
                                        <div className="department-label">
                                            <span></span>
                                            <span>Position Name</span>
                                            <span></span>
                                        </div>
                                        <div className="department-number">
                                            <span>Position { index + 1 }</span>
                                        </div>
                                        { position.isNew || position.isEditing ? (
                                            <div className={ `department-name-input ${position.hasError && 'department-name-input--error'}` }>
                                                <Input
                                                    key={ `department_name_${index}` }
                                                    value={ position.currentValue }
                                                    onChange={ ( value ) => this.handleChange( value, index ) }
                                                />
                                                { position.hasError && <span className="department-name-input--error-message">{ position.errorMessage}</span> }
                                            </div> ) : (
                                                <div className="department-name">
                                                    <span>{ position.value }</span>
                                                </div>
                                            )
                                        }
                                        { position.isNew ? (
                                            <div className="department-btns">
                                                <Button
                                                    type="neutral"
                                                    className="add-btn"
                                                    label={ <span><Icon name="plus" className="svg-icon" /><span>Add Position</span></span> }
                                                    onClick={ () => this.handleAddPosition( position.currentValue, index ) }
                                                />
                                            </div>
                                        ) : null }
                                        { !position.isNew && position.isEditable ? (
                                            <div className="department-btns">
                                                <Button
                                                    type="neutral"
                                                    label={ <span><Icon name="pencil" className="svg-icon" /><span>Edit</span></span> }
                                                    onClick={ () => this.handleEditPosition( position, index ) }
                                                />
                                                <Button
                                                    type="darkRed"
                                                    alt
                                                    label={ <span><Icon name="trash" className="svg-icon" /><span>Delete</span></span> }
                                                    onClick={ () => this.handleDeletePosition( index ) }
                                                />
                                            </div>
                                        ) : null }
                                        { !position.isNew && position.isEditing ? (
                                            <div className="department-btns">
                                                <Button
                                                    type="action"
                                                    label="Save"
                                                    onClick={ () => this.handleSavePosition( position, index ) }
                                                />
                                                <Button
                                                    type="darkRed"
                                                    alt
                                                    label="Cancel"
                                                    onClick={ () => this.handleCancelEditingPosition( index ) }
                                                />
                                            </div>
                                        ) : null }
                                    </Department>
                                ) ) }
                            </section>
                        </div>
                    </Container>
                </PageWrapper>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                type="action"
                                size="large"
                                label={ btnLoading ? <Loader /> : 'Submit' }
                                disabled={ btnLoading || this.state.isAddLoader }
                                onClick={ () => this.handleSubmit() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    btnLoading: makeSelectBtnLoad()
});

function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { ...addPositionAction },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddPosition );
