/* eslint-disable require-jsdoc */
import React from 'react';
import { debounce } from 'lodash';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import FooterTablePagination from 'components/FooterTablePagination';
import Icon from 'components/Icon';
import Input from 'components/Input';
import {
    formatDeleteLabel,
    formatPaginationLabel
} from '../../../utils/functions';
import { browserHistory } from '../../../utils/BrowserHistory';
import { getCompanySettingsSidebarLinks } from '../../../utils/sidebarHelper';
import {
    makeSelectLoading,
    makeSelectTableLoading,
    makeSelectPositions,
    makeSelectPagination,
    makeSelectNotification
} from './selectors';
import * as positionsActions from './actions';
import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';
import SnackBar from '../../../components/SnackBar';
import Table from '../../../components/Table';
import { H2, H3, H5 } from '../../../components/Typography';
import Button from '../../../components/Button';
import SalConfirm from '../../../components/SalConfirm';
import Sidebar from '../../../components/Sidebar';
import {
    DEFAULT_ORDER_BY,
    DEFAULT_ORDER_DIR
} from './constants';

export class View extends React.Component {
    static propTypes = {
        getPositions: React.PropTypes.func,
        positions: React.PropTypes.array,
        deletePositions: React.PropTypes.func,
        loading: React.PropTypes.bool,
        tableLoading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        pagination: React.PropTypes.object
    };

    static defaultProps = {
        loading: true,
        errors: {},
        pagination: {
            from: 1,
            to: 1,
            total: 1,
            current_page: 0,
            last_page: 1,
            per_page: 10
        }
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            showDelete: false,
            deleteLabel: '',
            showModal: false,
            pagination: {
                pages: 1,
                current_page: 1,
                per_page: 10,
                total: 0,
                last_page: 1
            },
            positions: [],
            orderBy: DEFAULT_ORDER_BY,
            orderDir: DEFAULT_ORDER_DIR,
            tablePageSize: 10,
            page: 1,
            totalPagination: 0,
            searchTerm: ''
        };

        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
        this.onSortingChange = this.onSortingChange.bind( this );
    }

    componentDidMount() {
        const data = {
            orderBy: this.state.orderBy,
            orderDir: this.state.orderDir,
            searchTerm: this.state.searchTerm,
            pagination: this.state.pagination
        };
        this.props.getPositions( data );
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.positions !== this.props.positions
            && this.setState({
                positions: nextProps.positions
            });
    }

    componentDidUpdate( prevState ) {
        if ( prevState.positions !== this.state.positions ) {
            this.handleTableChanges();
        }
    }

    onPageChange( page ) {
        const { pagination } = this.state;

        pagination.current_page = page;
        this.setState({ pagination });
        const data = {
            orderBy: this.state.orderBy,
            orderDir: this.state.orderDir,
            searchTerm: this.state.searchTerm,
            pagination: this.state.pagination
        };
        this.props.getPositions( data );
    }

    onPageSizeChange( perPage ) {
        const { pagination } = this.state;
        pagination.per_page = perPage;
        this.setState({ pagination });
        const data = {
            orderBy: this.state.orderBy,
            orderDir: this.state.orderDir,
            searchTerm: this.state.searchTerm,
            pagination: this.state.pagination
        };
        this.props.getPositions( data );
    }

    onSortingChange( column, additive ) {
        // clone current sorting sorting
        let newSorting = JSON.parse(
            JSON.stringify( this.positionsTable.state.sorting, ( key, value ) => {
                if ( typeof value === 'function' ) {
                    return value.toString();
                }

                return value;
            })
        );

        // check if already sorted by same column
        const existingIndex = newSorting.findIndex( ( d ) => d.id === column.id );

        let isDesc = false;
        if ( existingIndex > -1 ) {
            const existing = newSorting[ existingIndex ];
            if ( existing.desc ) {
                if ( additive ) {
                    newSorting.splice( existingIndex, 1 );
                } else {
                    existing.desc = false;
                    newSorting = [existing];
                }
            } else {
                existing.desc = true;
                isDesc = true;
                if ( !additive ) {
                    newSorting = [existing];
                }
            }
        } else if ( additive ) {
            newSorting.push({
                id: column.id,
                desc: false
            });
        } else {
            newSorting = [{
                id: column.id,
                desc: false
            }];
        }

        const orderDir = isDesc ? 'desc' : 'asc';

        // Fetch sorted data
        const data = {
            orderBy: this.state.orderBy,
            orderDir,
            searchTerm: this.state.searchTerm,
            pagination: this.state.pagination
        };

        this.props.getPositions( data );

        // set new sorting states
        this.setState({
            orderBy: column.id,
            orderDir
        });
        this.positionsTable.setState({
            sorting: newSorting
        });
        this.positionsTable.tableComponent.setState({
            sorting: newSorting
        });
    }

    handleTableChanges( tableProps = this.positionsTable.tableComponent.state ) {
        Object.assign( tableProps, {
            page: this.props.pagination.current_page - 1,
            pageSize: this.props.pagination.per_page,
            dataLength: this.props.pagination.total
        });
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    /**
     * handles filters and search inputs
     */
    handleSearch = debounce( ( term = '' ) => {
        this.setState({
            searchTerm: term.trim()
        });

        const data = {
            orderBy: this.state.orderBy,
            orderDir: this.state.orderDir,
            searchTerm: this.state.searchTerm,
            pagination: this.state.pagination
        };

        this.props.getPositions( data );
    }, 600 )

    deletePositions() {
        const ids = [];
        this.positionsTable.state.selected.forEach( ( selected, index ) => {
            if ( selected ) {
                ids.push( this.positionsTable.props.data[ index ].id );
            }
        });
        this.props.deletePositions( ids );
        this.setState({ showDelete: false, showModal: false });
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'name',
                header: 'Position Name',
                accessor: 'name',
                sortable: true,
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.name}
                    </div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => browserHistory.push( `/company-settings/company-structure/positions/${row.id}/edit`, true ) }
                    />
                )
            }
        ];

        return (
            <div>
                <Helmet
                    title="Positions"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <SalConfirm
                    onConfirm={ () => this.deletePositions() }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Do you want to delete the selected record(s)?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Position"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Positions.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Position</H3>
                            </div>
                            <div className="title">
                                <div className="search-wrapper">
                                    <H5 noBottomMargin>Position List</H5>
                                    <Input
                                        id="search"
                                        className="search"
                                        onChange={ () => {
                                            this.handleSearch(
                                                this.searchInput.state.value.toLowerCase()
                                            );
                                        } }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                    />
                                </div>
                                <div className="table-options">
                                    {
                                        this.state.showDelete ?
                                            (
                                                <div>
                                                    { this.state.deleteLabel }
                                                    &nbsp;
                                                    <Button
                                                        label={ <span>Delete</span> }
                                                        type="danger"
                                                        onClick={ () => this.setState({ showModal: true }) }
                                                    />
                                                </div>
                                            )
                                            : <div className="table-options--default">
                                                <p> { this.state.label } </p>
                                                <Button
                                                    label={ <span>Add Position</span> }
                                                    type="action"
                                                    onClick={ () => {
                                                        browserHistory.push( '/company-settings/company-structure/positions/add', true );
                                                    } }
                                                />
                                            </div>
                                    }
                                </div>
                            </div>
                            <Table
                                data={ this.state.positions || [] }
                                columns={ tableColumns }
                                loading={ this.props.tableLoading }
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.positionsTable = ref; } }
                                selectable
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    this.setState({
                                        showDelete: selectionLength > 0,
                                        deleteLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                                onSortingChange={ this.onSortingChange }
                                page={ this.state.pagination.currentPage - 1 }
                                pageSize={ this.state.pagination.perPage }
                                pages={ this.state.pagination.pages }
                                manual
                            />
                            <div>
                                <FooterTablePagination
                                    page={ this.props.pagination.current_page }
                                    pageSize={ this.props.pagination.per_page }
                                    pagination={ this.props.pagination }
                                    onPageChange={ this.onPageChange }
                                    onPageSizeChange={ this.onPageSizeChange }
                                    fluid
                                />
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    positions: makeSelectPositions(),
    pagination: makeSelectPagination(),
    notification: makeSelectNotification(),
    tableLoading: makeSelectTableLoading()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        positionsActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
