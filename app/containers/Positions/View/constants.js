const namespace = 'app/Positions/View';

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const TABLE_LOADING = `${namespace}/TABLE_LOADING`;
export const GET_POSITIONS = `${namespace}/GET_POSITIONS`;
export const SET_POSITIONS = `${namespace}/SET_POSITIONS`;
export const DELETE_POSITIONS = `${namespace}/DELETE_POSITIONS`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const SET_POSITIONS_PAGINATION = `${namespace}/SET_POSITIONS_PAGINATION`;

export const DEFAULT_ORDER_BY = 'name';
export const DEFAULT_ORDER_DIR = 'asc';
