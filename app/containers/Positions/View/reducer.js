/* eslint-disable require-jsdoc */
import { fromJS } from 'immutable';
import {
    SET_LOADING,
    TABLE_LOADING,
    NOTIFICATION_SAGA,
    SET_POSITIONS,
    SET_POSITIONS_PAGINATION
} from './constants';
import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: false,
    tableLoading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    positions: [],
    pagination: {
        from: 1,
        to: 1,
        total: 1,
        current_page: 0,
        last_page: 1,
        per_page: 10
    }
});

function positionsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case TABLE_LOADING:
            return state.set( 'tableLoading', action.payload );
        case SET_POSITIONS:
            return state.set( 'positions', fromJS( action.payload ) );
        case SET_POSITIONS_PAGINATION:
            return state.set( 'pagination', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default positionsReducer;
