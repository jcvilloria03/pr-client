import { createSelector } from 'reselect';

const selectViewPositionsDomain = () => ( state ) => state.get( 'positions' );

const makeSelectLoading = () => createSelector(
  selectViewPositionsDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectTableLoading = () => createSelector(
  selectViewPositionsDomain(),
( substate ) => substate.get( 'tableLoading' )
);

const makeSelectPositions = () => createSelector(
    selectViewPositionsDomain(),
  ( substate ) => substate.get( 'positions' ).toJS()
);

const makeSelectPagination = () => createSelector(
  selectViewPositionsDomain(),
( substate ) => substate.get( 'pagination' )
);

const makeSelectNotification = () => createSelector(
    selectViewPositionsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectTableLoading,
  makeSelectPositions,
  makeSelectPagination,
  makeSelectNotification
};
