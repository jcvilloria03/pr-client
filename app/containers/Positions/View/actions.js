/* eslint-disable require-jsdoc */
import {
    NOTIFICATION,
    DELETE_POSITIONS,
    GET_POSITIONS

} from './constants';

export function getPositions( payload ) {
    return {
        type: GET_POSITIONS,
        payload
    };
}

export function deletePositions( ids ) {
    return {
        type: DELETE_POSITIONS,
        payload: ids
    };
}

export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
