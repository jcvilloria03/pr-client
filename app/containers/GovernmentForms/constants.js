/**
 * Government Forms shared constants
 */
export const PAGE_STATUSES = {
    LOADING: 'LOADING',
    GENERATING: 'GENERATING',
    SAVING: 'SAVING',
    READY: 'READY'
};
