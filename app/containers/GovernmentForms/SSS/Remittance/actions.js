import { RESET_STORE } from 'containers/App/constants';

import {
    INITIAL_DATA,
    GENERATE_FORMS,
    SAVE_LINE_ITEM,
    GET_FORM_URI,
    SAVE_FORM_DETAILS,
    SET_URI,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_PAGE_STATUS
} from './constants';

/**
 * Initialize data for MCL Form page
 */
export function initializeData( payload ) {
    return {
        type: INITIAL_DATA,
        payload
    };
}

/**
 * Initialize data for Form page
 */
export function generateForm( data ) {
    return {
        type: GENERATE_FORMS,
        payload: data
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}

/**
 * Saves a line item
 */
export function saveLineItem( data ) {
    return {
        type: SAVE_LINE_ITEM,
        payload: data
    };
}

/**
 * Save form header info.
 */
export function saveFormHeader( data ) {
    return {
        type: SAVE_FORM_DETAILS,
        payload: data
    };
}

/**
 * Get download link
 */
export function getFileLink( data ) {
    return {
        type: GET_FORM_URI,
        payload: data
    };
}

/**
 * set URI
 */
export function setDownloadUrl( data ) {
    return {
        type: SET_URI,
        payload: data
    };
}

/**
 * Sets status of page
 * @param {String} payload - Page status
 * @returns {Object} action
 */
export function setPageStatus( payload ) {
    return {
        type: SET_PAGE_STATUS,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: NOTIFICATION_SAGA,
        payload
    };
}

/**
 * Notify user
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function notifyUser( payload ) {
    return {
        type: NOTIFICATION,
        payload
    };
}
