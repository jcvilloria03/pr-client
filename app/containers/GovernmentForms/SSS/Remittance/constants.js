/*
 *
 * SSS Remittance Forms constants
 *
 */

const namespace = 'app/GovernmentForms/SSS/Remittance';

export const INITIAL_DATA = `${namespace}/INITIAL_DATA`;

export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;

export const SET_FORM_OPTIONS = `${namespace}/SET_FORM_OPTIONS`;
export const SET_DATA = `${namespace}/SET_DATA`;
export const GET_FORM_URI = `${namespace}/GET_FORM_URI`;

export const GENERATE_FORMS = `${namespace}/GENERATE_FORMS`;
export const SET_URI = `${namespace}/SET_URI`;

export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const SAVE_LINE_ITEM = `${namespace}/SAVE_LINE_ITEM`;
export const SAVE_FORM_DETAILS = `${namespace}/SAVE_FORM_DETAILS`;

export const BUSINESS_TYPES = [
    { value: 'BUSINESS', label: 'Business' },
    { value: 'HOUSEHOLD', label: 'Household' }
];

export const REPORT_TYPES = [
    { value: 'SSS_R3', label: 'SSS R3' },
    { value: 'SSS_MEDICARE', label: 'SSS with Medicare' },
    { value: 'SSS_NO_MEDICARE', label: 'SSS without Medicare' }
];

export const LOCATOR_CODES = [
    { value: 'A', label: 'A - Baguio' },
    { value: 'B', label: 'B - Tarlac' },
    { value: 'C', label: 'C - NCR' },
    { value: 'D', label: 'D - San Pablo' },
    { value: 'E', label: 'E - Naga' },
    { value: 'F', label: 'F - Cebu' },
    { value: 'G', label: 'G - Bacolod' },
    { value: 'H', label: 'H - Cagayan' },
    { value: 'I', label: 'I - Davao' },
    { value: 'J', label: 'J - Zamboanga' }
];

export const DEFAULT_LOCATOR_CODE = 'C';

export const REQUIRED_FIELDS = {
    INPUTS: [
        'employer_sss_number',
        'employer_rm_flr_unit',
        'employer_city',
        'employer_name',
        'employer_tin',
        'subtotal_ss_contribution',
        'subtotal_ec_contribution',
        'amount_paid_in_figures',
        'total_amount_words',
        'tr_sbr_no'
    ],
    DATE_PICKERS: ['date_paid']
};
