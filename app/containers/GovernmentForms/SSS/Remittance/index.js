import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';
import debounce from 'lodash/debounce';

import GovernmentNumbersInput from 'components/GovernmentNumbersInput';
import Select from 'components/Select';
import SnackBar from 'components/SnackBar';
import Input from 'components/Input';
import { H3, H5 } from 'components/Typography';
import Table from 'components/Table';
import Button from 'components/Button';
import A from 'components/A';
import ToolTip from 'components/ToolTip';
import DatePicker from 'components/DatePicker';
import Search from 'components/Search';
import RadioGroup from 'components/RadioGroup';
import Radio from 'components/Radio';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { formatSSS, formatTIN, stripNonDigit, stripNonNumeric, formatInputDate } from 'utils/functions';
import { subscriptionService } from 'utils/SubscriptionService';

import { NavWrapper } from 'containers/GovernmentForms/Generate/styles';

import PageWrapper from '../../PageWrapper';
import * as sssmclActions from './actions';

import {
    makeSelectData,
    makeSelectPageStatus,
    makeSelectDownloadUrl,
    makeSelectNotification
} from './selectors';

import {
    GovtFormWrapper,
    MessageWrapperStyles,
    ContributionListHeader,
    SearchHeader,
    FormTitle,
    InputWrapper,
    AllCaps
} from './styles';
import {
    REPORT_TYPES,
    LOCATOR_CODES,
    DEFAULT_LOCATOR_CODE,
    REQUIRED_FIELDS
} from './constants';
import { PAGE_STATUSES } from '../../constants';

/**
 * SSS MCL component
 */
export class SSSMCLForm extends React.PureComponent {
    static headerFormProperties = {
        employer_type: true,
        employer_sss_number: true,
        employer_rm_flr_unit: true,
        employer_city: true,
        employer_zip_code: false,
        employer_name: true,
        employer_tin: true,
        employer_tel_no: false,
        employer_mobile_no: false,
        employer_email: false,
        employer_website: false,
        subtotal_ss_contribution: true,
        subtotal_ec_contribution: true,
        form_of_payment: false,
        cheque_number: false,
        cheque_date: false,
        bank_branch_name: false,
        amount_paid_in_figures: true,
        total_amount_words: true,
        cert_correct_and_paid_name: false,
        cert_correct_and_paid_designation: false,
        cert_correct_and_paid_date: false,
        tr_sbr_no: true,
        date_paid: true
    }

    static propTypes = {
        generateForm: React.PropTypes.func,
        data: React.PropTypes.object,
        pageStatus: React.PropTypes.string,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        downloadUrl: React.PropTypes.string,
        routeParams: React.PropTypes.object,
        getFileLink: React.PropTypes.func,
        initializeData: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        products: React.PropTypes.array,
        saveLineItem: React.PropTypes.func,
        saveFormHeader: React.PropTypes.func,
        setDownloadUrl: React.PropTypes.func,
        notifyUser: React.PropTypes.func
    }

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );
        this.editingRowValueRefs = {
            input: {},
            currencies: {},
            select: {},
            dates: {}
        };
        this.headerEdited = false;
        this.state = {
            data: {
                type: 'govt_form_sss_remittance',
                attributes: {
                    company_id: 0,
                    year_from: 0,
                    month_from: 0,
                    year_to: 0,
                    month_to: 0,
                    employer_type: '',
                    employer_sss_number: '',
                    employer_rm_flr_unit: '',
                    employer_house_lot: '',
                    employer_street: '',
                    employer_subdivision: '',
                    employer_brgy_district: '',
                    employer_city: '',
                    employer_province: '',
                    employer_zip_code: '',
                    employer_name: '',
                    employer_tin: '',
                    employer_tel_no: '',
                    quarter_ending_month: '',
                    quarter_ending_year: '',
                    adjustment_type: '',
                    employer_mobile_no: '',
                    employer_email: '',
                    employer_website: '',
                    applicable_period_month: '',
                    applicable_period_year: '',
                    tr_sbr_no: '',
                    date_paid: '',
                    amount_paid: '',
                    add_penalty_ss_contribution: '',
                    add_penalty_ec_contribution: '',
                    add_penalty_total_contribution: '',
                    add_underpayment_ss_contribution: '',
                    add_underpayment_ec_contribution: '',
                    add_underpayment_total_contribution: '',
                    subtotal_ss_contribution: '',
                    subtotal_ec_contribution: '',
                    subtotal_total_contribution: '',
                    total_amount_of_payment: '',
                    form_of_payment: '',
                    cheque_number: null,
                    cheque_date: null,
                    bank_branch_name: null,
                    amount_paid_in_figures: '',
                    total_amount_words: '',
                    cert_correct_and_paid_name: '',
                    cert_correct_and_paid_designation: '',
                    cert_correct_and_paid_date: '',
                    report_type: '',
                    locator_code: 'C',
                    line_items: []
                }
            },
            pager: '1 - 10',
            search: '',
            uri: '',
            showModal: false,
            editingRow: null,
            downloadUrl: ''
        };
        this.filterEmployees = debounce( this.filterEmployees.bind( this ), 250, { leading: true, trailing: true });
        this.setPager = this.setPager.bind( this );
    }

    /**
     * runs before render.
     * checks if there is a valid data to generate or if user is authorized to view this page.
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.government_form'], ( authorized ) => { !authorized && browserHistory.replace( '/unauthorized' ); });

        if ( this.props.routeParams.id ) {
            this.props.initializeData({ id: this.props.routeParams.id });
        } else {
            browserHistory.replace( '/forms' );
        }
    }

    /**
     * Perform actions when component props changes
     */
    componentWillReceiveProps( nextProps ) {
        ( nextProps.data !== this.props.data ) && this.setState({ data: nextProps.data });

        nextProps.downloadUrl !== this.props.downloadUrl
            && nextProps.downloadUrl
            && this.openFile( nextProps.downloadUrl );

        nextProps.pageStatus === PAGE_STATUSES.READY
            && this.props.pageStatus === PAGE_STATUSES.SAVING
            && this.setState({ editingRow: null }, () => {
                this.editingRowValueRefs = { input: {}, select: {}, dates: {}, currencies: {}};
            });
    }

    /**
     * Component form check
     */
    componentDidUpdate( prevProps ) {
        if ( prevProps.data !== this.props.data ) {
            REQUIRED_FIELDS.INPUTS.forEach( ( key ) => {
                const input = this[ key ];
                input && input._validate( input.state.value );
            });

            REQUIRED_FIELDS.DATE_PICKERS.forEach( ( key ) => {
                const datePicker = this[ key ];
                datePicker && datePicker.checkRequired();
            });
        }
    }

    /**
     * Called before component unmounts.
     */
    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * Sets the table pager's label
     */
    setPager( tableProps ) {
        if ( tableProps.data.length ) {
            const pageSize = tableProps.data.length < tableProps.pageSize ? tableProps.data.length : tableProps.pageSize;
            let totalItems = pageSize * ( tableProps.page + 1 );
            if ( tableProps.pages === tableProps.page + 1 ) {
                totalItems = tableProps.data.length < totalItems ? tableProps.data.length : totalItems;
            }
            this.setState({ pager: `${( pageSize * tableProps.page ) + 1} - ${totalItems}` });
        } else {
            this.setState({ pager: '0' });
        }
    }

    /**
     * Get the table columns for render
     */
    getTableColumns() {
        const pageNotReady = this.props.pageStatus !== PAGE_STATUSES.READY;
        const saving = this.props.pageStatus === PAGE_STATUSES.SAVING;

        const columns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'employee_id',
                accessor: 'employee_id',
                show: false
            },
            {
                header: 'SSS ID No.',
                accessor: 'sss_number',
                sortable: false,
                width: 180,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <GovernmentNumbersInput
                                type="sss"
                                id="sss_number"
                                className="editing"
                                value={ row.sss_number }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.sss_number = ref; } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.input.sss_number.setState(
                                        { value: formatSSS( value ) }
                                    );
                                } }
                                required
                            />
                        </InputWrapper>
                        :
                        <div>
                            {row.sss_number}
                        </div>
                )
            },
            {
                header: 'Last Name',
                accessor: 'last_name',
                sortable: false,
                minWidth: 180,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="last_name"
                                className="editing"
                                value={ row.last_name }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.last_name = ref; } }
                                required
                            />
                        </InputWrapper>
                        :
                        <AllCaps>
                            {row.last_name}
                        </AllCaps>
                )
            },
            {
                header: 'First Name',
                accessor: 'first_name',
                sortable: false,
                minWidth: 180,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="first_name"
                                className="editing"
                                value={ row.first_name }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.first_name = ref; } }
                                required
                            />
                        </InputWrapper>
                        :
                        <AllCaps>
                            {row.first_name}
                        </AllCaps>
                )
            },
            {
                header: 'Middle Name',
                accessor: 'middle_name',
                sortable: false,
                minWidth: 180,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="middle_name"
                                className="editing"
                                value={ row.middle_name }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.middle_name = ref; } }
                            />
                        </InputWrapper>
                        :
                        <AllCaps>
                            {row.middle_name}
                        </AllCaps>
                )
            },
            {
                header: 'Employee Contribution',
                accessor: 'ee_share',
                sortable: false,
                minWidth: 200,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="employee_contribution"
                                className="editing currency"
                                value={ this.formatCurrency( row.ee_share ) }
                                disabled={ pageNotReady }
                                onBlur={ ( value ) => {
                                    this.editingRowValueRefs.currencies.ee_share.setState(
                                        { value: this.formatCurrency( value ) }
                                    );
                                } }
                                onFocus={ () => {
                                    this.editingRowValueRefs.currencies.ee_share.setState(
                                        { value: stripNonDigit( this.editingRowValueRefs.currencies.ee_share.state.value ) }
                                    );
                                } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.currencies.ee_share.setState(
                                        { value: stripNonDigit( value ) }
                                    );
                                } }
                                ref={ ( ref ) => { this.editingRowValueRefs.currencies.ee_share = ref; } }
                                required
                            />
                        </InputWrapper>
                        :
                        <div>
                            {this.formatCurrency( row.ee_share || 0 )}
                        </div>
                )
            },
            {
                header: 'Employer Contribution',
                accessor: 'er_share',
                sortable: false,
                minWidth: 200,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="employer_contribution"
                                className="editing currency"
                                value={ this.formatCurrency( row.er_share ) }
                                onBlur={ ( value ) => {
                                    this.editingRowValueRefs.currencies.er_share.setState(
                                        { value: this.formatCurrency( value ) }
                                    );
                                } }
                                onFocus={ () => {
                                    this.editingRowValueRefs.currencies.er_share.setState(
                                        { value: stripNonDigit( this.editingRowValueRefs.currencies.er_share.state.value ) }
                                    );
                                } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.currencies.er_share.setState(
                                        { value: stripNonDigit( value ) }
                                    );
                                } }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.currencies.er_share = ref; } }
                                required
                            />
                        </InputWrapper>
                        :
                        <div>
                            {this.formatCurrency( row.er_share || 0 )}
                        </div>
                )
            },
            {
                header: 'Employee Compensation',
                accessor: 'ec_share',
                sortable: false,
                minWidth: 200,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="ec_share"
                                className="editing currency"
                                value={ this.formatCurrency( row.ec_share ) }
                                onBlur={ ( value ) => {
                                    this.editingRowValueRefs.currencies.ec_share.setState(
                                        { value: this.formatCurrency( value ) }
                                    );
                                } }
                                onFocus={ () => {
                                    this.editingRowValueRefs.currencies.ec_share.setState(
                                        { value: stripNonDigit( this.editingRowValueRefs.currencies.ec_share.state.value ) }
                                    );
                                } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.currencies.ec_share.setState(
                                        { value: stripNonDigit( value ) }
                                    );
                                } }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.currencies.ec_share = ref; } }
                                required
                            />
                        </InputWrapper>
                        :
                        <div>
                            {this.formatCurrency( row.ec_share || 0 )}
                        </div>
                )
            },
            {
                header: 'Provident Employee',
                accessor: 'ee_provident_fund',
                sortable: false,
                minWidth: 200,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="ee_provident_fund"
                                className="editing currency"
                                value={ this.formatCurrency( row.ee_provident_fund ) }
                                onBlur={ ( value ) => {
                                    this.editingRowValueRefs.currencies.ee_provident_fund.setState(
                                        { value: this.formatCurrency( value ) }
                                    );
                                } }
                                onFocus={ () => {
                                    this.editingRowValueRefs.currencies.ee_provident_fund.setState(
                                        { value: stripNonDigit( this.editingRowValueRefs.currencies.ee_provident_fund.state.value ) }
                                    );
                                } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.currencies.ee_provident_fund.setState(
                                        { value: stripNonDigit( value ) }
                                    );
                                } }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.currencies.ee_provident_fund = ref; } }
                                required
                            />
                        </InputWrapper>
                        :
                        <div>
                            {this.formatCurrency( row.ee_provident_fund || 0 )}
                        </div>
                )
            },
            {
                header: 'Provident Employer',
                accessor: 'er_provident_fund',
                sortable: false,
                minWidth: 200,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="er_provident_fund"
                                className="editing currency"
                                value={ this.formatCurrency( row.er_provident_fund ) }
                                onBlur={ ( value ) => {
                                    this.editingRowValueRefs.currencies.er_provident_fund.setState(
                                        { value: this.formatCurrency( value ) }
                                    );
                                } }
                                onFocus={ () => {
                                    this.editingRowValueRefs.currencies.er_provident_fund.setState(
                                        { value: stripNonDigit( this.editingRowValueRefs.currencies.er_provident_fund.state.value ) }
                                    );
                                } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.currencies.er_provident_fund.setState(
                                        { value: stripNonDigit( value ) }
                                    );
                                } }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.currencies.er_provident_fund = ref; } }
                                required
                            />
                        </InputWrapper>
                        :
                        <div>
                            {this.formatCurrency( row.er_provident_fund || 0 )}
                        </div>
                )
            },
            {
                header: 'Separation Date',
                accessor: 'separation_date',
                sortable: false,
                width: 200,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="separation_date"
                                className="editing"
                                placeholder={ this.getDateTemplate() }
                                value={ ( row.separation_date != null && row.separation_date !== '0000-00-00' ) ?
                                    moment( row.separation_date, 'YYYY-MM-DD' ).format( 'MM/DD/YYYY' ) : '' }
                                onBlur={ ( value ) => {
                                    if ( !value ) {
                                        return;
                                    }
                                    const regex = new RegExp( /^\d{2}\/\d{2}\/\d{4}$/ );
                                    const targetDate = moment( value, 'MM/DD/YYYY' );
                                    const valid = regex.test( value ) && targetDate.isValid();
                                    this.editingRowValueRefs.dates.separation_date.setState({
                                        error: !valid,
                                        errorMessage: valid ? ' ' : 'Invalid date'
                                    });
                                } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.dates.separation_date.setState(
                                        { value: formatInputDate( value ) }
                                    );
                                } }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.dates.separation_date = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            { ( row.separation_date !== null && row.separation_date !== '0000-00-00' ) ? moment( row.separation_date, 'YYYY-MM-DD' ).format( 'MM/DD/YYYY' ) : '' }
                        </div>
                )
            },
            {
                header: '',
                accessor: '',
                sortable: false,
                width: 200,
                render: ({ row }) => (
                    ( row.id !== this.state.editingRow )
                        ? <Button
                            id={ `button-edit-${row.id}` }
                            label={ <span>Edit</span> }
                            type="grey"
                            disabled={ saving }
                            onClick={ () => { this.setState({ editingRow: row.id }); } }
                        />
                        : <div>
                            <Button
                                id={ `button-cancel-${row.id}` }
                                label={ <span>Cancel</span> }
                                type="neutral"
                                disabled={ saving }
                                onClick={ () => {
                                    this.setState({ editingRow: null });
                                    this.editingRowValueRefs = { currencies: {}, input: {}, select: {}, dates: {}};
                                } }
                            />
                            <Button
                                id={ `button-submit-${row.id}` }
                                label={ saving ? 'Saving' : <span>Submit</span> }
                                type="action"
                                onClick={ () => { this.updateLineItem(); } }
                                disabled={ this.props.pageStatus === PAGE_STATUSES.SAVING }
                            />
                        </div>
                )
            }
        ];
        return columns;
    }

    /**
     * Get date placeholder
     */
    getDateTemplate() {
        const date = moment().format( 'MM/DD/YYYY' );
        return `Example: ${date}`;
    }
    /**
     * gets the filtered data
     */
    getFilteredData() {
        if ( this.state.search.length ) {
            const search = this.state.search.toLowerCase();
            return this.state.data.attributes.line_items.filter( ( employee ) =>
                employee.last_name.toLowerCase().includes( search )
                    || employee.first_name.toLowerCase().includes( search )
                    || employee.sss_number.toLowerCase().includes( search )
            );
        }
        return this.state.data.attributes.line_items;
    }

    /**
     * Get pager text with non breaking spaces
     */
    getPagerText() {
        return `Showing ${this.state.pager} of ${this.formatNumber( this.state.data.attributes.line_items.length )} entries`;
    }

    /**
     * Download file to browser
     */
    openFile( url ) {
        if ( url ) {
            const linkElement = document.getElementById( 'downloadLink' );
            linkElement.target = '';
            linkElement.download = '';
            linkElement.href = url;
            linkElement.click();
            linkElement.href = '';
            this.props.setDownloadUrl( '' );
        }
    }

    /**
     * format the data to currency
     * @param value
     * @returns {XML}
     */
    formatCurrency( value ) {
        const convertedValue = Number( stripNonDigit( value ) );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `${Number( convertedValue ).toFixed( 2 ).replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    /**
     * format the data to currency
     * @param value
     * @returns {XML}
     */
    formatNumber( value ) {
        const convertedValue = Number( value );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `${Number( convertedValue ).toString().replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    removeEmptyValues( obj ) {
        const newObj = {};
        Object.keys( obj ).forEach( ( key ) => {
            if ( obj[ key ] !== null && obj[ key ] !== '' && obj[ key ] !== 'Invalid date' ) {
                newObj[ key ] = obj[ key ];
            }
        });
        return newObj;
    }

    checkHeaderForm() {
        for ( const [ key, required ] of Object.entries( SSSMCLForm.headerFormProperties ) ) {
            if (
                    ( required && (
                        ( key === 'date_paid' && !this[ `${key}` ].state.selectedDay ) ||
                        ( key !== 'date_paid' && !this[ `${key}` ].state.value )
                        )
                    ) || this[ `${key}` ].state.error
                ) return false;
        }
        return true;
    }

    /**
     * Downloads file to user
     */
    saveAndDownloadFile() {
        if ( !this.checkHeaderForm() ) {
            this.props.notifyUser(
                {
                    show: true,
                    title: 'Error',
                    message: 'Missing required fields.',
                    type: 'error'
                }
            );
            return;
        }
        if ( this.headerEdited ) {
            this.props.saveFormHeader(
                this.removeEmptyValues({
                    id: this.props.routeParams.id,
                    company_id: this.state.data.attributes.company_id,
                    employer_type: this.employer_type.state.value,
                    employer_sss_number: this.employer_sss_number.state.value,
                    employer_rm_flr_unit: this.employer_rm_flr_unit.state.value,
                    employer_city: this.employer_city.state.value,
                    employer_zip_code: this.employer_zip_code.state.value,
                    employer_name: this.employer_name.state.value,
                    employer_tin: this.employer_tin.state.value,
                    employer_tel_no: this.employer_email.state.value,
                    employer_mobile_no: this.employer_mobile_no.state.value,
                    employer_email: this.employer_email.state.value,
                    employer_website: this.employer_website.state.value,
                    subtotal_ss_contribution: stripNonDigit( this.subtotal_ss_contribution.state.value ),
                    subtotal_ec_contribution: stripNonDigit( this.subtotal_ec_contribution.state.value ),
                    form_of_payment: this.form_of_payment.state.value,
                    cheque_number: this.cheque_number.state.value,
                    cheque_date: ( this.cheque_date.selectedDay !== null ) ?
                        moment( this.cheque_date.state.selectedDay, 'MM/DD/YYYY' ).format( 'YYYY-MM-DD' ) : null,
                    bank_branch_name: this.bank_branch_name.state.value,
                    amount_paid_in_figures: stripNonDigit( this.amount_paid_in_figures.state.value ),
                    total_amount_words: this.total_amount_words.state.value,
                    cert_correct_and_paid_name: this.cert_correct_and_paid_name.state.value,
                    cert_correct_and_paid_designation: this.cert_correct_and_paid_designation.state.value,
                    cert_correct_and_paid_date: ( this.cert_correct_and_paid_date.state.selectedDay !== null ) ?
                        moment( this.cert_correct_and_paid_date.state.selectedDay, 'MM/DD/YYYY' ).format( 'YYYY-MM-DD' ) : null,
                    report_type: this.report_type.state.value.value,
                    locator_code: this.locator_code.state.value.value,
                    tr_sbr_no: this.tr_sbr_no.state.value,
                    date_paid: ( this.date_paid.state.selectedDay !== null ) ?
                        moment( this.date_paid.state.selectedDay, 'MM/DD/YYYY' ).format( 'YYYY-MM-DD' ) : null
                })
            );
            this.headerEdited = false;
        } else {
            this.props.getFileLink({ id: this.props.routeParams.id });
        }
    }

    /**
     * regenerate form
     */
    regenerateForm() {
        this.props.generateForm(
            {
                company_id: this.state.data.attributes.company_id,
                month_from: this.state.data.attributes.month_from,
                year_from: this.state.data.attributes.year_from,
                month_to: this.state.data.attributes.month_to,
                year_to: this.state.data.attributes.year_to
            }
        );
    }

    /**
     * Update line item
     */
    updateLineItem() {
        const lineItem = {};
        for ( const [ key, obj ] of Object.entries( this.editingRowValueRefs.currencies ) ) {
            if ( obj.state.error ) return;
            lineItem[ `${key}` ] = stripNonDigit( `${obj.state.value}` );
        }

        for ( const [ key, obj ] of Object.entries( this.editingRowValueRefs.input ) ) {
            if ( obj.state.error ) return;
            lineItem[ `${key}` ] = `${obj.state.value}`.toUpperCase();
        }

        for ( const [ key, obj ] of Object.entries( this.editingRowValueRefs.select ) ) {
            if ( obj.state.error ) return;
            lineItem[ `${key}` ] = obj.state.value.value;
        }

        for ( const [ key, obj ] of Object.entries( this.editingRowValueRefs.dates ) ) {
            if ( obj.state.error ) return;
            const dateObj = moment( obj.state.value, 'MM/DD/YYYY' ).format( 'YYYY-MM-DD' );
            if ( dateObj !== 'Invalid date' ) {
                lineItem[ `${key}` ] = dateObj;
            }
        }

        lineItem.id = this.state.editingRow;
        this.props.saveLineItem(
            this.removeEmptyValues(
                { id: this.state.data.id, company_id: this.state.data.attributes.company_id, lineItem }
            )
        );
    }

    /**
     * Filter employees by search criteria
     * @param {*} criteria
     */
    filterEmployees( criteria ) {
        this.setState({ search: criteria }, () => this.setPager( this.contributionTable.tableComponent.state ) );
    }

    /**
     * Validate report type
     */
    checkReportType() {
        if ( this.report_type.state.value === null ) {
            this.report_type.setState(
                { value: REPORT_TYPES[ 0 ] }
            );
        }
        this.headerEdited = true;
    }

    /**
     * Check locator code
     */
    checkLocatorCode() {
        if ( this.locator_code.state.value === null ) {
            this.locator_code.setState(
                { value: LOCATOR_CODES[ 2 ] } // NCR
            );
        }
        this.headerEdited = true;
    }

    /**
     * renders a loading screen
     */
    renderLoadingPage( generating = false ) {
        return (
            <MessageWrapperStyles>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        <H3>{ generating ? 'Generating.' : 'Loading data.' } Please wait...</H3>
                    </div>
                </div>
            </MessageWrapperStyles>
        );
    }

    /**
     * renders the form fields necessary to finish generating the SSS forms
     */
    renderForm() {
        const { data } = this.state;
        const pageNotReady = this.props.pageStatus !== PAGE_STATUSES.READY;

        return (
            <PageWrapper step={ 2 } >
                <GovtFormWrapper>
                    <div className="sss-header">
                        <h5>Social Security System (SSS)</h5>
                        <p>To accomplish your SSS Monthly Contribution Collection List, follow the steps below:</p>
                        <div className="row msrf-directions-row">
                            <div className="col-sm-8 directions">
                                <ol>
                                    <li>Fill-out the fields required below.</li>
                                    <li>Generate and print the R3 and R5 PDF files.</li>
                                </ol>
                            </div>
                            <div className="col-sm-4 gov-form-options">
                                <div className="buttonbox">
                                    <Button
                                        id="button-regenerate"
                                        label="Regenerate Form"
                                        type="neutral"
                                        size="large"
                                        disabled={ pageNotReady }
                                        onClick={ () => { this.regenerateForm(); } }
                                    />
                                    <Button
                                        id="button-save-and-download"
                                        label="Save and Download"
                                        type="action"
                                        size="large"
                                        disabled={ pageNotReady }
                                        onClick={ () => { this.saveAndDownloadFile(); } }
                                    />
                                    <A download id="downloadLink"></A>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="company-section">
                        <FormTitle><h3>SSS MONTHLY CONTRIBUTION COLLECTION LIST</h3></FormTitle>

                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="period_covered"
                                    label="Period Covered"
                                    value={ moment( `${data.attributes.month_from}-01-${data.attributes.year_from}`, 'MM-DD-YYYY' ).format( 'MMMM YYYY' ) }
                                    disabled
                                />
                            </div>
                            <div className="col-xs-8">
                                <Input
                                    id="employer-name"
                                    label={
                                        <span>
                                            <span>Employer/Business Name</span>
                                            <span style={ { position: 'absolute', paddingLeft: '20px' } } >
                                                <ToolTip
                                                    id="employer_name_tip"
                                                    target={ <i className="fa fa-info-circle" /> }
                                                    placement="right"
                                                >
                                                    Please ensure that the Registered Business name is entered in this field.
                                                </ToolTip>
                                            </span>
                                        </span>
                                    }
                                    value={ data.attributes.employer_name || '' }
                                    ref={ ( ref ) => { this.employer_name = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    required
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">

                            <div className="col-xs-4">
                                <GovernmentNumbersInput
                                    type="sss"
                                    id="employer_sss_number"
                                    label="Employer SSS Number"
                                    value={ data.attributes.employer_sss_number || '' }
                                    ref={ ( ref ) => { this.employer_sss_number = ref; } }
                                    onChange={ ( value ) => {
                                        this.employer_sss_number.setState(
                                            { value: formatSSS( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    required
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="employer_address"
                                    label="Employer/Business Address"
                                    value={ data.attributes.employer_rm_flr_unit || '' }
                                    ref={ ( ref ) => { this.employer_rm_flr_unit = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    required
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="employer_city"
                                    label="City"
                                    value={ data.attributes.employer_city || '' }
                                    ref={ ( ref ) => { this.employer_city = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    required
                                    disabled={ pageNotReady }
                                />
                            </div>

                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="employer_zip_code"
                                    label="ZIP Code"
                                    value={ data.attributes.employer_zip_code || '' }
                                    ref={ ( ref ) => { this.employer_zip_code = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <GovernmentNumbersInput
                                    type="tin"
                                    id="employer_tin"
                                    label="Employer TIN"
                                    value={ data.attributes.employer_tin || '' }
                                    ref={ ( ref ) => { this.employer_tin = ref; } }
                                    onChange={ ( value ) => {
                                        this.employer_tin.setState(
                                            { value: formatTIN( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="employer_tel_no"
                                    label="Telephone Number"
                                    value={ data.attributes.employer_tel_no || '' }
                                    ref={ ( ref ) => { this.employer_tel_no = ref; } }
                                    onChange={ ( value ) => {
                                        this.employer_tel_no.setState(
                                            { value: stripNonNumeric( value ).substring( 0, 15 ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="label col-xs-4">
                                <div>
                                    <label htmlFor="employer_type">Type of Employer</label>
                                </div>
                                <RadioGroup
                                    id="employer_type"
                                    label="Type of Employer"
                                    horizontal
                                    className="radio-group"
                                    value={ data.attributes.employer_type }
                                    ref={ ( ref ) => { this.employer_type = ref; } }
                                    onChange={ () => {
                                        this.headerEdited = true;
                                    } }
                                    disabled={ pageNotReady }
                                >
                                    <Radio value="BUSINESS"><p className="label">Business</p></Radio>
                                    <Radio value="HOUSEHOLD"><p className="label">Household</p></Radio>
                                </RadioGroup>
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="employer_mobile_no"
                                    label="Mobile/Cellphone Number"
                                    value={ data.attributes.employer_mobile_no || '' }
                                    ref={ ( ref ) => { this.employer_mobile_no = ref; } }
                                    onChange={ ( value ) => {
                                        this.employer_mobile_no.setState({
                                            value: stripNonNumeric( value ).substring( 0, 15 )
                                        });
                                        this.headerEdited = true;
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="employer_email"
                                    label="Email"
                                    type="email"
                                    value={ data.attributes.employer_email || '' }
                                    ref={ ( ref ) => { this.employer_email = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="employer_website"
                                    label="Website"
                                    value={ data.attributes.employer_website || '' }
                                    ref={ ( ref ) => { this.employer_website = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Select
                                    id="locator_code"
                                    label="Locator Code"
                                    value={ data.attributes.locator_code || DEFAULT_LOCATOR_CODE }
                                    disabled={ pageNotReady }
                                    data={ LOCATOR_CODES }
                                    ref={ ( ref ) => { this.locator_code = ref; } }
                                    onChange={ () => { this.checkLocatorCode(); } }
                                />

                            </div>
                            <div className="col-xs-4">
                                <Select
                                    id="report_type"
                                    label="Report Type"
                                    value={ data.attributes.report_type || 'SSS_R3' }
                                    disabled={ pageNotReady }
                                    data={ REPORT_TYPES }
                                    ref={ ( ref ) => { this.report_type = ref; } }
                                    onChange={ () => { this.checkReportType(); } }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="subtotal_ss_contribution"
                                    label="Total SSS Contribution"
                                    className="currency"
                                    value={ this.formatCurrency( data.attributes.subtotal_ss_contribution ) || '' }
                                    onBlur={ ( value ) => {
                                        this.subtotal_ss_contribution.setState(
                                            { value: this.formatCurrency( value ) }
                                        );
                                    } }
                                    onFocus={ () => {
                                        this.subtotal_ss_contribution.setState(
                                            { value: stripNonDigit( this.subtotal_ss_contribution.state.value ) }
                                        );
                                    } }
                                    onChange={ ( value ) => {
                                        this.subtotal_ss_contribution.setState(
                                            { value: stripNonDigit( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    ref={ ( ref ) => { this.subtotal_ss_contribution = ref; } }
                                    required
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="subtotal_ec_contribution"
                                    label="Total Employee Compensation"
                                    className="currency"
                                    value={ this.formatCurrency( data.attributes.subtotal_ec_contribution ) || '' }
                                    onBlur={ ( value ) => {
                                        this.subtotal_ec_contribution.setState(
                                            { value: this.formatCurrency( value ) }
                                        );
                                    } }
                                    onFocus={ () => {
                                        this.subtotal_ec_contribution.setState(
                                            { value: stripNonDigit( this.subtotal_ec_contribution.state.value ) }
                                        );
                                    } }
                                    onChange={ ( value ) => {
                                        this.subtotal_ec_contribution.setState(
                                            { value: stripNonDigit( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    ref={ ( ref ) => { this.subtotal_ec_contribution = ref; } }
                                    required
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="amount_paid_in_figures"
                                    label="Total Amount Paid"
                                    className="currency"
                                    value={ this.formatCurrency( data.attributes.amount_paid_in_figures ) || '' }
                                    onBlur={ ( value ) => {
                                        this.amount_paid_in_figures.setState(
                                            { value: this.formatCurrency( value ) }
                                        );
                                    } }
                                    onFocus={ () => {
                                        this.amount_paid_in_figures.setState(
                                            { value: stripNonDigit( this.amount_paid_in_figures.state.value ) }
                                        );
                                    } }
                                    onChange={ ( value ) => {
                                        this.amount_paid_in_figures.setState(
                                            { value: stripNonDigit( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    ref={ ( ref ) => { this.amount_paid_in_figures = ref; } }
                                    required
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-8">
                                <Input
                                    id="total_amount_words"
                                    label="Total Amount Paid in Words"
                                    className="all-caps"
                                    value={ data.attributes.total_amount_words || '' }
                                    ref={ ( ref ) => { this.total_amount_words = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    required
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <div>
                                    <label htmlFor="form_of_payment">Form of Payment</label>
                                </div>
                                <RadioGroup
                                    id="form_of_payment"
                                    horizontal
                                    value={ data.attributes.form_of_payment }
                                    ref={ ( ref ) => { this.form_of_payment = ref; } }
                                    onChange={ ( value ) => {
                                        this.headerEdited = true;
                                        if ( value === 'CHEQUE' ) {
                                            this.cheque_number.setState({ disabled: false });
                                            this.cheque_date.setState({ disabled: false });
                                            this.bank_branch_name.setState({ disabled: false });
                                        } else {
                                            this.cheque_number.setState({ disabled: true });
                                            this.cheque_date.setState({ disabled: true });
                                            this.bank_branch_name.setState({ disabled: true });
                                        }
                                    } }
                                    disabled={ pageNotReady }
                                >
                                    <Radio value="CASH" ><p className="label">Cash</p></Radio>
                                    <Radio value="POSTAL" ><p className="label">Postal Money Order</p></Radio>
                                    <Radio value="CHEQUE"><p className="label">Check</p></Radio>
                                </RadioGroup>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="cheque_number"
                                    value={ data.attributes.cheque_number || '' }
                                    disabled={ this.form_of_payment !== 'CHEQUE' || pageNotReady }
                                    ref={ ( ref ) => { this.cheque_number = ref; } }
                                    label="Check Number"
                                    onChange={ () => { this.headerEdited = true; } }
                                />
                            </div>
                            <div className="col-xs-4">
                                <DatePicker
                                    id="cheque_date"
                                    label="Check Date"
                                    dayFormat="MM/DD/YYYY"
                                    selectedDay={ ( data.attributes.cheque_date !== null && data.attributes.cheque_date !== '0000-00-00' ) ?
                                                   moment( data.attributes.cheque_date, 'YYYY-MM-DD' ).format( 'MM/DD/YYYY' ) : null }
                                    disabled={ this.form_of_payment !== 'CHEQUE' || pageNotReady }
                                    ref={ ( ref ) => { this.cheque_date = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="bank_branch_name"
                                    label="Bank &amp; Branch Name"
                                    disabled={ this.form_of_payment !== 'CHEQUE' || pageNotReady }
                                    value={ data.attributes.bank_branch_name || '' }
                                    ref={ ( ref ) => { this.bank_branch_name = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="cert_correct_and_paid_name"
                                    label="Authorized Signatory"
                                    value={ data.attributes.cert_correct_and_paid_name || '' }
                                    ref={ ( ref ) => { this.cert_correct_and_paid_name = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="cert_correct_and_paid_designation"
                                    label="Designation"
                                    value={ data.attributes.cert_correct_and_paid_designation || '' }
                                    ref={ ( ref ) => { this.cert_correct_and_paid_designation = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <DatePicker
                                    label="Submission Date"
                                    dayFormat="MM/DD/YYYY"
                                    selectedDay={ ( data.attributes.cert_correct_and_paid_date !== null && data.attributes.cert_correct_and_paid_date !== '0000-00-00' ) ?
                                                   moment( data.attributes.cert_correct_and_paid_date, 'YYYY-MM-DD' ).format( 'MM/DD/YYYY' ) : null }
                                    ref={ ( ref ) => { this.cert_correct_and_paid_date = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="tr_sbr_no"
                                    label="Document Number"
                                    value={ data.attributes.tr_sbr_no || '' }
                                    ref={ ( ref ) => { this.tr_sbr_no = ref; } }
                                    required
                                    onChange={ ( value ) => {
                                        this.headerEdited = true;
                                        this.tr_sbr_no.setState({ value: stripNonNumeric( value ) });
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <DatePicker
                                    label="Document Date"
                                    dayFormat="MM/DD/YYYY"
                                    required
                                    selectedDay={ ( data.attributes.date_paid !== null && data.attributes.date_paid !== '0000-00-00' ) ?
                                                   moment( data.attributes.date_paid, 'YYYY-MM-DD' ).format( 'MM/DD/YYYY' ) : null }
                                    ref={ ( ref ) => { this.date_paid = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                    </div>
                    <div className="contribution-list">
                        { this.renderTableData() }
                    </div>
                </GovtFormWrapper>
            </PageWrapper>
        );
    }

    /**
     * Render table data if there are line items
     */
    renderTableData() {
        const items = this.state.data.attributes.line_items.length;
        if ( items ) {
            return (
                <div>
                    <ContributionListHeader>
                        <SearchHeader>
                            <div className="title"><H5>SSS Monthly Collection List</H5></div>
                            <div>
                                <Search
                                    id="textSearch"
                                    className="col-xs-4"
                                    placeholder="Search for SSS ID, last name or first name"
                                    handleSearch={ this.filterEmployees }
                                />
                            </div>
                        </SearchHeader>
                        <span id="pager">{ this.getPagerText() }</span>
                    </ContributionListHeader>
                    <Table
                        columns={ this.getTableColumns() }
                        data={ this.getFilteredData() || [] }
                        pagination={ ( this.state.data.attributes.line_items.length || 0 ) > 10 }
                        ref={ ( ref ) => { this.contributionTable = ref; } }
                        onDataChange={ this.setPager }
                    />
                </div>
            );
        }
        return (
            <div className="NoDataFound"><span>No Data</span></div>
        );
    }

    /**
     * Renders Form component to DOM
     */
    render() {
        const { notification, pageStatus } = this.props;
        const isLoading = pageStatus === PAGE_STATUSES.LOADING;
        const isGenerating = pageStatus === PAGE_STATUSES.GENERATING;

        return (
            <div>
                <Helmet
                    title="Government Forms: SSS"
                    meta={ [
                        { name: 'description', content: 'Generate SSS Forms' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A href="/payroll/forms">
                            &#8592; Back to Government Forms
                        </A>
                    </Container>
                </NavWrapper>
                {
                    ( isLoading || isGenerating )
                        ? this.renderLoadingPage( isGenerating )
                        : this.renderForm()
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    data: makeSelectData(),
    pageStatus: makeSelectPageStatus(),
    downloadUrl: makeSelectDownloadUrl(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        sssmclActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( SSSMCLForm );
