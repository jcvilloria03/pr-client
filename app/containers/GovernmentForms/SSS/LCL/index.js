import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';
import debounce from 'lodash/debounce';

import GovernmentNumbersInput from 'components/GovernmentNumbersInput';
import Select from 'components/Select';
import SnackBar from 'components/SnackBar';
import Input from 'components/Input';
import { H3, H5 } from 'components/Typography';
import Table from 'components/Table';
import Button from 'components/Button';
import A from 'components/A';
import ToolTip from 'components/ToolTip';
import DatePicker from 'components/DatePicker';
import Search from 'components/Search';
import { formatSSS, formatInputDate, stripNonDigit, stripNonNumeric } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';

import { NavWrapper } from 'containers/GovernmentForms/Generate/styles';

import PageWrapper from '../../PageWrapper';
import * as hdmfFormActions from './actions';

import {
    makeSelectData,
    makeSelectPageStatus,
    makeSelectDownloadUrl,
    makeSelectNotification
} from './selectors';

import {
    GovtFormWrapper,
    MessageWrapperStyles,
    ContributionListHeader,
    SearchHeader,
    FormTitle,
    InputWrapper
} from './styles';
import { SSS_LOAN_TYPES, REMARKS, REQUIRED_FIELDS } from './constants';
import { PAGE_STATUSES } from '../../constants';

/**
 * SSS LCL component
 */
export class SSSLCLForm extends React.PureComponent {
    static headerFormProperties = {
        employer_sss_number: true,
        employer_name: true,
        sss_branch_code: true,
        repayment_no: false,
        repayment_date: true,
        total_amount_paid: true,
        total_number_of_employees: true
    }
    static propTypes = {
        generateForm: React.PropTypes.func,
        data: React.PropTypes.object,
        pageStatus: React.PropTypes.string,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        routeParams: React.PropTypes.object,
        getFileLink: React.PropTypes.func,
        initializeData: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        products: React.PropTypes.array,
        saveLineItem: React.PropTypes.func,
        saveFormHeader: React.PropTypes.func,
        downloadUrl: React.PropTypes.string,
        setDownloadUrl: React.PropTypes.func,
        notifyUser: React.PropTypes.func
    }

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );
        this.editingRowValueRefs = {
            input: {},
            select: {},
            dates: {},
            currencies: {}
        };
        this.headerEdited = false;
        this.state = {
            data: {
                type: 'govt_form_sss_lcl',
                attributes: {
                    company_id: 0,
                    year_from: 0,
                    month_from: 0,
                    year_to: 0,
                    month_to: 0,
                    employer_sss_number: '',
                    employer_name: '',
                    sss_branch_code: '',
                    repayment_no: '',
                    repayment_date: '',
                    total_amount_paid: '',
                    total_number_of_employees: '',
                    line_items: []
                }
            },
            pager: '1 - 10',
            search: '',
            uri: '',
            showModal: false,
            editingRow: null,
            downloadUrl: ''
        };
        this.filterEmployees = debounce( this.filterEmployees.bind( this ), 250, { leading: true, trailing: true });
        this.setPager = this.setPager.bind( this );
    }

    /**
     * runs before render.
     * checks if there is a valid data to generate or if user is authorized to view this page.
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.government_form'], ( authorized ) => { !authorized && browserHistory.replace( '/unauthorized' ); });

        if ( this.props.routeParams.id ) {
            this.props.initializeData({ id: this.props.routeParams.id });
        } else {
            browserHistory.replace( '/forms' );
        }
    }

    /**
     * Perform actions when component props changes
     */
    componentWillReceiveProps( nextProps ) {
        ( nextProps.data !== this.props.data ) && this.setState({ data: nextProps.data });

        nextProps.downloadUrl !== this.props.downloadUrl
            && nextProps.downloadUrl
            && this.openFile( nextProps.downloadUrl );

        nextProps.pageStatus === PAGE_STATUSES.READY
            && this.props.pageStatus === PAGE_STATUSES.SAVING
            && this.setState({ editingRow: null }, () => {
                this.editingRowValueRefs = { input: {}, select: {}, dates: {}, currencies: {}};
            });
    }

    /**
     * Component form check
     */
    componentDidUpdate( prevProps ) {
        if ( prevProps.data !== this.props.data ) {
            REQUIRED_FIELDS.INPUTS.forEach( ( key ) => {
                const input = this[ key ];
                input && input._validate( input.state.value );
            });

            REQUIRED_FIELDS.DATE_PICKERS.forEach( ( key ) => {
                const datePicker = this[ key ];
                datePicker && datePicker.checkRequired();
            });
        }
    }

    /**
     * Called before component unmounts.
     */
    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * Sets the table pager's label
     */
    setPager( tableProps ) {
        if ( tableProps.data.length ) {
            const pageSize = tableProps.data.length < tableProps.pageSize ? tableProps.data.length : tableProps.pageSize;
            let totalItems = pageSize * ( tableProps.page + 1 );
            if ( tableProps.pages === tableProps.page + 1 ) {
                totalItems = tableProps.data.length < totalItems ? tableProps.data.length : totalItems;
            }
            this.setState({ pager: `${( pageSize * tableProps.page ) + 1} - ${totalItems}` });
        } else {
            this.setState({ pager: '0' });
        }
    }

    /**
     * Get the loan type label
     */
    getLoanTypeLabel( type ) {
        const loanType = SSS_LOAN_TYPES.find( ( obj ) => obj.value === type );
        if ( !loanType ) {
            return '';
        }
        return loanType.label;
    }

    getRemarkLabel( remark ) {
        const remarkType = REMARKS.find( ( obj ) => obj.value === remark );
        return ( !remarkType || remarkType.value == null ) ? '' : remarkType.label;
    }

    /**
     * Get the table columns for render
     */
    getTableColumns() {
        const pageNotReady = this.props.pageStatus !== PAGE_STATUSES.READY;
        const saving = this.props.pageStatus === PAGE_STATUSES.SAVING;

        const columns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'SSS ID No.',
                accessor: 'sss_number',
                sortable: false,
                width: 180,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <GovernmentNumbersInput
                                type="sss"
                                id="sss_number"
                                className="editing"
                                value={ row.sss_number }
                                required
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.sss_number = ref; } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.input.sss_number.setState(
                                        { value: formatSSS( value ) }
                                    );
                                } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {row.sss_number}
                        </div>
                )
            },
            {
                header: 'Last Name',
                accessor: 'last_name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="last_name"
                                className="editing"
                                value={ row.last_name }
                                required
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.last_name = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {row.last_name}
                        </div>
                )
            },
            {
                header: 'First Name',
                accessor: 'first_name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="first_name"
                                className="editing"
                                value={ row.first_name }
                                required
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.first_name = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {row.first_name}
                        </div>
                )
            },
            {
                header: 'Middle Name',
                accessor: 'middle_name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="middle_name"
                                className="editing"
                                value={ row.middle_name }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.middle_name = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {row.middle_name}
                        </div>
                )
            },
            {
                header: 'Loan Type',
                accessor: 'sss_loan_type',
                sortable: false,
                width: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <Select
                            id="sss_loan_type"
                            className="editing"
                            value={ row.sss_loan_type }
                            data={ SSS_LOAN_TYPES }
                            ref={ ( ref ) => { this.editingRowValueRefs.select.sss_loan_type = ref; } }
                            disabled={ pageNotReady }
                            required
                            onChange={ ( value ) => {
                                if ( !value ) {
                                    this.editingRowValueRefs.select.sss_loan_type.setState({
                                        value: SSS_LOAN_TYPES[ 0 ]
                                    });
                                }
                            } }
                        />
                    )
                    : (
                        <div>
                            { this.getLoanTypeLabel( row.sss_loan_type ) }
                        </div>
                    )
                )
            },
            {
                header: 'Loan Date',
                accessor: 'loan_date',
                sortable: false,
                width: 150,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="loan-date"
                                className="editing"
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.dates.loan_date = ref; } }
                                value={ ( row.loan_date != null && row.loan_date !== '0000-00-00' ) ?
                                    moment( row.loan_date, 'YYYY-MM-DD' ).format( 'MM/DD/YYYY' ) : '' }
                                onBlur={ ( value ) => {
                                    if ( !value ) {
                                        return;
                                    }
                                    const regex = new RegExp( /^\d{2}\/\d{2}\/\d{4}$/ );
                                    const targetDate = moment( value, 'MM/DD/YYYY' );
                                    const valid = regex.test( value ) && targetDate.isValid();
                                    this.editingRowValueRefs.dates.loan_date.setState({
                                        error: !valid,
                                        errorMessage: valid ? ' ' : 'Invalid date'
                                    });
                                } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.dates.loan_date.setState(
                                        { value: formatInputDate( value ) }
                                    );
                                } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            { moment( row.loan_date, 'YYYY-MM-DD' ).format( 'MM/DD/YYYY' ) }
                        </div>
                )
            },
            {
                header: 'Loan Amount',
                accessor: 'loan_amount',
                sortable: false,
                width: 120,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="loan_amount"
                                className="editing currency"
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.currencies.loan_amount = ref; } }
                                value={ this.formatCurrency( row.loan_amount ) }
                                onBlur={ ( value ) => {
                                    this.editingRowValueRefs.currencies.loan_amount.setState(
                                        { value: this.formatCurrency( value ) }
                                    );
                                } }
                                onFocus={ () => {
                                    this.editingRowValueRefs.currencies.loan_amount.setState(
                                        { value: stripNonDigit( this.editingRowValueRefs.currencies.loan_amount.state.value ) }
                                    );
                                } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.currencies.loan_amount.setState(
                                        { value: stripNonDigit( value ) }
                                    );
                                } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {this.formatCurrency( row.loan_amount )}
                        </div>
                )
            },
            {
                header: 'Amount',
                accessor: 'payment_amount',
                sortable: false,
                width: 120,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="payment_amount"
                                className="editing currency"
                                value={ row.payment_amount }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.currencies.payment_amount = ref; } }
                                onBlur={ ( value ) => {
                                    this.editingRowValueRefs.currencies.payment_amount.setState(
                                        { value: this.formatCurrency( value ) }
                                    );
                                } }
                                onFocus={ () => {
                                    this.editingRowValueRefs.currencies.payment_amount.setState(
                                        { value: stripNonDigit( this.editingRowValueRefs.currencies.payment_amount.state.value ) }
                                    );
                                } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.currencies.payment_amount.setState(
                                        { value: stripNonDigit( value ) }
                                    );
                                } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {this.formatCurrency( row.payment_amount )}
                        </div>
                )
            },
            {
                header: 'Remarks',
                accessor: 'remarks',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <Select
                            id="remarks"
                            className="editing"
                            value={ this.formatRemarks( row.remarks ) }
                            disabled={ pageNotReady }
                            data={ REMARKS }
                            ref={ ( ref ) => { this.editingRowValueRefs.select.remarks = ref; } }
                            onChange={ ( value ) => {
                                if ( !value ) {
                                    this.editingRowValueRefs.select.remarks.setState({
                                        value: REMARKS[ 0 ]
                                    });
                                }
                            } }
                        />
                    )
                    : (
                        <div>
                            {this.getRemarkLabel( this.formatRemarks( row.remarks ) ) }
                        </div>
                    )
                )
            },
            {
                header: '',
                accessor: '',
                sortable: false,
                width: 200,
                render: ({ row }) => (
                    ( row.id !== this.state.editingRow )
                        ? <Button
                            id={ `button-edit-${row.id}` }
                            label={ <span>Edit</span> }
                            type="grey"
                            disabled={ saving }
                            onClick={ () => { this.setState({ editingRow: row.id }); } }
                        />
                        : <div>
                            <Button
                                id={ `button-cancel-${row.id}` }
                                label={ <span>Cancel</span> }
                                type="neutral"
                                disabled={ saving }
                                onClick={ () => { this.setState({ editingRow: null }); this.editingRowValueRefs = { input: {}, select: {}, dates: {}, currencies: {}}; } }
                            />
                            <Button
                                id={ `button-submit-${row.id}` }
                                label={ saving ? 'Saving' : <span>Submit</span> }
                                type="action"
                                disabled={ this.props.pageStatus === PAGE_STATUSES.SAVING }
                                onClick={ () => { this.updateLineItem(); } }
                            />
                        </div>

                )
            }
        ];
        return columns;
    }

    /**
     * gets the filtered data
     */
    getFilteredData() {
        if ( this.state.search.length ) {
            const search = this.state.search.toLowerCase();
            return this.state.data.attributes.line_items.filter( ( employee ) =>
                employee.last_name.toLowerCase().includes( search )
                    || employee.first_name.toLowerCase().includes( search )
                    || employee.sss_number.toLowerCase().includes( search )
            );
        }
        return this.state.data.attributes.line_items;
    }

    /**
     * Get pager text with non breaking spaces
     */
    getPagerText() {
        return `Showing ${this.state.pager} of ${this.formatNumber( this.state.data.attributes.line_items.length )} entries`;
    }

    /**
     * Download file to browser
     */
    openFile( url ) {
        if ( url ) {
            const linkElement = document.getElementById( 'downloadLink' );
            linkElement.target = '_download';
            linkElement.download = '';
            linkElement.href = url;
            linkElement.click();
            linkElement.href = '';
            this.props.setDownloadUrl( '' );
        }
    }

    /**
     * format the data to currency
     * @param value
     * @returns {XML}
     */
    formatCurrency( value ) {
        const convertedValue = Number( stripNonDigit( value ) );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `${Number( convertedValue ).toFixed( 2 ).replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    /**
     * format the data to currency
     * @param value
     * @returns {XML}
     */
    formatNumber( value ) {
        const convertedValue = Number( value );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `${Number( convertedValue ).toString().replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    removeEmptyValues( obj ) {
        const newObj = {};
        Object.keys( obj ).forEach( ( key ) => {
            if ( obj[ key ] !== null && obj[ key ] !== '' && obj[ key ] !== 'Invalid date' ) {
                newObj[ key ] = obj[ key ];
            }
        });
        return newObj;
    }

    /**
     * Format the remarks value to remove extra date data
     * @param {} value
     */

    formatRemarks( value ) {
        return ([ 'T', 'H', '', null ].includes( value ) ) ? value : value[ 0 ];
    }

    /**
     * check header form
     */
    checkHeaderForm() {
        for ( const [ key, required ] of Object.entries( SSSLCLForm.headerFormProperties ) ) {
            if ( `${key}` === 'repayment_date' ) {
                if ( required && !this[ `${key}` ].state.selectedDay ) {
                    return false;
                }
            } else if ( required && !this[ `${key}` ].state.value ) return false;
            if ( this[ `${key}` ].state.error ) return false;
        }
        return true;
    }
    /**
     * Downloads file to user
     */
    saveAndDownloadFile() {
        if ( !this.checkHeaderForm() ) {
            this.props.notifyUser(
                {
                    show: true,
                    title: 'Error',
                    message: 'Missing required fields.',
                    type: 'error'
                }
            );
            return;
        }
        if ( this.headerEdited ) {
            this.props.saveFormHeader(
                this.removeEmptyValues({
                    id: this.props.routeParams.id,
                    company_id: this.state.data.attributes.company_id,
                    employer_sss_number: this.employer_sss_number.state.value,
                    employer_name: this.employer_name.state.value,
                    sss_branch_code: this.sss_branch_code.state.value,
                    repayment_no: this.repayment_no.state.value,
                    repayment_date: this.repayment_date.state.value,
                    total_amount_paid: this.total_amount_paid.state.value,
                    total_number_of_employees: this.total_number_of_employees.state.value
                })
            );
            this.headerEdited = false;
        } else {
            this.props.getFileLink({ id: this.props.routeParams.id });
        }
    }

    /**
     * regenerate form
     */
    regenerateForm() {
        this.props.generateForm(
            {
                company_id: this.state.data.attributes.company_id,
                month_from: this.state.data.attributes.month_from,
                year_from: this.state.data.attributes.year_from,
                month_to: this.state.data.attributes.month_to,
                year_to: this.state.data.attributes.year_to
            }
        );
    }

    /**
     * Update line item
     */
    updateLineItem() {
        const lineItem = {};
        if ( this.editingRowValueRefs.select.sss_loan_type.state.value === undefined
            || this.editingRowValueRefs.select.sss_loan_type.state.value === null ) {
            return;
        }
        for ( const [ key, obj ] of Object.entries( this.editingRowValueRefs.input ) ) {
            if ( obj.state.error ) return;
            lineItem[ `${key}` ] = obj.state.value;
        }

        for ( const [ key, obj ] of Object.entries( this.editingRowValueRefs.select ) ) {
            if ( obj.state.error ) return;
            lineItem[ `${key}` ] = obj.state.value.value;
        }

        for ( const [ key, obj ] of Object.entries( this.editingRowValueRefs.dates ) ) {
            if ( obj.state.error ) return;
            lineItem[ `${key}` ] = moment( obj.state.value, 'MM/DD/YYYY' ).format( 'YYYY-MM-DD' );
        }

        for ( const [ key, obj ] of Object.entries( this.editingRowValueRefs.currencies ) ) {
            if ( obj.state.error ) return;
            lineItem[ `${key}` ] = stripNonDigit( obj.state.value );
        }

        lineItem.id = this.state.editingRow;
        this.props.saveLineItem({ id: this.state.data.id, company_id: this.state.data.attributes.company_id, lineItem });
    }

    /**
     * Filter employees by search criteria
     * @param {*} criteria
     */
    filterEmployees( criteria ) {
        this.setState({ search: criteria }, () => this.setPager( this.contributionTable.tableComponent.state ) );
    }

    /**
     * renders a loading screen
     */
    renderLoadingPage( generating = false ) {
        return (
            <MessageWrapperStyles>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        <H3>{ generating ? 'Generating.' : 'Loading data.' } Please wait...</H3>
                    </div>
                </div>
            </MessageWrapperStyles>
        );
    }

    /**
     * renders the form fields necessary to finish generating the SSS forms
     */
    renderForm() {
        const { data } = this.state;
        const pageNotReady = this.props.pageStatus !== PAGE_STATUSES.READY;

        return (
            <PageWrapper step={ 2 } >
                <GovtFormWrapper>
                    <div className="hdmf-header">
                        <h5>Social Security System (SSS)</h5>
                        <p>To accomplish your SSS Loan Collection List (LCL), follow the steps below:</p>
                        <div className="row msrf-directions-row">
                            <div className="col-sm-8 directions">
                                <ol>
                                    <li>Fill-out the fields required below.</li>
                                    <li>Generate the SSS LCL file and save it in a portable drive.</li>
                                </ol>
                            </div>
                            <div className="col-sm-4 gov-form-options">
                                <div className="buttonbox">
                                    <Button
                                        id="button-regenerate"
                                        label="Regenerate Form"
                                        type="neutral"
                                        size="large"
                                        disabled={ pageNotReady }
                                        onClick={ () => { this.regenerateForm(); } }
                                    />
                                    <Button
                                        id="button-save-and-download"
                                        label="Save and Download"
                                        type="action"
                                        size="large"
                                        disabled={ pageNotReady }
                                        onClick={ () => { this.saveAndDownloadFile(); } }
                                    />
                                    <A download id="downloadLink"></A>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="company-section">
                        <FormTitle><h3>SSS LOAN COLLECTION LIST (LCL)</h3></FormTitle>

                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="period_covered"
                                    label="Period Covered"
                                    value={ moment( `${data.attributes.month_from}-01-${data.attributes.year_from}`, 'MM-DD-YYYY' ).format( 'MMMM YYYY' ) }
                                    disabled
                                />
                            </div>
                            <div className="col-xs-8">
                                <Input
                                    id="employer-name"
                                    label={
                                        <span>
                                            <span>Employer/Business Name</span>
                                            <span style={ { position: 'absolute', paddingLeft: '20px' } } >
                                                <ToolTip
                                                    id="employer_name_tip"
                                                    target={ <i className="fa fa-info-circle" /> }
                                                    placement="right"
                                                >
                                                    Please ensure that the Registered Business name is entered in this field.
                                                </ToolTip>
                                            </span>
                                        </span>
                                    }
                                    value={ data.attributes.employer_name }
                                    required
                                    ref={ ( ref ) => { this.employer_name = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <GovernmentNumbersInput
                                    type="sss"
                                    id="employer_sss_number"
                                    label="Employer SSS Number"
                                    value={ data.attributes.employer_sss_number }
                                    required
                                    ref={ ( ref ) => { this.employer_sss_number = ref; } }
                                    onChange={ ( value ) => {
                                        this.employer_sss_number.setState({
                                            value: formatSSS( value )
                                        });
                                        this.headerEdited = true;
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="total_amount_paid"
                                    className="currency"
                                    label="Total Amount Paid"
                                    value={ this.formatCurrency( data.attributes.total_amount_paid ) }
                                    ref={ ( ref ) => { this.total_amount_paid = ref; } }
                                    onBlur={ ( value ) => {
                                        this.total_amount_paid.setState(
                                            { value: this.formatCurrency( value ) }
                                        );
                                    } }
                                    onFocus={ () => {
                                        this.total_amount_paid.setState(
                                            { value: stripNonDigit( this.total_amount_paid.state.value ) }
                                        );
                                    } }
                                    onChange={ ( value ) => {
                                        this.total_amount_paid.setState(
                                            { value: stripNonDigit( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="total_number_of_employees"
                                    label="Total Number of Employees"
                                    value={ data.attributes.total_number_of_employees }
                                    ref={ ( ref ) => { this.total_number_of_employees = ref; } }
                                    onBlur={ ( value ) => {
                                        this.total_number_of_employees.setState(
                                            { value: stripNonDigit( value ) }
                                        );
                                    } }
                                    onFocus={ () => {
                                        this.total_number_of_employees.setState(
                                            { value: stripNonDigit( this.total_number_of_employees.state.value ) }
                                        );
                                    } }
                                    onChange={ ( value ) => {
                                        this.total_number_of_employees.setState(
                                            { value: stripNonDigit( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="sss_branch_code"
                                    label="SSS Branch Code"
                                    value={ data.attributes.sss_branch_code }
                                    required
                                    ref={ ( ref ) => { this.sss_branch_code = ref; } }
                                    onChange={ ( value ) => {
                                        this.headerEdited = true;
                                        this.sss_branch_code.setState(
                                            { value: stripNonNumeric( value ) }
                                        );
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="repayment_no"
                                    label="Repayment No."
                                    value={ data.attributes.repayment_no }
                                    ref={ ( ref ) => { this.repayment_no = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <DatePicker
                                    label="Repayment Date"
                                    dayFormat="MM/DD/YYYY"
                                    selectedDay={ moment( data.attributes.repayment_date, 'YYYY-MM-DD' ).format( 'MM/DD/YYYY' ) }
                                    ref={ ( ref ) => { this.repayment_date = ref; } }
                                    required
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                    </div>
                    <div className="contribution-list">
                        { this.renderTableData() }
                    </div>
                </GovtFormWrapper>
            </PageWrapper>
        );
    }

    /**
     * Render table data if there are line items
     */
    renderTableData() {
        const items = this.state.data.attributes.line_items.length;
        if ( items ) {
            return (
                <div>
                    <ContributionListHeader>
                        <SearchHeader>
                            <div className="title"><H5>SSS Loan Collection List</H5></div>
                            <div>
                                <Search
                                    id="textSearch"
                                    className="col-xs-4"
                                    placeholder="Search for SSS ID, last name or first name"
                                    handleSearch={ this.filterEmployees }
                                />
                            </div>
                        </SearchHeader>
                        <span id="pager">{ this.getPagerText() }</span>
                    </ContributionListHeader>
                    <Table
                        columns={ this.getTableColumns() }
                        data={ this.getFilteredData() || [] }
                        pagination={ ( this.state.data.attributes.line_items.length || 0 ) > 10 }
                        ref={ ( ref ) => { this.contributionTable = ref; } }
                        onDataChange={ this.setPager }
                    />
                </div>
            );
        }
        return (
            <div className="NoDataFound"><span>No Data</span></div>
        );
    }

    /**
     * Renders Form component to DOM
     */
    render() {
        const { notification, pageStatus } = this.props;
        const isLoading = pageStatus === PAGE_STATUSES.LOADING;
        const isGenerating = pageStatus === PAGE_STATUSES.GENERATING;

        return (
            <div>
                <Helmet
                    title="Government Forms: SSS"
                    meta={ [
                        { name: 'description', content: 'Generate SSS Forms' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A href="/payroll/forms">
                            &#8592; Back to Government Forms
                        </A>
                    </Container>
                </NavWrapper>
                {
                    ( isLoading || isGenerating )
                        ? this.renderLoadingPage( isGenerating )
                        : this.renderForm()
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    data: makeSelectData(),
    pageStatus: makeSelectPageStatus(),
    downloadUrl: makeSelectDownloadUrl(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        hdmfFormActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( SSSLCLForm );
