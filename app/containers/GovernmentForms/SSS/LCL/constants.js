/*
 *
 * SSS LCL Form constants
 *
 */

const namespace = 'app/GovernmentForms/SSS/LCL';

export const INITIAL_DATA = `${namespace}/INITIAL_DATA`;

export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;

export const SET_FORM_OPTIONS = `${namespace}/SET_FORM_OPTIONS`;
export const SET_DATA = `${namespace}/SET_DATA`;
export const GET_FORM_URI = `${namespace}/GET_FORM_URI`;

export const GENERATE_FORMS = `${namespace}/GENERATE_FORMS`;
export const SET_URI = `${namespace}/SET_URI`;

export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const SAVE_LINE_ITEM = `${namespace}/SAVE_LINE_ITEM`;
export const SAVE_FORM_DETAILS = `${namespace}/SAVE_FORM_DETAILS`;

export const SSS_LOAN_TYPES = [
    { value: 'SALARY', label: 'Salary Loan' },
    { value: 'CALAMITY', label: 'Calamity Loan' },
    { value: 'SLERP_1', label: 'SLERP 1' },
    { value: 'SLERP_3', label: 'SLERP 3' },
    { value: 'EDUCATIONAL', label: 'Educational Loan' },
    { value: 'EMERGENCY', label: 'Emergency Loan' },
    { value: 'STOCK_INVESTMENT', label: 'Stock Investment' }
];

export const REMARKS = [
    { value: null, label: 'None' },
    { value: 'T', label: 'Terminated' },
    { value: 'H', label: 'Newly Hired' }
];

export const REQUIRED_FIELDS = {
    INPUTS: [ 'employer_sss_number', 'employer_name', 'sss_branch_code', 'total_amount_paid', 'total_number_of_employees' ],
    DATE_PICKERS: ['repayment_date']
};
