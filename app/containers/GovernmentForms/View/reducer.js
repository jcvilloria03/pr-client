import { fromJS } from 'immutable';
import moment from 'moment/moment';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    NOTIFICATION_SAGA,
    SET_GOVERNMENT_FORMS
} from './constants';

const initialState = fromJS({
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    forms: []
});

/**
 * Formats data of government forms array from API
 * @param {Array} data - Government forms list
 * @returns {Array}
 */
function prepareGovernmentForms( data ) {
    return data.map( ({ id, attributes }) => {
        const monthFrom = moment({ year: attributes.year_from, month: attributes.month_from - 1 });
        const monthTo = moment({ year: attributes.year_to, month: attributes.month_to - 1 });

        const payrollPeriod = monthFrom.isSame( monthTo )
            ? monthFrom.format( 'MMMM YYYY' )
            : monthFrom.isSame( monthTo, 'year' )
                ? `${monthFrom.format( 'MMMM' )} - ${monthTo.format( 'MMMM YYYY' )}`
                : `${monthFrom.format( 'MMMM YYYY' )} - ${monthTo.format( 'MMMM YYYY' )}`;

        return {
            id,
            agency: attributes.agency_name,
            kind: attributes.kind,
            form_name: attributes.form_name,
            payroll_period: payrollPeriod
        };
    });
}

/**
 *
 * Government Forms view reducer
 *
 */
function governmentFormsViewReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_GOVERNMENT_FORMS:
            return state.set( 'forms', fromJS( prepareGovernmentForms( action.payload ) ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default governmentFormsViewReducer;
