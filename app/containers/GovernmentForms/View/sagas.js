import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    GET_GOVERNMENT_FORMS
} from './constants';
import {
    setLoading,
    setNotification,
    setGovernmentForms
} from './actions';

/**
 * Sends request to fetch list of generated government forms
 * @returns {Object}
 */
export function* getGovernmentForms() {
    try {
        yield put( setLoading( true ) );

        const companyId = company.getLastActiveCompanyId();
        const { data } = yield call( Fetch, '/government_forms', {
            method: 'GET',
            params: {
                'filter[company_id]': companyId
            }
        });

        yield put( setGovernmentForms( data ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getGovernmentForms );
}

/**
 * Watcher for GET_GOVERNMENT_FORMS
 */
export function* watchForGetGovernmentForms() {
    const watcher = yield takeEvery( GET_GOVERNMENT_FORMS, getGovernmentForms );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetGovernmentForms,
    watchForReinitializePage
];
