import { RESET_STORE } from 'containers/App/constants';

import {
    GET_GOVERNMENT_FORMS,
    SET_GOVERNMENT_FORMS,
    SET_LOADING,
    NOTIFICATION_SAGA
} from './constants';

/**
 * Sends request to fetch government forms
 * @returns {Object}
 */
export function getGovernmentForms() {
    return {
        type: GET_GOVERNMENT_FORMS
    };
}

/**
 * Sets government forms
 * @param {Array} payload - Government forms list
 * @returns {Object}
 */
export function setGovernmentForms( payload ) {
    return {
        type: SET_GOVERNMENT_FORMS,
        payload
    };
}

/**
 * Sets page loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object}
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets notification
 * @param {Object} payload - Notification config
 * @returns {Object}
 */
export function setNotification( payload ) {
    return {
        type: NOTIFICATION_SAGA,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
