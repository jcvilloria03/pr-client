import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import toLower from 'lodash/toLower';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';
import { PAYROLL_SUBHEADER_ITEMS } from 'utils/constants';
import { formatPaginationLabel } from 'utils/functions';

import SnackBar from 'components/SnackBar';
import { H2, H3, H5 } from 'components/Typography';
import Button from 'components/Button';
import Input from 'components/Input';
import Icon from 'components/Icon';
import Table from 'components/Table';

import SubHeader from 'containers/SubHeader';

import { Wrapper, LoadingStyles } from './styles';

import * as viewActions from './actions';

import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectGovernmentForms
} from './selectors';
import { BIR_FORMS } from './constants';

/**
 * GovernmentFormsView component
 */
export class GovernmentFormsView extends React.PureComponent {
    static propTypes = {
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        loading: React.PropTypes.bool,
        getGovernmentForms: React.PropTypes.func,
        governmentForms: React.PropTypes.array
    }

    /**
     * Component's constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayed_data: []
        };
    }

    /**
     * Checks permission before rendering component
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['view.government_form'], ( authorized ) => {
            if ( authorized ) {
                this.props.getGovernmentForms();
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.governmentForms !== this.props.governmentForms ) {
            this.handleSearch( nextProps.governmentForms );
        }
    }

    getFormUrl = ( row ) => {
        let kind = row.kind.replace( '_', '/' );
        if ( BIR_FORMS.includes( kind ) ) {
            kind = `bir/${kind}`;
        }
        return toLower( `/forms/${kind}/${row.id}` );
    }

    generateTableColumnsConfig() {
        return [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Government Agency',
                accessor: 'agency'
            },
            {
                header: 'Government Form',
                accessor: 'form_name'
            },
            {
                header: 'Payroll Period',
                accessor: 'payroll_period'
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    <Button
                        id={ `button-view-govt-form-${row.id}` }
                        label="View"
                        type="grey"
                        to={ this.getFormUrl( row ) }
                    />
                )
            }
        ];
    }

    handleTableChanges = () => {
        if ( !this.governmentFormsTable ) {
            return;
        }

        this.setState({ label: formatPaginationLabel( this.governmentFormsTable.tableComponent.state ) });
    };

    /**
     * Handles filters and search inputs
     */
    handleSearch = () => {
        let searchQuery = null;
        let dataToDisplay = this.props.governmentForms;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery ) {
            dataToDisplay = dataToDisplay.filter( ( governmentForm ) => (
                [ 'agency', 'kind', 'payroll_period' ].some( ( key ) => toLower( governmentForm[ key ]).indexOf( searchQuery ) >= 0 )
            ) );
        }

        this.setState({ displayed_data: dataToDisplay }, () => {
            this.handleTableChanges();
        });
    };

    /**
     * renders component to DOM
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Government Forms"
                    meta={ [
                        { name: 'description', content: 'Description of Government Forms' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                <Wrapper>
                    <Container>
                        { this.props.loading ? (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading Data</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        ) : (
                            <div className="content">
                                <div className="heading">
                                    <H3>Government Forms</H3>
                                    <p>Generate government forms to submit records and payment to the following government offices: BIR, Pag-IBIG, PhilHealth, and SSS</p>
                                    <Button
                                        label="Generate Government Form"
                                        size="large"
                                        type="action"
                                        to="/forms/generate"
                                    />
                                </div>
                                <div className="title">
                                    <H5>Government Forms List</H5>
                                    <div className="search-wrapper">
                                        <Input
                                            className="search"
                                            id="search"
                                            ref={ ( ref ) => { this.searchInput = ref; } }
                                            onChange={ this.handleSearch }
                                            addon={ {
                                                content: <Icon name="search" />,
                                                placement: 'right'
                                            } }
                                        />
                                    </div>
                                    <span>{ this.state.label }</span>
                                </div>
                                <Table
                                    data={ this.state.displayed_data }
                                    columns={ this.generateTableColumnsConfig() }
                                    pagination
                                    onDataChange={ this.handleTableChanges }
                                    ref={ ( ref ) => { this.governmentFormsTable = ref; } }
                                />
                            </div>
                        ) }
                    </Container>
                </Wrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    governmentForms: makeSelectGovernmentForms()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        viewActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( GovernmentFormsView );
