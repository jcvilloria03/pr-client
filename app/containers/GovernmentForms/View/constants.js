/*
 *
 * Government Forms constants
 *
 */
const namespace = 'app/containers/GovernmentForms/View';

export const SET_LOADING = `${namespace}/SET_LOADING`;

export const GET_GOVERNMENT_FORMS = `${namespace}/GET_GOVERNMENT_FORMS`;

export const SET_GOVERNMENT_FORMS = `${namespace}/SET_GOVERNMENT_FORMS`;

export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const BIR_FORMS = [ '1601C', '1601E', '1601F', '1604C' ];
