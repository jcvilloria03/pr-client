import { createSelector } from 'reselect';

/**
 * Direct selector to the government forms state domain
 */
const selectFormsDomain = () => ( state ) => state.get( 'government_forms' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
    selectFormsDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectFormsDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectGovernmentForms = () => createSelector(
    selectFormsDomain(),
    ( substate ) => substate.get( 'forms' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectGovernmentForms
};
