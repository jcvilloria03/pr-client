import styled from 'styled-components';

export const FormsWrapper = styled.div`
    padding-bottom: 50px;

    > div {
        width: 90%;
        margin: 20px auto 150px auto;

        div {
            &.heading {
                margin-bottom: 30px;

                h3 {
                    font-weight: 600;
                    margin-bottom: 50px;
                }

                span {
                    display: block;
                }

            }

            &.listing {
                background: #fff;
                box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
                padding: 40px 30px 30px;

                > div.heading {
                    display: flex;
                    justify-content: space-between;
                    margin-bottom: 30px;

                    .text-search {
                        width: 450px;
                        > p {
                            display: none;
                        }
                    }
                }

                .title {
                    display: flex;
                    align-items: center;
                    margin-bottom: 20px;

                    h5 {
                        margin: 0;
                        margin-right: 20px;
                        font-weight: 600;
                        font-size: 18px;
                    }

                    .search-wrapper {
                        flex-grow: 1;
                        
                        .search {
                            width: 300px;
                            border: 1px solid #333;
                            border-radius: 30px;

                            input {
                                border: none;
                            }
                        }

                        p {
                            display: none;
                        }

                        .input-group,
                        .form-control {
                            background-color: transparent;
                        }

                        .input-group-addon {
                            background-color: transparent;
                            border: none;
                        }

                        .isvg {
                            display: inline-block;
                            width: 1rem;
                        }
                    }

                    & > span {
                        margin-right: 10px;
                    }
                }

                .ReactTable {
                    .rt-noData {
                        background-color: rgba(0,0,0,0.9);
                        color: #fff;
                    }

                    .rt-td {
                        overflow: unset;
                    }

                    .Select {
                        margin-top: 12px;
                        font-size: 16px;
                        font-weight: 400;

                        .Select-control {
                            .Select-input {
                                height: 45px;
                            }
                            
                            .Select-value {
                                line-height: 45px;
                            }
                        }
                    }
                }
            }

            &.actions {
                margin: 20px 0;
                justify-content: flex-start;
                display: flex;
            }
        }
    }
`;

export const MessageWrapperStyles = styled.div`
    display: flex;
    justify-content: center;
    height: 100vh;

    > div {
        display: flex;
        flex-direction: column;
        justify-content: center;

        i {
            font-size: 7em;
            align-self: center;
        }

        .description {
            padding-top: 40px;
            text-align: center;

            h3 {
                font-weight: 600;
            }
        }
    }
`;

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

export const InputWrapper = styled.div`
    padding-bottom: 10px;
`;
