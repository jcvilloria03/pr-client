import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';
import {
    SET_PAGE_STATUS,
    SET_EMPLOYEES_DATA,
    NOTIFICATION_SAGA,
    SET_FORM_DATA,
    SET_DOWNLOAD_URL
} from './constants';
import { PAGE_STATUSES } from '../constants';

const initialState = fromJS({
    form: {},
    employees: [],
    page_status: PAGE_STATUSES.LOADING,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    download_url: ''
});

/**
 * Processes data to filter out invalid dates
 * @param {Array} data
 * @returns {Array}
 */
function processDates( data ) {
    return data.map( ( lineItem ) => ({
        ...lineItem,
        status_effective_date: lineItem.status_effective_date === '0000-00-00' ? '' : lineItem.status_effective_date
    }) );
}

/**
 * PhilHealthFormsReducer
 */
function PhilHealthFormsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_FORM_DATA:
            return state.set( 'form', fromJS( action.payload ) );
        case SET_EMPLOYEES_DATA:
            return state.set( 'employees', fromJS( processDates( action.payload ) ) );
        case SET_PAGE_STATUS:
            return state.set( 'page_status', action.payload );
        case SET_DOWNLOAD_URL:
            return state.set( 'download_url', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default PhilHealthFormsReducer;

