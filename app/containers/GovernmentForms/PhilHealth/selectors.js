import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'PhilHealthForms' );

/**
 * Other specific selectors
 */
const makeSelectEmployees = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'employees' ).toJS()
);

const makeSelectPageStatus = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'page_status' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectFormData = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'form' ).toJS()
);

const makeSelectDownloadUrl = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'download_url' )
);

export {
    makeSelectEmployees,
    makeSelectPageStatus,
    makeSelectNotification,
    makeSelectFormData,
    makeSelectDownloadUrl
};
