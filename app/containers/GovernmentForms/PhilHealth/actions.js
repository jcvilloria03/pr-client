import { RESET_STORE } from 'containers/App/constants';

import {
    INITIAL_DATA,
    SAVE_EDITED_ENTRY,
    SET_FORM_DATA,
    REGENERATE_FORM,
    DOWNLOAD_FORM,
    NOTIFICATION_SAGA,
    SET_PAGE_STATUS,
    SET_EMPLOYEES_DATA,
    SET_DOWNLOAD_URL
} from './constants';

/**
 * Initialize data for payslip page
 */
export function initializeData( payload ) {
    return {
        type: INITIAL_DATA,
        payload
    };
}

/**
 * Sets government form data response from API
 * @param {Object} payload - PhilHealth form data
 * @returns {Object}
 */
export function setFormData( payload ) {
    return {
        type: SET_FORM_DATA,
        payload
    };
}

/**
 * Sends request to save edited government form entry
 * @param {Integer} payload.company_id - Company ID
 * @param {String} payload.kind - Kind of government form
 * @param {Object} payload.attributes - Edited information
 * @returns {Object} action
 */
export function saveEditedEntry( payload ) {
    return {
        type: SAVE_EDITED_ENTRY,
        payload
    };
}

/**
 * Sends request to regenerate form
 * @param {String} payload.kind - Kind of government form
 * @param {String} payload.company_id - Company ID
 * @param {String} payload.month_from
 * @param {String} payload.year_from
 * @param {String} payload.month_to
 * @param {String} payload.year_to
 * @returns {Object} action
 */
export function regenerateForm( payload ) {
    return {
        type: REGENERATE_FORM,
        payload
    };
}

/**
 * Sends request to download form
 * @param {Integer} id - Government form ID
 * @returns {Object} action
 */
export function downloadForm( id ) {
    return {
        type: DOWNLOAD_FORM,
        payload: id
    };
}

/**
 * Sets download URL from API response
 * @param {String} payload - Download URL
 * @returns {Object} action
 */
export function setDownloadUrl( payload ) {
    return {
        type: SET_DOWNLOAD_URL,
        payload
    };
}

/**
 * Sets status of page
 * @param {String} payload - Page status
 * @returns {Object} action
 */
export function setPageStatus( payload ) {
    return {
        type: SET_PAGE_STATUS,
        payload
    };
}

/**
 * Sets employees data
 * @param {Object} payload - Employees data
 * @returns {Object} action
 */
export function setEmployeesData( payload ) {
    return {
        type: SET_EMPLOYEES_DATA,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: NOTIFICATION_SAGA,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
