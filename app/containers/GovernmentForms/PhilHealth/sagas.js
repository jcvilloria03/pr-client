import { take, call, put, cancel, select } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';

import { Fetch } from 'utils/request';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    INITIAL_DATA,
    NOTIFICATION,
    SAVE_EDITED_ENTRY,
    REGENERATE_FORM,
    DOWNLOAD_FORM
} from './constants';
import {
    setFormData,
    setNotification,
    setPageStatus,
    setEmployeesData,
    setDownloadUrl
} from './actions';
import { makeSelectFormData } from './selectors';
import { PAGE_STATUSES } from '../constants';

/**
 * Initialize data for Government Forms:Philhealth page
 */
export function* initializeData({ payload }) {
    try {
        yield [
            put( setPageStatus( PAGE_STATUSES.LOADING ) ),
            call( getData, payload )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Fetch data from API
 *
 * @param {Number} payload - Government Form ID
 */
export function* getData( payload ) {
    try {
        const { data } = yield call( Fetch, `/government_forms/${payload}`, { method: 'GET' });
        const { id, type, attributes } = data;
        const { line_items: employees, ...rest } = attributes;

        yield [
            put( setFormData({
                id,
                type,
                ...rest
            }) ),
            put( setEmployeesData( employees ) )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Sends request to save edited government form entry
 * @param {Integer} payload.company_id - Company ID
 * @param {String} payload.kind - Kind of government form
 * @param {Object} payload.attributes - Edited information
 */
export function* saveEditedEntry({ payload }) {
    yield put( setPageStatus( PAGE_STATUSES.SAVING ) );

    try {
        const { id } = yield select( makeSelectFormData() );
        yield call( Fetch, `/government_forms/${id}`, {
            method: 'PUT',
            data: {
                data: payload
            }
        });

        yield [
            call( notifyUser, {
                title: 'Success',
                message: 'Employee entry successfully updated',
                show: true,
                type: 'success'
            }),
            call( getData, id )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Sends request to regenerate form
 * @param {String} payload.kind - Kind of government form
 * @param {String} payload.company_id - Company ID
 * @param {String} payload.month_from
 * @param {String} payload.year_from
 * @param {String} payload.month_to
 * @param {String} payload.year_to
 */
export function* regenerateForm({ payload }) {
    try {
        yield put( setPageStatus( PAGE_STATUSES.GENERATING ) );

        const { data } = yield call( Fetch, '/government_forms', {
            method: 'POST',
            data: payload
        });

        const { id, type, attributes } = data;
        const { line_items: employees, ...rest } = attributes;

        yield [
            put( setFormData({
                id,
                type,
                ...rest
            }) ),
            put( setEmployeesData( employees ) ),
            call( notifyUser, {
                title: 'Success',
                message: 'Form successfully regenerated',
                show: true,
                type: 'success'
            })
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Sends request to download form
 * @param {Integer} id - Government form ID
 */
export function* downloadForm({ payload: id }) {
    try {
        const { uri } = yield call( Fetch, `/download/govt_form/${id}`, { method: 'GET' });
        yield put( setDownloadUrl( uri ) );
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watcher for INITIAL_DATA
 *
 */
export function* watchForInitialData() {
    const watcher = yield takeEvery( INITIAL_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SAVE_EDITED_ENTRY
 */
export function* watchForSaveEditedEntry() {
    const watcher = yield takeEvery( SAVE_EDITED_ENTRY, saveEditedEntry );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REGENERATE_FORM
 */
export function* watchForRegenerateForm() {
    const watcher = yield takeEvery( REGENERATE_FORM, regenerateForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SAVE_EDITED_ENTRY
 */
export function* watchForDownloadForm() {
    const watcher = yield takeEvery( DOWNLOAD_FORM, downloadForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitialData,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForSaveEditedEntry,
    watchForRegenerateForm,
    watchForDownloadForm
];
