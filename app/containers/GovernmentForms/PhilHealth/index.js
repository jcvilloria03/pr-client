import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';
import { Container } from 'reactstrap';
import toLower from 'lodash/toLower';
import startCase from 'lodash/startCase';

import SnackBar from 'components/SnackBar';
import { H3, H5 } from 'components/Typography';
import Table from 'components/Table';
import Input from 'components/Input';
import Button from 'components/Button';
import A from 'components/A';
import Icon from 'components/Icon';
import Select from 'components/Select';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';
import { formatPaginationLabel, formatDate, stripNonDigit } from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import PageWrapper from '../PageWrapper';
import * as philHealthActions from './actions';
import {
    makeSelectEmployees,
    makeSelectPageStatus,
    makeSelectNotification,
    makeSelectFormData,
    makeSelectDownloadUrl
} from './selectors';

import {
    FormsWrapper,
    MessageWrapperStyles,
    NavWrapper,
    InputWrapper
} from './styles';
import { CONTRIBUTION_STATUSES } from './constants';
import { GOVERNMENT_FORM_KINDS } from '../Generate/constants';
import { PAGE_STATUSES } from '../constants';

/**
 * Philhealth Form component
 */
export class PhilHealthForms extends React.PureComponent {
    static propTypes = {
        initializeData: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        employees: React.PropTypes.arrayOf(
            React.PropTypes.object
        ),
        pageStatus: React.PropTypes.string,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        routeParams: React.PropTypes.object,
        form: React.PropTypes.object,
        saveEditedEntry: React.PropTypes.func,
        regenerateForm: React.PropTypes.func,
        downloadForm: React.PropTypes.func,
        downloadUrl: React.PropTypes.string
    }

    /**
     * component's contructor
     * @param {*} props
     */
    constructor( props ) {
        super( props );

        this.state = {
            displayed_data: [],
            label: 'Showing 0-0 of 0 entries',
            editing_row: null,
            is_effective_date_required: false
        };
    }

    /**
     * runs before render.
     * checks if there is a valid data to generate or if user is authorized to view this page.
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.government_form'], ( authorized ) => {
            if ( authorized ) {
                this.props.initializeData( this.props.routeParams.id );
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.employees !== this.props.employees
            && this.handleSearch( nextProps.employees );

        nextProps.downloadUrl !== this.props.downloadUrl
            && nextProps.downloadUrl
            && this.downloadFile( nextProps.downloadUrl );

        nextProps.pageStatus === PAGE_STATUSES.READY
            && this.props.pageStatus === PAGE_STATUSES.SAVING
            && this.setState({ editing_row: null });
    }

    /**
     * Called before component unmounts.
     */
    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * return list of table columns
     */
    getColumns() {
        const pageNotReady = this.props.pageStatus !== PAGE_STATUSES.READY;
        const saving = this.props.pageStatus === PAGE_STATUSES.SAVING;

        return [
            {
                header: 'PIN',
                accessor: 'philhealth_number',
                sortable: true,
                width: 190,
                hideFilter: true,
                render: ({ row }) => (
                    row.id === this.state.editing_row ?
                        <InputWrapper>
                            <Input
                                id="input-philhealth-number"
                                value={ row.philhealth_number }
                                required
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editPhilHealthNumber = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            { row.philhealth_number }
                        </div>
                )
            },
            {
                header: 'Last Name',
                accessor: 'last_name',
                sortable: true,
                minWidth: 220,
                hideFilter: true,
                render: ({ row }) => (
                    row.id === this.state.editing_row ?
                        <InputWrapper>
                            <Input
                                id="input-last-name"
                                value={ row.last_name }
                                required
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editLastName = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            { row.last_name }
                        </div>
                )
            },
            {
                header: 'First Name',
                accessor: 'first_name',
                sortable: true,
                minWidth: 220,
                hideFilter: true,
                render: ({ row }) => (
                    row.id === this.state.editing_row ?
                        <InputWrapper>
                            <Input
                                id="input-first-name"
                                value={ row.first_name }
                                required
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editFirstName = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            { row.first_name }
                        </div>
                )
            },
            {
                header: 'Middle Name',
                accessor: 'middle_name',
                sortable: true,
                minWidth: 220,
                hideFilter: true,
                render: ({ row }) => (
                    row.id === this.state.editing_row ?
                        <InputWrapper>
                            <Input
                                id="input-middle-name"
                                value={ row.middle_name }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editMiddleName = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            { row.middle_name }
                        </div>
                )
            },
            {
                header: 'Status',
                accessor: 'status',
                sortable: true,
                hideFilter: true,
                minWidth: 190,
                render: ({ row }) => (
                    row.id === this.state.editing_row
                        ? <Select
                            id="select-status"
                            value={ row.status }
                            data={ CONTRIBUTION_STATUSES.OPTIONS }
                            placeholder="Select status"
                            required
                            disabled={ pageNotReady }
                            onChange={ ({ value }) => {
                                const isEffectiveDateRequired = value === 'NEW_HIRE' || value === 'SEPARATED';

                                this.setState({ is_effective_date_required: isEffectiveDateRequired });
                                this.editEffectivityDate._validate( this.editEffectivityDate.state.value );
                            } }
                            ref={ ( ref ) => { this.editStatus = ref; } }
                        />
                        : startCase( toLower( row.status ) )
                )
            },
            {
                header: 'Effectivity Date of Status',
                accessor: 'status_effective_date',
                sortable: true,
                hideFilter: true,
                minWidth: 230,
                render: ({ row }) => (
                    row.id === this.state.editing_row ?
                        <InputWrapper>
                            <Input
                                id="input-status-effectivity-date"
                                placeholder="MM/DD/YYYY"
                                value={ formatDate( row.status_effective_date, 'MM/DD/YYYY' ) || '' }
                                disabled={ pageNotReady }
                                onBlur={ ( value ) => {
                                    const regex = new RegExp( /^\d{2}\/\d{2}\/\d{4}$/ );
                                    const valid = regex.test( value ) && moment( value, 'MM/DD/YYYY' ).isValid();

                                    let errorObject;
                                    if ( !value ) {
                                        errorObject = {
                                            error: this.state.is_effective_date_required,
                                            errorMessage: this.state.is_effective_date_required ? 'This field is required' : ' '
                                        };
                                    } else {
                                        errorObject = {
                                            error: !valid,
                                            errorMessage: valid ? ' ' : 'Invalid date'
                                        };
                                    }

                                    this.editEffectivityDate.setState( errorObject );
                                } }
                                required={ this.state.is_effective_date_required }
                                ref={ ( ref ) => { this.editEffectivityDate = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            { formatDate( row.status_effective_date, 'MM/DD/YYYY' ) }
                        </div>
                )
            },
            {
                header: 'Date of Birth',
                accessor: 'date_of_birth',
                sortable: true,
                width: 180,
                hideFilter: true,
                render: ( row ) => ( row.value ? moment( row.value ).format( 'MMMM DD[,] YYYY' ) : '' )
            },
            {
                header: 'Monthly Salary',
                accessor: 'monthly_salary',
                sortable: true,
                width: 220,
                hideFilter: true,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editing_row ?
                        <InputWrapper>
                            <Input
                                id="input-monthly-salary"
                                value={ row.monthly_salary }
                                required
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editMonthlySalary = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {this.formatNumber( row.monthly_salary )}
                        </div>
                )
            },
            {
                header: 'PHIC EE',
                accessor: 'philhealth_ee_share',
                sortable: true,
                width: 220,
                hideFilter: true,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editing_row ?
                        <InputWrapper>
                            <Input
                                id="input-philhealth-ee-share"
                                value={ row.philhealth_ee_share }
                                required
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editPhilHealthEeShare = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {this.formatNumber( row.philhealth_ee_share )}
                        </div>
                )
            },
            {
                header: 'PHIC ER',
                accessor: 'philhealth_er_share',
                sortable: true,
                width: 220,
                hideFilter: true,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editing_row ?
                        <InputWrapper>
                            <Input
                                id="input-philhealth-er-share"
                                value={ row.philhealth_er_share }
                                required
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editPhilHealthErShare = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {this.formatNumber( row.philhealth_er_share )}
                        </div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 200,
                sortable: false,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editing_row ? (
                        <div>
                            <Button
                                id={ `button-cancel-row-${row.id}` }
                                label="Cancel"
                                type="neutral"
                                disabled={ saving }
                                onClick={ () => { this.setState({ editing_row: null }); } }
                            />
                            <Button
                                id={ `button-save-row-${row.id}` }
                                label="Save"
                                type="action"
                                disabled={ saving }
                                onClick={ () => this.saveEditingRow( row ) }
                            />
                        </div>
                    ) : (
                        <Button
                            id={ `button-edit-row-${row.id}` }
                            label="Edit"
                            type="grey"
                            disabled={ saving }
                            onClick={ () => this.setState({
                                editing_row: row.id,
                                is_effective_date_required: row.status === 'NEW_HIRE' || row.status === 'SEPARATED'
                            }) }
                        />
                    )
                )
            }
        ];
    }

    /**
     * Validate form entries
     */
    validateForm() {
        const inputRefs = [
            'editPhilHealthNumber',
            'editLastName',
            'editFirstName',
            'editMiddleName',
            'editEffectivityDate',
            'editMonthlySalary',
            'editPhilHealthEeShare',
            'editPhilHealthErShare'
        ];

        let valid = true;
        inputRefs.forEach( ( input ) => {
            if ( this[ input ]._validate( this[ input ].state.value ) ) {
                valid = false;
            }
        });

        const statusValue = this.editStatus.state.value;
        if ( !this.editStatus._checkRequire( typeof statusValue === 'object' && statusValue !== null ? statusValue.value : statusValue ) ) {
            valid = false;
        }

        return valid;
    }

    /**
     * Save changes in the row selected
     */
    saveEditingRow = ( row ) => {
        if ( this.validateForm() ) {
            const statusEffectiveDate = this.editEffectivityDate.state.value
                ? moment( this.editEffectivityDate.state.value, 'MM/DD/YYYY' ).format( DATE_FORMATS.API )
                : '';

            const payload = {
                kind: this.props.form.type,
                company_id: this.props.form.company_id,
                attributes: {
                    line_items: [{
                        philhealth_number: this.editPhilHealthNumber.state.value,
                        last_name: this.editLastName.state.value,
                        first_name: this.editFirstName.state.value,
                        middle_name: this.editMiddleName.state.value,
                        status: this.editStatus.state.value.value,
                        status_effective_date: statusEffectiveDate,
                        monthly_salary: this.editMonthlySalary.state.value,
                        philhealth_ee_share: this.editPhilHealthEeShare.state.value,
                        philhealth_er_share: this.editPhilHealthErShare.state.value,
                        date_of_birth: row.date_of_birth,
                        employee_id: row.employee_id,
                        id: row.id
                    }]
                }
            };

            this.props.saveEditedEntry( payload );
        }
    }

    /**
     * Regenerate government form
     */
    regenerateForm = () => {
        const data = {
            kind: GOVERNMENT_FORM_KINDS.PHILHEALTH_MCL,
            company_id: this.props.form.company_id,
            month_from: this.props.form.month_from,
            year_from: this.props.form.year_from,
            month_to: this.props.form.month_to,
            year_to: this.props.form.year_to
        };

        this.props.regenerateForm({ data });
    }

    /**
     * Download form
     */
    downloadForm = () => {
        this.props.downloadForm( this.props.form.id );
    }

    /**
     * Download file to browser
     */
    downloadFile( url ) {
        const linkElement = document.getElementById( 'downloadLink' );
        linkElement.href = url;
        linkElement.click();
        linkElement.href = '';
    }

    handleTableChanges = () => {
        if ( !this.contributionTable ) {
            return;
        }

        this.setState({ label: formatPaginationLabel( this.contributionTable.tableComponent.state ) });
    };

    /**
     * Handles filters and search inputs
     */
    handleSearch = () => {
        let searchQuery = null;
        let dataToDisplay = this.props.employees;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery ) {
            const searchKeys = [
                'philhealth_number',
                'last_name',
                'first_name',
                'middle_name',
                'date_of_birth',
                'status',
                'status_effective_date',
                'monthly_salary',
                'philhealth_ee_share',
                'philhealth_er_share'
            ];

            dataToDisplay = dataToDisplay.filter( ( governmentForm ) => (
                searchKeys.some( ( key ) => toLower( governmentForm[ key ]).indexOf( searchQuery ) >= 0 )
            ) );
        }

        this.setState({ displayed_data: dataToDisplay }, () => {
            this.handleTableChanges();
        });
    };

    /**
     * format the data to currency
     * @param value
     * @returns {XML}
     */
    formatNumber( value ) {
        const convertedValue = Number( stripNonDigit( value ) );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `${Number( convertedValue ).toString().replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    /**
     * renders a loading screen
     */
    renderLoadingScreen( generating = false ) {
        return (
            <MessageWrapperStyles>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        <H3>{ generating ? 'Generating.' : 'Loading data.' } Please wait...</H3>
                    </div>
                </div>
            </MessageWrapperStyles>
        );
    }

    /**
     * Renders component to DOM
     */
    render() {
        const {
            notification,
            pageStatus,
            employees
        } = this.props;

        const isLoading = pageStatus === PAGE_STATUSES.LOADING;
        const isGenerating = pageStatus === PAGE_STATUSES.GENERATING;

        return (
            <div>
                <NavWrapper>
                    <Container>
                        <A href="/payroll/forms">
                            &#8592; Back to Government Forms
                        </A>
                    </Container>
                </NavWrapper>
                <Helmet
                    title="Government Forms: Philhealth"
                    meta={ [
                        { name: 'description', content: 'Generate Philhealth Forms' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                {
                    ( isLoading || isGenerating )
                        ? this.renderLoadingScreen( isGenerating )
                        : <PageWrapper step={ 2 }>
                            <FormsWrapper>
                                <div>
                                    <div className="heading">
                                        <H3>PhilHealth Monthly Contribution List</H3>
                                        <p>To accomplish your PhilHealth monthly contribution form, follow the steps below:</p>
                                        <span>1. Visit <A href="https://www.philhealth.gov.ph/" target="_blank">https://www.philhealth.gov.ph/</A> and log in to your account.</span>
                                        <span>2. Use the data show on the table below as your reference when filling out the form on the PhilHealth portal.</span>
                                    </div>
                                    <div className="listing">
                                        <div className="title">
                                            <H5>Search Employee</H5>
                                            <div className="search-wrapper">
                                                <Input
                                                    id="textSearch"
                                                    className="search"
                                                    placeholder="Search for PIN, employee number, last name or first name"
                                                    onChange={ this.handleSearch }
                                                    ref={ ( ref ) => { this.searchInput = ref; } }
                                                    addon={ {
                                                        content: <Icon name="search" />,
                                                        placement: 'right'
                                                    } }
                                                />
                                            </div>
                                            <span>{ this.state.label }</span>
                                            <Button
                                                id="button-regenerate-form"
                                                label="Regenerate Form"
                                                type="action"
                                                size="large"
                                                alt
                                                disabled={ pageStatus !== PAGE_STATUSES.READY }
                                                onClick={ this.regenerateForm }
                                            />
                                            <Button
                                                id="button-download-form"
                                                label="Download Form"
                                                type="action"
                                                size="large"
                                                disabled={ pageStatus !== PAGE_STATUSES.READY }
                                                onClick={ this.downloadForm }
                                            />
                                            <A download id="downloadLink"></A>
                                        </div>
                                        <Table
                                            data={ this.state.displayed_data }
                                            columns={ this.getColumns() }
                                            pagination={ employees.length > 10 }
                                            className="table"
                                            ref={ ( ref ) => { this.contributionTable = ref; } }
                                        />
                                    </div>
                                </div>
                            </FormsWrapper>
                        </PageWrapper>
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    employees: makeSelectEmployees(),
    pageStatus: makeSelectPageStatus(),
    notification: makeSelectNotification(),
    form: makeSelectFormData(),
    downloadUrl: makeSelectDownloadUrl()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        philHealthActions,
        dispatch
    );
}
export default connect( mapStateToProps, mapDispatchToProps )( PhilHealthForms );
