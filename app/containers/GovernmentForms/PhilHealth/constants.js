/*
 *
 * Philhealth Forms constants
 *
 */

const namespace = 'app/GovernmentForms/PhilHealth';
export const INITIAL_DATA = `${namespace}/INITIAL_DATA`;

export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;
export const SET_FORM_DATA = `${namespace}/SET_FORM_DATA`;
export const SET_EMPLOYEES_DATA = `${namespace}/SET_EMPLOYEES_DATA`;

export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const SAVE_EDITED_ENTRY = `${namespace}/SAVE_EDITED_ENTRY`;
export const REGENERATE_FORM = `${namespace}/REGENERATE_FORM`;
export const DOWNLOAD_FORM = `${namespace}/DOWNLOAD_FORM`;
export const SET_DOWNLOAD_URL = `${namespace}/SET_DOWNLOAD_URL`;

export const CONTRIBUTION_STATUSES = {
    MAPPING: {
        NEW_HIRE: 'New Hire',
        ACTIVE: 'Active',
        SEPARATED: 'Separated',
        NO_EARNINGS: 'No Earnings'
    },
    OPTIONS: [
        {
            label: 'New Hire',
            value: 'NEW_HIRE'
        },
        {
            label: 'Active',
            value: 'ACTIVE'
        },
        {
            label: 'Separated',
            value: 'SEPARATED'
        },
        {
            label: 'No Earnings',
            value: 'NO_EARNINGS'
        }
    ]
};
