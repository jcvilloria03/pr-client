import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';
import debounce from 'lodash/debounce';

import GovernmentNumbersInput from 'components/GovernmentNumbersInput';
import SnackBar from 'components/SnackBar';
import Input from 'components/Input';
import { H3, H5 } from 'components/Typography';
import Table from 'components/Table';
import Button from 'components/Button';
import A from 'components/A';
import ToolTip from 'components/ToolTip';
import DatePicker from 'components/DatePicker';
import Search from 'components/Search';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';
import { stripNonDigit, formatHDMF } from 'utils/functions';

import { NavWrapper } from 'containers/GovernmentForms/Generate/styles';

import * as hdmfFormActions from './actions';
import PageWrapper from '../../PageWrapper';
import { PAGE_STATUSES } from '../../constants';
import {
    makeSelectData,
    makeSelectEditing,
    makeSelectPageStatus,
    makeSelectDownloadUrl,
    makeSelectNotification
} from './selectors';

import {
    GovtFormWrapper,
    MessageWrapperStyles,
    ContributionListHeader,
    SearchHeader,
    FormTitle,
    AllCaps
} from './styles';
import { REQUIRED_FIELDS } from './constants';

/**
 * MSRF component
 */
export class HDMFForms extends React.PureComponent {
    static headerFormProperties = {
        formDate: false,
        employer_address: true,
        employer_city: false,
        employer_country: false,
        employer_hdmf_number: true,
        employer_name: true,
        employer_zipcode: false,
        head_of_office: false,
        head_of_office_position: false,
        hdmf_branch_code: true,
        total_ee_share: true,
        total_er_share: true,
        total_share: true
    }

    static propTypes = {
        generateForm: React.PropTypes.func,
        data: React.PropTypes.object,
        pageStatus: React.PropTypes.string,
        editing: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        routeParams: React.PropTypes.object,
        getFileLink: React.PropTypes.func,
        initializeData: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        products: React.PropTypes.array,
        saveLineItem: React.PropTypes.func,
        saveFormHeader: React.PropTypes.func,
        downloadUrl: React.PropTypes.string,
        setDownloadUrl: React.PropTypes.func,
        notifyUser: React.PropTypes.func
    }

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );
        this.editingRowValueRefs = { input: {}, currencies: {}};
        this.headerEdited = false;
        this.state = {
            data: {
                type: 'govt_form_hdmf_msrf',
                attributes: {
                    company_id: 0,
                    year_from: 0,
                    month_from: 0,
                    year_to: 0,
                    month_to: 0,
                    employer_hdmf_number: '',
                    employer_name: '',
                    employer_address: '',
                    employer_city: '',
                    employer_counter: '',
                    employer_zipcode: '',
                    head_of_office: '',
                    head_of_office_position: '',
                    hdmf_branch_code: null,
                    date: '',
                    total_ee_share: '',
                    total_er_share: '',
                    total_share: '',
                    line_items: []
                }
            },
            pager: '1 - 10',
            search: '',
            uri: '',
            showModal: false,
            editingRow: null
        };
        this.filterEmployees = debounce( this.filterEmployees.bind( this ), 250, { leading: true, trailing: true });
        this.setPager = this.setPager.bind( this );
    }

    /**
     * runs before render.
     * checks if there is a valid data to generate or if user is authorized to view this page.
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.government_form'], ( authorized ) => { !authorized && browserHistory.replace( '/unauthorized' ); });

        if ( this.props.routeParams.id ) {
            this.props.initializeData({ id: this.props.routeParams.id });
        } else {
            browserHistory.replace( '/forms' );
        }
    }

    /**
     * Perform actions when component props changes
     */
    componentWillReceiveProps( nextProps ) {
        ( nextProps.data !== this.props.data ) && this.setState({ data: nextProps.data });
        nextProps.downloadUrl !== this.props.downloadUrl
            && nextProps.downloadUrl
            && this.openFile( nextProps.downloadUrl );

        nextProps.pageStatus === PAGE_STATUSES.READY
            && this.props.pageStatus === PAGE_STATUSES.SAVING
            && this.setState({ editingRow: null }, () => {
                this.editingRowValueRefs = { input: {}, currencies: {}};
            });

        nextProps.editing !== this.headerEdited &&
        this.setState({ editingRow: null }, () => {
            this.headerEdited = nextProps.editing;
        });
    }

    /**
     * Component form check
     */
    componentDidUpdate( prevProps ) {
        if ( prevProps.data !== this.props.data ) {
            REQUIRED_FIELDS.INPUTS.forEach( ( key ) => {
                const input = this[ key ];
                input && input._validate( input.state.value );
            });

            REQUIRED_FIELDS.SELECTS.forEach( ( key ) => {
                const select = this[ key ];
                select && select._checkRequire(
                    typeof select.state.value === 'object' && select.state.value !== null
                        ? select.state.value.value
                        : select.state.value
                );
            });
        }
    }

    /**
     * Called before component unmounts.
     */
    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * Sets the table pager's label
     */
    setPager( tableProps ) {
        if ( tableProps.data.length ) {
            const pageSize = tableProps.data.length < tableProps.pageSize ? tableProps.data.length : tableProps.pageSize;
            let totalItems = pageSize * ( tableProps.page + 1 );
            if ( tableProps.pages === tableProps.page + 1 ) {
                totalItems = tableProps.data.length < totalItems ? tableProps.data.length : totalItems;
            }
            this.setState({ pager: `${( pageSize * tableProps.page ) + 1} - ${totalItems}` });
        } else {
            this.setState({ pager: '0' });
        }
    }

    /**
     * Get the table columns for render
     */
    getTableColumns() {
        const pageNotReady = this.props.pageStatus !== PAGE_STATUSES.READY;
        const saving = this.props.pageStatus === PAGE_STATUSES.SAVING;

        const columns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Pag-IBIG MID No.',
                accessor: 'hdmf_number',
                sortable: false,
                width: 160,
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <GovernmentNumbersInput
                            type="hdmf"
                            id="hdmf_number"
                            className="editing"
                            required
                            value={ row.hdmf_number }
                            disabled={ pageNotReady }
                            ref={ ( ref ) => { this.editingRowValueRefs.input.hdmf_number = ref; } }
                            onChange={ ( value ) => {
                                this.editingRowValueRefs.input.hdmf_number.setState(
                                    { value: formatHDMF( value ) }
                                );
                            } }
                        />
                    )
                    : (
                        <AllCaps>
                            {row.hdmf_number}
                        </AllCaps>
                    )
                )
            },
            {
                header: 'Membership Program',
                accessor: 'membership_program',
                sortable: false,
                minWidth: 180,
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <Input
                            id="membership_program"
                            className="editing"
                            value={ row.membership_program }
                            disabled={ pageNotReady }
                            ref={ ( ref ) => { this.editingRowValueRefs.input.membership_program = ref; } }
                        />
                    )
                    : (
                        <AllCaps>
                            {row.membership_program}
                        </AllCaps>
                    )
                )
            },
            {
                header: 'Last Name',
                accessor: 'last_name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <Input
                            id="last_name"
                            className="editing"
                            required
                            value={ row.last_name }
                            disabled={ pageNotReady }
                            ref={ ( ref ) => { this.editingRowValueRefs.input.last_name = ref; } }
                        />
                    )
                    : (
                        <AllCaps>
                            {row.last_name}
                        </AllCaps>
                    )
                )
            },
            {
                header: 'First Name',
                accessor: 'first_name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <Input
                            id="first_name"
                            required
                            className="editing"
                            value={ row.first_name }
                            disabled={ pageNotReady }
                            ref={ ( ref ) => { this.editingRowValueRefs.input.first_name = ref; } }
                        />
                    )
                    : (
                        <div>
                            {row.first_name}
                        </div>
                    )
                )
            },
            {
                header: 'Name Ext.',
                accessor: 'name_ext',
                sortable: false,
                minWidth: 100,
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <Input
                            id="name_ext"
                            className="editing"
                            value={ row.name_ext }
                            disabled={ pageNotReady }
                            ref={ ( ref ) => { this.editingRowValueRefs.input.name_ext = ref; } }
                        />
                    )
                    : (
                        <AllCaps>
                            {row.name_ext}
                        </AllCaps>
                    )
                )
            },
            {
                header: 'Middle Name',
                accessor: 'middle_name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <Input
                            id="middle_name"
                            className="editing"
                            value={ row.middle_name }
                            disabled={ pageNotReady }
                            ref={ ( ref ) => { this.editingRowValueRefs.input.middle_name = ref; } }
                        />
                    )
                    : (
                        <div>
                            {row.middle_name}
                        </div>
                    )
                )
            },
            {
                header: 'EE Share',
                accessor: 'ee_share',
                sortable: false,
                width: 120,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <Input
                            id="ee_share"
                            className="editing currency"
                            value={ this.formatCurrency( row.ee_share ) }
                            disabled={ pageNotReady }
                            ref={ ( ref ) => { this.editingRowValueRefs.currencies.ee_share = ref; } }
                            onBlur={ ( value ) => {
                                this.editingRowValueRefs.currencies.ee_share.setState(
                                    { value: this.formatCurrency( value ) }
                                );
                            } }
                            onFocus={ () => {
                                this.editingRowValueRefs.currencies.ee_share.setState(
                                    { value: stripNonDigit( this.editingRowValueRefs.currencies.ee_share.state.value ) }
                                );
                            } }
                            onChange={ ( value ) => {
                                this.editingRowValueRefs.currencies.ee_share.setState(
                                    { value: stripNonDigit( value ) }
                                );
                                this.headerEdited = true;
                            } }
                        />
                    )
                    : (
                        <div>
                            {this.formatCurrency( row.ee_share )}
                        </div>
                    )
                )
            },
            {
                header: 'ER Share',
                accessor: 'er_share',
                sortable: false,
                width: 120,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <Input
                            id="er_share"
                            className="editing currency"
                            value={ this.formatCurrency( row.er_share ) }
                            disabled={ pageNotReady }
                            ref={ ( ref ) => { this.editingRowValueRefs.currencies.er_share = ref; } }
                            onBlur={ ( value ) => {
                                this.editingRowValueRefs.currencies.er_share.setState(
                                    { value: this.formatCurrency( value ) }
                                );
                            } }
                            onFocus={ () => {
                                this.editingRowValueRefs.currencies.er_share.setState(
                                    { value: stripNonDigit( this.editingRowValueRefs.currencies.er_share.state.value ) }
                                );
                            } }
                            onChange={ ( value ) => {
                                this.editingRowValueRefs.currencies.er_share.setState(
                                    { value: stripNonDigit( value ) }
                                );
                                this.headerEdited = true;
                            } }
                        />
                    )
                    : (
                        <div>
                            {this.formatCurrency( row.er_share )}
                        </div>
                    )
                )
            },
            {
                header: 'Total',
                accessor: 'total_share',
                sortable: false,
                width: 120,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <Input
                            id="total_share"
                            className="editing currency"
                            value={ this.formatCurrency( row.total_share ) }
                            disabled={ pageNotReady }
                            ref={ ( ref ) => { this.editingRowValueRefs.currencies.total_share = ref; } }
                            onBlur={ ( value ) => {
                                this.editingRowValueRefs.currencies.total_share.setState(
                                    { value: this.formatCurrency( value ) }
                                );
                            } }
                            onFocus={ () => {
                                this.editingRowValueRefs.currencies.total_share.setState(
                                    { value: stripNonDigit( this.editingRowValueRefs.currencies.total_share.state.value ) }
                                );
                            } }
                            onChange={ ( value ) => {
                                this.editingRowValueRefs.currencies.total_share.setState(
                                    { value: stripNonDigit( value ) }
                                );
                                this.headerEdited = true;
                            } }
                        />
                    )
                    : (
                        <div>
                            {this.formatCurrency( row.total_share )}
                        </div>
                    )
                )
            },
            {
                header: () => (
                    <span>
                        <span>Employer Remarks</span>
                        <ToolTip
                            id="gross_tip"
                            target={ <i className="fa fa-info-circle" /> }
                        >
                            Example:<br />
                            N - Newly Hired (N: mm/dd/yyyy)
                            RS - Resigned/Separated (RS: mm/dd/yyyy)
                        </ToolTip>
                    </span>
                ),
                accessor: 'employer_remarks',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ? (
                        <Input
                            id="employer_remarks"
                            className="editing"
                            value={ row.employer_remarks }
                            disabled={ pageNotReady }
                            ref={ ( ref ) => { this.editingRowValueRefs.input.employer_remarks = ref; } }
                        />
                    )
                    : (
                        <div>
                            {row.employer_remarks}
                        </div>
                    )
                )
            },
            {
                header: '',
                accessor: '',
                sortable: false,
                width: 200,
                render: ({ row }) => (
                    ( row.id !== this.state.editingRow )
                        ? <Button
                            id={ `button-edit-${row.id}` }
                            label={ <span>Edit</span> }
                            type="grey"
                            disabled={ saving }
                            onClick={ () => { this.setState({ editingRow: row.id }); } }
                        />
                        : <div>
                            <Button
                                id={ `button-cancel-${row.id}` }
                                label={ <span>Cancel</span> }
                                type="neutral"
                                disabled={ saving }
                                onClick={ () => { this.setState({ editingRow: null }); this.editingRowValueRefs = { input: {}, currencies: {}}; } }
                            />
                            <Button
                                id={ `button-submit-${row.id}` }
                                label={ saving ? 'Saving' : <span>Submit</span> }
                                type="action"
                                onClick={ () => { this.updateLineItem(); } }
                            />
                        </div>

                )
            }
        ];
        return columns;
    }

    /**
     * gets the filtered data
     */
    getFilteredData() {
        if ( this.state.search.length ) {
            const search = this.state.search.toLowerCase();
            return this.state.data.attributes.line_items.filter( ( employee ) =>
                employee.last_name.toLowerCase().includes( search )
                    || employee.first_name.toLowerCase().includes( search )
                    || employee.hdmf_number.toLowerCase().includes( search )
            );
        }
        return this.state.data.attributes.line_items;
    }

    /**
     * Get pager text with non breaking spaces
     */
    getPagerText() {
        return `Showing ${this.state.pager} of ${this.formatNumber( this.state.data.attributes.line_items.length )} entries`;
    }

    /**
     * Download file to browser
     */
    openFile( url ) {
        if ( url ) {
            const linkElement = document.getElementById( 'downloadLink' );
            linkElement.target = '';
            linkElement.download = '';
            linkElement.href = url;
            linkElement.click();
            linkElement.href = '';
            this.props.setDownloadUrl( '' );
        }
    }

    /**
     * format the data to currency
     * @param value
     * @returns {XML}
     */
    formatCurrency( value ) {
        const convertedValue = Number( stripNonDigit( value ) );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `${Number( convertedValue ).toFixed( 2 ).replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    /**
     * format the data to currency
     * @param value
     * @returns {XML}
     */
    formatNumber( value ) {
        const convertedValue = Number( value );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `${Number( convertedValue ).toString().replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    /**
     * Check form header data
     */
    checkHeaderForm() {
        for ( const [ key, required ] of Object.entries( HDMFForms.headerFormProperties ) ) {
            if ( required && !this[ `${key}` ].state.value ) return false;
            if ( this[ `${key}` ].state.error ) return false;
        }
        return true;
    }
    /**
     * Downloads file to user
     */
    saveAndDownloadFile() {
        if ( !this.checkHeaderForm() ) {
            this.props.notifyUser(
                {
                    show: true,
                    title: 'Error',
                    message: 'Missing required fields.',
                    type: 'error'
                }
            );
            return;
        }
        if ( this.headerEdited ) {
            this.props.saveFormHeader({
                id: this.props.routeParams.id,
                company_id: this.state.data.attributes.company_id,
                date: moment( this.formDate.state.selectedDay, 'MM/DD/YYYY' ).format( 'YYYY-MM-DD' ),
                employer_address: this.employer_address.state.value,
                employer_city: this.employer_city.state.value,
                employer_country: this.employer_country.state.value,
                employer_hdmf_number: this.employer_hdmf_number.state.value,
                employer_name: this.employer_name.state.value,
                employer_zipcode: this.employer_zipcode.state.value,
                head_of_office: this.head_of_office.state.value,
                head_of_office_position: this.head_of_office_position.state.value,
                hdmf_branch_code: this.hdmf_branch_code.state.value,
                total_ee_share: stripNonDigit( this.total_ee_share.state.value ),
                total_er_share: stripNonDigit( this.total_er_share.state.value ),
                total_share: stripNonDigit( this.total_share.state.value )
            });
            this.headerEdited = false;
        } else if ( !this.props.editing ) {
            this.props.getFileLink(
                { id: this.props.routeParams.id }
            );
        }
    }

    /**
     * regenerate form
     */
    regenerateForm() {
        this.props.generateForm(
            {
                company_id: this.state.data.attributes.company_id,
                month_from: this.state.data.attributes.month_from,
                year_from: this.state.data.attributes.year_from,
                month_to: this.state.data.attributes.month_to,
                year_to: this.state.data.attributes.year_to
            }
        );
    }

    /**
     * Update line item
     */
    updateLineItem() {
        const lineItem = {};
        for ( const [ key, value ] of Object.entries( this.editingRowValueRefs.input ) ) {
            if ( value.state.error ) return;
            lineItem[ `${key}` ] = value.state.value;
        }
        for ( const [ key, value ] of Object.entries( this.editingRowValueRefs.currencies ) ) {
            if ( value.state.error ) return;
            lineItem[ `${key}` ] = stripNonDigit( value.state.value );
        }
        lineItem.id = this.state.editingRow;
        this.props.saveLineItem(
            {
                id: this.state.data.id,
                company_id: this.state.data.attributes.company_id,
                lineItem
            }
        );
    }

    /**
     * Filter employees by search criteria
     * @param {*} criteria
     */
    filterEmployees( criteria ) {
        this.setState({ search: criteria }, () => this.setPager( this.contributionTable.tableComponent.state ) );
    }

    /**
     * renders a loading screen
     */
    renderLoadingPage( generating = false ) {
        return (
            <MessageWrapperStyles>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        <H3>{ generating ? 'Generating.' : 'Loading data.' } Please wait...</H3>
                    </div>
                </div>
            </MessageWrapperStyles>
        );
    }

    /**
     * renders the form fields necessary to finish generating the SSS forms
     */
    renderForm() {
        const { data } = this.state;
        const pageNotReady = this.props.pageStatus !== PAGE_STATUSES.READY;

        return (
            <PageWrapper step={ 2 } >
                <GovtFormWrapper>
                    <div className="hdmf-header">
                        <h5>Home Development Mutual Fund (HDMF)</h5>
                        <p>To accomplish your Membership Savings Remittance Form (MSRF), follow the steps below:</p>
                        <div className="row msrf-directions-row">
                            <div className="col-sm-8 directions">
                                <ol>
                                    <li>Fill-out the fields required below.</li>
                                    <li>Generate and print the MSRF PDF File.</li>
                                    <li>Bring the printed form to the Pag-IBIG branch near you.</li>
                                </ol>
                            </div>
                            <div className="col-sm-4 gov-form-options">
                                <div className="buttonbox">
                                    <Button
                                        id="button-regenerate"
                                        label="Regenerate Form"
                                        type="neutral"
                                        size="large"
                                        onClick={ () => { this.regenerateForm(); } }
                                        disabled={ pageNotReady }
                                    />
                                    <Button
                                        id="button-save-and-download"
                                        label="Save and Download"
                                        type="action"
                                        size="large"
                                        onClick={ () => { this.saveAndDownloadFile(); } }
                                        disabled={ pageNotReady }
                                    />
                                    <A download id="downloadLink"></A>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="company-section">
                        <FormTitle><h3>MEMBERSHIP SAVINGS REMITTANCE FORM (MSRF)</h3></FormTitle>

                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="period_covered"
                                    label="Period Covered"
                                    value={ moment( `${data.attributes.month_from}-01-${data.attributes.year_from}`, 'MM-DD-YYYY' ).format( 'MMMM YYYY' ) }
                                    disabled
                                />
                            </div>
                            <div className="col-xs-8">
                                <Input
                                    id="employer-name"
                                    label={
                                        <span>
                                            <span>Employer/Business Name</span>
                                            <span style={ { position: 'absolute', paddingLeft: '20px' } } >
                                                <ToolTip
                                                    id="employer_name_tip"
                                                    target={ <i className="fa fa-info-circle" /> }
                                                >
                                                    Please ensure that the Registered Business name is entered in this field.
                                                </ToolTip>
                                            </span>
                                        </span>
                                    }
                                    value={ data.attributes.employer_name || '' }
                                    ref={ ( ref ) => { this.employer_name = ref; } }
                                    required
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <GovernmentNumbersInput
                                    type="hdmf"
                                    id="employer_hdmf_number"
                                    label="Pag-IBIG Employer's ID Number"
                                    value={ data.attributes.employer_hdmf_number || '' }
                                    onChange={ ( value ) => {
                                        this.employer_hdmf_number.setState(
                                            { value: formatHDMF( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    required
                                    ref={ ( ref ) => { this.employer_hdmf_number = ref; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="address"
                                    label="Employer/Business Address"
                                    value={ data.attributes.employer_address || '' }
                                    ref={ ( ref ) => { this.employer_address = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    required
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="city"
                                    label="City"
                                    required
                                    value={ data.attributes.employer_city }
                                    ref={ ( ref ) => { this.employer_city = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="country"
                                    label="Country"
                                    value={ data.attributes.employer_country || 'PHILIPPINES' }
                                    ref={ ( ref ) => { this.employer_country = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="zip-code"
                                    label="ZIP Code"
                                    value={ data.attributes.employer_zipcode || '' }
                                    ref={ ( ref ) => { this.employer_zipcode = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="hdmf-branch-code"
                                    label="Branch Code"
                                    required
                                    value={ data.attributes.hdmf_branch_code || '' }
                                    ref={ ( ref ) => { this.hdmf_branch_code = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="head_of_office"
                                    label="Head of Office or Authorized Signatory"
                                    value={ data.attributes.head_of_office || '' }
                                    ref={ ( ref ) => { this.head_of_office = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="head_of_office_position"
                                    label="Designation/Position"
                                    value={ data.attributes.head_of_office_position || '' }
                                    ref={ ( ref ) => { this.head_of_office_position = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <DatePicker
                                    label="Date"
                                    dayFormat="MM/DD/YYYY"
                                    selectedDay={ moment( data.attributes.date, 'YYYY-MM-DD' ).format( 'MM/DD/YYYY' ) }
                                    ref={ ( ref ) => { this.formDate = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="total_ee_share"
                                    className="currency"
                                    label="Total Pag-IBIG Employee Share"
                                    value={ this.formatCurrency( data.attributes.total_ee_share ) || '' }
                                    ref={ ( ref ) => { this.total_ee_share = ref; } }
                                    required
                                    onBlur={ ( value ) => {
                                        this.total_ee_share.setState(
                                            { value: this.formatCurrency( value ) }
                                        );
                                    } }
                                    onFocus={ () => {
                                        this.total_ee_share.setState(
                                            { value: stripNonDigit( this.total_ee_share.state.value ) }
                                        );
                                    } }
                                    onChange={ ( value ) => {
                                        this.total_ee_share.setState(
                                            { value: stripNonDigit( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="total_ee_share"
                                    className="currency"
                                    label="Total Pag-IBIG Employer Share"
                                    required
                                    value={ this.formatCurrency( data.attributes.total_er_share ) || '' }
                                    ref={ ( ref ) => { this.total_er_share = ref; } }
                                    onBlur={ ( value ) => {
                                        this.total_er_share.setState(
                                            { value: this.formatCurrency( value ) }
                                        );
                                    } }
                                    onFocus={ () => {
                                        this.total_er_share.setState(
                                            { value: stripNonDigit( this.total_er_share.state.value ) }
                                        );
                                    } }
                                    onChange={ ( value ) => {
                                        this.total_er_share.setState(
                                            { value: stripNonDigit( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="total_pagibig_share"
                                    label="Total Pag-IBIG Share"
                                    className="currency"
                                    required
                                    value={ this.formatCurrency( data.attributes.total_share ) || '' }
                                    ref={ ( ref ) => { this.total_share = ref; } }
                                    onBlur={ ( value ) => {
                                        this.total_share.setState(
                                            { value: this.formatCurrency( value ) }
                                        );
                                    } }
                                    onFocus={ () => {
                                        this.total_share.setState(
                                            { value: stripNonDigit( this.total_share.state.value ) }
                                        );
                                    } }
                                    onChange={ ( value ) => {
                                        this.total_share.setState(
                                            { value: stripNonDigit( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                    </div>
                    <div className="contribution-list">
                        { this.renderTableData() }
                    </div>
                </GovtFormWrapper>
            </PageWrapper>
        );
    }

    /**
     * Render table data
     */
    renderTableData() {
        const items = this.state.data.attributes.line_items.length;
        if ( items ) {
            return (
                <div>
                    <ContributionListHeader>
                        <SearchHeader>
                            <div className="title"><H5>Pag-IBIG MSRF List</H5></div>
                            <div>
                                <Search
                                    id="textSearch"
                                    className="col-xs-4"
                                    placeholder="Search for HDMF number, last name or first name"
                                    handleSearch={ this.filterEmployees }
                                />
                            </div>
                        </SearchHeader>
                        <span id="pager">{ this.getPagerText() }</span>
                    </ContributionListHeader>
                    <Table
                        columns={ this.getTableColumns() }
                        data={ this.getFilteredData() || [] }
                        pagination={ ( this.state.data.attributes.line_items.length || 0 ) > 10 }
                        ref={ ( ref ) => { this.contributionTable = ref; } }
                        onDataChange={ this.setPager }
                    />
                </div>
            );
        }
        return (
            <div className="NoDataFound"><span>No Data</span></div>
        );
    }
    /**
     * Renders Form component to DOM
     */
    render() {
        const { pageStatus, notification } = this.props;
        const isLoading = pageStatus === PAGE_STATUSES.LOADING;
        const isGenerating = pageStatus === PAGE_STATUSES.GENERATING;

        return (
            <div>
                <Helmet
                    title="Government Forms: HDMF"
                    meta={ [
                        { name: 'description', content: 'Generate HDMF Forms' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A href="/payroll/forms">
                            &#8592; Back to Government Forms
                        </A>
                    </Container>
                </NavWrapper>
                {
                    ( isLoading || isGenerating )
                        ? this.renderLoadingPage( isGenerating )
                        : this.renderForm()
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    data: makeSelectData(),
    pageStatus: makeSelectPageStatus(),
    editing: makeSelectEditing(),
    downloadUrl: makeSelectDownloadUrl(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        hdmfFormActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( HDMFForms );
