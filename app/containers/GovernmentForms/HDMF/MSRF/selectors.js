import { createSelector } from 'reselect';

/**
 * Direct selector to the create state domain
 */
const selectCreateDomain = () => ( state ) => state.get( 'HDMFMSRFForms' );

/**
 * Other specific selectors
 */
const makeSelectData = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'data' ).toJS()
);

const makeSelectPageStatus = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'page_status' )
);

const makeSelectEditing = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'editing' )
);

const makeSelectNotification = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectDownloadUrl = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'downloadUrl' )
);

export {
    makeSelectData,
    makeSelectEditing,
    makeSelectPageStatus,
    makeSelectDownloadUrl,
    makeSelectNotification
};
