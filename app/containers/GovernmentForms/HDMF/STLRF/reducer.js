import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';
import {
    SET_FORM_OPTIONS,
    SET_DATA,
    SET_URI,
    NOTIFICATION_SAGA,
    SET_PAGE_STATUS
} from './constants';
import { PAGE_STATUSES } from '../../constants';

const initialState = fromJS({
    options: {},
    data: {
        type: 'govt_form_hdmf_stlrf',
        id: 0,
        attributes: {
            company_id: 0,
            year_from: 0,
            month_from: 0,
            year_to: 0,
            month_to: 0,
            employer_hdmf_number: '',
            employer_name: '',
            employer_address: '',
            employer_city: '',
            employer_counter: '',
            employer_zipcode: '',
            head_of_office: '',
            head_of_office_position: '',
            hdmf_branch_code: '',
            date: '',
            total_ee_share: '',
            total_er_share: '',
            total_share: '',
            line_items: []
        }
    },
    page_status: PAGE_STATUSES.LOADING,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    downloadUrl: ''
});

/**
 * transforms form options data from API to accomodate structure in app
 */
function prepareFormOptions( data ) {
    const response = {};
    const keys = Object.keys( data );
    keys.forEach( ( key ) => {
        const formattedOption = [];
        data[ key ].forEach( ({ label, code }) => {
            formattedOption.push({
                label: label.toString(),
                value: code.toString()
            });
        });
        response[ key ] = formattedOption;
    });

    return response;
}

/**
 * SSSFormsReducer
 */
function HDMFSTLRFFormsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_DATA:
            return state.set( 'data', fromJS( action.payload ) );
        case SET_FORM_OPTIONS:
            return state.set( 'options', fromJS( prepareFormOptions( action.payload ) ) );
        case SET_PAGE_STATUS:
            return state.set( 'page_status', action.payload );
        case SET_URI:
            return state.set( 'downloadUrl', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default HDMFSTLRFFormsReducer;
