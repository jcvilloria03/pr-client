import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';
import debounce from 'lodash/debounce';

import GovernmentNumbersInput from 'components/GovernmentNumbersInput';
import Select from 'components/Select';
import SnackBar from 'components/SnackBar';
import Input from 'components/Input';
import { H3, H5 } from 'components/Typography';
import Table from 'components/Table';
import Button from 'components/Button';
import A from 'components/A';
import ToolTip from 'components/ToolTip';
import DatePicker from 'components/DatePicker';
import Search from 'components/Search';
import { formatHDMF, stripNonNumeric, stripNonDigit } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';

import { NavWrapper } from 'containers/GovernmentForms/Generate/styles';

import PageWrapper from '../../PageWrapper';
import * as hdmfFormActions from './actions';

import {
    makeSelectData,
    makeSelectPageStatus,
    makeSelectDownloadUrl,
    makeSelectNotification
} from './selectors';

import {
    GovtFormWrapper,
    MessageWrapperStyles,
    ContributionListHeader,
    SearchHeader,
    FormTitle,
    InputWrapper,
    AllCaps
} from './styles';
import { STRLF_LOAN_TYPES, REQUIRED_FIELDS } from './constants';
import { PAGE_STATUSES } from '../../constants';

/**
 * STLRF component
 */
export class HDMFSTLRFForm extends React.PureComponent {
    static headerFormProperties = {
        employer_address: true,
        employer_city: true,
        employer_country: false,
        employer_hdmf_number: true,
        employer_name: true,
        employer_zipcode: false,
        employer_tel_no: false,
        head_of_office: false,
        head_of_office_position: false,
        hdmf_branch_code: true,
        formDate: true,
        total_loan_amount: true
    }
    static propTypes = {
        generateForm: React.PropTypes.func,
        data: React.PropTypes.object,
        pageStatus: React.PropTypes.string,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        routeParams: React.PropTypes.object,
        getFileLink: React.PropTypes.func,
        initializeData: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        products: React.PropTypes.array,
        saveLineItem: React.PropTypes.func,
        saveFormHeader: React.PropTypes.func,
        downloadUrl: React.PropTypes.string,
        setDownloadUrl: React.PropTypes.func,
        notifyUser: React.PropTypes.func
    }

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );
        this.editingRowValueRefs = {
            input: {},
            select: {},
            currencies: {}
        };
        this.headerEdited = false;
        this.state = {
            data: {
                type: 'govt_form_hdmf_stlrf',
                attributes: {
                    company_id: 0,
                    year_from: 0,
                    month_from: 0,
                    year_to: 0,
                    month_to: 0,
                    employer_hdmf_number: '',
                    employer_name: '',
                    employer_address: '',
                    employer_city: '',
                    employer_counter: '',
                    employer_zipcode: '',
                    employer_tel_no: '',
                    head_of_office: '',
                    head_of_office_position: '',
                    hdmf_branch_code: '',
                    date: '',
                    total_loan_amount: '',
                    total_ee_share: '',
                    total_er_share: '',
                    total_share: '',
                    line_items: []
                }
            },
            pager: '1 - 10',
            search: '',
            uri: '',
            showModal: false,
            editingRow: null
        };
        this.filterEmployees = debounce( this.filterEmployees.bind( this ), 250, { leading: true, trailing: true });
        this.setPager = this.setPager.bind( this );
    }

    /**
     * runs before render.
     * checks if there is a valid data to generate or if user is authorized to view this page.
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.government_form'], ( authorized ) => { !authorized && browserHistory.replace( '/unauthorized' ); });

        if ( this.props.routeParams.id ) {
            this.props.initializeData({ id: this.props.routeParams.id });
        } else {
            browserHistory.replace( '/forms' );
        }
    }

    /**
     * Perform actions when component props changes
     */
    componentWillReceiveProps( nextProps ) {
        ( nextProps.data !== this.props.data ) && this.setState({ data: nextProps.data });

        nextProps.downloadUrl !== this.props.downloadUrl
            && nextProps.downloadUrl
            && this.openFile( nextProps.downloadUrl );

        nextProps.pageStatus === PAGE_STATUSES.READY
            && this.props.pageStatus === PAGE_STATUSES.SAVING
            && this.setState({ editingRow: null }, () => {
                this.editingRowValueRefs = { input: {}, currencies: {}};
            });
    }

    /**
     * Component form check
     */
    componentDidUpdate( prevProps ) {
        if ( prevProps.data !== this.props.data ) {
            REQUIRED_FIELDS.INPUTS.forEach( ( key ) => {
                const input = this[ key ];
                input && input._validate( input.state.value );
            });

            REQUIRED_FIELDS.SELECTS.forEach( ( key ) => {
                const select = this[ key ];
                select && select._checkRequire(
                    typeof select.state.value === 'object' && select.state.value !== null
                        ? select.state.value.value
                        : select.state.value
                );
            });

            REQUIRED_FIELDS.DATE_PICKERS.forEach( ( key ) => {
                const datePicker = this[ key ];
                datePicker && datePicker.checkRequired();
            });
        }
    }

    /**
     * Called before component unmounts.
     */
    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * Sets the table pager's label
     */
    setPager( tableProps ) {
        if ( tableProps.data.length ) {
            const pageSize = tableProps.data.length < tableProps.pageSize ? tableProps.data.length : tableProps.pageSize;
            let totalItems = pageSize * ( tableProps.page + 1 );
            if ( tableProps.pages === tableProps.page + 1 ) {
                totalItems = tableProps.data.length < totalItems ? tableProps.data.length : totalItems;
            }
            this.setState({ pager: `${( pageSize * tableProps.page ) + 1} - ${totalItems}` });
        } else {
            this.setState({ pager: '0' });
        }
    }

    /**
     * Get Loan type label
     */
    getLoanTypeLabel( type ) {
        const loanType = STRLF_LOAN_TYPES.find( ( obj ) => obj.value === type );
        if ( !loanType ) {
            return '';
        }
        return loanType.label;
    }
    /**
     * Get the table columns for render
     */
    getTableColumns() {
        const pageNotReady = this.props.pageStatus !== PAGE_STATUSES.READY;
        const saving = this.props.pageStatus === PAGE_STATUSES.SAVING;

        const columns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'MID No.',
                accessor: 'hdmf_number',
                sortable: false,
                width: 180,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <GovernmentNumbersInput
                                type="hdmf"
                                id="hdmf_number"
                                className="editing"
                                required
                                value={ row.hdmf_number }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.hdmf_number = ref; } }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.input.hdmf_number.setState(
                                        { value: formatHDMF( value ) }
                                    );
                                } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {row.hdmf_number}
                        </div>
                )
            },
            {
                header: 'Application No.',
                accessor: 'application_number',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="application_number"
                                className="editing"
                                value={ row.application_number }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.application_number = ref; } }
                            />
                        </InputWrapper>
                        :
                        <AllCaps>
                            {row.application_number}
                        </AllCaps>
                )
            },
            {
                header: 'Last Name',
                accessor: 'last_name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="last_name"
                                className="editing"
                                required
                                value={ row.last_name }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.last_name = ref; } }
                            />
                        </InputWrapper>
                        :
                        <AllCaps>
                            {row.last_name}
                        </AllCaps>
                )
            },
            {
                header: 'First Name',
                accessor: 'first_name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="first_name"
                                className="editing"
                                value={ row.first_name }
                                required
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.first_name = ref; } }
                            />
                        </InputWrapper>
                        :
                        <AllCaps>
                            {row.first_name}
                        </AllCaps>
                )
            },
            {
                header: 'Name Ext.',
                accessor: 'name_ext',
                sortable: false,
                minWidth: 120,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="name_ext"
                                className="editing"
                                value={ row.name_ext }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.name_ext = ref; } }
                            />
                        </InputWrapper>
                        :
                        <AllCaps>
                            {row.name_ext}
                        </AllCaps>
                )
            },
            {
                header: 'Middle Name',
                accessor: 'middle_name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="middle_name"
                                className="editing"
                                value={ row.middle_name }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.middle_name = ref; } }
                            />
                        </InputWrapper>
                        :
                        <AllCaps>
                            {row.middle_name}
                        </AllCaps>
                )
            },
            {
                header: 'Loan Type',
                accessor: 'hdmf_loan_type',
                sortable: false,
                width: 150,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <Select
                            required
                            id="hdmf_loan_type"
                            className="editing"
                            value={ row.hdmf_loan_type }
                            data={ STRLF_LOAN_TYPES }
                            ref={ ( ref ) => { this.editingRowValueRefs.select.hdmf_loan_type = ref; } }
                            disabled={ pageNotReady }
                        />
                        :
                            <div>
                                {this.getLoanTypeLabel( row.hdmf_loan_type )}
                            </div>
                )
            },
            {
                header: 'Amount',
                accessor: 'payment_amount',
                sortable: false,
                width: 120,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="payment_amount"
                                className="editing currency"
                                value={ row.payment_amount }
                                disabled={ pageNotReady }
                                onChange={ ( value ) => {
                                    this.editingRowValueRefs.currencies.payment_amount.setState(
                                        { value: stripNonDigit( value ) }
                                    );
                                    this.headerEdited = true;
                                } }
                                onBlur={ ( value ) => {
                                    this.editingRowValueRefs.currencies.payment_amount.setState(
                                        { value: this.formatCurrency( value ) }
                                    );
                                } }
                                onFocus={ () => {
                                    this.editingRowValueRefs.currencies.payment_amount.setState(
                                        { value: stripNonDigit( this.editingRowValueRefs.currencies.payment_amount ) }
                                    );
                                } }

                                ref={ ( ref ) => { this.editingRowValueRefs.currencies.payment_amount = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {this.formatCurrency( row.payment_amount )}
                        </div>
                )
            },
            {
                header: 'Employer Remarks',
                accessor: 'employer_remarks',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <InputWrapper>
                            <Input
                                id="employer_remarks"
                                className="editing"
                                value={ row.employer_remarks }
                                disabled={ pageNotReady }
                                ref={ ( ref ) => { this.editingRowValueRefs.input.employer_remarks = ref; } }
                            />
                        </InputWrapper>
                        :
                        <div>
                            {row.employer_remarks}
                        </div>
                )
            },
            {
                header: '',
                accessor: '',
                sortable: false,
                width: 200,
                render: ({ row }) => (
                    ( row.id !== this.state.editingRow )
                        ? <Button
                            id={ `button-edit-${row.id}` }
                            label={ <span>Edit</span> }
                            type="grey"
                            disabled={ saving }
                            onClick={ () => { this.setState({ editingRow: row.id }); } }
                        />
                        : <div>
                            <Button
                                id={ `button-cancel-${row.id}` }
                                label={ <span>Cancel</span> }
                                type="neutral"
                                disabled={ saving }
                                onClick={ () => { this.setState({ editingRow: null }); this.editingRowValueRefs = { input: {}, select: {}, currencies: {}}; } }
                            />
                            <Button
                                id={ `button-submit-${row.id}` }
                                label={ saving ? 'Saving' : <span>Submit</span> }
                                type="action"
                                onClick={ () => { this.updateLineItem(); } }
                                disabled={ this.props.pageStatus === PAGE_STATUSES.SAVING }
                            />
                        </div>

                )
            }
        ];
        return columns;
    }

    /**
     * gets the filtered data
     */
    getFilteredData() {
        if ( this.state.search.length ) {
            const search = this.state.search.toLowerCase();
            return this.state.data.attributes.line_items.filter( ( employee ) =>
                employee.last_name.toLowerCase().includes( search )
                    || employee.first_name.toLowerCase().includes( search )
                    || employee.hdmf_number.toLowerCase().includes( search )
                    || employee.application_number.toLowerCase().includes( search )
            );
        }
        return this.state.data.attributes.line_items;
    }

    /**
     * Get pager text with non breaking spaces
     */
    getPagerText() {
        return `Showing ${this.state.pager} of ${this.formatNumber( this.state.data.attributes.line_items.length )} entries`;
    }

    /**
     * Download file to browser
     */
    openFile( url ) {
        if ( url ) {
            const linkElement = document.getElementById( 'downloadLink' );
            linkElement.target = '';
            linkElement.download = '';
            linkElement.href = url;
            linkElement.click();
            linkElement.href = '';
            this.props.setDownloadUrl( '' );
        }
    }

    /**
     * format the data to currency
     * @param value
     * @returns {XML}
     */
    formatCurrency( value ) {
        const convertedValue = Number( stripNonDigit( value ) );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `${Number( convertedValue ).toFixed( 2 ).replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    /**
     * format the data to currency
     * @param value
     * @returns {XML}
     */
    formatNumber( value ) {
        const convertedValue = Number( value );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `${Number( convertedValue ).toString().replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    /**
     * Chech header form requirements
     */
    checkHeaderForm() {
        for ( const [ key, required ] of Object.entries( HDMFSTLRFForm.headerFormProperties ) ) {
            if ( `${key}` === 'formDate' ) {
                if ( required && !this[ `${key}` ].state.selectedDay ) {
                    return false;
                }
            } else if ( required && !this[ `${key}` ].state.value ) {
                return false;
            }
            if ( this[ `${key}` ].state.error ) return false;
        }
        return true;
    }
    /**
     * Downloads file to user
     */
    saveAndDownloadFile() {
        if ( !this.checkHeaderForm() ) {
            this.props.notifyUser(
                {
                    show: true,
                    title: 'Error',
                    message: 'Missing required fields.',
                    type: 'error'
                }
            );
            return;
        }
        if ( this.headerEdited ) {
            this.props.saveFormHeader({
                id: this.props.routeParams.id,
                company_id: this.state.data.attributes.company_id,
                employer_address: this.employer_address.state.value,
                employer_city: this.employer_city.state.value,
                employer_country: this.employer_country.state.value,
                employer_hdmf_number: this.employer_hdmf_number.state.value,
                employer_name: this.employer_name.state.value,
                employer_zipcode: this.employer_zipcode.state.value,
                employer_tel_no: this.employer_tel_no.state.value,
                head_of_office: this.head_of_office.state.value,
                head_of_office_position: this.head_of_office_position.state.value,
                hdmf_branch_code: this.hdmf_branch_code.state.value,
                date: moment( this.formDate.state.selectedDay, 'MM/DD/YYYY' ).format( 'YYYY-MM-DD' ),
                total_loan_amount: stripNonDigit( this.total_loan_amount.state.value )
            });
            this.headerEdited = false;
        } else {
            this.props.getFileLink({ id: this.props.routeParams.id });
        }
    }

    /**
     * regenerate form
     */
    regenerateForm() {
        this.props.generateForm(
            {
                company_id: this.state.data.attributes.company_id,
                month_from: this.state.data.attributes.month_from,
                year_from: this.state.data.attributes.year_from,
                month_to: this.state.data.attributes.month_to,
                year_to: this.state.data.attributes.year_to
            }
        );
    }

    /**
     * Update line item
     */
    updateLineItem() {
        const lineItem = {};
        for ( const [ key, value ] of Object.entries( this.editingRowValueRefs.input ) ) {
            if ( value.state.error ) return;
            lineItem[ `${key}` ] = value.state.value;
        }

        for ( const [ key, value ] of Object.entries( this.editingRowValueRefs.select ) ) {
            if ( value.state.error ) return;
            lineItem[ `${key}` ] = value.state.value.value;
        }

        for ( const [ key, value ] of Object.entries( this.editingRowValueRefs.currencies ) ) {
            if ( value.state.error ) return;
            lineItem[ `${key}` ] = stripNonDigit( value.state.value );
        }
        lineItem.id = this.state.editingRow;
        this.props.saveLineItem(
            {
                id: this.state.data.id,
                company_id: this.state.data.attributes.company_id,
                lineItem
            }
        );
    }

    /**
     * Filter employees by search criteria
     * @param {*} criteria
     */
    filterEmployees( criteria ) {
        this.setState({ search: criteria }, () => this.setPager( this.contributionTable.tableComponent.state ) );
    }

    /**
     * renders a loading screen
     */
    renderLoadingPage( generating = false ) {
        return (
            <MessageWrapperStyles>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        <H3>{ generating ? 'Generating.' : 'Loading data.' } Please wait...</H3>
                    </div>
                </div>
            </MessageWrapperStyles>
        );
    }

    /**
     * renders the form fields necessary to finish generating the SSS forms
     */
    renderForm() {
        const { data } = this.state;
        const pageNotReady = this.props.pageStatus !== PAGE_STATUSES.READY;

        return (
            <PageWrapper step={ 2 } >
                <GovtFormWrapper>
                    <div className="hdmf-header">
                        <h5>Home Development Mutual Fund (HDMF)</h5>
                        <p>To accomplish your Short-Term Loan Remittance Form (STLRF), follow the steps below:</p>
                        <div className="row msrf-directions-row">
                            <div className="col-sm-8 directions">
                                <ol>
                                    <li>Fill-out the fields required below.</li>
                                    <li>Generate and print the STLRF PDF File.</li>
                                    <li>Bring the printed form to the Pag-IBIG branch near you.</li>
                                </ol>
                            </div>
                            <div className="col-sm-4 gov-form-options">
                                <div className="buttonbox">
                                    <Button
                                        id="button-regenerate"
                                        label="Regenerate Form"
                                        type="neutral"
                                        size="large"
                                        onClick={ () => { this.regenerateForm(); } }
                                        disabled={ pageNotReady }
                                    />
                                    <Button
                                        id="button-save-and-download"
                                        label="Save and Download"
                                        type="action"
                                        size="large"
                                        onClick={ () => { this.saveAndDownloadFile(); } }
                                        disabled={ pageNotReady }
                                    />
                                    <A download id="downloadLink"></A>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="company-section">
                        <FormTitle><h3>SHORT-TERM LOAN REMITTANCE FORM (STLRF)</h3></FormTitle>

                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="period_covered"
                                    label="Period Covered"
                                    value={ moment( `${data.attributes.month_from}-01-${data.attributes.year_from}`, 'MM-DD-YYYY' ).format( 'MMMM YYYY' ) }
                                    disabled
                                />
                            </div>
                            <div className="col-xs-8">
                                <Input
                                    id="employer-name"
                                    label={
                                        <span>
                                            <span>Employer/Business Name</span>
                                            <span style={ { position: 'absolute', paddingLeft: '20px' } } >
                                                <ToolTip
                                                    id="employer_name_tip"
                                                    target={ <i className="fa fa-info-circle" /> }
                                                    placement="right"
                                                >
                                                    Please ensure that the Registered Business name is entered in this field.
                                                </ToolTip>
                                            </span>
                                        </span>
                                    }
                                    value={ data.attributes.employer_name }
                                    ref={ ( ref ) => { this.employer_name = ref; } }
                                    required
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <GovernmentNumbersInput
                                    id="employer_hdmf_number"
                                    label="Pag-IBIG Employer's ID Number"
                                    type="hdmf"
                                    required
                                    value={ data.attributes.employer_hdmf_number }
                                    ref={ ( ref ) => { this.employer_hdmf_number = ref; } }
                                    onChange={ ( value ) => {
                                        this.headerEdited = true;
                                        this.employer_hdmf_number.setState({
                                            value: formatHDMF( value )
                                        });
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="address"
                                    label="Employer/Business Address"
                                    required
                                    value={ data.attributes.employer_address }
                                    ref={ ( ref ) => { this.employer_address = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="city"
                                    label="City"
                                    required
                                    value={ data.attributes.employer_city }
                                    ref={ ( ref ) => { this.employer_city = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="country"
                                    label="Country"
                                    value={ data.attributes.employer_country || 'PHILIPPINES' }
                                    ref={ ( ref ) => { this.employer_country = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="zip_code"
                                    label="Zip Code"
                                    value={ data.attributes.employer_zipcode }
                                    ref={ ( ref ) => { this.employer_zipcode = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="tel"
                                    label="Telephone Number"
                                    value={ data.attributes.employer_tel_no }
                                    ref={ ( ref ) => { this.employer_tel_no = ref; } }
                                    onChange={ ( value ) => {
                                        this.employer_tel_no.setState(
                                            { value: stripNonNumeric( value ).substring( 0, 15 ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="head_of_office"
                                    label="Head of Office or Authorized Signatory"
                                    value={ data.attributes.head_of_office }
                                    ref={ ( ref ) => { this.head_of_office = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="head_of_office_position"
                                    label="Designation/Position"
                                    value={ data.attributes.head_of_office_position }
                                    ref={ ( ref ) => { this.head_of_office_position = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <DatePicker
                                    label="Date"
                                    dayFormat="MM/DD/YYYY"
                                    selectedDay={ moment( data.attributes.date, 'YYYY-MM-DD' ).format( 'MM/DD/YYYY' ) }
                                    ref={ ( ref ) => { this.formDate = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <Input
                                    id="total_loan_amount"
                                    className="currency"
                                    label="Total Pag-IBIG Loan Amount"
                                    value={ this.formatCurrency( data.attributes.total_loan_amount ) }
                                    ref={ ( ref ) => { this.total_loan_amount = ref; } }
                                    onChange={ ( value ) => {
                                        this.total_loan_amount.setState(
                                            { value: stripNonDigit( value ) }
                                        );
                                        this.headerEdited = true;
                                    } }
                                    onBlur={ ( value ) => {
                                        this.total_loan_amount.setState(
                                            { value: this.formatCurrency( value ) }
                                        );
                                    } }
                                    onFocus={ () => {
                                        this.total_loan_amount.setState(
                                            { value: stripNonDigit( this.total_loan_amount.state.value ) }
                                        );
                                    } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id="hdmf-branch-code"
                                    label="Branch Code"
                                    required
                                    value={ data.attributes.hdmf_branch_code || '' }
                                    ref={ ( ref ) => { this.hdmf_branch_code = ref; } }
                                    onChange={ () => { this.headerEdited = true; } }
                                    disabled={ pageNotReady }
                                />
                            </div>
                        </div>
                    </div>
                    <div className="contribution-list">
                        { this.renderTableData() }
                    </div>
                </GovtFormWrapper>
            </PageWrapper>
        );
    }

    /**
     * Render Data
     */
    renderTableData() {
        const items = this.state.data.attributes.line_items.length;
        if ( items ) {
            return (
                <div>
                    <ContributionListHeader>
                        <SearchHeader>
                            <div className="title"><H5>Pag-IBIG STRLF List</H5></div>
                            <div>
                                <Search
                                    id="textSearch"
                                    className="col-xs-4"
                                    placeholder="Search for HDMF number, Application Number, last name or first name"
                                    handleSearch={ this.filterEmployees }
                                />
                            </div>
                        </SearchHeader>
                        <span id="pager">{ this.getPagerText() }</span>
                    </ContributionListHeader>
                    <Table
                        columns={ this.getTableColumns() }
                        data={ this.getFilteredData() || [] }
                        pagination={ ( this.state.data.attributes.line_items.length || 0 ) > 10 }
                        ref={ ( ref ) => { this.contributionTable = ref; } }
                        onDataChange={ this.setPager }
                    />
                </div>
            );
        }
        return (
            <div className="NoDataFound"><span>No Data</span></div>
        );
    }

    /**
     * Renders Form component to DOM
     */
    render() {
        const { pageStatus, notification } = this.props;
        const isLoading = pageStatus === PAGE_STATUSES.LOADING;
        const isGenerating = pageStatus === PAGE_STATUSES.GENERATING;

        return (
            <div>
                <Helmet
                    title="Government Forms: HDMF"
                    meta={ [
                        { name: 'description', content: 'Generate HDMF Forms' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A href="/payroll/forms">
                            &#8592; Back to Government Forms
                        </A>
                    </Container>
                </NavWrapper>
                {
                    ( isLoading || isGenerating )
                        ? this.renderLoadingPage( isGenerating )
                        : this.renderForm()
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    data: makeSelectData(),
    pageStatus: makeSelectPageStatus(),
    downloadUrl: makeSelectDownloadUrl(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        hdmfFormActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( HDMFSTLRFForm );
