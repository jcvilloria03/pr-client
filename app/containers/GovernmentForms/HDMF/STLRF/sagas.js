import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeLatest } from 'redux-saga';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';
import { Fetch } from 'utils/request';

import {
    INITIAL_DATA,
    GENERATE_FORMS,
    SET_DATA,
    GET_FORM_URI,
    NOTIFICATION,
    SAVE_LINE_ITEM,
    SET_URI,
    SAVE_FORM_DETAILS
} from './constants';

import {
    setNotification,
    setPageStatus
} from './actions';
import { PAGE_STATUSES } from '../../constants';

/**
 * Initialize data for Government Forms:STLRF page
 */
export function* initializeData({ payload }) {
    try {
        yield [
            put( setPageStatus( PAGE_STATUSES.LOADING ) ),
            call( getData, payload )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Get form uri
 */
export function* getFileLink({ payload }) {
    try {
        const response = yield call( Fetch, `/download/govt_form/${payload.id}`, { method: 'GET' });
        yield put({
            type: SET_URI,
            payload: response.uri
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * get prefill data from api
 */
export function* getData( payload ) {
    try {
        yield put( setPageStatus( PAGE_STATUSES.LOADING ) );

        const response = yield call( Fetch, `/government_forms/${payload.id}`, { method: 'GET' });
        yield put({
            type: SET_DATA,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Update line item
 */
export function* saveLineItem({ payload }) {
    const { lineItem } = payload;
    const data = {
        data: {
            kind: 'govt_form_hdmf_stlrf',
            company_id: payload.company_id,
            attributes: {
                line_items: [lineItem]
            }
        }
    };

    try {
        yield put( setPageStatus( PAGE_STATUSES.SAVING ) );

        yield call( Fetch, `/government_forms/${payload.id}`, { method: 'PUT', data });
        const response = yield call( Fetch, `/government_forms/${payload.id}`, { method: 'GET' });

        yield [
            call( notifyUser, {
                payload: {
                    title: 'Success',
                    message: 'Employee entry successfully updated',
                    show: true,
                    type: 'success'
                }
            }),
            put({
                type: SET_DATA,
                payload: response.data
            })
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}
/**
 * Generate and download HDMF MSRF forms
 */
export function* generateHDMFForms({ payload }) {
    try {
        const jsonAPIPayload = {
            data: {
                kind: 'HDMF_STLRF',
                company_id: payload.company_id,
                month_from: payload.month_from,
                year_from: payload.year_from,
                month_to: payload.month_from,
                year_to: payload.year_from
            }
        };
        yield put( setPageStatus( PAGE_STATUSES.GENERATING ) );

        const response = yield call( Fetch, '/government_forms', { method: 'POST', data: jsonAPIPayload });

        yield put({
            type: SET_DATA,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, { payload });
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser({ payload }) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 2000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Save Form Header
 *
 */
export function* saveFormHeader({ payload }) {
    const { id, ...rest } = payload;

    try {
        yield put( setPageStatus( PAGE_STATUSES.SAVING ) );

        const dataPayload = {
            data: {
                kind: 'govt_form_hdmf_stlrf',
                attributes: {
                    ...rest
                }
            }
        };
        yield call( Fetch, `/government_forms/${id}`, { method: 'PUT', data: dataPayload });
        yield call( getFileLink, { payload: { id }});
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watcher for INITIAL_DATA
 *
 */
export function* watchForInitialData() {
    const watcher = yield takeLatest( INITIAL_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Generate Forms
 *
 */
export function* watchForGenerateHDMFForms() {
    const watcher = yield takeLatest( GENERATE_FORMS, generateHDMFForms );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for save line item
 */
export function* watchForSaveLineItem() {
    const watcher = yield takeLatest( SAVE_LINE_ITEM, saveLineItem );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeLatest( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for get file link
 */
export function* watchForGetFileLink() {
    const watcher = yield takeLatest( GET_FORM_URI, getFileLink );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for saveFormHeader
 */
export function* watchForSaveFormHeader() {
    const watcher = yield takeLatest( SAVE_FORM_DETAILS, saveFormHeader );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitialData,
    watchForGenerateHDMFForms,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForSaveLineItem,
    watchForGetFileLink,
    watchForSaveFormHeader
];
