/*
 *
 * HDMF STLRF Form constants
 *
 */

const namespace = 'app/GovernmentForms/HDMF/STLRF';
export const INITIAL_DATA = `${namespace}/INITIAL_DATA`;

export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;

export const SET_FORM_OPTIONS = `${namespace}/SET_FORM_OPTIONS`;
export const SET_DATA = `${namespace}/SET_DATA`;
export const GET_FORM_URI = `${namespace}/GET_FORM_URI`;

export const GENERATE_FORMS = `${namespace}/GENERATE_FORMS`;
export const SET_URI = `${namespace}/SET_URI`;

export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const SAVE_LINE_ITEM = `${namespace}/SAVE_LINE_ITEM`;
export const SAVE_FORM_DETAILS = `${namespace}/SAVE_FORM_DETAILS`;

export const STRLF_LOAN_TYPES = [
    { value: 'MULTI_PURPOSE', label: 'Multi Purpose' },
    { value: 'CALAMITY', label: 'Calamity' },
    { value: 'HOUSING', label: 'Housing' },
    { value: 'SHORT_TERM', label: 'Short Term' }
];

export const BRANCH_CODES = [
    { value: '01', label: 'Makati I' },
    { value: '02', label: 'Makati II' },
    { value: '03', label: 'Imus' },
    { value: '04', label: 'Pasay' },
    { value: '05', label: 'Manila' },
    { value: '06', label: 'Caloocan' },
    { value: '07', label: 'Kamias' },
    { value: '08', label: 'Cubao' },
    { value: '09', label: 'Ortigas' },
    { value: '10', label: 'Makati III' },
    { value: '11', label: 'Provident Operations, International Operations' },
    { value: '15', label: 'Housing South' },
    { value: '16', label: 'Housing North' },
    { value: '17', label: 'Antipolo' },
    { value: '21', label: 'CHQ' },
    { value: '28', label: 'La Union' },
    { value: '29', label: 'Baguio' },
    { value: '30', label: 'Dagupan' },
    { value: '31', label: 'Vigan' },
    { value: '32', label: 'Ilocos Norte' },
    { value: '34', label: 'Tuguegarao' },
    { value: '35', label: 'Isabela' },
    { value: '36', label: 'Nueva Vizcaya' },
    { value: '39', label: 'Tarlac' },
    { value: '40', label: 'San Fernando' },
    { value: '41', label: 'Malolos' },
    { value: '42', label: 'Dinalupihan' },
    { value: '44', label: 'Cabanatuan' },
    { value: '46', label: 'Lucena' },
    { value: '47', label: 'Calamba' },
    { value: '49', label: 'Batangas' },
    { value: '50', label: 'Palawan' },
    { value: '52', label: 'Legaspi' },
    { value: '53', label: 'Naga' },
    { value: '54', label: 'Calapan' },
    { value: '58', label: 'Iloilo' },
    { value: '59', label: 'Bacolod' },
    { value: '60', label: 'Kalibo' },
    { value: '62', label: 'Capiz' },
    { value: '64', label: 'Cebu City' },
    { value: '65', label: 'Tagbilaran' },
    { value: '66', label: 'Dumaguete' },
    { value: '67', label: 'Mandaue' },
    { value: '70', label: 'Tacloban' },
    { value: '75', label: 'Catbalogan' },
    { value: '76', label: 'Zamboanga' },
    { value: '77', label: 'Pagadian' },
    { value: '80', label: 'Dipolog' },
    { value: '81', label: 'Bukidnon' },
    { value: '82', label: 'Cagayan de oro' },
    { value: '83', label: 'Butuan' },
    { value: '84', label: 'Surigao' },
    { value: '85', label: 'Koronadal' },
    { value: '88', label: 'Davao' },
    { value: '89', label: 'General Santos' },
    { value: '90', label: 'Tagum' },
    { value: '94', label: 'Cotabato' },
    { value: '95', label: 'Iligan' }
];

export const REQUIRED_FIELDS = {
    INPUTS: [ 'employer_address', 'employer_hdmf_number', 'employer_name', 'employer_city', 'total_loan_amount' ],
    SELECTS: ['hdmf_branch_code'],
    DATE_PICKERS: ['formDate']
};
