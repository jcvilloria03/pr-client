import styled, { keyframes } from 'styled-components';

export const GovtFormWrapper = styled.div`
    .NoDataFound {
        display: flex;
        justify-content: center;

        span {
            background: rgba(255,255,255, 0.8);
            padding: 10px 40px;
            border-radius: 4px;
            box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
        }
    }
    
    .msrf-directions-row {
        display: flex
    }

    .gov-form-options {
        display: flex;
        justify-content: flex-end;
        vertical-align: bottom;
    }

    .buttonbox {
        align-self: flex-end;
    }

    .currency input {
        text-align: right;
    }
    
    input {
        text-transform: uppercase;
    }

    .form-title {
        align-self: center;
        font-weight: bold;
    }
    padding-bottom: 50px;

    .company-section {
        margin: 0 auto;
        background: #fff;
        padding: 60px 80px;
        box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);

        & > h3 {
            font-weight: 600;
        }

        .Select + label {
            font-size: 14px;
            margin-bottom: 4px;
            font-weight: 400;
        }

        .DayPickerInput, .DayPickerInput input {
            width: 100%;
        }

        input:disabled, .Select.is-disabled .Select-value-label, input:disabled~label, .Select.is-disabled~label {
            color: #5b5b5b !important;
        }
    }

    .contribution-list {
        width: 100%;
        margin: 50px auto 150px auto;

        .heading {
            display: flex;
            justify-content: space-between;
            margin-bottom: 30px;

            .text-search {
                width: 180px;
                > p {
                    display: none;
                }
            }
        }

        & > h3 {
            font-weight: 600;
        }

        .DayPickerInput, .DayPickerInput input {
            width: 100%;
        }

        .actionWrapper {
            margin: 20px 0;
            display: flex;
            justify-content: space-between;
        }

        .rt-td:last-of-type > div {
            margin-bottom: 0;
            width: 100%;
        }

        .ReactTable {
            .rt-noData {
                background-color: rgba(0,0,0,0.9);
                color: #fff;
            }

            .rt-td {
                overflow: unset;
            }

            .Select {
                margin-top: 12px;
                font-size: 16px;
                font-weight: 400;

                .Select-control {
                    .Select-input {
                        height: 45px;
                    }
                    
                    .Select-value {
                        line-height: 45px;
                    }
                }
            }
        }
    }

    
`;

export const MessageWrapperStyles = styled.div`
    display: flex;
    justify-content: center;
    height: 100vh;

    > div {
        display: flex;
        flex-direction: column;
        justify-content: center;

        i {
            font-size: 7em;
            align-self: center;
        }

        .description {
            padding-top: 40px;
            text-align: center;

            h3 {
                font-weight: 600;
            }
        }
    }
`;

const bounce = keyframes`
    0%, 20%, 50%, 80%, 100% {
        transform: translate3d(0, 0, 0);
    }
    40% {
        transform: translate3d(0, -30px, 0);
    }
    60% {
        transform: translate3d(0, -15px, 0);
    }
`;

export const ContentWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;

    .downloadWrapper {
        display: flex;
        flex-direction: column;
        justify-content: center;

        .svg-wrapper {
            align-self: center;

            > svg {
                width: 160px;
                height: auto;
            }

            &.animate > svg {
                animation: ${bounce} 2s infinite;
            }
        }

        .description {
            padding-top: 40px;
            text-align: center;

            a:not([href]):not([tabindex]) {
                color: #00A5E5;
            }
            h3 {
                font-weight: 600;
            }
            #downloadLink {
                display: none;
            }
        }
    }
`;

export const ContributionListHeader = styled.div`
    display: flex;
    align-items: center;
    #pager {
        flex-grow: 1;
        justify-content: flex-end;
        flex-wrap: nowrap;
        white-space: nowrap;
    }
`;
export const SearchHeader = styled.div`
    display: flex;
    align-items: center;
    flex-direction: row;
    width: 100%;
    .title h5 {
        font-weight: 600;
    }
    div {
        padding: 5px;
    }
`;

export const FormTitle = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    h3 {
        font-weight: 400;
        margin: auto;
    }
    padding-bottom: 20px;
`;

export const InputWrapper = styled.div`
    padding-bottom: 10px;
`;

export const AllCaps = styled.div`
    text-transform: uppercase;
`;
