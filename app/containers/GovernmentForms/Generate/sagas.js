import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    INITIAL_DATA,
    SET_COMPANY_DATA,
    SET_PERIODS,
    SET_PAYMENT_METHODS,
    SET_LOADING,
    SSS,
    PAG_IBIG,
    BIR,
    PHIL_HEALTH,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    SET_LOAN_PERIODS,
    GENERATE_GOVERNMENT_FORM,
    SET_AVAILABLE_YEARS
} from './constants';

/**
 * Initialize data for payslip page
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const { agency } = payload;

        const companyId = company.getLastActiveCompanyId();
        yield put({
            type: SET_COMPANY_DATA,
            payload: companyId
        });

        yield [
            call( getPeriods, companyId ),
            call( getAvailableYears, companyId ),
            call( getLoanCollectionPeriods, companyId, agency ),
            call( getPaymentMethods, agency )
        ];
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Gets government form periods
 */
export function* getPeriods( companyId ) {
    try {
        yield put({
            type: SET_PERIODS,
            payload: []
        });
        const response = yield call( Fetch, `/philippine/company/${companyId}/government_form_periods`, { method: 'GET' });
        yield put({
            type: SET_PERIODS,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Gets BIR 1604C available years
 */
export function* getAvailableYears( companyId ) {
    try {
        const { data } = yield call( Fetch, '/government_forms/1604c/available_years', {
            method: 'GET',
            params: {
                company_id: companyId
            }
        });

        yield put({
            type: SET_AVAILABLE_YEARS,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Gets government form loan collection periods
 */
export function* getLoanCollectionPeriods( companyId, agency ) {
    const type = agency === 'SSS' ? 'SSS' : 'Pag-ibig';

    try {
        const response = yield call( Fetch, `/company/${companyId}/months_with_payroll_loans/${type}`, { method: 'GET' });

        const loanPeriods = response.map( ( period ) => {
            const dateTokens = period.split( ' ' );
            const monthToken = dateTokens[ 0 ];
            const yearToken = dateTokens[ 1 ];
            const month = new Date( `${monthToken} 01 ${yearToken}` ).getMonth() + 1;
            const year = parseInt( yearToken, 10 );

            return ({
                month,
                year,
                label: period
            });
        });

        yield put({
            type: SET_LOAN_PERIODS,
            payload: loanPeriods
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Gets payment methods
 */
export function* getPaymentMethods( agency ) {
    try {
        const paymentMethods = [];
        yield put({
            type: SET_PAYMENT_METHODS,
            payload: paymentMethods
        });

        switch ( agency ) {
            case SSS:
            case PAG_IBIG:
            case BIR:
            case PHIL_HEALTH:
            default:
                break;
        }

        yield put({
            type: SET_PAYMENT_METHODS,
            payload: paymentMethods
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Sends request to generate government for from the config provided
 * @param {Integer} payload.company_id - Company ID
 * @param {String} payload.kind - Government form to generate
 * @param {String} [payload.month_from]
 * @param {String} [payload.year_from]
 * @param {String} [payload.month_to]
 * @param {String} [payload.year_to]
 * @param {string} [payload.year]
 */
export function* generateGovernmentForm({ payload }) {
    const { agency_url: agencyUrl, ...restOfPayload } = payload;

    try {
        const { data } = yield call( Fetch, '/government_forms', {
            method: 'POST',
            data: {
                data: {
                    ...restOfPayload
                }
            }
        });

        yield call( browserHistory.push, `/forms/${agencyUrl}/${data.id}` );
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watcher for INITIAL_DATA
 *
 */
export function* watchForInitialData() {
    const watcher = yield takeEvery( INITIAL_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GENERATE_GOVERNMENT_FORM
 */
export function* watchForGenerateGovernmentForm() {
    const watcher = yield takeEvery( GENERATE_GOVERNMENT_FORM, generateGovernmentForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitialData,
    watchForNotifyUser,
    watchForGenerateGovernmentForm,
    watchForReinitializePage
];
