import { RESET_STORE } from 'containers/App/constants';

import {
    INITIAL_DATA,
    GENERATE_GOVERNMENT_FORM
} from './constants';

/**
 * Fetch initial data from server
 */
export function initializeData( companyId, agency ) {
    return {
        type: INITIAL_DATA,
        payload: {
            companyId,
            agency
        }
    };
}

/**
 * Sends request to generate government for from the config provided
 * @param {Integer} companyId - Company ID
 * @param {String} kind - Government form to generate
 * @param {String} [period.month_from]
 * @param {String} [period.year_from]
 * @param {String} [period.month_to]
 * @param {String} [period.year_to]
 * @param {string} [period.year]
 * @param {String} agencyUrl - Government form to generate
 * @returns {Object}
 */
export function generateGovernmentForm( companyId, kind, period, agencyUrl ) {
    return {
        type: GENERATE_GOVERNMENT_FORM,
        payload: {
            company_id: companyId,
            agency_url: agencyUrl,
            kind,
            ...period
        }
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
