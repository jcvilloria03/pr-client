import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_COMPANY_DATA,
    SET_LOADING,
    SET_PERIODS,
    SET_PAYMENT_METHODS,
    NOTIFICATION_SAGA,
    SET_LOAN_PERIODS,
    SET_AVAILABLE_YEARS
} from './constants';

const initialState = fromJS({
    loading: false,
    company_id: null,
    periods: [],
    available_years: [],
    loanPeriods: [],
    payment_methods: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 * modifies passed periods data to match client structure
 * @param periodList = list of period returned by API
 * @returns {array} = formatted list of periods
 */
function preparePeriodsData( periodList ) {
    let formattedData = [];
    if ( periodList ) {
        formattedData = periodList.map( ( period ) => {
            const {
                month,
                year,
                label
            } = period;
            return {
                value: JSON.stringify({ month, year }),
                label
            };
        });
    }
    return fromJS( formattedData );
}

/**
 * Prepares data to match Select options structure
 * @param {string[]} years - List of years
 * @returns {object[]}
 */
function prepareAvailableYearsData( years ) {
    return years.map( ( year ) => ({
        label: `${year}`,
        value: `${year}`
    }) );
}

/**
 *
 * Forms reducer
 *
 */
function formsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_PERIODS:
            return state.set( 'periods', preparePeriodsData( action.payload ) );
        case SET_AVAILABLE_YEARS:
            return state.set( 'available_years', fromJS( prepareAvailableYearsData( action.payload ) ) );
        case SET_LOAN_PERIODS:
            return state.set( 'loanPeriods', preparePeriodsData( action.payload ) );
        case SET_PAYMENT_METHODS:
            return state.set( 'payment_methods', fromJS( action.payload ) );
        case SET_COMPANY_DATA:
            return state.set( 'company_id', action.payload );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default formsReducer;
