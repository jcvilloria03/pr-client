import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';
import { Container } from 'reactstrap';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';
import SnackBar from 'components/SnackBar';
import A from 'components/A';

import { Wrapper, NavWrapper } from './styles';
import * as viewActions from './actions';
import PageWrapper from '../PageWrapper';
import PeriodPaymentView from '../templates/periodPayment';
import AgencyView from '../templates/agency';

import {
    GOVERNMENT_FORM_SETTING,
    SSS,
    PAG_IBIG,
    PHIL_HEALTH,
    BIR,
    BIR_FORMS,
    HDMF_FORMS,
    SSS_FORMS,
    GOVERNMENT_FORM_KINDS
} from './constants';

import {
    makeSelectLoading,
    makeSelectCompanyId,
    makeSelectPeriods,
    makeSelectAvailableYears,
    makeSelectPaymentMethods,
    makeSelectNotification,
    makeSelectLoanPeriods
} from './selectors';

/**
 * GovernmentForms component
 */
export class GovernmentForms extends React.Component {
    static propTypes = {
        initializeData: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        loading: React.PropTypes.bool,
        companyId: React.PropTypes.oneOfType([
            React.PropTypes.number,
            React.PropTypes.object
        ]),
        periods: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                value: React.PropTypes.string,
                label: React.PropTypes.string
            })
        ),
        availableYears: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                value: React.PropTypes.string,
                label: React.PropTypes.string
            })
        ),
        loanPeriods: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                value: React.PropTypes.string,
                label: React.PropTypes.string
            })
        ),
        paymentMethods: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                label: React.PropTypes.string,
                code: React.PropTypes.string,
                channels: React.PropTypes.arrayOf(
                    React.PropTypes.shape({
                        label: React.PropTypes.string,
                        code: React.PropTypes.string,
                        method: React.PropTypes.string
                    })
                )
            })
        ),
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        generateGovernmentForm: React.PropTypes.func
    }

    /**
     * Component's constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            step: 0,
            agency: '',
            selection: {
                period: null,
                paymentMethod: '',
                channel: '',
                bir: null,
                hdmf: null,
                sss: null
            },
            birForms: Object.keys( BIR_FORMS ).map( ( key ) => ({
                value: key,
                label: BIR_FORMS[ key ]
            }) ),
            hdmfForms: Object.keys( HDMF_FORMS ).map( ( key ) => ({
                value: key,
                label: HDMF_FORMS[ key ]
            }) ),
            sssForms: Object.keys( SSS_FORMS ).map( ( key ) => ({
                value: key,
                label: SSS_FORMS[ key ]
            }) )
        };

        this.switchStep = this.switchStep.bind( this );
    }

    /**
     * Checks permission before rendering component
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.government_form'], ( authorized ) => { !authorized && browserHistory.replace( '/unauthorized' ); });

        const setting = localStorage.getItem( GOVERNMENT_FORM_SETTING );
        if ( setting ) { // switch to step 1 if there is data in localstorage
            const selection = JSON.parse( setting );

            this.switchStep( 1, selection.agency, {
                period: {
                    value: selection.date,
                    label: selection.date.month ? moment( `${selection.date.month}-01-${selection.date.year}`, 'MM-DD-YYYY' ).format( 'MMMM YYYY' ) : selection.date
                },
                paymentMethod: selection.payment ? selection.payment.type : '',
                channel: selection.payment ? selection.payment.channel : '',
                bir: selection.bir,
                hdmf: selection.hdmf,
                sss: selection.sss
            });
        }
    }

    /**
     * triggers when component is to be removed from DOM
     */
    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * Switches to a step with the selected agency and settings
     */
    switchStep( step, agency, selection ) {
        switch ( step ) {
            case 0: // goto select agency page
                this.setState({ step, agency, selection });
                break;
            case 1: // goto select period/payment page
                this.setState({ step, agency, selection }, () => {
                    this.props.initializeData( this.props.companyId, agency );
                });
                break;
            case 2: {
                // goto agency's form page
                const setting = {
                    agency,
                    company_id: this.props.companyId,
                    date: selection.period.value
                };

                let agencyUrlPart;
                let kind;
                switch ( agency ) {
                    case SSS:
                        setting.sss = selection.sss;
                        agencyUrlPart = `SSS/${selection.sss.value}`;
                        kind = `SSS_${selection.sss.value}`;
                        break;
                    case PAG_IBIG:
                        setting.payment = {
                            type: selection.paymentMethod,
                            channel: selection.channel
                        };
                        setting.hdmf = selection.hdmf;
                        agencyUrlPart = `HDMF/${selection.hdmf.value}`;
                        kind = `HDMF_${selection.hdmf.value}`;
                        break;
                    case PHIL_HEALTH:
                        agencyUrlPart = 'PhilHealth/MCL';
                        kind = GOVERNMENT_FORM_KINDS.PHILHEALTH_MCL;
                        break;
                    case BIR:
                        setting.bir = selection.bir;
                        agencyUrlPart = `BIR/${selection.bir.value}`;
                        kind = selection.bir.value;
                        break;
                    default:
                        break;
                }

                const date = typeof setting.date === 'string'
                    ? JSON.parse( setting.date )
                    : setting.date;

                localStorage.setItem(
                    GOVERNMENT_FORM_SETTING,
                    JSON.stringify({
                        ...setting,
                        date
                    })
                );

                const period = kind === GOVERNMENT_FORM_KINDS.BIR_1604C
                    ? { year: date }
                    : {
                        month_from: date.month,
                        year_from: date.year,
                        month_to: date.month,
                        year_to: date.year
                    };
                this.props.generateGovernmentForm( this.props.companyId, kind, period, agencyUrlPart );
                break;
            }
            default:
                break;
        }
    }

    /**
     * renders component to DOM
     */
    render() {
        return (
            <Wrapper>
                <NavWrapper>
                    <Container>
                        <A href="/payroll/forms">
                            &#8592; Back to Government Forms
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <Helmet
                        title="Government Forms"
                        meta={ [
                            { name: 'description', content: 'Description of Government Forms' }
                        ] }
                    />
                    <SnackBar
                        message={ this.props.notification.message }
                        title={ this.props.notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ this.props.notification.show }
                        delay={ 5000 }
                        type={ this.props.notification.type }
                    />
                    <PageWrapper step={ this.state.step }>
                        {
                            this.state.step === 1
                                ? <PeriodPaymentView
                                    loading={ this.props.loading }
                                    periods={ this.props.periods }
                                    availableYears={ this.props.availableYears }
                                    loanPeriods={ this.props.loanPeriods }
                                    paymentMethods={ this.props.paymentMethods }
                                    birForms={ this.state.birForms }
                                    hdmfForms={ this.state.hdmfForms }
                                    sssForms={ this.state.sssForms }
                                    switchStep={ this.switchStep }
                                    agency={ this.state.agency }
                                    selection={ this.state.selection }
                                />
                                : <AgencyView
                                    selectAgency={ ( agency ) => {
                                        let selection = this.state.selection;
                                        if ( agency !== this.state.agency ) {
                                            selection = { period: null, paymentMethod: '', channel: '' };
                                            GOVERNMENT_FORM_SETTING in localStorage && localStorage.removeItem( GOVERNMENT_FORM_SETTING );
                                        }
                                        this.switchStep( 1, agency, selection );
                                    } }
                                />
                        }
                    </PageWrapper>
                </Container>
            </Wrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    companyId: makeSelectCompanyId(),
    periods: makeSelectPeriods(),
    availableYears: makeSelectAvailableYears(),
    paymentMethods: makeSelectPaymentMethods(),
    notification: makeSelectNotification(),
    loanPeriods: makeSelectLoanPeriods()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        viewActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( GovernmentForms );
