/*
 *
 * Forms constants
 *
 */

export const FRONT_END_ASSETS_URL = 'https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev';

const namespace = 'app/containers/GovernmentForms/Generate';
export const INITIAL_DATA = `${namespace}/INITIAL_DATA`;

export const SET_COMPANY_DATA = `${namespace}/SET_COMPANY_DATA`;
export const SET_PERIODS = `${namespace}/SET_PERIODS`;
export const SET_AVAILABLE_YEARS = `${namespace}/SET_AVAILABLE_YEARS`;
export const SET_PAYMENT_METHODS = `${namespace}/SET_PAYMENT_METHODS`;
export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_LOAN_PERIODS = `${namespace}/SET_LOAN_PERIODS`;

export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const GENERATE_GOVERNMENT_FORM = `${namespace}/GENERATE_GOVERNMENT_FORM`;

export const GOVERNMENT_FORM_SETTING = 'government-form-setting';
export const SSS = 'SSS';
export const PAG_IBIG = 'Pag-IBIG';
export const BIR = 'BIR';
export const PHIL_HEALTH = 'PhilHealth';
export const BPI = 'BPI';
export const SBC = 'SBC';

export const BIR_FORMS = {
    '1601C': 'BIR 1601C Form',
    '1604C': 'BIR 1604C Form'
};

export const HDMF_FORMS = {
    MSRF: 'Pag-IBIG MSRF',
    STLRF: 'Pag-IBIG STLRF'
};

export const SSS_FORMS = {
    REMITTANCE: 'SSS Monthly Contribution R3 and R5 Forms',
    LCL: 'SSS Loan Collection Report'
};

export const GOVERNMENT_FORM_KINDS = {
    BIR_1601C: '1601C',
    BIR_1604C: '1604C',
    PHILHEALTH_MCL: 'PHILHEALTH_MCL',
    SSS_LCL: 'SSS_LCL',
    SSS_REMITTANCE: 'SSS_REMITTANCE',
    HDMF_STLRF: 'HDMF_STLRF',
    HDMF_MSRF: 'HDMF_MSRF'
};

export const GOVERNMENT_AGENCIES = [
    {
        code: BIR,
        name: 'Bureau of Internal Revenue',
        logo: `${FRONT_END_ASSETS_URL}/branding/government-agencies/bir.png`
    },
    {
        code: PAG_IBIG,
        name: 'Pag-IBIG',
        logo: `${FRONT_END_ASSETS_URL}/branding/government-agencies/pagibig.png`
    },
    {
        code: PHIL_HEALTH,
        name: 'PhilHealth',
        logo: `${FRONT_END_ASSETS_URL}/branding/government-agencies/philhealth.jpg`
    },
    {
        code: SSS,
        name: 'Social Security System',
        logo: `${FRONT_END_ASSETS_URL}/branding/government-agencies/sss.png`
    }
];
