import styled from 'styled-components';

export const Wrapper = styled.div`
`;

export const PageWrapperStyles = styled.div`
    &.hidden {
        display: none;
    }
`;

export const HeaderStyles = styled.div`
    text-align: center;

    & > div {
        text-align: left;
    }

    h4 {
        font-weight: 600;
    }
`;

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;
