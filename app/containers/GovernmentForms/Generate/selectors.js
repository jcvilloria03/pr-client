import { createSelector } from 'reselect';

/**
 * Direct selector to the government forms state domain
 */
const selectFormsDomain = () => ( state ) => state.get( 'government_forms_generate' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
    selectFormsDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectCompanyId = () => createSelector(
    selectFormsDomain(),
    ( substate ) => substate.get( 'company_id' )
);

const makeSelectPeriods = () => createSelector(
    selectFormsDomain(),
    ( substate ) => substate.get( 'periods' ).toJS()
);

const makeSelectAvailableYears = () => createSelector(
    selectFormsDomain(),
    ( substate ) => substate.get( 'available_years' ).toJS()
);

const makeSelectPaymentMethods = () => createSelector(
    selectFormsDomain(),
    ( substate ) => substate.get( 'payment_methods' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectFormsDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLoanPeriods = () => createSelector(
    selectFormsDomain(),
    ( substate ) => substate.get( 'loanPeriods' ).toJS()
);
export {
    selectFormsDomain,
    makeSelectLoading,
    makeSelectCompanyId,
    makeSelectPeriods,
    makeSelectAvailableYears,
    makeSelectPaymentMethods,
    makeSelectNotification,
    makeSelectLoanPeriods
};
