import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_DATA,
    SET_FORM_OPTIONS,
    SET_GENERATING,
    DOWNLOAD_READY,
    SET_URI,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../../App/constants';

const initialState = fromJS({
    data: {
        any_taxes_withheld: 1,
        tin: '',
        rdo_code: '',
        withholding_agent_name: '',
        registered_address: '',
        zip_code: '',
        category_of_withholding_agent: '',
        computation_of_tax: {
            nature_of_income_payment: '',
            atc: '',
            tax_base: 0,
            tax_rate: 0,
            tax_required_to_be_withheld: 0
        }
    },
    options: {
        tax_relief: [],
        withholding_agent: [],
        rdo_code: []
    },
    status: {
        loading: true,
        generating: false,
        downloadReady: false
    },
    downloadUrl: '',
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 * transforms data from API to accomodate structure in app
 */
function prepareFormData( data ) {
    const formattedData = {};
    Object.keys( data ).forEach( ( key ) => {
        if ( key === 'computation_of_tax' ) {
            const { computation_of_tax } = data;
            const formattedTax = {};
            Object.keys( computation_of_tax[ 0 ]).forEach( ( taxKey ) => {
                formattedTax[ taxKey ] = computation_of_tax[ 0 ][ taxKey ];
            });
            formattedData[ key ] = formattedTax;
        } else {
            formattedData[ key ] = data[ key ];
        }
    });

    return formattedData;
}

/**
 * transforms form options data from API to accomodate structure in app
 */
function prepareFormOptions( data ) {
    const response = {};
    const keys = Object.keys( data );
    keys.forEach( ( key ) => {
        const formattedOption = [];
        data[ key ].forEach( ({ label, code }) => {
            formattedOption.push({
                label,
                value: code.toString()
            });
        });
        response[ key ] = formattedOption;
    });

    return response;
}

/**
 * BIR1601FFormsReducer
 */
function BIR1601FFormsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_FORM_OPTIONS:
            return state.set( 'options', fromJS( prepareFormOptions( action.payload ) ) );
        case SET_DATA:
            return state.set( 'data', fromJS( prepareFormData( action.payload ) ) );
        case DOWNLOAD_READY:
            return state.setIn([ 'status', 'downloadReady' ], action.payload );
        case SET_GENERATING:
            return state.setIn([ 'status', 'generating' ], action.payload );
        case SET_LOADING:
            return state.setIn([ 'status', 'loading' ], action.payload );
        case SET_URI:
            return state.set( 'downloadUrl', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default BIR1601FFormsReducer;
