/*
 *
 * BIR 1601F Forms constants
 *
 */

export const INITIAL_DATA = 'app/GovernmentForms/BIR/1601F/INITIAL_DATA';
export const SET_LOADING = 'app/GovernmentForms/BIR/1601F/SET_LOADING';
export const SET_DATA = 'app/GovernmentForms/BIR/1601F/SET_DATA';
export const SET_FORM_OPTIONS = 'app/GovernmentForms/BIR/1601F/SET_FORM_OPTIONS';

export const GENERATE_FORMS = 'app/GovernmentForms/BIR/1601F/GENERATE_FORMS';
export const SET_GENERATING = 'app/GovernmentForms/BIR/1601F/SET_GENERATING';

export const DOWNLOAD_READY = 'app/GovernmentForms/BIR/1601F/DOWNLOAD_READY';
export const SET_URI = 'app/GovernmentForms/BIR/1601F/SET_URI';

export const NOTIFICATION_SAGA = 'app/GovernmentForms/BIR/1601F/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/GovernmentForms/BIR/1601F/NOTIFICATION';
