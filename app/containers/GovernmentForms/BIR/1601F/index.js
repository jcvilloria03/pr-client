import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { delay } from 'redux-saga';
import decimal from 'js-big-decimal';
import moment from 'moment';

import PageWrapper from '../../PageWrapper';
import SnackBar from '../../../../components/SnackBar';
import { H3 } from '../../../../components/Typography';
import Input from '../../../../components/Input';
import SalSelect from '../../../../components/Select';
import DatePicker from '../../../../components/DatePicker';
import Button from '../../../../components/Button';
import ToolTip from '../../../../components/ToolTip';
import RadioGroup from '../../../../components/RadioGroup';
import Radio from '../../../../components/Radio';
import A from '../../../../components/A';
import SalConfirm from '../../../../components/SalConfirm';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { isAuthorized } from '../../../../utils/Authorization';
import { subscriptionService } from '../../../../utils/SubscriptionService';

import * as birFormActions from './actions';
import {
    makeSelectData,
    makeSelectOptions,
    makeSelectStatus,
    makeSelectDownloadUrl,
    makeSelectNotification
} from './selectors';

import {
    MessageWrapperStyles,
    FormsWrapper,
    InlineInputWrapper,
    InlineDateWrapper,
    DownloadWrapper,
    ConfirmBodyWrapperStyle
} from './styles';

/**
 * BIR Form 1601F component
 */
export class BIR1601FForms extends React.PureComponent {
    static propTypes = {
        initializeData: React.PropTypes.func,
        generateForm: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        data: React.PropTypes.object,
        options: React.PropTypes.object,
        status: React.PropTypes.shape({
            loading: React.PropTypes.bool,
            generating: React.PropTypes.bool,
            downloadReady: React.PropTypes.bool
        }),
        downloadUrl: React.PropTypes.any,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    };

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            uri: '',
            showConfirmTaxWithheldNone: false
        };

        this.validatedData = null;
        this.generateForm = this.generateForm.bind( this );
        this.downloadFile = this.downloadFile.bind( this );
        this.changeTaxWitheld = this.changeTaxWitheld.bind( this );
        this.setInputValue = this.setInputValue.bind( this );
        this.updateTaxTotals = this.updateTaxTotals.bind( this );
        this.setInitialValues = this.setInitialValues.bind( this );
    }

    /**
     * runs before render.
     * checks if there is a valid data to generate or if user is authorized to view this page.
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.government_form'], ( authorized ) => {
            if ( !authorized ) {
                browserHistory.replace( '/unauthorized' );
            } else {
                const settings = localStorage.getItem( 'government-form-setting' );
                if ( settings ) {
                    this.props.initializeData( JSON.parse( settings ) );
                } else {
                    browserHistory.replace( '/forms' );
                }
            }
        });
    }

    /**
     * Perform actions when component props changes
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.downloadUrl && nextProps.downloadUrl.length
            && nextProps.downloadUrl !== this.props.downloadUrl
            && this.setState({ uri: nextProps.downloadUrl }, () => {
                this.downloadFile();
            });

        nextProps.data
            && nextProps.data.computation_of_tax !== this.props.data.computation_of_tax
            && this.setInitialValues( nextProps );
    }

    /**
     * Called before component unmounts.
     */
    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * Sets initial values for certain fields
     */
    setInitialValues( nextProps ) {
        const computationOfTax = nextProps.data.computation_of_tax || {};

        this.tax_c && this.setInputValue( this.tax_c, computationOfTax.tax_base );
        this.tax_e && this.setInputValue( this.tax_e, computationOfTax.tax_required_to_be_withheld );
        this[ '14' ] && this.setInputValue( this[ '14' ], computationOfTax.tax_required_to_be_withheld );
        this[ '16' ] && this.setInputValue( this[ '16' ], computationOfTax.tax_required_to_be_withheld );
        this[ '18' ] && this.setInputValue( this[ '18' ], computationOfTax.tax_required_to_be_withheld );
        this[ '20' ] && this.setInputValue( this[ '20' ], computationOfTax.tax_required_to_be_withheld );
    }

    /**
     * Sets and validates input vlaue
     */
    setInputValue( input, value, callback ) {
        if ( value && value.toString().length ) {
            const number = this.stripNonDigit( value );
            if ( isNaN( Number( number ) ) ) {
                input.setState({ value: number, error: true, errorMessage: 'Please enter a valid amount' }, callback );
            } else if ( Number( number ) < 0 ) {
                input.setState({ value: number, error: true, errorMessage: 'This field cannot have a negative value' }, callback );
            } else {
                const formattedValue = this.formatCurrency( number );
                if ( formattedValue.length > 25 ) {
                    input.setState({ value: formattedValue, error: true, errorMessage: 'This field cannot exceed 25 characters.' }, callback );
                } else {
                    input.setState({
                        value: formattedValue,
                        error: false,
                        errorMessage: ''
                    }, callback );
                }
            }
        } else {
            input.setState({ value: '' }, callback );
        }
    }

    /**
     * removes non-digit character to string except dot.
     * @param value
     */
    stripNonDigit( value ) {
        if ( value && value.toString().length ) {
            return value.toString().replace( /[^0-9.-]/g, '' );
        }
        return '';
    }

    /**
     * removes non-numeric character
     * @param value
     */
    stripNonNumeric( value ) {
        if ( value && value.toString().length ) {
            return value.toString().replace( /\D/g, '' );
        }
        return '';
    }

    /**
     * formats number into currency
     * @param value
     */
    formatCurrency( value ) {
        if ( value && String( value ).length ) {
            const number = this.stripNonDigit( value );

            if ( !isNaN( Number( number ) ) ) {
                return decimal.getPrettyValue( decimal.round( number, 2 ) );
            }
            return value;
        }
        return '';
    }

    /**
     * transform value into TIN number format
     * @param value
     */
    formatTIN( value ) {
        if ( value ) {
            const tin = value.replace( /\D/g, '' ).split( '-' ).join( '' ).substring( 0, 12 );
            const set = [];
            set[ 0 ] = tin.substr( 0, 3 );
            if ( tin.length > 3 ) set[ 1 ] = tin.substr( 3, 3 );
            if ( tin.length > 6 ) set[ 2 ] = tin.substr( 6, 3 );
            if ( tin.length > 9 ) set[ 3 ] = tin.substr( 9, 3 );
            return set.join( '-' );
        }
        return value;
    }

    /**
     * apply changes when any tax witheld is set to None
     */
    changeTaxWitheld( any ) {
        if ( !any ) {
            const { data } = this.props;

            this.tax_c.setState({ value: '0' });
            this.tax_d.setState({ value: data.computation_of_tax.tax_rate });
            this.tax_e.setState({ value: '0' });
            this[ '14' ].setState({ value: '0' });
            this[ '15' ].setState({ value: '0' });
            this[ '16' ].setState({ value: '0' });
            this[ '17' ].setState({ value: '0' });
            this[ '18' ].setState({ value: '0' });
            this[ '19A' ].setState({ value: '0' });
            this[ '19B' ].setState({ value: '0' });
            this[ '19C' ].setState({ value: '0' });
            this[ '19D' ].setState({ value: '0' });
            this[ '20' ].setState({ value: '0' });

            this.tax_c.setState({ error: false, errorMessage: '' });
        }
        this[ '4' ].setState({ checkedIndex: any ? 0 : 1 });

        this.tax_a.setState({ disabled: !any });
        this.tax_b.setState({ disabled: !any });
        this.tax_c.setState({ disabled: !any });
    }

    /**
     * Update tax totals based on inputs
     */
    updateTaxTotals() {
        const field14 = this.stripNonDigit( this[ '14' ].state.value );
        const field15 = this.stripNonDigit( this[ '15' ].state.value );
        const field17 = this.stripNonDigit( this[ '17' ].state.value );
        const field19D = this.stripNonDigit( this[ '19D' ].state.value );

        if ( !isNaN( Number( field14 ) )
            && !isNaN( Number( field15 ) )
            && !isNaN( Number( field17 ) )
            && !isNaN( Number( field19D ) ) ) {
            const field16 = decimal.add( field14, field15 );
            this.setInputValue( this[ '16' ], field16 );

            const field18 = decimal.subtract( field16, field17 );
            this.setInputValue( this[ '18' ], field18 );

            const field20 = decimal.add( field18, field19D );
            this.setInputValue( this[ '20' ], field20 );
        }
    }

    /**
     * Update total penalties based on inputs
     */
    updateTotalPenalties() {
        const field18 = this.stripNonDigit( this[ '18' ].state.value );
        const field19A = this.stripNonDigit( this[ '19A' ].state.value );
        const field19B = this.stripNonDigit( this[ '19B' ].state.value );
        const field19C = this.stripNonDigit( this[ '19C' ].state.value );

        if ( !isNaN( Number( field18 ) )
            && !isNaN( Number( field19A ) )
            && !isNaN( Number( field19B ) )
            && !isNaN( Number( field19C ) ) ) {
            const field19D = decimal.add( decimal.add( field19A, field19B ), field19C );
            this.setInputValue( this[ '19D' ], field19D );

            const field20 = decimal.add( field18, field19D );
            this.setInputValue( this[ '20' ], field20 );
        }
    }

    /**
     * validates a numeric field
     */
    validateNumberField( input ) {
        const value = input.state.value && this.stripNonDigit( input.state.value );

        if ( input.state.error ) {
            return false;
        } else if ( input.props.required && value.length <= 0 ) {
            input.setState({ error: true, errorMessage: 'This field is required.' });
            return false;
        } else if ( input.props.max && !input._checkMax( value ) ) {
            input.setState({ error: true, errorMessage: `This field cannot exceed ${input.props.max} characters` });
            return false;
        } else if ( isNaN( Number( value ) ) ) {
            input.setState({ error: true, errorMessage: 'Please enter a valid amount.' });
            return false;
        } else if ( Number( value ) < 0 ) {
            input.setState({ error: true, errorMessage: 'This field cannot have a negative value.' });
            return false;
        }
        return true;
    }

    /**
     * validates a TIN field
     */
    validateTIN( input ) {
        const value = input.state.value && this.stripNonNumeric( input.state.value );
        if ( !value || ( ![ 9, 12 ].includes( value.length ) ) ) {
            input.setState({ error: true, errorMessage: 'TIN number must either be 9 or 12 digits' });
            return false;
        }
        input.setState({ error: false });
        return true;
    }

    /**
     * Validate date input
     */
    validateDate( input ) {
        if ( input.state.selectedDay === undefined || input.state.selectedDay === null || !input.state.selectedDay ) {
            input.setState({ error: true, message: 'This field is required' });
            return false;
        }
        return true;
    }

    /**
     * Validate form inputs for BIR 1601C Form generation
     */
    validateForm() {
        let valid = true;

        if ( this[ '3' ]._validate( this[ '3' ].state.value ) ) {
            valid = false;
        }

        if ( this[ '4' ].state.checkedIndex === 0 ) {
            if ( !this.validateNumberField( this.tax_c ) ) {
                valid = false;
            } else if ( Number( this.stripNonDigit( this.tax_c.state.value ) ) <= 0 ) {
                this.tax_c.setState({ error: true, errorMessage: 'This field requires a valid number greater than zero(0).' });
                valid = false;
            }
        } else {
            this.tax_c.setState({ error: false, errorMessage: '' });
        }

        if ( !this[ '6' ]._checkRequire( this[ '6' ].state.value ) ) {
            valid = false;
        }

        if ( this[ '7' ]._validate( this[ '7' ].state.value ) ) {
            valid = false;
        }

        if ( this[ '8' ]._validate( this[ '8' ].state.value ) ) {
            valid = false;
        }

        if ( this[ '9' ]._validate( this[ '9' ].state.value ) ) {
            valid = false;
        }

        if ( this[ '10' ]._validate( this[ '10' ].state.value ) ) {
            valid = false;
        }

        if ( this[ '11' ]._validate( this[ '11' ].state.value ) ) {
            valid = false;
        }

        if ( this[ '13' ].state.checkedIndex === 0 ) {
            if ( !this[ '13.1' ]._checkRequire( this[ '13.1' ].state.value ) ) {
                valid = false;
            }
        }

        if ( !this.validateNumberField( this[ '15' ]) ) {
            valid = false;
        } else if ( this[ '13' ].state.checkedIndex === 0 && Number( this[ '15' ].state.value ) === 0 ) {
            this[ '15' ].setState({ error: true, errorMessage: 'This field requires a valid number greater than zero(0).' });
            valid = false;
        }

        if ( this[ '2' ].state.checkedIndex === 0 ) {
            if ( !this.validateNumberField( this[ '17' ]) ) {
                valid = false;
            } else if ( Number( this[ '17' ].state.value ) === 0 ) {
                this[ '17' ].setState({ error: true, errorMessage: 'This field requires a valid number greater than zero(0).' });
                valid = false;
            }
        } else {
            this[ '17' ].state.error && this[ '17' ].setState({ error: false });
        }

        if ( !this.validateNumberField( this[ '19A' ]) ) {
            valid = false;
        }

        if ( !this.validateNumberField( this[ '19B' ]) ) {
            valid = false;
        }

        if ( !this.validateNumberField( this[ '19C' ]) ) {
            valid = false;
        }

        if ( !this.validateNumberField( this[ '20' ]) ) {
            valid = false;
        }

        if ( this[ '21A' ].state.value || this[ '21B' ].state.value || this[ '21C' ].state.value ) {
            if ( !this[ '21A' ].state.value ) {
                this[ '21A' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            }

            if ( !this[ '21B' ].state.value ) {
                this[ '21B' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            }

            if ( !this[ '21C' ].state.value ) {
                this[ '21C' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            } else if ( !this.validateTIN( this[ '21C' ]) ) {
                valid = false;
            }
        }

        if ( this[ '22A' ].state.value || this[ '22B' ].state.value || this[ '22C' ].state.value ) {
            if ( !this[ '22A' ].state.value ) {
                this[ '22A' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            }

            if ( !this[ '22B' ].state.value ) {
                this[ '22B' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            }

            if ( !this[ '22C' ].state.value ) {
                this[ '22C' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            } else if ( !this.validateTIN( this[ '22C' ]) ) {
                valid = false;
            }
        }

        if ( this.tax_agent_account_number.state.value || this.date_of_issuance.state.selectedDay || this.date_of_expiry.state.selectedDay ) {
            if ( !this.tax_agent_account_number.state.value ) {
                this.tax_agent_account_number.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            }

            if ( !this.date_of_issuance.state.selectedDay ) {
                this.date_of_issuance.setState({ error: true, message: 'This field is required if one of fields in the row has values.' });
                valid = false;
            } else if ( !this.validateDate( this.date_of_issuance ) ) {
                this.date_of_issuance.setState({ error: true, message: 'Invalid Date' });
                valid = false;
            }

            if ( !this.date_of_expiry.state.selectedDay ) {
                this.date_of_expiry.setState({ error: true, message: 'This field is required if one of fields in the row has values.' });
                valid = false;
            } else if ( !this.validateDate( this.date_of_expiry ) ) {
                this.date_of_expiry.setState({ error: true, message: 'Invalid Date' });
                valid = false;
            }
        }

        if ( this[ '23A' ].state.value || this[ '23B' ].state.value || this[ '23C' ].state.selectedDay || this.hasPaymentAmount( this[ '23D' ].state.value ) ) {
            if ( !this[ '23A' ].state.value ) {
                this[ '23A' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            }

            if ( !this[ '23B' ].state.value ) {
                this[ '23B' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            }

            if ( !this[ '23C' ].state.selectedDay ) {
                this[ '23C' ].setState({ error: true, message: 'This field is required if one of fields in the row has values.' });
                valid = false;
            } else if ( !this.validateDate( this[ '23C' ]) ) {
                this[ '23C' ].setState({ error: true, message: 'Invalid Date' });
                valid = false;
            }

            if ( !this[ '23D' ].state.value ) {
                this[ '23D' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            } else if ( !this.validateNumberField( this[ '23D' ]) ) {
                valid = false;
            } else if ( !this.hasPaymentAmount( this[ '23D' ].state.value ) ) {
                this[ '23D' ].setState({ error: true, errorMessage: 'This field requires an amount greater than zero(0).' });
                valid = false;
            }
        }

        if ( this[ '24A' ].state.value || this[ '24B' ].state.value || this[ '24C' ].state.selectedDay || this.hasPaymentAmount( this[ '24D' ].state.value ) ) {
            if ( !this[ '24A' ].state.value ) {
                this[ '24A' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            }

            if ( !this[ '24B' ].state.value ) {
                this[ '24B' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            }

            if ( !this[ '24C' ].state.selectedDay ) {
                this[ '24C' ].setState({ error: true, message: 'This field is required if one of fields in the row has values.' });
                valid = false;
            } else if ( !this.validateDate( this[ '24C' ]) ) {
                this[ '24C' ].setState({ error: true, message: 'Invalid Date' });
                valid = false;
            }

            if ( !this[ '24D' ].state.value ) {
                this[ '24D' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            } else if ( !this.validateNumberField( this[ '24D' ]) ) {
                valid = false;
            } else if ( !this.hasPaymentAmount( this[ '24D' ].state.value ) ) {
                this[ '24D' ].setState({ error: true, errorMessage: 'This field requires an amount greater than zero(0).' });
                valid = false;
            }
        }

        if ( this[ '25A' ].state.value || this[ '25B' ].state.value || this[ '25C' ].state.selectedDay || this.hasPaymentAmount( this[ '25D' ].state.value ) ) {
            if ( !this[ '25A' ].state.value ) {
                this[ '25A' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            }

            if ( !this[ '25B' ].state.value ) {
                this[ '25B' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            }

            if ( !this[ '25C' ].state.selectedDay ) {
                this[ '25C' ].setState({ error: true, message: 'This field is required if one of fields in the row has values.' });
                valid = false;
            } else if ( !this.validateDate( this[ '25C' ]) ) {
                this[ '25C' ].setState({ error: true, message: 'Invalid Date' });
                valid = false;
            }

            if ( !this[ '25D' ].state.value ) {
                this[ '25D' ].setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
                valid = false;
            } else if ( !this.validateNumberField( this[ '25D' ]) ) {
                valid = false;
            } else if ( !this.hasPaymentAmount( this[ '25D' ].state.value ) ) {
                this[ '25D' ].setState({ error: true, errorMessage: 'This field requires an amount greater than zero(0).' });
                valid = false;
            }
        }

        return valid;
    }

    /**
     * validates amount in payment section
     */
    hasPaymentAmount( value ) {
        const amount = Number( this.stripNonDigit( value ) );
        if ( !isNaN( amount ) && amount > 0 ) {
            return true;
        }
        return false;
    }

    /**
     * sends a request to server to start generating forms.
     */
    generateForm( validatedData ) {
        if ( validatedData || this.validateForm() ) {
            let data;
            if ( validatedData ) {
                data = validatedData;
            } else {
                const settings = JSON.parse( localStorage.getItem( 'government-form-setting' ) );
                const { computation_of_tax } = this.props.data;

                data = {
                    company_id: settings.company_id,
                    month: settings.date.month,
                    year: settings.date.year,
                    amendment_return: this[ '2' ].state.checkedIndex === 1 ? 0 : 1,
                    number_of_sheets_attached: Number( this[ '3' ].state.value ),
                    any_taxes_withheld: this[ '4' ].state.checkedIndex === 1 ? 0 : 1,
                    rdo_code: this[ '6' ].state.value.value || this[ '6' ].state.value,
                    line_of_business_occupation: this[ '7' ].state.value,
                    company_name: this[ '8' ].state.value,
                    telephone_number: Number( this.stripNonNumeric( this[ '9' ].state.value ) ),
                    registered_address: this[ '10' ].state.value,
                    zip_code: this[ '11' ].state.value,
                    category_of_withholding_agent: this[ '12' ].state.value,
                    tax_treaty: this[ '13' ].state.checkedIndex === 1 ? 0 : 1,
                    tax_treaty_law_name: this[ '13.1' ].state.value ? this[ '13.1' ].state.value.value : '',
                    total_treaty_tax_required: Number( this.stripNonDigit( this[ '15' ].state.value ) ),
                    tax_remitted_in_return_previously_filed: Number( this.stripNonDigit( this[ '17' ].state.value ) ),
                    surcharge: Number( this.stripNonDigit( this[ '19A' ].state.value ) ),
                    interest: Number( this.stripNonDigit( this[ '19B' ].state.value ) ),
                    compromise: Number( this.stripNonDigit( this[ '19C' ].state.value ) ),
                    total_amount_still_due: Number( this.stripNonDigit( this[ '20' ].state.value ) ),
                    computation_of_tax: [{
                        nature_of_income_payment: computation_of_tax.nature_of_income_payment,
                        atc: computation_of_tax.atc,
                        tax_base: Number( this.stripNonDigit( this.tax_c.state.value ) ),
                        tax_rate: computation_of_tax.tax_rate,
                        tax_required_to_be_withheld: Number( this.stripNonDigit( this.tax_e.state.value ) )
                    }],
                    signatory_name: this[ '21A' ].state.value,
                    signatory_position: this[ '21B' ].state.value,
                    signatory_tin: this.stripNonNumeric( this[ '21C' ].state.value ),
                    treasurer_name: this[ '22A' ].state.value,
                    treasurer_position: this[ '22B' ].state.value,
                    treasurer_tin: this.stripNonNumeric( this[ '22C' ].state.value ),
                    tax_agent_account_number: this.tax_agent_account_number.state.value,
                    date_of_issuance: moment( this.date_of_issuance.state.selectedDay ).isValid() ? moment( this.date_of_issuance.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    date_of_expiry: moment( this.date_of_expiry.state.selectedDay ).isValid() ? moment( this.date_of_expiry.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    cash_drawee_bank: this[ '23A' ].state.value,
                    cash_number: this[ '23B' ].state.value,
                    cash_date: moment( this[ '23C' ].state.selectedDay ).isValid() ? moment( this[ '23C' ].state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    cash_amount: Number( this.stripNonDigit( this[ '23D' ].state.value ) ),
                    check_drawee_bank: this[ '24A' ].state.value,
                    check_number: this[ '24B' ].state.value,
                    check_date: moment( this[ '24C' ].state.selectedDay ).isValid() ? moment( this[ '24C' ].state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    check_amount: Number( this.stripNonDigit( this[ '24D' ].state.value ) ),
                    others_drawee_bank: this[ '25A' ].state.value,
                    others_number: this[ '25B' ].state.value,
                    others_date: moment( this[ '25C' ].state.selectedDay ).isValid() ? moment( this[ '25C' ].state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    others_amount: Number( this.stripNonDigit( this[ '25D' ].state.value ) )
                };

                this.validatedData = data;
            }

            this.props.generateForm( data );
        } else {
            window.scrollTo( 0, 0 );
        }
    }

    /**
     * Downloads file to user
     */
    downloadFile() {
        const linkElement = document.getElementById( 'downloadLink' );
        linkElement.target = '';
        linkElement.download = '';
        linkElement.href = this.state.uri;
        delay( 200 );
        linkElement.click();
        delay( 1000 );
        linkElement.href = '';
    }

    /**
     * renders a loading screen
     */
    renderLoadingPage( generating = false ) {
        return (
            <MessageWrapperStyles>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        <H3>{ generating ? 'Generating.' : 'Loading data.' }  Please wait...</H3>
                    </div>
                </div>
            </MessageWrapperStyles>
        );
    }

    /**
     * renders header section
    */
    renderHeaderSection() {
        const settings = JSON.parse( localStorage.getItem( 'government-form-setting' ) );
        let applicableMonth = '';
        if ( settings ) {
            const months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];
            applicableMonth = `${months[ settings.date.month - 1 ]} ${settings.date.year}`;
        }
        const { data } = this.props;

        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Monthly Remittance Return of Income Taxes Withheld on Compensation 1601F</H3>
                    <p>Please fill out the fields below to complete the form.</p>
                </div>
                <div className="line flex-end">
                    <div>
                        <Input
                            label={
                                <span>
                                    <span>1. For the month</span>
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltip1" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Previously closed payrolls in the previous month
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="text1"
                            required
                            disabled
                            value={ applicableMonth }
                        />
                    </div>
                    <div style={ { paddingBottom: '12px' } }>
                        <label htmlFor="radioGroup2">
                            2. Amendment return ●
                            <span className="label-tooltip">
                                <ToolTip id="tooltip2" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                    No if the return is not an amendment for that period.<br />
                                    Yes if the return is an amendment for that period
                                </ToolTip>
                            </span>
                        </label>
                        <RadioGroup
                            id="radioGroup2"
                            value="No"
                            horizontal
                            className="radio-group"
                            onChange={ ( value ) => {
                                const changes = { disabled: value === 'No', error: false };
                                if ( value === 'No' ) {
                                    changes.value = '0';
                                }
                                const previousValue17 = this[ '17' ].state.value;
                                this[ '17' ].setState( changes, () => changes.value && changes.value !== previousValue17 && this.updateTaxTotals() );
                            } }
                            ref={ ( ref ) => { this[ '2' ] = ref; } }
                        >
                            <Radio value="Yes">Yes</Radio>
                            <Radio value="No">No</Radio>
                        </RadioGroup>
                    </div>
                    <div>
                        <Input
                            label={
                                <span>
                                    3. Number of sheets attached
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltip3" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Number of pages of attachments
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value="1"
                            id="text3"
                            ref={ ( ref ) => { this[ '3' ] = ref; } }
                            required
                            max={ 2 }
                            className="number-input"
                            onChange={ ( value ) => { this[ '3' ].setState({ value: this.stripNonNumeric( value ) }); } }
                        />
                    </div>
                </div>
                <div className="line">
                    <div>
                        <label htmlFor="radioGroup4">
                            4. Any taxes withheld ●
                            <span className="label-tooltip">
                                <ToolTip id="tooltip4" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                    Set to yes, unless the total withholding tax is &lsquo;0&lsquo;
                                </ToolTip>
                            </span>
                        </label>
                        <RadioGroup
                            id="radioGroup4"
                            value={ String( data.any_taxes_withheld ) }
                            horizontal
                            ref={ ( ref ) => { this[ '4' ] = ref; } }
                            className="radio-group"
                            onChange={ ( value ) => {
                                value === '0'
                                ? this.setState({ showConfirmTaxWithheldNone: false },
                                    () => this.setState({ showConfirmTaxWithheldNone: true }) )
                                : this.changeTaxWitheld( true );
                            } }
                        >
                            <Radio value="1">Yes</Radio>
                            <Radio value="0">No</Radio>
                        </RadioGroup>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * renders part 1 section to DOM
     */
    renderPart1Section() {
        const { data } = this.props;
        const options = this.props.options || {};
        let prefilledRdo = null;
        if ( this.props.options.rdo_code ) {
            const userRdoCode = data.rdo_code;
            prefilledRdo = this.props.options.rdo_code.find( ( rdo ) => rdo.value === userRdoCode );
        }

        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Part 1: Company Information</H3>
                </div>
                <div className="line">
                    <div>
                        <Input
                            label="5. TIN"
                            id="text5"
                            required
                            disabled
                            value={ data.tin }
                        />
                    </div>
                    <div>
                        <SalSelect
                            label={
                                <span>
                                    <span>6. RDO code</span>
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltip6" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            This is already defined in the company setup
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="select6"
                            required
                            value={ prefilledRdo }
                            data={ options.rdo_code }
                            placeholder="Choose your RDO code"
                            ref={ ( ref ) => { this[ '6' ] = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            label={
                                <span>
                                    7. Line of business occupation
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltip7" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Line of business of the company (i.e. Retail, manufacturing, BPO, service provider, etc)
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="text7"
                            required
                            ref={ ( ref ) => { this[ '7' ] = ref; } }
                            max={ 60 }
                        />
                    </div>
                </div>
                <div className="line">
                    <div className="flex2">
                        <Input
                            label={
                                <span>
                                    8. Withholding agent&#39;s name/registered for non-individual
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltip8" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Please ensure that the Registered Business name is entered in this field
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="text8"
                            required
                            value={ data.withholding_agent_name }
                            ref={ ( ref ) => { this[ '8' ] = ref; } }
                            max={ 50 }
                        />
                    </div>
                    <div>
                        <Input
                            label="9. Telephone number"
                            id="text9"
                            required
                            ref={ ( ref ) => { this[ '9' ] = ref; } }
                            max={ 7 }
                            onChange={ ( value ) => {
                                this[ '9' ].setState({ value: this.stripNonNumeric( value ) });
                            } }
                        />
                    </div>
                </div>
                <div className="line">
                    <div className="flex2">
                        <Input
                            label="10. Registered address"
                            id="text10"
                            required
                            value={ data.registered_address }
                            ref={ ( ref ) => { this[ '10' ] = ref; } }
                            max={ 150 }
                        />
                    </div>
                    <div>
                        <Input
                            label="11. Zip code"
                            id="text11"
                            required
                            value={ data.zip_code }
                            ref={ ( ref ) => { this[ '11' ] = ref; } }
                            max={ 4 }
                        />
                    </div>
                </div>
                <div className="line flex-end">
                    <div style={ { paddingBottom: '12px' } }>
                        <label htmlFor="radio12">12. Category of withholding agent ●</label>
                        <RadioGroup
                            id="radio12"
                            value={ data.category_of_withholding_agent ? data.category_of_withholding_agent : 'P' }
                            horizontal
                            ref={ ( ref ) => { this[ '12' ] = ref; } }
                            className="radio-group"
                        >
                            <Radio value="P" >Private</Radio>
                            <Radio value="G" >Government</Radio>
                        </RadioGroup>
                    </div>
                    <div className="tax-relief">
                        <div>
                            <label htmlFor="radio13">13. Are the payees availing of tax relief under Special Law or International Tax Treaty? ●</label>
                        </div>
                        <div>
                            <RadioGroup
                                value="No"
                                id="radio13"
                                horizontal
                                className="radio-group"
                                ref={ ( ref ) => { this[ '13' ] = ref; } }
                                onChange={ ( value ) => {
                                    this[ '13.1' ].setState({
                                        value: value === 'No' ? null : this[ '13.1' ].state.value,
                                        disabled: value === 'No',
                                        error: value === 'No' ? false : this[ '13.1' ].state.error });

                                    const changes15 = { disabled: value === 'No', error: value === 'No' ? false : this[ '15' ].state.error };
                                    if ( value === 'No' ) {
                                        changes15.value = '0';
                                    }
                                    const previousValue15 = this[ '15' ].state.value;
                                    this[ '15' ].setState( changes15, () => changes15.value && changes15.value !== previousValue15 && this.updateTaxTotals() );
                                } }
                            >
                                <Radio value="Yes">Yes</Radio>
                                <Radio value="No">No</Radio>
                            </RadioGroup>
                            <SalSelect
                                id="select13"
                                placeholder="If yes, please specify"
                                autosize
                                data={ options.tax_relief }
                                required
                                disabled
                                ref={ ( ref ) => { this[ '13.1' ] = ref; } }
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * renders part 2 section to DOM
     */
    renderPart2Section() {
        const computationOfTax = this.props.data.computation_of_tax ? this.props.data.computation_of_tax : {};
        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Part 2: Computation of Tax</H3>
                </div>
                <div className="line">
                    <p className="label">
                        Tax required to be withheld based on regular rates
                    </p>
                </div>
                <div className="line">
                    <div style={ { width: '20%' } }>
                        <Input
                            label={
                                <span>
                                    ATC
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipTaxA" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Indicate the nature of income of the taxes witthheld
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            required
                            id="textTaxA"
                            value={ computationOfTax.atc }
                            ref={ ( ref ) => { this.tax_a = ref; } }
                            disabled
                        />
                    </div>
                    <div style={ { width: '80%', paddingRight: '15px' } }>
                        <Input
                            label="Nature of income payment"
                            id="textTaxB"
                            ref={ ( ref ) => { this.tax_b = ref; } }
                            value={ computationOfTax.nature_of_income_payment }
                            disabled
                        />
                    </div>
                </div>
                <div className="line">
                    <div>
                        <Input
                            label={
                                <span>
                                    Tax base
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipTaxC" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Total taxable income for the month for ROHQ employees
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            required
                            id="textTaxC"
                            className="number-input"
                            ref={ ( ref ) => { this.tax_c = ref; } }
                            onChange={ ( value ) => {
                                this.tax_c.setState({ value: this.stripNonDigit( value ) });
                            } }
                            onBlur={ ( value ) => {
                                this.setInputValue( this.tax_c, value, () => {
                                    const fieldTaxC = this.stripNonDigit( value );
                                    const fieldTaxD = this.stripNonDigit( this.tax_d.state.value );

                                    if ( !isNaN( Number( fieldTaxC ) ) && !isNaN( Number( fieldTaxD ) ) ) {
                                        const fieldTaxE = decimal.multiply( fieldTaxC, decimal.divide( fieldTaxD, 100, 2 ) );

                                        this.setInputValue( this.tax_e, fieldTaxE );
                                        this.setInputValue( this[ '14' ], fieldTaxE, () => this.updateTaxTotals() );
                                    }
                                });
                            } }
                            max={ 25 }
                        />
                    </div>
                    <div>
                        <Input
                            label="Tax rate (%)"
                            id="textTaxD"
                            ref={ ( ref ) => { this.tax_d = ref; } }
                            value={ computationOfTax.tax_rate }
                            className="number-input"
                            disabled
                        />
                    </div>
                    <div>
                        <Input
                            label="Tax required to be withheld"
                            id="textTaxE"
                            ref={ ( ref ) => { this.tax_e = ref; } }
                            className="number-input"
                            disabled
                            max={ 25 }
                        />
                    </div>
                </div>
                <div className="line">
                    <InlineInputWrapper>
                        <Input
                            label={
                                <span>
                                    14. Total tax required to be withheld based on regular rates
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltip14" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            The sum of the listed Nature of Income Tax
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            required
                            id="text14"
                            ref={ ( ref ) => { this[ '14' ] = ref; } }
                            disabled
                            className="number-input"
                            max={ 25 }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line">
                    <InlineInputWrapper>
                        <Input
                            label="15. Total tax required to be withheld on tax treaty rates"
                            id="text15"
                            ref={ ( ref ) => { this[ '15' ] = ref; } }
                            className="number-input"
                            onChange={ ( value ) => {
                                this[ '15' ].setState({ value: this.stripNonDigit( value ) });
                            } }
                            onBlur={ ( value ) => { this.setInputValue( this[ '15' ], value, () => this.updateTaxTotals() ); } }
                            value=""
                            disabled
                            max={ 25 }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line">
                    <InlineInputWrapper>
                        <Input
                            label={
                                <span>
                                    16. Total
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipTotal" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            This is the sum of Items Total Tax Required to be Withheld based on Regular Rates (14) and Total Tax Required to be Withheld based on Tax Treaty Rates (15)
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="text16"
                            ref={ ( ref ) => { this[ '16' ] = ref; } }
                            className="number-input"
                            disabled
                            max={ 25 }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line">
                    <InlineInputWrapper>
                        <Input
                            label="17. Less: tax remitted in return previously filed, if this is an amended return"
                            id="text17"
                            ref={ ( ref ) => { this[ '17' ] = ref; } }
                            className="number-input"
                            onChange={ ( value ) => {
                                this[ '17' ].setState({ value: this.stripNonDigit( value ) });
                            } }
                            onBlur={ ( value ) => { this.setInputValue( this[ '17' ], value, () => this.updateTaxTotals() ); } }
                            value=""
                            disabled
                            max={ 25 }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line">
                    <InlineInputWrapper>
                        <Input
                            label="18. Tax still due (overremittance)"
                            id="text18"
                            ref={ ( ref ) => { this[ '18' ] = ref; } }
                            className="number-input"
                            disabled
                            max={ 25 }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line">
                    <p className="label">19. Add: penalties</p>
                </div>
                <div className="line" style={ { width: '66%' } }>
                    <div>
                        <Input
                            label={
                                <span>
                                    19A. Surcharge
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltip19A" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            There shall be imposed, in addition to the tax required to be paid, a penalty equivalent to twenty-five percent (25%) of the amount due
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="text19A"
                            value=""
                            ref={ ( ref ) => { this[ '19A' ] = ref; } }
                            className="number-input"
                            required
                            onChange={ ( value ) => {
                                this[ '19A' ].setState({ value: this.stripNonDigit( value ) });
                            } }
                            onBlur={ ( value ) => { this.setInputValue( this[ '19A' ], value, () => this.updateTotalPenalties() ); } }
                            max={ 25 }
                        />
                    </div>
                    <div>
                        <Input
                            label={
                                <span>
                                    19B. Interest
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltip19B" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            There shall be assessed and collected on any unpaid amount of tax, interest at the rate of twenty percent (20%) per annum
                                            , or such higher rate as may be prescribed by rules and regulations, from the date prescribed for payment until the amount is fully paid.
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value=""
                            ref={ ( ref ) => { this[ '19B' ] = ref; } }
                            id="text19B"
                            className="number-input"
                            required
                            onChange={ ( value ) => {
                                this[ '19B' ].setState({ value: this.stripNonDigit( value ) });
                            } }
                            onBlur={ ( value ) => { this.setInputValue( this[ '19B' ], value, () => this.updateTotalPenalties() ); } }
                            max={ 25 }
                        />
                    </div>
                    <div>
                        <Input
                            label={
                                <span>
                                    19C. Compromise
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltip19C" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            NIRC SEC. 255. Failure to File Return, Supply Correct and Accurate Information, Pay Tax Withhold and Remit Tax and Refund Excess Taxes Withheld on Compensation
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value=""
                            ref={ ( ref ) => { this[ '19C' ] = ref; } }
                            id="text19C"
                            className="number-input"
                            required
                            onChange={ ( value ) => {
                                this[ '19C' ].setState({ value: this.stripNonDigit( value ) });
                            } }
                            onBlur={ ( value ) => { this.setInputValue( this[ '19C' ], value, () => this.updateTotalPenalties() ); } }
                            max={ 25 }
                        />
                    </div>
                </div>
                <div className="line">
                    <InlineInputWrapper>
                        <Input
                            label={
                                <span>
                                    19D. Penalties
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltip19D" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Total of 19A to 19C
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="text19D"
                            ref={ ( ref ) => { this[ '19D' ] = ref; } }
                            className="number-input"
                            disabled
                            value=""
                            max={ 25 }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line">
                    <InlineInputWrapper>
                        <Input
                            label={ <span>20. Total amount still due/(overremittance)<br />(sum of items 18 and 19D)</span> }
                            id="text20"
                            ref={ ( ref ) => { this[ '20' ] = ref; } }
                            className="number-input"
                            required
                            disabled
                            max={ 25 }
                        />
                    </InlineInputWrapper>
                </div>
            </div>
        );
    }

    /**
     * renders signatories section to DOM
     */
    renderSignatoriesSection() {
        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Signatories</H3>
                    <p>
                        I declare under the penalties of perjury, that this return has been made in good faith, verified by us,&nbsp;
                        and to the best of our knowledge and belief, is true and correct, pursuant to the provisions of the&nbsp;
                        Natioanl Interna Revenue Code, as amended, and the regulations issued under authority thereof.
                    </p>
                </div>
                <div className="line flex-end">
                    <div>
                        <Input
                            label="21. President/Vice President/Principal Officer/Accredited Tax Agent/Authorized Representative/Taxpayer"
                            id="text21A"
                            ref={ ( ref ) => { this[ '21A' ] = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            label="Title/position of signatory"
                            id="text21B"
                            ref={ ( ref ) => { this[ '21B' ] = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            label="TIN of signatory"
                            id="text21C"
                            ref={ ( ref ) => { this[ '21C' ] = ref; } }
                            onChange={ ( value ) => { this[ '21C' ].setState({ value: this.formatTIN( value ) }); } }
                        />
                    </div>
                </div>
                <div className="line flex-end">
                    <div>
                        <Input
                            label="22. Treasurer/Assistant Treasurer"
                            id="text22A"
                            ref={ ( ref ) => { this[ '22A' ] = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            label="Title/position of signatory"
                            id="text22B"
                            ref={ ( ref ) => { this[ '22B' ] = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            label="TIN of signatory"
                            id="text22C"
                            ref={ ( ref ) => { this[ '22C' ] = ref; } }
                            onChange={ ( value ) => { this[ '22C' ].setState({ value: this.formatTIN( value ) }); } }
                        />
                    </div>
                </div>
                <div className="line flex-end">
                    <div>
                        <Input
                            label="Tax agent account number/Attorney's roll number (if applicable)"
                            id="textTaxAgentAccount"
                            ref={ ( ref ) => { this.tax_agent_account_number = ref; } }
                        />
                    </div>
                    <InlineDateWrapper>
                        <DatePicker
                            label="Date of issuance"
                            placeholder="Choose a date"
                            required
                            ref={ ( ref ) => { this.date_of_issuance = ref; } }
                            dayFormat="MMMM DD, YYYY"
                        />
                    </InlineDateWrapper>
                    <InlineDateWrapper>
                        <DatePicker
                            label="Date of expiry"
                            placeholder="Choose a date"
                            required
                            ref={ ( ref ) => { this.date_of_expiry = ref; } }
                            dayFormat="MMMM DD, YYYY"
                        />
                    </InlineDateWrapper>
                </div>
            </div>
        );
    }

    /**
     * renders part 3 section to DOM
     */
    renderPart3Section() {
        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Part 3: Details of Payment</H3>
                    <p className="label">23. Cash/bank debit memo</p>
                </div>
                <div className="line quad">
                    <div>
                        <Input
                            label="23A. Drawee bank/agency"
                            id="text23A"
                            ref={ ( ref ) => { this[ '23A' ] = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            label="23B. Number"
                            id="text23B"
                            ref={ ( ref ) => { this[ '23B' ] = ref; } }
                        />
                    </div>
                    <InlineDateWrapper>
                        <DatePicker
                            label="23C. Date"
                            placeholder="Choose a date"
                            ref={ ( ref ) => { this[ '23C' ] = ref; } }
                            dayFormat="MMMM DD, YYYY"
                        />
                    </InlineDateWrapper>
                    <div>
                        <Input
                            label="23D. Amount"
                            id="text23D"
                            ref={ ( ref ) => { this[ '23D' ] = ref; } }
                            className="number-input"
                            onChange={ ( value ) => {
                                this[ '23D' ].setState({ value: this.stripNonDigit( value ) });
                            } }
                            onBlur={ ( value ) => { this.setInputValue( this[ '23D' ], value ); } }
                            max={ 25 }
                        />
                    </div>
                </div>
                <div className="line">
                    <p className="label">24. Check</p>
                </div>
                <div className="line quad">
                    <div>
                        <Input
                            label="24A. Drawee bank/agency"
                            id="text24A"
                            ref={ ( ref ) => { this[ '24A' ] = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            label="24B. Number"
                            id="text24B"
                            ref={ ( ref ) => { this[ '24B' ] = ref; } }
                        />
                    </div>
                    <InlineDateWrapper>
                        <DatePicker
                            label="24C. Date"
                            placeholder="Choose a date"
                            ref={ ( ref ) => { this[ '24C' ] = ref; } }
                            dayFormat="MMMM DD, YYYY"
                        />
                    </InlineDateWrapper>
                    <div>
                        <Input
                            label="24D. Amount"
                            id="text24D"
                            ref={ ( ref ) => { this[ '24D' ] = ref; } }
                            className="number-input"
                            onChange={ ( value ) => {
                                this[ '24D' ].setState({ value: this.stripNonDigit( value ) });
                            } }
                            onBlur={ ( value ) => { this.setInputValue( this[ '24D' ], value ); } }
                            max={ 25 }
                        />
                    </div>
                </div>
                <div className="line">
                    <p className="label">25. Others</p>
                </div>
                <div className="line quad">
                    <div>
                        <Input
                            label="25A. Drawee bank/agency"
                            id="text25A"
                            ref={ ( ref ) => { this[ '25A' ] = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            label="25B. Number"
                            id="text25B"
                            ref={ ( ref ) => { this[ '25B' ] = ref; } }
                        />
                    </div>
                    <InlineDateWrapper>
                        <DatePicker
                            className="inline-date"
                            label="25C. Date"
                            placeholder="Choose a date"
                            ref={ ( ref ) => { this[ '25C' ] = ref; } }
                            dayFormat="MMMM DD, YYYY"
                        />
                    </InlineDateWrapper>
                    <div>
                        <Input
                            label="25D. Amount"
                            id="text25D"
                            ref={ ( ref ) => { this[ '25D' ] = ref; } }
                            className="number-input"
                            onChange={ ( value ) => {
                                this[ '25D' ].setState({ value: this.stripNonDigit( value ) });
                            } }
                            onBlur={ ( value ) => { this.setInputValue( this[ '25D' ], value ); } }
                            max={ 25 }
                        />
                    </div>
                </div>
            </div>
        );
    }

    /**
     * Renders the download screen for SSS forms
     */
    renderDownloadPage() {
        return (
            <DownloadWrapper>
                <div className="downloadWrapper">
                    <div className="svg-wrapper animate">
                        <svg xmlns="http://www.w3.org/2000/svg" width="195" height="195" viewBox="0 0 195 195">
                            <g fill="none">
                                <circle cx="97.5" cy="97.5" r="97.5" fill="#3DA8E3" />
                                <path d="M99 139.9L99 140 96 140 96 139.9 95.9 140 69 113.1 71.1 111 96 135.9 96 55 99 55 99 135.9 123.9 111 126 113.1 99.1 140 99 139.9Z" fill="#FFF" />
                            </g>
                        </svg>
                    </div>
                    <div className="description">
                        <H3>File will start downloading shortly</H3>
                        <p>The compressed file (.zip) contains:</p>
                        <p>You&quot;ll download a printable BIR 1601F form in PDF file.</p>
                        <p>
                            You may&nbsp;
                            <A onClick={ () => this.generateForm( this.validatedData ) } >
                                retry downloading
                            </A>
                            <A download id="downloadLink"></A>
                            &nbsp;if it doesn&apos;t start automatically.
                        </p>
                    </div>
                </div>
            </DownloadWrapper>
        );
    }

    /**
     * renders component to DOM
     */
    render() {
        const { status } = this.props;

        return (
            <div>
                <Helmet
                    title="Government Forms: BIR"
                    meta={ [
                        { name: 'description', content: 'Generate BIR 1601F Forms' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ () => this.changeTaxWitheld( false ) }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="icon"><i className="fa fa-exclamation-circle" /></div>
                            <div className="message">
                                You are about to change the value to &#39;No&#39;. Doing this will clear all computation fields.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ this.state.showConfirmTaxWithheldNone }
                />
                {
                    status.loading ? this.renderLoadingPage( false ) : false
                }
                {
                    status.downloadReady ? this.renderDownloadPage() : false
                }
                {
                    status.generating ? this.renderLoadingPage( true ) : false
                }
                <PageWrapper step={ 2 } visible={ !( status.loading || status.generating || status.downloadReady ) }>
                    <FormsWrapper>
                        <div>
                            <div className="heading">
                                <H3>Bureau of Internal Revenue Form 1601 F</H3>
                                <p>To accomplish your BIR 1601 F form, follow the steps below:</p>
                                <span>1. Fill out the fields required below.</span>
                                <span>2. Generate and print the 1601 F PDF file.</span>
                                <span>3. Bring the printed form to the nearest BIR office.</span>
                            </div>
                            <div className="sections">
                                { this.renderHeaderSection() }
                                { this.renderPart1Section() }
                                { this.renderPart2Section() }
                                { this.renderSignatoriesSection() }
                                { this.renderPart3Section() }
                            </div>
                            <div className="actions">
                                <Button label="Previous Step" type="neutral" size="large" alt onClick={ () => { browserHistory.push( '/forms' ); } } />
                                <Button label="Generate Form" type="action" size="large" onClick={ () => { this.generateForm(); } } />
                            </div>
                        </div>
                    </FormsWrapper>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    data: makeSelectData(),
    options: makeSelectOptions(),
    status: makeSelectStatus(),
    downloadUrl: makeSelectDownloadUrl(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        birFormActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( BIR1601FForms );
