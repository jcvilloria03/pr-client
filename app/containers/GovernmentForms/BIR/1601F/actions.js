import {
    INITIAL_DATA,
    GENERATE_FORMS
} from './constants';

import { RESET_STORE } from '../../../App/constants';

/**
 * Initialize data for payslip page
 */
export function initializeData( payload ) {
    return {
        type: INITIAL_DATA,
        payload
    };
}

/**
 * Initialize data for payslip page
 */
export function generateForm( data ) {
    return {
        type: GENERATE_FORMS,
        payload: data
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
