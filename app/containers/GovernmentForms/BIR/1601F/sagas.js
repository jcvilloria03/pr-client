import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';

import { Fetch } from '../../../../utils/request';

import {
    INITIAL_DATA,
    GENERATE_FORMS,
    SET_LOADING,
    SET_DATA,
    SET_FORM_OPTIONS,
    SET_GENERATING,
    SET_URI,
    DOWNLOAD_READY,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../../../App/constants';
import { resetStore } from '../../../App/sagas';

/**
 * Initialize data for Government Forms:BIR 1601F page
 */
export function* initializeData({ payload }) {
    try {
        yield [
            put({
                type: SET_LOADING,
                payload: true
            }),
            put({ type: DOWNLOAD_READY, payload: false }),
            call( getFormOptions ),
            call( getData, payload )
        ];
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * get prefill data from api
 */
export function* getData( payload ) {
    try {
        yield put({
            type: SET_DATA,
            payload: {}
        });
        const response = yield call( Fetch, `/philippine/government_forms/bir/1601f_values?company_id=${payload.company_id}&month=${payload.date.month}&year=${payload.date.year}`, { method: 'GET' });
        yield put({
            type: SET_DATA,
            payload: response
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * GET OPTIONS FOR 1601C FORMS IN GATEWAY
 */
export function* getFormOptions() {
    const response = yield call( Fetch, '/philippine/government_forms/bir/1601f_options', { method: 'GET' });
    yield put({
        type: SET_FORM_OPTIONS,
        payload: response
    });
}

/**
 * Generate and download BIR forms
 */
export function* generateBIRForms({ payload }) {
    try {
        yield put({
            type: SET_GENERATING,
            payload: true
        });
        const response = yield call( Fetch, '/philippine/government_forms/bir/1601f_generate', { method: 'POST', data: payload });

        yield put({ type: DOWNLOAD_READY, payload: true });
        yield put({ type: SET_URI, payload: response.uri });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_GENERATING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watcher for INITIAL_DATA
 *
 */
export function* watchForInitialData() {
    const watcher = yield takeEvery( INITIAL_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Generate Forms
 *
 */
export function* watchForGenerateBIRForms() {
    const watcher = yield takeEvery( GENERATE_FORMS, generateBIRForms );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitialData,
    watchForGenerateBIRForms,
    watchForNotifyUser,
    watchForReinitializePage
];
