import { createSelector } from 'reselect';

/**
 * Direct selector to the create state domain
 */
const selectCreateDomain = () => ( state ) => state.get( 'BIR1601FForms' );

/**
 * Other specific selectors
 */
const makeSelectData = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'data' ).toJS()
);

const makeSelectOptions = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'options' ).toJS()
);

const makeSelectStatus = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'status' ).toJS()
);

const makeSelectDownloadUrl = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'downloadUrl' )
);

const makeSelectNotification = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectData,
    makeSelectOptions,
    makeSelectStatus,
    makeSelectDownloadUrl,
    makeSelectNotification
};
