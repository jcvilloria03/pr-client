import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { delay } from 'redux-saga';
import decimal from 'js-big-decimal';
import moment from 'moment';

import PageWrapper from '../../PageWrapper';
import SnackBar from '../../../../components/SnackBar';
import Input from '../../../../components/Input';
import SalSelect from '../../../../components/Select';
import DatePicker from '../../../../components/DatePicker';
import { H3, H5 } from '../../../../components/Typography';
import Button from '../../../../components/Button';
import RadioGroup from '../../../../components/RadioGroup';
import Radio from '../../../../components/Radio';
import ToolTip from '../../../../components/ToolTip';
import A from '../../../../components/A';
import Table from '../../../../components/Table';
import SalConfirm from '../../../../components/SalConfirm';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { isAuthorized } from '../../../../utils/Authorization';
import { subscriptionService } from '../../../../utils/SubscriptionService';

import * as birFormActions from './actions';
import {
    makeSelectData,
    makeSelectStatus,
    makeSelectOptions,
    makeSelectDownloadUrl,
    makeSelectNotification
} from './selectors';

import {
    MessageWrapperStyles,
    FormsWrapper,
    InlineInputWrapper,
    InlineDateWrapper,
    DownloadWrapper,
    ConfirmBodyWrapperStyle
} from './styles';

/**
 * BIR 1601E Forms component
 */
export class BIR1601EForms extends React.Component {
    static propTypes = {
        notify: React.PropTypes.func,
        initializeData: React.PropTypes.func,
        generateForm: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        data: React.PropTypes.object,
        options: React.PropTypes.object,
        status: React.PropTypes.shape({
            loading: React.PropTypes.bool,
            generating: React.PropTypes.bool,
            downloadReady: React.PropTypes.bool
        }),
        downloadUrl: React.PropTypes.any,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    }

    /**
    * component's constructor
    */
    constructor( props ) {
        super( props );

        this.state = {
            uri: '',
            showConfirmTaxWithheldNone: false,
            atc: [],
            selectedATC: 0,
            amendmentReturn: 'No',
            taxRelief: 'No',
            withheld: 'Yes',
            companyType: 'P'
        };

        this.downloadFile = this.downloadFile.bind( this );
        this.addAtc = this.addAtc.bind( this );
        this.deleteAtc = this.deleteAtc.bind( this );
        this.formatTIN = this.formatTIN.bind( this );

        this.validatedData = undefined;
    }

    /**
     * runs before render.
     * checks if there is a valid data to generate or if user is authorized to view this page.
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.government_form'], ( authorized ) => {
            if ( !authorized ) {
                browserHistory.replace( '/unauthorized' );
            } else {
                const settings = localStorage.getItem( 'government-form-setting' );
                if ( settings ) {
                    this.props.initializeData( JSON.parse( settings ) );
                } else {
                    browserHistory.replace( '/forms' );
                }
            }
        });
    }

    /**
     * Perform actions when component props changes
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.downloadUrl && nextProps.downloadUrl.length
            && nextProps.downloadUrl !== this.props.downloadUrl
            && this.setState({ uri: nextProps.downloadUrl }, () => {
                this.downloadFile();
            });
    }

    /**
     * Called before component unmounts.
     */
    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * Downloads file to user
     */
    downloadFile() {
        const linkElement = document.getElementById( 'downloadLink' );
        linkElement.target = '';
        linkElement.download = '';
        linkElement.href = this.state.uri;
        delay( 200 );
        linkElement.click();
        delay( 1000 );
        linkElement.href = '';
    }

    /**
     * removes non-digit character to string except dot and dash.
     * @param value
     */
    stripNonDigit( value ) {
        let response;
        if ( value || value === '0' ) {
            response = value.toString().replace( /[^0-9.-]/g, '' );
        } else {
            response = '';
        }
        return response;
    }

    /**
     * transform value into TIN number format
     * @param value
     */
    formatTIN( value ) {
        if ( value ) {
            const tin = value.replace( /\D/g, '' ).split( '-' ).join( '' ).substring( 0, 12 );
            const set = [];
            set[ 0 ] = tin.substr( 0, 3 );
            if ( tin.length > 3 ) set[ 1 ] = tin.substr( 3, 3 );
            if ( tin.length > 6 ) set[ 2 ] = tin.substr( 6, 3 );
            if ( tin.length > 9 ) set[ 3 ] = tin.substr( 9, 3 );
            return set.join( '-' );
        }
        return value;
    }

    /**
     * checks for input errors before generating the PDF file.
     */
    validateForm() {
        let valid = true;

        const field3 = this[ '3' ].state.value;
        if ( !field3 || this[ '3' ].state.error ) {
            valid = false;
            !field3 && this[ '3' ]._validate( this[ '3' ].state.value );
        }

        const field6 = this[ '6' ].state.value;
        if ( !field6 || this[ '6' ].state.error ) {
            valid = false;
            !field6 && this[ '6' ]._checkRequire( field6 );
        }

        const field7 = this[ '7' ].state.value;
        if ( !field7 || this[ '7' ].state.error ) {
            valid = false;
            !field7 && this[ '7' ]._validate( this[ '7' ].state.value );
        }

        const field8 = this[ '8' ].state.value;
        if ( !field8 || this[ '8' ].state.error ) {
            valid = false;
            !field8 && this[ '8' ]._validate( this[ '8' ].state.value );
        }

        const field9 = this[ '9' ].state.value;
        if ( !field9 || this[ '9' ].state.error ) {
            valid = false;
            !field9 && this[ '9' ]._validate( this[ '9' ].state.value );
        }

        const field10 = this[ '10' ].state.value;
        if ( !field10 || this[ '10' ].state.error ) {
            valid = false;
            !field10 && this[ '10' ]._validate( this[ '10' ].state.value );
        }

        if ( this[ '11' ]._validate( this[ '11' ].state.value ) ) {
            valid = false;
        }

        if ( this[ '13' ].state.checkedIndex === 0 ) {
            const field13B = this[ '13.1' ].state.value;
            if ( !field13B || this[ '13.1' ].state.error ) {
                valid = false;
                !field13B && this[ '13.1' ]._checkRequire( field13B );
            }
        }

        if ( this[ '2' ].state.checkedIndex === 0 ) {
            const field15A = this[ '15A' ].state.value;
            if ( !field15A || this[ '15A' ].state.error ) {
                valid = false;
                !field15A && this[ '15A' ]._validate( field15A );
            }
        }

        const field15B = this[ '15B' ].state.value;
        if ( !field15B || this[ '15B' ].state.error ) {
            valid = false;
            !field15B && this[ '15B' ]._validate( field15B );
        }

        const field15C = this[ '15C' ].state.value;
        if ( !field15C || this[ '15C' ].state.error ) {
            valid = false;
            !field15C && this[ '15C' ]._validate( field15C );
        }

        const field16 = this[ '16' ].state.value;
        if ( !field16 || this[ '16' ].state.error ) {
            valid = false;
            !field16 && this[ '16' ]._validate( field16 );
        }

        const field17A = this[ '17A' ].state.value;
        if ( !field17A || this[ '17A' ].state.error ) {
            valid = false;
            !field17A && this[ '17A' ]._validate( field17A );
        }

        const field17B = this[ '17B' ].state.value;
        if ( !field17B || this[ '17B' ].state.error ) {
            valid = false;
            !field17B && this[ '17B' ]._validate( field17B );
        }

        const field17C = this[ '17C' ].state.value;
        if ( !field17C || this[ '17C' ].state.error ) {
            valid = false;
            !field17C && this[ '17C' ]._validate( field17C );
        }

        const field17D = this[ '17D' ].state.value;
        if ( !field17D || this[ '17D' ].state.error ) {
            valid = false;
            !field17D && this[ '17D' ]._validate( field17D );
        }

        const field18 = this[ '18' ].state.value;
        if ( !field18 || this[ '18' ].state.error ) {
            valid = false;
            !field18 && this[ '18' ]._validate( field18 );
        }

        if ( this.signatory_name.state.value || this.signatory_position.state.value || this.signatory_tin.state.vale ) {
            if ( !this.signatory_name.state.value ) {
                valid = false;
                this.signatory_name.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.signatory_position.state.value ) {
                valid = false;
                this.signatory_position.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.signatory_tin.state.value ) {
                valid = false;
                this.signatory_tin.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }
        } else {
            this.signatory_name.setState({ error: false });
            this.signatory_position.setState({ error: false });
            this.signatory_tin.setState({ error: false });
        }

        if ( this.treasurer_name.state.value || this.treasurer_position.state.value || this.treasurer_tin.state.vale ) {
            if ( !this.treasurer_name.state.value ) {
                valid = false;
                this.treasurer_name.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.treasurer_position.state.value ) {
                valid = false;
                this.treasurer_position.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.treasurer_tin.state.value ) {
                valid = false;
                this.treasurer_tin.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }
        } else {
            this.treasurer_name.setState({ error: false });
            this.treasurer_position.setState({ error: false });
            this.treasurer_tin.setState({ error: false });
        }

        if ( this.tax_agent_account_number.state.value || this.date_of_issuance.state.selectedDay || this.date_of_expiry.state.selectedDay ) {
            if ( !this.tax_agent_account_number.state.value ) {
                valid = false;
                this.tax_agent_account_number.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.date_of_issuance.state.selectedDay ) {
                valid = false;
                this.date_of_issuance.setState({ error: true, message: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.date_of_expiry.state.selectedDay ) {
                valid = false;
                this.date_of_expiry.setState({ error: true, message: 'This field is required if one of fields in the row has values.' });
            }
        } else {
            this.tax_agent_account_number.setState({ error: false });
            this.date_of_issuance.setState({ error: false });
            this.date_of_expiry.setState({ error: false });
        }

        if ( this.cash_drawee_bank.state.value || this.cash_number.state.value || this.cash_date.state.selectedDay || this.cash_amount.state.value ) {
            if ( !this.cash_drawee_bank.state.value ) {
                valid = false;
                this.cash_drawee_bank.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.cash_number.state.value ) {
                valid = false;
                this.cash_number.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.cash_date.state.selectedDay ) {
                valid = false;
                this.cash_date.setState({ error: true, message: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.cash_amount.state.value ) {
                valid = false;
                this.cash_amount.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            } else if ( !this.validateAmount( 'cash_amount' ) ) {
                valid = false;
            } else if ( Number( this.stripNonDigit( this.cash_amount.state.value ) ) <= 0 ) {
                this.cash_amount.setState({ error: true, errorMessage: 'This field requires a value greater than zero(0)' });
                valid = false;
            }
        } else {
            this.cash_drawee_bank.setState({ error: false });
            this.cash_number.setState({ error: false });
            this.cash_date.setState({ error: false });
            this.cash_amount.setState({ error: false });
        }

        if ( this.check_drawee_bank.state.value || this.check_number.state.value || this.check_date.state.selectedDay || this.check_amount.state.value ) {
            if ( !this.check_drawee_bank.state.value ) {
                valid = false;
                this.check_drawee_bank.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.check_number.state.value ) {
                valid = false;
                this.check_number.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.check_date.state.selectedDay ) {
                valid = false;
                this.check_date.setState({ error: true, message: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.check_amount.state.value ) {
                valid = false;
                this.check_amount.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            } else if ( !this.validateAmount( 'check_amount' ) ) {
                valid = false;
            } else if ( Number( this.stripNonDigit( this.check_amount.state.value ) ) <= 0 ) {
                this.check_amount.setState({ error: true, errorMessage: 'Please fields required a value greater than zero(0)' });
                valid = false;
            }
        } else {
            this.check_drawee_bank.setState({ error: false });
            this.check_number.setState({ error: false });
            this.check_date.setState({ error: false });
            this.check_amount.setState({ error: false });
        }

        if ( this.others_drawee_bank.state.value || this.others_number.state.value || this.others_date.state.selectedDay || this.others_amount.state.value ) {
            if ( !this.others_drawee_bank.state.value ) {
                valid = false;
                this.others_drawee_bank.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.others_number.state.value ) {
                valid = false;
                this.others_number.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.others_date.state.selectedDay ) {
                valid = false;
                this.others_date.setState({ error: true, message: 'This field is required if one of fields in the row has values.' });
            }

            if ( !this.others_amount.state.value ) {
                valid = false;
                this.others_amount.setState({ error: true, errorMessage: 'This field is required if one of fields in the row has values.' });
            } else if ( !this.validateAmount( 'others_amount' ) ) {
                valid = false;
            } else if ( Number( this.stripNonDigit( this.others_amount.state.value ) ) <= 0 ) {
                this.others_amount.setState({ error: true, errorMessage: 'Please fields required a value greater than zero(0)' });
                valid = false;
            }
        } else {
            this.others_drawee_bank.setState({ error: false });
            this.others_number.setState({ error: false });
            this.others_date.setState({ error: false });
            this.others_amount.setState({ error: false });
        }

        if ( this.state.atc.length === 0 ) {
            valid = false;
            this.props.notify({
                title: 'Error',
                message: 'There should be at least one(1) item in computation of tax section.',
                type: 'error'
            });
        }

        return valid;
    }

    /**
     * sends a request to server to start generating forms.
     */
    generateForms( validatedData = undefined ) {
        if ( validatedData || this.validateForm() ) {
            let data;
            if ( validatedData ) {
                data = validatedData;
            } else {
                const settings = JSON.parse( localStorage.getItem( 'government-form-setting' ) );
                data = {
                    company_id: settings.company_id,
                    month: settings.date.month,
                    year: settings.date.year,
                    amendment_return: this[ '2' ].state.checkedIndex === 1 ? 0 : 1,
                    number_of_sheets_attached: Number( this[ '3' ].state.value ),
                    any_taxes_withheld: this[ '4' ].state.checkedIndex === 1 ? 0 : 1,
                    rdo_code: this[ '6' ].state.value.value || this[ '6' ].state.value,
                    line_of_business_occupation: this[ '7' ].state.value,
                    company_name: this[ '8' ].state.value,
                    telephone_number: Number( this[ '9' ].state.value ),
                    registered_address: this[ '10' ].state.value,
                    zip_code: this[ '11' ].state.value,
                    category_of_withholding_agent: this[ '12' ].state.value,
                    tax_treaty: this[ '13' ].state.checkedIndex === 1 ? 0 : 1,
                    tax_treaty_law_name: this[ '13' ].state.checkedIndex === 0 ? this[ '13.1' ].state.value.value || this[ '13.1' ].state.value : '',
                    computation_of_tax: this.state.atc.length ? this.state.atc.map( ( entry ) => ({
                        nature_of_income_payment: entry.nature,
                        atc: entry.atc,
                        tax_base: entry.base,
                        tax_rate: entry.rate,
                        tax_required_to_be_withheld: entry.withheld
                    }) ) : [],
                    advance_payments_made: Number( this.stripNonDigit( this[ '15B' ].state.value ) ),
                    tax_remitted_in_return_previously_filed: Number( this.stripNonDigit( this[ '15A' ].state.value ) ),
                    surcharge: Number( this.stripNonDigit( this[ '17A' ].state.value ) ),
                    interest: Number( this.stripNonDigit( this[ '17B' ].state.value ) ),
                    compromise: Number( this.stripNonDigit( this[ '17C' ].state.value ) ),
                    signatory_name: this.signatory_name.state.value,
                    signatory_position: this.signatory_position.state.value,
                    signatory_tin: this.signatory_tin.state.value.replace( /[^0-9]/g, '' ),
                    treasurer_name: this.treasurer_name.state.value,
                    treasurer_position: this.treasurer_position.state.value,
                    treasurer_tin: this.treasurer_tin.state.value.replace( /[^0-9]/g, '' ),
                    tax_agent_account_number: this.tax_agent_account_number.state.value,
                    date_of_issuance: this.date_of_issuance.state.selectedDay ? moment( this.date_of_issuance.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    date_of_expiry: this.date_of_expiry.state.selectedDay ? moment( this.date_of_expiry.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    cash_drawee_bank: this.cash_drawee_bank.state.value,
                    cash_number: this.cash_number.state.value,
                    cash_date: this.cash_date.state.selectedDay ? moment( this.cash_date.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    cash_amount: Number( this.stripNonDigit( this.cash_amount.state.value ) ),
                    check_drawee_bank: this.check_drawee_bank.state.value,
                    check_number: this.check_number.state.value,
                    check_date: this.check_date.state.selectedDay ? moment( this.check_date.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    check_amount: Number( this.stripNonDigit( this.check_amount.state.value ) ),
                    others_drawee_bank: this.others_drawee_bank.state.value,
                    others_number: this.others_number.state.value,
                    others_date: this.others_date.state.selectedDay ? moment( this.others_date.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    others_amount: Number( this.stripNonDigit( this.others_amount.state.value ) )
                };

                this.validatedData = data;
            }
            this.props.generateForm( data );
        } else {
            window.scrollTo( 0, 0 );
        }
    }

    /**
     * apply changes when any tax witheld is set to None
     */
    changeTaxWitheld( any ) {
        const change = {
            disabled: !any
        };
        if ( !any ) {
            change.value = '';
            change.error = false;
        }
        this.atc_code.setState( change );
        this.atc_nature.setState( change );
        this.atc_base.setState( change );
        this.atc_rate.setState( change );
        this.atc_tax.setState( change );
        this.atc_add.setState({ disabled: !any });
        this.setState({ atc: [], withheld: any ? 'Yes' : 'No' }, () => {
            this[ '4' ].setState({ checkedIndex: any ? 0 : 1 }, () => {
                this.recalcForm();
            });
        });
    }

    /**
     * recalculate form values
     */
    recalcForm( refName = null ) {
        // GET FORM VALUES
        // GET 14 from ATC entries
        let field14 = '0';
        if ( this.state.atc.length ) {
            this.state.atc.forEach( ( entry ) => {
                field14 = decimal.add( field14, entry.withheld );
            });
        }
        // GET MANUALLY INPUTTED VALUES
        const field15A = this.stripNonDigit( this[ '15A' ].state.value || '0' );
        const field15B = this.stripNonDigit( this[ '15B' ].state.value || '0' );
        const field17A = this.stripNonDigit( this[ '17A' ].state.value || '0' );
        const field17B = this.stripNonDigit( this[ '17B' ].state.value || '0' );
        const field17C = this.stripNonDigit( this[ '17C' ].state.value || '0' );

        // CALCULATE AUTO COMPUTED VALUES
        const field15C = decimal.add( field15A, field15B );
        const field16 = decimal.subtract( field14, field15C );
        const field17D = decimal.add( field17A, decimal.add( field17B, field17C ) );
        const field18 = decimal.add( field16, field17D );

        // VALIDATE VALUES
        refName && this.validateAmount( refName );
        this.validateAmount( '14', field14 );
        this.validateAmount( '15C', field15C );
        this.validateAmount( '16', field16 );
        this.validateAmount( '17D', field17D );
        this.validateAmount( '18', field18 );

        // FORMAT AND DISPLAY NEW VALUES
        if ( refName && this[ refName ].state.value ) {
            this[ refName ].setState({ value: decimal.getPrettyValue( decimal.round( this[ refName ].state.value, 2 ) ) });
        }
        this[ '14' ].setState({ value: decimal.getPrettyValue( decimal.round( field14, 2 ) ) });
        this[ '15C' ].setState({ value: decimal.getPrettyValue( decimal.round( field15C, 2 ) ) });
        this[ '16' ].setState({ value: decimal.getPrettyValue( decimal.round( field16, 2 ) ) });
        this[ '17D' ].setState({ value: decimal.getPrettyValue( decimal.round( field17D, 2 ) ) });
        this[ '18' ].setState({ value: decimal.getPrettyValue( decimal.round( field18, 2 ) ) });
    }

    /**
     * validates amount fields
     * @param field = field to be checked
     * @param value = value to test
     * @returns {boolean} returns true if valid. otherwise, false.
     */
    validateAmount( field, value = null ) {
        let valid = true;
        const eValue = value || this[ field ].state.value;
        this[ field ].setState({ error: false }, () => {
            if ( eValue ) {
                const hasError = this[ field ].state.error;
                if ( !hasError ) {
                    const validAmount = !isNaN( this.stripNonDigit( eValue ) );
                    const positiveAmount = Number( this.stripNonDigit( eValue ) ) >= 0;
                    const validLength = eValue.length <= 25;

                    if ( !validAmount ) {
                        valid = false;
                        this[ field ].setState({ error: true, errorMessage: 'Please input a valid amount.' });
                    } else if ( !positiveAmount ) {
                        valid = false;
                        this[ field ].setState({ error: true, errorMessage: 'This field cannot have a negative value.' });
                    } else if ( !validLength ) {
                        valid = false;
                        this[ field ].setState({ error: true, errorMessage: 'This field cannot exceed 25 characters.' });
                    }
                } else {
                    valid = false;
                }
            } else if ( this[ field ].props.required ) {
                this[ field ].setState({ error: true, errorMessage: 'This field is required' });
                valid = false;
            }

            if ( valid ) {
                this[ field ].setState({ error: false, errorMessage: 'This field is required' });
            }
        });

        return valid;
    }

    /**
     * submit atc form values to add new atc to table.
     */
    addAtc() {
        let formValid = true;
        const formValues = {
            atc: this.atc_code.state.value,
            nature: this.atc_nature.state.value,
            base: this.stripNonDigit( this.atc_base.state.value ),
            rate: this.atc_rate.state.value,
            withheld: this.stripNonDigit( this.atc_tax.state.value )
        };

        if ( !formValues.atc ) {
            this.atc_code._validate( formValues.atc );
            formValid = false;
        } else if ( this.atc_code.state.error ) {
            formValid = false;
        }

        if ( !formValues.nature ) {
            this.atc_nature._validate( formValues.nature );
            formValid = false;
        } else if ( this.atc_nature.state.error ) {
            formValid = false;
        }

        if ( !formValues.base || this.atc_base.state.error || !this.validateAmount( 'atc_base' ) ) {
            !formValues.base && this.atc_base._validate( formValues.base );
            formValid = false;
        }

        if ( !formValues.rate || this.atc_rate.state.error ) {
            !formValues.rate && this.atc_rate._validate( formValues.rate );
            formValid = false;
        } else {
            const validAmount = !isNaN( this.stripNonDigit( formValues.rate ) );
            const withinRange = Number( this.stripNonDigit( formValues.rate ) ) >= 0 && Number( this.stripNonDigit( formValues.rate ) ) <= 100;
            if ( !validAmount || !withinRange ) {
                !validAmount && this.atc_rate.setState({ error: true, errorMessage: 'Please input a valid number.' });
                !withinRange && this.atc_rate.setState({ error: true, errorMessage: 'Value out of range. Must be within 0-100.' });
            }
        }

        if ( !this.validateAmount( 'atc_tax' ) ) {
            formValid = false;
        }

        // IF ALL FIELD VALIDATION PASSED, CHECK FOR DUPLICATE
        formValid && this.state.atc.length && this.state.atc.forEach( ( entry ) => {
            if ( entry.atc === formValues.atc ) {
                formValid = false;
                this.atc_code.setState({ error: true, errorMessage: 'ATC code has already been added.' });
            }
        });

        const newATC = this.state.atc.concat([formValues]);
        formValid && this.setState({ atc: newATC }, () => {
            this.setState({
                selectedATC: 0
            }, () => {
                if ( this.state.atc.length ) {
                    // clear selection
                    const pageSelected = this.atc_table.state.pageSelected;
                    const keys = Object.keys( pageSelected );
                    keys.forEach( ( key ) => {
                        pageSelected[ key ] = false;
                    });
                    this.atc_table.setState({
                        selected: this.atc_table.state.selected.map( () => false ),
                        pageSelected
                    }, () => {
                        this.atc_table.headerSelect.checked = false;
                    });
                }
                this.atc_code.setState({ value: '' });
                this.atc_nature.setState({ value: '' });
                this.atc_base.setState({ value: '' });
                this.atc_rate.setState({ value: '' });
                this.atc_tax.setState({ value: '' });

                this.recalcForm();
            });
        });
    }

    /**
     * removes one or more entry from the atc list.
     */
    deleteAtc() {
        if ( this.atc_table ) {
            const atc = this.state.atc.slice( 0 );
            this.atc_table.state.selected.forEach( ( selected, index ) => {
                if ( selected ) {
                    atc.splice( index, 1, null );
                }
            });

            this.setState({
                atc: atc.filter( ( entry ) => entry ),
                selectedATC: 0
            }, () => {
                this.recalcForm();
            });

            if ( this.state.atc.length ) {
                // clear selection
                const pageSelected = this.atc_table.state.pageSelected;
                const keys = Object.keys( pageSelected );
                keys.forEach( ( key ) => {
                    pageSelected[ key ] = false;
                });
                this.atc_table.setState({
                    selected: this.atc_table.state.selected.map( () => false ),
                    pageSelected
                }, () => {
                    this.atc_table.headerSelect.checked = false;
                });
            }
        }
    }

    /**
     * renders a loading screen
     */
    renderLoadingPage( generating = false ) {
        return (
            <MessageWrapperStyles>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        <H3>{ generating ? 'Generating.' : 'Loading data.' }  Please wait...</H3>
                    </div>
                </div>
            </MessageWrapperStyles>
        );
    }

    /**
     * renders header section
    */
    renderHeaderSection() {
        const settings = JSON.parse( localStorage.getItem( 'government-form-setting' ) );
        let applicableMonth = '';
        if ( settings ) {
            const months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];
            applicableMonth = `${months[ settings.date.month - 1 ]} ${settings.date.year}`;
        }

        return (
            <div className="section">
                <div className="row">
                    <H3>Monthly Remittance Return of Income Taxes Withheld on Compensation 1601 E</H3>
                    <p>Please fill out the fields below to complete the form.</p>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        <Input
                            label={
                                <span>
                                    <span>1. For the month</span>
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipPeriod" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Previously closed payrolls in the previous month
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="selectPeriod"
                            value={ applicableMonth }
                            disabled
                            ref={ ( ref ) => { this[ '1' ] = ref; } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <label htmlFor="radioAmendmentReturn">
                            2. Amendment return ●
                            <span className="label-tooltip">
                                <ToolTip id="radioAmendmentReturn" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                    A return filed in order to make corrections to a tax return for the previous months / year
                                </ToolTip>
                            </span>
                        </label>
                        <RadioGroup
                            id="radioAmendmentReturn"
                            value={ this.state.amendmentReturn }
                            horizontal
                            className="radio-group"
                            onChange={ ( value ) => {
                                this[ '15A' ].setState({ value: '', disabled: value === 'No' }, () => {
                                    this.recalcForm();
                                    this[ '15A' ].setState({ error: false });
                                    this.setState({ amendmentReturn: value });
                                });
                            } }
                            ref={ ( ref ) => { this[ '2' ] = ref; } }
                        >
                            <Radio value="Yes">Yes</Radio>
                            <Radio value="No">No</Radio>
                        </RadioGroup>
                    </div>
                    <div className="col-xs-4 number">
                        <Input
                            label={
                                <span>
                                    3. Number of sheets attached
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipNumberOfSheets" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Specify the number of attachments for this document
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="textNumberOfSheets"
                            required
                            value="1"
                            ref={ ( ref ) => { this[ '3' ] = ref; } }
                            onChange={ ( value ) => {
                                this[ '3' ].setState({ value: value.toString().replace( /[^0-9]/g, '' ) });
                            } }
                            max={ 2 }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        <label htmlFor="radioTaxesWithheld">
                            4. Any taxes withheld ●
                            <span className="label-tooltip">
                                <ToolTip id="radioTaxesWithheld" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                    Set to yes, unless the total withholding tax is &#39;0&#39;.
                                </ToolTip>
                            </span>
                        </label>
                        <RadioGroup
                            id="radioTaxesWithheld"
                            value={ this.state.withheld }
                            horizontal
                            className="radio-group"
                            ref={ ( ref ) => { this[ '4' ] = ref; } }
                            onChange={ ( value ) => {
                                value === 'No'
                                ? this.setState({ showConfirmTaxWithheldNone: false },
                                    () => this.setState({ showConfirmTaxWithheldNone: true }) )
                                : this.changeTaxWitheld( true );
                            } }
                        >
                            <Radio value="Yes">Yes</Radio>
                            <Radio value="No">No</Radio>
                        </RadioGroup>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * renders part 1 section to DOM
     */
    renderPart1Section() {
        const { data } = this.props;
        let prefilledRdo = null;
        if ( this.props.options.rdo_code ) {
            const userRdoCode = data.rdo_code;
            const matchRdo = this.props.options.rdo_code.filter( ( rdo ) => rdo.value === userRdoCode );
            prefilledRdo = matchRdo.length ? matchRdo[ 0 ] : null;
        }
        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Part 1: Company Information</H3>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        <Input
                            label="5. TIN"
                            id="textTIN"
                            value={ this.props.data.tin }
                            required
                            disabled
                            ref={ ( ref ) => { this[ '5' ] = ref; } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <SalSelect
                            label={
                                <span>
                                    <span>6. RDO code</span>
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipRDO" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            This is already defined in the company setup but can still be changed if needed
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="selectRDO"
                            required
                            value={ prefilledRdo }
                            placeholder="Choose your RDO code"
                            data={ this.props.options.rdo_code || [] }
                            ref={ ( ref ) => { this[ '6' ] = ref; } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            label={
                                <span>
                                    7. Line of business occupation
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipLOB" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            (i.e. Retail, manufacturing, BPO, service provider, etc.)
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="textLOB"
                            max={ 60 }
                            required
                            ref={ ( ref ) => { this[ '7' ] = ref; } }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-8">
                        <Input
                            label={
                                <span>
                                    8. Withholding agent&#39;s name/registered for non-individual
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipCompanyName" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Please ensure that the Registered Business name is entered in this field
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="textWithholdingAgent"
                            required
                            max={ 50 }
                            value={ this.props.data.withholding_agent_name }
                            ref={ ( ref ) => { this[ '8' ] = ref; } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            label="9. Telephone number"
                            id="textTelephone"
                            required
                            max={ 7 }
                            ref={ ( ref ) => { this[ '9' ] = ref; } }
                            onChange={ ( value ) => {
                                this[ '9' ].setState({ value: value.toString().replace( /[^0-9]/g, '' ) });
                            } }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-8">
                        <Input
                            label="10. Registered address"
                            id="textRegisteredAddress"
                            required
                            max={ 150 }
                            value={ this.props.data.registered_address }
                            ref={ ( ref ) => { this[ '10' ] = ref; } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            label="11. Zip code"
                            id="textZipCode"
                            required
                            max={ 4 }
                            ref={ ( ref ) => { this[ '11' ] = ref; } }
                            onChange={ ( value ) => {
                                this[ '11' ].setState({ value: value.toString().replace( /[^0-9]/g, '' ) });
                            } }
                            value={ this.props.data.zip_code }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        <label htmlFor="radioAgentCategory">12. Category of withholding agent ●</label>
                        <RadioGroup
                            value={ this.state.companyType }
                            id="radioAgentCategory"
                            horizontal
                            className="radio-group"
                            ref={ ( ref ) => { this[ '12' ] = ref; } }
                            onChange={ ( value ) => {
                                this.setState({ companyType: value });
                            } }
                        >
                            <Radio value="P">Private</Radio>
                            <Radio value="G">Government</Radio>
                        </RadioGroup>
                    </div>
                    <div className="col-xs-8">
                        <label htmlFor="radioTaxRelief">13. Are the payees availing of tax relief under Special Law or International Tax Treaty? ●</label>
                        <div className="row">
                            <div className="col-xs-6">
                                <RadioGroup
                                    value={ this.state.taxRelief }
                                    id="radioTaxRelief"
                                    horizontal
                                    className="radio-group"
                                    ref={ ( ref ) => { this[ '13' ] = ref; } }
                                    onChange={ ( value ) => {
                                        this[ '13.1' ].setState({ disabled: value === 'No', value: null }, () => {
                                            this.setState({ taxRelief: value });
                                        });
                                    } }
                                >
                                    <Radio value="Yes">Yes</Radio>
                                    <Radio value="No">No</Radio>
                                </RadioGroup>
                            </div>
                            <div className="col-xs-6" style={ { marginTop: '7px' } }>
                                <SalSelect
                                    id="selectTaxRelief"
                                    placeholder="If yes, please specify"
                                    autosize
                                    data={ this.props.options.tax_relief || [] }
                                    required
                                    disabled
                                    ref={ ( ref ) => { this[ '13.1' ] = ref; } }
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

        /**
     * renders part 2 section to DOM
     */
    renderPart2Section() {
        const columns = [
            {
                header: 'ATC',
                accessor: 'atc',
                width: 120,
                style: { justifyContent: 'center' }
            },
            {
                header: 'Nature of income payment',
                accessor: 'nature',
                width: 690
            },
            {
                header: 'Tax base',
                accessor: 'base',
                width: 200,
                style: { justifyContent: 'flex-end' },
                render: ( row ) => decimal.getPrettyValue( row.value )
            },
            {
                header: 'Tax rate',
                accessor: 'rate',
                width: 200,
                style: { justifyContent: 'center' },
                render: ( row ) => `${row.value}%`
            },
            {
                header: 'Tax required to be withheld',
                accessor: 'withheld',
                width: 300,
                style: { justifyContent: 'flex-end' },
                render: ( row ) => decimal.getPrettyValue( row.value )
            }
        ];

        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Part 2: Computation of Tax</H3>
                    <H5>Tax required to be withheld and remitted</H5>
                </div>
                <div className="atc-section">
                    <div className="row">
                        <div className="col-xs-4">
                            <Input
                                id="ATC"
                                label={
                                    <span>
                                        ATC
                                        <span className="label-tooltip">
                                            <ToolTip id="toolATC" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                Refer to the <A href="https://efps.bir.gov.ph/efps-war/EFPSWeb_war/help/help1601e.html" target="_blank">list</A> for the &#34;Nature of Income&#34;.
                                            </ToolTip>
                                        </span>
                                    </span>
                                }
                                required
                                max={ 5 }
                                ref={ ( ref ) => { this.atc_code = ref; } }
                            />
                        </div>
                        <div className="col-xs-8">
                            <Input
                                id="nature"
                                label="Nature of income payment"
                                required
                                ref={ ( ref ) => { this.atc_nature = ref; } }
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-4 number">
                            <Input
                                label={
                                    <span>
                                        Tax base
                                        <span className="label-tooltip">
                                            <ToolTip id="tooltipTaxBase" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                This is the total taxable income for a ceratain month for consultants only of the company
                                            </ToolTip>
                                        </span>
                                    </span>
                                }
                                id="textTaxBase"
                                required
                                ref={ ( ref ) => { this.atc_base = ref; } }
                                onFocus={ () => { this.atc_base.setState({ value: this.stripNonDigit( this.atc_base.state.value ) }); } }
                                onChange={ ( value ) => { this.atc_base.setState({ value: this.stripNonDigit( value ) }); } }
                                onBlur={ () => {
                                    // validate changed field
                                    const eValue = this.atc_base.state.value;
                                    const hasError = this.atc_base.state.error;

                                    // start recalc and formatting
                                    const atcBase = this.stripNonDigit( this.atc_base.state.value );
                                    const atcRate = this.atc_rate.state.value.toString().replace( /[^0-9]/g, '' );
                                    const formattedInput = decimal.getPrettyValue( decimal.round( atcBase, 2 ) );
                                    const atcWithheld = decimal.getPrettyValue( decimal.round( decimal.multiply( atcBase, decimal.divide( atcRate, '100' ) ), 2 ) );

                                    if ( !hasError ) {
                                        // check for length
                                        const validLength = formattedInput.length <= 25;
                                        !validLength && this.atc_base.setState({ error: true, errorMessage: 'This field cannot exceed 25 characters.' });

                                        // check for invalid number
                                        const validAmount = !isNaN( this.stripNonDigit( eValue ) );
                                        validLength && !validAmount && this.atc_base.setState({ error: true, errorMessage: 'Please input a valid amount.' });

                                        // check for negative amounts
                                        const positiveAmount = Number( this.stripNonDigit( eValue ) ) > 0;
                                        validAmount && validLength && !positiveAmount && this.atc_base.setState({ error: true, errorMessage: 'This field cannot have a negative value.' });
                                    }

                                    if ( atcBase && !isNaN( atcBase ) && atcRate && !isNaN( atcRate ) ) {
                                        this.atc_base.setState({ value: formattedInput });
                                        this.atc_tax.setState({
                                            value: atcWithheld,
                                            error: atcWithheld.length > 25,
                                            errorMessage: 'This field cannot exceed 25 characters.'
                                        });
                                    } else if ( atcBase && !isNaN( atcBase ) ) {
                                        this.atc_base.setState({ value: formattedInput });
                                        this.atc_tax.setState({ value: '', error: false });
                                    } else {
                                        this.atc_tax.setState({ value: '', error: false });
                                    }
                                } }
                            />
                        </div>
                        <div className="col-xs-4">
                            <Input
                                label="Tax rate"
                                id="textTaxRate"
                                required
                                ref={ ( ref ) => { this.atc_rate = ref; } }
                                max={ 3 }
                                onChange={ ( value ) => { this.atc_rate.setState({ value: value.toString().replace( /[^0-9]/g, '' ) }); } }
                                onBlur={ () => {
                                    // validate changed field
                                    const eValue = this.atc_rate.state.value;
                                    const hasError = this.atc_rate.state.error;

                                    if ( !hasError ) {
                                        // check for invalid number
                                        const validAmount = !isNaN( this.stripNonDigit( eValue ) );
                                        !validAmount && this.atc_rate.setState({ error: true, errorMessage: 'Please input a valid number.' });

                                        // check for possible range
                                        const withinRange = Number( this.stripNonDigit( eValue ) ) >= 0 && Number( this.stripNonDigit( eValue ) ) <= 100;
                                        validAmount && !withinRange && this.atc_rate.setState({ error: true, errorMessage: 'Value out of range. Must be within 0-100.' });
                                    }

                                    // start recalc and formatting
                                    const atcBase = this.stripNonDigit( this.atc_base.state.value );
                                    const atcRate = this.atc_rate.state.value.toString().replace( /[^0-9]/g, '' );
                                    const atcWithheld = decimal.getPrettyValue( decimal.round( decimal.multiply( atcBase, decimal.divide( atcRate, '100' ) ), 2 ) );

                                    if ( atcBase && !isNaN( atcBase ) && atcRate && !isNaN( atcRate ) ) {
                                        this.atc_tax.setState({
                                            value: atcWithheld,
                                            error: atcWithheld.length > 25,
                                            errorMessage: 'This field cannot exceed 25 characters.'
                                        });
                                    } else {
                                        this.atc_tax.setState({ value: '', error: false });
                                    }
                                } }
                            />
                        </div>
                        <div className="col-xs-4 number">
                            <Input
                                label="Tax required to be withheld"
                                id="textTaxRequired"
                                required
                                ref={ ( ref ) => { this.atc_tax = ref; } }
                                onChange={ ( value ) => { this.atc_tax.setState({ value: this.stripNonDigit( value || '' ) }); } }
                                onFocus={ () => { this.atc_tax.setState({ value: this.stripNonDigit( this.atc_tax.state.value ) }); } }
                                onBlur={ () => {
                                    const eValue = this.atc_tax.state.value;
                                    const hasError = this.atc_tax.state.error;
                                    const formattedInput = eValue ? decimal.getPrettyValue( decimal.round( this.stripNonDigit( eValue ), 2 ) ) : '';

                                    if ( !hasError ) {
                                        // check for length
                                        const validLength = formattedInput.length <= 25;
                                        !validLength && this.atc_tax.setState({ error: true, errorMessage: 'This field cannot exceed 25 characters.' });

                                        // check for invalid number
                                        const validAmount = !isNaN( this.stripNonDigit( eValue ) );
                                        validLength && !validAmount && this.atc_tax.setState({ error: true, errorMessage: 'Please input a valid amount.' });

                                        // check for negative amounts
                                        const positiveAmount = Number( this.stripNonDigit( eValue ) ) >= 0;
                                        validAmount && validLength && !positiveAmount && this.atc_tax.setState({ error: true, errorMessage: 'This field cannot have a negative value.' });
                                    }

                                    this.atc_tax.setState({ value: formattedInput });
                                } }
                            />
                        </div>
                    </div>
                    <div id="addATC">
                        <Button
                            label="Add"
                            size="large"
                            alt
                            ref={ ( ref ) => { this.atc_add = ref; } }
                            onClick={ this.addAtc }
                        />
                    </div>
                    <div className="row" style={ { display: this.state.atc.length ? '' : 'none' } } >
                        <div className="col-xs-12">
                            <div id="tableLabel">
                                <div style={ { opacity: this.state.selectedATC > 0 ? '1' : '0' } }>{ `${this.state.selectedATC} entr${this.state.selectedATC > 1 ? 'ies' : 'y'}` } selected</div>
                                <span style={ { opacity: this.state.selectedATC > 0 ? '1' : '0' } }>
                                    <Button
                                        label="DELETE"
                                        type="danger"
                                        size="large"
                                        ref={ ( ref ) => { this.delete_atc = ref; } }
                                        onClick={ this.deleteAtc }
                                    />
                                </span>
                            </div>
                            <Table
                                columns={ columns }
                                data={ this.state.atc }
                                selectable
                                showFilters={ false }
                                pagination={ this.state.atc.length > 10 }
                                ref={ ( ref ) => { this.atc_table = ref; } }
                                onSelectionChange={ () => {
                                    const noOfSelected = this.atc_table.state.selected.filter( ( selected ) => selected ).length;
                                    this.setState({ selectedATC: noOfSelected });
                                } }
                            />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 number">
                        <InlineInputWrapper>
                            <Input
                                label={
                                    <span>
                                        14. Total tax required to be withheld and remitted
                                        <span className="label-tooltip">
                                            <ToolTip id="tooltipTotalTaxWitheld" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                This is the sum of the Tax Required to be Withheld for multiple &#34;Nature of Income&#34; of the consultants
                                            </ToolTip>
                                        </span>
                                    </span>
                                }
                                id="textTotalTaxWitheld"
                                disabled
                                required
                                ref={ ( ref ) => { this[ '14' ] = ref; } }
                            />
                        </InlineInputWrapper>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12">
                        <p className="label">15. Less: tax credits/payments</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 number">
                        <InlineInputWrapper>
                            <Input
                                label="15A. Tax remitted in return previously filed, if this is an amended return"
                                id="textTaxRemittedInReturn"
                                required
                                ref={ ( ref ) => { this[ '15A' ] = ref; } }
                                disabled
                                onBlur={ () => { this.recalcForm( '15A' ); } }
                                onChange={ ( value ) => { this[ '15A' ].setState({ value: this.stripNonDigit( value ) }); } }
                                onFocus={ () => { this[ '15A' ].setState({ value: this.stripNonDigit( this[ '15A' ].state.value ) }); } }
                            />
                        </InlineInputWrapper>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 number">
                        <InlineInputWrapper>
                            <Input
                                label="15B. Advance payments made (please attach proof of payment BIR form no. 0605)"
                                id="textAdvancePayment"
                                required
                                ref={ ( ref ) => { this[ '15B' ] = ref; } }
                                onBlur={ () => { this.recalcForm( '15B' ); } }
                                onChange={ ( value ) => { this[ '15B' ].setState({ value: this.stripNonDigit( value ) }); } }
                                onFocus={ () => { this[ '15B' ].setState({ value: this.stripNonDigit( this[ '15B' ].state.value ) }); } }
                            />
                        </InlineInputWrapper>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 number">
                        <InlineInputWrapper>
                            <Input
                                label={
                                    <span>
                                        15C. Total tax credits/payments
                                        <span className="label-tooltip">
                                            <ToolTip id="tooltipTotalTaxCredits" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                The sum of &#34;Tax remitted in return previously filed&#34; (15A) and &#34;Advance payments made&#34; (15B)
                                            </ToolTip>
                                        </span>
                                    </span>
                                }
                                required
                                disabled
                                id="textTotalTaxCredits"
                                ref={ ( ref ) => { this[ '15C' ] = ref; } }
                            />
                        </InlineInputWrapper>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 number">
                        <InlineInputWrapper>
                            <Input
                                label={
                                    <span>
                                        16. Tax still due/(overremittance)
                                        <span className="label-tooltip">
                                            <ToolTip id="tooltipTaxStillDue" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                The result of this item will indicate if tax due is over or under remitted
                                            </ToolTip>
                                        </span>
                                    </span>
                                }
                                id="textTaxStillDue"
                                disabled
                                required
                                ref={ ( ref ) => { this[ '16' ] = ref; } }
                            />
                        </InlineInputWrapper>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12">
                        <p className="label">17. Add: penalties</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-8">
                        <div className="row">
                            <div className="col-xs-4 number">
                                <Input
                                    label={
                                        <span>
                                            17A. Surcharge
                                            <span className="label-tooltip">
                                                <ToolTip id="tooltipSurcharge" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                    There shall be imposed, in addition to the tax required to be paid, a penalty equivalent to twenty-five percent (25%) of the amount due
                                                </ToolTip>
                                            </span>
                                        </span>
                                    }
                                    id="textSurcharge"
                                    required
                                    ref={ ( ref ) => { this[ '17A' ] = ref; } }
                                    onBlur={ () => { this.recalcForm( '17A' ); } }
                                    onChange={ ( value ) => { this[ '17A' ].setState({ value: this.stripNonDigit( value ) }); } }
                                    onFocus={ () => { this[ '17A' ].setState({ value: this.stripNonDigit( this[ '17A' ].state.value ) }); } }
                                />
                            </div>
                            <div className="col-xs-4 number">
                                <Input
                                    label={
                                        <span>
                                            17B. Interest
                                            <span className="label-tooltip">
                                                <ToolTip id="tooltipInterest" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                    Interest at the rate of twenty percent (20%) per annum, or such higher rate as may be prescribed by rules and regulations, on any unpaid amount of tax, from the date prescribed for the payment until the amount is fully paid.
                                                </ToolTip>
                                            </span>
                                        </span>
                                    }
                                    id="textInterest"
                                    required
                                    ref={ ( ref ) => { this[ '17B' ] = ref; } }
                                    onBlur={ () => { this.recalcForm( '17B' ); } }
                                    onChange={ ( value ) => { this[ '17B' ].setState({ value: this.stripNonDigit( value ) }); } }
                                    onFocus={ () => { this[ '17B' ].setState({ value: this.stripNonDigit( this[ '17B' ].state.value ) }); } }
                                />
                            </div>
                            <div className="col-xs-4 number">
                                <Input
                                    label={
                                        <span>
                                            17C. Compromise
                                            <span className="label-tooltip">
                                                <ToolTip id="tooltipCompromise" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                    NIRC SEC. 255. Failure to File Return, Supply Correct and Accurate Information, Pay Tax Withhold and Remit Tax and Refund Excess Taxes Withheld on Compensation
                                                </ToolTip>
                                            </span>
                                        </span>
                                    }
                                    id="textCompromise"
                                    required
                                    ref={ ( ref ) => { this[ '17C' ] = ref; } }
                                    onBlur={ () => { this.recalcForm( '17C' ); } }
                                    onChange={ ( value ) => { this[ '17C' ].setState({ value: this.stripNonDigit( value ) }); } }
                                    onFocus={ () => { this[ '17C' ].setState({ value: this.stripNonDigit( this[ '17C' ].state.value ) }); } }
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 number">
                        <InlineInputWrapper>
                            <Input
                                label={
                                    <span>
                                        17D. Penalties
                                        <span className="label-tooltip">
                                            <ToolTip id="tooltipPenalties" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                Total amount of penalties. This is the sum of 17A to 17C
                                            </ToolTip>
                                        </span>
                                    </span>
                                }
                                id="textPenalties"
                                disabled
                                required
                                ref={ ( ref ) => { this[ '17D' ] = ref; } }
                            />
                        </InlineInputWrapper>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 number">
                        <InlineInputWrapper>
                            <Input
                                label={
                                    <span>
                                        18. Total amount still due/(overremittance)
                                        <span className="label-tooltip">
                                            <ToolTip id="tooltipAmountStillDue" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                This is the total sum of taxes required to be withheld of the company&#39;s consultants
                                            </ToolTip>
                                        </span>
                                    </span>
                                }
                                id="textAmountStillDue"
                                required
                                disabled
                                ref={ ( ref ) => { this[ '18' ] = ref; } }
                            />
                        </InlineInputWrapper>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * renders signatories section to DOM
     */
    renderSignatoriesSection() {
        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Signatories</H3>
                    <p>
                        We declare, under the penalties of perjury, that this return has been made in good faith,&nbsp;
                        verified by us, and to the best of our knowledge and belief, is true and correct,&nbsp;
                        pursuant to the provisions of the National Internal Revenue Code, as amended,&nbsp;
                        and the regulations issued under authority thereof.
                    </p>
                </div>
                <div className="row sign">
                    <div className="col-xs-4">
                        <Input
                            label="19. President/Vice President/Principal Officer/Accredited Tax Agent/Authorized Representative/Taxpayer"
                            id="textAuthorizedRep"
                            ref={ ( ref ) => { this.signatory_name = ref; } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            label="Title/position of signatory"
                            id="textRepTitle"
                            ref={ ( ref ) => { this.signatory_position = ref; } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            label="TIN of signatory"
                            id="textRepTIN"
                            ref={ ( ref ) => { this.signatory_tin = ref; } }
                            onChange={ ( value ) => { this.signatory_tin.setState({ value: this.formatTIN( value ) }); } }
                        />
                    </div>
                </div>
                <div className="row sign">
                    <div className="col-xs-4">
                        <Input
                            label="20. Treasurer/Assistant Treasurer"
                            id="textTreasurer"
                            ref={ ( ref ) => { this.treasurer_name = ref; } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            label="Title/position of signatory"
                            id="textTreasurerTitle"
                            ref={ ( ref ) => { this.treasurer_position = ref; } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <Input
                            label="TIN of signatory"
                            id="textTreasurerTIN"
                            ref={ ( ref ) => { this.treasurer_tin = ref; } }
                            onChange={ ( value ) => { this.treasurer_tin.setState({ value: this.formatTIN( value ) }); } }
                        />
                    </div>
                </div>
                <div className="row sign">
                    <div className="col-xs-4">
                        <Input
                            label="Tax agent account number/Attorney's roll number (if applicable)"
                            id="textTaxAgent"
                            ref={ ( ref ) => { this.tax_agent_account_number = ref; } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <InlineDateWrapper>
                            <DatePicker
                                label="Date of issuance"
                                placeholder="Choose a date"
                                dayFormat="MMMM DD, YYYY"
                                ref={ ( ref ) => { this.date_of_issuance = ref; } }
                            />
                        </InlineDateWrapper>
                    </div>
                    <div className="col-xs-4">
                        <InlineDateWrapper>
                            <DatePicker
                                label="Date of expiry"
                                placeholder="Choose a date"
                                dayFormat="MMMM DD, YYYY"
                                ref={ ( ref ) => { this.date_of_expiry = ref; } }
                            />
                        </InlineDateWrapper>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * renders part 3 section to DOM
     */
    renderPart3Section() {
        return (
            <div className="section">
                <div className="row">
                    <div className="col-xs-12">
                        <H3>Part 3: Details of Payment</H3>
                        <p className="label">21. Cash/bank debit memo</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-3">
                        <Input
                            label="21A. Drawee bank/agency"
                            id="textCashBank"
                            ref={ ( ref ) => { this.cash_drawee_bank = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <Input
                            label="21B. Number"
                            id="textCashPaymentNumber"
                            ref={ ( ref ) => { this.cash_number = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <InlineDateWrapper>
                            <DatePicker
                                label="21C. Date"
                                placeholder="Choose a date"
                                dayFormat="MMMM DD, YYYY"
                                ref={ ( ref ) => { this.cash_date = ref; } }
                            />
                        </InlineDateWrapper>
                    </div>
                    <div className="col-xs-3 number">
                        <Input
                            label="21D. Amount"
                            id="textCashAmount"
                            ref={ ( ref ) => { this.cash_amount = ref; } }
                            onBlur={ () => { this.cash_amount.setState({ value: decimal.getPrettyValue( decimal.round( this.stripNonDigit( this.cash_amount.state.value ), 2 ) ) }); } }
                            onChange={ ( value ) => { this.cash_amount.setState({ value: this.stripNonDigit( value ) }); } }
                            onFocus={ () => { this.cash_amount.setState({ value: this.stripNonDigit( this.cash_amount.state.value ) }); } }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12">
                        <p className="label">22. Check</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-3">
                        <Input
                            label="22A. Drawee bank/agency"
                            id="textCheckBank"
                            ref={ ( ref ) => { this.check_drawee_bank = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <Input
                            label="22B. Number"
                            id="textCheckPaymentNumber"
                            ref={ ( ref ) => { this.check_number = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <InlineDateWrapper>
                            <DatePicker
                                label="22C. Date"
                                placeholder="Choose a date"
                                dayFormat="MMMM DD, YYYY"
                                ref={ ( ref ) => { this.check_date = ref; } }
                            />
                        </InlineDateWrapper>
                    </div>
                    <div className="col-xs-3 number">
                        <Input
                            label="22D. Amount"
                            id="textCheckAmount"
                            ref={ ( ref ) => { this.check_amount = ref; } }
                            onBlur={ () => { this.check_amount.setState({ value: decimal.getPrettyValue( decimal.round( this.stripNonDigit( this.check_amount.state.value ), 2 ) ) }); } }
                            onChange={ ( value ) => { this.check_amount.setState({ value: this.stripNonDigit( value ) }); } }
                            onFocus={ () => { this.check_amount.setState({ value: this.stripNonDigit( this.check_amount.state.value ) }); } }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12">
                        <p className="label">23. Others</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-3">
                        <Input
                            label="23A. Drawee bank/agency"
                            id="textOthersBank"
                            ref={ ( ref ) => { this.others_drawee_bank = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <Input
                            label="23B. Number"
                            id="textOthersPaymentNumber"
                            ref={ ( ref ) => { this.others_number = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <InlineDateWrapper>
                            <DatePicker
                                label="23C. Date"
                                placeholder="Choose a date"
                                dayFormat="MMMM DD, YYYY"
                                ref={ ( ref ) => { this.others_date = ref; } }
                            />
                        </InlineDateWrapper>
                    </div>
                    <div className="col-xs-3 number">
                        <Input
                            label="23D. Amount"
                            id="textOthersAmount"
                            ref={ ( ref ) => { this.others_amount = ref; } }
                            onBlur={ () => { this.others_amount.setState({ value: decimal.getPrettyValue( decimal.round( this.stripNonDigit( this.others_amount.state.value ), 2 ) ) }); } }
                            onChange={ ( value ) => { this.others_amount.setState({ value: this.stripNonDigit( value ) }); } }
                            onFocus={ () => { this.others_amount.setState({ value: this.stripNonDigit( this.others_amount.state.value ) }); } }
                        />
                    </div>
                </div>
            </div>
        );
    }

    /**
     * Renders the download screen for SSS forms
     */
    renderDownloadPage() {
        return (
            <DownloadWrapper>
                <div className="downloadWrapper">
                    <div className="svg-wrapper animate">
                        <svg xmlns="http://www.w3.org/2000/svg" width="195" height="195" viewBox="0 0 195 195">
                            <g fill="none">
                                <circle cx="97.5" cy="97.5" r="97.5" fill="#3DA8E3" />
                                <path d="M99 139.9L99 140 96 140 96 139.9 95.9 140 69 113.1 71.1 111 96 135.9 96 55 99 55 99 135.9 123.9 111 126 113.1 99.1 140 99 139.9Z" fill="#FFF" />
                            </g>
                        </svg>
                    </div>
                    <div className="description">
                        <H3>File will start downloading shortly</H3>
                        <p>The compressed file (.zip) contains:</p>
                        <p>You&quot;ll download a printable BIR 1601E form in PDF file.</p>
                        <p>
                            You may&nbsp;
                            <A onClick={ () => { this.generateForms( this.validatedData ); } } >
                                retry downloading
                            </A>
                            <A download id="downloadLink"></A>
                            &nbsp;if it doesn&apos;t start automatically.
                        </p>
                    </div>
                </div>
            </DownloadWrapper>
        );
    }

    /**
     * render component to DOM
     */
    render() {
        const { status } = this.props;

        return (
            <div>
                <Helmet
                    title="Government Forms: BIR 1601E"
                    meta={ [
                        { name: 'description', content: 'Generate BIR 1601E Forms' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ () => this.changeTaxWitheld( false ) }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="icon"><i className="fa fa-exclamation-circle" /></div>
                            <div className="message">
                                You are about to change the value to &#39;No&#39;. Doing this will disable the ATC fields and remove any added entry.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ this.state.showConfirmTaxWithheldNone }
                />
                {
                    status.loading ? this.renderLoadingPage( false ) : false
                }
                {
                    status.generating ? this.renderLoadingPage( true ) : false
                }
                {
                    status.downloadReady ? this.renderDownloadPage() : false
                }
                <PageWrapper step={ 2 } visible={ !( status.loading || status.generating || status.downloadReady ) }>
                    <FormsWrapper>
                        <div>
                            <div className="heading">
                                <H3>Bureau of Internal Revenue Form 1601 E</H3>
                                <p>To accomplish your BIR 1601 E form, follow the steps below:</p>
                                <div>1. Fill out the fields required below.</div>
                                <div>2. Generate and print the 1601 E PDF file.</div>
                                <div>3. Bring the printed form to the nearest BIR office.</div>
                            </div>
                            <div className="sections">
                                { this.renderHeaderSection() }
                                { this.renderPart1Section() }
                                { this.renderPart2Section() }
                                { this.renderSignatoriesSection() }
                                { this.renderPart3Section() }
                            </div>
                            <div className="actions">
                                <Button label="Previous Step" type="neutral" size="large" alt onClick={ () => { browserHistory.push( '/forms' ); } } />
                                <Button label="Generate Form" type="action" size="large" onClick={ () => { this.generateForms(); } } />
                            </div>
                        </div>
                    </FormsWrapper>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    data: makeSelectData(),
    status: makeSelectStatus(),
    options: makeSelectOptions(),
    downloadUrl: makeSelectDownloadUrl(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        birFormActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( BIR1601EForms );

