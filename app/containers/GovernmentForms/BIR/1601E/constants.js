/*
 *
 * BIR 1601E Forms constants
 *
 */
export const INITIAL_DATA = 'app/GovernmentForms/BIR/1601E/INITIAL_DATA';
export const SET_LOADING = 'app/GovernmentForms/BIR/1601E/SET_LOADING';
export const SET_FORM_OPTIONS = 'app/GovernmentForms/BIR/1601E/SET_FORM_OPTIONS';

export const SET_DATA = 'app/GovernmentForms/BIR/1601E/SET_DATA';

export const GENERATE_FORMS = 'app/GovernmentForms/BIR/1601E/GENERATE_FORMS';
export const SET_GENERATING = 'app/GovernmentForms/BIR/1601E/SET_GENERATING';

export const DOWNLOAD_READY = 'app/GovernmentForms/BIR/1601E/DOWNLOAD_READY';
export const SET_URI = 'app/GovernmentForms/BIR/1601E/SET_URI';

export const NOTIFICATION_SAGA = 'app/GovernmentForms/BIR/1601E/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/GovernmentForms/BIR/1601E/NOTIFICATION';
export const NOTIFY = 'app/GovernmentForms/BIR/1601E/NOTIFY';
