import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_FORM_OPTIONS,
    SET_DATA,
    SET_GENERATING,
    DOWNLOAD_READY,
    SET_URI,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../../App/constants';

const initialState = fromJS({
    options: {},
    data: {},
    status: {
        loading: true,
        generating: false,
        downloadReady: false
    },
    downloadUrl: '',
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 * transforms form options data from API to accommodate structure in app
 */
function prepareFormOptions( data ) {
    const response = {};
    const keys = Object.keys( data );
    keys.forEach( ( key ) => {
        const formattedOption = [];
        data[ key ].forEach( ({ label, code }) => {
            formattedOption.push({
                label: label.toString(),
                value: code.toString()
            });
        });
        response[ key ] = formattedOption;
    });

    return response;
}

/**
 * BIR1601CFormsReducer
 */
function BIR1601CFormsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_FORM_OPTIONS:
            return state.set( 'options', fromJS( prepareFormOptions( action.payload ) ) );
        case SET_DATA:
            return state.set( 'data', fromJS( action.payload ) );
        case DOWNLOAD_READY:
            return state.setIn([ 'status', 'downloadReady' ], action.payload );
        case SET_GENERATING:
            return state.setIn([ 'status', 'generating' ], action.payload );
        case SET_LOADING:
            return state.setIn([ 'status', 'loading' ], action.payload );
        case SET_URI:
            return state.set( 'downloadUrl', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default BIR1601CFormsReducer;
