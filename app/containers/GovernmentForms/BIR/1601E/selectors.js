import { createSelector } from 'reselect';

/**
 * Direct selector to the create state domain
 */
const selectCreateDomain = () => ( state ) => state.get( 'BIR1601EForms' );

/**
 * Other specific selectors
 */
const makeSelectOptions = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'options' ).toJS()
);

const makeSelectData = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'data' ).toJS()
);

const makeSelectStatus = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'status' ).toJS()
);

const makeSelectDownloadUrl = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'downloadUrl' )
);

const makeSelectNotification = () => createSelector(
    selectCreateDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectData,
    makeSelectStatus,
    makeSelectDownloadUrl,
    makeSelectOptions,
    makeSelectNotification
};
