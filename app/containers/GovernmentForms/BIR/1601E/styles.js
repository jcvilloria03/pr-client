import styled, { keyframes } from 'styled-components';

export const MessageWrapperStyles = styled.div`
    display: flex;
    justify-content: center;
    height: 100vh;

    > div {
        display: flex;
        flex-direction: column;
        justify-content: center;

        i {
            font-size: 7em;
            align-self: center;
        }

        .description {
            padding-top: 40px;
            text-align: center;

            h3 {
                font-weight: 600;
            }
        }
    }
`;

export const FormsWrapper = styled.div`
    padding-bottom: 50px;

    h3 {
        font-weight: 600;
        margin-bottom: 30px;
    }

    .radio-group > div {
        display: table;
    }

    input:disabled {
        border-color: #95989a;
        background: #f8f8f8;
        color: #55595c;

        & + .input-group-addon {
            border-color: #c7c7c7;
        }

        &~label {
            color: #5b5b5b;
        }
    }

    > div {
        width: 90%;
        margin: 20px auto 150px auto;

        div {
            &.heading {
                margin-bottom: 30px;
            }

            &.sections {
                display: flex;
                flex-wrap: wrap;

                .section {
                    background: #fff;
                    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
                    padding: 40px 50px;
                    margin: 10px 0;
                    width: 100%;

                    .row > .col-xs-4 > label {
                        color: #5b5b5b;
                        font-size: 14px;
                        font-weight: 400;
                    }

                    .sign {
                        display: flex;
                        align-items: flex-end;

                        > div {
                            width: 100%;
                        }
                    }

                    .atc-section {
                        background: #F7F9FA;
                        padding: 40px 20px 50px;
                        margin: 30px 0;

                        #addATC {
                            text-align: center;
                            margin-bottom: 25px;

                            button {
                                min-width: 120px;
                            }
                        }

                        #tableLabel {
                            display: flex;
                            justify-content: flex-end;
                            align-items: center;
                            margin-bottom: 20px;

                            > * {
                                display: inline-flex;
                                margin-left: 20px;
                            }
                        }
                    }
                }

                .number input {
                    text-align: right;
                }
            }

            &.actions {
                display: flex;
                justify-content: flex-end;
                margin: 40px 0 20px;

                > button {
                    text-transform: capitalize;

                    &:first-child {
                        margin-right: 30px;
                    }
                }
            }
        }
    }

    .label-tooltip {
        position: absolute;
        padding-left: 20px;
    }

    .label {
        font-size: 15px;
        font-weight: 400;
        color: #5b5b5b;
    }
`;

export const InlineInputWrapper = styled.div`
    width: 100% !important;
    display: flex;

    &.number input {
        text-align: right !important;
    }

    > div {
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        align-items: center;

        > input {
            width: 33%;
        }

        > label {
            width: 66%;
        }

        > label, > input {
            display: inline-block;
        }

        > p {
            width: 33%;
            margin-left: 66%;
        }
    }
`;

export const InlineDateWrapper = styled.div`
    > div {
        display: flex;
        flex-direction: column;
        width: 100%;

        > div.DayPickerInput > input {
            width: 100%;
        }
    }
`;

const bounce = keyframes`
    0%, 20%, 50%, 80%, 100% {
        transform: translate3d(0, 0, 0);
    }
    40% {
        transform: translate3d(0, -30px, 0);
    }
    60% {
        transform: translate3d(0, -15px, 0);
    }
`;

export const DownloadWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;

    .downloadWrapper {
        display: flex;
        flex-direction: column;
        justify-content: center;

        .svg-wrapper {
            align-self: center;

            > svg {
                width: 160px;
                height: auto;
            }

            &.animate > svg {
                animation: ${bounce} 2s infinite;
            }
        }

        .description {
            padding-top: 40px;
            text-align: center;

            a:not([href]):not([tabindex]) {
                color: #00A5E5;
            }
            h3 {
                font-weight: 600;
            }
            #downloadLink {
                display: none;
            }
        }
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;
