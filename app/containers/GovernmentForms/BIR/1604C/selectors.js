import { createSelector } from 'reselect';

/**
 * Direct selector to the Government Form BIR 1604-C state domain
 */
const makeSelectPageDomain = () => (
    ( state ) => ( state.get( 'governmentFormBIR1604C' ) )
);

const makeSelectPageStatus = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'page_status' )
);

const makeSelectData = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'data' ).toJS()
);

const makeSelectDownloadUrl = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'download_url' )
);

const makeSelectNotification = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'notification' ).toJS()
);

export {
    makeSelectPageStatus,
    makeSelectData,
    makeSelectDownloadUrl,
    makeSelectNotification
};
