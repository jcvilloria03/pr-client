import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment/moment';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';
import isEqual from 'lodash/isEqual';
import isNil from 'lodash/isNil';

import PageWrapper from 'containers/GovernmentForms/PageWrapper';
import { GOVERNMENT_FORM_KINDS } from 'containers/GovernmentForms/Generate/constants';

import SnackBar from 'components/SnackBar';
import { H2, H3, H5, H6 } from 'components/Typography';
import Input from 'components/Input';
import RadioGroup from 'components/RadioGroup';
import Radio from 'components/Radio';
import Button from 'components/Button';
import ToolTip from 'components/ToolTip';
import A from 'components/A';
import Loader from 'components/Loader';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';
import {
    stripNonDigit,
    formatTIN,
    formatRDO,
    formatDate,
    formatCurrency,
    stripNonNumeric
} from 'utils/functions';
import { renderField } from 'utils/form';
import { company } from 'utils/CompanyService';
import { DATE_FORMATS } from 'utils/constants';

import * as actions from './actions';
import {
    makeSelectPageStatus,
    makeSelectData,
    makeSelectNotification,
    makeSelectDownloadUrl
} from './selectors';

import {
    LoadingWrapper,
    NavWrapper,
    FormWrapper,
    FormTitleWrapper,
    FormHeaderWrapper,
    SectionWrapper,
    SubSectionWrapper,
    SectionHeadingWrapper,
    BorderedWrapper,
    LineWrapper,
    InputWrapper
} from './styles';

import {
    PAGE_STATUSES,
    FORM_CONFIG,
    BIR_1604C_FORM_FIELDS
} from './constants';

/**
 * BIR Form 1604-C component
 */
export class BIR1604C extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        resetStore: React.PropTypes.func,
        pageStatus: React.PropTypes.string,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        initializeData: React.PropTypes.func,
        saveAndDownloadForm: React.PropTypes.func,
        regenerateForm: React.PropTypes.func,
        data: React.PropTypes.object,
        downloadUrl: React.PropTypes.string,
        routeParams: React.PropTypes.object,
        notify: React.PropTypes.func
    };

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            permissions: {
                edit: false
            },
            selected_month: 1,
            remittances: {}
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.government_form',
            'edit.government_form'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.government_form' ];

            if ( authorized ) {
                this.setState({
                    permissions: {
                        edit: authorization[ 'edit.government_form' ]
                    }
                });
                this.props.initializeData( this.props.routeParams.id );
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    /**
     * Perform actions when component props changes
     */
    componentWillReceiveProps( nextProps ) {
        !isEqual( nextProps.data, this.props.data )
            && this.setState({ remittances: nextProps.data.remittances });

        nextProps.downloadUrl !== this.props.downloadUrl
            && nextProps.downloadUrl
            && this.downloadFile( nextProps.downloadUrl );
    }

    componentDidUpdate( prevProps ) {
        prevProps.pageStatus !== this.props.pageStatus
            && this.props.pageStatus === PAGE_STATUSES.READY
            && this.recalculateComputedValues();
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * Fetches value for field
     * @param {string} id
     * @returns {*}
     */
    getValueForField( id ) {
        const value = this.props.data[ id ];

        if ( BIR_1604C_FORM_FIELDS.AMOUNTS.includes( id ) ) {
            return formatCurrency( value || 0, id );
        }

        const selectedMonth = this.state.selected_month;

        switch ( id ) {
            case 'dateOfRemittance':
            case 'draweeBank':
            case 'traErorEarNumber':
                return get( this.state, [ 'remittances', selectedMonth, id ], '' );
            case 'totalTaxesWithheld':
            case 'adjustment':
            case 'penalties':
                return formatCurrency( get( this.state, [ 'remittances', selectedMonth, id ], '' ) );
            case 'totalAmountRemitted':
                return formatCurrency( get( this.props.data, [ 'remittances', selectedMonth, id ]) );
            case 'month':
                return selectedMonth;
            case 'isTopAgent':
            case 'releasedRefund':
                return `${Number( value )}`;
            case 'monthOfFirstCrediting':
                return value ? `${value}`.padStart( 2, '0' ) : '';
            default:
                return value || '';
        }
    }

    /**
     * Generates specific field configs
     * @param {object} field
     * @returns {object}
     */
    getSpecificFieldConfig( field ) {
        const { id } = field;
        const value = this.getValueForField( id );

        let config = {};
        if ( BIR_1604C_FORM_FIELDS.AMOUNTS.includes( id ) ) {
            config = {
                onChange: ( newValue ) => this[ id ].setState({ value: stripNonDigit( newValue ) }),
                onFocus: () => this[ id ].setState( ( state ) => ({ value: `${Number( stripNonDigit( state.value ) )}` }) ),
                onBlur: () => this[ id ].setState( ( state ) => ({ value: formatCurrency( Number( stripNonDigit( state.value ) ) || 0 ) }) )
            };
        } else {
            switch ( id ) {
                case 'dateOfRefund':
                    config = { selectedDay: value };
                    break;
                case 'dateOfRemittance':
                    config = {
                        onChange: ( newValue ) => this.updateRemittanceFieldValueForSelectedMonth( id, newValue ),
                        selectedDay: value
                    };
                    break;
                case 'draweeBank':
                case 'traErorEarNumber':
                    config = { onBlur: () => this.updateRemittanceFieldValueForSelectedMonth( id, this[ id ].state.value ) };
                    break;
                case 'totalTaxesWithheld':
                case 'adjustment':
                case 'penalties':
                    config = {
                        onChange: ( newValue ) => this[ id ].setState({ value: stripNonDigit( newValue ) }),
                        onFocus: () => this[ id ].setState( ( state ) => ({ value: `${Number( stripNonDigit( state.value ) )}` }) ),
                        onBlur: () => this[ id ].setState( ( state ) => ({ value: formatCurrency( Number( stripNonDigit( state.value ) ) || 0 ) }), () =>
                            this.updateRemittanceFieldValueForSelectedMonth(
                                id,
                                formatCurrency( this[ id ].state.value || 0 ),
                                this.recalculateComputedValues
                            )
                        )
                    };
                    break;
                case 'month':
                    config = { onChange: ( selected ) => this.setState({ selected_month: get( selected, 'value' ) }, () =>
                        this.setState({ selected_month: get( selected, 'value', 1 ) })
                    ) };
                    break;
                case 'employerTin':
                    config = { onChange: ( newValue ) => this[ id ].setState({ value: formatTIN( newValue ) }) };
                    break;
                case 'employerRdoCode':
                    config = { onChange: ( newValue ) => this[ id ].setState({ value: formatRDO( newValue ) }) };
                    break;
                case 'employerZipCode':
                    config = { onChange: ( newValue ) => this[ id ].setState({ value: stripNonNumeric( newValue ) }) };
                    break;
                case 'monthOfFirstCrediting':
                    config = {
                        onChange: ( newValue ) => {
                            const clean = Number( stripNonNumeric( newValue ) || 0 );
                            const cappedValue = clean > 12 ? 12 : clean;
                            this[ id ].setState({ value: cappedValue || '' });
                        },
                        onFocus: () => this[ id ].setState( ( state ) => ({ value: Number( state.value ) || '' }) ),
                        onBlur: () => this[ id ].setState( ( state ) => ({ value: state.value ? `${state.value}`.padStart( 2, '0' ) : '' }) )
                    };
                    break;
                default:
            }
        }

        return { ...config, value };
    }

    updateRemittanceFieldValueForSelectedMonth( id, value, callback ) {
        let formattedValue = value;
        if ( id === 'dateOfRemittance' ) {
            const dateMoment = moment( value, 'MM/DD/YYYY' );
            formattedValue = dateMoment.isValid() ? dateMoment.format( DATE_FORMATS.API ) : '';
        }

        this.setState( ( state ) => {
            const remittances = cloneDeep( state.remittances );
            remittances[ state.selected_month ][ id ] = formattedValue;

            return { remittances };
        }, () => callback && callback() );
    }

    /**
     * Recalculates computed values
     */
    recalculateComputedValues = () => {
        const {
            selected_month: selectedMonth,
            remittances
        } = this.state;
        const remittancesKeys = Object.keys( remittances );

        if ( remittancesKeys.length === 0 ) return;

        const remittanceForSelectedMonth = remittances[ selectedMonth ];
        const totalForSelectedMonth = Number( stripNonDigit( remittanceForSelectedMonth.totalTaxesWithheld ) )
            + Number( stripNonDigit( remittanceForSelectedMonth.adjustment ) )
            + Number( stripNonDigit( remittanceForSelectedMonth.penalties ) );

        const total = remittancesKeys.reduce( ( totals, month ) => {
            /* eslint-disable no-param-reassign */
            totals.taxesWithheld += Number( stripNonDigit( remittances[ month ].totalTaxesWithheld ) );
            totals.adjustments += Number( stripNonDigit( remittances[ month ].adjustment ) );
            totals.penalties += Number( stripNonDigit( remittances[ month ].penalties ) );
            /* eslint-enable no-param-reassign */

            return totals;
        }, {
            taxesWithheld: 0,
            adjustments: 0,
            penalties: 0
        });

        total.amountRemitted = total.taxesWithheld + total.adjustments + total.penalties;

        this.totalAmountRemitted && this.totalAmountRemitted.setState({ value: formatCurrency( totalForSelectedMonth ) });
        this.totals_taxes_withheld && this.totals_taxes_withheld.setState({ value: formatCurrency( total.taxesWithheld ) });
        this.totals_adjustment && this.totals_adjustment.setState({ value: formatCurrency( total.adjustments ) });
        this.totals_penalties && this.totals_penalties.setState({ value: formatCurrency( total.penalties ) });
        this.totals_total_amount_remitted && this.totals_total_amount_remitted.setState({ value: formatCurrency( total.amountRemitted ) });
    }

    /**
     * Resets error state of fields in Remittance section
     */
    resetRemittanceFieldErrors() {
        BIR_1604C_FORM_FIELDS.REMITTANCES.forEach( ( field ) => {
            const errorObject = field === 'dateOfRemittance'
                ? {
                    message: '',
                    error: false
                }
                : {
                    errorMessage: '',
                    error: false
                };

            this[ field ] && this[ field ].setState( errorObject );
        });
    }

    /**
     * Validates remittances values
     * @returns {Boolean}
     */
    validateRemittances() {
        this.resetRemittanceFieldErrors();

        let valid = true;
        const monthsAndRemittances = Object.entries( this.state.remittances );

        for ( const [ month, remittance ] of monthsAndRemittances ) {
            const emptyFields = [];

            for ( const key of BIR_1604C_FORM_FIELDS.REMITTANCES ) {
                if ( remittance[ key ] === '' || isNil( remittance[ key ]) ) {
                    emptyFields.push( key );
                    valid = false;
                }
            }

            if ( !valid ) {
                this.setState({ selected_month: Number( month ) }, () =>
                    emptyFields.forEach( ( field ) => {
                        const errorObject = field === 'dateOfRemittance'
                            ? {
                                message: 'This field is required',
                                error: true
                            }
                            : {
                                errorMessage: 'This field is required',
                                error: true
                            };

                        this[ field ] && this[ field ].setState( errorObject );
                    })
                );
                break;
            }
        }

        return valid;
    }

    /**
     * Validates form before saving and downloading
     * @returns {Boolean}
     */
    validateForm() {
        let valid = true;

        const inputs = BIR_1604C_FORM_FIELDS.INPUTS.concat(
            BIR_1604C_FORM_FIELDS.NUMBER_INPUTS,
            BIR_1604C_FORM_FIELDS.AMOUNTS
        );

        inputs.forEach( ( input ) => {
            if ( this[ input ] && this[ input ]._validate( this[ input ].state.value ) ) {
                valid = false;
            }
        });

        if ( this.mweRegion && !this.mweRegion._checkRequire( get( this.mweRegion.state, 'value.value', this.mweRegion.state.value ) ) ) {
            valid = false;
        }

        return valid;
    }

    /**
     * Dispatches request to regenerate form
     */
    regenerateForm = () => {
        this.setState({ remittances: {}});

        const payload = {
            data: {
                company_id: company.getLastActiveCompanyId(),
                kind: GOVERNMENT_FORM_KINDS.BIR_1604C,
                year: get( this.props.data, 'year' )
            }
        };
        this.props.regenerateForm( payload );
    }

    /**
     * Generates payload for saving the form
     * @returns {object}
     */
    generatePayloadForSave() {
        const companyId = company.getLastActiveCompanyId();
        const remittances = Object.values( this.state.remittances ).map( ( remittance ) => Object.assign({}, remittance, {
            totalTaxesWithheld: Number( stripNonDigit( remittance.totalTaxesWithheld ) ),
            adjustment: Number( stripNonDigit( remittance.adjustment ) ),
            penalties: Number( stripNonDigit( remittance.penalties ) ),
            totalAmountRemitted: Number( stripNonDigit( remittance.totalAmountRemitted ) )
        }) );

        const attributes = {
            companyId,
            categoryWithholdingAgent: this.categoryWithholdingAgent.state.value,
            isAmendedReturn: Boolean( parseInt( this.isAmendedReturn.state.value, 10 ) ),
            isTopAgent: Boolean( parseInt( this.isTopAgent.state.value, 10 ) ),
            releasedRefund: Boolean( parseInt( this.releasedRefund.state.value, 10 ) ),
            mweRegion: get( this.mweRegion.state, 'value.value', this.mweRegion.state.value ),
            remittances
        };

        BIR_1604C_FORM_FIELDS.INPUTS.forEach( ( field ) => {
            attributes[ field ] = this[ field ].state.value;
        });

        BIR_1604C_FORM_FIELDS.NUMBER_INPUTS.forEach( ( field ) => {
            attributes[ field ] = Number( this[ field ].state.value );
        });

        BIR_1604C_FORM_FIELDS.DATES.forEach( ( field ) => {
            attributes[ field ] = formatDate( this[ field ].state.selectedDay, DATE_FORMATS.API ) || '';
        });

        BIR_1604C_FORM_FIELDS.AMOUNTS.forEach( ( field ) => {
            attributes[ field ] = Number( stripNonDigit( this[ field ].state.value ) );
        });

        const payload = {
            id: this.props.routeParams.id,
            data: {
                kind: GOVERNMENT_FORM_KINDS.BIR_1604C,
                company_id: companyId,
                attributes
            }
        };

        return payload;
    }

    /**
     * Dispatches request to save and download form
     */
    saveAndDownloadForm = () => {
        if ( this.validateForm() ) {
            const payload = this.generatePayloadForSave();
            this.props.saveAndDownloadForm( payload );
        } else {
            this.props.notify({
                show: true,
                title: 'Error',
                message: 'Missing required fields.',
                type: 'error'
            });
        }
    }

    /**
     * Download file to browser
     *
     * @param {string} url
     */
    downloadFile( url ) {
        const linkElement = document.getElementById( 'downloadLink' );
        linkElement.target = '_blank';
        linkElement.download = '';
        linkElement.href = url;
        linkElement.click();
        linkElement.href = '';
    }

    /**
     * Renders a loading screen
     *
     * @param {boolean} regenerating - Is regenerating flag
     */
    renderLoadingPage( regenerating = false ) {
        return (
            <LoadingWrapper>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw spinner" />
                    <div className="description">
                        <H3>{ regenerating ? 'Regenerating.' : 'Loading data.' } Please wait...</H3>
                    </div>
                </div>
            </LoadingWrapper>
        );
    }

    /**
     * Renders header section
    */
    renderHeaderSection() {
        const { data } = this.props;

        return (
            <SectionWrapper>
                <FormTitleWrapper>
                    <div>
                        <H5>BIR Form No.</H5>
                        <H2>1604-C</H2>
                    </div>
                    <div>
                        <H2>Annual Information Return</H2>
                        <H5>of Income Taxes Withheld on Compensation</H5>
                    </div>
                </FormTitleWrapper>
                <LineWrapper className="header">
                    <BorderedWrapper className="col-xs-4">
                        <InputWrapper className="col-xs-12">
                            <Input
                                id="input-year"
                                className="input-text-center"
                                label={
                                    <span>
                                        <span className="item-number">1.</span> For the Year (20YY)
                                        <span className="label-tooltip">
                                            <ToolTip id="tooltip-year" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                This is the return period year report you generated. You can edit this field if you wished to modify the return period year
                                            </ToolTip>
                                        </span>
                                    </span>
                                }
                                value={ data.year }
                                disabled
                                ref={ ( ref ) => { this.year = ref; } }
                            />
                        </InputWrapper>
                    </BorderedWrapper>

                    <BorderedWrapper className="col-xs-4 amendment">
                        <InputWrapper className="col-xs-12">
                            <label htmlFor="radio-amendment-return">
                                <span className="item-number">2.</span> Amendment Return?
                                <span className="label-tooltip">
                                    <ToolTip id="tooltip-amendment-return" target={ <i className="fa fa-info-circle" /> } placement="right">
                                        You can select if the report will be an amended return or not
                                    </ToolTip>
                                </span>
                            </label>
                            <RadioGroup
                                id="radio-amendment-return"
                                value={ `${Number( data.isAmendedReturn )}` }
                                horizontal
                                className="radio-group"
                                ref={ ( ref ) => { this.isAmendedReturn = ref; } }
                            >
                                <Radio value="1">Yes</Radio>
                                <Radio value="0">No</Radio>
                            </RadioGroup>
                        </InputWrapper>
                    </BorderedWrapper>

                    <BorderedWrapper className="col-xs-4">
                        <InputWrapper className="col-xs-12">
                            <Input
                                id="input-number-of-sheets"
                                className="input-text-center"
                                type="number"
                                label={
                                    <span>
                                        <span className="item-number">3.</span> Number of Sheets Attached
                                        <span className="label-tooltip">
                                            <ToolTip id="tooltipNumberOfSheets" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                This is the field where in you can define the number of attachment that you will submit together with the 1604-C report
                                            </ToolTip>
                                        </span>
                                    </span>
                                }
                                value={ data.noOfSheetsAttached }
                                minNumber={ 0 }
                                maxNumber={ 99 }
                                ref={ ( ref ) => { this.noOfSheetsAttached = ref; } }
                            />
                        </InputWrapper>
                    </BorderedWrapper>
                </LineWrapper>
            </SectionWrapper>
        );
    }

    renderField = ( field ) => {
        const {
            class_name: className,
            item_no: itemNum,
            label,
            tooltip,
            ...rest
        } = field;

        const specificFieldConfig = this.getSpecificFieldConfig( field );

        const formattedLabel = (
            <span>
                { itemNum && <span className="item-number">{ itemNum }</span> } { label }
                <span className="label-tooltip">
                    <ToolTip
                        id={ `tooltip-${field.id}` }
                        target={ <i className="fa fa-info-circle" /> }
                        placement="right"
                    >
                        { tooltip }
                    </ToolTip>
                </span>
            </span>
        );

        const config = {
            ...rest,
            ...specificFieldConfig,
            label: formattedLabel,
            ref: ( ref ) => { this[ field.id ] = ref; }
        };

        return (
            <InputWrapper className={ className } key={ field.id }>
                { renderField( config ) }
            </InputWrapper>
        );
    };

    renderFormSection( sectionName ) {
        const sectionConfig = FORM_CONFIG[ sectionName ];

        return (
            <SectionWrapper>
                <SectionHeadingWrapper>
                    <H5>{ sectionConfig.TITLE }</H5>
                </SectionHeadingWrapper>
                { sectionConfig.SECTIONS.map( ( section, index ) => (
                    <SubSectionWrapper key={ `${sectionConfig.TITLE}-subsection-${index}` }>
                        { section.TITLE && (
                            <SectionHeadingWrapper className="sub-section">
                                <H6>{ section.TITLE }</H6>
                            </SectionHeadingWrapper>
                        ) }
                        { section.ROWS.map( ( row, rowIndex ) => (
                            <LineWrapper key={ `${sectionName}-${rowIndex}` }>
                                { row.map( this.renderField ) }
                            </LineWrapper>
                        ) ) }
                    </SubSectionWrapper>
                ) ) }
            </SectionWrapper>
        );
    }

    /**
     * renders component to DOM
     */
    render() {
        const isRegenerating = this.props.pageStatus === PAGE_STATUSES.REGENERATING;
        const renderLoading = isRegenerating || this.props.pageStatus === PAGE_STATUSES.LOADING;
        const disableButtons = isRegenerating || this.props.pageStatus === PAGE_STATUSES.DOWNLOADING;

        return (
            <div>
                <Helmet
                    title="Government Forms: BIR 1604C"
                    meta={ [
                        { name: 'description', content: 'Generate BIR 1604C Form' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                { !renderLoading && (
                    <NavWrapper>
                        <Container>
                            <A
                                href="/payroll/forms"
                                onClick={ ( event ) => {
                                    event.preventDefault();
                                    browserHistory.push( '/forms' );
                                } }
                            >
                                &#8592; Back to Government Forms
                            </A>
                        </Container>
                    </NavWrapper>
                ) }

                { renderLoading && this.renderLoadingPage( isRegenerating ) }

                <PageWrapper step={ 2 } visible={ !renderLoading }>
                    <FormHeaderWrapper>
                        <div>
                            <H3>Bureau of Internal Revenue (BIR)</H3>
                            <p>To accomplish your BIR Form No. 1604-C report, follow the steps below:</p>
                            <ol>
                                <li>Fill-out the fields required below. (Click the &quot;Regenerate Form&quot; button if you wish to reload the original values).</li>
                                <li>Click the &quot;Save and Download Form&quot; button to generate the BIR 1604-C Form and save the 1604-C file in a portable drive.</li>
                                <li>Bring the generated 1604-C file and printed form with attached BIR 2316 form for the specified tax year to your company&apos; RDO branch</li>
                            </ol>
                        </div>
                        { this.state.permissions.edit && (
                            <div className="actions">
                                <Button
                                    id="button-regenerate-form"
                                    alt
                                    label="Regenerate Form"
                                    type="action"
                                    size="large"
                                    onClick={ this.regenerateForm }
                                    disabled={ disableButtons }
                                />
                                <Button
                                    id="button-save-and-download-form"
                                    label={ this.props.pageStatus === PAGE_STATUSES.DOWNLOADING ? <Loader /> : 'Save and Download Form' }
                                    type="action"
                                    size="large"
                                    onClick={ this.saveAndDownloadForm }
                                    disabled={ disableButtons }
                                />
                                <A download id="downloadLink" />
                            </div>
                        ) }
                    </FormHeaderWrapper>

                    { !renderLoading && (
                        <FormWrapper>
                            { this.renderHeaderSection() }
                            { this.renderFormSection( 'PART_1' ) }
                            { this.renderFormSection( 'PART_2' ) }
                            { this.renderFormSection( 'MINIMUM_WAGE_DETAILS' ) }
                        </FormWrapper>
                    ) }
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    pageStatus: makeSelectPageStatus(),
    data: makeSelectData(),
    downloadUrl: makeSelectDownloadUrl(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( BIR1604C );
