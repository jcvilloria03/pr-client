import { RESET_STORE } from 'containers/App/constants';

import {
    NOTIFY_USER,
    INITIALIZE_DATA,
    SAVE_AND_DOWNLOAD_FORM,
    REGENERATE_FORM
} from './constants';

/**
 * Initialize data
 * @param {number} payload - Government form ID
 * @returns {Object} action
 */
export function initializeData( payload ) {
    return {
        type: INITIALIZE_DATA,
        payload
    };
}

/**
 * Dispatches request to save and download form
 * @param {object} payload
 * @returns {object} action
 */
export function saveAndDownloadForm( payload ) {
    return {
        type: SAVE_AND_DOWNLOAD_FORM,
        payload
    };
}

/**
 * Dispatches request to regenerate form
 * @param {object} payload
 * @returns {object} action
 */
export function regenerateForm( payload ) {
    return {
        type: REGENERATE_FORM,
        payload
    };
}

/**
 * Dispatches request to show notification in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function notify( payload ) {
    return {
        type: NOTIFY_USER,
        payload
    };
}

/**
 * Resets the state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
