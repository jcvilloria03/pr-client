/**
 *
 * Government Form BIR 1604-C constants
 *
 */
const namespace = 'app/GovernmentForms/BIR/1604C';
export const NOTIFY_USER = `${namespace}/NOTIFY_USER`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;
export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const SET_DATA = `${namespace}/SET_DATA`;
export const SAVE_AND_DOWNLOAD_FORM = `${namespace}/SAVE_AND_DOWNLOAD_FORM`;
export const SET_DOWNLOAD_URL = `${namespace}/SET_DOWNLOAD_URL`;
export const REGENERATE_FORM = `${namespace}/REGENERATE_FORM`;

export const PAGE_STATUSES = {
    LOADING: 'LOADING',
    DOWNLOADING: 'DOWNLOADING',
    REGENERATING: 'REGENERATING',
    READY: 'READY'
};

export const OPTIONS = {
    CATEGORY_OF_WITHHOLDING_AGENT: [
        {
            label: 'Private',
            value: 'PRIVATE'
        },
        {
            label: 'Government',
            value: 'GOVERNMENT'
        }
    ],
    YES_NO: [
        {
            label: 'Yes',
            value: '1'
        },
        {
            label: 'No',
            value: '0'
        }
    ],
    MONTHS: [
        {
            label: 'January',
            value: 1
        },
        {
            label: 'February',
            value: 2
        },
        {
            label: 'March',
            value: 3
        },
        {
            label: 'April',
            value: 4
        },
        {
            label: 'May',
            value: 5
        },
        {
            label: 'June',
            value: 6
        },
        {
            label: 'July',
            value: 7
        },
        {
            label: 'August',
            value: 8
        },
        {
            label: 'September',
            value: 9
        },
        {
            label: 'October',
            value: 10
        },
        {
            label: 'November',
            value: 11
        },
        {
            label: 'December',
            value: 12
        }
    ],
    REGIONS: [
        {
            label: 'I',
            value: 'I'
        },
        {
            label: 'II',
            value: 'II'
        },
        {
            label: 'III',
            value: 'III'
        },
        {
            label: 'IV-A',
            value: 'IV-A'
        },
        {
            label: 'IV-B',
            value: 'IV-B'
        },
        {
            label: 'V',
            value: 'V'
        },
        {
            label: 'VI',
            value: 'VI'
        },
        {
            label: 'VII',
            value: 'VII'
        },
        {
            label: 'VIII',
            value: 'VIII'
        },
        {
            label: 'IX',
            value: 'IX'
        },
        {
            label: 'X',
            value: 'X'
        },
        {
            label: 'XI',
            value: 'XI'
        },
        {
            label: 'XII',
            value: 'XII'
        },
        {
            label: 'XIII',
            value: 'XIII'
        },
        {
            label: 'ARMM',
            value: 'ARMM'
        },
        {
            label: 'CAR',
            value: 'CAR'
        },
        {
            label: 'CARAGA',
            value: 'CARAGA'
        },
        {
            label: 'NCR',
            value: 'NCR'
        }

    ]
};

export const FORM_CONFIG = {
    PART_1: {
        TITLE: 'Part I: Background Information',
        SECTIONS: [
            {
                TITLE: null,
                ROWS: [
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 input-text-center',
                            id: 'employerTin',
                            item_no: '4.',
                            label: 'Tax Identification Number (TIN)',
                            placeholder: '000-000-000-000',
                            tooltip: 'This is the company TIN that you have entered in the company profile information. You can edit this field if you wished to modify the information',
                            validations: {
                                required: true
                            },
                            type: 'tin'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 input-text-center',
                            id: 'employerRdoCode',
                            item_no: '5.',
                            label: 'RDO Code',
                            placeholder: 'Enter RDO code',
                            tooltip: 'This is the RDO Code that you have entered in the company profile information. You can edit this field if you wished to modify the information',
                            validations: {
                                required: true
                            },
                            type: 'rdo'
                        }
                    ],
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-12',
                            id: 'employerName',
                            item_no: '6.',
                            label: 'Withholding Agent\'s Name (Last Name, First Name, Middle Name for Individual OR Registered Name for Non-Individual)',
                            placeholder: 'Enter name',
                            tooltip: 'This is the Company name that you have entered in the company profile information. You can edit this field if you wished to modify the information',
                            validations: {
                                required: true
                            }
                        }
                    ],
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-8',
                            id: 'employerAddress',
                            item_no: '7.',
                            label: 'Registered Address',
                            placeholder: 'Enter address',
                            tooltip: 'This is the address that you have entered in the company information. You can edit this field if you wished to modify the information',
                            validations: {
                                required: true
                            }
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 input-text-center',
                            id: 'employerZipCode',
                            item_no: '7A.',
                            label: 'ZIP Code',
                            placeholder: 'Enter ZIP code',
                            tooltip: 'This is the ZIP Code that you have entered in the company information. You can edit this field if you wished to modify the information',
                            validations: {
                                required: true
                            }
                        }
                    ],
                    [
                        {
                            field_type: 'radio-group',
                            class_name: 'col-xs-4',
                            id: 'categoryWithholdingAgent',
                            item_no: '8.',
                            label: 'Category of Withholding Agent',
                            horizontal: true,
                            tooltip: 'You can select between Private of Government depending on the category of your company',
                            options: OPTIONS.CATEGORY_OF_WITHHOLDING_AGENT
                        },
                        {
                            field_type: 'radio-group',
                            class_name: 'col-xs-4',
                            id: 'isTopAgent',
                            item_no: '8A.',
                            label: 'If private, top withholding agent?',
                            horizontal: true,
                            tooltip: 'If you have selected the category as Private, please identify if this company is your top withholding agent or not',
                            options: OPTIONS.YES_NO
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 input-text-center',
                            id: 'employerContactNumber',
                            item_no: '9.',
                            label: 'Contact Number',
                            placeholder: 'Enter contact number',
                            tooltip: 'This is the contact number you have provided in the company profile. You can edit this field if you wished to modify the information',
                            validations: {
                                required: false
                            }
                        }
                    ],
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4',
                            id: 'employerEmail',
                            item_no: '10.',
                            label: 'Email Address',
                            placeholder: 'Enter email',
                            tooltip: 'This is the email address you have provided in the company profile. You can edit this field if you wished to modify the information',
                            validations: {
                                required: false
                            },
                            type: 'email'
                        }
                    ],
                    [
                        {
                            field_type: 'radio-group',
                            class_name: 'col-xs-12',
                            id: 'releasedRefund',
                            item_no: '11.',
                            label: 'In case of overwithholding/overremittance after the year-end adjustments on compensation, have you released the refund/s to your employee/s?',
                            horizontal: true,
                            tooltip: 'You can identify whether you have over withheld/over remit the tax and that you have refunded the amount to the employee/s here',
                            options: OPTIONS.YES_NO
                        }
                    ],
                    [
                        {
                            field_type: 'date-picker',
                            class_name: 'col-xs-4',
                            id: 'dateOfRefund',
                            item_no: '11A.',
                            label: 'If Yes, specify the date of refund (MM/DD/YYYY)',
                            placeholder: 'Select date',
                            tooltip: 'If you have released the tax refund to the employee/s, please provide the date you have refunded the tax',
                            validations: {
                                required: false
                            },
                            display_format: 'MM/DD/YYYY'
                        }
                    ],
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-8 number total-amount-overremittance',
                            id: 'totalAmountOfOverremittance',
                            item_no: '12.',
                            label: 'Total amount of overremittance on Tax withheld under compenstation',
                            placeholder: 'Enter amount',
                            tooltip: 'If you have over remitted the withheld tax, please put the total over remittance value',
                            validations: {
                                required: false
                            }
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 input-text-center',
                            id: 'monthOfFirstCrediting',
                            item_no: '13.',
                            label: 'Month of First Crediting of Overremittance (MM)',
                            placeholder: '',
                            tooltip: 'If you have over remitted the taxes withheld, please identify the month on when you would like to credit the over remittance amount',
                            validations: {
                                required: false
                            }
                        }
                    ]
                ]
            }
        ]
    },
    PART_2: {
        TITLE: 'Part II: Summary of Remittances per BIR Form No. 1601-C',
        SECTIONS: [
            {
                TITLE: 'DETAILS',
                ROWS: [
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-4',
                            id: 'month',
                            label: 'Month',
                            placeholder: 'Select month',
                            tooltip: 'Displays the month on which the Tax details belongs to, you can navigate through different month/s you wished to view the tax details using the combo box',
                            validations: {
                                required: false
                            },
                            options: OPTIONS.MONTHS
                        },
                        {
                            field_type: 'date-picker',
                            class_name: 'col-xs-4',
                            id: 'dateOfRemittance',
                            label: 'Date of Remittance',
                            placeholder: 'Select date',
                            tooltip: 'This is the date on which you have remitted the withholding tax for the identified month based on your generated 1601-C report. You can edit this field if you wished to modify the remittance date',
                            validations: {
                                required: false
                            },
                            display_format: 'MM/DD/YYYY'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4',
                            id: 'draweeBank',
                            label: 'Drawee Bank/ Bank Code/ Agency',
                            placeholder: '',
                            tooltip: 'This is the information on which you identify the Drawee Bank/Agency the tax was paid through based on your generated 1601-C report. You can edit this field if you wished to modify the information',
                            validations: {
                                required: false
                            }
                        }
                    ],
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4',
                            id: 'traErorEarNumber',
                            label: 'TRA/eROR/eAR Number',
                            placeholder: '',
                            tooltip: 'This is the receipt number you have provided in your 1601-C report. You can edit this field if you wished to modify the information',
                            validations: {
                                required: false
                            }
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 number',
                            id: 'totalTaxesWithheld',
                            label: 'Taxes Withheld',
                            placeholder: '',
                            tooltip: 'This is the total taxes withheld you have identified in your 1601-C report for the specified month. You can edit this field if you wished to modify the values',
                            validations: {
                                required: false
                            }
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 number',
                            id: 'adjustment',
                            label: 'Adjustment',
                            placeholder: '',
                            tooltip: 'This is the adjustment amount you have identified in your 1601-C report for the specified month. You can edit this field if you wished to modify the values',
                            validations: {
                                required: false
                            }
                        }
                    ],
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 number',
                            id: 'penalties',
                            label: 'Penalties',
                            placeholder: '',
                            tooltip: 'This is the penalties amount you have identified in your 1601-C report for the specified month. You can edit this field if you wished to modify the values',
                            validations: {
                                required: false
                            }
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 number',
                            id: 'totalAmountRemitted',
                            label: 'Total Amount Remitted',
                            placeholder: '',
                            tooltip: 'This is the sum value of Tax Withheld, Adjustment, and Penalties. If you modify either of those values the Total Amount Remitted will adjust accordingly',
                            validations: {
                                required: false
                            },
                            disabled: true
                        }
                    ]
                ]
            },
            {
                TITLE: 'TOTALS',
                ROWS: [
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 number',
                            id: 'totals_taxes_withheld',
                            label: 'Taxes Withheld',
                            placeholder: '',
                            tooltip: 'This field will display the sum total of the Taxes Withheld from all the months the you have generated the 1601-C report. Whenever you modified a value in a certain month/s, this value will adjust accordingly',
                            validations: {
                                required: false
                            },
                            disabled: true
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 number',
                            id: 'totals_adjustment',
                            label: 'Adjustment',
                            placeholder: '',
                            tooltip: 'This field will display the sum total of the Adjustments from all the months the you have generated the 1601-C report. Whenever you modified a value in a certain month/s, this value will adjust accordingly',
                            validations: {
                                required: false
                            },
                            disabled: true
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 number',
                            id: 'totals_penalties',
                            label: 'Penalties',
                            placeholder: '',
                            tooltip: 'This field will display the sum total of the Penalties from all the months the you have generated the 1601-C report. Whenever you modified a value in a certain month/s, this value will adjust accordingly',
                            validations: {
                                required: false
                            },
                            disabled: true
                        }
                    ],
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 number',
                            id: 'totals_total_amount_remitted',
                            label: 'Total Amount Remitted',
                            placeholder: '',
                            tooltip: 'This field will display the sum total of the Total Amount Remitted from all the months the you have generated the 1601-C report. Whenever you modified a value in a certain month/s, this value will adjust accordingly',
                            validations: {
                                required: false
                            },
                            disabled: true
                        }
                    ]
                ]
            }
        ]
    },
    MINIMUM_WAGE_DETAILS: {
        TITLE: 'MINIMUM WAGE DETAILS',
        SECTIONS: [
            {
                TITLE: null,
                ROWS: [
                    [
                        {
                            field_type: 'select',
                            class_name: 'col-xs-4',
                            id: 'mweRegion',
                            label: 'Region',
                            placeholder: 'Select region',
                            tooltip: 'This is where you input the Region that your company is located. Information provided in this field will be used in your current Alphalist report',
                            validations: {
                                required: true
                            },
                            options: OPTIONS.REGIONS
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4',
                            id: 'mweDaysPerYear',
                            label: 'Number of Days per Year',
                            placeholder: '',
                            tooltip: 'This is where you provide the number of working days that your company operates in a year. Information provided in this field will be used in your current Alphalist report',
                            validations: {
                                required: false,
                                minNumber: 0,
                                maxNumber: 365
                            },
                            type: 'number'
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 number',
                            id: 'mwePerDay',
                            label: 'Minimum Wage per Day',
                            placeholder: '',
                            tooltip: 'This is where you provide the Minimum Wage Salary per day that your company\'s region has. Information provided in this field will be used in your current Alphalist report',
                            validations: {
                                required: false
                            }
                        }
                    ],
                    [
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 number',
                            id: 'mwePerMonth',
                            label: 'Minimum Wage per Month',
                            placeholder: '',
                            tooltip: 'This is where you provide the Minimum Wage Salary per month that your company\'s region has. Information provided in this field will be used in your current Alphalist report',
                            validations: {
                                required: false
                            }
                        },
                        {
                            field_type: 'input',
                            class_name: 'col-xs-4 number',
                            id: 'mwePerYear',
                            label: 'Minimum Wage per Year',
                            placeholder: '',
                            tooltip: 'This is where you provide the Minimum Wage Salary per year that your company\'s region has. Information provided in this field will be used in your current Alphalist report',
                            validations: {
                                required: false
                            }
                        }
                    ]
                ]
            }
        ]
    }
};

export const BIR_1604C_FORM_FIELDS = {
    INPUTS: [
        'year',
        'employerTin',
        'employerRdoCode',
        'employerName',
        'employerAddress',
        'employerZipCode',
        'employerContactNumber',
        'employerEmail',
        'monthOfFirstCrediting'
    ],
    NUMBER_INPUTS: [
        'noOfSheetsAttached',
        'monthOfFirstCrediting',
        'mweDaysPerYear'
    ],
    DATES: [
        'dateOfRefund',
        'dateOfRemittance'
    ],
    AMOUNTS: [
        'totalAmountOfOverremittance',
        'mwePerDay',
        'mwePerMonth',
        'mwePerYear'
    ],
    REMITTANCES: [
        'dateOfRemittance',
        'draweeBank',
        'traErorEarNumber',
        'totalTaxesWithheld',
        'adjustment',
        'penalties'
    ]
};
