import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';

import { formatNumber } from 'utils/functions';

import {
    SET_PAGE_STATUS,
    SET_NOTIFICATION,
    PAGE_STATUSES,
    SET_DATA,
    SET_DOWNLOAD_URL,
    BIR_1604C_FORM_FIELDS,
    OPTIONS
} from './constants';

const initialState = fromJS({
    page_status: PAGE_STATUSES.LOADING,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    data: {
        remittances: {}
    },
    download_url: ''
});

/**
 * Prepares data for form
 *
 * @param {object} data - BIR 1604-C data
 * @returns {object}
 */
function prepareData({
    attributes,
    ...rest
}) {
    const remittances = OPTIONS.MONTHS.reduce( ( remittancesObj, { value }) => ({
        ...remittancesObj,
        [ value ]: attributes.remittances.find( ( remittance ) => parseInt( remittance.month, 10 ) === value ) || {}
    }), {});

    const data = {
        ...rest,
        ...attributes,
        remittances
    };

    BIR_1604C_FORM_FIELDS.AMOUNTS.forEach( ( field ) => {
        if ( Object.prototype.hasOwnProperty.call( data, field ) ) {
            data[ field ] = formatNumber( attributes[ field ]);
        }
    });

    return data;
}

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case SET_PAGE_STATUS:
            return state.set( 'page_status', action.payload );
        case SET_DATA:
            return state.set( 'data', fromJS( prepareData( action.payload ) ) );
        case SET_DOWNLOAD_URL:
            return state.set( 'download_url', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
};
