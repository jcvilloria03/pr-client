import styled from 'styled-components';

const borderStyle = '1px solid #ccc';

export const LoadingWrapper = styled.div`
    display: flex;
    justify-content: center;
    height: 100vh;

    & > div {
        display: flex;
        flex-direction: column;
        justify-content: center;

        .spinner {
            font-size: 7em;
            align-self: center;
        }

        .description {
            padding-top: 40px;
            text-align: center;

            h3 {
                font-weight: 600;
            }
        }
    }
`;

export const FormWrapper = styled.div`
    margin-bottom: 80px;
`;

export const FormTitleWrapper = styled.div`
    width: 100%;
    text-align: center;
    display: flex;

    & > div {
        padding: 10px;
        border-bottom: ${borderStyle};

        &:first-child {
            padding-left: 30px;
            padding-right: 30px;
            border-right: ${borderStyle};
        }

        &:last-child {
            flex: 1;
        }
    }

    h2, h5 {
        font-weight: 600;
        margin: 0;
    }
`;

export const SectionWrapper = styled.section`
    display: flex;
    flex-wrap: wrap;
    background: #fff;
    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
    margin: 0;
    margin-bottom: 12px;
    width: 100%;

    .line-heading {
        padding: 0 15px;
        margin-bottom: 10px;

        h3 {
            margin-bottom: 20px;
        }
    }

    .specify-input {
        justify-content: space-between;
        padding-right: 15px;

        & > div:first-child {
            flex-direction: row;
            width: 50%;

            label {
                margin-right: 10px;
            }
        }
    }

    .pad-bottom {
        padding-bottom: 24px;
    }

    .item-number {
        color: #a1a1a1;
        margin-right: 0;
    }
`;

export const SubSectionWrapper = styled.div`
    width: 100%;
    margin-bottom: 12px;
`;

export const SectionHeadingWrapper = styled.div`
    width: 100%;
    text-align: center;
    border-bottom: ${borderStyle};
    padding: 8px;

    h5, h6 {
        margin: 0;
    }

    h5 {
        font-size: 18px;
    }

    &.sub-section {
        background-color: #E3F1FF;
        border-top: ${borderStyle};
    }
`;

export const BorderedWrapper = styled.div`
    padding: 10px;

    &.amendment {
        border-left: ${borderStyle};
        border-right: ${borderStyle};
    }
`;

export const LineWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    align-items: flex-start;
    margin: 8px 0;

    &.header {
        margin: 0;
    }

    label, p.label {
        font-size: 14px;
        font-weight: 400;
    }

    label {
        margin-bottom: 4px;
    }

    input, .Select-placeholder {
        text-transform: uppercase !important;
    }

    .DayPickerInput {
        width: 100%;

        input {
            width: 100%;
            text-transform: unset;
            padding-top: 0;
        }
    }

    .period {
        display: flex;
        flex: 1;
        flex-direction: column;

        & > div {
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: space-between;

            span {
                margin-right: 5px;
            }

            & > div {
                width: 50%;
                margin-left: 16px;
            }
        }

        & > span {
            font-size: 14px;
        }
    }

    .input-text-center input {
        text-align: center;
    }

    .number input {
        text-align: right;
    }
`;

export const InputWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    padding: 0 15px;

    & > span {
        font-size: 14px;
        font-weight: 400;
    }

    &.columns2{
        width: 66%;
    }

    &.columns3{
        width: 100%;
    }

    &.section-title {
        padding: 0;
        font-weight: 600;
    }

    &.pad-left {
        padding-left: 18px;
    }

    &.switch {
        height: 60px;
        display: flex;
        flex-direction: column;
        justify-content: center;

        & > div {
            display: flex;
        }

        .switch-label {
            margin-right: 20px;
        }
    }

    &.horizontal {
        width: 100%;

        &.full > div {
            align-items: flex-end;
            justify-content: center;
        }

        & > div {
            display: flex;
            position: relative;
            padding-right: 15px;
            align-items: center;
        }

        input {
            width: calc(33% - 15px);
        }

        label {
            width: 67%;
            position: absolute;
            left: 0;
            padding-right: 20px;
            align-self: flex-start;
        }

        p {
            width: 33%;
            padding-left: 15px;
        }

        &.indented label {
            left: 20px;
        }
    }

    &.total-amount-overremittance input {
        width: 50%;
    }

    .label-tooltip {
        padding-left: 12px;
    }
`;

export const PageHeaderWrapper = styled.header`
    text-align: center;
    margin-bottom: 30px;

    & > div {
        text-align: left;
    }

    h3 {
        font-weight: 600;
        margin-bottom: 10px;
    }
`;

export const FormHeaderWrapper = styled.header`
    margin-bottom: 10px;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    justify-content: space-between;

    span {
        display: block;
    }

    ol {
        padding-left: 16px;
    }

    h3 {
        font-weight: 600;
    }

    .actions {
        white-space: nowrap;
    }
`;

export const NavWrapper = styled.nav`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 30px;
`;
