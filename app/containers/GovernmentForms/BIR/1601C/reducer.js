import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';
import {
    SET_LOADING,
    SET_DATA,
    SET_GENERATING,
    DOWNLOAD_READY,
    SET_URI,
    NOTIFICATION_SAGA,
    FORM_FIELDS
} from './constants';

const initialState = fromJS({
    options: {},
    data: {
        id: null,
        type: '',
        date: '',
        is_amended_return: false,
        any_taxes_withheld: true,
        no_of_attached_sheets: 0,
        tin: '',
        rdo: '',
        withholding_agents_name: '',
        registered_address: '',
        zip_code: '',
        contact_number: '',
        category_of_withholding_agent: 'PRIVATE',
        email_address: '',
        has_tax_relief: false,
        tax_relief_specification: '',
        total_amount_of_compensation: '',
        statutory_minimum_wage_for_mwe: '',
        other_pays_for_mwe: '',
        '13th_month_pay_and_other_benefits': '',
        de_minimis_benefits: '',
        employee_govt_contribution: '',
        other_non_taxable_compensation_amount: '',
        other_non_taxable_compensation_specify: '',
        total_non_taxable_compensation: '',
        total_taxable_compensation: '',
        taxable_compensation_not_subject_to_wtax: '',
        net_taxable_compensation: '',
        total_taxes_withheld: '',
        adjustment_of_wtax_prev_months: '',
        taxes_withheld_for_remittance: '',
        less_tax_remitted_in_return: '',
        other_remittances_made: '',
        total_tax_remittances_made: '',
        tax_still_due: '',
        penalty_surcharge: '',
        penalty_interest: '',
        penalty_compromise: '',
        total_penalties: '',
        total_amount_still_due_over_remittance: '',
        dop_cash_bank_drawee: '',
        dop_cash_bank_number: '',
        dop_cash_bank_date: '',
        dop_cash_bank_amount: '',
        dop_check_bank_drawee: '',
        dop_check_bank_number: '',
        dop_check_bank_date: '',
        dop_check_bank_amount: '',
        dop_tax_bank_number: '',
        dop_tax_bank_date: '',
        dop_tax_bank_amount: '',
        dop_other_bank_specifics: '',
        dop_other_bank_drawee: '',
        dop_other_bank_number: '',
        dop_other_bank_date: '',
        dop_other_bank_amount: '',
        for_individual: '',
        for_non_individual: '',
        tax_agent_acc_no: '',
        date_of_issue: '',
        date_of_expiry: ''
    },
    status: {
        loading: true,
        generating: false,
        downloadReady: false
    },
    downloadUrl: '',
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 * Parses data from API response
 * @param {Object} data - BIR 1601C data
 * @returns {Object}
 */
function parseData({ type, id, attributes }) {
    const months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];

    const isIndividual = attributes.for_individual !== '' && attributes.individual_title_designation !== '' && attributes.individual_tin !== '';
    const signatoryProps = isIndividual ? {
        organization_type: 'I',
        signatory: attributes.for_individual,
        signatory_position: attributes.individual_title_designation,
        signatory_tin: attributes.individual_tin
    } : {
        organization_type: 'NI',
        signatory: attributes.for_non_individual,
        signatory_position: attributes.non_individual_title_designation,
        signatory_tin: attributes.non_individual_tin
    };

    const processedDates = FORM_FIELDS.DATES.reduce( ( dates, key ) => ({
        ...dates,
        [ key ]: attributes[ key ] === '0000-00-00' ? '' : attributes[ key ]
    }), {});

    return {
        type,
        id,
        date: `${months[ attributes.month_from - 1 ]} ${attributes.year_from}`,
        ...signatoryProps,
        ...attributes,
        ...processedDates
    };
}

/**
 * BIR1601CFormsReducer
 */
function BIR1601CFormsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_DATA:
            return state.set( 'data', fromJS( parseData( action.payload ) ) );
        case DOWNLOAD_READY:
            return state.setIn([ 'status', 'downloadReady' ], action.payload );
        case SET_GENERATING:
            return state.setIn([ 'status', 'generating' ], action.payload );
        case SET_LOADING:
            return state.setIn([ 'status', 'loading' ], action.payload );
        case SET_URI:
            return state.set( 'downloadUrl', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default BIR1601CFormsReducer;
