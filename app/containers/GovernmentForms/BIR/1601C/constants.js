/*
 *
 * BIR 1601C Forms constants
 *
 */

const namespace = 'app/GovernmentForms/BIR/1601C';
export const INITIAL_DATA = `${namespace}/INITIAL_DATA`;
export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_DATA = `${namespace}/SET_DATA`;

export const GENERATE_FORMS = `${namespace}/GENERATE_FORMS`;
export const SET_GENERATING = `${namespace}/SET_GENERATING`;

export const DOWNLOAD_READY = `${namespace}/DOWNLOAD_READY`;
export const SET_URI = `${namespace}/SET_URI`;
export const SET_FORM_OPTIONS = `${namespace}/SET_FORM_OPTIONS`;

export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const REGENERATE_FORM = `${namespace}/REGENERATE_FORM`;
export const SAVE_AND_DOWNLOAD_FORM = `${namespace}/SAVE_AND_DOWNLOAD_FORM`;

export const FORM_FIELDS = {
    INPUTS: [
        '1',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '9A',
        '10',
        '12',
        '13A',
        '14',
        '15',
        '16',
        '17',
        '18',
        '19',
        '20',
        '20.1',
        '21',
        '22',
        '23',
        '24',
        '25',
        '26',
        '27',
        '28',
        '29',
        '29.1',
        '30',
        '31',
        '32',
        '33',
        '34',
        '35',
        '36',
        'signatory',
        'signatory_position',
        'signatory_tin',
        'tax_agent_account_number',
        'cash_drawee_bank',
        'cash_number',
        'cash_amount',
        'check_drawee_bank',
        'check_number',
        'check_amount',
        'tax_debit_memo_number',
        'tax_debit_memo_amount',
        'others_payment',
        'others_drawee_bank',
        'others_number',
        'others_amount'
    ],
    DATES: [
        'date_of_issue',
        'date_of_expiry',
        'dop_cash_bank_date',
        'dop_check_bank_date',
        'dop_tax_bank_date',
        'dop_other_bank_date'
    ],
    DETAILS_OF_PAYMENT: {
        CASH: {
            INPUTS: [
                'cash_drawee_bank',
                'cash_number'
            ],
            AMOUNTS: ['cash_amount'],
            DATES: ['dop_cash_bank_date']
        },
        CHECK: {
            INPUTS: [
                'check_drawee_bank',
                'check_number'
            ],
            AMOUNTS: ['check_amount'],
            DATES: ['dop_check_bank_date']
        },
        TAX_DEBIT_MEMO: {
            INPUTS: ['tax_debit_memo_number'],
            AMOUNTS: ['tax_debit_memo_amount'],
            DATES: ['dop_tax_bank_date']
        },
        OTHERS: {
            INPUTS: [
                'others_payment',
                'others_drawee_bank',
                'others_number'
            ],
            AMOUNTS: ['others_amount'],
            DATES: ['dop_other_bank_date']
        }
    }
};
