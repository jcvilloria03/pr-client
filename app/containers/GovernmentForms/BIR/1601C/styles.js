import styled, { keyframes } from 'styled-components';

export const MessageWrapperStyles = styled.div`
    display: flex;
    justify-content: center;
    height: 100vh;

    > div {
        display: flex;
        flex-direction: column;
        justify-content: center;

        i {
            font-size: 7em;
            align-self: center;
        }

        .description {
            padding-top: 40px;
            text-align: center;

            h3 {
                font-weight: 600;
            }
        }
    }
`;

export const FormsWrapper = styled.div`
    padding-bottom: 50px;

    input:disabled {
        border-color: #95989a;
        background: #f8f8f8;
        color: #55595c;

        & + .input-group-addon {
            border-color: #c7c7c7;
        }

        &~label {
            color: #5b5b5b;
        }
    }

    h3 {
        font-weight: 600;
        margin-bottom: 30px;
    }

    > div {
        width: 90%;
        margin: 20px auto 150px auto;

        div {
            &.heading {
                margin-bottom: 10px;
                display: flex;
                flex-direction: row;
                align-items: flex-end;
                justify-content: space-between;

                span {
                    display: block;
                }
            }

            &.sections {
                display: flex;
                flex-wrap: wrap;

                .section {
                    display: flex;
                    flex-wrap: wrap;
                    background: #fff;
                    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
                    padding: 40px 50px;
                    margin: 10px 0;
                    width: 100%;

                    .line-heading {
                        padding: 0 15px;
                        margin-bottom: 10px;

                        h3 {
                            margin-bottom: 20px;
                        }
                    }

                    .line {
                        display: flex;
                        flex-wrap: wrap;
                        width: 100%;
                        align-items: flex-start;

                        &.section-a {
                            background: #F7F9FA;
                            padding: 40px 20px 50px;
                            margin: 30px 0;

                            > div {
                                width: 100%;
                                display: flex;

                                > * {
                                    padding: 0 15px;

                                    &.flex2{
                                        width: 66%;
                                    }

                                    .label-tooltip {
                                        position: absolute;
                                        padding-left: 20px;
                                    }

                                    .radio-group {
                                        > div {
                                            flex: inherit !important;
                                        }
                                    }
                                }

                                .pad-bottom {
                                    padding-bottom: 24px;
                                }
                            }
                        }

                        &.trio {
                            align-items: flex-start;

                            > div {
                                width: 33%;
                            }
                        }

                        &.quad {
                            align-items: flex-start;

                            > div {
                                width: 25%;
                            }
                        }

                        label,
                        p.label {
                            font-size: 14px;
                            font-weight: 400;
                        }

                        label {
                            margin-bottom: 4px;
                        }

                        > div {
                            display: flex;
                            flex-direction: column;
                            justify-content: flex-end;

                            &.tax-relief {
                                width: 66%;

                                > div {
                                    display: flex;

                                    &:last-child > div {
                                        width: 50% !important;

                                        &:last-child > div {
                                            padding-left: 15px;
                                        }
                                    }

                                    .Select + p {
                                        padding-left: 15px;
                                    }
                                }
                            }
                        }

                        > * {
                            width: 33%;
                            padding: 0 15px;

                            &.flex2{
                                width: 66%;
                            }

                            .label-tooltip {
                                position: absolute;
                                padding-left: 20px;
                            }

                            .radio-group {
                                white-space: nowrap;

                                > div {
                                    flex: inherit !important;
                                }
                            }
                        }

                        .horizontal-input {
                            flex-direction: row;
                            align-items: center;

                            label {
                                margin-right: 10px;
                            }
                        }

                        input {
                            text-transform: uppercase;
                        }

                        .DayPickerInput input {
                            text-transform: unset;
                        }

                        .others {
                            white-space: nowrap;
                        }
                    }

                    .specify-input {
                        justify-content: space-between;
                        padding-right: 15px;

                        & > div:first-child {
                            flex-direction: row;
                            width: 50%;

                            label {
                                margin-right: 10px;
                            }
                        }
                    }

                    .pad-bottom {
                        padding-bottom: 24px;
                    }

                    .pad-left {
                        padding-left: 20px;
                    }
                }
            }
        }
    }

    .numberInput input, .number input {
        text-align: right;
    }
`;

export const InlineInputWrapper = styled.div`
    width: 100% !important;

    &.number input {
        text-align: right !important;
    }

    > div {
        display: flex;
        position: relative;
        padding-right: 15px;
        align-items: center;

        &.full {
            align-items: flex-end;
            justify-content: center;
        }

        > input {
            width: calc(33% - 15px);
        }

        > label {
            width: 34%;
            position: absolute;
            left: 0;
            padding-right: 20px;
            align-self: flex-start;
        }

        > p {
            width: 33%;
            padding-left: 15px;
        }

        &.indented > label {
            left: 10%;
        }

        #textAddLessAdjustments, #textLessTaxRemitted {
            & ~ label {
                width: 50%;
            }
        }
    }
`;

export const InlineDateWrapper = styled.div`
    > div {
        display: flex;
        flex-direction: column;
        width: 100%;

        > div.DayPickerInput > input {
            width: 100%;
        }
    }
`;

const bounce = keyframes`
    0%, 20%, 50%, 80%, 100% {
        transform: translate3d(0, 0, 0);
    }
    40% {
        transform: translate3d(0, -30px, 0);
    }
    60% {
        transform: translate3d(0, -15px, 0);
    }
`;

export const DownloadWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;

    .downloadWrapper {
        display: flex;
        flex-direction: column;
        justify-content: center;

        .svg-wrapper {
            align-self: center;

            > svg {
                width: 160px;
                height: auto;
            }

            &.animate > svg {
                animation: ${bounce} 2s infinite;
            }
        }

        .description {
            padding-top: 40px;
            text-align: center;

            a:not([href]):not([tabindex]) {
                color: #00A5E5;
            }
            h3 {
                font-weight: 600;
            }
            #downloadLink {
                display: none;
            }
        }
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;
