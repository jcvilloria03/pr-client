import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { delay } from 'redux-saga';
import moment from 'moment';
import decimal from 'js-big-decimal';
import { Container } from 'reactstrap';
import get from 'lodash/get';

import SnackBar from 'components/SnackBar';
import { H3 } from 'components/Typography';
import Input from 'components/Input';
import GovernmentNumbersInput from 'components/GovernmentNumbersInput';
import DatePicker from 'components/DatePicker';
import Button from 'components/Button';
import ToolTip from 'components/ToolTip';
import RadioGroup from 'components/RadioGroup';
import Radio from 'components/Radio';
import A from 'components/A';
import SalConfirm from 'components/SalConfirm';
import PageWrapper from 'containers/GovernmentForms/PageWrapper';
import { GOVERNMENT_FORM_KINDS } from 'containers/GovernmentForms/Generate/constants';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';
import {
    stripNonDigit,
    formatTIN,
    formatRDO,
    formatNumber,
    formatCurrencyToDecimalNotation,
    stripNonNumeric
} from 'utils/functions';

import SectionAView from './templates/sectionA';
import * as birFormActions from './actions';
import {
    makeSelectData,
    makeSelectStatus,
    makeSelectOptions,
    makeSelectDownloadUrl,
    makeSelectNotification
} from './selectors';

import {
    MessageWrapperStyles,
    FormsWrapper,
    InlineInputWrapper,
    InlineDateWrapper,
    DownloadWrapper,
    ConfirmBodyWrapperStyle,
    NavWrapper
} from './styles';

import { FORM_FIELDS } from './constants';

/**
 * BIR Form 1601C component
 */
export class BIR1601CForms extends React.PureComponent {
    static propTypes = {
        initializeData: React.PropTypes.func,
        saveAndDownloadForm: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        data: React.PropTypes.object,
        status: React.PropTypes.shape({
            loading: React.PropTypes.bool,
            generating: React.PropTypes.bool,
            downloadReady: React.PropTypes.bool
        }),
        downloadUrl: React.PropTypes.any,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        routeParams: React.PropTypes.object,
        regenerateForm: React.PropTypes.func,
        notify: React.PropTypes.func
    };

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            uri: '',
            showConfirmTaxWithheldNone: false,
            is_amended_return: null,
            any_taxes_withheld: null,
            has_tax_relief: null,
            details_required: {
                cash: false,
                check: false,
                tax_debit_memo: false,
                others: false
            }
        };

        this.generatedData = undefined;
        this.downloadFile = this.downloadFile.bind( this );
        this.onChange26 = this.onChange26.bind( this );
        this.recalcForm = this.recalcForm.bind( this );
    }

    /**
     * runs before render.
     * checks if there is a valid data to generate or if user is authorized to view this page.
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.government_form'], ( authorized ) => {
            if ( authorized ) {
                this.props.initializeData( this.props.routeParams.id );
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    /**
     * Perform actions when component props changes
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.downloadUrl !== this.props.downloadUrl
            && nextProps.downloadUrl
            && this.downloadFile( nextProps.downloadUrl );
    }

    /**
     * Called before component unmounts.
     */
    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * updates changes based on sectionA changes
     * @param value
     */
    onChange26( value ) {
        this[ '24' ] && this[ '24' ].setState(
            {
                value: formatNumber( value ),
                error: value === ''
            },
            () => this.recalcForm()
        );
    }

    /**
     * Download file to browser
     */
    downloadFile( url ) {
        const linkElement = document.getElementById( 'downloadLink' );
        linkElement.target = '_blank';
        linkElement.download = '';
        linkElement.href = url;
        delay( 200 );
        linkElement.click();
        delay( 1000 );
        linkElement.href = '';
    }

    /**
     * formats number into currency
     * @param value
     */
    formatCurrency( value ) {
        const formattedValue = formatCurrencyToDecimalNotation( value );

        const valueArray = formattedValue.split( '.' );

        valueArray[ 0 ] = `${valueArray[ 0 ].replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' )}`;

        return valueArray.length > 1 ? `${valueArray[ 0 ]}.${valueArray[ 1 ]}` : `${valueArray[ 0 ]}`;
    }

    /**
     * apply changes when any tax witheld is set to None
     */
    changeTaxWitheld( any ) {
        if ( !any ) {
            this[ '14' ].setState({ value: '0.00' });
            this[ '15' ].setState({ value: '0.00' });
            this[ '16' ].setState({ value: '0.00' });
            this[ '17' ].setState({ value: '0.00' });
            this[ '18' ].setState({ value: '0.00' });
            this[ '19' ].setState({ value: '0.00' });
            this[ '20' ].setState({ value: '0.00' });
            this[ '21' ].setState({ value: '0.00' });
            this[ '22' ].setState({ value: '0.00' });
            this[ '23' ].setState({ value: '0.00' });
            this[ '24' ].setState({ value: '0.00' });
            this[ '25' ].setState({ value: '0.00' });
            this[ '26' ].setState({ value: '0.00' });
            this[ '27' ].setState({ value: '0.00' });
            this[ '28' ].setState({ value: '0.00' });
            this[ '29' ].setState({ value: '0.00' });
            this[ '30' ].setState({ value: '0.00' });
            this[ '31' ].setState({ value: '0.00' });
            this[ '32' ].setState({ value: '0.00' });
            this[ '33' ].setState({ value: '0.00' });
            this[ '34' ].setState({ value: '0.00' });
            this[ '35' ].setState({ value: '0.00' });
            this[ '36' ].setState({ value: '0.00' });

            this.sectionA.setState({ sectionAEntries: [], totalAdjustment: 0 });
        }
    }

    /**
     * recalculates form values when one changes
     * @param  {string} refName ref name of element modified
     * @return {void}
     */
    recalcForm( refName = null ) {
        // GET MANUALLY INPUTTED VALUES
        const field14 = formatCurrencyToDecimalNotation( this[ '14' ].state.value || '0' );
        const field15 = formatCurrencyToDecimalNotation( this[ '15' ].state.value || '0' );
        const field16 = formatCurrencyToDecimalNotation( this[ '16' ].state.value || '0' );
        const field17 = formatCurrencyToDecimalNotation( this[ '17' ].state.value || '0' );
        const field18 = formatCurrencyToDecimalNotation( this[ '18' ].state.value || '0' );
        const field19 = formatCurrencyToDecimalNotation( this[ '19' ].state.value || '0' );
        const field20 = formatCurrencyToDecimalNotation( this[ '20' ].state.value || '0' );
        const field23 = formatCurrencyToDecimalNotation( this[ '23' ].state.value || '0' );
        const field25 = formatCurrencyToDecimalNotation( this[ '25' ].state.value || '0' );
        const field28 = formatCurrencyToDecimalNotation( this[ '28' ].state.value || '0' );
        const field29 = formatCurrencyToDecimalNotation( this[ '29' ].state.value || '0' );
        const field32 = formatCurrencyToDecimalNotation( this[ '32' ].state.value || '0' );
        const field33 = formatCurrencyToDecimalNotation( this[ '33' ].state.value || '0' );
        const field34 = formatCurrencyToDecimalNotation( this[ '34' ].state.value || '0' );

        const field26 = formatCurrencyToDecimalNotation( this[ '26' ].state.value || '0' );
        // CALCULATE AUTO COMPUTED VALUES
        const field21 = [ field15, field16, field17, field18, field19, field20 ].reduce( ( total, item ) => decimal.add( total, item ) );
        const field22 = decimal.subtract( field14, field21 );
        const field24 = decimal.subtract( field22, field23 );
        const field27 = decimal.add( field25, field26 );
        const field30 = decimal.add( field28, field29 );
        const field31 = decimal.subtract( field27, field30 );
        const field35 = decimal.add( field32, decimal.add( field33, field34 ) );
        const field36 = decimal.add( field31, field35 );

        // VALIDATE VALUES
        refName && this.validateAmount( refName );
        this.validateAmount( '21', field21 );
        this.validateAmount( '22', field22 );
        this.validateAmount( '24', field24, false );
        this.validateAmount( '27', field27 );
        this.validateAmount( '30', field30 );
        this.validateAmount( '31', field31 );
        this.validateAmount( '35', field35 );
        this.validateAmount( '36', field36 );

        // FORMAT AND DISPLAY NEW VALUES
        if ( refName && this[ refName ].state.value ) {
            this[ refName ].setState({ value: formatNumber( this[ refName ].state.value ) });
        }
        this[ '21' ].setState({ value: formatNumber( field21 ) });
        this[ '22' ].setState({ value: formatNumber( field22 ) });
        this[ '24' ].setState({ value: formatNumber( field24 ) });
        this[ '27' ].setState({ value: formatNumber( field27 ) });
        this[ '30' ].setState({ value: formatNumber( field30 ) });
        this[ '31' ].setState({ value: formatNumber( field31 ) });
        this[ '35' ].setState({ value: formatNumber( field35 ) });
        this[ '36' ].setState({ value: formatNumber( field36 ) });
    }

    /**
     * validates amount fields
     * @param field = field to be checked
     * @param value = value to test
     * @returns {boolean} returns true if valid. otherwise, false.
     */
    validateAmount( field, value = null, absolute = true ) {
        let valid = true;
        const eValue = value || this[ field ].state.value;
        if ( eValue ) {
            if ( this[ field ].state.errorMessage === 'This field cannot have a negative value.' || this[ field ].state.errorMessage === 'This field cannot exceed 25 characters.' || this[ field ].state.errorMessage === 'Please input a valid amount.' ) {
                this[ field ].state.error = false;
            }
            const hasError = this[ field ].state.error;
            if ( !hasError ) {
                const validAmount = !isNaN( formatCurrencyToDecimalNotation( eValue ) );
                const positiveAmount = Number( formatCurrencyToDecimalNotation( eValue ) ) >= 0;
                const validLength = eValue.length <= 25;

                if ( !validAmount ) {
                    valid = false;
                    this[ field ].setState({ error: true, errorMessage: 'Please input a valid amount.' });
                } else if ( !positiveAmount && absolute ) {
                    valid = false;
                    this[ field ].setState({ error: true, errorMessage: 'This field cannot have a negative value.' });
                } else if ( !validLength ) {
                    valid = false;
                    this[ field ].setState({ error: true, errorMessage: 'This field cannot exceed 25 characters.' });
                }
            } else {
                valid = false;
            }
        } else if ( this[ field ].props.required ) {
            this[ field ].setState({ error: true, errorMessage: 'This field is required' });
            valid = false;
        }

        return valid;
    }

    /**
     * Regenerate government form
     */
    regenerateForm = () => {
        const { data: propsData } = this.props;

        const data = {
            kind: GOVERNMENT_FORM_KINDS.BIR_1601C,
            company_id: propsData.company_id,
            month_from: propsData.month_from,
            year_from: propsData.year_from,
            month_to: propsData.month_to,
            year_to: propsData.year_to
        };

        this.props.regenerateForm({ data });
    }

    /**
     * Checks if a Details of Payment group has a field with value,
     * then flags that group as required. Otherwise, sets flag to not required
     * @param {string} group - One of 'CASH', 'CHECK', 'TAX_DEBIT_MEMO', 'OTHERS'
     */
    checkIfGroupHasInput( group ) {
        const fieldGroup = FORM_FIELDS.DETAILS_OF_PAYMENT[ group ];
        const groupKey = group.toLowerCase();

        if ( fieldGroup ) {
            const hasInput = fieldGroup.INPUTS.some( ( input ) => Boolean( get( this[ input ], 'state.value' ) ) );
            const hasAmount = fieldGroup.AMOUNTS.some( ( input ) => Boolean( Number( get( this[ input ], 'state.value' ) ) ) );
            const hasDate = fieldGroup.DATES.some( ( date ) => Boolean( get( this[ date ], 'state.selectedDay' ) ) );
            const required = hasInput || hasAmount || hasDate;

            this.setState( ( state ) => ({
                details_required: {
                    ...state.details_required,
                    [ groupKey ]: required
                }
            }), () => {
                if ( !this.state.details_required[ groupKey ]) {
                    // Clear `field required` errors
                    fieldGroup.INPUTS.forEach( ( input ) =>
                        this[ input ] && this[ input ].setState({
                            errorMessage: '',
                            error: false
                        })
                    );
                }
            });
        }
    }

    /**
     * Validates form before saving and downloading
     * @returns {Boolean}
     */
    validateForm() {
        let valid = true;

        FORM_FIELDS.INPUTS.forEach( ( input ) => {
            if ( this[ input ] && this[ input ]._validate( this[ input ].state.value ) ) {
                valid = false;
            }
        });

        FORM_FIELDS.DATES.forEach( ( date ) => {
            if ( this[ date ] && this[ date ].checkRequired() ) {
                valid = false;
            }
        });

        return valid;
    }

    /**
     * Sends a request to server to save and download form.
     */
    saveAndDownloadForm = () => {
        const signatories = this.organization_type.state.value === 'I' ? {
            for_individual: this.signatory.state.value,
            individual_title_designation: this.signatory_position.state.value,
            individual_tin: this.signatory_tin.state.value
        } : {
            for_non_individual: this.signatory.state.value,
            non_individual_title_designation: this.signatory_position.state.value,
            non_individual_tin: this.signatory_tin.state.value
        };

        if ( this.validateForm() ) {
            const data = {
                kind: GOVERNMENT_FORM_KINDS.BIR_1601C,
                company_id: this.props.data.company_id,
                attributes: {
                    is_amended_return: Boolean( parseInt( this[ '2' ].state.value, 10 ) ),
                    any_taxes_withheld: Boolean( parseInt( this[ '3' ].state.value, 10 ) ),
                    no_of_attached_sheets: Number( this[ '4' ].state.value ),
                    tin: this[ '6' ].state.value,
                    rdo: this[ '7' ].state.value,
                    withholding_agents_name: this[ '8' ].state.value,
                    registered_address: this[ '9' ].state.value,
                    zip_code: this[ '9A' ].state.value,
                    contact_number: this[ '10' ].state.value,
                    category_of_withholding_agent: this[ '11' ].state.value,
                    email_address: this[ '12' ].state.value,
                    has_tax_relief: Boolean( parseInt( this[ '13' ].state.value, 10 ) ),
                    tax_relief_specification: this[ '13A' ].state.value,
                    total_amount_of_compensation: stripNonDigit( this[ '14' ].state.value ),
                    statutory_minimum_wage_for_mwe: stripNonDigit( this[ '15' ].state.value ),
                    other_pays_for_mwe: stripNonDigit( this[ '16' ].state.value ),
                    '13th_month_pay_and_other_benefits': stripNonDigit( this[ '17' ].state.value ),
                    de_minimis_benefits: stripNonDigit( this[ '18' ].state.value ),
                    employee_govt_contribution: stripNonDigit( this[ '19' ].state.value ),
                    other_non_taxable_compensation_amount: stripNonDigit( this[ '20' ].state.value ),
                    other_non_taxable_compensation_specify: this[ '20.1' ].state.value,
                    total_non_taxable_compensation: stripNonDigit( this[ '21' ].state.value ),
                    total_taxable_compensation: stripNonDigit( this[ '22' ].state.value ),
                    taxable_compensation_not_subject_to_wtax: stripNonDigit( this[ '23' ].state.value ),
                    net_taxable_compensation: stripNonDigit( this[ '24' ].state.value ),
                    total_taxes_withheld: stripNonDigit( this[ '25' ].state.value ),
                    adjustment_of_wtax_prev_months: stripNonDigit( this[ '26' ].state.value ),
                    taxes_withheld_for_remittance: stripNonDigit( this[ '27' ].state.value ),
                    less_tax_remitted_in_return: stripNonDigit( this[ '28' ].state.value ),
                    other_remittances_made: stripNonDigit( this[ '29' ].state.value ),
                    other_remittances_made_specify: this[ '29.1' ].state.value,
                    total_tax_remittances_made: stripNonDigit( this[ '30' ].state.value ),
                    tax_still_due: stripNonDigit( this[ '31' ].state.value ),
                    penalty_surcharge: stripNonDigit( this[ '32' ].state.value ),
                    penalty_interest: stripNonDigit( this[ '33' ].state.value ),
                    penalty_compromise: stripNonDigit( this[ '34' ].state.value ),
                    total_penalties: stripNonDigit( this[ '35' ].state.value ),
                    total_amount_still_due_over_remittance: stripNonDigit( this[ '36' ].state.value ),
                    ...signatories,
                    tax_agent_acc_no: this.tax_agent_account_number.state.value,
                    date_of_issue: this.date_of_issue.state.selectedDay ? moment( this.date_of_issue.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    date_of_expiry: this.date_of_expiry.state.selectedDay ? moment( this.date_of_expiry.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    dop_cash_bank_drawee: this.cash_drawee_bank.state.value,
                    dop_cash_bank_number: this.cash_number.state.value,
                    dop_cash_bank_date: this.dop_cash_bank_date.state.selectedDay ? moment( this.dop_cash_bank_date.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    dop_cash_bank_amount: stripNonDigit( this.cash_amount.state.value ),
                    dop_check_bank_drawee: this.check_drawee_bank.state.value,
                    dop_check_bank_number: this.check_number.state.value,
                    dop_check_bank_date: this.dop_check_bank_date.state.selectedDay ? moment( this.dop_check_bank_date.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    dop_check_bank_amount: stripNonDigit( this.check_amount.state.value ),
                    dop_tax_bank_number: this.tax_debit_memo_number.state.value,
                    dop_tax_bank_date: this.dop_tax_bank_date.state.selectedDay ? moment( this.dop_tax_bank_date.state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    dop_tax_bank_amount: stripNonDigit( this.tax_debit_memo_amount.state.value ),
                    dop_other_bank_specifics: this.others_payment.state.value,
                    dop_other_bank_drawee: this.others_drawee_bank.state.value,
                    dop_other_bank_number: this.others_number.state.value,
                    dop_other_bank_date: this.dop_other_bank_date.state.selectedDay ? moment( this[ '31C' ].state.selectedDay ).format( 'YYYY[-]MM[-]DD' ) : '',
                    dop_other_bank_amount: stripNonDigit( this.others_amount.state.value )
                }
            };

            // Check if all 'Details of Payment' are flagged as not required
            const isDetailsOfPaymentEmpty = Object.keys( this.state.details_required ).every( ( group ) => !this.state.details_required[ group ]);

            if ( isDetailsOfPaymentEmpty ) {
                this.props.notify({
                    show: true,
                    title: 'Error',
                    message: 'Payment Detail is required.',
                    type: 'error'
                });
            } else {
                this.props.saveAndDownloadForm({ id: this.props.data.id, data });
            }
        } else {
            this.props.notify({
                show: true,
                title: 'Error',
                message: 'Missing required fields.',
                type: 'error'
            });
        }
    }

    /**
     * renders a loading screen
     */
    renderLoadingPage( generating = false ) {
        return (
            <MessageWrapperStyles>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        <H3>{ generating ? 'Generating.' : 'Loading data.' }  Please wait...</H3>
                    </div>
                </div>
            </MessageWrapperStyles>
        );
    }

    /**
     * renders header section
    */
    renderHeaderSection() {
        const { data } = this.props;

        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Monthly Remittance Return of Income Taxes Withheld on Compensation 1601 C</H3>
                    <p>Please fill out the fields below to complete the form.</p>
                </div>
                <div className="line">
                    <div>
                        <Input
                            label={
                                <span>
                                    <span>1. For the month</span>
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipPeriod" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Previously closed payrolls in the previous month
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="selectPeriod"
                            disabled
                            value={ data.date }
                            ref={ ( ref ) => { this[ '1' ] = ref; } }
                        />
                    </div>
                    <div style={ { paddingBottom: '12px' } }>
                        <label htmlFor="radioAmendmentReturn">
                            2. Amendment Return?
                            <span className="label-tooltip">
                                <ToolTip id="tooltipAmendmentReturn" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                    No if the return is not an amendment for that period.<br />
                                    Yes if the return is an amendment for that period
                                </ToolTip>
                            </span>
                        </label>
                        <RadioGroup
                            id="radioAmendmentReturn"
                            value={ `${Number( this.state.is_amended_return || data.is_amended_return )}` }
                            horizontal
                            className="radio-group"
                            onChange={ ( value ) => {
                                this.setState({ is_amended_return: value });
                                this.recalcForm();
                            } }
                            ref={ ( ref ) => { this[ '2' ] = ref; } }
                        >
                            <Radio value="1">Yes</Radio>
                            <Radio value="0">No</Radio>
                        </RadioGroup>
                    </div>
                    <div>
                        <label htmlFor="radioTaxesWithheld">
                            3. Any Taxes Withheld?
                            <span className="label-tooltip">
                                <ToolTip id="tooltipTaxesWithheldTip" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                    Set to yes, unless the total withholding tax is &quot;0&quot;.
                                </ToolTip>
                            </span>
                        </label>
                        <RadioGroup
                            id="radioTaxesWithheld"
                            value={ `${Number( this.state.any_taxes_withheld || data.any_taxes_withheld )}` }
                            horizontal
                            className="radio-group"
                            onChange={ ( value ) => {
                                if ( Number( value ) ) {
                                    this.setState({ any_taxes_withheld: '1' });
                                    this.changeTaxWitheld( true );
                                } else {
                                    this.setState({ showConfirmTaxWithheldNone: false }, () =>
                                        this.setState({ showConfirmTaxWithheldNone: true })
                                    );
                                }
                            } }
                            ref={ ( ref ) => { this[ '3' ] = ref; } }
                        >
                            <Radio value="1">Yes</Radio>
                            <Radio value="0">No</Radio>
                        </RadioGroup>
                    </div>
                </div>
                <div className="line">
                    <div className="number">
                        <Input
                            id="textNumberOfSheets"
                            label={
                                <span>
                                    4. Number of sheets attached
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipNumberOfSheets" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Number of Sheet/s Attached
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ `${data.no_of_attached_sheets}` }
                            type="number"
                            max={ 2 }
                            onChange={ ( value ) => this[ '4' ].setState({ value: parseInt( stripNonNumeric( value ), 10 ).toString() }) }
                            ref={ ( ref ) => { this[ '4' ] = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            id="textATC"
                            label="5. Alphanumeric Tax Code (ATC)"
                            disabled
                            value="WW010"
                            ref={ ( ref ) => { this[ '5' ] = ref; } }
                        />
                    </div>
                </div>
            </div>
        );
    }

    /**
     * renders part 1 section to DOM
     */
    renderPart1Section() {
        const { data } = this.props;

        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Part 1: Background Information</H3>
                </div>
                <div className="line pad-bottom">
                    <div>
                        <GovernmentNumbersInput
                            id="employer_tin"
                            type="tin"
                            label={
                                <span>
                                    6. TIN
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipTIN" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Taxpayer Identification Number
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ data.tin || '' }
                            onChange={ ( value ) => { this[ '6' ].setState({ value: formatTIN( value ) }); } }
                            ref={ ( ref ) => { this[ '6' ] = ref; } }
                        />
                    </div>
                    <div>
                        <GovernmentNumbersInput
                            id="textRDO"
                            type="rdo"
                            label={
                                <span>
                                    7. RDO code
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipRDO" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            This is already defined in the company setup
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ data.rdo || '' }
                            onChange={ ( value ) => this[ '7' ].setState({ value: formatRDO( value ) }) }
                            ref={ ( ref ) => { this[ '7' ] = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            label={
                                <span>
                                    8. Withholding Agent&#39;s Name
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipAgent" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Please ensure that the Registered Business name is entered in this field
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            id="textAgent"
                            value={ data.withholding_agents_name || '' }
                            ref={ ( ref ) => { this[ '8' ] = ref; } }
                            max={ 50 }
                        />
                    </div>
                </div>
                <div className="line pad-bottom">
                    <div className="flex2">
                        <Input
                            label="9. Registered Address"
                            id="textAddress"
                            value={ data.registered_address || '' }
                            ref={ ( ref ) => { this[ '9' ] = ref; } }
                            max={ 150 }
                        />
                    </div>
                    <div>
                        <Input
                            label="9A. ZIP Code"
                            id="textZipCode"
                            value={ data.zip_code || '' }
                            ref={ ( ref ) => { this[ '9A' ] = ref; } }
                            max={ 4 }
                            type="number"
                        />
                    </div>
                </div>
                <div className="line">
                    <div>
                        <Input
                            id="textContactNumber"
                            label="10. Contact Number"
                            value={ data.contact_number || '' }
                            onChange={ ( value ) => this[ '10' ].setState({ value: stripNonDigit( value ) }) }
                            ref={ ( ref ) => { this[ '10' ] = ref; } }
                        />
                    </div>
                    <div style={ { paddingBottom: '12px' } }>
                        <label htmlFor="radioAgentCategory">11. Category of Withholding Agent</label>
                        <RadioGroup
                            id="radioAgentCategory"
                            value={ data.category_of_withholding_agent || 'PRIVATE' }
                            horizontal
                            className="radio-group"
                            ref={ ( ref ) => { this[ '11' ] = ref; } }
                        >
                            <Radio value="PRIVATE">Private</Radio>
                            <Radio value="GOVERNMENT">Government</Radio>
                        </RadioGroup>
                    </div>
                    <div>
                        <Input
                            id="textEmail"
                            label="12. Email Address"
                            value={ data.email_address || '' }
                            type="email"
                            ref={ ( ref ) => { this[ '12' ] = ref; } }
                        />
                    </div>
                </div>
                <div className="line">
                    <div className="flex2" style={ { paddingBottom: '12px' } }>
                        <label htmlFor="radioTaxRelief">
                            13. Are there payees availing of tax relief under Special Law or International Tax Treaty?
                        </label>
                        <RadioGroup
                            id="radioTaxRelief"
                            value={ `${Number( this.state.has_tax_relief || data.has_tax_relief )}` }
                            horizontal
                            className="radio-group"
                            onChange={ ( value ) => {
                                this.setState({ has_tax_relief: value });
                                this[ '13A' ].setState({ disabled: !Number( value ), value: '' });
                            } }
                            ref={ ( ref ) => { this[ '13' ] = ref; } }
                        >
                            <Radio value="1">Yes</Radio>
                            <Radio value="0">No</Radio>
                        </RadioGroup>
                    </div>
                    <div>
                        <Input
                            id="textTaxReliefReason"
                            label="13A. If yes, specify"
                            value={ data.tax_relief_specification || '' }
                            max={ 60 }
                            ref={ ( ref ) => { this[ '13A' ] = ref; } }
                        />
                    </div>
                </div>
            </div>
        );
    }

    /**
     * renders part 2 section to DOM
     */
    renderPart2Section() {
        const { data } = this.props;
        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Part 2: Computation of Tax</H3>
                </div>
                <div className="line">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textCompensationAmount"
                            className="full"
                            label={
                                <span>
                                    14. Total Amount of Compensation
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipCompensationAmount" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Gross compensation paid to the employees for the month.
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.total_amount_of_compensation || 0 ) }
                            onChange={ ( value ) => this[ '14' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '14' ) }
                            onFocus={ () => this[ '14' ].setState({ value: stripNonDigit( this[ '14' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '14' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-left">
                    <p className="label">Less: Non-Taxable/Exempt compensation</p>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textMWE"
                            className="full"
                            label={
                                <span>
                                    15. Statutory Minimum Wage for Minimum Wage Earners <i>(MWEs)</i>
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipMWE" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Minimum wage earners’ income less mandatory contributions
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.statutory_minimum_wage_for_mwe || 0 ) }
                            onChange={ ( value ) => this[ '15' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '15' ) }
                            onFocus={ () => this[ '15' ].setState({ value: stripNonDigit( this[ '15' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '15' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textHolidayPay"
                            className="full"
                            label={
                                <span>
                                    16. Holiday Pay, Overtime Pay, Night Shift Differential, Hazard Pay
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipHolidayPay" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            For MWEs only
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.other_pays_for_mwe || 0 ) }
                            onChange={ ( value ) => this[ '16' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '16' ) }
                            onFocus={ () => this[ '16' ].setState({ value: stripNonDigit( this[ '16' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '16' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="text13thMonth"
                            className="full"
                            label="17. 13th Month Pay and Other Benefits"
                            value={ this.formatCurrency( data[ '13th_month_pay_and_other_benefits' ] || 0 ) }
                            onChange={ ( value ) => this[ '17' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '17' ) }
                            onFocus={ () => this[ '17' ].setState({ value: stripNonDigit( this[ '17' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '17' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textDeMinimis"
                            className="full"
                            label="18. De Minimis Benefits"
                            value={ this.formatCurrency( data.de_minimis_benefits || 0 ) }
                            onChange={ ( value ) => this[ '18' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '18' ) }
                            onFocus={ () => this[ '18' ].setState({ value: stripNonDigit( this[ '18' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '18' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textGovtContributions"
                            className="full"
                            label={
                                <span>
                                    19. SSS, GSIS, PHIC, HDMF Mandatory Contributions &#38; Union Dues
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipDeMinimis" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Employee&#39;s share only
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.employee_govt_contribution || 0 ) }
                            onChange={ ( value ) => this[ '19' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '19' ) }
                            onFocus={ () => this[ '19' ].setState({ value: stripNonDigit( this[ '19' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '19' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line specify-input pad-bottom">
                    <Input
                        id="textOtherNonTaxableCompensation"
                        label={ <span>20. Other Non-Taxable Compensation <i>(specify)</i></span> }
                        value={ data.other_non_taxable_compensation_specify || '' }
                        ref={ ( ref ) => { this[ '20.1' ] = ref; } }
                    />
                    <Input
                        id="textOtherNonTaxableCompensationAmount"
                        className="number"
                        value={ this.formatCurrency( data.other_non_taxable_compensation_amount || 0 ) }
                        onChange={ ( value ) => this[ '20' ].setState({ value: stripNonDigit( value ) }) }
                        onBlur={ () => this.recalcForm( '20' ) }
                        onFocus={ () => this[ '20' ].setState({ value: stripNonDigit( this[ '20' ].state.value ) }) }
                        ref={ ( ref ) => { this[ '20' ] = ref; } }
                    />
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textTotalNonTaxableCompensation"
                            className="full"
                            label={
                                <span>
                                    21. Total Non-Taxable Compensation
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipTotalNonTaxableCompensation" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Sum of items 15 to 20
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ formatNumber( data.total_non_taxable_compensation || 0 ) }
                            ref={ ( ref ) => { this[ '21' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textTaxableCompensation"
                            className="full"
                            label={
                                <span>
                                    22. Total Taxable Compensation
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipTaxableCompensation" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Item 14 Less Item 21
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ formatNumber( data.total_taxable_compensation || 0 ) }
                            ref={ ( ref ) => { this[ '22' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textLessTaxableCompensation"
                            className="full"
                            label={
                                <span>
                                    23. Less: Taxable compensation not subject to withholding tax
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipLessTaxableCompensation" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            For employees, other than MWEs, receiving Php 250,000 &#38; below for the year
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.taxable_compensation_not_subject_to_wtax || 0 ) }
                            onChange={ ( value ) => this[ '23' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '23' ) }
                            onFocus={ () => this[ '23' ].setState({ value: stripNonDigit( this[ '23' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '23' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textNetTaxableCompensation"
                            className="full"
                            label={
                                <span>
                                    24. Net Taxable Compensation
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipNetTaxableCompensation" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Item 22 Less Item 23
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ formatNumber( data.net_taxable_compensation || 0 ) }
                            ref={ ( ref ) => { this[ '24' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textTotalTaxesWithheld"
                            className="full"
                            label="25. Total Taxes Withheld"
                            value={ this.formatCurrency( data.total_taxes_withheld || 0 ) }
                            onChange={ ( value ) => this[ '25' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '25' ) }
                            onFocus={ () => this[ '25' ].setState({ value: stripNonDigit( this[ '25' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '25' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textAddLessAdjustments"
                            className="full"
                            label={
                                <span>
                                    26. Add/(Less): Adjustment of Taxes Withheld from Previous Month/s
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipAddLessAdjustments" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            From Part IV-Schedule 1, Item 4
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ formatNumber( data.adjustment_of_wtax_prev_months || 0 ) }
                            onChange={ ( value ) => this[ '26' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '26' ) }
                            onFocus={ () => this[ '26' ].setState({ value: stripNonDigit( this[ '26' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '26' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>

                <SectionAView disabled onChange26={ this.onChange26 } ref={ ( ref ) => { this.sectionA = ref; } } />

                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textTaxRemittance"
                            className="full"
                            label={
                                <span>
                                    27. Taxes Withheld for Remittance
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipTaxRemittance" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Sum of Items 25 and 26
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.taxes_withheld_for_remittance || 0 ) }
                            ref={ ( ref ) => { this[ '27' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textLessTaxRemitted"
                            className="full"
                            label="28. Less: Tax Remitted in Return Previously Filed, if there is an amendment return"
                            value={ this.formatCurrency( data.less_tax_remitted_in_return || 0 ) }
                            onChange={ ( value ) => this[ '28' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '28' ) }
                            onFocus={ () => this[ '28' ].setState({ value: stripNonDigit( this[ '28' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '28' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line specify-input pad-bottom">
                    <Input
                        id="textOtherRemittances"
                        label={ <span>29. Other Remittances Made <i>(specify)</i></span> }
                        value={ data.other_remittances_made_specify || '' }
                        ref={ ( ref ) => { this[ '29.1' ] = ref; } }
                    />
                    <Input
                        id="textOtherRemittancesAmount"
                        className="number"
                        value={ this.formatCurrency( data.other_remittances_made || 0 ) }
                        onChange={ ( value ) => this[ '29' ].setState({ value: stripNonDigit( value ) }) }
                        onBlur={ () => this.recalcForm( '29' ) }
                        onFocus={ () => this[ '29' ].setState({ value: stripNonDigit( this[ '29' ].state.value ) }) }
                        ref={ ( ref ) => { this[ '29' ] = ref; } }
                    />
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textTotalTaxPayment"
                            className="full"
                            label={
                                <span>
                                    30. Total Tax Remittances Made
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipTotalTaxPayment" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Sum of Items 28 and 29
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.total_tax_remittances_made || 0 ) }
                            ref={ ( ref ) => { this[ '30' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line">
                    <InlineInputWrapper className="number" >
                        <Input
                            id="textTaxDue"
                            className="full"
                            label={
                                <span>
                                    31. Tax Still Due/(Over-remittance)
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipTaxDue" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Item 27 Less Item 30
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.tax_still_due || 0 ) }
                            ref={ ( ref ) => { this[ '31' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-left">
                    <p className="label">Add: Penalties</p>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number">
                        <Input
                            id="textSurcharge"
                            className="full indented"
                            label={
                                <span>
                                    32. Surcharge
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipSurcharge" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            There shall be imposed, in addition to the tax required to be paid, a penalty equivalent to twenty-five percent (25%) of the amount due
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.penalty_surcharge || 0 ) }
                            onChange={ ( value ) => this[ '32' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '32' ) }
                            onFocus={ () => this[ '32' ].setState({ value: stripNonDigit( this[ '32' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '32' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number">
                        <Input
                            id="textInterest"
                            className="full indented"
                            label={
                                <span>
                                    33. Interest
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipInterest" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            There shall be assessed and collected on any unpaid amount of tax, interest at the rate of twenty percent (20%) per annum
                                            , or such higher rate as may be prescribed by rules and regulations, from the date prescribed for payment until the amount is fully paid.
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.penalty_interest || 0 ) }
                            onChange={ ( value ) => this[ '33' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '33' ) }
                            onFocus={ () => this[ '33' ].setState({ value: stripNonDigit( this[ '33' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '33' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number">
                        <Input
                            id="textCompromise"
                            className="full indented"
                            label={
                                <span>
                                    34. Compromise
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipCompromise" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            NIRC SEC. 255. Failure to File Return, Supply Correct and Accurate Information, Pay Tax Withhold and Remit Tax and Refund Excess Taxes Withheld on Compensation
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.penalty_compromise || 0 ) }
                            onChange={ ( value ) => this[ '34' ].setState({ value: stripNonDigit( value ) }) }
                            onBlur={ () => this.recalcForm( '34' ) }
                            onFocus={ () => this[ '34' ].setState({ value: stripNonDigit( this[ '34' ].state.value ) }) }
                            ref={ ( ref ) => { this[ '34' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line pad-bottom">
                    <InlineInputWrapper className="number">
                        <Input
                            id="textPenalties"
                            className="full indented"
                            label={
                                <span>
                                    35. Total Penalties
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipPenalties" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Sum of Items 32 to 34
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.total_penalties || 0 ) }
                            ref={ ( ref ) => { this[ '35' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
                <div className="line">
                    <InlineInputWrapper className="number">
                        <Input
                            id="textTaxStillDue"
                            className="full"
                            label={
                                <span>
                                    36. TOTAL AMOUNT STILL DUE/(Over-remittance)
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipTaxStillDue" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Sum of Items 31 and item 35
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ this.formatCurrency( data.total_amount_still_due_over_remittance || 0 ) }
                            ref={ ( ref ) => { this[ '36' ] = ref; } }
                        />
                    </InlineInputWrapper>
                </div>
            </div>
        );
    }

    /**
     * renders signatories section to DOM
     */
    renderSignatoriesSection() {
        const { data } = this.props;

        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Signatories</H3>
                    <p>
                        I/We declare, under the penalties of perjury, that this remittance return,&nbsp;
                        and all its attachments, have been made in good faith, verified by me/us,&nbsp;
                        and to the best of my/our knowledge and belief, is true and correct,&nbsp;
                        pursuant to the provisions of the National Internal Revenue Code, as amended,&nbsp;
                        and the regulations issued under authority thereof.&nbsp;
                        Further, I give my consent to the processing of my information as contemplated under the&nbsp;
                        *Data Privacy Act of 2012 (R.A. No. 10173) for legitimate and lawful purposes.&nbsp;
                        <i>(If Authorized Representative, attach authorization letter)</i>
                    </p>
                </div>
                <div className="line">
                    <div>
                        <label htmlFor="radioTaxRelief">Organization Type</label>
                        <RadioGroup
                            id="radioOrganizationType"
                            value={ data.organization_type }
                            horizontal
                            className="radio-group"
                            ref={ ( ref ) => { this.organization_type = ref; } }
                        >
                            <Radio value="I">Individual</Radio>
                            <Radio value="NI">Non-Individual</Radio>
                        </RadioGroup>
                    </div>
                </div>
                <div className="line pad-bottom">
                    <div>
                        <Input
                            id="textSignatory"
                            label="Authorized Signatory"
                            value={ data.signatory || '' }
                            ref={ ( ref ) => { this.signatory = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            id="textSignatoryPosition"
                            label="Authorized Signatory Designation"
                            value={ data.signatory_position || '' }
                            ref={ ( ref ) => { this.signatory_position = ref; } }
                        />
                    </div>
                    <div>
                        <GovernmentNumbersInput
                            id="textSignatoryTIN"
                            type="tin"
                            label="Authorized Signatory TIN"
                            value={ data.signatory_tin || '' }
                            onChange={ ( value ) => { this.signatory_tin.setState({ value: formatTIN( value ) }); } }
                            ref={ ( ref ) => { this.signatory_tin = ref; } }
                        />
                    </div>
                </div>
                <div className="line">
                    <div>
                        <Input
                            id="textTaxAgentAccount"
                            label={
                                <span>
                                    Tax Agent Accreditation No./Attorney&#39;s Roll Number
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipTaxAgentAccount" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            if applicable
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            value={ data.tax_agent_acc_no || '' }
                            ref={ ( ref ) => { this.tax_agent_account_number = ref; } }
                        />
                    </div>
                    <InlineDateWrapper>
                        <DatePicker
                            id="dateSignatoryDateOfIssue"
                            label="Date of Issue"
                            placeholder="Choose a date"
                            dayFormat="MMMM DD, YYYY"
                            selectedDay={ data.date_of_issue }
                            ref={ ( ref ) => { this.date_of_issue = ref; } }
                        />
                    </InlineDateWrapper>
                    <InlineDateWrapper>
                        <DatePicker
                            id="dateSignatoryDateOfExpiry"
                            label="Date of Expiry"
                            placeholder="Choose a date"
                            dayFormat="MMMM DD, YYYY"
                            selectedDay={ data.date_of_expiry }
                            ref={ ( ref ) => { this.date_of_expiry = ref; } }
                        />
                    </InlineDateWrapper>
                </div>
            </div>
        );
    }

    /**
     * renders part 3 section to DOM
     */
    renderPart3Section() {
        const { data } = this.props;
        const { details_required: detailsRequired } = this.state;

        return (
            <div className="section">
                <div className="line-heading">
                    <H3>Part 3: Details of Payment</H3>
                    <p className="label">Cash/Bank Debit Memo</p>
                </div>
                <div className="line trio">
                    <div>
                        <Input
                            id="textCash"
                            label="Drawee Bank/Agency"
                            required={ detailsRequired.cash }
                            value={ data.dop_cash_bank_drawee || '' }
                            onBlur={ () => this.checkIfGroupHasInput( 'CASH' ) }
                            ref={ ( ref ) => { this.cash_drawee_bank = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            id="textCashPaymentNumber"
                            label={
                                <span>
                                    Number
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipCashPaymentNumber" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Bank Debit Memo Number
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            required={ detailsRequired.cash }
                            value={ data.dop_cash_bank_number || '' }
                            onBlur={ () => this.checkIfGroupHasInput( 'CASH' ) }
                            ref={ ( ref ) => { this.cash_number = ref; } }
                        />
                    </div>
                    <InlineDateWrapper>
                        <DatePicker
                            id="dateCashPaymentDate"
                            label="Date"
                            placeholder="Choose a date"
                            dayFormat="MMMM DD, YYYY"
                            required={ detailsRequired.cash }
                            selectedDay={ data.dop_cash_bank_date }
                            onChange={ () => this.checkIfGroupHasInput( 'CASH' ) }
                            ref={ ( ref ) => { this.dop_cash_bank_date = ref; } }
                        />
                    </InlineDateWrapper>
                </div>
                <div className="line pad-bottom">
                    <Input
                        id="textCashAmount"
                        className="number"
                        label="Amount"
                        required={ detailsRequired.cash }
                        value={ data.dop_cash_bank_amount || '' }
                        onChange={ ( value ) => this.cash_amount.setState({ value: stripNonDigit( value ) }) }
                        onBlur={ ( value ) => {
                            const isNegative = Number( formatCurrencyToDecimalNotation( value ) ) < 0;
                            this.cash_amount.setState({
                                value: this.formatCurrency( value ),
                                error: isNegative || this.cash_amount.state.error,
                                errorMessage: isNegative ? 'This field cannot have a negative value.' : this.cash_amount.state.errorMessage
                            }, () =>
                                this.checkIfGroupHasInput( 'CASH' )
                            );
                        } }
                        ref={ ( ref ) => { this.cash_amount = ref; } }
                    />
                </div>

                <div className="line-heading">
                    <p className="label">Check</p>
                </div>
                <div className="line trio">
                    <div>
                        <Input
                            id="textCheck"
                            label="Drawee Bank/Agency"
                            required={ detailsRequired.check }
                            value={ data.dop_check_bank_drawee || '' }
                            onBlur={ () => this.checkIfGroupHasInput( 'CHECK' ) }
                            ref={ ( ref ) => { this.check_drawee_bank = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            id="textCheckPaymentNumber"
                            label={
                                <span>
                                    Number
                                    <span className="label-tooltip">
                                        <ToolTip id="tooltipCheckPaymentNumber" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Check number
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            required={ detailsRequired.check }
                            value={ data.dop_check_bank_number || '' }
                            onBlur={ () => this.checkIfGroupHasInput( 'CHECK' ) }
                            ref={ ( ref ) => { this.check_number = ref; } }
                        />
                    </div>
                    <InlineDateWrapper>
                        <DatePicker
                            id="dateCheckPaymentDate"
                            label="Date"
                            placeholder="Choose a date"
                            dayFormat="MMMM DD, YYYY"
                            required={ detailsRequired.check }
                            selectedDay={ data.dop_check_bank_date }
                            onChange={ () => this.checkIfGroupHasInput( 'CHECK' ) }
                            ref={ ( ref ) => { this.dop_check_bank_date = ref; } }
                        />
                    </InlineDateWrapper>
                </div>
                <div className="line pad-bottom">
                    <Input
                        id="textCheckAmount"
                        className="number"
                        label="Amount"
                        required={ detailsRequired.check }
                        value={ data.dop_check_bank_amount || '' }
                        onChange={ ( value ) => this.check_amount.setState({ value: stripNonDigit( value ) }) }
                        onBlur={ ( value ) => {
                            const isNegative = Number( formatCurrencyToDecimalNotation( value ) ) < 0;
                            this.check_amount.setState({
                                value: this.formatCurrency( value ),
                                error: isNegative || this.check_amount.state.error,
                                errorMessage: isNegative ? 'This field cannot have a negative value.' : this.check_amount.state.errorMessage
                            }, () =>
                                this.checkIfGroupHasInput( 'CHECK' )
                            );
                        } }
                        ref={ ( ref ) => { this.check_amount = ref; } }
                    />
                </div>

                <div className="line-heading">
                    <p className="label">Tax Debit Memo</p>
                </div>
                <div className="line trio">
                    <div>
                        <Input
                            id="textTaxDebitMemoNumber"
                            label="Number"
                            required={ detailsRequired.tax_debit_memo }
                            value={ data.dop_tax_bank_number || '' }
                            onBlur={ () => this.checkIfGroupHasInput( 'TAX_DEBIT_MEMO' ) }
                            ref={ ( ref ) => { this.tax_debit_memo_number = ref; } }
                        />
                    </div>
                    <InlineDateWrapper>
                        <DatePicker
                            id="dateTaxBankDate"
                            label="Date"
                            placeholder="Choose a date"
                            dayFormat="MMMM DD, YYYY"
                            required={ detailsRequired.tax_debit_memo }
                            selectedDay={ data.dop_tax_bank_date }
                            onChange={ () => this.checkIfGroupHasInput( 'TAX_DEBIT_MEMO' ) }
                            ref={ ( ref ) => { this.dop_tax_bank_date = ref; } }
                        />
                    </InlineDateWrapper>
                    <Input
                        id="textTaxBankAmount"
                        className="number"
                        label="Amount"
                        required={ detailsRequired.tax_debit_memo }
                        value={ data.dop_tax_bank_amount || '' }
                        onChange={ ( value ) => this.tax_debit_memo_amount.setState({ value: stripNonDigit( value ) }) }
                        onBlur={ ( value ) => {
                            const isNegative = Number( formatCurrencyToDecimalNotation( value ) ) < 0;
                            this.tax_debit_memo_amount.setState({
                                value: this.formatCurrency( value ),
                                error: isNegative || this.tax_debit_memo_amount.state.error,
                                errorMessage: isNegative ? 'This field cannot have a negative value.' : this.tax_debit_memo_amount.state.errorMessage
                            }, () =>
                                this.checkIfGroupHasInput( 'TAX_DEBIT_MEMO' )
                            );
                        } }
                        ref={ ( ref ) => { this.tax_debit_memo_amount = ref; } }
                    />
                </div>

                <div className="line pad-bottom">
                    <Input
                        id="textOtherPayment"
                        className="horizontal-input others"
                        label="Others"
                        required={ detailsRequired.others }
                        value={ data.dop_other_bank_specifics || '' }
                        onBlur={ () => this.checkIfGroupHasInput( 'OTHERS' ) }
                        ref={ ( ref ) => { this.others_payment = ref; } }
                    />
                </div>
                <div className="line quad">
                    <div>
                        <Input
                            id="textOthersBank"
                            label="Drawee Bank/Agency"
                            required={ detailsRequired.others }
                            value={ data.dop_other_bank_drawee || '' }
                            onBlur={ () => this.checkIfGroupHasInput( 'OTHERS' ) }
                            ref={ ( ref ) => { this.others_drawee_bank = ref; } }
                        />
                    </div>
                    <div>
                        <Input
                            id="textOthersPaymentNumber"
                            label="Number"
                            required={ detailsRequired.others }
                            value={ data.dop_other_bank_number || '' }
                            onBlur={ () => this.checkIfGroupHasInput( 'OTHERS' ) }
                            ref={ ( ref ) => { this.others_number = ref; } }
                        />
                    </div>
                    <InlineDateWrapper>
                        <DatePicker
                            id="dateOtherBankDate"
                            className="inline-date"
                            label="Date"
                            placeholder="Choose a date"
                            dayFormat="MMMM DD, YYYY"
                            required={ detailsRequired.others }
                            selectedDay={ data.dop_other_bank_date }
                            onChange={ () => this.checkIfGroupHasInput( 'OTHERS' ) }
                            ref={ ( ref ) => { this.dop_other_bank_date = ref; } }
                        />
                    </InlineDateWrapper>
                    <div className="number">
                        <Input
                            id="textOthersAmount"
                            label="Amount"
                            required={ detailsRequired.others }
                            value={ data.dop_other_bank_amount || '' }
                            onChange={ ( value ) => this.others_amount.setState({ value: stripNonDigit( value ) }) }
                            onBlur={ ( value ) => {
                                const isNegative = Number( formatCurrencyToDecimalNotation( value ) ) < 0;
                                this.others_amount.setState({
                                    value: this.formatCurrency( value ),
                                    error: isNegative || this.others_amount.state.error,
                                    errorMessage: isNegative ? 'This field cannot have a negative value.' : this.others_amount.state.errorMessage
                                }, () =>
                                    this.checkIfGroupHasInput( 'OTHERS' )
                                );
                            } }
                            ref={ ( ref ) => { this.others_amount = ref; } }
                        />
                    </div>
                </div>
            </div>
        );
    }

    /**
     * Renders the download screen for SSS forms
     */
    renderDownloadPage() {
        return (
            <DownloadWrapper>
                <div className="downloadWrapper">
                    <div className="svg-wrapper animate">
                        <svg xmlns="http://www.w3.org/2000/svg" width="195" height="195" viewBox="0 0 195 195">
                            <g fill="none">
                                <circle cx="97.5" cy="97.5" r="97.5" fill="#3DA8E3" />
                                <path d="M99 139.9L99 140 96 140 96 139.9 95.9 140 69 113.1 71.1 111 96 135.9 96 55 99 55 99 135.9 123.9 111 126 113.1 99.1 140 99 139.9Z" fill="#FFF" />
                            </g>
                        </svg>
                    </div>
                    <div className="description">
                        <H3>File will start downloading shortly</H3>
                        <p>The compressed file (.zip) contains:</p>
                        <p>You&quot;ll download a printable BIR 1601C form in PDF file.</p>
                        <p>
                            You may&nbsp;
                            <A onClick={ this.saveAndDownloadForm } >
                                retry downloading
                            </A>
                            <A download id="downloadLink"></A>
                            &nbsp;if it doesn&#39;t start automatically.
                        </p>
                    </div>
                </div>
            </DownloadWrapper>
        );
    }

    /**
     * renders component to DOM
     */
    render() {
        const { status } = this.props;

        return (
            <div>
                { !( status.loading || status.generating ) && (
                    <NavWrapper>
                        <Container>
                            <A href="/payroll/forms">
                                &#8592; Back to Government Forms
                            </A>
                        </Container>
                    </NavWrapper>
                ) }
                <Helmet
                    title="Government Forms: BIR"
                    meta={ [
                        { name: 'description', content: 'Generate BIR 1601C Forms' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ () => {
                        this.setState({ any_taxes_withheld: '0' });
                        this.changeTaxWitheld( false );
                    } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="icon"><i className="fa fa-exclamation-circle" /></div>
                            <div className="message">
                                You are about to change the value to &#39;No&#39;. Doing this will clear all computation fields.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ this.state.showConfirmTaxWithheldNone }
                />
                {
                    status.loading && this.renderLoadingPage( false )
                }
                {
                    status.generating && this.renderLoadingPage( true )
                }
                {
                    status.downloadReady && this.renderDownloadPage()
                }
                <PageWrapper step={ 2 } visible={ !( status.loading || status.generating || status.downloadReady ) }>
                    <FormsWrapper>
                        <div>
                            <div className="heading">
                                <div>
                                    <H3>Bureau of Internal Revenue Form 1601 C</H3>
                                    <p>To accomplish your BIR 1601 C form, follow the steps below:</p>
                                    <span>1. Fill out the fields required below.</span>
                                    <span>2. Generate and print the 1601 C PDF file.</span>
                                    <span>3. Bring the printed form to the nearest BIR office.</span>
                                </div>
                                <div className="actions">
                                    <Button
                                        alt
                                        label="Regenerate Form"
                                        type="action"
                                        size="large"
                                        onClick={ this.regenerateForm }
                                    />
                                    <Button
                                        label="Save and Download Form"
                                        type="action"
                                        size="large"
                                        onClick={ this.saveAndDownloadForm }
                                    />
                                    <A download id="downloadLink" />
                                </div>
                            </div>
                            { !( status.loading || status.generating || status.downloadReady ) && (
                                <div className="sections">
                                    { this.renderHeaderSection() }
                                    { this.renderPart1Section() }
                                    { this.renderPart2Section() }
                                    { this.renderSignatoriesSection() }
                                    { this.renderPart3Section() }
                                </div>
                            ) }
                        </div>
                    </FormsWrapper>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    data: makeSelectData(),
    status: makeSelectStatus(),
    options: makeSelectOptions(),
    downloadUrl: makeSelectDownloadUrl(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        birFormActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( BIR1601CForms );
