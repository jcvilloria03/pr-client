import styled from 'styled-components';

export const TableWrapper = styled.div`
    width: 100% !important;
    padding-top: 30px !important;

    > .table-heading {
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 20px;

        h5 {
            margin: 0;
        }

        > div {
            display: inline-flex;
            align-items: center;

            > p {
                margin-right: 15px;
                margin-bottom: 0;
            }

            > button {
                padding: 11px 30px;
            }
        }
    }

    .ReactTable {
        margin-bottom: 30px;

        .rt-thead {
            .rt-tr {
                text-align: left;
            }
            .rt-th {
                display: inline-flex;
                align-items: center;
            }
        }
    }

    > .table-footer {
        > div > div {
            align-items: flex-start;
            padding-left: 120px;

            > input, > p {
                width: 100%;
                max-width: 350px;
            }
        }
    }
`;
