import React from 'react';
import moment from 'moment';
import decimal from 'js-big-decimal';

import { H5 } from 'components/Typography';
import Input from 'components/Input';
import DatePicker from 'components/DatePicker';
import Button from 'components/Button';
import ToolTip from 'components/ToolTip';
import Table from 'components/Table';
import Switch from 'components/Switch';

import { InlineDateWrapper } from '../styles';
import {
    TableWrapper
} from './styles';

/**
 * BIR Form 1601C Section A View component
 */
export class SectionAView extends React.Component {
    static propTypes = {
        disabled: React.PropTypes.bool,
        onChange26: React.PropTypes.func.isRequired
    };

    static defaultProps = {
        disabled: false
    }

    /**
     * component's constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            sectionAEntries: [],
            sectionAFrequency: 'Monthly',
            selectionCount: '',
            sectionAEntriesMaxWarning: '',
            totalAdjustment: 0,
            visible: false
        };

        this.addSectionA = this.addSectionA.bind( this );
        this.updateSelection = this.updateSelection.bind( this );
        this.deleteSelection = this.deleteSelection.bind( this );
    }

    /**
     * Gets the total adjustment
     */
    getTotalAdjustment() {
        let response;
        if ( this.state.sectionAEntries.length ) {
            const total = this.state.sectionAEntries.reduce( ( sum, entry ) =>
                decimal.getPrettyValue( decimal.add( this.stripNonDigit( sum ), decimal.add( this.stripNonDigit( entry.current_year_tax_due || 0 ), this.stripNonDigit( entry.year_end_adjustment_preceding_year || 0 ) ) ) )
            , 0 );
            this.props.onChange26( total );
            response = this.formatCurrency( total );
        } else {
            response = 0;
        }

        this.setState({ totalAdjustment: response }, () => {
            this.props.onChange26( response );
        });
    }

    /**
     * removes non-digit character to string except dot.
     * @param value
     */
    stripNonDigit( value ) {
        return decimal.round( value.toString().replace( /[^0-9.-]/g, '' ), 2 );
    }

    /**
     * formats number into currency
     * @param value
     */
    formatCurrency( value ) {
        const formattedValue = this.stripNonDigit( value );

        const valueArray = formattedValue.split( '.' );

        valueArray[ 0 ] = `${valueArray[ 0 ].replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' )}`;

        return valueArray.length > 1 ? `${valueArray[ 0 ]}.${valueArray[ 1 ]}` : `${valueArray[ 0 ]}`;
    }

    /**
     * validates a numeric field
     */
    validateNumberField( input ) {
        const value = input.state.value && this.stripNonDigit( input.state.value );

        if ( input.state.error ) {
            return false;
        } else if ( !value ) {
            input._validate( value );
            return false;
        } else if ( input.state.value.length > 25 ) {
            input.setState({ error: true, errorMessage: 'This field cannot exceed 25 characters.' });
            return false;
        } else if ( isNaN( Number( value ) ) ) {
            input.setState({ error: true, errorMessage: 'Please enter a valid amount' });
            return false;
        }
        return true;
    }

    /**
     * adds an entry to section A table
     */
    addSectionA() {
        // Check if frequency is selected
        let valid = true;
        const entry = {};
        const frequency = this.frequency.state.value.value || this.frequency.state.value;
        if ( this.frequency._checkRequire( frequency ) ) {
            entry.frequency = frequency;
            entry.selected = false;

            const requiredFields = {
                Yearly: [ 'previous_month', 'tax_paid', 'month_tax_due', 'year_end_adjustment_preceding_year' ],
                Monthly: [ 'previous_month', 'date_paid', 'bank_validation', 'bank_code', 'tax_paid', 'month_tax_due', 'current_year_tax_due' ]
            };

            requiredFields[ frequency ].forEach( ( field ) => {
                let value;
                switch ( field ) {
                    case 'date_paid':
                        value = this[ field ].state.selectedDay;
                        if ( !this[ field ].checkRequired() ) {
                            entry[ field ] = moment( value ).format( 'YYYY[-]MM[-]DD' );
                        } else {
                            valid = false;
                        }

                        break;
                    case 'tax_paid':
                    case 'month_tax_due':
                        if ( this.validateNumberField( this[ field ]) ) {
                            entry[ field ] = this.stripNonDigit( this[ field ].state.value );
                        } else {
                            valid = false;
                        }
                        break;
                    default:
                        value = this[ field ].state.value;
                        if ( !this[ field ]._validate( value ) ) {
                            entry[ field ] = [ 'year_end_adjustment_preceding_year', 'current_year_tax_due' ].includes( field ) ? this.stripNonDigit( value ) : value;
                        } else {
                            valid = false;
                        }
                }
            });

            // previous month value
            if ( this.previous_month.state.value ) {
                const value = this.previous_month.state.value;
                const validFormat = ( /^\d{2}\/\d{4}$/g ).test( value );
                if ( !validFormat ) {
                    valid = false;
                    this.previous_month.setState({ error: true, errorMessage: 'Invalid input format. Should match MM/YYYY' });
                } else if ( !moment( `${value}`, 'MM/YYYY' ).isValid() ) {
                    valid = false;
                    this.previous_month.setState({ error: true, errorMessage: 'Please enter a valid date' });
                } else if ( frequency === 'Yearly' ) {
                    const selectedMonth = value.substring( 0, 2 );
                    if ( selectedMonth !== '12' ) {
                        valid = false;
                        this.previous_month.setState({ error: true, errorMessage: 'Invalid input. Month should always be 12 when yearly frequency is selected' });
                    }
                }
            }
        } else {
            valid = false;
        }

        // ADD the entry to state
        valid && this.setState({ sectionAEntries: this.state.sectionAEntries.concat([entry]) }, () => {
            // reset the form values
            this.resetSectionAForms();

            // disable add button if maximum entries is reached
            this.buttonAdd.setState({ disabled: this.state.sectionAEntries.length > 2 });

            // warn user if maximum entries is reached
            this.setState({ sectionAEntriesMaxWarning: this.state.sectionAEntries.length > 2 ? 'You can only add up to 3 entries' : '' });

            this.getTotalAdjustment();
        });
    }

    /**
     * reset the form values
     */
    resetSectionAForms() {
        this.setState({ sectionAFrequency: 'Monthly' }, () => {
            this.frequency.setState({ value: { label: 'Monthly', value: 'Monthly' }}, () => {
                this.previous_month.setState({ value: '' });
                this.date_paid.setState({ selectedDay: '', error: false });
                this.bank_validation.setState({ value: '' });
                this.bank_code.setState({ value: '' });
                this.tax_paid.setState({ value: '' }, () => {
                    this.month_tax_due.setState({ value: '' }, () => {
                        this.current_year_tax_due.setState({ value: '' });
                        this.year_end_adjustment_preceding_year.setState({ value: '' });
                    });
                });
            });
        });
    }

    /**
     * Deletes selected entries from table
     */
    deleteSelection() {
        if ( this.tableAdjustment ) {
            const sectionAEntries = this.state.sectionAEntries.slice( 0 );
            this.tableAdjustment.state.selected.forEach( ( selected, index ) => {
                if ( selected ) {
                    sectionAEntries.splice( index, 1, null );
                }
            });

            this.setState({
                sectionAEntries: sectionAEntries.filter( ( entry ) => entry ),
                sectionAEntriesMaxWarning: '',
                selectionCount: 0
            }, () => {
                this.getTotalAdjustment();
            });

            // clear selection
            this.tableAdjustment.setState({
                selected: this.tableAdjustment.state.selected.map( () => false ),
                pageSelected: { 0: false }
            }, () => {
                this.tableAdjustment.headerSelect.checked = false;
            });
            this.buttonAdd.setState({ disabled: false });
        }
    }

    /**
     * update selection labels
     */
    updateSelection() {
        if ( this.tableAdjustment ) {
            const sectionAEntries = this.state.sectionAEntries.slice( 0 );
            this.tableAdjustment.state.selected.forEach( ( selected, index ) => {
                sectionAEntries[ index ].selected = selected;
            });

            const selectionCount = this.tableAdjustment.state.selected.filter( ( item ) => item ).length;
            this.setState({
                sectionAEntries,
                selectionCount
            });
        }
    }

    /**
     * renders Section table
     */
    renderTable() {
        let columns;

        if ( this.state.sectionAEntries.length ) {
            columns = [{
                header: 'Month',
                accessor: 'previous_month',
                width: 120,
                hideFilter: true
            }, {
                header: 'Date Paid',
                accessor: 'date_paid',
                width: 160,
                hideFilter: true,
                render: ( row ) => moment( row.value ).format( 'MMMM DD, YYYY' )
            }, {
                header: 'Bank Validation',
                accessor: 'bank_validation',
                width: 160,
                hideFilter: true
            }, {
                header: 'Bank Code',
                accessor: 'bank_code',
                width: 140,
                hideFilter: true
            }, {
                header: 'Tax Paid',
                accessor: 'tax_paid',
                width: 180,
                hideFilter: true,
                style: { justifyContent: 'flex-end' },
                render: ( row ) => this.formatCurrency( row.value || 0 )
            }, {
                header: () => <span>Should be tax due<br />for the month</span>,
                accessor: 'month_tax_due',
                width: 180,
                hideFilter: true,
                style: { justifyContent: 'flex-end' },
                render: ( row ) => this.formatCurrency( row.value || 0 )
            }, {
                header: 'For the current year',
                accessor: 'current_year_tax_due',
                width: 200,
                hideFilter: true,
                style: { justifyContent: 'flex-end' },
                render: ( row ) => this.formatCurrency( row.value || 0 )
            }, {
                header: () => <span style={ { fontSize: '15px' } }>From year end adjustment<br />of the immediately<br />preceeding year</span>,
                accessor: 'year_end_adjustment_preceding_year',
                width: 230,
                hideFilter: true,
                style: { justifyContent: 'flex-end' },
                render: ( row ) => this.formatCurrency( row.value || 0 )
            }];
        }

        return (
            <TableWrapper>
                {
                    this.state.sectionAEntries.length
                        ? [
                            <div key="0" className="table-heading">
                                <H5>Adjustment of Taxes Withheld</H5>
                                {
                                    this.state.selectionCount
                                    ? <div className="table-actions">
                                        <p>{ this.state.selectionCount ? `${this.state.selectionCount} entr${this.state.selectionCount > 1 ? 'ies' : 'y'} selected` : '' }</p>
                                        <Button
                                            ref={ ( ref ) => { this.buttonDelete = ref; } }
                                            label="Delete"
                                            size="large"
                                            type="danger"
                                            onClick={ this.deleteSelection }
                                        />
                                    </div>
                                    : false
                                }
                            </div>,
                            <Table
                                key="1"
                                ref={ ( ref ) => { this.tableAdjustment = ref; } }
                                columns={ columns }
                                data={ this.state.sectionAEntries }
                                pagination={ this.state.sectionAEntries.length > 10 }
                                selectable
                                onSelectionChange={ this.updateSelection }
                                onDataChange={ this.updateSelection }
                            />
                        ]
                        : false
                }
            </TableWrapper>
        );
    }

    /**
     *
     */
    render() {
        return (
            <div className="line section-a">
                <H5 style={ { width: '100%', marginBottom: this.state.visible ? '' : '0' } }>
                    Section A: Adjustment of Taxes Withheld on Compensation for Previous Months
                    <span style={ { float: 'right' } } >
                        <Switch
                            disabled={ this.props.disabled }
                            defaultChecked={ this.state.visible }
                            onChange={
                                ( enabled ) => {
                                    this.setState({ visible: enabled }, () => {
                                        if ( !this.state.visible ) {
                                            this.setState({ sectionAEntries: [], sectionAEntriesMaxWarning: '' }, () => {
                                                this.getTotalAdjustment();
                                                this.buttonAdd.setState({ disabled: false });
                                            });
                                        }
                                    });
                                }
                            }
                        />
                    </span>
                </H5>
                <div style={ { display: this.state.visible ? '' : 'none' } } >
                    <p style={ { width: '100%' } }>
                        All enabled fields are required.
                        <br />
                        Hiding this section will remove all entries added and will not be included in the generation.
                    </p>
                    <div className="line" style={ { alignItems: 'flex-start' } }>
                        <Input
                            label={
                                <span>
                                    Previous Month(s)
                                    <span className="label-tooltip">
                                        <ToolTip id="toolTipSectionAPreviousMonth" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Month/Year of tax overpayment or underpayment
                                        </ToolTip>
                                    </span>
                                </span>
                            }
                            ref={ ( ref ) => { this.previous_month = ref; } }
                            placeholder="MM/YYYY"
                            id="sectionAPreviousMonth"
                        />
                        <InlineDateWrapper style={ { display: 'flex', alignItems: 'flex-start' } }>
                            <label htmlFor="datePaid">
                                <span>
                                    Date Paid
                                    <span className="label-tooltip">
                                        <ToolTip id="toolTipSectionADatePaid" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                            Date of payment of the tax mentioned in Section A1
                                        </ToolTip>
                                    </span>
                                </span>
                            </label>
                            <DatePicker
                                label=""
                                dayFormat="MMMM DD[,] YYYY"
                                placeholder="Choose a date"
                                ref={ ( ref ) => { this.date_paid = ref; } }
                                disabled={ this.state.sectionAFrequency === 'Yearly' }
                            />
                        </InlineDateWrapper>
                        <Input
                            id="sectionADraweeBank"
                            label="Drawee Bank/Bank Code Agency"
                            ref={ ( ref ) => { this.drawee_bank = ref; } }
                        />
                    </div>
                    <div className="line pad-bottom">
                        <div className="numberInput">
                            <Input
                                label="Number"
                                id="sectionAPaymentNumber"
                                ref={ ( ref ) => { this.payment_number = ref; } }
                            />
                        </div>
                        <div className="numberInput">
                            <Input
                                label="Tax Paid"
                                id="sectionATaxPaid"
                                ref={ ( ref ) => { this.tax_paid = ref; } }
                            />
                        </div>
                        <div className="numberInput">
                            <Input
                                label={
                                    <span>
                                        Should be Tax Due for the Month
                                        <span className="label-tooltip">
                                            <ToolTip id="toolTipSectionATaxDue" target={ <i className="fa fa-info-circle" /> } placement="right" >
                                                The correct tax which should’ve been paid
                                            </ToolTip>
                                        </span>
                                    </span>
                                }
                                ref={ ( ref ) => { this.month_tax_due = ref; } }
                                id="sectionATaxDue"
                                onChange={ ( value ) => {
                                    this.month_tax_due.setState({ value: value.toString().replace( /[^0-9.-]/g, '' ) });
                                } }
                                onBlur={ ( value ) => {
                                    const monthTaxDue = this.stripNonDigit( value );
                                    const taxPaid = this.stripNonDigit( this.tax_paid.state.value );
                                    const frequency = this.frequency.state.value.value || this.frequency.state.value;

                                    const adjustment = decimal.subtract( monthTaxDue, taxPaid );

                                    this.month_tax_due.setState({ value: this.formatCurrency( monthTaxDue ) }, () => {
                                        if ( frequency === 'Monthly' ) {
                                            this.year_end_adjustment_preceding_year.setState({ value: '0' });
                                            this.current_year_tax_due.setState({ value: decimal.getPrettyValue( adjustment ) });
                                        } else {
                                            this.year_end_adjustment_preceding_year.setState({ value: decimal.getPrettyValue( adjustment ) });
                                            this.current_year_tax_due.setState({ value: '0' });
                                        }
                                    });
                                } }
                            />
                        </div>
                    </div>
                    <div className="line">
                        <div className="numberInput">
                            <Input
                                label="Adjustments"
                                id="sectionAAdjustments"
                                ref={ ( ref ) => { this.adjustments = ref; } }
                            />
                        </div>
                    </div>

                    <div className="line">
                        <div style={ { textAlign: 'center', width: '100%' } } >
                            <div style={ { width: '180px', margin: 'auto' } }>
                                <Button
                                    label="Add"
                                    alt
                                    size="large"
                                    block
                                    onClick={ this.addSectionA }
                                    ref={ ( ref ) => { this.buttonAdd = ref; } }
                                />
                            </div>
                            <span>{ this.state.sectionAEntriesMaxWarning }</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SectionAView;
