import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';

import { Fetch } from 'utils/request';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    INITIAL_DATA,
    GENERATE_FORMS,
    SET_LOADING,
    SET_DATA,
    SET_GENERATING,
    SET_URI,
    DOWNLOAD_READY,
    NOTIFICATION,
    REGENERATE_FORM,
    SAVE_AND_DOWNLOAD_FORM
} from './constants';
import { setNotification } from './actions';

/**
 * Initialize data for Government Forms:BIR 1601C page
 */
export function* initializeData({ payload }) {
    try {
        yield [
            put({
                type: SET_LOADING,
                payload: true
            }),
            put({ type: DOWNLOAD_READY, payload: false }),
            call( getData, payload )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * get prefill data from api
 */
export function* getData( payload ) {
    try {
        const { data } = yield call( Fetch, `/government_forms/${payload}`, { method: 'GET' });

        yield put({
            type: SET_DATA,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Generate and download BIR forms
 */
export function* generateBIRForms({ payload }) {
    try {
        yield put({ type: SET_GENERATING, payload: true });
        const response = yield call( Fetch, '/philippine/government_forms/bir/1601c_generate', { method: 'POST', data: payload });

        yield put({ type: DOWNLOAD_READY, payload: true });
        yield put({ type: SET_URI, payload: response.uri });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_GENERATING,
            payload: false
        });
    }
}

/**
 * Sends request to regenerate form
 * @param {String} payload.kind - Kind of government form
 * @param {String} payload.company_id - Company ID
 * @param {String} payload.month_from
 * @param {String} payload.year_from
 * @param {String} payload.month_to
 * @param {String} payload.year_to
 */
export function* regenerateForm({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const { data } = yield call( Fetch, '/government_forms', {
            method: 'POST',
            data: payload
        });

        yield put({
            type: SET_DATA,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Sends request to save and download form
 * @param {Object} payload - Form data
 * @returns {Object} action
 */
export function* saveAndDownloadForm({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const { id, ...data } = payload;

        yield call( Fetch, `/government_forms/${id}`, {
            method: 'PUT',
            data
        });

        const [download] = yield [
            call( Fetch, `/download/govt_form/${id}`, { method: 'GET' }),
            call( getData, id )
        ];

        yield put({ type: SET_URI, payload: download.uri });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, { payload });
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser({ payload }) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watcher for INITIAL_DATA
 *
 */
export function* watchForInitialData() {
    const watcher = yield takeEvery( INITIAL_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Generate Forms
 *
 */
export function* watchForGenerateBIRForms() {
    const watcher = yield takeEvery( GENERATE_FORMS, generateBIRForms );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REGENERATE_FORM
 */
export function* watchForRegenerateForm() {
    const watcher = yield takeEvery( REGENERATE_FORM, regenerateForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SAVE_AND_DOWNLOAD_FORM
 */
export function* watchForSaveAndDownloadForm() {
    const watcher = yield takeEvery( SAVE_AND_DOWNLOAD_FORM, saveAndDownloadForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitialData,
    watchForGenerateBIRForms,
    watchForRegenerateForm,
    watchForSaveAndDownloadForm,
    watchForNotifyUser,
    watchForReinitializePage
];
