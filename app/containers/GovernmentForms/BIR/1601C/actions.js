import { RESET_STORE } from 'containers/App/constants';

import {
    INITIAL_DATA,
    REGENERATE_FORM,
    SAVE_AND_DOWNLOAD_FORM,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

/**
 * Initialize data for payslip page
 * @param {Integer} payload - Government form ID
 * @returns {Object} action
 */
export function initializeData( payload ) {
    return {
        type: INITIAL_DATA,
        payload
    };
}

/**
 * Sends request to regenerate form
 * @param {String} payload.kind - Kind of government form
 * @param {String} payload.company_id - Company ID
 * @param {String} payload.month_from
 * @param {String} payload.year_from
 * @param {String} payload.month_to
 * @param {String} payload.year_to
 * @returns {Object} action
 */
export function regenerateForm( payload ) {
    return {
        type: REGENERATE_FORM,
        payload
    };
}

/**
 * Sends request to save and download form
 * @param {Object} payload - Form data
 * @returns {Object} action
 */
export function saveAndDownloadForm( payload ) {
    return {
        type: SAVE_AND_DOWNLOAD_FORM,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: NOTIFICATION_SAGA,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {string} payload.title
 * @param {*} payload.message
 * @param {boolean} payload.show
 * @param {string} payload.type - One of 'success', 'error', 'warning', 'default'
 * @returns {object} action
 */
export function notify( payload ) {
    return {
        type: NOTIFICATION,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
