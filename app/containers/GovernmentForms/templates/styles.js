import styled, { keyframes } from 'styled-components';

const bounce = keyframes`
    0%, 20%, 50%, 80%, 100% {
        transform: translate3d(0, 0, 0);
    }
    40% {
        transform: translate3d(-30px, 0, 0);
    }
    60% {
        transform: translate3d(-15px, 0, 0);
    }
`;

const fadeIn = keyframes`
    from {
        opacity: 0;
    }
    to {
        opacity: 1;
    }
`;

export const AgencyWrapper = styled.div`
    display: flex;
    justify-content: center;
    margin: 0 auto;
    padding-bottom: 40px;

    > div {
        display: inline-flex;
        flex-direction: column;

        p {
            padding: 0 15px;

            @media (max-width: 1280px) {
                padding: 0 5%;
            }
        }

        > div {
            display: inline-flex;
            padding-top: 10px;

            ul {
                display: inline-flex;
                flex-wrap: wrap;
                justify-content: center;
                margin: 0;
                padding: 0;
                list-style: none;
                width: 100%;

                li:last-child {
                    margin-right: 0;
                }
            }
        }
    }

    @media (max-width: 1440px) {
        > div {
            width: 100%;

            > div ul {
                width: 100%;
            }
        }
    }
`;

export const PeriodPaymentWrapper = styled.div`
    display: flex;
    justify-content: center;
    margin: 0 auto;
    padding-bottom: 40px;

    h4 {
        font-weight: bold;
    }

    > div {
        display: flex;
        flex-direction: column;
        position: relative;
        width: 100%;

        > div {
            display: flex;

            &.content {
                flex-direction: column;
                justify-content: space-around;
                background-color: #fff;
                margin-top: 40px;
            }

            > div {
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between;
                padding-top: 10px;

                &.middle {
                    > div {
                        flex: 1 0 49%;
                        max-width: 49%;

                        .select-bir-forms,
                        .select-period {
                        }

                        .payment-methods {
                            display: flex;
                            flex-wrap: wrap;

                            & > div {
                                padding-right: 20px !important;
                            }

                            @media (max-width: 1320px) {
                                flex-direction: column;

                                > div {
                                    flex-basis: auto !important;
                                }
                            }
                        }
                    }
                }

                &.channels {
                    flex-direction: column;
                    padding-top: 10px;

                    ul {
                        display: inline-flex;
                        flex-wrap: wrap;
                        justify-content: flex-start;
                        align-self: start;
                        margin: 0;
                        padding: 0;
                        list-style: none;
                        width: 100%;
                    }
                }
            }

            &.actions {
                flex-direction: row;
                flex-wrap: wrap;
                padding-top: 30px;
                justify-content: space-around;

                button {
                    padding-left: 40px;
                    padding-right: 40px;

                    &.previous {
                        margin-right: auto;
                    }
                }
            }
        }
    }

    @media (max-width: 1440px) {
        > div {
            width: 100%;
        }
    }

    @media (max-width: 1024px) {
        > div > div > div.channels ul {
            justify-content: center;
        }
    }
`;

export const MessageWrapperStyles = styled.div`
    display: flex;
    justify-content: center;
    padding: 120px 0 40px;
    align-items: center;

    > div {
        display: flex;
        flex-direction: column;
        justify-content: center;

        i {
            font-size: 7em;
            align-self: center;
        }

        .description {
            padding-top: 40px;
            text-align: center;

            h3 {
                font-weight: 600;
            }
        }
    }
`;

export const CardWrapper = styled.li`
    width: 280px;
    margin-right: 25px;

    &.channel {
        width: 33%;
        padding-right: 30px;
        margin-right: 0;

        .card div.image {
            height: 200px;
        }

        @media (min-width: 1921px) {
            width: 350px;
        }
    }

    .card {
        display: flex;
        flex-direction: column;
        background-color: #fff;
        border: 1px solid rgba(0,0,0,.15);
        cursor: pointer;
        cursor: pointer;
        transition: all 0.3s ease;

        animation: ${fadeIn} 1.5s ease;

        div.image {
            display: inline-flex;
            height: 240px;
        }

        div.name {
            border-top: 1px solid rgba(0,0,0,.15);
            margin: 0 15px;
            padding: 12px 5px;
            display: inline-flex;
            justify-content: space-between;

            i {
                color: #00A5E5;
                font-size: 1.2em;
                padding-top: 3px;
                align-self: center;
            }
        }

        &:hover {
            transform: translate3d(0, -10px, 0);
            div.name i {
                animation: ${bounce} 2s infinite;
            }
        }
    }

    @media (max-width: 1440px) {
        width: 25%;
        padding-right: 18px;
        margin-right: 0;

        div.image {
            height: 220px;
        }
    }

    @media (max-width: 1024px) {
        width: 280px;
        margin-right: 25px !important;
        padding-right: 0;

        &.channel {
            width: 280px;
            margin-right: 25px !important;
            padding-right: 0;
        }

        div.image {
            height: 240px;
        }
    }
`;

export const ImageWrapper = styled.div`
    background-image: url('${( props ) => props.url}');
    background-size: contain;
    background-position: center center;
    width: 100%;
    margin: 45px;

    &.channel,
    &.sss {
        margin: 65px;
    }

`;
