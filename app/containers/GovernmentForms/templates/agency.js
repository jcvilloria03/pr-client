import React from 'react';

import A from '../../../components/A';

import { GOVERNMENT_AGENCIES } from '../Generate/constants';

import {
    AgencyWrapper,
    CardWrapper,
    ImageWrapper
} from './styles';

/**
 * Government Agency Selection Component
 */
class AgencyView extends React.PureComponent {
    static propTypes = {
        selectAgency: React.PropTypes.func,
        pageType: React.PropTypes.oneOf([ 'company', 'employee' ])
    }

    static defaultProps = {
        pageType: 'company'
    }

    /**
     * returns static list of government agencies
     */
    getAgencies() {
        return this.props.pageType === 'company'
            ? GOVERNMENT_AGENCIES
            : [GOVERNMENT_AGENCIES[ 0 ]];
    }

    /**
     * renders component to DOM
     */
    render() {
        return (
            <AgencyWrapper>
                <div>
                    <p>To get started, choose which government agency you&#39;ll be needing a form to accomplish.</p>
                    <div>
                        <ul>
                            {
                                this.getAgencies().map( ( agency, index ) => (
                                    <CardWrapper key={ index }>
                                        <A className="card" onClick={ () => { this.props.selectAgency( agency.code ); } }>
                                            <div className="image">
                                                <ImageWrapper url={ agency.logo } className={ agency.code.toLocaleLowerCase() } />
                                            </div>
                                            <div className="name">
                                                <span>{ agency.name }</span>
                                                <i className="fa fa-chevron-right" />
                                            </div>
                                        </A>
                                    </CardWrapper>
                                ) )
                            }
                        </ul>
                    </div>
                </div>
            </AgencyWrapper>
        );
    }
}

export default AgencyView;
