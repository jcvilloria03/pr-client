import React from 'react';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

import { getCurrentYear } from 'utils/functions';

import A from 'components/A';
import { H3, H4 } from 'components/Typography';
import RadioGroup from 'components/RadioGroup';
import Radio from 'components/Radio';
import SalSelect from 'components/Select';
import Button from 'components/Button';
import ToolTip from 'components/ToolTip';

import {
    FRONT_END_ASSETS_URL,
    SSS,
    PAG_IBIG,
    BIR,
    PHIL_HEALTH,
    BPI,
    SBC,
    GOVERNMENT_FORM_KINDS
} from '../Generate/constants';

import {
    PeriodPaymentWrapper,
    MessageWrapperStyles,
    CardWrapper,
    ImageWrapper
} from './styles';

/**
 * Period and Payment Method Selection Component
 */
class PeriodPaymentView extends React.PureComponent {
    static propTypes = {
        switchStep: React.PropTypes.func,
        loading: React.PropTypes.bool.isRequired,
        periods: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                value: React.PropTypes.string,
                label: React.PropTypes.string
            })
        ).isRequired,
        availableYears: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                value: React.PropTypes.string,
                label: React.PropTypes.string
            })
        ),
        loanPeriods: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                value: React.PropTypes.string,
                label: React.PropTypes.string
            })
        ),
        paymentMethods: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                label: React.PropTypes.string,
                code: React.PropTypes.string,
                channels: React.PropTypes.arrayOf(
                    React.PropTypes.shape({
                        label: React.PropTypes.string,
                        code: React.PropTypes.string,
                        method: React.PropTypes.string
                    })
                )
            })
        ).isRequired,
        birForms: React.PropTypes.arrayOf(
            React.PropTypes.object
        ),
        hdmfForms: React.PropTypes.arrayOf(
            React.PropTypes.object
        ),
        sssForms: React.PropTypes.arrayOf(
            React.PropTypes.object
        ),
        agency: React.PropTypes.string,
        selection: React.PropTypes.shape({
            period: React.PropTypes.object,
            paymentMethod: React.PropTypes.string,
            channel: React.PropTypes.string,
            hdmf: React.PropTypes.shape({
                value: React.PropTypes.string,
                label: React.PropTypes.string
            }),
            sss: React.PropTypes.shape({
                value: React.PropTypes.string,
                label: React.PropTypes.string
            })
        }),
        pageType: React.PropTypes.oneOf([ 'company', 'employee' ])
    }

    static defaultProps = {
        pageType: 'company'
    }

    /**
     * component's constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            selection: props.selection,
            header_text: '',
            period_text: {
                label: '',
                tooltip: '',
                placeholder: ''
            },
            isContinueClicked: false
        };

        this.getPaymentChannelLogo = this.getPaymentChannelLogo.bind( this );
        this.validateSelection = this.validateSelection.bind( this );
        this.isDisableContinueButton = this.isDisableContinueButton.bind( this );
    }

    componentDidMount() {
        this.setAgencySpecificLabels();
    }

    /**
     * Perform actions before receiving new props
     */
    componentWillReceiveProps( nextProps ) {
        const period = this.state.selection.period;

        let paymentMethod = this.state.selection.paymentMethod;

        // Gets the default payment method if there's only one option
        if ( !paymentMethod.length && nextProps.paymentMethods.length === 1 && nextProps.paymentMethods !== this.props.paymentMethods ) {
            paymentMethod = nextProps.paymentMethods[ 0 ].code;
        }

        ( period !== this.state.selection.period || paymentMethod !== this.state.selection.paymentMethod )
            && this.setState({ selection: Object.assign({}, this.state.selection, { period, paymentMethod }) }, () => this.validateSelection() );

        nextProps.agency !== this.props.agency && this.setAgencySpecificLabels();
    }

    /**
     * Gets the payment channel logo url
     */
    getPaymentChannelLogo( code ) {
        switch ( code ) {
            case BPI:
                return `${FRONT_END_ASSETS_URL}/branding/banks/bpi.png`;
            case SBC:
                return `${FRONT_END_ASSETS_URL}/branding/banks/security-bank.png`;
            case SSS:
                return `${FRONT_END_ASSETS_URL}/branding/government-agencies/sss.png`;
            default:
                return '';
        }
    }

    /**
     * Gets the payment channels to display based of selected period
     */
    getPaymentChannels() {
        if ( this.props.paymentMethods.length && this.state.selection.paymentMethod.length ) {
            const paymentMethod = this.props.paymentMethods.length === 1
                                        ? this.props.paymentMethods[ 0 ].code
                                        : this.state.selection.paymentMethod;

            if ( paymentMethod.length > 0 ) {
                return this.props.paymentMethods
                        .find( ( method ) => method.code === paymentMethod )
                        .channels.map( ( channel ) => Object.assign({}, channel, { paymentMethod }) );
            }
        }

        return [];
    }

    /**
     * Get select period data for select box
     */
    getSelectPeriodsData() {
        const isSTLRForLoans = get( this.state.selection, 'hdmf.value' ) === 'STLRF' || get( this.state.selection, 'sss.value' ) === 'Loans';
        const is1604C = get( this.state.selection, 'bir.value' ) === GOVERNMENT_FORM_KINDS.BIR_1604C;

        return isSTLRForLoans
            ? this.props.loanPeriods
            : is1604C
                ? this.props.availableYears
                : this.props.periods;
    }

    /**
     * Sets labels depending on selected agency
     */
    setAgencySpecificLabels() {
        let headerText = '';
        let periodLabel = '';
        let periodTooltip = 'Only previously closed payroll months can be selected';
        let periodPlaceholder = 'Choose a payroll month';

        switch ( this.props.agency ) {
            case SSS:
                headerText = 'Social Security System';
                periodLabel = '* Select the month';
                break;
            case PAG_IBIG:
                headerText = 'Pag-IBIG';
                periodLabel = '* Select the month';
                break;
            case BIR:
                headerText = 'Bureau of Internal Revenue';

                if ( this.props.pageType === 'employee' ) {
                    periodLabel = '* Select the applicable year';
                    periodTooltip = 'You can only select the current year up-to past five (5) years';
                    periodPlaceholder = 'Choose year';
                } else if ( get( this.state.selection, 'bir.value' ) === GOVERNMENT_FORM_KINDS.BIR_1604C ) {
                    periodLabel = '* Select the Return Period';
                    periodTooltip = 'The return period year you want the report to generate';
                    periodPlaceholder = 'Choose a Return Period';
                } else {
                    periodLabel = '* Select the payroll month';
                }
                break;
            case PHIL_HEALTH:
                headerText = 'PhilHealth';
                periodLabel = '* Select the month for your PhilHealth contributions';
                break;
            default:
                break;
        }

        this.setState({
            header_text: headerText,
            period_text: {
                label: periodLabel,
                tooltip: periodTooltip,
                placeholder: periodPlaceholder
            }
        });
    }

    /**
     * Checks selection to enable continue button
     */
    isDisableContinueButton() {
        const { selection } = this.state;
        switch ( this.props.agency ) {
            case SSS:
                return this.isDisableSSSContinueButton();
            case PAG_IBIG:
                return this.isDisablePagibigContinueButton();
            case PHIL_HEALTH:
                return !selection.period;
            case BIR:
                return !( selection.bir && selection.period );
            default:
                return true;
        }
    }

    /**
     * Check if continue button is disabled for Pag-Ibig forms
     * @returns {boolean}
     */
    isDisablePagibigContinueButton() {
        const { selection } = this.state;
        return !( ( selection.hdmf
                && selection.hdmf.value === 'MSRF'
            ) || ( ( selection.hdmf
            && selection.hdmf.value === 'STLRF' ) )
            && selection.period );
    }

    /**
     * Check if continue button is disabled for SSS forms
     * @returns {boolean}
     */
    isDisableSSSContinueButton() {
        const { selection } = this.state;
        return !( ( selection.sss
                && selection.sss.value === 'REMITTANCE'
                && selection.period
              ) || ( ( selection.sss
            && selection.sss.value === 'LCL' ) )
            && selection.period
            && this.state.isContinueClicked === false );
    }

    /**
     * Determine if payment channels should be shown
     */
    shouldShowChannels( channels ) {
        if ( channels.length === 0 ) {
            return false;
        }

        return this.state.selection.hdmf && this.state.selection.hdmf.value === 'MSRF' ||
               this.state.selection.sss && this.state.selection.sss.value === 'R3_R5';
    }

    /**
     * Validates selection to enable continue button
     */
    validateSelection() {
        this.buttonContinue && this.buttonContinue.setState({ disabled: this.isDisableContinueButton() });
    }

    /**
     * renders a loading screen
     */
    renderLoadingScreen() {
        return (
            <MessageWrapperStyles>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        <H3>Loading data. Please wait...</H3>
                    </div>
                </div>
            </MessageWrapperStyles>
        );
    }

    /**
     * renders selection of birn forms
     */
    renderBirForms() {
        if ( this.props.agency === BIR ) {
            return (
                <div>
                    <div className="select-bir-forms">
                        <p>
                            Select which form you would like to generate
                            <span style={ { paddingLeft: '15px' } }>
                                <ToolTip
                                    id="tooltipBirForms"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="right"
                                >
                                    Select which BIR form to use.
                                </ToolTip>
                            </span>
                        </p>
                        <SalSelect
                            id="selectBirForms"
                            data={ this.props.birForms }
                            value={ this.state.selection.bir || null }
                            autoSize
                            placeholder="Choose a BIR form"
                            onChange={ ( selectedItem ) => {
                                const is1604C = get( selectedItem, 'value' ) === GOVERNMENT_FORM_KINDS.BIR_1604C;
                                const currentYear = getCurrentYear();

                                this.setState( ( state ) => ({
                                    selection: Object.assign({}, state.selection, {
                                        bir: selectedItem.value ? selectedItem : null,
                                        period: is1604C && !isEmpty( this.props.availableYears )
                                            ? {
                                                label: currentYear,
                                                value: currentYear
                                            }
                                            : null
                                    })
                                }), () => {
                                    this.validateSelection();
                                    this.setAgencySpecificLabels();
                                });
                            } }
                        />
                    </div>
                </div>
            );
        }
        return false;
    }

    /**
     * renders selection of HDMF forms
     */
    renderHDMFForms() {
        return this.props.agency === PAG_IBIG ?
            (
                <div>
                    <div className="select-bir-forms">
                        <p>
                            Select which form you would like to generate
                            <span style={ { paddingLeft: '15px' } }>
                                <ToolTip
                                    id="tooltipBirForms"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="right"
                                >
                                    Select which Pag-ibig form to use.
                                </ToolTip>
                            </span>
                        </p>
                        <SalSelect
                            id="selectHdmfForms"
                            data={ this.props.hdmfForms }
                            value={ this.state.selection.hdmf || null }
                            autoSize
                            placeholder="Choose a HDMF form"
                            onChange={ ( selectedItem ) => {
                                this.setState({
                                    selection: Object.assign({}, this.state.selection, {
                                        hdmf: selectedItem.value ? selectedItem : null,
                                        period: null
                                    })
                                }, () => { this.validateSelection(); });
                            } }
                        />
                    </div>
                </div>
            )
        : null;
    }

    /**
     * renders selection of SSS forms
     */
    renderSSSForms() {
        return this.props.agency === SSS ?
            (
                <div>
                    <div className="select-bir-forms">
                        <p>
                            Select which form you would like to generate
                            <span style={ { paddingLeft: '15px' } }>
                                <ToolTip
                                    id="tooltipSSSForms"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="right"
                                >
                                    Select which SSS form to use.
                                </ToolTip>
                            </span>
                        </p>
                        <SalSelect
                            id="selectSSSForms"
                            data={ this.props.sssForms }
                            value={ this.state.selection.sss || null }
                            autoSize
                            placeholder="Choose a SSS form"
                            onChange={ ( selectedItem ) => {
                                this.setState({
                                    selection: Object.assign({}, this.state.selection, {
                                        sss: selectedItem.value ? selectedItem : null,
                                        period: null
                                    })
                                }, () => { this.validateSelection(); });
                            } }
                        />
                    </div>
                </div>
            )
        : null;
    }

    /**
     * Render period section
     */
    renderPeriodForm() {
        return (
            <div>
                <div className="select-period">
                    <p>
                        { this.state.period_text.label }
                        <span style={ { paddingLeft: '15px' } }>
                            <ToolTip
                                id="tooltipSelectPeriods"
                                target={ <i className="fa fa-info-circle" /> }
                                placement="right"
                            >
                                { this.state.period_text.tooltip }
                            </ToolTip>
                        </span>
                    </p>
                    <SalSelect
                        id="selectPeriods"
                        data={ this.getSelectPeriodsData() }
                        value={ this.state.selection.period }
                        autoSize
                        required
                        placeholder={ this.state.period_text.placeholder }
                        onChange={ ( selectedItem ) => {
                            this.setState({
                                selection: Object.assign({}, this.state.selection, {
                                    period: selectedItem.value ? selectedItem : null
                                })
                            }, () => { this.validateSelection(); });
                        } }
                        ref={ ( ref ) => { this.periods = ref; } }
                    />
                </div>
            </div>
        );
    }

    /**
     * render payment methods section
     */
    renderPaymentMethods() {
        let tooltipText;

        switch ( this.props.agency ) {
            case SSS:
                tooltipText = 'Payment to SSS can be done over the counter or online bank transfer';
                break;
            default:
                tooltipText = 'Payment can be done over the counter or online bank transfer';
                break;
        }

        return this.props.paymentMethods.length > 1 && this.state.selection.sss && this.state.selection.sss.value === 'R3_R5'
            ? <div>
                <p>
                    * Choose your payment method
                    <span style={ { paddingLeft: '15px' } }>
                        <ToolTip
                            id="tooltipPaymentMethods"
                            target={ <i className="fa fa-info-circle" /> }
                            placement="right"
                        >
                            { tooltipText }
                        </ToolTip>
                    </span>
                </p>
                <RadioGroup
                    className="payment-methods"
                    value={ this.state.selection.paymentMethod }
                    onChange={ ( value ) => this.setState({
                        selection: Object.assign({}, this.state.selection, { paymentMethod: value })
                    }, () => { this.validateSelection(); }) }
                >
                    {
                        this.props.paymentMethods.map( ( paymentMethod, index ) => (
                            <Radio
                                key={ index }
                                value={ paymentMethod.code }
                                checked={ paymentMethod.code === this.state.selection.paymentMethod }
                                className="method"
                            >
                                { paymentMethod.label }
                            </Radio>
                        ) )
                    }
                </RadioGroup>
            </div>
            : false;
    }

    /**
     * render payment channels section
     */
    renderChannels() {
        let tooltipText;

        switch ( this.props.agency ) {
            case SSS:
                tooltipText = 'Select whether you’d like to pay via bank (BPI and Security Bank) or through SSS agencies';
                break;
            default:
                tooltipText = 'Select whether you’d like to pay via bank or through the agency';
                break;
        }

        const channels = this.getPaymentChannels();
        const selectChannel = ( channel ) => this.setState({
            selection: Object.assign({}, this.state.selection, {
                paymentMethod: channel.paymentMethod,
                channel: channel.code
            })
        }, () => { this.validateSelection(); });

        return this.shouldShowChannels( channels )
            ? <div className="channels">
                <p>
                    * Select your payment channel
                    <span style={ { paddingLeft: '15px' } }>
                        <ToolTip
                            id="tooltipPaymentChannels"
                            target={ <i className="fa fa-info-circle" /> }
                            placement="right"
                        >
                            { tooltipText }
                        </ToolTip>
                    </span>
                </p>
                <ul>
                    {
                        channels.map( ( channel, index ) =>
                            <CardWrapper key={ index } className="channel">
                                <A
                                    className="card"
                                    onClick={ () => selectChannel( channel ) }
                                >
                                    <div className="image">
                                        <ImageWrapper
                                            url={ this.getPaymentChannelLogo( channel.code ) }
                                            className={ `channel ${channel.code.toLocaleLowerCase()}` }
                                        />
                                    </div>
                                </A>
                                <Radio
                                    checked={ channel.code === this.state.selection.channel }
                                    onChange={ () => selectChannel( channel ) }
                                >
                                    { channel.label }
                                </Radio>
                            </CardWrapper>
                        )
                    }
                </ul>
            </div>
            : false;
    }

    /**
     * renders component to DOM
     */
    render() {
        return (
            <PeriodPaymentWrapper>
                {
                    this.props.loading
                        ? this.renderLoadingScreen()
                        : <div>
                            <div className="content">
                                <H4>
                                    { this.state.header_text }
                                </H4>
                                <div className="middle">
                                    { this.renderBirForms() }
                                    { this.renderHDMFForms() }
                                    { this.renderSSSForms() }
                                    { this.renderPeriodForm() }
                                    { this.renderPaymentMethods() }
                                </div>
                                { this.renderChannels() }
                            </div>
                            <div className="actions">
                                <Button
                                    size="large"
                                    type="neutral"
                                    label="Previous"
                                    className="previous"
                                    onClick={ () => { this.props.switchStep( 0, this.props.agency, this.state.selection ); } }
                                />
                                <Button
                                    ref={ ( ref ) => { this.buttonContinue = ref; } }
                                    size="large"
                                    type="action"
                                    label="Continue"
                                    onClick={ () => {
                                        this.props.switchStep( 2, this.props.agency, this.state.selection );
                                        this.setState({ isContinueClicked: true });
                                    } }
                                    disabled={ this.isDisableContinueButton() }
                                />
                            </div>
                        </div>
                }
            </PeriodPaymentWrapper>
        );
    }
}

export default PeriodPaymentView;
