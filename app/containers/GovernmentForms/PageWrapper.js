import React from 'react';
import { Container } from 'reactstrap';

import { PAYROLL_SUBHEADER_ITEMS } from 'utils/constants';
import Stepper from 'components/Stepper';
import SubHeader from 'containers/SubHeader';

import { PageWrapperStyles, HeaderStyles } from './Generate/styles';

/**
 * Wrapper for Government Forms Pages
 * contains the Headers and Stepper
 */
class PageWrapper extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
        children: React.PropTypes.node,
        step: React.PropTypes.number,
        visible: React.PropTypes.bool
    };

    static defaultProps = {
        step: 0,
        visible: true
    };

    /**
     * { render the application }
     */
    render() {
        const steps = [
            {
                label: 'Choose a Government Agency'
            },
            {
                label: 'Select the period'
            },
            {
                label: 'Generate form'
            }
        ];
        return (
            <Container>
                <PageWrapperStyles className={ !this.props.visible ? 'hidden' : '' }>
                    <HeaderStyles>
                        <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                        <h4>Government Forms</h4>
                        <p>Generate government forms to submit records and payment to the following government offices: BIR, Pag-IBIG, PhilHealth, and SSS</p>
                        <Stepper steps={ steps } activeStep={ this.props.step } />
                    </HeaderStyles>
                    {React.Children.toArray( this.props.children )}
                </PageWrapperStyles>
            </Container>
        );
    }
}

export default PageWrapper;
