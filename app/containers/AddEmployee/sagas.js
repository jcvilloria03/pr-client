import { take, call, put, cancel, select } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';
import get from 'lodash/get';
import toUpper from 'lodash/toUpper';

import { browserHistory } from 'utils/BrowserHistory';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { subscriptionService } from 'utils/SubscriptionService';

import {
    INITIALIZE,
    LOADING,
    SET_COMPANY_ID,
    SET_CLONE_DATA,
    SUBMIT_FORM,
    SUBMITTED,
    SET_ERRORS,
    SET_FORM_OPTIONS,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    CHECK_FOR_USER_MATCH,
    STATUS_TYPES
} from './constants';
import { makeSelectProducts, makeSelectProductSeats } from './selectors';

import { REINITIALIZE_PAGE } from '../App/constants';
import { resetStore } from '../App/sagas';
import { setHasUserMatch } from './actions';

/**
 * check for subscription
 *
 */
export function* initializeData() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const products = yield select( makeSelectProducts() );
        const isSubscribedToPayroll = subscriptionService.isSubscribedToPayroll( products );
        const isSubscribedToTA = subscriptionService.isSubscribedToTA( products );

        const companyId = company.getLastActiveCompanyId();
        yield put({
            type: SET_COMPANY_ID,
            payload: companyId
        });
        const formOptions = {};

        const [
            locations,
            departments,
            ranks,
            countries,
            positions,
            employmentTypes
        ] = yield [
            call( Fetch, `/philippine/company/${companyId}/locations`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/departments`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/ranks`, { method: 'GET' }),
            call( Fetch, '/countries', { method: 'GET' }),
            call( Fetch, `/company/${companyId}/positions`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/employment_types`, { method: 'GET' })
        ];

        formOptions.work_location = locations.data.map( ( value ) => ({ label: value.name, value: value.name }) );
        formOptions.department_name = departments.data.map( ( value ) => ({ label: value.name, value: value.name }) );
        formOptions.rank_name = ranks.data.map( ( value ) => ({ label: value.name, value: value.name }) );
        formOptions.country = countries.data.map( ( value ) => ({ label: value.name, value: value.name }) );
        formOptions.position_name = positions.data.map( ( value ) => ({ label: value.name, value: value.name }) );
        formOptions.employment_type_name = employmentTypes.data.map( ( value ) => ({ label: value.name, value: value.name }) );
        formOptions.employment_status = STATUS_TYPES;

        if ( isSubscribedToTA ) {
            const teams = yield call( Fetch, `/company/${companyId}/teams`, { method: 'GET' });
            formOptions.team_name = teams.data.map( ( value ) => ({ label: value.name, value: value.name }) );
        }

        if ( isSubscribedToPayroll ) {
            const [
                costCenters,
                payrollGroups,
                others
            ] = yield [
                call( Fetch, `/company/${companyId}/cost_centers`, { method: 'GET' }),
                call( Fetch, `/philippine/company/${companyId}/payroll_groups`, { method: 'GET' }),
                call( Fetch, '/philippine/employee/form_options', { method: 'GET' })
            ];

            formOptions.cost_center_name = costCenters.data.map( ( value ) => ({ label: value.name, value: value.name }) );
            formOptions.payroll_group_name = payrollGroups.data.map( ( value ) => ({ label: value.name, value: value.name }) );
            formOptions.base_pay_unit = others.data.base_pay_unit.map( ( value ) => ({ ...value, value: value.label }) );
            formOptions.tax_type = others.data.tax_type.map( ( value ) => ({ ...value, value: value.label }) );
            formOptions.tax_status = others.data.tax_status;
            formOptions.sss_basis = others.data.sss_basis.map( ( v ) => ({ ...v, value: v.value }) );
            formOptions.philhealth_basis = others.data.philhealth_basis.map( ( v ) => ({ ...v, value: v.value }) );
            formOptions.hdmf_basis = others.data.hdmf_basis.map( ( v ) => ({ ...v, value: v.value }) );
        }

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });

        // CHECK FOR EMPLOYEE TO CLONE
        const employeeToClone = localStorage.getItem( 'clone_employee' );

        if ( employeeToClone !== null ) {
            const cloneData = yield call( Fetch, `/philippine/employee/${employeeToClone}`, { method: 'GET' });

            const payrollData = isSubscribedToPayroll ? {
                payroll: {
                    ...cloneData.payroll,
                    sss_basis: toUpper( get( cloneData, 'payroll.sss_basis', '' ) ),
                    hdmf_basis: toUpper( get( cloneData, 'payroll.hdmf_basis', '' ) ),
                    philhealth_basis: toUpper( get( cloneData, 'payroll.philhealth_basis', '' ) )
                }
            } : {};

            const data = {
                ...cloneData,
                ...payrollData
            };

            yield put({
                type: SET_CLONE_DATA,
                payload: data
            });

            localStorage.removeItem( 'clone_employee' );
        }
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * resets the setup store to initial values
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        const [productSeat] = yield select( makeSelectProductSeats() );

        const data = Object.assign({}, payload, { product_seat_id: productSeat.id });
        const keys = Object.keys( payload );

        if ( !data.work_location && data.primary_location_name ) {
            data.work_location = data.primary_location_name;
        }

        // Transform data to remove null keys and convert boolean to 'Yes/No'
        keys.forEach( ( key ) => {
            if ( payload[ key ] === null ) {
                delete data[ key ];
            }

            const convertBoolToYesNo = [
                'timesheet_required',
                'rest_day_pay',
                'regular_holiday_pay',
                'holiday_premium_pay',
                'special_holiday_pay',
                'differential',
                'overtime',
                'entitled_deminimis'
            ].includes( key );

            if ( convertBoolToYesNo ) {
                data[ key ] = data[ key ] ? 'Yes' : 'No';
            }
        });

        yield call( Fetch, '/employee', { method: 'POST', data });
        yield call( delay, 500 );
        localStorage.setItem( 'added_employee', data.employee_id );
        yield put({
            type: SUBMITTED,
            payload: false
        });
        yield call( browserHistory.push, '/employees', true );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 ) {
            yield call( setErrors, error.response.data.data );

            if ( Object.prototype.hasOwnProperty.call( error.response.data.data, 'userlink_confirmed' ) ) {
                yield call( notifyUser, {
                    title: ' ',
                    message: get( error.response.data.data, 'userlink_confirmed' ),
                    show: true,
                    type: 'error'
                });
            } else if ( get( error, 'response.data.message' ) ) {
                yield call( notifyError, error );
            }
        } else {
            yield call( notifyError, error );
        }
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Sends request to check for an existing user record match
 *
 * @param {Object} payload.data - User details to check
 * @param {Function} payload.callback - Callback function to be called if no user matches
 */
export function* checkForUserMatch({ payload }) {
    const { data, callback } = payload;

    try {
        yield put({
            type: SUBMITTED,
            payload: true
        });

        try {
            yield call( Fetch, '/employee/has_usermatch', {
                method: 'POST',
                data
            });

            yield put( setHasUserMatch( true ) );
            yield put({
                type: SUBMITTED,
                payload: false
            });
        } catch ( error ) {
            yield [
                put( setHasUserMatch( false ) ),
                call( callback )
            ];
        }
    } catch ( error ) {
        yield call( notifyError, error );
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Individual exports for testing
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for CHECK_FOR_USER_MATCH
 */
export function* watchForCheckUserMatch() {
    const watcher = yield takeEvery( CHECK_FOR_USER_MATCH, checkForUserMatch );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForCheckUserMatch,
    watchForInitializeData,
    watchForReinitializePage,
    watchNotify
];
