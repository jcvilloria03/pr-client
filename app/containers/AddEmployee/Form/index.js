import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import get from 'lodash/get';
import toLower from 'lodash/toLower';
import isEqual from 'lodash/isEqual';
import isNil from 'lodash/isNil';
import flatten from 'lodash/flatten';
import snakeCase from 'lodash/snakeCase';

import Button from 'components/Button';
import Loader from 'components/Loader';
import { H4 } from 'components/Typography';
import SalConfirm from 'components/SalConfirm';
import moment from 'moment';
import { browserHistory } from 'utils/BrowserHistory';
import { DATE_FORMATS } from 'utils/constants';
import { renderField } from 'utils/form';

import {
    stripNonDigit,
    stripNonNumeric,
    formatDate,
    formatMobileNumber,
    formatCurrency,
    formatRDO,
    formatSSS,
    formatTIN,
    formatHDMF,
    formatPhilHealth
} from 'utils/functions';
import { subscriptionService } from 'utils/SubscriptionService';

import * as employeeAddActions from '../actions';
import {
    makeSelectErrors,
    makeSelectFormOptions,
    makeSelectCompanyId,
    makeSelectCloneData,
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectProducts,
    makeSelectHasUserMatch
} from '../selectors';
import {
    TA_FIELDS,
    PAYROLL_FIELDS,
    TA_PLUS_PAYROLL_FIELDS
} from '../constants';
import { ConfirmBodyWrapperStyle } from '../styles';

/**
 * Add Employee Form
 */
export class AddEmployeeForm extends React.PureComponent {
    static propTypes = {
        form_options: React.PropTypes.object,
        companyId: React.PropTypes.any,
        clone_data: React.PropTypes.object,
        submitEmployee: React.PropTypes.func,
        errors: React.PropTypes.object,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        onExit: React.PropTypes.func,
        onExitToBatch: React.PropTypes.func,
        willExitTo: React.PropTypes.string,
        products: React.PropTypes.array,
        setHasUserMatch: React.PropTypes.func,
        checkForUserMatch: React.PropTypes.func,
        hasUserMatch: React.PropTypes.bool
    }

    static defaultProps = {
        loading: true,
        errors: {}
    };

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );
        this.state = {
            employee_data: {
                employee_id: null,
                last_name: null,
                middle_name: null,
                first_name: null,
                birth_date: null,
                gender: null,
                status: 'single',
                email: null,
                mobile_number: null,
                telephone_number: null,
                address_line_1: null,
                address_line_2: null,
                country: null,
                city: null,
                zip_code: null,
                active: 'active',
                hours_per_day: null,
                rank_name: null,
                primary_location_name: 'Head Office',
                employment_type_name: 'Probationary',
                team_name: null,
                secondary_location_name: null,
                position_name: 'Employee',
                team_role: null,
                department_name: 'Operations',
                date_hired: null,
                date_ended: null,
                timesheet_required: true,
                rest_day_pay: true,
                regular_holiday_pay: true,
                holiday_premium_pay: true,
                special_holiday_pay: true,
                differential: true,
                overtime: true,
                payroll_group_name: 'Semi-monthly',
                cost_center_name: null,
                consultant_tax_rate: null,
                tax_status: 'Z',
                tax_type: 'Regular',
                base_pay: '0.00',
                base_pay_unit: 'Per Month',
                sss_number: '00-0000000-0',
                tin: '000-000-000-000',
                rdo: null,
                hdmf_number: '0000-0000-0000',
                philhealth_number: '00-000000000-0',
                sss_basis: 'GROSS INCOME',
                sss_amount: null,
                hdmf_basis: 'GROSS INCOME',
                hdmf_amount: null,
                philhealth_basis: 'BASIC PAY',
                philhealth_amount: null,
                entitled_deminimis: null,
                attendance_data_required: null
            },
            errors: {},
            subscribed_to_payroll: false,
            subscribed_to_ta: false,
            field_types: {
                input: [],
                select: [],
                date_picker: [],
                switch: []
            },
            show_confirm_modal: false
        };

        this.handleCloneData = this.handleCloneData.bind( this );
    }

    componentDidMount() {
        // eslint-disable-next-line react/no-did-mount-set-state
        this.props.products && this.setState({
            subscribed_to_payroll: subscriptionService.isSubscribedToPayroll( this.props.products ),
            subscribed_to_ta: subscriptionService.isSubscribedToTA( this.props.products )
        }, () => {
            this.setState({ field_types: this.getFieldTypes() });
        });

        // set default values
        this.setDefaultValue();
    }

    /**
     * displays API errors in fields
     */
    componentWillReceiveProps( nextProps ) {
        !isEqual( nextProps.clone_data, this.props.clone_data ) &&
            Object.keys( nextProps.clone_data ).length &&
            this.handleCloneData( nextProps.clone_data );

        !isEqual( nextProps.errors, this.props.errors ) &&
            this.setState({ errors: nextProps.errors }, () => {
                this.handleApiErrors();
            });

        nextProps.submitted !== this.props.submitted && this.submitButton.setState({ disabled: nextProps.submitted });

        if ( nextProps.willExitTo !== this.props.willExitTo && nextProps.willExitTo !== '' ) {
            this.confirmExit( nextProps.willExitTo );
        }

        nextProps.hasUserMatch !== this.props.hasUserMatch
            && nextProps.hasUserMatch
            && this.setState({ show_confirm_modal: true });
    }

    onSubmitValidation = () => {
        if ( this.validateForm() ) {
            this.checkForUserMatch();
        }
    }
    /**
     * Gets input types depending on the license
     *
     * @returns {Object}
     */
    getFieldTypes() {
        const {
            subscribed_to_ta: TASubscribed,
            subscribed_to_payroll: PayrollSubscribed,
            field_types: fieldTypes
        } = this.state;

        let fields = null;
        if ( TASubscribed ) {
            if ( PayrollSubscribed ) {
                fields = TA_PLUS_PAYROLL_FIELDS;
            } else {
                fields = TA_FIELDS;
            }
        } else if ( PayrollSubscribed ) {
            fields = PAYROLL_FIELDS;
        }

        const inputTypes = fields && fields.sections.reduce( ( inputs, section ) => {
            flatten( section.rows ).forEach( ( field ) => {
                if ( field.field_type !== '' ) {
                    const key = snakeCase( field.field_type );
                    inputs[ key ].push( field.id );
                }
            });
            return inputs;
        }, {
            input: [],
            select: [],
            date_picker: [],
            switch_2: []
        });

        return inputTypes || fieldTypes;
    }

    /**
     * Gets onChange handler for form fields
     *
     * @param {String} id - ID of field
     * @param {String} type - Field Type
     * @returns {Function}
     */
    getOnChangeHandler( id, type ) {
        if ( type === 'select' ) {
            switch ( id ) {
                case 'tax_type':
                    return ({ value }) => {
                        const isConsultant = toLower( value ) === 'consultant';

                        !isConsultant && this.consultant_tax_rate && this.consultant_tax_rate.setState({
                            value: '',
                            error: false
                        }, () => this.updateState( 'consultant_tax_rate', null ) );

                        this.updateState( id, value );
                    };
                case 'sss_basis':
                    return ({ value }) => {
                        const isFixed = toLower( value ) === 'fixed';
                        !isFixed && this.sss_amount && this.sss_amount.setState({ value: '' }, () =>
                            this.updateState( 'sss_amount', null )
                        );
                        this.updateState( id, value );
                    };
                case 'philhealth_basis':
                    return ({ value }) => {
                        const isFixed = toLower( value ) === 'fixed';
                        !isFixed && this.philhealth_amount && this.philhealth_amount.setState({ value: '' }, () =>
                            this.updateState( 'philhealth_amount', null )
                        );
                        this.updateState( id, value );
                    };
                case 'hdmf_basis':
                    return ({ value }) => {
                        const isFixed = toLower( value ) === 'fixed';
                        !isFixed && this.hdmf_amount && this.hdmf_amount.setState({ value: '' }, () =>
                            this.updateState( 'hdmf_amount', null )
                        );
                        this.updateState( id, value );
                    };
                default:
                    return ({ value }) => this.updateState( id, value );
            }
        }

        switch ( id ) {
            case 'birth_date':
            case 'date_hired':
            case 'date_ended':
                return ( value ) => {
                    const formatted = formatDate( value, id === 'birth_date' ? 'MM/DD/YYYY' : DATE_FORMATS.API_2 );
                    this.updateState( id, formatted );
                };
            case 'mobile_number':
                return ( value ) => {
                    const formatted = formatMobileNumber( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateState( id, formatted )
                    );
                };
            case 'telephone_number':
                return ( value ) => {
                    const clean = stripNonNumeric( value );
                    const formatted = clean.substring( 0, 7 );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateState( id, formatted )
                    );
                };
            case 'zip_code':
                return ( value ) => {
                    const formatted = stripNonNumeric( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateState( id, formatted )
                    );
                };
            case 'sss_amount':
            case 'philhealth_amount':
            case 'hdmf_amount':
            case 'hours_per_day':
                return ( value ) => {
                    const clean = stripNonDigit( value );
                    let formatted = clean;
                    if ( id === 'hours_per_day' && !isNaN( clean ) ) {
                        formatted = Number( clean ) > 24 ? '24' : clean;
                    }

                    this[ id ].setState({ value: formatted }, () =>
                        this.updateState( id, formatted )
                    );
                };
            case 'timesheet_required':
                return ( value ) => {
                    this.updateState( id, value, () => {
                        if ( !value ) {
                            this.setState( ( prevState ) => ({
                                employee_data: Object.assign({}, prevState.employee_data, {
                                    regular_holiday_pay: true,
                                    special_holiday_pay: true
                                })
                            }) );
                        }
                    });
                };
            case 'formatRDO':
                return ( value ) => {
                    const formatted = formatRDO( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateState( id, formatted )
                    );
                };
            case 'sss_number':
                return ( value ) => {
                    const formatted = formatSSS( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateState( id, formatted )
                    );
                };
            case 'tin':
                return ( value ) => {
                    const formatted = formatTIN( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateState( id, formatted )
                    );
                };
            case 'hdmf_number':
                return ( value ) => {
                    const formatted = formatHDMF( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateState( id, formatted )
                    );
                };
            case 'philhealth_number':
                return ( value ) => {
                    const formatted = formatPhilHealth( value );
                    this[ id ].setState({ value: formatted }, () =>
                        this.updateState( id, formatted )
                    );
                };
            default:
                return ( value ) => this.updateState( id, value );
        }
    }

    setDefaultValue() {
        const { field_types: fieldTypes } = this.state;
        // Set defaults on select field types
        fieldTypes.select.forEach( ( select ) => {
            const field = this[ select ];
            const fieldValue = field.props.value;
            let fieldId = field.props.id;
            fieldId = fieldId.replace( '_select', '' );
            const fieldData = field.props.data;
            let includedInData = false;
            fieldData.forEach( ( data ) => {
                const selectValue = data.value;
                if ( selectValue === fieldValue ) {
                    includedInData = true;
                }
            });
            if ( includedInData ) {
                this.updateState( fieldId, fieldValue );
            }
        });
        // Set hired date
        this.updateState( 'date_hired', this.state.employee_data && this.state.employee_data.date_hired ? this.state.employee_data.date_hired : moment( new Date() ) );
    }

    /**
     * Builds additional config for date picker field provided
     *
     * @param {Object} field - Details of field to be rendered
     * @returns {Object}
     */
    buildAdditionalDatePickerConfig( field ) {
        const { employee_data: employeeData } = this.state;

        const disabled = false;
        let disabledDays = [];
        let dateHired = null;
        switch ( field.id ) {
            case 'birth_date':
                disabledDays = [{ after: new Date() }];
                break;
            case 'date_ended':
                dateHired = employeeData.date_hired ? employeeData.date_hired : moment( new Date() );
                disabledDays = [{ before: new Date( dateHired ) }];
                break;
            case 'date_hired':
                dateHired = employeeData[ field.id ] ? employeeData[ field.id ] : moment( new Date() );
                break;
            default:
        }

        return {
            disabled,
            disabledDays,
            selectedDay: field.id === 'date_hired' ? dateHired : employeeData[ field.id ]
        };
    }

    /**
     * Builds additional config for input field provided
     *
     * @param {Object} field - Details of field to be rendered
     * @returns {Object}
     */
    buildAdditionalInputConfig( field ) {
        const { employee_data: employeeData } = this.state;

        let extraConfig = {};
        let disabled = false;
        let hide = false;
        let isFixedBasis;
        let isConsultant;
        switch ( field.id ) {
            case 'base_pay':
                extraConfig = {
                    onFocus: () => this.base_pay.setState({ value: stripNonDigit( employeeData.base_pay ) }),
                    onBlur: ( value ) => this.base_pay.setState({ value: formatCurrency( value ) }, () =>
                        this.updateState( 'base_pay', formatCurrency( value ) )
                    )
                };
                break;
            case 'consultant_tax_rate':
                isConsultant = toLower( employeeData.tax_type ) === 'consultant';
                hide = !isConsultant;
                disabled = !isConsultant;
                extraConfig = {
                    validations: {
                        required: isConsultant
                    }
                };
                break;
            case 'sss_amount':
                isFixedBasis = toLower( employeeData.sss_basis ) === 'fixed';
                disabled = !isFixedBasis;
                extraConfig = {
                    validations: {
                        required: isFixedBasis
                    }
                };
                break;
            case 'philhealth_amount':
                isFixedBasis = toLower( employeeData.philhealth_basis ) === 'fixed';
                disabled = !isFixedBasis;
                extraConfig = {
                    validations: {
                        required: isFixedBasis
                    }
                };
                break;
            case 'hdmf_amount':
                isFixedBasis = toLower( employeeData.hdmf_basis ) === 'fixed';
                disabled = !isFixedBasis;
                extraConfig = {
                    validations: {
                        required: isFixedBasis
                    }
                };
                break;
            default:
        }

        return {
            disabled,
            hide,
            max: get( field, 'validations.max', null ),
            ...extraConfig
        };
    }

    handleCreate = ( userLinkConfirmed = false ) => {
        const { field_types: fieldTypes } = this.state;
        // Convert null values on switch inputs to Boolean
        this.setState( ( prevState ) => {
            const switchInputs = fieldTypes.switch_2.reduce( ( fields, field ) => {
                fields[ field ] = Boolean( prevState.employee_data[ field ]); // eslint-disable-line no-param-reassign
                return fields;
            }, {});

            return {
                employee_data: Object.assign({}, prevState.employee_data, switchInputs ),
                show_confirm_modal: false
            };
        }, () => {
            this.props.setHasUserMatch( false );
            this.props.submitEmployee(
                Object.assign(
                    {},
                    this.state.employee_data,
                    {
                        company_id: this.props.companyId,
                        payment_method: 'cash',
                        userlink_confirmed: userLinkConfirmed
                    }
                ) );
        });
    }

    checkForUserMatch = () => {
        const {
            last_name,
            first_name,
            middle_name,
            email
        } = this.state.employee_data;

        const data = {
            company_id: this.props.companyId,
            first_name,
            last_name,
            middle_name,
            email
        };

        this.props.checkForUserMatch({
            data,
            callback: () => this.handleCreate()
        });
    }

    /**
     * updates the state with the form inputs
     * @param {String}        key - field
     * @param {*}           value - new value
     * @param {Function} callback - callback function
     */
    updateState( key, value, callback ) {
        this.setState( ( prevState ) => {
            const newValue = value !== '' && !isNil( value ) ? value : null;
            const employeeData = Object.assign({}, prevState.employee_data, { [ key ]: newValue });
            return { employee_data: employeeData };
        }, () => callback && callback() );
    }

    /**
     * validates add employee form
     * @returns {boolean} wether the form inputs are valid or not
     */
    validateForm() {
        let valid = true;
        const { field_types: fieldTypes } = this.state;

        fieldTypes.input.forEach( ( input ) => {
            const field = this[ input ];
            if ( field._validate( field.state.value ) ) {
                valid = false;
            }
        });

        fieldTypes.select.forEach( ( select ) => {
            const field = this[ select ];
            if ( !field._checkRequire( typeof field.state.value === 'object' && field.state.value !== null ? field.state.value.value : field.state.value ) ) {
                valid = false;
            }
        });

        fieldTypes.date_picker.forEach( ( datePicker ) => {
            const field = this[ datePicker ];
            if ( field.checkRequired() ) {
                valid = false;
            }
        });

        return valid;
    }

    /**
     * displays a modal to ask exit confirmation
     * @param {String} to - Flag where to exit to
     */
    confirmExit( to ) {
        const { employee_data: employeeData } = this.state;
        const modified = Object.keys( employeeData ).some( ( key ) => employeeData[ key ] !== null );

        let callback;
        let toBatch = false;
        switch ( to ) {
            case 'employees':
                callback = this.props.onExit;
                break;
            case 'batch':
                toBatch = true;
                callback = this.props.onExitToBatch;
                break;
            default:
        }

        if ( modified ) {
            callback && callback();
        } else {
            browserHistory.push( `/employees${toBatch ? '/batch-add' : ''}`, true );
        }
    }

    /**
     * handles display of errors from API
     */
    handleApiErrors() {
        const {
            field_types: fieldTypes,
            errors
        } = this.state;
        const keys = Object.keys( errors );

        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( fieldTypes.input.includes( key ) || fieldTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, errorMessage: this.state.errors[ key ][ 0 ] });
                }

                if ( fieldTypes.date_picker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    /**
     * prepares page to clone employee data
     * @param {Object} data = employee data to clone
     */
    handleCloneData( data ) {
        const { field_types: fieldTypes } = this.state;

        const keys = Object.keys( data );
        keys.forEach( ( key ) => {
            if ( data[ key ] !== null && fieldTypes.input.includes( key ) ) {
                this[ key ].setState({ value: data[ key ] });
            }
        });

        this.setState( ( prevState ) => ({ employee_data: Object.assign({}, prevState.employee_data, data ) }) );
    }

    /**
     * Renders field according to config provided
     *
     * @param {Object} field - Details of field to be rendered
     * @returns {Object}
     */
    renderField( field ) {
        const { form_options } = this.props;
        const { employee_data: employeeData } = this.state;

        const {
            class_name: className,
            options,
            ...rest
        } = field;

        let fieldConfig = {};
        switch ( field.field_type ) {
            case 'input':
                fieldConfig = this.buildAdditionalInputConfig( field );
                break;
            case 'select':
                fieldConfig = { data: Array.isArray( options ) ? options : form_options[ options ] };
                break;
            case 'date-picker':
                fieldConfig = this.buildAdditionalDatePickerConfig( field );
                break;
            case 'switch2':
                fieldConfig = {
                    checked: employeeData[ field.id ],
                    disabled: [ 'regular_holiday_pay', 'special_holiday_pay' ].includes( field.id ) && !employeeData.timesheet_required
                };
                break;
            default:
        }

        let defaultValue;

        switch ( field.id ) {
            case 'status':
                defaultValue = 'single';
                break;
            case 'active':
                defaultValue = 'active';
                break;
            case 'address_line_1':
                defaultValue = 'nil';
                break;
            case 'primary_location_name':
                defaultValue = 'Head Office';
                break;
            case 'department_name':
                defaultValue = 'Operations';
                break;
            case 'position_name':
                defaultValue = 'Employee';
                break;
            case 'employment_type_name':
                defaultValue = 'Probationary';
                break;
            case 'sss_number':
                defaultValue = '00-0000000-0';
                break;
            case 'tin':
                defaultValue = '000-000-000-000';
                break;
            case 'hdmf_number':
                defaultValue = '0000-0000-0000';
                break;
            case 'philhealth_number':
                defaultValue = '00-000000000-0';
                break;
            case 'payroll_group_name':
                defaultValue = 'Semi-monthly';
                break;
            case 'base_pay_unit':
                defaultValue = 'Per Month';
                break;
            case 'tax_status':
                defaultValue = 'Z';
                break;
            case 'tax_type':
                defaultValue = 'Regular';
                break;
            case 'sss_basis':
                defaultValue = 'GROSS INCOME';
                break;
            case 'philhealth_basis':
                defaultValue = 'BASIC PAY';
                break;
            case 'hdmf_basis':
                defaultValue = 'GROSS INCOME';
                break;

            default:
                defaultValue = employeeData[ field.id ] || '';
        }

        const config = {
            ...rest,
            ...fieldConfig,
            value: defaultValue,
            onChange: this.getOnChangeHandler( field.id, field.field_type ),
            ref: ( ref ) => { this[ field.id ] = ref; }
        };

        return (
            <div className={ `${className} ${config.hide ? 'hide' : ''}` } key={ field.id }>
                { field.field_type && renderField( config ) }
            </div>
        );
    }

    /**
     * Renders form according to license
     *
     * @returns {Object}
     */
    renderForm() {
        const {
            subscribed_to_ta: TASubscribed,
            subscribed_to_payroll: PayrollSubscribed
        } = this.state;

        let fields = null;
        if ( TASubscribed ) {
            if ( PayrollSubscribed ) {
                fields = TA_PLUS_PAYROLL_FIELDS;
            } else {
                fields = TA_FIELDS;
            }
        } else if ( PayrollSubscribed ) {
            fields = PAYROLL_FIELDS;
        }

        return (
            <Container>
                { fields && fields.sections.map( ({ title, rows }) => (
                    <div key={ title }>
                        <H4>{ title }</H4>
                        { rows.map( ( row, i ) => (
                            <div className="row" key={ `${title}_${i}` }>
                                { row.map( ( field ) => this.renderField( field ) ) }
                            </div>
                        ) ) }
                        <br />
                    </div>
                ) ) }
            </Container>
        );
    }

    /**
     * Component Render Method
     */
    render() {
        const {
            employee_data: {
                first_name: firstName,
                last_name: lastName,
                email
            },
            show_confirm_modal: showConfirmModal
        } = this.state;

        return !this.props.loading && (
            <div className="form">

                <SalConfirm
                    onConfirm={ () => this.handleCreate( true ) }
                    onClose={ () => {
                        this.setState({ show_confirm_modal: false });
                        this.props.setHasUserMatch( false );
                    } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                The employee you are trying to add
                                <strong> { `${firstName} ${lastName}` } </strong>
                                with email
                                <strong> { email } </strong>
                                already has an existing user profile.
                                <br /><br />
                                To create their employee profile and automatically link to the existing user profile, click proceed.
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ showConfirmModal }
                    confirmText="Proceed"
                    backdrop="static"
                    buttonStyle="primary"
                />

                { this.renderForm() }

                <div className={ `foot ${this.props.loading ? 'hide' : ''}` }>
                    <Container>
                        <Button
                            id="buttonCancel"
                            className="cancel"
                            label="Cancel"
                            type="action"
                            size="large"
                            alt
                            disabled={ this.props.submitted }
                            onClick={ () => this.props.onExit() }
                        />
                        <span>
                            <Button
                                id="buttonCreate"
                                label={ this.props.submitted ? <Loader /> : 'Create' }
                                type="action"
                                size="large"
                                disabled={ this.props.submitted }
                                onClick={ this.onSubmitValidation }
                                ref={ ( ref ) => { this.submitButton = ref; } }
                            />
                        </span>
                    </Container>
                </div>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    form_options: makeSelectFormOptions(),
    loading: makeSelectLoading(),
    companyId: makeSelectCompanyId(),
    clone_data: makeSelectCloneData(),
    errors: makeSelectErrors(),
    submitted: makeSelectSubmitted(),
    products: makeSelectProducts(),
    hasUserMatch: makeSelectHasUserMatch()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        employeeAddActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddEmployeeForm );
