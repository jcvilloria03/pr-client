import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import A from 'components/A';
import Toggle from 'components/Toggle';
import SalConfirm from 'components/SalConfirm';
import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS } from 'utils/constants';

import SubHeader from '../SubHeader';

import EmployeeForm from './Form';
import * as employeeAddActions from './actions';
import { MainWrapper, ConfirmBodyWrapperStyle } from './styles';
import {
    makeSelectLoading,
    makeSelectNotification
} from './selectors';

/**
 * Employees Container
 */
export class AddEmployee extends React.PureComponent {
    static propTypes = {
        InitializeData: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool
    }

    static defaultProps = {
        loading: true
    };

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );
        this.state = {
            showModal: false,
            showBatchModal: false,
            willExitTo: ''
        };

        this.toggle = null;
    }

    /**
     * page authorization check
     */
    componentWillMount() {
        isAuthorized(['create.employee'], ( authorized ) => { !authorized && browserHistory.replace( '/unauthorized' ); });
    }

    /**
     * called when component is mounted to fetch form options.
     */
    componentDidMount() {
        this.props.InitializeData();
    }

    /**
     * handles confirmExit in child
     */
    handleExit = () => {
        this.setState({ showModal: false }, () => {
            this.setState({ showModal: true });
        });
    }

    /**
     * handles confirmExitToBatch in child
     */
    handleExitToBatch = () => {
        this.setState({ showBatchModal: false }, () => {
            this.setState({ showBatchModal: true });
        });
    }

    renderForm() {
        return (
            <EmployeeForm
                willExitTo={ this.state.willExitTo }
                onExit={ this.handleExit }
                onExitToBatch={ this.handleExitToBatch }
            />
        );
    }

    /**
     * Component Render Method
     */
    render() {
        const BatchUploadIcon = (
            <svg width="76px" height="65px" viewBox="0 0 76 65" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <title>icon</title>
                <defs>
                    <rect id="path-1" x="43" y="37" width="8" height="8"></rect>
                    <rect id="path-2" x="55" y="31" width="7" height="8"></rect>
                </defs>
                <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g id="toggle-2" transform="translate(-127.000000, -29.000000)">
                        <g id="toggle">
                            <g id="batch-upload" transform="translate(127.000000, 29.000000)">
                                <g id="icon">
                                    <path d="M66.9858827,9 L59.4823127,9 L59.1997977,8.75349372 C53.5043225,3.78395107 46.2226186,1 38.5069517,1 C25.2906045,1 13.619215,9.21842448 9.03094067,21.3830945 L8.78687155,22.0301831 L5,22.0301831 L5,27 L3,27 L3,53 L14.5907049,53 L14.8892649,53.3382642 C20.832035,60.0713373 29.3509176,64 38.5069517,64 C49.3788719,64 59.2998897,58.4474133 65.0528567,49.4560635 C68.2666016,44.4332805 70,38.5961784 70,32.5 C70,27.8540722 68.9943856,23.3524733 67.0790999,19.2330331 L66.9858827,19.0325394 L66.9858827,9 Z M7.4113622,20.0301831 L8.09528391,20.0301831 L7.15962714,20.6772718 C7.24143209,20.4603863 7.32535406,20.2446804 7.41136529,20.0301754 Z" id="Combined-Shape" className="stroke" strokeWidth="2"></path>
                                    <path d="M70.0652174,15.004039 L70.0652174,15 L39.9347826,15 L39.9347826,15.004039 C36.6367785,15.1209394 34,17.7606896 34,21 C34,24.2393104 36.6367785,26.8790606 39.9347826,26.995961 L39.9347826,27 L70.0652174,27 L70.0652174,26.995961 C73.3632215,26.8790606 76,24.2393104 76,21 C76,17.7606896 73.3632215,15.1209394 70.0652174,15.004039 Z" id="Combined-Shape" fillOpacity="0.1" className="fill"></path>
                                    <path d="M36,47 L6.00000958,47 C2.6862915,47 0,44.3137085 0,41 C0,37.6862915 2.6862915,35 6,35 L36,35 C39.3137085,35 42,37.6862915 42,41 C42,44.3137085 39.3137085,47 36,47 Z" id="Combined-Shape" fillOpacity="0.1" className="fill"></path>
                                    <rect id="Rectangle-9" className="fill" x="2" y="26" width="2" height="28"></rect>
                                    <rect id="Rectangle-9" className="fill" x="43" y="26" width="2" height="28"></rect>
                                    <rect id="Rectangle-10" className="fill" x="3" y="52" width="41" height="2"></rect>
                                    <rect id="Rectangle-10" className="fill" x="3" y="26" width="41" height="2"></rect>
                                    <rect id="Rectangle-11" className="fill" x="4" y="23.0027119" width="2" height="2.99728814"></rect>
                                    <rect id="Rectangle-12" className="fill" x="4" y="21" width="22" height="2"></rect>
                                    <rect id="Rectangle-11" className="fill" x="24" y="22" width="2" height="4"></rect>
                                    <rect id="Rectangle-15" className="fill" x="34" y="26" width="17" height="2"></rect>
                                    <rect id="Rectangle-15" className="fill" x="34" y="20" width="17" height="2"></rect>
                                    <rect id="Rectangle-15" className="fill" x="43" y="32" width="8" height="2"></rect>
                                    <g id="Rectangle-16">
                                        <use fill="#E5F6FC" fillRule="evenodd"></use>
                                        <rect className="stroke" strokeWidth="2" x="44" y="38" width="6" height="6"></rect>
                                    </g>
                                    <rect id="Rectangle-14" className="fill" x="44" y="48" width="11" height="2"></rect>
                                    <rect id="Rectangle-14" className="fill" x="29" y="14" width="28" height="2"></rect>
                                    <rect id="Rectangle-13" className="fill" x="28" y="14" width="2" height="12"></rect>
                                    <rect id="Rectangle-13" className="fill" x="55" y="14" width="2" height="36"></rect>
                                    <rect id="Rectangle-15" className="fill" x="56" y="20" width="6" height="2"></rect>
                                    <rect id="Rectangle-15" className="fill" x="56" y="14" width="6" height="2"></rect>
                                    <rect id="Rectangle-15" className="fill" x="55" y="26" width="7" height="2"></rect>
                                    <g id="Rectangle-16">
                                        <use fill="#E5F6FC" fillRule="evenodd"></use>
                                        <rect className="stroke" strokeWidth="2" x="56" y="32" width="5" height="6"></rect>
                                    </g>
                                    <rect id="Rectangle-14" className="fill" x="55" y="42" width="11" height="2"></rect>
                                    <rect id="Rectangle-14" className="fill" x="40" y="8" width="28" height="2"></rect>
                                    <rect id="Rectangle-13" className="fill" x="39" y="8" width="2" height="8"></rect>
                                    <rect id="Rectangle-13" className="fill" x="66" y="8" width="2" height="36"></rect>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        );

        const ManualEntryIcon = (
            <svg width="76px" height="67px" viewBox="0 0 76 67" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <title>icon</title>
                <defs>
                    <polygon id="path-1" points="23 33 54 33 54 42 48.1835938 42 23 42"></polygon>
                    <rect id="path-2" x="23" y="45" width="14" height="9"></rect>
                    <rect id="path-3" x="40" y="45" width="14" height="9"></rect>
                </defs>
                <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g id="toggle-2" transform="translate(-709.000000, -26.000000)">
                        <g id="toggle">
                            <g id="single-entry" transform="translate(709.000000, 26.000000)">
                                <g id="icon">
                                    <path d="M61.0147622,1 L16.0147622,1 L16.0147622,12.4041844 L15.7380825,12.6940097 C10.1752704,18.5211221 7.01476217,26.2593234 7.01476217,34.5 C7.01476217,51.8980709 21.0472996,66 38.3548387,66 C49.1728227,66 59.0452408,60.4484779 64.770653,51.4578674 C67.9694808,46.4347505 69.6949153,40.5969834 69.6949153,34.5 C69.6949153,26.4173522 66.6551525,18.8159246 61.2816409,13.0234716 L61.0147622,12.7357859 L61.0147622,1 Z" id="Combined-Shape" className="stroke" strokeWidth="2"></path>
                                    <rect id="Rectangle-3" className="fill" transform="translate(38.500000, 1.000000) rotate(90.000000) translate(-38.500000, -1.000000) " x="37.5" y="-22.5" width="2" height="47"></rect>
                                    <rect id="Rectangle-3" className="fill" x="15" y="2" width="2" height="54"></rect>
                                    <rect id="Rectangle-3" className="fill" x="60" y="2" width="2" height="54"></rect>
                                    <rect id="Rectangle-3" className="fill" transform="translate(38.500000, 1.000000) rotate(90.000000) translate(-38.500000, -1.000000) " x="37.5" y="-22.5" width="2" height="47"></rect>
                                    <rect id="Rectangle-3" className="fill" x="15" y="2" width="2" height="54"></rect>
                                    <rect id="Rectangle-3" className="fill" x="60" y="2" width="2" height="54"></rect>
                                    <path d="M70.0652174,17.004039 L70.0652174,17 L39.9347826,17 L39.9347826,17.004039 C36.6367785,17.1209394 34,19.7606896 34,23 C34,26.2393104 36.6367785,28.8790606 39.9347826,28.995961 L39.9347826,29 L70.0652174,29 L70.0652174,28.995961 C73.3632215,28.8790606 76,26.2393104 76,23 C76,19.7606896 73.3632215,17.1209394 70.0652174,17.004039 Z" id="Combined-Shape" fillOpacity="0.1" className="fill"></path>
                                    <path d="M36.0652174,37.004039 L36.0652174,37 L5.93478261,37 L5.93478261,37.004039 C2.63677847,37.1209394 0,39.7606896 0,43 C0,46.2393104 2.63677847,48.8790606 5.93478261,48.995961 L5.93478261,49 L36.0652174,49 L36.0652174,48.995961 C39.3632215,48.8790606 42,46.2393104 42,43 C42,39.7606896 39.3632215,37.1209394 36.0652174,37.004039 Z" id="Combined-Shape" fillOpacity="0.1" className="fill"></path>
                                    <rect id="Rectangle-2" className="fill" x="23" y="10" width="31" height="2"></rect>
                                    <rect id="Rectangle-2" className="fill" x="23" y="20.5" width="31" height="2"></rect>
                                    <rect id="Rectangle-2" className="fill" x="23" y="26.5" width="31" height="2"></rect>
                                    <rect id="Rectangle-2" className="fill" x="23" y="15" width="14" height="2"></rect>
                                    <rect id="Rectangle-2" className="fill" x="40" y="15" width="14" height="2"></rect>
                                    <g id="Rectangle-8">
                                        <use fillOpacity="0.2" className="fill" fillRule="evenodd"></use>
                                        <path className="stroke" strokeWidth="2" d="M24,34 L24,41 L53,41 L53,34 L24,34 Z"></path>
                                    </g>
                                    <g id="Rectangle-8">
                                        <use fillOpacity="0.2" className="fill" fillRule="evenodd"></use>
                                        <rect className="stroke" strokeWidth="2" x="24" y="46" width="12" height="7"></rect>
                                    </g>
                                    <g id="Rectangle-8">
                                        <use fillOpacity="0.2" className="fill" fillRule="evenodd"></use>
                                        <rect className="stroke" strokeWidth="2" x="41" y="46" width="12" height="7"></rect>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        );

        return (
            <div>
                <Helmet
                    title="Add Employee"
                    meta={ [
                        { name: 'description', content: 'Create a new Employee' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <MainWrapper>
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/employees', true ); } }
                        onClose={ () => this.setState({ willExitTo: '' }, () => this.toggle.setState({ value: 'single' }) ) }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showModal }
                    />
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/employees/batch-add', true ); } }
                        onClose={ () => this.setState({ willExitTo: '' }, () => this.toggle.setState({ value: 'single' }) ) }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showBatchModal }
                    />
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => { e.preventDefault(); this.setState({ willExitTo: 'employees' }); } }
                            >
                                &#8592; Back to Employees
                            </A>
                        </Container>
                    </div>
                    <Container>
                        <div className="heading">
                            <h3>Add Employees</h3>
                            <p>Add employees along with their personal and payroll information to your existing records. You can add multiple entries at once through Batch Upload or create one entry after another through Manual Entry. </p>
                        </div>
                        <div className={ `loader ${this.props.loading ? '' : 'hide'}` }>
                            <Loader />
                        </div>
                        <div className={ `toggle ${this.props.loading ? 'hide' : ''}` }>
                            <Toggle
                                options={ [
                                        { title: 'Batch Upload', subtext: 'Upload multiple entries at once using template.', value: 'batch', icon: BatchUploadIcon },
                                        { title: 'Manual Entry', subtext: 'Create entries one at a time.', value: 'single', icon: ManualEntryIcon }
                                ] }
                                defaultSelected="single"
                                onChange={ ( value ) => { if ( value === 'batch' ) { this.setState({ willExitTo: 'batch' }); } } }
                                ref={ ( ref ) => { this.toggle = ref; } }
                            />
                        </div>
                    </Container>
                    { this.renderForm() }
                </MainWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        employeeAddActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddEmployee );
