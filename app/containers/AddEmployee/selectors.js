import { createSelector } from 'reselect';

/**
 * Direct selector to the top level domain
 */
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Direct selector to the view state domain
 */
const selectAddEmployeeDomain = () => ( state ) => state.get( 'add_employee' );

const makeSelectLoading = () => createSelector(
    selectAddEmployeeDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectFormOptions = () => createSelector(
    selectAddEmployeeDomain(),
    ( substate ) => substate.get( 'form_options' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectAddEmployeeDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
    selectAddEmployeeDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectCompanyId = () => createSelector(
    selectAddEmployeeDomain(),
    ( substate ) => substate.get( 'company_id' )
);

const makeSelectCloneData = () => createSelector(
    selectAddEmployeeDomain(),
    ( substate ) => {
        let cloneData;
        try {
            cloneData = substate.get( 'clone_data' ).toJS();
        } catch ( err ) {
            cloneData = substate.get( 'clone_data' );
        }

        return cloneData;
    }
);

const makeSelectErrors = () => createSelector(
    selectAddEmployeeDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

const makeSelectProducts = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'products' ).toJS()
);

const makeSelectProductSeats = () => createSelector(
    selectAppDomain(),
    ( substate ) => {
        let productSeats;
        try {
            productSeats = substate.get( 'userInformation' ).get( 'product_seats' ).toJS();
        } catch ( error ) {
            productSeats = substate.get( 'userInformation' ).get( 'product_seats' );
        }

        return productSeats;
    }
);

const makeSelectHasUserMatch = () => createSelector(
    selectAddEmployeeDomain(),
    ( substate ) => substate.get( 'has_user_match' )
);

export {
    makeSelectCloneData,
    makeSelectSubmitted,
    makeSelectCompanyId,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectProducts,
    makeSelectProductSeats,
    makeSelectHasUserMatch
};
