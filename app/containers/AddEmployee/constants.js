import { DATE_FORMATS } from 'utils/constants';

/*
 *
 * Add Employee constants
 *
 */

const namespace = 'app/AddEmployee';
export const INITIALIZE = `${namespace}/INITIALIZE`;
export const LOADING = `${namespace}/LOADING`;
export const SET_COMPANY_ID = `${namespace}/SET_COMPANY_ID`;
export const SET_ERRORS = `${namespace}/SET_ERRORS`;
export const SET_FORM_OPTIONS = `${namespace}/SET_FORM_OPTIONS`;
export const SET_CLONE_DATA = `${namespace}/SET_CLONE_DATA`;
export const SUBMIT_FORM = `${namespace}/SUBMIT_FORM`;
export const SUBMITTED = `${namespace}/SUBMITTED`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const CHECK_FOR_USER_MATCH = `${namespace}/CHECK_FOR_USER_MATCH`;
export const SET_HAS_USER_MATCH = `${namespace}/SET_HAS_USER_MATCH`;

export const OPTIONS = {
    GENDER: [
        { label: 'Male', value: 'male' },
        { label: 'Female', value: 'female' }
    ],
    MARITAL_STATUS: [
        { label: 'Single', value: 'single' },
        { label: 'Married', value: 'married' },
        { label: 'Separated', value: 'separated' },
        { label: 'Widowed', value: 'widowed' }
    ],
    TEAM_ROLE: [
        { label: 'Leader', value: 'leader' },
        { label: 'Member', value: 'member' }
    ]
};

export const STATUS_TYPES = [
    { label: 'Active', value: 'active' },
    { label: 'Inactive', value: 'inactive' },
    { label: 'Semi-Active', value: 'semi-active' }
];

/**
 * Fields to be rendered for TA account
 */
export const TA_FIELDS = {
    sections: [
        {
            title: 'Basic Information',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'employee_id',
                        label: 'Employee ID',
                        placeholder: 'Type in employee ID here',
                        validations: {
                            required: true
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'last_name',
                        label: 'Last Name',
                        placeholder: 'Last Name',
                        validations: {
                            required: true
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'first_name',
                        label: 'First Name',
                        placeholder: 'First Name',
                        validations: {
                            required: true
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'middle_name',
                        label: 'Middle Name',
                        placeholder: 'Middle Name',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'date-picker',
                        class_name: 'col-xs-4 date',
                        id: 'birth_date',
                        label: 'Birth Date',
                        placeholder: 'Select birthday',
                        validations: {
                            required: false
                        },
                        display_format: DATE_FORMATS.DISPLAY
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'gender',
                        label: 'Gender',
                        placeholder: 'Select gender',
                        validations: {
                            required: false
                        },
                        options: OPTIONS.GENDER
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'status',
                        label: 'Marital Status',
                        placeholder: 'Select marital status',
                        validations: {
                            required: true
                        },
                        options: OPTIONS.MARITAL_STATUS
                    }
                ]
            ]
        },
        {
            title: 'Contact Information',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'email',
                        label: 'Email Address',
                        placeholder: 'Email Address',
                        validations: {
                            required: true
                        },
                        type: 'email'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'mobile_number',
                        label: 'Mobile Number',
                        placeholder: 'Mobile number here',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'telephone_number',
                        label: 'Telephone Number',
                        placeholder: 'Telephone number here',
                        validations: {
                            required: false,
                            max: 7
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-12',
                        id: 'address_line_1',
                        label: 'Address Line 1',
                        placeholder: 'Enter Address',
                        validations: {
                            required: true
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-12',
                        id: 'address_line_2',
                        label: 'Address Line 2',
                        placeholder: 'Enter Address',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'country',
                        label: 'Country',
                        placeholder: 'Select country',
                        validations: {
                            required: false
                        },
                        options: 'country'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'city',
                        label: 'City',
                        placeholder: 'City',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'zip_code',
                        label: 'Zip code',
                        placeholder: 'Zip code',
                        validations: {
                            required: false
                        }
                    }
                ]
            ]
        },
        {
            title: 'Employee Information',
            rows: [
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'active',
                        label: 'Employee Status',
                        placeholder: 'Select Status',
                        validations: {
                            required: true
                        },
                        options: 'employment_status'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'hours_per_day',
                        label: 'Hour Per Day',
                        placeholder: 'Hour Per Day',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'rank_name',
                        label: 'Rank',
                        placeholder: 'Select rank',
                        validations: {
                            required: false
                        },
                        options: 'rank_name'
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'primary_location_name',
                        label: 'Primary Work Location',
                        placeholder: 'Select location',
                        validations: {
                            required: true
                        },
                        options: 'work_location'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'employment_type_name',
                        label: 'Employment Type',
                        placeholder: 'Select employment type',
                        validations: {
                            required: true
                        },
                        options: 'employment_type_name'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'team_name',
                        label: 'Team',
                        placeholder: 'Select team',
                        validations: {
                            required: false
                        },
                        options: 'team_name'
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'secondary_location_name',
                        label: 'Secondary Work Location',
                        placeholder: 'Select location',
                        validations: {
                            required: false
                        },
                        options: 'work_location'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'position_name',
                        label: 'Position',
                        placeholder: 'Select position',
                        validations: {
                            required: true
                        },
                        options: 'position_name'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'team_role',
                        label: 'Team Role',
                        placeholder: 'Select role',
                        validations: {
                            required: false
                        },
                        options: OPTIONS.TEAM_ROLE
                    }
                ],
                [
                    {
                        field_type: '',
                        class_name: 'col-xs-8'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'department_name',
                        label: 'Department',
                        placeholder: 'Select department',
                        validations: {
                            required: true
                        },
                        options: 'department_name'
                    }
                ]
            ]
        },
        {
            title: 'Hiring Information',
            rows: [
                [
                    {
                        field_type: 'date-picker',
                        class_name: 'col-xs-4 date',
                        id: 'date_hired',
                        label: 'Start Date',
                        placeholder: 'Select Date',
                        validations: {
                            required: true
                        },
                        display_format: DATE_FORMATS.DISPLAY,
                        api_format: DATE_FORMATS.API_2
                    },
                    {
                        field_type: 'date-picker',
                        class_name: 'col-xs-4 date',
                        id: 'date_ended',
                        label: 'End Date',
                        placeholder: 'Select Date',
                        validations: {
                            required: false
                        },
                        display_format: DATE_FORMATS.DISPLAY
                    }
                ]
            ]
        },
        {
            title: 'Entitlements',
            rows: [
                [
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'timesheet_required',
                        placeholder: 'Time Sheet Required'
                    },
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'rest_day_pay',
                        placeholder: 'Rest Day Pay'
                    }
                ],
                [
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch sub-switch',
                        id: 'regular_holiday_pay',
                        placeholder: 'Unworked Regular Holiday Pay'
                    },
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'holiday_premium_pay',
                        placeholder: 'Holiday Premium Pay'
                    }
                ],
                [
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch sub-switch',
                        id: 'special_holiday_pay',
                        placeholder: 'Unworked Special Holiday Pay'
                    },
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'differential',
                        placeholder: 'Night Differential Pay'
                    }
                ],
                [
                    {
                        field_type: '',
                        class_name: 'col-xs-4'
                    },
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'overtime',
                        placeholder: 'Overtime Pay'
                    }
                ]
            ]
        }
    ]
};

/**
 * Fields to be rendered for Payroll account
 */
export const PAYROLL_FIELDS = {
    sections: [
        {
            title: 'Basic Information',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'employee_id',
                        label: 'Employee ID',
                        placeholder: 'Type in employee ID here',
                        validations: {
                            required: true
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'last_name',
                        label: 'Last Name',
                        placeholder: 'Last Name',
                        validations: {
                            required: true
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'first_name',
                        label: 'First Name',
                        placeholder: 'First Name',
                        validations: {
                            required: true
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'middle_name',
                        label: 'Middle Name',
                        placeholder: 'Middle Name',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'date-picker',
                        class_name: 'col-xs-4 date',
                        id: 'birth_date',
                        label: 'Birth Date',
                        placeholder: 'Select birthday',
                        validations: {
                            required: false
                        },
                        display_format: DATE_FORMATS.DISPLAY
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'gender',
                        label: 'Gender',
                        placeholder: 'Select gender',
                        validations: {
                            required: false
                        },
                        options: OPTIONS.GENDER
                    }
                ]
            ]
        },
        {
            title: 'Contact Information',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'email',
                        label: 'Email Address',
                        placeholder: 'Email Address',
                        validations: {
                            required: true
                        },
                        type: 'email'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'mobile_number',
                        label: 'Mobile Number',
                        placeholder: 'Mobile number here',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'telephone_number',
                        label: 'Telephone Number',
                        placeholder: 'Telephone number here',
                        validations: {
                            required: false,
                            max: 7
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-12',
                        id: 'address_line_1',
                        label: 'Address Line 1',
                        placeholder: 'Enter Address',
                        validations: {
                            required: true
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-12',
                        id: 'address_line_2',
                        label: 'Address Line 2',
                        placeholder: 'Enter Address',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'country',
                        label: 'Country',
                        placeholder: 'Select country',
                        validations: {
                            required: false
                        },
                        options: 'country'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'city',
                        label: 'City',
                        placeholder: 'City',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'zip_code',
                        label: 'Zip code',
                        placeholder: 'Zip code',
                        validations: {
                            required: false
                        }
                    }
                ]
            ]
        },
        {
            title: 'Employment Information',
            rows: [
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'active',
                        label: 'Employee Status',
                        placeholder: 'Select Status',
                        validations: {
                            required: true
                        },
                        options: 'employment_status'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'primary_location_name',
                        label: 'Primary Location',
                        placeholder: 'Select location',
                        validations: {
                            required: true
                        },
                        options: 'work_location'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'department_name',
                        label: 'Department',
                        placeholder: 'Select department',
                        validations: {
                            required: true
                        },
                        options: 'department_name'
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'rank_name',
                        label: 'Rank',
                        placeholder: 'Select rank',
                        validations: {
                            required: false
                        },
                        options: 'rank_name'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'position_name',
                        label: 'Position',
                        placeholder: 'Select position',
                        validations: {
                            required: true
                        },
                        options: 'position_name'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'employment_type_name',
                        label: 'Employment Type',
                        placeholder: 'Select employment type',
                        validations: {
                            required: true
                        },
                        options: 'employment_type_name'
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'cost_center_name',
                        label: 'Cost Center',
                        placeholder: 'Select cost center',
                        validations: {
                            required: false
                        },
                        options: 'cost_center_name'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'rdo',
                        label: 'RDO',
                        placeholder: 'RDO',
                        validations: {
                            required: false
                        },
                        type: 'rdo'
                    }
                ]
            ]
        },
        {
            title: 'Government Numbers',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'sss_number',
                        label: 'Social Security System (SSS)',
                        placeholder: '00-0000000-0',
                        validations: {
                            required: true
                        },
                        type: 'sss'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'tin',
                        label: 'Tax Identification Number (TIN)',
                        placeholder: '000-000-000-000',
                        validations: {
                            required: true
                        },
                        type: 'tin'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'hdmf_number',
                        label: 'HDMF',
                        placeholder: '0000-0000-0000',
                        validations: {
                            required: true
                        },
                        type: 'hdmf'
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'philhealth_number',
                        label: 'PhilHealth',
                        placeholder: '00-000000000-0',
                        validations: {
                            required: true
                        },
                        type: 'philhealth'
                    }
                ]
            ]
        },
        {
            title: 'Hiring Information',
            rows: [
                [
                    {
                        field_type: 'date-picker',
                        class_name: 'col-xs-4 date',
                        id: 'date_hired',
                        label: 'Start Date',
                        placeholder: 'Select Date',
                        validations: {
                            required: true
                        },
                        display_format: DATE_FORMATS.DISPLAY,
                        api_format: DATE_FORMATS.API_2
                    },
                    {
                        field_type: 'date-picker',
                        class_name: 'col-xs-4 date',
                        id: 'date_ended',
                        label: 'End Date',
                        placeholder: 'Select Date',
                        validations: {
                            required: false
                        },
                        display_format: DATE_FORMATS.DISPLAY
                    }
                ]
            ]
        },
        {
            title: 'Entitlements',
            rows: [
                [
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'entitled_deminimis',
                        placeholder: 'De Minimis'
                    }
                ]
            ]
        },
        {
            title: 'Payroll Details',
            rows: [
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'payroll_group_name',
                        label: 'Payroll Group',
                        placeholder: 'Select payroll group',
                        validations: {
                            required: true
                        },
                        options: 'payroll_group_name'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'base_pay',
                        label: 'Base Pay',
                        placeholder: 'Enter basic pay',
                        validations: {
                            required: true
                        }
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'base_pay_unit',
                        label: 'Base Pay Unit',
                        placeholder: 'Select base pay unit',
                        validations: {
                            required: true
                        },
                        options: 'base_pay_unit'
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'tax_status',
                        label: 'Tax Status',
                        placeholder: 'Select tax status',
                        validations: {
                            required: true
                        },
                        options: 'tax_status'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'tax_type',
                        label: 'Tax Type',
                        placeholder: 'Select tax type',
                        validations: {
                            required: true
                        },
                        options: 'tax_type'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'consultant_tax_rate',
                        label: 'Consultant Tax Rate (%)',
                        placeholder: 'Enter rate',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'hours_per_day',
                        label: 'Hours per Day',
                        placeholder: 'Enter amount',
                        validations: {
                            required: false,
                            max: 2
                        }
                    }
                ]
            ]
        },
        {
            title: 'Government Contributions',
            rows: [
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'sss_basis',
                        label: 'SSS Basis',
                        placeholder: 'Select contribution basis',
                        validations: {
                            required: true
                        },
                        options: 'sss_basis'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'sss_amount',
                        label: 'SSS Amount',
                        placeholder: 'Enter amount',
                        validations: {
                            required: true
                        }
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'philhealth_basis',
                        label: 'PhilHealth Basis',
                        placeholder: 'Select contribution basis',
                        validations: {
                            required: true
                        },
                        options: 'philhealth_basis'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'philhealth_amount',
                        label: 'PhilHealth Amount',
                        placeholder: 'Enter amount',
                        validations: {
                            required: true
                        }
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'hdmf_basis',
                        label: 'HDMF Basis',
                        placeholder: 'Select contribution basis',
                        validations: {
                            required: true
                        },
                        options: 'hdmf_basis'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'hdmf_amount',
                        label: 'HDMF Amount',
                        placeholder: 'Enter amount',
                        validations: {
                            required: true
                        }
                    }
                ]
            ]
        }
    ]
};

/**
 * Fields to be rendered for TA+Payroll account
 */
export const TA_PLUS_PAYROLL_FIELDS = {
    sections: [
        {
            title: 'Basic Information',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'employee_id',
                        label: 'Employee ID',
                        placeholder: 'Type in employee ID here',
                        validations: {
                            required: true
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'last_name',
                        label: 'Last Name',
                        placeholder: 'Last Name',
                        validations: {
                            required: true
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'first_name',
                        label: 'First Name',
                        placeholder: 'First Name',
                        validations: {
                            required: true
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'middle_name',
                        label: 'Middle Name',
                        placeholder: 'Middle Name',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'date-picker',
                        class_name: 'col-xs-4 date',
                        id: 'birth_date',
                        label: 'Birth Date',
                        placeholder: 'Select birthday',
                        validations: {
                            required: false
                        },
                        display_format: DATE_FORMATS.DISPLAY
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'gender',
                        label: 'Gender',
                        placeholder: 'Select gender',
                        validations: {
                            required: false
                        },
                        options: OPTIONS.GENDER
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'status',
                        label: 'Marital Status',
                        placeholder: 'Select marital status',
                        validations: {
                            required: true
                        },
                        options: OPTIONS.MARITAL_STATUS
                    }
                ]
            ]
        },
        {
            title: 'Contact Information',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'email',
                        label: 'Email Address',
                        placeholder: 'Email Address',
                        validations: {
                            required: true
                        },
                        type: 'email'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'mobile_number',
                        label: 'Mobile Number',
                        placeholder: 'Mobile number here',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'telephone_number',
                        label: 'Telephone Number',
                        placeholder: 'Telephone number here',
                        validations: {
                            required: false,
                            max: 7
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-12',
                        id: 'address_line_1',
                        label: 'Address Line 1',
                        placeholder: 'Enter Address',
                        validations: {
                            required: true
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-12',
                        id: 'address_line_2',
                        label: 'Address Line 2',
                        placeholder: 'Enter Address',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'country',
                        label: 'Country',
                        placeholder: 'Select country',
                        validations: {
                            required: false
                        },
                        options: 'country'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'city',
                        label: 'City',
                        placeholder: 'City',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'zip_code',
                        label: 'Zip code',
                        placeholder: 'Zip code',
                        validations: {
                            required: false
                        }
                    }
                ]
            ]
        },
        {
            title: 'Employment Information',
            rows: [
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'active',
                        label: 'Employee Status',
                        placeholder: 'Select Status',
                        validations: {
                            required: true
                        },
                        options: 'employment_status'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'primary_location_name',
                        label: 'Primary Work Location',
                        placeholder: 'Select location',
                        validations: {
                            required: true
                        },
                        options: 'work_location'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'secondary_location_name',
                        label: 'Secondary Work Location',
                        placeholder: 'Select location',
                        validations: {
                            required: false
                        },
                        options: 'work_location'
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'position_name',
                        label: 'Position',
                        placeholder: 'Select position',
                        validations: {
                            required: true
                        },
                        options: 'position_name'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'rank_name',
                        label: 'Rank',
                        placeholder: 'Select rank',
                        validations: {
                            required: false
                        },
                        options: 'rank_name'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'department_name',
                        label: 'Department',
                        placeholder: 'Select department',
                        validations: {
                            required: true
                        },
                        options: 'department_name'
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'employment_type_name',
                        label: 'Employment Type',
                        placeholder: 'Select employment type',
                        validations: {
                            required: true
                        },
                        options: 'employment_type_name'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'cost_center_name',
                        label: 'Cost Center',
                        placeholder: 'Select cost center',
                        validations: {
                            required: false
                        },
                        options: 'cost_center_name'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'rdo',
                        label: 'RDO',
                        placeholder: 'RDO',
                        validations: {
                            required: false
                        },
                        type: 'rdo'
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'team_name',
                        label: 'Team',
                        placeholder: 'Select team',
                        validations: {
                            required: false
                        },
                        options: 'team_name'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'team_role',
                        label: 'Team Role',
                        placeholder: 'Select role',
                        validations: {
                            required: false
                        },
                        options: OPTIONS.TEAM_ROLE
                    }
                ]
            ]
        },
        {
            title: 'Government Numbers',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'sss_number',
                        label: 'Social Security System (SSS)',
                        placeholder: '00-0000000-0',
                        validations: {
                            required: true
                        },
                        type: 'sss'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'tin',
                        label: 'Tax Identification Number (TIN)',
                        placeholder: '000-000-000-000',
                        validations: {
                            required: true
                        },
                        type: 'tin'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'hdmf_number',
                        label: 'HDMF',
                        placeholder: '0000-0000-0000',
                        validations: {
                            required: true
                        },
                        type: 'hdmf'
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'philhealth_number',
                        label: 'PhilHealth',
                        placeholder: '00-000000000-0',
                        validations: {
                            required: true
                        },
                        type: 'philhealth'
                    }
                ]
            ]
        },
        {
            title: 'Hiring Information',
            rows: [
                [
                    {
                        field_type: 'date-picker',
                        class_name: 'col-xs-4 date',
                        id: 'date_hired',
                        label: 'Start Date',
                        placeholder: 'Select Date',
                        validations: {
                            required: true
                        },
                        display_format: DATE_FORMATS.DISPLAY,
                        api_format: DATE_FORMATS.API_2
                    },
                    {
                        field_type: 'date-picker',
                        class_name: 'col-xs-4 date',
                        id: 'date_ended',
                        label: 'End Date',
                        placeholder: 'Select Date',
                        validations: {
                            required: false
                        },
                        display_format: DATE_FORMATS.DISPLAY
                    }
                ]
            ]
        },
        {
            title: 'Entitlements',
            rows: [
                [
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'timesheet_required',
                        placeholder: 'Time Sheet Required'
                    },
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'rest_day_pay',
                        placeholder: 'Rest Day Pay'
                    },
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'entitled_deminimis',
                        placeholder: 'De Minimis'
                    }
                ],
                [
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch sub-switch',
                        id: 'regular_holiday_pay',
                        placeholder: 'Unworked Regular Holiday Pay'
                    },
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'holiday_premium_pay',
                        placeholder: 'Holiday Premium Pay'
                    },
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'overtime',
                        placeholder: 'Overtime Pay'
                    }
                ],
                [
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch sub-switch',
                        id: 'special_holiday_pay',
                        placeholder: 'Unworked Special Holiday Pay'
                    },
                    {
                        field_type: 'switch2',
                        class_name: 'col-xs-4 switch',
                        id: 'differential',
                        placeholder: 'Night Differential Pay'
                    }
                ]
            ]
        },
        {
            title: 'Payroll Details',
            rows: [
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'payroll_group_name',
                        label: 'Payroll Group',
                        placeholder: 'Select payroll group',
                        validations: {
                            required: true
                        },
                        options: 'payroll_group_name'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'base_pay',
                        label: 'Base Pay',
                        placeholder: 'Enter basic pay',
                        validations: {
                            required: true
                        }
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'base_pay_unit',
                        label: 'Base Pay Unit',
                        placeholder: 'Select base pay unit',
                        validations: {
                            required: true
                        },
                        options: 'base_pay_unit'
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'tax_status',
                        label: 'Tax Status',
                        placeholder: 'Select tax status',
                        validations: {
                            required: true
                        },
                        options: 'tax_status'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'tax_type',
                        label: 'Tax Type',
                        placeholder: 'Select tax type',
                        validations: {
                            required: true
                        },
                        options: 'tax_type'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'consultant_tax_rate',
                        label: 'Consultant Tax Rate (%)',
                        placeholder: 'Enter rate',
                        validations: {
                            required: false
                        }
                    }
                ],
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'hours_per_day',
                        label: 'Hours per Day',
                        placeholder: 'Enter amount',
                        validations: {
                            required: false,
                            max: 2
                        }
                    }
                ]
            ]
        },
        {
            title: 'Government Contributions',
            rows: [
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'sss_basis',
                        label: 'SSS Basis',
                        placeholder: 'Select contribution basis',
                        validations: {
                            required: true
                        },
                        options: 'sss_basis'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'sss_amount',
                        label: 'SSS Amount',
                        placeholder: 'Enter amount',
                        validations: {
                            required: true
                        }
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'philhealth_basis',
                        label: 'PhilHealth Basis',
                        placeholder: 'Select contribution basis',
                        validations: {
                            required: true
                        },
                        options: 'philhealth_basis'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'philhealth_amount',
                        label: 'PhilHealth Amount',
                        placeholder: 'Enter amount',
                        validations: {
                            required: true
                        }
                    }
                ],
                [
                    {
                        field_type: 'select',
                        class_name: 'col-xs-4',
                        id: 'hdmf_basis',
                        label: 'HDMF Basis',
                        placeholder: 'Select contribution basis',
                        validations: {
                            required: true
                        },
                        options: 'hdmf_basis'
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-4',
                        id: 'hdmf_amount',
                        label: 'HDMF Amount',
                        placeholder: 'Enter amount',
                        validations: {
                            required: true
                        }
                    }
                ]
            ]
        }
    ]
};
