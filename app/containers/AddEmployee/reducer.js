import { fromJS } from 'immutable';
import decimal from 'js-big-decimal';
import {
    LOADING,
    SET_FORM_OPTIONS,
    SET_COMPANY_ID,
    SET_CLONE_DATA,
    SET_ERRORS,
    SUBMITTED,
    NOTIFICATION_SAGA,
    SET_HAS_USER_MATCH
} from './constants';

import { RESET_STORE } from '../App/constants';
import { DATE_FORMATS } from '../../utils/constants';
import { formatDate } from '../../utils/functions';

const initialState = fromJS({
    loading: true,
    company_id: null,
    errors: {},
    submitted: false,
    clone_data: {},
    form_options: {
        cost_center_name: [],
        work_location: [],
        department_name: [],
        rank_name: [],
        payroll_group_name: [],
        base_pay_unit: [],
        tax_type: [],
        tax_status: [],
        sss_basis: [],
        philhealth_basis: [],
        hdmf_basis: [],
        country: [],
        team_name: [],
        position_name: [],
        employment_type_name: []
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    has_user_match: false
});

/**
 * transforms employee data to cater page needs in cloning employee
 * @param data
 * @returns {{company_id: null, employee_id: null, first_name: null, last_name: null, middle_name: null, email: null, telephone_number: null, mobile_number: null, address_line_1: null, address_line_2: null, country, city: null, zip_code: null, birth_date: null, gender: null, date_hired: (*|null), department_name: (Array|*|null), rank_name: (Array|*|null), cost_center_name: (Array|*|null), work_location: *, sss_number: null, tin: null, hdmf_number: null, philhealth_number: null, tax_type: (Array|*|null), tax_status: (*|Array|null), base_pay: (*|null), base_pay_unit: (Array|*|null), date_ended: null, hours_per_day: (*|number|null), payroll_group_name: (Array|*|null), sss_basis: (*|Array|null), sss_amount: (*|null), hdmf_basis: (Array|*|null), hdmf_amount: (*|null), philhealth_basis: (*|Array|null), philhealth_amount: (*|null), consultant_tax_rate: (*|null), rdo: *}}
 */
function prepareCloneData( data ) {
    return {
        active: '',
        company_id: '',
        employee_id: '',
        first_name: '',
        last_name: '',
        middle_name: '',
        email: '',
        telephone_number: '',
        mobile_number: '',
        address_line_1: '',
        address_line_2: '',
        country: data.address.country ? data.address.country : '',
        city: '',
        zip_code: '',
        birth_date: null,
        gender: '',
        date_hired: formatDate( data.date_hired, DATE_FORMATS.DISPLAY ),
        department_name: data.department_name ? data.department_name : '',
        rank_name: data.rank_name ? data.rank_name : '',
        cost_center_name: data.payroll.cost_center_name ? data.payroll.cost_center_name : '',
        work_location: data.location_name ? data.location_name : '',
        sss_number: '',
        tin: '',
        hdmf_number: '',
        philhealth_number: '',
        tax_type: data.payroll.tax_type ? data.payroll.tax_type : '',
        tax_status: data.payroll.tax_status ? data.payroll.tax_status : '',
        base_pay: data.payroll.base_pay ? decimal.getPrettyValue( decimal.round( data.payroll.base_pay, 2 ) ) : '',
        base_pay_unit: data.payroll.base_pay_unit ? data.payroll.base_pay_unit : '',
        date_ended: null,
        hours_per_day: data.hours_per_day ? Number( data.hours_per_day ).toString() : '',
        payroll_group_name: data.payroll_group_name ? data.payroll_group_name : '',
        sss_basis: data.payroll.sss_basis ? data.payroll.sss_basis : '',
        sss_amount: data.payroll.sss_amount ? decimal.round( data.payroll.sss_amount, 2 ) : '',
        hdmf_basis: data.payroll.hdmf_basis ? data.payroll.hdmf_basis : '',
        hdmf_amount: data.payroll.hdmf_amount ? decimal.round( data.payroll.hdmf_amount, 2 ) : '',
        philhealth_basis: data.payroll.philhealth_basis ? data.payroll.philhealth_basis : '',
        philhealth_amount: data.payroll.philhealth_amount ? decimal.round( data.payroll.philhealth_amount, 2 ) : '',
        consultant_tax_rate: data.payroll.consultant_tax_rate ? ( Number( data.payroll.consultant_tax_rate ) * 100 ).toString() : '',
        rdo: data.payroll.rdo ? data.payroll.rdo : ''
    };
}

/**
 *
 * Download Reducer
 *
 */
function addEmployee( state = initialState, action ) {
    switch ( action.type ) {
        case SET_COMPANY_ID:
            return state.set( 'company_id', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_CLONE_DATA:
            return state.set( 'clone_data', prepareCloneData( action.payload ) );
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'form_options', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_HAS_USER_MATCH:
            return state.set( 'has_user_match', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addEmployee;
