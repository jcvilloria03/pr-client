import {
    INITIALIZE,
    SUBMIT_FORM,
    NOTIFICATION,
    CHECK_FOR_USER_MATCH,
    SET_HAS_USER_MATCH
} from './constants';

/**
 * display a notification in page
 */
export function InitializeData() {
    return {
        type: INITIALIZE
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * submit a new employee
 */
export function submitEmployee( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}

/**
 * Sends request to check for user record match
 *
 * @param {Object} payload - Employee form data
 * @returns {Object} action
 */
export function checkForUserMatch( payload ) {
    return {
        type: CHECK_FOR_USER_MATCH,
        payload
    };
}

/**
 * Flag if the record submitted has a matching user record
 *
 * @param {Boolean} payload
 * @returns {Object} action
 */
export function setHasUserMatch( payload ) {
    return {
        type: SET_HAS_USER_MATCH,
        payload
    };
}
