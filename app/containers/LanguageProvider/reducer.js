/*
 *
 * LanguageProvider reducer
 *
 */

import { fromJS } from 'immutable';

import {
  CHANGE_LOCALE
} from './constants';
import {
  DEFAULT_LOCALE
} from '../App/constants'; // eslint-disable-line

const initialState = fromJS({
    locale: DEFAULT_LOCALE
});

/**
 * Reducer function for language provider
 *
 * @param      {Map}  state   Current State
 * @param      {Object}  action  The action to dispatch
 * @return     {Map}  new State
 */
function languageProviderReducer( state = initialState, action ) {
    switch ( action.type ) {
        case CHANGE_LOCALE:
            return state
        .set( 'locale', action.locale );
        default:
            return state;
    }
}

export default languageProviderReducer;
