/*
 *
 * LanguageProvider actions
 *
 */

import {
  CHANGE_LOCALE
} from './constants';

/**
 * action for changing language
 * @param  {string} languageLocale locale code for new language
 * @return {object}                payload
 */
export function changeLocale( languageLocale ) {
    return {
        type: CHANGE_LOCALE,
        locale: languageLocale
    };
}
