import styled from 'styled-components';

export const StyledWrapper = styled.div`
    min-height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    color: #444;
    padding: 2rem;

     @media (max-width: 480px) {
        padding: 0;
    }
`;

export const StyledContent = styled.div`
    background-color: #FFF;
    width: 50%;
    min-width: 400px;
    height: 100%;

    @media (max-width: 480px) {
        padding: 15px;
        width: 100%;
        min-width: 0;
    }

    #brand {
        text-align: center;
        margin-bottom: 20px;

        h5 {
            margin: 20px 0 6px 0;
            font-weight: 300;
        }

        .thank-you-icon {
            width: 100%;

            img {
                width: 75%
            }
        }

        .page-title {
            font-weight: bold;
        }

        .page-desc {
            font-weight: bold;
        }
    }

    p {
        text-align: center;
    }
    a {
        color: #00A5E5 !important;
    }

    .sal-icon {
        width: 350px;
        margin-bottom: 16px;

        @media (max-width: 480px) {
            width: 75%;
        }
    }

    .thank-you-icon {
        width: 100%;
    }

    .text-content {
        width: 100%;
        padding: 0px 24px 40px;
        text-align: center;
    }

    .main-text {
        font-size: 18px;
        font-weight: bold;
    }
`;
