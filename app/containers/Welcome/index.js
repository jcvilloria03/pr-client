import React, { PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { auth } from 'utils/AuthService';
import { H3, H2 } from 'components/Typography';
import Button from 'components/Button';
import { browserHistory } from 'utils/BrowserHistory';
import {
    StyledWrapper,
    StyledContent
} from './styles';
import salIcon from '../../assets/sal-icon.png';

/**
 *
 * Welcome
 *
 */
export class Welcome extends React.Component { // eslint-disable-line react/prefer-stateless-function
    /**
     * Component constructor method
     */
    constructor( props ) {
        super( props );
        this.redirect = this.redirect.bind( this );
    }

    componentDidMount() {
        localStorage.clear();
    }

    /**
     * Redirect to login page
     */
    redirect() {
        localStorage.clear();
        browserHistory.push( '/login', true );
    }

    /**
     *
     * Welcome render method
     *
     */
    render() {
        const buttonSyles = {
            borderRadius: '5px',
            padding: '.5em 3em'
        };

        return (
            <div>
                <StyledWrapper>
                    <Helmet
                        title="Welcome"
                        meta={ [
                            { name: 'Welcome Page', content: 'Welcome User' }
                        ] }
                    />
                    <StyledContent>
                        <div id="brand">
                            <img className="sal-icon" src={ salIcon } alt="Salarium" />
                            <H2 className="page-title">Welcome To Salarium</H2>
                            <H3 className="page-desc">Your access to a convenient and efficient time keeping and payroll system starts today.</H3>
                            <div className="thank-you-icon">
                                <img src="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/welcome-admin-user.png" alt="Salarium" />
                            </div>
                        </div>
                        <div className="text-content">
                            <p className="main-text">
                                Thank you for verifying your account. You may now sign in into the application.
                            </p>
                            <Button
                                className="main-text"
                                label="Login"
                                type="primary"
                                style={ buttonSyles }
                                onClick={ this.redirect }
                            />
                        </div>
                    </StyledContent>
                </StyledWrapper>
            </div>
        );
    }
}

Welcome.propTypes = {
    dispatch: PropTypes.func.isRequired
};

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return {
        dispatch
    };
}

export default connect( null, mapDispatchToProps )( Welcome );
