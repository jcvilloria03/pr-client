/*
 *
 * Edit constants
 *
 */

export const nameSpace = 'app/BonusType/Edit';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const GET_BONUSTYPE = `${nameSpace}/GET_BONUSTYPE`;
export const SET_BONUSTYPE = `${nameSpace}/SET_BONUSTYPE`;
export const IS_AVAILABLE = `${nameSpace}/IS_AVAILABLE`;
export const EDIT_BONUSTYPE = `${nameSpace}/EDIT_BONUSTYPE`;
export const SET_EDIT_BONUSTYPE = `${nameSpace}/SET_EDIT_BONUSTYPE`;
export const LOADING = `${nameSpace}/LOADING`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
