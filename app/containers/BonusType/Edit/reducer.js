import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, IS_AVAILABLE, LOADING, NOTIFICATION_SAGA, SET_BONUSTYPE, SET_EDIT_BONUSTYPE
} from './constants';

const initialState = fromJS({
    getBonustype: {},
    available: null,
    loading: false,
    editBonustype: '',
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    }
});

/**
 *
 * Edit reducer
 *
 */
function editReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_BONUSTYPE:
            return state.set( 'getBonustype', fromJS( action.payload ) );
        case IS_AVAILABLE:
            return state.set( 'available', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_EDIT_BONUSTYPE:
            return state.set( 'editBonustype', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default editReducer;
