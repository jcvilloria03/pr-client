import { createSelector } from 'reselect';

/**
 * Direct selector to the edit state domain
 */
const selectEditDomain = () => ( state ) => state.get( 'BonusTypesEdit' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */
const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

/**
 * Default selector used by Edit
 */

const makeSelectEdit = () => createSelector(
  selectEditDomain(),
  ( substate ) => substate.toJS()
);

const makeSelecGetBonusType = () => createSelector(
  selectEditDomain(),
  ( substate ) => substate.get( 'getBonustype' ).toJS()
);

const makeSelectIsAvailable = () => createSelector(
  selectEditDomain(),
  ( substate ) => substate.get( 'available' )
);

const makeSelectLoading = () => createSelector(
  selectEditDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSeleceditBonusType = () => createSelector(
  selectEditDomain(),
  ( substate ) => substate.get( 'editBonustype' )
);

const makeSelectNotification = () => createSelector(
  selectEditDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export default makeSelectEdit;
export {
  selectEditDomain,
  makeSelectProducts,
  makeSelecGetBonusType,
  makeSelectIsAvailable,
  makeSelectLoading,
  makeSeleceditBonusType,
  makeSelectNotification
};
