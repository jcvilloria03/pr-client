/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';

import RadioGroup from 'components/RadioGroup';
import { H3, H2, H1, P } from 'components/Typography';
import SnackBar from 'components/SnackBar';
import Sidebar from 'components/Sidebar';
import Select from 'components/Select';
import Radio from 'components/Radio';
import Button from 'components/Button';
import Input from 'components/Input';
import A from 'components/A';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import { Spinner } from 'components/Spinner';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';

import {
    makeSeleceditBonusType,
    makeSelecGetBonusType,
    makeSelectIsAvailable,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectProducts
} from './selectors';
import * as bonusTypeActions from './actions';

import {
    PageWrapper,
    NavWrapper,
    Footer,
    LoadingStyles
} from './styles';

/**
 *
 * Edit
 *
 */
export class Edit extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
        products: React.PropTypes.array,
        params: React.PropTypes.object,
        getBonusTypes: React.PropTypes.func,
        getBonusType: React.PropTypes.object,
        available: React.PropTypes.bool,
        loading: React.PropTypes.bool,
        editBonusTypes: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    static defaultProps = {
        loading: false
    };

    constructor( props ) {
        super( props );

        this.state = {
            formData: '',
            validateName: '',
            validateMaxNoTaxable: '',
            noTaxable: false
        };
    }

    componentDidMount() {
        this.props.getBonusTypes( this.props.params.id );
    }

    componentWillReceiveProps( nextProps ) {
        this.setState({
            formData: nextProps.getBonusType,
            validateName: nextProps.getBonusType.name,
            validateMaxNoTaxable: 90000
        });
    }

    handleSubmit() {
        this.props.editBonusTypes( this.state.formData );
    }

    handleValidNumber( values ) {
        if ( Math.sign( values ) === 0 ) {
            this.setState({
                noTaxable: true
            });
        } else {
            this.setState({
                noTaxable: false
            });
        }
    }

    /**
     *
     * Edit render method
     *
     */
    render() {
        const amountData = [
            { value: 'FIXED', label: 'Fixed' },
            { value: 'SALARY_BASED', label: 'Salary Based' }
        ];
        const frequencyData = [
            { value: 'ONE_TIME', label: 'One-Time' },
            { value: 'PERIODIC', label: 'Periodic' }
        ];
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });

        const { formData, validateName, validateMaxNoTaxable, nameExists } = this.state;
        const { available, loading, notification } = this.props;

        return (
            <div>
                { loading && <Spinner /> }
                <Helmet
                    title="Edit Bonus Types"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Bonus Types' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Sidebar items={ sidebarLinks } />
                <Modal
                    title="Discard Changes"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.DiscardModal = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => this.DiscardModal.toggle()
                        },
                        {
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: () => browserHistory.push( '/company-settings/payroll/bonus-types', true )
                        }
                    ] }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/company-settings/payroll/bonus-types', true );
                            } }
                        >
                            <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Bonuses</span>
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    {loading
                        ? <div className="loader" style={ { display: loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Edit bonus type.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        : <div className="content">
                            <div className="heading">
                                <H1 noBottomMargin>Edit Bonus Type</H1>
                            </div>
                            <div className="row-header">
                                <div>
                                    <P noBottomMargin>Bonus type name</P>
                                </div>
                                <div>
                                    <P noBottomMargin>Amount Basis</P>
                                </div>
                                <div>
                                    <P noBottomMargin>Frequency</P>
                                </div>
                                <div>
                                    <P noBottomMargin>Fully Taxable</P>
                                </div>
                                <div>
                                    <P noBottomMargin>If no, specify the maximum non taxable</P>
                                </div>
                            </div>
                            <div className="row-details">
                                <div className={ validateName === '' ? ' input-error' : '' }>
                                    <Input
                                        className="comm-detail-input"
                                        id="name"
                                        type="text"
                                        value={ formData.name }
                                        placeholder="Type in bonus type name"
                                        onChange={ ( value ) => this.setState({
                                            formData: { ...formData, name: value },
                                            validateName: value
                                        }) }
                                        ref={ ( ref ) => { this.name = ref; } }
                                    />
                                    {validateName === '' ? <P noBottomMargin className="comm-detail-warn">Please input valid data</P> : ''}
                                    <P noBottomMargin>{nameExists}</P>
                                </div>
                                <div>
                                    <Select
                                        className="comm-detail-input"
                                        id="basis"
                                        value={ formData.basis }
                                        data={ amountData }
                                        disabled={ !available }
                                        onChange={ ( value ) => this.setState({ formData: { ...formData, basis: value.value }}) }
                                    />
                                </div>
                                <div>
                                    <Select
                                        className="comm-detail-input"
                                        id="basis"
                                        value={ formData.frequency }
                                        data={ frequencyData }
                                        disabled={ !available }
                                        onChange={ ( value ) => this.setState({ formData: { ...formData, frequency: value.value }}) }
                                    />
                                </div>
                                <div style={ { marginTop: '-7px' } }>
                                    <RadioGroup
                                        horizontal
                                        value={ `${formData.fully_taxable}` }
                                        onChange={ ( value ) => this.setState({
                                            validateMaxNoTaxable: formData.fully_taxable === true ? 90000 : null,
                                            formData: {
                                                ...formData,
                                                fully_taxable: value === 'true',
                                                max_non_taxable: value === 'false' ? formData.max_non_taxable === null ? 90000.00 : formData.max_non_taxable : null
                                            }}) }
                                    >
                                        <Radio
                                            value="true"
                                            disabled={ !available }
                                        >
                                            Yes
                                        </Radio>
                                        <Radio
                                            value="false"
                                            disabled={ !available }
                                        >
                                            No
                                        </Radio>
                                    </RadioGroup>
                                </div>
                                <div className={ formData.fully_taxable === false && validateMaxNoTaxable === '' ? ' input-error' : formData.fully_taxable === false && this.state.noTaxable === true ? ' input-error' : '' }>
                                    {formData.fully_taxable || !available ?
                                        <Input
                                            id="name"
                                            type="number"
                                            disabled
                                        />
                                            : <Input
                                                className="comm-detail-input"
                                                id="name"
                                                value={ formData.max_non_taxable }
                                                type="number"
                                                onChange={ ( value ) => {
                                                    this.setState({
                                                        formData: { ...formData, max_non_taxable: parseFloat( value ) },
                                                        validateMaxNoTaxable: value
                                                    });
                                                    this.handleValidNumber( value );
                                                } }
                                            />
                                        }
                                    {formData.fully_taxable === false && validateMaxNoTaxable === '' ? <P noBottomMargin className="comm-detail-warn">Field is required</P> : formData.fully_taxable === false && this.state.noTaxable === true ? <p className="comm-detail-warn">This field minimal value is 1</p> : ''}
                                </div>
                            </div>
                        </div> }
                </PageWrapper>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.DiscardModal.toggle() }
                            />
                            <Button
                                label="Update"
                                type="action"
                                size="large"
                                disabled={ loading }
                                onClick={ () => this.handleSubmit() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    products: makeSelectProducts(),
    getBonusType: makeSelecGetBonusType(),
    available: makeSelectIsAvailable(),
    loading: makeSelectLoading(),
    editBonusType: makeSeleceditBonusType(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        bonusTypeActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Edit );
