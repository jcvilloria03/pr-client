import {
    DEFAULT_ACTION, EDIT_BONUSTYPE, GET_BONUSTYPE, NOTIFICATION
} from './constants';

/**
 *
 * Edit actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
/**
 *
 * Get bonus type action
 *
 */
export function getBonusTypes( payload ) {
    return {
        type: GET_BONUSTYPE,
        payload
    };
}

/**
 *
 * Edit bonus type action
 *
 */
export function editBonusTypes( payload ) {
    return {
        type: EDIT_BONUSTYPE,
        payload
    };
}

/**
 *
 * Notification action
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

