import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';
import { EDIT_BONUSTYPE, GET_BONUSTYPE, IS_AVAILABLE, LOADING, NOTIFICATION, NOTIFICATION_SAGA, SET_BONUSTYPE, SET_EDIT_BONUSTYPE } from './constants';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';
import { browserHistory } from '../../../utils/BrowserHistory';

import { REINITIALIZE_PAGE } from '../../App/constants';

/**
 * GET bonus type
 */
export function* getBonusType({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const res = yield call( Fetch, `/other_income_type/${payload}`, { method: 'GET' });

        yield put({
            type: SET_BONUSTYPE,
            payload: res
        });

        const available = yield call( Fetch, '/other_income_type/is_edit_available', {
            method: 'POST',
            data: { other_income_type_id: payload }
        });

        yield put({
            type: IS_AVAILABLE,
            payload: available.available
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize Page
 */
export function* reinitializePage() {
    yield call( getBonusType );
}

/**
 * EDIT bonus type
 */
export function* editBonusType({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        yield put({
            type: SET_EDIT_BONUSTYPE,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/philippine/company/${companyId}/bonus_type/is_name_available`, {
            method: 'POST',
            data: { name: payload.name, other_income_type_id: payload.id }
        });

        if ( response.available === true ) {
            yield call( Fetch, `/philippine/bonus_type/${payload.id}`, { method: 'PATCH', data: payload });

            yield call( notifyUser, {
                title: 'Success',
                message: 'Bonus Type Updated succesfully..',
                show: true,
                type: 'success'
            });

            yield call( browserHistory.push( '/company-settings/payroll/bonus-types', true ) );
        } else {
            yield call( notifyUser, {
                title: 'Error',
                message: `Bonus type name " ${payload.name} " is already exists ..`,
                show: true,
                type: 'error'
            });
        }
        yield put({
            type: LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EDIT_BONUSTYPE,
            payload: false
        });
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}
/**
 * Watcher for get bonus type
 */
export function* watchForGetLocation() {
    const watcher = yield takeEvery( GET_BONUSTYPE, getBonusType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For ReinitializePage
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for edit bonus type
 */
export function* watchForEditBonusType() {
    const watcher = yield takeEvery( EDIT_BONUSTYPE, editBonusType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Notification
 */
export function* watchFroNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetLocation,
    watchForEditBonusType,
    watchForReinitializePage,
    watchFroNotification
];
