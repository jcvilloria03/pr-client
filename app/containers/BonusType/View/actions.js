import {
    DEFAULT_ACTION, DELETE_BONUS_TYPE, GET_BONUSTYPE, NOTIFICATION
} from './constants';

/**
 *
 * Get BonusType actions
 *
 */
export function getbonustype() {
    return {
        type: GET_BONUSTYPE
    };
}
/**
 *
 *Delete BonusType actions
 *
 */
export function deleteBonusType( IDs ) {
    return {
        type: DELETE_BONUS_TYPE,
        payload: IDs
    };
}
/**
 *
 * BonusType actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
/**
 * Display a notification to a user
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
