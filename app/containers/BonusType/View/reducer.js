import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, SET_BONUSTYPE, LOADING, DELETED_BONUS_TYPE, NOTIFICATION_SAGA
} from './constants';

const initialState = fromJS({
    bonustype: [],
    loading: true,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * BonusType reducer
 *
 */
function bonusTypeReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_BONUSTYPE:
            return state.set( 'bonustype', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        case DELETED_BONUS_TYPE:
            return state.set( 'deleteData', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default bonusTypeReducer;
