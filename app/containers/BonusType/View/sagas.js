 import { take, call, put, cancel } from 'redux-saga/effects';
 import { takeEvery, delay, takeLatest } from 'redux-saga';
 import { LOCATION_CHANGE } from 'react-router-redux';
 import { GET_BONUSTYPE, SET_BONUSTYPE, LOADING, DELETE_BONUS_TYPE, DELETED_BONUS_TYPE, NOTIFICATION_SAGA,
    NOTIFICATION } from './constants';
 import { REINITIALIZE_PAGE } from '../../App/constants';
 import { Fetch } from '../../../utils/request';
 import { company } from '../../../utils/CompanyService';
/**
 * Individual exports for testing
 */
 export function* defaultSaga() {
    // Refer to samples
 }
/**
 * Get Bonus Type
 */
 export function* getBonusType() {
     try {
         yield put({
             type: LOADING,
             payload: true
         });
         const companyId = company.getLastActiveCompanyId();
         const res = yield call( Fetch, `/company/${companyId}/other_income_types/bonus_type?exclude_trashed=1`, { method: 'GET' });
         yield put({
             type: SET_BONUSTYPE,
             payload: res.data || []
         });
     } catch ( error ) {
         yield call( notifyUser, {
             show: true,
             title: error.response ? error.response.statusText : 'Error',
             message: error.response ? error.response.data.message : error.statusText,
             type: 'error'
         });
     } finally {
         yield put({
             type: LOADING,
             payload: false
         });
     }
 }
 /**
 * Delete Bonus Type
 */
 export function* Deletebounstypes({ payload }) {
     try {
         yield put({
             type: DELETED_BONUS_TYPE,
             payload: true
         });
         const companyId = company.getLastActiveCompanyId();
         const response = yield call( Fetch, `/company/${companyId}/other_income_type/is_delete_available`, { method: 'POST', data: payload });
         const formData = new FormData();
         if ( response.available === true ) {
             formData.append( '_method', 'DELETE' );
             formData.append( 'company_id', companyId );
             for ( const i of payload.ids ) {
                 formData.append( 'ids[]', i );
             }
             yield call( Fetch, `/company/${companyId}/other_income_type`, { method: 'POST', data: formData });
         }
         yield call( notifyUser, {
             title: 'Success',
             message: 'Record successfully deleted',
             show: true,
             type: 'success'
         });
     } catch ( error ) {
         yield call( notifyUser, {
             show: true,
             title: error.response ? error.response.statusText : 'Error',
             message: error.response ? error.response.data.message : error.statusText,
             type: 'error'
         });
     } finally {
         yield put({
             type: DELETED_BONUS_TYPE,
             payload: false
         });
     }
 }
 /**
 * notifyUser
 */
 export function* notifyUser( payload ) {
     yield put({
         type: NOTIFICATION_SAGA,
         payload: {
             title: ' ',
             message: ' ',
             show: false,
             type: payload.type
         }
     });

     yield put({
         type: NOTIFICATION_SAGA,
         payload
     });

     yield call( delay, 5000 );
     yield put({
         type: NOTIFICATION_SAGA,
         payload: {
             title: ' ',
             message: ' ',
             show: false,
             type: payload.type
         }
     });
 }
 /**
 * reinitialize
 */
 export function* reinitializePage() {
     yield call( getBonusType );
 }
/**
 * watchForReinitializePage
 */
 export function* watchForReinitializePage() {
     const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

     yield take( LOCATION_CHANGE );
     yield cancel( watcher );
 }
/**
 * watchForGetbonusTypes
 */
 export function* watchForgetBonusType() {
     const watcher = yield takeEvery( GET_BONUSTYPE, getBonusType );
     yield take( LOCATION_CHANGE );
     yield cancel( watcher );
 }
 /**
 * watchFordeltebonusTypes
 */
 export function* watchFordeltebonusTypes() {
     const watcher = yield takeEvery( DELETE_BONUS_TYPE, Deletebounstypes );
     yield take( LOCATION_CHANGE );
     yield cancel( watcher );
 }
 /**
 * watchForNotifyUser
 */
 export function* watchForNotifyUser() {
     const watcher = yield takeEvery( NOTIFICATION, notifyUser );

     yield take( LOCATION_CHANGE );
     yield cancel( watcher );
 }
// All sagas to be loaded
 export default [
     defaultSaga,
     watchForgetBonusType,
     watchForReinitializePage,
     watchFordeltebonusTypes,
     watchForNotifyUser
 ];
