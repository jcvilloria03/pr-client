/* eslint-disable react/sort-comp */
/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { isAnyRowSelected, getIdsOfSelectedRows } from 'components/Table/helpers';
import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';
import { H5, H3, H2, H1, P } from 'components/Typography';
import SalDropdown from 'components/SalDropdown';
import SnackBar from 'components/SnackBar';
import Button from 'components/Button';
import Sidebar from 'components/Sidebar';
import Table from 'components/Table';
import Modal from 'components/Modal';
import Icon from 'components/Icon';
import Input from 'components/Input';

import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';

import {
    makeSelectListBonusType,
    makeSelectProducts,
    makeSelectLoading,
    makeSelectNotification
} from './selectors';

import * as bonusTypeAction from './actions';

import {
    PageWrapper,
   LoadingStyles
} from './styles';
/**
 *
 * BonusType
 *
 */
export class BonusType extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static PropsTyes = {
        products: React.PropTypes.array,
        ListBounsType: React.PropTypes.array,
        getbonustype: React.PropTypes.func,
        deleteBonusType: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    static defaultProps = {
        loading: true
    };
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            },
            tableData: [],
            delete_label: '',
            click: false,
            editData: null,
            check: false
        };

        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
    }
    componentDidMount() {
        this.props.getbonustype();
    }
    /**
     *
     * BonusType render method
     *
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.ListBounsType !== this.props.ListBounsType
            && this.setState({
                tableData: nextProps.ListBounsType,
                pagination: {
                    total: nextProps.ListBounsType.length,
                    per_page: 10,
                    current_page: 1,
                    last_page: Math.ceil( nextProps.ListBounsType.length / 10 ),
                    to: 0
                }
            }, () => { this.handleTableChanges(); });
    }

    getDropdownItems() {
        return [
            {
                children: <div>Delete</div>,
                onClick: () => this.DeleteModel.toggle()
            }
        ];
    }

    handleTableChanges = ( tableProps = this.bonustypeTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.state.tableData.length });
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    handleSearch = ( term = '' ) => {
        let displayData = this.props.ListBounsType;

        if ( term ) {
            const regex = new RegExp( term, 'i' );
            displayData = displayData.filter( ( row ) => ( regex.test( row.name ) ) );
        }

        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: 10,
                current_page: 1
            },
            tableData: displayData
        }), () => {
            this.handleTableChanges();
        });
    }

    hanldeDelete = () => {
        const payload = getIdsOfSelectedRows( this.bonustypeTable );
        this.props.deleteBonusType({ ids: payload, company_id: company.getLastActiveCompanyId() });
        this.DeleteModel.toggle();
        setTimeout( async () => {
            await this.props.getbonustype();
        }, 3000 );
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.CommissionTypeTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.CommissionTypeTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    render() {
        const { loading } = this.props;
        const hasSelectedItems = isAnyRowSelected( this.bonustypeTable );
        const { tableData } = this.state;
        const tableColumns = [
            {
                id: 'name',
                header: 'Bonus Types ',
                minWidth: 200,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.name === '13TH_MONTH_PAY' ? '13th Month Pay' : row.name}
                    </div>
                )
            },
            {
                id: 'basis',
                header: 'Amount Basis  ',
                minWidth: 120,
                sortable: true,
                render: ({ row }) => (
                    <div>{row.basis === 'SALARY_BASED' ? 'Salary Based' : 'Fixed' }</div>
                       )
            },
            {
                id: 'frequency',
                header: 'Frequency',
                minWidth: 100,
                sortable: true,
                render: ({ row }) => (
                    <div>{row.frequency === 'ONE_TIME' ? 'One-Time' : 'Periodic'}</div>
                       )
            },
            {
                id: 'fully_taxable',
                header: 'Fully Taxable',
                minWidth: 120,
                sortable: true,
                render: ({ row }) => (
                    <div>{row.fully_taxable === false ? 'No' : 'Yes'}</div>
                       )
            },
            {
                id: 'max_non_taxable',
                header: 'Maximum Non Taxable',
                minWidth: 180,
                sortable: true,
                render: ({ row }) => (
                    <div>{row.max_non_taxable === null ? 'N/A' : row.max_non_taxable}</div>
                       )
            },
            {
                id: 'id',
                header: ' ',
                accessor: 'actions',
                minWidth: 80,
                sortable: false,
                style: { justifyContent: 'end' },
                render: ({ row }) => (
                    row.name === '13TH_MONTH_PAY' ? '' : <Button
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => browserHistory.push( `/company-settings/payroll/bonus-types/${row.id}/edit`, true ) }
                    />
                )
            }
        ];
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        return (
            <div style={ { display: 'flex' } }>
                <Helmet
                    title="Bonus Types"
                    meta={ [
                        { name: 'description', content: 'Description of Bonus Types' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="Confirm Your Action"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.DeleteModel = ref;
                    } }
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.DeleteModel.toggle()
                        },
                        {
                            type: 'darkRed',
                            label: 'Yes',
                            onClick: this.hanldeDelete
                        }
                    ] }
                />
                <PageWrapper>
                    <div className="loader" style={ { display: loading ? '' : 'none' } }>
                        <LoadingStyles>
                            <H2>Loading Bonus Types.</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    </div>
                    <div className="content" style={ { display: loading ? 'none' : '' } }>
                        <div className="heading">
                            <H1 noBottomMargin>Bonus Types</H1>

                            <div className="buttons-wrapper">
                                <Button
                                    label="Add Bonuses"
                                    type="neutral"
                                    onClick={ () => browserHistory.push( '/payroll/bonuses/add', true ) }
                                />
                                <Button
                                    label="Add Bonus Types"
                                    type="action"
                                    onClick={ () => browserHistory.push( '/company-settings/payroll/bonus-types/add', true ) }
                                />
                            </div>
                        </div>
                        <div className="title">
                            <div className="search-wrapper">
                                <H5 noBottomMargin>Bonus Type List</H5>

                                <Input
                                    id="search"
                                    className="search"
                                    onChange={ ( value ) => this.handleSearch( value.toLowerCase() ) }
                                    addon={ {
                                        content: <Icon name="search" />,
                                        placement: 'right'
                                    } }
                                    ref={ ( ref ) => { this.searchInput = ref; } }
                                />
                            </div>
                            <div className="actions-wrapper">
                                <P noBottomMargin>{hasSelectedItems ? this.state.delete_label : this.state.label}</P>

                                {hasSelectedItems && (
                                    <span style={ { marginLeft: '14px' } }>
                                        <SalDropdown dropdownItems={ this.getDropdownItems() } />
                                    </span>
                                )}
                            </div>
                        </div>
                        <div>
                            <Table
                                data={ tableData }
                                columns={ tableColumns }
                                onDataChange={ this.handleTableChanges }
                                loading={ this.props.loading }
                                ref={ ( ref ) => { this.bonustypeTable = ref; } }
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    this.setState({ delete_label: formatDeleteLabel( selectionLength ) });
                                } }
                                page={ this.state.pagination.current_page - 1 }
                                pageSize={ this.state.pagination.per_page }
                                pages={ this.state.pagination.total }
                                selectable
                                external
                            />

                            <div>
                                <FooterTablePaginationV2
                                    page={ this.state.pagination.current_page }
                                    pageSize={ this.state.pagination.per_page }
                                    pagination={ this.state.pagination }
                                    onPageChange={ this.onPageChange }
                                    onPageSizeChange={ this.onPageSizeChange }
                                    paginationLabel={ this.state.label }
                                    fluid
                                />
                            </div>
                        </div>
                    </div>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    ListBounsType: makeSelectListBonusType(),
    products: makeSelectProducts(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        bonusTypeAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( BonusType );
