import { createSelector } from 'reselect';

/**
 * Direct selector to the bonusType state domain
 */
const selectBonusTypeDomain = () => ( state ) => state.get( 'bonusType' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );
/**
 * Other specific selectors
 */

/**
 * Default selector used by BonusType
 */

const makeSelectListBonusType = () => createSelector(
  selectBonusTypeDomain(),
  ( substate ) => substate.get( 'bonustype' ).toJS()
);
const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);
const makeSelectNotification = () => createSelector(
  selectBonusTypeDomain(),
( substate ) => substate.get( 'notification' ).toJS()
);
const makeSelectLoading = () => createSelector(
  selectBonusTypeDomain(),
  ( substate ) => substate.get( 'loading' )
);

export {
  selectBonusTypeDomain,
  makeSelectListBonusType,
  makeSelectProducts,
  makeSelectLoading,
  makeSelectNotification
};
