/*
 *
 * BonusType constants
 *
 */

export const DEFAULT_ACTION = 'app/BonusType/DEFAULT_ACTION';
export const GET_BONUSTYPE = 'app/BonusType/GET_BONUSTYPE';
export const SET_BONUSTYPE = 'app/BonusType/SET_BONUSTYPE';
export const LOADING = 'app/BonusType/LOADING';
export const DELETE_BONUS_TYPE = 'app/BonusType/DELETE_BONUS_TYPE';
export const DELETED_BONUS_TYPE = 'app/BonusType/DELETED_BONUS_TYPE';
export const NOTIFICATION = 'app/BonusType/NOTIFICATION ';
export const NOTIFICATION_SAGA = 'app/BonusType/NOTIFICATION_SAGA ';

