import {
    ADD_BONUSTYPE,
    ADD_BONUS_TYPE_LIST,
    NOTIFICATION,
    DEFAULT_ACTION
} from './constants';

/**
 *
 * Add actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
/**
 *
 * Add actions
 *
 */
export function getAddBonusType( payload ) {
    return {
        type: ADD_BONUSTYPE,
        payload
    };
}
/**
 *
 * Add addbonusTypeList actions
 *
 */
export function addbonusTypeList( payload ) {
    return {
        type: ADD_BONUS_TYPE_LIST,
        payload
    };
}
/**
 *
 * Notification action
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
