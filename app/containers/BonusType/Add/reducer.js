import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
   SET_ADD_BONUSTYPE,
   SET_ADD_BONUS_TYPE_LIST,
   NOTIFICATION_SAGA,
   LOADING
} from './constants';

const initialState = fromJS({
    addbonustype: null,
    addbonustypeList: '',
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    }
});

/**
 *
 * Add reducer
 *
 */
function addReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_ADD_BONUSTYPE:
            return state.set( 'addbonustype', action.payload );
        case SET_ADD_BONUS_TYPE_LIST:
            return state.set( 'addbonustypeList', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        default:
            return state;
    }
}

export default addReducer;
