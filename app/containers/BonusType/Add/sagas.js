import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';

import {
    ADD_BONUSTYPE, ADD_BONUS_TYPE_LIST, SET_ADD_BONUSTYPE, SET_ADD_BONUS_TYPE_LIST, NOTIFICATION, NOTIFICATION_SAGA, LOADING
} from './constants';
import { company } from '../../../utils/CompanyService';
import { browserHistory } from '../../../utils/BrowserHistory';
import { Fetch } from '../../../utils/request';
/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}
/**
 * Add bonus type
 */
export function* Addbonustypesaga({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/philippine/company/${companyId}/bonus_type/is_name_available`, { method: 'POST', data: { name: payload.name }
        });

        yield put({
            type: SET_ADD_BONUSTYPE,
            payload: response.available
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error || 'Something went wrong',
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}
/**
 * Add bonus type List
 */
export function* AddbonustypeListsaga({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        yield put({
            type: SET_ADD_BONUS_TYPE_LIST,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        yield call( Fetch, `/philippine/company/${companyId}/bonus_type/bulk_create`, { method: 'POST', data: payload });
        yield call( notifyUser, {
            title: 'Success',
            message: 'New record saved',
            show: true,
            type: 'success'
        });
        yield call( browserHistory.goBack );
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error || 'Something went wrong',
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
        yield put({
            type: SET_ADD_BONUS_TYPE_LIST,
            payload: false
        });
    }
}
/**
 * notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * watch for add bonustype
 */
export function* watchForAddbonustype() {
    const watcher = yield takeEvery( ADD_BONUSTYPE, Addbonustypesaga );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * watch for add bonustype List
 */
export function* watchForAddbonustypeList() {
    const watcher = yield takeEvery( ADD_BONUS_TYPE_LIST, AddbonustypeListsaga );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for Notification
 */
export function* watchFroNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
// All sagas to be loaded
export default [
    defaultSaga,
    watchForAddbonustype,
    watchForAddbonustypeList,
    watchFroNotification
];
