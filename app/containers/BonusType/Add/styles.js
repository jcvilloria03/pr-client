import styled from 'styled-components';
import { Container } from 'reactstrap';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;

    .icon-arrow {
        width: 14px;
        font-size: 14px;
        display: inline-block;
        margin-right: 4px;

        > i {
            align-self: center;
        }
    }

    .back-text {
        font-size: 14px;
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;
        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;

export const PageWrapper = styled( Container )`
    height: 100%;
    margin-bottom: 20px;
    padding-bottom: 100px;

    .content {
        padding-top: 130px;
        padding-bottom: 90px;

        .input-error {
            input {
                border-color:#eb7575 !important;
            }

            label {
                color:#eb7575;
            }

            span {
                color:#eb7575;
            }

            p {
                color:#eb7575;
            }

            .Select {
                .Select-control {
                    border-color: #eb7e87 !important;
                }
            }
        }

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 20px auto 50px auto;

            h1 {
                font-weight: 700;
                font-size: 36px;
                line-height: 57.6px;
            }

            p {
                text-align: center;
                line-height: 22.4px;
            }
        }

        .row-header,
        .row-details {
            display: grid;
            grid-template-columns: 100px 1fr 1fr 1fr 1fr 1fr 200px;
            column-gap: 12px;
        }

        .row-header {
            p {
                font-weight: 700;
                line-height: 22.4px;
            }
        }

        .row-details {
            margin-top: 16px;

            button {
                span {
                    display: flex;
                    justify-content: center;

                    .icon {
                        font-size: 14px;
                        color: black;
                        display: inline-flex;
                        width: 14px;
                        margin-right: 5px;

                        > i {
                            align-self: center;
                        }
                    }
                }
            }

            .button-add-type {
                width: 100%;
            }
        }
    }
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;
