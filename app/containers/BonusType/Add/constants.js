/*
 *
 * Add constants
 *
 */

export const DEFAULT_ACTION = 'app/Add/DEFAULT_ACTION';
export const ADD_BONUSTYPE = 'app/Add/ADD_BONUS_TYPE';
export const SET_ADD_BONUSTYPE = 'app/Add/SET_ADD_BONUSTYPE';
export const ADD_BONUS_TYPE_LIST = 'app/Add/ADD_BONUS_TYPE_LIST';
export const SET_ADD_BONUS_TYPE_LIST = 'app/Add/SET_ADD_BONUS_TYPE_LIST';
export const NOTIFICATION = 'app/Add/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/Add/NOTIFICATION_SAGA';
export const LOADING = 'app/Add/LOADING';
