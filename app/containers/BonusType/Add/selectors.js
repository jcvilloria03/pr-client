import { createSelector } from 'reselect';

/**
 * Direct selector to the add state domain
 */
const selectAddDomain = () => ( state ) => state.get( 'BonusTypesAdd' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );
/**
 * Other specific selectors
 */

/**
 * Default selector used by Add
 */

const makeSelectAdd = () => createSelector(
  selectAddDomain(),
  ( substate ) => substate.toJS()
);
const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

const makeSelectSetAddbonustype = () => createSelector(
  selectAddDomain(),
  ( substate ) => substate.get( 'addbonustype' )
);
const makeSelectSetListbonustype = () => createSelector(
  selectAddDomain(),
  ( substate ) => substate.get( 'addbonustypeList' )
);
const makeSelectNotification = () => createSelector(
  selectAddDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);
const makeSelectLoading = () => createSelector(
  selectAddDomain(),
  ( substate ) => substate.get( 'loading' )
);
export default makeSelectAdd;
export {
  selectAddDomain,
  makeSelectProducts,
  makeSelectSetAddbonustype,
  makeSelectSetListbonustype,
  makeSelectNotification,
  makeSelectLoading
};
