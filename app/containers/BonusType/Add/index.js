/* eslint-disable react/prop-types */
import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import RadioGroup from 'components/RadioGroup';
import SnackBar from 'components/SnackBar';
import { H1, P } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Select from 'components/Select';
import Button from 'components/Button';
import Radio from 'components/Radio';
import Input from 'components/Input';
import Icon from 'components/Icon';
import A from 'components/A';
import Modal from 'components/Modal';
import { Spinner } from 'components/Spinner';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';

import {
    makeSelectProducts,
    makeSelectSetAddbonustype,
    makeSelectSetListbonustype,
    makeSelectNotification,
    makeSelectLoading
} from './selectors';
import * as bonusAction from './actions';

import {
    PageWrapper,
    Footer,
    NavWrapper
} from './styles';

/**
 *
 * Add
 *
 */
export class Add extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static PropsTyes = {
        products: React.PropTypes.array,
        BonusTypeAdd: React.PropTypes.bool,
        getAddBonusType: React.PropTypes.func,
        addbonusTypeList: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    static defaultProps = {
        loading: false
    };
    /**
     *
     * Add render method
     *
     */
    constructor( props ) {
        super( props );
        const companyId = company.getLastActiveCompanyId();
        this.state = {
            formData: {
                id: '',
                name: '',
                basis: '',
                frequency: '',
                fully_taxable: true,
                max_non_taxable: '',
                company_id: companyId,
                unavailableName: false
            },
            bonusDataList: [],
            listId: '',
            showModel: false,
            showbutton: false,
            editData: '',
            validmax_no_taxable: 1,
            nameValid: false,
            maxNonValid: 'taxable',
            noTaxable: false,
            noTaxableEdit: false
        };
    }
    getUniqueId = () => `${Math.floor( Math.random() * 1000000 )}`;

    addBonusType() {
        const companyId = company.getLastActiveCompanyId();
        if ( this.validateFields() ) {
            if ( this.state.formData.name !== '' ) {
                this.props.getAddBonusType( this.state.formData );
                setTimeout( () => {
                    if ( this.state.formData.name !== '' && this.validatedNameExist() ) {
                        this.setState({
                            ValidName: 'name',
                            ExistName: 'name',
                            ValidBasis: 'basis',
                            ValidFrequency: 'frequency',
                            ValidMaxNonTaxable: 'taxable',
                            formData: {
                                id: '',
                                name: '',
                                basis: '',
                                frequency: '',
                                fully_taxable: true,
                                max_non_taxable: '',
                                company_id: companyId,
                                unavailableName: false
                            },
                            bonusDataList: [
                                ...this.state.bonusDataList,
                                { ...this.state.formData,
                                    id: this.getUniqueId() }
                            ]
                        });
                    }
                }, 2000 );
            }
        }
    }
    addBonusList() {
        this.props.addbonusTypeList( this.state.bonusDataList );
    }
    deleteList = () => {
        this.setState({
            bonusDataList: this.state.bonusDataList.filter( ( arr ) => arr.id !== this.state.listId ),
            showModel: false
        });
    }
    showTextBox( id ) {
        return this.state.editData && this.state.editData.id === id;
    }
    handleEdit = () => {
        this.state.bonusDataList && this.state.bonusDataList.map( ( val, i ) => {
            if ( val.id === this.state.editData.id ) {
                const value = this.state.bonusDataList;
                value[ i ] = this.state.editData;
                this.setState({
                    bonusDataList: value,
                    showbutton: false
                });
                this.setState({
                    editData: ''
                });
            }
            return !val;
        });
    }

    validatedNameExist = () => {
        let valid = true;
        if ( this.props.BonusTypeAdd === false ) {
            const message = 'Record name already exists';
            this.name.setState({ error: true, errorMessage: message });
            valid = false;
        }
        return valid;
    }

    validateFields = () => {
        let valid = true;

        if ( this.state.bonusDataList.length > 0 ) {
            this.state.bonusDataList.map( ( val ) => {
                if ( val.name === this.state.formData.name ) {
                    const message = 'Record name already added';
                    this.name.setState({ error: true, errorMessage: message });
                    valid = false;
                }
                return !val;
            });
        }

        if ( this.state.formData.name === '' ) {
            this.setState({
                ValidName: ''
            });
            valid = false;
        }
        if ( this.state.formData.basis === '' ) {
            this.setState({
                ValidBasis: ''
            });
            valid = false;
        }
        if ( this.state.formData.frequency === '' ) {
            this.setState({
                ValidFrequency: ''
            });
            valid = false;
        }
        if ( this.state.formData.fully_taxable === false && this.state.formData.max_non_taxable === '' ) {
            this.setState({
                ValidMaxNonTaxable: ''
            });
            valid = false;
        }
        return valid;
    }

    handleValidNumber( values ) {
        if ( Math.sign( values ) === 0 ) {
            this.setState({
                noTaxable: true
            });
        } else {
            this.setState({
                noTaxable: false
            });
        }
    }

    handleValidEditNumber( values ) {
        if ( Math.sign( values ) === 0 ) {
            this.setState({
                noTaxableEdit: true
            });
        } else {
            this.setState({
                noTaxableEdit: false
            });
        }
    }

    render() {
        const { notification, loading } = this.props;
        const data = [
            { value: 'FIXED', label: 'Fixed' },
            { value: 'SALARY_BASED', label: 'Salary Based' }
        ];
        const data1 = [
            { value: 'ONE_TIME', label: 'One-Time' },
            { value: 'PERIODIC', label: 'Periodic' }
        ];
        const { formData, editData } = this.state;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        return (
            <div>
                <div>
                    { loading && <Spinner /> }
                    <Helmet
                        title="Add Bonus Types"
                        meta={ [
                        { name: 'description', content: 'Description of Add Bonus Types' }
                        ] }
                    />
                    <Sidebar items={ sidebarLinks } />
                    <SnackBar
                        message={ notification.message }
                        title={ notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ notification.show }
                        delay={ 5000 }
                        type={ notification.type }
                    />
                    <NavWrapper>
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/payroll/bonus-types', true );
                                } }
                            >
                                <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Bonuses</span>
                            </A>
                        </Container>
                    </NavWrapper>
                    <Modal
                        title="Confirm Your Action"
                        className="modal-md modal-commission-type"
                        ref={ ( ref ) => {
                            this.DeleteModel = ref;
                        } }
                        center
                        showClose={ false }
                        body={ (
                            <div>
                                <p>Do you wish to remove this record?</p>
                            </div>
                        ) }
                        buttons={ [
                            {
                                type: 'grey',
                                label: 'No',
                                onClick: () => this.DeleteModel.toggle()
                            },
                            {
                                type: 'action',
                                label: 'Yes',
                                onClick: this.deleteList
                            }
                        ] }
                    />
                    <Modal
                        title="Discard Changes"
                        className="modal-md modal-commission-type"
                        ref={ ( ref ) => {
                            this.DiscardModal = ref;
                        } }
                        center
                        showClose={ false }
                        body={ (
                            <div>
                                <p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p>
                            </div>
                        ) }
                        buttons={ [
                            {
                                type: 'grey',
                                label: 'Stay on this page',
                                onClick: () => this.DiscardModal.toggle()
                            },
                            {
                                type: 'darkRed',
                                label: 'Discard',
                                onClick: () => browserHistory.push( '/company-settings/payroll/bonus-types', true )
                            }
                        ] }
                    />
                    <PageWrapper>
                        <div className="content">
                            <div className="heading">
                                <H1 noBottomMargin>Add Bonus Types</H1>
                            </div>
                            <div className="row-header">
                                <div></div>
                                <div>
                                    <P noBottomMargin>Bonus type name</P>
                                </div>
                                <div>
                                    <P noBottomMargin>Amount Basis</P>
                                </div>
                                <div>
                                    <P noBottomMargin>Frequency</P>
                                </div>
                                <div>
                                    <P noBottomMargin>Fully Taxable</P>
                                </div>
                                <div>
                                    <P noBottomMargin>If no, specify the maximum non taxable</P>
                                </div>
                                <div></div>
                            </div>
                            {this.state.bonusDataList && this.state.bonusDataList.map( ( value, index ) =>
                                <div className="row-details" key={ value.id }>
                                    <div>
                                        <P noBottomMargin>Bonus type { index + 1 }</P>
                                    </div>
                                    <div className={ value.id === this.state.editData.id && this.state.nameValid ? ' input-error' : '' }>
                                        {
                                            this.showTextBox( value.id ) && this.state.showbutton
                                            ? <Input
                                                id="name"
                                                type="text"
                                                placeholder="Enter name"
                                                value={ this.state.editData.name }
                                                ref={ ( ref ) => { this.name = ref; } }
                                                onChange={ ( values ) => this.setState({ nameValid: this.name.state.value === '', editData: { ...editData, name: values }}) }
                                            />
                                            : <P noBottomMargin>{value.name}</P>
                                        }
                                        {value.id === this.state.editData.id && this.state.nameValid
                                            ? <P noBottomMargin>Please input valid data</P>
                                            : ''
                                        }
                                    </div>
                                    <div>
                                        {
                                            this.showTextBox( value.id ) && this.state.showbutton
                                            ? <Select
                                                id="basis"
                                                data={ data }
                                                value={ this.state.editData.basis }
                                                ref={ ( ref ) => { this.basis = ref; } }
                                                onChange={ ( values ) => this.setState({ editData: { ...editData, basis: values.value }}) }
                                            />
                                            : <P noBottomMargin>{ value.basis === 'FIXED' ? 'Fixed' : 'Salary Based'}</P>
                                        }
                                    </div>
                                    <div>
                                        {
                                            this.showTextBox( value.id ) && this.state.showbutton
                                            ? <Select
                                                id="frequency"
                                                data={ data1 }
                                                value={ this.state.editData.frequency }
                                                ref={ ( ref ) => { this.frequency = ref; } }
                                                onChange={ ( values ) => this.setState({ editData: { ...editData, frequency: values.value }}) }
                                            />
                                            : <P noBottomMargin>{ value.frequency === 'ONE_TIME' ? 'One-time' : 'Periodic'}</P>
                                        }
                                    </div>
                                    <div>
                                        {
                                            this.showTextBox( value.id ) && this.state.showbutton
                                            ? <RadioGroup
                                                horizontal
                                                value={ `${this.state.editData.fully_taxable}` }
                                                onChange={ ( values ) => this.setState({ maxNonValid: 1,
                                                    editData: { ...editData,
                                                        fully_taxable: values === 'true',
                                                        max_non_taxable: values === 'false' ? editData.max_non_taxable === '' ? 90000.00 : editData.max_non_taxable : ''
                                                    }}) }
                                            >
                                                <Radio
                                                    value="true"
                                                    checked={ `${this.state.editData.fully_taxable}` === 'true' }
                                                >Yes</Radio>
                                                <Radio
                                                    value="false"
                                                    checked={ `${this.state.editData.fully_taxable}` === 'false' }
                                                >No</Radio>
                                            </RadioGroup>
                                            : <P noBottomMargin>{ value.fully_taxable === true ? 'Yes' : 'No' }</P>
                                        }
                                    </div>
                                    <div
                                        className={ editData.fully_taxable === false
                                            && value.id === this.state.editData.id
                                            && this.state.maxNonValid === ''
                                                ? ' input-error'
                                                : editData.fully_taxable === false
                                                    && value.id === this.state.editData.id
                                                    && this.state.noTaxableEdit === true
                                                        ? ' input-error'
                                                        : ''
                                        }
                                    >
                                        {
                                            this.showTextBox( value.id ) && this.state.showbutton
                                            ?
                                                <Input
                                                    id="max_non_taxable"
                                                    type="number"
                                                    value={ this.state.editData.max_non_taxable }
                                                    onChange={ ( values ) => {
                                                        this.setState({ maxNonValid: this.max_non_taxable.state.value, editData: { ...editData, max_non_taxable: parseFloat( values ) }});
                                                        this.handleValidEditNumber( values );
                                                    } }
                                                    ref={ ( ref ) => { this.max_non_taxable = ref; } }
                                                    disabled={ `${this.state.editData.fully_taxable}` === 'true' }
                                                />
                                            : <P noBottomMargin>{ value.fully_taxable === true
                                                    ? value.max_non_taxable === ''
                                                        ? 'N/A'
                                                        : value.max_non_taxable
                                                    : value.max_non_taxable}
                                            </P>
                                        }
                                        {editData.fully_taxable === false
                                            && value.id === this.state.editData.id
                                            && this.state.maxNonValid === ''
                                                ? <P noBottomMargin>Please input valid data</P>
                                                : editData.fully_taxable === false
                                                    && value.id === this.state.editData.id
                                                    && this.state.noTaxableEdit === true
                                                        ? <P noBottomMargin>This field minimal value is 1</P>
                                                        : ''
                                        }
                                    </div>
                                    <div>
                                        {
                                            this.showTextBox( value.id ) && this.state.showbutton
                                            ? <div className="save-cancel-add-btn">
                                                <Button
                                                    className="addType"
                                                    label="Save"
                                                    type="action"
                                                    size="default"
                                                    onClick={ () => this.handleEdit() }
                                                />
                                                <Button
                                                    className="addType"
                                                    label="Cancel"
                                                    type="darkRed"
                                                    alt
                                                    onClick={ () => this.setState({
                                                        showbutton: false,
                                                        editData: ''
                                                    }) }
                                                />
                                            </div>
                                            : <div style={ { display: 'flex' } } className="tranlate-btn">
                                                <Button
                                                    className="addType"
                                                    label={ <span><Icon name="pencil" className="icon" /> Edit</span> }
                                                    type="neutral"
                                                    onClick={ () => this.setState({ showbutton: true, editData: value }) }
                                                />
                                                <Button
                                                    className="addType"
                                                    label={ <span><Icon name="trash" className="icon" /> Delete</span> }
                                                    type="darkRed"
                                                    alt
                                                    onClick={ () => {
                                                        this.setState({
                                                            showModel: true,
                                                            listId: value.id
                                                        });
                                                        this.DeleteModel.toggle();
                                                    } }
                                                />
                                            </div>
                                        }
                                    </div>
                                </div>
                            )}
                            {
                                    this.state.editData !== ''
                                    ? ''
                                    : <div className="row-details">
                                        <div>
                                            <P noBottomMargin>Bonus type { this.state.bonusDataList ? this.state.bonusDataList.length + 1 : 1 }</P>
                                        </div>
                                        <div
                                            className={
                                            this.state.ExistName === '' || this.state.ValidName === ''
                                                ? ' input-error'
                                                : ''
                                            }
                                        >
                                            <Input
                                                id="name"
                                                type="text"
                                                placeholder="Enter name"
                                                value={ formData.name }
                                                ref={ ( ref ) => { this.name = ref; } }
                                                onChange={ ( values ) => this.setState({ ValidName: values, formData: { ...formData, name: values }}) }
                                            />
                                            {this.state.ValidName === '' ? <P noBottomMargin>Please input valid data</P> : ''}
                                        </div>
                                        <div className={ this.state.ValidBasis === '' ? ' input-error' : '' }>
                                            <Select
                                                id="basis"
                                                data={ data }
                                                value={ formData.basis }
                                                placeholder="Select option"
                                                ref={ ( ref ) => { this.basis = ref; } }
                                                onChange={ ( values ) => this.setState({ ValidBasis: values, formData: { ...formData, basis: values.value }}) }
                                            />
                                            {this.state.ValidBasis === '' ? <P noBottomMargin>Please input valid data</P> : ''}
                                        </div>
                                        <div className={ this.state.ValidFrequency === '' ? ' input-error' : '' }>
                                            <Select
                                                id="frequency"
                                                data={ data1 }
                                                value={ formData.frequency }
                                                placeholder="Select option"
                                                ref={ ( ref ) => { this.frequency = ref; } }
                                                onChange={ ( values ) => this.setState({ ValidFrequency: values, formData: { ...formData, frequency: values.value }}) }
                                            />
                                            {this.state.ValidFrequency === '' ? <P noBottomMargin>Please input valid data</P> : ''}
                                        </div>
                                        <div>
                                            <RadioGroup
                                                horizontal
                                                value={ `${formData.fully_taxable}` }
                                                onChange={ ( values ) => this.setState({ validmax_no_taxable: 1,
                                                    formData: { ...formData,
                                                        fully_taxable: values === 'true',
                                                        max_non_taxable: values === 'false' ? formData.max_non_taxable === '' ? 90000.00 : formData.max_non_taxable : ''
                                                    }}) }
                                            >
                                                <Radio value="true" padding={ 4 }>Yes</Radio>
                                                <Radio value="false" padding={ 4 }>No</Radio>
                                            </RadioGroup>
                                        </div>
                                        <div
                                            className={
                                            this.state.validmax_no_taxable === ''
                                                ? ' input-error'
                                                : this.state.noTaxable === true
                                                    ? ' input-error'
                                                    : ' taxable-input'
                                            }
                                        >
                                            {this.state.formData.fully_taxable === true
                                            ? <Input
                                                id="name"
                                                type="text"
                                                ref={ ( ref ) => { this.max_non_taxable = ref; } }
                                                disabled
                                            />
                                            : <Input
                                                id="max_non_taxable"
                                                type="number"
                                                value={ formData.max_non_taxable }
                                                onChange={ ( values ) => {
                                                    this.setState({ validmax_no_taxable: values, formData: { ...formData, max_non_taxable: parseFloat( values ) }});
                                                    this.handleValidNumber( values );
                                                } }
                                                ref={ ( ref ) => { this.max_non_taxable = ref; } }
                                            />
                                            }
                                            { formData.fully_taxable === false && this.state.validmax_no_taxable === '' ? <P noBottomMargin>Please input valid data</P> : formData.fully_taxable === false && this.state.noTaxable === true ? <p>This field minimal value is 1</p> : ''}
                                        </div>
                                        <div>
                                            <Button
                                                className="button-add-type"
                                                label={ <span><Icon name="plus" className="icon" /> Add Bonus Type</span> }
                                                type="neutral"
                                                disabled={ loading }
                                                onClick={ () => this.addBonusType() }
                                            />
                                        </div>
                                    </div>
                                }
                        </div>
                    </PageWrapper>
                </div>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.DiscardModal.toggle() }
                            />
                            <Button
                                label="Submit"
                                type="action"
                                size="large"
                                disabled={ this.state.bonusDataList.length <= 0 ? true : !!loading }
                                onClick={ () => this.addBonusList() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    products: makeSelectProducts(),
    BonusTypeAdd: makeSelectSetAddbonustype(),
    BonusTypeAddList: makeSelectSetListbonustype(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        bonusAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Add );
