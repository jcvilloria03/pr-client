import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;
`;

export const PageWrapper = styled.div`
    padding-top: 120px;
    .content {
        margin-top: 40px;
        .entriTitle{
            text-align:right;
        }
         
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: bold;
            }
            p{
                margin-bottom:0.25rem;
            }
        }

        .add_switch{
             margin-top: 16px;
             }
        .add_checkswitch{
            display: flex;
            align-items: center;
        }
        .add_checkswitch label{
            margin-bottom: 0px;
            padding-right: 10px;
        }
        .add_checkswitch .switch_label{
            margin-left: 15px;
            font-size: 14px;
        }
        .add_checkswitch.add_teamswitch{
            label.switch{
                height: initial;
            }
            .rc-switch{
                width: 35px;
                height: 7px;
                &:after{
                    width: 17px;
                    height: 17px;
                    top: -6px;
                    left: -1px;
                    background-color: #fff;
                    box-shadow: 0 2px 5px rgb(0 0 0 / 26%);
                }
            }
            .rc-switch.rc-switch-checked{
                border: 1px solid #ccc;
                background-color: #ccc;
                &:after{
                    left: 22px;
                    background-color: #83d24b;
                    box-shadow: none;
                }
            }
        }
        .add_switch .switch {
            position: relative;
            display: inline-block;
            width: 35px;
            height: 8px;
        }
        .switch input { 
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .slider:before {
            position: absolute;
            content: "";
            height: 17px;
            width: 17px;
            left: 0px;
            top: -5px;
            background-color: #474747;
            -webkit-transition: .4s;
            transition: .4s;
        }
        input:checked + .slider {
            background: #cccccc;
        }
        input:checked + .slider:before{
            background: #83D24B;
            left: -10px;
            top: -5px;
        }
        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }
        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
        .teamTitle{
            font-size:14px;
            margin-bottom: 0.25rem;
        }
        .errorMessage{
            color:#f21108;
            font-size:14px;
        }
    }
`;

export const FootWrapper = styled.div`
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        margin-top: 20px;
        position: fixed;
        bottom: 0;
        left: 0;
        right: 0;
        button {
            min-width: 120px;
        }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
    margin-top: 110px;
`;
