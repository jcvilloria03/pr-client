import {
    DEFAULT_ACTION,
    GET_ADD_TEAMS_LIST,
    NOTIFICATION
} from './constants';
/**
 *  Search Empolyee actions
 */
export function addTeamsAction( payload ) {
    return {
        type: GET_ADD_TEAMS_LIST,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 *
 * AddTeam actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
