/*
 *
 * AddTeam constants
 *
 */

export const namespace = 'app/containers/AddTeam/Add';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const GET_ADD_TEAMS_LIST = `${namespace}/GET_ADD_TEAMS_LIST`;
export const SET_ADD_TEAMS_LIST = `${namespace}/SET_ADD_TEAMS_LIST`;
export const LOADING = `${namespace}/SET_SEARCH_LIST`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
