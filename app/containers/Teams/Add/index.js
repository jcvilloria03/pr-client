/* eslint-disable no-param-reassign */
/* eslint-disable react/sort-comp */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable react/prop-types */
/* eslint-disable import/first */
import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';

import makeSelectAddTeam, { makeSelectTeamsList, makeSelectNotification } from './selectors';
import { makeSelectProducts } from '../View/selectors';
import * as addTeamActions from './actions';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';

import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import { H3 } from 'components/Typography';
import Button from 'components/Button';
import A from 'components/A';
import Input from 'components/Input';
import MultiSelect from 'components/MultiSelect';
import Loader from 'components/Loader';

import {
    NavWrapper,
    PageWrapper,
    FootWrapper
} from './styles';

/**
 *
 * AddTeam
 *
 */
export class AddTeam extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        submitted: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            name: '',
            edit_schedule: true,
            team_attendance: true,
            leaderErrorMsg: true,
            memberErrorMsg: true
        };

        this.handleClick = this.handleClick.bind( this );
    }

    /**
     * Member List
     * @param {string} keyword
     * @param {*} callback
     */
    loadLeaderAndMemberList = ( keyword, callback ) => {
        const companyId = company.getLastActiveCompanyId();
        Fetch( `/company/${companyId}/ta_employees/search?term=${keyword}&limit=10`, { method: 'GET' })
            .then( ( result ) => {
                const list = result.data.map( ( option ) => ({
                    value: option.id,
                    label: `${option.first_name} ${option.middle_name} ${option.last_name} - ${option.employee_id}`.replace( /\s+/g, ' ' )
                }) );
                callback( null, { options: list });
            })
            .catch( ( error ) => callback( error, null ) );
    }

    errorMessagedata() {
        const errorMessage = this.members && this.members.state.value.filter( ( member ) => this.leaders.state.value.some( ( leader ) => member.label === leader.label ) ).length === 0;
        this.setState({ leaderErrorMsg: errorMessage });
    }

    handleClick() {
        const newMemberArrayOfObj = this.members.state.value.map( ( data ) => ({
            id: data.value,
            name: data.label
        }) );

        const newLeaderList = this.leaders.state.value.map( ( data ) => ({
            id: data.value,
            name: data.label
        }) );

        this.errorMessagedata();

        const dataValue = {
            name: this.state.name,
            leaders: newLeaderList,
            leaders_ids: newLeaderList.map( ({ id }) => id ),
            members: newMemberArrayOfObj,
            members_ids: newMemberArrayOfObj.map( ({ id }) => id ),
            edit_team_schedule: this.state.edit_schedule,
            regenerate_team_attendance: this.state.team_attendance

        };
        this.props.addTeamsAction( dataValue );
    }

    /**
     *
     * AddTeam render method
     *
     */
    render() {
        const { notification } = this.props;
        const { name, leaderErrorMsg } = this.state;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        return (
            <div>
                <Helmet
                    title="Add Team"
                    meta={ [
                        { name: 'description', content: 'Description of Add Team' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/company-settings/company-structure/teams', true );
                            } }
                        >
                            &#8592; Back to Teams
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="heading">
                                <H3>Add Team</H3>
                                <p>Provide a new group that an employee can lead or be a member of.</p>
                            </div>
                            <div className="w-100">
                                <div className="row">
                                    <div className="col-md-6">
                                        <p className="teamTitle">* Team name</p>
                                        <Input
                                            id="name"
                                            name="name"
                                            value={ name }
                                            ref={ ( ref ) => { this.names = ref; } }
                                            onChange={ ( value ) => {
                                                this.setState({ name: value });
                                            } }
                                            required
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <p className="teamTitle">* Team leaders</p>
                                        <MultiSelect
                                            id="leaders"
                                            async
                                            loadOptions={ this.loadLeaderAndMemberList }
                                            ref={ ( ref ) => { this.leaders = ref; } }
                                            placeholder="Enter employee name"
                                            required
                                        />
                                        {leaderErrorMsg === false && <p className="errorMessage">Please input data that is not selected as team member</p>}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <p className="teamTitle">* Team members</p>
                                        <MultiSelect
                                            id="members"
                                            async
                                            loadOptions={ this.loadLeaderAndMemberList }
                                            ref={ ( ref ) => { this.members = ref; } }
                                            placeholder="Enter employee name"
                                            required
                                        />
                                        {leaderErrorMsg === false && <p className="errorMessage">Please input data that is not selected as team leader</p>}
                                    </div>
                                </div>
                                <div className="add_switch add_checkswitch">
                                    <label className="switch">
                                        <input
                                            type="checkbox"
                                            id="edit_schedule"
                                            name="edit_schedule"
                                            checked={ this.state.edit_schedule }
                                            ref={ ( ref ) => { this.edit_schedule = ref; } }
                                            onChange={ () => {
                                                this.setState({ edit_schedule: !this.state.edit_schedule });
                                            } }
                                        />
                                        <span className="slider round"></span>
                                    </label>
                                    <p className="switch_label mb-0">Allow team leader to edit assigned shift of team members
                                    </p>
                                </div>
                                <div className="add_switch add_checkswitch">
                                    <label className="switch">
                                        <input
                                            type="checkbox"
                                            id="team_attendance"
                                            name="team_attendance"
                                            checked={ this.state.team_attendance }
                                            ref={ ( ref ) => { this.team_attendance = ref; } }
                                            onChange={ () => {
                                                this.setState({ team_attendance: !this.state.team_attendance });
                                            } }
                                        />
                                        <span className="slider round"></span>
                                    </label>
                                    <p className="switch_label mb-0">Allow team leader to regenerate attendance of team members
                                    </p>
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <FootWrapper>
                    <div className="pull-right">
                        <Button
                            label="Cancel"
                            type="action"
                            size="large"
                            alt
                            onClick={ () => { browserHistory.push( '/company-settings/company-structure/teams', true ); } }
                        />
                        <Button
                            id="submitButton"
                            label={ this.props.submitted ? <Loader /> : 'Submit' }
                            type="action"
                            size="large"
                            ref={ ( ref ) => { this.submitButton = ref; } }
                            onClick={ this.handleClick }
                        />
                    </div>
                </FootWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    AddTeam: makeSelectAddTeam(),
    products: makeSelectProducts(),
    submitted: makeSelectTeamsList(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        addTeamActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddTeam );
