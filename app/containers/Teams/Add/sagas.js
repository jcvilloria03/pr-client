import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import {
    NOTIFICATION,
    NOTIFICATION_SAGA,
    GET_ADD_TEAMS_LIST,
    SET_ADD_TEAMS_LIST
} from './constants';

/**
 * search Data API
 */
export function* addTeamSaga({ payload }) {
    try {
        yield put({
            type: SET_ADD_TEAMS_LIST,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/team/is_name_available`, { method: 'POST', data: payload });
        if ( response.available === true ) {
            yield call( Fetch, '/time_attendance_team', { method: 'POST', data: { ...payload, company_id: companyId }});
            yield put({
                type: SET_ADD_TEAMS_LIST,
                payload: false
            });

            yield call( browserHistory.push( '/company-settings/company-structure/teams', true ) );
        } else {
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: 'Please check field',
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_ADD_TEAMS_LIST,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
}

/**
 * Display a notification to user
 */
export function* notifyFromClient({ payload }) {
    yield call( notifyUser, payload );
}

/**
 * Watcher for NOTIFICATION
 *
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_ADD_TEAMS_LIST
 *
 */
export function* watchForAddTeamData() {
    const watcher = yield takeEvery( GET_ADD_TEAMS_LIST, addTeamSaga );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForAddTeamData,
    watchForNotifyUser
];
