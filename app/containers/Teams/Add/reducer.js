import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_ADD_TEAMS_LIST,
    NOTIFICATION_SAGA
} from './constants';

const initialState = fromJS({
    submitted: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * AddTeam reducer
 *
 */
function addTeamReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_ADD_TEAMS_LIST:
            return state.set( 'submitted', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default addTeamReducer;
