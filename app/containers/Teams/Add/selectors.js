import { createSelector } from 'reselect';

/**
 * Direct selector to the addTeam state domain
 */
const selectAddTeamDomain = () => ( state ) => state.get( 'addTeam' );

/**
 * Other specific selectors
 */
const makeSelectTeamsList = () => createSelector(
  selectAddTeamDomain(),
  ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
  selectAddTeamDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);
/**
 * Default selector used by AddTeam
 */

const makeSelectAddTeam = () => createSelector(
  selectAddTeamDomain(),
  ( substate ) => substate.toJS()
);

export default makeSelectAddTeam;
export {
  selectAddTeamDomain,
  makeSelectTeamsList,
  makeSelectNotification
};
