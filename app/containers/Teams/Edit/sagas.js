import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    NOTIFICATION,
    NOTIFICATION_SAGA,
    GET_TEAM_LIST,
    SET_TEAM_LIST,
    GET_TEAM_MESSAGE,
    SET_TEAM_MESSAGE
} from './constants';

/**
 * edit Team saga
 */
export function* editTeamList({ payload }) {
    try {
        yield put({
            type: SET_TEAM_MESSAGE,
            payload: true
        });
        const response = yield call( Fetch, `/team/${payload.id}`, { method: 'GET' });
        yield put({
            type: SET_TEAM_LIST,
            payload: response || {}
        });
        yield call( delay, 3000 );
        yield put({
            type: SET_TEAM_MESSAGE,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_TEAM_MESSAGE,
            payload: false
        });
    }
}

/**
 * edit Team Message Saga
 */
export function* editTeamAction({ payload }) {
    try {
        yield put({
            type: SET_TEAM_MESSAGE,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const payloadvalue = {
            edit_team_schedule: payload.edit_team_schedule,
            id: payload.team_id,
            leader: payload.leader,
            members: payload.members,
            name: payload.name,
            regenerate_team_attendance: payload.regenerate_team_attendance,
            team_id: payload.team_id
        };
        const response = yield call( Fetch, `/company/${companyId}/team/is_name_available`, { method: 'POST', data: payloadvalue });
        if ( response.available === true ) {
            const payloadList = { ...payload, company_id: companyId };
            yield call( Fetch, `/team/${payload.team_id}`, { method: 'PUT', data: payloadList });

            yield call( notifyUser, {
                show: true,
                title: 'Success',
                message: 'Successfully updated this team\'s information.',
                type: 'success'
            });
            yield call( browserHistory.push( '/company-settings/company-structure/teams', true ) );
            yield put({
                type: SET_TEAM_MESSAGE,
                payload: false
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_TEAM_MESSAGE,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
}

/**
 * Display a notification to user
 */
export function* notifyFromClient({ payload }) {
    yield call( notifyUser, payload );
}

/**
 * Watcher for NOTIFICATION
 *
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    yield call( resetStore );

    yield call( editTeamList );
}

/**
 * Watcher for GET_TEAMS
 *
 */
export function* watchForTeamData() {
    const watcher = yield takeEvery( GET_TEAM_LIST, editTeamList );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_TEAMS
 *
 */
export function* watchForTeamListData() {
    const watcher = yield takeEvery( GET_TEAM_MESSAGE, editTeamAction );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForReinitializePage,
    watchForTeamData,
    watchForTeamListData
];
