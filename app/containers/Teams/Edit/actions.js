import {
    DEFAULT_ACTION,
    GET_TEAM_LIST,
    GET_TEAM_MESSAGE,
    NOTIFICATION
} from './constants';

/**
 * Display a select Data
 */
export function editTeamList( payload ) {
    return {
        type: GET_TEAM_LIST,
        payload
    };
}

/**
 * Display a select Data
 */
export function editTeamAction( payload ) {
    return {
        type: GET_TEAM_MESSAGE,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
/**
 *
 * EditTeams actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
