import { createSelector } from 'reselect';

/**
 * Direct selector to the editTeams state domain
 */
const selectEditTeamsDomain = () => ( state ) => state.get( 'editTeams' );

/**
 * Other specific selectors
 */
const makeSelectEditTeamsList = () => createSelector(
  selectEditTeamsDomain(),
  ( substate ) => substate.get( 'editTeam' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectEditTeamsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectEditTeamsData = () => createSelector(
  selectEditTeamsDomain(),
  ( substate ) => substate.get( 'editMessage' )
);

/**
 * Default selector used by EditTeams
 */

const makeSelectEditTeams = () => createSelector(
  selectEditTeamsDomain(),
  ( substate ) => substate.toJS()
);

export default makeSelectEditTeams;
export {
  selectEditTeamsDomain,
  makeSelectEditTeamsList,
  makeSelectNotification,
  makeSelectEditTeamsData
};
