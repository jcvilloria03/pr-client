/*
 *
 * EditTeams constants
 *
 */

export const namespace = 'app/containers/EditTeams/edit';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const GET_TEAM_LIST = `${namespace}/GET_TEAM_LIST`;
export const SET_TEAM_LIST = `${namespace}/SET_TEAM_LIST`;
export const GET_TEAM_MESSAGE = `${namespace}/GET_TEAM_MESSAGE`;
export const SET_TEAM_MESSAGE = `${namespace}/SET_TEAM_MESSAGE`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
