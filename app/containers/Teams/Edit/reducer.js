import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    NOTIFICATION_SAGA,
    SET_TEAM_LIST,
    SET_TEAM_MESSAGE
} from './constants';

const initialState = fromJS({
    editTeam: {},
    editMessage: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * EditTeams reducer
 *
 */
function editTeamsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_TEAM_LIST:
            return state.set( 'editTeam', fromJS( action.payload ) );
        case SET_TEAM_MESSAGE:
            return state.set( 'editMessage', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default editTeamsReducer;
