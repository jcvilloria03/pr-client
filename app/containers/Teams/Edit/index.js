/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable import/first */
/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';

import { createStructuredSelector } from 'reselect';
import makeSelectEditTeams, {
    makeSelectEditTeamsList,
    makeSelectNotification,
    makeSelectEditTeamsData
} from './selectors';
import * as EditTeamActions from './actions';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';

import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import { H3, H2 } from 'components/Typography';
import Button from 'components/Button';
import Switch from 'components/Switch';
import A from 'components/A';
import Input from 'components/Input';
import MultiSelect from 'components/MultiSelect';
import Loader from 'components/Loader';

import {
    NavWrapper,
    PageWrapper,
    FootWrapper,
    LoadingStyles
} from '../Add/styles';

/**
 *
 * EditTeams
 *
 */
export class EditTeams extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        params: React.PropTypes.object,
        EditTeamsList: React.PropTypes.object,
        editTeamList: React.PropTypes.func,
        editTeamAction: React.PropTypes.func,
        editted: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            teamDataList: {},
            name: '',
            edit_schedule: '',
            team_attendance: '',
            leaderErrorMsg: true,
            memberErrorMsg: true,
            leadersList: '',
            membersList: ''
        };

        this.handleEditClick = this.handleEditClick.bind( this );
    }

    componentDidMount() {
        this.props.editTeamList({ id: this.props.params.id });
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.EditTeamsList && nextProps.EditTeamsList !== this.props.EditTeamsList &&
            this.setState({
                edit_schedule: this.state.edit_schedule ? this.state.edit_schedule : nextProps.EditTeamsList.edit_team_schedule,
                team_attendance: this.state.team_attendance ? this.state.team_attendance : nextProps.EditTeamsList.regenerate_team_attendance
            });

        this.defaultLeadersList();
        this.defaultMemberList();
    }

    /**
     * Member List
     * @param {string} keyword
     * @param {*} callback
     */
    loadLeaderAndMemberList = ( keyword, callback ) => {
        const companyId = company.getLastActiveCompanyId();
        Fetch( `/company/${companyId}/ta_employees/search?term=${keyword}&limit=10`, { method: 'GET' })
            .then( ( result ) => {
                const list = result.data.map( ( option ) => ({
                    value: option.id,
                    label: `${option.first_name} ${option.middle_name} ${option.last_name} - ${option.employee_id}`.replace( /\s+/g, ' ' )
                }) );
                callback( null, { options: list });
            })
            .catch( ( error ) => callback( error, null ) );
    }

    errorMessagedata() {
        const errorMessage = this.members && this.members.state.value.filter( ( member ) => this.leaders.state.value.some( ( leader ) => member.label === leader.label ) ).length === 0;
        this.setState({ leaderErrorMsg: errorMessage });
    }

    handleEditClick() {
        const newMemberList = this.state.membersList.map( ( data ) => ({
            id: data.value,
            name: data.label
        }) );

        const newLeaderList = this.state.leadersList.map( ( data ) => ({
            id: data.value,
            name: data.label
        }) );

        this.errorMessagedata();
        const setEditValue = this.props.EditTeamsList;
        const dataValue = {
            name: this.state.name === '' ? setEditValue.name : this.state.name,
            leaders: newLeaderList,
            leaders_ids: newLeaderList.map( ({ id }) => id ),
            members: newMemberList,
            members_ids: newMemberList.map( ({ id }) => id ),
            edit_team_schedule: this.state.edit_schedule,
            regenerate_team_attendance: this.state.team_attendance,
            team_id: this.props.params.id

        };
        this.props.editTeamAction( dataValue );
    }

    defaultLeadersList = () => {
        if ( !this.props.EditTeamsList.leaders ) {
            return [];
        }

        const leaders = this.props.EditTeamsList.leaders.map( ( leader ) => ({
            value: leader.id,
            label: leader.name
        }) );

        this.setState({ leadersList: leaders });
        return leaders;
    }

    defaultMemberList = () => {
        if ( !this.props.EditTeamsList.members ) {
            return [];
        }

        const members = this.props.EditTeamsList.members.map( ( member ) => ({
            value: member.id,
            label: member.name
        }) );
        this.setState({ membersList: members });
        return members;
    }
    /**
     *
     * EditTeams render method
     *
     */
    render() {
        const { EditTeamsList, notification } = this.props;
        const { leaderErrorMsg } = this.state;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        return (
            <div>
                <Helmet
                    title="Edit Team"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Team' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/company-settings/company-structure/teams', true );
                            } }
                        >
                            &#8592; Back to Teams
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container>
                        {this.props.editted ?
                            <div className="loader" style={ { display: this.props.editted ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Loading Teams.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div> : ''}
                        {!this.props.editted ?
                            <div className="content">
                                <div className="heading">
                                    <H3>Edit Team</H3>
                                    <p>Update this team&apos; s name, leader, members, and permissions.</p>
                                </div>
                                <div className="w-100">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <p className="teamTitle">* Team name</p>
                                            <Input
                                                id="name"
                                                name="name"
                                                value={ EditTeamsList ? EditTeamsList.name : '' }
                                                ref={ ( ref ) => { this.names = ref; } }
                                                onChange={ ( value ) => {
                                                    this.setState({ name: value });
                                                } }
                                                required
                                            />
                                        </div>
                                        <div className="col-md-6">
                                            <p className="teamTitle">* Team leaders</p>
                                            <MultiSelect
                                                id="leaders"
                                                async
                                                loadOptions={ this.loadLeaderAndMemberList }
                                                value={ this.state.leadersList }
                                                ref={ ( ref ) => { this.leaders = ref; } }
                                                placeholder="Enter employee name"
                                                onChange={ () => { this.setState({ leadersList: this.leaders.state.value }); } }
                                                required
                                            />
                                            {leaderErrorMsg === false && <p className="errorMessage">Please input data that is not selected as team member</p>}
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <p className="teamTitle">* Team members</p>
                                            <MultiSelect
                                                id="members"
                                                async
                                                loadOptions={ this.loadLeaderAndMemberList }
                                                value={ this.state.membersList }
                                                ref={ ( ref ) => { this.members = ref; } }
                                                onChange={ () => {
                                                    this.setState({ membersList: this.members.state.value });
                                                } }
                                                placeholder="Enter employee name"
                                                required
                                            />
                                            {leaderErrorMsg === false && <p className="errorMessage">Please input data that is not selected as team leader</p>}
                                        </div>
                                    </div>
                                    <div className="add_switch add_checkswitch add_teamswitch">
                                        <label className="switch">
                                            <Switch
                                                checked={ this.state.edit_schedule }
                                                ref={ ( ref ) => { this.edit_schedule = ref; } }
                                                onChange={ () => {
                                                    this.setState({ edit_schedule: !this.state.edit_schedule });
                                                } }
                                            />
                                        </label>
                                        <p className="switch_label mb-0">Allow team leader to edit assigned shift of team members
                                        </p>
                                    </div>
                                    <div className="add_switch add_checkswitch add_teamswitch">
                                        <label className="switch">
                                            <Switch
                                                checked={ this.state.team_attendance }
                                                ref={ ( ref ) => { this.team_attendance = ref; } }
                                                onChange={ () =>
                                                    this.setState({ team_attendance: !this.state.team_attendance })
                                                }
                                            />
                                        </label>
                                        <p className="switch_label mb-0">Allow team leader to regenerate attendance of team members
                                        </p>
                                    </div>
                                </div>
                            </div> : ''}
                    </Container>
                </PageWrapper>
                <FootWrapper>
                    <div className="pull-right">
                        <Button
                            label="Cancel"
                            type="action"
                            size="large"
                            alt
                            onClick={ () => { browserHistory.push( '/company-settings/company-structure/teams', true ); } }
                        />
                        <Button
                            id="updateButton"
                            label={ this.props.editted ? <Loader /> : 'Update' }
                            type="action"
                            size="large"
                            ref={ ( ref ) => { this.updateButton = ref; } }
                            onClick={ this.handleEditClick }
                        />
                    </div>
                </FootWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    EditTeams: makeSelectEditTeams(),
    EditTeamsList: makeSelectEditTeamsList(),
    notification: makeSelectNotification(),
    editted: makeSelectEditTeamsData()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        EditTeamActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EditTeams );
