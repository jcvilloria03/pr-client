/*
 *
 * Teams constants
 *
 */
export const namespace = 'app/containers/Teams/view';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const GET_TEAMS = `${namespace}/GET_TEAMS`;
export const SET_TEAMS = `${namespace}/SET_TEAMS`;
export const SET_TEAMS_LENGTH = `${namespace}/SET_TEAMS_LENGTH`;
export const DELETE_TEAM = `${namespace}/DELETE_TEAM `;
export const DELETED_TEAM = `${namespace}/ DELETED_TEAM `;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
