import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    SET_TEAMS,
    SET_TEAMS_LENGTH,
    DELETED_TEAM,
    NOTIFICATION_SAGA
} from './constants';

const initialState = fromJS({
    loading: true,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    teamList: [],
    length: 0
});

/**
 *
 * Teams reducer
 *
 */
function teamsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_TEAMS:
            return state.set( 'teamList', fromJS( action.payload ) );
        case SET_TEAMS_LENGTH:
            return state.set( 'length', action.payload );
        case DELETED_TEAM:
            return state.set( 'deleteTeam', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default teamsReducer;
