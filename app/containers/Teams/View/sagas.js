/* eslint-disable require-jsdoc */
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    GET_TEAMS,
    SET_TEAMS,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    DELETE_TEAM,
    DELETED_TEAM,
    LOADING,
    SET_TEAMS_LENGTH
} from './constants';

/**
 * Teams Saga functions
 */
export function* getTeams() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/teams`, { method: 'GET' });

        yield put({
            type: SET_TEAMS,
            payload: response && response.data || []
        });
        yield put({
            type: SET_TEAMS_LENGTH,
            payload: response && response.data.length || []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

export function* deletedTeam({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();
        const formData = new FormData();
        formData.append( '_method', 'DELETE' );
        formData.append( 'company_id', companyId );
        formData.append( 'team_ids[]', payload.ids );
        yield call( Fetch, '/team/bulk_delete', { method: 'POST', data: formData });
        yield put({
            type: DELETED_TEAM,
            payload
        });
        yield call( getTeams, { payload: {}});
        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully deleted',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
}

/**
 * Display a notification to user
 */
export function* notifyFromClient({ payload }) {
    yield call( notifyUser, payload );
}

/**
 * Watcher for NOTIFICATION
 *
 */
export function* watchForDeleteTeam() {
    const watcher = yield takeEvery( DELETE_TEAM, deletedTeam );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    yield call( resetStore );

    yield call( getTeams );
}

/**
 * Watcher for GET_TEAMS
 *
 */
export function* watchForTeamData() {
    const watcher = yield takeEvery( GET_TEAMS, getTeams );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForReinitializePage,
    watchForTeamData,
    watchForDeleteTeam
];
