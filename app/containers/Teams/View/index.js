/* eslint-disable no-unused-vars */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable consistent-return */
/* eslint-disable react/prop-types */
/* eslint-disable import/first */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import makeSelectTeams, {
    makeSelectProducts,
    makeSelectTeamsList,
    makeSelectTeamsLength,
    makeSelectLoading,
    makeSelectNotification

} from './selectors';
import * as teamsActions from './actions';
import Modal from 'components/Modal';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';

import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import { H2, H3, H5 } from 'components/Typography';
import Button from 'components/Button';
import SalDropdown from 'components/SalDropdown';
import Table from 'components/Table';
import { isAnyRowSelected, getIdsOfSelectedRows } from 'components/Table/helpers';
import {
    PageWrapper,
    LoadingStyles
} from './styles';

import logo from '../../../../app/favicon.png';

/**
 *
 * Teams
 *
 */
export class Teams extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        loading: React.PropTypes.bool,
        teamsList: React.PropTypes.array,
        getTeamAction: React.PropTypes.func,
        deleteTeam: React.PropTypes.func,
        length: React.PropTypes.number,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            notification: {
                title: '',
                message: '',
                show: false,
                type: 'error'
            },
            label: 'Showing 0-0 of 0 entries',
            selection_label: '',
            editableCell: false,
            edited: null
        };
        this.membersListdata = this.membersListdata.bind( this );
    }

    componentWillMount() {
        this.props.getTeamAction();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.teamsList !== this.props.teamsList &&
            this.setState({
                label: formatPaginationLabel( this.teamsTable.tableComponent.state )
            });
    }

    // eslint-disable-next-line react/sort-comp
    membersListdata = ( row ) => {
        if ( !row || row === '' || row.length === 0 ) {
            return '';
        }
        if ( row.length === 1 ) {
            return row[ 0 ].name;
        }
        return row.length > 2 ? `${row[ 0 ].name} and ${row.length - 1} more` : `${row[ 0 ].name} and ${row[ 1 ].name}`;
    }

    handleTableChanges = ( tableProps = this.teamsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            selection_label: formatDeleteLabel()
        });
    };

    handleSelectionChanges = ({ selected }) => {
        const selectionLength = selected.filter( ( row ) => row ).length;
        this.setState({
            selection_label: formatDeleteLabel( selectionLength )
        });
    }

    getDropdownItems() {
        return [
            {
                children: <div>Delete</div>,
                onClick: () => this.DeleteModel.toggle()
            }
        ];
    }
    hanldeDelete = () => {
        const payload = getIdsOfSelectedRows( this.teamsTable );
        this.props.deleteTeam({ ids: payload });
        this.DeleteModel.toggle();
    }

    getTableColumns() {
        return [
            {
                id: 'name',
                header: 'Teams Name',
                minWidth: 100,
                sortable: false,
                render: ({ row }) => (
                    <div>{row.name}</div>
                )
            },
            {
                id: 'leader_name',
                header: 'Teams Leaders',
                minWidth: 300,
                sortable: false,
                render: ({ row }) => (
                    <div>{this.membersListdata( row.leaders )}</div>
                )
            },
            {
                id: 'members',
                header: 'Members',
                minWidth: 400,
                sortable: false,
                render: ({ row }) => (
                    <div>{this.membersListdata( row.members ) }</div>
                )
            },
            {
                id: 'name',
                header: <span>Teams Leader can edit team&apos;s assigned shift</span>,
                minWidth: 320,
                sortable: false,
                render: ({ row }) => (
                    <div>{row.edit_team_schedule ? 'Yes' : 'No'}</div>
                )
            },
            {
                id: 'name',
                header: 'Teams Leader can regenerate attendance',
                minWidth: 300,
                sortable: false,
                render: ({ row }) => (
                    <div>{row.regenerate_team_attendance ? 'Yes' : 'No' }</div>
                )
            },
            {
                header: ' ',
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => {
                            browserHistory.push( `/company-settings/company-structure/teams/${row.id}/edit`, true );
                        } }
                    />
                )
            }
        ];
    }

    /**
     *
     * Teams render method
     *
     */
    render() {
        const { label } = this.state;
        const { teamsList, loading } = this.props;

        const selectedItems = isAnyRowSelected( this.teamsTable );
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        return (
            <div>
                <Helmet
                    title="Teams"
                    meta={ [
                        { name: 'description', content: 'Description of Teams' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <Modal
                    title="Confirm Your Action"
                    ref={ ( ref ) => {
                        this.DeleteModel = ref;
                    } }
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.DeleteModel.toggle()
                        },
                        {
                            type: 'danger',
                            label: 'Yes',
                            onClick: this.hanldeDelete
                        }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Teams</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Teams</H3>
                                <p>View and update groups that an employee can lead or be a member of.</p>
                            </div>
                            <div className="title">
                                <H5>Teams List</H5>
                                <span className="title-Content">
                                    {selectedItems ? this.state.selection_label : label}
                                    <div className="ml-1">
                                        {selectedItems
                                        ? ( <SalDropdown dropdownItems={ this.getDropdownItems() } /> )
                                        : ( <Button
                                            id="button-filter"
                                            label="Add Team"
                                            type="action"
                                            onClick={ () => browserHistory.push( '/company-settings/company-structure/teams/add', true ) }
                                        /> )
                                         }
                                    </div>
                                </span>
                            </div>
                            <Table
                                loading={ this.props.teamsTable }
                                data={ teamsList }
                                columns={ this.getTableColumns() }
                                showFilters={ false }
                                pagination
                                selectable
                                onDataChange={ this.handleTableChanges }
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    this.setState({ selection_label: formatDeleteLabel( selectionLength ) });
                                } }
                                emptyTableComponent={ () => (
                                    <div className="empty-table">
                                        There are no Teams forms available to display
                                    </div>
                                ) }
                                ref={ ( ref ) => { this.teamsTable = ref; } }
                            />
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    Teams: makeSelectTeams(),
    products: makeSelectProducts(),
    teamsList: makeSelectTeamsList(),
    length: makeSelectTeamsLength(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        teamsActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Teams );
