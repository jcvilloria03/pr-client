/* eslint-disable require-jsdoc */
import {
    DEFAULT_ACTION,
    GET_TEAMS,
    NOTIFICATION,
    DELETE_TEAM
} from './constants';

/**
 * EmploymentTypes get data
 */
export function getTeamAction() {
    return {
        type: GET_TEAMS
    };
}
/**
 * Delete Team
 * @param {array} IDs
 */
export function deleteTeam( Ids ) {
    return {
        type: DELETE_TEAM,
        payload: Ids
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 *
 * Teams actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
