import { createSelector } from 'reselect';

/**
 * Direct selector to the teams state domain
 */
const selectTeamsDomain = () => ( state ) => state.get( 'teams' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
  selectTeamsDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectTeamsList = () => createSelector(
  selectTeamsDomain(),
  ( substate ) => substate.get( 'teamList' ).toJS()
);

const makeSelectTeamsLength = () => createSelector(
  selectTeamsDomain(),
  ( substate ) => substate.get( 'length' )
);

const makeSelectNotification = () => createSelector(
  selectTeamsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

/**
 * Default selector used by Teams
 */
const makeSelectTeams = () => createSelector(
  selectTeamsDomain(),
  ( substate ) => substate.toJS()
);

export default makeSelectTeams;
export {
  selectTeamsDomain,
  makeSelectProducts,
  makeSelectLoading,
  makeSelectTeamsList,
  makeSelectTeamsLength,
  makeSelectNotification
};
