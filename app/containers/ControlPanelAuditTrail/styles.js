import styled, { css } from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 400px;
`;

const commonFlexCenterStyle = css`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

export const PageWrapper = styled.div`
    .content {

        display: flex;
        flex-direction: column;
        position: relative;
        height: 100%;
        padding-top: 2rem;

        margin-top: 44px;

        width: 100%;
        font-size: 14px;
        max-width: 1600px;
        margin-left: auto;
        margin-right: auto;
        margin-bottom: 60px;

        
        .heading {
            ${commonFlexCenterStyle}

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
            }

            div {
                text-align: center;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;

            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 600;
                font-size: 20px;
            }
        }
    }

    .search-wrapper {
        flex-grow: 1;
        display: flex;
        .search {
            width: 300px;
            border: 1px solid #333;
            border-radius: 2px;
            margin-right: 3px;

            .input-group {
              height: 35px;

              input {
                height: 35px;
                border: none;
              }
            }
        }
        p {
            display: none;
        }

        .input-group,
        .form-control {
            background-color: transparent;
        }

   
        .isvg {
            display: inline-block;
            width: 1rem;
        }
    }


    .export_btn {
        background-color: #83d24b;
        color: #ffffff;
        border-color: #83d24b;
    }

    .filter-icon > svg {
        height: 10px;
    }
    .btn.btn-secondary.btn-default {
        border-radius: 0;
    }
    .btn.btn-secondary.btn-default.export-button {
        border-radius: 0;
    }
    .cpMTyx .input-group .input-group-addon {
        border: none;
    }
    .remove_sort{}
    .remove_sort .rt-thead{}
    .remove_sort .rt-thead .rt-tr{}
    .remove_sort .rt-thead .rt-tr .rt-th:nth-child(6),
    .remove_sort .rt-thead .rt-tr .rt-th:nth-child(7){
        pointer-events: none !important;
    }

    .remove_sort .rt-thead .rt-tr .rt-th:nth-child(6):after,
    .remove_sort .rt-thead .rt-tr .rt-th:nth-child(7):after{
        display: none !important
    }

    .rt-table *{
        word-wrap: break-word;
    }
    .rt-table{
        margin-bottom: 50px;
    }
    .rt-table .rt-thead{
        display: table !important;
        min-width: 100% !important
    }
    .rt-table .rt-tbody{
        display: table-row-group !important;
        min-width: 100% !important
    }

    .rt-table .rt-thead .rt-tr{
        display: table-row !important;
    }
    .rt-table .rt-thead .rt-th{
        display: table-cell !important;
        vertical-align: middle;
    }
    .rt-table .rt-thead .rt-th:after{
        margin: auto;
        bottom: 0;
    }
    .rt-table .rt-thead .rt-th:last-child{}


    .rt-table .rt-tbody .rt-tr{
        display: flex !important;
        flex-wrap: wrap;
    }
    .rt-table .rt-tbody .rt-td{
        display: inline-block !important;
        flex: initial !important;
        padding: 7px 22px;
    }
    .rt-table .rt-tbody .rt-td:last-child{}




    .rt-table .rt-thead .rt-tr,
    .rt-table .rt-tbody .rt-tr{
    }
    .rt-table .rt-thead .rt-th,
    .rt-table .rt-tbody .rt-td{
        width: calc(100% / 7) !important;
        min-width: calc(100% / 7) !important;
        max-width: calc(100% / 7) !important;
        white-space: initial !important;
    }
    .rt-table .rt-thead .rt-th:last-child,
    .rt-table .rt-tbody .rt-td:last-child{
        width: calc((100% / 8) * 1) !important;
        min-width: calc((100% / 8) * 1) !important;
        max-width: calc((100% / 8) * 1) !important;
    }

    .rt-resizable-header-content {
        padding: 12px 10px !important;
    }
    .loading_section {
        position: relative !important;
    }
    .mini-loading {
        display: flex !important;
        background: transparent !important;
        justify-content: center !important; 
        align-items: center !important;
        position: absolute !important;
        top: 0 !important; 
        left: 0 !important;
        right: 0 !important;
        bottom: 0 !important;
        z-index: 3 !important;
        background: rgba(255,255,255,0.8) !important;
        -webkit-transition: all 0.3s ease !important;
        transition: all 0.3s ease !important;
    }
    .ReactTable .rt-tbody .rt-tr.-padRow {
        display: none !important;
    }
    span.more_sec {
        font-weight: 700;
        cursor: pointer;
    }
    span.less_sec {
        font-weight: 700;
        cursor: pointer;
    }
`;

export const SectionWrapper = styled.section`
    border-radius: 5px;
    letter-spacing: 0.12px;
    border: solid 1px #adadad;
    background-color: #fff;
    padding: 20px 0;
    margin-bottom: 20px;

    .summary {
        display: flex;
        align-items: center;
        margin: 30px 50px;

        .fa {
            width: 35px;
            margin-right: 20px;
            text-align: center;
        }

        .fa, strong {
            color: #00A4E4;
        }
    }

    .details {
        margin: 0;
        display: flex;
        align-items: flex-start;
        justify-content: space-evenly;

        .licenses {
            flex-grow: 1;

            & > div {
                display: flex;
                justify-content: space-around;

                & > {
                    display: flex;
                    justify-content: center;
                }
            }

            .total {
                margin-top: 20px;
            }
        }
    }
`;

export const ModalBody = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 100%;

    * {
        font-size: 15px;
        font-variant: normal;
        padding: 0;
        margin: 0;
    }

    form {
        width: 100%;
        margin: 20px 0;
    }

    .title {
        display: flex;
        align-items: center;
        margin-bottom: 20px;
        justify-content: space-between;
        border-bottom: solid 2px #d0e1e1;
        margin-top: -15px;

        h3 {
            margin: 0;
            margin-right: 20px;
            margin-bottom: 20px;
            font-size: 15px;
            font-weight: 500
        }
    }

    .fa-icon > svg {
        height: 16px;
    }

    .mr-30 {
        margin-right: 30px;
    }

    .mt-25 {
        margin-top: 25px;
    }

    label {
        color: #6b7c93;
        font-weight: 300;
        letter-spacing: 0.025em;
    }

    input {
        font-weight: 300 !important;
        border: 0 !important;
        color: #31325F !important;
        outline: none !important;
        cursor: text !important;
        width: 100% !important;
        background: white;
        box-shadow: 0 3px 6px 0 rgba(49,49,93,0.10),
                    0 1px 2px 0 rgba(0,0,0,0.08);
        border-radius: 4px;
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .input::-webkit-input-placeholder { color: #CFD7E0; }
    .input::-moz-placeholder { color: #CFD7E0; }

    .address-section {
        margin-top: -20px;

        input {
            font-weight: 300 !important;
            border: 0 !important;
            color: #31325F !important;
            outline: none !important;
            cursor: text !important;
            width: 100% !important;
            background: white;
            box-shadow: 0 3px 6px 0 rgba(49,49,93,0.10),
                        0 1px 2px 0 rgba(0,0,0,0.08);
            border-radius: 4px;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .input::-webkit-input-placeholder { color: #CFD7E0; }
        .input::-moz-placeholder { color: #CFD7E0; }
    }

    .card-section {
        .group {
            background: white;
            box-shadow: 0 3px 6px 0 rgba(49,49,93,0.10),
                        0 1px 2px 0 rgba(0,0,0,0.08);
            border-radius: 4px;
            margin-bottom: 40px;
        }

        #card-element.CardField-cvc {

        }

        input,
        .StripeElement {
            display: block;
            margin: 10px 0 20px 0;
            padding: 17px 14px;
            font-size: 15px;
            box-shadow: 0 3px 6px 0 rgba(49,49,93,0.10),
                        0 1px 2px 0 rgba(0,0,0,0.08);
            border: 0;
            outline: 0;
            border-radius: 4px;
            background: white;
            height: 50px;
        }

        input:focus,
        .StripeElement--focus {
            box-shadow: rgba(50, 50, 93, 0.109804) 0px 4px 6px, rgba(0, 0, 0, 0.0784314) 0px 1px 3px;
            -webkit-transition: all 150ms ease;
            transition: all 150ms ease;
        }


        .card {
            padding-top: 11px !important;
        }

        .field {
            background: transparent;
            font-weight: 300;
            border: 0;
            color: #31325F;
            outline: none;
            flex: 1;
            padding-right: 10px;
            padding-left: 10px;
            cursor: text;
        }

        .card-error {
            -ms-flex-order: 3;
            -webkit-order: 3;
            order: 3;
            color: #f21108;
            font-size: 13px;
            padding-left: 2px;
            margin-bottom: 6px;
            margin-top: -16px !important;
        }

        .field::-webkit-input-placeholder { color: #8898AA; }
        .field::-moz-placeholder { color: #8898AA; }
    }

    button {
        white-space: nowrap;
        border: 0;
        outline: 0;
        display: inline-block;
        height: 40px;
        line-height: 40px;
        padding: 0 14px;
        box-shadow: 0 4px 6px rgba(50, 50, 93, .11), 0 1px 3px rgba(0, 0, 0, .08);
        color: #fff;
        border-radius: 4px;
        font-size: 15px;
        font-weight: 600;
        text-transform: uppercase;
        letter-spacing: 0.025em;
        background-color: #83d24b;
        text-decoration: none;
        -webkit-transition: all 150ms ease;
        transition: all 150ms ease;
        margin-top: 20px;
        width: 100%;
    }

    button:hover {
        color: #fff;
        cursor: pointer;
        background-color: #9ddb70;
        transform: translateY(-1px);
        box-shadow: 0 7px 14px rgba(50, 50, 93, .10), 0 3px 6px rgba(0, 0, 0, .08);
    }

    button:focus {
        background: #9ddb70;
    }

    button:active {
        background: #b9e699;
    }

    button:disabled,
    button[disabled] {
        background-color: #ade188;
        color: #ecf6fa;
        border-color: #ade188;
        opacity: 1;
        cursor: not-allowed;
        transform: none;
        box-shadow: 0;
    }

    .confirm-terms {
        font-size: 12px;
        font-weight: 550;
        color: gray;
        margin-top: 10px;
        text-align: center;
    }

    .payment-card-view {
        font-size: 14px;
        text-transform: uppercase;
        margin: 5px;

        .value {
            font-weight: 600;
        }

        .text-lc {
            font-weight: 500 !important;
            text-transform: lowercase !important;
        }
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    justify-content: center;
    min-height: 100%;
    text-align: center;
`;

export const SpinnerWrapper = styled.span`
    padding: 0 20px;
`;
