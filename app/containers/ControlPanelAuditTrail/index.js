/* eslint-disable react/jsx-no-undef, import/first  */
/* eslint-disable react/prop-types */
/* eslint-disable import/no-unresolved */
/* eslint-disable import/extensions */
import React from 'react';
import Helmet from 'react-helmet';
import { debounce } from 'lodash';
import moment from 'moment/moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import Button from 'components/Button';
import Icon from 'components/Icon';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import FooterTablePagination from 'components/FooterTablePagination';
import { H2, H3 } from 'components/Typography';

import { DATE_FORMATS } from 'utils/constants';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { formatPaginationLabel } from 'utils/functions';
import { TABLE_COLUMNS_SIMPLE } from './constants';
import { isAnyRowSelected } from '../../components/Table/helpers';
import Filter from './Filter';

import * as actions from './actions';

// data Table Imports
import Table from 'components/Table';
import {
    makeSelectFilterData,
    makeSelectAuditTrailTableLoading,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSubscriptionSummary,
    makeSelectSubscriptionStats,
    makeSelectCheckExpired,
    makeSelectAuditTrailDetails,
    makeSelectPagination,
    makeSelectMiniLoading
} from './selectors';
import {
    PageWrapper,
    LoadingStyles
} from './styles';
/**
 * Subscriptions Component
 */
export class ControlPanelAuditTrail extends React.PureComponent {
    static propTypes = {
        loading: React.PropTypes.bool,
        initializeData: React.PropTypes.func,
        filterData: React.PropTypes.shape({
            companies: React.PropTypes.array,
            users: React.PropTypes.array,
            transaction_types: React.PropTypes.array,
            modules: React.PropTypes.array
        }),
        auditTrailDetails: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        getAuditTrailDetails: React.PropTypes.func,
        generateAuditFile: React.PropTypes.func,
        pagination: React.PropTypes.object,
        checkExpired: React.PropTypes.bool
    }

    static defaultProps = {
        loading: false,
        downloading: false,
        errors: {},
        hasLicensesAvailable: false,
        pagination: {
            from: 1,
            to: 1,
            total: 1,
            current_page: 1,
            last_page: 1,
            per_page: 10
        }
    };

    constructor( props ) {
        super( props );

        this.state = {
            has_filters_applied: false,
            showFilter: false,
            hasFiltersApplied: false,
            filtersEnabled: false,
            salpayViewPermission: false,
            label: 'Showing 0-0 of 0 entries',
            filters: {
                start_date: moment().format( DATE_FORMATS.API ),
                end_date: moment().format( DATE_FORMATS.API ),
                company_id: '',
                user: [],
                transaction_type: [],
                module: []
            },
            searchTerm: '',
            displayed_data: [],
            start_date: moment().format( DATE_FORMATS.API ),
            end_date: moment().format( DATE_FORMATS.API ),
            page: 1,
            page_size: 10,
            pages: 0,
            orderBy: 'action_date',
            orderDir: 'desc'
        };

        this.handleSearch = this.handleSearch.bind( this );
        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.onSortingChange = this.onSortingChange.bind( this );
    }
    // eslint-disable-next-line react/sort-comp
    handleTableChanges( tableProps = this.auditTrailFormsTable.tableComponent.state ) {
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }
// Sorting Columns
    onSortingChange( column, additive ) {
    // clone current sorting sorting
        let newSorting = JSON.parse( JSON.stringify( this.auditTrailFormsTable.state.sorting, ( key, value ) => {
            if ( typeof value === 'function' ) {
                return value.toString();
            }
            return value;
        }) );

    // check if already sorted by same column
        const existingIndex = newSorting.findIndex( ( d ) => d.id === column.id );

        let isDesc = false;
        if ( existingIndex > -1 ) {
            const existing = newSorting[ existingIndex ];
            if ( existing.desc ) {
                if ( additive ) {
                    newSorting.splice( existingIndex, 1 );
                } else {
                    existing.desc = false;
                    newSorting = [existing];
                }
            } else {
                existing.desc = true;
                isDesc = true;
                if ( !additive ) {
                    newSorting = [existing];
                }
            }
        } else if ( additive ) {
            newSorting.push({
                id: column.id,
                desc: false
            });
        } else {
            newSorting = [{
                id: column.id,
                desc: false
            }];
        }

        const orderDir = isDesc ? 'desc' : 'asc';

        // Fetch sorted data
        this.props.getAuditTrailDetails(
            this.state.filters,
            this.props.pagination.current_page,
            this.state.page_size,
            column.id,
            orderDir
        );

        // set new sorting states
        this.setState({
            orderBy: column.id,
            orderDir
        });
        this.auditTrailFormsTable.setState({
            sorting: newSorting
        });
        this.auditTrailFormsTable.tableComponent.setState({
            sorting: newSorting
        });
    }
    componentWillMount() {
        this.props.getAuditTrailDetails( this.state.filters );
    }

    componentDidMount() {
        this.props.initializeData();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.auditTrailDetails !== this.props.auditTrailDetails
            && this.setState({
                displayed_data: nextProps.auditTrailDetails,
                pages: Math.ceil( nextProps.totalForms / this.state.page_size )
            });
        setTimeout( () => {
            if ( this.props.pagination.total ) {
                const page = this.props.pagination.current_page || 0;
                const size = this.props.pagination.per_page || 0;
                const dataLength = this.props.pagination.total || 0;
                this.setState({
                    label: `Showing ${dataLength ? ( ( page * size ) + 1 ) - size : 0} - ${( page * size ) > dataLength ? dataLength : ( page * size )} of ${dataLength} entr${dataLength > 1 || dataLength === 0 ? 'ies' : 'y'}`
                });
            }
        });
    }

    /**
     * Handles applying of filters
     * @param {object} filters
     */

    applyFilters = ( filters ) => {
        this.setState({
            filters,
            filtersEnabled: true,
            hasFiltersApplied: true,
            page: 1
        }, () => {
            this.props.getAuditTrailDetails( filters );
        });
        this.toggleFilter();
    }

    /**
     * Resets applied filters
     */
    resetFilters = () => {
        this.setState({
            showFilter: false,
            // displayedData: this.props.employees,
            hasFiltersApplied: false,
            filtersEnabled: false
        });
    }

    /**
     * Clears search input
     */
    clearSearchInput() {
        this.searchInput && this.searchInput.setState({ value: '' });
    }

    /**
     * Dispatches request to fetch audit data
     * @param {object} [filters]
     */
    getAuditTrailDetails = ( filters = this.filter.state ) => {
        const {
            page,
            page_size: pageSize
        } = this.state;

        const appliedFilters = Object.assign({}, filters );
        delete appliedFilters.is_expanded;

        this.props.getAuditTrailDetails({
            ...appliedFilters,
            page,
            limit: pageSize
        });
    }

    /**
     * Toggles filter show state
     */
    toggleFilter = () => {
        this.setState( ( state ) => ({
            showFilter: !state.showFilter
        }) );
    }

    generateAuditFile = () => {
        // dispatch an action to send a request to API
        this.props.generateAuditFile( this.state.filters, this.state.searchTerm );
    }

    /**
     * Handles table page changes
     * @param {number} page
     */
    onPageChange = ( page ) => {
        this.setState({ page });
        this.auditTrailFormsTable.tableComponent.setState({ page });
        this.props.getAuditTrailDetails(
            this.state.filters,
            page,
            this.state.page_size,
            this.state.orderBy,
            this.state.orderDir,
            this.state.searchTerm
        );
    }

    /**
     * Handles table page changes
     * @param {number} page
     */
// To change the size of page
    onPageSizeChange=( pageSize ) => {
        this.setState({ page: 1 });
        this.setState({ page_size: pageSize });
        this.auditTrailFormsTable.tableComponent.setState({ pageSize });

        this.props.getAuditTrailDetails(
            this.state.filters,
            1,
            pageSize,
            this.state.orderBy,
            this.state.orderDir,
            this.state.searchTerm
        );
    }

    /**
     * handles filters and search inputs
     */
    handleSearch = debounce( ( term = '' ) => {
        this.setState({
            searchTerm: term.trim()
        });
        if ( this.state.searchTerm ) {
            this.props.getAuditTrailDetails(
                this.state.filters,
                1,
                this.state.page_size,
                this.state.orderBy,
                this.state.orderDir,
                this.state.searchTerm
            );
        } else {
            this.props.getAuditTrailDetails(
                this.state.filters,
                1,
                this.state.page_size,
                this.state.orderBy,
                this.state.orderDir );
        }
    }, 600 )

    recordsPerPageSelect = ({ value }) => {
        this.setState({ page: 1 });
        this.setState({ page_size: value });
        this.auditTrailFormsTable.tableComponent.setState({ value });

        this.props.getAuditTrailDetails(
            this.state.filters,
            1,
            value,
            this.state.orderBy,
            this.state.orderDir,
            this.state.searchTerm
        );
    }

    pageSelect = ({ value }) => {
        this.setState({ page: value });
        this.auditTrailFormsTable.tableComponent.setState({ value });
        this.props.getAuditTrailDetails(
            this.state.filters,
            value,
            this.state.page_size,
            this.state.orderBy,
            this.state.orderDir,
            this.state.searchTerm
        );
    }

    previous = () => {
        this.setState({
            page: this.state.page - 1
        }, () => {
            const page = this.state.page;
            this.setState({ page });
            this.auditTrailFormsTable.tableComponent.setState({ page });
            this.props.getAuditTrailDetails(
                this.state.filters,
                page,
                this.state.page_size,
                this.state.orderBy,
                this.state.orderDir,
                this.state.searchTerm
            );
        });
    }

    next = () => {
        this.setState({
            page: this.state.page + 1
        }, () => {
            const page = this.state.page;
            this.setState({ page });
            this.auditTrailFormsTable.tableComponent.setState({ page });
            this.props.getAuditTrailDetails(
                this.state.filters,
                page,
                this.state.page_size,
                this.state.orderBy,
                this.state.orderDir,
                this.state.searchTerm
            );
        });
    }

    /**
     * Component Render Method
     */
    render() {
        const { salpayViewPermission } = this.state;
        const hasSelectedItems = isAnyRowSelected( this.auditTrailFormsTable );
        return (
            <main>
                <Helmet
                    title="Audit Trail"
                    meta={ [
                        { name: 'Audit Trail', content: 'Audit Trail' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 7000 }
                    type={ this.props.notification.type }
                />
                {
                    this.props.loading ?
                        <div className="loader">
                            <LoadingStyles>
                                <H2>Loading Audit Trail</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        : (
                            <div>
                                <PageWrapper>
                                    <Container>
                                        <Sidebar
                                            items={ getControlPanelSidebarLinks({
                                                isExpired: this.props.checkExpired,
                                                accountViewPermission: true,
                                                salpayViewPermission,
                                                isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products )
                                            }) }
                                        />
                                        <div className="content">
                                            <br />
                                            <div className="heading">
                                                <H3><b>Audit Trail</b></H3>
                                            </div>
                                            <div className="title">
                                                <div className="search-wrapper">
                                                </div>
                                                <span>
                                                    <div>
                                                        <Button
                                                            id="button-filter"
                                                            label={ <span><Icon name="filter" className="filter-icon" /> Filter</span> }
                                                            type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                                            onClick={ this.toggleFilter }
                                                        />
                                                        &nbsp;
                                                        {/* TODO: Remove export button for now; uncomment later when needed.
                                                        See here: https://chat.ops.salarium.com/salarium/pl/ogutd3epqfrr5kgqcubsxuf5ze */}

                                                        <Button
                                                            className="export_btn"
                                                            label={ <span>Export</span> }
                                                            type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                                            onClick={ this.generateAuditFile }
                                                        />
                                                    </div>
                                                </span>
                                                {hasSelectedItems && (
                                                    <span className="sl-u-gap-left--sm">
                                                        <SalDropdown dropdownItems={ this.getDropdownItems() } />
                                                    </span>
                                                )}
                                            </div>

                                            <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                                                <Filter
                                                    filterData={ this.props.filterData }
                                                    onCancel={ this.resetFilters }
                                                    onApply={ this.applyFilters }
                                                    loading={ this.props.auditTrailTableLoading }
                                                    ref={ ( ref ) => { this.filter = ref; } }
                                                />
                                            </div>
                                            <div className="loading_section">
                                                { this.props.miniLoading &&
                                                    <div className="mini-loading">
                                                        Loading...
                                                    </div>
                                                }
                                                <Table
                                                    className="remove_sort"
                                                    data={ this.state.displayed_data }
                                                    columns={ TABLE_COLUMNS_SIMPLE }
                                                    showPageSizeOptions
                                                    loading={ this.props.auditTrailTableLoading }
                                                    onDataChange={ this.handleTableChanges }
                                                    ref={ ( ref ) => { this.auditTrailFormsTable = ref; } }
                                                    onSelectionChange={ () => {
                                                        this.setSelectedValues();
                                                    } }
                                                    pages={ this.props.pagination.last_page || 1 }
                                                    onPageChange={ this.onPageChange }
                                                    onSortingChange={ this.onSortingChange }
                                                    manual
                                                />
                                            </div>
                                        </div>
                                    </Container>
                                </PageWrapper>
                                <div
                                    className="loader"
                                    style={ { display: this.props.auditTrailTableLoading ? 'none' : '', position: 'fixed', bottom: '0', background: '#fff', paddingBottom: '5px' } }
                                >
                                    <br />
                                    <FooterTablePagination
                                        page={ this.state.page }
                                        pageSize={ this.state.page_size }
                                        pagination={ this.props.pagination }
                                        onPageChange={ this.onPageChange }
                                        onPageSizeChange={ this.onPageSizeChange }
                                        paginationLabel={ this.state.label }
                                    />
                                </div>
                            </div>
                        )
                    }
            </main>
        );
    }
}
const mapStateToProps = createStructuredSelector({
    filterData: makeSelectFilterData(),
    auditTrailTableLoading: makeSelectAuditTrailTableLoading(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    summary: makeSelectSubscriptionSummary(),
    stats: makeSelectSubscriptionStats(),
    checkExpired: makeSelectCheckExpired(),
    auditTrailDetails: makeSelectAuditTrailDetails(),
    pagination: makeSelectPagination(),
    miniLoading: makeSelectMiniLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ControlPanelAuditTrail );
