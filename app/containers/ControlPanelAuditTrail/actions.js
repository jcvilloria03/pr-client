/* eslint-disable no-param-reassign */
/* eslint-disable no-else-return */
import { RESET_STORE } from 'containers/App/constants';
import { company } from 'utils/CompanyService';
import { DATE_FORMATS } from 'utils/constants';
import moment from 'moment/moment';
import {
    INITIALIZE,
    SET_LOADING,
    GET_AUDIT_TRAIL_DATA,
    GENERATE_AUDITFILE,
    SET_ACCOUNT_DETAILS,
    GET_SUBSCRIPTION_SUMMARY,
    SET_SUBSCRIPTION_SUMMARY,
    SET_NOTIFICATION,
    GET_SUBSCRIPTION_STATS,
    SET_SUBSCRIPTION_STATS,
    SET_SUBSCRIBE_LOADING,
    SUBSCRIBE,
    SET_SUBSCRIBE_MODAL,
    SET_PAYMENT_METHOD,
    NOTIFY_USER,
    SET_CLIENT_SECRET,
    REQUEST_SETUP_INTENT_LOADING,
    REQUEST_SETUP_INTENT,
    SET_CARD_MODAL,
    SET_BILLING_INFORMATION
} from './constants';

/**
 * Initialize data
 */

/**
 * Fetch audit trail list
 */
export function initializeData( filters ) {
    return {
        type: INITIALIZE,
        filters
    };
}

/**
 * Sends request to fetch employee government forms
 * @param {Object} payload
 * @param {string} payload.start_date
 * @param {string} payload.end_date
 * @param {number} payload.page
 * @param {number} payload.page_size
 * @param {number} [payload.company_id]
 * @param {number} [payload.user_id]
 * @param {number} [payload.transaction]
 * @param {number} [payload.module_id]
 * @returns {Object} action
 */
/**
 * Generate AuditTrailDetails
 */
export function getAuditTrailDetails( payloadData, page = 1, pageSize = 10,
    orderBy = 'action_date', orderDir = 'desc' ) {
    const companyId = company.getLastActiveCompanyId();
    if ( payloadData.company_id === 0 ) {
        payloadData.company_id = companyId;
    }
    if ( payloadData.start_date === '' ) {
        payloadData.start_date = moment().subtract( 1, 'months' ).format( DATE_FORMATS.API );
    }
    if ( payloadData.end_date === '' ) {
        payloadData.end_date = moment().format( DATE_FORMATS.API );
    }
    if ( payloadData.action_type === undefined ) {
        payloadData.action_type = '';
    }
    if ( payloadData.module_name === undefined ) {
        payloadData.module_name = '';
    }
    if ( payloadData.user_id === undefined ) {
        return {
            type: GET_AUDIT_TRAIL_DATA,
            payload: {
                company_id: payloadData.company_id,
                start_date: payloadData.start_date,
                end_date: payloadData.end_date,
                action_type: payloadData.action_type,
                module_name: payloadData.module_name,
                page,
                page_size: pageSize,
                sort_by: {
                    [ orderBy ]: orderDir
                }
            }
        };
    } else {
        return {
            type: GET_AUDIT_TRAIL_DATA,
            payload: {
                company_id: payloadData.company_id,
                start_date: payloadData.start_date,
                end_date: payloadData.end_date,
                action_type: payloadData.action_type,
                module_name: payloadData.module_name,
                user_id: payloadData.user_id,
                page,
                page_size: pageSize,
                sort_by: {
                    [ orderBy ]: orderDir
                }
            }
        };
    }
}

/**
 * Generate AuditFile
 */
export function generateAuditFile( payloadData, orderBy = 'action_date', orderDir = 'desc' ) {
    const companyId = company.getLastActiveCompanyId();
    if ( payloadData.company_id === 0 ) {
        payloadData.company_id = companyId;
    }
    if ( payloadData.start_date === '' ) {
        payloadData.start_date = moment().subtract( 1, 'months' ).format( DATE_FORMATS.API );
    }
    if ( payloadData.end_date === '' ) {
        payloadData.end_date = moment().format( DATE_FORMATS.API );
    }
    if ( payloadData.action_type === undefined ) {
        payloadData.action_type = '';
    }
    if ( payloadData.module_name === undefined ) {
        payloadData.module_name = '';
    }
    if ( payloadData.user_id === undefined ) {
        return {
            type: GENERATE_AUDITFILE,
            payload: {
                company_id: payloadData.company_id,
                start_date: payloadData.start_date,
                end_date: payloadData.end_date,
                action_type: payloadData.action_type,
                module_name: payloadData.module_name,
                sort_by: {
                    [ orderBy ]: orderDir
                }
            }
        };
    } else {
        return {
            type: GENERATE_AUDITFILE,
            payload: {
                company_id: payloadData.company_id,
                start_date: payloadData.start_date,
                end_date: payloadData.end_date,
                action_type: payloadData.action_type,
                module_name: payloadData.module_name,
                user_id: payloadData.user_id,
                sort_by: {
                    [ orderBy ]: orderDir
                }
            }
        };
    }
}

/**
 * Set loading status of the page
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets account details
 * @param {Object} payload - Account details
 * @returns {Object} action
 */
export function setAccountDetails( payload ) {
    return {
        type: SET_ACCOUNT_DETAILS,
        payload
    };
}

/**
 * Request to fetch subscription details
 * @returns {Object} action
 */
export function getSubscriptionDetails() {
    return {
        type: GET_SUBSCRIPTION_SUMMARY
    };
}

/**
 * Sets subscription details from API
 * @param {Object} payload - Subscription details
 * @returns {Object} action
 */
export function setSubscriptionSummary( payload ) {
    return {
        type: SET_SUBSCRIPTION_SUMMARY,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Request to fetch subscription stats
 * @returns {Object} action
 */
export function getSubscriptionStats() {
    return {
        type: GET_SUBSCRIPTION_STATS
    };
}

/**
 * Sets subscription stats from API
 * @param {Object} payload - Subscription stats
 * @returns {Object} action
 */
export function setSubscriptionStats( payload ) {
    return {
        type: SET_SUBSCRIPTION_STATS,
        payload
    };
}

/**
 * Set loading status of the subscription
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setSubscribeLoading( payload ) {
    return {
        type: SET_SUBSCRIBE_LOADING,
        payload
    };
}

/**
 * Subscribe
 */
export function subscribe( payload ) {
    return {
        type: SUBSCRIBE,
        payload
    };
}

/**
 * Set Subscribe Modal
 */
export function setSubscribeModal( payload ) {
    return {
        type: SET_SUBSCRIBE_MODAL,
        payload
    };
}

/**
 * Sets current payment method
 * @param {Object} payload - Payment method
 * @returns {Object} action
 */
export function setPaymentMethod( payload ) {
    return {
        type: SET_PAYMENT_METHOD,
        payload
    };
}

/**
 * Notify User
 */
export function notifyUser( payload ) {
    return {
        type: NOTIFY_USER,
        payload
    };
}

/**
 * Request Setup Intent
 */
export function requestSetupIntent() {
    return {
        type: REQUEST_SETUP_INTENT
    };
}

/**
 * Request Setup Intent Loading state
 */
export function requestSetupIntentLoading( payload ) {
    return {
        type: REQUEST_SETUP_INTENT_LOADING,
        payload
    };
}

/**
 * Set client secret
 */
export function setClientSecret( payload ) {
    return {
        type: SET_CLIENT_SECRET,
        payload
    };
}

/**
 * Set card modal
 */
export function setCardModal( payload ) {
    return {
        type: SET_CARD_MODAL,
        payload
    };
}

/**
 * Sets billing information
 * @param {Object} payload - Billing information
 * @returns {Object} action
 */
export function setBillingInformation( payload ) {
    return {
        type: SET_BILLING_INFORMATION,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
