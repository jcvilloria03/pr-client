import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'controlPanelAuditTrail' );

/**
 * Default selector used by Subscription
 */

const makeSelectAuditTrailTableLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'audit_trail_table_loading' )
);

const makeSelectFilterData = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'filterData' ).toJS()
);

const makeSelectSubscriptionSummary = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'summary' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectSubscriptionStats = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'stats' ).toJS()
);

const makeSelectAccountDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'account_details' ).toJS()
);

const makeSelectSuscribeLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'subscribe_loading' )
);

const makeSelectSubscribeModal = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'subscribe_modal' )
);

const makeSelectPaymentMethod = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'payment_method' ).toJS()
);

const makeSelectClientSecret = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'client_secret' )
);

const makeSelectSetupIntentLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'setup_intent_loading' )
);

const makeSelectCardModal = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'card_modal' )
);

const makeSelectBillingInformation = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'billing_information' ).toJS()
);

const makeSelectGenerateAuditFile = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'generateAuditFile' )
);

const makeSelectCheckExpired = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'checkExpired' )
);

const makeSelectAuditTrailDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'audit_trail_details' )
);

const makeSelectPagination = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'pagination' )
);
const makeSelectMiniLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'miniLoading' )
);

export {
    makeSelectAuditTrailTableLoading,
    makeSelectFilterData,
    makeSelectGenerateAuditFile,
    makeSelectSubscriptionSummary,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSubscriptionStats,
    makeSelectAccountDetails,
    makeSelectSuscribeLoading,
    makeSelectSubscribeModal,
    makeSelectPaymentMethod,
    makeSelectClientSecret,
    makeSelectSetupIntentLoading,
    makeSelectCardModal,
    makeSelectBillingInformation,
    makeSelectCheckExpired,
    makeSelectAuditTrailDetails,
    makeSelectPagination,
    makeSelectMiniLoading
};
