import { fromJS } from 'immutable';
import { formatNumber } from 'utils/functions';
import { subscriptionService } from 'utils/SubscriptionService';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_FILTER_DATA,
    GENERATE_AUDITFILE,
    SET_ACCOUNT_DETAILS,
    SET_SUBSCRIPTION_SUMMARY,
    SET_NOTIFICATION,
    SET_SUBSCRIPTION_STATS,
    SET_SUBSCRIBE_LOADING,
    SET_SUBSCRIBE_MODAL,
    SET_PAYMENT_METHOD,
    SET_CLIENT_SECRET,
    REQUEST_SETUP_INTENT_LOADING,
    SET_CARD_MODAL,
    SET_BILLING_INFORMATION,
    CHECK_EXPIRED,
    SET_AUDIT_TRAIL_DATA,
    SET_AUDIT_TRAIL_PAGINATION,
    SET_MINI_LOADING
} from './constants';

const initialState = fromJS({
    loading: true,
    filterData: {},
    account_details: {},
    summary: {
        is_trial: true
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    },
    stats: {},
    subscribe_loading: false,
    subscribe_modal: false,
    payment_method: {},
    client_secret: null,
    setup_intent_loading: false,
    card_modal: false,
    billing_information: {},
    adjustments: [],
    pagination: {
        from: 1,
        to: 1,
        total: 1,
        current_page: 0,
        last_page: 1,
        per_page: 10
    },
    checkExpired: true,
    audit_trail_details: [],
    miniLoading: true
});

/**
 * Parses subscription summary details from API response
 * @param {*} data
 * @returns {Object}
 */
function parseSubscriptionSummary( data ) {
    const [subscription] = data.subscriptions;
    subscriptionService.updateSubscription( subscription );

    const mainPlan = subscription.subscription_licenses.find(
        ( license ) => license.product.id === subscription.main_product_id
    ) || subscription.subscription_licenses[ 0 ];

    return {
        id: subscription.id,
        owner_id: subscription.user_id,
        is_trial: subscription.is_trial,
        is_expired: subscription.is_expired,
        start_date: subscription.start_date,
        end_date: subscription.end_date,
        days_left: subscription.days_left,
        product: {
            id: mainPlan.product.id,
            name: mainPlan.product.name,
            total_licenses_assigned: mainPlan.units,
            currency: mainPlan.product.currency,
            price: mainPlan.product.price
        }
    };
}

/**
 * Parses subsciption stats from API response
 * @param {Object} data
 * @returns {Object}
 */
function parseSubscriptionStats( data ) {
    return {
        ...data,
        current_setup_price: `${formatNumber( data.products.reduce( ( price, product ) => price + product.amount, 0 ), true, 0 )} ${data.products[ 0 ].currency}`
    };
}

/**
 *
 * Subscription reducer
 *)
 */
function auditTrailReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_MINI_LOADING:
            return state.set( 'miniLoading', action.payload );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case GENERATE_AUDITFILE:
            return state.set( 'generateAuditFile', action.payload );
        case SET_ACCOUNT_DETAILS:
            return state.set( 'account_details', fromJS( action.payload ) );
        case SET_SUBSCRIPTION_SUMMARY:
            return state.set( 'summary', fromJS( parseSubscriptionSummary( action.payload ) ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_SUBSCRIPTION_STATS:
            return state.set( 'stats', fromJS( parseSubscriptionStats( action.payload ) ) );
        case SET_SUBSCRIBE_LOADING:
            return state.set( 'subscribe_loading', action.payload );
        case SET_SUBSCRIBE_MODAL:
            return state.set( 'subscribe_modal', action.payload );
        case SET_PAYMENT_METHOD:
            return state.set( 'payment_method', fromJS( action.payload ) );
        case SET_CLIENT_SECRET:
            return state.set( 'client_secret', action.payload );
        case REQUEST_SETUP_INTENT_LOADING:
            return state.set( 'setup_intent_loading', action.payload );
        case SET_CARD_MODAL:
            return state.set( 'card_modal', action.payload );
        case SET_BILLING_INFORMATION:
            return state.set( 'billing_information', fromJS( action.payload ) );
        case CHECK_EXPIRED:
            return state.set( 'checkExpired', fromJS( action.payload ) );
        case SET_AUDIT_TRAIL_DATA:
            return state.set( 'audit_trail_details', action.payload.data.logs );
        case SET_AUDIT_TRAIL_PAGINATION:
            return state.set( 'pagination', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default auditTrailReducer;
