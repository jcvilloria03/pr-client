/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign, radix */

import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { Fetch } from 'utils/request';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';
import { company } from 'utils/CompanyService';
import { DATE_FORMATS } from 'utils/constants';
import moment from 'moment/moment';
import {
    INITIALIZE,
    GET_AUDIT_TRAIL_DATA,
    PAGE_STATUSES,
    SET_AUDIT_TRAIL_DATA,
    SET_FILTER_DATA,
    SET_PAGE_STATUS,
    TRANSACTION_TYPES,
    LOADING,
    SET_LOADING,
    SET_NOTIFICATION,
    GENERATE_AUDITFILE,
    CHECK_EXPIRED,
    SET_MINI_LOADING,
    SET_AUDIT_TRAIL_PAGINATION
} from './constants';

/**
 * Initialize data
 */
export function* initializeData() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.LOADING
        });
        const companyId = company.getLastActiveCompanyId();
        const [ companies, users, transactionTypes, modules ] = yield [
            call( Fetch, '/account/philippine/companies', { method: 'GET' }),
            call( Fetch, `/account/allusers?mode=FILTER_MODE&company_id=${companyId}&keyword=`, { method: 'GET' }),
            TRANSACTION_TYPES,
            call( Fetch, '/audit-trail/modules', { method: 'GET' })
        ];

        const moduleData = [];
        modules.map(
            ( x ) => moduleData.push( JSON.parse( `{"id": "${x}", "name": "${x}"}` ) )
        );

        moduleData.unshift( JSON.parse( '{"id": "", "name": "All modules"}' ) );

        const usersData = [];
        users.map( ( x ) => usersData.push( JSON.parse( `{"id": "${x.id}", "name": "${x.name}"}` ) ) );
        usersData.unshift( JSON.parse( '{"id": "", "name": "All Users"}' ) );

        const companiesData = [];
        companies.data.map(
            ( x ) => companiesData.push( JSON.parse( `{"id": "${x.id}", "name": "${x.name}"}` ) )
        );
        companiesData.unshift( JSON.parse( '{"id": "", "name": "All Companies" }' ) );

        const filterData = {
            companies: companiesData,
            users: usersData,
            transactionTypes,
            modules: moduleData
        };

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });

        const response = yield call( Fetch, '/account', { method: 'GET' });
        const checkExpired = response.subscriptions[ 0 ].is_expired;
        yield put({
            type: CHECK_EXPIRED,
            payload: checkExpired
        });
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });

        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.READY
        });
    }
}

/**
 * Sends request to fetch employee government forms
 * @param {Object} payload
 * @param {string} payload.company_id
 * @param {string} payload.start_date
 * @param {number} payload.end_date
 * @param {number} [payload.page]
 * @param {number} [payload.company_id]
 * @param {number} [payload.user_id]
 * @param {number} [payload.transaction]
 * @param {number} [payload.module_id]
 */
export function* getAuditTrailDetails({ payload }) {
    const { filter, page = 1, perPage = 10,
        orderBy = 'action_date', orderDir = 'desc', searchKeyword = ''
    } = payload;

    if ( payload.company_id === '' || payload.company_id === undefined ) {
        delete payload.company_id;
    } else {
        payload.company_id = parseInt( payload.company_id );
        payload;
    }
    if ( payload.module_name === '' || payload.module_name === undefined ) {
        delete payload.module_name;
    } else {
        payload;
    }
    if ( payload.action_type === '' || payload.action_type === undefined ) {
        delete payload.action_type;
    } else {
        payload;
    }
    if ( payload.user_id === '' || payload.user_id === undefined ) {
        delete payload.user_id;
    } else {
        payload.user_id = parseInt( payload.user_id );
        payload;
    }

    yield put({
        type: SET_PAGE_STATUS,
        payload: PAGE_STATUSES.LOADING
    });

    try {
        yield put({
            type: SET_MINI_LOADING,
            payload: true
        });
        if ( payload.company_id === 0 ) {
            payload.company_id = payload;
        }
        if ( payload.start_date === '' ) {
            payload.start_date = moment().subtract( 1, 'months' ).format( DATE_FORMATS.API );
        }
        if ( payload.end_date === '' ) {
            payload.end_date = moment().format( DATE_FORMATS.API );
        }

        const auditData = yield call( Fetch, '/audit-trail/logs', {
            method: 'POST',
            data: payload
        });

        yield put({
            type: SET_AUDIT_TRAIL_DATA,
            payload: auditData
        });

        let endPage = 0;
        let tmpLastPage = 0;
        let tmpModPage = 0;
        tmpLastPage = parseInt( ( auditData.data.total / auditData.data.page_size ), 10 );
        tmpModPage = auditData.data.total % auditData.data.page_size;
        endPage = ( tmpModPage === 0 ) ? tmpLastPage : tmpLastPage + 1;

        const paginationData = {
            from: auditData.data.page || 1,
            to: endPage || 1,
            total: auditData.data.total || 1,
            current_page: auditData.data.page || 1,
            last_page: endPage || 1,
            per_page: auditData.data.page_size
        };

        yield put({
            type: SET_AUDIT_TRAIL_PAGINATION,
            payload: paginationData
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_MINI_LOADING,
            payload: false
        });

        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.READY
        });
    }
}

/**
 * Generate Auditfile
 */
export function* generateAuditFile({ payload, page = 1, pageSize = 10, orderBy = 'action_date', orderDir = 'desc' }) {
    let payloadData;
    yield put({
        type: LOADING,
        payload: true
    });
    try {
        // Generate file
        const companyId = company.getLastActiveCompanyId();
        if ( payload.company_id === '' || payload.company_id === undefined ) {
            delete payload.company_id;
        } else {
            payload.company_id = parseInt( payload.company_id );
            payload;
        }
        if ( payload.start_date === '' ) {
            payload.start_date = moment().subtract( 1, 'months' ).format( DATE_FORMATS.API );
        }
        if ( payload.end_date === '' ) {
            payload.end_date = moment().format( DATE_FORMATS.API );
        }
        if ( payload.action_type === undefined ) {
            payload.action_type = '';
        }
        if ( payload.module_name === undefined ) {
            payload.module_name = '';
        }
        if ( payload.user_id === undefined ) {
            payloadData = {
                company_id: parseInt( payload.company_id ),
                start_date: payload.start_date,
                end_date: payload.end_date,
                action_type: payload.action_type,
                module_name: payload.module_name,
                current_company_id: parseInt( companyId ),
                sort_by: {
                    [ orderBy ]: orderDir
                }
            };
        } else {
            payloadData = {
                company_id: parseInt( payload.company_id ),
                start_date: payload.start_date,
                end_date: payload.end_date,
                action_type: payload.action_type,
                module_name: payload.module_name,
                user_id: parseInt( payload.user_id ),
                current_company_id: parseInt( companyId ),
                sort_by: {
                    [ orderBy ]: orderDir
                }
            };
        }
        const response = yield call( Fetch, '/audit-trail/logs/export', {
            method: 'POST',
            data: payloadData
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Your audit trail list is being prepared. We will send a download link to your registered email address once the download process is complete.',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: LOADING,
            payload: false
        });
    }
    yield put({
        type: LOADING,
        payload: false
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
    yield call( initializeData );
}

/**
 * Watcher for INITIALIZE_DATA
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_AUDIT_TRAIL_DATA
 */
export function* watchForGetAuditTrailDetails() {
    const watcher = yield takeEvery( GET_AUDIT_TRAIL_DATA, getAuditTrailDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Generate Masterfile
 */
export function* watchForGenerateAuditFile() {
    const watcher = yield takeEvery( GENERATE_AUDITFILE, generateAuditFile );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForReinitializePage,
    watchForGetAuditTrailDetails,
    watchForGenerateAuditFile
];
