/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable array-callback-return */
/* eslint-disable default-case */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-useless-escape */
/* eslint-disable no-unused-vars */
/* eslint-disable no-lonely-if */
/*
 *
 * Subscription constants
 *
 */
import React from 'react';
import { DATE_FORMATS } from 'utils/constants';
import moment from 'moment/moment';
import TableRowData from '../../utils/TableRowData';
const namespace = 'app/ControlPanelAuditTrail';
export const INITIALIZE = `${namespace}/INITIALIZE`;
export const SET_FILTER_DATA = `${namespace}/SET_FILTER_DATA`;
export const PAGE_STATUSES = {
    LOADING: 'LOADING',
    REGENERATING: 'REGENERATING',
    DOWNLOADING: 'DOWNLOADING',
    READY: 'READY'
};
export const GET_AUDIT_TRAIL_DATA = `${namespace}/GET_AUDIT_TRAIL_DATA`;
export const SET_AUDIT_TRAIL_DATA = `${namespace}/SET_AUDIT_TRAIL_DATA`;
export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;
export const LOADING = `${namespace}/LOADING`;
export const SET_DOWNLOAD_URL = `${namespace}/SET_DOWNLOAD_URL`;
export const GENERATE_AUDITFILE = `${namespace}/GENERATE_AUDITFILE`;
export const CHECK_EXPIRED = `${namespace}/CHECK_EXPIRED`;
export const SET_AUDIT_TRAIL_PAGINATION = `${namespace}/SET_AUDIT_TRAIL_PAGINATION`;
export const TABLE_COLUMNS_SIMPLE = [
    {
        header: 'Modification Date',
        id: 'action_date',
        minWidth: 150,
        accessor: ( row ) => (
            row.action_date ? moment( row.action_date ).format( DATE_FORMATS.DEFAULT_SHIFT_DATETIME ) : '-'
        )
    },
    {
        header: 'Company',
        id: 'company_id',
        minWidth: 200,
        accessor: ( row ) => (
            // Code for data from API
            row.details
            ? dataParse( row.details, 'company' )
            : '-'
        )
    },
    {
        header: 'Type',
        id: 'action_type',
        minWidth: 100,
        accessor: ( row ) => (
            row.action_type ? capitalizeFirstLetter( row.action_type ) : '-'
        )
    },
    {
        header: 'User',
        id: 'user_id',
        minWidth: 150,
        accessor: ( row ) => (
            // Code for test data
            /*
            row.details.user.name ? row.details.user.name : '-'
            */
            // Code for data from API
            row.details
            ? dataParse( row.details, 'user' )
            : '-'
        )
    },
    {
        header: 'Module',
        id: 'module_name',
        minWidth: 150,
        accessor: ( row ) => (
            row.module_name ? row.module_name : '-'
        )
    },
    {
        header: 'Original Value',
        id: 'old_value',
        minWidth: 300,
        accessor: ( row ) => (
            dataParse( row.old_value, 'old' )
        ),
        render: ( rowInfo ) => (
            rowInfo.value ?
                <TableRowData data={ rowInfo.value } />
            :
                ''
        ),
        style: { whiteSpace: 'pre' }
    },
    {
        header: 'Updated Value',
        id: 'new_value',
        minWidth: 300,
        accessor: ( row ) => (
            dataParse( row.new_value, 'new' )
        ),
        render: ( rowInfo ) => (
            rowInfo.value ?
                <TableRowData data={ rowInfo.value } />
            :
                ''
        ),
        style: { whiteSpace: 'pre' }
    }
];

/**
 * Convert string with uppercase first letter
 */
function capitalizeFirstLetter( string ) {
    const stringStd = string.toLowerCase();
    return stringStd.charAt( 0 ).toUpperCase() + stringStd.slice( 1 );
}

/**
 * Convert data human readable characters
 */
function dataParse( jsonStr, keyId ) {
    let resultData = '';
    let parseData = jsonStr;
    if ( ( typeof jsonStr === 'object' && jsonStr === null ) ||
        ( typeof jsonStr === 'string' && jsonStr === 'null' ) ) {
        resultData = '-';
    } else {
        if ( typeof jsonStr === 'string' ) {
            parseData = JSON.parse( jsonStr );
        }
        switch ( keyId ) {
            case 'company':
                resultData = parseData.company.name;
                break;
            case 'user':
                resultData = parseData.user.name;
                break;
            case 'old':
            case 'new':
                resultData = organize( parseData );
                break;
        }
    }

    return resultData;
}

/**
 * Organize readability in table cell
 */
function organizeTest( obj ) {
    let resStr = '';
    let tempStr = '';

    const result = Object.keys( obj ).map( ( key ) => [ String( key ), obj[ key ] ]);
    result.map( ( items ) => {
        items.map( ( item, key ) => {
            if ( key === 0 ) {
                tempStr = item;
                if ( tempStr.includes( 'id' ) === false &&
                    tempStr.includes( '_at' ) === false &&
                    tempStr.includes( 'position' ) === false ) {
                    if ( resStr.length === 0 ) {
                        resStr = `${humanize( item )}: `;
                    } else {
                        resStr = `${resStr}\r\n${humanize( item )}: `;
                    }
                }
            } else if ( tempStr.includes( 'params' ) === true ) {
                if ( item != null && typeof item === 'object' ) {
                    const presult = Object.keys( item ).map( ( key2 ) => [ String( key2 ), item[ key2 ] ]);
                    presult.map( ( pitems ) => {
                        pitems.map( ( pitem, key2 ) => {
                            if ( key2 === 0 ) {
                                tempStr = pitem;
                                if ( tempStr.includes( 'id' ) === false &&
                                    tempStr.includes( '_at' ) === false ) {
                                    if ( resStr.length === 0 ) {
                                        resStr = `${humanize( pitem )}: `;
                                    } else {
                                        resStr = `${resStr}\r\n\t${humanize( pitem )}: `;
                                    }
                                }
                            } else if ( tempStr.includes( 'id' ) === false &&
                                tempStr.includes( '_at' ) === false ) {
                                if ( item != null && typeof item === 'object' ) {
                                    const presult2 = Object.keys( pitem ).map( ( key3 ) => [ String( key3 ), pitem[ key3 ] ]);
                                    presult2.map( ( pitems2 ) => {
                                        pitems2.map( ( pitem2, key3 ) => {
                                            if ( key3 === 0 ) {
                                                tempStr = pitem2;
                                                if ( tempStr.includes( 'id' ) === false &&
                                                    tempStr.includes( '_at' ) === false ) {
                                                    if ( resStr.length === 0 ) {
                                                        resStr = `${humanize( pitem2 )}: `;
                                                    } else {
                                                        resStr = `${resStr}\r\n\t\t${humanize( pitem2 )}: `;
                                                    }
                                                }
                                            } else if ( tempStr.includes( 'id' ) === false &&
                                                tempStr.includes( '_at' ) === false ) {
                                                resStr += humanize( pitem2 );
                                            }
                                        });
                                    });
                                } else if ( tempStr.includes( 'id' ) === false &&
                                    tempStr.includes( '_at' ) === false ) {
                                    resStr += humanize( pitem );
                                }
                            }
                        });
                    });
                }
            } else if ( tempStr.includes( 'id' ) === false &&
                tempStr.includes( '_at' ) === false &&
                tempStr.includes( 'position' ) === false ) {
                resStr += humanize( item );
            }
        });
    });

    return resStr;
}

/**
 * Organize readability in table cell
 */
function organizeTest2( obj ) {
    let resStr = '';
    let obj2;
    let obj3;
    for ( const key in obj ) {
        if ( key.includes( 'params' ) === false ) {
            if ( key.includes( 'id' ) === false &&
                key.includes( '_at' ) === false &&
                key.includes( 'position' ) === false ) {
                if ( resStr.length === 0 ) {
                    resStr = `${humanize( key )}: ${humanize( obj[ key ])}`;
                } else {
                    resStr = `${resStr}\r\n${humanize( key )}: ${humanize( obj[ key ])}`;
                }
            }
        } else if ( key.includes( 'params' ) === true ) {
            obj2 = obj[ key ];
            if ( obj2 != null && typeof obj2 === 'object' ) {
                if ( resStr.length === 0 ) {
                    resStr = `${humanize( key )}: `;
                } else {
                    resStr = `${resStr}\r\n${humanize( key )}: `;
                }
                for ( const key2 in obj2 ) {
                    if ( obj2[ key2 ] != null && typeof obj2[ key2 ] === 'object' ) {
                        if ( resStr.length === 0 ) {
                            resStr = `${humanize( key2 )}: `;
                        } else {
                            resStr = `${resStr}\r\n\t${humanize( key2 )}: `;
                        }
                        obj3 = obj2[ key2 ];
                        for ( const key3 in obj3 ) {
                            if ( key3.includes( 'id' ) === false &&
                                key3.includes( '_at' ) === false ) {
                                if ( resStr.length === 0 ) {
                                    resStr = `${humanize( key3 )}: ${humanize( obj3[ key3 ])}`;
                                } else {
                                    resStr = `${resStr}\r\n\t\t${humanize( key3 )}: ${humanize( obj3[ key3 ])}`;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return resStr;
}

/**
 * Convert data human readable characters
 */
function humanize( str ) {
    let i;
    if ( str !== null ) {
        const frags = str.split( '_' );
        for ( i = 0; i < frags.length; i += 1 ) {
            frags[ i ] = frags[ i ].charAt( 0 ).toUpperCase() + frags[ i ].slice( 1 );
        }
        return frags.join( ' ' );
    }

    return 'null';
}

/**
 * Organize readability in table cell
 */
function organizeObject( obj ) {
    let resStr = '';
    let tempStr = '';
    let tmpKey = 0;
    let boolIdChecker = false;
    let boolHumanizeChecker = true;
    let tempVIStr = '';

    const result = Object.keys( obj ).map( ( keys ) => [ String( keys ), obj[ keys ] ]);
    result.map( ( items, keys ) => {
        tmpKey = keys;
        boolIdChecker = false;
        boolHumanizeChecker = true;
        items.map( ( item, key ) => {
            if ( item !== null && typeof item !== 'string' ) {
                tempStr = item.toString();
            } else if ( item !== null ) {
                tempStr = item;
            } else {
                tempStr = '';
            }
            if ( key === 0 ) {
                if ( tempStr.includes( 'id' ) === true ||
                    tempStr.includes( '_at' ) === true ||
                    tempStr.includes( 'position' ) === true ) {
                    boolIdChecker = true;
                }
                if ( tempStr.includes( 'email' ) === true ) {
                    boolHumanizeChecker = false;
                }
                if ( boolIdChecker === false ) {
                    if ( resStr.length === 0 ) {
                        resStr = `${humanize( tempStr )}: `;
                    } else {
                        resStr = `${resStr}\r\n${humanize( tempStr )}: `;
                    }
                }
            } else {
                if ( item != null && typeof item === 'object' ) {
                    const items2 = Object.keys( item ).map( ( keys2 ) => [ String( keys2 ), item[ keys2 ] ]);
                    items2.map( ( item2, key2 ) => {
                        boolIdChecker = false;
                        const item2List = Object.entries( item2 ).map( ([ keyInner, valueInner ]) => {
                            if ( Object.prototype.toString.call( valueInner ) === '[object Object]' ) {
                                const items3 = Object.keys( valueInner ).map( ( keys3 ) => [ String( keys3 ), valueInner[ keys3 ] ]);
                                items3.map( ( item3, key3 ) => {
                                    boolIdChecker = false;
                                    const item3List = Object.entries( item3 ).map( ([ keyInner2, valueInner2 ]) => {
                                        if ( Object.prototype.toString.call( valueInner2 ) === '[object Object]' ) {
                                            const items4 = Object.keys( valueInner2 ).map( ( keys4 ) => [ String( keys4 ), valueInner2[ keys4 ] ]);
                                            items4.map( ( item4, key4 ) => {
                                                boolIdChecker = false;
                                                item4.map( ( item5, key5 ) => {
                                                    if ( item5 !== null ) {
                                                        tempStr = item5.toString();
                                                        if ( tempStr.includes( 'id' ) === true ) {
                                                            boolIdChecker = true;
                                                        }
                                                        if ( key5 === 0 ) {
                                                            if ( boolIdChecker === false ) {
                                                                resStr += `\r\n\t\t${humanize( tempStr )}: `;
                                                            }
                                                        } else {
                                                            if ( boolIdChecker === false ) {
                                                                resStr += humanize( tempStr );
                                                            }
                                                        }
                                                    }
                                                });
                                            });
                                        } else {
                                            if ( valueInner2 !== null ) {
                                                tempStr = valueInner2.toString();
                                                if ( tempStr.includes( 'id' ) === true ||
                                                    tempStr.includes( 'timezone' ) === true ||
                                                    tempStr.includes( 'date' ) === true ) {
                                                    boolIdChecker = true;
                                                }
                                                if ( keyInner2 === '0' ) {
                                                    if ( boolIdChecker === false ) {
                                                        resStr += `\r\n\t${humanize( tempStr )}: `;
                                                    }
                                                } else {
                                                    if ( boolIdChecker === false ) {
                                                        resStr += humanize( tempStr );
                                                    }
                                                }
                                            }
                                        }
                                    });
                                });
                            } else {
                                if ( valueInner != null && typeof valueInner !== 'string' ) {
                                    tempVIStr = valueInner.toString();
                                } else if ( valueInner === null ) {
                                    tempVIStr = '';
                                } else {
                                    tempVIStr = valueInner;
                                }
                                if ( tempVIStr.includes( 'id' ) === true ||
                                    tempVIStr.includes( 'timezone' ) === true ||
                                    tempVIStr.includes( 'date' ) === true ) {
                                    boolIdChecker = true;
                                }
                                if ( keyInner === '0' ) {
                                    if ( boolIdChecker === false ) {
                                        if ( tempVIStr !== '0' ) {
                                            resStr += `\r\n\t${humanize( tempVIStr )}: `;
                                        }
                                    }
                                } else {
                                    if ( boolIdChecker === false ) {
                                        if ( tempVIStr !== '0' ) {
                                            resStr += humanize( tempVIStr );
                                        }
                                    }
                                }
                            }
                        });
                    });
                } else {
                    if ( boolIdChecker === false ) {
                        if ( boolHumanizeChecker === false ) {
                            resStr += tempStr;
                        } else {
                            resStr += humanize( tempStr );
                        }
                    }
                }
            }
        });
    });

    return resStr;
}

/**
 * Organize readability in table cell
 */
function organize( obj ) {
    let resStr = '';

    if ( Object.prototype.toString.call( obj ) === '[object Object]' ) {
        resStr = organizeObject( obj );
    } else if ( Object.prototype.toString.call( obj ) === '[object Array]' ) {
        resStr = organizeObjectArray( obj );
    }

    return resStr;
}

/**
 * Organize readability in table cell
 */
function organizeObjectArray( obj ) {
    let resStr = '';
    let tmpStr = '';
    let tmpKey = 0;
    let arrSplit;
    const deepData = [];
    const deepDataItem = [];
    let boolIdChecker = false;

    obj.map( ( obj2 ) => {
        const items = Object.keys( obj2 ).map( ( keys ) => [ String( keys ), obj2[ keys ] ]);
        items.map( ( item, key ) => {
            tmpKey = key;
            boolIdChecker = false;
            item.map( ( item2, key2 ) => {
                if ( item2 !== null && typeof item2 === 'object' ) {
                    const items2 = Object.keys( item2 ).map( ( keys2 ) => [ String( keys2 ), item2[ keys2 ] ]);
                    items2.map( ( item3, key3 ) => {
                        if ( item3 !== null && typeof item3 === 'object' ) {
                            tmpStr = item3.toString();
                            if ( tmpKey === key ) {
                                if ( key3 === 0 ) {
                                    if ( tmpStr.includes( ',' ) ) {
                                        arrSplit = tmpStr.split( ',' );
                                        if ( arrSplit[ 0 ].length > 1 ) {
                                            resStr = `${resStr}\r\n\t`;
                                            resStr = `${resStr}${humanize( arrSplit[ 0 ])}: ${humanize( arrSplit[ 1 ])}`;
                                        } else {
                                            const items3 = Object.keys( item3 ).map( ( keys3 ) => [ String( keys3 ), item3[ keys3 ] ]);
                                            items3.map( ( item4, key4 ) => {
                                                if ( key4 === 1 ) {
                                                    const item4List = Object.entries( item4 ).map( ([ keyInner, valueInner ]) => {
                                                        deepData.push( valueInner );
                                                    });
                                                    const items4 = Object.keys( deepData[ 1 ]).map( ( keys4 ) => [ String( keys4 ), deepData[ 1 ][ keys4 ] ]);
                                                    items4.map( ( item5, key5 ) => {
                                                        deepDataItem.push( item5 );
                                                    });
                                                    if ( tmpKey === key ) {
                                                        const deepDataList = Object.entries( deepDataItem ).map( ([ itemKey, itemValue ]) => {
                                                            const deepDataList2 = Object.entries( itemValue ).map( ([ itemKey2, itemValue2 ]) => {
                                                                if ( itemValue2 !== null && typeof itemValue2 !== 'object' ) {
                                                                    if ( tmpKey === key ) {
                                                                        tmpStr = itemValue2.toString();
                                                                        if ( itemKey2 === '0' ) {
                                                                            resStr = `${resStr}\r\n\t`;
                                                                            resStr = `${resStr}${humanize( tmpStr )}: `;
                                                                        } else {
                                                                            resStr = `${resStr}${humanize( tmpStr )}`;
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    } else {
                                        if ( key3 === 0 ) {
                                            resStr = `${resStr}${humanize( tmpStr )}: `;
                                        } else {
                                            resStr = `${resStr}${humanize( tmpStr )}`;
                                        }
                                    }
                                }
                            }
                        } else {
                            resStr = `${resStr}\r\n\t`;
                            tmpStr = item3;
                            if ( tmpKey === key ) {
                                if ( key3 === 0 ) {
                                    resStr = `${resStr}${humanize( tmpStr )}: `;
                                } else {
                                    resStr = `${resStr}${humanize( tmpStr )}`;
                                }
                            }
                        }
                    });
                } else {
                    if ( item2 !== null && typeof item2 !== 'string' ) {
                        tmpStr = item2.toString();
                    } else if ( item2 !== null ) {
                        tmpStr = item2;
                    } else {
                        tmpStr = '';
                    }
                    if ( tmpKey === key ) {
                        if ( tmpStr.includes( 'id' ) === true ) {
                            boolIdChecker = true;
                        } else {
                            if ( key2 === 0 ) {
                                if ( resStr.length !== 0 ) {
                                    resStr = `${resStr}\r\n`;
                                }
                                resStr = `${resStr}${humanize( tmpStr )}: `;
                            } else {
                                if ( boolIdChecker === false ) {
                                    resStr = `${resStr}${humanize( tmpStr )}`;
                                }
                            }
                        }
                    }
                }
            });
        });
    });

    return resStr;
}

export const TRANSACTION_TYPES = [
    {
        id: '',
        name: 'All Transaction Types'
    },
    {
        id: 'view',
        name: 'View'
    },
    {
        id: 'add',
        name: 'Add'
    },
    {
        id: 'update',
        name: 'Update'
    },
    {
        id: 'delete',
        name: 'Delete'
    }
];

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_ACCOUNT_DETAILS = `${namespace}/SET_ACCOUNT_DETAILS`;
export const GET_SUBSCRIPTION_SUMMARY = `${namespace}/GET_SUBSCRIPTION_SUMMARY`;
export const SET_SUBSCRIPTION_SUMMARY = `${namespace}/SET_SUBSCRIPTION_SUMMARY`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const GET_SUBSCRIPTION_STATS = `${namespace}/GET_SUBSCRIPTION_STATS`;
export const SET_SUBSCRIPTION_STATS = `${namespace}/SET_SUBSCRIPTION_STATS`;
export const GET_LICENSE_UNITS = `${namespace}/GET_LICENSE_UNITS`;

export const SUBSCRIBE = `${namespace}/SUBSCRIBE`;
export const SET_SUBSCRIBE_LOADING = `${namespace}/SET_SUBSCRIBE_LOADING`;
export const SET_SUBSCRIBE_MODAL = `${namespace}/SET_SUBSCRIBE_MODAL`;
export const REQUEST_SETUP_INTENT = `${namespace}/REQUEST_SETUP_INTENT`;
export const REQUEST_SETUP_INTENT_LOADING = `${namespace}/REQUEST_SETUP_INTENT_LOADING`;
export const SET_CLIENT_SECRET = `${namespace}/SET_CLIENT_SECRET`;
export const SET_CARD_MODAL = `${namespace}/SET_CARD_MODAL`;
export const SET_PAYMENT_METHOD = `${namespace}/SET_PAYMENT_METHOD`;

export const SET_BILLING_INFORMATION = `${namespace}/SET_BILLING_INFORMATION`;

export const NOTIFY_USER = `${namespace}/NOTIFY_USER`;
export const SET_MINI_LOADING = `${namespace}/SET_MINI_LOADING`;

