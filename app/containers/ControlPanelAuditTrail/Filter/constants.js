export const FILTER_TYPES = {
    START_DATE: 'start_date',
    END_DATE: 'end_date',
    COMPANY: 'company_id',
    USER: 'user_id',
    TRANSACTION_TYPE: 'action_type',
    MODULE: 'module_name'
};
