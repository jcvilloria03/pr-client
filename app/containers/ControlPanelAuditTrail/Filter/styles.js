import styled from 'styled-components';

export const FilterWrapper = styled.div`
    display: flex;
    flex-direction: column;
    border: 1px solid #ccc;
    padding: 30px 15px;
    padding-bottom: 10px;
    margin-bottom: 25px;
    border-radius: .5rem;

    .sl-c-filter-actions {
        display: flex;
        justify-content: space-between;
        border-top: 1px solid lightgrey;
        padding-top: 20px;
        align-items: center;

        .sl-c-filter-reset > .btn {
            margin-left: 0;
            color: #00A5E5;
            cursor: pointer;
            border: none;
        }

        .sl-c-filter-buttons > .btn {
            width: 100px;
        }
    }

    .sl-c-filter-actions-no-border {
        display: flex;
        justify-content: space-between;
        padding-top: 20px;
        align-items: center;

        .sl-c-filter-toggle > .btn {
            margin-left: 0;
            color: #00A5E5;
            cursor: pointer;
            border: none;
        }

        .sl-c-filter-buttons > .btn {
            width: 100px;
        }
    }

    .date-picker {
        .DayPickerInput {
            width: 100%;
        }
        span {
            display: block;
        }
        input {
            width: 100%;
            padding-top: 0px !important;
        }
    }
`;
