/* eslint-disable no-new-object */
import React from 'react';
import moment from 'moment/moment';
import get from 'lodash/get';

import Button from 'components/Button';
import SalSelect from 'components/Select';
import DatePicker from 'components/DatePicker';

import { DATE_FORMATS } from 'utils/constants';
import { formatDate } from 'utils/functions';

import { FilterWrapper } from './styles';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.PureComponent {
    static propTypes = {
        filterData: React.PropTypes.shape({
            companies: React.PropTypes.array,
            users: React.PropTypes.array,
            transactionTypes: React.PropTypes.array,
            modules: React.PropTypes.array
        }),
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    static initialState = {
        start_date: moment().format( DATE_FORMATS.API ),
        end_date: moment().format( DATE_FORMATS.API ),
        is_expanded: false,
        values: {
            comp: null,
            user: null,
            transactionType: null,
            module: null
        }
    };

    constructor( props ) {
        super( props );

        this.state = Filter.initialState;
        this.onApply = this.onApply.bind( this );
    }

    onApply = () => {
        const obj = new Object();

        if ( this.state.start_date !== null ) {
            obj.start_date = this.state.start_date;
        }
        if ( this.state.end_date !== null ) {
            obj.end_date = this.state.end_date;
        }

        const { comp, user, transactionType, module } = this.state.values;
        if ( comp !== null ) {
            obj.company_id = comp.value;
        }
        if ( user !== null ) {
            obj.user_id = user.value;
        }
        if ( transactionType !== null && transactionType !== undefined ) {
            obj.action_type = transactionType.value;
        }
        if ( module !== null && module !== undefined ) {
            obj.module_name = module.value;
        }
        this.props.onApply( obj );
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    getCompanies() {
        if ( this.props.filterData.companies === undefined || this.props.filterData.companies.length === 0 ) {
            return [];
        }
        const companies = this.props.filterData.companies.map( ( comp ) => ({
            value: comp.id,
            label: comp.name,
            disabled: false
        }) );

        return companies;
    }

    getUsers() {
        if ( this.props.filterData.users === undefined || this.props.filterData.users.length === 0 ) {
            return [];
        }

        const users = this.props.filterData.users.map( ( user ) => ({
            value: user.id,
            label: user.name,
            disabled: false
        }) );

        return users;
    }

    getTransactionTypes() {
        if ( this.props.filterData.transactionTypes === undefined || this.props.filterData.transactionTypes.length === 0 ) {
            return [];
        }

        const transactionTypes = this.props.filterData.transactionTypes.map( ( transactionType ) => ({
            value: transactionType.id,
            label: transactionType.name,
            disabled: false
        }) );

        return transactionTypes;
    }

    getModules() {
        if ( this.props.filterData.modules === undefined || this.props.filterData.modules.length === 0 ) {
            return [];
        }

        const modules = this.props.filterData.modules.map( ( module ) => ({
            value: module.id,
            label: module.name,
            disabled: false
        }) );

        return modules;
    }

    toggleExpanded = () => {
        this.setState( ( state ) => ({
            is_expanded: !state.is_expanded
        }), () =>
            this.state.is_expanded && this.onApply()
        );
    }

    resetFilters = () => {
        this.comp.setState({ value: null });
        this.user.setState({ value: null });
        this.transactionType.setState({ value: null });
        this.module.setState({ value: null });
    }

    render() {
        return (
            <FilterWrapper>
                <div className="row">
                    <div className="col-xs-3 date-picker">
                        <DatePicker
                            label="Modification Start Date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            ref={ ( ref ) => { this.start_date = ref; } }
                            selectedDay={ this.state.start_date }
                            onChange={ ( value ) => this.setState({ start_date: formatDate( value, DATE_FORMATS.API ) }) }
                            disabledDays={ this.state.end_date
                                ? [{ after: new Date( this.state.end_date ) }]
                                : []
                            }
                        />
                    </div>
                    <div className="col-xs-3 date-picker">
                        <DatePicker
                            label="Modification End Date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            ref={ ( ref ) => { this.end_date = ref; } }
                            selectedDay={ this.state.end_date }
                            onChange={ ( value ) => this.setState({ end_date: formatDate( value, DATE_FORMATS.API ) }) }
                            disabled={ !this.state.start_date }
                            disabledDays={ this.state.start_date
                                ? [{ before: new Date( this.state.start_date ) }]
                                : []
                            }
                        />
                    </div>
                    <div className="col-xs-3">
                        <SalSelect
                            id="comp"
                            key="comp"
                            label="Company"
                            placeholder="All Companies"
                            value={ get( this.state.value, 'comp', null ) }
                            data={ this.getCompanies() }
                            onChange={ ( value ) => this.setState( ( state ) => ({
                                values: {
                                    ...state.values,
                                    comp: value
                                }
                            }) ) }
                            ref={ ( ref ) => { this.comp = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <SalSelect
                            id="user"
                            key="user"
                            label="User"
                            placeholder="All Users"
                            value={ get( this.state.value, 'user', null ) }
                            data={ this.getUsers() }
                            onChange={ ( value ) => this.setState( ( state ) => ({
                                values: {
                                    ...state.values,
                                    user: value
                                }
                            }) ) }
                            ref={ ( ref ) => { this.user = ref; } }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-3">
                        <SalSelect
                            id="transactionType"
                            key="transactionType"
                            label="Transaction Type"
                            placeholder="All Transaction Types"
                            value={ get( this.state.value, 'transactionType', null ) }
                            data={ this.getTransactionTypes() }
                            onChange={ ( value ) => this.setState( ( state ) => ({
                                values: {
                                    ...state.values,
                                    transactionType: value
                                }
                            }) ) }
                            ref={ ( ref ) => { this.transactionType = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <SalSelect
                            id="module"
                            key="module"
                            label="Module"
                            placeholder="All modules"
                            value={ get( this.state.value, 'module', null ) }
                            data={ this.getModules() }
                            onChange={ ( value ) => this.setState( ( state ) => ({
                                values: {
                                    ...state.values,
                                    module: value
                                }
                            }) ) }
                            ref={ ( ref ) => { this.module = ref; } }
                        />
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            disabled={ !( this.state.start_date && this.state.end_date ) }
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
