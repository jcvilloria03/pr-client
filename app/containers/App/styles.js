import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    padding-top: 200px;
`;

export const CountdownWrapper = styled.div`
    position: fixed;
    z-index: 100;
    right: 0;
    left: 0;
    min-height: 50px;
    margin: auto;
    display: flex;
    align-items: center;
    justify-content: space-between;
    transition: top 500ms cubic-bezier(0, 0, 0.30, 1);
    font-weight: 600;
    text-transform: initial;
    font-size: 16px;
    box-shadow: 0px -3px 20px 0px rgba(0,0,0,0.75);
    color: #ffffff;
    background-color: rgba(250, 131, 131, 0.83);

    .delete-countdown-progress {
        position: inherit;
        height: 50px;
        background-color: rgba(214, 43, 43, 0.501);
        z-index: 2;
    }

    .delete-countdown-text {
        position: relative;
        z-index: 3;
        padding: 0 10px;
    }
`;
