/* eslint-disable camelcase */
/* eslint-disable no-undef */
/**
 *
 * App.react.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import get from 'lodash/get';
import moment from 'moment';
import userflow from 'userflow.js';

import { auth } from 'utils/AuthService';
import { H2, H3 } from 'components/Typography';
import withProgressBar from 'components/ProgressBar';

import { Container } from 'reactstrap';
import Header from '../Header';

import { INLINEMANUAL_API_KEY, UNPROTECTED_ROUTES } from '../../constants';

import { LoadingStyles, PageWrapper, CountdownWrapper } from './styles';

import {
    makeSelectCurrentCompanyState,
    makeSelectCompaniesState,
    makeSelectProductsState,
    makeSelectUserInformationState,
    makeSelectUnreadAnnouncementsState,
    makeSelectUserNotificationsState,
    makeSelectRedirectingToTA,
    makeSelectSetupProgress,
    makeSelectIsExpired,
    makeSelectAccountDeleteRequestData,
    makeSelectHasApprovals
} from './selectors';

import * as topLevelActions from './actions';
import DemoStepper from '../../components/DemoCompany/DemoStepper';

/**
 * Main App Component
 */
export class App extends React.PureComponent {
    static propTypes = {
        initializeData: React.PropTypes.func,
        setLastActiveCompany: React.PropTypes.func,
        children: React.PropTypes.node,
        currentCompany: React.PropTypes.object,
        companies: React.PropTypes.array,
        products: React.PropTypes.array,
        userInformation: React.PropTypes.object,
        unreadAnnouncements: React.PropTypes.array,
        markAnnouncementAsRead: React.PropTypes.func,
        markReplyAsRead: React.PropTypes.func,
        userNotifications: React.PropTypes.object,
        loadMoreNotifications: React.PropTypes.func,
        pushNotification: React.PropTypes.func,
        updateNotificationClickedStatus: React.PropTypes.func,
        redirectingToTA: React.PropTypes.bool,
        router: React.PropTypes.object, // eslint-disable-line
        isExpired: React.PropTypes.bool,
        getNotificationsAndAnnouncements: React.PropTypes.func,
        accountDeleteRequestData: React.PropTypes.object,
        hasApprovals: React.PropTypes.bool
    };

    constructor( props ) {
        super( props );
        this.state = {
            previousRoute: {},
            currentRoute: {},
            isZohoLoaded: false
        };
    }

    componentDidMount() {
        if ( auth.loggedIn() && auth.isAdmin() ) {
            this.props.initializeData();
            this.props.getNotificationsAndAnnouncements();
        }

        this.loadZoho();
        this.loadUserFlow();
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.state.currentRoute.path !== nextProps.router.routes[ 1 ].path ) {
            this.setState({
                previousRoute: this.state.currentRoute,
                currentRoute: nextProps.router.routes[ 1 ]
            });
        }

        nextProps.accountDeleteRequestData !== this.props.accountDeleteRequestData
            && auth.setAccountDeleteRequestData( nextProps.accountDeleteRequestData );
    }

    componentDidUpdate( prevProps, prevState ) {
        if ( ( prevState.isZohoLoaded !== this.state.isZohoLoaded ) && this.state.isZohoLoaded ) {
            userflow.setResourceCenterLauncherHidden( false );

            if ( $zoho && $zoho.salesiq && $zoho.salesiq.chat ) {
                $zoho.salesiq.chat.start();
            }
        }
    }

    getCountdownPercentage() {
        const data = this.props.accountDeleteRequestData;
        if ( data && data.deleteAt && data.requestedAt ) {
            const start = moment( data.requestedAt );
            const end = moment( data.deleteAt );
            const today = moment();

            const percentile = 100.0 * today.diff( start ) / end.diff( start );
            const result = Math.floor( percentile );

            if ( result >= 100 ) { localStorage.clear(); }

            return result;
        }

        return 0;
    }

    loadZoho = () => {
        if ( !process.env.ZOHO_WIDGET_CODE ) {
            return;
        }

        window.$zoho = window.$zoho || {};
        window.$zoho.salesiq = window.$zoho.salesiq || {
            widgetcode: process.env.ZOHO_WIDGET_CODE,
            values: {
                email: this.props.userInformation.email
            },
            ready() {}
        };

        const s = document.createElement( 'script' );
        s.setAttribute( 'type', 'text/javascript' );
        s.setAttribute( 'id', 'zsiqscript' );
        s.setAttribute( 'defer', true );
        s.setAttribute( 'src', 'https://salesiq.zoho.com/widget' );
        const t = document.getElementsByTagName( 'script' )[ 0 ];
        t.parentNode.insertBefore( s, t );

        const zohodiv = document.createElement( 'div' );
        zohodiv.setAttribute( 'id', 'zsiqwidget' );
        document.body.append( zohodiv );
    }

    loadUserFlow= () => {
        const user = JSON.parse( localStorage.getItem( 'user' ) );

        if ( !user ) {
            return;
        }

        const isTrial = get( user, 'subscription.is_trial', true );
        if ( process.env.USERFLOW_TOKEN ) {
            userflow.init( process.env.USERFLOW_TOKEN );
            userflow.identify( user.account_id, {
                company: user.company_id,
                email: user.email,
                first_name: user.first_name,
                last_name: user.last_name,
                name: user.full_name,
                signed_up_at: moment( user.created_at ).format()
            });

            userflow.group( user.company_id, {
                name: user.company_name,
                plan: isTrial ? 'Trial' : 'Paid'
            }, {
                membership: {
                    email: user.email,
                    role: user.role,
                    employeed_id: user.employee_id,
                    location: user.employee && user.employee.location,
                    department: user.employee && user.employee.department,
                    rank: user.employee && user.employee.rank,
                    salpay_integration_status: user.salpay_integration_status
                }
            });

            userflow.setResourceCenterLauncherHidden( true );

            userflow.on( 'resourceCenterChanged', () => {
                if ( $zoho.salesiq.chat && !this.state.isZohoLoaded ) {
                    this.setState({ isZohoLoaded: true });
                }
            });
        }
    }

    redirectToCompanySettingsIfFirstLogin( props ) {
        if ( UNPROTECTED_ROUTES.includes( props.router.routes[ 1 ].name ) || !auth.loggedIn() ) {
            return;
        }

        const currentCompany = props.currentCompany;
        const firstLoginCompleted = props.setupProgress.first_login;

        if ( currentCompany && currentCompany.id && !firstLoginCompleted ) {
            window.location.href = '/company-settings/company-structure/company-details';
        }
    }

    formatDeletedAt() {
        const data = this.props.accountDeleteRequestData;
        return data.deleteAt && moment( data.deleteAt ).format( 'LLL' );
    }

    isNotLandingPages() {
        const isWelcomePage = window.location.pathname.includes( '/welcome' );
        const loginPage = window.location.pathname.includes( '/login' );

        return !isWelcomePage && !loginPage;
    }

    renderLoader = () => (
        <PageWrapper>
            <Container>
                <LoadingStyles>
                    <H2>Loading...</H2>
                    <br />
                    <H3>Please wait...</H3>
                </LoadingStyles>
            </Container>
        </PageWrapper>
    )

    /**
     * { render the application }
     */
    render() {
        const userInactive = get( this.props, 'router.location.state.userInactive', false );
        return (
            <div>
                { this.isNotLandingPages() && auth.isAdmin() && auth.loggedIn() && auth.emailVerified() && !auth.isFreshAccount() ?
                    <div>
                        <Header
                            currentCompany={ this.props.currentCompany }
                            companies={ this.props.companies }
                            onSetCompany={ this.props.setLastActiveCompany }
                            userInformation={ this.props.userInformation }
                            unreadAnnouncements={ this.props.unreadAnnouncements }
                            markAnnouncementAsRead={ this.props.markAnnouncementAsRead }
                            markReplyAsRead={ this.props.markReplyAsRead }
                            userNotifications={ this.props.userNotifications }
                            loadMoreNotifications={ this.props.loadMoreNotifications }
                            pushNotification={ this.props.pushNotification }
                            updateNotificationClickedStatus={ this.props.updateNotificationClickedStatus }
                            hasApprovals={ this.props.hasApprovals }
                        />
                        <DemoStepper />
                    </div> : ''
                }

                { auth.loggedIn() && this.props.accountDeleteRequestData && (
                    <CountdownWrapper style={ { top: '120px', width: '90%' } }>
                        <div className="delete-countdown-text">{ `Your account will be deleted on ${this.formatDeletedAt()}. If you do not want this to occur please contact support.` }</div>
                        <div className="delete-countdown-progress" style={ { width: `calc(${this.getCountdownPercentage()}% - 10%)` } }></div>
                    </CountdownWrapper>
                )}

                { !this.state.hasRootAdminPermission
                    && get( this.props, 'router.location.pathname', '' ) === '/payroll/unauthorized'
                    && React.Children.map( this.props.children, ( child ) => React.cloneElement( child ) )
                }

                { ( this.props.products || !auth.loggedIn() || !auth.emailVerified() || auth.isFreshAccount() || auth.isNonAdmin() || userInactive || !auth.isExpired() ) && !this.props.redirectingToTA
                    ? React.Children.map( this.props.children, ( child ) =>
                            React.cloneElement( child, { products: this.props.products, previousRoute: this.state.previousRoute })
                        )
                    : ( this.renderLoader() )
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    currentCompany: makeSelectCurrentCompanyState(),
    companies: makeSelectCompaniesState(),
    products: makeSelectProductsState(),
    userInformation: makeSelectUserInformationState(),
    unreadAnnouncements: makeSelectUnreadAnnouncementsState(),
    userNotifications: makeSelectUserNotificationsState(),
    redirectingToTA: makeSelectRedirectingToTA(),
    setupProgress: makeSelectSetupProgress(),
    isExpired: makeSelectIsExpired(),
    accountDeleteRequestData: makeSelectAccountDeleteRequestData(),
    hasApprovals: makeSelectHasApprovals()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        topLevelActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( withProgressBar( App ) );
