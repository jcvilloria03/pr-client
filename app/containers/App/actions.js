import {
    SET_COMPANY,
    SET_LAST_ACTIVE_COMPANY,
    MARK_ANNOUNCEMENT_AS_READ,
    MARK_REPLY_AS_READ,
    INITIALIZE_DATA,
    LOAD_MORE_NOTIFICATIONS,
    PUSH_NOTIFICATION,
    UPDATE_NOTIFICATION_CLICKED_STATUS,
    SET_IS_EXPIRED,
    GET_NOTIFICATIONS_AND_ANNOUNCEMENTS,
    REINITIALIZE_APP_DATA
} from './constants';

/**
 * Set company.
 */
export function setLastActiveCompany( company ) {
    return {
        type: SET_LAST_ACTIVE_COMPANY,
        payload: company
    };
}

/**
 * Set Current company.
 */
export function setCurrentCompany( payload ) {
    return {
        type: SET_COMPANY,
        payload
    };
}

/**
 * Initialize data
 */
export function initializeData( callback = null ) {
    return {
        type: INITIALIZE_DATA,
        callback
    };
}

/**
 * Mark announcement as read
 */
export function markAnnouncementAsRead( announcementId, notificationId ) {
    return {
        type: MARK_ANNOUNCEMENT_AS_READ,
        payload: {
            announcementId,
            notificationId
        }
    };
}

/**
 * Mark announcement reply as read
 */
export function markReplyAsRead( announcementId, replyId, isCurrentUserAnnouncementOwner, notificationId ) {
    return {
        type: MARK_REPLY_AS_READ,
        payload: {
            announcementId,
            replyId,
            isCurrentUserAnnouncementOwner,
            notificationId
        }
    };
}

/**
 * Load more notifications
 */
export function loadMoreNotifications( notifications, page ) {
    return {
        type: LOAD_MORE_NOTIFICATIONS,
        payload: {
            notifications,
            page
        }
    };
}

/**
 * Push notification into array when it arrives from sockets
 */
export function pushNotification( notifications, notification ) {
    return {
        type: PUSH_NOTIFICATION,
        payload: {
            notifications,
            notification
        }
    };
}

/**
 * Update notification clicked status
 */
export function updateNotificationClickedStatus( id, callback ) {
    return {
        type: UPDATE_NOTIFICATION_CLICKED_STATUS,
        payload: {
            id,
            callback
        }
    };
}

/**
 * Update expired status of account
 *
 * @param {Boolean} payload - Expired status
 * @returns {Object}
 */
export function updateExpiredStatus( payload ) {
    return {
        type: SET_IS_EXPIRED,
        payload
    };
}

/**
 * Get user notifications and announcements
 *
 * @returns {Array}
 */
export function getNotificationsAndAnnouncements() {
    return {
        type: GET_NOTIFICATIONS_AND_ANNOUNCEMENTS
    };
}

/**
 * Get user account latest data
 *
 * @returns {Array}
 */
export function reinitializeAppData() {
    return {
        type: REINITIALIZE_APP_DATA
    };
}
