import { createSelector } from 'reselect';

// makeSelectLocationState expects a plain JS object for the routing state
const makeSelectLocationState = () => {
    let prevRoutingState;
    let prevRoutingStateJS;

    return ( state ) => {
        const routingState = state.get( 'route' ); // or state.route

        if ( !routingState.equals( prevRoutingState ) ) {
            prevRoutingState = routingState;
            prevRoutingStateJS = routingState.toJS();
        }

        return prevRoutingStateJS;
    };
};

const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

const makeSelectCurrentCompanyState = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'currentCompany' ).toJS()
);

const makeSelectCompaniesState = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'companies' ).toJS()
);

const makeSelectUserInformationState = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'userInformation' ).toJS()
);

const makeSelectUserDataState = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'userData' ).toJS()
);

const makeSelectUnreadAnnouncementsState = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'unreadAnnouncements' ).toJS()
);

const makeSelectUserNotificationsState = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'userNotifications' ).toJS()
);

const makeSelectRedirectingToTA = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'redirectingToTA' )
);

const makeSelectProductsState = () => createSelector(
    selectAppDomain(),
    ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

const makeSelectSetupProgress = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'setupProgress' ).toJS()
);

const makeSelectIsExpired = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'is_expired' )
);

const makeSelectAccountDeleteRequestData = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'accountDeleteRequestData' )
);

const makeSelectHasApprovals = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'hasApprovals' )
);

export {
    makeSelectLocationState,
    makeSelectCurrentCompanyState,
    makeSelectCompaniesState,
    makeSelectProductsState,
    makeSelectUserInformationState,
    makeSelectUnreadAnnouncementsState,
    makeSelectUserNotificationsState,
    makeSelectRedirectingToTA,
    makeSelectUserDataState,
    makeSelectSetupProgress,
    makeSelectIsExpired,
    makeSelectAccountDeleteRequestData,
    makeSelectHasApprovals
};
