import { fromJS } from 'immutable';
import {
    SET_COMPANY,
    SET_COMPANIES,
    SET_PRODUCTS,
    SET_USER_INFORMATION,
    SET_UNREAD_ANNOUNCEMENTS,
    SET_USER_NOTIFICATIONS,
    SET_NOTIFICATIONS_TOTAL_PAGES,
    LOADING_MORE_NOTIFICATIONS,
    PUSH_NOTIFICATION,
    REDIRECTING_TO_TA,
    SET_USER_DATA,
    SET_SETUP_PROGRESS,
    SET_IS_EXPIRED,
    SET_ACCOUNT_DELETION_DATA,
    SET_HAS_APPROVALS
} from './constants';

const initialState = fromJS({
    currentCompany: {},
    companies: [],
    products: null,
    userInformation: {},
    userData: {},
    unreadAnnouncements: [],
    userNotifications: {
        data: [],
        totalPages: 0,
        loadingMore: false
    },
    redirectingToTA: false,
    setupProgress: {},
    is_expired: false,
    isSalpayAdmin: false,
    isAllowedToInvite: false,
    accountDeleteRequestData: null,
    hasApprovals: false
});

/**
 * Handle companies for company switcher in header.
 *
 * @param {Object} state
 * @param {Object} action
 */
function reducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_COMPANY:
            return state.set( 'currentCompany', fromJS( action.payload ) );
        case SET_COMPANIES:
            return state.set( 'companies', fromJS( action.payload ) );
        case SET_PRODUCTS:
            return state.set( 'products', fromJS( action.payload ) );
        case SET_USER_INFORMATION:
            return state.set( 'userInformation', fromJS( action.payload ) );
        case SET_USER_DATA:
            return state.set( 'userData', fromJS( action.payload ) );
        case SET_SETUP_PROGRESS:
            return state.set( 'setupProgress', fromJS( action.payload ) );
        case SET_UNREAD_ANNOUNCEMENTS:
            return state.set( 'unreadAnnouncements', fromJS( action.payload ) );
        case SET_USER_NOTIFICATIONS:
            return state.setIn([ 'userNotifications', 'data' ], fromJS( action.payload ) );
        case SET_NOTIFICATIONS_TOTAL_PAGES:
            return state.setIn([ 'userNotifications', 'totalPages' ], fromJS( action.payload ) );
        case LOADING_MORE_NOTIFICATIONS:
            return state.setIn([ 'userNotifications', 'loadingMore' ], action.payload );
        case REDIRECTING_TO_TA:
            return state.set( 'redirectingToTA', action.payload );
        case PUSH_NOTIFICATION: {
            const notifications = state.toJS().userNotifications.data;
            notifications.unshift( action.payload.notification );
            return state.setIn([ 'userNotifications', 'data' ], fromJS( notifications ) );
        }
        case SET_IS_EXPIRED:
            return state.set( 'is_expired', action.payload );
        case SET_ACCOUNT_DELETION_DATA:
            return state.set( 'accountDeleteRequestData', action.payload );
        case SET_HAS_APPROVALS:
            return state.set( 'hasApprovals', action.payload );
        default:
            return state;
    }
}

export default reducer;
