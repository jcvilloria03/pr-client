/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const DEFAULT_LOCALE = 'en';
export const SET_COMPANY = 'app/App/SET_COMPANY';
export const SET_COMPANIES = 'app/App/SET_COMPANIES';
export const SET_PRODUCTS = 'app/App/SET_PRODUCTS';
export const SET_USER_INFORMATION = 'app/App/SET_USER_INFORMATION';
export const SET_LAST_ACTIVE_COMPANY = 'app/App/SET_LAST_ACTIVE_COMPANY';
export const REINITIALIZE_PAGE = 'app/App/REINITIALIZE_PAGE';
export const RESET_STORE = 'app/App/RESET_STORE';
export const INITIALIZE_DATA = 'app/App/INITIALIZE_DATA';
export const SET_USER_DATA = 'app/App/SET_USER_DATA';

export const SET_UNREAD_ANNOUNCEMENTS = 'app/App/SET_UNREAD_ANNOUNCEMENTS';
export const MARK_ANNOUNCEMENT_AS_READ = 'app/App/MARK_ANNOUNCEMENT_AS_READ';
export const MARK_REPLY_AS_READ = 'app/App/MARK_REPLY_AS_READ';
export const REDIRECTING_TO_TA = 'app/App/REDIRECTING_TO_TA';

export const SET_USER_NOTIFICATIONS = 'app/App/SET_USER_NOTIFICATIONS';
export const SET_NOTIFICATIONS_TOTAL_PAGES = 'app/App/SET_NOTIFICATIONS_TOTAL_PAGES';
export const LOAD_MORE_NOTIFICATIONS = 'app/App/LOAD_MORE_NOTIFICATIONS';
export const LOADING_MORE_NOTIFICATIONS = 'app/App/LOADING_MORE_NOTIFICATIONS';
export const PUSH_NOTIFICATION = 'app/App/PUSH_NOTIFICATION';
export const UPDATE_NOTIFICATION_CLICKED_STATUS = 'app/App/UPDATE_NOTIFICATION_CLICKED_STATUS';

export const GET_SETUP_PROGRESS = 'app/App/GET_SETUP_PROGRESS';
export const SET_SETUP_PROGRESS = 'app/App/SET_SETUP_PROGRESS';

export const SET_IS_EXPIRED = 'app/App/SET_IS_EXPIRED';

export const SET_ACCOUNT_DELETION_DATA = 'app/App/SET_ACCOUNT_DELETION_DATA';

export const SET_HAS_APPROVALS = 'app/App/SET_HAS_APPROVALS';

export const GET_NOTIFICATIONS_AND_ANNOUNCEMENTS = 'app/App/GET_NOTIFICATIONS_AND_ANNOUNCEMENTS';
export const REINITIALIZE_APP_DATA = 'app/App/REINITIALIZE_APP_DATA';
