import uniqBy from 'lodash/uniqBy';
import { call, put, take, cancel } from 'redux-saga/effects';
import { takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import { auth } from 'utils/AuthService';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import axios from 'axios';

import {
    SET_COMPANIES,
    SET_PRODUCTS,
    SET_USER_INFORMATION,
    REINITIALIZE_PAGE,
    SET_LAST_ACTIVE_COMPANY,
    RESET_STORE,
    SET_UNREAD_ANNOUNCEMENTS,
    MARK_ANNOUNCEMENT_AS_READ,
    MARK_REPLY_AS_READ,
    REDIRECTING_TO_TA,
    SET_USER_NOTIFICATIONS,
    LOAD_MORE_NOTIFICATIONS,
    SET_NOTIFICATIONS_TOTAL_PAGES,
    LOADING_MORE_NOTIFICATIONS,
    UPDATE_NOTIFICATION_CLICKED_STATUS,
    INITIALIZE_DATA,
    SET_SETUP_PROGRESS,
    GET_NOTIFICATIONS_AND_ANNOUNCEMENTS,
    SET_IS_EXPIRED,
    REINITIALIZE_APP_DATA,
    SET_ACCOUNT_DELETION_DATA,
    SET_HAS_APPROVALS
} from './constants';
import { setCurrentCompany } from './actions';
import { AUTHZ } from '../../utils/constants';

/**
 * Get user companies.
 */
export function* initializeData({ callback }) {
    if ( !auth.loggedIn() || auth.isNonAdmin() ) {
        return;
    }

    yield call( getCompanies );

    if ( auth.isExpired() ) {
        yield put({ type: SET_IS_EXPIRED, payload: auth.isExpired() });
    }

    // Make sure to set the `products` state first as a lot of
    // containers/components depend on this
    yield put({ type: SET_PRODUCTS, payload: auth.getProducts() });

    const user = auth.getUser();
    yield put({ type: SET_USER_INFORMATION, payload: user });

    yield call( updateAccountStatus );

    try {
        try {
            if ( user && !( 'salpay_integration_status' in user ) ) {
                const companyId = user.last_active_company_id ? user.last_active_company_id : user.company_id;
                const result = yield call( Fetch, `/salpay/companies/${companyId}/integration-status`, {
                    method: 'GET',
                    authzModules: AUTHZ.ROOT_ADMIN,
                    authzCompanyId: companyId
                });

                auth.setSalpayIntegrationStatus( result.salpay_integration );
            }

            yield put({
                type: SET_ACCOUNT_DELETION_DATA,
                payload: auth.getAccountDeleteRequestData()
            });
        } catch ( error ) {
            if ( error.response && error.response.status !== 418 ) {
                throw error;
            } else {
                yield put({ type: SET_IS_EXPIRED, payload: auth.isExpired() });
            }
        }

        if ( typeof callback === 'function' ) {
            yield call( callback );
        }
    } catch ( error ) {
        // return nothing
    }
}

/**
 * get notification and announcements
 */
export function* getNotificationsAndAnnouncements() {
    if ( !auth.loggedIn() || auth.isNonAdmin() || auth.isExpired() ) {
        return;
    }

    const lastActiveCompanyId = company.getLastActiveCompanyId();

    try {
        let unreadAnnouncements = {
            data: []
        };

        let notifications = {
            data: []
        };

        if ( lastActiveCompanyId ) {
            [ unreadAnnouncements, notifications ] = yield [
                call( Fetch, '/announcement_notifications', {
                    method: 'GET',
                    params: {
                        paginate: 0
                    },
                    authzModules: AUTHZ.ROOT_ADMIN
                }),
                call( Fetch, 'notifications', {
                    method: 'GET',
                    params: {
                        page: 1,
                        per_page: 3
                    },
                    authzModules: AUTHZ.ROOT_ADMIN
                })
            ];

            yield [
                put({
                    type: SET_UNREAD_ANNOUNCEMENTS,
                    payload: unreadAnnouncements.data.slice( 0, 3 )
                }),
                put({
                    type: SET_USER_NOTIFICATIONS,
                    payload: notifications.data
                }),
                notifications.meta && put({
                    type: SET_NOTIFICATIONS_TOTAL_PAGES,
                    payload: notifications.meta.pagination.total_pages
                })
            ];

            if ( window.location.pathname !== '/time/company/approvals/list' ) {
                const approvals = yield call( Fetch, '/user/approvals', {
                    method: 'POST',
                    params: {
                        page: 1,
                        per_page: 1,
                        my_level: 1,
                        statuses: ['pending'],
                        company_id: lastActiveCompanyId
                    },
                    authzModules: 'time_and_attendance.approvals_list'
                });

                yield approvals.meta && put({
                    type: SET_HAS_APPROVALS,
                    payload: approvals.meta.pagination.total > 0
                });
            }
        }
    } catch ( error ) {
        // return nothing
    }
}

/**
 * get companies
 */
export function* getCompanies() {
    const user = auth.getUser();

    try {
        const result = yield call( Fetch, '/account/philippine/companies', { method: 'GET', authzModules: AUTHZ.ROOT_ADMIN });
        const companies = result.data;

        yield put({ type: SET_COMPANIES, payload: companies });

        let companyId = user.last_active_company_id;
        if ( !companyId ) {
            companyId = user.company_id;
        }

        let lastActiveCompany = companies.find( ( comp ) => comp.id === companyId );

        if ( !lastActiveCompany ) {
            lastActiveCompany = yield call( Fetch, `/philippine/company/${companyId}`, {
                method: 'GET',
                authzModules: AUTHZ.ROOT_ADMIN
            });
        }

        yield put( setCurrentCompany( lastActiveCompany ) );

        if ( !user.last_active_company_id ) {
            yield call( setLastActiveCompany, { payload: lastActiveCompany });
        }
    } catch ( error ) {
        throw error;
    }
}

/**
 * Set last active company id
 */
export function* setLastActiveCompany({ payload }) {
    const user = auth.getUser();

    if ( payload.id === user.last_active_company_id ) {
        return;
    }

    try {
        // Set as authz default header
        axios.defaults.headers.common[ 'X-Authz-Company-Id' ] = payload.id;

        yield call( Fetch, '/user/last_active_company', {
            method: 'PATCH',
            data: { company_id: payload.id },
            authzModules: AUTHZ.ROOT_ADMIN
        });

        yield put( setCurrentCompany( payload ) );
        company.setLastActiveCompanyId( payload.id );

        yield put({
            type: REINITIALIZE_PAGE
        });

        const isDemoRestrictedRoute = window.location.pathname.includes( 'employees/batch-add' );
        if ( isDemoRestrictedRoute && company.isDemoCompany() ) {
            window.location.replace( '/employees' );
        } else {
            window.location.reload();
        }
    } catch ( error ) {
        // Do nothing
    }
}

/**
 * Mark announcement as read
 */
export function* markAnnouncementAsRead({ payload }) {
    try {
        yield put({ type: REDIRECTING_TO_TA, payload: true });
        const { announcementId, notificationId } = payload;
        const companyId = company.getLastActiveCompanyId();

        yield call(
            Fetch,
            `/announcement/${announcementId}/recipient_seen`,
            { method: 'PUT', authzModules: AUTHZ.ROOT_ADMIN }
        );

        yield call( updateNotificationClickedStatus, { payload: { id: notificationId }});

        window.location.href = `/time/company/announcements/received?id=${announcementId}`;
    } catch ( error ) {
        // TODO Move notifications to App container.
    }
}

/**
 * Mark reply as read
*/
export function* markReplyAsRead({ payload }) {
    try {
        yield put({ type: REDIRECTING_TO_TA, payload: true });
        const { announcementId, replyId, isCurrentUserAnnouncementOwner, notificationId } = payload;
        const companyId = company.getLastActiveCompanyId();

        yield call(
            Fetch,
            `/announcement/reply/${replyId}/seen`,
            { method: 'PUT', authzModules: AUTHZ.ROOT_ADMIN }
        );

        yield call( updateNotificationClickedStatus, { payload: { id: notificationId }});
        window.location.href = `/time/company/announcements/${isCurrentUserAnnouncementOwner ? 'sent' : 'received'}?id=${announcementId}`;
    } catch ( error ) {
        // TODO Move notifications to App container.
    }
}

/**
 * Load more notifications
 */
export function* loadMoreNotifications({ payload }) {
    const { page, notifications } = payload;

    yield put({
        type: LOADING_MORE_NOTIFICATIONS,
        payload: true
    });

    try {
        const response = yield call(
            Fetch,
            `/notifications?page=${page}&per_page=3`,
            { method: 'GET', authzModules: AUTHZ.ROOT_ADMIN }
        );
        const allNotifications = [
            ...notifications,
            ...response.data
        ];
        yield put({
            type: SET_USER_NOTIFICATIONS,
            payload: uniqBy( allNotifications, 'id' )
        });
    } catch ( error ) {
        // TODO Move notifications to App container.
    } finally {
        yield put({
            type: LOADING_MORE_NOTIFICATIONS,
            payload: false
        });
    }
}

/**
 * Update notification clicked status
 */
export function* updateNotificationClickedStatus({ payload }) {
    const { id } = payload;
    try {
        yield call(
            Fetch,
            `/notifications/${id}/clicked`,
            { method: 'PUT', authzModules: AUTHZ.ROOT_ADMIN }
        );

        payload.callback && payload.callback();
    } catch ( error ) {
        // TODO Move notifications to App container.
    }
}

/**
 * Update account expired status
 */
export function* updateAccountStatus() {
    try {
        let isExpired = auth.isExpired();

        if ( !isExpired ) {
            const account = yield call( Fetch, '/account', { method: 'GET', authzModules: AUTHZ.ROOT_ADMIN });
            const hasSubscription = account.subscriptions && account.subscriptions.length > 0;

            if ( hasSubscription ) {
                const subscription = hasSubscription && account.subscriptions[ 0 ];

                isExpired = hasSubscription && subscription.is_expired;

                yield put({ type: SET_IS_EXPIRED, payload: isExpired });

                let user = auth.getUser();
                user = Object.assign({}, user, {
                    subscription
                });

                auth.setUserData( user );
            }
        }
    } catch ( error ) {
        // TODO: error handling
    }
}

/**
 * resets the setup store to initial values
 */
export function* resetStore() {
    yield put({
        type: RESET_STORE
    });
}

/**
 * set account deletion request data
 */
export function* setAccountDeleteRequestData( payload ) {
    yield put({
        type: SET_ACCOUNT_DELETION_DATA,
        payload
    });
}

/**
 * resets the user account data
 */
export function* reinitializeAppData() {
    yield put({ type: RESET_STORE });
    yield put({ type: REINITIALIZE_PAGE });
}

/**
 * watch GET_NOTIFICATIONS_AND_ANNOUNCEMENTS
 */
export function* watchForGetNotificationsAndAnnouncements() {
    const watcher = yield takeLatest( GET_NOTIFICATIONS_AND_ANNOUNCEMENTS, getNotificationsAndAnnouncements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForSetLastActiveCompany() {
    const watcher = yield takeLatest( SET_LAST_ACTIVE_COMPANY, setLastActiveCompany );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for MARK_ANNOUNCEMENT_AS_READ
 */
export function* watchForMarkAnnouncementAsRead() {
    const watcher = yield takeLatest( MARK_ANNOUNCEMENT_AS_READ, markAnnouncementAsRead );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for MARK_REPLY_AS_READ
 */
export function* watchForMarkReplyAsRead() {
    const watcher = yield takeLatest( MARK_REPLY_AS_READ, markReplyAsRead );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for INITIALIZE_DATA
 */
export function* watchForInitializeData() {
    yield takeLatest( INITIALIZE_DATA, initializeData );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for LOAD_MORE_NOTIFICATIONS
 */
export function* watchForLoadMoreNotifications() {
    const watcher = yield takeLatest( LOAD_MORE_NOTIFICATIONS, loadMoreNotifications );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPDATE_NOTIFICATION_CLICKED_STATUS
 */
export function* watchForUpdateNotificationClickedStatus() {
    const watcher = yield takeLatest( UPDATE_NOTIFICATION_CLICKED_STATUS, updateNotificationClickedStatus );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_APP_DATA
 */
export function* watchForReinitializeAppData() {
    const watcher = yield takeLatest( REINITIALIZE_APP_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export default [
    watchForReinitializePage,
    watchForInitializeData,
    watchForSetLastActiveCompany,
    watchForMarkAnnouncementAsRead,
    watchForMarkReplyAsRead,
    watchForLoadMoreNotifications,
    watchForUpdateNotificationClickedStatus,
    watchForGetNotificationsAndAnnouncements,
    watchForReinitializeAppData
];
