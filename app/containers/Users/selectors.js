import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectUsersDomain = () => ( state ) => state.get( 'users' );

const makeSelectLoading = () => createSelector(
  selectUsersDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectUsers = () => createSelector(
  selectUsersDomain(),
  ( substate ) => substate.get( 'users' ).toJS()
);

const makeSelectRoles = () => createSelector(
  selectUsersDomain(),
  ( substate ) => substate.get( 'roles' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectUsersDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectUsers,
  makeSelectRoles,
  makeSelectNotification
};
