import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    padding-top: 76px;

    .content {
        padding: 6vh 50px;

        & > h2 {
            text-align: center;
            font-weight: 500;
        }

        & > p {
            text-align: center;
        }

        #create {
            text-align: center;
            margin-bottom: 50px;
        }

        .tableAction button {
            width: 100px;

            &:first-of-type {
                margin-right: 10px;
            }
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;
            
            h5 {
                margin: 0;
                margin-right: 10px;
            }

            .search-wrapper {
                flex-grow: 1;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 30px;

                    input {
                        border: none;
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }

            & > a {
                margin-left: 10px;
                padding: 4px 10px;
                border: 1px solid #00A5E5;
                color: #00A5E5;
                border-radius: 6px;
                background: #fff;
                text-decoration: none !important;
            }
        }
    }

    .radiogroup {
        padding: 0;
        & > span {
            padding: 0 15px;
            color: #5b5b5b;
            font-size: 14px;
            margin-bottom: 4px;
            margin-right: 1rem;
            font-weight: 400;
        }
    }

    #filters {
        padding: 30px 15px;
        background: #fff;
        margin: 0 0 20px 0;
        border-radius: 4px;
        border: 1px solid rgba(0,0,0,0.1);

        & > div:first-of-type > div {
            border-bottom: 1px solid rgba(0,0,0,0.1);
        }
    }

    #filter-reset {
        color: #00A5E5;
        text-decoration: none !important;
    }
`;
