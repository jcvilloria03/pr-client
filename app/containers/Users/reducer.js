import { fromJS } from 'immutable';

import { RESET_STORE } from '../App/constants';

const initialState = fromJS({
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    users: [
        {
            id: 132,
            name: 'asdasdas',
            email: 'edzon.labayan+dev@salarium.com',
            active: true,
            role: 'Owner'
        },
        {
            id: 143,
            name: 'asdasdasd',
            email: 'edzon.labayan+dev123@salarium.com',
            active: true,
            role: 'Test Innovations Inc. Admin'
        },
        {
            id: 389,
            name: 'permissionless',
            email: 'edzon.labayan+permissionless@salarium.com',
            active: false,
            role: 'Permissionless'
        },
        {
            id: 613,
            name: 'test',
            email: 'edzon.labayan+000@salarium.com',
            active: true,
            role: 'Owner'
        },
        {
            id: 1282,
            name: 'Sample ESS',
            email: 'edzon.labayan+ess102@salarium.com',
            active: false,
            role: 'Test Innovations Inc. Employee'
        }
    ],
    roles: [
        {
            value: 'Owner',
            label: 'Owner'
        },
        {
            value: 'Super Admin',
            label: 'Super Admin'
        },
        {
            value: 'Permissionless',
            label: 'Permissionless'
        }
    ]
});

/**
 *
 * Users reducer
 *
 */
function usersReducer( state = initialState, action ) {
    switch ( action.type ) {
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default usersReducer;
