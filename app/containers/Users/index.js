import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import {
    makeSelectLoading,
    makeSelectUsers,
    makeSelectRoles,
    makeSelectNotification
} from './selectors';
import * as usersActions from './actions';

import SnackBar from '../../components/SnackBar';

import {
    PageWrapper,
    LoadingStyles
} from './styles';

import { H2, H3, H5, P } from '../../components/Typography';
import Button from '../../components/Button';
import Table from '../../components/Table';
import Input from '../../components/Input';
import SalSelect from '../../components/Select';
import RadioGroup from '../../components/RadioGroup';
import Radio from '../../components/Radio';
import Icon from '../../components/Icon';

/**
 *
 * Users
 *
 */
export class Users extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool,
        users: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                id: React.PropTypes.number,
                name: React.PropTypes.string,
                email: React.PropTypes.string,
                active: React.PropTypes.bool,
                role: React.PropTypes.string
            })
        ),
        roles: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                value: React.PropTypes.any,
                label: React.PropTypes.string
            })
        )
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: this.props.users,
            showFilter: false,
            filter: {
                role: null,
                status: 'all'
            }
        };

        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.handleSearchAndFilters = this.handleSearchAndFilters.bind( this );
        this.searchInput = null;
        this.resetFilters = this.resetFilters.bind( this );
        this.applyFilters = this.applyFilters.bind( this );
        this.cancelChanges = this.cancelChanges.bind( this );
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.users !== this.props.users && this.handleSearchAndFilters();
    }

    /**
     * handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges( tableProps = this.usersTable.tableComponent.state ) {
        const page = tableProps.page + 1;
        const size = tableProps.pageSize;
        const dataLength = tableProps.data.length;
        this.setState({
            label: `Showing ${( ( page * size ) + 1 ) - size} - ${( page * size ) > dataLength ? dataLength : ( page * size )} of ${dataLength} entr${dataLength > 1 ? 'ies' : 'y'}`
        });
    }

    /**
     * handles filters and search inputs
     */
    handleSearchAndFilters() {
        let searchQuery = null;

        let dataToDisplay = this.props.users;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery ) {
            dataToDisplay = dataToDisplay.filter( ( user ) => {
                const { name, email, role } = user;
                return name.toLowerCase().indexOf( searchQuery ) >= 0 || email.toLowerCase().indexOf( searchQuery ) >= 0 || role.toLowerCase().indexOf( searchQuery ) >= 0;
            });
        }

        // Handles filters
        if ( this.state.filter.role ) {
            dataToDisplay = dataToDisplay.filter( ( user ) => {
                const { role } = user;
                return role.toLowerCase() === this.state.filter.role.toLowerCase();
            });
        }

        if ( this.state.filter.status !== 'all' ) {
            dataToDisplay = dataToDisplay.filter( ( user ) => {
                const { active } = user;
                const filter = this.state.filter.status === 'active';

                return filter === active;
            });
        }

        this.setState({ displayedData: dataToDisplay }, () => {
            this.handleTableChanges();
        });
    }

    /**
     * apply new filters chosen by user.
     */
    applyFilters() {
        this.setState({
            filter: {
                role: this.roleFilter.state.value ? this.roleFilter.state.value.value : null,
                status: this.statusFilter.state.value
            },
            showFilter: false
        }, () => {
            this.handleSearchAndFilters();
        });
    }

    /**
     * resets filters to default options
     */
    resetFilters() {
        this.setState({
            filter: {
                role: null,
                status: 'all'
            }
        }, () => {
            this.roleFilter.setState({ value: null });
            this.statusFilter.setState({ checkedIndex: 2, value: 'all' });
        });
    }

    /**
     * resets filter form to previous values
     */
    cancelChanges() {
        const roleMatch = this.roleFilter.props.data.filter( ( role ) => role.value === this.state.filter.role );
        const selectedRole = roleMatch.length ? roleMatch[ 0 ] : null;
        let selectedStatusIndex = -1;
        const selectedStatus = this.state.filter.status;

        switch ( selectedStatus ) {
            case 'all':
                selectedStatusIndex = 2;
                break;
            case 'active':
                selectedStatusIndex = 0;
                break;
            case 'inactive':
                selectedStatusIndex = 1;
                break;
            default:
        }

        this.roleFilter.setState({ value: selectedRole });
        this.statusFilter.setState({ checkedIndex: selectedStatusIndex, value: selectedStatus });
        this.setState({ showFilter: false });
    }

    /**
     *
     * Users render method
     *
     */
    render() {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Name',
                accessor: 'name',
                minWidth: 390
            },
            {
                header: 'Role',
                accessor: 'role',
                minWidth: 390
            },
            {
                header: 'Email',
                accessor: 'email',
                minWidth: 520
            },
            {
                header: 'Status',
                accessor: 'active',
                minWidth: 130,
                render: ( row ) => ( row.value ? 'Active' : 'Inactive' )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 260,
                sortable: false,
                render: ({ row }) => (
                    <div className="tableAction">
                        <Button
                            label={ <span><i className="fa fa-edit" /> Edit</span> }
                            alt
                            to={ `/user/${row.id}/edit` }
                        />
                        <Button
                            label={ row.active ? 'Deactivate' : 'Activate' }
                            type={ row.active ? 'danger' : 'action' }
                            alt
                        />
                    </div>
                )
            }
        ];
        return (
            <div>
                <Helmet
                    title="Users"
                    meta={ [
                        { name: 'description', content: 'List of Users in your Salarium Account' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <PageWrapper>
                    <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                        <LoadingStyles>
                            <H2>Loading Users.</H2>
                            <i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" />
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    </div>
                    <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                        <H2>Users</H2>
                        <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lobortis pulvinar sapien</P>
                        <div id="create">
                            <Button
                                label={ <span><i className="fa fa-user-plus" /> Create New User</span> }
                                size="large"
                                type="action"
                                to="/user/create"
                            />
                        </div>
                        <div className="title">
                            <H5>Users List</H5>
                            <div className="search-wrapper">
                                <Input
                                    className="search"
                                    id="search"
                                    placeholder="Search for name, role, or email"
                                    ref={ ( ref ) => { this.searchInput = ref; } }
                                    onChange={ this.handleSearchAndFilters }
                                    addon={ {
                                        content: <Icon name="search" />,
                                        placement: 'right'
                                    } }
                                />
                            </div>
                            <span>{this.state.label}</span>
                            <a
                                href=""
                                id="filter-toggle"
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    this.setState({ showFilter: !this.state.showFilter });
                                } }
                            >
                                <i className="fa fa-filter" /> Filter
                            </a>
                        </div>
                        <div className="row" id="filters" style={ { display: this.state.showFilter ? '' : 'none' } } >
                            <div className="col-xs-12">
                                <div className="row" >
                                    <div className="col-xs-4">
                                        <SalSelect
                                            data={ this.props.roles }
                                            id="role-filter"
                                            label="Role"
                                            placeholder="Select Role"
                                            ref={ ( ref ) => { this.roleFilter = ref; } }
                                        />
                                    </div>
                                    <div className="col-xs-4 radiogroup">
                                        <span>Status</span>
                                        <RadioGroup
                                            horizontal
                                            ref={ ( ref ) => { this.statusFilter = ref; } }
                                            value={ this.state.filter.status }
                                        >
                                            <Radio value="active">Active</Radio>
                                            <Radio value="inactive">Inactive</Radio>
                                            <Radio value="all">All</Radio>
                                        </RadioGroup>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12">
                                <div className="row" style={ { paddingTop: '20px' } }>
                                    <div className="col-xs-6">
                                        <a
                                            href=""
                                            id="filter-reset"
                                            onClick={ ( e ) => {
                                                e.preventDefault();
                                                this.resetFilters();
                                            } }
                                        >
                                            Reset to default
                                        </a>
                                    </div>
                                    <div className="col-xs-6" style={ { textAlign: 'right' } }>
                                        <Button
                                            label="cancel"
                                            type="neutral"
                                            size="large"
                                            onClick={ this.cancelChanges }
                                        />
                                        <Button
                                            label="Apply"
                                            type="action"
                                            size="large"
                                            onClick={ this.applyFilters }
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Table
                            data={ this.state.displayedData }
                            columns={ tableColumns }
                            pagination
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => { this.usersTable = ref; } }
                        />
                    </div>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    users: makeSelectUsers(),
    roles: makeSelectRoles(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        usersActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Users );
