// import { take, call, put, select } from 'redux-saga/effects';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

// All sagas to be loaded
export default [
    defaultSaga
];
