import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import get from 'lodash/get';

import A from 'components/A';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import Button from 'components/Button';
import { H2, H3 } from 'components/Typography';
import Modal from 'components/Modal';

import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';
import { isAuthorized } from 'utils/Authorization';

import {
    makeSelectNotification,
    makeSelectTerminationDetails,
    makeSelectPageStatus,
    makeSelectEmployee
} from './selectors';

import * as actions from './actions';

import {
    NavWrapper,
    HeadingWrapper,
    FormWrapper,
    Footer,
    LoadingWrapper,
    ModalBodyWrapper
} from './styles';

import FinalPayComputation from './FinalPayComputation';
import { PAGE_STATUSES } from './constants';

/**
 * Computed Projected Final Pay Component
 */
class ComputatedProjectedFinalPay extends React.PureComponent {
    static propTypes = {
        params: React.PropTypes.object,
        pageStatus: React.PropTypes.string,
        initializeData: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        resetStore: React.PropTypes.func,
        terminationDetails: React.PropTypes.object,
        saveFinalPayDetails: React.PropTypes.func
    };

    constructor( props ) {
        super( props );

        this.confirmSaveModal = null;
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.terminations'], ( authorized ) => {
            if ( authorized ) {
                const { id, terminationId } = this.props.params;
                this.props.initializeData( id, terminationId );
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    handleSave = () => {
        this.confirmSaveModal && this.confirmSaveModal.toggle();

        const payload = {
            final_pay_id: this.props.terminationDetails.final_pay_id,
            employee_id: parseInt( this.props.params.id, 10 )
        };

        this.props.saveFinalPayDetails( payload );
    }

    renderLoading( calculating = false ) {
        if ( calculating ) {
            return (
                <LoadingWrapper>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw loader" />
                    <div className="description">
                        <H3>Calculating projected final pay.</H3>
                        <p>Please wait...</p>
                    </div>
                </LoadingWrapper>
            );
        }

        return (
            <LoadingWrapper>
                <H2>Loading...</H2>
                <br />
                <H3>Please wait...</H3>
            </LoadingWrapper>
        );
    }

    /**
     * Component Render Method
     */
    render() {
        const isSaving = this.props.pageStatus === PAGE_STATUSES.SAVING;
        const isCalculating = this.props.pageStatus === PAGE_STATUSES.CALCULATING;
        const isLoading = isCalculating || this.props.pageStatus === PAGE_STATUSES.LOADING;
        const finalPayComputation = get( this.props.terminationDetails, 'final_pay_projected', null );

        return (
            <div>
                <Helmet
                    title="Projected Final Pay"
                    meta={ [
                        { name: 'description', content: 'Projected Final Pay of Employee' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="You are about to save this Final Pay details"
                    body={
                        <ModalBodyWrapper>
                            Saving this employee&apos;s final pay details will automatically
                            include this employee in the list for processing of final pay and
                            will convert the employee&apos;s status to &quot;Active&quot;
                            to &quot;Semi-Active&quot;.
                        </ModalBodyWrapper>
                    }
                    buttons={ [
                        {
                            id: 'buttonProceed',
                            type: 'action',
                            label: 'Proceed',
                            onClick: () => this.handleSave(),
                            disabled: isLoading || isSaving
                        },
                        {
                            id: 'buttonCancel',
                            type: 'danger',
                            alt: true,
                            label: 'Cancel',
                            onClick: () => this.confirmSaveModal.toggle()
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.confirmSaveModal = ref; } }
                />
                { isLoading
                    ? this.renderLoading( isCalculating )
                    : [
                        <NavWrapper key="nav">
                            <Container>
                                <A
                                    href
                                    onClick={ ( e ) => {
                                        e.preventDefault();
                                        browserHistory.push( `/employee/${this.props.params.id}`, true );
                                    } }
                                >
                                    &#8592; Back to Profile
                                </A>
                            </Container>
                        </NavWrapper>,
                        <div key="content">
                            <Container>
                                <HeadingWrapper>
                                    <h3>Projected Final Pay</h3>
                                    <p>Values below reflects the final pay items that you have pre-selected to be included on this employee&apos;s final pay computation</p>
                                </HeadingWrapper>
                            </Container>
                            <FormWrapper>
                                { Boolean( finalPayComputation ) && (
                                    <FinalPayComputation data={ finalPayComputation } />
                                ) }
                            </FormWrapper>
                            <Footer>
                                <Button
                                    id="buttonModify"
                                    label="Modify Final Pay Items"
                                    disabled={ isSaving }
                                    type="neutral"
                                    size="large"
                                    onClick={ () => { browserHistory.push( `/employee/${this.props.params.id}/termination/${this.props.params.terminationId}/checklist`, true ); } }
                                    ref={ ( ref ) => { this.submitButton = ref; } }
                                />
                                <Button
                                    id="buttonSave"
                                    label={ isSaving ? <Loader /> : 'Save Final Pay Details' }
                                    disabled={ isSaving || isLoading }
                                    type="action"
                                    size="large"
                                    onClick={ () => this.confirmSaveModal.toggle() }
                                    ref={ ( ref ) => { this.submitButton = ref; } }
                                />
                            </Footer>
                        </div>
                    ]
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    pageStatus: makeSelectPageStatus(),
    terminationDetails: makeSelectTerminationDetails(),
    employee: makeSelectEmployee()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( ComputatedProjectedFinalPay );
