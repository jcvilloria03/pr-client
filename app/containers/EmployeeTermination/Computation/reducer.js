import { fromJS } from 'immutable';
import {
    SET_ERRORS,
    NOTIFICATION_SAGA,
    SET_TERMINATION_DETAILS,
    SET_EMPLOYEE,
    SET_PAGE_STATUS,
    PAGE_STATUSES
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    errors: {},
    page_status: PAGE_STATUSES.LOADING,
    terminationDetails: {
        last_date: null,
        pay_date: null,
        reason_for_leaving: '',
        remarks: '',
        disburse_through_special_pay_run: false
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    employee: {}
});

/**
 * Formats termination details to be accessed by page
 * @param {object} data - Termination Details
 * @returns {object}
 */
function formatTerminationDetails( data ) {
    const { attributes, ...rest } = data;
    return { ...rest, ...attributes };
}

/**
 *
 * Edit employee termination manually
 *
 */
function employeeTerminationComputation( state = initialState, action ) {
    switch ( action.type ) {
        case SET_PAGE_STATUS:
            return state.set( 'page_status', action.payload );
        case SET_TERMINATION_DETAILS:
            return state.set( 'terminationDetails', fromJS( formatTerminationDetails( action.payload ) ) );
        case SET_EMPLOYEE:
            return state.set( 'employee', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default employeeTerminationComputation;
