import {
    INITIALIZE,
    SAVE_FINAL_PAY_DETAILS,
    NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData( id, terminationId ) {
    return {
        type: INITIALIZE,
        payload: {
            id,
            terminationId
        }
    };
}

/**
 * Display notification in page
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Dispatches request to save final pay details
 * @param {object} payload - Payload object
 * @param {number} payload.final_pay_id - Final Pay ID
 * @param {number} payload.employee_id - Employee ID
 */
export function saveFinalPayDetails( payload ) {
    return {
        type: SAVE_FINAL_PAY_DETAILS,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
