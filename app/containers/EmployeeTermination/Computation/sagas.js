import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from 'utils/BrowserHistory';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import {
    INITIALIZE,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SAVE_FINAL_PAY_DETAILS,
    SET_TERMINATION_DETAILS,
    SET_PAGE_STATUS,
    PAGE_STATUSES
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    const { id: employeeId, terminationId } = payload;

    try {
        yield put({ type: SET_PAGE_STATUS, payload: PAGE_STATUSES.LOADING });

        yield call( getTerminationInformation, {
            termination_id: terminationId,
            employee_id: employeeId
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({ type: SET_PAGE_STATUS, payload: PAGE_STATUSES.READY });
    }
}

/**
 * Fetch termination information
 * @param {object} payload
 * @param {number} payload.termination_id
 * @param {number} payload.employee_id
 */
export function* getTerminationInformation( payload ) {
    const {
        termination_id: terminationId,
        employee_id: employeeId
    } = payload;

    try {
        const { data } = yield call( Fetch, `/termination_informations/${terminationId}`, {
            method: 'GET',
            params: {
                include_final_pay: 1
            }
        });

        const {
            final_pay_projection_job_id: finalPayProjectionJobId,
            final_pay_projected: finalPayProjected
        } = data.attributes;

        if ( finalPayProjectionJobId ) {
            if ( !finalPayProjected ) {
                yield call( checkCalculateStatus, {
                    job_id: finalPayProjectionJobId,
                    employee_id: employeeId,
                    termination_id: terminationId
                });
            } else {
                yield put({
                    type: SET_TERMINATION_DETAILS,
                    payload: data
                });
            }
        } else {
            yield call( browserHistory.push, `/employee/${employeeId}/termination/${terminationId}/checklist`, true );
        }
    } catch ( error ) {
        yield call( notifyUser, error );
        yield call( browserHistory.push, `/employee/${employeeId}/termination/${terminationId}/checklist`, true );
    }
}

/**
 * Checks status of job ID
 * @param {string} jobId - Calculate Projected Final Pay Job ID
 */
export function* checkCalculateStatus( payload ) {
    const {
        job_id: jobId,
        employee_id: employeeId,
        termination_id: terminationId
    } = payload;

    try {
        const { data } = yield call( Fetch, `/payroll/0/calculate/status?job_id=${jobId}&include=errors` );

        const {
            attributes: {
                status
            },
            relationships: {
                errors
            }
        } = data;

        if ( errors.data ) {
            yield call( notifyUser, {
                show: true,
                title: 'Calculation failed',
                message: 'Errors encountered during calculation. Please try again.',
                type: 'error'
            });
            yield call( browserHistory.replace, `/employee/${employeeId}/termination/${terminationId}/checklist`, true );
        } else if ( status === 'FINISHED' ) {
            yield call( getTerminationInformation, {
                termination_id: terminationId,
                employee_id: employeeId
            });
        } else {
            yield put({ type: SET_PAGE_STATUS, payload: PAGE_STATUSES.CALCULATING });
            yield call( delay, 3000 );
            yield call( checkCalculateStatus, payload );
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Sends request to save final pay details
 * @param {object} payload - Payload object
 * @param {number} payload.final_pay_id - Final Pay ID
 * @param {number} payload.employee_id - Employee ID
 */
export function* saveFinalPayDetails({ payload }) {
    try {
        yield put({ type: SET_PAGE_STATUS, payload: PAGE_STATUSES.SAVING });

        yield call( Fetch, `/final_pay/${payload.final_pay_id}`, {
            method: 'PATCH',
            data: {
                data: {
                    employee_id: payload.employee_id,
                    company_id: company.getLastActiveCompanyId()
                }
            }
        });

        yield call( browserHistory.push, `/employee/${payload.employee_id}`, true );
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({ type: SET_PAGE_STATUS, payload: PAGE_STATUSES.READY });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.message ? error.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SAVE_FINAL_PAY_DETAILS
 */
export function* watchForSaveFinalPayDetails() {
    const watcher = yield takeEvery( SAVE_FINAL_PAY_DETAILS, saveFinalPayDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSaveFinalPayDetails,
    watchForInitializeData,
    watchForReinitializePage,
    watchNotify
];
