import React from 'react';

import Table from 'components/Table';
import { formatCurrency } from 'utils/functions';
import { H5, H6 } from 'components/Typography';

import { SectionWrapper, BreakdownWrapper, LoadingWrapper } from './styles';

/**
 * Formats value for display
 * @param {number} value
 * @returns {string}
 */
function formatValueForDisplay( value ) {
    const isNegative = Number( value ) < 0;
    const formatted = formatCurrency( Math.abs( value ) );
    return isNegative ? `( ${formatted} )` : formatted;
}

/**
 * Generates config for Table columns
 * @returns {object[]} column config
 */
function getTableColumns() {
    return [
        {
            header: 'Gross Income',
            accessor: 'gross_income',
            sortable: false,
            minWidth: 140,
            render: ({ value }) => formatValueForDisplay( value ),
            className: 'align-center',
            headerClassName: 'align-center'
        },
        {
            header: 'Non-Taxable Income',
            accessor: 'non_taxable_income',
            sortable: false,
            minWidth: 185,
            render: ({ value }) => formatValueForDisplay( value ),
            className: 'align-center',
            headerClassName: 'align-center'
        },
        {
            header: 'Taxable Income',
            accessor: 'taxable_income',
            sortable: false,
            minWidth: 150,
            render: ({ value }) => formatValueForDisplay( value ),
            className: 'align-center',
            headerClassName: 'align-center'
        },
        {
            header: 'Withholding Tax as Adjusted',
            accessor: 'withholding_tax_as_adjusted',
            sortable: false,
            minWidth: 250,
            render: ({ value }) => formatValueForDisplay( value ),
            className: 'align-center',
            headerClassName: 'align-center'
        },
        {
            header: 'Net Pay',
            accessor: 'net_pay',
            sortable: false,
            minWidth: 100,
            render: ({ value }) => formatValueForDisplay( value ),
            className: 'align-center',
            headerClassName: 'align-center'
        }
    ];
}

/**
 * Payroll Item component
 */
function Item({ label, amount }) {
    return (
        <div className="item">
            <div className="item-name">{ label }</div>
            <div className="item-value">{ formatValueForDisplay( amount ) }</div>
        </div>
    );
}

Item.propTypes = {
    label: React.PropTypes.string.isRequired,
    amount: React.PropTypes.number.isRequired
};

/**
 * Final Pay Computation component
 */
function FinalPayComputation({ data, loading, hasPayroll }) {
    const {
        taxable_incomes: taxableIncomeItems,
        non_taxable_incomes: nonTaxableIncomeItems,
        deductions: deductionItems,
        ...restOfData
    } = data;

    const tableData = [restOfData];

    /**
     * Renders final pay items
     */
    function renderItems( items, additionalClass = '' ) {
        return (
            <div className={ `items ${additionalClass}` }>
                { items.map( ( item, index ) => (
                    <Item key={ index } { ...item } />
                ) ) }
            </div>
        );
    }

    return (
        <SectionWrapper>
            <div className="section-heading">
                <H5>{ hasPayroll ? 'Calculated' : 'Projected' } Final Pay</H5>
            </div>
            { ( loading || !data ) ? (
                <LoadingWrapper>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw loader" />
                    <div className="description">
                        <H6>Calculating projected final pay...</H6>
                    </div>
                </LoadingWrapper>
            ) : (
                <div className="section-body">
                    <Table
                        data={ tableData }
                        columns={ getTableColumns() }
                    />

                    <BreakdownWrapper>
                        <div className="column">
                            <H5>Taxable Income</H5>
                            { renderItems( taxableIncomeItems ) }
                        </div>
                        <div className="column">
                            <H5>Non-Taxable Income</H5>
                            { renderItems( nonTaxableIncomeItems ) }
                        </div>
                        <div className="column">
                            <H5>Deductions</H5>
                            { renderItems( deductionItems ) }
                        </div>
                        <div className="column">
                            <H5>Net Pay</H5>
                            <div className="items net-pay">
                                <Item label="" amount={ data.net_pay } />
                            </div>
                        </div>
                    </BreakdownWrapper>
                </div>
            ) }
        </SectionWrapper>
    );
}

FinalPayComputation.propTypes = {
    data: React.PropTypes.shape({
        gross_income: React.PropTypes.number.isRequired,
        non_taxable_income: React.PropTypes.number.isRequired,
        taxable_income: React.PropTypes.number.isRequired,
        withholding_tax_as_adjusted: React.PropTypes.number.isRequired,
        net_pay: React.PropTypes.number.isRequired,
        taxable_incomes: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                label: React.PropTypes.string.isRequired,
                amount: React.PropTypes.number.isRequired
            })
        ).isRequired,
        non_taxable_incomes: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                label: React.PropTypes.string.isRequired,
                amount: React.PropTypes.number.isRequired
            })
        ).isRequired,
        deductions: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                label: React.PropTypes.string.isRequired,
                amount: React.PropTypes.number.isRequired
            })
        ).isRequired
    }),
    loading: React.PropTypes.bool,
    hasPayroll: React.PropTypes.bool
};

FinalPayComputation.defaultProps = {
    loading: false,
    hasPayroll: false
};

export default FinalPayComputation;
