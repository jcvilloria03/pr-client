import styled from 'styled-components';

export const SectionWrapper = styled.section`
    background-color: #fff;
    border: 1px solid #e5e5e5;
    border-left: 4px solid #00a5e5;
    padding: 25px;
    margin-bottom: 75px;

    .section-heading {
        display: flex;
        align-items: center;
        justify-content: space-between;
        border-bottom: 1px solid #e5e5e5;
    }

    .section-body {
        padding-top: 25px;
    }

    .align-center {
        text-align: center !important;
        justify-content: center;
    }
`;

export const BreakdownWrapper = styled.section`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    flex-wrap: wrap;
    margin-top: 32px;

    .column {
        flex: 1;
        display: flex;
        flex-direction: column;
        margin: 0 8px 8px;
    }

    .items {
        padding-left: 10px;
        padding-right: 24px;
    }

    .items:not( .net-pay ) {
        flex: 1;
        padding-bottom: 12px;
    }

    .item {
        display: flex;
        align-items: center;
        justify-content: space-between;
        line-height: 1.5;
        margin: 8px 0;
    }

    .item-name {
        margin-right: 8px;
    }

    .item-value {
        margin-left: 8px;
        white-space: nowrap;
    }

    .items, h5 {
        border: 1px solid rgba( 0, 0, 0, 0.1 );
    }

    h5 {
        margin-bottom: 0;
        padding: 8px 4px;
        border-bottom: 0;
    }
`;

export const LoadingWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 80px;

    .loader {
        font-size: 3em;
    }

    .description {
        padding-top: 20px;
        text-align: center;

        h6 {
            font-weight: 600;
            margin: 0;
        }
    }
`;
