import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectEmployeeTerminationComputationDomain = () => ( state ) => state.get( 'employeeTerminationComputation' );

const makeSelectEmployee = () => createSelector(
    selectEmployeeTerminationComputationDomain(),
    ( substate ) => substate.get( 'employee' ).toJS()
);

const makeSelectTerminationDetails = () => createSelector(
    selectEmployeeTerminationComputationDomain(),
    ( substate ) => substate.get( 'terminationDetails' ).toJS()
);

const makeSelectPageStatus = () => createSelector(
    selectEmployeeTerminationComputationDomain(),
    ( substate ) => substate.get( 'page_status' )
);

const makeSelectNotification = () => createSelector(
    selectEmployeeTerminationComputationDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectEmployeeTerminationComputationDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectErrors,
    makeSelectTerminationDetails,
    makeSelectNotification,
    makeSelectPageStatus,
    makeSelectEmployee
};
