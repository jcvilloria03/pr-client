import styled from 'styled-components';

export const LoadingWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100vh;

    .loader {
        font-size: 7em;
        align-self: center;
    }

    .description {
        padding-top: 40px;
        text-align: center;

        h3 {
            font-weight: 600;
        }
    }
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    padding-top: 85px;
`;

export const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;

    h3 {
        font-weight: 600;
    }

    p {
        text-align: center;
        max-width: 800px;
    }
`;

export const FormWrapper = styled.div`
    padding: 20px 10%;
    width: 100%;
    height: 100%;
`;

export const ModalBodyWrapper = styled.div`
    padding-bottom: 40px;
`;
