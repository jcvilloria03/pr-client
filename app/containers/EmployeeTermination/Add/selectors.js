import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectAddEmployeeTerminationDomain = () => ( state ) => state.get( 'addEmployeeTermination' );

const makeSelectFormOptions = () => createSelector(
    selectAddEmployeeTerminationDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectAddEmployeeTerminationDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectLoading = () => createSelector(
    selectAddEmployeeTerminationDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectAddEmployeeTerminationDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectAddEmployeeTerminationDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectErrors,
    makeSelectFormOptions,
    makeSelectNotification,
    makeSelectSubmitted,
    makeSelectLoading
};
