export const INITIALIZE = 'app/EmployeeTermination/Add/INITIALIZE';
export const NOTIFICATION = 'app/EmployeeTermination/Add/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/EmployeeTermination/Add/NOTIFICATION_SAGA';
export const SET_ERRORS = 'app/EmployeeTermination/Add/SET_ERRORS';
export const SET_FORM_OPTIONS = 'app/EmployeeTermination/Add/SET_FORM_OPTIONS';
export const SUBMITTED = 'app/EmployeeTermination/Add/SUBMITTED';
export const LOADING = 'app/EmployeeTermination/Add/LOADING';
export const SUBMIT_FORM = 'app/EmployeeTermination/Add/SUBMIT_FORM';

export const REASONS_FOR_LEAVING = [
    { value: 'TERMINATION', label: 'Termination' },
    { value: 'RESIGNATION', label: 'Resignation' },
    { value: 'RETIREMENT', label: 'Retirement' }
];

export const MAX_REMARKS_LENGTH = 260;
