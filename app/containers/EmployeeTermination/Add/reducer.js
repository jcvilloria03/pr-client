import { fromJS } from 'immutable';
import {
    SET_FORM_OPTIONS,
    SET_ERRORS,
    SUBMITTED,
    LOADING,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    errors: {},
    submitted: false,
    loading: false,
    formOptions: {
        reasonsForLeaving: [],
        employee: {}
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Add employee termination manually
 *
 */
function addEmployeeTermination( state = initialState, action ) {
    switch ( action.type ) {
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addEmployeeTermination;
