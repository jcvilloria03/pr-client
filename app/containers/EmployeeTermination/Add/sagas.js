import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';

import {
    INITIALIZE,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_ERRORS,
    SET_FORM_OPTIONS,
    SUBMITTED,
    LOADING,
    SUBMIT_FORM,
    REASONS_FOR_LEAVING
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { INACTIVE } from '../../Employee/View/constants';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({ type: LOADING, payload: true });

        const employee = yield call( Fetch, `/employee/${payload.id}`, { method: 'GET' });

        if ( employee.active === INACTIVE ) {
            yield call( notifyUser, {
                show: true,
                title: '',
                message: 'Terminating employee not allowed because the employee\'s status is Inactive',
                type: 'error'
            });
            yield call( browserHistory.replace, `/employee/${payload.id}`, true );
            return;
        }

        const formOptions = {};
        formOptions.reasonsForLeaving = REASONS_FOR_LEAVING;
        formOptions.employee = employee;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
        yield call( browserHistory.push, `/employee/${payload.id}`, true );
    } finally {
        yield put({ type: LOADING, payload: false });
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        const { employee_id, ...rest } = payload; // eslint-disable-line

        const data = {
            employee_id,
            ...rest,
            company_id: company.getLastActiveCompanyId()
        };

        const terminationInformation = yield call( Fetch, '/termination_informations', { method: 'POST', data });

        yield call( delay, 500 );
        yield call( browserHistory.push, `/employee/${payload.employee_id}/termination/${terminationInformation.data.id}/checklist`, true );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, toErrorPayload( error ) );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * converts api error to notification payload
 */
function toErrorPayload( error ) {
    return {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForInitializeData,
    watchForReinitializePage,
    watchNotify
];
