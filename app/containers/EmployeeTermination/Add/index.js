import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from 'components/A';
import SnackBar from 'components/SnackBar';
import DatePicker from 'components/DatePicker';
import Input from 'components/Input';
import SalSelect from 'components/Select';
import Loader from 'components/Loader';
import Button from 'components/Button';
import { H2, H3, H5 } from 'components/Typography';
import ToolTip from 'components/ToolTip';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';
import {
    isValid,
    formatDate,
    isAfter,
    isSameOrAfter
} from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import {
    makeSelectNotification,
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectLoading
} from './selectors';

import { MAX_REMARKS_LENGTH } from './constants';

import * as createTerminationInformation from './actions';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper,
    SectionWrapper,
    Footer,
    LoadingStyles
} from './styles';

const inputTypes = {
    datePicker: [ 'last_date', 'proposed_release_date', 'start_date_basis' ]
};

/**
 * Add Termination Information Component
 */
class Add extends Component {
    static propTypes = {
        params: React.PropTypes.object,
        submitted: React.PropTypes.bool,
        loading: React.PropTypes.bool,
        formOptions: React.PropTypes.object,
        submitForm: React.PropTypes.func,
        initializeData: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    };

    constructor( props ) {
        super( props );
        this.state = {
            terminationDetails: {
                last_date: null,
                start_date_basis: null,
                proposed_release_date: null,
                reason_for_leaving: '',
                remarks: ''
            }
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.terminations'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        this.props.initializeData( parseInt( this.props.params.id, 10 ) );
    }

    validateTerminationDetails = () => {
        let valid = true;

        if ( !this.reason_for_leaving._checkRequire( this.reason_for_leaving.state.value ) ) {
            valid = false;
        }

        if ( this.last_date.checkRequired() ) {
            valid = false;
        }

        if ( this.start_date_basis.checkRequired() ) {
            valid = false;
        }

        if ( this.proposed_release_date.checkRequired() ) {
            valid = false;
        }

        if ( this.state.terminationDetails.remarks && !this.remarks._checkMax( this.remarks.state.value ) ) {
            valid = false;
        }

        if ( this.state.terminationDetails.proposed_release_date
            && this.state.terminationDetails.last_date
            && !isSameOrAfter( this.state.terminationDetails.proposed_release_date, this.state.terminationDetails.last_date, [DATE_FORMATS.API])
        ) {
            const message = 'Proposed Final Pay Release Date should be greater than or equal to the employee\'s last day';
            this.proposed_release_date.setState({ error: true, message });

            valid = false;
        }

        if ( this.state.terminationDetails.start_date_basis
            && this.state.terminationDetails.last_date
            && !isSameOrAfter( this.state.terminationDetails.last_date, this.state.terminationDetails.start_date_basis, [DATE_FORMATS.API])
        ) {
            const message = 'Final Pay Start Date Basis should be less than or equal to the employee\'s last day';
            this.start_date_basis.setState({ error: true, message });

            valid = false;
        }

        if ( this.state.terminationDetails.last_date
            && !isAfter( this.state.terminationDetails.last_date, this.props.formOptions.employee.date_hired, [DATE_FORMATS.API])
        ) {
            const message = 'The last day must be a date after date hired.';
            this.last_date.setState({ error: true, message });

            valid = false;
        }

        return valid;
    }

    updateTerminationDetails( key, value ) {
        if ( typeof value === 'undefined' ) {
            return;
        }

        if ( inputTypes.datePicker.includes( key ) ) {
            this.state.terminationDetails[ key ] = value;
            return;
        }

        const dataClone = { ...this.state.terminationDetails };
        dataClone[ key ] = value !== '' && value !== null ? value : '';

        this.setState({ terminationDetails: Object.assign( this.state.terminationDetails, dataClone ) });
    }

    submitForm = () => {
        if ( this.validateTerminationDetails() ) {
            const data = {
                ...this.state.terminationDetails,
                employee_id: parseInt( this.props.params.id, 10 )
            };

            this.props.submitForm( data );
        }
    }

    renderLoading = () => (
        <LoadingStyles>
            <H2>Loading Termination Details.</H2>
            <br />
            <H3>Please wait...</H3>
        </LoadingStyles>
    )

    renderForm = () => (
        <div>
            <div>
                <div className="row">
                    <div className="col-xs-4 date">
                        <DatePicker
                            label={
                                <span>
                                    <span>Final Pay Start Date Basis</span>
                                    <ToolTip
                                        id="start_date_basis_tip"
                                        target={ <i className="fa fa-info-circle" /> }
                                        placement="right"
                                    >
                                        The process attendance start date that will be fetched when the employee’s final pay is processed. If the start date basis is within a specific cut-off date, then the employee shall no longer be included in the regular payroll processing.
                                    </ToolTip>
                                </span>
                                    }
                            placeholder="Select Final Pay Basis"
                            required
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            ref={ ( ref ) => { this.start_date_basis = ref; } }
                            onChange={ ( value ) => {
                                const selectedDay = isValid( value, DATE_FORMATS.DISPLAY ) ? formatDate( value, DATE_FORMATS.API ) : null;
                                this.updateTerminationDetails( 'start_date_basis', selectedDay );
                            } }
                        />
                    </div>
                    <div className="col-xs-4 date">
                        <DatePicker
                            label={
                                <span>
                                    <span>Employee Last Day</span>
                                    <ToolTip
                                        id="last_date_tip"
                                        target={ <i className="fa fa-info-circle" /> }
                                        placement="right"
                                    >
                                            The employee&rsquo;s separation effectivity date
                                    </ToolTip>
                                </span>
                                    }
                            placeholder="Select Last Date"
                            required
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            ref={ ( ref ) => { this.last_date = ref; } }
                            onChange={ ( value ) => {
                                const selectedDay = isValid( value, DATE_FORMATS.DISPLAY ) ? formatDate( value, DATE_FORMATS.API ) : null;
                                this.updateTerminationDetails( 'last_date', selectedDay );
                            } }
                        />
                    </div>
                    <div className="col-xs-4 date">
                        <DatePicker
                            label={
                                <span>
                                    <span>Proposed Final Pay Release Date</span>
                                    <ToolTip
                                        id="proposed_release_date_tip"
                                        target={ <i className="fa fa-info-circle" /> }
                                        placement="right"
                                    >
                                            The suggested date when the employee&rsquo;s last pay shall be released
                                    </ToolTip>
                                </span>
                                    }
                            placeholder="Select Pay Date"
                            required
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            ref={ ( ref ) => { this.proposed_release_date = ref; } }
                            onChange={ ( value ) => {
                                const selectedDay = isValid( value, DATE_FORMATS.DISPLAY ) ? formatDate( value, DATE_FORMATS.API ) : null;
                                this.updateTerminationDetails( 'proposed_release_date', selectedDay );
                            } }
                        />
                    </div>
                </div>
                <div className="row" style={ { marginTop: '30px' } }>
                    <div className="col-xs-4">
                        <SalSelect
                            id="reason_for_leaving"
                            label="Reason For Leaving"
                            required
                            data={ this.props.formOptions.reasonsForLeaving }
                            placeholder="Select Reason"
                            ref={ ( ref ) => { this.reason_for_leaving = ref; } }
                            onChange={ ({ value }) => { this.updateTerminationDetails( 'reason_for_leaving', value ); } }
                        />
                    </div>
                </div>
                <div className="row" style={ { marginTop: '30px' } }>
                    <div className="col-xs-8">
                        <Input
                            id="remarks"
                            label="Remarks"
                            type="textarea"
                            placeholder="Remarks"
                            ref={ ( ref ) => { this.remarks = ref; } }
                            max={ MAX_REMARKS_LENGTH }
                            onChange={ ( value ) => { this.updateTerminationDetails( 'remarks', value ); } }
                        />
                    </div>
                </div>
            </div>
        </div>
    )

    /**
     * Component Render Method
     */
    render() {
        return (
            <PageWrapper>
                <Helmet
                    title="Add Termination Information"
                    meta={ [
                        { name: 'description', content: 'Add Termination Information' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( `/employee/${this.props.params.id}`, true );
                            } }
                        >
                            &#8592; Back to Profile
                        </A>
                    </Container>
                </NavWrapper>
                { this.props.loading ? this.renderLoading() : (
                    <div>
                        <Container>
                            <HeadingWrapper>
                                <h3>Termination Process</h3>
                            </HeadingWrapper>
                        </Container>
                        <FormWrapper>
                            <SectionWrapper>
                                <div className="section-heading">
                                    <H5>Termination Details</H5>
                                </div>
                                <div className="section-body">
                                    { this.renderForm() }
                                </div>
                            </SectionWrapper>
                        </FormWrapper>
                        <Footer>
                            <Button
                                label={ this.props.submitted ? <Loader /> : 'Proceed to Final Pay' }
                                disabled={ this.props.submitted }
                                type="action"
                                size="large"
                                onClick={ this.submitForm }
                                ref={ ( ref ) => { this.submitButton = ref; } }
                            />
                        </Footer>
                    </div>
                ) }
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    formOptions: makeSelectFormOptions(),
    submitted: makeSelectSubmitted(),
    loading: makeSelectLoading()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        createTerminationInformation,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Add );
