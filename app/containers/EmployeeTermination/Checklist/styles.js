import styled from 'styled-components';

const PageWrapper = styled.div`

    .selected .fill {
        fill: #fff;
    }

    .selected .stroke {
        stroke: #fff;
    }

    .fill {
        fill: rgb(0,165,226);
    }

    .stroke {
        stroke: rgb(0,165,226);
    }

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        margin-top: 20px;

        button {
            min-width: 120px;
        }
    }
`;

const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    padding-top: 85px;
`;

const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;

    h3 {
        font-weight: 600;
    }

    p {
        text-align: center;
        max-width: 800px;
    }
`;

const FormWrapper = styled.div`
    padding: 40px 10%;
    width: 100%;
    height: 100%;

    .date {
        & > div > span {
            display: block;
        }
        .DayPickerInput {
            display: block;

            input {
                width: 100%;
            }
        }
    }

    > div {
        & > h4 {
            text-align: left;
        }

        & > button {
            padding: 12px 40px;
            margin-top: 2rem;
        }

        .steps {
            display: flex;
            align-items: stretch;
            justify-content: space-between;
            margin: 1rem 0;

            > .step {
                width: 49%;
            }
        }

        .template,
        .upload {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
            width: 100%;
            height: 100%;
            padding: 2rem;
            text-align: center;
            border: 2px dashed #ccc;
            border-radius: 12px;

            .sl-c-btn--wide {
                min-width: 12rem;
            }
        }

        .upload > div:first-of-type > div > div {
            margin: 0 auto;
        }

        .errors {
            .rt-thead .rt-tr {
                background: #F21108;
                color: #fff;
            }
            .rt-tbody .rt-tr {
                background: rgba(242, 17, 8, .1)
            }
            .ReactTable.-striped .rt-tr.-odd {
                background: rgba(242, 17, 8, .2)
            }

            .ReactTable .rt-th,
            .ReactTable .rt-td {
                flex: 1 0 0px;
                white-space: initial;
                text-overflow: ellipsis;
                padding: 7px 20px;
                overflow: hidden;
                transition: 0.3s ease;
                transition-property: width, min-width, padding, opacity;
            }

            .react-bs-container-body tr {
                background: rgba(249, 210, 210, 0.8);

                &:hover {
                    background: rgba(249, 210, 210, 1);
                }
            }
        }
    }
`;

const SectionWrapper = styled.div`
    background-color: #fff;
    border: 1px solid #e5e5e5;
    border-left: 4px solid #00a5e5;
    padding: 25px;

    .section-heading {
        display: flex;
        align-items: center;
        justify-content: space-between;
        border-bottom: 1px solid #e5e5e5;

        h5 {
            padding-top: 5px;
        }

        button {
            margin-bottom: 20px;
        }

        a {
            border-bottom: none;

            button {
                background-color: #4aba4a;
                border-radius: 50%;
                width: 30px;
                height: 30px;
                border: none;
                outline: none;
                cursor: pointer;

                &:disabled {
                    cursor: not-allowed;
                    opacity: .65;
                }

                i {
                    color: #fff;
                }
            }
        }
    }

    .section-body {
        padding-top: 25px;

        h4 {
            font-weight: 700;
            padding-left: 35px;
        }

        > div {
            &.section-column {
                > div {
                    margin-bottom: 25px;
                }
            }

            > div {
                .section-row {
                    display: flex;
                    flex-wrap: wrap;
                    padding-left: 35px;

                    > div {
                        margin-bottom: 25px;

                        .radio-group {
                            > div:first-child{
                                padding-left: 0 !important;
                            }
                        }
                    }

                    &.edit {
                        > div:not(:last-child) {
                            margin-bottom: 0;
                        }
                    }
                }
            }
        }

        .content {
            .heading {
                display: flex;
                align-items: center;
                flex-direction: column;
                margin: 0 auto 50px auto;

                h3 {
                    font-weight: 600;
                }

                p {
                    text-align: center;
                    max-width: 800px;
                }
            }

            .title {
                display: flex;
                align-items: center;
                margin-bottom: 20px;

                h5 {
                    margin: 0;
                    margin-right: 20px;
                    font-weight: 600;
                    font-size: 20px;
                }
            }
        }
    }
`;

const ValueWrapper = styled.div`
    padding-top: 15px;
    padding-left: 12.5px;
    padding-bottom: 30px;
`;

export {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper,
    SectionWrapper,
    Footer,
    LoadingStyles,
    ValueWrapper
};
