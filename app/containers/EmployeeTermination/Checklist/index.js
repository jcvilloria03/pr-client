import React from 'react';
import Helmet from 'react-helmet';
import startCase from 'lodash/startCase';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import get from 'lodash/get';

import A from 'components/A';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import ToolTip from 'components/ToolTip';
import Button from 'components/Button';
import { H2, H3, H5 } from 'components/Typography';

import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';
import { isAuthorized } from 'utils/Authorization';
import { formatDate } from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import EmployeeBonuses from './templates/sections/bonuses';
import EmployeeAdjustments from './templates/sections/adjustments';
import EmployeeLoans from './templates/sections/loans';
import EmployeeDeductions from './templates/sections/deductions';
import EmployeeAllowances from './templates/sections/allowances';
import EmployeeCommissions from './templates/sections/commissions';
import EmployeeContributions from './templates/sections/contributions';

import {
    makeSelectNotification,
    makeSelectTerminationDetails,
    makeSelectSubmitted,
    makeSelectLoading,
    makeSelectEmployee,
    makeSelectAvailableItems
 } from './selectors';

import { INITIAL_PERMISSIONS, FINAL_PAY_STATUSES } from './constants';

import * as editTerminationDetails from './actions';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper,
    SectionWrapper,
    Footer,
    LoadingStyles,
    ValueWrapper
} from './styles';

/**
 * Edit Termination Information Component
 */
class Checklist extends React.PureComponent {
    static propTypes = {
        params: React.PropTypes.object,
        submitted: React.PropTypes.bool,
        loading: React.PropTypes.bool,
        terminationDetails: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        employee: React.PropTypes.object,
        products: React.PropTypes.array,
        availableItemsDetails: React.PropTypes.object,
        resetStore: React.PropTypes.func,
        saveFinalPayDetailsAndCalculate: React.PropTypes.func
    };

    constructor( props ) {
        super( props );
        this.state = {
            terminationDetails: {},
            bonusPermission: INITIAL_PERMISSIONS,
            adjustmentPermission: INITIAL_PERMISSIONS,
            loanPermission: INITIAL_PERMISSIONS,
            deductionPermission: INITIAL_PERMISSIONS,
            allowancePermission: INITIAL_PERMISSIONS,
            commissionPermission: INITIAL_PERMISSIONS,
            contributionPermission: INITIAL_PERMISSIONS,
            isFinalPayStatusClosedOrDisbursed: false
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.terminations'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        this.props.initializeData( parseInt( this.props.params.id, 10 ), parseInt( this.props.params.terminationId, 10 ) );
    }

    componentWillReceiveProps( nextProps ) {
        if ( Object.keys( nextProps.terminationDetails ).length !== Object.keys( this.props.terminationDetails ).length ) {
            this.setTerminationDetails( nextProps.terminationDetails );
        }
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    setTerminationDetails( terminationDetails ) {
        const payrollStatus = get( terminationDetails, 'payroll.status', '' );
        const isFinalPayStatusClosedOrDisbursed = payrollStatus === FINAL_PAY_STATUSES.CLOSED || payrollStatus === FINAL_PAY_STATUSES.DISBURSED;

        this.setState({
            terminationDetails,
            isFinalPayStatusClosedOrDisbursed
        });
    }

    calculateProjectedFinalPay = () => {
        const { employee } = this.props;
        const allowances = this.employeeAllowances.getFinalPayPayload();
        const adjustments = this.employeeAdjustments.getFinalPayPayload();
        const bonuses = this.employeeBonuses.getFinalPayPayload();
        const commissions = this.employeeCommissions.getFinalPayPayload();
        const deductions = this.employeeDeductions.getFinalPayPayload();
        const loans = this.employeeLoans.getFinalPayPayload();
        const contributions = this.employeeContributions.getFinalPayPayload();

        const payload = {
            data: {
                termination_information_id: this.state.terminationDetails.id,
                employee_id: employee.data.id,
                company_id: employee.data.company_id,
                final_pay_items: [
                    ...allowances,
                    ...adjustments,
                    ...bonuses,
                    ...commissions,
                    ...deductions,
                    ...loans,
                    ...contributions
                ]
            }
        };

        this.props.saveFinalPayDetailsAndCalculate( payload );
    }

    /**
     * Renders allowances for employee
     */
    renderEmployeeAllowances() {
        const { employee, availableItemsDetails } = this.props;

        return employee.data.payroll && (
            <EmployeeAllowances
                ref={ ( ref ) => { this.employeeAllowances = ref; } }
                loading={ employee.loadingAllowances }
                allowances={ availableItemsDetails.allowance || [] }
                employee={ employee.data }
                permission={ this.state.allowancePermission }
                key="allowance"
                disabled={ this.state.isFinalPayStatusClosedOrDisbursed }
            />
        );
    }

    /**
     * Renders adjustments for employee
     */
    renderEmployeeAdjustments() {
        const { employee, availableItemsDetails } = this.props;

        return employee.data.payroll && (
            <EmployeeAdjustments
                ref={ ( ref ) => { this.employeeAdjustments = ref; } }
                loading={ employee.loadingAdjustments }
                adjustments={ availableItemsDetails.adjustment || [] }
                employee={ employee.data }
                permission={ this.state.adjustmentPermission }
                key="adjustment"
                disabled={ this.state.isFinalPayStatusClosedOrDisbursed }
            />
        );
    }

    /**
     * Renders bonuses for employee
     */
    renderEmployeeBonuses() {
        const { employee, availableItemsDetails } = this.props;

        return employee.data.payroll && (
            <EmployeeBonuses
                ref={ ( ref ) => { this.employeeBonuses = ref; } }
                loading={ employee.loadingBonuses }
                bonuses={ availableItemsDetails.bonus || [] }
                employee={ employee.data }
                permission={ this.state.bonusPermission }
                key="bonus"
                disabled={ this.state.isFinalPayStatusClosedOrDisbursed }
            />
        );
    }

    /**
     * Renders commissions for employee
     */
    renderEmployeeCommissions() {
        const { employee, availableItemsDetails } = this.props;

        return employee.data.payroll && (
            <EmployeeCommissions
                ref={ ( ref ) => { this.employeeCommissions = ref; } }
                loading={ employee.loadingCommissions }
                commissions={ availableItemsDetails.commission || [] }
                employee={ employee.data }
                permission={ this.state.commissionPermission }
                key="commission"
                disabled={ this.state.isFinalPayStatusClosedOrDisbursed }
            />
        );
    }

    /**
     * Renders deductions for employee
     */
    renderEmployeeDeductions() {
        const { employee, availableItemsDetails } = this.props;

        return employee.data.payroll && (
            <EmployeeDeductions
                ref={ ( ref ) => { this.employeeDeductions = ref; } }
                loading={ employee.loadingDeductions }
                deductions={ availableItemsDetails.deduction || [] }
                employee={ employee.data }
                permission={ this.state.deductionPermission }
                key="deduction"
                disabled={ this.state.isFinalPayStatusClosedOrDisbursed }
            />
        );
    }

    /**
     * Renders loans for employee
     */
    renderEmployeeLoans() {
        const { employee, availableItemsDetails } = this.props;

        return employee.data.payroll && (
            <EmployeeLoans
                ref={ ( ref ) => { this.employeeLoans = ref; } }
                loading={ employee.loadingLoans }
                loans={ availableItemsDetails.loan || [] }
                employee={ employee.data }
                permission={ this.state.loanPermission }
                key="loan"
                disabled={ this.state.isFinalPayStatusClosedOrDisbursed }
            />
        );
    }

    /**
     * Renders contributions for employee
     */
    renderEmployeeContributions() {
        const { employee, terminationDetails, availableItemsDetails } = this.props;

        return employee.data.payroll && (
            <EmployeeContributions
                ref={ ( ref ) => { this.employeeContributions = ref; } }
                loading={ employee.loadingContributions }
                contributions={ availableItemsDetails.contribution }
                employee={ employee.data }
                terminationDetails={ terminationDetails }
                permission={ this.state.contributionPermission }
                key="contribution"
                disabled={ this.state.isFinalPayStatusClosedOrDisbursed }
            />
        );
    }

    renderLoading = () => (
        <LoadingStyles>
            <H2>Loading...</H2>
            <br />
            <H3>Please wait...</H3>
        </LoadingStyles>
    )

    renderTerminationInformation = () => {
        const { terminationDetails } = this.state;
        return (
            <div>
                <div className="section-row row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-4">
                        <div>Final Pay Start Date Basis:</div>
                        <ValueWrapper>{ formatDate( terminationDetails.start_date_basis, DATE_FORMATS.DISPLAY ) }</ValueWrapper>
                    </div>
                    <div className="col-xs-4">
                        <div>Employee Last Day:</div>
                        <ValueWrapper>{ formatDate( terminationDetails.last_date, DATE_FORMATS.DISPLAY ) }</ValueWrapper>
                    </div>
                    <div className="col-xs-4">
                        <div>Proposed Final Pay Release Date:</div>
                        <ValueWrapper>{ formatDate( terminationDetails.proposed_release_date, DATE_FORMATS.DISPLAY ) }</ValueWrapper>
                    </div>
                </div>
                <div className="section-row row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-4">
                        <div>Reason for Leaving:</div>
                        <ValueWrapper>{ terminationDetails.reason_for_leaving ? startCase( terminationDetails.reason_for_leaving.toLowerCase() ) : '' }</ValueWrapper>
                    </div>
                </div>
                <div className="section-row row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-7">
                        <div>Remarks:</div>
                        <ValueWrapper>{ terminationDetails.remarks }</ValueWrapper>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * Component Render Method
     */
    render() {
        return (
            <PageWrapper>
                <Helmet
                    title="Edit Termination Information"
                    meta={ [
                        { name: 'description', content: 'Edit Termination Information' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( `/employee/${this.props.params.id}`, true );
                            } }
                        >
                            &#8592; Back to Profile
                        </A>
                    </Container>
                </NavWrapper>
                <div>
                    <Container>
                        <HeadingWrapper>
                            <h3>Termination Process</h3>
                        </HeadingWrapper>
                    </Container>
                    <FormWrapper>
                        <SectionWrapper>
                            <div className="section-heading">
                                <H5>Termination Details</H5>
                                <ToolTip
                                    id="tooltipTermination"
                                    target={
                                        <button
                                            id="editTermination"
                                            disabled={ this.props.submitted || this.props.loading || this.state.isFinalPayStatusClosedOrDisbursed }
                                            className="edit"
                                            onClick={ () => {
                                                browserHistory.push( `/employee/${this.props.params.id}/termination/${this.props.params.terminationId}/edit`, true );
                                            } }
                                            ref={ ( ref ) => { this.submitButton = ref; } }
                                        >
                                            <i className={ 'fa fa-pencil edit' }></i>
                                        </button>
                                    }
                                    placement="top"
                                >
                                    Edit termination
                                </ToolTip>
                            </div>
                            <div className="section-body">
                                { this.props.loading ? this.renderLoading() : this.renderTerminationInformation() }
                            </div>
                        </SectionWrapper>
                        { !this.props.loading && [
                            this.renderEmployeeAllowances(),
                            this.renderEmployeeAdjustments(),
                            this.renderEmployeeBonuses(),
                            this.renderEmployeeCommissions(),
                            this.renderEmployeeDeductions(),
                            this.renderEmployeeLoans(),
                            this.renderEmployeeContributions()
                        ] }
                    </FormWrapper>
                    <Footer>
                        <Button
                            label={ this.props.submitted ? <Loader /> : 'Calculate Final Pay' }
                            disabled={ this.props.submitted || this.props.loading || this.state.isFinalPayStatusClosedOrDisbursed }
                            type="action"
                            size="default"
                            onClick={ this.calculateProjectedFinalPay }
                            ref={ ( ref ) => { this.submitButton = ref; } }
                        />
                    </Footer>
                </div>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    submitted: makeSelectSubmitted(),
    loading: makeSelectLoading(),
    terminationDetails: makeSelectTerminationDetails(),
    employee: makeSelectEmployee(),
    availableItemsDetails: makeSelectAvailableItems()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        editTerminationDetails,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Checklist );
