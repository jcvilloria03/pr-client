import {
    INITIALIZE,
    SUBMIT,
    NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData( id, terminationId ) {
    return {
        type: INITIALIZE,
        payload: {
            id,
            terminationId
        }
    };
}

/**
 * Display notification in page
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Submit a new deduction
 * @param {object} payload
 */
export function saveFinalPayDetailsAndCalculate( payload ) {
    return {
        type: SUBMIT,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
