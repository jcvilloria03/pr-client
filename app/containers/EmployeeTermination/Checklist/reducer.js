import { fromJS } from 'immutable';
import toLower from 'lodash/toLower';
import has from 'lodash/has';

import {
    SET_ERRORS,
    SUBMITTED,
    LOADING,
    NOTIFICATION_SAGA,
    SET_TERMINATION_DETAILS,
    SET_EMPLOYEE,
    SET_EMPLOYEE_AVAILABLE_ITEMS_DETAILS,
    SET_EMPLOYEE_LOADING_AVAILABLE_ITEMS_DETAILS
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    errors: {},
    submitted: false,
    loading: false,
    terminationDetails: {
        last_date: null,
        pay_date: null,
        reason_for_leaving: '',
        remarks: '',
        disburse_through_special_pay_run: false
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    employee: {
        data: {},
        loadingAvailableItemsDetails: true
    },
    availableItemsDetails: {}
});

/**
 * Transforms termination details from API to accommodated structure in application
 * @param {Object} data - Employee termination details
 */
function prepareTerminationDetails({ id, type, attributes }) {
    return id && type
        ? { id, type, ...attributes }
        : {};
}

/**
 *
 * @param {array} transforms data from API to accomodate structure in app
 */
function prepareAvailableIncomes( data ) {
    return data.reduce( ( items, item ) => {
        const type = toLower( item.type );
        if ( !has( items, type ) ) {
            // eslint-disable-next-line no-param-reassign
            items[ type ] = [];
        }

        items[ type ].push({
            item_id: item.item_id,
            other_income_type: item.type,
            ...item.meta
        });

        return items;
    }, {});
}

/**
 *
 * Edit employee termination manually
 *
 */
function employeeTerminationChecklist( state = initialState, action ) {
    switch ( action.type ) {
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_TERMINATION_DETAILS:
            return state.set( 'terminationDetails', fromJS( prepareTerminationDetails( action.payload ) ) );
        case SET_EMPLOYEE:
            return state.setIn([ 'employee', 'data' ], fromJS( action.payload ) );
        case SET_EMPLOYEE_LOADING_AVAILABLE_ITEMS_DETAILS:
            return state.set( 'loadingAvailableItemsDetails', action.payload );
        case SET_EMPLOYEE_AVAILABLE_ITEMS_DETAILS:
            return state.set( 'availableItemsDetails', fromJS( prepareAvailableIncomes( action.payload ) ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default employeeTerminationChecklist;
