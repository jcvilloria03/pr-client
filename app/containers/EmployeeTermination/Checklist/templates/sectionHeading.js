import React from 'react';

import Button from '../../../../components/Button';
import { H5 } from '../../../../components/Typography';

/**
 * Section heading for employee records table
 */
const SectionHeading = ({ permission, heading, onAdd, createButtonLabel, disableAdd }) => (
    <div className="section-heading">
        <H5>{ heading }</H5>
        { onAdd && ( <Button
            className={ permission.create ? '' : 'hide' }
            label={ createButtonLabel }
            size="default"
            type="action"
            onClick={ onAdd }
            disabled={ disableAdd }
        /> ) }
    </div>
);

SectionHeading.propTypes = {
    permission: React.PropTypes.object,
    heading: React.PropTypes.string,
    onAdd: React.PropTypes.func,
    createButtonLabel: React.PropTypes.string,
    disableAdd: React.PropTypes.bool
};

SectionHeading.defaultProps = {
    disableAdd: false
};

export default SectionHeading;
