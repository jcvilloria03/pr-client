import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: '200px';
    min-height: ${( props ) => ( props.minHeight ? props.minHeight : '200px' )};
    max-height: ${( props ) => ( props.maxHeight ? props.maxHeight : '200px' )};
    justify-content: center;
    padding: 140px 0;
`;

export const ValueWrapper = styled.div`
    padding-top: 15px;
    padding-left: 12.5px;
    padding-bottom: 30px;

    .Select-control {
        border: none;
        color: #373a3c !important;
        background: #ffffff !important;
        cursor: default !important;
        vertical-align: top;
    }

    .Select.is-disabled {
        .Select-value-label {
            color: #373a3c !important;
        }
    }

    .Select-arrow-zone {
        display: none;
    }
`;

export const LoansWrapper = styled.div`
    .content {
        margin-top: 40px;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .main {
            .btn {
                min-width: 140px;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;

            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 600;
                font-size: 22px;
            }

            .search-wrapper {
                flex-grow: 1;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 30px;

                    input {
                        border: none;
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }
        }
    }

    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }
`;

export const MessageWrapper = styled.div`
    display: flex;
    justify-content: center;
    height: calc(100% - 90px);
    width: 100%;
    margin-bottom: 0 !important;

    > div {
        display: flex;
        flex-direction: column;
        justify-content: center;

        i {
            font-size: 7em;
            align-self: center;
        }

        .description {
            padding-top: 40px;
            text-align: center;

            h3 {
                font-weight: 600;
            }
        }
    }
`;

export const ProfileWrapper = styled.div`
    padding: 40px 5%;
    width: 100%;
    height: 100%;

    .final-pay-info {
        width: 100%;
        height: 55px;
        background-color: rgba(184, 233, 134, 0.5);
        text-align: end;
        line-height: 55px;
        padding-right: 10px;
        font-weight: bold;
    }

    .summary-panel {
        font-size: 14px;
        display: flex;
        padding: 25px;
        background-color: rgba(229, 246, 252, 1);
        border-radius: 5px;
        margin-bottom: 70px;

        &__column {
            flex-grow: 1;
            flex-basis: 0;
            padding: 10px;

            &:not(:last-child) {
                border-right: solid 2px rgb(216, 216, 216);
                margin-right: 40px
            }

            p {
                margin: 0;
            }
        }
    }

    .emp_name {
        font-size: 30px;
        margin-bottom: 10px;
        font-weight: 600;

        span:first-of-type {
            font-weight: 400;
            color: #00a5e5;
            margin-right: 4px;

            .brace {
                color: #4e4e4e;
            }
        }
    }

    .employee-header {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .terminate-button > button {
        border-color: #F21108;
    }

    @media (max-width: 1440px) {
        padding: 40px 15px 40px 50px;
    }

    @media (max-width: 1280px) {
        padding: 40px 25px 40px 50px;
    }

    .col-xs-4,
    .col-xs-6 {
        .DayPickerInput,
        .DayPickerInput input {
            width: 100%;
        }
    }

    .offset-right-4 {
        margin-right: 33.333333%;
    }


    > div {
        margin-bottom: 20px;

        &.actions {
            display: flex;
            margin-bottom: 25px;

            button {
                text-transform: none;
                color: inherit;
                padding: 12px 30px;

                &:first-child {
                    margin-right: 15px;
                }

                &:last-child {
                    margin-left: auto;
                }
            }
        }
    }
`;

export const ProfileSectionWrapper = styled.div`
    background-color: #fff;
    border: 1px solid #e5e5e5;
    border-left: 4px solid #00a5e5;
    padding: 25px;

    .section-heading {
        display: flex;
        align-items: center;
        justify-content: space-between;
        border-bottom: 1px solid #e5e5e5;

        h5 {
            padding-top: 5px;
        }

        button {
            margin-bottom: 20px;
        }

        a {
            border-bottom: none;

            button {
                background-color: #4aba4a;
                border-radius: 50%;
                width: 30px;
                height: 30px;
                border: none;
                outline: none;
                cursor: pointer;

                &:disabled {
                    cursor: not-allowed;
                    opacity: .65;
                }

                i {
                    color: #fff;
                }
            }
        }
    }

    .section-body {
        padding-top: 25px;

        h4 {
            font-weight: 700;
            padding-left: 35px;
        }

        i.fa {
            color: #00a5e5;
            font-size: 1.12em;
            width: 35px;
            display: inline-block;
        }

        > div {
            &.section-column {
                > div {
                    margin-bottom: 25px;
                }
            }

            > div {
                .section-row {
                    display: flex;
                    flex-wrap: wrap;
                    padding-left: 35px;

                    > div {
                        margin-bottom: 25px;

                        .radio-group {
                            > div:first-child{
                                padding-left: 0 !important;
                            }
                        }
                    }

                    &.edit {
                        > div:not(:last-child) {
                            margin-bottom: 0;
                        }
                    }
                }
            }
        }

        .content {
            .heading {
                display: flex;
                align-items: center;
                flex-direction: column;
                margin: 0 auto 50px auto;

                h3 {
                    font-weight: 600;
                }

                p {
                    text-align: center;
                    max-width: 800px;
                }
            }

            .tableAction button {
                width: 130px;
            }

            .title {
                display: flex;
                align-items: center;
                margin-bottom: 20px;

                h5 {
                    margin: 0;
                    margin-right: 20px;
                    font-weight: 600;
                    font-size: 20px;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }
        }
        .search-wrapper {
            flex-grow: 1;

            .search {
                width: 300px;
                border: 1px solid #333;
                border-radius: 30px;

                input {
                    border: none;
                }
            }

            p {
                display: none;
            }

            .input-group,
            .form-control {
                background-color: transparent;
            }

            .input-group-addon {
                background-color: transparent;
                border: none;
            }
        }
    }

    .section-footer {
        display: flex;
        justify-content: flex-end;
        border-top: 1px solid #e5e5e5;
        padding: 25px 5% 0 0;

        button {
            text-transform: none;
            padding: 12px 45px

            &:first-child {
                color: inherit;
                margin-right: 15px;
            }
        }
    }
`;

export const ListWrapper = styled.div`
    display: flex;
    flex-direction: column;
    background-color: #fff;
    border-right: 1px solid #e5e5e5;
    padding-top: 40px;
    min-width: 500px;
    height: calc(100vh - 120px);
    margin-top: 6px;
    left: 0;
    bottom: 0;

    @media (max-width: 1440px) {
        min-width: 400px;
        position: absolute;
        top: 0;

        > div {
            padding-left: 15px;
            padding-right: 15px;
        }
    }

    @media (max-width: 1280px) {
        display: none;
        position: absolute;
        z-index: 10;
        min-width: 500px;

        > div {
            padding-left: 40px;
            padding-right: 40px;
        }
    }

    &.collapsed {
        display: none;

        @media (max-width: 1280px) {
            display: flex;
        }
    }

    .actions {
        display: flex;
        padding: 0 45px 25px;

        .input-search-group {
            width: 100%;
            display: flex;
            position: relative;

            > div {
                width: 100%;
                input {
                    border-radius: 50px;
                    padding-right: 45px;
                }

                p {
                    display: none;
                }
            }

            .button-search {
                border: none;
                outline: none;
                position: absolute;
                right: 0;
                cursor: pointer;
                padding-right: 15px;
                align-self: center;

                @media (-ms-high-contrast: active), (-ms-high-contrast: none) {
                    margin-top: 10px;
                 }

                i {
                    font-size: 1.5em;
                }
            }
        }

        .button-collapse {
            border: none;
            outline: none;
            margin-left: 10px;
            cursor: pointer;

            i {
                color: #adadad;
                font-size: 1.8em;
            }
        }
    }

    .list {
        height: 100%;
        overflow-y: auto;
        padding: 0 40px;

        ul {
            list-style: none;
            display: flex;
            flex-direction: column;
            padding: 0 0 25px;
        }
    }
`;

export const ListItemWrapper = styled.li`
    display: flex;
    padding: 15px 20px;
    border-bottom: 1px solid #e5e5e5;
    cursor: pointer;
    border-left: 4px solid #fff;

    div:first-child {
        width: 35%;
    }

    &:hover {
        background-color: rgba(0, 165, 229, 0.01);
    }

    &.active,
    &:hover.active {
        border-left: 4px solid #00a5e5;
        font-weight: 700;
        background-color: #F5FCEF;
    }
`;

export const ListCollapsedWrapper = styled.div`
    top: 40px;
    position: absolute;
    background-color: transparent;
    z-index: 10;

    &.collapsed {
        display: none;

        @media (max-width: 1280px) {
            display: block;
        }
    }

    button {
        border: 1px solid #e5e5e5;
        border-left: none;
        border-radius: 10px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        background-color: #fff;
        outline: none;
        cursor: pointer;
        padding: 6px 11px;
        font-size: 13px;

        i {
            color: #adadad;
            font-size: 1.8em;
        }
    }

`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;
