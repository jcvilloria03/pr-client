import React from 'react';

import Search from '../../../../components/Search';
import SalDropdown from '../../../../components/SalDropdown';

import { isAnyRowSelected } from '../../../../components/Table/helpers';

/**
 * Section body for employee records table
 */
export default class SectionBody extends React.Component {
    static propTypes = {
        table: React.PropTypes.object,
        deleteLabel: React.PropTypes.string,
        label: React.PropTypes.string,
        onSearch: React.PropTypes.func
    };

    constructor( props ) {
        super( props );

        this.state = {
            showModal: false,
            dropdownItems: []
        };
    }

    render() {
        return (
            <div>
                <div className="title">
                    <div className="search-wrapper">
                        <Search
                            ref={ ( ref ) => { this.search = ref; } }
                            handleSearch={ this.props.onSearch }
                        />
                    </div>
                    <span>
                        { isAnyRowSelected( this.props.table ) ? this.props.deleteLabel : this.props.label }
                    </span>
                    <span className="sl-u-gap-left--sm">
                        { isAnyRowSelected( this.props.table ) ? (
                            <SalDropdown
                                dropdownItems={ this.state.dropdownItems }
                            /> ) : null
                        }
                    </span>
                </div>
            </div>
        );
    }
}
