import React from 'react';

import Switch from 'components/Switch';
import Table from 'components/Table';
import { DATE_FORMATS } from 'utils/constants';
import { browserHistory } from 'utils/BrowserHistory';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate
} from 'utils/functions';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';
import { ProfileSectionWrapper } from '../styles';

/**
 * Employee loans component
 */
export default class EmployeeLoans extends React.PureComponent {
    static propTypes = {
        loans: React.PropTypes.array,
        employee: React.PropTypes.object,
        permission: React.PropTypes.object,
        loading: React.PropTypes.bool,
        disabled: React.PropTypes.bool
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: props.loans
        };
    }

    componentDidMount() {
        this.handleTableChanges();
    }

    getTableColumns = () => (
        [
            {
                header: 'Include',
                id: 'include',
                accessor: ( row ) => row.include,
                minWidth: 70,
                render: ({ row, index }) => (
                    <Switch
                        defaultChecked={ row.include }
                        onChange={ ( value ) => {
                            this.setState( ( state ) => {
                                const dataCopy = Array.from( state.displayedData );
                                dataCopy.splice( index, 1, {
                                    ...dataCopy[ index ],
                                    include: value,
                                    is_full: false
                                });

                                return {
                                    displayedData: dataCopy
                                };
                            });
                        } }
                        disabled={ this.props.disabled }
                    />
                )
            },
            {
                id: 'deduction',
                header: 'Deduct in Full?',
                accessor: ( row ) => row.is_full,
                minWidth: 100,
                render: ({ row, index }) => (
                    <Switch
                        defaultChecked={ row.is_full }
                        disabled={ !row.include || this.props.disabled }
                        onChange={ ( value ) => {
                            this.setState( ( state ) => {
                                const dataCopy = Array.from( state.displayedData );
                                dataCopy.splice( index, 1, {
                                    ...dataCopy[ index ],
                                    is_full: value
                                });

                                return {
                                    displayedData: dataCopy
                                };
                            });
                        } }
                    />
                )
            },
            {
                id: 'id',
                header: 'id',
                accessor: ( row ) => row.id,
                show: false
            },
            {
                id: 'type_name',
                header: 'Loan Type',
                accessor: ( row ) => row.name,
                minWidth: 150
            },
            {
                id: 'category',
                header: 'Loan Category',
                accessor: ( row ) => row.category,
                minWidth: 150
            },
            {
                id: 'remaining_balance',
                header: 'Remaning Balance',
                accessor: ( row ) => row.remaining_balance,
                minWidth: 200,
                render: ({ row }) => `${formatCurrency( row.remaining_balance )}`
            },
            {
                id: 'amortization',
                header: 'Monthly Amortization',
                accessor: ( row ) => row.monthly_amortization,
                minWidth: 150
            },
            {
                id: 'repayment',
                header: 'Repayment Date',
                accessor: ( row ) => row.repayment_date,
                minWidth: 150,
                render: ({ row }) => ( formatDate( row.repayment_date, DATE_FORMATS.DISPLAY ) )
            }
        ]
    )

    getFinalPayPayload() {
        const { displayedData = []} = this.state;

        return displayedData
            .reduce( ( items, item ) => {
                const {
                    item_id,
                    other_income_type: type,
                    ...meta
                } = item;

                item.include && items.push({
                    item_id,
                    type,
                    meta
                });

                return items;
            }, []);
    }

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.loansTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    handleSearch = ( term = '' ) => {
        const matchingRows = this.loansTable.getFilteredData( this.props.loans, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <div style={ { marginTop: '50px' } }>
                <ProfileSectionWrapper>
                    <SectionHeading
                        permission={ this.props.permission }
                        heading="Loans"
                        id="tooltipLoans"
                        createButtonLabel="Add Loans"
                        onAdd={ () => {
                            localStorage.setItem( 'prepopulatedEmployeeForAddLoan', JSON.stringify( this.props.employee ) );
                            browserHistory.push( '/loans/add' );
                        } }
                        disableAdd={ this.props.disabled }
                    />
                    <div className="section-body">
                        <div className="content">
                            <SectionBody
                                table={ this.loansTable }
                                permission={ this.props.permission }
                                label={ this.state.label }
                                onSearch={ this.handleSearch }
                            />
                            <Table
                                data={ this.state.displayedData }
                                columns={ this.getTableColumns() }
                                pagination
                                loading={ this.props.loading }
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.loansTable = ref; } }
                            />
                        </div>
                    </div>
                </ProfileSectionWrapper>
            </div>
        );
    }
}
