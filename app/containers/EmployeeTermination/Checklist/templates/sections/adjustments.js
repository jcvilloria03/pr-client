import React from 'react';
import startCase from 'lodash/startCase';

import Table from 'components/Table';
import Switch from 'components/Switch';
import { DATE_FORMATS } from 'utils/constants';

import { browserHistory } from 'utils/BrowserHistory';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate,
    stripNonDigit
} from 'utils/functions';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';
import { ProfileSectionWrapper } from '../styles';

/**
 * Employee adjustments component
 */
export default class EmployeeAdjustments extends React.PureComponent {
    static propTypes = {
        adjustments: React.PropTypes.array,
        employee: React.PropTypes.object,
        permission: React.PropTypes.object,
        loading: React.PropTypes.bool,
        disabled: React.PropTypes.bool
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: props.adjustments
        };

        this.getFinalPayPayload = this.getFinalPayPayload.bind( this );
    }

    componentDidMount() {
        this.handleTableChanges();
    }

    getTableColumns = () => (
        [
            {
                header: 'Include',
                id: 'include',
                accessor: ( row ) => row.include,
                minWidth: 70,
                render: ({ row, index }) => (
                    <Switch
                        defaultChecked={ row.include }
                        onChange={ ( value ) => {
                            this.setState( ( state ) => {
                                const dataCopy = Array.from( state.displayedData );
                                dataCopy.splice( index, 1, {
                                    ...dataCopy[ index ],
                                    include: value
                                });

                                return {
                                    displayedData: dataCopy
                                };
                            });
                        } }
                        disabled={ this.props.disabled || row.name === 'PHILHEALTH_EMPLOYER_CONTRIBUTION' }
                    />
                )
            },
            {
                id: 'id',
                header: 'id',
                accessor: ( row ) => row.id,
                show: false
            },
            {
                id: 'type_name',
                header: 'Adjustment Type',
                accessor: ( row ) => row.name,
                minWidth: 150,
                render: ({ row }) => ( startCase( `${row.name.toLowerCase()}` ) )

            },
            {
                id: 'reason',
                header: 'Reason',
                accessor: ( row ) => row.reason,
                minWidth: 150,
                render: ({ row }) => startCase( row.reason )
            },
            {
                id: 'amount',
                header: 'Amount',
                accessor: ( row ) => row.amount,
                minWidth: 150,
                render: ({ row }) => `${formatCurrency( row.amount )}`
            },
            {
                id: 'release_date',
                header: 'Release Date',
                accessor: ( row ) => row.release_date,
                minWidth: 150,
                render: ({ row }) => ( formatDate( row.release_date, DATE_FORMATS.DISPLAY ) )
            }
        ]
    )

    getFinalPayPayload() {
        const { displayedData = []} = this.state;

        return displayedData
            .reduce( ( items, item ) => {
                const {
                    item_id,
                    item_type: type,
                    ...meta
                } = item;

                item.include && items.push({
                    item_id,
                    type,
                    meta: {
                        ...meta,
                        amount: Number( stripNonDigit( item.amount ) )
                    }
                });

                return items;
            }, []);
    }
    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.adjustmentsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    handleSearch = ( term = '' ) => {
        const dataForDisplay = this.props.adjustments;
        const matchingRows = this.adjustmentsTable.getFilteredData( dataForDisplay, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <div style={ { marginTop: '50px' } }>
                <ProfileSectionWrapper>
                    <SectionHeading
                        permission={ this.props.permission }
                        heading="Adjustments"
                        id="tooltipAdjustments"
                        createButtonLabel="Add Adjustments"
                        onAdd={ () => {
                            localStorage.setItem( 'prepopulatedEmployeeForAddAdjustment', JSON.stringify( this.props.employee ) );
                            browserHistory.push( '/adjustments/add' );
                        } }
                        disableAdd={ this.props.disabled }
                    />
                    <div className="section-body">
                        <div className="content">
                            <SectionBody
                                table={ this.adjustmentsTable }
                                permission={ this.props.permission }
                                label={ this.state.label }
                                onSearch={ this.handleSearch }
                            />
                            <Table
                                data={ this.state.displayedData }
                                columns={ this.getTableColumns() }
                                pagination
                                loading={ this.props.loading }
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.adjustmentsTable = ref; } }
                            />
                        </div>
                    </div>
                </ProfileSectionWrapper>
            </div>
        );
    }
}
