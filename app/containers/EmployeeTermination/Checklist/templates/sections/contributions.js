import React from 'react';

import Table from 'components/Table';
import Switch from 'components/Switch';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    stripNonDigit
} from 'utils/functions';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';
import { ProfileSectionWrapper } from '../styles';

/**
 * Employee contributions component
 */
export default class EmployeeContributions extends React.PureComponent {
    static propTypes = {
        contributions: React.PropTypes.array,
        permission: React.PropTypes.object,
        loading: React.PropTypes.bool,
        disabled: React.PropTypes.bool
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: props.contributions
        };
    }

    componentDidMount() {
        this.handleTableChanges();
    }

    getTableColumns = () => (
        [
            {
                header: 'Include',
                id: 'include',
                accessor: ( row ) => row.include,
                minWidth: 70,
                render: ({ row, index }) => (
                    <Switch
                        defaultChecked={ row.include }
                        onChange={ ( value ) => {
                            this.setState( ( state ) => {
                                const dataCopy = Array.from( state.displayedData );
                                dataCopy.splice( index, 1, {
                                    ...dataCopy[ index ],
                                    include: value,
                                    refund: !value
                                });

                                return {
                                    displayedData: dataCopy
                                };
                            });
                        } }
                        disabled={ this.props.disabled }
                    />
                )
            },
            {
                header: 'Refund',
                id: 'refund',
                accessor: ( row ) => row.refund,
                minWidth: 70,
                render: ({ row, index }) => (
                    <Switch
                        defaultChecked={ row.refund }
                        onChange={ ( value ) => {
                            this.setState( ( state ) => {
                                const dataCopy = Array.from( state.displayedData );
                                dataCopy.splice( index, 1, {
                                    ...dataCopy[ index ],
                                    refund: value,
                                    include: !value
                                });

                                return {
                                    displayedData: dataCopy
                                };
                            });
                        } }
                        disabled={ this.props.disabled }
                    />
                )
            },
            {
                header: 'Government Agency',
                id: 'government_agency',
                accessor: ( row ) => row.name,
                minWidth: 150
            },
            {
                header: 'Amount Basis',
                id: 'amount_basis',
                accessor: ( row ) => row.basis,
                minWidth: 150
            },
            {
                header: 'Deducted Amount',
                id: 'deducted',
                accessor: ( row ) => row.amount,
                minWidth: 150,
                render: ({ row }) => `${formatCurrency( row.amount )}`
            }
        ]
    )

    getFinalPayPayload() {
        const { displayedData = []} = this.state;

        return displayedData
            .reduce( ( items, item ) => {
                const {
                    item_id,
                    other_income_type: type,
                    ...meta
                } = item;

                items.push({
                    item_id,
                    type,
                    meta: {
                        ...meta,
                        amount: Number( stripNonDigit( item.amount ) )
                    }
                });

                return items;
            }, []);
    }

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.contributionsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    handleSearch = ( term = '' ) => {
        const matchingRows = this.contributionsTable.getFilteredData( this.props.contributions, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <div style={ { marginTop: '50px' } }>
                <ProfileSectionWrapper>
                    <SectionHeading
                        permission={ this.props.permission }
                        heading="Contributions"
                        id="tooltipContributions"
                        disableAdd={ this.props.disabled }
                    />
                    <div className="section-body">
                        <div className="content">
                            <SectionBody
                                table={ this.contributionsTable }
                                permission={ this.props.permission }
                                label={ this.state.label }
                                onSearch={ this.handleSearch }
                                onExport={ this.exportContributions }
                            />
                            <Table
                                data={ this.state.displayedData }
                                columns={ this.getTableColumns() }
                                pagination
                                loading={ this.props.loading }
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.contributionsTable = ref; } }
                            />
                        </div>
                    </div>
                </ProfileSectionWrapper>
            </div>
        );
    }
}
