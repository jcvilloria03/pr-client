import React from 'react';

import Switch from 'components/Switch';
import Table from 'components/Table';
import { browserHistory } from 'utils/BrowserHistory';
import { DATE_FORMATS } from 'utils/constants';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate,
    stripNonDigit
} from 'utils/functions';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';
import { ProfileSectionWrapper } from '../styles';

/**
 * Employee commissions component
 */
export default class EmployeeCommissions extends React.PureComponent {
    static propTypes = {
        commissions: React.PropTypes.array,
        employee: React.PropTypes.object,
        permission: React.PropTypes.object,
        loading: React.PropTypes.bool,
        disabled: React.PropTypes.bool
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: props.commissions
        };
    }

    componentDidMount() {
        this.handleTableChanges();
    }

    getTableColumns = () => (
        [
            {
                header: 'Include',
                id: 'include',
                accessor: ( row ) => row.include,
                minWidth: 70,
                render: ({ row, index }) => (
                    <Switch
                        defaultChecked={ row.include }
                        onChange={ ( value ) => {
                            this.setState( ( state ) => {
                                const dataCopy = Array.from( state.displayedData );
                                dataCopy.splice( index, 1, {
                                    ...dataCopy[ index ],
                                    include: value
                                });

                                return {
                                    displayedData: dataCopy
                                };
                            });
                        } }
                        disabled={ this.props.disabled }
                    />
                )
            },
            {
                id: 'id',
                header: 'id',
                accessor: ( row ) => row.id,
                show: false
            },
            {
                id: 'type_name',
                header: 'Commission Type',
                accessor: ( row ) => row.name,
                minWidth: 150
            },
            {
                id: 'amount',
                header: 'Amount',
                accessor: ( row ) => row.amount,
                minWidth: 150,
                render: ({ row }) => `${formatCurrency( row.amount )}`
            },
            {
                id: 'release_date',
                header: 'Release Date',
                accessor: ( row ) => row.release_date,
                minWidth: 150,
                render: ({ row }) => ( formatDate( row.release_date, DATE_FORMATS.DISPLAY ) )
            }
        ]
    )

    getFinalPayPayload() {
        const { displayedData = []} = this.state;

        return displayedData
            .reduce( ( items, item ) => {
                const {
                    item_id,
                    other_income_type: type,
                    ...meta
                } = item;

                item.include && items.push({
                    item_id,
                    type,
                    meta: {
                        ...meta,
                        amount: Number( stripNonDigit( item.amount ) )
                    }
                });

                return items;
            }, []);
    }

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.commissionsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    handleSearch = ( term = '' ) => {
        const matchingRows = this.commissionsTable.getFilteredData( this.props.commissions, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <div style={ { marginTop: '50px' } }>
                <ProfileSectionWrapper>
                    <SectionHeading
                        permission={ this.props.permission }
                        heading="Commissions"
                        id="tooltipCommissions"
                        createButtonLabel="Add Commissions"
                        onAdd={ () => {
                            localStorage.setItem( 'prepopulatedEmployeeForAddCommission', JSON.stringify( this.props.employee ) );
                            browserHistory.push( '/commissions/add' );
                        } }
                        disableAdd={ this.props.disabled }
                    />
                    <div className="section-body">
                        <div className="content">
                            <SectionBody
                                table={ this.commissionsTable }
                                permission={ this.props.permission }
                                label={ this.state.label }
                                onSearch={ this.handleSearch }
                            />
                            <Table
                                data={ this.state.displayedData }
                                columns={ this.getTableColumns() }
                                pagination
                                loading={ this.props.loading }
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.commissionsTable = ref; } }
                            />
                        </div>
                    </div>
                </ProfileSectionWrapper>
            </div>
        );
    }
}
