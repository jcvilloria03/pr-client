import React from 'react';
import startCase from 'lodash/startCase';

import Switch from 'components/Switch';
import Table from 'components/Table';
import { getSortedData } from 'components/Table/helpers';
import { DATE_FORMATS } from 'utils/constants';
import { browserHistory } from 'utils/BrowserHistory';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate,
    stripNonDigit
} from 'utils/functions';

import SectionHeading from '../sectionHeading';
import SectionBody from '../sectionBody';
import { ProfileSectionWrapper } from '../styles';
import { BONUS_TYPES } from '../../constants';

/**
 * Employee bonuses component
 */
export default class EmployeeBonuses extends React.PureComponent {
    static propTypes = {
        bonuses: React.PropTypes.array,
        employee: React.PropTypes.object,
        permission: React.PropTypes.object,
        loading: React.PropTypes.bool,
        disabled: React.PropTypes.bool
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: props.bonuses
        };
    }

    componentDidMount() {
        this.handleTableChanges();
    }

    getTableColumns = () => (
        [
            {
                header: 'Include',
                id: 'include',
                accessor: ( row ) => ( row.include ),
                minWidth: 70,
                render: ({ row, index }) => (
                    <Switch
                        defaultChecked={ row.include }
                        onChange={ ( value ) => {
                            this.setState( ( state ) => {
                                const dataCopy = Array.from( state.displayedData );
                                dataCopy.splice( index, 1, {
                                    ...dataCopy[ index ],
                                    include: value
                                });

                                return {
                                    displayedData: dataCopy
                                };
                            });
                        } }
                        disabled={ this.props.disabled }
                    />
                )
            },
            {
                id: 'id',
                header: 'id',
                accessor: ( row ) => row.id,
                show: false
            },
            {
                id: 'type_name',
                header: 'Bonus Type',
                accessor: ( row ) => row.name,
                minWidth: 150,
                render: ({ row }) => ( startCase( row.name.toLowerCase() ) )
            },
            {
                id: 'type_basis',
                header: 'Type',
                accessor: ( row ) => row.type,
                minWidth: 150,
                render: ({ row }) => ( startCase( row.type.toLowerCase() ) )
            },
            {
                id: 'bonus_per_recipient',
                header: 'Amount',
                accessor: ( row ) => row.type,
                minWidth: 150,
                render: ({ row }) => ( row.type === BONUS_TYPES.FIXED ? `${formatCurrency( row.amount )}` : startCase( row.basis.toLowerCase() ) )
            },
            {
                id: 'release_date',
                header: 'Release Date',
                accessor: ( row ) => row.release_date,
                minWidth: 150,
                render: ({ row }) => ( formatDate( row.release_date, DATE_FORMATS.DISPLAY ) )
            }
        ]
    )

    getFinalPayPayload() {
        const { displayedData = []} = this.state;

        return displayedData
            .reduce( ( items, item ) => {
                const {
                    item_id,
                    other_income_type: type,
                    ...meta
                } = item;

                const specificMeta = item.type === BONUS_TYPES.FIXED
                    ? {
                        amount: Number( stripNonDigit( item.amount ) )
                    }
                    : {
                        percentage: item.percentage,
                        basis: item.basis
                    };

                item.include && items.push({
                    item_id,
                    type,
                    meta: {
                        ...meta,
                        ...specificMeta
                    }
                });

                return items;
            }, []);
    }

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.bonusesTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    prepareComputationData = () => (
        getSortedData( this.bonusesTable ).map( ( bonus ) => bonus.id )
    )

    handleSearch = ( term = '' ) => {
        const dataForDisplay = this.props.bonuses;
        const matchingRows = this.bonusesTable.getFilteredData( dataForDisplay, term );

        this.setState({ displayedData: matchingRows }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        return (
            <div style={ { marginTop: '50px' } }>
                <ProfileSectionWrapper>
                    <SectionHeading
                        permission={ this.props.permission }
                        heading="Bonuses"
                        id="tooltipBonuses"
                        createButtonLabel="Add Bonuses"
                        onAdd={ () => {
                            localStorage.setItem( 'prepopulatedEmployeeForAddBonus', JSON.stringify( this.props.employee ) );
                            browserHistory.push( '/bonuses/add' );
                        } }
                        disableAdd={ this.props.disabled }
                    />
                    <div className="section-body">
                        <div className="content">
                            <SectionBody
                                table={ this.bonusesTable }
                                permission={ this.props.permission }
                                label={ this.state.label }
                                onSearch={ this.handleSearch }
                            />
                            <Table
                                data={ this.state.displayedData }
                                columns={ this.getTableColumns() }
                                pagination
                                loading={ this.props.loading }
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.bonusesTable = ref; } }
                            />
                        </div>
                    </div>
                </ProfileSectionWrapper>
            </div>
        );
    }
}
