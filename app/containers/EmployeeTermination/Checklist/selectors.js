import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectEmployeeTerminationChecklistDomain = () => ( state ) => state.get( 'employeeTerminationChecklist' );

const makeSelectEmployee = () => createSelector(
    selectEmployeeTerminationChecklistDomain(),
    ( substate ) => substate.get( 'employee' ).toJS()
);

const makeSelectTerminationDetails = () => createSelector(
    selectEmployeeTerminationChecklistDomain(),
    ( substate ) => substate.get( 'terminationDetails' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectEmployeeTerminationChecklistDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectLoading = () => createSelector(
    selectEmployeeTerminationChecklistDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectEmployeeTerminationChecklistDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectAvailableItems = () => createSelector(
    selectEmployeeTerminationChecklistDomain(),
    ( substate ) => substate.get( 'availableItemsDetails' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectEmployeeTerminationChecklistDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectErrors,
    makeSelectTerminationDetails,
    makeSelectNotification,
    makeSelectSubmitted,
    makeSelectLoading,
    makeSelectEmployee,
    makeSelectAvailableItems
};
