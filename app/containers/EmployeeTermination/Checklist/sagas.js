import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from 'utils/BrowserHistory';
import { Fetch } from 'utils/request';
import { formatFeedbackMessage } from 'utils/functions';
import { company } from 'utils/CompanyService';

import {
    INITIALIZE,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_ERRORS,
    SET_EMPLOYEE,
    SUBMITTED,
    LOADING,
    SUBMIT,
    SET_TERMINATION_DETAILS,
    SET_EMPLOYEE_AVAILABLE_ITEMS_DETAILS,
    SET_EMPLOYEE_LOADING_AVAILABLE_ITEMS_DETAILS
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { INACTIVE } from '../../Employee/View/constants';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({ type: LOADING, payload: true });

        const [ employee, { data }] = yield [
            call( Fetch, `/employee/${payload.id}`, { method: 'GET' }),
            call( Fetch, `/termination_informations/${payload.terminationId}`, {
                method: 'GET',
                params: {
                    include_final_pay: 1
                }
            }),
            call( getEmployeeAvailableIncomes, payload.id )
        ];

        if ( employee.active === INACTIVE ) {
            yield call( notifyUser, {
                show: true,
                title: '',
                message: 'Terminating employee not allowed because the employee\'s status is Inactive',
                type: 'error'
            });
            yield call( browserHistory.replace, `/employee/${payload.id}`, true );
            return;
        }

        const {
            final_pay_projection_job_id: finalPayProjectionJobId,
            final_pay_projected: finalPayProjected
        } = data.attributes;

        if ( finalPayProjectionJobId && !finalPayProjected ) {
            // redirect to computation page if calculation of projected final pay is ongoing
            yield call( browserHistory.push, `/employee/${payload.id}/termination/${payload.terminationId}/computation`, true );
        } else {
            yield put({
                type: SET_EMPLOYEE,
                payload: employee
            });

            yield put({
                type: SET_TERMINATION_DETAILS,
                payload: data
            });
        }
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
        yield call( browserHistory.push, `/employee/${payload.id}`, true );
    } finally {
        yield put({ type: LOADING, payload: false });
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submit({ payload }) {
    try {
        const {
            employee_id: employeeId,
            termination_information_id: terminationInformationId
        } = payload.data;

        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        yield call( Fetch, '/final_pay', { method: 'POST', data: payload });
        yield call( Fetch, '/payroll/0/calculate', {
            method: 'POST',
            data: {
                projectedCalculation: true,
                employeeId,
                companyId: company.getLastActiveCompanyId()
            }
        });

        yield call( browserHistory.push, `/employee/${employeeId}/termination/${terminationInformationId}/computation`, true );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, toErrorPayload( error ) );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage()
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });
}

/**
 * Fetch Loans, Contributions and Other Incomes by employee
 */
export function* getEmployeeAvailableIncomes( payload ) {
    try {
        yield put({ type: SET_EMPLOYEE_LOADING_AVAILABLE_ITEMS_DETAILS, payload: true });

        const { data } = yield call( Fetch, `/employee/${payload}/final_pay/available_items`, { method: 'GET' });

        yield put({ type: SET_EMPLOYEE_AVAILABLE_ITEMS_DETAILS, payload: data.available_items });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({ type: SET_EMPLOYEE_LOADING_AVAILABLE_ITEMS_DETAILS, payload: false });
    }
}

/**
 * converts api error to notification payload
 */
function toErrorPayload( error ) {
    return {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT
 */
export function* watchForSubmit() {
    const watcher = yield takeEvery( SUBMIT, submit );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmit,
    watchForInitializeData,
    watchForReinitializePage,
    watchNotify
];
