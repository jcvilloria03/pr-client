import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';

import {
    INITIALIZE,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_ERRORS,
    SET_FORM_OPTIONS,
    SUBMITTED,
    LOADING,
    SUBMIT_FORM,
    REASONS_FOR_LEAVING,
    SET_TERMINATION_DETAILS
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({ type: LOADING, payload: true });

        const [ employee, { data }] = yield [
            call( Fetch, `/employee/${payload.id}`, { method: 'GET' }),
            call( Fetch, `/termination_informations/${payload.terminationId}`, {
                method: 'GET',
                params: {
                    include_final_pay: 1
                }
            })
        ];

        const {
            final_pay_projection_job_id: finalPayProjectionJobId,
            final_pay_projected: finalPayProjected
        } = data.attributes;

        if ( finalPayProjectionJobId && !finalPayProjected ) {
            // redirect to computation page if calculation of projected final pay is ongoing
            yield call( browserHistory.push, `/employee/${payload.id}/termination/${payload.terminationId}/computation`, true );
        } else {
            const formOptions = {
                reasonsForLeaving: REASONS_FOR_LEAVING,
                employee
            };

            yield put({
                type: SET_FORM_OPTIONS,
                payload: formOptions
            });

            yield put({
                type: SET_TERMINATION_DETAILS,
                payload: data
            });
        }
    } catch ( error ) {
        yield call( notifyUser, error );
        yield call( browserHistory.push, `/employee/${payload.id}`, true );
    } finally {
        yield put({ type: LOADING, payload: false });
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        const { employee_id, ...rest } = payload; // eslint-disable-line

        const data = {
            ...rest,
            company_id: company.getLastActiveCompanyId()
        };

        const terminationInformation = yield call( Fetch, `/termination_informations/${payload.terminationId}`, { method: 'PATCH', data });
        yield call( delay, 500 );
        yield call( browserHistory.push, `/employee/${payload.employee_id}/termination/${terminationInformation.data.id}/checklist`, true );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForInitializeData,
    watchForReinitializePage,
    watchNotify
];
