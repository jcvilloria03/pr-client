import { fromJS } from 'immutable';
import {
    SET_FORM_OPTIONS,
    SET_ERRORS,
    SUBMITTED,
    LOADING,
    NOTIFICATION_SAGA,
    SET_TERMINATION_DETAILS
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    errors: {},
    submitted: false,
    loading: false,
    terminationDetails: {
        last_date: null,
        pay_date: null,
        reason_for_leaving: '',
        remarks: '',
        disburse_through_special_pay_run: false
    },
    formOptions: {
        reasonsForLeaving: [],
        employee: {}
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 * Transforms termination details from API to accommodated structure in application
 * @param {Object} data - Employee termination details
 */
function prepareTerminationDetails({ id, type, attributes }) {
    return id && type
        ? { id, type, ...attributes }
        : {};
}

/**
 *
 * Edit employee termination manually
 *
 */
function editEmployeeTermination( state = initialState, action ) {
    switch ( action.type ) {
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_TERMINATION_DETAILS:
            return state.set( 'terminationDetails', fromJS( prepareTerminationDetails( action.payload ) ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default editEmployeeTermination;
