import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectEditEmployeeTerminationDomain = () => ( state ) => state.get( 'editEmployeeTermination' );

const makeSelectFormOptions = () => createSelector(
    selectEditEmployeeTerminationDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectTerminationDetails = () => createSelector(
    selectEditEmployeeTerminationDomain(),
    ( substate ) => substate.get( 'terminationDetails' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectEditEmployeeTerminationDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectLoading = () => createSelector(
    selectEditEmployeeTerminationDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectEditEmployeeTerminationDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectEditEmployeeTerminationDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectErrors,
    makeSelectFormOptions,
    makeSelectTerminationDetails,
    makeSelectNotification,
    makeSelectSubmitted,
    makeSelectLoading
};
