export const INITIALIZE = 'app/EmployeeTermination/Edit/INITIALIZE';
export const NOTIFICATION = 'app/EmployeeTermination/Edit/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/EmployeeTermination/Edit/NOTIFICATION_SAGA';
export const SET_ERRORS = 'app/EmployeeTermination/Edit/SET_ERRORS';
export const SET_FORM_OPTIONS = 'app/EmployeeTermination/Edit/SET_FORM_OPTIONS';
export const SUBMITTED = 'app/EmployeeTermination/Edit/SUBMITTED';
export const LOADING = 'app/EmployeeTermination/Edit/LOADING';
export const SUBMIT_FORM = 'app/EmployeeTermination/Edit/SUBMIT_FORM';
export const SET_TERMINATION_DETAILS = 'app/EmployeeTermination/Edit/SET_TERMINATION_DETAILS';

export const REASONS_FOR_LEAVING = [
    { value: 'TERMINATION', label: 'Termination' },
    { value: 'RESIGNATION', label: 'Resignation' },
    { value: 'RETIREMENT', label: 'Retirement' }
];

export const MAX_REMARKS_LENGTH = 260;

export const FINAL_PAY_STATUSES = {
    OPENED: 'OPENED',
    CLOSED: 'CLOSED',
    DISBURSED: 'DISBURSED'
};
