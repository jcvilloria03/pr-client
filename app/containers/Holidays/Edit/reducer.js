import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    SET_EDIT,
    SET_NOTIFICATION,
    BTN_LOADING
} from './constants';

const initialState = fromJS({
    loading: false,
    isEdit: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    btnLoading: false
});

/**
 *
 * EditHolidays reducer
 *
 */
function editHolidaysReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_EDIT:
            return state.set( 'isEdit', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default editHolidaysReducer;
