/* eslint-disable require-jsdoc */
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery } from 'redux-saga';
import { cancel, put, call, take } from 'redux-saga/effects';
import { Fetch } from 'utils/request';
import {
    LOADING,
    GET_EDIT,
    SET_EDIT,
    GET_UPDATE,
    SET_NOTIFICATION,
    NOTIFICATION,
    BTN_LOADING
} from './constants';
import { company } from '../../../utils/CompanyService';
import { browserHistory } from '../../../utils/BrowserHistory';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* getIsEditData({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const isEditData = yield call( Fetch, `/holiday/${payload}`, { method: 'GET' });

        yield put({ type: SET_EDIT, payload: isEditData && isEditData || {}});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

export function* updateForm({ payload }) {
    try {
        yield put({
            type: BTN_LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();

        const data = {
            company_id: companyId,
            ...payload
        };
        yield call( Fetch, `/holiday/${data.id}`, { method: 'PUT', data });

        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Record Updated Successfully',
            type: 'success'
        });
        yield call( browserHistory.push, '/company-settings/schedule-settings/holidays', true );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}

export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 2000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

export function* watchForGetIsEditHoliday() {
    const watcher = yield takeEvery( GET_EDIT, getIsEditData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForUpdateForm() {
    const watcher = yield takeEvery( GET_UPDATE, updateForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetIsEditHoliday,
    watchForUpdateForm
];
