import { createSelector } from 'reselect';

/**
 * Direct selector to the editHolidays state domain
 */
const selectEditHolidaysDomain = () => ( state ) => state.get( 'editHolidays' );

/**
 * Other specific selectors
 */

const makeSelectLoading = () => createSelector(
  selectEditHolidaysDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectEditHolidaysDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBtnLoad = () => createSelector(
  selectEditHolidaysDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

/**
 * Default selector used by EditHolidays
 */

const makeSelectEditHolidays = () => createSelector(
  selectEditHolidaysDomain(),
  ( substate ) => substate.toJS()
);

// export default makeSelectEditHolidays;
export {
  makeSelectEditHolidays,
  selectEditHolidaysDomain,
  makeSelectLoading,
  makeSelectNotification,
  makeSelectBtnLoad
};
