/*
 *
 * EditHolidays constants
 *
 */
const namespace = 'app/Holidays/Edit';
export const DEFAULT_ACTION = `${namespace}DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const GET_EDIT = `${namespace}/GET_EDIT`;
export const SET_EDIT = `${namespace}/SET_EDIT`;
export const SET_UPDATE = `${namespace}/SET_UPDATE`;
export const GET_UPDATE = `${namespace}/GET_UPDATE`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const BTN_LOADING = `${namespace}/BTN_LOADING`;

export const HOLIDAYS_TYPE = [
    { label: 'Special Working Holiday', value: 'Special Working', disabled: false },
    { label: 'Special Non-Working Holiday', value: 'Special', disabled: false },
    { label: 'Regular Holiday', value: 'Regular', disabled: false }
];

export const SELECT_ALL_EMPLOYEES_OPTIONS = [
    {
        value: null,
        label: 'All locations',
        uid: 'All Locationnull'
    }
];
