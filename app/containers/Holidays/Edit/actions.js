/* eslint-disable require-jsdoc */
import {
    DEFAULT_ACTION,
    GET_EDIT,
    GET_UPDATE
} from './constants';

/**
 *
 * EditHolidays actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function getIsEditData( payload ) {
    return {
        type: GET_EDIT,
        payload
    };
}

export function updateForm( payload ) {
    return {
        type: GET_UPDATE,
        payload
    };
}
