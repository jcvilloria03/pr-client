/* eslint-disable no-param-reassign */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment';

import A from 'components/A';
import { H2, H3, P } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Input from 'components/Input';
import SalSelect from 'components/Select';
import DatePicker from 'components/DatePicker';
import MultiSelect from 'components/MultiSelect';
import SnackBar from 'components/SnackBar';
import Modal from 'components/Modal/index';
import Loader from 'components/Loader';
import Icon from 'components/Icon';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';
import { DATE_FORMATS } from 'utils/constants';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';

import * as holidayEditAction from './actions';
import {
    makeSelectEditHolidays,
    makeSelectNotification,
    makeSelectLoading,
    makeSelectBtnLoad
} from './selectors';
import { HOLIDAYS_TYPE, SELECT_ALL_EMPLOYEES_OPTIONS } from './constants';
import {
    LoadingStyles,
    Header,
    PageWrapper,
    Footer
} from './styles';

/**
 *
 * EditHolidays
 *
 */
export class EditHolidays extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        params: React.PropTypes.object,
        loading: React.PropTypes.bool,
        getIsEditData: React.PropTypes.func,
        holidayData: React.PropTypes.object,
        updateForm: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        btnLoading: React.PropTypes.bool
    }

    constructor( props ) {
        super( props );

        this.state = {
            form: {
                name: '',
                type: '',
                date: '',
                entitled_locations: [],
                recurring: false,
                holiday_id: '',
                id: ''
            },
            isNameNull: false,
            hasAlreadyName: false,
            isNameSubmitNull: false,
            isType: false,
            isTypeSubmitNull: false,
            dateValidate: {
                flag: false,
                message: ''
            },
            isLocationSubmitNull: false,
            isFormDirty: false,
            isLocationChanged: false
        };
        this.discardModal = null;
    }

    componentDidMount() {
        this.props.getIsEditData( +this.props.params.id );
    }

    componentDidUpdate( prevProps ) {
        const { holidayData } = this.props;
        if ( holidayData !== prevProps.holidayData ) {
            this.setState({
                form: {
                    ...this.state.form,
                    holiday_id: holidayData.isEdit.id,
                    id: holidayData.isEdit.id
                }
            });
        }
    }

    mapEntitledLocations = ( locations ) => {
        return locations && locations.map( ( opt ) => ({
            value: opt.id || opt.value,
            label: opt.name || opt.label
        }) || []);
    }

    validateForm( form ) {
        let valid = true;

        const isNameSubmitNull = form.name === '' || !form.name;
        this.setState({ isNameSubmitNull });

        if ( isNameSubmitNull ) {
            valid = false;
        }

        const isTypeSubmitNull = form.type === '' || !form.type;
        this.setState({ isTypeSubmitNull });
        if ( isTypeSubmitNull ) {
            valid = false;
        }

        if ( !form.date || form.date === 'Invalid date' || form.date === '' ) {
            const message = 'Please choose a date';
            this.setState({ dateValidate: { flag: true, message }});
            valid = false;
        } else {
            this.setState({ dateValidate: { flag: false, message: '' }});
        }

        if ( form.entitled_locations.length < 1 ) {
            valid = false;
            this.setState({ isLocationSubmitNull: true });
        } else {
            this.setState({ isLocationSubmitNull: false });
        }

        return valid;
    }

    async submitForm() {
        const { holidayData } = this.props;
        const { isFormDirty, isLocationChanged, form } = this.state;

        const updateForm = async ( formValue ) => {
            const tages = formValue.entitled_locations.map( ( value ) => ({ id: value.value }) );
            formValue.entitled_locations = tages;
            await this.props.updateForm( formValue );
        };
        const consolidatedForm = {
            ...this.props.holidayData.isEdit,
            name: ( form.name && form.name ) || holidayData.isEdit.name,
            type: ( form.type && form.type ) || holidayData.isEdit.type,
            date: ( form.date && form.date ) || holidayData.isEdit.date,
            entitled_locations: isLocationChanged ? this.mapEntitledLocations( form.entitled_locations ) : this.mapEntitledLocations( holidayData.isEdit.entitled_locations ),
        };

        const isFormValid = isFormDirty || isLocationChanged ? this.validateForm( consolidatedForm ) : true;
        const shouldValidateName = isFormDirty ? this.state.form.name && this.state.form.name !== this.props.holidayData.isEdit.name : false;
        if ( isFormValid && shouldValidateName ) {
            const companyId = company.getLastActiveCompanyId();
            const response = await Fetch( `/company/${companyId}/holiday/is_name_available`, { method: 'POST', data: consolidatedForm });
            if ( response.available ) {
                await updateForm( consolidatedForm );
            } else {
                this.setState({ hasAlreadyName: true });
            }
        } else {
            await updateForm( consolidatedForm );
        }
    }

    addDate = ( name, value ) => {
        const values = value === '';
        this.setState({ [ name ]: values });
    }

    addState = ( name, value ) => {
        this.setState({
            form: {
                ...this.state.form,
                [ name ]: value
            }
        });
    }

    loadLeaderList = ( keyword, callback ) => {
        const companyId = company.getLastActiveCompanyId();
        Fetch( `/company/${companyId}/time_attendance_locations?name=${keyword}&limit=10&is_simplified=true`, { method: 'GET' })
            .then( ( result ) => {
                const list = result.data.map( ( option ) => ({
                    account_id: option.account_id,
                    company_id: companyId,
                    value: option.id,
                    is_headquarters: false,
                    label: option.name,
                    uid: option.name + option.id
                }) );
                callback( null, { options: [ ...SELECT_ALL_EMPLOYEES_OPTIONS, ...list ] });
            })
            .catch( ( error ) => callback( error, null ) );
    }

    handleDateFormat = ( date ) => moment( date ).format( 'YYYY-MM-DD' )

    dateValidation = ( value ) => {
        const date = this.handleDateFormat( value );
        if ( value === 'Invalid date' || value === '' ) {
            const message = 'Please choose a date';
            this.setState({ dateValidate: { flag: true, message }});
        } else {
            this.setState({ dateValidate: { flag: false, message: '' }});
        }
        return date;
    }

    /**
     *
     * EditHolidays render method
     *
     */
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const {
            form,
            isNameSubmitNull,
            isNameNull,
            hasAlreadyName,
            isTypeSubmitNull,
            isType,
            dateValidate,
            isLocationSubmitNull
        } = this.state;

        const {
            notification,
            btnLoading
        } = this.props;

        return (
            <div>
                <Helmet
                    title="Edit Holidays"
                    meta={ [
                        { name: 'description', content: 'Description of Holiday' }
                    ] }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Modal
                    title="Discard Changes"
                    body={
                        <div>
                            <p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.discardModal.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/company-settings/schedule-settings/holidays', true );
                            }
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                {this.props.loading ? (
                    <div className="loader">
                        <LoadingStyles>
                            <H2>Loading Holidays</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    </div>
                    ) : (
                        <div>
                            <Header>
                                <div className="nav">
                                    <Container>
                                        <A
                                            href
                                            onClick={ ( e ) => {
                                                e.preventDefault();
                                                browserHistory.push( '/company-settings/schedule-settings/holidays', true );
                                            } }
                                        >
                                            <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Holidays</span>
                                        </A>
                                    </Container>
                                </div>
                            </Header>
                            <PageWrapper>
                                <div className="main_section">
                                    <div className="heading">
                                        <H3>Edit Holiday</H3>
                                    </div>

                                    <div className="row">
                                        <div className={ isNameSubmitNull || hasAlreadyName ? 'col-xs-6 input_pay' : 'col-xs-6' }>
                                            <P noBottomMargin className={ isNameSubmitNull || hasAlreadyName ? 'label_err' : '' }>* Holiday Name</P>
                                            <Input
                                                id="holiday_name"
                                                type="text"
                                                name="name"
                                                className={ isNameNull || isNameSubmitNull ? 'input_pay' : '' }
                                                ref={ ( ref ) => { this.name = ref; } }
                                                value={ form.name || this.props.holidayData.isEdit.name }
                                                onChange={ ( value ) => {
                                                    this.setState({
                                                        isNameNull: value === '',
                                                        isNameSubmitNull: false,
                                                        hasAlreadyName: false,
                                                        isFormDirty: true
                                                    });
                                                    this.addState( 'name', value );
                                                } }
                                                onBlur={ ( value ) => {
                                                    this.setState({ isNameNull: value === '' });
                                                } }
                                            />
                                            {isNameSubmitNull ? <P noBottomMargin>This field is required</P> : ''}
                                            { hasAlreadyName ? <P noBottomMargin>Record name already exists</P> : ''}
                                        </div>
                                        <div className={ isTypeSubmitNull ? 'col-xs-6 input_pay' : 'col-xs-6 select-holiday-type' }>
                                            <P noBottomMargin className={ isTypeSubmitNull ? 'label_err' : '' }>* Holiday Type</P>
                                            <SalSelect
                                                id="type"
                                                value={ form.type || this.props.holidayData.isEdit.type }
                                                ref={ ( ref ) => { this.type = ref; } }
                                                name="holidayType"
                                                className={ isType || isTypeSubmitNull ? 'input_pay' : '' }
                                                data={ HOLIDAYS_TYPE }
                                                onChange={ ({ value }) => {
                                                    this.setState({
                                                        isType: value === '',
                                                        isTypeSubmitNull: false,
                                                        isFormDirty: true
                                                    });
                                                    this.addState( 'type', value );
                                                } }
                                                onBlur={ ( value ) => {
                                                    this.setState({ isType: value === '' });
                                                } }
                                            />
                                            {isTypeSubmitNull ? <P noBottomMargin>Please input valid data</P> : ''}
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className={ dateValidate.flag ? 'col-xs-6 datepicker_contain datealert' : 'col-xs-6 datepicker_contain' }>
                                            <div className="DayPickerInput ">
                                                <DatePicker
                                                    id="date"
                                                    label="* Date"
                                                    name="date"
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    selectedDay={ form.date || this.props.holidayData.isEdit.date }
                                                    ref={ ( ref ) => { this.date = ref; } }
                                                    onChange={ ( value ) => {
                                                        this.setState({
                                                            form: {
                                                                ...form,
                                                                date: this.dateValidation( value ),
                                                                isFormDirty: true
                                                            }}
                                                        , this.addDate( 'date', value ) );
                                                    } }
                                                />
                                                <div className="calendar-icon">
                                                    <Icon name="calendar" className="icon" />
                                                </div>
                                            </div>
                                            {dateValidate.flag ? <P noBottomMargin>{dateValidate.message}</P> : ''}
                                        </div>
                                    </div>
                                    <div className={ isLocationSubmitNull ? 'location_err' : '' }>
                                        <P noBottomMargin className={ isLocationSubmitNull ? 'label_err' : '' }>* Entitled Location/s</P>
                                        <MultiSelect
                                            id="leader"
                                            async
                                            loadOptions={ this.loadLeaderList }
                                            ref={ ( ref ) => { this.leader = ref; } }
                                            value={ this.state.isLocationChanged ? this.mapEntitledLocations( form.entitled_locations ) : this.mapEntitledLocations( this.props.holidayData.isEdit.entitled_locations ) }
                                            placeholder="Enter location"
                                            onChange={ ( value ) => {
                                                this.setState({
                                                    isLocationChanged: true,
                                                    form: {
                                                        ...form,
                                                        entitled_locations: value,
                                                        isFormDirty: true
                                                    }
                                                });
                                            } }
                                            multi
                                        />
                                        {isLocationSubmitNull ? <P noBottomMargin className="label_err">This field is required</P> : ''}
                                    </div>
                                </div>
                            </PageWrapper>
                            <Footer>
                                <div className="submit">
                                    <div className="col-xs-12">
                                        <Button
                                            label="Cancel"
                                            type="action"
                                            size="large"
                                            alt
                                            onClick={ () => this.discardModal.toggle() }
                                        />
                                        <Button
                                            label={ btnLoading ? <Loader /> : 'Update' }
                                            type="action"
                                            size="large"
                                            disabled={ btnLoading }
                                            onClick={ () => this.submitForm() }
                                        />
                                    </div>
                                </div>
                            </Footer>
                        </div>
                    )}
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    holidayData: makeSelectEditHolidays(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    btnLoading: makeSelectBtnLoad()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        holidayEditAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EditHolidays );
