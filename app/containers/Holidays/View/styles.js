import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 200px;
    height:100vh;
`;

export const PageWrapper = styled.div`
    .content{
        margin-top: 90px;
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                margin-bottom: 0;
                font-size: 36px;
                font-weight: 700;
                line-height: 57.6px;
                margin-top: 20px;
            }
        }
    }
    .title {
        display: flex;
        align-items: center;
        margin-bottom: 10px;
        justify-content: space-between;
        height:45px;
        .title-Content{
            display: flex;
            align-items: center;
            p,.dropdown span,span{
                font-size: 14px;
            }
            .btnFilter{
                background-color: transparent;
                color: #adadad;
                border-color: #adadad;
                border-radius: 4px;
                padding: 7px 21px;
                margin-right: 12px;

                >span{
                    color: #474747;
                }
            }

            .filter-icon > svg {
                height: 10px;
            }

            .dropdown {
                span {
                    border-radius: 2px;
                }
            }

            span{
                font-size: 14px;
            }

            .dropdown,
            .dropdown-item {
                font-size: 14px;
            }

            .dropdown-menu {
                min-width: 122px;
            }
        }
    }
    .ReactTable{
        .rt-table{
            .rt-tbody{
                .rt-td{
                    height: 56px;
                }
                .rt-tr-group{
                    .rt-tr.selected{
                        background-color: rgba(131,210,75,.15); 
                    }
                }
            }
        }

        .rt-resizable-header-content {
            font-weight: 700;
        }
    }

    .filter-holiday-type {
        width: 620px;

        .Select-multi-value-wrapper {
            min-width: 561px;
        }
    }
`;
