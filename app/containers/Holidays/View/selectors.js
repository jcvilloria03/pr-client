import { createSelector } from 'reselect';

/**
 * Direct selector to the holidays state domain
 */
const selectHolidaysDomain = () => ( state ) => state.get( 'holidays' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
  selectHolidaysDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectHolidays = () => createSelector(
  selectHolidaysDomain(),
  ( substate ) => substate && substate.get( 'holidays' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectHolidaysDomain(),
  ( substate ) => substate && substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectHolidays,
  makeSelectNotification
};
