/* eslint-disable require-jsdoc */
import {
    DEFAULT_ACTION,
    GET_HOLIDAYS,
    DELETE_HOLIDAYS
} from './constants';

/**
 *
 * Holidays actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function getHolidaysList( payload ) {
    return {
        type: GET_HOLIDAYS,
        payload
    };
}

export function deleteHolidays( payload ) {
    return {
        type: DELETE_HOLIDAYS,
        payload
    };
}
