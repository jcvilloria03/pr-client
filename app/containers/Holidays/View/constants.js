/*
 *
 * Holidays constants
 *
 */
const namespace = 'app/Holidays/View';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const SET_HOLIDAYS = `${namespace}/SET_HOLIDAYS`;
export const GET_HOLIDAYS = `${namespace}/GET_HOLIDAYS`;
export const DELETE_HOLIDAYS = `${namespace}/DELETE_HOLIDAYS`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const GET_NOTIFICATION = `${namespace}/GET_NOTIFICATION`;
