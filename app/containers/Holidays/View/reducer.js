import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    SET_HOLIDAYS,
    SET_NOTIFICATION
} from './constants';

const initialState = fromJS({
    loading: true,
    holidays: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Holidays reducer
 *
 */
function holidaysReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_HOLIDAYS:
            return state.set( 'holidays', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default holidaysReducer;
