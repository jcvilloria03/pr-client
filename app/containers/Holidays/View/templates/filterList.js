import React from 'react';
import moment from 'moment';
import Button from 'components/Button';
import MultiSelect from 'components/MultiSelect';
import { FilterWrapper } from './styles';
import { filterHolidayYears, filterHolidayTypes } from './constants';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {
    static propTypes = {
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    constructor( props ) {
        super( props );
        this.state = {
            years: [moment().year()],
            types: []
        };
    }

    onClickApply = () => {
        const { years, types } = this.state;
        this.props.onApply({
            years,
            types
        });
    }

    onClickCancel = () => {
        this.resetFilters();
    }

    // eslint-disable-next-line react/sort-comp
    resetFilters = () => {
        this.setState({
            years: [moment().year()],
            types: []
        }, () => {
            this.onClickApply();
        });
    }

    render() {
        const { years, types } = this.state;

        return (
            <FilterWrapper>
                <div className="filterMain">
                    <div className="row mb-0 filterRow approvalMain" key="1">
                        <div className="col-xs-3 colSect">
                            <MultiSelect
                                id="year"
                                label={
                                    <span>Year</span>
                                }
                                placeholder="Select Year"
                                value={ years }
                                data={ filterHolidayYears }
                                ref={ ( ref ) => { this.year = ref; } }
                                onChange={ ( data ) => {
                                    const yrs = data.map( ( d ) => d.value );
                                    this.setState({ years: yrs });
                                } }
                            />
                        </div>
                        <div className="filter-holiday-type colSect">
                            <MultiSelect
                                id="types"
                                label={
                                    <span>Holiday Type</span>
                                }
                                placeholder="Select Holiday Type"
                                value={ types }
                                data={ filterHolidayTypes }
                                ref={ ( ref ) => { this.types = ref; } }
                                onChange={ ( data ) => {
                                    const selectedTypes = data.map( ( d ) => d.value );
                                    this.setState({ types: selectedTypes });
                                } }
                            />
                        </div>
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ () => this.resetFilters() }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ () => this.props.onCancel() }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ () => this.onClickApply() }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
