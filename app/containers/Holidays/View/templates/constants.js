/* eslint-disable no-plusplus */
import moment from 'moment';

export const filterData = {
    year: [
        { label: '2019', name: '2019' },
        { label: '2020', name: '2020' },
        { label: '2021', name: '2021' },
        { label: '2022', name: '2022' },
        { label: '2023', name: '2023' },
        { label: '2024', name: '2024' },
        { label: '2025', name: '2025' }
    ],
    types: [
        { name: 'special working', label: 'Special Working Holiday' },
        { name: 'special', label: 'Special Non-Working Holiday' },
        { name: 'regular', label: 'Regular Holiday' }
    ]
};

const getYears = () => {
    const years = [];

    for ( let index = -3; index <= 3; index++ ) {
        const year = moment().add( index, 'years' ).format( 'Y' );

        years.push({
            label: year,
            value: year
        });
    }

    return years;
};

export const filterHolidayYears = getYears();

export const filterHolidayTypes = [
    {
        label: 'Special Working Holiday',
        value: 'special working'
    },
    {
        label: 'Special Non-Working Holiday',
        value: 'special'
    },
    {
        label: 'Regular Holiday',
        value: 'regular'
    }
];
