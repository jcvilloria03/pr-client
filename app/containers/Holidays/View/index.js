/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { isEqual } from 'lodash';

import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';
import Sidebar from 'components/Sidebar';
import { H2, H3, H5 } from 'components/Typography';
import Table from 'components/Table';
import { getIdsOfSelectedRows, isAnyRowSelected } from 'components/Table/helpers';
import Button from 'components/Button';
import SalDropdown from 'components/SalDropdown';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import SnackBar from 'components/SnackBar';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import * as holidaysActions from './actions';
import {
    makeSelectLoading,
    makeSelectHolidays,
    makeSelectNotification
} from './selectors';
import Filter from './templates/filterList';
import {
    PageWrapper,
    LoadingStyles
} from './styles';

/**
 *
 * Holidays
 *
 */
export class Holidays extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        loading: React.PropTypes.bool,
        getHolidaysList: React.PropTypes.func,
        holidays: React.PropTypes.array,
        deleteHolidays: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            holidaysList: [],
            showFilter: false,
            label: 'Showing 0-0 of 0 entries',
            selectionLabel: '',
            totalSelected: 0,
            totalPages: 0,
            flag: true,
            selectPage: false,
            sortParams: {
                sort: 'date',
                sortOrder: 'asc'
            },
            filters: {
                years: [moment().year()],
                types: []
            },
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            }
        };
        this.confirmSaveModal = null;
    }

    componentWillMount() {
        const data = {
            filters: this.state.filters,
            sortParams: this.state.sortParams
        };

        this.props.getHolidaysList( data );
    }

    componentWillReceiveProps( nextProps ) {
        const listChanged = !isEqual( nextProps.holidays, this.props.holidays );

        this.setState( ( prevState ) => ({
            displayedData: nextProps.holidays,
            label: formatPaginationLabel( this.holidaysTable.tableComponent.state ),
            pagination: {
                total: nextProps.holidays.length,
                per_page: 10,
                current_page: listChanged ? 1 : prevState.pagination.current_page,
                last_page: Math.ceil( nextProps.holidays.length / 10 ),
                to: 0
            }
        }) );
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.holidaysTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.holidaysTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    getDropdownItems() {
        return [
            {
                children: <div>Delete</div>,
                onClick: this.deletePayrollRecord
            }
        ];
    }

    deletePayrollRecord=() => {
        this.confirmSaveModal.toggle();
    }

    modalDeleteClick=() => {
        const ids = getIdsOfSelectedRows( this.holidaysTable );
        const companyId = company.getLastActiveCompanyId();
        const payload = {
            company_id: companyId,
            ids,
            filters: this.state.filters,
            sortParams: this.state.sortParams
        };

        this.props.deleteHolidays( payload );
        this.confirmSaveModal.toggle();
    }

    handleTableChanges = ( tableProps = this.holidaysTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            selection_Label: formatDeleteLabel()
        });
    }

    toggleFilter = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    pageSelectOne=( pageObj ) => {
        let isTrue;
        // eslint-disable-next-line no-restricted-syntax
        for ( const key in pageObj ) {
            if ( ( this.holidaysTable.state.page === key ) && ( pageObj[ key ] === true ) ) {
                isTrue = pageObj[ key ];
            }
        }
        return isTrue;
    }

    formatList( list ) {
        if ( !list || list === undefined ) return '';

        return this.convertListToString( list.map( ( item ) => item.name ) );
    }

    convertListToString( list ) {
        if ( list && list.length === 0 ) {
            return '';
        }
        const firstItem = list[ 0 ];

        if ( list.length === 1 ) {
            return firstItem;
        }

        return list.length > 2
          ? `${firstItem} and ${list.length - 1} more`
          : `${firstItem} and ${list[ 1 ]}`;
    }

    applyFilters( filters ) {
        this.setState({ filters }, () => {
            const data = {
                filters: this.state.filters,
                sortParams: this.state.sortParams
            };

            this.props.getHolidaysList( data );
        });
    }

    /**
     *
     * Holidays render method
     *
     */
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const tableColumn = [
            {
                header: 'Holiday Name',
                accessor: 'name',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.name}
                    </div>
                )
            },
            {
                header: 'Holiday Date',
                accessor: 'date',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { moment( row.date ).format( 'LL' ) }
                    </div>
                )
            },
            {
                header: 'Holiday Type',
                accessor: 'true',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.display_type}
                    </div>
                )
            },
            {
                header: 'Entitled Locations',
                accessor: 'entitled_locations',
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {this.formatList( row.entitled_locations )}
                    </div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                width: 100,
                sortable: false,
                style: { justifyContent: 'end' },
                render: ({ row }) => (
                    <Button
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => browserHistory.push( `/company-settings/schedule-settings/holidays/${row.id}/edit`, true ) }
                    />
                )
            }
        ];
        const { notification, loading, holidays, getHolidaysList } = this.props;
        const { selectionLabel, label, selectPage, flag, totalSelected, showFilter } = this.state;

        return (
            <div>
                <Helmet
                    title="Holidays"
                    meta={ [
                        { name: 'description', content: 'Description of Holidays' }
                    ] }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <SnackBar
                    message={ notification && notification.message }
                    title={ notification && notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification && notification.show }
                    delay={ 5000 }
                    type={ notification && notification.type }
                />
                <Modal
                    title="Confirm Your Action"
                    body={
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonCancel',
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.confirmSaveModal.toggle()
                        },
                        {
                            id: 'buttonProceed',
                            type: 'darkRed',
                            label: 'Yes',
                            onClick: () => this.modalDeleteClick()
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.confirmSaveModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Holiday List</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Holidays</H3>
                            </div>
                            <div className="title">
                                <H5 noBottomMargin>Holiday List</H5>
                                <span className="title-Content">
                                    {isAnyRowSelected( this.holidaysTable ) ?
                                        <div>
                                            <span className="mr-1">{selectionLabel}</span>
                                            <Button
                                                id="button-filter"
                                                label={ <span><Icon name="filter" className="filter-icon" /> Filter</span> }
                                                type="neutral"
                                                size="large"
                                                onClick={ () => this.toggleFilter() }
                                                className="btnFilter"
                                            />
                                        </div> :
                                        <div>
                                            <span className="mr-1">{label}</span>
                                            <Button
                                                id="button-filter"
                                                label={ <span><Icon name="filter" className="filter-icon" /> Filter</span> }
                                                type="neutral"
                                                size="large"
                                                onClick={ () => this.toggleFilter() }
                                                className="btnFilter"
                                            />
                                            <Button
                                                label="Add holiday"
                                                type="action"
                                                onClick={ () => browserHistory.push( '/company-settings/schedule-settings/holidays/add', true ) }
                                            />
                                        </div>
                                    }
                                    { isAnyRowSelected( this.holidaysTable ) && (
                                        <SalDropdown
                                            label="More Actions"
                                            dropdownItems={ this.getDropdownItems() }
                                            className="sagar"
                                        />
                                    ) }
                                </span>
                            </div>
                            {selectPage === true && flag === 'false' ?
                                <div className="selectPage">
                                    <p>{`All ${totalSelected} entries on this page are selected.`}<a onClick={ () => this.checkBoxchanged() }> {`Select all ${holidays.length} entries from all pages`}</a></p>
                                </div>
                                : ''
                            }
                            <div style={ { display: showFilter ? 'block' : 'none' } }>
                                <Filter
                                    products={ getHolidaysList }
                                    onCancel={ () => { this.toggleFilter(); } }
                                    onApply={ ( filters ) => {
                                        this.applyFilters( filters );
                                    } }
                                />
                            </div>
                            <Table
                                data={ holidays }
                                columns={ tableColumn }
                                onDataChange={ this.handleTableChanges }
                                selectable
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    const isSelected = holidays.length === selectionLength ? 'true' : 'false';
                                    this.setState({
                                        totalSelected: selectionLength,
                                        selectPage: this.pageSelectOne( this.holidaysTable.state.pageSelected ),
                                        flag: isSelected,
                                        selectionLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                                ref={ ( ref ) => { this.holidaysTable = ref; } }
                            />

                            <div>
                                <FooterTablePaginationV2
                                    page={ this.state.pagination.current_page }
                                    pageSize={ this.state.pagination.per_page }
                                    pagination={ this.state.pagination }
                                    onPageChange={ this.onPageChange }
                                    onPageSizeChange={ this.onPageSizeChange }
                                    paginationLabel={ this.state.label }
                                    fluid
                                />
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    holidays: makeSelectHolidays(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        holidaysActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Holidays );
