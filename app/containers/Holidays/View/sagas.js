/* eslint-disable import/first */
/* eslint-disable require-jsdoc */
import { LOCATION_CHANGE } from 'react-router-redux';
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay } from 'redux-saga';
import { Fetch } from 'utils/request';
import moment from 'moment';

import {
    LOADING,
    SET_HOLIDAYS,
    GET_HOLIDAYS,
    DELETE_HOLIDAYS,
    SET_NOTIFICATION,
    GET_NOTIFICATION
} from './constants';
import { company } from 'utils/CompanyService';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* holidysList({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const types = payload.filters && payload.filters.types ? payload.filters.types : [];
        const years = payload.filters && payload.filters.years ? payload.filters.years : [];

        let url = `/company/${companyId}/holidays?sort_by=${payload.sortParams.sort}&sort_order=${payload.sortParams.sortOrder}`;
        if ( years.length ) {
            years.forEach( ( year ) => {
                url += `&years[]=${year}`;
            });
        } else {
            url += `&years[]=${moment().format( 'Y' )}`;
        }

        if ( types.length ) {
            types.forEach( ( type ) => {
                url += `&types[]=${type}`;
            });
        }

        const holidaysData = yield call( Fetch, url, {
            method: 'GET'
        });
        yield put({
            type: SET_HOLIDAYS,
            payload: holidaysData && holidaysData.data || []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

export function* deleteHolidays({ payload }) {
    try {
        const formData = new FormData();

        formData.append( '_method', 'DELETE' );
        formData.append( 'company_id', payload.company_id );
        payload.ids.forEach( ( id ) => {
            formData.append( 'holidays_ids[]', id );
        });

        yield call( Fetch, '/holiday/bulk_delete', {
            method: 'POST',
            data: formData
        });

        yield call( holidysList, { payload });

        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Records deleted successfully',
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 2000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

export function* watchForApprovalList() {
    const watcher = yield takeEvery( GET_HOLIDAYS, holidysList );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForDeleteHolidays() {
    const watcher = yield takeEvery( DELETE_HOLIDAYS, deleteHolidays );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchNotify() {
    const watcher = yield takeEvery( GET_NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForApprovalList,
    watchForDeleteHolidays,
    watchNotify
];
