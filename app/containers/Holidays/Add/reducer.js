import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SUBMITTED,
    SET_NOTIFICATION,
    BTN_LOADING
} from './constants';

const initialState = fromJS({
    submitted: '',
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    btnLoading: false
});

/**
 *
 * AddHolidays reducer
 *
 */
function addHolidaysReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SUBMITTED:
            return state.set( 'submitted', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default addHolidaysReducer;
