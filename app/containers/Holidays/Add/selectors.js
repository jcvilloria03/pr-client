import { createSelector } from 'reselect';

/**
 * Direct selector to the addHolidays state domain
 */
const selectAddHolidaysDomain = () => ( state ) => state.get( 'addHolidays' );

const makeSelectNotification = () => createSelector(
  selectAddHolidaysDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBtnLoad = () => createSelector(
  selectAddHolidaysDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

/**
 * Default selector used by AddHolidays
 */
export {
  selectAddHolidaysDomain,
  makeSelectNotification,
  makeSelectBtnLoad
};
