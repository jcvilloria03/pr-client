/* eslint-disable require-jsdoc */
import get from 'lodash/get';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { cancel, put, call, take } from 'redux-saga/effects';
import { Fetch } from 'utils/request';

import { company } from '../../../utils/CompanyService';

import {
    NOTIFICATION,
    SET_NOTIFICATION,
    SET_SUBMITTED,
    BTN_LOADING
} from './constants';

import { browserHistory } from '../../../utils/BrowserHistory';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* submitForm({ payload }) {
    try {
        yield put({
            type: BTN_LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();

        const data = {
            company_id: companyId,
            ...payload
        };
        yield call( Fetch, '/holiday', { method: 'POST', data });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully Added',
            show: true,
            type: 'success'
        });
        browserHistory.push( '/company-settings/schedule-settings/holidays', true );
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message || `Request failed with statuscode ${error.response.status}` : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}

export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 2000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

export function* watchForSubmitForm() {
    const watcher = yield takeLatest( SET_SUBMITTED, submitForm );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForSubmitForm
];
