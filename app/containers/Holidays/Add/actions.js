/* eslint-disable require-jsdoc */
import {
    DEFAULT_ACTION,
    SET_SUBMITTED
} from './constants';

/**
 *
 * AddHolidays actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function submitHoliday( payload ) {
    return {
        type: SET_SUBMITTED,
        payload
    };
}
