/*
 *
 * AddHolidays constants
 *
 */
const namespace = 'app/Holidays/Add';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const SET_SUBMITTED = `${namespace}/SET_SUBMITTED`;
export const SUBMITTED = `${namespace}/SUBMITTED`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const BTN_LOADING = `${namespace}/BTN_LOADING`;

export const HOLIDAYS_TYPE = [
    { label: 'Special Working Holiday', value: 'Special Working', disabled: false },
    { label: 'Special Non-Working Holiday', value: 'Special', disabled: false },
    { label: 'Regular Holiday', value: 'Regular', disabled: false }
];

export const SELECT_ALL_EMPLOYEES_OPTIONS = [
    {
        value: null,
        label: 'All Location',
        uid: 'All Locationnull'
    }
];
