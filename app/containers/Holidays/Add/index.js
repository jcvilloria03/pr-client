import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment';

import A from 'components/A';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Input from 'components/Input';
import SalSelect from 'components/Select';
import DatePicker from 'components/DatePicker';
import MultiSelect from 'components/MultiSelect';
import Modal from 'components/Modal';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import Icon from 'components/Icon';
import { H3, P } from 'components/Typography';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';
import { DATE_FORMATS } from 'utils/constants';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';

import { HOLIDAYS_TYPE, SELECT_ALL_EMPLOYEES_OPTIONS } from './constants';
import * as holidayAction from './actions';
import {
    makeSelectNotification,
    makeSelectBtnLoad
} from './selectors';
import {
    Header,
    PageWrapper,
    Footer
} from './styles';

/**
 *
 * AddHolidays
 *
 */
export class AddHolidays extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        submitHoliday: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        btnLoading: React.PropTypes.bool
    }

    constructor( props ) {
        super( props );

        this.state = {
            form: {
                name: '',
                type: '',
                date: '',
                entitled_locations: [],
                recurring: false
            },
            isNameNull: false,
            hasAlreadyName: false,
            isNameSubmitNull: false,
            isType: false,
            isTypeSubmitNull: false,
            dateValidate: {
                flag: false,
                message: ''
            },
            isLocationSubmitNull: false
        };
        this.discardModal = null;
    }

    validateForm() {
        let valid = true;

        const isNameSubmitNull = this.state.form.name === '';
        this.setState({ isNameSubmitNull });

        if ( isNameSubmitNull ) {
            valid = false;
        }

        const isTypeSubmitNull = this.state.form.type === '';
        this.setState({ isTypeSubmitNull });
        if ( isTypeSubmitNull ) {
            valid = false;
        }

        const value = this.state.form.date;
        if ( value === 'Invalid date' || value === '' ) {
            const message = 'Please choose a date';
            this.setState({ dateValidate: { flag: true, message }});
            valid = false;
        } else {
            this.setState({ dateValidate: { flag: false, message: '' }});
        }

        const formValue = this.state.form;
        if ( formValue.entitled_locations.length === 0 ) {
            valid = false;
            this.setState({ isLocationSubmitNull: true });
        } else {
            this.setState({ isLocationSubmitNull: false });
        }

        return valid;
    }

    async submitForm() {
        const valid = this.validateForm();

        if ( valid ) {
            const formValue = this.state.form;
            const companyId = company.getLastActiveCompanyId();
            const response = await Fetch( `/company/${companyId}/holiday/is_name_available`, { method: 'POST', data: formValue });
            if ( response.available ) {
                const tages = formValue.entitled_locations.map( ( value ) => ({ id: value.value }) );
                await this.props.submitHoliday({ ...formValue, entitled_locations: tages });
            } else {
                this.setState({ hasAlreadyName: true });
            }
        }
    }

    addDate = ( name, value ) => {
        const values = value === '';
        this.setState({ [ name ]: values });
    }

    addState = ( name, value ) => {
        this.setState({
            form: {
                ...this.state.form,
                [ name ]: value
            }
        });
    }

    loadLeaderList = ( keyword, callback ) => {
        const companyId = company.getLastActiveCompanyId();
        Fetch( `/company/${companyId}/time_attendance_locations?name=${keyword}&limit=10&is_simplified=true`, { method: 'GET' })
            .then( ( result ) => {
                const list = result.data.map( ( option ) => ({
                    account_id: option.account_id,
                    company_id: companyId,
                    value: option.id,
                    is_headquarters: false,
                    label: option.name,
                    uid: option.name + option.id
                }) );
                callback( null, { options: SELECT_ALL_EMPLOYEES_OPTIONS.concat( list ) });
            })
            .catch( ( error ) => callback( error, null ) );
    }

    handleDateFormat = ( date ) => moment( date ).format( 'YYYY-MM-DD' )

    dateValidation = ( value ) => {
        const date = this.handleDateFormat( value );
        if ( value === 'Invalid date' || value === '' ) {
            const message = 'Please choose a date';
            this.setState({ dateValidate: { flag: true, message }});
        } else {
            this.setState({ dateValidate: { flag: false, message: '' }});
        }
        return date;
    }
    /**
     *
     * AddHolidays render method
     *
     */
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const {
            form,
            isNameSubmitNull,
            isNameNull,
            hasAlreadyName,
            isTypeSubmitNull,
            isType,
            dateValidate,
            isLocationSubmitNull
        } = this.state;

        const {
            notification,
            btnLoading
        } = this.props;

        return (
            <div>
                <Helmet
                    title="Add Holiday"
                    meta={ [
                        { name: 'description', content: 'Description of AddHolidays' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Discard Changes"
                    body={
                        <div>
                            <p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.discardModal.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/company-settings/schedule-settings/holidays', true );
                            }
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Header>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/schedule-settings/holidays', true );
                                } }
                            >
                                <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Holidays</span>
                            </A>
                        </Container>
                    </div>
                </Header>
                <PageWrapper>
                    <div className="main_section">
                        <div className="heading">
                            <H3>Add Holiday</H3>
                        </div>
                        <div className="row">
                            <div className={ ( isNameSubmitNull || hasAlreadyName ) ? 'col-xs-6 input_pay' : 'col-xs-6' }>
                                <P noBottomMargin className={ ( isNameSubmitNull || hasAlreadyName ) ? 'label_err' : '' }>* Holiday Name</P>
                                <Input
                                    id="holiday_name"
                                    type="text"
                                    name="name"
                                    className={ ( isNameNull || isNameSubmitNull ) ? 'input_pay' : '' }
                                    ref={ ( ref ) => { this.name = ref; } }
                                    value={ form.name }
                                    onChange={ ( value ) => {
                                        this.setState({ isNameNull: value === '', isNameSubmitNull: false, hasAlreadyName: false });
                                        this.addState( 'name', value );
                                    } }
                                    onBlur={ ( value ) => {
                                        this.setState({ isNameNull: value === '' });
                                    } }
                                />
                                {isNameSubmitNull ? <P noBottomMargin>This field is required</P> : ''}
                                {hasAlreadyName ? <P noBottomMargin>Record name already exists</P> : ''}
                            </div>
                            <div className={ isTypeSubmitNull ? 'col-xs-6 input_pay' : 'col-xs-6 select-holiday-type' }>
                                <P noBottomMargin className={ isTypeSubmitNull ? 'label_err' : '' }>* Holiday Type</P>
                                <SalSelect
                                    id="type"
                                    value={ form.type }
                                    ref={ ( ref ) => { this.type = ref; } }
                                    name="holidayType"
                                    className={ ( isType || isTypeSubmitNull ) ? 'input_pay' : '' }
                                    data={ HOLIDAYS_TYPE }
                                    onChange={ ({ value }) => {
                                        this.setState({ isType: value === '', isTypeSubmitNull: false });
                                        this.addState( 'type', value );
                                    } }
                                    onBlur={ ( value ) => {
                                        this.setState({ isType: value === '' });
                                    } }
                                />
                                {isTypeSubmitNull ? <P noBottomMargin>Please input valid data</P> : ''}
                            </div>
                        </div>
                        <div className="row">
                            <div className={ dateValidate.flag ? 'col-xs-6 datepicker_contain datealert' : 'col-xs-6 datepicker_contain' }>
                                <div className="DayPickerInput ">
                                    <DatePicker
                                        id="date"
                                        label="* Date"
                                        name="date"
                                        dayFormat={ DATE_FORMATS.DISPLAY }
                                        selectedDay={ form.date }
                                        ref={ ( ref ) => { this.date = ref; } }
                                        onChange={ ( value ) => {
                                            this.setState({ form: { ...form, date: this.dateValidation( value ) }}, this.addDate( 'date', value ) );
                                        } }
                                    />
                                    <div className="calendar-icon">
                                        <Icon name="calendar" className="icon" />
                                    </div>
                                </div>
                                {dateValidate.flag ? <P noBottomMargin>{dateValidate.message}</P> : ''}
                            </div>
                        </div>
                        <div className={ isLocationSubmitNull ? 'location_err' : '' }>
                            <P noBottomMargin className={ isLocationSubmitNull ? 'label_err' : '' }>* Entitled Location/s</P>
                            <MultiSelect
                                id="leader"
                                async
                                loadOptions={ this.loadLeaderList }
                                ref={ ( ref ) => { this.leader = ref; } }
                                value={ form.entitled_locations }
                                placeholder="Enter location"
                                onChange={ ( value ) => {
                                    this.setState({ form: { ...form, entitled_locations: value }});
                                    if ( value.length > 0 ) {
                                        this.setState({ isLocationSubmitNull: false });
                                    } else {
                                        this.setState({ isLocationSubmitNull: true });
                                    }
                                } }
                                multi
                            />
                            {isLocationSubmitNull ? <P noBottomMargin className="label_err">This field is required</P> : ''}
                        </div>
                    </div>
                </PageWrapper>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                label={ btnLoading ? <Loader /> : 'Submit' }
                                type="action"
                                size="large"
                                disabled={ btnLoading }
                                onClick={ () => this.submitForm() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    btnLoading: makeSelectBtnLoad()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        holidayAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddHolidays );
