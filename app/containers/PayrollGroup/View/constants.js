/*
 *
 * CompanyPayroll constants
 *
 */
const namespace = 'app/containers/CompanyPayroll';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const REINITIALIZE_PAGE = `${namespace}/REINITIALIZE_PAGE`;
export const GET_COMPANY_PAYROLL_LIST = `${namespace}/GET_COMPANY_PAYROLL_LIST`;
export const SET_COMPANY_PAYROLL_LIST = `${namespace}/SET_COMPANY_PAYROLL_LIST`;
export const SET_TOTAL_PAGE = `${namespace}/SET_TOTAL_PAGE`;
export const DELETE_PAYROLL_GROUP = `${namespace}/DELETE_PAYROLL_GROUP`;
