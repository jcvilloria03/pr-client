/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-restricted-syntax */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';

import SnackBar from 'components/SnackBar';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import SalDropdown from 'components/SalDropdown';
import Modal from 'components/Modal';
import Table from 'components/Table';
import { H2, H3, H5 } from 'components/Typography';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';

import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { getIdsOfSelectedRows, isAnyRowSelected } from 'components/Table/helpers';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';
import * as companyPayrollAction from './actions';
import { makeSelectCompanyPayroll, makeSelectNotification, makeSelectLoading } from './selectors';
import { LoadingStyles, PageWrapper } from './styles';

/**
 *
 * CompanyPayroll
 *
 */
export class CompanyPayroll extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes={
        getCompanyPayroll: React.PropTypes.func,
        CompanyPayrollData: React.PropTypes.object,
        deletePayrollGroup: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            permissions: {
                create: false,
                edit: true
            },
            label: 'Showing 0-0 of 0 entries',
            selectionLabel: '',
            selectLength: '',
            payrollList: [],
            totalPages: 0,
            totalSelected: 0,
            flag: true,
            selectPage: false
        };
        this.confirmSaveModal = null;
    }
    componentDidMount() {
        this.props.getCompanyPayroll();
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.CompanyPayrollData.payrolls !== this.props.CompanyPayrollData.payrolls ) {
            this.setState({
                payrollList: nextProps.CompanyPayrollData.payrolls,
                totalPages: nextProps.CompanyPayrollData.totalPage
            });
        }
    }

    getTableColumns() {
        return [
            {
                id: 'name',
                header: 'Payroll Group',
                accessor: 'name',
                minWidth: 130,
                style: { whiteSpace: 'break-spaces' }
            },
            {
                id: 'payroll_frequency',
                header: 'Pay Frequency',
                accessor: 'payroll_frequency',
                minWidth: 120
            },
            {
                id: 'enforce_gap_loans',
                header: 'Gap Loans',
                accessor: 'enforce_gap_loans',
                minWidth: 100
            },
            {
                id: 'day_factor',
                header: 'Working Days',
                accessor: 'day_factor',
                minWidth: 120
            },
            {
                id: 'hours_per_day',
                header: 'Working Hours',
                accessor: 'hours_per_day',
                minWidth: 120
            },
            {
                id: 'cut_off_date',
                header: 'Cut-off Date',
                accessor: 'cut_off_date',
                minWidth: 190,
                sortable: false,
                style: { whiteSpace: 'break-spaces' },
                render: ({ row }) => (
                    <div>
                        <p>{row.cut_off_date}</p>
                        <p> {row.cut_off_date_2 && row.cut_off_date_2}</p>
                    </div>
                )
            },
            {
                id: 'non_working_day_option',
                header: 'Payrun falls on non-working day',
                accessor: 'non_working_day_option',
                minWidth: 250,
                style: { whiteSpace: 'break-spaces' }
            },
            {
                id: 'pay_date',
                header: 'Pay Date',
                accessor: 'pay_date',
                sortable: false,
                minWidth: 190,
                style: { whiteSpace: 'break-spaces' },
                render: ({ row }) => (
                    <div>
                        <p>{row.pay_date}</p>
                        <p> {row.pay_date_2 && row.pay_date_2}</p>
                    </div>
                )
            },
            {
                id: 'wtax_schedule',
                header: 'WTAX Schedule',
                accessor: 'wtax_schedule',
                minWidth: 110
            },
            {
                id: 'sss_schedule',
                header: 'SSS Schedule',
                accessor: 'sss_schedule',
                minWidth: 110
            },
            {
                id: 'philhealth_schedule',
                header: 'PhilHealth Schedule',
                accessor: 'philhealth_schedule',
                minWidth: 110
            },
            {
                id: 'hdmf_schedule',
                header: 'HDMF Schedule',
                accessor: 'hdmf_schedule',
                minWidth: 110
            },
            {
                id: 'wtax_method',
                header: 'WTAX Method',
                accessor: 'wtax_method',
                minWidth: 150
            },
            {
                id: 'sss_method',
                header: 'SSS Method',
                accessor: 'sss_method',
                minWidth: 110
            },
            {
                id: 'philhealth_method',
                header: 'PhilHealth Method',
                accessor: 'philhealth_method',
                minWidth: 110
            },
            {
                id: 'hdmf_method',
                header: 'HDMF Method',
                accessor: 'hdmf_method',
                minWidth: 110
            },
            {
                header: ' ',
                minWidth: 150,
                sortable: false,
                style: { justifyContent: 'end' },
                render: ({ row }) => this.state.permissions.edit && (
                    <Button
                        label={ <span>Edit </span> }
                        type="grey"
                        size="small"
                        to={ [ `/company-settings/payroll/payroll-groups/${row.id}/edit/manual`, true ] }
                    />
                )
            }
        ];
    }

    /**
     * Generates config for dropdown items
     * @returns {Object[]}
     */
    getDropdownItems() {
        return [
            {
                children: <div>Delete</div>,
                onClick: this.deletePayrollRecord
            }
        ];
    }

    deletePayrollRecord=() => {
        this.confirmSaveModal.toggle();
    }

    modalDeleteClick=() => {
        const ids = getIdsOfSelectedRows( this.companyPayrollTable );
        const companyId = company.getLastActiveCompanyId();
        const payload = ({ company_id: companyId, ids });

        this.props.deletePayrollGroup( payload );
        this.confirmSaveModal.toggle();

        setTimeout( () => this.props.getCompanyPayroll(), 5000 );
    }

    checkBoxchanged=() => {
        this.companyPayrollTable.state.selected = this.companyPayrollTable.state.selected.map( ( value ) => {
            this.setState({ flag: false });
            return ( value || !value );
        });
        const selectionLength = this.companyPayrollTable.state.selected.length;
        this.setState({ selectPage: false, selectionLabel: formatDeleteLabel( selectionLength ) });
    }

    /**
    * Handles table changes
    */
    handleTableChanges = ( tableProps = this.companyPayrollTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            selection_Label: formatDeleteLabel()
        });

        if ( this.companyPayrollTable ) {
            this.setState({ selectPage: this.pageSelectOne( this.companyPayrollTable.state.pageSelected ) || false });
        }
    }

    pageSelectOne=( pageObj ) => {
        let isTrue;
        for ( const key in pageObj ) {
            if ( ( this.companyPayrollTable.tableComponent.state.page === +key ) && ( pageObj[ key ] === true ) ) {
                isTrue = pageObj[ key ];
            }
        }
        return isTrue;
    }
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { loading, CompanyPayrollData } = this.props;
        const { selectPage, flag, totalSelected, selectionLabel, label } = this.state;
        return (
            <div style={ { height: '100vh' } }>
                <Helmet
                    title="Payroll Groups"
                    meta={ [
                        { name: 'description', content: 'Description of Payroll Groups' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />

                <Modal
                    title="Confirm Your Action"
                    body={
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonCancel',
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.confirmSaveModal.toggle()
                        },
                        {
                            id: 'buttonProceed',
                            type: 'danger',
                            label: 'Yes',
                            onClick: () => this.modalDeleteClick()
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.confirmSaveModal = ref; } }
                />
                <PageWrapper>
                    <Container>
                        {CompanyPayrollData.loading === true &&
                        <div className="loader" style={ { display: loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Company Payroll Groups.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        }
                        {CompanyPayrollData.loading === false &&
                        <div className="content" style={ { display: loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Payroll Groups</H3>
                                <p>View and update payroll groups. You can add, edit, or delete payroll groups.</p>
                            </div>
                            <div className="title">
                                <H5>Payroll Groups List</H5>
                                <span className="title-Content">
                                    {isAnyRowSelected( this.companyPayrollTable ) ? <p className="mb-0 mr-1">{selectionLabel}</p> :
                                    <div>
                                        <span className="mr-1">{label}</span>
                                        <Button
                                            label="Add Payroll Group"
                                            type="action"
                                            onClick={ () => browserHistory.push( '/company-settings/payroll/payroll-groups/add/manual', true ) }
                                        />
                                    </div>
                                    }
                                    { isAnyRowSelected( this.companyPayrollTable ) && (
                                        <SalDropdown dropdownItems={ this.getDropdownItems() } />
                                    ) }
                                </span>
                            </div>
                            {selectPage === true && flag === 'false' ?
                                <div className="selectPage">
                                    <p>{`All ${totalSelected} entries on this page are selected.`}<a onClick={ () => this.checkBoxchanged() }> {`Select all ${CompanyPayrollData.payrolls.length} entries from all pages`}</a></p>
                                </div>
                                : ''
                            }
                            <Table
                                data={ CompanyPayrollData.payrolls }
                                columns={ this.getTableColumns() }
                                pagination
                                selectable
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.companyPayrollTable = ref; } }
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    const isSelected = CompanyPayrollData.payrolls.length === selectionLength ? 'true' : 'false';
                                    this.setState({
                                        totalSelected: selectionLength,
                                        selectPage: this.pageSelectOne( this.companyPayrollTable.state.pageSelected ),
                                        flag: isSelected,
                                        selectionLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                            />
                        </div>
                        }
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    CompanyPayrollData: makeSelectCompanyPayroll(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        companyPayrollAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( CompanyPayroll );
