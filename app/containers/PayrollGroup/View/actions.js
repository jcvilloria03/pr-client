import {
    GET_COMPANY_PAYROLL_LIST,
    DELETE_PAYROLL_GROUP,
    DEFAULT_ACTION
} from './constants';

/**
 *
 * CompanyPayroll actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * payroll Request Type
 * @param {object} payload
 */
export function getCompanyPayroll() {
    return {
        type: GET_COMPANY_PAYROLL_LIST
    };
}

/**
 * payroll Request Type
 * @param {object} payload
 */
export function deletePayrollGroup( payload ) {
    return {
        type: DELETE_PAYROLL_GROUP,
        payload
    };
}
