import { createSelector } from 'reselect';

/**
 * Direct selector to the companyPayroll state domain
 */
const selectCompanyPayrollDomain = () => ( state ) => state.get( 'PayrollGroup' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
  selectCompanyPayrollDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectPayrollList = () => createSelector(
  selectCompanyPayrollDomain(),
  ( substate ) => substate.get( 'payrolls' )
);

const makeSelectNotification = () => createSelector(
  selectCompanyPayrollDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

/**
 * Default selector used by CompanyPayroll
 */
const makeSelectCompanyPayroll = () => createSelector(
  selectCompanyPayrollDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectCompanyPayroll,
  selectCompanyPayrollDomain,
  makeSelectLoading,
  makeSelectPayrollList,
  makeSelectNotification
};
