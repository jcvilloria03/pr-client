/* eslint-disable no-unused-expressions */
/* eslint-disable no-param-reassign */
// import { take, call, put, select } from 'redux-saga/effects';

import { get } from 'lodash';
import moment from 'moment';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { cancel, put, call, take } from 'redux-saga/effects';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { PAYROLL_GROUP_VALUES, PAY_FREQUENCY_VALUES, RECORD_DELETE_MESSAGE } from 'utils/constants';
import { formatFeedbackMessage } from 'utils/functions';
import { resetStore } from '../../App/sagas';
import { DELETE_PAYROLL_GROUP, GET_COMPANY_PAYROLL_LIST, LOADING, NOTIFICATION, REINITIALIZE_PAGE, SET_COMPANY_PAYROLL_LIST, SET_NOTIFICATION, SET_TOTAL_PAGE } from './constants';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Company-Payroll fetch data
 */
export function* getCompanyPayroll() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const PayrollList = yield call( Fetch, `/philippine/company/${companyId}/payroll_groups`, { method: 'GET' });
        yield put({ type: SET_TOTAL_PAGE, payload: PayrollList && PayrollList.data.length || 0 });
        const stateCase = ( value, type ) => ( value && type ? PAYROLL_GROUP_VALUES[ type ][ value ] : '' );
        if ( PayrollList.data.length ) {
            PayrollList.data.map( ( payroll ) => {
                if ( payroll.payroll_frequency === PAY_FREQUENCY_VALUES.weekly ) {
                    payroll.cut_off_date = `Every ${moment( payroll.cut_off_date ).format( 'dddd' )} of the Month`;
                    payroll.pay_date = `Every ${moment( payroll.pay_date ).format( 'dddd' )} of the Month`;
                } else if ( payroll.payroll_frequency === PAY_FREQUENCY_VALUES.fortnightly ) {
                    payroll.cut_off_date = 'Every 14 days of the Month';
                    payroll.pay_date = 'Every 14 days of the Month';
                } else if ( payroll.payroll_frequency === PAY_FREQUENCY_VALUES.semiMonthly ) {
                    payroll.cut_off_date = `1st run: Every ${moment( payroll.cut_off_date ).format( 'Do' )} of the Month`;
                    payroll.cut_off_date_2 = `2nd run:Every ${moment( payroll.cut_off_date_2 ).format( 'Do' )} of the Month`;
                    payroll.pay_date = `1st run: Every ${moment( payroll.pay_date ).format( 'Do' )} of the Month`;
                    payroll.pay_date_2 = `2nd run:Every ${moment( payroll.pay_date_2 ).format( 'Do' )} of the Month`;
                } else {
                    payroll.cut_off_date = `Every ${moment( payroll.cut_off_date ).format( 'Do' )} of the Month`;
                    payroll.pay_date = `Every ${moment( payroll.pay_date ).format( 'Do' )} of the Month`;
                }

                payroll.payroll_frequency = stateCase( payroll.payroll_frequency, 'FREQUENCY' );
                payroll.non_working_day_option = stateCase( payroll.non_working_day_option, 'NON_WORKING_DAYS' );
                payroll.hdmf_schedule = stateCase( payroll.hdmf_schedule, 'SCHEDULES_AND_METHODS' );
                payroll.sss_schedule = stateCase( payroll.sss_schedule, 'SCHEDULES_AND_METHODS' );
                payroll.philhealth_schedule = stateCase( payroll.philhealth_schedule, 'SCHEDULES_AND_METHODS' );
                payroll.wtax_schedule = stateCase( payroll.wtax_schedule, 'SCHEDULES_AND_METHODS' );
                payroll.wtax_method = stateCase( payroll.wtax_method, 'SCHEDULES_AND_METHODS' );
                payroll.hdmf_method = stateCase( payroll.hdmf_method, 'SCHEDULES_AND_METHODS' );
                payroll.sss_method = stateCase( payroll.sss_method, 'SCHEDULES_AND_METHODS' );
                payroll.philhealth_method = stateCase( payroll.philhealth_method, 'SCHEDULES_AND_METHODS' );
                payroll.day_factor = `${Math.ceil( payroll.day_factor )} per year`;
                payroll.hours_per_day = `${Math.ceil( payroll.hours_per_day )} per day`;
                payroll.enforce_gap_loans === true ? payroll.enforce_gap_loans = 'On' : payroll.enforce_gap_loans = 'Off';
                return payroll;
            });
        }
        yield put({ type: SET_COMPANY_PAYROLL_LIST, payload: PayrollList && PayrollList.data || []});
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Delete Payroll Group
 */
export function* deletePayrollGroup({ payload }) {
    try {
        const isUse = yield call( Fetch, '/company/payroll_group/is_in_use', { method: 'POST', data: payload });

        if ( isUse && isUse.in_use === 0 ) {
            const formData = new FormData();

            formData.append( '_method', 'DELETE' );
            formData.append( 'company_id', payload.company_id );
            payload.ids.forEach( ( id ) => {
                formData.append( 'payroll_group_ids[]', id );
            });

            yield call( Fetch, '/payroll_group/bulk_delete', {
                method: 'POST',
                data: formData
            });

            yield call( showSuccessMessage );
        } else {
            const error = {
                message: "Cannot read properties of undefined reading 'status'"
            };

            yield call( notifyError, {
                show: true,
                title: 'Error',
                message: error.message,
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getCompanyPayroll );
}

 /**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( true, 'success', RECORD_DELETE_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetCompanyPayroll() {
    const watcher = yield takeEvery( GET_COMPANY_PAYROLL_LIST, getCompanyPayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForDeletePayrollGroup() {
    const watcher = yield takeEvery( DELETE_PAYROLL_GROUP, deletePayrollGroup );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetCompanyPayroll,
    watchForReinitializePage,
    watchForDeletePayrollGroup
];
