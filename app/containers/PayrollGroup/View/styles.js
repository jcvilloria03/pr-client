import styled from 'styled-components';

export const PageWrapper = styled.div`
    height:100%;
    .container{
        height:100%;
        .loader{
            height:100%;
        }
    }
    .content {
        margin-top: 110px;
        .ReactTable{
            .rt-table{
                font-size: 14px;
            }
            .rt-tbody{
                .rt-tr-group:nth-child(even){
                    background-color:#fafbfc;
                }
                .rt-tr{
                    border-top:0 !important;
                }
                .selected{
                    background-color: rgba(131, 210, 75, 0.15);
                    &:hover{
                        background-color: rgba(131, 210, 75, 0.15) !important;
                    }
                }
            }
        }
        .selectPage{
            p{
                text-align: end;
                background-color: #e5f6fc;
                margin: 0;
                padding: 12px 10px;
                font-size: 14px;
                font-weight: 600;
                a{
                    color: #00a5e5;
                    padding: 0;
                }
            }
        }
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 600;
                margin-bottom: 5px;
            }

            p {
                text-align: center;
                max-width: 800px;
                font-size:14px;
                margin-bottom: 0px;
            }
        }

        .main {
            .btn {
                min-width: 140px;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
            justify-content: space-between;
            .title-Content{
                display: flex;
                align-items: center;
                p,.dropdown span,span{
                    font-size: 14px;
                }
            }
            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 700;
                font-size: 18px;
            }

            .search-wrapper {
                flex-grow: 1;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 30px;

                    input {
                        border: none;
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }
        }
    }

    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }

    .no-license-error {
        color: #e13232;
    }
`;
export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    height: 100%;
    justify-content: center;
    padding: 140px 0;
`;
