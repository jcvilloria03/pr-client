import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, LOADING, SET_COMPANY_PAYROLL_LIST, SET_NOTIFICATION, SET_TOTAL_PAGE
} from './constants';

const initialState = fromJS({
    payrolls: [],
    loading: false,
    totalPage: 0,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * CompanyPayroll reducer
 *
 */
function companyPayrollReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_COMPANY_PAYROLL_LIST:
            return state.set( 'payrolls', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_TOTAL_PAGE:
            return state.set( 'totalPage', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default companyPayrollReducer;

