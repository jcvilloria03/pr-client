/* eslint-disable no-constant-condition */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment';
import { H6 } from 'components/Typography';

import A from 'components/A';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';

import Button from 'components/Button';
import Loader from 'components/Loader';
import SnackBar from '../../../components/SnackBar';
import Sidebar from '../../../components/Sidebar';
import Input from '../../../components/Input';
import SalSelect from '../../../components/Select';
import DatePicker from '../../../components/DatePicker';
import Switch from '../../../components/Switch';
import SalCheckbox from '../../../components/SalCheckbox/index';
import Modal from '../../../components/Modal/index';

import { DATE_FORMATS, PAY_FREQUENCY, PAYRUN_NON_WORKING_DAYS, PAYMENT_SCHEDULE } from '../../../utils/constants';
import { browserHistory } from '../../../utils/BrowserHistory';
import { stripNonDigit, formatCurrencyToDecimalNotation } from '../../../utils/functions';

import * as companyPayrollAction from './actions';

import {
    makeSelectBtnLoading,
    makeSelectCompanyPayroll,
    makeSelectLoading,
    makeSelectNotification
} from './selectors';

import { Footer, Header, ModalBody, PageWrapper } from './styles';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';

/**
 *
 * CompanyPayroll
 *
 */
export class CompanyPayroll extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        submitPayrollGroup: React.PropTypes.func,
        BtnLoading: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            form: {
                name: '',
                minimumNetTakeHomePay: '4000.00',
                enforceGapLoans: true,
                attendancePeriod: false,
                dayFactor: '260.00',
                hoursPayDay: '8.00',
                payrollFrequency: 'MONTHLY',
                cutOffDate: '',
                payDate: '',
                payRunPosting: '',
                cutOffDate2: '',
                payDate2: '',
                payRunPosting2: '',
                fcdEndofMonth: false,
                fpdEndofMonth: false,
                fppEndofMonth: false,
                computeTardiness: false,
                computeOverbreak: false,
                computeUndertime: false,
                scdEndofMonth: false,
                spdEndofMonth: false,
                sppEndofMonth: false,
                sssSchedule: 'EVERY_PAY',
                sssMethod: 'BASED_ON_ACTUAL',
                philhealthSchedule: 'EVERY_PAY',
                philhealthMethod: 'BASED_ON_ACTUAL',
                hdmfSchedule: 'EVERY_PAY',
                hdmfMethod: 'BASED_ON_ACTUAL',
                wtaxSchedule: 'EVERY_PAY',
                wtaxMethod: 'BASED_ON_ACTUAL',
                nonWorkingDayOption: 'BEFORE',
                attCutOffDate: null,
                attCutOffDate2: null,
                facdEndofMonth: false,
                sacdEndofMonth: false
            },
            hasMinTakeAmtNull: false,
            hasDaycareNull: false,
            hasDayHourNull: false,
            isNameNull: false,
            hasAlreadyName: false,
            isNameSubmitNull: false,
            cutOffValidate: {
                flag: false,
                message: ''
            },
            payDateValidate: {
                flag: false,
                message: ''
            },
            payRunningDateValidate: {
                flag: false,
                message: ''
            },
            cutOffValidateOne: {
                flag: false,
                message: ''
            },
            payDateValidateOne: {
                flag: false,
                message: ''
            },
            payRunningDateValidateOne: {
                flag: false,
                message: ''
            },
            minNetTakeAmt: {
                flag: false,
                message: ''
            },
            attCutOffValidate: {
                flag: false,
                message: ''
            },
            attCutOffValidateOne: {
                flag: false,
                message: ''
            }

        };
        this.showModal = null;
        this.discardModal = null;
    }

    getLastDate = ( date ) => moment( ( date !== 'Invalid date' && date && date ) || new Date() ).endOf( 'months' ).format( DATE_FORMATS.API )
    async submitForm() {
        this.setState({ isNameSubmitNull: this.state.form.name === '' });
        const DataList = {
            compute_overbreak: this.state.form.computeOverbreak,
            compute_tardiness: this.state.form.computeTardiness,
            compute_undertime: this.state.form.computeUndertime,
            cut_off_date: this.state.form.cutOffDate !== 'Invalid date' ? this.state.form.cutOffDate : '',
            cut_off_date_2: this.state.form.cutOffDate2 !== 'Invalid date' ? this.state.form.cutOffDate2 : '',
            day_factor: this.state.form.dayFactor,
            enforce_gap_loans: this.state.form.enforceGapLoans,
            fcd_end_of_month: this.state.form.fcdEndofMonth,
            fpd_end_of_month: this.state.form.fpdEndofMonth,
            fpp_end_of_month: this.state.form.fppEndofMonth,
            hdmf_method: this.state.form.hdmfMethod,
            hdmf_schedule: this.state.form.hdmfSchedule,
            hours_per_day: this.state.form.hoursPayDay,
            minimum_net_take_home_pay: this.state.form.minimumNetTakeHomePay,
            name: this.state.form.name,
            non_working_day_option: this.state.form.nonWorkingDayOption,
            pay_date: this.state.form.payDate !== 'Invalid date' ? this.state.form.payDate : '',
            pay_date_2: this.state.form.payDate2 !== 'Invalid date' ? this.state.form.payDate2 : '',
            pay_run_posting: this.state.form.payRunPosting !== 'Invalid date' ? this.state.form.payRunPosting : '',
            pay_run_posting_2: this.state.form.payRunPosting2 !== 'Invalid date' ? this.state.form.payRunPosting2 : '',
            payroll_frequency: this.state.form.payrollFrequency,
            philhealth_method: this.state.form.philhealthMethod,
            philhealth_schedule: this.state.form.philhealthSchedule,
            scd_end_of_month: this.state.form.scdEndofMonth,
            spd_end_of_month: this.state.form.spdEndofMonth,
            spp_end_of_month: this.state.form.sppEndofMonth,
            sss_method: this.state.form.sssMethod,
            sss_schedule: this.state.form.sssSchedule,
            wtax_method: this.state.form.wtaxMethod,
            wtax_schedule: this.state.form.wtaxSchedule,
            attend_period_switch: this.state.form.attendancePeriod,
            att_first_cut_off_date: this.state.form.attendancePeriod === true && this.state.form.attCutOffDate !== 'Invalid date' ? this.state.form.attCutOffDate : null,
            att_second_cut_off_date: this.state.form.attendancePeriod === true && this.state.form.attCutOffDate2 !== 'Invalid date' ? this.state.form.attCutOffDate2 : null,
            att_fcd_end_of_month: this.state.form.facdEndofMonth,
            att_scd_end_of_month: this.state.form.sacdEndofMonth
        };

        this.validateFirstCutOffDate( this.state.form.cutOffDate );
        this.validateFirstPayDate( this.state.form.payDate );
        this.validateFirstPayRunningDate( this.state.form.payRunPosting );
        this.validateAfterCutOffDate( this.state.form.cutOffDate2 );
        this.validateAfterPayDate( this.state.form.payDate2 );
        this.validateAfterPayRunningDate( this.state.form.payRunPosting2 );
        if ( this.state.form.attendancePeriod === true ) {
            this.validateFirstAttCutOffDate( this.state.form.attCutOffDate );
            this.validateAfterAttCutOffDate( this.state.form.attCutOffDate2 );
        }
        const companyId = company.getLastActiveCompanyId();
        const response = await Fetch( `/company/${companyId}/payroll_group/is_name_available`, { method: 'POST', data: DataList });
        if ( response.available ) {
            const { form } = this.state;
            if ( form.name !== '' && ( ( form.enforceGapLoans && form.minimumNetTakeHomePay !== '' && +form.minimumNetTakeHomePay > 0 ) || true ) &&
            ( form.dayFactor !== '' && ( +form.dayFactor > 0 && +form.dayFactor <= 365 ) ) &&
            ( form.hoursPayDay !== '' && ( +form.hoursPayDay > 0 && +form.hoursPayDay <= 24 ) &&
                ( !this.state.cutOffValidate.flag ) &&
                ( !this.state.attCutOffValidate.flag ) &&
                ( !this.state.payDateValidate.flag ) &&
                ( !this.state.payRunningDateValidate.flag ) &&
                ( !this.state.payRunningDateValidateOne.flag ) &&
                ( !this.state.payDateValidateOne.flag )
                ) ) {
                const payload = { data: DataList };
                this.props.submitPayrollGroup( payload );
            }
        } else {
            this.setState({ hasAlreadyName: true });
        }
    }

    handleMinTakeAmt=( fre ) => ( fre === 'MONTHLY' ? formatCurrencyToDecimalNotation( 4000 ) : fre === 'WEEKLY' ? formatCurrencyToDecimalNotation( 1000 ) : formatCurrencyToDecimalNotation( 2000 ) )

    handleDateFormat = ( date ) => moment( date ).format( 'YYYY-MM-DD' )

    handleSetAmt=( fre ) => {
        this.minimumNetTakeHomePay.setState({ error: false, message: '' });
        const value = this.handleMinTakeAmt( fre );
        return value;
    }

    dateValidate = ( name, value ) => {
        const values = value === '';
        this.setState({ [ name ]: values });
    }
    addState = ( name, value ) => {
        this.setState({
            form: {
                ...this.state.form,
                [ name ]: value
            }
        });
    }

    handleValidateMinTakeAmt=( value ) => {
        const amt = formatCurrencyToDecimalNotation( value );
        if ( +amt <= 0 ) {
            this.setState({ minNetTakeAmt: { flag: true, message: 'this field is required' }});
        } else {
            this.setState({ minNetTakeAmt: { flag: false, message: '' }});
        }
        return amt;
    }

    validateFirstCutOffDate = ( value ) => {
        const date = this.handleDateFormat( value );
        if ( value === 'Invalid date' || value === '' ) {
            const message = 'Please choose a date';
            this.setState({ cutOffValidateOne: { flag: true, message }});
        } else {
            this.setState({ cutOffValidateOne: { flag: false, message: '' }});
        }
        return date;
    }

    validateFirstAttCutOffDate = ( value ) => {
        const date = this.handleDateFormat( value );
        if ( value === 'Invalid date' || value === '' || value === null ) {
            const message = 'Please choose a date';
            this.setState({ attCutOffValidateOne: { flag: true, message }});
        } else {
            this.setState({ attCutOffValidateOne: { flag: false, message: '' }});
        }
        return date;
    }

    validateFirstPayDate = ( value ) => {
        const date = this.handleDateFormat( value );
        if ( value === 'Invalid date' || value === '' ) {
            const message = 'Please choose a date';
            this.setState({ payDateValidateOne: { flag: true, message }});
        } else if ( !moment( date ).isSameOrAfter( this.state.form.cutOffDate ) ) {
            const message = ' Pay date must be after Cut-Off date';
            this.setState({ payDateValidateOne: { flag: true, message }});
        } else {
            this.setState({ payDateValidateOne: { flag: false, message: '' }});
        }
        return date;
    }

    validateFirstPayRunningDate = ( value ) => {
        const date = this.handleDateFormat( value );
        if ( value === 'Invalid date' || value === '' ) {
            const message = 'Please choose a date';
            this.setState({ payRunningDateValidateOne: { flag: true, message }});
        } else {
            this.setState({ payRunningDateValidateOne: { flag: false, message: '' }});
        }
        return date;
    }

    handleDateSameAfter=( date, afterDate ) => ( !!( moment( date ).isSame( afterDate, 'day' ) || moment( date ).isAfter( afterDate, 'day' ) ) );

    validateAfterCutOffDate = ( value ) => {
        const date = this.handleDateFormat( value );
        if ( this.state.form.payrollFrequency === 'SEMI_MONTHLY' && ( value === 'Invalid date' || value === '' ) ) {
            const message = 'Please choose a date';
            this.setState({ cutOffValidate: { flag: true, message }});
        } else if ( this.state.form.payrollFrequency === 'SEMI_MONTHLY' && !moment( date ).isAfter( this.state.form.cutOffDate, 'day' ) ) {
            const message = 'Second Cut-Off date must be after First Cut-Off date';
            this.setState({ cutOffValidate: { flag: true, message }});
        } else {
            this.setState({ cutOffValidate: { flag: false, message: '' }});
        }
        return date;
    }
    validateAfterAttCutOffDate = ( value ) => {
        const date = this.handleDateFormat( value );
        if (this.state.form.payrollFrequency === 'SEMI_MONTHLY' && ( value === 'Invalid date' || value === '' || value === null ) ) {
            const message = 'Please choose a date';
            this.setState({ attCutOffValidate: { flag: true, message }});
        } else if ( this.state.form.payrollFrequency === 'SEMI_MONTHLY' && !moment( date ).isAfter( this.state.form.attCutOffDate, 'day' ) ) {
            const message = 'Second Attendance Cut-Off date must be after First Period Attendance Cut-Off date';
            this.setState({ attCutOffValidate: { flag: true, message }});
        } else {
            this.setState({ attCutOffValidate: { flag: false, message: '' }});
        }
        return date;
    }
    validateAfterPayDate = ( value ) => {
        const date = this.handleDateFormat( value );
        if ( this.state.form.payrollFrequency === 'SEMI_MONTHLY' && ( value === 'Invalid date' || value === '' ) ) {
            const message = 'Please choose a date';
            this.setState({ payDateValidate: { flag: true, message }});
        } else if ( this.state.form.payrollFrequency === 'SEMI_MONTHLY' && !moment( date ).isAfter( this.state.form.payDate, 'day' ) ) {
            const message = 'Second Pay date must be after Second Cut-Off date and First Pay date';
            this.setState({ payDateValidate: { flag: true, message }});
        } else {
            this.setState({ payDateValidate: { flag: false, message: '' }});
        }
        return date;
    }

    validateAfterPayRunningDate = ( value ) => {
        const date = this.handleDateFormat( value );
        if ( this.state.form.payrollFrequency === 'SEMI_MONTHLY' && ( value === 'Invalid date' || value === '' ) ) {
            const message = 'Please choose a date';
            this.setState({ payRunningDateValidate: { flag: true, message }});
        } else if ( this.state.form.payrollFrequency === 'SEMI_MONTHLY' && !moment( date ).isAfter( this.state.form.payRunPosting, 'day' ) ) {
            const message = 'Second pay run posting must be after First pay run posting';
            this.setState({ payRunningDateValidate: { flag: true, message }});
        } else {
            this.setState({ payRunningDateValidate: { flag: false, message: '' }});
        }
        return date;
    }

    handleMinCondition=( value, names ) => {
        if ( names === 'dayFactor' && ( +value < 1 || value > 365 ) ) {
            this.dayFactor.setState({ error: true, errorMessage: 'Value must be between 1 and 365' });
        }
        if ( names === 'hoursPayDay' && ( +value < 1 || value > 24 ) ) {
            this.hoursPayDay.setState({ error: true, errorMessage: 'Value must be between 1 and 24' });
        }
    }

    renderModalBody = () => (
        <ModalBody>
               With Gap Loans turned off, employees may receive salaries lower than the inimum net take-home pay allowed by your company and/or the law.
        </ModalBody>
    )

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { form } = this.state;
        const { BtnLoading } = this.props;
        return (
            <div>
                <Helmet
                    title="Add Payroll Groups"
                    meta={ [
                        { name: 'description', content: 'Description of Add Payroll Groups' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Header>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/payroll/payroll-groups', true );
                                } }
                            >
                                &#8592; Back to Payroll Groups
                            </A>
                        </Container>
                    </div>
                </Header>
                <PageWrapper>
                    <Sidebar
                        items={ sidebarLinks }
                    />
                    <br />
                    <div className="main_section">
                        <div className="heading">
                            <h1>Add Payroll Group</h1>
                            <p>View and update payroll groups. You can add, edit, or delete payroll groups.</p>
                        </div>
                        <div className="row">
                            <div><H6 className="row-title">Payroll Group Details</H6>
                                <div className="row">
                                    <div className={ this.state.isNameSubmitNull || this.state.hasAlreadyName ? 'col-xs-4 input_pay' : 'col-xs-4' }>
                                        <Input
                                            id="group_name"
                                            label=" Payroll group name"
                                            placeholder="Type in payroll group name"
                                            type="text"
                                            name="name"
                                            className={ this.state.isNameNull || this.state.isNameSubmitNull ? 'input_pay' : '' }
                                            ref={ ( ref ) => { this.name = ref; } }
                                            value={ this.state.form.name }
                                            onChange={ ( value ) => {
                                                this.setState({ isNameNull: value === '', isNameSubmitNull: false, hasAlreadyName: false });
                                                this.addState( 'name', value );
                                            } }
                                            onBlur={ ( value ) => {
                                                this.setState({ isNameNull: value === '' });
                                            } }
                                            required
                                        />{this.state.isNameSubmitNull ? <p>This field is required</p> : ''}
                                        { this.state.hasAlreadyName ? <p>Record name already exists</p> : ''}
                                    </div>
                                    <div className="col-xs-4">
                                        <Input
                                            id="minimum_pay"
                                            label=" Minimum net take home pay"
                                            placeholder="Type in payroll group minimum net take home pay"
                                            type="number"
                                            name="minimumNetTakeHomePay"
                                            className={ form.minimumNetTakeHomePay === '' || +form.minimumNetTakeHomePay < 0 ? 'input_pay' : '' }
                                            value={ ( this.state.form.minimumNetTakeHomePay ) }
                                            ref={ ( ref ) => { this.minimumNetTakeHomePay = ref; } }
                                            onChange={ ( value ) => {
                                                this.setState({
                                                    hasMinTakeAmtNull: value === '' || +form.minimumNetTakeHomePay <= 0,
                                                    form: {
                                                        ...form,
                                                        minimumNetTakeHomePay: value
                                                    }
                                                });
                                            }}
                                            onBlur={ ( value ) => {
                                                this.setState({
                                                    form: {
                                                        ...form,
                                                        minimumNetTakeHomePay: this.handleValidateMinTakeAmt( value )
                                                    }
                                                });
                                            }}
                                            required
                                            minNumber={ 1 }
                                            disabled={ this.state.form.enforceGapLoans === false }
                                        />
                                    </div>
                                    <div className="col-xs-4 SalSelects">
                                        <SalSelect
                                            id="pay_frequency"
                                            label={
                                                <span className="schdule_tooltip"><p className="start_point">* </p> Pay frequency
                                                    <div className="bg_tooltip">
                                                        <p>?</p>
                                                        <span className="tooltiptext"> Select the frequency of employee <br />salary disbursement.</span>
                                                    </div>
                                                </span>
                                            }
                                            ref={ ( ref ) => { this.payrollFrequency = ref; } }
                                            value={ this.state.form.payrollFrequency }
                                            data={ PAY_FREQUENCY }
                                            name="payrollFrequency"
                                            onChange={ ({ value }) => this.setState({
                                                form: {
                                                    ...form,
                                                    payrollFrequency: value,
                                                    sssMethod: value === 'MONTHLY' ? 'BASED_ON_ACTUAL' : 'EQUALLY_SPLIT',
                                                    philhealthMethod: value === 'MONTHLY' ? 'BASED_ON_ACTUAL' : 'EQUALLY_SPLIT',
                                                    hdmfMethod: value === 'MONTHLY' ? 'BASED_ON_ACTUAL' : 'EQUALLY_SPLIT',
                                                    sssSchedule: value !== 'WEEKLY' || value !== 'FORTNIGHTLY' ? 'EVERY_PAY' : '',
                                                    philhealthSchedule: value !== 'WEEKLY' || value !== 'FORTNIGHTLY' ? 'EVERY_PAY' : '',
                                                    hdmfSchedule: value !== 'WEEKLY' || value !== 'FORTNIGHTLY' ? 'EVERY_PAY' : '',
                                                    minimumNetTakeHomePay: this.handleSetAmt( value ),
                                                    cutOffDate2: '',
                                                    payDate2: '',
                                                    payRunPosting2: '',
                                                    attCutOffDate2: null,
                                                    attCutOffDate: null,
                                                    scdEndofMonth: false,
                                                    spdEndofMonth: false,
                                                    sppEndofMonth: false,
                                                    sacdEndofMonth: false

                                                }
                                            }) }
                                        />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-4" >
                                        <Input
                                            id="noof_workingday_per_year"
                                            label=" Number of working days per year"
                                            className={ this.state.dayFactor === '' ? 'input_pay' : ( +form.dayFactor <= 0 || +form.dayFactor > 365 ) ? 'input_pay' : '' }
                                            type="number"
                                            name="dayFactor"
                                            ref={ ( ref ) => { this.dayFactor = ref; } }
                                            value={ this.state.form.dayFactor }
                                            onChange={ ( value ) => {
                                                this.setState({ hasDaycareNull: value === '' || ( +form.dayFactor <= 0 || +form.dayFactor > 365 ) });
                                                this.addState( 'dayFactor', stripNonDigit( value ) );
                                            } }
                                            onBlur={ ( value ) => {
                                                this.addState( 'dayFactor', formatCurrencyToDecimalNotation( value ) );
                                                this.setState({ hasDaycareNull: value === '' || ( +form.dayFactor <= 0 || +form.dayFactor > 365 ) }, this.handleMinCondition( value, 'dayFactor' ) );
                                            } }
                                            required
                                        />
                                    </div>
                                    <div className="col-xs-4">
                                        <Input
                                            id="noof_workinghours_per_day"
                                            label="Number of working hours per day"
                                            className={ this.state.hoursPayDay === '' ? 'input_pay' : ( +form.hoursPayDay <= 0 || +form.hoursPayDay > 24 ) ? 'input_pay' : '' }
                                            type="number"
                                            name="hoursPayDay"
                                            value={ this.state.form.hoursPayDay }
                                            ref={ ( ref ) => { this.hoursPayDay = ref; } }
                                            onChange={ ( value ) => {
                                                this.setState({ hasDayHourNull: value === '' || ( +form.hoursPayDay <= 0 || +form.hoursPayDay > 24 ) });
                                                this.addState( 'hoursPayDay', stripNonDigit( value ) );
                                            } }
                                            onBlur={ ( value ) => {
                                                this.addState( 'hoursPayDay', formatCurrencyToDecimalNotation( value ) );
                                                this.setState({ hasDayHourNull: value === '' || ( +form.hoursPayDay <= 0 || +form.hoursPayDay > 24 ) }, this.handleMinCondition( value, 'hoursPayDay' ) );
                                            } }
                                            required
                                        />
                                    </div>
                                    <div className="col-xs-4">
                                        <SalSelect
                                            id="non_working_day"
                                            label={
                                                <p className="schdule_tooltip"><p className="start_point">* </p>If pay run falls on a non-working day
                                                <div className="bg_tooltip">
                                                    <p>?</p>
                                                    <span className="tooltiptext"> Select condition from dropdown</span>
                                                </div>
                                                </p>
                                        }
                                            value={ this.state.form.nonWorkingDayOption }
                                            ref={ ( ref ) => { this.nonWorkingDayOption = ref; } }
                                            name="nonWorkingDayOption"
                                            data={ PAYRUN_NON_WORKING_DAYS }
                                            onChange={ ({ value }) => this.addState( 'nonWorkingDayOption', value ) }
                                        />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-4">
                                        <div className="enforce_switch">
                                            <Switch
                                                id="enforce_gap_loans"
                                                ref={ ( ref ) => { this.enforce_gap_loans = ref; } }
                                                checked={ this.state.form.enforceGapLoans }
                                                name="enforceGapLoans"
                                                onChange={ ( value ) => {
                                                    value === false ? this.showModal.toggle() : this.setState({ form: { ...form, enforceGapLoans: true, minimumNetTakeHomePay: this.handleMinTakeAmt( form.payrollFrequency ) }});
                                                } }
                                            />
                                            <span>Enforce Gap Loans</span>
                                        </div>
                                        {
                                            this.state.form.enforceGapLoans === false ? <p>{this.renderModalBody()}</p> : ''
                                        }
                                    </div>
                                    <div className="col-xs-4 SalSelects">
                                        <div className="enforce_switch">
                                            <Switch
                                                id="attend_period_switch"
                                                ref={ ( ref ) => { this.attend_period_switch = ref; } }
                                                checked={ this.state.form.attendancePeriod }
                                                name="attendancePeriod"
                                                onChange={ ( value ) => { this.setState({ form: { ...form, attendancePeriod: value }}); } }
                                            />
                                            <span>Attendance Period different from Pay Period</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div><H6 className="row-title">Pay Period Schedule</H6>
                                <div className="row">
                                    <div className={ this.state.cutOffValidateOne.flag ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                        <div className="DayPickerInput ">
                                            <DatePicker
                                                id="cutOffDate"
                                                label={
                                                    <span className="schdule_tooltip"><p className="start_point">* </p>{form.payrollFrequency === 'SEMI_MONTHLY' ? 'First' : ''} Pay Period Cut-Off date
                                                    <div className="bg_tooltip">
                                                        <span className="bg_whylabel">?</span>
                                                        <span className="tooltiptext"> Select the end-date of the attendance cut-off period.</span>
                                                    </div>
                                                    </span>
                                            }
                                                name="cutOffDate"
                                                dayFormat={ DATE_FORMATS.DISPLAY }
                                                selectedDay={ this.state.form.cutOffDate }
                                                ref={ ( ref ) => { this.cutOffDate = ref; } }
                                                onChange={ ( value ) => {
                                                    this.setState({ form: { ...form, cutOffDate: this.validateFirstCutOffDate( value ), fcdEndofMonth: this.getLastDate( value ) === this.handleDateFormat( value ) }}, this.dateValidate( 'cutOffDate', value ) );
                                                }
                                                }
                                            />
                                        </div>
                                        {this.state.cutOffValidateOne.flag ? <span>{this.state.cutOffValidateOne.message}</span> : ''}
                                    </div>
                                    <div className={ this.state.payDateValidateOne.flag ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                        <div className="DayPickerInput">
                                            <DatePicker
                                                id="payDate"
                                                label={
                                                    <span className="schdule_tooltip"><p className="start_point">* </p>{form.payrollFrequency === 'SEMI_MONTHLY' ? 'First' : ''} Period Pay date
                                                    <div className="bg_tooltip">
                                                        <span className="bg_whylabel">?</span>
                                                        <span className="tooltiptext"> Select the date of the salary payout.</span>
                                                    </div>
                                                    </span>
                                            }
                                                name="payDate"
                                                dayFormat={ DATE_FORMATS.DISPLAY }
                                                ref={ ( ref ) => { this.payDate = ref; } }
                                                selectedDay={ this.state.form.payDate }
                                                onChange={ ( value ) => {
                                                    this.setState({
                                                        form:
                                                        {
                                                            ...form,
                                                            payDate: this.validateFirstPayDate( value ),
                                                            fpdEndofMonth: this.getLastDate( value ) === this.handleDateFormat( value )
                                                        }
                                                    }, this.dateValidate( 'payDate', value ) );
                                                } }
                                            />
                                        </div>
                                        {this.state.payDateValidateOne.flag ? <span>{this.state.payDateValidateOne.message}</span> : ''}
                                    </div>
                                    <div className={ this.state.payRunningDateValidateOne.flag ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                        <div className="DayPickerInput">
                                            <DatePicker
                                                id="payRunPosting"
                                                label={
                                                    <span className="schdule_tooltip"><p className="start_point">* </p>{form.payrollFrequency === 'SEMI_MONTHLY' ? 'First' : ''}  Period Posting date
                                                    <div className="bg_tooltip">
                                                        <span className="bg_whylabel">?</span>
                                                        <span className="tooltiptext"> Select the reporting date of the payroll.</span>
                                                    </div>
                                                    </span>
                                            }
                                                name="payRunPosting"
                                                dayFormat={ DATE_FORMATS.DISPLAY }

                                                ref={ ( ref ) => { this.payRunPosting = ref; } }
                                                selectedDay={ this.state.form.payRunPosting }
                                                onChange={ ( value ) => {
                                                    this.setState({
                                                        form: { ...form,
                                                            payRunPosting: this.validateFirstPayRunningDate( value ),
                                                            fppEndofMonth: this.getLastDate( value ) === this.handleDateFormat( value )
                                                        }
                                                    }, this.dateValidate( 'payRunPosting', value ) );
                                                } }
                                            />
                                        </div>
                                        {this.state.payRunningDateValidateOne.flag ? <span>{this.state.payRunningDateValidateOne.message}</span> : ''}
                                    </div>
                                </div>
                                {this.state.form.payrollFrequency === 'MONTHLY' || this.state.form.payrollFrequency === 'SEMI_MONTHLY' ?
                            (
                                <div className="row">
                                    <div className="col-xs-4 checkcontaion">
                                        <SalCheckbox
                                            id="fcdEndOfMonth"
                                            name="fcdEndOfMonth"
                                            ref={ ( ref ) => { this.fcdEndofMonth = ref; } }
                                            checked={ this.state.form.fcdEndofMonth }
                                            onChange={ ( value ) => this.setState({
                                                cutOffValidateOne: { flag: false, message: '' },
                                                form: { ...form,
                                                    fcdEndofMonth: value,
                                                    cutOffDate: this.getLastDate( this.state.form.cutOffDate ) }
                                            }) }
                                        />&nbsp;
                                        <p>Always end of the month?</p>
                                    </div>
                                    <div className="col-xs-4 checkcontaion">
                                        <SalCheckbox
                                            id="fpdEndofMonth"
                                            name="fpdEndOfMonth"
                                            ref={ ( ref ) => { this.fpdEndofMonth = ref; } }
                                            checked={ this.state.form.fpdEndofMonth }
                                            onChange={ ( value ) => this.setState({
                                                payDateValidateOne: { flag: false, message: '' },
                                                form: { ...form,
                                                    fpdEndofMonth: value,
                                                    payDate: this.getLastDate( this.state.form.payDate ) }
                                            }) }
                                        />&nbsp;
                                        <p>Always end of the month?</p>
                                    </div>
                                    <div className="col-xs-4 checkcontaion">
                                        <SalCheckbox
                                            id="fppEndofMonth"
                                            name="fppEndOfMonth"
                                            ref={ ( ref ) => { this.fppEndofMonth = ref; } }
                                            checked={ this.state.form.fppEndofMonth }
                                            onChange={ ( value ) => this.setState({
                                                payRunningDateValidateOne: { flag: false, message: '' },
                                                form: { ...form,
                                                    fppEndofMonth: value,
                                                    payRunPosting: this.getLastDate( this.state.form.payRunPosting ) }
                                            }) }
                                        />&nbsp;
                                        <p>Always end of the month?</p>
                                    </div>
                                </div>
                            ) : ''
                            }
                                {
                                this.state.form.payrollFrequency === 'SEMI_MONTHLY' ? (
                                    <div>
                                        <div className="row">
                                            <div className={ this.state.cutOffValidate.flag ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                                <DatePicker
                                                    id="cutOffDate2"
                                                    name="cutOffDate2"
                                                    label={
                                                        <span className="schdule_tooltip"><p className="start_point">* </p>Second Pay Period Cut-Off date
                                                            <div className="bg_tooltip">
                                                                <span className="bg_whylabel">?</span>
                                                                <span className="tooltiptext"> Select the end-date of the attendance cut-off period.</span>
                                                            </div>
                                                        </span>
                                                    }
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    ref={ ( ref ) => { this.cutOffDate2 = ref; } }
                                                    selectedDay={ this.state.form.cutOffDate2 }
                                                    onChange={ ( value ) => {
                                                        this.setState({
                                                            form: { ...form,
                                                                cutOffDate2: this.validateAfterCutOffDate( value ),
                                                                scdEndofMonth: this.getLastDate( value ) === this.handleDateFormat( value )
                                                            }
                                                        }, this.dateValidate( 'cutOffDate2', value ) );
                                                    } }
                                                />{this.state.cutOffValidate.flag ? <span>{this.state.cutOffValidate.message}</span> : ''}
                                            </div>
                                            <div className={ this.state.payDateValidate.flag ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                                <DatePicker
                                                    id="payDate2"
                                                    name="payDate2"
                                                    label={
                                                        <span className="schdule_tooltip"><p className="start_point">* </p>  Second Period Pay date
                                                        <div className="bg_tooltip">
                                                            <span className="bg_whylabel">?</span>
                                                            <span className="tooltiptext"> Select the date of the salary payout.</span>
                                                        </div>
                                                        </span>
                                                }
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    ref={ ( ref ) => { this.payDate2 = ref; } }
                                                    selectedDay={ this.state.form.payDate2 }
                                                    onChange={ ( value ) => {
                                                        this.setState({
                                                            form: { ...form,
                                                                payDate2: this.validateAfterPayDate( value ),
                                                                spdEndofMonth: this.getLastDate( value ) === this.handleDateFormat( value )
                                                            }
                                                        }, this.dateValidate( 'payDate2', value ) );
                                                    } }
                                                />{this.state.payDateValidate.flag ? <span>{this.state.payDateValidate.message}</span> : ''}
                                            </div>
                                            <div className={ this.state.payRunningDateValidate.flag ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                                <DatePicker
                                                    id="payRunPosting2"
                                                    name="payRunPosting2"
                                                    label={
                                                        <span className="schdule_tooltip"><p className="start_point">* </p>  Second Period Posting date
                                                        <div className="bg_tooltip">
                                                            <span className="bg_whylabel">?</span>
                                                            <span className="tooltiptext"> Select the reporting date of the payroll.</span>
                                                        </div>
                                                        </span>
                                                    }
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    ref={ ( ref ) => { this.payRunPosting2 = ref; } }
                                                    selectedDay={ this.state.form.payRunPosting2 }
                                                    onChange={ ( value ) => {
                                                        this.setState({
                                                            form: { ...form,
                                                                payRunPosting2: this.validateAfterPayRunningDate( value ),
                                                                sppEndofMonth: this.getLastDate( value ) === this.handleDateFormat( value )
                                                            }
                                                        }, this.dateValidate( 'payRunPosting2', value ) );
                                                    } }
                                                />{this.state.payRunningDateValidate.flag ? <span>{this.state.payRunningDateValidate.message}</span> : ''}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-4">
                                                <div className="checkcontaion">
                                                    <SalCheckbox
                                                        id="scdEndofMonth"
                                                        name="scdEndofMonth"
                                                        ref={ ( ref ) => { this.scdEndofMonth = ref; } }
                                                        checked={ this.state.form.scdEndofMonth }
                                                        onChange={ ( value ) => {
                                                            const lDate = this.getLastDate( this.state.form.cutOffDate2 );
                                                            this.setState({
                                                                form: { ...form,
                                                                    scdEndofMonth: value,
                                                                    cutOffDate2: this.validateAfterCutOffDate( lDate )
                                                                }
                                                            });
                                                        } }
                                                    />
                                                    <p>Always end of the month?</p>
                                                </div>
                                            </div>
                                            <div className="col-xs-4">
                                                <div className="checkcontaion">
                                                    <SalCheckbox
                                                        id="spdEndofMonth"
                                                        name="spdEndofMonth"
                                                        ref={ ( ref ) => { this.spdEndofMonth = ref; } }
                                                        checked={ this.state.form.spdEndofMonth }
                                                        onChange={ ( value ) => {
                                                            const lDate = this.getLastDate( this.state.form.cutOffDate2 );
                                                            this.setState({
                                                                form: { ...form,
                                                                    spdEndofMonth: value,
                                                                    payDate2: this.validateAfterPayDate( lDate )
                                                                }
                                                            });
                                                        } }
                                                    />&nbsp;
                                                    <p>Always end of the month?</p>
                                                </div>
                                            </div>
                                            <div className="col-xs-4">
                                                <div className="checkcontaion">
                                                    <SalCheckbox
                                                        id="sppEndofMonth"
                                                        name="sppEndofMonth"
                                                        ref={ ( ref ) => { this.sppEndofMonth = ref; } }
                                                        checked={ this.state.form.sppEndofMonth }
                                                        onChange={ ( value ) => {
                                                            const lDate = this.getLastDate( this.state.form.cutOffDate2 );
                                                            this.setState({
                                                                form: { ...form,
                                                                    sppEndofMonth: value,
                                                                    payRunPosting2: this.validateAfterPayRunningDate( lDate )
                                                                }
                                                            });
                                                        } }
                                                    />&nbsp;
                                                    <p>Always end of the month?</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ) : ''
                            }
                            </div>
                            {
                            this.state.form.attendancePeriod === true ? (
                                <div><H6 className="row-title">Attendance Period Schedule</H6>
                                    <div className="row">
                                        <div className={ this.state.attCutOffValidateOne.flag ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                            <div className="DayPickerInput ">
                                                <DatePicker
                                                    id="attCutOffDate"
                                                    label={
                                                        <span className="schdule_tooltip"><p className="start_point">* </p>First Period Attendance Cut-Off date
                                                            <div className="bg_tooltip">
                                                                <span className="bg_whylabel">?</span>
                                                                <span className="tooltiptext"> Select the end-date of the attendance cut-off period.</span>
                                                            </div>
                                                        </span>
                                                    }
                                                    name="attCutOffDate"
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    selectedDay={ this.state.form.attCutOffDate }
                                                    ref={ ( ref ) => { this.attCutOffDate = ref; } }
                                                    onChange={ ( value ) => {
                                                        this.setState({ form: { ...form, attCutOffDate: this.validateFirstAttCutOffDate( value ), facdEndofMonth: this.getLastDate( value ) === this.handleDateFormat( value ) }}, this.dateValidate( 'attCutOffDate', value ) );
                                                    }
                                                    }
                                                />
                                            </div>
                                            {this.state.attCutOffValidateOne.flag ? <span>{this.state.attCutOffValidateOne.message}</span> : ''}
                                        </div>
                                        {
                                        this.state.form.payrollFrequency === 'SEMI_MONTHLY' ? (
                                            <div className={ this.state.attCutOffValidate.flag ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                                <DatePicker
                                                    id="attCutOffDate2"
                                                    name="attCutOffDate2"
                                                    label={
                                                        <span className="schdule_tooltip"><p className="start_point">* </p>Second Period Attendance Cut-Off date
                                                            <div className="bg_tooltip">
                                                                <span className="bg_whylabel">?</span>
                                                                <span className="tooltiptext"> Select the end-date of the attendance cut-off period.</span>
                                                            </div>
                                                        </span>
                                                    }
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    ref={ ( ref ) => { this.attCutOffDate2 = ref; } }
                                                    selectedDay={ this.state.form.attCutOffDate2 }
                                                    onChange={ ( value ) => {
                                                        this.setState({
                                                            form: {
                                                                ...form,
                                                                attCutOffDate2: this.validateAfterAttCutOffDate( value ),
                                                                sacdEndofMonth: this.getLastDate( value ) === this.handleDateFormat( value )
                                                            }
                                                        }, this.dateValidate( 'attCutOffDate2', value ) );
                                                    } }
                                                />{this.state.attCutOffValidate.flag ? <span>{this.state.attCutOffValidate.message}</span> : ''}
                                            </div>
                                            ) : ''
                                        }
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-4 checkcontaion">
                                            <SalCheckbox
                                                id="facdEndOfMonth"
                                                name="facdEndOfMonth"
                                                ref={ ( ref ) => { this.facdEndofMonth = ref; } }
                                                checked={ this.state.form.facdEndofMonth }
                                                onChange={ ( value ) => this.setState({
                                                    attCutOffValidateOne: { flag: false, message: '' },
                                                    form: {
                                                        ...form,
                                                        facdEndofMonth: value,
                                                        attCutOffDate: this.getLastDate( this.state.form.attCutOffDate )
                                                    }
                                                }) }
                                            />&nbsp;
                                            <p>Always end of the month?</p>
                                        </div>
                                        {
                                            this.state.form.payrollFrequency === 'SEMI_MONTHLY' ? (
                                                <div className="col-xs-4">
                                                    <div className="checkcontaion">
                                                        <SalCheckbox
                                                            id="sacdEndofMonth"
                                                            name="sacdEndofMonth"
                                                            ref={ ( ref ) => { this.sacdEndofMonth = ref; } }
                                                            checked={ this.state.form.sacdEndofMonth }
                                                            onChange={ ( value ) => {
                                                                const lDate = this.getLastDate( this.state.form.attCutOffDate2 );
                                                                this.setState({
                                                                    form: {
                                                                        ...form,
                                                                        sacdEndofMonth: value,
                                                                        attCutOffDate2: this.validateAfterAttCutOffDate( lDate )
                                                                    }
                                                                });
                                                            } }
                                                        />
                                                        <p>Always end of the month?</p>
                                                    </div>
                                                </div>
                                            ) : ''
                                        }
                                    </div>
                                </div>
                            ) : ''
                            }
                            <hr />
                            <div><H6 className="row-title">Contribution Schedule</H6>
                                <div className="row">
                                    <div className="col-xs-8 mb-2">
                                        <div className="col-xs-6 pl-0">
                                            <span>* SSS payment schedule</span>
                                            {this.state.form.payrollFrequency === 'SEMI_MONTHLY' ? (
                                                <SalSelect
                                                    id="sssSchedule"
                                                    name="sssSchedule"
                                                    value={ this.state.form.sssSchedule }
                                                    ref={ ( ref ) => { this.sssSchedule = ref; } }
                                                    data={ PAYMENT_SCHEDULE }
                                                    onChange={ ({ value }) => this.setState({
                                                        form: { ...form,
                                                            sssSchedule: value,
                                                            sssMethod: value !== 'EVERY_PAY' ? 'BASED_ON_ACTUAL' : 'EQUALLY_SPLIT'
                                                        }
                                                    }) }
                                                /> ) : ( <p>{this.state.form.sssSchedule === 'EVERY_PAY' ? 'Every Pay' : 'Every Pay'}</p> )}
                                        </div>
                                        <div className="col-xs-6 payment_contain">
                                            <span>* SSS payment method</span>
                                            <p>{this.state.form.sssMethod === 'BASED_ON_ACTUAL' ? 'Based On Actual' : 'Equally Split'}</p>
                                        </div>
                                        <div className="col-xs-6 mt-3 pl-0">
                                            <span>* Philhealth payment schedule</span>
                                            {this.state.form.payrollFrequency === 'SEMI_MONTHLY' ? (
                                                <SalSelect
                                                    id="philhealthSchedule"
                                                    name="philhealthSchedule"
                                                    value={ this.state.form.philhealthSchedule }
                                                    ref={ ( ref ) => { this.philhealthSchedule = ref; } }
                                                    data={ PAYMENT_SCHEDULE }
                                                    onChange={ ({ value }) => this.setState({
                                                        form: { ...form,
                                                            philhealthSchedule: value,
                                                            philhealthMethod: value !== 'EVERY_PAY' ? 'BASED_ON_ACTUAL' : 'EQUALLY_SPLIT'
                                                        }
                                                    }) }
                                                /> ) : ( <p>{this.state.form.philhealthSchedule === 'EVERY_PAY' ? 'Every Pay' : 'Every Pay'}</p> )}
                                        </div>
                                        <div className="col-xs-6 mt-3 payment_contain">
                                            <span>* Philhealth payment method</span>
                                            <p>{this.state.form.philhealthMethod === 'BASED_ON_ACTUAL' ? 'Based On Actual' : 'Equally Split'}</p>
                                        </div>
                                    </div>
                                    <div className="col-xs-4">
                                        <div className="col-xs-12">
                                            <div>Attendance Option:</div>
                                            <div className="checkout_compute">
                                                <Switch
                                                    id="computeTardiness"
                                                    name="computeTardiness"
                                                    ref={ ( ref ) => { this.computeTardiness = ref; } }
                                                    checked={ this.state.form.computeTardiness }
                                                    onChange={ ( value ) => this.addState( 'computeTardiness', value ) }
                                                />
                                                <span>Compute Tardiness</span><br />
                                            </div>
                                            <div className="checkout_compute">
                                                <Switch
                                                    id="computeOverbreak"
                                                    name="computeOverbreak"
                                                    ref={ ( ref ) => { this.computeOverbreak = ref; } }
                                                    checked={ this.state.form.computeOverbreak }
                                                    onChange={ ( value ) => this.addState( 'computeOverbreak', value ) }
                                                />
                                                <span>Compute Overbreak</span><br />
                                            </div>
                                            <div className="checkout_compute">
                                                <Switch
                                                    id="computeUdertime"
                                                    name="computeUdertime"
                                                    ref={ ( ref ) => { this.computeUndertime = ref; } }
                                                    checked={ this.state.form.computeUndertime }
                                                    onChange={ ( value ) => { this.setState({ form: { ...form, computeUndertime: value }}); } }
                                                />
                                                <span>Compute Undertime</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-4">
                                        <span>* HDMF payment schedule</span>
                                        {this.state.form.payrollFrequency === 'SEMI_MONTHLY' ? (
                                            <SalSelect
                                                id="hdmfSchedule"
                                                name="hdmfSchedule"
                                                value={ this.state.form.hdmfSchedule }
                                                data={ PAYMENT_SCHEDULE }
                                                onChange={ ({ value }) => this.setState({
                                                    form: { ...form,
                                                        hdmfSchedule: value,
                                                        hdmfMethod: value !== 'EVERY_PAY' ? 'BASED_ON_ACTUAL' : 'EQUALLY_SPLIT' }
                                                }) }
                                            /> ) : ( <p>{this.state.form.hdmfSchedule === 'EVERY_PAY' ? 'Every Pay' : 'Every Pay'}</p> )}
                                    </div>
                                    <div className="col-xs-4">
                                        <span>* HDMF payment method</span>
                                        <p>{this.state.form.hdmfMethod === 'BASED_ON_ACTUAL' ? 'Based On Actual' : 'Equally Split'}</p>
                                    </div>

                                </div>
                                <div className="row">
                                    <div className="col-xs-4">
                                        <span>* WTAX payment schedule</span>
                                        <p>{this.state.form.wtaxSchedule === 'EVERY_PAY' ? 'Every Pay' : ''}</p>
                                    </div>
                                    <div className="col-xs-4">
                                        <span>* WTAX payment method</span>
                                        <p>{this.state.form.wtaxMethod === 'BASED_ON_ACTUAL' ? 'Based on Actual' : ''}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </PageWrapper>
                <Footer>
                    <div className="from_footer">
                        <Container>
                            <div className="footer_button">
                                <Button
                                    label="Cancel"
                                    type="neutral"
                                    size="large"
                                    onClick={ () => this.discardModal.toggle() }
                                />
                                <Button
                                    label={ BtnLoading ? <Loader /> : 'Submit' }
                                    type="action"
                                    size="large"
                                    onClick={ () => this.submitForm() }
                                />
                            </div>
                        </Container>
                    </div>
                </Footer>
                <Modal
                    title="Confirm Your Action"
                    body={ this.renderModalBody() }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.setState({ form: { ...form, enforceGapLoans: true }}, this.showModal.toggle() )
                        },
                        {
                            type: 'danger',
                            label: 'Yes',
                            onClick: () => {
                                this.minimumNetTakeHomePay.setState({ error: false, message: '' });
                                this.setState({ form: { ...form, enforceGapLoans: false, minimumNetTakeHomePay: this.handleMinTakeAmt( form.payrollFrequency ) }}, this.showModal.toggle() );
                            }
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.showModal = ref; } }
                />
                <Modal
                    title="Discard Changes"
                    body={
                        <ModalBody>
                           Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?
                        </ModalBody>
                    }
                    buttons={ [

                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.setState({ form: { ...form, eenforceGapLoans: false }});
                                this.discardModal.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'danger',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/company-settings/payroll/payroll-groups', true );
                                this.discardModal.toggle();
                            }
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                />
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    CompanyPayroll: makeSelectCompanyPayroll(),
    notification: makeSelectNotification(),
    Loading: makeSelectLoading(),
    BtnLoading: makeSelectBtnLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        companyPayrollAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( CompanyPayroll );
