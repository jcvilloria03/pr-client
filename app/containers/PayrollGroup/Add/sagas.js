/* eslint-disable no-unused-expressions */
/* eslint-disable no-param-reassign */
import get from 'lodash/get';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { cancel, put, call, take } from 'redux-saga/effects';
import { Fetch } from 'utils/request';

import { company } from '../../../utils/CompanyService';
import { auth } from '../../../utils/AuthService';

import {

    BTN_LOADING,
    NOTIFICATION,
    REINITIALIZE_PAGE,
    SET_NOTIFICATION,
    SET_SUBMITTED
} from './constants';

import { resetStore } from '../../App/sagas';

import { browserHistory } from '../../../utils/BrowserHistory';

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield put({ type: BTN_LOADING, payload: true });
        const companyId = company.getLastActiveCompanyId();
        const data = {
            company_id: companyId,
            account_id: auth.getUser().account_id,
            ...payload.data
        };
        yield call( Fetch, '/philippine/payroll_group', { method: 'POST', data });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully Added',
            show: true,
            type: 'success'
        });
        browserHistory.push( '/company-settings/payroll/payroll-groups', true );
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message || `Request failed with statuscode ${error.response.status}` : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}
/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * submitForm
 */
export function* watchForSubmitForm() {
    const watcher = yield takeLatest( SET_SUBMITTED, submitForm );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForReinitializePage
];
