/*
 *
 * CompanyPayroll constants
 *
 */
const namespace = 'app/containers/CompanyPayroll/Add';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const REINITIALIZE_PAGE = `${namespace}/REINITIALIZE_PAGE`;

export const SET_TOTAL_PAGE = `${namespace}/SET_TOTAL_PAGE`;
export const SET_SUBMITTED = `${namespace}/SET_SUBMITTED`;
export const SUBMITTED = `${namespace}/SUBMITTED`;

export const BTN_LOADING = `${namespace}/BTN_LOADING`;
