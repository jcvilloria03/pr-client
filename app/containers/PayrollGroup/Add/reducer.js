import { fromJS } from 'immutable';
import {
    BTN_LOADING,
    DEFAULT_ACTION,
    LOADING,
    SET_NOTIFICATION,
    SET_TOTAL_PAGE,
    SUBMITTED
} from './constants';

const initialState = fromJS({
    payrolls: [],
    loading: false,
    btnLoading: false,
    submitted: '',
    totalPage: 0,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * CompanyPayroll reducer
 *
 */
function companyPayrollReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );

        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_TOTAL_PAGE:
            return state.set( 'totalPage', fromJS( action.payload ) );
        case SUBMITTED:
            return state.set( 'submitted', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', action.payload );
        default:
            return state;
    }
}

export default companyPayrollReducer;

