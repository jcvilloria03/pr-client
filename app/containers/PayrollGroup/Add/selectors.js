import { createSelector } from 'reselect';

/**
 * Direct selector to the companyPayroll state domain
 */
const selectCompanyPayrollDomain = () => ( state ) => state.get( 'PayrollGroupAdd' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
  selectCompanyPayrollDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectBtnLoading = () => createSelector(
  selectCompanyPayrollDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

const makeSelectPayrollList = () => createSelector(
  selectCompanyPayrollDomain(),
  ( substate ) => substate.get( 'payrolls' )
);

const makeSelectNotification = () => createSelector(
  selectCompanyPayrollDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
  selectCompanyPayrollDomain(),
  ( substate ) => substate.get( 'submitted' )
);

/**
 * Default selector used by CompanyPayroll
 */
const makeSelectCompanyPayroll = () => createSelector(
  selectCompanyPayrollDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectCompanyPayroll,
  selectCompanyPayrollDomain,
  makeSelectLoading,
  makeSelectPayrollList,
  makeSelectNotification,
  makeSelectBtnLoading,
  makeSelectSubmitted
};
