import styled from 'styled-components';

export const Header = styled.div`
.nav{
    padding:10px 10px;
    background:#f0f4f6;
    margin-bottom:10px;
    position: fixed;
    top: 76px;
    right: 0;
    left: 0;
    width: 100%;
    z-index:9;
    }
`;

export const Footer = styled.div`
    .from_footer{
        margin: 14px 0px 0px;
        padding: 14px 0px;
        background: #F0F4F6;
        margin-top:280px;
        position: fixed;
        bottom: 0;
        right: 0;
        left: 0;
    }
    .from_footer .footer_button{
        width: 100%;
        text-align: end;
    }
    .from_footer .footer_button button{
        padding: 14px 28px;
        border-radius: 2rem;
        &:focus{
            outline:none;
        }
        cursor:pointer
    }
    .from_footer .footer_button .btn_cancel{
        color: #474747;
        border: 1px solid #83D24B;
        background-color: #ffffff;
        margin-right: 5px;
        
    }
    
    
`;

export const PageWrapper = styled.div`
    padding-left: 91px;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 120px;
    .loader {
        padding: 140px 0;
    }
    .Select-control{
        .Select-menu-outer{
            z-index: 9;
        }
    }
    .schdule_tooltip{
        display: flex;
        align-items: center;
        font-size: 14px;
        margin-bottom:0px;
        color:#5b5b5b;
        button{
            width: 20px;
            height: 20px;
            padding: 0;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
        }
        a{
            padding: 0;
            border-bottom: none;
            padding-left: 6px;
        }
        .bg_tooltip {
            position: relative;
            display: inline-block;
            width:20px;
            height:20px;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
            margin-left: 6px;
            text-align: center;
            p{
                font-size: 15px;
                font-weight: bold;
                color: #adadad;
            }
        }

        .bg_tooltip .tooltiptext {
            visibility: hidden;
            width: 242px;
            background-color: #fff;
            color: #03a9f4;
            border: 2px solid #b1dcf0;
            text-align: left;
            border-radius: 6px;
            padding: 5px 16px;
            position: absolute;
            left: 26px;
            top: -50%;
            transform: translateY(-15%);
            z-index: 1;
        }
        .bg_tooltip .tooltiptextStart{
            transform: translateY(-26%);
        }

        .bg_tooltip .tooltiptextEnd{
            transform: translateY(-20%);
            width: 262px;
        }
        .bg_tooltip .tooltiptextHours{
            transform: translateY(-17%);
            width: 255px;
        }
        .bg_tooltip:hover .tooltiptext {
            visibility: visible;
        }
        .bg_tooltip .tooltiptext::after {
            content: " ";
            position: absolute;
            top: 50%;
            right: 99%;
            margin-top: -5px;
            border: solid #b1dcf0;
            border-width: 0 2px 2px 0;
            display: inline-block;
            padding: 4px;
            transform: rotate(135deg);
            background-color: #ffff;
        }
    }
    .checkcontaion{
        display:flex;
        gap:1px;
        color:gray;
        .rc-checkbox-inner{
            width: 1rem;
            height: 1rem;
            border: 1px solid #000;
            &:after{
                width: 7px;
                height: 10px;
            }
        }
    }
    .main_section{
        width: 85%;
        padding-bottom: 80px;
        margin: auto;
        .heading{
            margin-top: 20px;
            margin-bottom: 50px;
            h1{
                text-align: center;
                font-size: 24px;
                font-weight: 600;
                margin-bottom: 5px;
            }
            p{
                text-align: center;
                font-size: 14px;
                margin-bottom: 0px;
            }
        }
        .input_pay
        {
            input{
                border-color:#eb7575;
            }
            lable{
                span{
                    color:#eb7575;
                }
            }
            span{
                color:#eb7575;
            }
            p{
                color:#eb7575;
            }
        }
        .checkout_compute{
            margin-top: 1rem;
            display: flex;
            gap: 12px;
            .rc-switch{
                height: 0.6rem;
                width: 42px;
                margin-right: 8px;
                &:after{
                    background-color: #474747;
                    width: 1.25rem;
                    height: 1.25rem;
                    top: -6px;
                    left:0px;
                }
            }
            .rc-switch-checked{
                border: 1px solid #ccc;
                background-color: #ccc;
                &:after{
                    left:22px;
                    background-color: #83d24b;
                }
            }
        }
        .enforce_switch{
            margin-top:1rem;
            .rc-switch{
                height: 0.6rem;
                width: 42px;
                margin-right: 8px;
                &:after{
                    background-color: #474747;
                    width: 1.25rem;
                    height: 1.25rem;
                    top: -6px;
                    left:0px;
                }
            }
            .rc-switch-checked{
                border: 1px solid #ccc;
                background-color: #ccc;
                &:after{
                    left:22px;
                    background-color: #83d24b;
                }
            }
            
        }
        .payment_contain{
            p{
                margin-top: 1rem;
            }
        }
    }

    .datepicker_contain{
        p{
            display:none;
        }
        span{
            margin: 0px;
        }
        .DayPickerInput{
            width:100%;
            input{
                width:100%;
            }
        }
        .bg_whylabel{
            color:  #5b5b5b;
        }
    }
    .start_point{
        display:block !important;
        color:#eb7575;
        margin-right:3px;
        margin-bottom:0px;
        order: unset;
    }
    .datealert{
        input{
            border-color:#eb7575;
        }
        lable{
            span{
                color:#eb7575;
            }
        }
        span{
            color:#eb7575;
        }
        p{
            color:#eb7575;
        }
        .start_point{
            display:block !important;
            color:#eb7575;
            margin-bottom:0px;
            margin-right:3px;
        }

`;

export const ModalBody = styled.div`
font-size:14px;
margin-bottom:20px;
`;
