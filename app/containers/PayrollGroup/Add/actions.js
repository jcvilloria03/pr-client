
import {
    DEFAULT_ACTION,
    SET_SUBMITTED
} from './constants';

/**
 *
 * CompanyPayroll actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * payroll Request Type
 * @param {object} payload
 */
export function submitPayrollGroup( payload ) {
    return {
        type: SET_SUBMITTED,
        payload
    };
}
