/*
 *
 * CompanyPayroll constants
 *
 */
const namespace = 'app/containers/CompanyPayroll/Edit';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const REINITIALIZE_PAGE = `${namespace}/REINITIALIZE_PAGE`;
export const GET_COMPANY_PAYROLL_LIST = `${namespace}/GET_COMPANY_PAYROLL_LIST`;
export const SET_COMPANY_PAYROLL_LIST = `${namespace}/SET_COMPANY_PAYROLL_LIST`;
export const SET_TOTAL_PAGE = `${namespace}/SET_TOTAL_PAGE`;
export const SET_SUBMITTED = `${namespace}/SET_SUBMITTED`;
export const SUBMITTED = `${namespace}/SUBMITTED`;

export const GET_EDIT = `${namespace}/GET_EDIT`;
export const SET_EDIT = `${namespace}/SET_EDIT`;

export const GET_GENERATE = `${namespace}/GET_GENERATE`;
export const SET_GENERATE = `${namespace}/SET_GENERATE`;

export const SET_SUBMIT_DATA = `${namespace}/SET_SUBMIT_DATA`;
export const SUBMIT_DATA = `${namespace}/SUBMIT_DATA`;

export const BTN_LOADING = `${namespace}/BTN_LOADING`;
