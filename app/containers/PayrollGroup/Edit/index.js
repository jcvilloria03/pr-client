/* eslint-disable no-constant-condition */
/* eslint-disable no-empty */
/* eslint-disable no-param-reassign */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment';

import { Fetch } from 'utils/request';
import { formatCurrencyToDecimalNotation, stripNonDigit } from 'utils/functions';
import Modal from 'components/Modal';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { H2, H3, H6 } from 'components/Typography';
import SnackBar from 'components/SnackBar';
import Sidebar from 'components/Sidebar';
import Input from 'components/Input';
import A from 'components/A';
import Button from 'components/Button';
import Loader from 'components/Loader';
import SalSelect from '../../../components/Select';
import DatePicker from '../../../components/DatePicker';
import Switch from '../../../components/Switch';

import { DATE_FORMATS, PAYMENT_SCHEDULE, PAYRUN_NON_WORKING_DAYS, PAY_FREQUENCY } from '../../../utils/constants';
import { browserHistory } from '../../../utils/BrowserHistory';

import * as companyPayrollEditAction from './actions';
import {
    makeSelectCompanyPayroll,
    makeSelectHasGenerated,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSubmitted,
    makeSelectBtnLoading

} from './selectors';

import { Footer, Header, LoadingStyles, PageWrapper, ModalBody } from './styles';
import { company } from '../../../utils/CompanyService';
import SalCheckbox from '../../../components/SalCheckbox';

/**
 *
 * CompanyPayroll
 *
 */
export class CompanyPayroll extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        params: React.PropTypes.object,
        setSubmitEditData: React.PropTypes.func,
        getIsEditData: React.PropTypes.func,
        getHasGenerated: React.PropTypes.func,
        EditCompanyPayroll: React.PropTypes.object,
        hasGenerated: React.PropTypes.bool,
        Loading: React.PropTypes.bool,
        BtnLoading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            form: {
                id: '',
                account_id: '',
                company_id: '',
                name: '',
                isEmptyName: false,
                minimum_net_take_home_pay: 4000.00,
                enforce_gap_loans: false,
                day_factor: 260.00,
                hours_pay_day: 8.00,
                payroll_frequency: 'MONTHLY',
                cut_off_date: '',
                pay_date: '',
                pay_run_posting: '',
                cut_off_date_2: null,
                pay_date_2: null,
                pay_run_posting_2: null,
                fcd_endof_month: false,
                fpd_endof_month: false,
                fpp_endof_month: false,
                compute_tardiness: false,
                compute_overbreak: false,
                compute_undertime: false,
                scd_endof_month: false,
                spd_endof_month: false,
                spp_endof_month: false,
                sss_pay: 'EVERY_PAY',
                philhealth_pay: 'EVERY_PAY',
                hdmf_pay: 'EVERY_PAY',
                wtax_pay: 'EVERY_PAY',
                wtax_method: 'BASED_ON_ACTUAL',
                hdmf_method: 'BASED_ON_ACTUAL',
                sss_method: 'BASED_ON_ACTUAL',
                philhealth_method: 'BASED_ON_ACTUAL',
                payrun_non_working_day: '',
                attend_period_switch: false,
                att_first_cut_off_date: null,
                att_second_cut_off_date: null,
                att_fcd_endof_month: false,
                att_scd_endof_month: false
            },
            hasGenerated: false,
            isNameAvailable: false,
            cut_off_Validate: false,
            cut_off_Validate_2: false,
            pay_Validate: false,
            pay_Validate_2: false,
            pay_run_posting_Validate: false,
            pay_run_posting_Validate_2: false,
            att_cut_off_Validate: false,
            att_cut_off_Validate_2: false

        };
        this.companyId = company.getLastActiveCompanyId();
        this.gapModal = null;
        this.discardModal = null;
    }

    componentDidMount() {
        this.props.getIsEditData( +this.props.params.id );
        this.props.getHasGenerated( +this.props.params.id );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.EditCompanyPayroll !== this.props.EditCompanyPayroll ) {
            const isEdit = nextProps.EditCompanyPayroll.isEdit;
            this.setState( ( state ) => {
                state.form.id = isEdit.id;
                state.form.account_id = isEdit.account_id;
                state.form.company_id = isEdit.company_id;
                state.form.name = isEdit.name;
                state.form.minimum_net_take_home_pay = isEdit.minimum_net_take_home_pay !== '0.00' ? stripNonDigit( isEdit.minimum_net_take_home_pay ) : this.handleMinTakeAmt( isEdit.payroll_frequency );
                state.form.enforce_gap_loans = isEdit.enforce_gap_loans;
                state.form.day_factor = isEdit.day_factor;
                state.form.hours_pay_day = isEdit.hours_per_day;
                state.form.payroll_frequency = isEdit.payroll_frequency;
                state.form.cut_off_date = isEdit.cut_off_date;
                state.form.pay_date = isEdit.pay_date;
                state.form.pay_run_posting = isEdit.pay_run_posting;
                state.form.cut_off_date_2 = isEdit.cut_off_date_2;
                state.form.pay_date_2 = isEdit.pay_date_2;
                state.form.pay_run_posting_2 = isEdit.pay_run_posting_2;
                state.form.fcd_endof_month = isEdit.fcd_end_of_month;
                state.form.fpd_endof_month = isEdit.fpd_end_of_month;
                state.form.fpp_endof_month = isEdit.fpp_end_of_month;
                state.form.compute_tardiness = isEdit.compute_tardiness;
                state.form.compute_overbreak = isEdit.compute_overbreak;
                state.form.compute_undertime = isEdit.compute_undertime;
                state.form.scd_endof_month = isEdit.scd_end_of_month;
                state.form.spd_endof_month = isEdit.spd_end_of_month;
                state.form.spp_endof_month = isEdit.spp_end_of_month;
                state.form.sss_pay = isEdit.sss_schedule;
                state.form.philhealth_pay = isEdit.philhealth_schedule;
                state.form.hdmf_pay = isEdit.hdmf_schedule;
                state.form.wtax_pay = isEdit.wtax_schedule;
                state.form.wtax_method = isEdit.wtax_method;
                state.form.hdmf_method = isEdit.hdmf_method;
                state.form.sss_method = isEdit.sss_method;
                state.form.philhealth_method = isEdit.philhealth_method;
                state.form.payrun_non_working_day = isEdit.non_working_day_option;
                state.form.attend_period_switch = isEdit.attend_period_switch;
                state.form.att_first_cut_off_date = isEdit.att_first_cut_off_date;
                state.form.att_second_cut_off_date = isEdit.att_second_cut_off_date;
                state.form.att_fcd_endof_month = isEdit.att_fcd_end_of_month;
                state.form.att_scd_endof_month = isEdit.att_scd_end_of_month;
            });
        }
    }

    submitForm=async () => {
        try {
            const hasEditedData = {
                compute_overbreak: this.state.form.compute_overbreak,
                compute_tardiness: this.state.form.compute_tardiness,
                compute_undertime: this.state.form.compute_undertime,
                cut_off_date: this.state.form.cut_off_date !== 'Invalid date' ? this.state.form.cut_off_date : '',
                cut_off_date_2: this.state.form.cut_off_date_2 !== 'Invalid date' ? this.state.form.cut_off_date_2 : '',
                day_factor: this.state.form.day_factor,
                enforce_gap_loans: this.state.form.enforce_gap_loans,
                fcd_end_of_month: this.state.form.fcd_endof_month,
                fpd_end_of_month: this.state.form.fpd_endof_month,
                fpp_end_of_month: this.state.form.fpp_endof_month,
                hdmf_method: this.state.form.hdmf_method,
                hdmf_schedule: this.state.form.hdmf_pay,
                hours_per_day: this.state.form.hours_pay_day,
                minimum_net_take_home_pay: this.state.form.enforce_gap_loans ? this.state.form.minimum_net_take_home_pay : '',
                name: this.state.form.name,
                non_working_day_option: this.state.form.payrun_non_working_day,
                pay_date: this.state.form.pay_date !== 'Invalid date' ? this.state.form.pay_date : '',
                pay_date_2: this.state.form.pay_date_2 !== 'Invalid date' ? this.state.form.pay_date_2 : '',
                pay_run_posting: this.state.form.pay_run_posting !== 'Invalid date' ? this.state.form.pay_run_posting : '',
                pay_run_posting_2: this.state.form.pay_run_posting_2 !== 'Invalid date' ? this.state.form.pay_run_posting_2 : '',
                payroll_frequency: this.state.form.payroll_frequency,
                philhealth_method: this.state.form.philhealth_method,
                philhealth_schedule: this.state.form.philhealth_pay,
                scd_end_of_month: this.state.form.scd_endof_month,
                spd_end_of_month: this.state.form.spd_endof_month,
                spp_end_of_month: this.state.form.spp_endof_month,
                sss_method: this.state.form.sss_method,
                sss_schedule: this.state.form.sss_pay,
                wtax_method: this.state.form.wtax_method,
                wtax_schedule: this.state.form.wtax_pay,
                account_id: this.state.form.account_id,
                company_id: this.state.form.company_id,
                payroll_group_id: this.state.form.id,
                attend_period_switch: this.state.form.attend_period_switch,
                att_first_cut_off_date: this.state.form.attend_period_switch === true && this.state.form.att_first_cut_off_date !== 'Invalid date' ? this.state.form.att_first_cut_off_date : null,
                att_second_cut_off_date: this.state.form.attend_period_switch === true && this.state.form.att_second_cut_off_date !== 'Invalid date' ? this.state.form.att_second_cut_off_date : null,
                att_fcd_end_of_month: this.state.form.attend_period_switch === true ? this.state.form.att_fcd_endof_month : false,
                att_scd_end_of_month: this.state.form.attend_period_switch === true ? this.state.form.att_scd_endof_month : false
            };

            if ( ( this.state.form.payroll_frequency === 'SEMI_MONTHLY' ) && ( this.state.form.cut_off_date_2 === '' || this.state.form.cut_off_date_2 === 'Invalid date' ) ) {
                this.setState({ cut_off_Validate_2: true });
            }
            if ( ( this.state.form.payroll_frequency === 'SEMI_MONTHLY' ) && ( this.state.form.pay_date_2 === '' || this.state.form.pay_date_2 === 'Invalid date' ) ) {
                this.setState({ pay_Validate_2: true });
            }
            if ( ( this.state.form.payroll_frequency === 'SEMI_MONTHLY' ) && ( this.state.form.pay_run_posting_2 === '' || this.state.form.pay_run_posting_2 === 'Invalid date' ) ) {
                this.setState({ pay_run_posting_Validate_2: true });
            }
            if ( ( this.state.form.payroll_frequency === 'SEMI_MONTHLY' ) && ( this.state.form.attend_period_switch === true ) && ( this.state.form.att_second_cut_off_date === '' || this.state.form.att_second_cut_off_date === 'Invalid date' ) ) {
                this.setState({ att_cut_off_Validate_2: true });
            }

            const response = await Fetch( `/company/${this.state.form.company_id}/payroll_group/is_name_available`, { method: 'POST', data: hasEditedData });
            if ( response.available ) {
                const isEditPayload = this.props.hasGenerated ?
                {
                    account_id: this.state.form.account_id,
                    company_id: this.state.form.company_id,
                    compute_overbreak: this.state.form.compute_overbreak,
                    compute_tardiness: this.state.form.compute_tardiness,
                    compute_undertime: this.state.form.compute_undertime,
                    name: this.state.form.name
                } : hasEditedData;

                const { form } = this.state;
                if ( this.state.form.name !== '' && ( ( this.state.form.enforce_gap_loans && this.state.form.minimum_net_take_home_pay !== '' && +this.state.form.minimum_net_take_home_pay > 0 ) || true ) &&
                    ( this.state.form.day_factor !== '' && ( +this.state.form.day_factor > 0 && +this.state.form.day_factor <= 365 ) ) &&
                    ( this.state.form.hours_pay_day !== '' && ( +this.state.form.hours_pay_day > 0 && +this.state.form.hours_pay_day <= 24 ) ) &&
                    this.state.cut_off_Validate_2 === false && this.state.cut_off_Validate === false && this.state.pay_Validate === false &&
                    this.state.att_cut_off_Validate_2 === false && this.state.att_cut_off_Validate === false &&
                    this.state.pay_Validate_2 === false && this.state.pay_run_posting_Validate === false &&
                    this.state.pay_run_posting_Validate_2 === false && ( !this.handleIsAfter( form.cut_off_date, form.pay_date ) || moment( form.cut_off_date ).isSame( form.pay_date, 'day' ) ) &&
                    !this.secondCutOffValidate( form ) && !this.secondPayValidate( form ) && !this.secondPayRunValidate( form ) && !this.attsecondCutOffValidate( form )
                ) {
                    const payload = { id: this.props.params.id, data: isEditPayload };
                    this.props.setSubmitEditData( payload );
                }
            } else {
                this.setState({ isNameAvailable: true });
            }
        } catch ( err ) {
        }
    }
    dateValidate=( name, value ) => {
        const values = value === '';
        this.setState({ [ name ]: values });
    }
    secondCutOffValidate=( form ) => !!( ( !this.state.cut_off_Validate_2 && this.handleDateSameAfter( form.cut_off_date, form.cut_off_date_2 ) ) )

    attsecondCutOffValidate=( form ) => !!( ( !this.state.att_cut_off_Validate_2 && ( this.state.form.attend_period_switch === true ) && this.handleDateSameAfter( form.att_first_cut_off_date, form.att_second_cut_off_date ) ) )

    secondPayValidate=( form ) => ( !!( !this.state.pay_Validate_2 && ( this.handleDateSameAfter( form.pay_date, form.pay_date_2 ) || this.handleIsAfter( form.cut_off_date_2, form.pay_date_2 ) ) ) )

    secondPayRunValidate=( form ) => ( !!( !this.state.pay_run_posting_Validate_2 && this.handleDateSameAfter( form.pay_run_posting, form.pay_run_posting_2 ) ) )

    handleLastMonthDate=( date ) => moment( ( date !== 'Invalid date' && date && date ) || new Date() ).endOf( 'month' ).format( 'YYYY-MM-DD' )

    handleDateFormat=( date ) => moment( date ).format( 'YYYY-MM-DD' )

    handleIsAfter=( date, afterDate ) => moment( date ).isAfter( afterDate, 'day' ) ;

    handleDateSameAfter=( date, afterDate ) => ( !!( moment( date ).isSame( afterDate, 'day' ) || moment( date ).isAfter( afterDate, 'day' ) ) );

    handleMinTakeAmt=( fre ) => ( fre === 'MONTHLY' ? formatCurrencyToDecimalNotation( 4000 ) : fre === 'WEEKLY' ? formatCurrencyToDecimalNotation( 1000 ) : formatCurrencyToDecimalNotation( 2000 ) )

    handleSetAmt=( fre ) => {
        this.minimumNetTakeHomePay.setState({ error: false, message: '' });
        const value = this.handleMinTakeAmt( fre );
        return value;
    }

    handleMinCondition=( value, names ) => {
        if ( names === 'day_factor' && ( +value < 1 || value > 365 ) ) {
            this.day_factor.setState({ error: true, errorMessage: 'Value must be between 1 and 365' });
        }
        if ( names === 'hours_pay_day' && ( +value < 1 || value > 24 ) ) {
            this.hours_pay_day.setState({ error: true, errorMessage: 'Value must be between 1 and 24' });
        }
    }
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { form } = this.state;
        const { hasGenerated, BtnLoading } = this.props;
        return (
            <div style={ { height: '100vh', overflow: 'auto' } }>
                <Helmet
                    title="Edit Payroll Groups"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Payroll Groups' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                {this.props.Loading === false &&
                <Header>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/payroll/payroll-groups', true );
                                } }
                            >
                                &#8592; Back to Payroll Groups
                            </A>
                        </Container>
                    </div>
                </Header>}
                <PageWrapper>
                    {this.props.Loading === true &&
                    <LoadingStyles>
                        <div className="loading-main">
                            <H2>Loading Company Payroll Groups.</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </div>
                    </LoadingStyles>
                        }
                    {this.props.Loading === false &&
                    <div className="main_container">
                        <Sidebar
                            items={ sidebarLinks }
                        />

                        <div className="heade">
                            <div className="heading">
                                <H3>Edit Payroll Group</H3>
                                <p>View and update payroll groups. You can add, edit, or delete payroll groups.</p>
                            </div>
                            <div className="row" style={ { paddingRight: '15px', paddingLeft: '15px' } }>
                                <div><H6 className="row-title">Payroll Group Details</H6>
                                    <div className="row mb-2">
                                        <div className="col-xs-4">
                                            <Input
                                                className={ this.state.isNameAvailable || form.name === '' ? 'input_pay' : '' }
                                                id="group_name"
                                                label="Payroll group name"
                                                placeholder="Type in payroll group name"
                                                type="text"
                                                value={ form.name }
                                                onChange={ ( value ) => this.setState({ isNameAvailable: false, form: { ...form, name: value, isEmptyName: value.length <= 0 }}) }
                                                required
                                            />{this.state.isNameAvailable ? <p className="isName">Record name already exists</p> : ''}
                                        </div>
                                        <div className="col-xs-4">
                                            <Input
                                                className={ form.minimum_net_take_home_pay === '' || +form.minimum_net_take_home_pay < 0 ? 'input_pay' : '' }
                                                id="minimum_pay"
                                                label=" Minimum net take home pay"
                                                placeholder="Type in payroll group minimum net take home pay"
                                                type="number"
                                                minNumber={ 1 }
                                                ref={ ( ref ) => { this.minimumNetTakeHomePay = ref; } }
                                                value={ form.minimum_net_take_home_pay }
                                                onChange={ ( value ) => this.setState({ form: { ...form, minimum_net_take_home_pay: value }}) }
                                                onBlur={ ( value ) => this.setState({ form: { ...form, minimum_net_take_home_pay: formatCurrencyToDecimalNotation( value ) }}) }
                                                required
                                                disabled={ hasGenerated || !form.enforce_gap_loans }
                                            />
                                        </div>
                                        <div className="col-xs-4">
                                            <SalSelect
                                                id="pay_frequency"
                                                label={
                                                    <span className="schdule_tooltip"><p className="start_point">* </p> Pay frequency
                                                        <div className="bg_tooltip">
                                                            <p>?</p>
                                                            <span className="tooltiptext"> Select the frequency of employee <br />salary disbursement.</span>
                                                        </div>
                                                    </span>
                                                }
                                                value={ form.payroll_frequency }
                                                ref={ ( ref ) => { this.payroll_frequency = ref; } }
                                                data={ PAY_FREQUENCY }
                                                disabled={ hasGenerated }
                                                onChange={ ({ value }) => this.setState({
                                                    cut_off_Validate_2: value === 'SEMI_MONTHLY' && false,
                                                    pay_Validate_2: value === 'SEMI_MONTHLY' && false,
                                                    pay_run_posting_Validate_2: value === 'SEMI_MONTHLY' && false,
                                                    form: {
                                                        ...form,
                                                        payroll_frequency: value,
                                                        minimum_net_take_home_pay: this.handleSetAmt( value ),
                                                        hdmf_method: value === 'MONTHLY' ? 'BASED_ON_ACTUAL' : 'EQUALLY_SPLIT',
                                                        sss_method: value === 'MONTHLY' ? 'BASED_ON_ACTUAL' : 'EQUALLY_SPLIT',
                                                        philhealth_method: value === 'MONTHLY' ? 'BASED_ON_ACTUAL' : 'EQUALLY_SPLIT',
                                                        cut_off_date_2: null,
                                                        pay_date_2: '',
                                                        pay_run_posting_2: '',
                                                        scd_endof_month: false,
                                                        spd_endof_month: false,
                                                        spp_endof_month: false,
                                                        att_second_cut_off_date: null,
                                                        att_first_cut_off_date: null,
                                                        att_scd_endof_month: false,
                                                        att_fcd_endof_month: false
                                                    }}) }
                                            />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-4" >
                                            <Input
                                                className={ form.day_factor === '' ? 'input_pay' : +form.day_factor <= 0 || +form.day_factor > 365 ? 'input_pay' : '' }
                                                id="noof_workingday_per_year"
                                                label=" Number of working days per year"
                                                type="number"
                                                value={ form.day_factor }
                                                disabled={ hasGenerated }
                                                ref={ ( ref ) => { this.day_factor = ref; } }
                                                onChange={ ( value ) => this.setState({ form: { ...form, day_factor: value }}) }
                                                onBlur={ ( value ) => this.setState({ form: { ...form, day_factor: formatCurrencyToDecimalNotation( value ) }}, this.handleMinCondition( value, 'day_factor' ) ) }
                                                required
                                            />
                                        </div>
                                        <div className="col-xs-4">
                                            <Input
                                                className={ form.hours_pay_day === '' ? 'input_pay' : +form.hours_pay_day <= 0 || +form.hours_pay_day > 24 ? 'input_pay' : '' }
                                                id="noof_workinghours_per_day"
                                                label="Number of working hours per day"
                                                type="number"
                                                value={ form.hours_pay_day }
                                                disabled={ hasGenerated }
                                                ref={ ( ref ) => { this.hours_pay_day = ref; } }
                                                onChange={ ( value ) => this.setState({ form: { ...form, hours_pay_day: value }}) }
                                                onBlur={ ( value ) => this.setState({ form: { ...form, hours_pay_day: formatCurrencyToDecimalNotation( value ) }}, this.handleMinCondition( value, 'hours_pay_day' ) ) }
                                                required
                                            />
                                        </div>
                                        <div className="col-xs-4">
                                            <SalSelect
                                                id="non_working_day"
                                                label={
                                                    <p className="schdule_tooltip"><p className="start_point">* </p>If pay run falls on a non-working day
                                                        <div className="bg_tooltip">
                                                            <p>?</p>
                                                            <span className="tooltiptext"> Select condition from dropdown</span>
                                                        </div>
                                                    </p>
                                                }
                                                defaultValue="Move payday beforenon-working days"
                                                value={ form.payrun_non_working_day }
                                                data={ PAYRUN_NON_WORKING_DAYS }
                                                disabled={ hasGenerated }
                                                onChange={ ({ value }) => this.setState({ form: { ...form, payrun_non_working_day: value }}) }
                                            />
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col-xs-4">
                                            <div className="enforce_switch">
                                                <Switch
                                                    id="enforce_gap_loans"
                                                    ref={ ( ref ) => { this.enforce_gap_loans = ref; } }
                                                    checked={ form.enforce_gap_loans }
                                                    disabled={ hasGenerated }
                                                    onChange={ ( value ) => {
                                                        value === false ? this.gapModal.toggle() : this.setState({ form: { ...form, enforce_gap_loans: true, minimum_net_take_home_pay: this.handleMinTakeAmt( form.payroll_frequency ) }});
                                                    } }
                                                />
                                                <span>Enforce Gap Loans</span>
                                            </div>
                                            {!form.enforce_gap_loans ? <p>With Gap Loans turned off, employees may receive salaries lower than the minimum net take-home pay allowed by your company and/or the law.</p> : ''}
                                        </div>
                                        <div className="col-xs-4 SalSelects">
                                            <div className="enforce_switch">
                                                <Switch
                                                    id="attend_period_switch"
                                                    ref={ ( ref ) => { this.attend_period_switch = ref; } }
                                                    checked={ this.state.form.attend_period_switch }
                                                    disabled={ hasGenerated }
                                                    onChange={ ( value ) => { this.setState({ form: { ...form, attend_period_switch: value }}); } }
                                                />
                                                <span>Attendance Period different from Pay Period</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div><H6 className="row-title">Pay Period Schedule</H6>
                                    <div className="row">
                                        <div className={ this.state.cut_off_Validate ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                            <DatePicker
                                                label={
                                                    <span className="schdule_tooltip"><p className="start_point">* </p>{form.payroll_frequency === 'SEMI_MONTHLY' ? 'First' : ''} Payroll Period Cut-off Date
                                                        <div className="bg_tooltip">
                                                            <span className="bg_whylabel">?</span>
                                                            <span className="tooltiptext"> Select the end-date of the attendance cut-off period.</span>
                                                        </div>
                                                    </span>
                                                }
                                                dayFormat={ DATE_FORMATS.DISPLAY }
                                                selectedDay={ form.cut_off_date }
                                                onChange={ ( value ) => this.setState({ form: { ...form, cut_off_date: this.handleDateFormat( value ), fcd_endof_month: this.handleLastMonthDate( value ) === this.handleDateFormat( value ) }}, this.dateValidate( 'cut_off_Validate', value ) ) }
                                                disabled={ hasGenerated }
                                            />{this.state.cut_off_Validate ? <span>Please choose a data</span> : ''}
                                        </div>
                                        <div className={ ( this.state.pay_Validate ) || ( !this.state.pay_Validate && this.handleIsAfter( form.cut_off_date, form.pay_date ) ) ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                            <DatePicker
                                                label={
                                                    <span className="schdule_tooltip"><p className="start_point">* </p>{form.payroll_frequency === 'SEMI_MONTHLY' ? 'First' : ''} Pay date
                                                        <div className="bg_tooltip">
                                                            <span className="bg_whylabel">?</span>
                                                            <span className="tooltiptext"> Select the date of the salary payout.</span>
                                                        </div>
                                                    </span>
                                                }
                                                dayFormat={ DATE_FORMATS.DISPLAY }
                                                selectedDay={ form.pay_date }
                                                onChange={ ( value ) => this.setState({ form: { ...form, pay_date: this.handleDateFormat( value ), fpd_endof_month: this.handleLastMonthDate( value ) === this.handleDateFormat( value ) }}, this.dateValidate( 'pay_Validate', value ) ) }
                                                disabled={ hasGenerated }
                                            />{this.state.pay_Validate ? <span>Please choose a data</span> : ''}
                                            {!this.state.pay_Validate && this.handleIsAfter( form.cut_off_date, form.pay_date ) ? <span>Pay date must be after Cut-Off date</span> : ''}
                                        </div>
                                        <div className={ this.state.pay_run_posting_Validate ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                            <DatePicker
                                                label={
                                                    <span className="schdule_tooltip"><p className="start_point">* </p>{form.payroll_frequency === 'SEMI_MONTHLY' ? 'First' : ''}  Pay run posting
                                                        <div className="bg_tooltip">
                                                            <span className="bg_whylabel">?</span>
                                                            <span className="tooltiptext"> Select the reporting date of the payroll.</span>
                                                        </div>
                                                    </span>
                                                }
                                                dayFormat={ DATE_FORMATS.DISPLAY }
                                                selectedDay={ form.pay_run_posting }
                                                onChange={ ( value ) => this.setState({ form: { ...form, pay_run_posting: this.handleDateFormat( value ), fpp_endof_month: this.handleLastMonthDate( value ) === this.handleDateFormat( value ) }}, this.dateValidate( 'pay_run_posting_Validate', value ) ) }
                                                disabled={ hasGenerated }
                                            />{this.state.pay_run_posting_Validate ? <span>Please choose a data</span> : ''}
                                        </div>
                                    </div>
                                    {( form.payroll_frequency === 'MONTHLY' ) || ( form.payroll_frequency === 'SEMI_MONTHLY' ) ?
                                (
                                    <div className="row">
                                        <div className="col-xs-4">
                                            <div className="checkcontaion">
                                                <SalCheckbox
                                                    id="fcd_endof_month"
                                                    ref={ ( ref ) => { this.fcd_endof_month = ref; } }
                                                    checked={ form.fcd_endof_month }
                                                    onChange={ () => this.setState({
                                                        cut_off_Validate: false,
                                                        form: {
                                                            ...form,
                                                            fcd_endof_month: !form.fcd_endof_month,
                                                            cut_off_date: this.handleLastMonthDate( form.cut_off_date ) }}) }
                                                    disabled={ hasGenerated === true }
                                                />
                                                <p>Always end of the month?</p>
                                            </div>
                                        </div>
                                        <div className="col-xs-4">
                                            <div className="checkcontaion">
                                                <SalCheckbox
                                                    id="fpd_endof_month"
                                                    ref={ ( ref ) => { this.fpd_endof_month = ref; } }
                                                    checked={ form.fpd_endof_month }
                                                    onChange={ () => this.setState({
                                                        pay_Validate: false,
                                                        form: {
                                                            ...form,
                                                            fpd_endof_month: !form.fpd_endof_month,
                                                            pay_date: this.handleLastMonthDate( form.pay_date ) }}) }
                                                    disabled={ hasGenerated }
                                                />
                                                <p>Always end of the month?</p>
                                            </div>
                                        </div>
                                        <div className="col-xs-4">
                                            <div className="checkcontaion">
                                                <SalCheckbox
                                                    id="fpp_endof_month"
                                                    ref={ ( ref ) => { this.fpp_endof_month = ref; } }
                                                    checked={ form.fpp_endof_month }
                                                    onChange={ () => this.setState({
                                                        pay_run_posting_Validate: false,
                                                        form: {
                                                            ...form,
                                                            fpp_endof_month: !form.fpp_endof_month,
                                                            pay_run_posting: this.handleLastMonthDate( form.pay_run_posting ) }}) }
                                                    label="Always end of the month?"
                                                    disabled={ hasGenerated }
                                                />
                                                <p>Always end of the month?</p>
                                            </div>
                                        </div>
                                    </div>
                                ) : ''}
                                    { form.payroll_frequency === 'SEMI_MONTHLY' ? (
                                        <div>
                                            <div className="row">
                                                <div className={ this.state.cut_off_Validate_2 || this.secondCutOffValidate( form ) ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                                    <DatePicker
                                                        label={
                                                            <span className="schdule_tooltip"><p className="start_point">* </p>  Second Cut-Off date
                                                                <div className="bg_tooltip">
                                                                    <span className="bg_whylabel">?</span>
                                                                    <span className="tooltiptext"> Select the end-date of the attendance cut-off period.</span>
                                                                </div>
                                                            </span>
                                                        }
                                                        dayFormat={ DATE_FORMATS.DISPLAY }
                                                        selectedDay={ form.cut_off_date_2 }
                                                        disabled={ hasGenerated }
                                                        onChange={ ( value ) => this.setState({ form: { ...form,
                                                            cut_off_date_2: this.handleDateFormat( value ),
                                                            scd_endof_month: this.handleLastMonthDate( value ) === this.handleDateFormat( value ) }},
                                                        this.dateValidate( 'cut_off_Validate_2', value ) ) }
                                                    />{this.state.cut_off_Validate_2 ? <span>Please choose a data</span> : ''}
                                                    {this.secondCutOffValidate( form ) ? <span>Second Cut-Off date must be after First Cut-Off date</span> : ''}
                                                </div>
                                                <div className={ this.state.pay_Validate_2 || ( this.secondPayValidate( form ) ) ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                                    <DatePicker
                                                        label={
                                                            <span className="schdule_tooltip"><p className="start_point">* </p>  Second Pay date
                                                                <div className="bg_tooltip">
                                                                    <span className="bg_whylabel">?</span>
                                                                    <span className="tooltiptext"> Select the date of the salary payout.</span>
                                                                </div>
                                                            </span>
                                                        }
                                                        dayFormat={ DATE_FORMATS.DISPLAY }
                                                        selectedDay={ form.pay_date_2 }
                                                        disabled={ hasGenerated }
                                                        onChange={ ( value ) => this.setState({ form: { ...form,
                                                            pay_date_2: this.handleDateFormat( value ),
                                                            spd_endof_month: this.handleLastMonthDate( value ) === this.handleDateFormat( value ) }},
                                                        this.dateValidate( 'pay_Validate_2', value ) ) }
                                                    />{this.state.pay_Validate_2 ? <span>Please choose a data</span> : ''}
                                                    {this.secondPayValidate( form ) ? <span>Second Pay date must be after Second Cut-Off date and First Pay date</span> : ''}
                                                </div>
                                                <div className={ this.state.pay_run_posting_Validate_2 || ( this.secondPayRunValidate( form ) ) ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                                    <DatePicker
                                                        label={
                                                            <span className="schdule_tooltip"><p className="start_point">* </p>  Second Pay run posting
                                                                <div className="bg_tooltip">
                                                                    <span className="bg_whylabel">?</span>
                                                                    <span className="tooltiptext"> Select the reporting date of the payroll.</span>
                                                                </div>
                                                            </span>
                                                        }
                                                        dayFormat={ DATE_FORMATS.DISPLAY }
                                                        selectedDay={ form.pay_run_posting_2 }
                                                        disabled={ hasGenerated }
                                                        onChange={ ( value ) => this.setState({ form: { ...form,
                                                            pay_run_posting_2: this.handleDateFormat( value ),
                                                            spp_endof_month: this.handleLastMonthDate( value ) === this.handleDateFormat( value ) }},
                                                        this.dateValidate( 'pay_run_posting_Validate_2', value ) ) }
                                                    />{this.state.pay_run_posting_Validate_2 ? <span>Please choose a data</span> : ''}
                                                    {this.secondPayRunValidate( form ) ? <span>Second pay run posting must be after First pay run posting</span> : ''}
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xs-4">
                                                    <div className="checkcontaion">
                                                        <SalCheckbox
                                                            id="scd_endof_month"
                                                            ref={ ( ref ) => { this.scd_endof_month = ref; } }
                                                            disabled={ hasGenerated }
                                                            checked={ form.scd_endof_month }
                                                            onChange={ () => this.setState({
                                                                cut_off_Validate_2: false,
                                                                form: {
                                                                    ...form,
                                                                    scd_endof_month: !form.scd_endof_month,
                                                                    cut_off_date_2: this.handleLastMonthDate( form.cut_off_date_2 )
                                                                }}) }
                                                        />
                                                        <p>Always end of the month?one</p>
                                                    </div>
                                                </div>
                                                <div className="col-xs-4">
                                                    <div className="checkcontaion">
                                                        <SalCheckbox
                                                            id="spd_endof_month"
                                                            ref={ ( ref ) => { this.spd_endof_month = ref; } }
                                                            disabled={ hasGenerated }
                                                            checked={ form.spd_endof_month }
                                                            onChange={ () => this.setState({
                                                                pay_Validate_2: false,
                                                                form: {
                                                                    ...form,
                                                                    spd_endof_month: !form.spd_endof_month,
                                                                    pay_date_2: this.handleLastMonthDate( form.pay_date_2 )
                                                                }}) }
                                                        />
                                                        <p>Always end of the month?two</p>
                                                    </div>
                                                </div>
                                                <div className="col-xs-4">
                                                    <div className="checkcontaion">
                                                        <SalCheckbox
                                                            id="spp_endof_month"
                                                            ref={ ( ref ) => { this.spp_endof_month = ref; } }
                                                            disabled={ hasGenerated }
                                                            checked={ form.spp_endof_month }
                                                            onChange={ () => this.setState({
                                                                pay_run_posting_Validate_2: false,
                                                                form: {
                                                                    ...form,
                                                                    spp_endof_month: !form.spp_endof_month,
                                                                    pay_run_posting_2: this.handleLastMonthDate( form.pay_run_posting_2 ) }}) }
                                                        />
                                                        <p>Always end of the month?three</p>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                            ) : ''}
                                </div>
                            </div>
                            {
                            form.attend_period_switch === true ? (
                                <div><H6 className="row-title">Attendance Period Schedule</H6>
                                    <div className="row">
                                        <div className={ this.state.att_cut_off_Validate ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                            <DatePicker
                                                label={
                                                    <span className="schdule_tooltip"><p className="start_point">* </p>First Period Attendance Cut-Off date
                                                        <div className="bg_tooltip">
                                                            <span className="bg_whylabel">?</span>
                                                            <span className="tooltiptext"> Select the end-date of the attendance cut-off period.</span>
                                                        </div>
                                                    </span>
                                                }
                                                dayFormat={ DATE_FORMATS.DISPLAY }
                                                selectedDay={ form.att_first_cut_off_date }
                                                onChange={ ( value ) => this.setState({ form: { ...form,
                                                    att_first_cut_off_date: this.handleDateFormat( value ),
                                                    att_fcd_endof_month: this.handleLastMonthDate( value ) === this.handleDateFormat( value ) }},
                                                    this.dateValidate( 'att_cut_off_Validate', value ) ) }
                                                disabled={ hasGenerated }
                                            />{this.state.att_cut_off_Validate ? <span>Please choose a data</span> : ''}
                                        </div>
                                        {
                                        form.payroll_frequency === 'SEMI_MONTHLY' ? (
                                            <div className={ this.state.att_cut_off_Validate_2 || this.attsecondCutOffValidate( form ) ? 'col-xs-4 datepicker_contain datealert' : 'col-xs-4 datepicker_contain' }>
                                                <DatePicker
                                                    label={
                                                        <span className="schdule_tooltip"><p className="start_point">* </p>  Second Period Attendance Cut-Off date
                                                            <div className="bg_tooltip">
                                                                <span className="bg_whylabel">?</span>
                                                                <span className="tooltiptext"> Select the end-date of the attendance cut-off period.</span>
                                                            </div>
                                                        </span>
                                                    }
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    selectedDay={ form.att_second_cut_off_date }
                                                    disabled={ hasGenerated }
                                                    onChange={ ( value ) => this.setState({ form: { ...form,
                                                        att_second_cut_off_date: this.handleDateFormat( value ),
                                                        att_scd_endof_month: this.handleLastMonthDate( value ) === this.handleDateFormat( value ) }},
                                                    this.dateValidate( 'att_cut_off_Validate_2', value ) ) }
                                                />{this.state.att_cut_off_Validate_2 ? <span>Please choose a data</span> : ''}
                                                {this.attsecondCutOffValidate( form ) ? <span>Second Period Attendance Cut-Off date must be after First Cut-Off date</span> : ''}
                                            </div>
                                            ) : ''
                                        }
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-4">
                                            <div className="checkcontaion">
                                                <SalCheckbox
                                                    id="att_fcd_endof_month"
                                                    ref={ ( ref ) => { this.att_fcd_endof_month = ref; } }
                                                    checked={ form.att_fcd_endof_month }
                                                    onChange={ () => this.setState({
                                                        att_cut_off_Validate: false,
                                                        form: {
                                                            ...form,
                                                            att_fcd_endof_month: !form.att_fcd_endof_month,
                                                            att_first_cut_off_date: this.handleLastMonthDate( form.att_first_cut_off_date ) }}) }
                                                    disabled={ hasGenerated === true }
                                                />
                                                <p>Always end of the month?</p>
                                            </div>
                                        </div>
                                        {
                                            form.payroll_frequency === 'SEMI_MONTHLY' ? (
                                                <div className="col-xs-4">
                                                    <div className="checkcontaion">
                                                        <SalCheckbox
                                                            id="att_scd_endof_month"
                                                            ref={ ( ref ) => { this.att_scd_endof_month = ref; } }
                                                            disabled={ hasGenerated }
                                                            checked={ form.att_scd_endof_month }
                                                            onChange={ () => this.setState({
                                                                att_cut_off_Validate_2: false,
                                                                form: {
                                                                    ...form,
                                                                    att_scd_endof_month: !form.att_scd_endof_month,
                                                                    att_second_cut_off_date: this.handleLastMonthDate( form.att_second_cut_off_date )
                                                                }}) }
                                                        />
                                                        <p>Always end of the month?one</p>
                                                    </div>
                                                </div>
                                            ) : ''
                                        }
                                    </div>
                                </div>
                            ) : ''
                            }
                            <hr />
                            <div><H6 className="row-title">Contribution Schedule</H6>
                                <div className="row">
                                    <div className="col-xs-8">
                                        <div className="col-xs-6  pl-0">
                                            <span>* SSS payment schedule</span>
                                            {form.payroll_frequency === 'SEMI_MONTHLY' ? (
                                                <SalSelect
                                                    id="sss_pay"
                                                    value={ form.sss_pay }
                                                    data={ PAYMENT_SCHEDULE }
                                                    disabled={ hasGenerated }
                                                    onChange={ ({ value }) => this.setState({ form: { ...form, sss_pay: value, sss_method: value === 'EVERY_PAY' ? 'EQUALLY_SPLIT' : 'BASED_ON_ACTUAL' }}) }
                                                /> ) :
                                                    <p>{ form.sss_pay === 'EVERY_PAY' ? 'Every Pay' : 'Every Pay'}</p>}
                                        </div>
                                        <div className="col-xs-6  payment_contain">
                                            <span>* SSS payment method</span>
                                            <p>{ form.sss_method === 'BASED_ON_ACTUAL' ? 'Based on Actual' : 'Equally Split' }</p>
                                        </div>
                                        <div className="col-xs-6 mt-3 pl-0">
                                            <span>* Philhealth payment schedule</span>
                                            {form.payroll_frequency === 'SEMI_MONTHLY' ? (
                                                <SalSelect
                                                    id="philhealth_pay"
                                                    value={ form.philhealth_pay }
                                                    data={ PAYMENT_SCHEDULE }
                                                    disabled={ hasGenerated }
                                                    onChange={ ({ value }) => this.setState({ form: { ...form, philhealth_pay: value, philhealth_method: value === 'EVERY_PAY' ? 'EQUALLY_SPLIT' : 'BASED_ON_ACTUAL' }}) }
                                                /> ) :
                                                    <p>{ form.philhealth_pay === 'EVERY_PAY' ? 'Every Pay' : 'Every Pay' }</p>}
                                        </div>
                                        <div className="col-xs-6 mt-3 payment_contain">
                                            <span>* Philhealth payment method</span>
                                            <p>{ form.philhealth_method === 'BASED_ON_ACTUAL' ? 'Based on Actual' : 'Equally Split' }</p>
                                        </div>
                                    </div>
                                    <div className="col-xs-4">
                                        <div>Attendance Option:</div>
                                        <div className="checkout_compute">
                                            <Switch
                                                ref={ ( ref ) => { this.compute_tardiness = ref; } }
                                                checked={ form.compute_tardiness }
                                                onChange={ ( value ) => this.setState({ form: { ...form, compute_tardiness: value }}) }
                                            />
                                            <span>Compute Tardiness</span>
                                        </div>
                                        <div className="checkout_compute">
                                            <Switch
                                                ref={ ( ref ) => { this.compute_overbreak = ref; } }
                                                checked={ form.compute_overbreak }
                                                onChange={ ( value ) => this.setState({ form: { ...form, compute_overbreak: value }}) }
                                            />
                                            <span>Compute Overbreak</span>
                                        </div>
                                        <div className="checkout_compute">
                                            <Switch
                                                ref={ ( ref ) => { this.compute_undertime = ref; } }
                                                checked={ form.compute_undertime }
                                                onChange={ ( value ) => this.setState({ form: { ...form, compute_undertime: value }}) }
                                            />
                                            <span>Compute Undertime</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">

                                </div>
                                <div className="row">
                                    <div className="col-xs-4">
                                        <span>* HDMF payment schedule</span>
                                        {form.payroll_frequency === 'SEMI_MONTHLY' ? (
                                            <SalSelect
                                                id="hdmf_pay"
                                                value={ form.hdmf_pay }
                                                data={ PAYMENT_SCHEDULE }
                                                disabled={ hasGenerated }
                                                onChange={ ({ value }) => this.setState({ form: { ...form, hdmf_pay: value, hdmf_method: value === 'EVERY_PAY' ? 'EQUALLY_SPLIT' : 'BASED_ON_ACTUAL' }}) }
                                            /> ) : <p>{ form.hdmf_pay === 'EVERY_PAY' ? 'Every Pay' : 'Every Pay' }</p>}
                                    </div>
                                    <div className="col-xs-4 payment_contain">
                                        <span>* HDMF payment method</span>
                                        <p>{ form.hdmf_method === 'BASED_ON_ACTUAL' ? 'Based on Actual' : 'Equally Split' }</p>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-4 payment_contain">
                                        <span>* WTAX payment schedule</span>
                                        <p>{form.wtax_pay === 'EVERY_PAY' ? 'Every Pay' : 'Every Pay' }</p>
                                    </div>
                                    <div className="col-xs-4 payment_contain">
                                        <span>* WTAX payment method</span>
                                        <p>{form.wtax_method === 'BASED_ON_ACTUAL' ? 'Based on actual' : ''}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    }
                </PageWrapper>
                {this.props.Loading === false &&
                <Footer>
                    <div className="from_footer">
                        <Container>
                            <div className="footer_button">
                                <Button
                                    label="Cancel"
                                    type="neutral"
                                    size="large"
                                    onClick={ () => this.discardModal.toggle() }
                                />
                                <Button
                                    label={ BtnLoading ? <Loader /> : 'Update' }
                                    type="action"
                                    size="large"
                                    onClick={ () => this.submitForm() }
                                />
                            </div>
                        </Container>
                    </div>
                </Footer>
                }
                <Modal
                    title="Confirm Your Action"
                    body={
                        <ModalBody>
                            With Gap Loans turned of, employees may receive salaries lower than the minimum net take-home pay allowed by your company and/or the law. Are you sure you want to proceed?
                        </ModalBody>
                    }
                    buttons={ [
                        {
                            id: 'buttonCancel',
                            type: 'grey',
                            label: 'No',
                            onClick: () => {
                                this.setState({ form: { ...form, enforce_gap_loans: true }});
                                this.gapModal.toggle();
                            }
                        },
                        {
                            id: 'buttonProceed',
                            type: 'danger',
                            label: 'Yes',
                            onClick: () => {
                                this.minimumNetTakeHomePay.setState({ error: false, message: '' });
                                this.setState({ form: { ...form, enforce_gap_loans: false, minimum_net_take_home_pay: this.handleMinTakeAmt( form.payroll_frequency ) }});
                                this.gapModal.toggle();
                            }
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.gapModal = ref; } }
                />
                <Modal
                    title="Discard Changes"
                    body={
                        <ModalBody>
                           Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?
                        </ModalBody>
                    }
                    buttons={ [
                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.setState({ form: { ...form, enforce_gap_loans: false }});
                                this.discardModal.toggle();
                            }
                        }, {
                            id: 'buttonCancel',
                            type: 'danger',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/company-settings/payroll/payroll-groups', true );
                                this.discardModal.toggle();
                            }
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                />
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    EditCompanyPayroll: makeSelectCompanyPayroll(),
    notification: makeSelectNotification(),
    Loading: makeSelectLoading(),
    submitted: makeSelectSubmitted(),
    hasGenerated: makeSelectHasGenerated(),
    BtnLoading: makeSelectBtnLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        companyPayrollEditAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( CompanyPayroll );
