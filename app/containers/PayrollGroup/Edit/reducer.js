import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    SET_NOTIFICATION,
    SET_TOTAL_PAGE,
    SET_SUBMITTED,
    SET_EDIT,
    SET_GENERATE,
    SET_SUBMIT_DATA,
    BTN_LOADING
} from './constants';

const initialState = fromJS({
    loading: false,
    submitted: '',
    isEdit: {},
    isGenerate: false,
    btnLoading: false,
    totalPage: 0,
    submitResponse: '',
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * CompanyPayroll reducer
 *
 */
function companyPayrollReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_TOTAL_PAGE:
            return state.set( 'totalPage', fromJS( action.payload ) );
        case SET_SUBMITTED:
            return state.set( 'submitted', fromJS( action.payload ) );
        case SET_EDIT:
            return state.set( 'isEdit', fromJS( action.payload ) );
        case SET_GENERATE:
            return state.set( 'isGenerate', fromJS( action.payload ) );
        case SET_SUBMIT_DATA:
            return state.set( 'submitResponse', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', action.payload );
        default:
            return state;
    }
}

export default companyPayrollReducer;

