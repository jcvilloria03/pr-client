import {
    DEFAULT_ACTION,
    SET_SUBMITTED,
    GET_EDIT,
    GET_GENERATE,
    SUBMIT_DATA
} from './constants';

/**
 *
 * CompanyPayroll actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * payroll Request Type
 * @param {object} payload
 */
export function editPayroll( payload ) {
    return {
        type: SET_SUBMITTED,
        payload
    };
}

/**
 * payroll Request Type
 * @param {object} payload
 */
export function getIsEditData( payload ) {
    return {
        type: GET_EDIT,
        payload
    };
}

/**
 * payroll Request Type
 * @param {object} payload
 */
export function getHasGenerated( payload ) {
    return {
        type: GET_GENERATE,
        payload
    };
}

/**
 * payroll Request Type
 * @param {object} payload
 */
export function setSubmitEditData( payload ) {
    return {
        type: SUBMIT_DATA,
        payload
    };
}
