import { get } from 'lodash';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { cancel, put, call, take } from 'redux-saga/effects';
import { Fetch } from 'utils/request';
import {
    LOADING, NOTIFICATION,
    REINITIALIZE_PAGE,
    SET_NOTIFICATION,
    GET_EDIT,
    SET_EDIT,
    SET_GENERATE,
    GET_GENERATE,
    SUBMIT_DATA,
    BTN_LOADING
} from './constants';
import { resetStore } from '../../App/sagas';
import { company } from '../../../utils/CompanyService';
import { RECORD_UPDATED_MESSAGE } from '../../../utils/constants';
import { formatFeedbackMessage } from '../../../utils/functions';
import { browserHistory } from '../../../utils/BrowserHistory';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Edit form Data
 * @param payload
 */
export function* getIsEditData({ payload }) {
    try {
        yield put({ type: LOADING, payload: true });

        const isEditData = yield call( Fetch, `/philippine/payroll_group/${payload}`, { method: 'GET' });

        yield put({ type: SET_EDIT, payload: isEditData && isEditData || {}});
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Check generate
 * @param payload
 */
export function* getHasGenerated({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/payrolls`, { method: 'GET' });
        const hasGenerate = response && response.data.some( ( data ) => data.payroll_group_id === payload );

        yield put({ type: SET_GENERATE, payload: hasGenerate && hasGenerate || false });
    } catch ( error ) {
        yield put({
            type: SET_GENERATE,
            payload: false
        });
    }
}

/**
 * Submit Edit
 * @param payload
 */
export function* setSubmitEditData({ payload }) {
    try {
        yield put({ type: BTN_LOADING, payload: true });
        const { id, data } = payload;
        const response = yield call( Fetch, `/philippine/payroll_group/${id}`, { method: 'PATCH', data });

        if ( response.id && response.id ) {
            yield call( showSuccessMessage );
            browserHistory.push( '/company-settings/payroll/payroll-groups', true );
        }
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message || `Request failed with statuscode ${error.response.status}` : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

 /**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetIsEditPayroll() {
    const watcher = yield takeEvery( GET_EDIT, getIsEditData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetHasGenerate() {
    const watcher = yield takeEvery( GET_GENERATE, getHasGenerated );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForEditSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_DATA, setSubmitEditData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetIsEditPayroll,
    watchForReinitializePage,
    watchForEditSubmitForm,
    watchForGetHasGenerate
];
