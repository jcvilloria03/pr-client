
import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_LOAD,
    SET_USERS,
    SET_PAGINATION,
    SET_FILTER_DATA,
    NOTIFICATION_SAGA,
    SET_UNLINK_USERS,
    SET_DEVICE,
    SET_USER,
    SET_PAGE
} from './constants';

import { RESET_STORE } from '../../../App/constants';

const initialState = fromJS({
    loading: false,
    load: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    user: {},
    filterData: {},
    pagination: {
        current_page: 0,
        from: 0,
        last_page: 0,
        per_page: 0,
        to: 0,
        total: 0
    },
    page: {
        current_page: 0,
        from: 0,
        last_page: 0,
        per_page: 0,
        to: 0,
        total: 0
    },
    unlinkUser: [],
    deviceList: [],
    data: []
});

/**
 * user details view reducer
 */
function usersReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_LOAD:
            return state.set( 'load', action.payload );
        case SET_USERS:
            return state.set( 'user', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_PAGE:
            return state.set( 'page', fromJS( action.payload ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_UNLINK_USERS:
            return state.set( 'unlinkUser', fromJS( action.payload ) );
        case SET_DEVICE:
            return state.set( 'deviceList', fromJS( action.payload ) );
        case SET_USER:
            return state.set( 'data', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default usersReducer;
