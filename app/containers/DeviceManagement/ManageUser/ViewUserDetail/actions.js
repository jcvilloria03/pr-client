/* eslint-disable require-jsdoc */

import {
    GET_USERS,
    GET_MANAGE_USERS,
    NOTIFICATION,
    GET_UNLINK_USERS,
    GET_DEVICE,
    GET_USER,
    DELETE_UNLINKUSER,
    SYNC_USER,
    LINK_DATA
} from './constants';

export function getUsers() {
    return {
        type: GET_USERS
    };
}

export function getManageUsers( payload ) {
    return {
        type: GET_MANAGE_USERS,
        payload
    };
}

export function getUnlinkUsers( payload ) {
    return {
        type: GET_UNLINK_USERS,
        payload
    };
}

export function getDevice( payload ) {
    return {
        type: GET_DEVICE,
        payload
    };
}

export function getUser( payload ) {
    return {
        type: GET_USER,
        payload
    };
}

/**
 * Delete unlinkUser
 */
export function deleteUnlinkUser( payload ) {
    return {
        type: DELETE_UNLINKUSER,
        payload
    };
}

/**
 * sync unlinkUser
 */
export function syncUser( payload ) {
    return {
        type: SYNC_USER,
        payload
    };
}

export function linkData( payload ) {
    return {
        type: LINK_DATA,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
