
import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectViewUserDetails = () => ( state ) => state.get( 'viewUser' );

const makeSelectLoading = () => createSelector(
    selectViewUserDetails(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectLoad = () => createSelector(
  selectViewUserDetails(),
( substate ) => substate.get( 'load' )
);

const makeSelectDownloading = () => createSelector(
    selectViewUserDetails(),
    ( substate ) => substate.get( 'downloading' )
);

const makeSelectUser = () => createSelector(
    selectViewUserDetails(),
  ( substate ) => substate && substate.get( 'user' ).toJS()
);

const makeSelectFilterData = () => createSelector(
    selectViewUserDetails(),
  ( substate ) => substate.get( 'filterData' ).toJS()
);

const makeSelectPagination = () => createSelector(
    selectViewUserDetails(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectPage = () => createSelector(
  selectViewUserDetails(),
( substate ) => substate.get( 'page' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewUserDetails(),
  ( substate ) => substate && substate.get( 'notification' ).toJS()
);

const makeSelectUnlinkUser = () => createSelector(
    selectViewUserDetails(),
  ( substate ) => substate && substate.get( 'unlinkUser' ).toJS()
);

const makeSelectDevice = () => createSelector(
  selectViewUserDetails(),
( substate ) => substate && substate.get( 'deviceList' ).toJS()
);

const makeSelectData = () => createSelector(
  selectViewUserDetails(),
( substate ) => substate && substate.get( 'data' ).toJS()
);

const makeSelectApprovals = () => createSelector(
  selectViewUserDetails(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectApprovals,
  makeSelectLoading,
  makeSelectLoad,
  makeSelectDownloading,
  makeSelectUser,
  makeSelectFilterData,
  makeSelectNotification,
  makeSelectPagination,
  makeSelectUnlinkUser,
  makeSelectDevice,
  makeSelectData,
  makeSelectPage
};
