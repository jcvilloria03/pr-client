// /*
//  *
//  * ViewUserDetail constants
//  *
//  */

export const SET_LOADING = 'app/DeviceManagement/SET_LOADING';
export const SET_LOAD = 'app/DeviceManagement/SET_LOAD';
export const SET_USERS = 'app/DeviceManagement/SET_USERS';
export const GET_USERS = 'app/DeviceManagement/GET_USERS';
export const GET_MANAGE_USERS = 'app/DeviceManagement/GET_MANAGE_USERS';
export const SET_PAGINATION = 'app/DeviceManagement/SET_PAGINATION';
export const SET_PAGE = 'app/DeviceManagement/SET_PAGE';
export const SET_FILTER_DATA = 'app/DeviceManagement/SET_FILTER_DATA';
export const NOTIFICATION = 'app/DeviceManagement/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/DeviceManagement/NOTIFICATION_SAGA';
export const SET_UNLINK_USERS = 'app/DeviceManagement/SET_UNLINK_USERS';
export const GET_UNLINK_USERS = 'app/DeviceManagement/GET_UNLINK_USERS';
export const SET_DEVICE = 'app/DeviceManagement/SET_DEVICE';
export const GET_DEVICE = 'app/DeviceManagement/GET_DEVICE';
export const GET_USER = 'app/DeviceManagement/GET_USER';
export const SET_USER = 'app/DeviceManagement/SET_USER';
export const DELETE_UNLINKUSER = 'app/DeviceManagement/DELETE_UNLINKUSER';
export const SYNC_USER = 'app/DeviceManagement/SYNC_USER';
export const LINK_DATA = 'app/DeviceManagement/LINK_DATA';
