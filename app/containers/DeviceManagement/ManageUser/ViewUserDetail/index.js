/* eslint-disable no-confusing-arrow */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/img-has-alt */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable import/first */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import isEqual from 'lodash/isEqual';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import SubHeader from 'containers/SubHeader';
import Sidebar from 'components/Sidebar';
import Table from 'components/Table';
import Input from 'components/Input';
import Icon from 'components/Icon';
import Button from 'components/Button';
import SalConfirm from 'components/SalConfirm';
import { H2, H3, H5, H6, P } from 'components/Typography';
import SnackBar from 'components/SnackBar';

import {
    DEVICE_MANAGEMENT_SUBHEADER_ITEMS
} from 'utils/constants';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { formatPaginationLabel } from 'utils/functions';

import {
    makeSelectLoading,
    makeSelectLoad,
    makeSelectUser,
    makeSelectFilterData,
    makeSelectNotification,
    makeSelectDownloading,
    makeSelectPagination,
    makeSelectUnlinkUser,
    makeSelectDevice,
    makeSelectData,
    makeSelectPage
} from './selectors';
import { bindActionCreators } from 'redux';
import * as viewUserActions from './actions';
import {
    PageWrapper,
    ConfirmBodyWrapperStyle,
    LoadingStyles
} from './styles';

/**
 *
 * View
 *
 */
export class ViewUserDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        loading: React.PropTypes.bool,
        load: React.PropTypes.bool,
        params: React.PropTypes.object,
        products: React.PropTypes.array,
        getManageUsers: React.PropTypes.func,
        pagination: React.PropTypes.object,
        page: React.PropTypes.object,
        user: React.PropTypes.object,
        unlinkUser: React.PropTypes.array,
        getUnlinkUsers: React.PropTypes.func,
        deviceList: React.PropTypes.array,
        getDevice: React.PropTypes.func,
        person: React.PropTypes.array,
        getUser: React.PropTypes.func,
        deleteUnlinkUser: React.PropTypes.func,
        syncUser: React.PropTypes.func,
        linkData: React.PropTypes.func,
        errors: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            subscription: null,
            salpayViewPermission: false,
            deviceData: [],
            showDelete: false,
            deleteLabel: '',
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            id: '',
            showDetails: false,
            value: []
        };
        this.searchInput = null;
        this.handleSearch = this.handleSearch.bind( this );
        this.handleTableChanges = this.handleTableChanges.bind( this );
    }

    componentWillMount() {
        const value = {
            id: this.props.params.userId
        };
        this.props.getManageUsers( value );
        this.props.getUser( value );
        const data = {
            page: 1,
            perPage: 10
        };
        this.props.getUnlinkUsers( data );
        this.props.getDevice( data );
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.user.with_registered_face === false ) {
            if ( !this.props.unlinkUser.length || !isEqual( nextProps.users, this.props.unlinkUser ) ) {
                this.handleSearch( nextProps.users );
            }
        }
    }

    deleteUnlinkUser() {
        const unlinkIDs = [];
        this.usersTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                unlinkIDs.push( this.usersTable.props.data[ index ].id );
            }
        });
        const data = {
            id: this.props.params.userId,
            ids: unlinkIDs
        };
        this.props.deleteUnlinkUser( data );
        this.setState({ showDelete: false });
    }

    syncUnlinkUser() {
        const unlinkIDs = [];
        this.usersTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                unlinkIDs.push( this.usersTable.props.data[ index ].id );
            }
        });
        const data = {
            id: this.props.params.userId,
            ids: unlinkIDs
        };
        this.props.syncUser( data );
        this.setState({ syncModal: false });
    }

    linkUser() {
        const data = {
            ids: this.props.params.userId,
            id: this.state.selectedRow.id
        };
        this.props.linkData( data );
    }

    handleTableChanges( tableProps = this.usersTable.tableComponent.state ) {
        Object.assign( tableProps, { dataLength: this.props && this.props.pagination.total });
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    handleSearch() {
        let searchQuery = null;
        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( this.props.user.with_registered_face === false ) {
            if ( searchQuery ) {
                const data = {
                    page: 1,
                    perPage: 10,
                    search: searchQuery
                };
                this.props.getUnlinkUsers( data );
            }
        } else if ( searchQuery ) {
            const value = {
                page: 1,
                perPage: 10,
                search: searchQuery
            };
            this.props.getDevice( value );
        }
    }

    /**
     *
     * View render method
     *
     */
    render() {
        const {
            salpayViewPermission,
            selectedRow,
            showDetails
        } = this.state;

        const {
            products,
            user,
            pagination,
            getManageUsers,
            deviceList,
            person,
            page,
            unlinkUser
        } = this.props;

        const showHeader = unlinkUser.length > 0;

        const getTableColumns = [
            {
                id: 'user',
                header: showHeader ? 'User' : '',
                minWidth: 100,
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.user_name}
                    </div>
                )
            },
            {
                id: 'person_id',
                header: showHeader ? 'PersonID' : '',
                minWidth: 100,
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.person_id}
                    </div>
                )
            },
            {
                id: 'device',
                header: showHeader ? 'Device' : '',
                minWidth: 100,
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.device_name}
                    </div>
                )
            },
            {
                id: 'userSelect',
                header: ' ',
                minWidth: 50,
                sortable: false,
                render: ({ row }) => (
                    <Button
                        id={ `button-view-payroll-${row.id}` }
                        label={ <span>View</span> }
                        type="grey"
                        size="small"
                        onClick={ () => this.setState({ selectedRow: row, showDetails: true }) }
                    />
                )
            }
        ];

        const getDeviceColumns = [
            {
                id: 'device',
                header: 'Device Name',
                minWidth: 100,
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.device_name}
                    </div>
                )
            },
            {
                id: 'with_registered _face?',
                header: 'With Registered Face?',
                minWidth: 100,
                sortable: false,
                render: ({ row }) => (
                    person && person.map( ( p ) => +row.id === +p.device_id ?
                        <div>{p.status ? p.status : 'YES'}</div> :
                        <div>{p.status !== 'AWAITING_SYNC' ? 'AWAITING_SYNC' : 'No'}</div>
                    )
                )
            }
        ];
        return (
            <div>
                <Helmet
                    title="View"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SnackBar
                    message={ this.props.errors.message }
                    title={ this.props.errors.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.errors = ref; } }
                    show={ this.props.errors.show }
                    delay={ 5000 }
                    type={ this.props.errors.type }
                />
                <SalConfirm
                    onConfirm={ () => this.deleteUnlinkUser() }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Deleting this user will erase all its user records from the device. The device will no longer be able to recognize the user once the record is deleted. Do you want to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Unlinked User"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <SalConfirm
                    onConfirm={ () => this.syncUnlinkUser() }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Are you sure you want to sync the enrolled FaceID of the user to the selected device(s)?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Sync Enrolled FaceID"
                    buttonStyle="success"
                    showCancel
                    confirmText="sync"
                    cancelText="Cancel"
                    visible={ this.state.syncModal }
                />
                <SubHeader
                    items={ DEVICE_MANAGEMENT_SUBHEADER_ITEMS }
                />
                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        isExpired: false,
                        accountViewPermission: true,
                        salpayViewPermission,
                        isSubscribedToPayroll: products && subscriptionService.isSubscribedToPayroll( products )
                    }) }
                />
                <PageWrapper>
                    <Container>
                        {this.props.load ? (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading User Detail...</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        ) : (
                            <div>
                                <div className="content">
                                    <div className="heading">
                                        <H2 noBottomMargin>User Details</H2>
                                        <P noBottomMargin>Link your user account information into a registered FacePassID</P>
                                        <div className="main_heading">
                                            <div className="heading_1">
                                                <P noBottomMargin><b>Name: </b></P>
                                                <P noBottomMargin className="name_01">{user.full_name}</P>
                                            </div>
                                            <div className="heading_1">
                                                <P noBottomMargin><b>Employee Of: </b></P>
                                                <P noBottomMargin className="name_01">
                                                    {user && user.employee_of && user.employee_of.map( ( data ) => data.company_name )}
                                                </P>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="content second">
                                    {user.with_registered_face === false && (
                                        <div className="heading">
                                            <p className="schdule_tooltip">
                                                <H3>List of Unlinked Users</H3>
                                                <div className="bg_tooltip">
                                                    <label>?</label>
                                                    <span className="tooltiptext">This shows the list of users who was registered directly on FacePass and not yet linked into a Salarium V3 user account</span>
                                                </div>
                                            </p>
                                        </div>
                                    )}
                                    {user.with_registered_face === false ?
                                        <div>
                                            <div className={ showDetails === true ? 'tableData' : 'dataTable' }>
                                                <div className="title">
                                                    <H5>Unlinked Users List</H5>
                                                    <div className="search-wrapper">
                                                        <Input
                                                            className="search"
                                                            id="search"
                                                            ref={ ( ref ) => { this.searchInput = ref; } }
                                                            onKeyPress={ ( e ) => {
                                                                e.charCode === 13 && this.handleSearch();
                                                            } }
                                                            addon={ {
                                                                content: <Icon name="search" />,
                                                                placement: 'right'
                                                            } }
                                                        />
                                                    </div>
                                                </div>
                                                <div className="data">
                                                    <Table
                                                        className="table-unlink-users"
                                                        data={ unlinkUser }
                                                        columns={ getTableColumns }
                                                        pagination={ unlinkUser.length > 0 }
                                                        ref={ ( ref ) => { this.usersTable = ref; } }
                                                        onDataChange={ this.handleTableChanges }
                                                        onPageChange={ ( data ) => getManageUsers({
                                                            page: data + 1,
                                                            perPage: page.per_page
                                                        }) }
                                                        onPageSizeChange={ ( data ) => getManageUsers({
                                                            page: 1,
                                                            perPage: data
                                                        }) }
                                                        pageSize={ page.per_page }
                                                        pages={ page.last_page }
                                                        manual
                                                        noDataText="No Data"
                                                    />
                                                </div>
                                                <div className="details">
                                                    {showDetails === true &&
                                                        <div>
                                                            <img src={ selectedRow.s3_uri } /><br />
                                                            <lable>Name : {selectedRow.user_name}</lable><br />
                                                            <lable>PersonID : {selectedRow.person_id}</lable><br />
                                                            <lable>Device : {selectedRow.device_name}</lable><br />
                                                            <Button
                                                                className={ this.state.permission.delete ? '' : 'hide' }
                                                                label={ <span>Link Data</span> }
                                                                type="action"
                                                                onClick={ () => this.linkUser() }
                                                            />
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </div> :
                                        <div>
                                            <div className="dataTable">
                                                <div className="title">
                                                    <H6>Device List</H6>
                                                    <div className="search-wrapper">
                                                        <Input
                                                            className="search"
                                                            id="searched"
                                                            ref={ ( ref ) => { this.searchInput = ref; } }
                                                            onKeyPress={ ( e ) => {
                                                                e.charCode === 13 && this.handleSearch();
                                                            } }
                                                            addon={ {
                                                                content: <Icon name="search" />,
                                                                placement: 'right'
                                                            } }
                                                        />
                                                    </div>
                                                    <span className="btnbtn">
                                                        {
                                                            this.state.showDelete &&
                                                            (
                                                                <div>
                                                                    <Button
                                                                        className={ this.state.permission.view ? '' : 'hide' }
                                                                        label={ <span>Sync FacePassID</span> }
                                                                        type="action"
                                                                        onClick={ () => {
                                                                            this.setState({ syncModal: false }, () => {
                                                                                this.setState({ syncModal: true });
                                                                            });
                                                                        } }
                                                                    />
                                                                    <Button
                                                                        className={ this.state.permission.delete ? '' : 'hide' }
                                                                        label={ <span>Delete FacePassID</span> }
                                                                        type="danger"
                                                                        onClick={ () => {
                                                                            this.setState({ showModal: false }, () => {
                                                                                this.setState({ showModal: true });
                                                                            });
                                                                        } }
                                                                    />
                                                                </div>
                                                            )
                                                        }
                                                    </span>
                                                </div>
                                                <div>
                                                    <Table
                                                        data={ deviceList }
                                                        columns={ getDeviceColumns }
                                                        pagination
                                                        selectable
                                                        onSelectionChange={ ({ selected }) => {
                                                            const selectionLength = selected.filter( ( row ) => row ).length;
                                                            this.setState({
                                                                showDelete: selectionLength > 0
                                                            });
                                                        } }
                                                        ref={ ( ref ) => { this.usersTable = ref; } }
                                                        onDataChange={ this.handleTableChanges }
                                                        onPageChange={ ( data ) => getManageUsers({
                                                            page: data + 1,
                                                            perPage: pagination.per_page
                                                        }) }
                                                        onPageSizeChange={ ( data ) => getManageUsers({
                                                            page: 1,
                                                            perPage: data
                                                        }) }
                                                        pageSize={ pagination.per_page }
                                                        pages={ pagination.last_page }
                                                        manual
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>
                        )}
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    load: makeSelectLoad(),
    downloading: makeSelectDownloading(),
    user: makeSelectUser(),
    filterData: makeSelectFilterData(),
    errors: makeSelectNotification(),
    pagination: makeSelectPagination(),
    unlinkUser: makeSelectUnlinkUser(),
    deviceList: makeSelectDevice(),
    person: makeSelectData(),
    page: makeSelectPage()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        viewUserActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ViewUserDetail );
