/* eslint-disable require-jsdoc */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import {
    SET_LOADING,
    SET_LOAD,
    GET_MANAGE_USERS,
    SET_PAGINATION,
    SET_USERS,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_UNLINK_USERS,
    GET_UNLINK_USERS,
    GET_DEVICE,
    SET_DEVICE,
    SET_USER,
    GET_USER,
    DELETE_UNLINKUSER,
    SYNC_USER,
    SET_PAGE,
    LINK_DATA
} from './constants';

import { REINITIALIZE_PAGE } from '../../../App/constants';
import { resetStore } from '../../../App/sagas';

import { Fetch } from '../../../../utils/request';

export function* getManageUsers({ payload }) {
    try {
        yield put({
            type: SET_LOAD,
            payload: true
        });

        const { id } = payload;
        const response = yield call(
                            Fetch,
                            `/user/${id}`,
                            { method: 'GET' }
                        );

        const { ...others } = response;

        yield put({
            type: SET_PAGINATION,
            payload: others
        });

        yield put({
            type: SET_USERS,
            payload: response
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOAD,
            payload: false
        });
    }
}

export function* getUnlinkUsers({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const { page, perPage, search } = payload;
        const params = search ? `unlinked_device_users?page=${page}&per_page=${perPage}&filter[query]=${search}` :
                                `unlinked_device_users?page=${page}&per_page=${perPage}`;
        const response = yield call(
                            Fetch,
                            `/api/face_pass/ra08t/accounts/${params}`,
                            { method: 'GET' }
                        );

        const { data, ...others } = response;

        yield put({
            type: SET_PAGE,
            payload: others
        });

        yield put({
            type: SET_UNLINK_USERS,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* getDevice({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const { page, perPage, search } = payload;
        const params = search ? `devices?page=${page}&per_page=${perPage}&filter[keyword]=${search}` :
                                `devices?page=${page}&per_page=${perPage}`;
        const response = yield call(
                            Fetch,
                            `/api/face_pass/ra08t/${params}`,
                            { method: 'GET' }
                        );

        const { data, ...others } = response;

        yield put({
            type: SET_PAGINATION,
            payload: others
        });

        yield put({
            type: SET_DEVICE,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* getUser({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const { id } = payload;
        const response = yield call(
                            Fetch,
                            `/api/face_pass/ra08t/users/${id}`,
                            { method: 'GET' }
                        );

        const { data, ...others } = response;
        yield put({
            type: SET_PAGINATION,
            payload: others
        });

        yield put({
            type: SET_USER,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* deleteUnlinkUser({ payload }) {
    try {
        const { id, ids } = payload;
        yield call( Fetch, `/api/face_pass/ra08t/users/${id}/devices`, {
            method: 'DELETE',
            data: {
                device_ids: ids
            }
        });
        yield [
            call( notifyUser, {
                show: true,
                title: 'Success',
                message: 'Record successfully deleted',
                type: 'success'
            }),
            call( getUser, { payload: {
                id
            }}),
            call( getDevice, { payload: {
                page: 1,
                perPage: 10
            }

            })
        ];
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

export function* syncUser({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const { id, ids } = payload;
        yield call( Fetch, `/api/face_pass/ra08t/users/${id}/sync`, {
            method: 'POST',
            data: {
                device_ids: ids
            }
        });
        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'sync Successfull',
            type: 'success'
        });
        yield call( getUser, { payload: {
            id
        }});
        yield call( getDevice, { payload: {
            page: 1,
            perPage: 10
        }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield call( notifyUser, error );
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* linkData({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const { id, ids } = payload;
        yield call( Fetch, '/api/face_pass/ra08t/users', {
            method: 'POST',
            data: {
                user_id: ids,
                unlinked_device_user_id: id
            }
        });
        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'sync Successfull',
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, error );
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Watcher for Users
 */
export function* watchForManageUsers() {
    const watcher = yield takeEvery( GET_MANAGE_USERS, getManageUsers );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForUnlinkUsers() {
    const watcher = yield takeEvery( GET_UNLINK_USERS, getUnlinkUsers );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForDevice() {
    const watcher = yield takeEvery( GET_DEVICE, getDevice );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForUser() {
    const watcher = yield takeEvery( GET_USER, getUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForDeleteUnlinkUser() {
    const watcher = yield takeEvery( DELETE_UNLINKUSER, deleteUnlinkUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForSyncUser() {
    const watcher = yield takeEvery( SYNC_USER, syncUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForLinkData() {
    const watcher = yield takeEvery( LINK_DATA, linkData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForReinitializePage,
    watchForManageUsers,
    watchForUnlinkUsers,
    watchForDevice,
    watchForUser,
    watchForDeleteUnlinkUser,
    watchForSyncUser,
    watchForLinkData

];
