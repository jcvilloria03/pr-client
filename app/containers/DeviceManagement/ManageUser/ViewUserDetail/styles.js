import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 200px;
`;

export const PageWrapper = styled.div`
    .loader {
        padding: 140px 0;
    }

    .content {
        .ReactTable{
            .selected{
                background-color: rgba(131, 210, 75, 0.15);
            }
        }
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin-bottom: 0;

            h2 {
                font-weight: 700;
                font-size: 36px;
                margin-top: 30px;
                margin-bottom: 0px;
                line-height: 57.6px;
            }

            h3 {
                font-weight: 700;
                font-size: 32px;
                margin-top: 30px;
                margin-bottom: 0px;
            }

            p {
                text-align: center;
                max-width: 800px;
            }

            .main_heading {
                display: flex;
                width: 70%;
                justify-content: space-evenly;
                margin-top: 50px;
                border-bottom: 1px solid #ccc;
                font-weight: 600;
                padding-bottom: 20px;

                .heading_1 p {
                    text-transform: capitalize;
                    display: flex;
                }
                .name_01 {
                    margin-top: 16px;
                    padding-left: 10px;
                }
            }

            .schdule_tooltip{
                display: flex;
                align-items: center;
                font-size: 14px;
                margin-bottom:5px;
                button{
                    width: 20px;
                    height: 20px;
                    padding: 0;
                    border-radius: 50%;
                    background-color: #F0F4F6;
                    color: #ADADAD;
                }
                a{
                    padding: 0;
                    border-bottom: none;
                    padding-left: 6px;
                }
                .bg_tooltip {
                    position: relative;
                    display: inline-block;
                    width:20px;
                    height:20px;
                    border-radius: 50%;
                    background-color: #F0F4F6;
                    color: #ADADAD;
                    margin-left: 6px;
                    text-align: center;
                    margin-top: 40px;
                    label{
                        font-size: 15px;
                        font-weight: bold;
                    }
                }
                .bg_tooltip .tooltiptext {
                    visibility: hidden;
                    width: 242px;
                    background-color: #fff;
                    color: #03a9f4;
                    border: 2px solid #03a9f4;
                    text-align: left;
                    border-radius: 6px;
                    padding: 5px 16px;
                    position: absolute;
                    left: 26px;
                    top: -50%;
                    transform: translateY(-38%);
                    z-index: 1;
                }
                .bg_tooltip .tooltiptextStart{
                    transform: translateY(-26%);
                }
                .bg_tooltip .tooltiptextEnd{
                    transform: translateY(-20%);
                    width: 262px;
                }
                .bg_tooltip .tooltiptextHours{
                    transform: translateY(-17%);
                    width: 255px;
                }
                .bg_tooltip:hover .tooltiptext {
                    visibility: visible;
                }
                .bg_tooltip .tooltiptext::after {
                    content: " ";
                    position: absolute;
                    top: 50%;
                    right: 99%;
                    margin-top: -5px;
                    border: solid #03a9f4;
                    border-width: 0 2px 2px 0;
                    display: inline-block;
                    padding: 4px;
                    transform: rotate(135deg);
                    background-color: #ffff;
                }
            }
        }

        .tableAction button {
            width: 130px;
        }

        .displayData{
            display: flex;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;
            margin-top: 60px;

            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 600;
                font-size: 18px;
            }
        }

        .table-unlink-users {
            .rt-noData {
                display: block !important;
                position: relative;
                transform: none;
                background-color: white;
                left: 0;
                color: #474747;
                font-size: 14px;
            }
        }

        .tableData{
            display: flex;
            justify-content:space-around;

            .data{
                width: 60%;
            }

            .details{
                margin-left: -196px;

                img{
                    height: 200px;
                    margin-bottom: 20px;
                }
                
                Button{
                    width: 144px;
                }
            }
        }

        .dataTable{
            width: 70%;
            margin: auto;
            .title{
                h6{
                    margin-bottom: 0;
                    margin-right: 10px;
                }
            }
        }
    }
    .search-wrapper {
        flex-grow: 1;

        .search {
            width: 200px;
            border: 1px solid #333;
            border-radius: 30px;

            input {
                border: none;
            }

            input, .input-group {
                height: 42px;
            }
        }

        p {
            display: none;
        }

        .input-group,
        .form-control {
            background-color: transparent;
        }

        .input-group-addon {
            background-color: transparent;
            border: none;
            position: relative;
            top: -2px;
            
            svg {
                height: 14px;
                width: 14px;
            }
        }
        .isvg {
            display: inline-block;
            width: 1rem;
        }
    }

    .leave-types {
        clear:both;
    }

    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }

    .editing {
        input{
            height: 25px;
        }

        p {
            display:none;
        }
    }
`;

export const SectionWrapper = styled.section`
    border-radius: 5px;
    letter-spacing: 0.12px;
    border: solid 1px #adadad;
    background-color: #fff;
    padding: 20px 0;
    margin-bottom: 20px;

    .summary {
        display: flex;
        align-items: center;
        margin: 30px 50px;

        .fa {
            width: 35px;
            margin-right: 20px;
            text-align: center;
        }

        .fa, strong {
            color: #00A4E4;
        }
    }

    .details {
        margin: 0;
        display: flex;
        align-items: flex-start;
        justify-content: space-evenly;

        .licenses {
            flex-grow: 1;

            & > div {
                display: flex;
                justify-content: space-around;

                & > {
                    display: flex;
                    justify-content: center;
                }
            }

            .total {
                margin-top: 20px;
            }
        }
    }
`;

export const ModalBody = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 100%;

    * {
        font-size: 15px;
        font-variant: normal;
        padding: 0;
        margin: 0;
    }

    form {
        width: 100%;
        margin: 20px 0;
    }

    .title {
        display: flex;
        align-items: center;
        margin-bottom: 20px;
        justify-content: space-between;
        border-bottom: solid 2px #d0e1e1;
        margin-top: -15px;

        h3 {
            margin: 0;
            margin-right: 20px;
            margin-bottom: 20px;
            font-size: 15px;
            font-weight: 500
        }
    }

    .fa-icon > svg {
        height: 16px;
    }

    .mr-30 {
        margin-right: 30px;
    }

    .mt-25 {
        margin-top: 25px;
    }

    label {
        color: #6b7c93;
        font-weight: 300;
        letter-spacing: 0.025em;
    }

    input {
        font-weight: 300 !important;
        border: 0 !important;
        color: #31325F !important;
        outline: none !important;
        cursor: text !important;
        width: 100% !important;
        background: white;
        box-shadow: 0 3px 6px 0 rgba(49,49,93,0.10),
                    0 1px 2px 0 rgba(0,0,0,0.08);
        border-radius: 4px;
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .input::-webkit-input-placeholder { color: #CFD7E0; }
    .input::-moz-placeholder { color: #CFD7E0; }

    .address-section {
        margin-top: -20px;

        input {
            font-weight: 300 !important;
            border: 0 !important;
            color: #31325F !important;
            outline: none !important;
            cursor: text !important;
            width: 100% !important;
            background: white;
            box-shadow: 0 3px 6px 0 rgba(49,49,93,0.10),
                        0 1px 2px 0 rgba(0,0,0,0.08);
            border-radius: 4px;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .input::-webkit-input-placeholder { color: #CFD7E0; }
        .input::-moz-placeholder { color: #CFD7E0; }
    }

    .card-section {
        .group {
            background: white;
            box-shadow: 0 3px 6px 0 rgba(49,49,93,0.10),
                        0 1px 2px 0 rgba(0,0,0,0.08);
            border-radius: 4px;
            margin-bottom: 40px;
        }

        #card-element.CardField-cvc {

        }

        input,
        .StripeElement {
            display: block;
            margin: 10px 0 20px 0;
            padding: 17px 14px;
            font-size: 15px;
            box-shadow: 0 3px 6px 0 rgba(49,49,93,0.10),
                        0 1px 2px 0 rgba(0,0,0,0.08);
            border: 0;
            outline: 0;
            border-radius: 4px;
            background: white;
            height: 50px;
        }

        input:focus,
        .StripeElement--focus {
            box-shadow: rgba(50, 50, 93, 0.109804) 0px 4px 6px, rgba(0, 0, 0, 0.0784314) 0px 1px 3px;
            -webkit-transition: all 150ms ease;
            transition: all 150ms ease;
        }


        .card {
            padding-top: 11px !important;
        }

        .field {
            background: transparent;
            font-weight: 300;
            border: 0;
            color: #31325F;
            outline: none;
            flex: 1;
            padding-right: 10px;
            padding-left: 10px;
            cursor: text;
        }

        .card-error {
            -ms-flex-order: 3;
            -webkit-order: 3;
            order: 3;
            color: #f21108;
            font-size: 13px;
            padding-left: 2px;
            margin-bottom: 6px;
            margin-top: -16px !important;
        }

        .field::-webkit-input-placeholder { color: #8898AA; }
        .field::-moz-placeholder { color: #8898AA; }
    }

    button {
        white-space: nowrap;
        border: 0;
        outline: 0;
        display: inline-block;
        height: 40px;
        line-height: 40px;
        padding: 0 14px;
        box-shadow: 0 4px 6px rgba(50, 50, 93, .11), 0 1px 3px rgba(0, 0, 0, .08);
        color: #fff;
        border-radius: 4px;
        font-size: 15px;
        font-weight: 600;
        text-transform: uppercase;
        letter-spacing: 0.025em;
        background-color: #83d24b;
        text-decoration: none;
        -webkit-transition: all 150ms ease;
        transition: all 150ms ease;
        margin-top: 20px;
        width: 100%;
    }

    button:hover {
        color: #fff;
        cursor: pointer;
        background-color: #9ddb70;
        transform: translateY(-1px);
        box-shadow: 0 7px 14px rgba(50, 50, 93, .10), 0 3px 6px rgba(0, 0, 0, .08);
    }

    button:focus {
        background: #9ddb70;
    }

    button:active {
        background: #b9e699;
    }

    button:disabled,
    button[disabled] {
        background-color: #ade188;
        color: #ecf6fa;
        border-color: #ade188;
        opacity: 1;
        cursor: not-allowed;
        transform: none;
        box-shadow: 0;
    }

    .confirm-terms {
        font-size: 12px;
        font-weight: 550;
        color: gray;
        margin-top: 10px;
        text-align: center;
    }

    .payment-card-view {
        font-size: 14px;
        text-transform: uppercase;
        margin: 5px;

        .value {
            font-weight: 600;
        }

        .text-lc {
            font-weight: 500 !important;
            text-transform: lowercase !important;
        }
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    justify-content: center;
    min-height: 100%;
`;

export const SpinnerWrapper = styled.span`
    padding: 0 20px;
`;
