/* eslint-disable import/first */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-curly-spacing */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React from 'react';

import Button from 'components/Button';
import MultiSelect from 'components/MultiSelect';
import Select from 'components/Select';

import { FilterWrapper } from './styles';
import { filterUserStatus, filterFaceStatus } from './constants';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {
    static propTypes = {
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func,
        companies: React.PropTypes.array
    };

    constructor( props ) {
        super( props );
        this.state = {
            status: 'active',
            companyList: [],
            selectedCompanyIds: [],
            selectedFaceStatus: ''
        };

        this.onApply = this.onApply.bind( this );
    }

    componentWillReceiveProps( nextProps ) {
        const companyAll = {
            label: 'All',
            value: 'all'
        };

        const companyList = nextProps.companies.map( ( company ) => ({
            label: company.name,
            value: company.id
        }) );

        this.setState({ companyList: [ companyAll, ...companyList ]});
    }

    onApply = () => {
        const filters = {
            userStatus: this.state.status,
            companyList: this.state.selectedCompanyIds,
            faceStatus: this.state.selectedFaceStatus
        };

        this.props.onApply( filters );
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    // eslint-disable-next-line react/sort-comp
    resetFilters = () => {
        this.setState({
            status: 'active',
            selectedCompanyIds: [],
            selectedFaceStatus: ''
        });
    }

    formatDataForMultiselect = ( data ) => (
        {
            value: data.name,
            label: data.label,
            disabled: false
        }
    )

    handleCompanySelect = ( selected ) => {
        const { companyList } = this.state;

        const selectedAll = selected.find( ( s ) => s.value === 'all' );

        if ( selectedAll ) {
            const ids = companyList.map( ( c ) => c.value ).filter( ( id ) => id !== 'all' );
            this.companyListSelect.value = ids;
            this.setState({ selectedCompanyIds: ids });
        } else {
            const ids = selected.map( ( s ) => s.value );
            this.setState({ selectedCompanyIds: ids });
        }
    }

    render() {
        const { status, companyList, selectedCompanyIds, selectedFaceStatus } = this.state;

        return (
            <FilterWrapper>
                <div className="filterMain">
                    <div className="row mb-0 filterRow approvalMain" key="1">
                        <div className="col-xs-4 colSect">
                            <Select
                                id="userStatus"
                                label="User Status"
                                value={status}
                                onChange={ ( st ) => {
                                    this.setState({
                                        status: st.value
                                    });
                                } }
                                data={ filterUserStatus }
                                ref={ ( ref ) => { this.userStatus = ref; } }
                            />
                        </div>
                        <div className="col-xs-4 colSect selCompany">
                            <MultiSelect
                                id="companyListSelect"
                                label="Companies"
                                data={ companyList }
                                value={ selectedCompanyIds }
                                placeholder="Choose Company"
                                onChange={ this.handleCompanySelect }
                                ref={ ( ref ) => { this.companyListSelect = ref; } }
                            />
                        </div>
                        <div className="col-xs-4 colSect">
                            <Select
                                id="faceStatus"
                                label="Registered Face Status"
                                placeholder="Select Registered Face Status"
                                value={selectedFaceStatus}
                                onChange={ ( st ) => {
                                    this.setState({
                                        selectedFaceStatus: st.value
                                    });
                                }}
                                data={ filterFaceStatus }
                                ref={ ( ref ) => { this.faceStatus = ref; } }
                            />
                        </div>
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ () => this.resetFilters() }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ () => this.onCancel() }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ () => this.onApply() }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
