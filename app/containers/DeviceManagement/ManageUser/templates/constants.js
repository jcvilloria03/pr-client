export const FILTER_TYPES = {
    USERS_STATUS: 'userStatus',
    COMPANY: 'companyList',
    FACE_STATUS: 'faceStatus'
};

export const filterData = {
    userStatus: [
        { label: 'Active', name: 'active' },
        { label: 'Semi-Active', name: 'semi-active' },
        { label: 'Inactive', name: 'inactive' }
    ],
    companyList: [
        { label: 'All', name: 'all' },
        { label: 'BBB Company', name: 'bbb-company' }
    ],
    faceStatus: [
        { label: 'Yes', name: true },
        { label: 'No', name: false }
    ]
};

export const filterUserStatus = [
    {
        label: 'Active',
        value: 'active'
    },
    {
        label: 'Semi-Active',
        value: 'semi-active'
    },
    {
        label: 'Inactive',
        value: 'inactive'
    }
];

export const filterFaceStatus = [
    {
        label: 'Yes',
        value: 'true'
    },
    {
        label: 'No',
        value: 'false'
    }
];
