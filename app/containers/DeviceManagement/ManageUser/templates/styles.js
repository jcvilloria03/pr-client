import styled from 'styled-components';

export const FilterWrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    margin-bottom: 42px;
    border: 1px solid #ccc;
    border-radius: .5rem;

    &:before,
    &:after {
        position: absolute;
        right: 9.5rem;
        display: block;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 0 10px 10px 10px;
        content: '';
    }

    &:before {
        top: -10px;
        border-color: transparent transparent #ccc transparent;
    }
    
    &:after {
        top: -9px;
        z-index: 2;
        border-color: transparent transparent #fff transparent;
    }
    .filterMain{
        padding: 14px 14px 28px 14px;
    }
    .sl-c-filter-actions {
        display: flex;
        justify-content: space-between;
        border-top: 1px solid lightgrey;
        padding-top: 20px;
        align-items: center;
        padding: 6px 14px;

        .sl-c-filter-reset > .btn {
            margin-left: 0;
            color: #00A5E5;
            cursor: pointer;
            border: none;
        }

        .sl-c-filter-buttons > .btn {
            padding: .5rem 1.5rem;
        }
    }
    .date-picker {
        .DayPickerInput {
            width: 100%;
        }
        span {
            display: block;
        }
        input {
            width: 100%;
            padding-top: 0px !important;
            border: 1px solid #474747;
        }
    }
    .filterRow{
        padding: 0px 7px;
        .colSect{
            padding: 0px 7px;
            .Select-control{
                border: 1px solid #474747;
                color: #adadad;
                .Select-value-label{
                    color: #adadad;
                }
            }
        }
        .selCompany{
            label {
                line-height: 24px;
            }
        }
    }
    .approvalMain{
        display: flex;
    }
    .fontBlue{
        color: #00A5E5;
        font-size: 14px;
        padding-left: 30px;
    }
`;
