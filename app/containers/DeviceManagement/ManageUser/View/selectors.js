import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectManageUserView = () => ( state ) => state.get( 'deviceManagement' );

const makeSelectLoading = () => createSelector(
    selectManageUserView(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectDownloading = () => createSelector(
    selectManageUserView(),
    ( substate ) => substate.get( 'downloading' )
);

const makeSelectUsers = () => createSelector(
    selectManageUserView(),
  ( substate ) => substate && substate.get( 'manageUser' ).toJS()
);

const makeSelectFilterData = () => createSelector(
    selectManageUserView(),
  ( substate ) => substate.get( 'filterData' ).toJS()
);

const makeSelectPagination = () => createSelector(
    selectManageUserView(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectManageUserView(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectManageUserList = () => createSelector(
  selectManageUserView(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectManageUserList,
  makeSelectLoading,
  makeSelectDownloading,
  makeSelectUsers,
  makeSelectFilterData,
  makeSelectNotification,
  makeSelectPagination
};
