/* eslint-disable react/no-unused-prop-types */
/* eslint-disable import/first */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { debounce } from 'lodash';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import { makeSelectIsExpired, makeSelectCompaniesState } from 'containers/App/selectors';

import { H3, H5, P } from 'components/Typography';
import Input from 'components/Input';
import Icon from 'components/Icon';
import Button from 'components/Button';
import Table from 'components/Table';
import SubHeader from 'containers/SubHeader';
import Sidebar from 'components/Sidebar';
import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';

import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';
import { formatPaginationLabel } from 'utils/functions';
import { DEVICE_MANAGEMENT_SUBHEADER_ITEMS } from 'utils/constants';

import Filter from '../templates/filterList';

import {
    makeSelectLoading,
    makeSelectUsers,
    makeSelectFilterData,
    makeSelectNotification,
    makeSelectDownloading,
    makeSelectPagination
} from './selectors';
import * as manageUserActions from './actions';

import {
    PageWrapper
} from './styles';

/**
 *
 * DeviceManagement
 *
 */
export class View extends React.Component {
    static propTypes = {
        getUsers: React.PropTypes.func,
        getManageUsers: React.PropTypes.func,
        users: React.PropTypes.array,
        pagination: React.PropTypes.object,
        filterData: React.PropTypes.shape({
            loanTypes: React.PropTypes.array,
            positions: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        notificationList: React.PropTypes.func,
        isExpired: React.PropTypes.bool,
        companies: React.PropTypes.array
    }

    static defaultProps = {
        pagination: {
            from: 1,
            to: 1,
            total: 1,
            current_page: 1,
            last_page: 1,
            per_page: 10
        }
    };

    constructor( props ) {
        super( props );

        this.state = {
            stripe: null,
            subscription: null,
            license_to_purchase: {},
            salpayViewPermission: false,
            hasPaymentMethod: false,
            showConfirmModal: false,
            label: 'Showing 0-0 of 0 entries',
            showFilter: false,
            displayedData: this.props.users,
            permission: {
                view: true
            },
            filters: {
                userStatus: 'active',
                companyList: [],
                faceStatus: ''
            },
            hasFiltersApplied: false,
            tablePayslipsPage: 0,
            tablePayslipsPageSize: 10,
            orderBy: 'full_name',
            orderDir: 'asc',
            tablePageSize: 10,
            search: ''
        };

        this.searchInput = null;
        this.handleSearch = this.handleSearch.bind( this );
        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
    }

    componentWillMount() {
        this.props.getManageUsers( this.state.filters );
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.users !== this.props.users
            && this.setState({
                displayedData: nextProps.users
            });

        // Set table details label
        if ( nextProps.pagination.total ) {
            const page = nextProps.pagination.current_page || 0;
            const size = nextProps.pagination.per_page || 0;
            const dataLength = nextProps.pagination.total || 0;

            this.setState({
                label: `Showing ${dataLength ? ( ( page * size ) + 1 ) - size : 0} - ${( page * size ) > dataLength ? dataLength : ( page * size )} of ${dataLength} entr${dataLength > 1 || dataLength === 0 ? 'ies' : 'y'}`
            });
        }
    }

    // eslint-disable-next-line react/sort-comp
    toggleFilter = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    resetFilters = () => {
        this.setState({
            showFilter: false
        }, this.props.getManageUsers(
            { filters: { userStatus: 'active',
                faceStatus: '' }
            }
        ) );
    }

    handleTableChanges( tableProps = this.usersTable.tableComponent.state ) {
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    onPageChange( page ) {
        if ( this.props.loading ) {
            return;
        }

        this.setState({ tablePage: page });
        this.usersTable.tableComponent.setState({ page });
        this.props.getManageUsers(
            this.state.filters,
            page,
            this.state.tablePageSize,
            this.state.search
        );
    }

    onPageSizeChange( pageSize ) {
        if ( this.props.loading ) {
            return;
        }

        this.setState({ tablePageSize: pageSize });
        this.usersTable.tableComponent.setState({ pageSize });

        this.props.getManageUsers(
            this.state.filters,
            1,
            pageSize,
            this.state.searchTerm
        );
    }

    handleSearch = debounce( ( term = '' ) => {
        this.setState({
            search: term.trim()
        });
        if ( this.state.search ) {
            this.props.getManageUsers(
                this.state.filters,
                1,
                this.state.tablePageSize,
                this.state.search
            );
        } else {
            this.props.getManageUsers(
                this.state.filters,
                1,
                this.state.tablePageSize
            );
        }
    }, 600 )

    applyFilters = ( filters ) => {
        this.setState({
            filters
        }, () => {
            this.props.getManageUsers( filters );
        });
    }

    /**
     *
     * DeviceManagement render method
     *
     */
    render() {
        const {
            salpayViewPermission,
            label,
            showFilter
        } = this.state;
        const {
            products,
            isExpired,
            companies
        } = this.props;

        const getTableColumns = [
            {
                id: 'id',
                show: false
            },
            {
                id: 'name',
                header: 'Name',
                sortable: false,
                minWidth: 180,
                render: ({ row }) => (
                    <div>
                        { row.full_name }
                    </div>
                    )
            },
            {
                id: 'userRole',
                header: 'User role',
                sortable: false,
                minWidth: 180,
                render: ({ row }) => (
                    <div>
                        { row.crbac_role ? row.crbac_role.name : '' }
                    </div>
                    )
            },
            {
                id: 'employeeOf',
                header: 'Employee Of',
                sortable: false,
                minWidth: 180,
                render: ({ row }) => (
                    <div>
                        { row.companies.map( ( data ) => data.name ).join( ', ' ) }
                    </div>
                    )
            },
            {
                id: 'withRegisteredFace',
                header: 'With Registered Face',
                sortable: false,
                width: 160,
                render: ({ row }) => (
                        row.with_registered_face === false ?
                            <div>
                            false
                        </div> :
                        <div>true</div>
                    )
            },
            {
                header: ' ',
                width: 120,
                sortable: false,
                style: { justifyContent: 'end' },
                render: ({ row }) => (
                    <Button
                        id={ `button-view-users-${row.id}` }
                        label={ <span>View</span> }
                        type="grey"
                        size="small"
                        onClick={ () => browserHistory.push( `/control-panel/device-management/users/${row.id}`, true ) }
                    />
                    )
            }
        ];
        return (
            <div>
                <Helmet
                    title="Device Management"
                    meta={ [
                        { name: 'description', content: 'Description of DeviceManagement' }
                    ] }
                />
                <SubHeader
                    items={ DEVICE_MANAGEMENT_SUBHEADER_ITEMS }
                />
                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        isExpired,
                        accountViewPermission: true,
                        salpayViewPermission,
                        isSubscribedToPayroll: products && subscriptionService.isSubscribedToPayroll( products )
                    }) }
                />
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="heading">
                                <H3>Manage Users</H3>
                                <P noBottomMargin>This module allows you to manage registered users FacePassID. Link Registered face into a Salarium User, sync registered FacePassID into other devices, or delete user FacePassID in both Salarium and the device.</P>
                            </div>
                            <div className="title">
                                <H5>User Name</H5>
                                <div className="search-wrapper">
                                    <Input
                                        id="search"
                                        className="search"
                                        onChange={ ( value ) => this.handleSearch( value.toLowerCase() ) }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                    />
                                </div>
                                <span>
                                    <div className="filterGroup">
                                        {label} &nbsp;&nbsp;
                                        <Button
                                            id="button-filter"
                                            label={ <span><Icon name="filter" className="filter-icon" /> Filter</span> }
                                            onClick={ this.toggleFilter }
                                            className="btnFilter"
                                        />
                                    </div>
                                </span>
                            </div>
                            <div style={ { display: showFilter ? 'block' : 'none' } }>
                                <Filter
                                    companies={ companies }
                                    onCancel={ () => { this.resetFilters(); } }
                                    onApply={ ( filters ) => { this.applyFilters( filters ); } }
                                />
                            </div>
                            <Table
                                data={ this.props.users }
                                columns={ getTableColumns }
                                pages={ this.props.pagination.last_page || 1 }
                                pageSize={ this.props.pagination.per_page }
                                page={ 0 }
                                onDataChange={ this.handleTableChanges }
                                loading={ this.props.loading }
                                ref={ ( ref ) => { this.usersTable = ref; } }
                                external
                            />
                            <div>
                                <FooterTablePaginationV2
                                    page={ this.props.pagination.current_page || 1 }
                                    pageSize={ this.state.tablePageSize || 10 }
                                    pagination={ this.props.pagination }
                                    onPageChange={ this.onPageChange }
                                    onPageSizeChange={ this.onPageSizeChange }
                                    paginationLabel={ this.state.tableLabel }
                                    fluid
                                />
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    downloading: makeSelectDownloading(),
    users: makeSelectUsers(),
    filterData: makeSelectFilterData(),
    notification: makeSelectNotification(),
    pagination: makeSelectPagination(),
    isExpired: makeSelectIsExpired(),
    companies: makeSelectCompaniesState()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        manageUserActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
