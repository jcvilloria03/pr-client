/* eslint-disable require-jsdoc */
// /* eslint-disable require-jsdoc */

import {
    GET_MANAGE_USERS,
    NOTIFICATION
} from './constants';

/**
 * Fetch list of Users Manager
 */
export function getManageUsers({
    userStatus = '',
    faceStatus = '',
    companyList = []
}, page = 1, perPage = 10, search = '' ) {
    return {
        type: GET_MANAGE_USERS,
        payload: {
            filter: {
                userStatus,
                faceStatus,
                companyList
            },
            page,
            perPage,
            search
        }
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
