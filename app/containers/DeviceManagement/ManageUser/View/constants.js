// /*
//  *
//  * DeviceManagement constants
//  *
//  */

export const SET_LOADING = 'app/DeviceManagement/SET_LOADING';
export const GET_DATA = 'app/DeviceManagement/GET_DATA';
export const GET_MANAGE_USERS = 'app/DeviceManagement/GET_MANAGE_USERS';
export const SET_PAGINATION = 'app/DeviceManagement/SET_PAGINATION';
export const SET_DATA = 'app/DeviceManagement/SET_DATA';
export const SET_FILTER_DATA = 'app/DeviceManagement/SET_FILTER_DATA';
export const NOTIFICATION = 'app/DeviceManagement/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/DeviceManagement/NOTIFICATION_SAGA';
