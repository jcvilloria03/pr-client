import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_DATA,
    SET_PAGINATION,
    SET_FILTER_DATA,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../../App/constants';

const initialState = fromJS({
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    manageUser: [],
    filterData: {},
    pagination: {
        from: 1,
        to: 1,
        total: 1,
        current_page: 1,
        last_page: 1,
        per_page: 10
    }
});

/**
 * Manage User view reducer
 */
function usersReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_DATA:
            return state.set( 'manageUser', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default usersReducer;
