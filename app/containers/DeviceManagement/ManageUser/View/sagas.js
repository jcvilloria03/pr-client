/* eslint-disable require-jsdoc */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import {
    SET_LOADING,
    GET_MANAGE_USERS,
    SET_PAGINATION,
    SET_DATA,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';

import { REINITIALIZE_PAGE } from '../../../App/constants';
import { resetStore } from '../../../App/sagas';

import { Fetch } from '../../../../utils/request';
import { company } from '../../../../utils/CompanyService';

/**
 * getManager Data
 */
export function* getManageUsers({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const {
            filter,
            page = 1,
            perPage = 10,
            search = ''
        } = payload;

        const companyId = company.getLastActiveCompanyId();
        const {
            userStatus = 'active',
            faceStatus = '',
            companyList = [companyId]
        } = filter;

        let params = `users?page=${page}&per_page=${perPage}&filter[user_status]=${userStatus}`;
        if ( search !== '' ) {
            params += `&filter[user_name_keyword]=${search}`;
        }
        if ( faceStatus !== '' ) {
            params += `&filter[has_faceid]=${faceStatus}`;
        }
        if ( companyList.length > 0 ) {
            companyList.forEach( ( id ) => {
                params += `&filter[company_ids][]=${id}`;
            });
        }

        const response = yield call( Fetch, `/account/${params}`, { method: 'GET' });
        const { data, per_page, ...others } = response;

        yield put({
            type: SET_PAGINATION,
            payload: { per_page: Number( per_page ), ...others }
        });
        yield put({
            type: SET_DATA,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getManageUsers );
}

/**
 * Watcher for users
 */
export function* watchForManageUsers() {
    const watcher = yield takeEvery( GET_MANAGE_USERS, getManageUsers );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForReinitializePage,
    watchForManageUsers
];
