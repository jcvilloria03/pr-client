/* eslint-disable no-confusing-arrow */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/img-has-alt */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable import/first */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import isEqual from 'lodash/isEqual';
import { createStructuredSelector } from 'reselect';
import SubHeader from 'containers/SubHeader';
import Sidebar from 'components/Sidebar';
import Table from 'components/Table';
import Input from 'components/Input';
import Icon from 'components/Icon';
import Button from 'components/Button';
import {
    DEVICE_MANAGEMENT_SUBHEADER_ITEMS
} from 'utils/constants';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectPagination,
    makeSelectUnlinkUser
} from './selectors';
import { bindActionCreators } from 'redux';
import * as unlinkUserActions from './actions';
import {
    PageWrapper,
    ConfirmBodyWrapperStyle,
    LoadingStyles
} from './styles';
import { Container } from 'reactstrap';
import { H1, H2, H3, H6 } from 'components/Typography';
import { formatPaginationLabel } from 'utils/functions';
import SalConfirm from 'components/SalConfirm';
import { makeSelectIsExpired } from 'containers/App/selectors';

/**
 *
 * View
 *
 */
export class ManageUnlinkUser extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        loading: React.PropTypes.bool,
        params: React.PropTypes.object,
        products: React.PropTypes.array,
        pagination: React.PropTypes.object,
        unlinkUser: React.PropTypes.array,
        getUnlinkUsers: React.PropTypes.func,
        deleteUnlinkDeviceUser: React.PropTypes.func,
        isExpired: React.PropTypes.bool
    }

    constructor( props ) {
        super( props );

        this.state = {
            subscription: null,
            salpay_View_Permission: false,
            displayedData: this.props.unlinkUser,
            selectedRow: {},
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            showDetails: false
        };
        this.searchInput = null;
        this.handleSearch = this.handleSearch.bind( this );
        this.handleTableChanges = this.handleTableChanges.bind( this );
    }

    componentDidMount() {
        const data = {
            page: 1,
            perPage: 10
        };
        this.props.getUnlinkUsers( data );
    }

    componentWillReceiveProps( nextProps ) {
        if ( !this.props.unlinkUser.length || !isEqual( nextProps.users, this.props.unlinkUser ) ) {
            this.handleSearch( nextProps.users );
        }
    }

    deleteUnlinkUser() {
        const id = this.state.selectedRow.id;
        this.props.deleteUnlinkDeviceUser( id );
        this.setState({ showDelete: false });
    }

    handleTableChanges( tableProps = this.usersTable.tableComponent.state ) {
        Object.assign( tableProps, { dataLength: this.props && this.props.pagination.total });
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    handleSearch() {
        let searchQuery = null;
        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }
        if ( searchQuery ) {
            const data = {
                page: 1,
                perPage: 10,
                search: searchQuery
            };
            this.props.getUnlinkUsers( data );
        }
    }

    /**
     *
     * View render method
     *
     */
    render() {
        const {
            salpay_View_Permission,
            selectedRow,
            showDetails
        } = this.state;
        const {
            products,
            pagination,
            getUnlinkUsers,
            isExpired
        } = this.props;
        const getTableColumns = [
            {
                id: 'user',
                minWidth: 100,
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.user_name}
                    </div>
                )
            },
            {
                id: 'person_id',
                minWidth: 100,
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.person_id}
                    </div>
                )
            },
            {
                id: 'device',
                minWidth: 100,
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.device_name}
                    </div>
                )
            },
            {
                id: 'userSelect',
                header: ' ',
                minWidth: 50,
                sortable: false,
                render: ({ row }) => (
                    <Button
                        id={ `button-view-payroll-${row.id}` }
                        label={ <span>View</span> }
                        type="grey"
                        size="small"
                        onClick={ () => this.setState({ selectedRow: row, showDetails: true }) }
                    />
                )
            }
        ];
        return (
            <div>
                <Helmet
                    title="View"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SubHeader items={ DEVICE_MANAGEMENT_SUBHEADER_ITEMS } />
                <SalConfirm
                    onConfirm={ () => this.deleteUnlinkUser() }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                            Deleting this user will erase all its user records from the device. The device will no longer be able to recognize the user once the record is deleted. Do you want to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Unlinked User"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <PageWrapper>
                    <Sidebar
                        items={ getControlPanelSidebarLinks({
                            isExpired,
                            accountViewPermission: true,
                            salpayViewPermission: salpay_View_Permission,
                            isSubscribedToPayroll: products && subscriptionService.isSubscribedToPayroll( products )
                        }) }
                    />
                    <Container>
                        {this.props.loading ? (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading unlinked Users ...</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        ) : (
                            <div>
                                <div className="content">
                                    <div className="heading">
                                        <H1>Manage Unlinked Users</H1>
                                        <p>You can manage the list of unlinked users through this page.</p>
                                    </div>
                                    <div>
                                        <div className={ showDetails === true ? 'title2' : 'title' }>
                                            <H6>Unlinked Users List</H6>
                                            <div className="search-wrapper">
                                                <Input
                                                    className="search"
                                                    id="search"
                                                    ref={ ( ref ) => { this.searchInput = ref; } }
                                                    onKeyPress={ ( e ) => {
                                                        e.charCode === 13 && this.handleSearch();
                                                    } }
                                                    addon={ {
                                                        content: <Icon name="search" />,
                                                        placement: 'right'
                                                    } }
                                                />
                                            </div>
                                        </div>
                                        <div className={ showDetails === true ? 'tableData' : 'dataTable' }>
                                            <div className="data">
                                                <Table
                                                    className="table-unlink-users"
                                                    data={ this.props.unlinkUser }
                                                    columns={ getTableColumns }
                                                    pagination={ this.props.unlinkUser.length > 0 }
                                                    ref={ ( ref ) => { this.usersTable = ref; } }
                                                    onDataChange={ this.handleTableChanges }
                                                    onPageChange={ ( data ) => getUnlinkUsers({
                                                        page: data + 1,
                                                        perPage: pagination.per_page
                                                    }) }
                                                    onPageSizeChange={ ( data ) => getUnlinkUsers({
                                                        page: 1,
                                                        perPage: data
                                                    }) }
                                                    pageSize={ pagination.per_page }
                                                    pages={ pagination.last_page }
                                                    manual
                                                    noDataText="No Data"
                                                />
                                            </div>
                                            <div className="details">
                                                {showDetails === true &&
                                                    <div>
                                                        <img src={ selectedRow.s3_uri } /><br />
                                                        <lable>Name : {selectedRow.user_name}</lable><br />
                                                        <lable>PersonID : {selectedRow.person_id}</lable><br />
                                                        <lable>Device : {selectedRow.device_name}</lable><br />
                                                        <Button
                                                            className={ this.state.permission.delete ? '' : 'hide' }
                                                            label={ <span>Delete Unlink User</span> }
                                                            type="danger"
                                                            onClick={ () => {
                                                                this.setState({ showModal: false }, () => {
                                                                    this.setState({ showModal: true });
                                                                });
                                                            } }
                                                        />
                                                    </div>
                                                    }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    pagination: makeSelectPagination(),
    unlinkUser: makeSelectUnlinkUser(),
    isExpired: makeSelectIsExpired()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        unlinkUserActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ManageUnlinkUser );
