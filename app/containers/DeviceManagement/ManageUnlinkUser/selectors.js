
import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectViewUnlinkUsers = () => ( state ) => state.get( 'unlinkUser' );

const makeSelectLoading = () => createSelector(
    selectViewUnlinkUsers(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectPagination = () => createSelector(
    selectViewUnlinkUsers(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewUnlinkUsers(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectUnlinkUser = () => createSelector(
    selectViewUnlinkUsers(),
  ( substate ) => substate && substate.get( 'unlinkUser' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectNotification,
  makeSelectPagination,
  makeSelectUnlinkUser
};
