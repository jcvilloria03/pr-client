// /*
//  *
//  * ManageUnlinkUser constants
//  *
//  */

export const SET_LOADING = 'app/DeviceManagement/SET_LOADING';
export const SET_PAGINATION = 'app/DeviceManagement/SET_PAGINATION';
export const NOTIFICATION = 'app/DeviceManagement/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/DeviceManagement/NOTIFICATION_SAGA';
export const SET_UNLINK_USERS = 'app/DeviceManagement/SET_UNLINK_USERS';
export const GET_UNLINK_USERS = 'app/DeviceManagement/GET_UNLINK_USERS';
export const DELETE_DEVICE_USERS = 'app/DeviceManagement/DELETE_DEVICE_USERS';
