/* eslint-disable require-jsdoc */

import {
    NOTIFICATION,
    GET_UNLINK_USERS,
    DELETE_DEVICE_USERS
} from './constants';

/**
 * Fetch list of Unlink Users
 */

export function getUnlinkUsers( payload ) {
    return {
        type: GET_UNLINK_USERS,
        payload
    };
}

export function deleteUnlinkDeviceUser( payload ) {
    return {
        type: DELETE_DEVICE_USERS,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
