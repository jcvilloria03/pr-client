
import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_PAGINATION,
    NOTIFICATION_SAGA,
    SET_UNLINK_USERS
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    pagination: {
        current_page: 0,
        from: 0,
        last_page: 0,
        per_page: 0,
        to: 0,
        total: 0
    },
    unlinkUser: []
});

/**
 * Unlink Users reducer
 */
function unlinkUsersReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_UNLINK_USERS:
            return state.set( 'unlinkUser', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default unlinkUsersReducer;
