/* eslint-disable require-jsdoc */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import {
    SET_LOADING,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_UNLINK_USERS,
    GET_UNLINK_USERS,
    SET_PAGINATION,
    DELETE_DEVICE_USERS
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { Fetch } from '../../../utils/request';

export function* getUnlinkUsers({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const { page, perPage, search } = payload;
        const params = search ? `unlinked_device_users?page=${page}&per_page=${perPage}&filter[query]=${search}` :
                                `unlinked_device_users?page=${page}&per_page=${perPage}`;
        const response = yield call(
                            Fetch,
                            `/api/face_pass/ra08t/accounts/${params}`,
                            { method: 'GET' }
                        );

        const { data, ...others } = response;

        yield put({
            type: SET_PAGINATION,
            payload: others
        });

        yield put({
            type: SET_UNLINK_USERS,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* deleteUnlinkDeviceUser( payload ) {
    try {
        yield call( Fetch, '/api/face_pass/ra08t/accounts/unlinked_device_users', {
            method: 'DELETE',
            data: {
                unlinked_user_ids: [payload.payload]
            }
        });
        yield [
            call( notifyUser, {
                show: true,
                title: 'Success',
                message: 'Record successfully deleted',
                type: 'success'
            }),
            call( getUnlinkUsers, {
                page: 1,
                perPage: 10
            })
        ];
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Watcher for Unlink Users
 */

export function* watchForUnlinkUsers() {
    const watcher = yield takeEvery( GET_UNLINK_USERS, getUnlinkUsers );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForDeleteDeviceUser() {
    const watcher = yield takeEvery( DELETE_DEVICE_USERS, deleteUnlinkDeviceUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForReinitializePage,
    watchForUnlinkUsers,
    watchForDeleteDeviceUser

];
