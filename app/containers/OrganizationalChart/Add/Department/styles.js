import styled from 'styled-components';

export const Header = styled.div`
    .nav {
        padding:10px 10px;
        background:#f0f4f6;
        margin-bottom:10px;
        position: fixed;
        top: 76px;
        right: 0;
        left: 0;
        width: 100%;
    }
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 6vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

export const ModalAction = styled.div`
    font-size:14px;
    padding-bottom:16px;

`;
export const PageWrapper = styled.div`
    .add-dpt-container{
        margin-top: 160px;
    }
    .loader {
        padding: 140px 0;
    }
    .nav{
        padding:10px 10px;
        background:#f0f4f6;
        margin-bottom:10px;
        padding-top:60px;
        width: 100%;
        position: fixed;
        top: 25px;
        right: 0;
        left: 0;
        z-index: 9;

        .icon-arrow {
            width: 14px;
            font-size: 14px;
            display: inline-block;
            margin-right: 4px;

            > i {
                align-self: center;
            }
        }

        .back-text {
            font-size: 14px;
        }
    }
    .main_container{
        min-width: 80vw;
        margin:auto;
        height:100%;
        margin-bottom: 250px;
        padding-bottom: 80px;
        .schdule_tooltip{
            display: flex;
            align-items: center;
            font-size: 14px;
            margin-bottom:0px;
            color:#5b5b5b;
            button{
                width: 20px;
                height: 20px;
                padding: 0;
                border-radius: 50%;
                background-color: #F0F4F6;
                color: #ADADAD;
            }
            a{
                padding: 0;
                border-bottom: none;
                padding-left: 6px;
            }
            .bg_tooltip {
                position: relative;
                display: inline-block;
                width:20px;
                height:20px;
                border-radius: 50%;
                background-color: #F0F4F6;
                color: #ADADAD;
                margin-left: 6px;
                text-align: center;
                p{
                    font-size: 15px;
                    font-weight: bold;
                    color: #adadad;
                }
            }
    
            .bg_tooltip .tooltiptext {
                visibility: hidden;
                width: 242px;
                background-color: #fff;
                color: #03a9f4;
                border: 2px solid #b1dcf0;
                text-align: left;
                border-radius: 6px;
                padding: 5px 16px;
                position: absolute;
                left: 26px;
                top: -50%;
                transform: translateY(-15%);
                z-index: 1;
            }
            .bg_tooltip .tooltiptextStart{
                transform: translateY(-26%);
            }
    
            .bg_tooltip .tooltiptextEnd{
                transform: translateY(-20%);
                width: 262px;
            }
            .bg_tooltip .tooltiptextHours{
                transform: translateY(-17%);
                width: 255px;
            }
            .bg_tooltip:hover .tooltiptext {
                visibility: visible;
            }
            .bg_tooltip .tooltiptext::after {
                content: " ";
                position: absolute;
                top: 50%;
                right: 99%;
                margin-top: -5px;
                border: solid #b1dcf0;
                border-width: 0 2px 2px 0;
                display: inline-block;
                padding: 4px;
                transform: rotate(135deg);
                background-color: #ffff;
            }
        }
        .dept-content{
            margin-bottom:70px !important;
        }

        .content {
            margin: 40px 0;
            padding: 0 20px;
            width: 100%;

            .heading {
                display: flex;
                align-items: center;
                flex-direction: column;
                margin: 0 0 30px ;
                width:100%;

                h3 {
                    font-weight: bold;
                    font-size: 36px;
                }

                p {
                    text-align: center;
                    max-width: 800px;
                    font-size:14px;
                }

                a {
                    text-decoration: underline !important;
                }
            }

            .expired {
                width: 100%;
                text-align: center;
            }

            .header {
                font-weight: 600;
            }

            .department-item,
            .department-inputs,
            .department-display-header {
                display: grid;
                grid-template-columns: 1fr 1fr 1fr 185px;
                gap: 14px;
                font-size: 14px;
                padding: 0 7px;

                .department-btns {
                    button {
                        svg {
                            width: 10px;
                        }
                    }

                    .add-btn {
                        width: 100%;
                    }
                }
            }

            .department-display-header {
                margin-bottom: 28px;
            }

            .department-inputs {
                align-items: start;

                .department-number {
                    padding-top: 36px;
                }

                .department-btns {
                    padding-top: 26px;
                }
            }

            .department-item + .department-item {
                margin-top: 14px;
            }
        }

        input:focus~label,.Select.is-focused~label{
            color:#5b5b5b !important;
        }
    }
    .invalid_content{
        color:#eb7575;
        input{
            border-color: #eb7575 !important;
        }
        span{
            color:#eb7575;
        }
        label{
            color: #eb7575 !important;
        }
    }
`;

