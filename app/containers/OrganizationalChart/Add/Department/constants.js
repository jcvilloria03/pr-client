/*
 *
 * AddDepartment constants
 *
 */

export const namespace = 'app/containers/OrganizationalChart/Add/Department';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const BTN_LOADING = `${namespace}/BTN_LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;

export const CREATE_DEPARTMENT = `${namespace}/CREATE_DEPARTMENT`;
export const SET_NEW_DEPARTMENT = `${namespace}/SET_NEW_DEPARTMENT`;

export const CREATE_BULKCREATE = `${namespace}/CREATE_BULKCREATE`;
export const CREATE_BULKUPDATE = `${namespace}/CREATE_BULKUPDATE`;

export const SET_BULK_CREATE = `${namespace}/SET_BULK_CREATE`;
export const SET_BULK_UPDATE = `${namespace}/SET_BULK_UPDATE`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const SET_ERRORS = `${namespace}/SET_ERRORS`;

export const SET_DEPARTMENT = `${namespace}/SET_DEPARTMENT`;
export const GET_DEPARTMENT = `${namespace}/GET_DEPARTMENT`;
