import { createSelector } from 'reselect';

/**
 * Direct selector to the addDepartment state domain
 */
const selectAddDepartmentDomain = () => ( state ) => state.get( 'organizationalAddDepartments' );

/**
 * Other specific selectors
 */
const makeSelectNameApiResponse = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'newDepartment' )
);

const makeSelectBulkCreate = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'bulkCreate' )
);

const makeSelectNotification = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectData = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'department' ).toJS()
);

const makeSelectBtnLoad = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

/**
 * Default selector used by AddDepartment
 */
const makeSelectAddDepartment = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectAddDepartment,
  makeSelectNameApiResponse,
  makeSelectBulkCreate,
  makeSelectNotification,
  selectAddDepartmentDomain,
  makeSelectData,
  makeSelectBtnLoad
};
