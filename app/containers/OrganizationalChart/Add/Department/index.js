/* eslint-disable consistent-return */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import _ from 'lodash';

import Sidebar from 'components/Sidebar';
import { H3 } from 'components/Typography';
import SnackBar from 'components/SnackBar';
import Button from 'components/Button';
import SalSelect from 'components/Select';
import Input from 'components/Input';
import A from 'components/A';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import Loader from 'components/Loader';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { auth } from 'utils/AuthService';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';

import * as addDepartmentAction from './actions';

import {
    makeSelectAddDepartment,
    makeSelectBtnLoad,
    makeSelectNotification
} from './selectors';

import { PageWrapper, Footer, ModalAction } from './styles';

/**
 *
 * AddDepartment
 *
 */
export class AddDepartment extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        getDepartments: React.PropTypes.func,
        createBulkCreate: React.PropTypes.func,
        AddDepartmentChart: React.PropTypes.object,
        btnLoading: React.PropTypes.bool,
        resetStore: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    constructor( props ) {
        super( props );
        this.state = {
            departments: [],
            departmentName: '',
            reportingDep: '',
            profile: null,
            editingDepartment: false,
            deleteDepartment: '',
            isNameAvailable: false,
            isNameAvailableOne: false,
            prevState: '',
            isAddLoader: false
        };
        this.confirmOpenModal = null;
        this.successModal = null;
        this.cancelOption = '';
        this.discardModal = null;
        this.companyID = company.getLastActiveCompanyId();
    }

    componentWillMount() {
        const profile = auth.getUser();
        this.setState({ profile });
        this.props.getDepartments();
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    getDepartments( dep ) {
        if ( this.props.AddDepartmentChart.department.length === 0 ) {
            return [];
        }
        const departments = this.props.AddDepartmentChart.department.map( ( department ) => {
            if ( department.id !== ( dep && dep.id ) ) {
                return ({
                    value: department.id,
                    label: department.name,
                    disabled: false
                });
            }
            return ({
                value: department.id,
                label: department.name,
                disabled: true
            });
        });
        return departments;
    }

    getUniqueId = () => `${Math.floor( Math.random() * 1000000 )}`;

    getDefaultDepartment( id, parent = this.state.reportingDep.value, name = this.state.departmentName, isEdit = false, newName = '' ) {
        const isParent = _.find( this.reportingDepartment(), { id: parent });
        const isName = name;
        if ( isEdit === true ) {
            isParent.name = newName;
        }
        return {
            id,
            name: isName,
            parent: isParent || null,
            unavailableName: false,
            editing: false
        };
    }

    handleChangeAdd=( name, values ) => {
        if ( values === '' || values.length <= 0 ) {
            return this.setState({ isNameAvailableOne: true, departmentName: '', isNameAvailable: false });
        }
        const value = values;
        const isName = this.props.AddDepartmentChart.department.filter( ( department ) => ( department.name.toLowerCase() === value.toLowerCase() ? department : '' ) );
        if ( isName.length <= 0 ) {
            this.setState({ departmentName: value, isNameAvailable: false, isNameAvailableOne: false });
        } else {
            this.setState({ departmentName: value, isNameAvailable: true, isNameAvailableOne: false });
        }
    }

    formHandleSubmit = async () => {
        const depName = _.clone( this.state.departmentName );
        const repDep = _.clone( this.state.reportingDep );

        this.setState({
            departmentName: '',
            reportingDep: ''
        });
        if ( depName.length <= 0 && depName === '' ) {
            this.setState({ isNameAvailableOne: true });
        } else if ( this.state.isNameAvailable === false ) {
            const id = this.getUniqueId();
            this.setState({ isAddLoader: true, departments: [ ...this.state.departments, { id, departmentName: depName, reportingDepartment: repDep, isEditing: false, isSaving: false }]});
            const companyId = company.getLastActiveCompanyId();
            await Fetch( `/company/${companyId}/department/is_name_available`, { method: 'POST', data: { name: depName }})
                .then( ({ available }) => {
                    if ( available ) {
                        this.props.AddDepartmentChart.department.push( _.clone( this.getDefaultDepartment( id, repDep.value, depName ) ) );
                    } else {
                        const defaultDepartment = this.getDefaultDepartment();
                        defaultDepartment.unavailableName = true;
                        this.props.AddDepartmentChart.department.push( _.clone( defaultDepartment ) );
                    }
                }).catch( () => {
                    this.setState({ departmentName: '', isAddLoader: false });
                });
            this.setState({
                departmentName: '',
                reportingDep: '',
                isAddLoader: false
            });
        } else {
            this.setState({ isNameAvailable: true, isAddLoader: false });
        }
    }

    reportingDepartment() {
        return _.filter( this.props.AddDepartmentChart.department, ( department ) => !department.editing );
    }

    handleEdit = ( dep ) => {
        this.cancelOption = _.clone( dep );
        this.setState( ( state ) => {
            const newDepartment = state.departments.map( ( department ) => {
                const departmentOne = department;
                if ( departmentOne.id === dep.id ) {
                    departmentOne.isEditing = true;
                } else {
                    departmentOne.isEditing = false;
                }
                return departmentOne;
            });
            return { ...state, departments: newDepartment, prevState: this.cancelOption };
        });
        if ( this.state.isNameAvailable || this.state.isNameAvailableOne ) {
            this.setState( ( state ) => {
                const newDepartment = state.departments.map( ( department ) => {
                    let departmentOne = department;
                    if ( departmentOne.id === this.state.prevState.id ) {
                        departmentOne = this.state.prevState;
                    }
                    return departmentOne;
                });
                return { ...state, departments: newDepartment };
            });
        }
        this.setState({ editingDepartment: true, isNameAvailableOne: false, isNameAvailable: false });
    }

    handleEditSave = ( department ) => {
        this.props.AddDepartmentChart.department = this.props.AddDepartmentChart.department.map( ( dep ) => {
            let depOne = dep;
            if ( depOne.id === department.id ) {
                depOne = this.getDefaultDepartment( department.id, department.reportingDepartment.value, department.departmentName );
            } else if ( department.id === ( depOne.parent && depOne.parent.id && depOne.parent.id ) ) {
                depOne = this.getDefaultDepartment( depOne.id, depOne.parent.id, depOne.name, true, department.departmentName );
            } else {
                return depOne;
            }
            return depOne;
        });
        this.setState( ( state ) => {
            const newDepartment = state.departments.map( ( dep ) => {
                const depOne = dep;
                if ( depOne.id === department.id ) {
                    depOne.departmentName = department.departmentName;
                    depOne.isEditing = false;
                } else if ( department.id === dep.reportingDepartment.value ) {
                    depOne.reportingDepartment.label = department.departmentName;
                }
                return depOne;
            });
            return { ...state, departments: newDepartment };
        });
        this.setState({ editingDepartment: false });
    }
    /**
    *
    * handle change value
    *
    */
    handleChange =( name, values, dep ) => {
        if ( name === 'departmentName' ) {
            if ( values === '' ) {
                this.setState({ isNameAvailable: false, isNameAvailableOne: true });
            } else {
                this.setState({ isNameAvailable: false, isNameAvailableOne: false });
            }
            const value = values.trim();
            const isName = this.props.AddDepartmentChart.department.filter( ( department ) => ( ( department.name.toLowerCase() === value.toLowerCase() ) ? department : '' ) );
            if ( isName.length <= 0 ) {
                this.setState( ( state ) => {
                    const newDepartment = state.departments.map( ( department ) => {
                        const departmentOne = department;
                        if ( departmentOne.id === dep.id ) {
                            departmentOne[ name ] = value;
                        }
                        return departmentOne;
                    });
                    return { ...state, positions: newDepartment, isNameAvailable: false };
                });
            } else if ( value === this.cancelOption.departmentName ) {
                this.setState({ isNameAvailable: false, isNameAvailableOne: false });
            } else {
                this.setState({ isNameAvailable: true, isNameAvailableOne: false });
            }
        }

        this.setState( ( state ) => {
            const newDepartment = state.departments.map( ( department ) => {
                const departmentOne = department;
                if ( departmentOne.id === dep.id ) {
                    departmentOne[ name ] = values;
                }
                return departmentOne;
            });
            return { ...state, departments: newDepartment };
        });
    }

    handleCancel = () => {
        this.setState( ( state ) => {
            const newDepartment = state.departments.map( ( department ) => {
                let departmentOne = department;
                if ( departmentOne.id === this.cancelOption.id ) {
                    departmentOne = this.cancelOption;
                }
                return departmentOne;
            });
            return { ...state, departments: newDepartment };
        });
        this.setState({ editingDepartment: false, isNameAvailable: false, isNameAvailableOne: false });
    }

    handleDelete = ( dep ) => {
        this.confirmOpenModal.toggle();
        this.successModal.toggle();
        const isReporting = this.isReportingDepartment( dep.id );
        if ( isReporting.length >= 1 ) {
            this.reportingDepartmentRemove( dep.id );
        } else {
            this.isDeleteDepartment( dep.id );
        }
    }

    handleDeleteButton = ( department ) => {
        this.setState({ deleteDepartment: department });
        this.confirmOpenModal.toggle();
    }

    isDeleteDepartment = ( dep ) => {
        this.props.AddDepartmentChart.department = this.props.AddDepartmentChart.department.filter( ( department ) => ( department.id ? department.id !== dep : false ) );

        this.setState( ( state ) => {
            const newDepartment = state.departments.filter( ( department ) => department.id !== dep );
            return { ...state, departments: newDepartment };
        });
    }

    reportingDepartmentRemove = ( dep ) => {
        this.props.AddDepartmentChart.department = this.props.AddDepartmentChart.department.map( ( department ) => {
            const departmentOne = department;
            if ( departmentOne.parent && departmentOne.parent.id === dep ) {
                departmentOne.parent = undefined;
            }
            return departmentOne;
        });

        this.props.AddDepartmentChart.department = this.props.AddDepartmentChart.department.filter( ( department ) => ( department.id ? department.id !== dep : false ) );

        this.setState( ( state ) => {
            let newDepartment = state.departments.map( ( department ) => {
                const departmentOne = department;
                if ( departmentOne.reportingDepartment.value === dep ) {
                    departmentOne.reportingDepartment = undefined;
                }
                return department;
            });
            newDepartment = newDepartment.filter( ( department ) => department.id !== dep );
            return { ...state, departments: newDepartment };
        });
    }

    isReportingDepartment = ( dep ) => {
        const isReportingDepartments = this.state.departments.filter( ( department ) => ( department.reportingDepartment ? department.reportingDepartment.value === dep : false ) );
        return isReportingDepartments;
    }

    modalContent = ( dep ) => {
        const response = this.isReportingDepartment( dep.id );
        if ( response.length >= 1 ) {
            return (
                <ModalAction>
                    This Department record is used as Reporting Department to other records.
                    By choosing this action, this record will be removed entirely within the departments list. Proceed anyway?
                </ModalAction>
            );
        }
        return (
            <ModalAction>
                Do you wish to remove this record?
            </ModalAction>
        );
    }

    // Submit Button
    handleSubmit = () => {
        const companyId = company.getLastActiveCompanyId();
        const newDepartment = this.props.AddDepartmentChart.department.filter( ( department ) => ( ( ( typeof ( department.id ) ) === 'string' ) ? department : '' ) );
        if ( newDepartment.length > 0 ) {
            const payload = { company_id: companyId, data: newDepartment };
            this.props.createBulkCreate({ payload, departmentList: this.props.AddDepartmentChart.department });
        } else {
            this.setState({ isNameAvailable: false, isNameAvailableOne: true });
        }
    }

    EditDepartment({ department, index }) {
        return department.isEditing === false
            ? (
                <div className="department-item" key={ department.id }>
                    <div className="department-number">
                        <span>Department {index + 1}</span>
                    </div>

                    <div className="department-name-input">
                        <span>{department.departmentName}</span>
                    </div>

                    <div className="schedule-box">
                        <span>{department.reportingDepartment && department.reportingDepartment.label}</span>
                    </div>

                    <div className="department-btns">
                        <Button
                            type="neutral"
                            label={ <span><Icon name="pencil" className="plus-icon" />Edit</span> }
                            onClick={ () => this.handleEdit( department ) }
                        />
                        <Button
                            type="darkRed"
                            alt
                            label={ <span><Icon name="trash" className="plus-icon" />Delete</span> }
                            onClick={ () => this.handleDeleteButton( department ) }
                        />
                    </div>
                </div>
            ) : (
                <div className="department-inputs" key={ department.id }>
                    <div className="department-number">
                        <span>Department {index + 1}</span>
                    </div>

                    <div className={ this.state.isNameAvailable || this.state.isNameAvailableOne ? 'invalid_content department-name-input' : 'department-name-input' }>
                        <Input
                            id="department_name"
                            label="* Department name"
                            key="department_name"
                            value={ department.departmentName }
                            onChange={ ( value ) => this.handleChange( 'departmentName', value, department ) }
                        />
                        {this.state.isNameAvailable ? <span>Department name already exists</span> : ''}
                        {this.state.isNameAvailableOne ? <span>Please input valid Department name.</span> : ''}
                    </div>

                    <div className="schedule-box">
                        <SalSelect
                            id="reporting_department"
                            label={
                                <p className="schdule_tooltip">Reporting Department
                                    <span className="bg_tooltip">
                                        <span>?</span>
                                        <span className="tooltiptext"> Department that reports to the<br /> specified Department Name.</span>
                                    </span>
                                </p>
                            }
                            key="reporting_department"
                            data={ this.getDepartments( department ) }
                            value={ department.reportingDepartment && department.reportingDepartment.value }
                            placeholder="Choose an option"
                            ref={ ( ref ) => { this.reportingDep = ref; } }
                            onChange={ ( value ) => this.handleChange( 'reportingDepartment', value, department ) }
                            clearable
                        />
                    </div>

                    <div className="department-btns">
                        <Button
                            type="action"
                            label="Save"
                            onClick={ () => this.handleEditSave( department ) }
                        />
                        <Button
                            type="darkRed"
                            alt
                            label="Cancel"
                            onClick={ () => this.handleCancel() }
                        />
                    </div>
                </div>
            );
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { departments, editingDepartment, isNameAvailable, isNameAvailableOne } = this.state;
        const { btnLoading } = this.props;
        return (
            <div>
                <Helmet
                    title="Add Departments"
                    meta={ [
                        { name: 'description', content: 'Description of Add Departments' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p></ModalAction> }
                    buttons={ [
                        {
                            label: 'Stay on this page',
                            type: 'grey',
                            onClick: () => this.discardModal.toggle()
                        },
                        {
                            label: 'Discard',
                            type: 'darkRed',
                            onClick: () => browserHistory.push( '/company-settings/company-structure/organizational-chart', true )
                        }
                    ] }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Modal
                    size="md"
                    title="Confirm Your Action"
                    body={ this.modalContent( this.state.deleteDepartment ) }
                    buttons={ [
                        {
                            label: 'No',
                            type: 'grey',
                            onClick: () => this.confirmOpenModal.toggle()
                        },
                        {
                            label: 'Yes',
                            type: 'action',
                            onClick: () => this.handleDelete( this.state.deleteDepartment )
                        }
                    ] }
                    ref={ ( ref ) => { this.confirmOpenModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Record successfully removed.</p></ModalAction> }
                    buttons={ [
                        {
                            label: 'Okay',
                            type: 'action',
                            onClick: () => this.successModal.toggle()
                        }
                    ] }
                    ref={ ( ref ) => { this.successModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <PageWrapper>
                    <Container className="add-dpt-container">
                        <div className="nav">
                            <Container>
                                <A
                                    href
                                    onClick={ ( e ) => {
                                        e.preventDefault();
                                        browserHistory.push( '/company-settings/company-structure/organizational-chart', true );
                                    } }
                                >
                                    <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Organizational Chart</span>
                                </A>
                            </Container>
                        </div>
                        <div className="main_container">
                            <section className="content">
                                <header className="heading">
                                    <H3>Add Departments</H3>
                                    <p>A department should have a reporting to department. In this manner, organizational hierarchy structure will be determined.</p>
                                </header>
                            </section>
                            <section className="content dept-content">
                                <div className="department-display-header">
                                    <div></div>
                                    <div>
                                        <span>Department name</span>
                                    </div>
                                    <div>
                                        <span>Reporting department</span>
                                    </div>
                                    <div></div>
                                </div>
                                {departments.map( ( department, index ) => this.EditDepartment({ department, index }) )}
                            </section>
                            {editingDepartment === false ?
                                <section className="content">
                                    <div className="department-inputs">
                                        <div className="department-number">
                                            <span>Department {this.state.departments.length + 1}</span>
                                        </div>

                                        <div className={ isNameAvailable || isNameAvailableOne ? 'invalid_content department-name-input' : 'department-name-input' }>
                                            <Input
                                                id="department_name"
                                                label="* Department name"
                                                key="department_name"
                                                value={ this.state.departmentName }
                                                onChange={ ( value ) => this.handleChangeAdd( 'departmentName', value ) }
                                            />
                                            {isNameAvailable ? <span>Department name already exists</span> : ''}
                                            {isNameAvailableOne ? <span>Please input valid Department name.</span> : ''}
                                        </div>

                                        <div className="schedule-box">
                                            <SalSelect
                                                id="reporting_department"
                                                label={
                                                    <p className="schdule_tooltip">Reporting Department
                                                        <span className="bg_tooltip">
                                                            <span>?</span>
                                                            <span className="tooltiptext"> Department that reports to the<br /> specified Department Name.</span>
                                                        </span>
                                                    </p>
                                                }
                                                key="reporting_department"
                                                data={ this.getDepartments() }
                                                value={ this.state.reportingDep }
                                                placeholder="Choose an option"
                                                ref={ ( ref ) => { this.reportingDep = ref; } }
                                                onChange={ ( value ) => { this.setState({ reportingDep: value }); } }
                                                clearable
                                            />
                                        </div>

                                        <div className="department-btns">
                                            <Button
                                                type="neutral"
                                                className="add-btn"
                                                label={
                                                    this.state.isAddLoader
                                                        ? <Loader />
                                                        : <span><Icon name="plus" className="plus-icon" />Add Department</span>
                                                }
                                                disabled={ this.state.isNameAvailable || this.state.isNameAvailableOne }
                                                onClick={ this.formHandleSubmit }
                                            />
                                        </div>
                                    </div>
                                </section>
                                : null
                            }
                        </div>
                    </Container>
                </PageWrapper>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                type="action"
                                size="large"
                                label={ btnLoading ? <Loader /> : 'Submit' }
                                disabled={ btnLoading || this.state.isAddLoader }
                                onClick={ () => this.handleSubmit() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    AddDepartmentChart: makeSelectAddDepartment(),
    notification: makeSelectNotification(),
    btnLoading: makeSelectBtnLoad()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { ...addDepartmentAction },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddDepartment );
