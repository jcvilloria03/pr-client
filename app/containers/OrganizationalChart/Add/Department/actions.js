import { RESET_STORE } from '../../../App/constants';
import {
    DEFAULT_ACTION,
    NOTIFICATION,
    CREATE_DEPARTMENT,
    CREATE_BULKCREATE,
    GET_DEPARTMENT
} from './constants';

/**
 *
 * AddDepartment actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * Department Request Type
 * @param {object} payload
 */
export function getDepartments() {
    return {
        type: GET_DEPARTMENT
    };
}

/**
 * add department page
 * @param {object} payload
 */
export function createDepartment( payload ) {
    return {
        type: CREATE_DEPARTMENT,
        payload
    };
}

/**
 * create bulk create
 * @param {object} payload
 */
export function createBulkCreate( payload ) {
    return {
        type: CREATE_BULKCREATE,
        payload
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Resets the payrolls
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
