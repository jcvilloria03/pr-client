import { fromJS } from 'immutable';
import { RESET_STORE } from '../../../App/constants';
import {
    BTN_LOADING,
    DEFAULT_ACTION,
    NOTIFICATION_SAGA,
    SET_BULK_CREATE,
    SET_BULK_UPDATE,
    SET_DEPARTMENT,
    SET_ERRORS,
    SET_NEW_DEPARTMENT
} from './constants';

const initialState = fromJS({
    btnLoading: false,
    department: [],
    newDepartment: null,
    bulkCreate: [],
    bulkUpdate: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    errors: {}
});

/**
 *
 * AddDepartment reducer
 *
 */
function addDepartmentReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_DEPARTMENT:
            return state.set( 'department', fromJS( action.payload ) );
        case SET_NEW_DEPARTMENT:
            return state.set( 'newDepartment', fromJS( action.payload ) );
        case SET_BULK_CREATE:
            return state.set( 'bulkCreate', action.payload );
        case SET_BULK_UPDATE:
            return state.set( 'bulkUpdate', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addDepartmentReducer;
