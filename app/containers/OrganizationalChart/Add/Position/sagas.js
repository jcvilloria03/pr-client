import { get } from 'lodash';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { call, cancel, put, take } from 'redux-saga/effects';
import { browserHistory } from '../../../../utils/BrowserHistory';
import { company } from '../../../../utils/CompanyService';
import { RECORD_UPDATED_MESSAGE } from '../../../../utils/constants';
import { formatFeedbackMessage } from '../../../../utils/functions';
import { Fetch } from '../../../../utils/request';
import { REINITIALIZE_PAGE } from '../../../App/constants';
import { resetStore } from '../../../App/sagas';
import { BTN_LOADING, CREATE_BULKPOSITION, GET_DEPARTMENT, GET_POSITION, LOADING, NOTIFICATION, NOTIFICATION_SAGA, SET_BULKCREATE, SET_BULKUPDATE, SET_DEPARTMENT, SET_ERRORS, SET_POSITION } from './constants';

/**
 * Position fetch Data
 */
export function* getPositions() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const positionPreview = yield call( Fetch, `/company/${companyId}/positions` );

        yield put({
            type: SET_POSITION,
            payload: positionPreview && positionPreview.data || []
        });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Department fetch Data
 */
export function* getDepartments() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const DepartmentValue = yield call( Fetch, `/company/${companyId}/departments?mode=DEFAULT_MODE` );

        yield put({
            type: SET_DEPARTMENT,
            payload: DepartmentValue && DepartmentValue.data || []
        });
    } catch ( error ) {
        yield call( notifyError, error.response );
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

 /**
 * Create Bulk Position
 */
export function* createBulkPosition({ payload }) {
    try {
        yield put({
            type: BTN_LOADING,
            payload: true
        });
        const payloadData = payload.data;
        const toCreate = payload.data.map( ( position ) => ({ name: position.name, department_id: position.reporting_department ? position.reporting_department.id : null }) );
        const newPayload = { company_id: payload.company_id, data: toCreate };
        const storedPositions = yield call( Fetch, '/position/bulk_create', { method: 'POST', data: newPayload });

        let updatePayload;
        if ( storedPositions.data.length > 0 ) {
            const updateLoadData = payloadData.map( ( position ) => {
                const positionOne = position;
                const isIdData = storedPositions.data.find( ( isPosition ) => isPosition.name === positionOne.name );
                const isReporting = payload.data.find( ( oldData ) => oldData.reporting_position && oldData.reporting_position.name === isIdData.name && isIdData.name );
                if ( isReporting && isReporting.reporting_position && isReporting.reporting_position ) {
                    isReporting.reporting_position.id = isIdData.id;
                }
                positionOne.id = isIdData.id;
                return positionOne;
            });

            const toUpdate = updateLoadData.filter( ( position ) => position.reporting_position ).map( ( position ) => ({
                id: position.id,
                parent_id: position.reporting_position.id
            }) );
            if ( toUpdate.length ) {
                updatePayload = { company_id: payload.company_id, data: toUpdate };
                const updatePositions = yield call( Fetch, '/position/bulk_update', { method: 'PUT', data: updatePayload });
                yield put({ type: SET_BULKUPDATE, payload: updatePositions || []});
                if ( updatePositions.data.length > 0 ) {
                    browserHistory.push( '/company-settings/company-structure/organizational-chart', true );
                }
            }
            yield put({ type: SET_BULKCREATE, payload: storedPositions || []});

            browserHistory.push( '/company-settings/company-structure/organizational-chart', true );
        }
    } catch ( error ) {
        yield call( notifyError, error.response );
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
    yield call( getDepartments );
    yield call( getPositions );
}

 /**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.data.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Individual exports for testing
 */
export function* watchForGetPosition() {
    const watcher = yield takeEvery( GET_POSITION, getPositions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetDepartment() {
    const watcher = yield takeEvery( GET_DEPARTMENT, getDepartments );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for CeateBulkPosition
 */
export function* wacthForCreateBulkPosition() {
    const watcher = yield takeEvery( CREATE_BULKPOSITION, createBulkPosition );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForReinitializePage,
    watchForGetPosition,
    watchForGetDepartment,
    wacthForCreateBulkPosition
];
