import { fromJS } from 'immutable';
import { RESET_STORE } from '../../../App/constants';
import {
    BTN_LOADING,
    DEFAULT_ACTION, NOTIFICATION_SAGA, SET_BULKCREATE, SET_BULKUPDATE, SET_DEPARTMENT, SET_ERRORS, SET_POSITION
} from './constants';

const initialState = fromJS({
    createBulk: [],
    btnLoading: false,
    department: [],
    position: [],
    updateBulk: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    errors: {}
});

/**
 *
 * AddPosition reducer
 *
 */
function addPositionReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_DEPARTMENT:
            return state.set( 'department', fromJS( action.payload ) );
        case SET_POSITION:
            return state.set( 'position', fromJS( action.payload ) );
        case SET_BULKCREATE:
            return state.set( 'createBulk', fromJS( action.payload ) );
        case SET_BULKUPDATE:
            return state.set( 'updateBulk', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addPositionReducer;
