import { RESET_STORE } from '../../../App/constants';
import {
    CREATE_BULKPOSITION,
    DEFAULT_ACTION,
    GET_DEPARTMENT,
    GET_POSITION,
    NOTIFICATION
} from './constants';

/**
 *
 * AddPosition actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * Department Request Type
 * @param {object} payload
 */
export function getDepartments() {
    return {
        type: GET_DEPARTMENT
    };
}

/**
 * Department Request Type
 * @param {object} payload
 */
export function getPositions() {
    return {
        type: GET_POSITION
    };
}

/**
 * Create Bulk Position
 *
 */
export function createBulkPosition( payload ) {
    return {
        type: CREATE_BULKPOSITION,
        payload
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Resets the payrolls
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
