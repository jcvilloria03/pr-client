import { createSelector } from 'reselect';

/**
 * Direct selector to the addPosition state domain
 */
const selectAddPositionDomain = () => ( state ) => state.get( 'addPosition' );

/**
 * Other specific selectors
 */

const makeSelectNotification = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectPositionData = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'position' ).toJS()
);

const makeSelectData = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'department' ).toJS()
);

const makeSelectBtnLoad = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

/**
 * Default selector used by AddPosition
 */
const makeSelectAddPosition = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectAddPosition,
  makeSelectPositionData,
  makeSelectNotification,
  selectAddPositionDomain,
  makeSelectBtnLoad,
  makeSelectData
};
