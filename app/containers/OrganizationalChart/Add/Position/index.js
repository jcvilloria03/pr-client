/* eslint-disable consistent-return */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';
import _ from 'lodash';

import Sidebar from 'components/Sidebar';
import { H3 } from 'components/Typography';
import A from 'components/A';
import SnackBar from 'components/SnackBar';
import SalSelect from 'components/Select';
import Input from 'components/Input';
import Button from 'components/Button';
import Modal from 'components/Modal';
import Icon from 'components/Icon';
import Loader from 'components/Loader';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';

import {
    makeSelectNotification,
    makeSelectAddPosition,
    makeSelectData,
    makeSelectBtnLoad
} from './selectors';
import * as addPostion from './actions';
import { PageWrapper, Footer, ModalAction } from './styles';

/**
 *
 * AddPosition
 *
 */
export class AddPosition extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        getDepartments: React.PropTypes.func,
        getPositions: React.PropTypes.func,
        createBulkPosition: React.PropTypes.func,
        btnLoading: React.PropTypes.bool,
        resetStore: React.PropTypes.func,
        AddDepartment: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        PositionAll: React.PropTypes.object
    };

    constructor( props ) {
        super( props );

        this.state = {
            isNameAvailable: false,
            isNameAvailableOne: false,
            positionName: '',
            repoPosition: '',
            repoDepartment: '',
            positions: [],
            deletePosition: null,
            isEditPosition: false,
            prevState: '',
            isAddLoader: false
        };
        this.confirmModal = null;
        this.successModal = null;
        this.discardModal = null;
        this.cancelOption = '';
    }

    componentDidMount() {
        this.props.getDepartments();
        this.props.getPositions();
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    getUniqueId = () => `${Math.floor( Math.random() * 1000000 )}`;

    getDepartments( pos ) {
        if ( this.props.AddDepartment.length === 0 ) {
            return [];
        }
        const departments = this.props.AddDepartment.map( ( department ) => {
            if ( department.id !== ( pos && pos.id ) ) {
                return ({
                    value: department.id,
                    label: department.name,
                    disabled: false
                });
            }
            return ({
                value: department.id,
                label: department.name,
                disabled: true
            });
        });
        return departments;
    }

    getPostions( pos ) {
        if ( this.props.PositionAll.position.length === 0 ) {
            return [];
        }
        const positions = this.props.PositionAll.position.map( ( position ) => {
            if ( position.id !== ( pos && pos.id ) ) {
                return ({
                    value: position.id,
                    label: position.name,
                    disabled: false
                });
            }
            return ({
                value: position.id,
                label: position.name,
                disabled: true
            });
        });
        return positions;
    }

    getDefaultPosition( id, parent = ( this.state.repoPosition && this.state.repoPosition.value ), parentDep = ( this.state.repoDepartment && this.state.repoDepartment.value ), name = this.state.positionName, isEdit = false, newName = '' ) {
        const isName = name;
        const isParent = _.find( this.reportingPosition(), { id: parent });
        if ( isEdit === true ) {
            isParent.name = newName;
        }
        const department = _.find( this.props.AddDepartment, { id: parentDep });
        return {
            id,
            name: isName,
            reporting_position: isParent || undefined,
            reporting_department: department || undefined,
            editing: false,
            unavailableName: false
        };
    }

    handleChangeAdd=( name, values ) => {
        if ( values === '' || values.length <= 0 ) {
            return this.setState({ isNameAvailableOne: true, positionName: '', isNameAvailable: false });
        }
        const value = values.trim();
        const isName = this.props.PositionAll.position.filter( ( position ) => ( ( position.name.toLowerCase() === value.toLowerCase() ) ? position : '' ) );
        if ( isName.length <= 0 ) {
            this.setState({ positionName: value, isNameAvailable: false, isNameAvailableOne: false });
        } else {
            this.setState({ isNameAvailable: true, isNameAvailableOne: false });
        }
    }

    handleAddPosition = async () => {
        const posName = await _.clone( this.state.positionName );
        const repDep = await _.clone( this.state.repoDepartment );
        const repPos = await _.clone( this.state.repoPosition );
        this.setState({
            positionName: '',
            repoPosition: '',
            repoDepartment: ''
        });

        if ( posName.length <= 0 && posName === '' ) {
            this.setState({ isNameAvailableOne: true });
        } else if ( this.state.isNameAvailable === false ) {
            const id = this.getUniqueId();
            this.setState({ isAddLoader: true, positions: [ ...this.state.positions, { id, positionName: posName, reportingPosition: repPos, reportingDepartment: repDep, isEditing: false }]});
            const companyId = company.getLastActiveCompanyId();

            await Fetch( `/company/${companyId}/position/is_name_available`, { method: 'POST', data: { name: posName }})
            .then( ({ available }) => {
                if ( available ) {
                    this.props.PositionAll.position.push( _.clone( this.getDefaultPosition( id, repPos.value, repDep.value, posName ) ) );
                } else {
                    const defaultPosition = this.getDefaultPosition();
                    defaultPosition.unavailableName = true;
                    this.props.PositionAll.position( _.clone( defaultPosition ) );
                }
            }).catch( () => {
                this.setState({ positionName: '', isAddLoader: false });
            });

            this.setState({
                isAddLoader: false,
                positionName: '',
                repoPosition: '',
                repoDepartment: ''
            });
        } else {
            this.setState({ isNameAvailable: true, isAddLoader: false });
        }
    }

    reportingPosition() {
        return _.filter( this.props.PositionAll.position, ( position ) => !position.editing );
    }

    handleEdit=( pos ) => {
        this.cancelOption = _.clone( pos );
        this.setState( ( state ) => {
            const newPosition = state.positions.map( ( position ) => {
                const positionOne = position;
                if ( positionOne.id === pos.id ) {
                    positionOne.isEditing = true;
                } else {
                    positionOne.isEditing = false;
                }
                return positionOne;
            });
            return { ...state, positions: newPosition, prevState: this.cancelOption };
        });
        if ( this.state.isNameAvailable || this.state.isNameAvailableOne ) {
            this.setState( ( state ) => {
                const newPosition = state.positions.map( ( position ) => {
                    let positionOne = position;
                    if ( positionOne.id === this.state.prevState.id ) {
                        positionOne = this.state.prevState;
                    }
                    return positionOne;
                });
                return { ...state, positions: newPosition };
            });
        }
        this.setState({ isEditPosition: true, isNameAvailable: false, isNameAvailableOne: false });
    }

    handleChange=( name, values, pos ) => {
        if ( name === 'positionName' ) {
            if ( values === '' ) {
                this.setState({ isNameAvailable: false, isNameAvailableOne: true });
            } else {
                this.setState({ isNameAvailable: false, isNameAvailableOne: false });
            }
            const value = values.trim();
            const isName = this.props.PositionAll.position.filter( ( position ) => ( ( position.name.toLowerCase() === value.toLowerCase() ) ? position : '' ) );
            if ( isName.length <= 0 ) {
                this.setState( ( state ) => {
                    const newPosition = state.positions.map( ( position ) => {
                        const positionOne = position;
                        if ( positionOne.id === pos.id ) {
                            positionOne[ name ] = value;
                        }
                        return positionOne;
                    });
                    return { ...state, positions: newPosition, isNameAvailable: false };
                });
            } else if ( value === this.cancelOption.positionName ) {
                this.setState({ isNameAvailable: false, isNameAvailableOne: false });
            } else {
                this.setState({ isNameAvailable: true, isNameAvailableOne: false });
            }
        }

        this.setState( ( state ) => {
            const newPosition = state.positions.map( ( position ) => {
                const positionOne = position;
                if ( positionOne.id === pos.id ) {
                    positionOne[ name ] = values;
                }
                return positionOne;
            });
            return { ...state, positions: newPosition };
        });
    }

    handleEditSave=( pos ) => {
        this.props.PositionAll.position = this.props.PositionAll.position.map( ( position ) => {
            let positionOne = position;
            if ( positionOne.id === pos.id ) {
                positionOne = this.getDefaultPosition( pos.id, ( pos.reportingPosition && pos.reportingPosition.value ), ( pos.reportingDepartment && pos.reportingDepartment.value ), pos.positionName );
            } else if ( pos.id === ( positionOne.reporting_position && positionOne.reporting_position.id && positionOne.reporting_position.id ) ) {
                positionOne = this.getDefaultPosition( positionOne.id, ( positionOne.reporting_position && positionOne.reporting_position.id ), ( positionOne.reporting_department && positionOne.reporting_department.id ), positionOne.name, true, pos.positionName );
            } else {
                return positionOne;
            }
            return positionOne;
        });

        this.setState( ( state ) => {
            const newPosition = state.positions.map( ( position ) => {
                const positionOne = position;
                if ( positionOne.id === pos.id ) {
                    positionOne.positionName = pos.positionName;
                    positionOne.isEditing = false;
                } else if ( pos.id === positionOne.reportingPosition.value ) {
                    positionOne.reportingPosition.label = pos.positionName;
                } else {
                    return positionOne;
                }
                return positionOne;
            });
            return { ...state, positions: newPosition };
        });

        this.setState({ isEditPosition: false });
    }

    handleCancel=() => {
        this.setState( ( state ) => {
            const newPosition = state.positions.map( ( position ) => {
                let positionOne = position;
                if ( positionOne.id === this.cancelOption.id ) {
                    positionOne = this.cancelOption;
                }
                return positionOne;
            });
            return { ...state, positions: newPosition };
        });
        this.setState({ isEditPosition: false, isNameAvailable: false, isNameAvailableOne: false });
    }

    // delete functionality
    isReportingPosition=( id ) => {
        const isReportingPositions = this.state.positions.filter( ( position ) => ( position.reportingPosition ? position.reportingPosition.value === id : false ) );
        return isReportingPositions;
    }

    modalContent = ( position ) => {
        const response = this.isReportingPosition( ( position && position.id ) );
        if ( response.length >= 1 ) {
            return (
                <ModalAction>
                   This Position record is used as Reporting Position to other records. By choosing this action, this record will be removed entirely within the positions list. Proceed anyway?
                </ModalAction> );
        }
        return (
            <ModalAction>
                Do you wish to remove this record?
            </ModalAction> );
    }

    handleDeleteButton=( pos ) => {
        this.setState({ deletePosition: pos });
        this.confirmModal.toggle();
    }

    handleDelete=( position ) => {
        const isReporting = this.isReportingPosition( position.id );
        if ( isReporting.length >= 1 ) {
            this.isReportingDelete( position.id );
        } else {
            this.isDeletePosition( position.id );
        }
        this.confirmModal.toggle();
        this.successModal.toggle();
    }

    isReportingDelete=( id ) => {
        this.props.PositionAll.position = this.props.PositionAll.position.map( ( position ) => {
            const positionOne = position;
            if ( positionOne.reporting_position && positionOne.reporting_position.id === id ) {
                positionOne.reporting_position = undefined;
            }
            return positionOne;
        });

        this.props.PositionAll.position = this.props.PositionAll.position.filter( ( position ) => ( position.id ? position.id !== id : false ) );

        this.setState( ( state ) => {
            let newPosition = state.positions.map( ( position ) => {
                const positionOne = position;
                if ( positionOne.reportingPosition.value === id ) {
                    positionOne.reportingPosition = undefined;
                }
                return positionOne;
            });
            newPosition = newPosition.filter( ( position ) => position.id !== id );
            return { ...state, positions: newPosition };
        });
    }

    isDeletePosition=( id ) => {
        this.props.PositionAll.position = this.props.PositionAll.position.filter( ( position ) => ( position.id ? position.id !== id : false ) );

        this.setState( ( state ) => {
            const newPosition = state.positions.filter( ( position ) => ( position.id !== id ) );
            return { ...state, positions: newPosition };
        });
    }

    // submit button
    handleSubmit= () => {
        const companyId = company.getLastActiveCompanyId();
        const newPositions = this.props.PositionAll.position.filter( ( position ) => ( ( ( typeof ( position.id ) ) === 'string' ) ? position : '' ) );
        if ( newPositions.length > 0 ) {
            const payload = { company_id: companyId, data: newPositions, positionList: this.props.PositionAll.position };
            this.props.createBulkPosition( payload );
        } else {
            this.setState({ isNameAvailable: false, isNameAvailableOne: true });
        }
    }

    EditPostion({ position, index }) {
        return (
            <div className="row" key={ position.id }>
                <div className="col-md-10 schedule_main">
                    {position.isEditing === false
                        ? (
                            <div>
                                <div className="col-md-3 ">
                                    <span> Position {index + 1}</span>
                                </div>
                                <div className="col-md-3 ">
                                    <div className="break_type">{position.positionName}</div>
                                </div>
                                <div className="col-md-3 ">
                                    <div className="break_type">{position.reportingPosition ? position.reportingPosition.label : ''}</div>
                                </div>
                                <div className="col-md-3 ">
                                    <div className="break_type">{position.reportingDepartment ? position.reportingDepartment.label : ''}</div>
                                </div>
                            </div>
                        ) : (
                            <div>
                                <div className="col-md-3 schedule_main schedule_add">
                                    <p>Position {index + 1}</p>
                                </div>
                                <div className={ this.state.isNameAvailable || this.state.isNameAvailableOne ? 'col-md-3 schedule_main breakType schedule_main_delet' : 'col-md-3 schedule_main breakType' }>
                                    <Input
                                        id="postion_name"
                                        label="* Position name"
                                        key="position_name"
                                        value={ position.positionName }
                                        onChange={ ( value ) => this.handleChange( 'positionName', value, position ) }
                                    />
                                    {this.state.isNameAvailable ? <span>Position name already exists</span> : ''}
                                    {this.state.isNameAvailableOne ? <span>Please input valid Position name.</span> : ''}
                                </div>
                                <div className="col-md-3 schdule_box">
                                    <SalSelect
                                        id="reporting_position"
                                        label={
                                            <p className="schdule_tooltip">Reporting Position
                                                <span className="bg_tooltip">
                                                    <span>?</span>
                                                    <span className="tooltiptext">Job title of a person who supervises or manages someone who holds a Position.<br /> specified Department Name.</span>
                                                </span>
                                            </p>
                                        }
                                        key="reporting_position"
                                        data={ this.getPostions( position ) }
                                        value={ position.reportingPosition && position.reportingPosition.value }
                                        placeholder="Choose an option"
                                        onChange={ ( value ) => this.handleChange( 'reportingPosition', value, position ) }
                                        clearable
                                    />
                                </div>
                                <div className="col-md-3 schdule_box">
                                    <SalSelect
                                        id="position_reporting_department"
                                        label={
                                            <p className="schdule_tooltip">Reporting Department
                                                <span className="bg_tooltip">
                                                    <span>?</span>
                                                    <span className="tooltiptext"> Name of a Department that someone with a Position belongs to.<br /> specified Department Name.</span>
                                                </span>
                                            </p>
                                        }
                                        key="reporting_department"
                                        data={ this.getDepartments() }
                                        value={ position.reportingDepartment && position.reportingDepartment.value }
                                        placeholder="Choose an option"
                                        onChange={ ( value ) => this.handleChange( 'reportingDepartment', value, position ) }
                                        clearable
                                    />
                                </div>
                            </div>
                        )
                    }
                </div>

                <div className="col-md-2 p-0 btn_section">
                    {position.isEditing === false
                        ? (
                            <div>
                                <Button
                                    className="edit_btn"
                                    label={ <span><Icon name="pencil" className="plus-icon" />Edit</span> }
                                    onClick={ () => this.handleEdit( position ) }
                                />
                                <Button
                                    className="delete_btn"
                                    label={ <span><Icon name="trash" className="plus-icon" />Delete</span> }
                                    onClick={ () => this.handleDeleteButton( position ) }
                                />
                            </div>
                        ) : (
                            <div className="schedule_save">
                                <Button
                                    className="btn_save" label="Save"
                                    onClick={ () => this.handleEditSave( position ) }
                                    disabled={ this.state.isNameAvailable === true || this.state.isNameAvailableOne === true }
                                />
                                <Button
                                    className="delete_btn" label="Cancel"
                                    onClick={ () => this.handleCancel( position ) }
                                />
                            </div>
                        )
                    }
                </div>
            </div>
        );
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { positions, isEditPosition, isNameAvailable, isNameAvailableOne } = this.state;
        const { btnLoading } = this.props;
        return (
            <div>
                <Helmet
                    title="Add Positions"
                    meta={ [
                        { name: 'description', content: 'Description of Add Positions' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    size="md"
                    title="Confirm Your Action"
                    body={ this.modalContent( this.state.deletePosition ) }
                    buttons={
                    [
                        {
                            label: 'No',
                            type: 'grey',
                            onClick: () => this.confirmModal.toggle()
                        },
                        {
                            label: 'Yes',
                            type: 'action',
                            onClick: () => this.handleDelete( this.state.deletePosition )
                        }
                    ] }
                    ref={ ( ref ) => { this.confirmModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Record successfully removed</p></ModalAction> }
                    buttons={
                    [
                        {
                            label: 'Okay',
                            type: 'action',
                            onClick: () => this.successModal.toggle()
                        }
                    ] }
                    ref={ ( ref ) => { this.successModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p></ModalAction> }
                    buttons={
                    [
                        {
                            label: 'Stay on this page',
                            type: 'grey',
                            onClick: () => this.discardModal.toggle()
                        },
                        {
                            label: 'Discard',
                            type: 'darkRed',
                            onClick: () => browserHistory.push( '/company-settings/company-structure/organizational-chart', true )
                        }

                    ] }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <PageWrapper>
                    <Container className="addposition-container">
                        <Sidebar
                            items={ sidebarLinks }
                        />
                        <div className="nav">
                            <Container>
                                <A
                                    href
                                    onClick={ ( e ) => {
                                        e.preventDefault();
                                        browserHistory.push( '/company-settings/company-structure/organizational-chart', true );
                                    } }
                                >
                                    <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Organizational Chart</span>
                                </A>
                            </Container>
                        </div>
                        <div className="main_container">
                            <section className="content">
                                <header className="heading">
                                    <H3>Add Positions</H3>
                                    <p>A position should have a lead position to report to. In this manner, a position will also be tagged with a department so that organizational hierarchy structure will be determined.</p>
                                </header>
                            </section>
                            <div className="brecks_title">
                                <div className="row">
                                    <div className="col-md-10 schedule_main">
                                        <div className="col-md-3 schedule_main"></div>
                                        <div className="col-md-3 schedule_main">
                                            <div className="break_type">Position name</div>
                                        </div>
                                        <div className="col-md-3 schedule_main">
                                            <div className="break_type">Reporting position</div>
                                        </div>
                                        <div className="col-md-3 schedule_main">
                                            <div className="break_type">Reporting department</div>
                                        </div>
                                    </div>
                                </div>
                                {positions.map( ( position, index ) => this.EditPostion({ position, index }) )}
                                {isEditPosition === false ?
                                    <div className="row schedule_section">
                                        <div>
                                            <div className="col-md-10 schedule_main">
                                                <div className=" col-md-3 schedule_main schedule_add" ><p>Position {positions.length + 1}</p></div>
                                                <div className={ isNameAvailable || isNameAvailableOne ? 'col-md-3 schedule_main breakType schedule_main_delet' : 'col-md-3 schedule_main breakType' }>
                                                    <Input
                                                        id="postion_name"
                                                        label="* Position name"
                                                        key="position_name"
                                                        value={ this.state.positionName }
                                                        onChange={ ( value ) => this.handleChangeAdd( 'positionName', value ) }
                                                    />
                                                    {isNameAvailable ? <span>Position name already exists</span> : ''}
                                                    {isNameAvailableOne ? <span>Please input valid Position name</span> : ''}
                                                </div>
                                                <div className="col-md-3 schdule_box">
                                                    <SalSelect
                                                        id="reporting_position"
                                                        label={
                                                            <p className="schdule_tooltip">Reporting Position
                                                                <span className="bg_tooltip">
                                                                    <span>?</span>
                                                                    <span className="tooltiptext">Job title of a person who supervises or manages someone who holds a Position.<br /> specified Department Name.</span>
                                                                </span>
                                                            </p>
                                                        }
                                                        key="reporting_position"
                                                        data={ this.getPostions() }
                                                        value={ this.state.repoPosition }
                                                        placeholder="Choose an option"
                                                        ref={ ( ref ) => { this.repoPosition = ref; } }
                                                        onChange={ ( value ) => { this.setState({ repoPosition: value }); } }
                                                        clearable
                                                    />
                                                </div>
                                                <div className="col-md-3 schdule_box ">
                                                    <SalSelect
                                                        id="position_reporting_department"
                                                        label={
                                                            <p className="schdule_tooltip">Reporting Department
                                                                <span className="bg_tooltip">
                                                                    <span>?</span>
                                                                    <span className="tooltiptext"> Name of a Department that someone with a Position belongs to.<br /> specified Department Name.</span>
                                                                </span>
                                                            </p>
                                                        }
                                                        key="reporting_department"
                                                        data={ this.getDepartments() }
                                                        value={ this.state.repoDepartment }
                                                        placeholder="Choose an option"
                                                        ref={ ( ref ) => { this.repoDepartment = ref; } }
                                                        onChange={ ( value ) => { this.setState({ repoDepartment: value }); } }
                                                        clearable
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-2 schedule_main schedule_add">
                                            <div className="breck_button">
                                                <Button
                                                    id="button-addPosition"
                                                    className="sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost"
                                                    onClick={ this.handleAddPosition }
                                                    label={ this.state.isAddLoader ? <Loader /> : <span><Icon name="plus" className="plus-icon" />Add Position</span> }
                                                    disabled={ this.state.isNameAvailable || this.state.isNameAvailableOne }
                                                />
                                            </div>
                                        </div>
                                    </div> : ''}
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                type="action"
                                size="large"
                                label={ btnLoading ? <Loader /> : 'Submit' }
                                disabled={ btnLoading || this.state.isAddLoader }
                                onClick={ () => this.handleSubmit() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    AddDepartment: makeSelectData(),
    PositionAll: makeSelectAddPosition(),
    notification: makeSelectNotification(),
    btnLoading: makeSelectBtnLoad()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { ...addPostion },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddPosition );
