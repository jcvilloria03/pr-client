import { createSelector } from 'reselect';

/**
 * Direct selector to the organizationalChart state domain
 */
const selectOrganizationalChartDomain = () => ( state ) => state.get( 'organizationalChart' );

/**
 * Other specific selectors
 */
const makeSelectData = () => createSelector(
  selectOrganizationalChartDomain(),
  ( substate ) => substate.get( 'department' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectOrganizationalChartDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectPositionData = () => createSelector(
  selectOrganizationalChartDomain(),
  ( substate ) => substate.get( 'position' ).toJS()
);

const makeSelectBtnLoad = () => createSelector(
  selectOrganizationalChartDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

const makeSelectChartData = () => createSelector(
  selectOrganizationalChartDomain(),
  ( substate ) => substate.get( 'chartData' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectOrganizationalChartDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);
/**
 * Default selector used by OrganizationalChart
 */

const makeSelectOrganizationalChart = () => createSelector(
  selectOrganizationalChartDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectOrganizationalChart,
  selectOrganizationalChartDomain,
  makeSelectData,
  makeSelectLoading,
  makeSelectBtnLoad,
  makeSelectNotification,
  makeSelectPositionData,
  makeSelectChartData
};
