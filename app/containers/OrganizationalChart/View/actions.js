import {
    DEFAULT_ACTION,
    GET_DEPARTMENT,
    NOTIFICATION,
    GET_POSITION,
    DELETE_DEPARTMENT,
    DELETE_POSITION,
    GET_CHART_DATA
} from './constants';

/**
 *
 * OrganizationalChart actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * Department Request Type
 * @param {object} payload
 */
export function getDepartments() {
    return {
        type: GET_DEPARTMENT
    };
}

/**
 * Department Request Type
 * @param {object} payload
 */
export function getPositions() {
    return {
        type: GET_POSITION
    };
}

/**
 * Department Request Type
 * @param {object} payload
 */
export function getChartData() {
    return {
        type: GET_CHART_DATA
    };
}

/**
 * Delete Department
 * @param {object} payload
 */
export function deleteDepartment( payload ) {
    return {
        type: DELETE_DEPARTMENT,
        payload
    };
}

/**
 * Delete Position
 * @param {object} payload
 */
export function deletePosition( payload ) {
    return {
        type: DELETE_POSITION,
        payload
    };
}
/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
