/*
 *
 * OrganizationalChart constants
 *
 */

const namespace = 'app/containers/OrganizationalChart/View';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const BTN_LOADING = `${namespace}/BTN_LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const SET_DEPARTMENT = `${namespace}/SET_DEPARTMENT`;
export const GET_DEPARTMENT = `${namespace}/GET_DEPARTMENT`;
export const SET_POSITION = `${namespace}/SET_POSITION`;
export const GET_POSITION = `${namespace}/GET_POSITION`;
export const DELETE_DEPARTMENT = `${namespace}/DELETE_DEPARTMENT`;
export const DELETE_POSITION = `${namespace}/DELETE_POSITION`;
export const GET_CHART_DATA = `${namespace}/GET_CHART_DATA`;
export const SET_CHART_DATA = `${namespace}/SET_CHART_DATA`;

