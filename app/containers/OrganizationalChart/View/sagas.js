import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import get from 'lodash/get';

import { Fetch } from 'utils/request';

import {
    LOADING,
    SET_DEPARTMENT,
    GET_DEPARTMENT,
    NOTIFICATION,
    SET_NOTIFICATION,
    GET_POSITION,
    SET_POSITION,
    DELETE_DEPARTMENT,
    DELETE_POSITION,
    BTN_LOADING,
    GET_CHART_DATA,
    SET_CHART_DATA
} from './constants';
import { resetStore } from '../../App/sagas';
import { REINITIALIZE_PAGE } from '../../App/constants';
import { company } from '../../../utils/CompanyService';
import { formatFeedbackMessage } from '../../../utils/functions';
import { RECORD_DELETE_MESSAGE } from '../../../utils/constants';
/**
 * Individual exports for testing
 */
export function* getChartData() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();

        const [ departments, positions ] = yield [
            call( Fetch, `/company/${companyId}/departments?mode=DEFAULT_MODE`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/positions`, { method: 'GET' })
        ];

        const filterData = {
            departments: departments && departments.data || [],
            positions: positions && positions.data || []
        };

        yield put({
            type: SET_CHART_DATA,
            payload: filterData
        });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Department fetch data
 */
export function* getDepartments() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const DepartmentValue = yield call( Fetch, `/company/${companyId}/departments?mode=DEFAULT_MODE` );

        yield put({
            type: SET_DEPARTMENT,
            payload: DepartmentValue && DepartmentValue.data || []
        });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Position fetch Data
 */
export function* getPositions() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const positionPreview = yield call( Fetch, `/company/${companyId}/positions` );

        yield put({
            type: SET_POSITION,
            payload: positionPreview && positionPreview.data || []
        });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Delete Department
 */
export function* deleteDepartment({ payload }) {
    try {
        yield put({
            type: BTN_LOADING,
            payload: true
        });
        const isUse = yield call( Fetch, '/company/department/is_in_use', { method: 'POST', data: payload });

        if ( isUse && isUse.in_use === 0 ) {
            const [id] = payload.ids;
            yield call( Fetch, `/department/${id}`, { method: 'DELETE' });

            yield call( showSuccessMessage );
            yield call( getChartData );
        } else {
            const error = {
                message: "Cannot read properties of undefined reading 'status'"
            };

            yield call( notifyError, {
                show: true,
                title: 'Error',
                message: error.message,
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}

/**
 * Delete Position
 */
export function* deletePosition({ payload }) {
    try {
        yield put({
            type: BTN_LOADING,
            payload: true
        });
        const isUse = yield call( Fetch, '/company/position/is_in_use', { method: 'POST', data: payload });

        if ( isUse && isUse.in_use === 0 ) {
            const [id] = payload.ids;
            yield call( Fetch, `/position/${id}`, { method: 'DELETE' });

            yield call( showSuccessMessage );
            yield call( getChartData );
        } else {
            const error = {
                message: "Cannot read properties of undefined reading 'status'"
            };

            yield call( notifyError, {
                show: true,
                title: 'Error',
                message: error.message,
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getDepartments );
    yield call( getPositions );
}

 /**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( true, 'success', RECORD_DELETE_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetDepartment() {
    const watcher = yield takeEvery( GET_DEPARTMENT, getDepartments );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetPosition() {
    const watcher = yield takeEvery( GET_POSITION, getPositions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetChartData() {
    const watcher = yield takeEvery( GET_CHART_DATA, getChartData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForDeleteDepartment() {
    const watcher = yield takeEvery( DELETE_DEPARTMENT, deleteDepartment );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForDeletePosition() {
    const watcher = yield takeEvery( DELETE_POSITION, deletePosition );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetChartData,
    watchNotify,
    watchForGetDepartment,
    watchForGetPosition,
    watchForDeleteDepartment,
    watchForDeletePosition,
    watchForReinitializePage
];
