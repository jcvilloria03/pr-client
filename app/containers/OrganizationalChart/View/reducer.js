import { fromJS } from 'immutable';
import {
    BTN_LOADING,
    DEFAULT_ACTION,
    LOADING,
    SET_CHART_DATA,
    SET_DEPARTMENT,
    SET_NOTIFICATION,
    SET_POSITION
} from './constants';

const initialState = fromJS({
    department: [],
    btnLoading: false,
    position: [],
    loading: false,
    chartData: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * OrganizationalChart reducer
 *
 */
function organizationalChartReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_DEPARTMENT:
            return state.set( 'department', fromJS( action.payload ) );
        case SET_POSITION:
            return state.set( 'position', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_CHART_DATA:
            return state.set( 'chartData', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default organizationalChartReducer;
