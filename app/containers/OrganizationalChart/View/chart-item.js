/* eslint-disable no-param-reassign */
/* eslint-disable require-jsdoc */

import { cloneDeep } from 'lodash';

export class ChartItems {
    constructor( array = []) {
        this.array = array;
    }

    getParentId( item ) {
        let parentId = item.parent_id;

        if ( item.type === 'position' ) {
            if ( item.parent_id ) {
                parentId = item.parent_id;
            } else {
                parentId = item.department_id;
            }
        }
        return parentId;
    }

    doesParentExist( parentId ) {
        const stack = cloneDeep( this.array );

        while ( stack.length > 0 ) {
            const item = stack.pop();

            if ( item.id === parentId ) {
                return true;
            }

            if ( item.children && item.children.length > 0 ) {
                stack.push( ...item.children );
            }
        }

        return false;
    }

    addChildById( parentId, childItem ) {
        const stack = [...this.array];

        while ( stack.length > 0 ) {
            const item = stack.pop();

            if ( item.id === parentId ) {
                childItem.hasIssue = false;
                item.children.push( childItem );
                return true;
            }

            if ( item.children && item.children.length > 0 ) {
                stack.push( ...item.children );
            }
        }

        return false;
    }

    addItem( item ) {
        const parentId = this.getParentId( item );

        if ( this.doesParentExist( parentId ) ) {
            this.addChildById( parentId, item );
        } else {
            this.array.push( item );
        }
    }

    moveItem( item ) {
        const parentId = this.getParentId( item );

        const itemToMove = this.array.find( ( it ) => it.id === item.id );
        const itemIndex = this.array.findIndex( ( it ) => it.id === item.id );

        this.addChildById( parentId, itemToMove );
        this.array.splice( itemIndex, 1 );
    }

    isParentIdPartOfChildren( currentItem, parentId ) {
        const stack = [];
        stack.push( currentItem );

        while ( stack.length > 0 ) {
            const item = stack.pop();

            if ( item.id === parentId ) {
                return true;
            }

            if ( item.children && item.children.length > 0 ) {
                stack.push( ...item.children );
            }
        }

        return false;
    }

    checkIssue() {
        const array = cloneDeep( this.array );

        array.forEach( ( item ) => {
            if ( item.hasIssue ) {
                const parentId = this.getParentId( item );

                if ( !this.isParentIdPartOfChildren( item, parentId ) ) {
                    if ( this.doesParentExist( parentId ) ) {
                        this.moveItem( item );
                    }
                }
            }
        });
    }

    toJSON() {
        return JSON.stringify( this.array );
    }

    getArray() {
        this.checkIssue();

        return this.array;
    }
}
