import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';
import axios from 'axios';
import { isEqual, cloneDeep, filter, concat } from 'lodash';

import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import { H3, H2, P } from 'components/Typography';
import A from 'components/A';
import Button from 'components/Button';
import Modal from 'components/Modal';
import Icon from 'components/Icon';

import { browserHistory } from 'utils/BrowserHistory';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { company } from 'utils/CompanyService';

import { ChartItems } from './chart-item';

import * as departmentsActions from './actions';

import {
    LoadingStyles,
    PageWrapper
} from './styles';
import {
    makeSelectOrganizationalChart,
    makeSelectNotification,
    makeSelectBtnLoad,
    makeSelectChartData
} from './selectors';

/**
 *
 * OrganizationalChart
 *
 */
export class OrganizationalChart extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        getChartData: React.PropTypes.func,
        deleteDepartment: React.PropTypes.func,
        deletePosition: React.PropTypes.func,
        btnLoading: React.PropTypes.bool,
        OrganizationalChartValue: React.PropTypes.object,
        chartData: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    };

    static defaultProps = {
        chartValue: []
    }

    constructor( props ) {
        super( props );

        this.state = {
            showRoot: false,
            showChildRoot: false,
            showChildToChildRoot: false,
            departments: [],
            positions: [],
            treeNodeValue: [],
            isDelete: {},
            hiddenChildren: []
        };

        this.confirmSaveModal = null;
    }

    componentWillMount() {
        this.props.getChartData();
    }
    componentDidUpdate( prevProps ) {
        if ( !isEqual( this.props.OrganizationalChartValue, prevProps.OrganizationalChartValue ) ) {
            this.fetchData();
        }
    }

    /**
     * Delete department List
    * @param {Number|String} id
    */
    // eslint-disable-next-line react/sort-comp
    destroyDepartment( id ) {
        return axios.delete( `/department/${id}` );
    }

    /**
   * Delete position List
   * @param {Number|String} id
   */
    destroyPosition( id ) {
        return axios.delete( `/position/${id}` );
    }

    fetchData() {
        if ( this.props.OrganizationalChartValue.chartData.departments && this.props.OrganizationalChartValue.chartData.positions ) {
            this.createChildNodes([ this.props.OrganizationalChartValue.chartData.departments, this.props.OrganizationalChartValue.chartData.positions ]);
        }
    }

    /**
      * Transform departments and position to Tree Chart graph.
      * @param {Array} values
      */
    createChildNodes( values ) {
        this.chartValue = this.buildTree([ ...values[ 0 ], ...values[ 1 ] ]);
    }
    /**
      * Prepare departments and positions for Tree Chart.
      * @param {Array} values
      */
    treeToChartNodes( values ) {
        return [ ...values[ 0 ], ...values[ 1 ] ]
            .map( ( node ) => {
                // eslint-disable-next-line no-prototype-builtins
                const type = node.hasOwnProperty( 'department_id' )
                    ? 'position'
                    : 'department';

                return {
                    id: node.id,
                    name: node.name,
                    type,
                    parent_id: node.parent_id,
                    department_id: node.department_id,
                    companyId: this.companyId,
                    children: []
                };
            });
    }

    /**
      * @param {Array} childs
      */
    getIndex( childs ) {
        return childs.reduce( ( result, child ) => {
            // eslint-disable-next-line no-param-reassign
            result[ `${child.type}-${child.id}` ] = child;
            return result;
        }, {});
    }

    /**
      * @param {Array} childs
      * @param {Object} valuesList
      */
    childersToParents( childs, valuesList ) {
        return childs.filter( ( node ) => {
            const parent = valuesList[ `${node.type}-${node.parent_id}` ] || valuesList[ `department-${node.department_id}` ];

            if ( parent ) {
                parent.children.push( node );
            }

            return !parent;
        });
    }

    buildTree( data ) {
        const chartItems = new ChartItems();

        for ( const item of data ) {
            // eslint-disable-next-line no-prototype-builtins
            const type = item.hasOwnProperty( 'department_id' )
                ? 'position'
                : 'department';

            chartItems.addItem({
                id: item.id,
                name: item.name,
                type,
                parent_id: item.parent_id,
                department_id: item.department_id,
                companyId: null,
                children: [],
                hasIssue: !!item.parent_id
            });
        }

        return chartItems.getArray();
    }

    handleAddClick=( data ) => {
        if ( data.type === 'department' ) {
            browserHistory.push( '/company-settings/company-structure/organizational-chart/departments/add', true );
        } else if ( data.type === 'position' ) {
            browserHistory.push( '/company-settings/company-structure/organizational-chart/positions/add', true );
        }
    }

    handleEditClick=( data ) => {
        if ( data.type === 'department' ) {
            browserHistory.push( `/company-settings/company-structure/organizational-chart/departments/${data.id}/edit`, true );
        } else if ( data.type === 'position' ) {
            browserHistory.push( `/company-settings/company-structure/organizational-chart/positions/${data.id}/edit`, true );
        }
    }

    handleDeleteClick=( data ) => {
        this.setState({ isDelete: data });
        this.confirmSaveModal.toggle();
    }

    modalDeleteClick=async () => {
        const ids = [];
        ids.push( this.state.isDelete.id );
        const companyId = company.getLastActiveCompanyId();
        const payload = { company_id: companyId, ids };

        if ( this.state.isDelete.type === 'department' ) {
            this.props.deleteDepartment( payload );
        } else if ( this.state.isDelete.type === 'position' ) {
            this.props.deletePosition( payload );
        }

        this.confirmSaveModal.toggle();
    }

    showHideChildren( parentId ) {
        let hiddenChildren = cloneDeep( this.state.hiddenChildren );

        if ( hiddenChildren.includes( parentId ) ) {
            hiddenChildren = filter( hiddenChildren, ( ch ) => ch !== parentId );
        } else {
            hiddenChildren = concat( hiddenChildren, parentId );
        }

        this.setState({ hiddenChildren });
    }

    TreeNode( node ) {
        return (
            <li className="tree-children" key={ node.id }>
                <div className="node-tree-wrap">
                    <span className={ node.type === 'department' ? 'tree-item tree-item-active' : 'tree-item' }>
                        <Icon name="hierarchy" />
                        {node.name}
                    </span>
                    <div className={ node.children && node.children.length ? 'tree-action' : 'tree-action treeNode' }>
                        { node.children && node.children.length ?
                            (
                                <A className="btn-links" onClick={ () => this.showHideChildren( node.id ) }>
                                    {this.state.hiddenChildren.includes( node.id )
                                        ? <Icon name="plusCircle" />
                                        : <Icon name="minusCircle" className="minus-list" />
                                    }
                                </A>
                            ) : <A></A>
                        }
                        <div className="btn-group-link">
                            <A onClick={ () => this.handleAddClick( node ) }>
                                <Icon name="plusCircle" />
                            </A>
                            <A onClick={ () => this.handleEditClick( node ) }>
                                <Icon name="pencil" />
                            </A>
                            <A onClick={ () => this.handleDeleteClick( node ) }>
                                <Icon name="trash" />
                            </A>
                        </div>
                    </div>

                    {node.hasIssue && (
                        <div className="warning-indicator">
                            <div>
                                <Icon name="warning" className="warning-icon" />
                            </div>
                            <div>
                                <P noBottomMargin className="tooltiptext">There is an issue with the reporting department/position</P>
                            </div>
                        </div>
                    )}
                </div>

                {!this.state.hiddenChildren.includes( node.id ) && node.children.length > 0 ?
                    (
                        <ul className="pl-0">
                            {node.children.map( ( x ) => this.TreeNode( x ) )}
                        </ul>
                    ) : null
                }
            </li>
        );
    }
    /**
     *
     * OrganizationalChart render method
     *
     */
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { OrganizationalChartValue, btnLoading } = this.props;

        return (
            <div>
                <Helmet
                    title="Organizational Chart"
                    meta={ [
                        { name: 'description', content: 'Description of Organizational Chart' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="Confirm Your Action"
                    body={
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.confirmSaveModal.toggle()
                        },
                        {
                            id: 'buttonCancel',
                            type: 'darkRed',
                            label: 'Yes',
                            onClick: () => this.modalDeleteClick()
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.confirmSaveModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <PageWrapper >
                    <Container>
                        {!btnLoading && OrganizationalChartValue.loading === true &&
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading Organizational Charts</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        }
                        {btnLoading &&
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Please wait...</H2>
                                </LoadingStyles>
                            </div>
                        }
                        {!btnLoading && OrganizationalChartValue.loading === false &&
                            <section className="content">
                                <header className="heading">
                                    <H3 noBottomMargin>Organizational Chart</H3>
                                    <P noBottomMargin>View and update your organizational structure. You can add,edit, or delete departments and positions.</P>
                                    <div style={ { textAlign: 'center' } } >
                                        <div className="main">
                                            <Button
                                                label="Add
                                                department"
                                                size="large"
                                                type="action"
                                                onClick={ () => { browserHistory.push( '/company-settings/company-structure/organizational-chart/departments/add', true ); } }
                                            />
                                            <Button
                                                label="Add
                                                position"
                                                size="large"
                                                type="action"
                                                onClick={ () => { browserHistory.push( '/company-settings/company-structure/organizational-chart/positions/add', true ); } }
                                            />
                                        </div>
                                    </div>
                                </header>
                                <div className="node-tree-main">
                                    <ul className="node-tree">
                                        {this.chartValue && this.chartValue.map( ( node ) => this.TreeNode( node ) )}
                                    </ul>
                                </div>
                            </section>
                        }
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    OrganizationalChartValue: makeSelectOrganizationalChart(),
    notification: makeSelectNotification(),
    btnLoading: makeSelectBtnLoad(),
    chartData: makeSelectChartData()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        departmentsActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( OrganizationalChart );
