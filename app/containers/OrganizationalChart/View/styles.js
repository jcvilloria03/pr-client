import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    .container{
        margin-top: 100px;
    }
    .loader {
        padding: 215px 0;
    }
    .bupwGM{
        background-color:#eb7575
    }
     .content {
        margin-top: 14px;
        margin-bottom: 40px;
        width: 100%;
        max-width: 85vw;
        display: flex;
        flex-direction: column;
        align-items: center;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: bold;
                font-size: 36px;
            }

            p {
                text-align: center;
                max-width: 800px;
                margin: 14px 0;
            }

            a {
                text-decoration: underline !important;
            }
        }
        .node-tree-main{
            width: 100%;
            .node-tree{
                position: relative;
                list-style: none;
                padding-left: 0px;
                margin:0;
                li:last-child:after{
                    height: 4.5rem;
                }
                &:before{
                    position: absolute;
                    top: -24px;
                    left: 0;
                    z-index: 1;
                    width: 2rem;
                    height: 2.5rem;
                    background-color: #fff;
                    content: "";
                }
                svg{
                    width: 1rem;
                    height: 1rem;
                }
                li{
                    position: relative;
                    box-sizing: border-box;
                    margin-left: 0;
                    padding-left: 0;
                    padding-top: 1rem;
                    &:before{
                        position: absolute;
                        top: 3rem;
                        left: 1.5rem;
                        height: 1px;
                        width: 3rem;
                        margin: auto;
                        border-bottom:1px dotted #ccc;
                        content: "";
                    }
                    &:after{
                        position: absolute;
                        top: -24px;
                        bottom: 0;
                        left: 1.5rem;
                        width: 1px;
                        height: 100%;
                        border-left: 1px dotted #ccc;
                        content: "";
                    }
                    ul{
                        li{
                            padding-left: 4rem;
                        }
                    }
                    .node-tree-wrap{
                        position: relative;
                        display: inline-block;
                        &:hover{
                            .btn-group-link{
                                display: inline-flex !important;
                            }
                        }
                        .tree-item-active{
                            background-color: #e5f6fc !important;
                        }
                        .tree-item{
                            position: relative;
                            z-index: 5;
                            display: inline-flex;
                            align-items: center;
                            padding: 1rem;
                            min-width: 9rem;
                            max-width: 15rem;
                            white-space: nowrap;
                            overflow: hidden;
                            text-overflow: ellipsis;
                            background-color: #f0f4f6;
                            border: 1px solid #adadad;
                            border-radius: .25rem;
                            font-size: 14px;
                            line-height: 22.4px;
                            svg{
                                margin-right: 1rem;
                            }
                        }
                        .treeNode{
                            .btn-group-link{
                                top: -8px !important;
                            }
                        }
                        .tree-action{
                            position: absolute;
                            bottom: -.9rem;
                            left: .6rem;
                            transform: none;
                            z-index: 5;
                            vertical-align: middle;
                            a.btn-links{
                                svg{
                                    background-color: #fff;
                                }
                            }
                            a{
                                padding: .25rem .5rem;
                                cursor: pointer;
                                font-size: .75rem;
                                text-transform: uppercase;
                                line-height: 1.6;
                                text-decoration: none;
                                vertical-align: middle;
                                border-radius: 2rem;
                                background-color: transparent;
                                border: 1px solid transparent;
                                text-align: center;
                                display: inline-block;
                                svg{
                                    width: .75rem;
                                    height: .75rem;
                                    fill: #83d24b;
                                }
                            }
                            .btn-group-link{
                                display: flex;
                                justify-content: flex-start;
                                align-items: stretch;
                                position: absolute;
                                right: -90px;
                                top: 0;
                                display:none;
                                a{
                                    color: #fff;
                                    background-color: #83d24b;
                                    padding: .25rem .5rem;
                                    border-radius:0;
                                    &:first-child{
                                        border-top-left-radius: 2rem;
                                        border-bottom-left-radius: 2rem;
                                    }
                                    &:last-child{
                                        border-top-right-radius: 2rem;
                                        border-bottom-right-radius: 2rem;
                                    }
                                    svg{
                                        width: .75rem;
                                        height: .75rem;
                                        fill: #fff;
                                    }
                                    &:hover,&:focus{
                                        background-color: #9fdc74 !important;
                                        box-shadow:none !important;
                                    }
                                }
                            }
                        }

                        .warning-indicator {
                            position: absolute;
                            right: -230px;
                            top: 6px;
                            display: flex;
                            width: 220px;

                            .warning-icon {
                                color: #eb7575;
                                margin-right: 12px;
                            }
                        }
                    }
                    ul{
                        list-style: none;
                        margin: 0;
                    }
                }
            }
        }
        .expired {
            width: 100%;
            text-align: center;
        }

        .header {
            font-weight: 600;
        }
    }
`;
