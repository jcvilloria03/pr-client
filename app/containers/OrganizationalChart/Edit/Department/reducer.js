import { fromJS } from 'immutable';
import {
    BTN_LOADING,
    DEFAULT_ACTION,
    LOADING,
    NOTIFICATION_SAGA,
    SET_DEPARTMENT,
    SET_EDIT_BULK_UPDATE,
    SET_ERRORS
} from './constants';

const initialState = fromJS({
    edit_BulkUpdate: [],
    loading: false,
    btnLoading: false,
    department: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    errors: {}
});

/**
 *
 * AddDepartment reducer
 *
 */
function addDepartmentReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_EDIT_BULK_UPDATE:
            return state.set( 'edit_BulkUpdate', action.payload );
        case SET_DEPARTMENT:
            return state.set( 'department', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default addDepartmentReducer;
