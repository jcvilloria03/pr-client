import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import { take, call, put, cancel } from 'redux-saga/effects';
import { get } from 'lodash';
import { Fetch } from '../../../../utils/request';
import { BTN_LOADING, EDIT_BULKUPDATE, GET_DEPARTMENT, LOADING, NOTIFICATION, NOTIFICATION_SAGA, SET_DEPARTMENT, SET_EDIT_BULK_UPDATE, SET_ERRORS } from './constants';
import { RECORD_UPDATED_MESSAGE } from '../../../../utils/constants';
import { formatFeedbackMessage } from '../../../../utils/functions';
import { browserHistory } from '../../../../utils/BrowserHistory';
import { resetStore } from '../../../App/sagas';
import { REINITIALIZE_PAGE } from '../../../App/constants';
import { company } from '../../../../utils/CompanyService';
/**
 * Individual exports for testing
 */

/**
 * Department fetch data
 */
export function* getDepartments() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const DepartmentValue = yield call( Fetch, `/company/${companyId}/departments?mode=DEFAULT_MODE` );

        yield put({
            type: SET_DEPARTMENT,
            payload: DepartmentValue && DepartmentValue.data || []
        });
    } catch ( error ) {
        yield call( notifyError, error.response );
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Create bulk
 */
export function* bulkUpdate({ payload }) {
    try {
        yield put({
            type: BTN_LOADING,
            payload: true
        });
        const storedDepartments = yield call( Fetch, '/department/bulk_update', { method: 'PUT', data: payload });
        yield put({ type: SET_EDIT_BULK_UPDATE, payload: storedDepartments || []});
        if ( storedDepartments.data.length >= 0 ) {
            browserHistory.push( '/company-settings/company-structure/organizational-chart', true );
        }
    } catch ( error ) {
        yield call( notifyError, error.response );
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getDepartments );
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.data.message ),
        type: 'error'
    };
    yield call( notifyUser, payload );
}

/**
 * Watcher for bulk create
 */
export function* watchForCreateBulkCreate() {
    const watcher = yield takeEvery( EDIT_BULKUPDATE, bulkUpdate );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetDepartment() {
    const watcher = yield takeEvery( GET_DEPARTMENT, getDepartments );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForCreateBulkCreate,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForGetDepartment
];
