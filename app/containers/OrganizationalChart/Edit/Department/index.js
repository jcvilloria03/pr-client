/* eslint-disable consistent-return */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import _ from 'lodash';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import Sidebar from 'components/Sidebar';
import { H3, H2 } from 'components/Typography';
import SnackBar from 'components/SnackBar';
import Modal from 'components/Modal';
import SalSelect from 'components/Select';
import Input from 'components/Input';
import A from 'components/A';
import Loader from 'components/Loader';
import Button from 'components/Button';
import Icon from 'components/Icon';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { auth } from 'utils/AuthService';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import * as addDepartmentAction from './actions';
import {
    makeSelectAddDepartment,
    makeSelectBtnLoad,
    makeSelectLoading,
    makeSelectNotification
} from './selectors';
import { PageWrapper, Footer, LoadingStyles, ModalAction } from './styles';

/**
 *
 * AddDepartment
 *
 */
export class AddDepartment extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        params: React.PropTypes.object,
        getDepartments: React.PropTypes.func,
        bulkUpdate: React.PropTypes.func,
        loading: React.PropTypes.bool,
        btnLoading: React.PropTypes.bool,
        AddDepartmentChart: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    constructor( props ) {
        super( props );
        this.state = {
            departments: [],
            departmentName: '',
            reportingDep: '',
            profile: null,
            editingDepartment: false,
            deleteDepartment: '',
            isNameAvailable: false,
            isNameAvailableOne: false,
            isEditDepartment: []
        };
        this.discardModal = null;
        this.companyID = company.getLastActiveCompanyId();
        this.editDepartmentId = +( this.props.params.id );
    }

    componentWillMount() {
        const profile = auth.getUser();
        this.setState({ profile });
        this.props.getDepartments();
    }

    componentWillReceiveProps( nextProps ) {
        if ( !nextProps.AddDepartmentChart.loading ) {
            this.setEditData();
        }
    }
    getDepartments( dep ) {
        if ( this.props.AddDepartmentChart.department.length === 0 ) {
            return [];
        }
        const departments = this.props.AddDepartmentChart.department.map( ( department ) => {
            if ( department.name !== ( dep && dep.name ) ) {
                return ({
                    value: department.id,
                    label: department.name,
                    disabled: false
                });
            }
            return ({
                value: department.id,
                label: department.name,
                disabled: true
            });
        });
        return departments;
    }

    // edit code
    setEditData=() => {
        const department = this.props.AddDepartmentChart.department.filter( ( dep ) => dep.id && dep.id === this.editDepartmentId );
        if ( department.length ) {
            this.setState({ isEditDepartment: department, departmentName: department[ 0 ].name, reportingDep: { value: department[ 0 ].parent && department[ 0 ].parent.id, label: department[ 0 ].parent && department[ 0 ].parent.name, disabled: true }});
        }
    }

    handleChangeAdd=( name, values ) => {
        const cloneEditDep = _.clone( this.state.isEditDepartment );
        if ( values === '' || values.length <= 0 ) {
            return this.setState({ isNameAvailableOne: true, departmentName: '', isNameAvailable: false });
        }
        const value = values.trim();
        const isName = this.props.AddDepartmentChart.department.filter( ( department ) => (
             ( ( department.name.toLowerCase() === value.toLowerCase() ) && ( cloneEditDep[ 0 ].name !== department.name ) ) ? department : '' ) );
        if ( isName.length <= 0 ) {
            this.setState({ departmentName: value, isNameAvailable: false, isNameAvailableOne: false });
        } else {
            this.setState({ departmentName: value, isNameAvailable: true, isNameAvailableOne: false });
        }
    }

    // Submit Button
    handleSubmit = () => {
        const companyId = company.getLastActiveCompanyId();
        const newParent = this.props.AddDepartmentChart.department.find( ( dep ) => dep.id === ( this.state.reportingDep && this.state.reportingDep.value && this.state.reportingDep.value ) );
        const isEditDepartment = this.state.isEditDepartment.map( ( dep ) => {
            if ( dep.id === this.editDepartmentId ) {
                return {
                    ...dep,
                    name: this.state.departmentName,
                    parent: newParent !== undefined ? newParent : null,
                    parent_id: newParent !== undefined ? newParent.id : null
                };
            }
            return dep;
        });

        const payload = { company_id: companyId, data: isEditDepartment };
        this.props.bulkUpdate( payload );
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { departments, isNameAvailable, isNameAvailableOne } = this.state;
        const { loading, btnLoading } = this.props;
        return (
            <div>
                <Helmet
                    title="Edit Department"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Department' }
                    ] }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p></ModalAction> }
                    buttons={
                    [
                        {
                            label: 'Stay on this page',
                            type: 'grey',
                            onClick: () => this.discardModal.toggle()
                        },
                        {
                            label: 'Discard',
                            type: 'darkRed',
                            onClick: () => browserHistory.push( '/company-settings/company-structure/organizational-chart', true )
                        }

                    ] }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <PageWrapper>
                    <Container className="position-container">
                        {!btnLoading && loading === false &&
                            <div className="nav">
                                <Container>
                                    <A
                                        href
                                        onClick={ ( e ) => {
                                            e.preventDefault();
                                            browserHistory.push( '/company-settings/company-structure/organizational-chart', true );
                                        } }
                                    >
                                        <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Organizational Chart</span>
                                    </A>
                                </Container>
                            </div>
                        }
                        {!btnLoading && loading === true &&
                            <div className="loader" style={ { display: loading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Loading Edit Department.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        }
                        {btnLoading === true &&
                            <div className="loader" style={ { display: btnLoading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Please wait...</H2>
                                </LoadingStyles>
                            </div>
                        }
                        {!btnLoading && loading === false &&
                            <div className="main_container">
                                <section className="content">
                                    <header className="heading">
                                        <H3>Edit Department</H3>
                                        <p>A department should have a reporting to department. In this manner, organizational hierarchy structure will be determined.</p>
                                    </header>
                                </section>
                                <section className="content">
                                    <div className="row department_section">
                                        <div className="col-md-12">
                                            <div className="col-md-4">
                                                <div className="department_namber">
                                                    <span>Department {departments.length + 1}</span>
                                                </div>
                                            </div>
                                            <div className={ isNameAvailable || isNameAvailableOne ? 'col-md-4 invalid_content department-name-input' : 'col-md-4 department-name-input' }>
                                                <Input
                                                    id="department_name"
                                                    label="* Department name"
                                                    key="department_name"
                                                    value={ this.state.departmentName }
                                                    onChange={ ( value ) => this.handleChangeAdd( 'departmentName', value ) }
                                                />
                                                {isNameAvailable ? <span>Department name already exists</span> : ''}
                                                {isNameAvailableOne ? <span>Please input valid Department name</span> : ''}
                                            </div>
                                            <div className="col-md-4 schdule_box">
                                                <SalSelect
                                                    id="reporting_department"
                                                    label={
                                                        <p className="schdule_tooltip">Reporting Department
                                                            <span className="bg_tooltip">
                                                                <span>?</span>
                                                                <span className="tooltiptext"> Department that reports to the<br /> specified Department Name.</span>
                                                            </span>
                                                        </p>
                                                    }
                                                    key="reporting_department"
                                                    data={ this.getDepartments( this.state.isEditDepartment[ 0 ]) }
                                                    value={ this.state.reportingDep }
                                                    placeholder="Choose an option"
                                                    ref={ ( ref ) => { this.reportingDep = ref; } }
                                                    onChange={ ( value ) => {
                                                        this.setState({ reportingDep: value });
                                                    } }
                                                    clearable
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-2">
                                        </div>
                                    </div>
                                </section>
                            </div>
                        }
                    </Container>
                </PageWrapper>
                {!btnLoading && loading === false &&
                    <Footer>
                        <div className="submit">
                            <div className="col-xs-12">
                                <Button
                                    label="Cancel"
                                    type="action"
                                    size="large"
                                    alt
                                    onClick={ () => this.discardModal.toggle() }
                                />
                                <Button
                                    type="action"
                                    size="large"
                                    label={ btnLoading ? <Loader /> : 'Update' }
                                    disabled={ btnLoading || isNameAvailable || isNameAvailableOne }
                                    onClick={ () => this.handleSubmit() }
                                />
                            </div>
                        </div>
                    </Footer>
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    AddDepartmentChart: makeSelectAddDepartment(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    btnLoading: makeSelectBtnLoad()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { ...addDepartmentAction },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddDepartment );
