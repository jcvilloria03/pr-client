import {
    DEFAULT_ACTION,
    NOTIFICATION,
    EDIT_BULKUPDATE,
    GET_DEPARTMENT
} from './constants';

/**
 *
 * AddDepartment actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * create bulk create
 * @param {object} payload
 */
export function bulkUpdate( payload ) {
    return {
        type: EDIT_BULKUPDATE,
        payload
    };
}

/**
 * Department Request Type
 * @param {object} payload
 */
export function getDepartments() {
    return {
        type: GET_DEPARTMENT
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
