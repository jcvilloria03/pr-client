import { createSelector } from 'reselect';

/**
 * Direct selector to the addDepartment state domain
 */
const selectAddDepartmentDomain = () => ( state ) => state.get( 'organizationalEditDepartments' );

/**
 * Other specific selectors
 */
const makeSelectData = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'department' ).toJS()
);

const makeSelectNameApiResponse = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'newDepartment' )
);

const makeSelectBulkCreate = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'bulkCreate' )
);

const makeSelectLoading = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectBtnLoad = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

const makeSelectNotification = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

/**
 * Default selector used by AddDepartment
 */
const makeSelectAddDepartment = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectAddDepartment,
  makeSelectNameApiResponse,
  makeSelectBulkCreate,
  makeSelectLoading,
  makeSelectNotification,
  selectAddDepartmentDomain,
  makeSelectBtnLoad,
  makeSelectData
};
