import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
    height: 100%;
`;

export const Header = styled.div`
    .nav{
        padding:10px 10px;
        background:#f0f4f6;
        margin-bottom:10px;
        padding-top:80px;
        position: fixed;
        top: 0;
        right: 0;
        left: 0;
        z-index: 9;
    }
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 6vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

export const ModalAction = styled.div`
    font-size:14px;
    padding-bottom:16px;

`;
export const PageWrapper = styled.div`
    height: calc(100vh - 0px);
    .position-container{
        height: 100%;
    }
    .loader {
        height: 100%;
    }
    .nav{
        padding:10px 10px;
        background:#f0f4f6;
        margin-bottom:10px;
        padding-top:60px;
        width: 100%;
        position: fixed;
        top: 25px;
        right: 0;
        left: 0;
        z-index: 9;

        .icon-arrow {
            width: 14px;
            font-size: 14px;
            display: inline-block;
            margin-right: 4px;

            > i {
                align-self: center;
            }
        }

        .back-text {
            font-size: 14px;
        }
    }
    .schdule_tooltip{
        display: flex;
        align-items: center;
        font-size: 14px;
        margin-bottom:0px;
        color:#5b5b5b;
        button{
            width: 20px;
            height: 20px;
            padding: 0;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
        }
        a{
            padding: 0;
            border-bottom: none;
            padding-left: 6px;
        }
        .bg_tooltip {
            position: relative;
            display: inline-block;
            width:20px;
            height:20px;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
            margin-left: 6px;
            text-align: center;
            p{
                font-size: 15px;
                font-weight: bold;
                color: #adadad;
            }
        }

        .bg_tooltip .tooltiptext {
            visibility: hidden;
            width: 242px;
            background-color: #fff;
            color: #03a9f4;
            border: 2px solid #b1dcf0;
            text-align: left;
            border-radius: 6px;
            padding: 5px 16px;
            position: absolute;
            left: 26px;
            top: -50%;
            transform: translateY(-15%);
            z-index: 1;
        }
        .bg_tooltip .tooltiptextStart{
            transform: translateY(-26%);
        }

        .bg_tooltip .tooltiptextEnd{
            transform: translateY(-20%);
            width: 262px;
        }
        .bg_tooltip .tooltiptextHours{
            transform: translateY(-17%);
            width: 255px;
        }
        .bg_tooltip:hover .tooltiptext {
            visibility: visible;
        }
        .bg_tooltip .tooltiptext::after {
            content: " ";
            position: absolute;
            top: 50%;
            right: 99%;
            margin-top: -5px;
            border: solid #b1dcf0;
            border-width: 0 2px 2px 0;
            display: inline-block;
            padding: 4px;
            transform: rotate(135deg);
            background-color: #ffff;
        }
    }
    .main_container{
        min-width: 80vw;
        margin:auto;
        height:100%;
        margin-top: 150px;
        .dept-content{
            margin-bottom:70px !important;
        }
        .content {
            margin: 40px 0;
            padding: 0 20px;
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            span{
                font-size:14px;
            }
            .heading {
                display: flex;
                align-items: center;
                flex-direction: column;
                margin: 0 0 30px ;
                width:100%;

                h3 {
                    font-weight: 700;
                    font-size: 36px;
                }

                p {
                    text-align: center;
                    max-width: 800px;
                    font-size:14px;
                }

                a {
                    text-decoration: underline !important;
                }
            }

            .expired {
                width: 100%;
                text-align: center;
            }

            .header {
                font-weight: 600;
            }
            .department_editor{
                width:100%;
                display: flex;
                align-items: end;
                margin-bottom:10px;
                .deprFirst{
                    display: flex;
                    align-items: end;
                }
                .department_edit{
                    .edit_btn{
                        color: #474747;
                        border-color: #83D24B;
                        background-color: #ffffff;
                        width:45%;
                        margin: 0;
                    }
                    .delete_btn{
                        color: #474747;
                        border-color: #EB7575;
                        background-color: #ffffff;
                        width:45%;
                    }
                    .btn_save{
                        background-color:#83d24b;
                        color:#fff;
                    }
                    svg{
                        width: 14px;
                    }
                }
            }
            .department_section{
                width:100%;
                .department_namber{
                    padding-top: 2.5rem;
                }
                .department_btn{
                    button{
                        color: #474747;
                        border-color: #83d24b;
                        background-color: #fff;
                        width: 100%;
                        margin-top: 2rem;
                        padding: 0.5rem;
                        svg{
                            width: 14px;
                        }
                    }
                }
                .schdule_box{
                    .RsTJf{
                        display: flex;
                        flex-wrap: wrap;
                    }
                }

                .department-name-input {
                    margin-top: 3px;
                }
            }
        }

        input:focus~label,.Select.is-focused~label{
            color:#5b5b5b !important;
        }
    }
    .invalid_content{
        color:#eb7575;
        input{
            border-color: #eb7575 !important;
        }
        span{
            color:#eb7575;
        }
        label{
            color: #eb7575 !important;
        }
    }
`;

