/* eslint-disable consistent-return */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import _ from 'lodash';

import { createStructuredSelector } from 'reselect';

import Sidebar from 'components/Sidebar';
import { H3, H2 } from 'components/Typography';
import A from 'components/A';
import SnackBar from 'components/SnackBar';
import SalSelect from 'components/Select';
import Input from 'components/Input';
import Modal from 'components/Modal';
import Loader from 'components/Loader';
import Button from 'components/Button';
import Icon from 'components/Icon';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';

import {
    makeSelectNotification,
    makeSelectAddPosition,
    makeSelectData,
    makeSelectLoading,
    makeSelectBtnLoad
} from './selectors';
import * as addPostion from './actions';
import { PageWrapper, Footer, LoadingStyles, ModalAction } from './styles';

/**
 *
 * AddPosition
 *
 */
export class AddPosition extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        params: React.PropTypes.object,
        getDepartments: React.PropTypes.func,
        getPositions: React.PropTypes.func,
        bulkUpdate: React.PropTypes.func,
        btnLoading: React.PropTypes.bool,
        AddDepartment: React.PropTypes.array,
        loading: React.PropTypes.bool,
        PositionAll: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    };

    constructor( props ) {
        super( props );

        this.state = {
            isNameAvailable: false,
            isNameAvailableOne: false,
            positionName: '',
            repoPosition: '',
            repoDepartment: '',
            positions: [],
            deletePosition: null,
            isEditPosition: false,
            EditPosition: []
        };
        this.discardModal = null;
        this.cancelOption = '';
        this.editPositionId = +( this.props.params.id );
    }

    componentWillMount() {
        this.props.getDepartments();
        this.props.getPositions();
    }
    componentWillReceiveProps( nextProps ) {
        if ( !nextProps.PositionAll.loading ) {
            this.setEditData();
        }
    }
    getDepartments( pos ) {
        if ( this.props.AddDepartment.length === 0 ) {
            return [];
        }
        const departments = this.props.AddDepartment.map( ( department ) => {
            if ( department.id !== ( pos && pos.id ) ) {
                return ({
                    value: department.id,
                    label: department.name,
                    disabled: false
                });
            }
            return ({
                value: department.id,
                label: department.name,
                disabled: true
            });
        });
        return departments;
    }

    getPostions( pos ) {
        if ( this.props.PositionAll.position.length === 0 ) {
            return [];
        }
        const positions = this.props.PositionAll.position.map( ( position ) => {
            if ( position.name !== ( pos && pos.name ) ) {
                return ({
                    value: position.id,
                    label: position.name,
                    disabled: false
                });
            }
            return ({
                value: position.id,
                label: position.name,
                disabled: true
            });
        });
        return positions;
    }

    setEditData=() => {
        const position = this.props.PositionAll.position.filter( ( pos ) => pos.id && pos.id === this.editPositionId );
        if ( position.length ) {
            const repoortingPosition = this.props.PositionAll.position.filter( ( pos ) => pos.id === position[ 0 ].parent_id && position[ 0 ].parent_id );
            const reportingDepartment = this.props.PositionAll.department.filter( ( dep ) => dep.id === position[ 0 ].department_id && position[ 0 ].department_id );
            if ( repoortingPosition.length ) {
                this.setState({
                    repoPosition: { value: repoortingPosition[ 0 ] && repoortingPosition[ 0 ].id, label: repoortingPosition[ 0 ] && repoortingPosition[ 0 ].name, disabled: true }
                });
            }
            if ( reportingDepartment.length ) {
                this.setState({
                    repoDepartment: { value: reportingDepartment[ 0 ] && reportingDepartment[ 0 ].id, label: reportingDepartment[ 0 ] && reportingDepartment[ 0 ].name, disabled: true }
                });
            }
            this.setState({ EditPosition: position, positionName: position[ 0 ].name });
        }
    }

    handleChangeAdd=( name, values ) => {
        const cloneEditPos = _.clone( this.state.EditPosition );
        if ( values === '' || values.length <= 0 ) {
            return this.setState({ isNameAvailableOne: true, positionName: '', isNameAvailable: false });
        }
        const value = values.trim();
        const isName = this.props.PositionAll.position.filter( ( position ) => ( ( ( position.name.toLowerCase() === value.toLowerCase() ) && ( cloneEditPos[ 0 ].name !== position.name ) ) ? position : '' ) );
        if ( isName.length <= 0 ) {
            this.setState({ positionName: value, isNameAvailable: false, isNameAvailableOne: false });
        } else {
            this.setState({ positionName: value, isNameAvailable: true, isNameAvailableOne: false });
        }
    }

    // submit button
    handleSubmit= () => {
        const companyId = company.getLastActiveCompanyId();
        const reportingDepartment = this.props.AddDepartment.find( ( dep ) => ( ( this.state.repoDepartment && this.state.repoDepartment.value && this.state.repoDepartment.value ) === dep.id ? dep : null ) );
        const reportingPosition = this.props.PositionAll.position.find( ( pos ) => ( ( this.state.repoPosition && this.state.repoPosition.value && this.state.repoPosition.value ) === pos.id ? pos : null ) );
        this.state.EditPosition = this.state.EditPosition.map( ( pos ) => {
            if ( pos.id === this.editPositionId ) {
                return {
                    ...pos,
                    name: this.state.positionName,
                    parent_id: reportingPosition !== undefined ? reportingPosition.id : null,
                    department_id: reportingDepartment !== undefined ? reportingDepartment.id : null,
                    reporting_department: reportingDepartment !== undefined ? reportingDepartment : null,
                    reporting_position: reportingPosition !== undefined ? reportingPosition : null
                };
            }
            return pos;
        });

        const payload = { company_id: companyId, data: this.state.EditPosition };
        this.props.bulkUpdate( payload );
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { positions, isNameAvailable, isNameAvailableOne } = this.state;
        const { loading, btnLoading } = this.props;
        return (
            <div>
                <Helmet
                    title="Edit Position"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Position' }
                    ] }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p></ModalAction> }
                    buttons={ [
                        {
                            label: 'Stay on this page',
                            type: 'grey',
                            onClick: () => this.discardModal.toggle()
                        },
                        {
                            label: 'Discard',
                            type: 'darkRed',
                            onClick: () => browserHistory.push( '/company-settings/company-structure/organizational-chart', true )
                        }
                    ] }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <PageWrapper>
                    <Container className="edit-dept-container">
                        {!btnLoading && loading === false &&
                            <div className="nav">
                                <Container>
                                    <A
                                        href
                                        onClick={ ( e ) => {
                                            e.preventDefault();
                                            browserHistory.push( '/company-settings/company-structure/organizational-chart', true );
                                        } }
                                    >
                                        <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Organizational Chart</span>
                                    </A>
                                </Container>
                            </div>
                        }
                        {!btnLoading && loading === true &&
                            <div className="loader" style={ { display: loading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Loading Edit Position.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        }
                        {btnLoading === true &&
                            <div className="loader" style={ { display: btnLoading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Please wait...</H2>
                                </LoadingStyles>
                            </div>
                        }
                        {!btnLoading && loading === false &&
                            <div className="main_container">
                                <section className="content">
                                    <header className="heading">
                                        <H3>Edit Position</H3>
                                        <p>A position should have a lead position to report to. In this manner, a position will also be tagged with a department so that organizational hierarchy structure will be determined.</p>
                                    </header>
                                </section>
                                <div className="brecks_title">
                                    <div className="row schedule_section">
                                        <div>
                                            <div className="col-md-12 schedule_main">
                                                <div className=" col-md-3 schedule_main schedule_add" ><p>Position {positions.length + 1}</p></div>
                                                <div className={ isNameAvailable || isNameAvailableOne ? 'col-md-3 schedule_main position-name schedule_main_delet' : 'col-md-3 schedule_main position-name' }>
                                                    <Input
                                                        id="postion_name"
                                                        label="* Position name"
                                                        key="position_name"
                                                        value={ this.state.positionName }
                                                        onChange={ ( value ) => this.handleChangeAdd( 'positionName', value ) }
                                                    />
                                                    {isNameAvailable ? <span>Position name already exists</span> : ''}
                                                    {isNameAvailableOne ? <span>Please input valid Position name</span> : ''}
                                                </div>
                                                <div className="col-md-3 schdule_box">
                                                    <SalSelect
                                                        id="reporting_position"
                                                        label={
                                                            <p className="schdule_tooltip">Reporting Position
                                                                <span className="bg_tooltip">
                                                                    <span>?</span>
                                                                    <span className="tooltiptext">Job title of a person who supervises or manages someone who holds a Position.<br /> specified Department Name.</span>
                                                                </span>
                                                            </p>
                                                        }
                                                        key="reporting_position"
                                                        data={ this.getPostions( this.state.EditPosition[ 0 ]) }
                                                        value={ this.state.repoPosition }
                                                        placeholder="Choose an option"
                                                        ref={ ( ref ) => { this.repoPosition = ref; } }
                                                        onChange={ ( value ) => { this.setState({ repoPosition: value }); } }
                                                        clearable
                                                    />
                                                </div>
                                                <div className="col-md-3 schdule_box">
                                                    <SalSelect
                                                        id="position_reporting_department"
                                                        label={
                                                            <p className="schdule_tooltip">Reporting Department
                                                                <span className="bg_tooltip">
                                                                    <span>?</span>
                                                                    <span className="tooltiptext">Name of a Department that someone with a Position belongs to.<br /> specified Department Name.</span>
                                                                </span>
                                                            </p>
                                                        }
                                                        key="reporting_department"
                                                        data={ this.getDepartments() }
                                                        value={ this.state.repoDepartment }
                                                        placeholder="Choose an option"
                                                        ref={ ( ref ) => { this.repoDepartment = ref; } }
                                                        onChange={ ( value ) => { this.setState({ repoDepartment: value }); } }
                                                        clearable
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }
                    </Container>
                </PageWrapper>
                {!btnLoading && loading === false &&
                    <Footer>
                        <div className="submit">
                            <div className="col-xs-12">
                                <Button
                                    label="Cancel"
                                    type="action"
                                    size="large"
                                    alt
                                    onClick={ () => this.discardModal.toggle() }
                                />
                                <Button
                                    type="action"
                                    size="large"
                                    label={ btnLoading ? <Loader /> : 'Update' }
                                    disabled={ btnLoading || isNameAvailable || isNameAvailableOne }
                                    onClick={ () => this.handleSubmit() }
                                />
                            </div>
                        </div>
                    </Footer>
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    AddDepartment: makeSelectData(),
    PositionAll: makeSelectAddPosition(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    btnLoading: makeSelectBtnLoad()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { ...addPostion },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddPosition );
