import styled from 'styled-components';

export const Header = styled.div`
    .nav{
        padding:10px 10px;
        background:#f0f4f6;
        margin-bottom:10px;
        padding-top:80px;
    }
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 6vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

export const ModalAction = styled.div`
    font-size:14px;
    padding-bottom:16px;
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
    height: 100%;
`;
export const PageWrapper = styled.div`
    height: calc(100vh - 0px);
    .edit-dept-container{
        height: 100%;
    }
    .loader {
        height: 100%;
    }
    .nav{
        padding:10px 10px;
        background:#f0f4f6;
        margin-bottom:10px;
        padding-top:60px;
        width: 100%;
        position: fixed;
        top: 25px;
        right: 0;
        left: 0;
        z-index: 9;

        .icon-arrow {
            width: 14px;
            font-size: 14px;
            display: inline-block;
            margin-right: 4px;

            > i {
                align-self: center;
            }
        }

        .back-text {
            font-size: 14px;
        }
    }
    .schdule_tooltip{
        display: flex;
        align-items: center;
        font-size: 14px;
        margin-bottom:0px;
        color:#5b5b5b;
        button{
            width: 20px;
            height: 20px;
            padding: 0;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
        }
        a{
            padding: 0;
            border-bottom: none;
            padding-left: 6px;
        }
        .bg_tooltip {
            position: relative;
            display: inline-block;
            width:20px;
            height:20px;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
            margin-left: 6px;
            text-align: center;
            p{
                font-size: 15px;
                font-weight: bold;
                color: #adadad;
            }
        }

        .bg_tooltip .tooltiptext {
            visibility: hidden;
            width: 242px;
            background-color: #fff;
            color: #03a9f4;
            border: 2px solid #b1dcf0;
            text-align: left;
            border-radius: 6px;
            padding: 5px 16px;
            position: absolute;
            left: 26px;
            top: -50%;
            transform: translateY(-15%);
            z-index: 1;
        }
        .bg_tooltip .tooltiptextStart{
            transform: translateY(-26%);
        }

        .bg_tooltip .tooltiptextEnd{
            transform: translateY(-20%);
            width: 262px;
        }
        .bg_tooltip .tooltiptextHours{
            transform: translateY(-17%);
            width: 255px;
        }
        .bg_tooltip:hover .tooltiptext {
            visibility: visible;
        }
        .bg_tooltip .tooltiptext::after {
            content: " ";
            position: absolute;
            top: 50%;
            right: 99%;
            margin-top: -5px;
            border: solid #b1dcf0;
            border-width: 0 2px 2px 0;
            display: inline-block;
            padding: 4px;
            transform: rotate(135deg);
            background-color: #ffff;
        }
    }
    .main_container{
        margin:auto;
        height:100%;
        margin-top: 150px;
        .content {
            margin: 40px 0;
            padding: 0 20px;
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;

            .heading {
                display: flex;
                align-items: center;
                flex-direction: column;
                margin: 0 0 0px ;
                width:100%;

                h3 {
                    font-weight: 700;
                    font-size: 36px;
                }

                p {
                    text-align: center;
                    font-size: 14px;
                }

                a {
                    text-decoration: underline !important;
                }
            }

        }

        input:focus~label,.Select.is-focused~label{
            color:#5b5b5b !important;
        }
    }
    .brecks_title .breck_button button{
        width: 100%;
        padding: 7px;
        border-radius: 2rem;
        &:focus{
            outline:none;
        }
    }
    .schedule_section{
        margin: 64px - 15px;
        .schdule_box{
            .RsTJf{
                display: flex;
                flex-wrap: wrap;
            }
        }
    }
    .schedule_main{
        padding: 0px 15px 14px;
        p{
            font-size:14px;
        }
        .break_type{
            font-size:14px;
        }
        span{
            font-size:14px;
        }
        .schedule_timeSect{
            input{
                width: 100%;
                height: 46px;
                border: 1px solid #95989a;
                &:focus{
                    outline:none;
                }
            }
        }
    }
    .position-name{
        margin-top: 3px;
        select{
            border: 1px solid #95989a;
            height: 46px;
            width: 100%;
            &:focus-visible{
                outline:none;
            }
        }
    }

    .schedule_main_delet{
        color:#eb7575;
        input{
            border-color: #eb7575 !important;
        }
        span{
            color:#eb7575;
        }
        label{
            color: #eb7575 !important;
        }
    }
    .schedule_add{
        padding: 30px 15px 14px !important;
    }
    .schedule_save{
        padding: 30px 0px 14px !important;
    }
    .btn_section{
        .edit_btn{
            color: #474747;
            border-color: #83d24b;
            background-color: #fff;
            width: 50%;
            margin: 0px;
            svg{
                width: 1rem;
                height: 1rem;
            }
            &:active:focus{
                color: #474747;
                border-color: #83d24b;
                background-color: unset;
            } 
        }
        .delete_btn{
            color: #474747;
            border-color: #eb7575;
            background-color: #fff;
            width: 50%;
            margin: 0px;
            svg{
                width: 1rem;
                height: 1rem;
            }
            &:active:focus{
                color: #474747;
                border-color: #eb7575;
                background-color: unset;
            } 
        }
        .btn_save{
            color: #fff !important;
            background-color: #83d24b;
            border-color: #83d24b;
            width: 50%;
            margin: 0px;
            &:hover{
                color: #fff;
                background-color: #83d24b;
                border-color: #83d24b;
            }
        }
    }
`;

