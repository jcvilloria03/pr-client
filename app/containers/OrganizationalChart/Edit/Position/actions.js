import {
    EDIT_BULKPOSITION,
    DEFAULT_ACTION,
    NOTIFICATION,
    GET_POSITION,
    GET_DEPARTMENT
} from './constants';

/**
 *
 * AddPosition actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * Create Bulk Position
 *
 */
export function bulkUpdate( payload ) {
    return {
        type: EDIT_BULKPOSITION,
        payload
    };
}

/**
 * Department Request Type
 * @param {object} payload
 */
export function getDepartments() {
    return {
        type: GET_DEPARTMENT
    };
}

/**
 * Department Request Type
 * @param {object} payload
 */
export function getPositions() {
    return {
        type: GET_POSITION
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
