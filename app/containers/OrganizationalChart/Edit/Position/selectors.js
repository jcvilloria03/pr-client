import { createSelector } from 'reselect';

/**
 * Direct selector to the addPosition state domain
 */
const selectAddPositionDomain = () => ( state ) => state.get( 'editPosition' );

/**
 * Other specific selectors
 */

const makeSelectNotification = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectPositionData = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'position' ).toJS()
);

const makeSelectData = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'department' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectBtnLoad = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

const makeSelectDepLoading = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.get( 'dep_loading' )
);

/**
 * Default selector used by AddPosition
 */
const makeSelectAddPosition = () => createSelector(
  selectAddPositionDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectAddPosition,
  makeSelectNotification,
  makeSelectPositionData,
  makeSelectLoading,
  makeSelectDepLoading,
  selectAddPositionDomain,
  makeSelectBtnLoad,
  makeSelectData
};
