/*
 *
 * AddPosition constants
 *
 */

export const namespace = 'app/containers/OrganizationalChart/Edit/Position';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const BTN_LOADING = `${namespace}/BTN_LOADING`;
export const DEP_LOADING = `${namespace}/DEP_LOADING`;

export const EDIT_BULKPOSITION = `${namespace}/EDIT_BULKPOSITION`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const SET_ERRORS = `${namespace}/SET_ERRORS`;

export const SET_BULKCREATE = `${namespace}/SET_CREATE_BULKCREATE`;
export const SET_EDIT_BULKUPDATE = `${namespace}/SET_EDIT_BULKUPDATE`;

export const SET_POSITION = `${namespace}/SET_POSITION`;
export const GET_POSITION = `${namespace}/GET_POSITION`;

export const SET_DEPARTMENT = `${namespace}/SET_DEPARTMENT`;
export const GET_DEPARTMENT = `${namespace}/GET_DEPARTMENT`;
