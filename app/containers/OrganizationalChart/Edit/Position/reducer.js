import { fromJS } from 'immutable';
import {
    BTN_LOADING,
    DEFAULT_ACTION, DEP_LOADING, LOADING, NOTIFICATION_SAGA, SET_DEPARTMENT, SET_EDIT_BULKUPDATE, SET_ERRORS, SET_POSITION
} from './constants';

const initialState = fromJS({
    createBulk: [],
    EditBulk: [],
    loading: false,
    dep_loading: false,
    department: [],
    btnLoading: false,
    position: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    errors: {}
});

/**
 *
 * AddPosition reducer
 *
 */
function addPositionReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case DEP_LOADING:
            return state.set( 'dep_loading', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_DEPARTMENT:
            return state.set( 'department', fromJS( action.payload ) );
        case SET_EDIT_BULKUPDATE:
            return state.set( 'EditBulk', fromJS( action.payload ) );
        case SET_POSITION:
            return state.set( 'position', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default addPositionReducer;
