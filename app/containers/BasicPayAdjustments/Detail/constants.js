export const INITIALIZE = 'app/BasicPayAdjustments/Detail/INITIALIZE';
export const NOTIFICATION = 'app/BasicPayAdjustments/Detail/NOTIFICATION';

export const SET_LOADING = 'app/BasicPayAdjustments/Detail/SET_LOADING';
export const SET_EMPLOYEE = 'app/BasicPayAdjustments/Detail/SET_EMPLOYEE';
export const SET_BASIC_PAY_ADJUSTMENT = 'app/BasicPayAdjustments/Detail/SET_BASIC_PAY_ADJUSTMENT';
export const SET_NOTIFICATION = 'app/BasicPayAdjustments/Detail/SET_NOTIFICATION';
