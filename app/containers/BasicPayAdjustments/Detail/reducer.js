import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_EMPLOYEE,
    SET_BASIC_PAY_ADJUSTMENT,
    SET_NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    employee: {},
    basicPayAdjustment: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Basic pay adjustment detail reducer
 *
 */
function basicPayAdjustmentDetail( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_EMPLOYEE:
            return state.set( 'employee', fromJS( action.payload ) );
        case SET_BASIC_PAY_ADJUSTMENT:
            return state.set( 'basicPayAdjustment', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default basicPayAdjustmentDetail;
