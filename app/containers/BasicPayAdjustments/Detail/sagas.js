import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { Fetch } from '../../../utils/request';

import {
    INITIALIZE,
    NOTIFICATION,
    SET_LOADING,
    SET_EMPLOYEE,
    SET_BASIC_PAY_ADJUSTMENT,
    SET_NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const employee = yield call( Fetch, `/employee/${payload.employeeId}`, { method: 'GET' });
        const basicPayAdjustment = yield call( Fetch, `/basic_pay_adjustment/${payload.adjustmentId}`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE,
            payload: employee
        });

        yield put({
            type: SET_BASIC_PAY_ADJUSTMENT,
            payload: basicPayAdjustment
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForReinitializePage,
    watchNotify
];
