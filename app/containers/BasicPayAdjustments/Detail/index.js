import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../components/A';
import Button from '../../../components/Button';
import Loader from '../../../components/Loader';
import SnackBar from '../../../components/SnackBar';

import {
    makeSelectLoading,
    makeSelectEmployee,
    makeSelectBasicPayAdjustment,
    makeSelectNotification
} from './selectors';

import {
    NavWrapper,
    PageWrapper,
    FormWrapper,
    MainWrapper,
    ValueWrapper,
    HeadingWrapper
} from './styles';

import * as addBasicPayAdjustmentActions from './actions';

import { DATE_FORMATS } from '../../../utils/constants';
import { browserHistory } from '../../../utils/BrowserHistory';
import { getEmployeeFullName, formatDate, formatCurrency } from '../../../utils/functions';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { company } from '../../../utils/CompanyService';

const BASE_PAY_UNIT = [
    { value: 'per_hour', label: 'Per Hour' },
    { value: 'per_day', label: 'Per Day' },
    { value: 'per_month', label: 'Per Month' },
    { value: 'per_year', label: 'Per Year' }
];

/**
 * Detail Basic Pay Adjustment Component
 */
class Detail extends Component {
    static propTypes = {
        params: React.PropTypes.object,
        products: React.PropTypes.array,
        loading: React.PropTypes.bool,
        employee: React.PropTypes.object,
        basicPayAdjustment: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    };

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        this.props.initializeData({
            employeeId: this.props.params.id,
            adjustmentId: this.props.params.adjustmentId
        });
    }

    /**
     * Component Render Method
     */
    render() {
        const isDemo = company.isDemoCompany();
        const { employee, basicPayAdjustment, notification } = this.props;

        return (
            <PageWrapper>
                <Helmet
                    title="Basic Pay Adjustments Detail"
                    meta={ [
                        { name: 'description', content: 'Basic pay adjustments detail' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( `/employee/${employee.id}`, true );
                            } }
                        >
                            &#8592; Back to Profile
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <HeadingWrapper>
                        <h3>Edit Basic Pay Adjustment</h3>
                        <p>You may edit and manage the employee adjustment through this page.</p>
                    </HeadingWrapper>
                </Container>
                <FormWrapper>
                    <MainWrapper>
                        { this.props.loading ? (
                            <div className="loader">
                                <Loader />
                            </div>
                        ) : (
                            <div>
                                <Container>
                                    <div className="row">
                                        <div className="col-xs-12">
                                            <h5>
                                                <b>{ getEmployeeFullName( employee )}</b> ID:{employee.employee_id}
                                            </h5>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-4">
                                            Current Basic Pay:
                                            <ValueWrapper>
                                                { `Php ${formatCurrency( basicPayAdjustment.amount )}` }
                                            </ValueWrapper>
                                        </div>
                                        <div className="col-xs-4">
                                            Rate:
                                            <ValueWrapper>
                                                { BASE_PAY_UNIT.find( ( u ) => u.value === basicPayAdjustment.unit ).label}
                                            </ValueWrapper>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-4">
                                            Effective Date:
                                            <ValueWrapper>
                                                { formatDate( basicPayAdjustment.effective_date, DATE_FORMATS.DISPLAY ) }
                                            </ValueWrapper>
                                        </div>
                                        <div className="col-xs-4">
                                            Adjustment Date:
                                            <ValueWrapper>
                                                { basicPayAdjustment.adjustment_date
                                                    ? formatDate( basicPayAdjustment.adjustment_date, DATE_FORMATS.DISPLAY )
                                                    : 'N/A' }
                                            </ValueWrapper>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-8">
                                            Reason for Adjustment:
                                            <ValueWrapper>
                                                { basicPayAdjustment.reason ? basicPayAdjustment.reason : 'N/A' }
                                            </ValueWrapper>
                                        </div>
                                    </div>
                                </Container>
                                <div className="foot">
                                    <Button
                                        label="Cancel"
                                        type="neutral"
                                        size="large"
                                        onClick={ () => {
                                            browserHistory.push( `/employee/${employee.id}`, true );
                                        } }
                                    />
                                    <Button
                                        label="Edit"
                                        type="action"
                                        size="large"
                                        onClick={ () => {
                                            browserHistory.push( `/employee/${employee.id}/basic-pay-adjustments/${basicPayAdjustment.id}/edit`, true );
                                        } }
                                        disabled={ isDemo }
                                    />
                                </div>
                            </div>
                        ) }
                    </MainWrapper>
                </FormWrapper>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    employee: makeSelectEmployee(),
    basicPayAdjustment: makeSelectBasicPayAdjustment(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators( addBasicPayAdjustmentActions, dispatch )
);

export default connect( mapStateToProps, mapDispatchToProps )( Detail );
