import { createSelector } from 'reselect';

/**
 * Direct selector to the basic pay adjustment detail state domain
 */
const selectAddBasicPayAdjustmentDomain = () => ( state ) => state.get( 'basicPayAdjustmentDetail' );

const makeSelectLoading = () => createSelector(
    selectAddBasicPayAdjustmentDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectEmployee = () => createSelector(
    selectAddBasicPayAdjustmentDomain(),
    ( substate ) => substate.get( 'employee' ).toJS()
);

const makeSelectBasicPayAdjustment = () => createSelector(
    selectAddBasicPayAdjustmentDomain(),
    ( substate ) => substate.get( 'basicPayAdjustment' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectAddBasicPayAdjustmentDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectEmployee,
    makeSelectBasicPayAdjustment,
    makeSelectNotification
};
