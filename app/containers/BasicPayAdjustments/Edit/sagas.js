import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { Fetch } from '../../../utils/request';

import {
    INITIALIZE,
    SUBMIT_FORM,
    NOTIFICATION,
    SET_LOADING,
    SET_EMPLOYEE,
    SET_BASIC_PAY_ADJUSTMENT,
    SET_SUBMITTED,
    SET_NOTIFICATION,
    SET_ERRORS
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const employee = yield call( Fetch, `/employee/${payload.employeeId}`, { method: 'GET' });
        const basicPayAdjustment = yield call( Fetch, `/basic_pay_adjustment/${payload.adjustmentId}`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE,
            payload: employee
        });

        yield put({
            type: SET_BASIC_PAY_ADJUSTMENT,
            payload: basicPayAdjustment
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SET_SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        yield call( Fetch, `/employee/${payload.employeeId}/basic_pay_adjustment/${payload.adjustmentId}/update`, { method: 'PATCH', data: payload });
        yield call( delay, 500 );

        yield call( browserHistory.push, `/employee/${payload.employeeId}`, true );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SET_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForInitializeData,
    watchForReinitializePage,
    watchNotify
];
