import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment';

import A from '../../../components/A';
import Input from '../../../components/Input';
import Loader from '../../../components/Loader';
import Button from '../../../components/Button';
import SalSelect from '../../../components/Select';
import SnackBar from '../../../components/SnackBar';
import DatePicker from '../../../components/DatePicker';

import {
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectEmployee,
    makeSelectBasicPayAdjustment,
    makeSelectNotification,
    makeSelectErrors
} from './selectors';

import {
    NavWrapper,
    PageWrapper,
    FormWrapper,
    MainWrapper,
    HeadingWrapper
} from './styles';

import * as addBasicPayAdjustmentActions from './actions';

import { DATE_FORMATS } from '../../../utils/constants';
import { browserHistory } from '../../../utils/BrowserHistory';
import { getEmployeeFullName, formatDate, stripNonDigit, formatCurrency } from '../../../utils/functions';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { company } from '../../../utils/CompanyService';

const BASE_PAY_UNIT = [
    { value: 'per_hour', label: 'Per Hour' },
    { value: 'per_day', label: 'Per Day' },
    { value: 'per_month', label: 'Per Month' },
    { value: 'per_year', label: 'Per Year' }
];

const inputTypes = {
    input: [ 'amount', 'reason' ],
    select: ['unit'],
    datePicker: [ 'effective_date', 'adjustment_date' ]
};

/**
 * Edit Basic Pay Adjustment Component
 */
class Edit extends Component {
    static propTypes = {
        params: React.PropTypes.object,
        products: React.PropTypes.array,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        employee: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        submitForm: React.PropTypes.func,
        errors: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            didSetInitialState: false,
            basicPayAdjustment: {
                amount: null,
                unit: null,
                effective_date: null,
                adjustment_date: null,
                reason: ''
            },
            errors: {}
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        this.props.initializeData({
            employeeId: this.props.params.id,
            adjustmentId: this.props.params.adjustmentId
        });
    }

    componentWillReceiveProps( nextProps ) {
        if ( Object.keys( nextProps.basicPayAdjustment ).length !== 0 && !this.state.didSetInitialState ) {
            this.setInitialStateFromProps( nextProps.basicPayAdjustment );
        }

        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });
    }

    setInitialStateFromProps = ( props ) => {
        const initialState = {};

        Object.keys( this.state.basicPayAdjustment ).forEach( ( key ) => {
            initialState[ key ] = props[ key ];
        });

        this.setState({
            basicPayAdjustment: initialState,
            didSetInitialState: true
        });
    }

    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.input.includes( key ) || inputTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, label: this.state.errors[ key ][ 0 ] });
                } else if ( inputTypes.datePicker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    updateBasicPayAdjustment = ( field, value ) => {
        const { basicPayAdjustment } = this.state;

        this.setState({
            basicPayAdjustment: {
                ...basicPayAdjustment,
                [ field ]: value
            }
        });
    }

    validateForm = () => {
        let valid = true;

        if ( this.amount._validate( stripNonDigit( this.amount.state.value ) ) ) {
            valid = false;
        }

        if ( !this.unit._checkRequire( this.unit.state.value ) ) {
            valid = false;
        }

        if ( this.effective_date.checkRequired() ) {
            valid = false;
        }

        if (
            this.state.basicPayAdjustment.effective_date &&
            moment(
                this.state.basicPayAdjustment.effective_date,
                [ DATE_FORMATS.API, DATE_FORMATS.DISPLAY ]
            ).isBefore(
                this.props.employee.date_hired,
                [ DATE_FORMATS.API, DATE_FORMATS.DISPLAY ]
            )
        ) {
            const message = 'The effective date must be a date after employee\'s date hired.';
            this.effective_date.setState({ error: true, message });

            valid = false;
        } else if ( this.effective_date.checkRequired() ) {
            valid = false;
        }

        return valid;
    }

    submitForm = () => {
        if ( this.validateForm() ) {
            this.props.submitForm({
                ...this.state.basicPayAdjustment,
                employeeId: this.props.params.id,
                adjustmentId: this.props.params.adjustmentId,
                amount: stripNonDigit( this.state.basicPayAdjustment.amount )
            });
        }
    }

    /**
     * Component Render Method
     */
    render() {
        const isDemo = company.isDemoCompany();
        const { basicPayAdjustment } = this.state;
        const { employee, notification } = this.props;

        return (
            <PageWrapper>
                <Helmet
                    title="Edit Basic Pay Adjustments"
                    meta={ [
                        { name: 'description', content: 'Edit basic pay adjustments' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( `/employee/${employee.id}`, true );
                            } }
                        >
                            &#8592; Back to Profile
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <HeadingWrapper>
                        <h3>Edit Basic Pay Adjustment</h3>
                        <p>You may edit and manage the employee adjustment through this page.</p>
                    </HeadingWrapper>
                </Container>
                <FormWrapper>
                    <MainWrapper>
                        { this.props.loading ? (
                            <div className="loader">
                                <Loader />
                            </div>
                        ) : (
                            <div>
                                <Container>
                                    <div className="row">
                                        <div className="col-xs-12">
                                            <h5>
                                                <b>{ getEmployeeFullName( employee )}</b> ID:{employee.employee_id}
                                            </h5>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-4">
                                            <Input
                                                id="amount"
                                                placeholder="Enter current basic pay"
                                                label="Current Basic Pay"
                                                required
                                                value={ basicPayAdjustment.amount }
                                                minNumber={ 0 }
                                                ref={ ( ref ) => { this.amount = ref; } }
                                                onChange={ ( value ) => { this.amount.setState({ value: stripNonDigit( value ) }); } }
                                                onFocus={ () => { this.amount.setState({ value: stripNonDigit( this.amount.state.value ) }); } }
                                                onBlur={ ( value ) => {
                                                    this.amount.setState({
                                                        value: formatCurrency( value )
                                                    }, () => {
                                                        this.updateBasicPayAdjustment( 'amount', formatCurrency( value ) );
                                                    });
                                                } }
                                                disabled={ isDemo }
                                            />
                                        </div>
                                        <div className="col-xs-3">
                                            <SalSelect
                                                id="unit"
                                                label="Basic Pay Unit"
                                                required
                                                key="unit"
                                                data={ BASE_PAY_UNIT }
                                                value={ basicPayAdjustment.unit }
                                                placeholder="Select unit"
                                                ref={ ( ref ) => { this.unit = ref; } }
                                                onChange={ ({ value }) => {
                                                    this.updateBasicPayAdjustment( 'unit', value );
                                                } }
                                                disabled={ isDemo }
                                            />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-4 date">
                                            <DatePicker
                                                label="Effective Date"
                                                dayFormat={ DATE_FORMATS.DISPLAY }
                                                required
                                                selectedDay={ basicPayAdjustment.effective_date }
                                                ref={ ( ref ) => { this.effective_date = ref; } }
                                                onChange={ ( value ) => {
                                                    const selectedDay = formatDate( value, DATE_FORMATS.API );

                                                    if ( selectedDay !== this.state.basicPayAdjustment.effective_date ) {
                                                        this.updateBasicPayAdjustment( 'effective_date', formatDate( value, DATE_FORMATS.API ) );
                                                    }
                                                } }
                                                disabled={ isDemo }
                                            />
                                        </div>
                                        <div className="col-xs-4 date">
                                            <DatePicker
                                                label="Adjustment Date"
                                                dayFormat={ DATE_FORMATS.DISPLAY }
                                                selectedDay={ basicPayAdjustment.adjustment_date }
                                                ref={ ( ref ) => { this.adjustment_date = ref; } }
                                                onChange={ ( value ) => {
                                                    const selectedDay = formatDate( value, DATE_FORMATS.API );

                                                    if ( selectedDay !== this.state.basicPayAdjustment.adjustment_date ) {
                                                        this.updateBasicPayAdjustment( 'adjustment_date', formatDate( value, DATE_FORMATS.API ) );
                                                    }
                                                } }
                                                disabled={ isDemo }
                                            />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-8">
                                            <Input
                                                id="reason"
                                                placeholder="Add adjustment reason"
                                                label="Reason for Adjustment"
                                                value={ basicPayAdjustment.reason }
                                                ref={ ( ref ) => { this.reason = ref; } }
                                                onChange={ ( value ) => {
                                                    this.updateBasicPayAdjustment( 'reason', value );
                                                } }
                                                disabled={ isDemo }
                                            />
                                        </div>
                                    </div>
                                </Container>
                                <div className="foot">
                                    <Button
                                        label="Cancel"
                                        type="neutral"
                                        size="large"
                                        onClick={ () => {
                                            browserHistory.push( `/employee/${employee.id}`, true );
                                        } }
                                    />
                                    <Button
                                        label={ this.props.submitted ? <Loader /> : 'Update' }
                                        disabled={ this.props.submitted || isDemo }
                                        type="action"
                                        size="large"
                                        onClick={ this.submitForm }
                                        ref={ ( ref ) => { this.submitButton = ref; } }
                                    />
                                </div>
                            </div>
                        ) }
                    </MainWrapper>
                </FormWrapper>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    submitted: makeSelectSubmitted(),
    employee: makeSelectEmployee(),
    basicPayAdjustment: makeSelectBasicPayAdjustment(),
    notification: makeSelectNotification(),
    errors: makeSelectErrors()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators( addBasicPayAdjustmentActions, dispatch )
);

export default connect( mapStateToProps, mapDispatchToProps )( Edit );
