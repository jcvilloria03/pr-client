import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_EMPLOYEE,
    SET_BASIC_PAY_ADJUSTMENT,
    SET_SUBMITTED,
    SET_NOTIFICATION,
    SET_ERRORS
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    submitted: false,
    employee: {},
    basicPayAdjustment: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    errors: {}
});

/**
 *
 * Edit basic pay adjustment reducer
 *
 */
function editBasicPayAdjustment( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_EMPLOYEE:
            return state.set( 'employee', fromJS( action.payload ) );
        case SET_BASIC_PAY_ADJUSTMENT:
            return state.set( 'basicPayAdjustment', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default editBasicPayAdjustment;
