export const INITIALIZE = 'app/BasicPayAdjustments/Edit/INITIALIZE';
export const SUBMIT_FORM = 'app/BasicPayAdjustments/Edit/SUBMIT_FORM';
export const NOTIFICATION = 'app/BasicPayAdjustments/Edit/NOTIFICATION';

export const SET_LOADING = 'app/BasicPayAdjustments/Edit/SET_LOADING';
export const SET_EMPLOYEE = 'app/BasicPayAdjustments/Edit/SET_EMPLOYEE';
export const SET_BASIC_PAY_ADJUSTMENT = 'app/BasicPayAdjustments/Edit/SET_BASIC_PAY_ADJUSTMENT';
export const SET_SUBMITTED = 'app/BasicPayAdjustments/Edit/SET_SUBMITTED';
export const SET_NOTIFICATION = 'app/BasicPayAdjustments/Edit/SET_NOTIFICATION';
export const SET_ERRORS = 'app/BasicPayAdjustments/Edit/SET_ERRORS';
