export const INITIALIZE = 'app/BasicPayAdjustments/Add/INITIALIZE';
export const SUBMIT_FORM = 'app/BasicPayAdjustments/Add/SUBMIT_FORM';
export const NOTIFICATION = 'app/BasicPayAdjustments/Add/NOTIFICATION';

export const SET_LOADING = 'app/BasicPayAdjustments/Add/SET_LOADING';
export const SET_EMPLOYEE = 'app/BasicPayAdjustments/Add/SET_EMPLOYEE';
export const SET_SUBMITTED = 'app/BasicPayAdjustments/Add/SET_SUBMITTED';
export const SET_NOTIFICATION = 'app/BasicPayAdjustments/Add/SET_NOTIFICATION';
export const SET_ERRORS = 'app/BasicPayAdjustments/Add/SET_ERRORS';
