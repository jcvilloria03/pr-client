import { createSelector } from 'reselect';

/**
 * Direct selector to the add basic pay adjustment state domain
 */
const selectAddBasicPayAdjustmentDomain = () => ( state ) => state.get( 'addBasicPayAdjustment' );

const makeSelectLoading = () => createSelector(
    selectAddBasicPayAdjustmentDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectSubmitted = () => createSelector(
    selectAddBasicPayAdjustmentDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectEmployee = () => createSelector(
    selectAddBasicPayAdjustmentDomain(),
    ( substate ) => substate.get( 'employee' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectAddBasicPayAdjustmentDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectAddBasicPayAdjustmentDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectEmployee,
    makeSelectNotification,
    makeSelectErrors
};
