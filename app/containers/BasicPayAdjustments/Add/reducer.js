import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_EMPLOYEE,
    SET_SUBMITTED,
    SET_NOTIFICATION,
    SET_ERRORS
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    submitted: false,
    employee: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    errors: {}
});

/**
 *
 * Add basic pay adjustment reducer
 *
 */
function addBasicPayAdjustment( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_EMPLOYEE:
            return state.set( 'employee', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addBasicPayAdjustment;
