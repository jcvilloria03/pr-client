import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { subscriptionService } from 'utils/SubscriptionService';
import { auth } from 'utils/AuthService';

import {
    LOADING,
    NOTIFICATION,
    CHECK_LICENSES_AVAILABLE,
    SET_HAS_LICENSES_AVAILABLE,
    SET_EMPLOYEES_PAGINATION,
    GET_EMPLOYEES_SIMPLE,
    SET_EMPLOYEES_SIMPLE,
    GENERATE_MASTERFILE,
    GET_EMPLOYEE_ROLES,
    SET_EMPLOYEE_ROLES,
    GET_EMPLOYEE_LOCATIONS,
    SET_EMPLOYEE_LOCATIONS,
    CHECK_FOR_USER_MATCH,
    SUBMITTED,
    SUBMIT_FORM,
    SET_ERRORS,
    DEFAULT_ORDER_BY,
    DEFAULT_ORDER_DIR,
    INITIAL_EMPLOYEES_PAYLOAD
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import {
    setLoadingTable,
    setNotification,
    setDownloadLoading
} from './actions';

/**
 * Simple mode for employees list
 *
 */
export function* getEmployeesSimple({ payload }) {
    const companyId = company.getLastActiveCompanyId();

    const {
        filter,
        page = 1,
        perPage = 10,
        searchKeyword = '',
        orderBy = DEFAULT_ORDER_BY,
        orderDir = DEFAULT_ORDER_DIR
    } = payload;
    const {
        employeeStatus = 'active',
        location = [],
        department = [],
        payrollGroup = []
    } = filter;

    const parseToParams = ( filterIndex, filterArray ) => {
        if ( filterArray.length > 0 ) {
            return filterArray.reduce(
                ( accumulator, currentValue ) =>
                    `${accumulator}&filter[${filterIndex}][]=${currentValue}`, '' );
        }

        return '';
    };

    // http://gw-api.local.salarium.test/company/12073/ta_employees?page=10&per_page=2&response_size=simple
    const parsedInitialParams = `employees?mode=simple&page=${page}&per_page=${perPage}&order_by=${orderBy}&order_dir=${orderDir}`;

    // ! check url spelling
    const parsedEmployeeStatus = ( employeeStatus.trim() ) ? `&status=${employeeStatus}` : ''; // check if empty string
    const parsedLocation = parseToParams( 'location', location );
    const parsedDepartment = parseToParams( 'department', department );
    const parsedPayrollGroup = parseToParams( 'payroll_group', payrollGroup );
    // * Check if searchKeyword is not empty, and send keyword value for search
    const parsedKeyword = ( searchKeyword.trim() ) ? `&keyword=${searchKeyword}` : '';

    const filterParams = parsedEmployeeStatus + parsedLocation + parsedDepartment + parsedPayrollGroup;
    const urlSuffix = parsedInitialParams + filterParams + parsedKeyword;

    try {
        yield put( setLoadingTable( true ) );

        const response = yield call( Fetch, `/company/${companyId}/${urlSuffix}`, { method: 'GET' });

        if ( response.meta ) {
            const { meta } = response;
            const paginationData = {
                from: meta.pagination.current_page || 1,
                to: meta.pagination.total_pages || 1,
                total: meta.pagination.total || 1,
                current_page: meta.pagination.current_page || 1,
                last_page: meta.pagination.total_pages || 1,
                per_page: meta.pagination.per_page
            };

            yield put({
                type: SET_EMPLOYEES_PAGINATION,
                payload: paginationData
            });
        } else if ( response.total ) {
            const paginationData = {
                from: response.from || 1,
                to: response.to || 1,
                total: response.total || 1,
                current_page: response.current_page || 1,
                last_page: response.last_page || 1,
                per_page: response.per_page
            };

            yield put({
                type: SET_EMPLOYEES_PAGINATION,
                payload: paginationData
            });
        }

        yield put({
            type: SET_EMPLOYEES_SIMPLE,
            payload: response.data || []
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoadingTable( false ) );
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Get employee roles
 */
export function* getEmployeeRoles() {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const response = yield call( Fetch, `/companies/${companyId}/roles`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE_ROLES,
            payload: response.data || []
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Get employee locations
 */
export function* getEmployeeLocations() {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const response = yield call( Fetch, `/philippine/company/${companyId}/locations`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE_LOCATIONS,
            payload: response.data || []
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Check if licenses available for specified user products
 */
export function* checkLicensesAvailable() {
    const products = auth.getProducts();
    const subscriptionId = yield call( subscriptionService.getSubscriptionId );

    try {
        const { data } = yield call( Fetch, `/subscriptions/${subscriptionId}/stats`, { method: 'GET' });
        const currentProduct = data.products.find( ( product ) => products.indexOf( product.product_code ) !== -1 );

        if ( currentProduct ) {
            const hasLicensesAvailable = currentProduct.licenses_available > 0;

            yield put({
                type: SET_HAS_LICENSES_AVAILABLE,
                payload: hasLicensesAvailable
            });
        }
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Generate Masterfile
 */
export function* generateMasterfile({ payload }) {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put( setDownloadLoading( true ) );
        // Generate file
        yield call( Fetch, `/company/${companyId}/employees/generate_masterfile`, {
            method: 'POST',
            data: {
                companyId,
                employeeStatus: payload.filters.employeeStatus,
                location: payload.filters.location,
                department: payload.filters.department,
                payrollGroup: payload.filters.payrollGroup,
                searchTerm: payload.searchTerm
            }
        });
        yield call( notifyUser, {
            title: 'Success',
            message: 'Your employee master list is being prepared. We will send a download link to your registered email address once the download process is complete.',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyError, error );
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put( setDownloadLoading( false ) );
    }
}

/**
 * Sends request to check for an existing user record match
 *
 * @param {Object} payload.data - User details to check
 * @param {Function} payload.callback - Callback function to be called if no user matches
 */
export function* checkForUserMatch({ payload }) {
    const { data, successCallback, errorCallback } = payload;

    const companyId = company.getLastActiveCompanyId();

    try {
        yield put({
            type: SUBMITTED,
            payload: true
        });

        try {
            const matchedUserData = yield call( Fetch, '/employee/has_usermatch', {
                method: 'POST',
                data: { ...data, company_id: companyId }
            });

            const matchedUserFirstName = get( matchedUserData, 'first_name' ) || '';
            const matchedUserLastName = get( matchedUserData, 'last_name' ) || '';
            const matchedUserEmail = get( matchedUserData, 'email' ) || '';
            const shouldDisplayWarningModal = matchedUserFirstName === data.first_name &&
                matchedUserLastName === data.last_name &&
                matchedUserEmail === data.email;

            yield put({
                type: SUBMITTED,
                payload: false
            });

            if ( !shouldDisplayWarningModal ) {
                yield call(
                    notifyError,
                    'The given email address already exists in employee records.'
                );

                return;
            }

            yield call( successCallback );
        } catch ( error ) {
            yield call( errorCallback );
        }
    } catch ( error ) {
        yield call( notifyError, error );

        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * resets the setup store to initial values
 */
export function* submitForm({
    payload: {
        data: employeeData,
        successCallback,
        errorCallback
    }
}) {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        const data = Object.assign({}, employeeData );
        const keys = Object.keys( employeeData );

        // Transform data to remove null keys and convert boolean to 'Yes/No'
        keys.forEach( ( key ) => {
            if ( employeeData[ key ] === null ) {
                delete data[ key ];
            }

            if ( data[ key ] === null || data[ key ] === 'Head Office' ) delete data[ key ];

            if ( key === 'active' ) {
                data[ key ] = data[ key ] ? 'active' : 'inactive';
            }
        });

        Object.assign( data, { company_id: companyId });

        yield call( Fetch, '/people', { method: 'POST', data });

        yield put({
            type: SUBMITTED,
            payload: false
        });

        yield call(
            getEmployeesSimple,
            { payload: INITIAL_EMPLOYEES_PAYLOAD }
        );

        yield call( notifyUser, {
            title: 'Success',
            message: 'Employee record successfully saved',
            show: true,
            type: 'success'
        });

        if ( successCallback ) yield call( successCallback );

        yield [
            call( notifyUser, {
                title: 'Success',
                message: 'Employee record successfully saved',
                show: true,
                type: 'success'
            }),

            // Refetch employees
            call( getEmployeesSimple, {
                payload: {
                    filter: {
                        department: [],
                        employeeStatus: 'active',
                        enabled: false,
                        location: [],
                        payrollGroup: []
                    },
                    orderBy: 'last_name',
                    orderDir: 'asc',
                    page: 1,
                    perPage: 10,
                    searchKeyword: ''
                }
            })
        ];
    } catch ( error ) {
        yield put({
            type: SUBMITTED,
            payload: false
        });

        if ( errorCallback ) yield call( errorCallback );

        if ( error.response && error.response.status === 406 ) {
            if ( error.response.data.data.email ) {
                yield call( notifyUser, {
                    title: 'Error',
                    message: error.response.data.data.email,
                    show: true,
                    type: 'error'
                });
            } else if ( error.response.data.data.employee_id ) {
                yield call( notifyUser, {
                    title: 'Error',
                    message: error.response.data.data.employee_id,
                    show: true,
                    type: 'error'
                });
            } else {
                yield put({
                    type: SET_ERRORS,
                    payload: error.response.data.data,
                    show: true
                });
            }

            if ( Object.prototype.hasOwnProperty.call( error.response.data.data, 'userlink_confirmed' ) ) {
                yield call( notifyError, {
                    title: ' ',
                    message: get( error.response.data.data, 'userlink_confirmed' ),
                    show: true,
                    type: 'error'
                });
            } else if ( get( error, 'response.data.message' ) ) {
                yield call( notifyError, error );
            }
        } else {
            yield call( notifyError, error );
        }
    }
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );

    yield put( setNotification( emptyNotification ) );
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: typeof error === 'string'
            ? error
            : get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( checkLicensesAvailable );

    yield call(
        getEmployeesSimple,
        { payload: INITIAL_EMPLOYEES_PAYLOAD }
    );
}

/**
 * Individual exports for testing
 */
export function* watchForGetEmployeesSimple() {
    const watcher = yield takeEvery( GET_EMPLOYEES_SIMPLE, getEmployeesSimple );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * fetch employee roles
 */
export function* watchForGetEmployeeRoles() {
    const watcher = yield takeEvery( GET_EMPLOYEE_ROLES, getEmployeeRoles );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
  * fetch employee locations
  */
export function* watchForGetEmployeeLocations() {
    const watcher = yield takeEvery( GET_EMPLOYEE_LOCATIONS, getEmployeeLocations );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForCheckLicensesAvailable() {
    const watcher = yield takeEvery( CHECK_LICENSES_AVAILABLE, checkLicensesAvailable );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Generate Masterfile
 */
export function* watchForGenerateMasterfile() {
    const watcher = yield takeEvery( GENERATE_MASTERFILE, generateMasterfile );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for CHECK_FOR_USER_MATCH
 */
export function* watchForCheckUserMatch() {
    const watcher = yield takeEvery( CHECK_FOR_USER_MATCH, checkForUserMatch );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetEmployeesSimple,
    watchForGetEmployeeRoles,
    watchForGetEmployeeLocations,
    watchForCheckLicensesAvailable,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForGenerateMasterfile,
    watchForCheckUserMatch,
    watchForSubmitForm
];
