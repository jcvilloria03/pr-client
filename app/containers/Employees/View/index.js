/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/sort-comp */
import React from 'react';
import orderBy from 'lodash/orderBy';
import { debounce } from 'lodash';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import get from 'lodash/get';
import isNil from 'lodash/isNil';
import snakeCase from 'lodash/snakeCase';
import flatten from 'lodash/flatten';
import trim from 'lodash/trim';

import SubHeader from 'containers/SubHeader';

import A from 'components/A';
import Icon from 'components/Icon';
import Table from 'components/Table';
import Modal from 'components/Modal';
import Input from 'components/Input';
import Button from 'components/Button';
import Loader from 'components/Loader';
import SnackBar from 'components/SnackBar';
import { H2, H3 } from 'components/Typography';
import SalDropdown from 'components/SalDropdown';
import FooterTablePagination from 'components/FooterTablePagination';

import { browserHistory } from 'utils/BrowserHistory';
import { formatPaginationLabel } from 'utils/functions';
import { EMPLOYEE_SUBHEADER_ITEMS } from 'utils/constants';
import { company } from 'utils/CompanyService';

import {
  makeSelectEmployeesSimple,
  makeSelectLoading,
  makeSelectTableLoading,
  makeSelectDownloadLoading,
  makeSelectNotification,
  makeSelectProductsState,
  makeSelectFilterData,
  makeSelectHasLicensesAvailable,
  makeSelectPagination,
  makeSelectDownloadUrl,
  makeSelectSubmitted,
  makeSelectLocations,
  makeSelectRoles
} from './selectors';

import * as employeesActions from './actions';

import { renderField } from './templates/formv2';
import Filter from './templates/filter';

import {
  ModalWrapper,
  PageWrapper,
  LoadingStyles,
  EmployeeName,
  EmployeePhoto,
  ModalButtonLabel,
  ConfirmBodyWrapperStyle
} from './styles';

import {
    DEFAULT_ORDER_BY,
    DEFAULT_ORDER_DIR,
    INITIAL_EMPLOYEE_DATA,
    PEOPLE_FIELDS
} from './constants';

import { PROFILE_PICTURE } from '../../Employee/View/constants';

const TABLE_COLUMNS = [
    {
        id: 'id',
        header: 'Employee ID',
        minWidth: 150,
        accessor: 'employee_id'
    },
    {
        id: 'first_name',
        header: 'Name',
        accessor: ( row ) => {
            const firstName = get( row, 'first_name' ) || '';
            const middleName = get( row, 'middle_name' ) || '';
            const lastName = get( row, 'last_name' ) || '';
            const picture = get( row, 'picture' );
            const fullName = [ firstName, middleName, lastName ]
                .filter( ( name ) => trim( name ) )
                .join( ' ' );

            return (
                <EmployeeName>
                    <EmployeePhoto src={ picture || PROFILE_PICTURE } />

                    {fullName}
                </EmployeeName>
            );
        },
        minWidth: 260
    },
    {
        id: 'email',
        header: 'Email',
        minWidth: 310,
        accessor: 'email'
    },
    {
        id: 'department',
        header: 'Department',
        minWidth: 240,
        accessor: ( row ) => get( row, 'department.name', '' )
    },
    {
        id: 'location',
        header: 'Location',
        minWidth: 240,
        accessor: ( row ) => get( row, 'location.name', '' )
    }
];

// eslint-disable-next-line require-jsdoc
export class View extends React.Component {
    static propTypes = {
        getEmployeesSimple: React.PropTypes.func,
        getEmployeeRoles: React.PropTypes.func,
        getLocations: React.PropTypes.func,
        exportEmployees: React.PropTypes.func,
        generateMasterfile: React.PropTypes.func,
        checkLicensesAvailable: React.PropTypes.func,
        employees: React.PropTypes.array,
        roles: React.PropTypes.array,
        locations: React.PropTypes.array,
        employeesSimple: React.PropTypes.array,
        filterData: React.PropTypes.object,
        loading: React.PropTypes.bool,
        tableLoading: React.PropTypes.bool,
        downloadLoading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        hasLicensesAvailable: React.PropTypes.bool,
        pagination: React.PropTypes.object,
        downloadUrl: React.PropTypes.string,
        checkForUserMatch: React.PropTypes.func,
        submitEmployee: React.PropTypes.func,
        submitted: React.PropTypes.bool
    };

    static defaultProps = {
        loading: true,
        downloading: false,
        errors: {},
        hasLicensesAvailable: false,
        pagination: {
            from: 1,
            to: 1,
            total: 1,
            current_page: 0,
            last_page: 1,
            per_page: 10
        }
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: this.props.employees,
            showMoreActions: false,
            selectedValues: [],
            permission: {
                view: true,
                create: true,
                edit: true
            },
            dropdownItems: [
                {
                    label: 'Download',
                    children: <div>Download</div>,
                    onClick: this.exportLeaveCredits
                }
            ],
            filters: {
                enabled: false,
                employeeStatus: 'active',
                location: [],
                department: [],
                payrollGroup: []
            },
            showModal: false,
            showWarningModal: false,
            showFilter: false,
            hasFiltersApplied: false,
            filtersEnabled: false,
            hasLicensesAvailable: false,
            tablePayslipsPage: 0,
            tablePayslipsPageSize: 10,
            orderBy: DEFAULT_ORDER_BY,
            orderDir: DEFAULT_ORDER_DIR,
            tablePageSize: 10,
            searchTerm: '',
            roles: [],
            locations: [],
            employee_data: { ...INITIAL_EMPLOYEE_DATA },
            field_types: {
                input: [],
                select: [],
                switch: []
            },
            page: 1,
            totalPagination: 0,
            hasUserMatch: false,
            hasNoLicenses: false
        };

        this.handleSearch = this.handleSearch.bind( this );
        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
        this.onSortingChange = this.onSortingChange.bind( this );
        this.updateState = this.updateState.bind( this );
        this.clearEmployeeData = this.clearEmployeeData.bind( this );
        this.checkForUserMatch = this.checkForUserMatch.bind( this );
        this.handleCreate = this.handleCreate.bind( this );
        this.handleProceedCreateWithMatchedUser = this.handleProceedCreateWithMatchedUser.bind( this );
    }

    componentWillMount() {
        this.props.getEmployeesSimple( this.state.filters );
        this.props.getEmployeeRoles();
        this.props.getLocations();
    }

    /**
     * Fetch loan list
     */
    componentDidMount() {
        this.props.checkLicensesAvailable();
        this.setFieldTypes();
    }

    componentWillReceiveProps( nextProps ) {
        // Set table details label
        if ( this.props.pagination.total ) {
            const page = this.props.pagination.current_page || 0;
            const size = this.props.pagination.per_page || 0;
            const dataLength = this.props.pagination.total || 0;
            this.setState({
                label: `Showing ${dataLength ? ( ( page * size ) + 1 ) - size : 0} - ${( page * size ) > dataLength ? dataLength : ( page * size )} of ${dataLength} entr${dataLength > 1 || dataLength === 0 ? 'ies' : 'y'}`
            });
        }

        if ( this.props.loading ) this.setState({ totalPagination: this.props.pagination.total });

        if ( this.state.roles.length !== nextProps.roles.length ) {
            this.setState({ roles: nextProps.roles.map( ( role ) => ({
                value: role.id,
                label: role.name
            }) ) });
        }

        if ( this.state.locations.length !== nextProps.locations.length ) {
            this.setState({ locations: nextProps.locations.map( ( location ) => ({
                value: location.id,
                label: location.name
            }) ) });
        }
    }

    /**
     * On table page change handler
     */
    onPageChange( page ) {
        this.setState({ tablePage: page });
        this.employeesTable.tableComponent.setState({ page });
        this.props.getEmployeesSimple(
            this.state.filters,
            page,
            this.state.tablePageSize,
            this.state.orderBy,
            this.state.orderDir,
            this.state.searchTerm
        );
    }

    onPageSizeChange( pageSize ) {
        this.setState({ tablePageSize: pageSize });
        this.employeesTable.tableComponent.setState({ pageSize });

        this.props.getEmployeesSimple(
            this.state.filters,
            1,
            pageSize,
            this.state.orderBy,
            this.state.orderDir,
            this.state.searchTerm
        );
    }

    /**
     * On table page size change handler
     */

    onSortingChange( column, additive ) {
        // clone current sorting sorting
        let newSorting = JSON.parse(
            JSON.stringify( this.employeesTable.state.sorting, ( key, value ) => {
                if ( typeof value === 'function' ) {
                    return value.toString();
                }

                return value;
            })
        );

        // check if already sorted by same column
        const existingIndex = newSorting.findIndex( ( d ) => d.id === column.id );

        let isDesc = false;
        if ( existingIndex > -1 ) {
            const existing = newSorting[ existingIndex ];
            if ( existing.desc ) {
                if ( additive ) {
                    newSorting.splice( existingIndex, 1 );
                } else {
                    existing.desc = false;
                    newSorting = [existing];
                }
            } else {
                existing.desc = true;
                isDesc = true;
                if ( !additive ) {
                    newSorting = [existing];
                }
            }
        } else if ( additive ) {
            newSorting.push({
                id: column.id,
                desc: false
            });
        } else {
            newSorting = [{
                id: column.id,
                desc: false
            }];
        }

        const orderDir = isDesc ? 'desc' : 'asc';

        // Fetch sorted data
        this.props.getEmployeesSimple(
            this.state.filters,
            this.props.pagination.current_page,
            this.state.tablePageSize,
            column.id,
            orderDir
        );

        // set new sorting states
        this.setState({
            orderBy: column.id,
            orderDir
        });
        this.employeesTable.setState({
            sorting: newSorting
        });
        this.employeesTable.tableComponent.setState({
            sorting: newSorting
        });
    }

    onSubmitValidation() {
        if ( !this.props.hasLicensesAvailable ) {
            this.setState({ hasNoLicenses: true });
        } else {
            if ( this.validateForm() ) {
                this.checkForUserMatch();
            }
        }
    }

    /**
     * Gets onChange handler for form fields
     *
     * @param {String} id - ID of field
     * @param {String} type - Field Type
     * @returns {Function}
     */
    getOnChangeHandler( id, type ) {
        if ( type === 'select' ) {
            switch ( id ) {
                default:
                    return ({ value }) => this.updateState( id, value );
            }
        }

        switch ( id ) {
            default:
                return ( value ) => this.updateState( id, value );
        }
    }

    setFieldTypes() {
        this.setState({ field_types: this.getFieldTypes() });
    }

    /**
     * Gets selected values ids.
     * @returns {Array}
     */
    getSelectedValuesIds = () => this.state.selectedValues.map( ( selectedValue ) => selectedValue.id )

    /**
     * Gets ids of selected values from table.
     * Sets valueIds state.
     * @returns {Array}
     */
    setSelectedValues = () => {
        const selectedValues = [];

        this.employeesTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedValues.push( this.employeesTable.props.data[ index ]);
            }
        });

        this.setState({ selectedValues });

        return this.state.selectedValues.map( ( selectedValue ) => selectedValue.id );
    }

    /**
     * Gets input types depending on the license
     *
     * @returns {Object}
     */
    getFieldTypes() {
        const {
            field_types: fieldTypes
        } = this.state;

        const fields = PEOPLE_FIELDS;

        const inputTypes = fields && fields.sections.reduce( ( inputs, section ) => {
            flatten( section.rows ).forEach( ( field ) => {
                if ( field.field_type !== '' ) {
                    const key = snakeCase( field.field_type );
                    inputs[ key ].push( field.id );
                }
            });
            return inputs;
        }, {
            input: [],
            select: [],
            date_picker: [],
            switch: []
        });

        return inputTypes || fieldTypes;
    }

    handleCreate = ( callbacks ) => {
        this.setState( ( prevState ) => {
            const employeeData = Object.assign({}, prevState.employee_data );

            return { employee_data: employeeData };
        }, () => {
            this.props.submitEmployee({
                data: Object.assign(
                    {},
                    this.state.employee_data,
                ),
                successCallback: callbacks && callbacks.successCallback,
                errorCallback: callbacks && callbacks.errorCallback
            });
        });
    }

    /**
     * Check is table mounted.
     * @returns {Boolean}
     */
    tableIsNotMounted() {
        return !this.employeesTable ||
            !this.employeesTable.tableComponent ||
            !this.employeesTable.tableComponent.state;
    }

    /**
     * send a request to export loans
     */
    exportEmployees = () => {
        // Get employee IDs of selected rows
        const selectedEmployees = [];

        const sortingOptions = this.employeesTable.state.sorting;

        this.employeesTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedEmployees.push( this.employeesTable.props.data[ index ]);
            }
        });

        const sortedEmployees = orderBy(
            selectedEmployees,
            sortingOptions.map( ( o ) => o.id ),
            sortingOptions.map( ( o ) => ( o.desc ? 'desc' : 'asc' ) )
        );

        // dispatch an action to send a request to API
        this.props.exportEmployees( sortedEmployees.map( ( employee ) => employee.id ) );
    };

    /**
     * converts a number to currency format.
     * @param value = value to format
     * @returns {string} value in currency format
     */
    formatCurrency( value = 0 ) {
        const convertedValue = Number( value );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `PHP ${Number( convertedValue ).toFixed( 2 ).replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    /**
     * Checks is any table row selected.
     * @returns {Boolean}
     */
    tableHasSelectedRows() {
        if ( this.tableIsNotMounted() ) {
            return false;
        }

        return !!this.employeesTable.state.selected.filter( ( item ) => item ).length;
    }

    isTaSubscribed = () => {
        const products = this.props.products;
        return products && ( products.includes( 'time and attendance' ) || products.includes( 'time and attendance plus payroll' ) );
    }

    isPayrollSubscribed = () => {
        const products = this.props.products;
        return products && ( products.includes( 'payroll' ) || products.includes( 'time and attendance plus payroll' ) );
    }

    applyFilters = ( filters ) => {
        this.setState({
            filters,
            filtersEnabled: true,
            hasFiltersApplied: filters.enabled
        }, () => {
            this.props.getEmployeesSimple( filters );
            this.handleTableChanges();
        });
    }

    resetFilters = () => {
        this.setState({
            showFilter: false,
            displayedData: this.props.employees,
            hasFiltersApplied: false,
            filtersEnabled: false
        });
    }

    /**
     * Toggles filters.
     */
    toggleFilter = () => {
        if ( this.props.loading || this.filterDataIsNotFetched() ) {
            return;
        }

        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    generateMasterfile = () => {
        // dispatch an action to send a request to API
        this.props.generateMasterfile( this.state.filters, this.state.searchTerm );
    }

    /**
     * Checks is filterData fetched.
     * @returns {Boolean}
     */
    filterDataIsNotFetched() {
        return !this.props.filterData;
    }

    /**
     * Redirect to Employee 201 page
     */
    goto201Page( state, rowInfo, column ) {
        return {
            onClick: () => {
                if ( column.id !== 'select' ) {
                    browserHistory.push( `/employee/${rowInfo.row.id || rowInfo.row.employee_number}`, true );
                }
            },
            style: {
                cursor: column.id !== 'select' ? 'pointer' : 'default'
            }
        };
    }

    /**
     * updates the state with the form inputs
     * @param {String} key - field
     * @param {*}      value - new value
     */
    updateState( key, value ) {
        this.setState( ( prevState ) => {
            const newValue = value !== '' && !isNil( value ) ? value : null;
            const employeeData = {
                ...prevState.employee_data,
                [ key ]: newValue
            };

            return { employee_data: employeeData };
        });
    }

    /**
     * handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges( tableProps = this.employeesTable.tableComponent.state ) {
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    /**
     * handles filters and search inputs
     */
    handleSearch = debounce( ( term = '' ) => {
        this.setState({
            searchTerm: term.trim()
        });

        if ( this.state.searchTerm ) {
            this.props.getEmployeesSimple(
                this.state.filters,
                1,
                this.state.tablePageSize,
                this.state.orderBy,
                this.state.orderDir,
                this.state.searchTerm
            );
        } else {
            this.props.getEmployeesSimple(
                this.state.filters,
                1,
                this.state.tablePageSize,
                this.state.orderBy,
                this.state.orderDir
            );
        }
    }, 600 )

    /**
     * validates add employee form
     * @returns {boolean} wether the form inputs are valid or not
     */
    validateForm() {
        let valid = true;
        const { field_types: fieldTypes } = this.state;

        fieldTypes.input.forEach( ( input ) => {
            const field = this[ input ];
            if ( field._validate( field.state.value ) ) {
                valid = false;
            }
        });

        fieldTypes.select.forEach( ( select ) => {
            const field = this[ select ];
            if ( !field._checkRequire( typeof field.state.value === 'object' && field.state.value !== null ? field.state.value.value : field.state.value ) ) {
                valid = false;
            }
        });

        fieldTypes.date_picker.forEach( ( datePicker ) => {
            const field = this[ datePicker ];
            if ( field.checkRequired() ) {
                valid = false;
            }
        });

        return valid;
    }

    checkForUserMatch = () => {
        const {
            last_name,
            first_name,
            middle_name,
            email
        } = this.state.employee_data;

        const data = {
            first_name,
            last_name,
            middle_name,
            email
        };

        this.props.checkForUserMatch({
            data,
            // Callback to handle if user has match
            successCallback: () => {
                this.setState({ hasUserMatch: true }, () => {
                    this.addPersonModal.toggle();
                    this.warningModal.toggle();
                });
            },
            // Callback to proceed with employee creation
            errorCallback: () => {
                this.handleCreate({
                    // Close form modal after successful call
                    successCallback: () => {
                        this.setState({ hasUserMatch: false }, () => {
                            this.addPersonModal.toggle();
                        });
                    }
                });
            }
        });
    }

    /**
     * Renders field according to config provided
     *
     * @param {Object} field - Details of field to be rendered
     * @returns {Object}
     */
    renderField( field ) {
        const { employee_data: employeeData } = this.state;

        const {
            class_name: className,
            options,
            ...rest
        } = field;

        let fieldConfig = {};

        switch ( field.field_type ) {
            case 'input':
                fieldConfig = {
                    max: get( field, 'validations.max', null )
                };
                break;
            case 'select':
                fieldConfig = { options: this.state[ options ] };
                break;
            case 'switch':
                fieldConfig = {
                    checked: employeeData[ field.id ],
                    disabled: [ 'regular_holiday_pay', 'special_holiday_pay' ].includes( field.id ) && !employeeData.timesheet_required
                };
                break;
            default:
        }

        let defaultValue;

        switch ( field.id ) {
            case 'primary_location_name':
                defaultValue = 'Head Office';
                break;
            default:
                defaultValue = employeeData[ field.id ] || '';
        }

        const config = {
            ...rest,
            ...fieldConfig,
            value: defaultValue,
            onChange: this.getOnChangeHandler( field.id, field.field_type ),
            ref: ( ref ) => { this[ field.id ] = ref; }
        };

        return (
            <div
                className={ `${className} ${config.hide ? 'hide' : ''}` }
                style={ { margin: '0 0 8px 0' } }
                key={ field.id }
            >
                { field.field_type && renderField( config ) }
            </div>
        );
    }

    /**
     * Renders form according to license
     *
     * @returns {Object}
     */
    renderForm() {
        const fields = PEOPLE_FIELDS;

        return (
            <Container>
                { fields && fields.sections.map( ({ title, rows }) => (
                    <div key={ title }>
                        { rows.map( ( row, i ) => (
                            <div className="row" key={ `${title}_${i}` }>
                                { row.map( ( field ) => this.renderField( field ) ) }
                            </div>
                        ) ) }
                        <br />
                    </div>
                ) ) }
            </Container>
        );
    }

    clearEmployeeData() {
        this.setState({ employee_data: { ...INITIAL_EMPLOYEE_DATA }});
        this.setState({ hasNoLicenses: false });
    }

    handleProceedCreateWithMatchedUser() {
        this.handleCreate({
            successCallback: () => {
                this.setState({ hasUserMatch: false }, () => {
                    this.clearEmployeeData();
                    this.warningModal.toggle();
                });
            },
            errorCallback: () => {
                this.setState({ hasUserMatch: false }, () => {
                    this.warningModal.toggle();
                    this.addPersonModal.toggle();
                });
            }
        });
    }

    render() {
        const isDemo = company.isDemoCompany();
        const { submitted } = this.props;
        const {
            employee_data: employeeData,
            hasUserMatch
        } = this.state;

        return (
            <div>
                <Helmet
                    title="Employees"
                    meta={ [
                        { name: 'description', content: 'Employee List' }
                    ] }
                />

                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                
                { this.state.hasNoLicenses && (
                    <SnackBar
                        message="You don't have sufficient licenses to add any Employees, please go to the License Page in the Control Panel to add additional licenses."
                        title= 'Error'
                        offset={ { top: 70 } }
                        show={ this.state.hasNoLicenses }
                        ref={ ( ref ) => { this.state.hasNoLicenses = ref; } }
                        delay={ 5000 }
                        type="error"
                    />
                ) }

                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />

                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Employees</H2>
                                <br />

                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>

                        <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Employees</H3>
                            </div>

                            <div className="title">
                                <div className="search-wrapper">
                                    <Input
                                        className="search"
                                        id="search"
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                        onChange={ () => {
                                            this.handleSearch(
                                                this.searchInput.state.value.toLowerCase()
                                            );
                                        } }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                    />

                                    <Button
                                        label={ <span><Icon className="filter-icon" name="filter" /> Filter</span> }
                                        type="neutral"
                                        onClick={ this.toggleFilter }
                                    />
                                </div>

                                <div className="actions-wrapper">
                                    <div style={ { display: 'flex', alignItems: 'center' } }>
                                        <SalDropdown
                                            new
                                            label="Add People"
                                            dropdownItems={ [
                                                {
                                                    label: 'Add Single Person',
                                                    children: <div>Add Single Person</div>,
                                                    disabled: isDemo,
                                                    onClick: () => {
                                                        this.addPersonModal.toggle();
                                                        this.setState({ hasUserMatch: false });
                                                    }
                                                },
                                                {
                                                    label: 'Add Multiple People',
                                                    children: <div>Add Multiple People</div>,
                                                    disabled: isDemo,
                                                    onClick: () => browserHistory.push( '/employees/batch-add', true )
                                                }
                                            ] }
                                        />

                                        &nbsp;

                                        <Button
                                            label="Mass Update"
                                            type="neutral"
                                            onClick={ () => { browserHistory.push( '/employees/batch-update', true ); } }
                                        />

                                        <Button
                                            label={ this.props.downloadLoading ? <Loader /> : <span>Download</span> }
                                            type="neutral"
                                            onClick={ this.generateMasterfile }
                                        />

                                        { this.tableHasSelectedRows() && (
                                            <span className="sl-u-gap-left--sm">
                                                <SalDropdown
                                                    dropdownItems={ this.state.dropdownItems }
                                                />
                                            </span>
                                        )}
                                    </div>
                                </div>

                                <span />
                            </div>
                            <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                                <Filter
                                    products={ this.props.products }
                                    onCancel={ () => { this.resetFilters(); } }
                                    onApply={ ( filters ) => { this.applyFilters( filters ); } }
                                />
                            </div>
                            <Table
                                data={ this.props.employeesSimple }
                                columns={ TABLE_COLUMNS }
                                showPageSizeOptions
                                onRowClick={ this.goto201Page }
                                loading={ this.props.tableLoading }
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.employeesTable = ref; } }
                                onSelectionChange={ () => {
                                    this.setSelectedValues();
                                } }
                                pages={ this.props.pagination.last_page || 1 }
                                onSortingChange={ this.onSortingChange }
                                manual
                            />
                        </div>

                        <A download id="downloadLink" />
                    </Container>
                </PageWrapper>

                <div
                    className="loader"
                    style={ { display: this.props.loading ? 'none' : 'block', position: 'fixed', bottom: '0', background: '#fff' } }
                >
                    <FooterTablePagination
                        page={ this.props.pagination.current_page }
                        pageSize={ this.props.pagination.per_page }
                        pagination={ this.props.pagination }
                        onPageChange={ this.onPageChange }
                        onPageSizeChange={ this.onPageSizeChange }
                        paginationLabel={ this.state.label }
                    />
                </div>

                <Modal
                    center
                    ref={ ( ref ) => { this.addPersonModal = ref; } }
                    size="md"
                    title="Add a New Person"
                    backdrop="static"
                    body={
                        <ModalWrapper className="modal-dialog-centered">
                            { this.renderForm() }
                        </ModalWrapper>
                    }
                    buttons={ [
                        {
                            label: submitted
                                ? <Loader />
                                : <ModalButtonLabel>Add</ModalButtonLabel>,
                            type: 'action',
                            disabled: submitted,
                            onClick: () => { this.onSubmitValidation(); }
                        },
                        {
                            label: 'Cancel',
                            type: 'grey',
                            disabled: submitted,
                            onClick: () => {
                                this.addPersonModal.toggle();
                                this.clearEmployeeData();
                            }
                        }
                    ] }
                    onClose={
                        !submitted
                            ? () => {
                                if ( !hasUserMatch ) {
                                    this.clearEmployeeData();
                                }
                            } : undefined
                    }
                    showClose={ !submitted }
                />

                <Modal
                    center
                    ref={ ( ref ) => { this.warningModal = ref; } }
                    title="Warning!"
                    backdrop="static"
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                The employee you are trying to add, {' '}
                                <strong>{ `${get( employeeData, 'first_name' )} ${get( employeeData, 'last_name' )}` }</strong>{', '}
                                with email
                                <strong> { get( employeeData, 'email' ) } </strong>
                                already has an existing user profile.
                                <br /><br />
                                To create their employee profile and automatically link to the existing user profile, click proceed.
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    buttons={ [
                        {
                            label: submitted
                                ? <Loader />
                                : <ModalButtonLabel>Proceed</ModalButtonLabel>,
                            type: 'action',
                            onClick: () => this.handleProceedCreateWithMatchedUser()
                        },
                        {
                            label: 'Cancel',
                            type: 'grey',
                            disabled: submitted,
                            onClick: () => {
                                this.setState({ hasUserMatch: false }, () => {
                                    this.warningModal.toggle();
                                    this.addPersonModal.toggle();
                                });
                            }
                        }
                    ] }
                    onClose={
                        !submitted && hasUserMatch
                            ? () => {
                                this.addPersonModal.toggle();
                                this.setState({ hasUserMatch: false });
                            } : undefined
                    }
                    showClose={ !submitted }
                />

            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    roles: makeSelectRoles(),
    locations: makeSelectLocations(),
    tableLoading: makeSelectTableLoading(),
    downloadLoading: makeSelectDownloadLoading(),
    notification: makeSelectNotification(),
    products: makeSelectProductsState(),
    filterData: makeSelectFilterData(),
    hasLicensesAvailable: makeSelectHasLicensesAvailable(),
    pagination: makeSelectPagination(),
    downloadUrl: makeSelectDownloadUrl(),
    employeesSimple: makeSelectEmployeesSimple(),
    submitted: makeSelectSubmitted()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        employeesActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
