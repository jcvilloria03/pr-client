import {
    NOTIFICATION,
    CHECK_LICENSES_AVAILABLE,
    TABLE_LOADING,
    DOWNLOAD_LOADING,
    GENERATE_MASTERFILE,
    GET_EMPLOYEE_ROLES,
    GET_EMPLOYEE_LOCATIONS,
    GET_EMPLOYEES_SIMPLE,
    CHECK_FOR_USER_MATCH,
    SUBMIT_FORM,
    NOTIFICATION_SAGA,
    DEFAULT_ORDER_BY,
    DEFAULT_ORDER_DIR
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Generate Masterfile
 */
export function generateMasterfile( filters, searchTerm ) {
    return {
        type: GENERATE_MASTERFILE,
        payload: {
            filters,
            searchTerm
        }
    };
}

/**
 * display a notification in page
 */
export function getEmployeesSimple(
    {
        enabled = false,
        employeeStatus = '',
        location = [],
        department = [],
        payrollGroup = []
    },
    page = 1,
    perPage = 10,
    orderBy = DEFAULT_ORDER_BY,
    orderDir = DEFAULT_ORDER_DIR,
    searchKeyword = ''
) {
    return {
        type: GET_EMPLOYEES_SIMPLE,
        payload: {
            filter: {
                enabled,
                employeeStatus,
                location,
                department,
                payrollGroup
            },
            page,
            perPage,
            orderBy,
            orderDir,
            searchKeyword
        }
    };
}

/**
 * fetch employee roles
 */
export function getEmployeeRoles() {
    return {
        type: GET_EMPLOYEE_ROLES
    };
}

/**
 * fetch employee locations
 */
export function getLocations() {
    return {
        type: GET_EMPLOYEE_LOCATIONS
    };
}

/**
 * Get subscription stats for check for Add Employees button
 *
 * @param {String} currentUserProduct Product code for current user logged in
 */
export function checkLicensesAvailable( currentUserProduct ) {
    return {
        type: CHECK_LICENSES_AVAILABLE,
        payload: {
            currentUserProduct
        }
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}

/**
 * resets the store to initial state
 */
export function setLoadingTable( payload ) {
    return {
        type: TABLE_LOADING,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function setDownloadLoading( payload ) {
    return {
        type: DOWNLOAD_LOADING,
        payload
    };
}

/**
 * Sets notification
 * @param {Object} payload - Notification config
 * @returns {Object}
 */
export function setNotification( payload ) {
    return {
        type: NOTIFICATION_SAGA,
        payload
    };
}

/**
 * Sends request to check for user record match
 *
 * @param {Object} payload - Employee form data
 * @returns {Object} action
 */
export function checkForUserMatch( payload ) {
    return {
        type: CHECK_FOR_USER_MATCH,
        payload
    };
}

/**
 * submit a new employee
 */
export function submitEmployee( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}
