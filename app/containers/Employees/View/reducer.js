import { fromJS } from 'immutable';
import {
    LOADING,
    TABLE_LOADING,
    DOWNLOAD_LOADING,
    NOTIFICATION_SAGA,
    SET_FILTER_DATA,
    SET_HAS_LICENSES_AVAILABLE,
    SET_EMPLOYEES_PAGINATION,
    SET_EMPLOYEES_SIMPLE,
    SET_FILTER_LOCATIONS,
    SET_FILTER_DEPARTMENTS,
    SET_FILTER_PAYROLL_GROUPS,
    SET_EMPLOYEE_ROLES,
    SET_EMPLOYEE_LOCATIONS,
    SUBMITTED
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    tableLoading: true,
    downloadLoading: false,
    submitted: false,
    roles: [],
    locations: [],
    employeesSimple: [],
    filterData: {},
    filterLocations: [],
    filterDepartments: [],
    filterPayrollGroups: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    hasLicensesAvailable: false,
    pagination: {
        from: 1,
        to: 1,
        total: 1,
        current_page: 0,
        last_page: 1,
        per_page: 10
    }
});

/**
 *
 * Download Reducer
 *
 */
function employeesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case TABLE_LOADING:
            return state.set( 'tableLoading', action.payload );
        case DOWNLOAD_LOADING:
            return state.set( 'downloadLoading', action.payload );
        case SET_EMPLOYEES_SIMPLE:
            return state.set( 'employeesSimple', fromJS( action.payload ) );
        case SET_EMPLOYEES_PAGINATION:
            return state.set( 'pagination', action.payload );
        case SET_EMPLOYEE_ROLES:
            return state.set( 'roles', fromJS( action.payload ) );
        case SET_EMPLOYEE_LOCATIONS:
            return state.set( 'locations', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case SET_FILTER_LOCATIONS:
            return state.set( 'filterLocations', fromJS( action.payload ) );
        case SET_FILTER_DEPARTMENTS:
            return state.set( 'filterDepartments', fromJS( action.payload ) );
        case SET_FILTER_PAYROLL_GROUPS:
            return state.set( 'filterPayrollGroups', fromJS( action.payload ) );
        case SET_HAS_LICENSES_AVAILABLE:
            return state.set( 'hasLicensesAvailable', action.payload );
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default employeesReducer;
