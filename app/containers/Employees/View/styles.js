import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    .content {
        margin-top: 40px;
        margin-bottom: 3rem;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 20px auto;

            h3 {
                font-weight: 600;
                margin-bottom: 0;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .main {
            .btn {
                min-width: 140px;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .title {
            display: flex;
            justify-content: space-between;
            align-items: center;
            width: 100%;
            margin-bottom: 1rem;

            button {
              border-radius: 2px;
            }

            .search-wrapper {
                display: flex;
                align-items: center;
                flex-direction: row;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 2px;
                    margin-right: 3px;

                    .input-group {
                      height: 35px;

                      input {
                        height: 35px;
                        border: none;
                      }
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }

            .actions-wrapper {
                display: flex;
                flex-grow: 1;
                align-items: center;
                justify-content: flex-end;

                .dropdown {
                  span {
                    border-radius: 2px;
                  }
                }
            }
        }
    }

    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }

    .caret-icon > svg {
        height: 8px;
    }

    .no-license-error {
        font-size: 20px;
        margin-bottom: 0;
        color: #e13232;
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        text-align: left;
    }
`;

export const ModalWrapper = styled.section`
  .toggleWrapper {
    display: flex;
    flex-direction: row;
    align-items: center;
    line-height: 1;
    margin: 15px 0;
  }

  .download-icon > svg {
    width: 12px;
    height: 12px;
    margin: 0 6px 0 0;
  }
`;

export const ModalButtonLabel = styled.span`
    padding: 7px 11px;
`;

export const EmployeeName = styled.span`
    display: flex;
    gap: 0.5rem;
    align-items: center;
`;

export const EmployeePhoto = styled.img`
    width: 1.5rem;
    height: 1.5rem;
    object-fit: cover;
    object-position: center;
    border: 1px solid #bbb;
    border-radius: 50%;
    background-color: #ccc;
`;
