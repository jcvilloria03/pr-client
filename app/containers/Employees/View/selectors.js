import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectViewDomain = () => ( state ) => state.get( 'employees' );

const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

const makeSelectEmployeesSimple = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'employeesSimple' ).toJS()
);

const makeSelectRoles = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'roles' ).toJS()
);

const makeSelectLocations = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'locations' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectTableLoading = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'tableLoading' )
);

const makeSelectDownloadLoading = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'downloadLoading' )
);

const makeSelectNotification = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectProductsState = () => createSelector(
    selectAppDomain(),
    ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

const makeSelectFilterData = () => createSelector(
    selectViewDomain(),
    ( state ) => state.get( 'filterData' ).toJS()
);

const makeSelectHasLicensesAvailable = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'hasLicensesAvailable' )
);

const makeSelectPagination = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'pagination' )
);

const makeSelectDownloadUrl = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'download_url' )
);

const makeSelectProductSeats = () => createSelector(
    selectAppDomain(),
    ( substate ) => {
        let productSeats;
        try {
            productSeats = substate.get( 'userInformation' ).get( 'product_seats' ).toJS();
        } catch ( error ) {
            productSeats = substate.get( 'userInformation' ).get( 'product_seats' );
        }

        return productSeats;
    }
);

const makeSelectSubmitted = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'submitted' )
);

export {
    makeSelectLoading,
    makeSelectRoles,
    makeSelectLocations,
    makeSelectTableLoading,
    makeSelectNotification,
    makeSelectProductsState,
    makeSelectFilterData,
    makeSelectDownloadLoading,
    makeSelectHasLicensesAvailable,
    makeSelectPagination,
    makeSelectEmployeesSimple,
    makeSelectDownloadUrl,
    makeSelectProductSeats,
    makeSelectSubmitted
};
