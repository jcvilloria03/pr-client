import { company } from 'utils/CompanyService';
import { auth } from 'utils/AuthService';

/**
 * Helper for persisting filters
 */
export const GetAccountAndCompanyId = () => ({
    accountId: auth.getUser().account_id,
    companyId: company.getLastActiveCompanyId()
});

/**
 * Parser for retrieving/setting filter
 */
export const ParseFilterKey = ( accountId, companyId ) => `employeesFilter&accountId=${accountId}&companyId=${companyId}`;

 /**
  * Helper for persisting filters
  */
export const PersistFilter = ( filterKey, filters ) =>
     localStorage.setItem( filterKey, JSON.stringify( filters ) );

 /**
  * Helper for retrieving filters. Returns null if nothing is retrieved
  */
export const RetrievePersistedFilter = ( filterKey ) =>
     JSON.parse( localStorage.getItem( filterKey ) );

 /**
  * Remove the filter for this user/company
  */
export const RemovePersistedFilter = ( filterKey ) =>
     localStorage.removeItem( filterKey );

/**
 * Exist only to verify the persistentFilter
 */
export const VerifyPersistentFilter = () => {
    const { accountId, companyId } = GetAccountAndCompanyId();
    return !!localStorage.getItem( ParseFilterKey( accountId, companyId ) );
};

