import React from 'react';

import { subscriptionService } from 'utils/SubscriptionService';
import { Fetch } from 'utils/request';

import Button from 'components/Button';
import MultiSelect from 'components/MultiSelect';
import Select from 'components/Select';

import {
    GetAccountAndCompanyId,
    ParseFilterKey,
    PersistFilter,
    RetrievePersistedFilter,
    RemovePersistedFilter
} from './persistentHelpers';

import { FilterWrapper } from './styles';

const EMPLOYEE_STATUS = [
    {
        value: 'active',
        label: 'Active',
        disabled: false
    },
    {
        value: 'semi-active',
        label: 'Semi-active',
        disabled: false
    },
    {
        value: 'inactive',
        label: 'Inactive',
        disabled: false
    }
];

/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {
    static propTypes = {
        products: React.PropTypes.array,
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    constructor( props ) {
        super( props );
        this.state = {
            companyId: null,
            persistentFilterKey: null,
            defaultFilters: {
                enabled: false,
                employeeStatus: EMPLOYEE_STATUS[ 0 ],
                location: [],
                department: [],
                payrollGroup: []
            }

        };

        this.onApply = this.onApply.bind( this );
    }

    componentWillMount() {
        const { accountId, companyId } = GetAccountAndCompanyId();

        this.setState({
            persistentFilterKey: ParseFilterKey( accountId, companyId ),
            companyId
        }, () => {
            this.retrieveFilters( RetrievePersistedFilter( this.state.persistentFilterKey ) );
        });
    }

    onApply = ( callback = ( () => null ), enabled = true ) => {
        const filters = {
            employeeStatus: this.employeeStatus && this.employeeStatus.state.value,
            location: this.location && this.location.state.value,
            department: this.department && this.department.state.value,
            payrollGroup: this.payrollGroup && this.payrollGroup.state.value

        };
        const employeeStatus = filters.employeeStatus.value || '';
        const locationValue = filters.location || [];
        const departmentValue = filters.department || [];
        const payrollGroupValue = filters.payrollGroup || [];

        const location = locationValue.map( ( data ) => ( data.value ) );
        const department = departmentValue.map( ( data ) => ( data.value ) );
        const payrollGroup = payrollGroupValue.map( ( data ) => ( data.value ) );

        const filterValues = {
            enabled,
            employeeStatus,
            location,
            department,
            payrollGroup
        };

        this.props.onApply( filterValues );
        PersistFilter( this.state.persistentFilterKey, filters );
        callback();
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    retrieveFilters = ( defaultFilters ) => {
        if ( defaultFilters ) {
            this.setState({ defaultFilters }, () => {
                this.onApply();
            });
        }
    }

    loadFilterLocations = ( keyword, callback ) => {
        const { companyId } = this.state;

        Fetch( `/philippine/company/${companyId}/locations?keyword=${keyword}`, { method: 'GET' })
          .then( ( result ) => {
              const locations = result.data.map( ({ id, name }) => ({
                  value: id,
                  label: name,
                  disabled: false
              }) );
              callback( null, { options: locations });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    loadFilterDepartments = ( keyword, callback ) => {
        const { companyId } = this.state;

        Fetch( `/company/${companyId}/departments?keyword=${keyword}`, { method: 'GET' })
          .then( ( result ) => {
              const departments = result.data.map( ({ id, name }) => ({
                  value: id,
                  label: name,
                  disabled: false
              }) );
              callback( null, { options: departments });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    loadFilterPayrollGroups = ( keyword, callback ) => {
        const { companyId } = this.state;

        Fetch( `/philippine/company/${companyId}/payroll_groups?keyword=${keyword}`, { method: 'GET' })
          .then( ( result ) => {
              const payrollGroup = result.data.map( ({ id, name }) => ({
                  value: id,
                  label: name,
                  disabled: false
              }) );
              callback( null, { options: payrollGroup });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    resetFilters = () => {
        this.employeeStatus.setState({ value: EMPLOYEE_STATUS[ 0 ] });
        this.location.setState({ value: null });
        this.department.setState({ value: null });
        this.payrollGroup.setState({ value: null }, () => {
            this.setState({
                filters: {
                    ...this.state.filters,
                    enabled: false
                }
            });
            this.onApply( ( () => RemovePersistedFilter( this.state.persistentFilterKey ) ), false );
        });
    }

    render() {
        const {
            employeeStatus,
            location,
            department,
            payrollGroup
        } = this.state.defaultFilters;
        return (
            <FilterWrapper>
                <div className="row" key="1">
                    <div className="col-xs-3">
                        <Select
                            id="employee-status"
                            label="Employee Status"
                            placeholder="Employee Status"
                            data={ EMPLOYEE_STATUS }
                            value={ employeeStatus }
                            ref={ ( ref ) => { this.employeeStatus = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            async
                            id="location"
                            label="Location"
                            loadOptions={ this.loadFilterLocations }
                            value={ location }
                            placeholder="All locations"
                            ref={ ( ref ) => { this.location = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            async
                            id="department"
                            label="Department"
                            loadOptions={ this.loadFilterDepartments }
                            value={ department }
                            placeholder="All departments"
                            ref={ ( ref ) => { this.department = ref; } }
                        />
                    </div>
                    {
                        subscriptionService.isSubscribedToPayroll( this.props.products ) &&
                        <div className="col-xs-3">
                            <MultiSelect
                                async
                                id="payroll-groups"
                                label="Payroll Groups"
                                placeholder="All Payroll Groups"
                                value={ payrollGroup }
                                loadOptions={ this.loadFilterPayrollGroups }
                                ref={ ( ref ) => { this.payrollGroup = ref; } }
                            />
                        </div>
                    }
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
