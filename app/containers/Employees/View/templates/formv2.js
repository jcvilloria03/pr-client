import React from 'react';
import Input from 'components/Input';
import Select from 'components/Select';
import Switch from 'components/Switch';
import Switch2 from 'components/Switch2';
import ToolTip from 'components/ToolTip';

/**
 * Form utility
 */
function renderField( config ) {
    const {
        field_type: fieldType,
        id,
        label,
        placeholder,
        validations,
        options,
        ...rest
    } = config;

    const inputNew = true;

    switch ( fieldType ) {
        case 'select':
            return (
                <Select
                    new
                    id={ `${id}_select` }
                    label={ label }
                    placeholder={ placeholder }
                    required={ validations.required }
                    data={ options }
                    { ...rest }
                />
            );
        case 'switch':
        case 'switch2': {
            const SwitchComponent = fieldType === 'switch' ? Switch : Switch2;

            return (
                <div style={ { display: 'flex', justifyContent: 'space-between' } }>
                    { label && (
                        <div className="switch-label">{ label }</div>
                    ) }

                    <span>
                        { placeholder } &nbsp;
                        <ToolTip
                            id="tooltipBasicInformation"
                            target={ <i className="fa fa-question-circle" /> }
                            placement="left"
                        >
                            When this is set to active, the employee will receive a verification email and will consume one license based on the active subscription plan of the account.
                        </ToolTip>
                    </span>

                    <SwitchComponent
                        new
                        id={ `${id}_switch` }
                        { ...rest }
                    />
                </div>
            );
        }
        case 'input':
        default:
            return (
                <Input
                    new={ inputNew }
                    id={ `${id}_input` }
                    label={ label }
                    placeholder={ placeholder }
                    required={ validations.required }
                    minNumber={ validations.minNumber }
                    maxNumber={ validations.maxNumber }
                    { ...rest }
                />
            );
    }
}

export { renderField };
