export const FILTER_TYPES = {
    LOCATION: 'location',
    POSITION: 'position',
    DEPARTMENT: 'department'
};
