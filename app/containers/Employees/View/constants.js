/*
 *
 * Employees constants
 *
 */

export const INITIALIZE = 'app/Employees/INITIALIZE';
export const LOADING = 'app/Employees/LOADING';
export const TABLE_LOADING = 'app/Employees/TABLE_LOADING';
export const DOWNLOAD_LOADING = 'app/Employees/DOWNLOAD_LOADING';
export const SET_EMPLOYEES_SIMPLE = 'app/Employees/SET_EMPLOYEES_SIMPLE';
export const NOTIFICATION_SAGA = 'app/Employees/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Employees/NOTIFICATION';
export const SET_FILTER_DATA = 'app/Employees/SET_FILTER_DATA';
export const SET_FILTER_LOCATIONS = 'app/Employees/SET_FILTER_LOCATIONS';
export const SET_FILTER_DEPARTMENTS = 'app/Employees/SET_FILTER_DEPARTMENTS';
export const SET_FILTER_PAYROLL_GROUPS = 'app/Employees/SET_FILTER_PAYROLL_GROUPS';
export const GET_FILTER_LOCATIONS = 'app/Employees/GET_FILTER_LOCATIONS';
export const GET_FILTER_DEPARTMENTS = 'app/Employees/GET_FILTER_DEPARTMENTS';
export const GET_FILTER_PAYROLL_GROUPS = 'app/Employees/GET_FILTER_PAYROLL_GROUPS';
export const CHECK_LICENSES_AVAILABLE = 'app/Employees/CHECK_LICENSES_AVAILABLE';
export const SET_HAS_LICENSES_AVAILABLE = 'app/Employees/SET_HAS_LICENSES_AVAILABLE';
export const GET_EMPLOYEES_SIMPLE = 'app/Employees/GET_EMPLOYEES_SIMPLE';
export const GET_EMPLOYEE_ROLES = 'app/Employees/GET_EMPLOYEE_ROLES';
export const SET_EMPLOYEE_ROLES = 'app/Employees/SET_EMPLOYEE_ROLES';
export const GET_EMPLOYEE_LOCATIONS = 'app/Employees/GET_EMPLOYEE_LOCATIONS';
export const SET_EMPLOYEE_LOCATIONS = 'app/Employees/SET_EMPLOYEE_LOCATIONS';
export const SET_EMPLOYEES_PAGINATION = 'app/Employees/SET_EMPLOYEES_PAGINATION';
export const GENERATE_MASTERFILE = 'app/Employees/GENERATE_MASTERFILE';
export const SET_COMPANY_ID = 'app/Employees/SET_COMPANY_ID';
export const CHECK_FOR_USER_MATCH = 'app/Employees/CHECK_FOR_USER_MATCH';
export const SUBMIT_FORM = 'app/Employees/SUBMIT_FORM';
export const SUBMITTED = 'app/Employees/SUBMITTED';
export const SET_ERRORS = 'app/Employees/SET_ERRORS';

export const DEFAULT_ORDER_BY = 'first_name';
export const DEFAULT_ORDER_DIR = 'asc';
export const INITIAL_EMPLOYEES_PAYLOAD = {
    filter: {
        department: [],
        employeeStatus: 'active',
        enabled: false,
        location: [],
        payrollGroup: []
    },
    orderBy: DEFAULT_ORDER_BY,
    orderDir: DEFAULT_ORDER_DIR,
    page: 1,
    perPage: 10,
    searchKeyword: ''
};

export const INITIAL_EMPLOYEE_DATA = {
    employee_id: null,
    last_name: null,
    middle_name: null,
    first_name: null,
    email: null,
    primary_location_id: 'Head Office',
    role_id: null,
    active: true
};

// Fields to be rendered for People
export const PEOPLE_FIELDS = {
    sections: [
        {
            title: 'Basic Information',
            rows: [
                [
                    {
                        field_type: 'input',
                        class_name: 'col-xs-12',
                        id: 'employee_id',
                        label: 'Employee ID',
                        placeholder: 'Enter Employee ID here',
                        validations: {
                            required: true
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-12',
                        id: 'first_name',
                        label: 'First Name',
                        placeholder: 'Enter employee first name',
                        validations: {
                            required: true
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-12',
                        id: 'middle_name',
                        label: 'Middle Name',
                        placeholder: 'Enter employee middle name (optional)',
                        validations: {
                            required: false
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-12',
                        id: 'last_name',
                        label: 'Last Name',
                        placeholder: 'Enter employee last name',
                        validations: {
                            required: true
                        }
                    },
                    {
                        field_type: 'input',
                        class_name: 'col-xs-12',
                        id: 'email',
                        label: 'Email Address',
                        placeholder: 'Enter employee email address',
                        validations: {
                            required: true
                        },
                        type: 'email'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-12',
                        id: 'primary_location_id',
                        label: 'Primary Location',
                        placeholder: 'Select a location',
                        validations: {
                            required: false
                        },
                        options: 'locations'
                    },
                    {
                        field_type: 'select',
                        class_name: 'col-xs-12',
                        id: 'role_id',
                        label: 'User Role',
                        placeholder: 'Select user role',
                        validations: {
                            required: true
                        },
                        options: 'roles'
                    },
                    {
                        field_type: 'switch',
                        class_name: 'col-xs-12 switch',
                        id: 'active',
                        placeholder: 'Auto-invite and Set to active'
                    }
                ]
            ]
        }
    ]
};
