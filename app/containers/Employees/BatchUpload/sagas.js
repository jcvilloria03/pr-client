/* eslint-disable camelcase */

import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';
import { auth } from 'utils/AuthService';

import { saveJobId } from './actions';

import {
    UPLOAD_FILE,
    GET_PREVIEW,
    SAVE_EMPLOYEES,
    SAVING_STATUS,
    STATUS,
    ERRORS,
    PREVIEWS,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    CHECK_LICENSES_AVAILABLE,
    SET_HAS_LICENSES_AVAILABLE
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* uploadPersonalInfo({ payload }) {
    const is_subscribed_to_ta_only = subscriptionService.isSubscribedToTAOnly( payload.products );
    const is_subscribed_to_payroll_only = subscriptionService.isSubscribedToPayrollOnly( payload.products );
    const companyId = company.getLastActiveCompanyId();
    try {
        const data = new FormData();
        data.append( 'company_id', companyId );
        data.append( 'file', payload.file );

        const url = `/employee/batch_add/${
            is_subscribed_to_ta_only
              ? 'time_attendance_info'
              : is_subscribed_to_payroll_only
                ? 'payroll_info'
                : 'people'
        }`;

        const upload = yield call( Fetch, url, {
            method: 'POST',
            data
        });
        if ( upload.id ) {
            yield put({
                type: STATUS,
                payload: 'validation_queued'
            });
            yield put( saveJobId( upload.id ) );
        }

        yield call( checkValidation, { payload: {
            step: is_subscribed_to_ta_only
              ? 'time_attendance'
              : is_subscribed_to_payroll_only
                ? 'payroll'
                : 'people',
            job_id: upload.id
        }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* saveEmployees({ payload }) {
    const is_subscribed_to_ta_only = subscriptionService.isSubscribedToTAOnly( payload.products );
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put({
            type: SAVING_STATUS,
            payload: 'saving'
        });

        const url = is_subscribed_to_ta_only ? '/employee/batch_add/ta_save' : '/employee/batch_add/save';

        const upload = yield call( Fetch, url, {
            method: 'POST',
            data: {
                company_id: companyId,
                job_id: payload.job_id
            }
        });

        if ( upload.id ) {
            yield call( checkValidation, { payload: {
                step: is_subscribed_to_ta_only ? 'ta_save' : 'save',
                job_id: payload.job_id
            }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* getPreview({ payload }) {
    const companyId = company.getLastActiveCompanyId();
    try {
        yield put({
            type: STATUS,
            payload: 'busy'
        });
        const preview = yield call( Fetch, `/employee/batch_add/preview?company_id=${companyId}&job_id=${payload.job_id}`, {
            method: 'GET'
        });
        yield put({
            type: PREVIEWS,
            payload: preview
        });
        yield put({
            type: STATUS,
            payload: 'ready'
        });
    } catch ( error ) {
        yield put({
            type: STATUS,
            payload: 'ready'
        });
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    const companyId = company.getLastActiveCompanyId();
    try {
        const check = yield call( Fetch, `/employee/batch_add/status?company_id=${companyId}&job_id=${payload.job_id}&step=${payload.step}`, {
            method: 'GET'
        });

        if ([ 'people', 'time_attendance', 'payroll' ].includes( payload.step ) ) {
            if ( check.status === 'validation_failed' ) {
                yield put({
                    type: STATUS,
                    payload: check.status
                });
                yield put({
                    type: ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'validated' ) {
                yield call( getPreview, { payload: { job_id: payload.job_id }});
                yield put({
                    type: STATUS,
                    payload: check.status
                });
                yield put({
                    type: ERRORS,
                    payload: {}
                });
            } else {
                yield put({
                    type: STATUS,
                    payload: check.status
                });
                yield call( delay, 2000 );
                yield call( checkValidation, { payload: { step: payload.step, job_id: payload.job_id }});
            }
        } else if ( payload.step === 'save' || payload.step === 'ta_save' ) {
            if ( check.status === 'save_failed' ) {
                yield put({
                    type: SAVING_STATUS,
                    payload: check.status
                });
                yield put({
                    type: ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'saved' ) {
                yield put({
                    type: SAVING_STATUS,
                    payload: check.status
                });
                yield call( notifyUser, {
                    title: 'Success',
                    message: 'Successfully added the employee profile of your employees.',
                    show: true,
                    type: 'success'
                });
                yield call( browserHistory.push, '/employees', true );
            } else {
                yield put({
                    type: SAVING_STATUS,
                    payload: check.status
                });
                yield call( delay, 2000 );
                yield call( checkValidation, { payload: { step: payload.step, job_id: payload.job_id }});
            }
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Check if licenses available for specified user products
 */
export function* checkLicensesAvailable() {
    const products = auth.getProducts();
    const subscriptionId = yield call( subscriptionService.getSubscriptionId );

    try {
        const { data } = yield call( Fetch, `/subscriptions/${subscriptionId}/stats`, { method: 'GET' });
        const currentProduct = data.products.find( ( product ) => products.indexOf( product.product_code ) !== -1 );

        if ( currentProduct ) {
            const hasLicensesAvailable = currentProduct.licenses_available > 0;

            yield put({
                type: SET_HAS_LICENSES_AVAILABLE,
                payload: hasLicensesAvailable
            });
        }
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( checkLicensesAvailable );
}

/**
 * Individual exports for testing
 */
export function* watchForPersonalInfoUpload() {
    const watcher = yield takeLatest( UPLOAD_FILE, uploadPersonalInfo );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetPreview() {
    const watcher = yield takeEvery( GET_PREVIEW, getPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForSaveEmployees() {
    const watcher = yield takeEvery( SAVE_EMPLOYEES, saveEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForCheckLicensesAvailable() {
    const watcher = yield takeEvery( CHECK_LICENSES_AVAILABLE, checkLicensesAvailable );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForPersonalInfoUpload,
    watchForGetPreview,
    watchForSaveEmployees,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForCheckLicensesAvailable
];
