/* eslint-disable camelcase */
/* eslint-disable react/sort-comp */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import { isEmpty, isEqual } from 'lodash';

import A from 'components/A';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import Table from 'components/Table';
import Button from 'components/Button';
import SnackBar from 'components/SnackBar';
import FileInput from 'components/FileInput';
import { H4, H6, P } from 'components/Typography';
import FooterTablePagination from 'components/FooterTablePagination';

import { browserHistory } from 'utils/BrowserHistory';
import { formatDate, formatPaginationLabel } from 'utils/functions';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from 'utils/constants';
import { subscriptionService } from 'utils/SubscriptionService';

import EmployeeGuide from '../../Guide/EmployeeGuide/BatchAdd/index';
import SubHeader from '../../SubHeader';

import * as batchUploadActions from './actions';

import {
  PageWrapper,
  StyledLoader,
  ModalBodyWrapper,
  ModalFooterWrapper,
  DemoInfo
} from './styles';

import {
  makeSelectEmployees,
  makeSelectNotification,
  makeSelectProductsState,
  makeSelectHasLicensesAvailable
} from './selectors';

import {
  TIME_AND_ATTENDANCE_ONLY_COLUMNS,
  PAYROLL_ONLY_COLUMNS,
  PREVIEW_TABLE_COLUMN,
  ERROR_TABLE_COLUMN
} from './constants';
import { company } from '../../../utils/CompanyService';

/**
 * EmployeeBatchUpload Container
 */
export class EmployeeBatchUpload extends React.Component {
    static propTypes = {
        reset: React.PropTypes.func,
        uploadPersonalInfo: React.PropTypes.func,
        saveEmployees: React.PropTypes.func,
        checkLicensesAvailable: React.PropTypes.func,
        employees: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        hasLicensesAvailable: React.PropTypes.bool,
    }

    /**
     * component's constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            jobID: null,
            savingStatus: '',
            errors: {},
            preview: [],
            status: null,
            submit: false,
            file: null,
            tableData: [],
            tableColumns: [],
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            },
            is_subscribed_to_payroll_only: false,
            is_subscribed_to_ta_only: false,
            is_subscribed_to_ta_plus_payroll: false,
            hasNoLicenses: false
        };

        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
    }

    /**
     * this displays all existing locations onload
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.employees, this.props.employees ) ) {
            this.setState({
                savingStatus: nextProps.employees.savingStatus,
                preview: nextProps.employees.previews,
                errors: nextProps.employees.errors,
                status: nextProps.employees.status,
                jobID: nextProps.employees.jobID,
                tableColumns: this.getTableColumns( nextProps.employees ),
                tableData: nextProps.employees.status === 'validation_failed'
                  ? this.errorPreviewData( nextProps.employees.errors, nextProps.employees.status )
                  : this.getPreviewData( nextProps.employees.previews, nextProps.employees.status )
            });
        }

        if ( !isEqual( nextProps.employees.previews, this.props.employees.previews ) ) {
            this.setState({
                pagination: {
                    total: nextProps.employees.previews.length,
                    per_page: 10,
                    current_page: 1,
                    last_page: Math.ceil( nextProps.employees.previews.length / 10 ),
                    to: 0
                }
            });
        }

        if ( !isEqual( nextProps.employees.errors, this.props.employees.errors ) ) {
            if ( nextProps.employees.errors ) {
                this.setState({
                    pagination: {
                        total: Object.keys( nextProps.employees.errors ).length,
                        per_page: 10,
                        current_page: 1,
                        last_page: Math.ceil( Object.keys( nextProps.employees.errors ).length / 10 ),
                        to: 0
                    }
                });
            }
        }
    }

    componentDidMount() {
        this.getSubscriptionStatus();
        this.props.checkLicensesAvailable();
    }

    getSubscriptionStatus() {
        this.setState({
            is_subscribed_to_ta_only: subscriptionService.isSubscribedToTAOnly( this.props.products ),
            is_subscribed_to_payroll_only: subscriptionService.isSubscribedToPayrollOnly( this.props.products ),
            is_subscribed_to_ta_plus_payroll: subscriptionService.isSubscribedToBothPayrollAndTA( this.props.products )
        });
    }

    handleTableChanges = ( tableProps = this.previewTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.state.tableData.length });
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    /**
     * this renders the personal infomation upload section of the add employees
     */
    errorPreviewData( data, status ) {
        const errorData = [];

        if ( Object.keys( data ).length > 0 && status === 'validation_failed' ) {
            const keys = Object.keys( data );

            keys.forEach( ( key ) => {
                errorData.push({
                    row: <h6 style={ { margin: '0', textAlign: 'center' } }>{ key }</h6>,
                    issue: <ul>{data[ key ].map( ( error, index ) => <li key={ index } style={ { margin: '0' } } >{error}</li> ) }</ul>
                });
            });
        }

        return errorData;
    }

    getTableColumns( data ) {
        const { is_subscribed_to_ta_only, is_subscribed_to_payroll_only, is_subscribed_to_ta_plus_payroll } = this.state;
        let tableColumns = [...PREVIEW_TABLE_COLUMN];

        if ( data.status === 'validation_failed' ) {
            return ERROR_TABLE_COLUMN;
        }

        if ( is_subscribed_to_ta_only ) {
            tableColumns = [ ...tableColumns, ...TIME_AND_ATTENDANCE_ONLY_COLUMNS ];
        }

        if ( is_subscribed_to_payroll_only ) {
            tableColumns = [ ...tableColumns, ...PAYROLL_ONLY_COLUMNS ];
        }

        if ( is_subscribed_to_ta_plus_payroll ) {
            tableColumns = [ ...tableColumns, ...TIME_AND_ATTENDANCE_ONLY_COLUMNS, ...PAYROLL_ONLY_COLUMNS ];
        }

        return tableColumns;
    }

    /**
     * Table Preview of validated data.
     */
    getPreviewData( data, status ) {
        const previewData = [];

        if ( data.length > 0 && status === 'validated' ) {
            data.forEach( ( employee ) => {
                previewData.push({
                    employee_id: employee.employee_id,
                    last_name: employee.last_name,
                    first_name: employee.first_name,
                    middle_name: employee.middle_name,
                    email: employee.email,
                    active: employee.active,
                    telephone_number: employee.telephone_number,
                    mobile_number: employee.mobile_number,
                    address_line_1: employee.address_line_1,
                    address_line_2: employee.address_line_2,
                    country: employee.country,
                    city: employee.city,
                    zip_code: employee.zip_code,
                    birth_date: employee.birth_date ? formatDate( employee.birth_date, DATE_FORMATS.API ) : '',
                    gender: employee.gender,
                    status: employee.status,
                    date_hired: employee.date_hired ? formatDate( employee.date_hired, DATE_FORMATS.DISPLAY ) : '',
                    date_ended: employee.date_ended ? formatDate( employee.date_ended, DATE_FORMATS.DISPLAY ) : '',
                    employment_type_name: employee.employment_type_name,
                    position_name: employee.position_name,
                    department_name: employee.department_name,
                    rank_name: employee.rank_name,
                    team_name: employee.team_name,
                    team_role: employee.team_role,
                    primary_location_name: employee.primary_location_name,
                    secondary_location_name: employee.secondary_location_name,
                    timesheet_required: employee.timesheet_required ? 'Yes' : 'No',
                    overtime: employee.overtime ? 'Yes' : 'No',
                    differential: employee.differential ? 'Yes' : 'No',
                    regular_holiday_pay: employee.regular_holiday_pay ? 'Yes' : 'No',
                    special_holiday_pay: employee.special_holiday_pay ? 'Yes' : 'No',
                    holiday_premium_pay: employee.holiday_premium_pay ? 'Yes' : 'No',
                    rest_day_pay: employee.rest_day_pay ? 'Yes' : 'No',
                    hours_per_day: employee.hours_per_day,
                    tax_type: employee.tax_type,
                    consultant_tax_rate: employee.consultant_tax_rate,
                    tax_status: employee.tax_status,
                    base_pay: employee.base_pay,
                    base_pay_unit: employee.base_pay_unit,
                    payroll_group_name: employee.payroll_group_name,
                    cost_center_name: employee.cost_center_name,
                    payment_method: employee.payment_method,
                    bank_name: employee.bank_name,
                    bank_type: employee.bank_type,
                    bank_account_number: employee.bank_account_number,
                    sss_basis: employee.sss_basis,
                    sss_amount: employee.sss_amount,
                    philhealth_basis: employee.philhealth_basis,
                    philhealth_amount: employee.philhealth_amount,
                    hdmf_basis: employee.hdmf_basis,
                    hdmf_amount: employee.hdmf_amount,
                    tin: employee.tin,
                    rdo: employee.rdo,
                    sss_number: employee.sss_number,
                    hdmf_number: employee.hdmf_number,
                    philhealth_number: employee.philhealth_number,
                    entitled_deminimis: employee.entitled_deminimis
                });
            });
        }

        return previewData;
    }

    reset() {
        this.props.reset({
            status: '',
            errors: {},
            previews: []
        });
        this.setState({
            errors: {},
            savingStatus: '',
            status: null,
            submit: false,
            previews: [],
            tableData: [],
            tableColumns: [],
            hasNoLicenses: false
        });
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.previewTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.previewTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    handleUploadGuide = () => {
        this.uploadEmployeeGuideModal.toggle();
    }

    handleTemplateDownload = () => {
        const { is_subscribed_to_ta_only, is_subscribed_to_payroll_only, is_subscribed_to_ta_plus_payroll } = this.state;

        if ( is_subscribed_to_ta_plus_payroll ) {
            window.location.assign( 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/employees.csv' );
        }

        if ( is_subscribed_to_ta_only ) {
            window.location.assign( 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/employees_tna_only.csv' );
        }

        if ( is_subscribed_to_payroll_only ) {
            window.location.assign( 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/employees_payroll_only.csv' );
        }
    }

    /**
     * renders component to DOM
     */
    render() {
        const isDemo = company.isDemoCompany();
        return (
            <div>
                <Helmet
                    title="Add Multiple People"
                    meta={ [
                        { name: 'description', content: 'Add Multiple People' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                { this.state.hasNoLicenses && (
                    <SnackBar
                        message="You don't have sufficient licenses to add any Employees, please go to the License Page in the Control Panel to add additional licenses."
                        title= 'Error'
                        offset={ { top: 70 } }
                        show={ this.state.hasNoLicenses }
                        ref={ ( ref ) => { this.state.hasNoLicenses = ref; } }
                        delay={ 5000 }
                        type="error"
                    />
                ) }

                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => { e.preventDefault(); browserHistory.push( '/employees', true ); } }
                            >
                                &#8592; Back to Employees
                            </A>
                        </Container>
                    </div>
                    <Container>
                        <div className="heading">
                            <h3>Add Multiple People</h3>
                            <p>You can add, change, & manage employee records like personal, contact, employment, basic pay, government contributions, and government IDs information here. Some of this information may be referenced by both payroll & time modules during processing.</p>
                        </div>
                        <div className="employees-form">
                            <div className="personal">
                                <div className="row steps">
                                    <div className="step col-sm-6">
                                        <div className="template">
                                            <H6>Step 1:</H6>
                                            <p>Download and fill-out the Employee Template.</p>
                                            <P><A onClick={ this.handleUploadGuide }>Click here to view the upload guide</A></P>
                                            <Button
                                                label={ <span><Icon className="download-icon" name="download" /> Download CSV Template</span> }
                                                type="neutral"
                                                onClick={ this.handleTemplateDownload }
                                            />
                                        </div>
                                    </div>
                                    <div className="step col-sm-6">
                                        <div className="upload">
                                            <H6>Step 2:</H6>
                                            <p>After completely filling out the Employee template, choose and upload it here.</p>
                                            <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'none' : 'block' } } >
                                                <FileInput
                                                    accept=".csv"
                                                    onDrop={ ( files ) => {
                                                        const { acceptedFiles } = files;

                                                        this.setState({
                                                            file: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null
                                                        }, () => this.reset() );
                                                    } }
                                                    ref={ ( ref ) => { this.personalInput = ref; } }
                                                    disableClick={ isDemo }
                                                />
                                            </div>
                                            <div style={ { display: this.state.status === 'validated' ? 'block' : 'none' } }>
                                                <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                                            </div>
                                            <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'validated' ? 'none' : 'block' } } >
                                                <Button
                                                    label={
                                                        this.state.submit ? (
                                                            <StyledLoader className="animation">
                                                                UPLOADING <div className="anim3"></div>
                                                            </StyledLoader>
                                                        ) : (
                                                            'UPLOAD'
                                                        )
                                                    }
                                                    size="large"
                                                    disabled={ isEmpty( this.state.file ) }
                                                    type="action"
                                                    ref={ ( ref ) => { this.validatePersonalInfo = ref; } }
                                                    onClick={ () => {
                                                        this.reset();
                                                        this.setState({
                                                            submit: true
                                                        });
                                                        this.validatePersonalInfo.setState({ disabled: true }, () => {
                                                            if ( !this.props.hasLicensesAvailable ) {
                                                                this.setState({ hasNoLicenses: true });
                                                            } else {
                                                                this.props.uploadPersonalInfo({ file: this.state.file, products: this.props.products });
                                                            }

                                                            setTimeout( () => {
                                                                this.setState({ hasNoLicenses: false });
                                                                this.setState({ submit: false });
                                                            }, 5000 );
                                                        });
                                                    } }
                                                />
                                            </div>
                                            <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'block' : 'none' } } >
                                                <StyledLoader className="animation">
                                                    <H4 style={ { margin: '0' } }>{ this.state.status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                                </StyledLoader>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                { this.state.status === 'validating' && (
                                <div style={ { textAlign: 'center', margin: '20px' } }>
                                    <i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" />
                                    <br />
                                    <H4>Loading Data. Please Wait...</H4>
                                </div>
                                ) }

                                { this.state.tableData.length > 0 && (
                                    <div className={ this.state.status === 'validated' ? 'preview' : 'errors' }>
                                        <Table
                                            ref={ ( ref ) => { this.previewTable = ref; } }
                                            onDataChange={ this.handleTableChanges }
                                            columns={ this.state.tableColumns }
                                            data={ this.state.tableData }
                                            page={ this.state.pagination.current_page - 1 }
                                            pageSize={ this.state.pagination.per_page }
                                            pages={ this.state.pagination.total }
                                            external
                                        />
                                    </div>
                                )}

                                {
                                    this.state.status === 'validated' && (
                                    <div className="action_button">
                                        <Button
                                            disabled={ this.state.status !== 'validated' }
                                            label="Cancel"
                                            size="large"
                                            type="neutral"
                                            onClick={ () => { browserHistory.push( '/employees', true ); } }
                                        />
                                        <Button
                                            disabled={ this.state.status !== 'validated' || this.state.savingStatus === 'saving' }
                                            label={ this.state.savingStatus === 'saving' ? <StyledLoader className="animation"> Please Wait <div className="anim3"></div> </StyledLoader> : 'Create' }
                                            size="large"
                                            type="action"
                                            onClick={ () => {
                                                this.props.saveEmployees({ job_id: this.state.jobID, products: this.props.products });
                                            } }
                                        />
                                    </div>
                                    )
                                  }
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <div
                    className="loader"
                    style={ { display: this.state.status === 'validation_failed' || this.state.status === 'validated' ? '' : 'none', position: 'fixed', bottom: '0', background: '#fff', width: '100%' } }
                >
                    <FooterTablePagination
                        page={ this.state.pagination.current_page }
                        pageSize={ this.state.pagination.per_page }
                        pagination={ this.state.pagination }
                        onPageChange={ this.onPageChange }
                        onPageSizeChange={ this.onPageSizeChange }
                        paginationLabel={ this.state.label }
                    />
                </div>
                <Modal
                    ref={ ( ref ) => { this.uploadEmployeeGuideModal = ref; } }
                    size="lg"
                    center
                    title="Employee Upload Guide"
                    body={
                        <ModalBodyWrapper>
                            <EmployeeGuide subscription={ this.props.products } />
                        </ModalBodyWrapper>
                    }
                    footer={
                        <ModalFooterWrapper>
                            <div>
                                <div style={ { color: 'red' } }>* Required Columns</div>
                                <div style={ { color: '#00A5E5' } }>* { "Cell can't be blank if column is present" }</div>
                            </div>
                            <div style={ { display: 'flex', alignItems: 'center' } }>
                                <Button
                                    label={ <span><Icon className="download-icon" name="download" /> Download CSV Template</span> }
                                    type="neutral"
                                    onClick={ this.handleTemplateDownload }
                                />
                                <Button
                                    label="Close"
                                    type="neutral"
                                    onClick={ this.handleUploadGuide }
                                />
                            </div>
                        </ModalFooterWrapper>
                    }
                    buttons={ [] }
                />
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    employees: makeSelectEmployees(),
    products: makeSelectProductsState(),
    hasLicensesAvailable: makeSelectHasLicensesAvailable(),
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        batchUploadActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EmployeeBatchUpload );
