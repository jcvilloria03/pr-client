/*
 *
 * Setup constants
 *
 */
export const GET_EMPLOYEES = 'app/MultipleUpload/GET_EMPLOYEES';
export const GET_PREVIEW = 'app/MultipleUpload/GET_PREVIEW';

export const UPLOAD_FILE = 'app/MultipleUpload/UPLOAD_FILE';

export const SAVE_EMPLOYEES = 'app/MultipleUpload/SAVE_EMPLOYEES';
export const SAVING_STATUS = 'app/MultipleUpload/SAVING_STATUS';
export const SAVE_JOB_ID = 'app/MultipleUpload/SAVE_JOB_ID';

export const RESET = 'app/MultipleUpload/RESET';
export const STATUS = 'app/MultipleUpload/STATUS';
export const ERRORS = 'app/MultipleUpload/ERRORS';
export const PREVIEWS = 'app/MultipleUpload/PREVIEWS';

export const NOTIFICATION = 'app/MultipleUpload/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/MultipleUpload/NOTIFICATION_SAGA';

export const CHECK_LICENSES_AVAILABLE = 'app/Employees/CHECK_LICENSES_AVAILABLE';
export const SET_HAS_LICENSES_AVAILABLE = 'app/Employees/SET_HAS_LICENSES_AVAILABLE';

export const ERROR_TABLE_COLUMN = [
    {
        header: 'Row Number',
        accessor: 'row',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Issue(s)',
        accessor: 'issue',
        minWidth: 550,
        sortable: false
    }
];

export const PREVIEW_TABLE_COLUMN = [
    {
        header: 'Employee Id',
        accessor: 'employee_id',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Last Name',
        accessor: 'last_name',
        minWidth: 200,
        sortable: false
    },
    {
        header: 'First Name',
        accessor: 'first_name',
        minWidth: 200,
        sortable: false
    },
    {
        header: 'Middle Name',
        accessor: 'middle_name',
        minWidth: 200,
        sortable: false
    },
    {
        header: 'Email Address',
        accessor: 'email',
        minWidth: 300,
        sortable: false
    },
    {
        header: 'Employee Status',
        accessor: 'active',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Telephone Number',
        accessor: 'telephone_number',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Mobile Number',
        accessor: 'mobile_number',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Address Line 1',
        accessor: 'address_line_1',
        minWidth: 250,
        sortable: false
    },
    {
        header: 'Address Line 2',
        accessor: 'address_line_2',
        minWidth: 250,
        sortable: false
    },
    {
        header: 'Country',
        accessor: 'country',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'City',
        accessor: 'city',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Zip',
        accessor: 'zip_code',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Birthdate',
        accessor: 'birth_date',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Gender',
        accessor: 'gender',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Marital Status',
        accessor: 'status',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Date Hired',
        accessor: 'date_hired',
        minWidth: 200,
        sortable: false
    },
    {
        header: 'Date Resigned',
        accessor: 'date_ended',
        minWidth: 200,
        sortable: false
    },
    {
        header: 'Primary Location',
        accessor: 'primary_location_name',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Secondary Location',
        accessor: 'secondary_location_name',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Employment Type',
        accessor: 'employment_type_name',
        minWidth: 300,
        sortable: false
    },
    {
        header: 'Position',
        accessor: 'position_name',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Department',
        accessor: 'department_name',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Rank',
        accessor: 'rank_name',
        minWidth: 150,
        sortable: false
    }
];

export const TIME_AND_ATTENDANCE_ONLY_COLUMNS = [
    {
        header: 'Team',
        accessor: 'team_name',
        minWidth: 250,
        sortable: false
    },
    {
        header: 'Team Role',
        accessor: 'team_role',
        minWidth: 250,
        sortable: false
    },
    {
        header: 'Timesheet Required',
        accessor: 'timesheet_required',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Entitled to Overtime',
        accessor: 'overtime',
        minWidth: 180,
        sortable: false
    },
    {
        header: 'Entitled Night Differential',
        accessor: 'differential',
        minWidth: 180,
        sortable: false
    },
    {
        header: 'Entitled Unworked Regular Holiday Pay',
        accessor: 'regular_holiday_pay',
        minWidth: 250,
        sortable: false
    },
    {
        header: 'Entitled Unworked Special Holiday Pay',
        accessor: 'special_holiday_pay',
        minWidth: 250,
        sortable: false
    },
    {
        header: 'Entitled Holiday Premium Pay',
        accessor: 'holiday_premium_pay',
        minWidth: 250,
        sortable: false
    },
    {
        header: 'Entitled Rest Day Pay',
        accessor: 'rest_day_pay',
        minWidth: 200,
        sortable: false
    }
];

export const PAYROLL_ONLY_COLUMNS = [
    {
        header: 'Hours Per Day',
        accessor: 'hours_per_day',
        minWidth: 300,
        sortable: false
    },
    {
        header: 'Tax Type',
        accessor: 'tax_type',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Consultant Tax Rate',
        accessor: 'consultant_tax_rate',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Tax Status',
        accessor: 'tax_status',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Base Pay',
        accessor: 'base_pay',
        minWidth: 250,
        sortable: false
    },
    {
        header: 'Base Pay Unit',
        accessor: 'base_pay_unit',
        minWidth: 250,
        sortable: false
    },
    {
        header: 'Payroll Group',
        accessor: 'payroll_group_name',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Cost Center',
        accessor: 'cost_center_name',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Payment Method',
        accessor: 'payment_method',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Bank Name',
        accessor: 'bank_name',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Bank Type',
        accessor: 'bank_type',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Bank Account No.',
        accessor: 'bank_account_number',
        minWidth: 180,
        sortable: false
    },
    {
        header: 'SSS Basis',
        accessor: 'sss_basis',
        minWidth: 200,
        sortable: false
    },
    {
        header: 'SSS Amount',
        accessor: 'sss_amount',
        minWidth: 200,
        sortable: false
    },
    {
        header: 'PhilHealth Basis',
        accessor: 'philhealth_basis',
        minWidth: 300,
        sortable: false
    },
    {
        header: 'PhilHealth Amount',
        accessor: 'philhealth_amount',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'HDMF Basis',
        accessor: 'hdmf_basis',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'HDMF Amount',
        accessor: 'hdmf_amount',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'TIN',
        accessor: 'tin',
        minWidth: 250,
        sortable: false
    },
    {
        header: 'RDO',
        accessor: 'rdo',
        minWidth: 250,
        sortable: false
    },
    {
        header: 'SSS Number',
        accessor: 'sss_number',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'HDMF Number',
        accessor: 'hdmf_number',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'PhilHealth Number',
        accessor: 'philhealth_number',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Entitled Deminimis',
        accessor: 'entitled_deminimis',
        minWidth: 150,
        sortable: false
    }
];
