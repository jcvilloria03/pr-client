import {
    RESET,
    SAVE_JOB_ID,
    UPLOAD_FILE,
    GET_PREVIEW,
    SAVE_EMPLOYEES,
    NOTIFICATION,
    CHECK_LICENSES_AVAILABLE
} from './constants';

/**
 * upload personal info
 */
export function uploadPersonalInfo( payload ) {
    return {
        type: UPLOAD_FILE,
        payload
    };
}

/**
 * upload payroll info
 */
export function getPreview( companyId, jobId ) {
    return {
        type: GET_PREVIEW,
        payload: {
            company_id: companyId,
            job_id: jobId
        }
    };
}

/**
 * reset
 */
export function reset( payload ) {
    return {
        type: RESET,
        payload
    };
}

/**
 * save job id
 */
export function saveJobId( payload ) {
    return {
        type: SAVE_JOB_ID,
        payload
    };
}

/**
 * upload payroll info
 */
export function saveEmployees( payload ) {
    return {
        type: SAVE_EMPLOYEES,
        payload
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Get subscription stats for check for Add Employees button
 *
 * @param {String} currentUserProduct Product code for current user logged in
 */
export function checkLicensesAvailable( currentUserProduct ) {
    return {
        type: CHECK_LICENSES_AVAILABLE,
        payload: {
            currentUserProduct
        }
    };
}
