import { fromJS } from 'immutable';

import {
    RESET,
    STATUS,
    ERRORS,
    SAVE_JOB_ID,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    employees: {
        jobID: '',
        status: '',
        errors: {}
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Batch Upload reducer
 *
 */
function batchUploadReducer( state = initialState, action ) {
    switch ( action.type ) {
        case STATUS:
            return state.setIn([ 'employees', 'status' ], action.payload );
        case ERRORS:
            return state.setIn([ 'employees', 'errors' ], action.payload );
        case SAVE_JOB_ID:
            return state.setIn([ 'employees', 'jobID' ], action.payload );
        case RESET:
            return state.set( 'employees', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default batchUploadReducer;

