/* eslint-disable camelcase */
/* eslint-disable react/sort-comp */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import { isEmpty, isEqual } from 'lodash';

import A from 'components/A';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import Table from 'components/Table';
import Button from 'components/Button';
import SnackBar from 'components/SnackBar';
import FileInput from 'components/FileInput';
import { H4, H6, P } from 'components/Typography';
import FooterTablePagination from 'components/FooterTablePagination';

import { browserHistory } from 'utils/BrowserHistory';
import { formatPaginationLabel } from 'utils/functions';
import { EMPLOYEE_SUBHEADER_ITEMS } from 'utils/constants';
import { subscriptionService } from 'utils/SubscriptionService';

import EmployeeGuide from '../../Guide/EmployeeGuide/BatchAdd/index';
import SubHeader from '../../SubHeader';

import * as batchUploadActions from './actions';

import {
  PageWrapper,
  StyledLoader,
  ModalBodyWrapper,
  ModalFooterWrapper,
  DemoInfo
} from './styles';

import {
  makeSelectEmployees,
  makeSelectNotification,
  makeSelectProductsState
} from './selectors';

import {
  ERROR_TABLE_COLUMN
} from './constants';
import { company } from '../../../utils/CompanyService';

/**
 * EmployeeBatchUpload Container
 */
export class EmployeeBatchUpload extends React.Component {
    static propTypes = {
        reset: React.PropTypes.func,
        uploadPersonalInfo: React.PropTypes.func,
        employees: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    }

    /**
     * component's constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            jobID: null,
            errors: {},
            status: null,
            submit: false,
            file: null,
            tableData: [],
            tableColumns: [],
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            },
            is_subscribed_to_payroll_only: false,
            is_subscribed_to_ta_only: false,
            is_subscribed_to_ta_plus_payroll: false
        };

        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
    }

    /**
     * this displays all existing locations onload
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.employees, this.props.employees ) ) {
            this.setState({
                errors: nextProps.employees.errors,
                status: nextProps.employees.status,
                jobID: nextProps.employees.jobID,
                tableColumns: ERROR_TABLE_COLUMN,
                tableData: this.errorPreviewData( nextProps.employees.errors, nextProps.employees.status )
            });
        }

        if ( !isEqual( nextProps.employees.errors, this.props.employees.errors ) ) {
            if ( nextProps.employees.errors ) {
                this.setState({
                    pagination: {
                        total: Object.keys( nextProps.employees.errors ).length,
                        per_page: 10,
                        current_page: 1,
                        last_page: Math.ceil( Object.keys( nextProps.employees.errors ).length / 10 ),
                        to: 0
                    }
                });
            }
        }
    }

    componentDidMount() {
        this.getSubscriptionStatus();
    }

    getSubscriptionStatus() {
        this.setState({
            is_subscribed_to_ta_only: subscriptionService.isSubscribedToTAOnly( this.props.products ),
            is_subscribed_to_payroll_only: subscriptionService.isSubscribedToPayrollOnly( this.props.products ),
            is_subscribed_to_ta_plus_payroll: subscriptionService.isSubscribedToBothPayrollAndTA( this.props.products )
        });
    }

    handleTableChanges = ( tableProps = this.previewTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.state.tableData.length });
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    /**
     * this renders the personal infomation upload section of the add employees
     */
    errorPreviewData( data, status ) {
        const errorData = [];

        if ( Object.keys( data ).length > 0 && status === 'validation_failed' ) {
            const keys = Object.keys( data );

            keys.forEach( ( key ) => {
                errorData.push({
                    row: <h6 style={ { margin: '0', textAlign: 'center' } }>{ key }</h6>,
                    issue: <ul>{data[ key ].map( ( error, index ) => <li key={ index } style={ { margin: '0' } } >{error}</li> ) }</ul>
                });
            });
        }

        return errorData;
    }

    reset() {
        this.props.reset({
            status: '',
            errors: {}
        });
        this.setState({
            errors: {},
            status: null,
            submit: false,
            tableData: [],
            tableColumns: []
        });
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.previewTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.previewTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    handleUploadGuide = () => {
        this.uploadEmployeeGuideModal.toggle();
    }

    handleTemplateDownload = () => {
        const { is_subscribed_to_ta_only, is_subscribed_to_payroll_only, is_subscribed_to_ta_plus_payroll } = this.state;

        if ( is_subscribed_to_ta_plus_payroll ) {
            window.location.assign( 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/employees_update.csv' );
        }

        if ( is_subscribed_to_ta_only ) {
            window.location.assign( 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/employees_tna_only.csv' );
        }

        if ( is_subscribed_to_payroll_only ) {
            window.location.assign( 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/employees_payroll_only_update.csv' );
        }
    }

    /**
     * renders component to DOM
     */
    render() {
        const isDemo = company.isDemoCompany();
        return (
            <div>
                <Helmet
                    title="Mass Update Profile"
                    meta={ [
                        { name: 'description', content: 'Mass Update Profile' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => { e.preventDefault(); browserHistory.push( '/employees', true ); } }
                            >
                                &#8592; Back to Employees
                            </A>
                        </Container>
                    </div>
                    <Container>
                        <div className="heading">
                            <h3>Mass Update Employee Profile</h3>
                            <p>You can add, change, & manage employee records like personal, contact, employment, basic pay, government contributions, and government IDs information here. Some of this information may be referenced by both payroll & time modules during processing.</p>
                        </div>
                        <div className="employees-form">
                            { isDemo && (
                                <DemoInfo>
                                    <Icon className="info-icon" name="infoCircle" />
                                    <span className="info-message">You are not allowed to modify the employee ID, first name, last name, email address, and other basic information.</span>
                                </DemoInfo> )
                            }
                            <div className="personal">
                                <div className="row steps">
                                    <div className="step col-sm-6">
                                        <div className="template">
                                            <H6>Step 1:</H6>
                                            <p>Download and fill-out the Employee Template.</p>
                                            <P><A onClick={ this.handleUploadGuide }>Click here to view the upload guide</A></P>
                                            <Button
                                                label={ <span><Icon className="download-icon" name="download" /> Download CSV Template</span> }
                                                type="neutral"
                                                onClick={ this.handleTemplateDownload }
                                            />
                                        </div>
                                    </div>
                                    <div className="step col-sm-6">
                                        <div className="upload">
                                            <H6>Step 2:</H6>
                                            <p>After completely filling out the Employee template, choose and upload it here.</p>
                                            <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'saving' ? 'none' : 'block' } } >
                                                <FileInput
                                                    accept=".csv"
                                                    onDrop={ ( files ) => {
                                                        const { acceptedFiles } = files;

                                                        this.setState({
                                                            file: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null
                                                        }, () => this.reset() );
                                                    } }
                                                    ref={ ( ref ) => { this.personalInput = ref; } }
                                                />
                                            </div>
                                            <div style={ { display: this.state.status === 'validated' ? 'block' : 'none' } }>
                                                <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                                            </div>
                                            <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'validated' || this.state.status === 'saving' ? 'none' : 'block' } } >
                                                <Button
                                                    label={
                                                        this.state.submit ? (
                                                            <StyledLoader className="animation">
                                                                UPLOADING <div className="anim3"></div>
                                                            </StyledLoader>
                                                        ) : (
                                                            'UPLOAD'
                                                        )
                                                    }
                                                    size="large"
                                                    disabled={ isEmpty( this.state.file ) || this.state.status === 'saved' }
                                                    type="action"
                                                    ref={ ( ref ) => { this.validatePersonalInfo = ref; } }
                                                    onClick={ () => {
                                                        this.reset();
                                                        this.setState({
                                                            submit: true
                                                        });
                                                        this.validatePersonalInfo.setState({ disabled: true }, () => {
                                                            this.props.uploadPersonalInfo({ file: this.state.file, products: this.props.products });
                                                            setTimeout( () => {
                                                                this.setState({ submit: false });
                                                            }, 5000 );
                                                        });
                                                    } }
                                                />
                                            </div>
                                            <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'saving' ? 'block' : 'none' } } >
                                                <StyledLoader className="animation">
                                                    <H4 style={ { margin: '0' } }>{
                                                    this.state.status === 'saving'
                                                        ? 'SAVING'
                                                        : this.state.status === 'validation_queued'
                                                            ? 'QUEUED FOR VALIDATION'
                                                            : 'VALIDATING'
                                                    }</H4> <div className="anim3"></div>
                                                </StyledLoader>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                { this.state.tableData.length > 0 && (
                                    <div className="errors" >
                                        <Table
                                            ref={ ( ref ) => { this.previewTable = ref; } }
                                            onDataChange={ this.handleTableChanges }
                                            columns={ this.state.tableColumns }
                                            data={ this.state.tableData }
                                            page={ this.state.pagination.current_page - 1 }
                                            pageSize={ this.state.pagination.per_page }
                                            pages={ this.state.pagination.total }
                                            external
                                        />
                                    </div>
                                )}
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <div
                    className="loader"
                    style={ { display: this.state.status === 'validation_failed' || this.state.status === 'validated' ? '' : 'none', position: 'fixed', bottom: '0', background: '#fff', width: '100%' } }
                >
                    <FooterTablePagination
                        page={ this.state.pagination.current_page }
                        pageSize={ this.state.pagination.per_page }
                        pagination={ this.state.pagination }
                        onPageChange={ this.onPageChange }
                        onPageSizeChange={ this.onPageSizeChange }
                        paginationLabel={ this.state.label }
                    />
                </div>
                <Modal
                    ref={ ( ref ) => { this.uploadEmployeeGuideModal = ref; } }
                    size="lg"
                    center
                    title="Employee Upload Guide"
                    body={
                        <ModalBodyWrapper>
                            <EmployeeGuide subscription={ this.props.products } />
                        </ModalBodyWrapper>
                    }
                    footer={
                        <ModalFooterWrapper>
                            <div>
                                <div style={ { color: 'red' } }>* Required Columns</div>
                                <div style={ { color: '#00A5E5' } }>* { "Cell can't be blank if column is present" }</div>
                            </div>
                            <div style={ { display: 'flex', alignItems: 'center' } }>
                                <Button
                                    label={ <span><Icon className="download-icon" name="download" /> Download CSV Template</span> }
                                    type="neutral"
                                    onClick={ this.handleTemplateDownload }
                                />
                                <Button
                                    label="Close"
                                    type="neutral"
                                    onClick={ this.handleUploadGuide }
                                />
                            </div>
                        </ModalFooterWrapper>
                    }
                    buttons={ [] }
                />
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    employees: makeSelectEmployees(),
    products: makeSelectProductsState()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        batchUploadActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EmployeeBatchUpload );
