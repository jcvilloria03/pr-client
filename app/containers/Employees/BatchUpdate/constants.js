/*
 *
 * Setup constants
 *
 */
export const GET_EMPLOYEES = 'app/MultipleUpload/GET_EMPLOYEES';

export const UPLOAD_FILE = 'app/MultipleUpload/UPLOAD_FILE';

export const SAVE_JOB_ID = 'app/MultipleUpload/SAVE_JOB_ID';

export const RESET = 'app/MultipleUpload/RESET';
export const STATUS = 'app/MultipleUpload/STATUS';
export const ERRORS = 'app/MultipleUpload/ERRORS';

export const NOTIFICATION = 'app/MultipleUpload/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/MultipleUpload/NOTIFICATION_SAGA';

export const ERROR_TABLE_COLUMN = [
    {
        header: 'Row Number',
        accessor: 'row',
        minWidth: 150,
        sortable: false
    },
    {
        header: 'Issue(s)',
        accessor: 'issue',
        minWidth: 550,
        sortable: false
    }
];
