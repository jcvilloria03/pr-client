/* eslint-disable camelcase */

import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
// import { subscriptionService } from 'utils/SubscriptionService';

import { saveJobId } from './actions';

import {
    UPLOAD_FILE,
    STATUS,
    ERRORS,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* uploadPersonalInfo({ payload }) {
    const companyId = company.getLastActiveCompanyId();
    try {
        const data = new FormData();
        data.append( 'company_id', companyId );
        data.append( 'file', payload.file );

        const upload = yield call( Fetch, '/employee/batch_update/people', {
            method: 'POST',
            data
        });
        if ( upload.id ) {
            yield put({
                type: STATUS,
                payload: 'validation_queued'
            });
            yield put( saveJobId( upload.id ) );
        }

        yield call( checkValidation, { payload: { step: 'people', job_id: upload.id }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    const companyId = company.getLastActiveCompanyId();
    const retry = payload.retry || 0;
    try {
        const check = yield call( Fetch, `/employee/batch_update/status?company_id=${companyId}&job_id=${payload.job_id}&step=${payload.step}`, {
            method: 'GET'
        });

        if ( check.status === 'validation_failed' ) {
            yield put({
                type: STATUS,
                payload: check.status
            });
            yield put({
                type: ERRORS,
                payload: check.errors
            });
        } else if ( check.status === 'saved' ) {
            yield put({
                type: STATUS,
                payload: check.status
            });
            yield call( notifyUser, {
                title: 'Success',
                message: 'Successfully updated the employee profile of your employees.',
                show: true,
                type: 'success'
            });
            yield call( browserHistory.push, '/employees', true );
        } else {
            yield put({
                type: STATUS,
                payload: check.status
            });
            yield call( delay, 2000 );
            yield call( checkValidation, { payload: { step: payload.step, job_id: payload.job_id }});
        }
    } catch ( error ) {
        if ( retry < 3 ) {
            yield call( delay, 2000 );
            yield call( checkValidation, { payload: { step: payload.step, job_id: payload.job_id, retry: retry + 1 }});
        } else {
            yield call( notifyUser, {
                show: true,
                title: error.response ? error.response.statusText : 'Error',
                message: error.response ? error.response.data.message : error.statusText,
                type: 'error'
            });
        }
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Individual exports for testing
 */
export function* watchForPersonalInfoUpload() {
    const watcher = yield takeLatest( UPLOAD_FILE, uploadPersonalInfo );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForPersonalInfoUpload,
    watchForNotifyUser,
    watchForReinitializePage
];
