import {
    RESET,
    SAVE_JOB_ID,
    UPLOAD_FILE,
    NOTIFICATION
} from './constants';

/**
 * upload personal info
 */
export function uploadPersonalInfo( payload ) {
    return {
        type: UPLOAD_FILE,
        payload
    };
}

/**
 * reset
 */
export function reset( payload ) {
    return {
        type: RESET,
        payload
    };
}

/**
 * save job id
 */
export function saveJobId( payload ) {
    return {
        type: SAVE_JOB_ID,
        payload
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
