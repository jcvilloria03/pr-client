/* eslint-disable no-undef */
/* eslint-disable prefer-rest-params */
/* eslint-disable require-jsdoc */
import React, { PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';

import TagManager from 'react-gtm-module';
import ReactGA from 'react-ga';

import SnackBar from '../../components/SnackBar';
import A from '../../components/A';
import { auth } from '../../utils/AuthService';
import { SUB } from '../../components/Typography';
import {
    StyledWrapper,
    StyledContent
} from './styles';
import salIcon from '../../assets/sal-icon.png';
import thankYouIcon from '../../assets/icons/thank-you.svg';

import {
    TRACKING_ID,
    TAG_MANAGER_ARGS
} from '../../constants';

/**
 *
 * Verify
 *
 */
export class Verify extends React.Component { // eslint-disable-line react/prefer-stateless-function
    /**
     * Component constructor method
     */
    constructor( props ) {
        super( props );
        this.state = {
            notification: {
                title: '',
                message: '',
                type: 'error',
                show: false
            },
            submit: false
        };
        this._handleClick = this._handleClick.bind( this );
        this._verifyCallback = this._verifyCallback.bind( this );
    }

    componentWillMount() {
        this.initGoogleAdWords();
    }

    componentDidMount() {
        this.initGTags();
        if ( auth.emailVerified() ) {
            window.location.href = '/dashboard/';
        }
    }

    initGTags = () => {
        TagManager.initialize( TAG_MANAGER_ARGS );
        ReactGA.initialize( TRACKING_ID );
        ReactGA.pageview( window.location.pathname );
    };

    initGoogleAdWords() {
        const s = document.createElement( 'script' );
        s.setAttribute( 'type', 'text/javascript' );
        s.setAttribute( 'charset', 'UTF-8' );
        s.setAttribute( 'async', 1 );
        s.setAttribute( 'src', 'https://www.googletagmanager.com/gtag/js?id=AW-1008238271' );

        const t = document.getElementsByTagName( 'script' )[ 0 ];
        t.parentNode.insertBefore( s, t );

        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push( arguments ); }

        gtag( 'js', new Date() );
        gtag( 'config', 'AW-1008238271' );
        gtag( 'event', 'conversion', { send_to: 'AW-1008238271/KRXFCJL97vYCEL_94eAD' });
    }

    /**
     * Resends an email verification.
     */
    _handleClick() {
        !this.state.submit && this.setState({
            submit: true
        }, () => {
            auth.resendVerification( this._verifyCallback );
        });
    }

    /**
     * Resends an email verification.
     */
    _verifyCallback( error ) {
        if ( error ) {
            this.setState({
                notification: {
                    title: 'Error',
                    message: 'Something went wrong. Please try again.',
                    type: 'error',
                    show: true
                }
            });
        } else {
            this.setState({
                notification: {
                    title: 'Success',
                    message: 'Email successfully sent. Please check your inbox.',
                    type: 'success',
                    show: true
                }
            });
        }

        this.setState({ submit: false });

        setTimeout( () => {
            this.setState({
                notification: {
                    title: '',
                    message: '',
                    type: 'error',
                    show: false
                }
            });
        }, 5500 );
    }

    /**
     *
     * Verify render method
     *
     */
    render() {
        return (
            <div>
                <StyledWrapper>
                    <Helmet
                        title="Verify your Account"
                        meta={ [
                            { name: 'Account Verification', content: 'Verify your Salarium Account' }
                        ] }
                    />
                    <SnackBar
                        message={ this.state.notification.message }
                        title={ this.state.notification.title }
                        type={ this.state.notification.type }
                        show={ this.state.notification.show }
                        delay={ 5000 }
                        offset={ { top: 0 } }
                        ref={ ( notification ) => { this.notification = notification; } }
                    />
                    <StyledContent>
                        <div id="brand">
                            <img className="sal-icon" src={ salIcon } alt="Salarium" />
                            <div className="thank-you-icon">
                                <img src={ thankYouIcon } alt="Salarium" />
                            </div>
                        </div>
                        <div className="text-content">
                            <p className="main-text">
                                You&apos;re nearly set. We sent you an email, so go ahead and click the link to verify your registration.
                            </p>
                            <p className="main-text">
                                Your 30-day free trial for an effortless payroll system awaits.
                            </p>
                            <SUB>
                                Didn&apos;t receive our email?
                                <br />
                                <A style={ { display: this.state.submit ? 'none' : 'inline' } } onClick={ this._handleClick }>
                                    Resend verification link
                                </A>
                                <span className="spinner" style={ { display: this.state.submit ? 'inline' : 'none' } }>
                                    <span className="rect1"></span>
                                    <span className="rect2"></span>
                                    <span className="rect3"></span>
                                    <span className="rect4"></span>
                                    <span className="rect5"></span>
                                </span>
                            </SUB>
                        </div>
                    </StyledContent>
                    <p>© 2021 Salarium LTD. ALL RIGHTS RESERVED</p>
                </StyledWrapper>
            </div>
        );
    }
}

Verify.propTypes = {
    dispatch: PropTypes.func.isRequired
};

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return {
        dispatch
    };
}

export default connect( null, mapDispatchToProps )( Verify );
