import styled, { keyframes } from 'styled-components';

const stretchdelay = keyframes`
    0%, 40%, 100% {
        transform: scaleY(0.4);
    }
    20% {
        transform: scaleY(1.0);
    }
`;

export const StyledWrapper = styled.div`
    background-color: #f0f4f6;
    min-height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    color: #444;
    padding: 2rem;
`;

export const StyledContent = styled.div`
    background-color: #FFF;
    width: 60%;
    min-width: 400px;
    height: 100%;
    box-shadow: 0px 0px 20px 15px rgba(204,204,204,.2);
    padding-top: 24px;
    margin-bottom: 3rem;

    @media (max-width: 480px) {
        padding: 15px;
        width: 100%;
        min-width: 0;
    }

    #brand {
        text-align: center;
        margin-bottom: 20px;

        h5 {
            margin: 20px 0 6px 0;
            font-weight: 300;
        }
    }

    p {
        text-align: center;
    }
    a {
        color: #00A5E5 !important;
    }

    .spinner {
        margin: 0px 4px;
        display: inline-block;

        & > span {
            background-color: #00A5E5;
            height: 20px;
            width: 4px;
            margin: 0 1px;
            display: inline-block;

            -webkit-animation: ${stretchdelay} 1.2s infinite ease-in-out;
            animation: ${stretchdelay} 1.2s infinite ease-in-out;
        }

        .rect2 {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s;
        }

        .rect3 {
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s;
        }

        .rect4 {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s;
        }

        .rect5 {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s;
        }
    }

    .sal-icon {
        width: 150px;
        margin-bottom: 16px;
    }

    .thank-you-icon {
        width: 100%;
    }

    .text-content {
        width: 100%;
        padding: 0px 24px 40px;
    }

    .main-text {
        font-size: 16px;
    }
`;
