import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import Icon from '../../../components/Icon';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { formatPaginationLabel } from '../../../utils/functions';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { getCompanySettingsSidebarLinks } from '../../../utils/sidebarHelper';

import {
    makeSelectLoading,
    makeSelectEditLoading,
    makeSelectLoanTypes,
    makeSelectNotification,
    makeSelectProductsState
} from './selectors';

import * as loanTypesActions from './actions';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';

import Input from '../../../components/Input';
import Sidebar from '../../../components/Sidebar';
import SnackBar from '../../../components/SnackBar';
import Table from '../../../components/Table';
import { H2, H3, H5 } from '../../../components/Typography';
import Button from '../../../components/Button';
import SalConfirm from '../../../components/SalConfirm';

/**
 * View Loan Types Component
 */
export class View extends React.Component {
    static propTypes = {
        getLoanTypes: React.PropTypes.func,
        deleteLoanTypes: React.PropTypes.func,
        editLoanType: React.PropTypes.func,
        loanTypes: React.PropTypes.array,
        loading: React.PropTypes.bool,
        editLoading: React.PropTypes.bool,
        products: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    };

    static defaultProps = {
        loading: true,
        editLoading: false,
        errors: {}
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: props.loanTypes,
            showDelete: false,
            deleteLabel: '',
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            showModal: false,
            editingRow: null,
            editingRowValue: ''
        };

        this.searchInput = null;
    }

    /**
     * Page authorization check
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.payroll_loan_types',
            'create.payroll_loan_types',
            'delete.payroll_loan_types',
            'edit.payroll_loan_types'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.payroll_loan_types' ];

            if ( authorized ) {
                this.setState({ permission: {
                    view: true,
                    create: true,
                    delete: true,
                    edit: true
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    /**
     * Fetch loan types list
     */
    componentDidMount() {
        this.props.getLoanTypes();
    }

    /**
     * Handle new props
     */
    componentWillReceiveProps( nextProps ) {
        if ( nextProps.loanTypes !== this.props.loanTypes ) {
            this.handleSearch();
            if ( this.props.editLoading ) {
                this.setState({ editingRow: null });
            }
        }
    }

    /**
     * Handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges = ( tableProps = this.loanTypesTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    };

    /**
     * Send a request to delete loan types
     */
    deleteLoanTypes = () => {
        // Get loan type IDs of selected rows
        const loanTypeIDs = [];
        this.loanTypesTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                loanTypeIDs.push( this.loanTypesTable.props.data[ index ].id );
            }
        });
        // dispatch an action to send a request to API
        this.props.deleteLoanTypes( loanTypeIDs );
    };

    /**
     * handles filters and search inputs
     */
    handleSearch = () => {
        let searchQuery = null;

        let dataToDisplay = this.props.loanTypes;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery ) {
            dataToDisplay = dataToDisplay.filter( ( loan ) => {
                let match = false;
                const { name } = loan;
                if ( name.toLowerCase().indexOf( searchQuery ) >= 0 ) {
                    match = true;
                }

                return match;
            });
        }

        this.setState({ displayedData: dataToDisplay, showDelete: false }, () => {
            this.handleTableChanges();
        });
    };

    /**
     * Send a request to edit loan type
     */
    editLoanType = () => {
        if ( !this.props.editLoading ) {
            // dispatch an action to send a request to API
            this.props.editLoanType({ loanTypes: this.props.loanTypes, id: this.state.editingRow, name: this.editingRowValue.state.value });
        }
    };

    /**
     * Component Render Method
     */
    render() {
        const {
            permission,
            displayedData,
            label,
            showDelete,
            deleteLabel,
            showModal
        } = this.state;

        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA: this.props.products && subscriptionService.isSubscribedToTA( this.props.products )
        });

        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Loan Types',
                accessor: 'name',
                style: { textAlign: 'left' },
                render: ({ row }) => (
                    row.id === this.state.editingRow ?
                        <div>
                            <div>
                                <Input
                                    id="name"
                                    className="editing"
                                    value={ row.name }
                                    required
                                    disabled={ this.props.editLoading }
                                    ref={ ( ref ) => { this.editingRowValue = ref; } }
                                />
                            </div>
                        </div>
                        :
                        <div>
                            <div>
                                {row.name}
                            </div>
                        </div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    row.is_editable ?
                        ( row.id !== this.state.editingRow )
                            ? <Button
                                className={ permission.edit ? '' : 'hide' }
                                label={ <span>Edit</span> }
                                type="grey"
                                onClick={ () => { this.setState({ editingRow: row.id }); } }
                            />
                            : <div>
                                <Button
                                    label={ <span>Cancel</span> }
                                    type="neutral"
                                    disabled={ this.props.editLoading }
                                    onClick={ () => { this.setState({ editingRow: null }); } }
                                />
                                <Button
                                    label={ this.props.editLoading ? 'Saving' : <span>Submit</span> }
                                    type="action"
                                    onClick={ () => { this.editLoanType(); } }
                                />
                            </div>
                    : ''
                )
            }
        ];

        return (
            <div>
                <Helmet
                    title="View Loan Types"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.deleteLoanTypes }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                This process cannot be undone.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Entries!"
                    buttonStyle="danger"
                    confirmText="Delete"
                    visible={ showModal }
                />
                <Sidebar items={ sidebarLinks } />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Loan Types.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Loan Types</H3>
                                <p>
                                    Add loan types that will be available for employees through this page.
                                    Aside from government loans, you can add loans that are available in
                                    your company (i.e. Car Loans).
                                </p>
                                <div style={ { textAlign: 'center' } } >
                                    <div className="main">
                                        <Button
                                            className={ permission.create ? '' : 'hide' }
                                            label="Add Loan Types"
                                            size="large"
                                            type="action"
                                            to="/loan-types/add"
                                        />
                                        <Button
                                            className={ permission.create ? '' : 'hide' }
                                            label="Add Loans"
                                            size="large"
                                            type="neutral"
                                            to="/loans/add"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="title">
                                <H5>Loan Types List</H5>
                                <div className="search-wrapper">
                                    <Input
                                        className="search"
                                        id="search"
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                        onChange={ this.handleSearch }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                    />
                                </div>
                                <div className="pull-right">
                                    {
                                        showDelete ?
                                            (
                                                <div>
                                                    { deleteLabel }
                                                    &nbsp;
                                                    <Button
                                                        className={ permission.delete ? '' : 'hide' }
                                                        label={ <span>Delete</span> }
                                                        type="danger"
                                                        disabled={ this.props.editLoading }
                                                        onClick={ () => {
                                                            this.setState({ showModal: false }, () => {
                                                                this.setState({ showModal: true });
                                                            });
                                                        } }
                                                    />
                                                </div>
                                            )
                                            : <div> { label } </div>
                                    }
                                </div>
                            </div>
                            <div className="clearfix"></div>
                            <div className="loan-types">
                                <Table
                                    data={ displayedData }
                                    columns={ tableColumns }
                                    pagination
                                    onDataChange={ this.handleTableChanges }
                                    ref={ ( ref ) => { this.loanTypesTable = ref; } }
                                    selectable
                                    onSelectionChange={ ({ selected }) => {
                                        const selectionLength = selected.filter( ( row ) => row ).length;
                                        this.setState({
                                            showDelete: selectionLength > 0,
                                            deleteLabel: `${selectionLength} item${selectionLength > 1 ? 's' : ''} selected`
                                        });
                                    } }
                                />
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    editLoading: makeSelectEditLoading(),
    loanTypes: makeSelectLoanTypes(),
    notification: makeSelectNotification(),
    products: makeSelectProductsState()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        loanTypesActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
