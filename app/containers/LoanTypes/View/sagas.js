import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import {
    SET_LOADING,
    SET_EDIT_LOADING,
    GET_LOAN_TYPES,
    SET_LOAN_TYPES,
    DELETE_LOAN_TYPES,
    EDIT_LOAN_TYPE,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    UPDATE_LOAN_TYPES
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

/**
 * Get loan types for table
 */
export function* getLoanTypes() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/payroll_loan_types` );
        yield put({
            type: SET_LOAN_TYPES,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Batch delete loan types
 */
export function* deleteLoanTypes({ payload }) {
    try {
        yield call( Fetch, '/payroll_loan_type/bulk_delete', {
            method: 'DELETE',
            data: {
                payroll_loan_type_ids: payload,
                company_id: company.getLastActiveCompanyId()
            }
        });
        yield call( getLoanTypes );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Batch delete loan types
 */
export function* editLoanType({ payload }) {
    try {
        yield put({
            type: SET_EDIT_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const { name, id } = payload;

        const check = yield call( Fetch, `/company/${companyId}/payroll_loan_types/is_name_available`, {
            method: 'POST',
            data: {
                id: companyId,
                name,
                payroll_loan_id: id
            }
        });

        if ( check.available ) {
            const response = yield call( Fetch, `/payroll_loan_type/${payload.id}`, {
                method: 'PATCH',
                data: {
                    name,
                    company_id: companyId
                }
            });

            yield put({
                type: UPDATE_LOAN_TYPES,
                payload: response
            });
        } else {
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: `Loan type name '${name}' already exist.`,
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EDIT_LOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getLoanTypes );
}

/**
 * Watcher for GET loan types
 */
export function* watchForGetLoanTypes() {
    const watcher = yield takeEvery( GET_LOAN_TYPES, getLoanTypes );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE loan types
 */
export function* watchForDeleteLoanTypes() {
    const watcher = yield takeEvery( DELETE_LOAN_TYPES, deleteLoanTypes );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for EDIT loan type
 */
export function* watchForEditLoanType() {
    const watcher = yield takeEvery( EDIT_LOAN_TYPE, editLoanType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetLoanTypes,
    watchForDeleteLoanTypes,
    watchForEditLoanType,
    watchForNotifyUser,
    watchForReinitializePage
];
