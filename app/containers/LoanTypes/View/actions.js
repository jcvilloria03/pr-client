import {
    GET_LOAN_TYPES,
    DELETE_LOAN_TYPES,
    EDIT_LOAN_TYPE,
    NOTIFICATION
} from './constants';

/**
 * Fetch list of loan types
 */
export function getLoanTypes() {
    return {
        type: GET_LOAN_TYPES
    };
}

/**
 * Delete loan types
 */
export function deleteLoanTypes( IDs ) {
    return {
        type: DELETE_LOAN_TYPES,
        payload: IDs
    };
}

/**
 * Edit loan type
 */
export function editLoanType( data ) {
    return {
        type: EDIT_LOAN_TYPE,
        payload: data
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
