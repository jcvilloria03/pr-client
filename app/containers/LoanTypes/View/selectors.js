import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectLoanTypesDomain = () => ( state ) => state.get( 'loanTypes' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

const makeSelectLoading = () => createSelector(
    selectLoanTypesDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectEditLoading = () => createSelector(
    selectLoanTypesDomain(),
    ( substate ) => substate.get( 'editLoading' )
);

const makeSelectLoanTypes = () => createSelector(
    selectLoanTypesDomain(),
  ( substate ) => substate.get( 'loanTypes' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectLoanTypesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectProductsState = () => createSelector(
    selectAppDomain(),
    ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

export {
  makeSelectLoading,
  makeSelectEditLoading,
  makeSelectLoanTypes,
  makeSelectNotification,
  makeSelectProductsState
};
