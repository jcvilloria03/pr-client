import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_EDIT_LOADING,
    SET_LOAN_TYPES,
    NOTIFICATION_SAGA,
    UPDATE_LOAN_TYPES
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: false,
    editLoading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    loanTypes: [],
    editingRow: null
});

/**
 * Prepare list of loan types fetched from API to accomodate FE needs
 * @param {*array} data array of loan types
 */
function prepareLoanTypesData( data ) {
    return data.map( ( element ) => ({
        ...element,
        noselect: !element.is_editable,
        edit: false
    }) );
}

/**
 * Prepare list of loan types fetched from API after loan type edit
 * @param {*array} data array of loan types
 */
function updateLoanTypesData( loanTypesArray, loanType ) {
    return loanTypesArray.map( ( element ) => {
        if ( element.id === loanType.id ) {
            return Object.assign({}, element, { name: loanType.name });
        }
        return element;
    });
}
/**
 * Loan types view reducer
 */
function loanTypes( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_EDIT_LOADING:
            return state.set( 'editLoading', action.payload );
        case UPDATE_LOAN_TYPES:
            return state.set( 'loanTypes', fromJS( updateLoanTypesData( state.get( 'loanTypes' ).toJS(), action.payload ) ) );
        case SET_LOAN_TYPES:
            return state.set( 'loanTypes', fromJS( prepareLoanTypesData( action.payload ) ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default loanTypes;
