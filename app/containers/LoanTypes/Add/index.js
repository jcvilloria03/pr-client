import React from 'react';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import A from '../../../components/A';
import Input from '../../../components/Input';
import Button from '../../../components/Button';
import SnackBar from '../../../components/SnackBar';
import Loader from '../../../components/Loader';
import Sidebar from '../../../components/Sidebar';

import { company } from '../../../utils/CompanyService';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { getCompanySettingsSidebarLinks } from '../../../utils/sidebarHelper';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper
} from './styles';

import {
    makeSelectSubmitted,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectProductsState
} from './selectors';

import * as createLoanTypeActions from './actions';

/**
 * Add Loan Types Component
 */
export class Add extends React.Component {
    static propTypes = {
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitPayrollLoanType: React.PropTypes.func,
        errors: React.PropTypes.object,
        submitted: React.PropTypes.bool,
        products: React.PropTypes.array
    };
    static defaultProps = {
        errors: {}
    };

    /**
     * Component constructor method.
     */
    constructor( props ) {
        super( props );
        this.state = {
            names: null,
            counter: 0,
            errors: {}
        };
    }

    /**
     * Page Authorization check
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.payroll_loan_types'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });
    }

    /**
     * Display API errors in fields
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });

        nextProps.submitted !== this.props.submitted && this.submitButton.setState({ disabled: nextProps.submitted });
    }

    /**
     * Display errors from API
     */
    handleApiErrors() {
        if ( this.state.errors.message ) {
            this.names.setState({ error: true, errorMessage: this.state.errors.message });
        }
    }

    /**
     * Calculate counter for displaying number of entries
     * @param value
     */
    updateCounter( value ) {
        this.setState({
            counter: value.split( ',' ).length
        });
    }

    /**
     * Validate form
     * @returns {boolean}
     */
    validateForm() {
        return !!this.state.names;
    }

    /**
     * Handle click
     */
    handleClick = () => {
        const valid = this.validateForm();
        valid &&
        this.props.submitPayrollLoanType( Object.assign({}, { company_id: company.getLastActiveCompanyId(), names: this.state.names }) );
    }

    /**
     * Component Render Method
     */
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA: this.props.products && subscriptionService.isSubscribedToTA( this.props.products )
        });

        return (
            <PageWrapper>
                <Helmet
                    title="Add Loan Types"
                    meta={ [
                        { name: 'description', content: 'Create a new Loan Type' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar items={ sidebarLinks } />
                <NavWrapper>
                    <Container>
                        <A href="../loan-types">
                            &#8592; Back to Loan Types
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <HeadingWrapper>
                        <h3>Add Loan Types</h3>
                        <p>
                            Add Loan Type options through this page. Note that the loan types
                            added in this page will be referenced when assigning loans to
                            employees in the Payroll module.
                        </p>
                    </HeadingWrapper>
                    <FormWrapper>
                        <div className="row">
                            <div className="col-xs-12">
                                <p className="pull-right">{ this.state.counter } entries added</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-3">
                                <p>Use comma to separate values. <br />Example: Home Loan, Salary Loan, Personal Loan.</p>
                            </div>
                            <div className="col-xs-9 type-names">
                                <Input
                                    id="names"
                                    required
                                    ref={ ( ref ) => { this.names = ref; } }
                                    onChange={ ( value ) => {
                                        this.setState({ names: value, counter: value.split( ',' ).length });
                                    } }
                                />
                            </div>
                        </div>
                    </FormWrapper>
                </Container>
                <Container>
                    <div className="pull-right">
                        <Button
                            label="Cancel"
                            type="action"
                            size="large"
                            alt
                            onClick={ () => { this.setState({ names: ' ' }); } }
                        />
                        <Button
                            id="submitButton"
                            label={ this.props.submitted ? <Loader /> : 'Submit' }
                            type="action"
                            size="large"
                            ref={ ( ref ) => { this.submitButton = ref; } }
                            onClick={ this.handleClick }
                        />
                    </div>
                </Container>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    submitted: makeSelectSubmitted(),
    errors: makeSelectErrors(),
    notification: makeSelectNotification(),
    products: makeSelectProductsState()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createLoanTypeActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Add );
