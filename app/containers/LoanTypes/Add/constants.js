export const LOADING = 'app/LoanTypes/LOADING';
export const SET_ERRORS = 'app/LoanTypes/SET_ERRORS';
export const SUBMIT_FORM = 'app/LoanTypes/SUBMIT_FORM';
export const SUBMITTED = 'app/LoanTypes/SUBMITTED';
export const NOTIFICATION_SAGA = 'app/LoanTypes/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/LoanTypes/NOTIFICATION';
