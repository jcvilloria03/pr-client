import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectAddLoanTypeDomain = () => ( state ) => state.get( 'addLoanType' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

const makeSelectSubmitted = () => createSelector(
    selectAddLoanTypeDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
    selectAddLoanTypeDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectAddLoanTypeDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

const makeSelectProductsState = () => createSelector(
    selectAppDomain(),
    ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

export {
    makeSelectSubmitted,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectProductsState
};
