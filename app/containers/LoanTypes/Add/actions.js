import {
    SUBMIT_FORM,
    NOTIFICATION
} from './constants';

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Submit a new payroll loan type
 */
export function submitPayrollLoanType( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}
