import { fromJS } from 'immutable';
import {
    SET_ERRORS,
    SUBMITTED,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    errors: {},
    submitted: false,
    submitButton: null,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Loan type add Reducer
 *
 */
function addLoanType( state = initialState, action ) {
    switch ( action.type ) {
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addLoanType;
