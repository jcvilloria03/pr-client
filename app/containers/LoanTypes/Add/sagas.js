import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';
import { Fetch } from '../../../utils/request';
import { browserHistory } from '../../../utils/BrowserHistory';

import {
    SUBMIT_FORM,
    SUBMITTED,
    SET_ERRORS,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';
/**
 * resets the setup store to initial values
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];
        // Transform data to remove null keys
        const data = Object.assign({}, payload );
        const keys = Object.keys( payload );
        keys.forEach( ( key ) => {
            if ( payload[ key ] === null ) {
                delete data[ key ];
            }
        });

        yield call( Fetch, '/payroll_loan_type', { method: 'POST', data });
        yield call( delay, 500 );
        browserHistory.push( '/loan-types' );
        yield put({
            type: SUBMITTED,
            payload: false
        });
    } catch ( error ) {
        if ( error.response && error.response.status === 406 ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    yield takeLatest( SUBMIT_FORM, submitForm );
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForReinitializePage,
    watchNotify
];
