import styled from 'styled-components';

const PageWrapper = styled.div`
    .selected .fill {
        fill: #fff;
    }

    .selected .stroke {
        stroke: #fff;
    }

    .fill {
        fill: rgb(0,165,226);
    }

    .stroke {
        stroke: rgb(0,165,226);
    }
`;

const NavWrapper = styled.div`
    margin-top: 70px;
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;

    h3 {
        font-weight: 600;
    }

    p {
        text-align: center;
        max-width: 800px;
    }
`;

const FormWrapper = styled.div`
    padding: 1rem;

    > div {
        & > h4 {
            text-align: left;
        }

        & > button {
            padding: 12px 40px;
            margin-top: 2rem;
        }

        .steps {
            display: flex;
            align-items: stretch;
            justify-content: space-between;
            margin: 1rem 0;

            > .step {
                width: 49%;
            }
        }

        .template,
        .upload {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
            width: 100%;
            height: 100%;
            text-align: center;
            padding: 2rem;
            border: 2px dashed #ccc;
            border-radius: 12px;
        }

        .upload > div:first-of-type > div > div {
            margin: 0 auto;
        }

        .type-names {
            input {
                height: 150px
            }
        }
    }
`;

export {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper
};
