import styled, { keyframes } from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const CompanyWrapper = styled.div`
    background-color: #fff;
    display: flex;
    justify-content: center;
    flex-direction: column;
    color: #444;
    padding: 20px 60px;

    button {
        padding: 16px 40px;
        margin: 20px 0;
    }

    .radioOptions > div > div:last-of-type {
        margin-bottom: 0 !important;
    }
`;

export const PayrollGroupWrapper = styled.div`
    background-color: #fff;
    display: flex;
    justify-content: center;
    flex-direction: column;
    color: #444;
    padding: 20px 60px;

    button {
        padding: 16px 40px;
        margin: 20px 0;
    }

    .frequency {
        margin: 0 auto;
        margin-top: 40px;
        text-align: center;

        .frequencies {
            a {
                width: 120px;
                height: 120px;
                display: inline-flex;
                align-items: center;
                border-radius: 50%;
                padding: 10px;
                border: 1px solid #00a5e2;
                color: #00a5e2;
                margin: 20px;
                background: #fff;
                cursor: pointer;

                span {
                    text-align: center;
                    width: 100%;
                }

                &.selected {
                    color: #fff;
                    background: #00a5e2;
                }
            }
        }
    }

    .payroll-dates {
        margin: 0 auto;
        margin-top: 40px;
        text-align: center;
        display: flex;
        box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
        min-width: 600px;
        width: auto;
        margin-bottom: 30px;

        .formInput {
            text-align: left;
            flex-grow: 1;
            width: 100%;

            & > div > div {
                padding: 8px 20px;
            }

            .activeInput {
                background: rgba(0, 165, 229, 0.1);
            }
        }
    }

    .nonworking > div > div:last-of-type {
        margin-bottom: 0 !important;
    }

    .DayPicker-Day--cutoff1 {
        background: none !important;
        border: 2px solid #00A5E5 !important;
        color: #444 !important;
    }

    .DayPicker-Day--cutoff2 {
        background: none !important;
        border: 2px solid #f2c650 !important;
        color: #444 !important;
    }

    .DayPicker-Day--payday1 {
        background: #00A5E5 !important;
        border: 2px solid #00A5E5 !important;
        color: #FFF !important;
    }

    .DayPicker-Day--payday2 {
        background: #f2c650 !important;
        border: 2px solid #f2c650 !important;
        color: #FFF !important;
    }

    .working-days {
        margin: 50px 0;
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;

        .entry {
            padding: 0 60px;

            h5 {
                margin-bottom: 0px;
            }
            .fa {
                margin: 0;
                cursor: pointer;
            }
            p {
                display: none;
            }
            input {
                height: 120px;
                width: 200px;
                font-size: 90px;
                text-align: center;
            }

            &:first-of-type {
                input {
                    width: 300px;
                }
            }
        }
    }

    .contributions {
        text-align: center;
        & > h4 {
            text-align: center;
            margin: 80px 0 50px 0;
        }

        & > .wrapper {
            margin: auto;
            width: auto;
            display: inline-block;
            box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
            padding: 30px;

            td {
                padding: 4px 10px;
                text-align: center;
                min-width: 90px;
            }

            td[colspan='3'], td[colspan='2'] {
                & > div {
                    display: flex;
                    justify-content: space-around;
                }
            }
            tr > td:first-of-type {
                text-align: left;
            }

            tr:not(:first-of-type) > td:nth-of-type(1), tr > td:nth-of-type(4), tr:not(:nth-of-type(2)):not(:first-of-type) > td:nth-of-type(2) {
                border-right: 1px solid #ccc;
            }
        }
    }

    .payroll-group-name {
        text-align: center;
        display: inline-block;
        min-width: 400px;
        width: 0;
        margin: 40px auto 0;
    }

    .preview-section {
        text-align: center;
        min-width: 400px;
        margin: 40px auto;
        display: relative;
        max-width: 100%;
        overflow: auto;

        td {
            p {
                margin: 0;
            }
            h6 {
                margin-bottom: 8px;
            }
            h6:last-of-type {
                margin-bottom: 0;
            }
        }
    }

    .list-section {
        align-self: center;
        width: 100%;
        overflow: auto;

        button {
            padding: 8px 13px;
        }
    }

    .list-section + div {
        button:first-of-type {
            margin-right: 10px;
        }
    }
`;

export const UserWrapper = styled.div`
    background-color: #fff;
    display: flex;
    justify-content: center;
    flex-direction: column;
    color: #444;
    max-width: 600;
    padding: 20px 60px;
    .done {
        padding: 16px 50px;
        margin: 20px 0;
    }
    .user {
         width: 100%;
         max-width: 1200px;
         margin: 0 auto;
         min-height: 60px;
         display: inline-flex;
         border-bottom: 1px solid rgba(236, 238, 239, 0.6);
         h5 {
            margin: 0;
         }
         & > div {
            display: inline-flex;
            float: none;
            align-items: center;
            & > div {
                width: 100%;
            }
         }
         &.form {
            min-height: 100px;
            border-bottom: none;
            margin-top: 20px;
            align-items: flex-start;
            button {
                padding: 8px 24px;
            }
         }
    }
    .user.data > div {
        padding-left: 27px;
        padding-right: 27px;
    }
    .Select {
        margin-bottom: -25px;
    }
`;

export const LocationWrapper = styled.div`
    background-color: #fff;
    display: flex;
    justify-content: center;
    flex-direction: column;
    color: #444;
    padding: 20px 60px;
    label {
        font-size: 14px;
        font-weight: 400;
        margin-bottom: 4px;
    }
    button {
        padding: 16px 40px;
        margin: 20px 0;
    }

    .cityzip {
        display: flex;

        & > div {
            flex-grow: 1;
            &:first-of-type {
                margin-right: 6px;
            }
        }
    }

    .saveandnew button:first-of-type {
        margin-right: 10px;
    }
`;

export const EmployeeWrapper = styled.div`
    display: flex;
    justify-content: center;
    flex-direction: column;
    color: #444;
    padding: 20px 60px;

    button .animation {
        color: #FFF;

        .anim3:before, .anim3:after {
            border-color: #fff;
        }
    }
    .personal, .payroll {
        margin: 3rem 0;
        display: flex;
        flex-direction: column;
        align-items: center;

        & > button {
            padding: 12px 40px;
            margin-top: 2rem;
        }

        .step {
            display: inline-flex;
            padding-top: 2rem;

            & > div {
                margin: 20px;
                align-self: center;
            }

            & > div:first-of-type {
                width: 400px;

                p {
                    margin-left: 20px;
                }
            }

            &:first-of-type {
                border-bottom: 1px dashed #ccc;
                padding-top: 0;
            }
        }

        .template, .upload {
            width: 380px;
            text-align: center;
            padding: 2rem;
            border: 1px solid #ccc;

            a {
                background-color: #FFF;
                color: #00A5E5;
                border: 3px solid #00A5E5;
                padding: 8px 12px;
                border-radius: 30px;
                font-weight: 700;
                text-transform: uppercase;
                display: inline;
                letter-spacing: 1px;
                vertical-align: middle;
                margin: 2px;

                &:hover, &:focus, &:active:focus {
                    outline: none;
                    background-color: #fff;
                    color: #0086b7;
                    border-color: #0086b7;
                }
            }
        }

        .errors {
            .rt-thead .rt-tr {
                background: #F21108;
                color: #fff;
            }
            .rt-tbody .rt-tr {
                background: rgba(242, 17, 8, .1)
            }
            .ReactTable.-striped .rt-tr.-odd {
                background: rgba(242, 17, 8, .2)
            }

            .ReactTable .rt-th,
            .ReactTable .rt-td {
                flex: 1 0 0px;
                white-space: initial;
                text-overflow: ellipsis;
                padding: 7px 20px;
                overflow: hidden;
                transition: 0.3s ease;
                transition-property: width, min-width, padding, opacity;
            }

            .react-bs-container-body tr {
                background: rgba(249, 210, 210, 0.8);

                &:hover {
                    background: rgba(249, 210, 210, 1);
                }
            }
        }
    }

    .preview {
        .react-bs-table-container {
            overflow: auto;
        }

        .previewActions {
            button {
                min-width: 140px;
                padding: 16px 10px;

                &:first-of-type {
                    margin-right: 20px;
                }
            }
        }
    }
`;

export const DrccWrapper = styled.div`
    background-color: #fff;
    display: flex;
    justify-content: center;
    flex-direction: column;
    color: #444;
    padding: 20px 20px;

    .content {
        display: flex;
        float: none;
        justify-content: center;
        flex-direction: column;

        .header {
            padding: 40px 20px;
        }
        .columns, .data {
            display: inline-flex;
            flex-direction: column;

            .line {
                display: inline-flex;
                flex-direction: column;
                border-bottom: 1px solid rgba(236, 238, 239, 0.6);
                max-width: 1200px;

                > div {
                    display: inline-flex;
                }
            }
        }
        .data .line > div > div {
            padding: 20px 27px;
        }
        .inputs {
            padding-top: 20px;
            display: inline-flex;
            flex-direction: row;

            > div {
                display: inline-flex;
            }
            div.button {
                align-self: flex-start;
                padding: 0;

                button {
                    padding: 8px 24px;
                }
            }
        }
        .col-xs-1, .col-xs-5, .col-xs-7, .col-xs-11 {
            display: inline-flex;
            float: none;
        }
    }

    .footer {
        text-align: center;
        padding: 10px 0;
    }
`;

const anim3 = keyframes`
    to {
        transform: rotate(45deg) translate(3px, 3px);
    }
`;

export const StyledLoader = styled.div`
    font-size: 16px;
    font-weight: 600;
    color: #00a5e5;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;

    .anim3 {
        padding-left: 10px;
        &:before {
            display: block;
            width: 12px;
            height: 12px;
            margin-top: -1px;
            border: 2px solid #00a5e5;
            content: "";
            animation: ${anim3} .5s ease infinite alternate;
            border-radius: 100% 100% 0 100%;
            transform: rotate( 45deg );
        }

        &:after {
            display: block;
            width: 12px;
            height: 3px;
            margin-top: 8px;
            background-color: #00a5e5;
            content: "";
            border-radius: 100%;
        }
    }
`;
