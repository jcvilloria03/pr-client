/**
 * Annual Earnings View constants
 */

const namespace = 'app/AnnualEarnings/View';
export const GET_ANNUAL_EARNINGS = `${namespace}/GET_ANNUAL_EARNINGS`;
export const SET_ANNUAL_EARNINGS = `${namespace}/SET_ANNUAL_EARNINGS`;
export const SET_FILTER_DATA = `${namespace}/SET_FILTER_DATA`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const EXPORT_ANNUAL_EARNINGS = `${namespace}/EXPORT`;
export const DELETE_ANNUAL_EARNINGS = `${namespace}/DELETE_ANNUAL_EARNINGS`;
export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;

export const PAGE_STATUSES = {
    LOADING: 'LOADING',
    DELETING: 'DELETING',
    DOWNLOADING: 'DOWNLOADING',
    READY: 'READY'
};
