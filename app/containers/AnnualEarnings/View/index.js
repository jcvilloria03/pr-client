import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';
import {
    formatPaginationLabel,
    formatDeleteLabel
} from 'utils/functions';
import { filterHelper } from 'utils/filterHelper';
import { EMPLOYEE_SUBHEADER_ITEMS } from 'utils/constants';

import SubHeader from 'containers/SubHeader';
import SalDropdown from 'components/SalDropdown';
import Search from 'components/Search';
import Loader from 'components/Loader';
import SnackBar from 'components/SnackBar';
import Table from 'components/Table';
import {
    isAnyRowSelected,
    getIdsOfSelectedRows
} from 'components/Table/helpers';
import { H2, H3, H5 } from 'components/Typography';
import Button from 'components/Button';
import Icon from 'components/Icon';
import SalConfirm from '../../../components/SalConfirm';

import {
    makeSelectPageStatus,
    makeSelectAnnualEarnings,
    makeSelectFilterData,
    makeSelectNotification
} from './selectors';
import * as annualEarningsActions from './actions';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';
import { PAGE_STATUSES } from './constants';

import Filter from './templates/filter';

/**
 * View AnnualEarnings Component
 */
export class View extends React.Component {
    static propTypes = {
        getAnnualEarnings: React.PropTypes.func,
        deleteAnnualEarnings: React.PropTypes.func,
        annualEarnings: React.PropTypes.array,
        filterData: React.PropTypes.shape({
            annualEarningsTypes: React.PropTypes.array,
            positions: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        pageStatus: React.PropTypes.string,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    };

    static defaultProps = {
        pageStatus: PAGE_STATUSES.LOADING,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            displayedData: this.props.annualEarnings,
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            showModal: false,
            showFilter: false,
            hasFiltersApplied: false
        };

        this.searchInput = null;
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.annual_earnings',
            'create.annual_earnings',
            'delete.annual_earnings',
            'edit.annual_earnings'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.annual_earnings' ];

            if ( authorized ) {
                this.setState({ permission: {
                    view: authorization[ 'view.annual_earnings' ],
                    create: authorization[ 'create.annual_earnings' ],
                    delete: authorization[ 'delete.annual_earnings' ],
                    edit: authorization[ 'edit.annual_earnings' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.getAnnualEarnings();
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.annualEarnings !== this.props.annualEarnings ) {
            this.setState({ displayedData: nextProps.annualEarnings }, () => {
                this.handleSearch();
            });
        }
    }

    handleTableChanges = ( tableProps = this.annualEarningsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    handleSearch = ( term = '' ) => {
        const dataForDisplay = this.props.annualEarnings;
        const matchingRows = this.annualEarningsTable
            ? this.annualEarningsTable.getFilteredData( dataForDisplay, term )
            : dataForDisplay;

        this.setState({ displayedData: matchingRows }, () => {
            this.annualEarningsTable && this.handleTableChanges();
        });
    }

    deleteAnnualEarnings = () => {
        const selected = getIdsOfSelectedRows( this.annualEarningsTable );
        this.props.pageStatus === PAGE_STATUSES.READY && this.props.deleteAnnualEarnings( selected );
    };

    toggleFilter = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    applyFilters = ( filters ) => {
        const filteredData = filterHelper.filter( this.props.annualEarnings.map( ( annualEarning ) => (
            { ...annualEarning, type_id: annualEarning.year.toString() }
        ) ), filters, 'year' );

        this.setState({
            displayedData: filteredData,
            hasFiltersApplied: !!filters.length
        }, () => {
            this.handleTableChanges();
        });
    }

    goToDetailsPage = ( _, rowInfo, column ) => ({
        onClick: () => {
            if ( column.id !== 'select' && column.id !== 'actions' ) {
                browserHistory.push( `/annual-earnings/${rowInfo.row.id}/detail` );
            } else if ( column.id === 'actions' ) {
                browserHistory.push( `/annual-earnings/${rowInfo.row.id}/edit` );
            }
        },
        style: {
            cursor: column.id !== 'select' ? 'pointer' : 'default'
        }
    })

    resetFilters = () => {
        this.setState({
            showFilter: false,
            displayedData: this.props.annualEarnings,
            hasFiltersApplied: false
        });
    }

    /**
     * Component Render Method
     */
    render() {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'company_employee_id',
                header: 'Employee ID',
                accessor: ( row ) => row.company_employee_id
            },
            {
                id: 'full_name',
                header: 'Employee Name',
                accessor: ( row ) => row.full_name,
                minWidth: 200
            },
            {
                id: 'year',
                header: 'Year',
                accessor: ( row ) => row.year,
                minWidth: 100
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.state.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        to={ `/annual-earnings/${row.id}/edit` }
                        disabled={ this.props.pageStatus === PAGE_STATUSES.DELETING }
                    />
                )
            }
        ];

        return (
            <div>
                <Helmet
                    title="View Annual Earnings"
                    meta={ [
                        { name: 'description', content: 'View Annual Earnings' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.deleteAnnualEarnings }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                            Are you sure you want to delete these Annual Earnings?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Annual Earnings"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <Container>
                        { this.props.pageStatus === PAGE_STATUSES.LOADING ? (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading Annual Earnings.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        ) : (
                            <div className="content">
                                <div className="heading">
                                    <H3>Annual Earnings</H3>
                                    <p>You may now add employees annual earnings through this page. You have the option to add earnings via batch or one by one.</p>
                                    <div style={ { textAlign: 'center' } } >
                                        <div className="main">
                                            <Button
                                                className={ this.state.permission.create ? '' : 'hide' }
                                                label="Batch Add Annual Earnings"
                                                size="large"
                                                type="neutral"
                                                to="/annual-earnings/batch-upload"
                                            />
                                            <Button
                                                className={ this.state.permission.create ? '' : 'hide' }
                                                label="Add Annual Earnings"
                                                size="large"
                                                type="action"
                                                to="/annual-earnings/manual-entry"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="title">
                                    <H5>Annual Earnings List</H5>
                                    <div className="search-wrapper">
                                        <Search
                                            ref={ ( ref ) => { this.searchInput = ref; } }
                                            handleSearch={ this.handleSearch }
                                        />
                                    </div>
                                    <span>
                                        { isAnyRowSelected( this.annualEarningsTable ) ? this.state.deleteLabel : this.state.label }
                                    </span>
                                    <span className="sl-u-gap-left--sm">
                                        <Button
                                            label={ <span><Icon className="filter-icon" name="filter" /> Filter</span> }
                                            type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                            onClick={ this.toggleFilter }
                                        />
                                        { isAnyRowSelected( this.annualEarningsTable ) && (
                                            <SalDropdown
                                                dropdownItems={ [
                                                    {
                                                        label: 'Delete',
                                                        children: this.props.pageStatus === PAGE_STATUSES.DELETING ? <Loader /> : <div>Delete</div>,
                                                        onClick: () => {
                                                            this.setState({ showModal: false }, () => {
                                                                this.setState({ showModal: true });
                                                            });
                                                        }
                                                    }
                                                ] }
                                            />
                                        )
                                        }
                                    </span>
                                </div>
                                <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                                    <Filter
                                        filterData={ this.props.filterData }
                                        onCancel={ () => { this.resetFilters(); } }
                                        onApply={ ( values ) => { this.applyFilters( values ); } }
                                    />
                                </div>
                                <Table
                                    data={ this.state.displayedData }
                                    columns={ tableColumns }
                                    pagination
                                    onDataChange={ this.handleTableChanges }
                                    ref={ ( ref ) => { this.annualEarningsTable = ref; } }
                                    selectable={ this.state.permission.create }
                                    onSelectionChange={ ({ selected }) => {
                                        const selectionLength = selected.filter( ( row ) => row ).length;

                                        this.setState({
                                            deleteLabel: formatDeleteLabel( selectionLength )
                                        });
                                    } }
                                />
                            </div>
                        ) }
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    pageStatus: makeSelectPageStatus(),
    annualEarnings: makeSelectAnnualEarnings(),
    filterData: makeSelectFilterData(),
    notification: makeSelectNotification()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        annualEarningsActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
