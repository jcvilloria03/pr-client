import { fromJS } from 'immutable';
import flattenDeep from 'lodash/flattenDeep';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_ANNUAL_EARNINGS,
    SET_FILTER_DATA,
    NOTIFICATION_SAGA,
    SET_PAGE_STATUS
} from './constants';

const initialState = fromJS({
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    annualEarnings: [],
    filterData: {},
    page_status: ''
});

/**
 * Processes annual earnings data to be read by table
 * @param {Object} data - Annual earnings data grouped by year
 * @returns {Array}
 */
function processAnnualEarningsData( data ) {
    return flattenDeep( Object.values( data ) );
}

/**
 * Annual Earnings view reducer
 * @param {object} state
 * @param {object} action
 */
function annualEarningsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_PAGE_STATUS:
            return state.set( 'page_status', action.payload );
        case SET_ANNUAL_EARNINGS:
            return state.set( 'annualEarnings', fromJS( processAnnualEarningsData( action.payload ) ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default annualEarningsReducer;
