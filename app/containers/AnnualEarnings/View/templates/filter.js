import React from 'react';
import MultiSelect from 'components/MultiSelect';
import Button from 'components/Button';

import { FilterWrapper } from './styles';

import { FILTER_TYPES } from './constants';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.PureComponent {
    static propTypes = {
        filterData: React.PropTypes.shape({
            years: React.PropTypes.array
        }),
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    onApply = () => {
        const filters = [];
        this.years.state.value && this.years.state.value.forEach( ( year ) => {
            filters.push( Object.assign( year, { type: FILTER_TYPES.YEAR }) );
        });
        this.props.onApply( filters );
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    getyears = () => {
        if ( !this.props.filterData.years ) {
            return [];
        }

        const years = this.props.filterData.years.map( ( year ) => (
            this.formatDataForMultiselect( Object.assign( year, { name: year.toString() }, { id: year.toString() }) )
        ) );

        return years;
    }

    resetFilters = () => {
        this.years.setState({ value: null }, () => {
            this.onApply();
        });
    }

    formatDataForMultiselect = ( data ) => (
        {
            value: data.id,
            label: data.name,
            disabled: false
        }
    )

    render() {
        return (
            <FilterWrapper>
                <div className="row">
                    <div className="col-xs-3">
                        <MultiSelect
                            id="years"
                            label={
                                <span>Annual Earnings Years</span>
                            }
                            ref={ ( ref ) => { this.years = ref; } }
                            data={ this.getyears() }
                            placeholder="All Years"
                        />
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
