import {
    GET_ANNUAL_EARNINGS,
    NOTIFICATION,
    EXPORT_ANNUAL_EARNINGS,
    DELETE_ANNUAL_EARNINGS,
    SET_PAGE_STATUS
} from './constants';

/**
 * Fetch list of Annual Earnings
 */
export function getAnnualEarnings() {
    return {
        type: GET_ANNUAL_EARNINGS
    };
}

/**
 * Export Annual Earnings
 * @param {array} IDs
 */
export function exportAnnualEarnings( IDs ) {
    return {
        type: EXPORT_ANNUAL_EARNINGS,
        payload: IDs
    };
}

/**
 * Sends request to delete Annual Earnings
 * @param {Array} ids - Annual Earnings IDs
 * @returns {Object} action
 */
export function deleteAnnualEarnings( ids ) {
    return {
        type: DELETE_ANNUAL_EARNINGS,
        payload: ids
    };
}

/**
 * Sets page status for Annual Earnings page
 * @param {String} payload - Page status ('loading', 'downloading', 'deleting')
 * @returns {Object} action
 */
export function setPageStatus( payload ) {
    return {
        type: SET_PAGE_STATUS,
        payload
    };
}

/**
 * Display a notification to a user
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
