import { createSelector } from 'reselect';

/**
 * Direct selector to the Annual Earnings state domain
 */
const selectViewAnnualEarningsDomain = () => ( state ) => state.get( 'annualEarnings' );

const makeSelectAnnualEarnings = () => createSelector(
    selectViewAnnualEarningsDomain(),
  ( substate ) => substate.get( 'annualEarnings' ).toJS()
);

const makeSelectFilterData = () => createSelector(
    selectViewAnnualEarningsDomain(),
  ( substate ) => substate.get( 'filterData' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewAnnualEarningsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectPageStatus = () => createSelector(
    selectViewAnnualEarningsDomain(),
    ( substate ) => substate.get( 'page_status' )
);

export {
  makeSelectAnnualEarnings,
  makeSelectFilterData,
  makeSelectNotification,
  makeSelectPageStatus
};
