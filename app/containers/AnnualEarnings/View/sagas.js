import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import fileDownload from 'js-file-download';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import {
    GET_ANNUAL_EARNINGS,
    SET_ANNUAL_EARNINGS,
    SET_FILTER_DATA,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    EXPORT_ANNUAL_EARNINGS,
    DELETE_ANNUAL_EARNINGS,
    PAGE_STATUSES
} from './constants';
import { setPageStatus } from './actions';

/**
 * Get Annual Earnings for table
 */
export function* getAnnualEarnings() {
    try {
        yield put( setPageStatus( PAGE_STATUSES.LOADING ) );

        const companyId = company.getLastActiveCompanyId();
        const filterData = {};

        const annualEarnings = yield call( Fetch, `/company/${companyId}/annual_earning/group_by_year`, { method: 'GET' });

        filterData.years = Object.keys( annualEarnings );

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });
        yield put({
            type: SET_ANNUAL_EARNINGS,
            payload: annualEarnings
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Export Annual Earnings
 */
export function* exportAnnualEarnings({ payload }) {
    try {
        yield put( setPageStatus( PAGE_STATUSES.DOWNLOADING ) );
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/annual_earning/download`, {
            method: 'POST',
            data: { ids: payload }
        });

        fileDownload( response, 'annual-earnings.csv' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Sends request to delete Annual Earnings
 * @param {Array} payload - Annual Earnings IDs
 */
export function* deleteAnnualEarnings({ payload }) {
    try {
        yield put( setPageStatus( PAGE_STATUSES.DELETING ) );
        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, `/company/${companyId}/annual_earning/bulk_delete`, {
            method: 'POST',
            data: { ids: payload }
        });

        yield [
            call( notifyUser, {
                show: true,
                title: 'Success',
                message: 'Record successfully deleted',
                type: 'success'
            }),
            call( getAnnualEarnings )
        ];
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getAnnualEarnings );
}

/**
 * Watcher for GET Annual Earnings
 */
export function* watchForGetAnnualEarnings() {
    const watcher = yield takeEvery( GET_ANNUAL_EARNINGS, getAnnualEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for EXPORT Annual Earnings
 */
export function* watchForExportAnnualEarnings() {
    const watcher = yield takeEvery( EXPORT_ANNUAL_EARNINGS, exportAnnualEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE_ANNUAL_EARNINGS Annual Earnings
 */
export function* watchForDeleteAnnualEarnings() {
    const watcher = yield takeEvery( DELETE_ANNUAL_EARNINGS, deleteAnnualEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetAnnualEarnings,
    watchForExportAnnualEarnings,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForDeleteAnnualEarnings
];
