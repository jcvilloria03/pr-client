import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectEditAnnualEarningsDomain = () => ( state ) => state.get( 'editAnnualEarnings' );

const makeSelectLoading = () => createSelector(
    selectEditAnnualEarningsDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectSubmitted = () => createSelector(
    selectEditAnnualEarningsDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectFormOptions = () => createSelector(
    selectEditAnnualEarningsDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectAnnualEarningsData = () => createSelector(
    selectEditAnnualEarningsDomain(),
    ( substate ) => substate.get( 'annualEarningsData' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectEditAnnualEarningsDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectFormOptions,
    makeSelectAnnualEarningsData,
    makeSelectNotification
};
