import { take, call, put, cancel } from 'redux-saga/effects';
import { takeLatest, takeEvery, delay } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import get from 'lodash/get';

import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    INITIALIZE,
    SET_LOADING,
    SET_SUBMITTED,
    SET_API_ERRORS,
    SET_FORM_OPTIONS,
    SET_ANNUAL_EARNINGS_DATA,
    SUBMIT_FORM,
    NOTIFICATION
} from './constants';
import { setNotification } from './actions';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formOptions = {};

        const annualEarningsData = yield call( Fetch, `/company/${companyId}/annual_earning/${payload.id}`, { method: 'GET' });
        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&filter[employee_ids][]=${annualEarningsData.employee_id}`, { method: 'GET' });

        formOptions.employees = employees.data.filter( ( e ) => e.payroll );

        yield put({
            type: SET_ANNUAL_EARNINGS_DATA,
            payload: annualEarningsData
        });

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Edit Annual Earnings
 */
export function* submitForm({ payload }) {
    try {
        yield put({
            type: SET_SUBMITTED,
            payload: true
        });

        yield put({
            type: SET_API_ERRORS,
            payload: {
                editAnnualEarnings: false
            }
        });

        const { previousRouteName, ...data } = payload;

        const companyId = company.getLastActiveCompanyId();
        yield call( Fetch, `/company/${companyId}/annual_earning/set`, { method: 'POST', data });

        yield call( resetStore );

        yield call( notifyUser, {
            title: 'Success',
            message: 'Employee record successfully saved',
            show: true,
            type: 'success'
        });

        previousRouteName ? yield call( browserHistory.goBack ) : yield call( browserHistory.push, '/annual-earnings' );
    } catch ( error ) {
        yield call( notifyError, error );

        yield put({
            type: SET_API_ERRORS,
            payload: {
                editAnnualEarnings: true
            }
        });
    } finally {
        yield put({
            type: SET_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    yield call( resetStore );
    yield call( initializeData );
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error.response, 'statusText', 'Error' ),
        message: get( error.response, 'data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Watcher for INITIALIZE
 */
export function* watcherForInitializeData() {
    const watcher = yield takeLatest( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for edit Annual Earnings
 */
export function* watchForSubmitForm() {
    const watcher = yield takeLatest( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for reinitialize page
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export default [
    watcherForInitializeData,
    watchForSubmitForm,
    watchForNotifyUser,
    watchForReinitializePage
];
