import styled from 'styled-components';

const MainWrapper = styled.div`
    background: #fff;
    margin-bottom: 80px;

    .nav {
        padding: 10px 20px;
        background: #f0f4f6;
        margin-bottom: 50px;
    }

    .summary-panel {
        font-size: 14px;
        display: flex;
        padding: 25px;
        background-color: rgba(229, 246, 252, 1);
        border-radius: 5px;
        margin-bottom: 70px;

        &__column {
            flex-grow: 1;
            flex-basis: 0;
            padding: 10px;

            &:not(:last-child) {
                border-right: solid 2px rgb(216, 216, 216);
                margin-right: 40px
            }

            p {
                margin: 0;
            }
        }
    }


    .loader {
        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .date {
        & > div > span {
            display: block;
        }
        .DayPickerInput {
            display: block;

            input {
                width: 100%;
            }
        }

        .item-number {
            margin-right: 0;
        }
    }

    .heading {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 0 auto 30px auto;

        h3 {
            font-weight: 600;
        }

        p {
            text-align: center;
            max-width: 800px;
        }
    }

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        margin-top: 20px;

        button {
            min-width: 120px;
        }
    }

    .hide {
        display: none;
    }

    h4 {
        font-weight: bold;
    }

    h5 {
        font-size: 0.9rem;
    }

    .label {
        color: #5b5b5b;
        font-size: 14px;
        margin-bottom: 19px;
        font-weight: 400;
    }
`;

const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;

const FormWrapper = styled.div`
    > div {
        & > h4 {
            text-align: left;
        }

        & > button {
            padding: 12px 40px;
            margin-top: 2rem;
        }

        .steps {
            display: flex;
            align-items: stretch;
            justify-content: space-between;
            margin: 1rem 0;

            > .step {
                width: 49%;
            }
        }

        .template,
        .upload {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
            width: 100%;
            height: 100%;
            padding: 2rem;
            text-align: center;
            border: 2px dashed #ccc;
            border-radius: 12px;

            .sl-c-btn--wide {
                min-width: 12rem;
            }
        }

        .upload > div:first-of-type > div > div {
            margin: 0 auto;
        }

        .errors {
            .rt-thead .rt-tr {
                background: #F21108;
                color: #fff;
            }
            .rt-tbody .rt-tr {
                background: rgba(242, 17, 8, .1)
            }
            .ReactTable.-striped .rt-tr.-odd {
                background: rgba(242, 17, 8, .2)
            }

            .ReactTable .rt-th,
            .ReactTable .rt-td {
                flex: 1 0 0px;
                white-space: initial;
                text-overflow: ellipsis;
                padding: 7px 20px;
                overflow: hidden;
                transition: 0.3s ease;
                transition-property: width, min-width, padding, opacity;
            }

            .react-bs-container-body tr {
                background: rgba(249, 210, 210, 0.8);

                &:hover {
                    background: rgba(249, 210, 210, 1);
                }
            }
        }
    }

    .prev-employer {
        margin-bottom: 100px;
    }

    .prev-employer-toggle {
        display: flex;

        h4 {
            margin-right: 30px;
        }
    }

    .item-number {
        color: #a1a1a1;
    }

    .employee-data {
        margin-bottom: 40px;

        .value {
            padding-left: 10px;
            font-weight: 600;
        }
    }
`;

const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;

    h3 {
        font-weight: 600;
    }

    p {
        text-align: center;
        max-width: 800px;
    }
`;

export {
    MainWrapper,
    ConfirmBodyWrapperStyle,
    FormWrapper,
    Footer,
    NavWrapper,
    HeadingWrapper
};
