import { fromJS } from 'immutable';

import {
    SET_LOADING,
    SET_SUBMITTED,
    SET_API_ERRORS,
    SET_FORM_OPTIONS,
    SET_ANNUAL_EARNINGS_DATA,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    submitted: false,
    formOptions: {
        employees: [],
        years: []
    },
    annualEarningsData: {},
    apiErrors: {
        editAnnualEarnings: false
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 * Annual Earnings edit reducer
 */
const annualEarningsEditReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_API_ERRORS:
            return state.set( 'apiErrors', fromJS( action.payload ) );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_ANNUAL_EARNINGS_DATA:
            return state.set( 'annualEarningsData', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
};

export default annualEarningsEditReducer;
