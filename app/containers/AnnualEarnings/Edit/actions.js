import {
    INITIALIZE,
    SUBMIT_FORM,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';

/**
 * Initialize data
 */
export function initializeData( payload ) {
    return {
        type: INITIALIZE,
        payload
    };
}

/**
 * Update Annual Earnings preview
 */
export function submitForm( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: NOTIFICATION_SAGA,
        payload
    };
}
