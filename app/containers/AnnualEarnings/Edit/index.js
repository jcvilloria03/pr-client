import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import get from 'lodash/get';

import A from 'components/A';
import { H3, H4, H5 } from 'components/Typography';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import Button from 'components/Button';
import Switch from 'components/Switch';
import SubHeader from 'containers/SubHeader';
import {
    formatCurrency,
    stripNonDigit,
    formatTIN,
    stripNonNumeric,
    formatDate
} from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from 'utils/constants';
import { isAuthorized } from 'utils/Authorization';
import {
    CURRENCY_FIELDS,
    FORM_FIELDS,
    PREVIOUS_EMPLOYER_INFO_FIELDS
} from 'utils/AnnualEarnings/constants';
import { renderField } from 'utils/form';

import {
    MainWrapper,
    FormWrapper,
    Footer,
    NavWrapper,
    HeadingWrapper
} from './styles';

import {
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectNotification,
    makeSelectLoading,
    makeSelectAnnualEarningsData
} from './selectors';

import * as createAnnualEarningsActions from './actions';

const inputTypes = {
    input: [ 'amount', 'reason' ],
    select: ['type_id'],
    datePicker: ['date']
};

/**
 * AnnualEarnings Edit Container
 */
export class Edit extends React.PureComponent {
    static propTypes = {
        initializeData: React.PropTypes.func,
        annualEarningsData: React.PropTypes.object,
        formOptions: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitForm: React.PropTypes.func,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        params: React.PropTypes.object,
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    }

    static defaultProps = {
        loading: true,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            annual_earnings: {},
            errors: {},
            prepopulatedEmployeeId: null,
            show_previous_employer_form: false
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['edit.annual_earnings'], ( authorized ) => {
            if ( authorized ) {
                this.props.initializeData( this.props.params );

                const prepopulatedEmployeeId = JSON.parse( localStorage.getItem( 'employeeIdForEditHistoricalEarnings' ) );
                prepopulatedEmployeeId && this.setState({ prepopulatedEmployeeId });
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentWillReceiveProps( nextProps ) {
        if ( Object.keys( nextProps.annualEarningsData ).length !== Object.keys( this.props.annualEarningsData ).length ) {
            this.setInitialStateFromProps( nextProps );
        }
    }

    componentWillUnmount() {
        localStorage.removeItem( 'employeeIdForEditHistoricalEarnings' );
    }

    setInitialStateFromProps = ( props ) => {
        const { formOptions, annualEarningsData } = props;

        if ( Object.keys( formOptions ).length === 0 ) {
            return;
        }

        const hasPrevEmployeeInfo = PREVIOUS_EMPLOYER_INFO_FIELDS.some( ( field ) => annualEarningsData[ field ]);
        const hasPrevEmployeeIncomes = CURRENCY_FIELDS.PREVIOUS_EMPLOYER.some( ( field ) => Number( annualEarningsData[ field ]) );

        this.setState({
            annual_earnings: annualEarningsData,
            show_previous_employer_form: hasPrevEmployeeInfo || hasPrevEmployeeIncomes
        });
    }

    getEmployeeFullName = () => {
        const employee = this.props.formOptions.employees.find( ( e ) => {
            const employeeId = this.state.prepopulatedEmployeeId
                ? parseInt( this.state.prepopulatedEmployeeId, 10 )
                : this.props.annualEarningsData.employee_id;
            return e.id === employeeId;
        });

        return employee ? `${employee.first_name} ${employee.last_name} ${employee.employee_id}` : '';
    }

    /**
     * Formats data from currency fields
     * @param {String} type - One of 'current' or 'previous'
     * @returns {Object}
     */
    getCurrencyFieldsForPayload( type ) {
        const fields = type === 'current' ? CURRENCY_FIELDS.CURRENT_EMPLOYER : CURRENCY_FIELDS.PREVIOUS_EMPLOYER;

        return fields.reduce( ( data, field ) => ({
            ...data,
            [ field ]: parseFloat( stripNonDigit( get( this[ field ], 'state.value', '' ) ) ) || 0
        }), {});
    }

    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.input.includes( key ) || inputTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, label: this.state.errors[ key ][ 0 ] });
                } else if ( inputTypes.datePicker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    submitForm = () => {
        const blankPrevEmployerData = {
            prev_date_from: '',
            prev_date_to: '',
            prev_employer_business_name: '',
            prev_employer_tin: '',
            prev_employer_business_address: '',
            prev_employer_zip_code: '',
            ...CURRENCY_FIELDS.PREVIOUS_EMPLOYER.reduce( ( acc, field ) => ({ ...acc, [ field ]: '' }), {})
        };

        const prevEmployerData = this.state.show_previous_employer_form
            ? {
                prev_date_from: formatDate( this.prev_date_from.state.selectedDay, DATE_FORMATS.API ) || '',
                prev_date_to: formatDate( this.prev_date_to.state.selectedDay, DATE_FORMATS.API ) || '',
                prev_employer_business_name: this.prev_employer_business_name.state.value,
                prev_employer_tin: this.prev_employer_tin.state.value,
                prev_employer_business_address: this.prev_employer_business_address.state.value,
                prev_employer_zip_code: this.prev_employer_zip_code.state.value,
                ...this.getCurrencyFieldsForPayload( 'previous' )
            }
            : blankPrevEmployerData;

        const payload = {
            employee_id: this.props.annualEarningsData.employee_id,
            year: this.props.annualEarningsData.year,
            ...this.getCurrencyFieldsForPayload( 'current' ),
            ...prevEmployerData
        };

        this.props.submitForm({
            ...payload,
            previousRouteName: this.props.previousRoute.name
        });
    }

    /**
     * Clears previous employer form fields
     */
    clearPreviousEmployerForm() {
        const previousEmployerFields = [
            ...PREVIOUS_EMPLOYER_INFO_FIELDS,
            ...CURRENCY_FIELDS.PREVIOUS_EMPLOYER
        ].reduce( ( fields, field ) => ({
            ...fields,
            [ field ]: ''
        }), {});

        this.setState( ( state ) => ({
            annual_earnings: {
                ...state.annual_earnings,
                ...previousEmployerFields
            }
        }) );
    }

    /**
     * Renders field according to config provided
     *
     * @param {Object} field - Details of field to be rendered
     * @returns {Object}
     */
    renderField( field ) {
        const {
            class_name: className,
            ...rest
        } = field;

        let onChange;
        let onFocus;
        let onBlur;
        let value = this.state.annual_earnings[ field.id ];
        let dateConfig = {};
        switch ( field.id ) {
            case 'prev_employer_tin':
                onChange = ( _value ) => this[ field.id ].setState({ value: formatTIN( _value ) });
                break;
            case 'prev_employer_zip_code':
                onChange = ( _value ) => this[ field.id ].setState({ value: stripNonNumeric( _value ) });
                break;
            case 'prev_employer_business_name':
            case 'prev_employer_business_address':
                onChange = ( _value ) => this[ field.id ].setState({ value: _value });
                break;
            case 'prev_date_from':
                onChange = ( selectedDate ) => this.setState( ( state ) => ({
                    annual_earnings: {
                        ...state.annual_earnings,
                        prev_date_from: formatDate( selectedDate, DATE_FORMATS.API ) || '',
                        prev_date_to: ''
                    }
                }) );
                dateConfig = {
                    selectedDay: value
                };
                break;
            case 'prev_date_to':
                onChange = ( selectedDate ) => this.setState( ( state ) => ({
                    annual_earnings: {
                        ...state.annual_earnings,
                        prev_date_to: formatDate( selectedDate, DATE_FORMATS.API ) || ''
                    }
                }) );
                dateConfig = {
                    disabledDays: [{ before: new Date( this.state.annual_earnings.prev_date_from ) }],
                    disabled: !this.state.annual_earnings.prev_date_from,
                    selectedDay: value
                };
                break;
            default:
                onChange = ( _value ) => this[ field.id ].setState({ value: stripNonDigit( _value ) });
                onFocus = () => this[ field.id ].setState({ value: stripNonDigit( this[ field.id ].state.value ) });
                onBlur = () => this[ field.id ].setState({ value: formatCurrency( this[ field.id ].state.value ) });
                value = formatCurrency( value );
        }

        const config = {
            ...rest,
            ...dateConfig,
            onChange,
            onFocus,
            onBlur,
            value,
            ref: ( ref ) => { this[ field.id ] = ref; }
        };

        return (
            <div className={ className } key={ field.id }>
                { field.field_type && renderField( config ) }
            </div>
        );
    }

    renderCurrentEmployerForm = () => (
        <div>
            <H4>Current Employer</H4>
            { FORM_FIELDS.CURRENT_EMPLOYER_SECTIONS.map( ({ title, rows }) => (
                <div key={ title }>
                    <H5>{ title }</H5>
                    { rows.map( ( row, i ) => (
                        <div className="row" key={ `${title}_${i}` }>
                            { row.map( ( field ) => this.renderField( field ) ) }
                        </div>
                    ) ) }
                    <br />
                </div>
            ) ) }
        </div>
    );

    renderPreviousEmployerForm = () => (
        <div>
            { FORM_FIELDS.PREVIOUS_EMPLOYER_SECTIONS.map( ({ title, rows }) => (
                <div key={ title }>
                    <H5>{ title }</H5>
                    { rows.map( ( row, i ) => (
                        <div className="row" key={ `${title}_${i}` }>
                            { row.map( ( field ) => this.renderField( field ) ) }
                        </div>
                    ) ) }
                    <br />
                </div>
            ) ) }
        </div>
    )

    render() {
        return (
            <div>
                <MainWrapper>
                    <Helmet
                        title="Edit Annual Earnings"
                        meta={ [
                            { name: 'description', content: 'Edit Annual Earnings' }
                        ] }
                    />
                    <SnackBar
                        message={ this.props.notification.message }
                        title={ this.props.notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ this.props.notification.show }
                        delay={ 5000 }
                        type={ this.props.notification.type }
                    />
                    <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                    <NavWrapper>
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/annual-earnings' );
                                } }
                            >
                                &#8592; Back to { this.props.previousRoute.name ? this.props.previousRoute.name : 'Annual Earnings' }
                            </A>
                        </Container>
                    </NavWrapper>

                    { !this.props.loading && (
                        <HeadingWrapper>
                            <div className="heading">
                                <H3>Edit Annual Earnings</H3>
                            </div>
                        </HeadingWrapper>
                    ) }

                    { this.props.loading && (
                        <div className="loader">
                            <Loader />
                        </div>
                    ) }

                    { !this.props.loading && (
                        <div>
                            <FormWrapper>
                                <Container>
                                    <div className="row employee-data">
                                        <div className="col-xs-4">
                                            <div className="label">Employee Name or Employee ID</div>
                                            <div className="value">{ this.getEmployeeFullName() }</div>
                                        </div>
                                        <div className="col-xs-3">
                                            <div className="label">Year</div>
                                            <div className="value">{ this.props.annualEarningsData.year }</div>
                                        </div>
                                    </div>

                                    { this.renderCurrentEmployerForm() }
                                    <div className="prev-employer">
                                        <div className="prev-employer-toggle" ref={ ( ref ) => { this.previous_employer = ref; } }>
                                            <H4>Previous Employer</H4>
                                            <Switch
                                                checked={ this.state.show_previous_employer_form }
                                                onChange={ ( value ) =>
                                                    this.setState({ show_previous_employer_form: value }, () => {
                                                        if ( value ) {
                                                            const offset = this.previous_employer.offsetTop;
                                                            window.scrollTo({ top: offset - 150, behavior: 'smooth' });
                                                        } else {
                                                            this.clearPreviousEmployerForm();
                                                        }
                                                    })
                                                }
                                            />
                                        </div>
                                        { this.state.show_previous_employer_form && this.renderPreviousEmployerForm() }
                                    </div>
                                </Container>
                            </FormWrapper>
                        </div>
                    ) }
                </MainWrapper>

                <Footer>
                    <Button
                        label="Cancel"
                        type="neutral"
                        size="large"
                        onClick={ () => {
                            this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/annual-earnings' );
                        } }
                    />
                    <Button
                        label={ this.props.submitted ? <Loader /> : 'Submit' }
                        disabled={ this.props.submitted }
                        type="action"
                        size="large"
                        onClick={ this.submitForm }
                        ref={ ( ref ) => { this.submitButton = ref; } }
                    />
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    formOptions: makeSelectFormOptions(),
    annualEarningsData: makeSelectAnnualEarningsData(),
    submitted: makeSelectSubmitted(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createAnnualEarningsActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Edit );
