import {
    UPLOAD_ANNUAL_EARNINGS,
    SAVE_ANNUAL_EARNINGS,
    NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Display notification in page
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Upload Annual Earnings
 * @param {object} payload
 */
export function uploadAnnualEarnings( payload ) {
    return {
        type: UPLOAD_ANNUAL_EARNINGS,
        payload
    };
}

/**
 * Save Annual Earnings
 * @param {object} payload
 */
export function saveAnnualEarnings( payload ) {
    return {
        type: SAVE_ANNUAL_EARNINGS,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
