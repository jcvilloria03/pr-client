import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectBatchAddAnnualEarningsDomain = () => ( state ) => state.get( 'batchAddAnnualEarnings' );

const makeSelectAnnualEarnings = () => createSelector(
    selectBatchAddAnnualEarningsDomain(),
    ( substate ) => substate.get( 'annualEarnings' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectBatchAddAnnualEarningsDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectBatchAddAnnualEarningsDomain(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectBatchAddAnnualEarningsDomain(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectBatchAddAnnualEarningsDomain(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectAnnualEarningsPreview = () => createSelector(
    selectBatchAddAnnualEarningsDomain(),
    ( substate ) => substate.get( 'annualEarningsPreview' ).toJS()
);

const makeSelectSaving = () => createSelector(
    selectBatchAddAnnualEarningsDomain(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectBatchAddAnnualEarningsDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectBatchUploadErrors,
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectAnnualEarnings,
    makeSelectAnnualEarningsPreview,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectSaving
};
