import styled, { keyframes } from 'styled-components';

const anim3 = keyframes`
    to {
        transform: rotate(45deg) translate(3px, 3px);
    }
`;

const StyledLoader = styled.div`
    font-size: 16px;
    font-weight: 600;
    color: #00a5e5;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;

    span {
        color: white;
    }

    .anim3 {
        padding-left: 10px;
        &:before {
            display: block;
            width: 12px;
            height: 12px;
            margin-top: -1px;
            border: 2px solid #00a5e5;
            content: "";
            animation: ${anim3} .5s ease infinite alternate;
            border-radius: 100% 100% 0 100%;
            transform: rotate( 45deg );
        }

        &:after {
            display: block;
            width: 12px;
            height: 3px;
            margin-top: 8px;
            background-color: rgba(0, 0, 0, 0.2);
            content: "";
            border-radius: 100%;
        }
    }
`;

const PageWrapper = styled.div`
    .template, .upload {
        text-align: center;
        padding: 2rem;
        border: 2px dashed #ccc;
        border-radius: 12px;
        width: 100%;
        height: 100%;

        & > a {
            background-color: #FFF;
            color: #333;
            border: 1px solid #4ABA4A;
            padding: 8px 12px;
            border-radius: 30px;
            display: inline;
            vertical-align: middle;
            margin: 0 auto;
            max-width: 200px;

            &:hover, &:focus, &:active:focus {
                outline: none;
                background-color: #fff;
                color: #4ABA4A;
            }
        }
    }

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;

        button {
            min-width: 120px;
        }
    }
`;

const FormWrapper = styled.div`
    > div {
        & > h4 {
            text-align: left;
        }

        & > button {
            padding: 12px 40px;
            margin-top: 2rem;
        }

        .steps {
            display: flex;
            align-items: stretch;
            justify-content: space-between;
            margin: 1rem 0;

            > .step {
                width: 49%;
            }
        }

        .template,
        .upload {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
            width: 100%;
            height: 100%;
            padding: 2rem;
            text-align: center;
            border: 2px dashed #ccc;
            border-radius: 12px;

            .sl-c-btn--wide {
                min-width: 12rem;
            }
        }

        .upload > div:first-of-type > div > div {
            margin: 0 auto;
        }

        .errors {
            .rt-thead .rt-tr {
                background: #F21108;
                color: #fff;
            }
            .rt-tbody .rt-tr {
                background: rgba(242, 17, 8, .1)
            }
            .ReactTable.-striped .rt-tr.-odd {
                background: rgba(242, 17, 8, .2)
            }

            .ReactTable .rt-th,
            .ReactTable .rt-td {
                flex: 1 0 0px;
                white-space: initial;
                text-overflow: ellipsis;
                padding: 7px 20px;
                overflow: hidden;
                transition: 0.3s ease;
                transition-property: width, min-width, padding, opacity;
            }

            .react-bs-container-body tr {
                background: rgba(249, 210, 210, 0.8);

                &:hover {
                    background: rgba(249, 210, 210, 1);
                }
            }
        }
    }
`;

const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;

    h3 {
        font-weight: 600;
    }

    p {
        text-align: center;
        max-width: 800px;
    }
`;

const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

export { StyledLoader, PageWrapper, FormWrapper, NavWrapper, HeadingWrapper, Footer };
