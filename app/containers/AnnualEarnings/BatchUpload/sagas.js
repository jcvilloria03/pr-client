import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';

import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { formatFeedbackMessage } from 'utils/functions';

import {
    GET_ANNUAL_EARNINGS_PREVIEW,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SAVE_ANNUAL_EARNINGS,
    SET_BATCH_UPLOAD_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_ANNUAL_EARNINGS_PREVIEW_DATA,
    SET_ANNUAL_EARNINGS_PREVIEW_STATUS,
    SET_ERRORS,
    SET_SAVING_STATUS,
    UPLOAD_ANNUAL_EARNINGS
} from './constants';

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * converts api error to notification payload
 */
function toErrorPayload( error ) {
    return {
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });

    const notification = Object.assign({}, payload, { show: true });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: notification
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });
}

/**
 * Uploads the CSV of Annual Earnings to add and starts the validation process
 */
export function* uploadAnnualEarnings({ payload }) {
    try {
        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: {}
        });

        const data = new FormData();
        const companyId = company.getLastActiveCompanyId();

        data.append( 'file', payload.file );
        data.append( 'company_id', companyId );

        const upload = yield call( Fetch, '/annual_earning/upload', {
            method: 'POST',
            data
        });

        if ( upload.id ) {
            yield put({
                type: SET_BATCH_UPLOAD_JOB_ID,
                payload: upload.id
            });

            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: 'validation_queued'
            });

            yield call( checkValidation, { payload: { jobId: upload.id }});
        }
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();
        const check = yield call( Fetch, '/annual_earning/upload/status', {
            method: 'GET',
            params: {
                job_id: payload.jobId,
                step: payload.step,
                company_id: companyId
            }
        });

        switch ( check.status ) {
            case 'validation_failed':
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });
                yield put({
                    type: SET_ANNUAL_EARNINGS_PREVIEW_DATA,
                    payload: []
                });
                yield put({
                    type: SET_BATCH_UPLOAD_ERRORS,
                    payload: check.errors
                });

                break;
            case 'saved':
                yield put({
                    type: SET_SAVING_STATUS,
                    payload: check.status
                });
                yield call( notifyUser, formatFeedbackMessage() );
                yield call( browserHistory.push, '/annual-earnings' );

                break;
            default:
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });
                yield call( delay, 2000 );
                yield call( checkValidation, { payload });
        }
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Fetches preview for uploaded Annual Earnings
 */
export function* getAnnualEarningsPreview({ payload }) {
    try {
        yield put({
            type: SET_ANNUAL_EARNINGS_PREVIEW_DATA,
            payload: []
        });

        yield put({
            type: SET_ANNUAL_EARNINGS_PREVIEW_STATUS,
            payload: 'busy'
        });

        const preview = yield call(
            Fetch,
            `/annual_earning/upload/preview?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}`,
            { method: 'GET' }
        );

        yield put({
            type: SET_ANNUAL_EARNINGS_PREVIEW_DATA,
            payload: preview
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    } finally {
        yield put({
            type: SET_ANNUAL_EARNINGS_PREVIEW_STATUS,
            payload: 'ready'
        });
    }
}

/**
 * Saves validated Annual Earnings
 */
export function* saveAnnualEarnings({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const upload = yield call( Fetch, '/annual_earning/upload/save', {
            method: 'POST',
            data: {
                company_id: companyId,
                job_id: payload.jobId
            }
        });

        if ( upload.id ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: 'save_queued'
            });

            yield call( checkValidation, { payload: {
                step: 'save',
                jobId: payload.jobId
            }});
        }
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Watch for UPLOAD_ANNUAL_EARNINGS
 */
export function* watchForUploadAnnualEarnings() {
    const watcher = yield takeEvery( UPLOAD_ANNUAL_EARNINGS, uploadAnnualEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_ANNUAL_EARNINGS_PREVIEW
 */
export function* watchForGetAnnualEarningsPreview() {
    const watcher = yield takeEvery( GET_ANNUAL_EARNINGS_PREVIEW, getAnnualEarningsPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SAVE_ANNUAL_EARNINGS
 */
export function* watchForSaveAnnualEarnings() {
    const watcher = yield takeEvery( SAVE_ANNUAL_EARNINGS, saveAnnualEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetAnnualEarningsPreview,
    watchForUploadAnnualEarnings,
    watchForSaveAnnualEarnings,
    watchNotify
];
