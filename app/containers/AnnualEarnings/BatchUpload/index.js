import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../components/A';
import Table from '../../../components/Table';
import Button from '../../../components/Button';
import Clipboard from '../../../components/Clipboard';
import FileInput from '../../../components/FileInput';
import { H4, P } from '../../../components/Typography';
import SnackBar from '../../../components/SnackBar';
import SubHeader from '../../../containers/SubHeader';

import { browserHistory } from '../../../utils/BrowserHistory';
import { formatPaginationLabel, formatCurrency } from '../../../utils/functions';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import { BASE_PATH_NAME } from '../../../constants';

import * as actions from './actions';

import {
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectAnnualEarningsPreview,
    makeSelectSaving,
    makeSelectNotification
} from './selectors';

import {
    StyledLoader,
    PageWrapper,
    FormWrapper,
    NavWrapper,
    HeadingWrapper
} from './styles';

/**
 * Annual Earnings Batch Upload Component
 */
class BatchUpload extends React.Component {
    static propTypes = {
        uploadAnnualEarnings: React.PropTypes.func,
        batchUploadStatus: React.PropTypes.string,
        batchUploadErrors: React.PropTypes.oneOfType([
            React.PropTypes.object,
            React.PropTypes.array
        ]),
        annualEarningsPreview: React.PropTypes.shape({
            status: React.PropTypes.string,
            data: React.PropTypes.array
        }),
        saving: React.PropTypes.shape({
            status: React.PropTypes.string,
            errors: React.PropTypes.object
        }),
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            files: [],
            status: null,
            errors: {},
            clipboard: '',
            submit: false,
            annualEarningsTableLabel: 'Showing 0-0 of 0 entries',
            errorsTableLabel: 'Showing 0-0 of 0 entries',
            annualEarningsPreview: {
                data: [],
                status: 'ready'
            },
            savingStatus: '',
            savingErrors: {}
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.batchUploadStatus !== this.props.batchUploadStatus && this.setState({
            status: nextProps.batchUploadStatus
        }, () => {
            this.validateButton && this.validateButton.setState({
                disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.batchUploadStatus )
            });
        });

        nextProps.batchUploadErrors !== this.props.batchUploadErrors && this.setState({
            errors: nextProps.batchUploadErrors
        }, () => {
            let clipboard = '';
            const rows = Object.keys( nextProps.batchUploadErrors );

            if ( rows.length > 0 ) {
                rows.forEach( ( row ) => {
                    Object.values( nextProps.batchUploadErrors[ row ]).forEach( ( value ) => {
                        clipboard += `Row ${row}, ${value}\n`;
                    });
                });
            }

            this.handleErrorsTableChanges();
            this.setState({ clipboard });
        });

        nextProps.annualEarningsPreview.data !== this.props.annualEarningsPreview.data && this.setState({
            annualEarningsPreview: { ...this.state.annualEarningsPreview, data: nextProps.annualEarningsPreview.data }
        }, () => {
            this.handleAnnualEarningsTableChanges();
        });

        nextProps.annualEarningsPreview.status !== this.props.annualEarningsPreview.status && this.setState({
            annualEarningsPreview: { ...this.state.annualEarningsPreview, status: nextProps.annualEarningsPreview.status }
        });

        nextProps.saving.status !== this.props.saving.status && this.setState({
            savingStatus: nextProps.saving.status
        }, () => {
            this.submitButton.setState({ disabled: nextProps.saving.status !== 'save_failed' });
        });
    }

    handleErrorsTableChanges = () => {
        if ( !this.errorsTable ) {
            return;
        }

        this.setState({
            errorsTableLabel: formatPaginationLabel( this.errorsTable.tableComponent.state )
        });
    };

    handleAnnualEarningsTableChanges = () => {
        if ( !this.annualEarningsTable ) {
            return;
        }

        this.setState({
            annualEarningsTableLabel: formatPaginationLabel( this.annualEarningsTable.tableComponent.state )
        });
    };

    renderAnnualEarningsSection = () => {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'employee_id',
                header: 'Employee ID',
                accessor: ( row ) => row.employee_uid,
                minWidth: 200
            },
            {
                id: 'year',
                header: 'Year',
                accessor: ( row ) => row.year,
                minWidth: 200
            },
            {
                id: 'current_minimum_wage_earner',
                header: 'Current Employer Minimum Wage',
                accessor: ( row ) => row.current_minimum_wage_earner,
                minWidth: 200
            },
            {
                id: 'current_non_taxable_13_month_pay_other_benefits',
                header: 'Current Employer Non Taxable 13th Month Pay and Other Benefits',
                accessor: ( row ) => formatCurrency( row.current_non_taxable_13_month_pay_other_benefits ),
                minWidth: 200
            },
            {
                id: 'current_non_taxable_de_minimis',
                header: 'Current Employer Non Taxable De Minimis Benefits',
                accessor: ( row ) => formatCurrency( row.current_non_taxable_de_minimis ),
                minWidth: 200
            },
            {
                id: 'current_non_taxable_salaries_other_forms_compensation',
                header: 'Current Employer Non Taxable Salaries and Other Forms of Compensation',
                accessor: ( row ) => formatCurrency( row.current_non_taxable_salaries_other_forms_compensation ),
                minWidth: 200
            },
            {
                id: 'current_taxable_basic_salary',
                header: 'Current Employer Taxable Basic Salary',
                accessor: ( row ) => formatCurrency( row.current_taxable_basic_salary ),
                minWidth: 200
            },
            {
                id: 'current_taxable_13_month_pay_other_benefits',
                header: 'Current Employer Taxable 13th Month Pay and Other Benefits',
                accessor: ( row ) => formatCurrency( row.current_taxable_13_month_pay_other_benefits ),
                minWidth: 200
            },
            {
                id: 'current_taxable_salaries_other_forms_compensation',
                header: 'Current Employer Taxable Salaries and Other Forms of Compensation',
                accessor: ( row ) => formatCurrency( row.current_taxable_salaries_other_forms_compensation ),
                minWidth: 200
            },
            {
                id: 'current_total_employee_contributions',
                header: 'Current Employer Total Employee Contributions',
                accessor: ( row ) => formatCurrency( row.current_total_employee_contributions ),
                minWidth: 200
            },
            {
                id: 'current_employer_withholding_tax',
                header: 'Current Employer Witholding Tax',
                accessor: ( row ) => formatCurrency( row.current_employer_withholding_tax ),
                minWidth: 200
            },
            {
                id: 'prev_employer_name',
                header: 'Previous Employer Name',
                accessor: ( row ) => row.prev_employer_name,
                minWidth: 200
            },
            {
                id: 'prev_employer_address',
                header: 'Previous Employer Address',
                accessor: ( row ) => row.prev_employer_address,
                minWidth: 200
            },
            {
                id: 'prev_employer_tin',
                header: 'Previous Employer TIN',
                accessor: ( row ) => row.prev_employer_tin,
                minWidth: 200
            },
            {
                id: 'prev_employer_zip_code',
                header: 'Previous Employer Zip Code',
                accessor: ( row ) => row.prev_employer_zip_code,
                minWidth: 200
            },

            {
                id: 'prev_minimum_wage_earner',
                header: 'Previous Employer Minimum Wage',
                accessor: ( row ) => row.prev_minimum_wage_earner,
                minWidth: 200
            },
            {
                id: 'prev_non_taxable_13_month_pay_other_benefits',
                header: 'Previous Employer Non Taxable 13th Month Pay and Other Benefits',
                accessor: ( row ) => formatCurrency( row.prev_non_taxable_13_month_pay_other_benefits ),
                minWidth: 200
            },
            {
                id: 'prev_non_taxable_de_minimis',
                header: 'Previous Employer Non Taxable De Minimis Benefits',
                accessor: ( row ) => formatCurrency( row.prev_non_taxable_de_minimis ),
                minWidth: 200
            },
            {
                id: 'prev_non_taxable_salaries_other_forms_compensation',
                header: 'Previous Employer Non Taxable Salaries and Other Forms of Compensation',
                accessor: ( row ) => formatCurrency( row.prev_non_taxable_salaries_other_forms_compensation ),
                minWidth: 200
            },
            {
                id: 'prev_taxable_basic_salary',
                header: 'Previous Employer Taxable Basic Salary',
                accessor: ( row ) => formatCurrency( row.prev_taxable_basic_salary ),
                minWidth: 200
            },
            {
                id: 'prev_taxable_13_month_pay_other_benefits',
                header: 'Previous Employer Taxable 13th Month Pay and Other Benefits',
                accessor: ( row ) => formatCurrency( row.prev_taxable_13_month_pay_other_benefits ),
                minWidth: 200
            },
            {
                id: 'prev_taxable_salaries_other_forms_compensation',
                header: 'Previous Employer Taxable Salaries and Other Forms of Compensation',
                accessor: ( row ) => formatCurrency( row.prev_taxable_salaries_other_forms_compensation ),
                minWidth: 200
            },
            {
                id: 'prev_total_employee_contributions',
                header: 'Previous Employer Total Employee Contributions',
                accessor: ( row ) => formatCurrency( row.prev_total_employee_contributions ),
                minWidth: 200
            },
            {
                id: 'prev_employer_withholding_tax',
                header: 'Previous Employer Witholding Tax',
                accessor: ( row ) => formatCurrency( row.prev_employer_withholding_tax ),
                minWidth: 200
            }
        ];

        const dataForDisplay = this.state.annualEarningsPreview.data;

        return this.state.annualEarningsPreview.data.length === 0
            ? null
            : (
                <div>
                    <H4>Annual Earnings List:</H4>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.annualEarningsTableLabel }
                        </span>
                    </div>
                    <Table
                        columns={ tableColumns }
                        data={ dataForDisplay }
                        pagination
                        ref={ ( ref ) => { this.annualEarningsTable = ref; } }
                        onDataChange={ this.handleAnnualEarningsTableChanges }
                    />
                </div>
            );
    };

    renderErrorsSection = () => {
        const errorDisplay = [];
        const errorList = this.state.errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'Row Number',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'Error Type',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            const keys = Object.keys( errorList );

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row {key}</H4>,
                    error: <ul>{Object.values( errorList[ key ]).map( ( value ) => <li key={ value } style={ { margin: '0' } } >{value}</li> )}</ul>
                });
            });

            return (
                <div className="errors">
                    <H4>Data field errors</H4>
                    <p>
                        There seems to be an error in the file you uploaded. Review the list below, correct the errors and upload the file again.
                    </p>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.errorsTableLabel }
                        </span>
                        <Clipboard value={ this.state.clipboard } />
                    </div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                        ref={ ( ref ) => { this.errorsTable = ref; } }
                        onDataChange={ this.handleErrorsTableChanges }
                    />
                </div>
            );
        }

        return null;
    }

    render() {
        return (
            <div>
                <PageWrapper>
                    <Helmet
                        title="Batch Add Annual Earnings"
                        meta={ [
                            { name: 'description', content: 'Batch Add Annual Earnings' }
                        ] }
                    />
                    <SnackBar
                        message={ this.props.notification.message }
                        title={ this.props.notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ this.props.notification.show }
                        delay={ 5000 }
                        type={ this.props.notification.type }
                    />
                    <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                    <NavWrapper>
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddAnnualEarnings' ) );
                                    prepopulatedEmployee ? browserHistory.push( `/employee/${prepopulatedEmployee.id}`, true ) : browserHistory.push( '/annual-earnings' );
                                } }
                            >
                                &#8592; Back to { localStorage.getItem( 'prepopulatedEmployeeForAddAnnualEarnings' ) ? 'Employee' : 'Annual Earnings' }
                            </A>
                        </Container>
                    </NavWrapper>
                    <HeadingWrapper>
                        <h3>Batch Add Annual Earnings</h3>
                        <p>Enter the details of the annual earnings you would like to add. Remember to double check all the details before clicking Submit.</p>
                    </HeadingWrapper>
                    <FormWrapper>
                        <Container className="sl-u-gap-bottom--xlg">
                            <p className="sl-u-gap-top--lg sl-u-gap-bottom--lg">
                                Follow the steps below for adding annual earnings via batch upload. A template will be available for you to fill out.
                            </p>

                            <div className="steps">
                                <div className="step">
                                    <div className="template">
                                        <H4>Step 1:</H4>
                                        <P>Download and fill-out the Employee Payroll Information Template.</P>
                                        <P><A target="_blank" href={ `${BASE_PATH_NAME}/guides/annual-earnings/batch-upload` }>Click here to view the upload guide.</A></P>
                                        <A className="sl-c-btn--wide" href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/guides/annual-earnings.csv" download>Download Template</A>
                                    </div>
                                </div>
                                <div className="step">
                                    <div className="upload">
                                        <H4>Step 2:</H4>
                                        <P>After completely filling out the template, choose and upload it here.</P>
                                        <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'none' : 'block' } } >
                                            <FileInput
                                                accept=".csv"
                                                onDrop={ ( files ) => {
                                                    const { acceptedFiles } = files;

                                                    this.setState({
                                                        files: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null,
                                                        errors: {},
                                                        status: null
                                                    }, () => {
                                                        this.validateButton && this.validateButton.setState({ disabled: acceptedFiles.length <= 0 });
                                                    });
                                                } }
                                                ref={ ( ref ) => { this.fileInput = ref; } }
                                            />
                                        </div>
                                        <div style={ { display: this.state.status === 'validated' ? 'block' : 'none' } }>
                                            <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                                        </div>
                                        <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'validated' ? 'none' : 'block' } } >
                                            <Button
                                                label={
                                                    this.state.submit ? (
                                                        <StyledLoader className="animation">
                                                            Uploading <div className="anim3"></div>
                                                        </StyledLoader>
                                                    ) : (
                                                        'Upload'
                                                    )
                                                }
                                                disabled={ this.state.files ? this.state.files.length <= 0 : true }
                                                type="neutral"
                                                ref={ ( ref ) => { this.validateButton = ref; } }
                                                onClick={ () => {
                                                    this.setState({
                                                        errors: {},
                                                        status: null,
                                                        submit: true
                                                    }, () => {
                                                        this.validateButton && this.validateButton.setState({ disabled: true }, () => {
                                                            this.props.uploadAnnualEarnings({ file: this.state.files });

                                                            setTimeout( () => {
                                                                this.validateButton.setState({ disabled: false });
                                                                this.setState({ submit: false });
                                                            }, 5000 );
                                                        });
                                                    });
                                                } }
                                            />
                                        </div>
                                        <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'block' : 'none' } } >
                                            <StyledLoader className="animation">
                                                <H4 style={ { margin: '0' } }>{ this.state.status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                            </StyledLoader>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            { this.renderAnnualEarningsSection() }
                            { this.renderErrorsSection() }
                        </Container>
                    </FormWrapper>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    batchUploadJobId: makeSelectBatchUploadJobId(),
    batchUploadStatus: makeSelectBatchUploadStatus(),
    batchUploadErrors: makeSelectBatchUploadErrors(),
    annualEarningsPreview: makeSelectAnnualEarningsPreview(),
    saving: makeSelectSaving(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( BatchUpload );
