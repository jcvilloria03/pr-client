export const INITIALIZE = 'app/AnnualEarnings/Add/INITIALIZE';
export const GET_COMPANY_EMPLOYEES = 'app/Adjustments/Add/GET_COMPANY_EMPLOYEES';
export const LOADING = 'app/AnnualEarnings/Add/LOADING';
export const NOTIFICATION = 'app/AnnualEarnings/Add/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/AnnualEarnings/Add/NOTIFICATION_SAGA';
export const SET_ERRORS = 'app/AnnualEarnings/Add/SET_ERRORS';
export const SET_FORM_OPTIONS = 'app/AnnualEarnings/Add/SET_FORM_OPTIONS';
export const SUBMITTED = 'app/AnnualEarnings/Add/SUBMITTED';
export const SUBMIT_FORM = 'app/AnnualEarnings/Add/SUBMIT_FORM';

