import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';
import get from 'lodash/get';

import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import {
    getCurrentYear,
    getRange
} from 'utils/functions';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    INITIALIZE,
    GET_COMPANY_EMPLOYEES,
    LOADING,
    NOTIFICATION,
    SET_ERRORS,
    SET_FORM_OPTIONS,
    SUBMITTED,
    SUBMIT_FORM
} from './constants';
import { setNotification } from './actions';

/**
 * Initialize data
 */
export function* initializeData() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const formOptions = {};

        const currentYear = getCurrentYear();
        formOptions.years = getRange( currentYear - 5, currentYear )
            .reverse()
            .map( ( year ) => ({ label: year.toString(), value: year }) );

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Get Company Employees
 */
export function* getCompanyEmployees({ payload }) {
    try {
        const keyword = payload.keyword ? payload.keyword : '';
        const companyId = company.getLastActiveCompanyId();
        const formOptions = payload.formOptions;
        let filterEmployeeIdsQueryString = '';

        if ( payload.filterEmployeeIds ) {
            const filterEmployeeIds = payload.filterEmployeeIds;
            if ( filterEmployeeIds.length > 0 ) {
                for ( const employeeId of filterEmployeeIds ) {
                    filterEmployeeIdsQueryString += `&filter[employee_ids][]=${employeeId}`;
                }
            }
        }

        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL&keyword=${keyword}${filterEmployeeIdsQueryString}`, { method: 'GET' });

        formOptions.employees = employees.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        const companyId = company.getLastActiveCompanyId();

        const { previousRouteName, ...data } = payload;
        yield call( Fetch, `/company/${companyId}/annual_earning/set`, {
            method: 'POST',
            data
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Employee record successfully saved',
            show: true,
            type: 'success'
        });

        previousRouteName ? yield call( browserHistory.goBack ) : yield call( browserHistory.push, '/annual-earnings' );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyError, error );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error.response, 'statusText', 'Error' ),
        message: get( error.response, 'data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_COMPANY_EMPLOYEES
 */
export function* watchForGetCompanyEmployees() {
    const watcher = yield takeEvery( GET_COMPANY_EMPLOYEES, getCompanyEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForInitializeData,
    watchForGetCompanyEmployees,
    watchForReinitializePage,
    watchNotify
];
