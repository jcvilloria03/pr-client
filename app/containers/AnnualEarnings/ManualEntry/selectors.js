import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectAddAnnualEarningsDomain = () => ( state ) => state.get( 'addAnnualEarnings' );

const makeSelectLoading = () => createSelector(
    selectAddAnnualEarningsDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectFormOptions = () => createSelector(
    selectAddAnnualEarningsDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectAddAnnualEarningsDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
    selectAddAnnualEarningsDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectAddAnnualEarningsDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectErrors,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSubmitted
};
