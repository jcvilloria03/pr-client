import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import get from 'lodash/get';

import A from 'components/A';
import { H3, H4, H5 } from 'components/Typography';
import Typeahead from 'components/Typeahead';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import Button from 'components/Button';
import Select from 'components/Select';
import Switch from 'components/Switch';
import SubHeader from 'containers/SubHeader';

import {
    formatCurrency,
    stripNonDigit,
    formatTIN,
    stripNonNumeric,
    formatDate
} from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';
import { isAuthorized } from 'utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from 'utils/constants';

import {
    CURRENCY_FIELDS,
    FORM_FIELDS
} from 'utils/AnnualEarnings/constants';
import { renderField } from 'utils/form';

import {
    MainWrapper,
    FormWrapper,
    Footer,
    NavWrapper,
    HeadingWrapper
} from './styles';

import {
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectLoading
} from './selectors';

import * as createAnnualEarningsActions from './actions';

const inputTypes = {
    input: [ 'amount', 'reason' ],
    select: ['type_id'],
    datePicker: ['date']
};

/**
 * ManualEntry Container
 */
export class ManualEntry extends React.PureComponent {
    static propTypes = {
        initializeData: React.PropTypes.func,
        getCompanyEmployees: React.PropTypes.func,
        formOptions: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitForm: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        errors: React.PropTypes.object,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    }

    static defaultProps = {
        loading: true,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            errors: {},
            prepopulatedEmployee: null,
            show_previous_employer_form: false,
            prev_date_from: '',
            typeaheadTimeout: null
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.annual_earnings'], ( authorized ) => {
            if ( authorized ) {
                this.props.initializeData();

                const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddAnnualEarnings' ) );
                this.setState({ prepopulatedEmployee });
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });

        nextProps.submitted !== this.props.submitted && this.submitButton.setState({ disabled: nextProps.submitted });
    }

    componentWillUnmount() {
        localStorage.removeItem( 'prepopulatedEmployeeForAddAnnualEarnings' );
        this.props.resetStore();
    }

    // eslint-disable-next-line consistent-return
    getEmployeeFullName = () => {
        if ( this.props.formOptions.employees ) {
            // const employee = this.props.formOptions.employees.find( ( e ) => e.id === parseInt(
            //     this.state.prepopulatedEmployee ? this.state.prepopulatedEmployee.id : this.state.formOptions.employee_id, 10
            // ) );

            // return employee ? `${employee.first_name} ${employee.last_name} ${employee.employee_id}` : '';
            const employee = this.props.formOptions.employees.find( ( e ) => e.id === parseInt( get( this.state.prepopulatedEmployee, 'id' ), 10 ) );
            return employee ? `${employee.first_name} ${employee.last_name} ${employee.employee_id}` : '';
        }
    }

    /**
     * Formats data from currency fields
     * @param {String} type - One of 'current' or 'previous'
     * @returns {Object}
     */
    getCurrencyFieldsForPayload( type ) {
        const fields = type === 'current' ? CURRENCY_FIELDS.CURRENT_EMPLOYER : CURRENCY_FIELDS.PREVIOUS_EMPLOYER;

        return fields.reduce( ( data, field ) => ({
            ...data,
            [ field ]: parseFloat( stripNonDigit( get( this[ field ], 'state.value', '' ) ) ) || 0
        }), {});
    }

    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.input.includes( key ) || inputTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, label: this.state.errors[ key ][ 0 ] });
                } else if ( inputTypes.datePicker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    /**
     * Validates form inputs
     *
     * @returns {Boolean}
     */
    validateForm() {
        let valid = true;

        if ( this.employee._validate( this.employee.state.value ) ) {
            valid = false;
        }

        if ( !this.year._checkRequire( get( this.year.state.value, 'value' ) || this.year.state.value ) ) {
            valid = false;
        }

        return valid;
    }

    /**
     * Sends request to submit form data
     */
    submitForm = () => {
        if ( this.validateForm() ) {
            const blankPrevEmployerData = {
                prev_date_from: '',
                prev_date_to: '',
                prev_employer_business_name: '',
                prev_employer_tin: '',
                prev_employer_business_address: '',
                prev_employer_zip_code: '',
                ...CURRENCY_FIELDS.PREVIOUS_EMPLOYER.reduce( ( acc, field ) => ({ ...acc, [ field ]: '' }), {})
            };

            const prevEmployerData = this.state.show_previous_employer_form
                ? {
                    prev_date_from: formatDate( this.prev_date_from.state.selectedDay, DATE_FORMATS.API ) || '',
                    prev_date_to: formatDate( this.prev_date_to.state.selectedDay, DATE_FORMATS.API ) || '',
                    prev_employer_business_name: this.prev_employer_business_name.state.value,
                    prev_employer_tin: this.prev_employer_tin.state.value,
                    prev_employer_business_address: this.prev_employer_business_address.state.value,
                    prev_employer_zip_code: this.prev_employer_zip_code.state.value,
                    ...this.getCurrencyFieldsForPayload( 'previous' )
                }
                : blankPrevEmployerData;

            const payload = {
                employee_id: get( this.employee, 'state.value[0]id', '' ),
                year: parseInt( get( this.year, 'state.value.value' ) || this.year.state.value, 10 ),
                ...this.getCurrencyFieldsForPayload( 'current' ),
                ...prevEmployerData
            };

            this.props.submitForm({
                ...payload,
                previousRouteName: this.props.previousRoute.name
            });
        }
    }

    /**
     * Renders field according to config provided
     *
     * @param {Object} field - Details of field to be rendered
     * @returns {Object}
     */
    renderField( field ) {
        const {
            class_name: className,
            ...rest
        } = field;

        let onChange;
        let onFocus;
        let onBlur;
        let dateConfig = {};
        switch ( field.id ) {
            case 'prev_employer_tin':
                onChange = ( value ) => this[ field.id ].setState({ value: formatTIN( value ) });
                break;
            case 'prev_employer_zip_code':
                onChange = ( value ) => this[ field.id ].setState({ value: stripNonNumeric( value ) });
                break;
            case 'prev_employer_business_name':
            case 'prev_employer_business_address':
                onChange = ( value ) => this[ field.id ].setState({ value });
                break;
            case 'prev_date_from':
                onChange = ( selectedDate ) => this.setState({
                    prev_date_from: formatDate( selectedDate, DATE_FORMATS.API ) || ''
                }, () =>
                    this.prev_date_to.setState({ selectedDay: '' })
                );
                break;
            case 'prev_date_to':
                dateConfig = {
                    disabledDays: [{ before: new Date( this.state.prev_date_from ) }],
                    disabled: !this.state.prev_date_from
                };
                break;
            default:
                onChange = ( value ) => this[ field.id ].setState({ value: stripNonDigit( value ) });
                onFocus = () => this[ field.id ].setState({ value: stripNonDigit( this[ field.id ].state.value ) });
                onBlur = () => this[ field.id ].setState({ value: formatCurrency( this[ field.id ].state.value ) });
        }

        const config = {
            ...rest,
            ...dateConfig,
            onChange,
            onFocus,
            onBlur,
            ref: ( ref ) => { this[ field.id ] = ref; }
        };

        return (
            <div className={ className } key={ field.id }>
                { field.field_type && renderField( config ) }
            </div>
        );
    }

    renderCurrentEmployerForm = () => (
        <div>
            <H4>Current Employer</H4>
            { FORM_FIELDS.CURRENT_EMPLOYER_SECTIONS.map( ({ title, rows }) => (
                <div key={ title }>
                    <H5>{ title }</H5>
                    { rows.map( ( row, i ) => (
                        <div className="row" key={ `${title}_${i}` }>
                            { row.map( ( field ) => this.renderField( field ) ) }
                        </div>
                    ) ) }
                    <br />
                </div>
            ) ) }
        </div>
    );

    renderPreviousEmployerForm = () => (
        <div>
            { FORM_FIELDS.PREVIOUS_EMPLOYER_SECTIONS.map( ({ title, rows }) => (
                <div key={ title }>
                    <H5>{ title }</H5>
                    { rows.map( ( row, i ) => (
                        <div className="row" key={ `${title}_${i}` }>
                            { row.map( ( field ) => this.renderField( field ) ) }
                        </div>
                    ) ) }
                    <br />
                </div>
            ) ) }
        </div>
    )

    render() {
        return (
            <div>
                <MainWrapper>
                    <Helmet
                        title="Add Annual Earnings"
                        meta={ [
                            { name: 'description', content: 'Add Annual Earnings' }
                        ] }
                    />
                    <SnackBar
                        message={ this.props.notification.message }
                        title={ this.props.notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ this.props.notification.show }
                        delay={ 5000 }
                        type={ this.props.notification.type }
                    />
                    <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                    <NavWrapper>
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/annual-earnings' );
                                } }
                            >
                                &#8592; Back to { this.props.previousRoute.name ? this.props.previousRoute.name : 'Annual Earnings' }
                            </A>
                        </Container>
                    </NavWrapper>
                    { !this.props.loading && (
                        <HeadingWrapper>
                            <div className="heading">
                                <H3>Annual Earnings</H3>
                                <p>Enter the Year-to-Date earnings and Government deduction of the employee you wish to add</p>
                            </div>
                        </HeadingWrapper>
                    ) }

                    { this.props.loading && (
                        <div className="loader">
                            <Loader />
                        </div>
                    ) }

                    { !this.props.loading && (
                        <div>
                            <FormWrapper>
                                <Container>
                                    <div className="row">
                                        <div className="col-xs-4">
                                            <Typeahead
                                                id="input-employee-id"
                                                label="Employee Name or Employee ID"
                                                placeholder="Type in Employee Name or Employee ID"
                                                disabled={ !!this.state.prepopulatedEmployee }
                                                required
                                                options={ this.props.formOptions.employees }
                                                onInputChange={ ( value ) => {
                                                    if ( value.trim() === '' ) {
                                                        return false;
                                                    }

                                                    if ( this.state.typeaheadTimeout ) {
                                                        clearTimeout( this.state.typeaheadTimeout );
                                                    }

                                                    this.state.typeaheadTimeout = setTimeout( () => {
                                                        if ( value.length >= 3 ) {
                                                            this.props.getCompanyEmployees({
                                                                keyword: value,
                                                                formOptions: this.props.formOptions
                                                            });
                                                        }
                                                    }, 1000 );

                                                    return true;
                                                } }
                                                labelKey={ ( option ) => `${option.first_name} ${option.last_name} ${option.employee_id}` }
                                                filterBy={ [ 'first_name', 'last_name', 'employee_id' ] }
                                                defaultValue={ this.getEmployeeFullName() }
                                                ref={ ( ref ) => { this.employee = ref; } }
                                            />
                                        </div>
                                        <div className="col-xs-3">
                                            <Select
                                                id="select-year"
                                                label="Year"
                                                required
                                                data={ this.props.formOptions.years }
                                                placeholder="Select a Year"
                                                ref={ ( ref ) => { this.year = ref; } }
                                            />
                                        </div>
                                    </div>

                                    { this.renderCurrentEmployerForm() }
                                    <div className="prev-employer">
                                        <div className="prev-employer-toggle" ref={ ( ref ) => { this.previous_employer = ref; } }>
                                            <H4>Previous Employer</H4>
                                            <Switch
                                                checked={ this.state.show_previous_employer_form }
                                                onChange={ ( value ) =>
                                                    this.setState({ show_previous_employer_form: value }, () => {
                                                        if ( value ) {
                                                            const offset = this.previous_employer.offsetTop;
                                                            window.scrollTo({ top: offset - 150, behavior: 'smooth' });
                                                        }
                                                    })
                                                }
                                            />
                                        </div>
                                        { this.state.show_previous_employer_form && this.renderPreviousEmployerForm() }
                                    </div>
                                </Container>
                            </FormWrapper>
                        </div>
                    ) }
                </MainWrapper>
                <Footer>
                    <Button
                        label="Cancel"
                        type="neutral"
                        size="large"
                        onClick={ () => {
                            this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/annual-earnings' );
                        } }
                    />
                    <Button
                        label={ this.props.submitted ? <Loader /> : 'Submit' }
                        disabled={ this.props.submitted }
                        type="action"
                        size="large"
                        onClick={ this.submitForm }
                        ref={ ( ref ) => { this.submitButton = ref; } }
                    />
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    formOptions: makeSelectFormOptions(),
    submitted: makeSelectSubmitted(),
    errors: makeSelectErrors(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createAnnualEarningsActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ManualEntry );
