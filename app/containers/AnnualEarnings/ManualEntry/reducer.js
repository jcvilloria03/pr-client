import { fromJS } from 'immutable';
import {
    LOADING,
    SET_FORM_OPTIONS,
    SET_ERRORS,
    SUBMITTED,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    errors: {},
    submitted: false,
    batchUploadJobId: null,
    batchUploadStatus: '',
    batchUploadErrors: {},
    saving: {
        status: '',
        errors: {}
    },
    annualEarnings: {
        type_id: null
    },
    formOptions: {
        annualEarningsTypes: [],
        employees: [],
        payrollGroups: []
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Manual or batch assign Annual Earnings
 *
 */
function addAnnualEarnings( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addAnnualEarnings;
