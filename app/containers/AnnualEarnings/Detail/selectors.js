import { createSelector } from 'reselect';

/**
 * Direct selector to the Annual Earnings detail state domain
 */
const selectAnnualEarningsDetailDomain = () => ( state ) => state.get( 'annualEarningsDetail' );

const makeSelectLoading = () => createSelector(
    selectAnnualEarningsDetailDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectAnnualEarnings = () => createSelector(
    selectAnnualEarningsDetailDomain(),
    ( substate ) => substate.get( 'annualEarningsData' ).toJS()
);

const makeSelectFormOptions = () => createSelector(
    selectAnnualEarningsDetailDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectAnnualEarningsDetailDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectAnnualEarnings,
    makeSelectFormOptions,
    makeSelectNotification
};
