export const INITIALIZE = 'app/AnnualEarnings/Detail/INITIALIZE';
export const LOADING = 'app/AnnualEarnings/Detail/LOADING';

export const SET_ANNUAL_EARNINGS = 'app/AnnualEarnings/Detail/SET_ANNUAL_EARNINGS';

export const NOTIFICATION_SAGA = 'app/AnnualEarnings/Detail/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/AnnualEarnings/Detail/NOTIFICATION';
