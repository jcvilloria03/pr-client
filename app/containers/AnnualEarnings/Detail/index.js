import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';

import A from '../../../components/A';
import SnackBar from '../../../components/SnackBar';
import Button from '../../../components/Button';
import SubHeader from '../../../containers/SubHeader';
import { H2, H3, H4 } from '../../../components/Typography';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { getEmployeeFullName, formatDate, formatCurrency } from '../../../utils/functions';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper,
    MainWrapper,
    ValueWrapper,
    LoaderWrapper
} from './styles';

import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectAnnualEarnings
} from './selectors';

import * as editAllowanceActions from './actions';

/**
 * Annual Earnings Detail Component
 */
export class Detail extends React.Component {
    static propTypes = {
        loading: React.PropTypes.bool,
        annualEarningsData: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        params: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    }

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            prepopulatedEmployeeId: ''
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['edit.annual_earnings'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        const { id: annualEarningsDataId } = this.props.params;
        this.props.initializeData({ annualEarningsDataId });

        const prepopulatedEmployeeId = JSON.parse( localStorage.getItem( 'employeeIdForAnnualEarningsDetail' ) );
        prepopulatedEmployeeId && this.setState({ prepopulatedEmployeeId });
    }

    componentWillUnmount() {
        localStorage.removeItem( 'employeeIdForAnnualEarningsDetail' );
        this.props.resetStore();
    }

    renderCurrentEmployerData = () => {
        const { annualEarningsData } = this.props;
        return (
            <div>
                <div className="row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-3">
                        Employee Name and ID:
                        <ValueWrapper>
                            { annualEarningsData.employee ? getEmployeeFullName( annualEarningsData.employee ) : 'N/A' } { annualEarningsData.employee_id }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Date Hired:
                        <ValueWrapper>
                            { annualEarningsData.employee ? formatDate( annualEarningsData.employee.date_hired, DATE_FORMATS.DISPLAY ) : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Year:
                        <ValueWrapper>
                            { annualEarningsData.year ? annualEarningsData.year : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <H4>Current employer</H4>
                <div className="row" style={ { marginTop: '30px' } }>
                    <div className="col-xs-3">
                        Minimum Wage Earner:
                        <ValueWrapper>
                            {
                                annualEarningsData.current_minimum_wage_earner !== null
                                    ? annualEarningsData.current_minimum_wage_earner
                                        ? 'Yes'
                                        : 'No'
                                    : 'N/A'
                            }
                        </ValueWrapper>
                    </div>
                </div>
                <strong>Non-Taxable Income</strong>
                <div className="row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-3">
                        13th Month Pay and Other Benefits:
                        <ValueWrapper>
                            { annualEarningsData.current_non_taxable_13_month_pay_other_benefits ? `Php ${formatCurrency( annualEarningsData.current_non_taxable_13_month_pay_other_benefits )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        De Minimis Benefits:
                        <ValueWrapper>
                            { annualEarningsData.current_non_taxable_de_minimis ? `Php ${formatCurrency( annualEarningsData.current_non_taxable_de_minimis )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Salary and Other Compensation:
                        <ValueWrapper>
                            { annualEarningsData.current_non_taxable_salaries_other_forms_compensation ? `Php ${formatCurrency( annualEarningsData.current_non_taxable_salaries_other_forms_compensation )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <strong>Taxable Income</strong>
                <div className="row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-3">
                        Basic Salary:
                        <ValueWrapper>
                            { annualEarningsData.current_taxable_basic_salary ? `Php ${formatCurrency( annualEarningsData.current_taxable_basic_salary )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        13th Month Pay and Other Benefits:
                        <ValueWrapper>
                            { annualEarningsData.current_taxable_13_month_pay_other_benefits ? `Php ${formatCurrency( annualEarningsData.current_taxable_13_month_pay_other_benefits )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Salary and Other Compensation:
                        <ValueWrapper>
                            { annualEarningsData.current_taxable_salaries_other_forms_compensation ? `Php ${formatCurrency( annualEarningsData.current_taxable_salaries_other_forms_compensation )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <strong>Mandatory Deductions</strong>
                <div className="row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-3">
                        SSS Employee Contribution
                        <ValueWrapper>
                            { annualEarningsData.current_sss_employee_share ? `Php ${formatCurrency( annualEarningsData.current_sss_employee_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        SSS Employer Contribution:
                        <ValueWrapper>
                            { annualEarningsData.current_sss_employer_share ? `Php ${formatCurrency( annualEarningsData.current_sss_employer_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Total SSS Contribution:
                        <ValueWrapper>
                            { annualEarningsData.current_total_sss ? `Php ${formatCurrency( annualEarningsData.current_total_sss )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <div className="row" style={ { marginTop: '30px' } }>
                    <div className="col-xs-3">
                        Philhealth Employee Contribution
                        <ValueWrapper>
                            { annualEarningsData.current_philhealth_employee_share ? `Php ${formatCurrency( annualEarningsData.current_philhealth_employee_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Philhealth Employer Contribution:
                        <ValueWrapper>
                            { annualEarningsData.current_philhealth_employer_share ? `Php ${formatCurrency( annualEarningsData.current_philhealth_employer_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Total Philhealth Contribution:
                        <ValueWrapper>
                            { annualEarningsData.current_total_philhealth ? `Php ${formatCurrency( annualEarningsData.current_total_philhealth )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <div className="row" style={ { marginTop: '30px' } }>
                    <div className="col-xs-3">
                        PagIBIG Employee Contribution
                        <ValueWrapper>
                            { annualEarningsData.current_pagibig_employee_share ? `Php ${formatCurrency( annualEarningsData.current_pagibig_employee_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        PagIBIG Employer Contribution:
                        <ValueWrapper>
                            { annualEarningsData.current_pagibig_employer_share ? `Php ${formatCurrency( annualEarningsData.current_pagibig_employer_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Total PagIBIG Contribution:
                        <ValueWrapper>
                            { annualEarningsData.current_total_pagibig ? `Php ${formatCurrency( annualEarningsData.current_total_pagibig )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <div className="row" style={ { marginTop: '30px' } }>
                    <div className="col-xs-3">
                        Total Employee Contributions:
                        <ValueWrapper>
                            { annualEarningsData.current_total_employee_contributions ? `Php ${formatCurrency( annualEarningsData.current_total_employee_contributions )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <div className="row" style={ { marginTop: '30px' } }>
                    <div className="col-xs-3">
                        Withholding Tax:
                        <ValueWrapper>
                            { annualEarningsData.current_employer_withholding_tax ? `Php ${formatCurrency( annualEarningsData.current_employer_withholding_tax )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
            </div>
        );
    }

    renderPreviousEmployerData = () => {
        const { annualEarningsData } = this.props;
        return (
            <div>
                <H4>Previous Company Information</H4>
                <div className="row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-3">
                        Minimum Wage Earner:
                        <ValueWrapper>
                            {
                                annualEarningsData.prev_minimum_wage_earner !== null
                                    ? annualEarningsData.prev_minimum_wage_earner
                                        ? 'Yes'
                                        : 'No'
                                    : 'N/A'
                            }
                        </ValueWrapper>
                    </div>
                </div>
                <strong>Previous Company Information</strong>
                <div className="row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-3">
                        Company Name:
                        <ValueWrapper>
                            { annualEarningsData.prev_employer_name ? annualEarningsData.prev_employer_name : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Company Address:
                        <ValueWrapper>
                            { annualEarningsData.prev_employer_address ? annualEarningsData.prev_employer_address : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        TIN:
                        <ValueWrapper>
                            { annualEarningsData.prev_employer_tin ? annualEarningsData.prev_employer_tin : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Employer Zip Code:
                        <ValueWrapper>
                            { annualEarningsData.prev_employer_zip ? annualEarningsData.prev_employer_zip : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <strong>Non-Taxable Income</strong>
                <div className="row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-3">
                        13th Month Pay and Other Benefits:
                        <ValueWrapper>
                            { annualEarningsData.prev_non_taxable_13_month_pay_other_benefits ? `Php ${formatCurrency( annualEarningsData.prev_non_taxable_13_month_pay_other_benefits )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        De Minimis Benefits:
                        <ValueWrapper>
                            { annualEarningsData.prev_non_taxable_de_minimis ? `Php ${formatCurrency( annualEarningsData.prev_non_taxable_de_minimis )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Salary and Other Compensation:
                        <ValueWrapper>
                            { annualEarningsData.prev_non_taxable_salaries_other_forms_compensation ? `Php ${formatCurrency( annualEarningsData.prev_non_taxable_salaries_other_forms_compensation )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <strong>Taxable Income</strong>
                <div className="row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-3">
                        Basic Salary:
                        <ValueWrapper>
                            { annualEarningsData.prev_taxable_basic_salary ? `Php ${formatCurrency( annualEarningsData.prev_taxable_basic_salary )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        13th Month Pay and Other Benefits:
                        <ValueWrapper>
                            { annualEarningsData.prev_taxable_13_month_pay_other_benefits ? `Php ${formatCurrency( annualEarningsData.prev_taxable_13_month_pay_other_benefits )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Salary and Other Compensation:
                        <ValueWrapper>
                            { annualEarningsData.prev_taxable_salaries_other_forms_compensation ? `Php ${formatCurrency( annualEarningsData.prev_taxable_salaries_other_forms_compensation )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <strong>Mandatory Deductions</strong>
                <div className="row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-3">
                        SSS Employee Contribution
                        <ValueWrapper>
                            { annualEarningsData.prev_sss_employee_share ? `Php ${formatCurrency( annualEarningsData.prev_sss_employee_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        SSS Employer Contribution:
                        <ValueWrapper>
                            { annualEarningsData.prev_sss_employer_share ? `Php ${formatCurrency( annualEarningsData.prev_sss_employer_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Total SSS Contribution:
                        <ValueWrapper>
                            { annualEarningsData.prev_total_sss ? `Php ${formatCurrency( annualEarningsData.prev_total_sss )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <div className="row" style={ { marginTop: '30px' } }>
                    <div className="col-xs-3">
                        Philhealth Employee Contribution
                        <ValueWrapper>
                            { annualEarningsData.prev_philhealth_employee_share ? `Php ${formatCurrency( annualEarningsData.prev_philhealth_employee_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Philhealth Employer Contribution:
                        <ValueWrapper>
                            { annualEarningsData.prev_philhealth_employer_share ? `Php ${formatCurrency( annualEarningsData.prev_philhealth_employer_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Total Philhealth Contribution:
                        <ValueWrapper>
                            { annualEarningsData.prev_total_philhealth ? `Php ${formatCurrency( annualEarningsData.prev_total_philhealth )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <div className="row" style={ { marginTop: '30px' } }>
                    <div className="col-xs-3">
                        PagIBIG Employee Contribution
                        <ValueWrapper>
                            { annualEarningsData.prev_pagibig_employee_share ? `Php ${formatCurrency( annualEarningsData.prev_pagibig_employee_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        PagIBIG Employer Contribution:
                        <ValueWrapper>
                            { annualEarningsData.prev_pagibig_employer_share ? `Php ${formatCurrency( annualEarningsData.prev_pagibig_employer_share )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-3">
                        Total PagIBIG Contribution:
                        <ValueWrapper>
                            { annualEarningsData.prev_total_pagibig ? `Php ${formatCurrency( annualEarningsData.prev_total_pagibig )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <div className="row" style={ { marginTop: '30px' } }>
                    <div className="col-xs-3">
                        Total Employee Contributions:
                        <ValueWrapper>
                            { annualEarningsData.prev_total_employee_contributions ? `Php ${formatCurrency( annualEarningsData.prev_total_employee_contributions )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
                <div className="row" style={ { marginTop: '30px' } }>
                    <div className="col-xs-3">
                        Withholding Tax:
                        <ValueWrapper>
                            { annualEarningsData.prev_employer_withholding_tax ? `Php ${formatCurrency( annualEarningsData.prev_employer_withholding_tax )}` : 'N/A' }
                        </ValueWrapper>
                    </div>
                </div>
            </div>
        );
    }

    renderSummary = () => {
        const { annualEarningsData } = this.props;
        return (
            <div className="row" style={ { marginTop: '20px' } }>
                <div className="col-xs-12">
                    <p style={ { fontWeight: 'bold' } }>Summary</p>
                    <div className="summary-panel">
                        <div className="summary-panel__column">
                            <strong>Gross Income</strong>
                            <p>
                                {annualEarningsData.gross_income ? `Php ${formatCurrency( annualEarningsData.gross_income )}` : 'N/A' }
                            </p>
                        </div>
                        <div className="summary-panel__column">
                            <strong>Taxable Income</strong>
                            <p>
                                {annualEarningsData.taxable_income ? `Php ${formatCurrency( annualEarningsData.taxable_income )}` : 'N/A' }
                            </p>
                        </div>
                        <div className="summary-panel__column">
                            <strong>Withholding Tax</strong>
                            <p>
                                {annualEarningsData.total_withholding_tax ? `Php ${formatCurrency( annualEarningsData.total_withholding_tax )}` : 'N/A' }
                            </p>
                        </div>
                        <div className="summary-panel__column">
                            <strong>Net Income</strong>
                            <p>
                                {annualEarningsData.net_income ? `Php ${formatCurrency( annualEarningsData.net_income )}` : 'N/A' }
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * Component Render Method
     */
    render() {
        return (
            <PageWrapper>
                <Helmet
                    title="Annual Earnings Detail"
                    meta={ [
                        { name: 'description', content: 'Annual Earnings Detail' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/annual-earnings' );
                            } }
                        >
                         &#8592; Back to { this.props.previousRoute.name ? this.props.previousRoute.name : 'Annual Earnings' }
                        </A>
                    </Container>
                </NavWrapper>
                { this.props.loading ? (
                    <LoaderWrapper>
                        <H2>Loading</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoaderWrapper>
                ) : (
                    <div>
                        <Container>
                            <HeadingWrapper>
                                <h3>Annual Earnings</h3>
                            </HeadingWrapper>
                        </Container>
                        <FormWrapper>
                            <MainWrapper>
                                <div>
                                    <Container>
                                        { this.renderCurrentEmployerData() }
                                        { this.renderPreviousEmployerData() }
                                        { this.renderSummary() }
                                    </Container>
                                    <div className="foot">
                                        <Button
                                            label="Cancel"
                                            type="neutral"
                                            size="large"
                                            onClick={ () => {
                                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/annual-earnings' );
                                            } }
                                        />
                                        <Button
                                            label="Edit"
                                            type="action"
                                            size="large"
                                            onClick={ () => {
                                                browserHistory.push( `/annual-earnings/${this.props.params.id}/edit` );
                                            } }
                                        />
                                    </div>
                                </div>
                            </MainWrapper>
                        </FormWrapper>
                    </div>
                ) }
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    annualEarningsData: makeSelectAnnualEarnings(),
    notification: makeSelectNotification()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        editAllowanceActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Detail );
