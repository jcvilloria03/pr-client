import { fromJS } from 'immutable';
import {
    LOADING,
    SET_ANNUAL_EARNINGS,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    annualEarningsData: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Annual Earnings detail reducer
 *
 */
function annualEarningsDataDetailReducer( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_ANNUAL_EARNINGS:
            return state.set( 'annualEarningsData', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default annualEarningsDataDetailReducer;
