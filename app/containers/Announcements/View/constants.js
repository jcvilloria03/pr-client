/*
 *
 * Announcements constants
 *
 */
const namespace = 'app/Announcements/View';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;

export const GET_ANNOUNCEMENT = `${namespace}/GET_ANNOUNCEMENT`;
export const SET_ANNOUNCEMENT = `${namespace}/SET_ANNOUNCEMENT`;

export const LOADING = `${namespace}/LOADING`;

export const SET_ANNOUNCEMENT_PAGINATION = `${namespace}/SET_ANNOUNCEMENT_PAGINATION`;

export const DOWNLOAD_LOADING = `${namespace}/DOWNLOAD_LOADING`;
export const EXPORT_SET_ANNOUNCEMENT = `${namespace}/EXPORT_SET_ANNOUNCEMENT`;
export const EXPORT_ANNOUNCEMENT = `${namespace}/EXPORT_ANNOUNCEMENT`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;

export const CREATE_ANNOUNCEMENTS = `${namespace}/CREATE_ANNOUNCEMENTS`;
export const CREATE_ANNOUNCEMENTS_LOADING = `${namespace}/CREATE_ANNOUNCEMENTS_LOADING`;
