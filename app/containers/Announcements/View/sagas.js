/* eslint-disable require-jsdoc */

import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery } from 'redux-saga';
import { get } from 'lodash';
import fileDownload from 'js-file-download';
import { Fetch } from 'utils/request';
import { company } from '../../../utils/CompanyService';
import { GET_ANNOUNCEMENT, SET_ANNOUNCEMENT, LOADING, SET_ANNOUNCEMENT_PAGINATION, EXPORT_ANNOUNCEMENT, DOWNLOAD_LOADING, NOTIFICATION, SET_NOTIFICATION, CREATE_ANNOUNCEMENTS } from './constants';

export function* getAnnouncement({ payload }) {
    try {
        yield put({
            type: DOWNLOAD_LOADING,
            payload: true
        });
        const { page, perPage, orderBy = 'created_at', sortOrder = 'desc', term = '', role = 'recipient' } = payload;

        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/announcements?per_page=${perPage}&page=${page}&role=${role}&sort_by=${orderBy}&sort_order=${sortOrder}&term=${term}`, { method: 'GET' });
        const { ...other } = res;

        yield put({ type: SET_ANNOUNCEMENT, payload: res });
        yield put({ type: SET_ANNOUNCEMENT_PAGINATION, payload: other.meta.pagination });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
        yield put({ type: LOADING, payload: false });
        yield put({ type: DOWNLOAD_LOADING, payload: false });
    } finally {
        yield put({ type: DOWNLOAD_LOADING, payload: false });
        yield put({ type: LOADING, payload: false });
    }
}

export function* exportAnnouncement({ payload }) {
    try {
        yield put({ type: DOWNLOAD_LOADING, payload: true });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/announcements/download`, {
            method: 'POST', data: payload
        });
        fileDownload( response, 'received-announcements.csv' );
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
        yield put({ type: DOWNLOAD_LOADING, payload: false });
    } finally {
        yield put({ type: DOWNLOAD_LOADING, payload: false });
    }
}
/**
 * Create Announcements
 */
export function* createAnnouncements({ payload }) {
    try {
        yield put({ type: LOADING, payload: true });
        yield call( Fetch, '/announcement', { method: 'POST', data: payload });
        yield call( getAnnouncement, { payload: {
            perPage: 10,
            role: 'recipient',
            page: 1,
            orderBy: 'created_at',
            sortOrder: 'desc',
            term: ''
        }});
        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Announcement successfully created',
            type: 'success'
        });
        console.log( 'Called user:', payload );
    } catch ( error ) {
        console.log( '!!!', error );
        yield call( notifyError, error );
        yield put({ type: LOADING, payload: false });
    } finally {
        yield put({ type: LOADING, payload: false });
    }
}
/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}
/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for Announcement
 */
export function* watchForAnnouncement() {
    const watcher = yield takeEvery( GET_ANNOUNCEMENT, getAnnouncement );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for CREATE_ANNOUNCEMENTS
 */
export function* watchForCreateAnnouncements() {
    const watcher = yield takeEvery( CREATE_ANNOUNCEMENTS, createAnnouncements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
export function* watchForExportAnnouncement() {
    const watcher = yield takeEvery( EXPORT_ANNOUNCEMENT, exportAnnouncement );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
// All sagas to be loaded
export default [
    watchForAnnouncement,
    watchForExportAnnouncement,
    watchForCreateAnnouncements
];
