/* eslint-disable react/no-did-update-set-state */
/* eslint-disable camelcase */

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { debounce } from 'lodash';
import * as qs from 'qs';
import moment from 'moment';
import { createStructuredSelector } from 'reselect';
import {
    makeSelectAnnouncement,
    makeSelectAnnouncementPagination,
    makeSelectCreateLoading,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectTableLoading
} from './selectors';

import Modal from '../../../components/Modal/index';
import Search from '../../../components/Search';
import Switch from '../../../components/Switch';
import MultiSelect from '../../../components/MultiSelect';
import Button from '../../../components/Button';
import AsyncTable from '../../../components/AsyncTable';
import Input from '../../../components/Input';
import SalDropdown from '../../../components/SalDropdown';
import SnackBar from '../../../components/SnackBar';
import Loader from '../../../components/Loader';
import { getIdsOfSelectedRows } from '../../../components/Table/helpers';

import {
    formatDate,
    formatDeleteLabel,
    formatPaginationLabel
} from '../../../utils/functions';
import { browserHistory } from '../../../utils/BrowserHistory';
import { DATE_FORMATS } from '../../../utils/constants';
import { auth } from '../../../utils/AuthService';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';

import { SELECT_ALL_EMPLOYEES_OPTIONS } from '../../Dashboard/constants';

import * as announcementsAction from './actions';
import {
    ConfirmBodyWrapperStyle,
    ModalWrapper,
    PageWrapper,
    StyledModalFooter
} from './styles';
import { Spinner } from '../../../components/Spinner';

/**
 *
 * Announcements
 *
 */
export class Announcements extends React.Component {
    static propTypes = {
        pagination: React.PropTypes.object,
        received: React.PropTypes.object,
        getAnnouncement: React.PropTypes.func,
        exportAnnouncement: React.PropTypes.func,
        createAnnouncements: React.PropTypes.func,
        loading: React.PropTypes.bool,
        tableLoad: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        location: React.PropTypes.object
    };
    static defaultProps = {
        loading: { loading: true }
    };
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            profile: null,
            selectLabel: '',
            showLabel: false,
            announcementsList: [],
            subject: '',
            message: '',
            role: 'recipient',
            orderBy: 'created_at',
            sortOrder: 'desc',
            term: '',
            reply: '',
            page: 0,
            replyResponse: '',
            allow_response: false,
            read_receipt: false,
            inactiveUsers: [],
            toValue: [],
            toFlag: false,
            msgFlag: false,
            subjectFlag: false,
            expandedRows: {},
            announcementIndex: null,
            isLoading: false
        };
        this.searchInput = null;
        this.createModal = null;
        this.inActiveModal = null;
        this.replyResponse = null;
        this.handleSearch = this.handleSearch.bind( this );
        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.onSortingChange = this.onSortingChange.bind( this );
    }
    componentWillMount() {
        const profile = auth.getUser();

        this.setState({ profile });
    }
    componentDidMount() {
        this.props.getAnnouncement({
            page: 1,
            perPage: 10,
            role: this.state.role,
            orderBy: this.state.orderBy,
            sortOrder: this.state.sortOrder,
            term: this.state.term
        });
    }
    componentWillReceiveProps( nextProps ) {
        if ( nextProps.received.data !== this.props.received.data ) {
            this.setState({
                announcementsList: nextProps.received.data,
                label: formatPaginationLabel({
                    page: nextProps.pagination.current_page - 1,
                    pageSize: nextProps.pagination.per_page,
                    dataLength: nextProps.pagination.total
                })
            });
        }
    }
    componentDidUpdate( prevProps, prevState ) {
        if ( this.state.announcementsList !== prevState.announcementsList ) {
            const announcementId = qs.parse( this.props.location.search, { ignoreQueryPrefix: true }).id;
            if ( announcementId ) {
                const idx = this.state.announcementsList.findIndex( ( a ) => a.id === parseInt( announcementId, 10 ) );
                if ( idx ) {
                    this.setState({
                        expandedRows: {
                            [ idx ]: true
                        },
                        announcementIndex: idx
                    });
                }
            }
        }
    }
    onSortingChange( column, additive ) {
        let newSorting = JSON.parse(
            JSON.stringify( this.receivedTable.state.sorting, ( key, value ) => {
                if ( typeof value === 'function' ) {
                    return value.toString();
                }
                return value;
            }),
        );
        const existingIndex = newSorting.findIndex( ( a ) => a.id === column.id );
        let isDesc = false;
        if ( existingIndex > -1 ) {
            const existing = newSorting[ existingIndex ];
            if ( existing.desc ) {
                if ( additive ) {
                    newSorting.splice( existingIndex, 1 );
                } else {
                    existing.desc = false;
                    newSorting = [existing];
                }
            } else {
                existing.desc = true;
                isDesc = true;
                if ( !additive ) {
                    newSorting = [existing];
                }
            }
        } else if ( additive ) {
            newSorting.push({
                id: column.id,
                desc: false
            });
        } else {
            newSorting = [
                {
                    id: column.id,
                    desc: false
                }
            ];
        }
        const orderDir = isDesc ? 'desc' : 'asc';
        this.setState(
            {
                orderBy: column.id,
                sortOrder: orderDir
            },
            () => {
                this.props.getAnnouncement({
                    page: 1,
                    perPage: this.props.pagination.per_page,
                    role: this.state.role,
                    orderBy: this.state.orderBy,
                    sortOrder: this.state.sortOrder,
                    term: this.state.term
                });
                setTimeout( () => this.handleTableChanges(), 2000 );
            },
        );
        this.receivedTable.setState({
            sorting: newSorting
        });
        this.receivedTable.tableComponent.setState({
            sorting: newSorting
        });
    }

    onPageChange = ( page ) => {
        this.props.getAnnouncement({
            page,
            perPage:
                this.props.pagination.per_page,
            role: this.state.role,
            orderBy: this.state.orderBy,
            sortOrder: this.state.sortOrder,
            term: this.state.term
        });
        this.receivedTable.setState({
            selected: [],
            selectedRows: []
        });
        this.setState({ showLabel: false });
        setTimeout(
            () => this.handleTableChanges(),
            2000,
        );
    }

    onPageSizeChange = ( data ) => {
        if ( !data ) {
            return;
        }
        this.props.getAnnouncement({
            page: 1,
            perPage: data,
            role: this.state.role,
            orderBy: this.state.orderBy,
            sortOrder: this.state.sortOrder,
            term: this.state.term
        });
        this.receivedTable.setState({
            selected: [],
            selectedRows: []
        });
        this.setState({ showLabel: false });
        setTimeout(
            () => this.handleTableChanges(),
            2100,
        );
    }

    getTableColumns() {
        return [
            {
                header: 'Sender',
                sortable: true,
                minWidth: 250,
                accessor: 'sender',
                style: { height: '4.5rem' },
                render: ({ row }) => <div>{row.sender}</div>
            },
            {
                header: 'Subject',
                sortable: true,
                accessor: 'subject',
                render: ({ row }) => <div>{row.subject}</div>,
                minWidth: 200
            },
            {
                id: 'Date Received',
                header: 'Date Received',
                sortable: false,
                accessor: 'date_received',
                render: ({ row }) => <div>{row.date_received}</div>,
                minWidth: 200
            },
            {
                id: 'created_at',
                header: 'Created On',
                accessor: 'created_at',
                render: ({ row }) => <div>{row.created_at}</div>,
                sortable: false,
                minWidth: 300
            }
        ];
    }

    getRecipients = ( announcementList ) => {
        let recipients = announcementList.recipient_groups || [];

        if ( this.state.profile.account_id ) {
            recipients = recipients.filter(
                ( recipient ) => recipient.rel_id !== this.state.profile.account_id,
            );
        }

        const countOfRecipients = recipients.length;
        const name = recipients[ 0 ].name;

        if ( !countOfRecipients ) return '';

        return countOfRecipients === 1
            ? `me and ${name}`
            : `me and ${name} and ${countOfRecipients - 1} ${
                  countOfRecipients === 2 ? 'other' : 'others'
              }`;
    };
    getInactiveEmployees = () => {
        this.setState({ isLoading: true });

        const { subject, message, toValue } = this.state;
        if ( !toValue.length ) {
            this.setState({ toFlag: true });
        }
        if ( subject.length <= 0 ) {
            this.setState({ subjectFlag: true });
        }
        if ( message.length <= 0 ) {
            this.setState({ msgFlag: true });
        }
        if ( toValue.length && subject.length > 0 && message.length > 0 ) {
            Fetch(
                `/company/${this.state.profile.last_active_company_id}/inactive_employees`,
                {
                    method: 'POST',
                    data: {
                        affected_employees: this.employees.state.value
                    }
                },
            ).then( ({ data }) => {
                if ( data.length ) {
                    this.setState({
                        inactiveUsers: data
                    });
                    this.inActiveModal.toggle();
                } else {
                    this.props.createAnnouncements({
                        affected_employees: this.employees.state.value,
                        company_id: this.state.profile.last_active_company_id,
                        subject: this.state.subject,
                        message: this.state.message,
                        allow_response: this.state.allow_response,
                        read_receipt: this.state.read_receipt
                    });

                    this.reinitialize();
                    this.createModal.toggle();
                }
            });
        }
        this.setState({ isLoading: false });
    };
    loadEmployeesList = ( keyword, callback ) => {
        Fetch(
            `/company/${this.state.profile.last_active_company_id}/affected_employees/search?term=${keyword}&limit=10&without_users=yes`,
            { method: 'GET' },
        )
            .then( ( result ) => {
                const list = result.map( ({ id, name, type }) => ({
                    id,
                    type,
                    uid: `${type}${id}`,
                    value: id,
                    label: name,
                    field: type,
                    disabled: false
                }) );
                callback( null, {
                    options: SELECT_ALL_EMPLOYEES_OPTIONS.concat( list )
                });
            })
            .catch( ( error ) => callback( error, null ) );
    };
    handleTableChanges( tableProps = this.receivedTable.tableComponent.state ) {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });
        this.setState({
            label: formatPaginationLabel( tableProps ),
            selectLabel: formatDeleteLabel()
        });
    }
    handleSearch = async ( term ) => {
        if ( term !== this.state.term ) {
            this.setState({ term });
            this.props.getAnnouncement({
                page: 1,
                perPage: 10,
                role: this.state.role,
                orderBy: this.state.orderBy,
                sortOrder: this.state.sortOrder,
                term
            });
            setTimeout( () => this.handleTableChanges(), 2000 );
        } else {
            this.setState({ term });
            this.props.getAnnouncement({
                page: 1,
                perPage: 10,
                role: this.state.role,
                orderBy: this.state.orderBy,
                sortOrder: this.state.sortOrder,
                term: this.state.term
            });
            setTimeout( () => this.handleTableChanges(), 2000 );
        }
    };

    exportAnnouncement = () => {
        const Ids = getIdsOfSelectedRows( this.receivedTable );
        const payload = {
            announcements_ids: Ids,
            company_id: company.getLastActiveCompanyId(),
            role: 'recipient',
            download_all: false,
            term: '',
            sort_by: 'created_at',
            sort_order: 'desc'
        };
        this.props.exportAnnouncement( payload );
    };
    Replysubmit = ( data ) => {
        const dataList = {
            message: this.replyInput.state.value
        };
        Fetch( `/announcement/${data.id}/reply`, {
            method: 'POST',
            data: { message: dataList.message }
        }).then( () => {
            this.props.getAnnouncement({
                page: 1,
                perPage: 10,
                role: this.state.role,
                orderBy: this.state.orderBy,
                sortOrder: this.state.sortOrder,
                term: this.state.term
            });
        });
    };
    reinitialize = () =>
        this.setState({
            subject: '',
            message: '',
            toValue: [],
            allow_response: false,
            read_receipt: false,
            toFlag: false,
            msgFlag: false,
            subjectFlag: false
        });

    render() {
        const createAnnouncementBtns = [
            {
                id: 'buttonCancel',
                type: 'grey',
                label: 'Cancel',
                onClick: () => {
                    this.reinitialize();
                    this.createModal.toggle();
                },
                className: 'cancelBtn'
            },
            {
                id: 'buttonProceed',
                type: 'action',
                label: loading ? <Loader /> : <span>Send</span>,
                onClick: () => {
                    this.getInactiveEmployees();
                },
                className: 'sendBtn'
            }
        ];

        const TableDataDisplay = this.state.announcementsList
            ? this.state.announcementsList.map( ( data ) => ({
                ...data,
                sender: data.sender.full_name,
                subject: data.subject,
                date_received:
                      formatDate(
                          data.recipient.seen_at,
                          DATE_FORMATS.DISPLAY,
                      ) || 'N/A',
                created_at: formatDate( data.created_at, DATE_FORMATS.DISPLAY )
            }) )
            : [];
        const { loading, tableLoad, createAnnouncements } = this.props;
        const {
            profile,
            subject,
            message,
            inactiveUsers,
            allow_response,
            read_receipt
        } = this.state;

        return (
            <div>
                <Helmet
                    title="Announcements"
                    meta={ [
                        {
                            name: 'description',
                            content: 'Description of Announcements'
                        }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => {
                        this.notification = ref;
                    } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <PageWrapper>
                    {( this.state.isLoading || this.props.loading ) && <Spinner />}
                    <Container>
                        <div>
                            <div className="announcement_title">
                                <h1>Announcements </h1>
                                <p>
                                    Easily communicate company policies and
                                    government proclamations through this
                                    page.
                                </p>
                                <Button
                                    id="creatBtn"
                                    type="action"
                                    size="large"
                                    label="Create Announcement"
                                    onClick={ () => {
                                        this.createModal.toggle();
                                    } }
                                />
                            </div>
                            <div className="content">
                                <div className="button_header">
                                    <Button
                                        className="inbox"
                                        id="inboxBtn"
                                        type="neutral"
                                        size="default"
                                        label="Inbox"
                                    />
                                    <Button
                                        className="outbox"
                                        id="inboxBtn"
                                        type="grey"
                                        size="default"
                                        label="Outbox"
                                        onClick={ () => {
                                            browserHistory.push(
                                            '/time/company/announcements/sent/',
                                            true,
                                        );
                                        } }
                                    />
                                </div>
                                <div className="outbox_section">
                                    <h5>Announcement List: Inbox</h5>
                                    <Search
                                        ref={ ( ref ) => {
                                            this.search = ref;
                                        } }
                                        handleSearch={ debounce(
                                        this.handleSearch,
                                        1000,
                                    ) }
                                    />
                                    <div className="showing_contain">
                                        {this.state.showLabel ? (
                                            <div className="showing_box">
                                                {this.state.selectLabel}
                                                <span>
                                                    <SalDropdown
                                                        dropdownItems={ [
                                                            {
                                                                label: 'Download',
                                                                children: (
                                                                    <div>
                                                                    Download
                                                                </div>
                                                            ),
                                                                onClick:
                                                                this
                                                                    .exportAnnouncement
                                                            }
                                                        ] }
                                                    />
                                                </span>
                                            </div>
                                    ) : (
                                        <p>{this.state.label}</p>
                                    )}
                                    </div>
                                </div>
                                <AsyncTable
                                    indexToScroll={ this.state.announcementIndex }
                                    expandedRows={ this.state.expandedRows }
                                    data={ TableDataDisplay }
                                    columns={ this.getTableColumns() }
                                    selectable
                                    pagination
                                    manual
                                    loading={ tableLoad }
                                    ref={ ( ref ) => {
                                        this.receivedTable = ref;
                                    } }
                                    onDataChange={ this.handleTableChanges }
                                    onPageChange={ this.onPageChange }
                                    onPageSizeChange={ this.onPageSizeChange }
                                    onSelectionChange={ ({ selected }) => {
                                        const selectionLength = selected.filter(
                                        ( row ) => row,
                                    ).length;
                                        this.setState({
                                            showLabel: selectionLength > 0,
                                            selectLabel:
                                            formatDeleteLabel(
                                                selectionLength,
                                            )
                                        });
                                    } }
                                    sizeOptions={ [ 10, 25, 50, 100 ] }
                                    tablePage={
                                    this.props.pagination.current_page -
                                        1 || 0
                                }
                                    pageSize={
                                    this.props.pagination.per_page || 10
                                }
                                    pages={ this.props.pagination.total_pages }
                                    onSortingChange={ this.onSortingChange }
                                    SubComponent={ ( data ) => (
                                        <section className="details">
                                            <div className="head">
                                                <p>
                                                    <strong>Subject:</strong>{' '}
                                                    {data.row.subject}
                                                </p>
                                                <p>
                                                    <strong>
                                                    Date received:
                                                </strong>{' '}
                                                    {data.row.date_received}
                                                </p>
                                            </div>
                                            <p>
                                                <strong>From:</strong>{' '}
                                                {data.row.sender}
                                            </p>
                                            <p>
                                                <strong>To:</strong>{' '}
                                                {this.getRecipients( data.row )}
                                            </p>
                                            <p>{data.row.message}</p>
                                            {data.row.allow_response === true &&
                                        !data.row.reply ? (
                                            <div className="replysection">
                                                <strong className="title">
                                                    Reply
                                                </strong>

                                                <Input
                                                    className="replyinput"
                                                    id="rpText"
                                                    type="text"
                                                    name="replyMsg"
                                                    value=""
                                                    ref={ ( ref ) => {
                                                        this.replyInput =
                                                            ref;
                                                    } }
                                                />

                                                <p className="replybtn">
                                                    <Button
                                                        label="Reply"
                                                        size="large"
                                                        type="action"
                                                        onClick={ () => {
                                                            this.Replysubmit(
                                                                data.row,
                                                            );
                                                        } }
                                                    />
                                                </p>
                                            </div>
                                        ) : data.row.allow_response ===
                                                true &&
                                            data.row.reply.message !== '' ? (
                                                <div>
                                                    <p>
                                                        <strong>Reply</strong>
                                                    </p>
                                                    <div className="head">
                                                        <p>
                                                            {
                                                            data.rowValues
                                                                .sender
                                                        }
                                                        </p>
                                                        <p>
                                                        Replied on{' '}
                                                            {moment(
                                                            data.row.reply
                                                                .created_at &&
                                                                data.row
                                                                    .reply
                                                                    .created_at,
                                                        ).format(
                                                            'MMMM DD, YYYY - HH:mm',
                                                        )}
                                                        </p>
                                                    </div>
                                                    <p>
                                                        {data.row.reply.message}
                                                    </p>
                                                </div>
                                        ) : (
                                            ''
                                        )}
                                        </section>
                                ) }
                                />
                            </div>
                        </div>
                    </Container>
                    <Modal
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    Listed Below are Inactive Recipients:
                                    {inactiveUsers.map( ( user, index ) => (
                                        <div key={ index }>
                                            <span>{user.full_name}</span>
                                        </div>
                                    ) )}
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        buttons={ [
                            {
                                id: 'buttonCancel',
                                type: 'action',
                                label: 'Edit',
                                onClick: () => {
                                    this.inActiveModal.toggle();
                                }
                            },
                            {
                                id: 'buttonProceed',
                                type: 'grey',
                                label: 'Send Anyway',
                                onClick: () => {
                                    this.createModal.toggle();
                                    this.reinitialize();
                                    this.inActiveModal.toggle();
                                    createAnnouncements({
                                        affected_employees:
                                            this.employees.state.value,
                                        company_id: profile.last_active_company_id,
                                        subject,
                                        message,
                                        allow_response,
                                        read_receipt
                                    });
                                }
                            }
                        ] }
                        ref={ ( ref ) => {
                            this.inActiveModal = ref;
                        } }
                        title="Inactive recipient"
                        size="mb"
                        showClose={ false }
                        backdrop="static"
                    />
                    <Modal
                        className="max840px"
                        title="Create Announcement"
                        size="lg"
                        body={
                            <ModalWrapper>
                                <div
                                    className={
                                        this.state.toFlag
                                            ? 'location_sectionadd'
                                            : 'location_section'
                                    }
                                >
                                    <MultiSelect
                                        id="announcement"
                                        async
                                        label={ <span>To:</span> }
                                        loadOptions={ this.loadEmployeesList }
                                        ref={ ( ref ) => {
                                            this.employees = ref;
                                        } }
                                        onChange={ ( value ) =>
                                            this.setState({
                                                toValue: value,
                                                toFlag: !value.length
                                            })
                                        }
                                        placeholder="Enter location, department name, position, employee name"
                                        hasSearchIcon
                                    />
                                    {this.state.toFlag ? (
                                        <p>Please add recipients</p>
                                    ) : (
                                        ''
                                    )}
                                </div>
                                <div
                                    className={
                                        this.state.subjectFlag
                                            ? 'location_sectionadd'
                                            : 'subject_contain'
                                    }
                                >
                                    <Input
                                        className=""
                                        label="Subject:"
                                        id="subject"
                                        name="subject"
                                        onChange={ ( data ) => {
                                            this.setState({
                                                subject: data,
                                                subjectFlag: data.length <= 0
                                            });
                                        } }
                                        ref={ ( ref ) => {
                                            this.subject = ref;
                                        } }
                                    />
                                    {this.state.subjectFlag ? (
                                        <p>Please enter a subject</p>
                                    ) : (
                                        ''
                                    )}
                                </div>
                                <div
                                    className={
                                        this.state.msgFlag
                                            ? 'location_sectionadd'
                                            : 'message_contain'
                                    }
                                >
                                    <Input
                                        className=""
                                        id="message"
                                        label="Message:"
                                        type="textarea"
                                        onChange={ ( data ) => {
                                            this.setState({
                                                message: data,
                                                msgFlag: data.length <= 0
                                            });
                                        } }
                                        ref={ ( ref ) => {
                                            this.message = ref;
                                        } }
                                    />
                                    {this.state.msgFlag ? (
                                        <p>Please enter a message</p>
                                    ) : (
                                        ''
                                    )}
                                </div>
                                <div className="toggleWrapper enforce_switch">
                                    <Switch
                                        checked={ allow_response }
                                        onChange={ ( data ) => {
                                            this.setState({
                                                allow_response: data
                                            });
                                        } }
                                    />
                                    &nbsp; Allow response from recipients
                                </div>

                                <div className="toggleWrapper enforce_switch">
                                    <Switch
                                        checked={ read_receipt }
                                        onChange={ ( data ) => {
                                            this.setState({
                                                read_receipt: data
                                            });
                                        } }
                                    />
                                    &nbsp; Show Read Receipt
                                </div>
                            </ModalWrapper>
                        }

                        ref={ ( ref ) => {
                            this.createModal = ref;
                        } }
                        backdrop="static"
                        footer={
                            <StyledModalFooter>
                                {createAnnouncementBtns.map( ( btn ) => (
                                    <Button
                                        key={ btn.id }
                                        id={ btn.id }
                                        type={ btn.type }
                                        size={ btn.size }
                                        label={ btn.label }
                                        onClick={ btn.onClick }
                                        className={ btn.className }
                                    />
                                ) )}
                            </StyledModalFooter>
                        }
                    />
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    received: makeSelectAnnouncement(),
    loading: makeSelectLoading(),
    pagination: makeSelectAnnouncementPagination(),
    notification: makeSelectNotification(),
    tableLoad: makeSelectTableLoading(),
    createLoad: makeSelectCreateLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators( announcementsAction, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( Announcements );
