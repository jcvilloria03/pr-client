import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_ANNOUNCEMENT,
    LOADING,
    SET_ANNOUNCEMENT_PAGINATION,
    DOWNLOAD_LOADING,
    SET_NOTIFICATION,
    EXPORT_SET_ANNOUNCEMENT
} from './constants';

const initialState = fromJS({
    received: {},
    downloading: false,
    pagination: {
        count: 0,
        current_page: 0,
        links: [],
        per_page: 0,
        total: 0,
        total_pages: 0
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    loading: true,
    downloadLoading: false,
    createLoading: false
});

/**
 *
 * Announcements reducer
 *
 */
function announcementsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case EXPORT_SET_ANNOUNCEMENT:
            return state.set( 'downloading', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case DOWNLOAD_LOADING:
            return state.set( 'downloadLoading', action.payload );
        case SET_ANNOUNCEMENT:
            return state.set( 'received', fromJS( action.payload ) );
        case SET_ANNOUNCEMENT_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default announcementsReducer;
