import { createSelector } from 'reselect';

/**
 * Direct selector to the announcements state domain
 */
const selectAnnouncementsDomain = () => ( state ) => state.get( 'announcements' );
/**
 * Default selector used by Announcements
 */

const makeSelectAnnouncements = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.toJS()
);

const makeSelectAnnouncement = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'received' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectDownloading = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'downloading' )
);

const makeSelectAnnouncementPagination = () => createSelector(
  selectAnnouncementsDomain(),
( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectCreateLoading = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'createLoading' )
);

const makeSelectTableLoading = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'downloadLoading' )
);
export {
  makeSelectAnnouncements,
  selectAnnouncementsDomain,
  makeSelectAnnouncement,
  makeSelectLoading,
  makeSelectDownloading,
  // makeSelectReply,
  makeSelectAnnouncementPagination,
  makeSelectNotification,
  makeSelectCreateLoading,
  makeSelectTableLoading
};
