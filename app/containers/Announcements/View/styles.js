import styled from 'styled-components';
export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    height: 100%;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    height: 100%;
    .container {
        height: 100%;
        .loader {
            height: calc(100vh - 100px);
        }

        .ReactTable {
            .rt-thead .rt-th.-cursor-pointer,
            .rt-thead .rt-td.-cursor-pointer {
                &:after {
                    position: relative;
                    top: 2px;
                }
            }

            .rt-thead .rt-resizable-header-content {
                padding-right: 5px;
                font-size: 14px;
            }

            .rt-td {
                font-size: 14px;
            }

            .rt-tr {
                cursor: pointer;

                .rt-expander-header,
                .rt-td:first-child {
                    display: none;
                }
            }

            .-even {
                background-color: #fafbfc;
            }

            .checkbox {
                border-color: #333;

                &:checked {
                    border-color: #333;
                }
            }

            .selected {
                position: relative;
                background-color: rgba(131, 210, 75, 0.15);
            }
        }
    }
    .content {
        .ReactTable {
            .rt-th {
                height: 63px;
            }
            .rt-table {
                font-size: 14px;
            }
            .rt-tbody {
                .rt-tr-group:nth-child(even) {
                    background-color: #fafbfc;
                }
                .rt-tr {
                    border-top: 0 !important;
                }
            }
        }
        .selectPage {
            p {
                text-align: end;
                background-color: #e5f6fc;
                margin: 0;
                padding: 12px 10px;
                font-size: 14px;
                font-weight: 600;
                a {
                    color: #00a5e5;
                    padding: 0;
                }
            }
        }
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 600;
                margin-bottom: 5px;
            }

            p {
                text-align: center;
                max-width: 800px;
                font-size: 14px;
                margin-bottom: 0px;
            }
        }

        .main {
            .btn {
                min-width: 140px;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
            justify-content: space-between;
            .title-Content {
                display: flex;
                align-items: center;
                p,
                .dropdown span,
                span {
                    font-size: 14px;
                }
            }
            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 700;
                font-size: 18px;
            }

            .search-wrapper {
                flex-grow: 1;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 30px;

                    input {
                        border: none;
                    }
                }

                p {
                    display: none;
                }
            }
        }
    }

    a {
        padding: 4px 10px;
        color: #00a5e5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }

    .no-license-error {
        color: #e13232;
    }
    .announcement_title {
        text-align: center;
        margin-top: 7rem;
        margin-bottom: 40px;

        h1 {
            font-weight: 700;
            font-size: 36px;
            margin-bottom: 10px;
        }

        p {
            font-size: 14px;
            margin-bottom: 25px;
        }

        button {
            font-size: 15px;
            padding: 7px 21px;
        }
    }
    .button_header {
        margin-bottom: 10px;

        > button {
            margin-right: 10px;
            font-size: 14px;
            padding: 0.5rem 1.5rem;
            color: #474747;
            background: unset;
        }
    }
    .outbox_section {
        display: flex;
        align-items: center;
        margin-bottom: 1rem;
        h5 {
            margin: 0;
            font-size: 18px;
            color: #474747;
            font-weight: 600;
        }

        P {
            margin: 0;
            font-size: 14px;
        }
        .search-wrapper {
            .search {
                width: 200px;
                border: 1px solid #333;
                border-radius: 30px;
                margin-left: 2rem;
            }

            .input-group,
            .form-control {
                height: 42px;
                background-color: transparent;
                overflow: hidden;
            }

            .input-group-addon {
                border-left: none;
                border: none;
                background: none;

                .isvg {
                    display: inline-block;

                    svg {
                        position: relative;
                        top: -1px;
                        width: 14px;
                        height: auto;
                    }
                }
            }
        }
        .showing_contain {
            flex-grow: 1;
            text-align: end;
            .showing_box {
                display: flex;
                justify-content: end;
                gap: 20px;
                align-items: center;
            }
        }
    }
    .details {
        padding: 10px 25px;
        font-size: 14px;
        background: #fafbfc;
        border: 1px solid #f0f4f6;

        .head {
            display: flex;
            justify-content: space-between;
        }

        .details_link {
            strong {
                color: #4e4e4e;
            }
            color: #00a5e5;
        }
    }
    .replysection {
        .replyinput {
            margin-top: 1rem;
        }
        .replybtn {
            text-align: end;
            margin-top: 1rem;
        }
    }
`;
export const ModalWrapper = styled.section`
    .location_section {
        label {
            span {
                color: #474747 !important;
                font-size: 14px !important;
            }
        }
        .Select-value {
            flex-flow: row-reverse;
            border-radius: 2rem;
            padding: 0.25rem 0.5rem;
            background: unset;
            color: #000;
            border-color: #ccc;
            .Select-value-icon {
                border-right: 0;
            }
        }

        .icon-search {
            top: 50px;
        }

        margin-bottom: 1rem;

        .Select-control {
            border-color: #474747 !important;
        }

    }
    .location_sectionadd {
        color: #eb7575;
        span {
            top: 50px;
            color: #eb7575 !important;
            font-size: 14px !important;
        }
        .Select-control {
            border-color: #eb7575 !important;
        }
        label {
            color: #eb7575 !important;
            font-size: 14px !important;
        }
        input {
            border-color: #eb7575;
        }
        textarea {
            border-color: #eb7575;
        }
        p {
            margin-top: 4px;
        }
    }
    .subject_contain {
        margin-bottom: 1rem;
        label {
            font-size: 14px !important;
            color: #474747 !important;
        }
    }
    .message_contain {
        margin-bottom: 1rem;
        label {
            font-size: 14px !important;
            color: #474747 !important;
        }
    }
    .toggleWrapper {
        display: flex;
        flex-direction: row;
        align-items: center;
        line-height: 1;
    }
    .enforce_switch {
        margin-top: 20px;
        .rc-switch {
            height: 8px;
            width: 32px;
            margin-right: 8px;
            &:after {
                background-color: #474747;
                width: 17px;
                height: 17px;
                top: -6px;
                left: 0px;
            }
        }
        .rc-switch-checked {
            border: 1px solid #ccc;
            background-color: #ccc;
            &:after {
                left: 16px;
                background-color: #83d24b;
            }
        }
    }
`;
export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        text-align: left;
    }
`;

export const StyledModalFooter = styled.div`
    display: flex;
    flex: 1;
    justify-content: flex-end;

    button {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 135px;
        padding: 8px 21px;
        font-size: 14px;
    }

    .cancelBtn {
        background-color: #fff;
    }
`;
