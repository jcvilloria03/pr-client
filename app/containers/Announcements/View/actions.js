import {
    DEFAULT_ACTION,
    GET_ANNOUNCEMENT,
    EXPORT_ANNOUNCEMENT,
    // GET_REPLY,
    CREATE_ANNOUNCEMENTS
} from './constants';
/**
 *
 * Announcements actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * payroll Request Type
 * @param {object} payload
 */
export function getAnnouncement( payload ) {
    return {
        type: GET_ANNOUNCEMENT,
        payload
    };
}

/**
 * payroll Request Type
 * @param {object} payload
 */
export function exportAnnouncement( payload ) {
    return {
        type: EXPORT_ANNOUNCEMENT,
        payload
    };
}

/**
 * Create announcements
 * @param {Array} payload
 * @returns {Object}
 */
export function createAnnouncements( payload ) {
    return {
        type: CREATE_ANNOUNCEMENTS,
        payload
    };
}
