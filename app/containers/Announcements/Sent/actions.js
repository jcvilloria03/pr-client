import {
    CREATE_ANNOUNCEMENTS,
    DEFAULT_ACTION, EXPORT_ANNOUNCEMENT, GET_SENT_DATA
} from './constants';

/**
 *
 * Announcements actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * announcement sent data
 * @param {object} payload
 */
export function getSentTableData( payload ) {
    return {
        type: GET_SENT_DATA,
        payload
    };
}

/**
 * announcement export
 * @param {object} payload
 */
export function exportAnnouncement( payload ) {
    return {
        type: EXPORT_ANNOUNCEMENT,
        payload
    };
}

/**
 * Create announcements
 * @param {Array} payload
 * @returns {Object}
 */
export function createAnnouncements( payload ) {
    return {
        type: CREATE_ANNOUNCEMENTS,
        payload
    };
}
