/* eslint-disable camelcase */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import moment from 'moment';
import { debounce } from 'lodash';

import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import Button from 'components/Button';
import Search from 'components/Search';
import AsyncTable from 'components/AsyncTable';
import SalDropdown from 'components/SalDropdown';
import { getIdsOfSelectedRows } from 'components/Table/helpers';
import Modal from 'components/Modal/index';
import Switch from 'components/Switch';
import MultiSelect from 'components/MultiSelect';
import Input from 'components/Input';

import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';
import { formatDate, formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import { ConfirmBodyWrapperStyle, ModalWrapper, PageWrapper } from './styles';

import { makeSelectAnnouncements, makeSelectCreateLoading, makeSelectLoading, makeSelectNotification, makeSelectPagination, makeSelectTableLoading } from './selectors';
import * as announcementsAction from './actions';
import { SELECT_ALL_EMPLOYEES_OPTIONS } from '../../Dashboard/constants';
import { browserHistory } from '../../../utils/BrowserHistory';
import FooterTablePaginationV2 from '../../../components/FooterTablePagination/FooterTablePaginationV2';
import { Spinner } from '../../../components/Spinner';
import { StyledModalFooter } from '../View/styles';

/**
 *
 * Announcements
 *
 */
export class Announcements extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes={
        AnnouncementsList: React.PropTypes.object,
        loading: React.PropTypes.bool,
        pagination: React.PropTypes.object,
        getSentTableData: React.PropTypes.func,
        createAnnouncements: React.PropTypes.func,
        exportAnnouncement: React.PropTypes.func,
        tableLoad: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    static defaultProps = {
        loading: { loading: true }
    }
    constructor( props ) {
        super( props );
        this.state = {
            sentAnnouncement: '',
            profile: null,
            role: 'sender',
            sortBy: 'created_at',
            sortOrder: 'desc',
            term: '',
            label: 'Showing 0-0 of 0 entries',
            showSelect: false,
            deleteLabel: '',
            subject: '',
            message: '',
            allow_response: false,
            read_receipt: false,
            inactiveUsers: [],
            toValue: [],
            toFlag: false,
            msgFlag: false,
            subjectFlag: false,
            isLoading: false
        };

        this.onSortingChange = this.onSortingChange.bind( this );
        this.createModal = null;
        this.inActiveModal = null;
    }

    componentWillMount() {
        const profile = auth.getUser();

        this.setState({ profile });
    }
    componentDidMount() {
        this.props.getSentTableData({
            page: 1,
            perPage: 10,
            role: this.state.role,
            sortBy: this.state.sortBy,
            sortOrder: this.state.sortOrder,
            term: this.state.term
        });
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps !== this.props.AnnouncementsList ) {
            const listData = nextProps.AnnouncementsList.sentDataList;
            this.setState({
                sentAnnouncement: listData,
                label: formatPaginationLabel({
                    page: nextProps.pagination.current_page - 1,
                    pageSize: nextProps.pagination.per_page,
                    dataLength: nextProps.pagination.total
                })
            });
        }
    }

    onSortingChange( column, additive ) {
        // clone current sorting sorting
        let newSorting = JSON.parse( JSON.stringify( this.announcement.state.sorting, ( key, value ) => {
            if ( typeof value === 'function' ) {
                return value.toString();
            }
            return value;
        }) );

        // check if already sorted by same column
        const existingIndex = newSorting.findIndex( ( d ) => d.id === column.id );

        let isDesc = false;
        if ( existingIndex > -1 ) {
            const existing = newSorting[ existingIndex ];
            if ( existing.desc ) {
                if ( additive ) {
                    newSorting.splice( existingIndex, 1 );
                } else {
                    existing.desc = false;
                    newSorting = [existing];
                }
            } else {
                existing.desc = true;
                isDesc = true;
                if ( !additive ) {
                    newSorting = [existing];
                }
            }
        } else if ( additive ) {
            newSorting.push({
                id: column.id,
                desc: false
            });
        } else {
            newSorting = [{
                id: column.id,
                desc: false
            }];
        }

        const orderDir = isDesc ? 'desc' : 'asc';

        // set new sorting states
        this.setState({
            sortBy: column.id,
            sortOrder: orderDir
        }, () => {
            // Fetch sorted data
            this.props.getSentTableData({
                page: 1,
                perPage: this.props.pagination.per_page,
                role: this.state.role,
                sortBy: this.state.sortBy,
                sortOrder: this.state.sortOrder,
                term: this.state.term
            });
            setTimeout( () => this.handleTableChanges(), 2000 );
        });

        this.announcement.setState({
            sorting: newSorting
        });
        this.announcement.tableComponent.setState({
            sorting: newSorting
        });
    }

    getDropdownItems() {
        return [
            {
                children: <div>Downloads</div>,
                onClick: this.handleExportAnnouncement
            }
        ];
    }

    getRecipients = ( announcement ) => {
        let recipients = announcement.recipient_groups || [];

        if ( this.state.profile.account_id ) {
            recipients = recipients.filter( ( recipient ) => recipient.rel_id !== this.state.profile.account_id );
        }

        const countOfRecipients = recipients.length;
        const name = recipients[ 0 ].name;

        if ( !countOfRecipients ) return '';
        return countOfRecipients === 1 ? name : `${name} and ${countOfRecipients - 1} ${countOfRecipients === 2 ? 'other' : 'others'}`;
    }

    getInactiveEmployees = () => {
        this.setState({ isLoading: true });

        const { subject, message, toValue } = this.state;
        if ( !toValue.length ) {
            this.setState({ toFlag: true });
        }
        if ( subject.length <= 0 ) {
            this.setState({ subjectFlag: true });
        }
        if ( message.length <= 0 ) {
            this.setState({ msgFlag: true });
        }
        if ( toValue.length && subject.length > 0 && message.length > 0 ) {
            Fetch( `/company/${this.state.profile.last_active_company_id}/inactive_employees`,
                { method: 'POST',
                    data: {
                        affected_employees: this.employees.state.value
                    }
                }).then( ({ data }) => {
                    if ( data.length ) {
                        this.setState({
                            inactiveUsers: data
                        });
                        this.inActiveModal.toggle();
                    } else {
                        this.props.createAnnouncements({
                            affected_employees: this.employees.state.value,
                            company_id: this.state.profile.last_active_company_id,
                            subject: this.state.subject,
                            message: this.state.message,
                            allow_response: this.state.allow_response,
                            read_receipt: this.state.read_receipt
                        });

                        this.reinitialize();
                        this.createModal.toggle();
                    }
                });
        }
        this.setState({ isLoading: false });
    }

    getViews( recipients = []) {
        const views = recipients.filter( ( recipient ) => recipient.seen ).length;
        return `${views} of ${recipients.length} views`;
    }

    handleSearch=async ( term ) => {
        if ( term !== this.state.term ) {
            this.setState({ term });
            this.props.getSentTableData({
                page: 1,
                perPage: 10,
                role: this.state.role,
                sortBy: this.state.sortBy,
                sortOrder: this.state.sortOrder,
                term
            });
            setTimeout( () => this.handleTableChanges(), 2000 );
        } else {
            this.setState({ term });
            this.props.getSentTableData({
                page: 1,
                perPage: 10,
                role: this.state.role,
                sortBy: this.state.sortBy,
                sortOrder: this.state.sortOrder,
                term: this.state.term
            });
            setTimeout( () => this.handleTableChanges(), 2000 );
        }
    }

    handleExportAnnouncement=() => {
        const Ids = getIdsOfSelectedRows( this.announcement );
        const payload = {
            announcements_ids: Ids,
            companyId: company.getLastActiveCompanyId(),
            download_all: false,
            role: this.state.role,
            sort_by: this.state.sortBy,
            sort_order: this.state.sortOrder,
            term: this.state.term };
        this.props.exportAnnouncement( payload );
    }

    handlePageChange = ( page ) => {
        this.props.getSentTableData({
            page,
            perPage: this.props.pagination.per_page,
            role: this.state.role,
            sortBy: this.state.sortBy,
            sortOrder: this.state.sortOrder,
            term: this.state.term
        });
        this.announcement.setState({ selectedRows: []});
        this.setState({ showSelect: false }, );
        setTimeout(
            () => this.handleTableChanges(),
            2000,
        );
    }

    handlePageSizeChange = ( pageSize ) => {
        if ( !pageSize ) {
            return;
        }
        const page = 1;
        this.props.getSentTableData({
            page,
            perPage: pageSize,
            role: this.state.role,
            sortBy: this.state.sortBy,
            sortOrder: this.state.sortOrder,
            term: this.state.term
        });
        this.announcement.setState({ selectedRows: []});
        this.setState({ showSelect: false });
        setTimeout( () => this.handleTableChanges(), 2000 );
    }

    handleTableChanges=( tableProps = this.announcement.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    loadEmployeesList = ( keyword, callback ) => {
        Fetch( `/company/${this.state.profile.last_active_company_id}/affected_employees/search?term=${keyword}&limit=10&without_users=yes`, { method: 'GET' })
          .then( ( result ) => {
              const list = result.map( ({ id, name, type }) => ({
                  id,
                  type,
                  uid: `${type}${id}`,
                  value: id,
                  label: name,
                  field: type,
                  disabled: false
              }) );
              callback( null, { options: SELECT_ALL_EMPLOYEES_OPTIONS.concat( list ) });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    reinitialize=() => this.setState({
        subject: '',
        message: '',
        toValue: [],
        allow_response: false,
        read_receipt: false,
        toFlag: false,
        msgFlag: false,
        subjectFlag: false })

    TableColumns() {
        return [
            {
                header: 'Recipient',
                accessor: 'recipient',
                minWidth: 250,
                sortable: true,
                style: { height: '4.5rem' },
                render: ({ row }) => ( <div>{row.recipient}</div> )
            },
            {
                header: 'Subject',
                accessor: 'subject',
                minWidth: 200,
                sortable: true,
                render: ({ row }) => ( <div>{row.subject}</div> )
            },
            {
                header: 'Date Sent',
                accessor: 'date_sent',
                minWidth: 200,
                sortable: false,
                render: ({ row }) => ( <div>{row.date_sent}</div> )
            },
            {
                header: 'Views',
                accessor: 'views',
                minWidth: 200,
                sortable: false,
                render: ({ row }) => ( <div>{row.views}</div> )
            },
            {
                header: 'Status',
                accessor: 'status',
                minWidth: 300,
                sortable: false,
                render: ({ row }) => ( <div>{row.status}</div> )
            }
        ];
    }

    render() {
        const createAnnouncementBtns = [
            {
                id: 'buttonCancel',
                type: 'grey',
                label: 'Cancel',
                onClick: () => {
                    this.reinitialize();
                    this.createModal.toggle();
                },
                className: 'cancelBtn'
            },
            {
                id: 'buttonProceed',
                type: 'action',
                label: loading ? <Loader /> : <span>Send</span>,
                onClick: () => {
                    this.getInactiveEmployees();
                },
                className: 'sendBtn'
            }
        ];
        const TableData = this.state.sentAnnouncement ? this.state.sentAnnouncement.map( ( data ) => ({
            ...data,
            recipient: this.getRecipients( data ),
            subject: data.subject,
            date_sent: formatDate( data.created_at, DATE_FORMATS.DISPLAY ),
            views: this.getViews( data.recipients ),
            status: 'Sent'
        }) ) : [];
        const { loading, tableLoad, createAnnouncements } = this.props;
        const {
            profile,
            subject,
            message,
            inactiveUsers,
            allow_response,
            read_receipt
          } = this.state;

        return (
            <div>
                <Helmet
                    title="Announcements"
                    meta={ [
                        { name: 'description', content: 'Description of Announcements' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <PageWrapper>
                    <Container>
                        {( this.state.isLoading || this.props.loading ) && <Spinner />}
                        <div>
                            <div className="announcement_title">
                                <h1>Announcements </h1>
                                <p>Easily communicate company policies and government proclamations through this page.</p>
                                <Button
                                    id="creatBtn"
                                    type="action"
                                    size="large"
                                    label="Create Announcement"
                                    onClick={ () => { this.createModal.toggle(); } }
                                />
                            </div>
                            <div className="button_header">
                                <Button
                                    className="inboxbut"
                                    id="inboxBtn"
                                    type="grey"
                                    size="default"
                                    label="Inbox"
                                    onClick={ () => { browserHistory.push( '/time/company/announcements/received/', true ); } }
                                />
                                <Button
                                    className="Outboxbut"
                                    id="inboxBtn"
                                    type="neutral"
                                    size="default"
                                    label="Outbox"
                                />
                            </div>
                            <div className="outbox_section">
                                <h5>Announcement List: Outbox</h5>
                                <Search
                                    ref={ ( ref ) => { this.search = ref; } }
                                    handleSearch={ debounce( this.handleSearch, 1000 ) }
                                />
                                <div className="showing_contain">
                                    {!this.state.showSelect ?
                                        <p>{this.state.label}</p> :
                                ( <div className="showing_box"> <p>{this.state.deleteLabel}</p> <SalDropdown dropdownItems={ this.getDropdownItems() } /></div> )}
                                </div>
                            </div>
                            <AsyncTable
                                columns={ this.TableColumns() }
                                data={ TableData }
                                selectable
                                pagination={ false }
                                manual
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.announcement = ref; } }
                                loading={ tableLoad }
                                noDataText="Loading"
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    this.setState({
                                        showSelect: selectionLength > 0,
                                        deleteLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                                sizeOptions={ [ 10, 25, 50, 100 ] }
                                onPageChange={ this.handlePageChange }
                                onPageSizeChange={ this.handlePageSizeChange }
                                onSortingChange={ this.onSortingChange }
                                tablePage={ this.props.pagination.current_page - 1 || 0 }
                                pageSize={ this.props.pagination.per_page || 10 }
                                pages={ this.props.pagination.total_pages }
                                SubComponent={ ( data ) => (
                                    <section className="details">
                                        <div className="head">
                                            <p><strong>Subject:</strong> {data.rowValues.subject}</p>
                                            <p><strong>Date sent:</strong> {data.rowValues.date_sent}</p>
                                        </div>
                                        <p><strong>From:</strong> {data.row.sender.full_name}</p>
                                        <p className="details_link"><strong>To:</strong> {this.getRecipients( data.row )}</p>
                                        <div className="details-message-container">
                                            <p className="details-message">{data.row.message}</p>
                                            { data.row.views && data.row.views !== 'N/A' && (
                                            <div className="details-views-container">
                                                <p className="details-views">{ this.getViews( data.row.recipients ) }</p>
                                            </div>
                                                ) }
                                        </div>
                                        <div className="details-replies">
                                            {data.row.replies.length ? data.row.replies.map( ( rData ) => (
                                                <div>
                                                    <p className="details-replies-label"><strong>Replies</strong></p>
                                                    <div className="head">
                                                        <p> {rData.sender.full_name}</p>
                                                        <p>Replied on {moment( rData.created_at && rData.created_at ).format( 'MMMM DD, YYYY - HH:mm' )}</p>
                                                    </div>
                                                    <p>{rData.message && rData.message}</p>
                                                </div> ) ) : ''}
                                        </div>
                                    </section>
                                      ) }
                            />
                            { this.props.pagination.total > 0 && (
                                <FooterTablePaginationV2
                                    page={ this.props.pagination.current_page }
                                    pageSize={ this.props.pagination.per_page }
                                    pagination={ {
                                        ...this.props.pagination,
                                        last_page: this.props.pagination.total_pages
                                    } }
                                    onPageChange={ this.handlePageChange }
                                    onPageSizeChange={ this.onPageSizeChange }
                                    paginationLabel={ this.state.label }
                                    fluid
                                />
                            ) }
                        </div>
                    </Container>
                    <Modal
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    Listed Below are Inactive Recipients:

                                    {inactiveUsers.map( ( user, index ) => <div key={ index }>
                                        <span >{user.full_name}</span>
                                    </div> )}

                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        buttons={ [
                            {
                                id: 'buttonCancel',
                                type: 'action',
                                label: 'Edit',
                                onClick: () => {
                                    this.inActiveModal.toggle();
                                }
                            },
                            {
                                id: 'buttonProceed',
                                type: 'grey',
                                label: 'Send Anyway',
                                onClick: () => {
                                    this.createModal.toggle();
                                    this.reinitialize();
                                    this.inActiveModal.toggle();
                                    createAnnouncements({
                                        affected_employees: this.employees.state.value,
                                        company_id: profile.last_active_company_id,
                                        subject,
                                        message,
                                        allow_response,
                                        read_receipt
                                    });
                                }
                            }
                        ] }
                        ref={ ( ref ) => { this.inActiveModal = ref; } }
                        title="Inactive recipient"
                        size="mb"
                        showClose={ false }
                        backdrop="static"
                    />
                    <Modal
                        title="Create Announcement"
                        size="lg"
                        body={
                            <ModalWrapper>
                                <div className={ this.state.toFlag ? 'location_sectionadd' : 'location_section' }>
                                    <MultiSelect
                                        id="announcement"
                                        async
                                        label={ <span>To:</span> }
                                        loadOptions={ this.loadEmployeesList }
                                        ref={ ( ref ) => { this.employees = ref; } }
                                        onChange={ ( value ) => ( this.setState({ toValue: value, toFlag: !value.length }) ) }
                                        placeholder="Enter location, department name, position, employee name"
                                        hasSearchIcon
                                    />{this.state.toFlag ? <p>Please add recipients</p> : ''}
                                </div>
                                <div className={ this.state.subjectFlag ? 'location_sectionadd' : 'subject_contain' }>
                                    <Input
                                        className=""
                                        label="Subject:"
                                        id="subject"
                                        name="subject"
                                        onChange={ ( data ) => {
                                            this.setState({ subject: data, subjectFlag: data.length <= 0 });
                                        } }
                                        ref={ ( ref ) => { this.subject = ref; } }
                                    />{this.state.subjectFlag ? <p>Please enter a subject</p> : ''}
                                </div>
                                <div className={ this.state.msgFlag ? 'location_sectionadd' : 'message_contain' }>
                                    <Input
                                        className=""
                                        id="message"
                                        label="Message:"
                                        type="textarea"
                                        onChange={ ( data ) => {
                                            this.setState({ message: data, msgFlag: data.length <= 0 });
                                        } }
                                        ref={ ( ref ) => { this.message = ref; } }
                                    />{this.state.msgFlag ? <p>Please enter a message</p> : ''}
                                </div>
                                <div className="toggleWrapper enforce_switch">
                                    <Switch
                                        checked={ allow_response }
                                        onChange={ ( data ) => {
                                            this.setState({ allow_response: data });
                                        } }
                                    />
                              &nbsp; Allow response from recipients
                            </div>

                                <div className="toggleWrapper enforce_switch">
                                    <Switch
                                        checked={ read_receipt }
                                        onChange={ ( data ) => {
                                            this.setState({ read_receipt: data });
                                        } }
                                    />
                              &nbsp; Show Read Receipt
                            </div>
                            </ModalWrapper>
                        }
                        ref={ ( ref ) => { this.createModal = ref; } }
                        backdrop="static"
                        footer={
                            <StyledModalFooter>
                                {createAnnouncementBtns.map( ( btn ) => (
                                    <Button
                                        key={ btn.id }
                                        id={ btn.id }
                                        type={ btn.type }
                                        size={ btn.size }
                                        label={ btn.label }
                                        onClick={ btn.onClick }
                                        className={ btn.className }
                                    />
                                ) )}
                            </StyledModalFooter>
                        }
                    />
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    AnnouncementsList: makeSelectAnnouncements(),
    pagination: makeSelectPagination(),
    notification: makeSelectNotification(),
    tableLoad: makeSelectTableLoading(),
    createLoad: makeSelectCreateLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        announcementsAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Announcements );
