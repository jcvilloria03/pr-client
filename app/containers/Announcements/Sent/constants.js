/*
 *
 * Announcements constants
 *
 */
const nameSpace = 'app/Announcements/Sent';
export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const LOADING = `${nameSpace}/LOADING`;

export const GET_SENT_DATA = `${nameSpace}/GET_SENT_DATA`;
export const SET_SENT_DATA = `${nameSpace}/SET_SENT_DATA`;
export const SET_PAGGINATION = `${nameSpace}/SET_PAGGINATION`;

export const DOWNLOAD_LOADING = `${nameSpace}/DOWNLOAD_LOADING`;
export const EXPORT_ANNOUNCEMENT = `${nameSpace}/EXPORT_ANNOUNCEMENT`;
export const EXPORT_SET_ANNOUNCEMENT = `${nameSpace}/EXPORT_SET_ANNOUNCEMENT`;

export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const SET_NOTIFICATION = `${nameSpace}/SET_NOTIFICATION`;

export const CREATE_ANNOUNCEMENTS = `${nameSpace}/CREATE_ANNOUNCEMENTS`;
export const CREATE_ANNOUNCEMENTS_LOADING = `${nameSpace}/CREATE_ANNOUNCEMENTS_LOADING`;
