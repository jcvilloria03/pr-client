import { createSelector } from 'reselect';

/**
 * Direct selector to the announcements state domain
 */
const selectAnnouncementsDomain = () => ( state ) => state.get( 'announcementsSent' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'loading' )
);

/**
 * Other specific selectors
 */
const makeSelectPagination = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

/**
 * Other specific selectors
 */
const makeSelectExportCSV = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'exportAnnouncement' )
);

/**
 * Other specific selectors
 */
const makeSelectNotification = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

/**
 * Other specific selectors
 */
const makeSelectTableLoading = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'downloadLoading' )
);

/**
 * Other specific selectors
 */
const makeSelectCreateLoading = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.get( 'createLoading' )
);

/**
 * Default selector used by Announcements
 */
const makeSelectAnnouncements = () => createSelector(
  selectAnnouncementsDomain(),
  ( substate ) => substate.toJS()
);

export {
  selectAnnouncementsDomain,
  makeSelectTableLoading,
  makeSelectCreateLoading,
  makeSelectNotification,
  makeSelectLoading,
  makeSelectAnnouncements,
  makeSelectExportCSV,
  makeSelectPagination
};
