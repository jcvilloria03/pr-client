import { fromJS } from 'immutable';
import {
    CREATE_ANNOUNCEMENTS_LOADING,
    DEFAULT_ACTION, DOWNLOAD_LOADING, EXPORT_SET_ANNOUNCEMENT, LOADING, SET_NOTIFICATION, SET_PAGGINATION, SET_SENT_DATA
} from './constants';

const initialState = fromJS({
    loading: true,
    sentDataList: '',
    exportAnnouncement: '',
    downloadLoading: false,
    createLoading: false,
    pagination: {
        count: 0,
        current_page: 0,
        links: [],
        per_page: 0,
        total: 0,
        total_pages: 0
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Announcements reducer
 *
 */
function announcementsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_SENT_DATA:
            return state.set( 'sentDataList', action.payload );
        case SET_PAGGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case EXPORT_SET_ANNOUNCEMENT:
            return state.set( 'exportAnnouncement', action.payload );
        case DOWNLOAD_LOADING:
            return state.set( 'downloadLoading', action.payload );
        case CREATE_ANNOUNCEMENTS_LOADING:
            return state.set( 'createLoading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default announcementsReducer;
