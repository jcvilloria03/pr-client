/* eslint-disable prefer-const */
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery } from 'redux-saga';
import { get } from 'lodash';
import { call, cancel, put, take } from 'redux-saga/effects';
import fileDownload from 'js-file-download';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { CREATE_ANNOUNCEMENTS, DOWNLOAD_LOADING, EXPORT_ANNOUNCEMENT, GET_SENT_DATA, LOADING, NOTIFICATION, SET_NOTIFICATION, SET_PAGGINATION, SET_SENT_DATA } from './constants';

/**
 * GET SENT DATA for Table
 */
export function* getSentTableData({ payload }) {
    try {
        yield put({ type: DOWNLOAD_LOADING, payload: true });
        const { page, perPage, role, sortBy, sortOrder, term } = payload;
        const companyId = company.getLastActiveCompanyId();
        const URLsuffix = `announcements?per_page=${perPage}&page=${page}&role=${role}&sort_by=${sortBy}&sort_order=${sortOrder}&term=${term}`;
        const response = yield call( Fetch, `/company/${companyId}/${URLsuffix}`, { method: 'GET' });
        let { data, ...other } = response;

        const handleViewsCount = ( recipients ) => {
            const viewCount = recipients.length;
            const viewedCount = getCountOfRecipientsSeenAnnouncement( recipients );

            return `${viewedCount} of ${viewCount} `;
        };

        const getCountOfRecipientsSeenAnnouncement = ( recipients ) => {
            if ( recipients || !recipients.length ) {
                return 0;
            }

            return filterRecipientsThatHaveSeenAnnouncement( recipients ).length;
        };

        const filterRecipientsThatHaveSeenAnnouncement = ( recipients ) => (
                recipients.filter( ( recipient ) => recipient && recipient.seen ) || []
            );

        data = data.map( ( singleData ) => ({
            ...singleData,
            views: singleData.read_receipt ? `${handleViewsCount( singleData.recipients )} views` : 'N/A'
        }) );
        yield put({ type: SET_PAGGINATION, payload: other.meta.pagination });
        yield put({ type: SET_SENT_DATA, payload: data });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
        yield put({ type: LOADING, payload: false });
        yield put({ type: DOWNLOAD_LOADING, payload: false });
    } finally {
        yield put({ type: DOWNLOAD_LOADING, payload: false });
        yield put({ type: LOADING, payload: false });
    }
}

/**
 * GET SENT DATA for Table
 */
export function* exportAnnouncement({ payload }) {
    try {
        yield put({ type: DOWNLOAD_LOADING, payload: true });
        const { companyId } = payload;

        const response = yield call( Fetch, `/company/${companyId}/announcements/download`, { method: 'POST', data: payload });
        fileDownload( response, 'Sent-Announcement.csv' );
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
        yield put({ type: DOWNLOAD_LOADING, payload: false });
    } finally {
        yield put({ type: DOWNLOAD_LOADING, payload: false });
    }
}

/**
 * Create Announcements
 */
export function* createAnnouncements({ payload }) {
    try {
        yield put({ type: LOADING, payload: true });

        yield call( Fetch, '/announcement', { method: 'POST', data: payload });

        yield call( getSentTableData, { payload: {
            perPage: 10,
            role: 'sender',
            page: 1,
            sortBy: 'created_at',
            sortOrder: 'desc',
            term: ''
        }});

        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Announcement successfully created',
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyError, error );
        yield put({ type: LOADING, payload: false });
    } finally {
        yield put({ type: LOADING, payload: false });
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watcherForSentTableData() {
    const watcher = yield takeEvery( GET_SENT_DATA, getSentTableData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for CREATE_ANNOUNCEMENTS
 */
export function* watchForCreateAnnouncements() {
    const watcher = yield takeEvery( CREATE_ANNOUNCEMENTS, createAnnouncements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watcherForExportCSV() {
    const watcher = yield takeEvery( EXPORT_ANNOUNCEMENT, exportAnnouncement );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchNotify,
    watcherForSentTableData,
    watcherForExportCSV,
    watchForCreateAnnouncements
];
