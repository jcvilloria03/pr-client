const namespace = 'app/Disbursements/View';

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const GET_DISBURSEMENT_METHODS = `${namespace}/GET_DISBURSEMENT_METHODS`;
export const SET_DISBURSEMENT_METHODS = `${namespace}/SET_DISBURSEMENT_METHODS`;
export const DELETE_DISBURSEMENT_METHODS = `${namespace}/DELETE_DISBURSEMENT_METHODS`;
