import { createSelector } from 'reselect';

/**
 * Direct selector to the Disbursements state domain
 */
const selectViewDisbursementsDomain = () => ( state ) => state.get( 'disbursements' );

const makeSelectLoading = () => createSelector(
    selectViewDisbursementsDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectDisbursementMethods = () => createSelector(
    selectViewDisbursementsDomain(),
    ( substate ) => substate.get( 'disbursementMethods' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewDisbursementsDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectDisbursementMethods,
    makeSelectNotification
};
