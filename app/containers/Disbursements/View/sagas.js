import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import get from 'lodash/get';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import {
    SET_LOADING,
    GET_DISBURSEMENT_METHODS,
    SET_DISBURSEMENT_METHODS,
    NOTIFICATION,
    SET_NOTIFICATION,
    DELETE_DISBURSEMENT_METHODS
} from './constants';

/**
 * Get Disbursement Methods of current company
 */
export function* getDisbursementMethods() {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const { data } = yield call( Fetch, `/company/${companyId}/banks` );

        yield put({
            type: SET_DISBURSEMENT_METHODS,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: get( error, 'response.statusText', 'Error' ),
            message: get( error, 'response.data.message', error.statusText ),
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Delete Disbursement Methods
 */
export function* deleteDisbursementMethods({ payload }) {
    const { ids: data } = payload;
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        yield call( Fetch, `/company/${companyId}/bank`, {
            method: 'DELETE',
            data: { data }
        });

        yield [
            call( notifyUser, {
                show: true,
                title: 'Success',
                message: 'Disbursement methods deleted',
                type: 'success'
            }),
            call( getDisbursementMethods )
        ];
    } catch ( error ) {
        yield put({
            type: SET_LOADING,
            payload: false
        });
        yield call( notifyUser, {
            show: true,
            title: get( error, 'response.statusText', 'Error' ),
            message: get( error, 'response.data.message', error.statusText ),
            type: 'error'
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: '',
        message: '',
        show: false,
        type: payload.type
    };
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getDisbursementMethods );
}

/**
 * Watcher for GET Disbursement Methods
 */
export function* watchForGetDisbursementMethods() {
    const watcher = yield takeEvery( GET_DISBURSEMENT_METHODS, getDisbursementMethods );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE Disbursement Methods
 */
export function* watchForDeleteDisbursementMethods() {
    const watcher = yield takeEvery( DELETE_DISBURSEMENT_METHODS, deleteDisbursementMethods );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetDisbursementMethods,
    watchForDeleteDisbursementMethods,
    watchForNotifyUser,
    watchForReinitializePage
];
