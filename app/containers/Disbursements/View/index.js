import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import get from 'lodash/get';
import toLower from 'lodash/toLower';
import isEqual from 'lodash/isEqual';

import { subscriptionService } from 'utils/SubscriptionService';
import {
    formatPaginationLabel,
    formatDeleteLabel
} from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { isAuthorized } from 'utils/Authorization';

import SnackBar from 'components/SnackBar';
import SalConfirm from 'components/SalConfirm';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Search from 'components/Search';
import Table from 'components/Table';
import SalDropdown from 'components/SalDropdown';
import { H2, H3, H5 } from 'components/Typography';
import { isAnyRowSelected } from 'components/Table/helpers';

import * as disbursementsActions from './actions';
import {
    makeSelectLoading,
    makeSelectDisbursementMethods,
    makeSelectNotification
} from './selectors';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from '../styles';

/**
 * View Disbursements Component
 */
export class ViewDisbursements extends React.PureComponent {
    static propTypes = {
        resetStore: React.PropTypes.func,
        getDisbursementMethods: React.PropTypes.func,
        deleteDisbursementMethods: React.PropTypes.func,
        disbursementMethods: React.PropTypes.array,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    };

    static defaultProps = {
        loading: true,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            delete_label: '',
            displayed_data: props.disbursementMethods,
            permission: {
                view: false,
                create: false,
                delete: false
            },
            show_modal: false,
            dropdown_items: [
                {
                    label: 'Delete',
                    children: <div>Delete</div>,
                    onClick: this.onClickDelete
                }
            ]
        };

        this.search = null;
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.disbursement',
            'create.disbursement',
            'delete.disbursement'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.disbursement' ];

            if ( authorized ) {
                this.setState({
                    permission: {
                        view: authorization[ 'view.disbursement' ],
                        create: authorization[ 'create.disbursement' ],
                        delete: authorization[ 'delete.disbursement' ]
                    }
                });
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.getDisbursementMethods();
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.disbursementMethods, this.props.disbursementMethods ) ) {
            this.handleSearch( nextProps.disbursementMethods );
        }
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    onClickDelete = () => {
        this.setState({ show_modal: false }, () => {
            this.setState({ show_modal: true });
        });
    }

    /**
     * Get ids and type for delete
     * @param {Object} table
     * @returns {Array}
     */
    getIdAndTypeOfSelectedRows( table ) {
        const idAndTypeArray = [];

        table.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                idAndTypeArray.push({
                    id: table.props.data[ index ].id,
                    type: table.props.data[ index ].type
                });
            }
        });

        return idAndTypeArray;
    }

    getTableColumns() {
        return [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'type',
                accessor: 'type',
                show: false
            },
            {
                header: 'Account Type',
                accessor: 'account_type',
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.account_type}
                    </div>
                )
            },
            {
                header: 'Account Details',
                accessor: 'account_details',
                minWidth: 260,
                render: ({ row }) => (
                    <div>
                        {row.account_details}
                    </div>
                )
            },
            {
                header: 'Branch',
                accessor: 'branch_name',
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.branch_name}
                    </div>
                )
            },
            {
                header: 'Account Number',
                accessor: 'account_number',
                minWidth: 260,
                render: ({ row }) => (
                    <div>
                        {row.account_number}
                    </div>
                )
            },
            {
                header: 'Company Code',
                accessor: 'company_code',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.company_code}
                    </div>
                )
            }
        ];
    }

    handleTableChanges = ( tableProps = this.disbursementsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            delete_label: formatDeleteLabel()
        });
    }

    handleSearch = ( disbursementMethods = this.props.disbursementMethods ) => {
        let dataToDisplay = disbursementMethods;
        const searchQuery = toLower( get( this.search, 'searchInput.state.value', '' ) );

        if ( searchQuery ) {
            dataToDisplay = dataToDisplay.filter( ( disbursementMethod ) => (
                Object.keys( disbursementMethod ).some( ( key ) => toLower( disbursementMethod[ key ]).indexOf( searchQuery ) >= 0 )
            ) );
        }

        this.setState({ displayed_data: dataToDisplay }, () => {
            this.handleTableChanges();
        });
    }

    deleteDisbursementMethods = () => {
        const selectedRows = this.getIdAndTypeOfSelectedRows( this.disbursementsTable );
        if ( selectedRows.length > 0 ) {
            this.props.deleteDisbursementMethods( selectedRows );
        }
        this.setState({ show_modal: false });
    }

    render() {
        const {
            displayed_data: displayedData,
            show_modal: showModal,
            permission,
            delete_label: deleteLabel,
            label,
            dropdown_items: dropdownItems
        } = this.state;

        const {
            notification,
            loading
        } = this.props;

        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA: this.props.products && subscriptionService.isSubscribedToTA( this.props.products )
        });

        const tableColumns = this.getTableColumns();

        return (
            <div>
                <Helmet
                    title="View Disbursements"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SnackBar
                    offset={ { top: 70 } }
                    message={ notification.message }
                    title={ notification.title }
                    show={ notification.show }
                    type={ notification.type }
                    delay={ 5000 }
                    ref={ ( ref ) => { this.notification = ref; } }
                />
                <SalConfirm
                    onConfirm={ this.deleteDisbursementMethods }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Are you sure you want to delete this disbursement method?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Disbursement Method"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ showModal }
                />
                <Sidebar items={ sidebarLinks } />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Payment Methods.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Company Payment Methods</H3>
                                <p>Add your company payment methods here. After payroll is run, salaries can be disbursed either via bank transfer or SALPay. Learn more about disbursement or SALPay.</p>
                                <div style={ { textAlign: 'center' } } >
                                    <div className="main">
                                        <Button
                                            className={ permission.create ? '' : 'hide' }
                                            label="Add Company Payment Method"
                                            size="large"
                                            type="action"
                                            to="/disbursements/add"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="title">
                                <H5>Company Payment Method List</H5>
                                <div className="search-wrapper">
                                    <Search
                                        ref={ ( ref ) => { this.search = ref; } }
                                        handleSearch={ () => this.handleSearch() }
                                    />
                                </div>
                                <span>
                                    { isAnyRowSelected( this.disbursementsTable ) ? deleteLabel : label }
                                </span>
                                <span className="sl-u-gap-left--sm">
                                    { isAnyRowSelected( this.disbursementsTable ) && (
                                        <SalDropdown dropdownItems={ dropdownItems } /> )
                                    }
                                </span>
                            </div>
                            <Table
                                data={ displayedData }
                                columns={ tableColumns }
                                pagination
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.disbursementsTable = ref; } }
                                selectable
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    this.setState({ delete_label: formatDeleteLabel( selectionLength ) });
                                } }
                            />
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    disbursementMethods: makeSelectDisbursementMethods(),
    notification: makeSelectNotification()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        disbursementsActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ViewDisbursements );
