import { RESET_STORE } from 'containers/App/constants';

import {
    GET_DISBURSEMENT_METHODS,
    NOTIFICATION,
    DELETE_DISBURSEMENT_METHODS
} from './constants';

/**
 * Fetch list of Disbursement Methods
 */
export function getDisbursementMethods() {
    return {
        type: GET_DISBURSEMENT_METHODS
    };
}

/**
 * Delete Disbursement Methods
 * @param {Array} ids - IDs of disbursement methods to delete
 */
export function deleteDisbursementMethods( ids ) {
    return {
        type: DELETE_DISBURSEMENT_METHODS,
        payload: {
            ids
        }
    };
}

/**
 * Display a notification to a user
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
