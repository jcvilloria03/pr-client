import { fromJS } from 'immutable';
import capitalize from 'lodash/capitalize';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_DISBURSEMENT_METHODS,
    SET_NOTIFICATION
} from './constants';

const initialState = fromJS({
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    disbursementMethods: []
});

/**
 * Processes disbursement methods for dispay
 *
 * @param {Array} data - Company's banks
 * @returns {Array}
 */
function processDisbursementMethods( data ) {
    return data.map( ( disbursementMethod ) => {
        const {
            id,
            type,
            attributes: {
                bank,
                bankBranch,
                accountNumber,
                accountType,
                companyCode
            }
        } = disbursementMethod;

        return {
            id,
            type,
            account_type: 'Bank Account',
            account_details: bank.name,
            branch_name: bankBranch,
            account_number: `${accountNumber} (${capitalize( accountType )})`,
            company_code: companyCode,
            noselect: bank.name === 'SALPAY'
        };
    });
}

/**
 * Disbursements reducer
 * @param {Object} state
 * @param {Object} action
 */
function disbursementsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_DISBURSEMENT_METHODS:
            return state.set( 'disbursementMethods', fromJS( processDisbursementMethods( action.payload ) ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default disbursementsReducer;
