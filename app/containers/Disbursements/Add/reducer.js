import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_BANK_LIST,
    SET_COMPANY_BANK_LIST,
    SET_NOTIFICATION,
    SET_SUBMITTING
} from './constants';

const { SALPAY_BANK_ID } = process.env;

const initialState = fromJS({
    loading: false,
    submitting: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    bankList: []
});

/**
 * Transfroms response from API to cater `Select` component options format
 * @param {Array} data - List of banks
 */
function prepareBankList( data ) {
    return data.map( ( bank ) => ({
        group: bank.attributes.group,
        label: bank.attributes.name,
        value: bank.id
    }) );
}

/**
 * Transfroms response from API to cater `Select` component options format
 * @param {Array} data - List of Company's banks
 */
function prepareCompanyBankList( data ) {
    return data.reduce( ( banks, _bank ) => {
        const {
            attributes: {
                bank,
                bankBranch
            }
        } = _bank;

        if ( bank.id === parseInt( SALPAY_BANK_ID, 10 ) ) {
            return banks;
        }

        return banks.concat({
            label: bank.name,
            value: bank.id,
            branch: bankBranch
        });
    }, []);
}

/**
 * Add Disbursement reducer
 * @param {Object} state
 * @param {Object} action
 */
function addDisbursementReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_BANK_LIST:
            return state.set( 'bankList', fromJS( prepareBankList( action.payload ) ) );
        case SET_COMPANY_BANK_LIST:
            return state.set( 'bankList', fromJS( prepareCompanyBankList( action.payload ) ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_SUBMITTING:
            return state.set( 'submitting', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addDisbursementReducer;
