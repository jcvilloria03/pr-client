import { createSelector } from 'reselect';

/**
 * Direct selector to the Add Disbursement state domain
 */
const selectAddisbursementDomain = () => ( state ) => state.get( 'addDisbursement' );

const makeSelectLoading = () => createSelector(
    selectAddisbursementDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectBankList = () => createSelector(
    selectAddisbursementDomain(),
    ( substate ) => substate.get( 'bankList' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectAddisbursementDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectSubmitting = () => createSelector(
    selectAddisbursementDomain(),
    ( substate ) => substate.get( 'submitting' )
);

export {
    makeSelectLoading,
    makeSelectBankList,
    makeSelectNotification,
    makeSelectSubmitting
};
