import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import isObject from 'lodash/isObject';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';

import SnackBar from 'components/SnackBar';
import SalConfirm from 'components/SalConfirm';
import Sidebar from 'components/Sidebar';
import A from 'components/A';
import Select from 'components/Select';
import Input from 'components/Input';
import Loader from 'components/Loader';
import Button from 'components/Button';
import { H3, H4 } from 'components/Typography';

import { stripNonAlphaNum, stripNonNumeric, stripNonAlphaNumHypenUnderscore } from 'utils/functions';
import * as addDisbursementActions from './actions';
import {
    makeSelectLoading,
    makeSelectBankList,
    makeSelectNotification,
    makeSelectSubmitting
} from './selectors';
import { OPTIONS } from './constants';

import {
    PageWrapper,
    NavWrapper,
    ConfirmBodyWrapperStyle
} from '../styles';

/**
 * Add Disbursement Component
 */
export class AddDisbursement extends React.PureComponent {
    static propTypes = {
        addBankAccount: React.PropTypes.func,
        addEmployeeBankAccount: React.PropTypes.func,
        submitting: React.PropTypes.bool,
        getBankList: React.PropTypes.func,
        getCompanyBankList: React.PropTypes.func,
        bankList: React.PropTypes.array,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    };

    static defaultProps = {
        loading: true,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            form: {
                bank: '',
                bank_group: '',
                bank_branch: '',
                account_type: '',
                account_number: '',
                account_source: '',
                account_destination: '',
                company_code: ''
            },
            show_modal: false,
            employee: null
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.disbursement'], ( authorized ) => { !authorized && browserHistory.replace( '/unauthorized' ); });

        /**
         * Checks previous route to prevent unexpected loading
         * of employee details when using multiple tabs and
         * accessing disbursements in company settings
         */
        if ( this.props.previousRoute.path !== '/employee/:id' ) {
            localStorage.removeItem( 'prepopulatedEmployeeForAddPaymentMethod' );
        }
    }

    componentDidMount() {
        /**
         * Checks local storage for employee details
         * then fetches corresponding bank list
         */
        const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddPaymentMethod' ) );
        if ( prepopulatedEmployee ) {
            // eslint-disable-next-line react/no-did-mount-set-state
            this.setState({ employee: prepopulatedEmployee });
            this.props.getCompanyBankList( prepopulatedEmployee.company_id );
        } else {
            this.props.getBankList();
        }
    }

    componentWillUnmount() {
        localStorage.removeItem( 'prepopulatedEmployeeForAddPaymentMethod' );
    }

    /**
     * Updates the state with the form inputs
     *
     * @param {Object} fields - key-value pair of the field to be updated
     */
    updateState( fields ) {
        this.setState( ( prevState ) => {
            const form = Object.assign({}, prevState.form, fields );
            return { form };
        });
    }

    /**
     * Validates form
     * @returns {Boolean}
     */
    validateForm() {
        let valid = true;

        let inputs;
        if ( this.state.employee ) {
            inputs = ['account_number'];
        } else {
            inputs = [ 'bank_branch', 'account_number', 'company_code' ];
        }

        const selects = [ 'bank', 'account_type' ];

        inputs.forEach( ( field ) => {
            if ( this[ field ]._validate( this.state.form[ field ]) ) {
                valid = false;
            }
        });

        selects.forEach( ( field ) => {
            if ( !this[ field ]._checkRequire( isObject( this[ field ].state.value ) ? this[ field ].state.value.value : this[ field ].state.value ) ) {
                valid = false;
            }
        });

        return valid;
    }

    handleAddBank = () => {
        const { employee, form } = this.state;
        const formValid = this.validateForm();

        if ( formValid ) {
            if ( employee ) {
                const payload = {
                    id: employee.id,
                    company_id: employee.company_id,
                    bank_id: form.bank,
                    account_type: form.account_type,
                    account_number: form.account_number
                };

                this.props.addEmployeeBankAccount( payload );
            } else {
                this.props.addBankAccount( form );
            }
        }
    }

    handleCancel = () => {
        const { form } = this.state;
        const hasChanges = Object.keys( form ).some( ( field ) => form[ field ] !== '' );

        if ( hasChanges ) {
            this.setState({ show_modal: true });
            return;
        }

        this.goBack();
    }

    handleAlphaNumericFormatting = ( value, item ) => {
        const strippedValue = stripNonAlphaNum( value );

        item.setState({ value: strippedValue });
        this.updateState({ [ item.props.id ]: strippedValue });
    }

    selectedBank = ({ value, branch, group }) => {
        this.updateState({ bank: value, branch, bank_group: group });
    }

    goBack() {
        if ( this.state.employee ) {
            browserHistory.push( `/employee/${this.state.employee.id}`, true );
        } else {
            browserHistory.push( '/disbursements' );
        }
    }

    render() {
        const {
            form,
            employee,
            show_modal: showModal
        } = this.state;

        const {
            notification,
            submitting,
            loading,
            bankList
        } = this.props;

        const sidebarLinks = !employee && getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA: this.props.products && subscriptionService.isSubscribedToTA( this.props.products )
        });

        const securityBankGroup = form.bank_group === 'SECURITYBANK';

        return (
            <div>
                <Helmet
                    title={ `Add ${employee ? 'Payment Method' : 'Bank Account'}` }
                    meta={ [{
                        name: `Add ${employee ? 'Payment Method' : 'Bank Account'}`,
                        content: 'Page to add bank accounts for disbursements'
                    }] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <SalConfirm
                    onConfirm={ () => this.goBack() }
                    onClose={ () => this.setState({ show_modal: false }) }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                You are about to leave the page with unsaved changes.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ showModal }
                />
                { sidebarLinks && (
                    <Sidebar items={ sidebarLinks } />
                ) }
                <NavWrapper>
                    <Container>
                        <A
                            href={ employee ? `/employee/${employee.id}` : '/payroll/disbursements' }
                            disabled={ submitting }
                        >
                            &#8592; Back to { employee ? 'Profile' : 'Payment Methods' }
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container>
                        { loading && (
                            <div className="loader">
                                <Loader />
                            </div>
                        ) }
                        <div className={ `content add${loading ? ' hide' : ''}` }>
                            <div className="heading">
                                <H3>Add Company Payment Method</H3>
                                { !employee && (
                                    <p>Add your company payment methods here. After payroll is run, salaries can be disbursed either via bank transfer or SALPay. Learn more about disbursement or SALPay.</p>
                                ) }
                            </div>

                            { employee && (
                                <H4 className="employee-name">
                                    <span>{ employee.name }</span>
                                    &nbsp;ID: { employee.employee_id }
                                </H4>
                            ) }

                            <div className="row">
                                <div className="col-xs-4">
                                    <Select
                                        id="bank"
                                        label="Bank Name"
                                        placeholder="Select bank"
                                        required
                                        data={ bankList }
                                        value={ form.bank }
                                        ref={ ( ref ) => { this.bank = ref; } }
                                        onChange={ this.selectedBank }
                                        disabled={ submitting }
                                    />
                                </div>
                                <div className="col-xs-4">
                                    { employee ? (
                                        <div className="branch">
                                            <div>
                                                Branch: <span>{ form.branch }</span>
                                            </div>
                                        </div>
                                    ) : (
                                        <Input
                                            id="bank_branch"
                                            label="Bank Branch"
                                            placeholder="Bank branch here"
                                            required
                                            ref={ ( ref ) => { this.bank_branch = ref; } }
                                            onChange={ ( value ) => this.updateState({ bank_branch: value }) }
                                            disabled={ submitting }
                                        />
                                    ) }
                                </div>
                                <div className="col-xs-4" />
                            </div>
                            <div className="row">
                                <div className="col-xs-4">
                                    <Input
                                        id="account_number"
                                        label="Account Number"
                                        placeholder="Account number here"
                                        required
                                        ref={ ( ref ) => { this.account_number = ref; } }
                                        onChange={ ( value ) => {
                                            const strippedValue = stripNonNumeric( value );
                                            this.account_number.setState({ value: strippedValue });
                                            this.updateState({ account_number: strippedValue });
                                        } }
                                        disabled={ submitting }
                                    />
                                </div>
                                <div className="col-xs-4">
                                    <Select
                                        id="account_type"
                                        label="Account Type"
                                        placeholder="Select account type"
                                        required
                                        data={ OPTIONS.ACCOUNT_TYPES }
                                        value={ form.account_type }
                                        ref={ ( ref ) => { this.account_type = ref; } }
                                        onChange={ ({ value }) => this.updateState({ account_type: value }) }
                                        disabled={ submitting }
                                    />
                                </div>
                                <div className="col-xs-4" />
                            </div>
                            { securityBankGroup && ( <div className="row">
                                <div className="col-xs-4">
                                    <Input
                                        id="account_source"
                                        label="Account Source"
                                        placeholder="Account source here"
                                        ref={ ( ref ) => { this.account_source = ref; } }
                                        onChange={ ( value ) => this.handleAlphaNumericFormatting( value, this.account_source ) }
                                        disabled={ submitting }
                                    />
                                </div>
                                <div className="col-xs-4">
                                    <Input
                                        id="account_destination"
                                        label="Account Destination"
                                        placeholder="Account destination here"
                                        ref={ ( ref ) => { this.account_destination = ref; } }
                                        onChange={ ( value ) => this.handleAlphaNumericFormatting( value, this.account_destination ) }
                                        disabled={ submitting }
                                    />
                                </div>
                                <div className="col-xs-4" />
                            </div> )}
                            { !employee && (
                                <div className="row">
                                    <div className="col-xs-4">
                                        <Input
                                            id="company_code"
                                            label="Company Code"
                                            placeholder="Enter bank code here"
                                            ref={ ( ref ) => { this.company_code = ref; } }
                                            onChange={ ( value ) => {
                                                const strippedValue = stripNonAlphaNumHypenUnderscore( value );
                                                this.company_code.setState({ value: strippedValue });
                                                this.updateState({ company_code: strippedValue });
                                            } }
                                            disabled={ submitting }
                                        />
                                        <div className="help-text">For bank file use only</div>
                                    </div>
                                    <div className="col-xs-8" />
                                </div>
                            ) }
                        </div>
                    </Container>
                    { !loading && (
                        <Container>
                            <div className="pull-right">
                                <Button
                                    label="Cancel"
                                    type="action"
                                    size="large"
                                    alt
                                    onClick={ this.handleCancel }
                                    disabled={ submitting }
                                />
                                <Button
                                    id="addButton"
                                    label={ submitting ? <Loader /> : 'Add' }
                                    type="action"
                                    size="large"
                                    ref={ ( ref ) => { this.addButton = ref; } }
                                    onClick={ this.handleAddBank }
                                    disabled={ submitting }
                                />
                            </div>
                        </Container>
                    ) }
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    submitting: makeSelectSubmitting(),
    bankList: makeSelectBankList(),
    notification: makeSelectNotification()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        addDisbursementActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddDisbursement );
