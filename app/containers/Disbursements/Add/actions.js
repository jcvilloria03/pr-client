import {
    GET_BANK_LIST,
    GET_COMPANY_BANK_LIST,
    NOTIFICATION,
    ADD_BANK_ACCOUNT,
    ADD_EMPLOYEE_BANK_ACCOUNT
} from './constants';

/**
 * Fetch list of Banks
 */
export function getBankList() {
    return {
        type: GET_BANK_LIST
    };
}

/**
 * Fetch list of Banks defined in Company Settings
 * @param {Number|String} companyId
 */
export function getCompanyBankList( companyId ) {
    return {
        type: GET_COMPANY_BANK_LIST,
        payload: companyId
    };
}

/**
 * Add Bank Account
 * @param {Object} payload - Disbursement method form
 */
export function addBankAccount( payload ) {
    return {
        type: ADD_BANK_ACCOUNT,
        payload
    };
}

/**
 * Add Employee's Bank Account to Disburse to
 * @param {Object} payload - employee and bank account details
 */
export function addEmployeeBankAccount( payload ) {
    return {
        type: ADD_EMPLOYEE_BANK_ACCOUNT,
        payload
    };
}

/**
 * Display a notification to a user
 * @param {Boolean} show
 * @param {String} title
 * @param {String} message
 * @param {String} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
