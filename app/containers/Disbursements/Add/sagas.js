import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    SET_LOADING,
    GET_BANK_LIST,
    GET_COMPANY_BANK_LIST,
    SET_BANK_LIST,
    SET_COMPANY_BANK_LIST,
    NOTIFICATION,
    SET_NOTIFICATION,
    ADD_BANK_ACCOUNT,
    ADD_EMPLOYEE_BANK_ACCOUNT,
    SET_SUBMITTING
} from './constants';

/**
 * Get list of Banks
 */
export function* getBankList() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const { data } = yield call( Fetch, '/banks' );

        yield put({
            type: SET_BANK_LIST,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: get( error, 'response.statusText', 'Error' ),
            message: get( error, 'response.data.message', error.statusText ),
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Get Disbursement Methods of current company
 * @param {Number|String} payload - Company ID
 */
export function* getCompanyDisbursementMethods({ payload }) {
    const companyId = payload;

    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const { data } = yield call( Fetch, `/company/${companyId}/banks` );

        yield put({
            type: SET_COMPANY_BANK_LIST,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: get( error, 'response.statusText', 'Error' ),
            message: get( error, 'response.data.message', error.statusText ),
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Add Bank Account
 * @param {Object} payload - New Bank information
 */
export function* addBankAccount({ payload }) {
    const companyId = company.getLastActiveCompanyId();
    const {
        bank,
        bank_branch,
        account_source,
        account_destination,
        account_type,
        account_number,
        company_code
    } = payload;

    try {
        yield put({
            type: SET_SUBMITTING,
            payload: true
        });

        const data = {
            type: 'company-bank-account',
            attributes: {
                bank_id: bank,
                bank_branch,
                account_type,
                account_number,
                company_code
            }
        };

        if ( payload.bank_group === 'SECURITYBANK' ) {
            data.attributes.bank_details = {
                account_source,
                account_destination
            };
        }

        yield call( Fetch, `/company/${companyId}/bank`, {
            method: 'POST',
            data: { data }
        });

        yield call( browserHistory.push, '/disbursements' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: get( error, 'response.statusText', 'Error' ),
            message: get( error, 'response.data.message', error.statusText ),
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_SUBMITTING,
            payload: false
        });
    }
}

/**
 * Add Employee Bank Account
 * @param {Object} payload - Employee Bank information
 */
export function* addEmployeeBankAccount({ payload }) {
    const employeeId = payload.id;

    try {
        yield put({
            type: SET_SUBMITTING,
            payload: true
        });

        const data = {
            type: 'employee-payment-method',
            attributes: {
                company_id: payload.company_id,
                bank_id: payload.bank_id,
                account_type: payload.account_type,
                account_number: payload.account_number
            }
        };

        yield call( Fetch, `/employee/${employeeId}/bank`, {
            method: 'POST',
            data: { data }
        });

        yield call( browserHistory.push, `/employee/${employeeId}`, true );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: get( error, 'response.statusText', 'Error' ),
            message: get( error, 'response.data.message', error.statusText ),
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_SUBMITTING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield call( hideNotification, payload );

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );
    yield call( hideNotification, payload );
}

/**
 * Hides notification
 */
function* hideNotification({ type }) {
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getBankList );
}

/**
 * Watcher for GET_BANK_LIST action
 */
export function* watchForGetBankList() {
    const watcher = yield takeEvery( GET_BANK_LIST, getBankList );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_COMPANY_BANK_LIST action
 */
export function* watchForGetCompanyBankList() {
    const watcher = yield takeEvery( GET_COMPANY_BANK_LIST, getCompanyDisbursementMethods );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for ADD_BANK_ACCOUNT action
 */
export function* watchForAddBankAccount() {
    const watcher = yield takeEvery( ADD_BANK_ACCOUNT, addBankAccount );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for ADD_BANK_ACCOUNT action
 */
export function* watchForAddEmployeeBankAccount() {
    const watcher = yield takeEvery( ADD_EMPLOYEE_BANK_ACCOUNT, addEmployeeBankAccount );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetBankList,
    watchForGetCompanyBankList,
    watchForAddBankAccount,
    watchForAddEmployeeBankAccount,
    watchForNotifyUser,
    watchForReinitializePage
];
