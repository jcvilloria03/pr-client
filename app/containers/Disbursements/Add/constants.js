export const OPTIONS = {
    ACCOUNT_TYPES: [
        { label: 'Savings', value: 'SAVINGS' },
        { label: 'Current', value: 'CURRENT' }
    ]
};

const namespace = 'app/Disbursements/Add';

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const GET_BANK_LIST = `${namespace}/GET_BANK_LIST`;
export const SET_BANK_LIST = `${namespace}/SET_BANK_LIST`;
export const SET_COMPANY_BANK_LIST = `${namespace}/SET_COMPANY_BANK_LIST`;
export const ADD_BANK_ACCOUNT = `${namespace}/ADD_BANK_ACCOUNT`;
export const ADD_EMPLOYEE_BANK_ACCOUNT = `${namespace}/ADD_EMPLOYEE_BANK_ACCOUNT`;
export const SET_SUBMITTING = `${namespace}/SET_SUBMITTING`;
export const GET_COMPANY_BANK_LIST = `${namespace}/GET_COMPANY_BANK_LIST`;
