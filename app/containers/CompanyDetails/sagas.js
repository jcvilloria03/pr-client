import { delay, takeEvery, takeLatest } from 'redux-saga';
import { call, put } from 'redux-saga/effects';

import { Fetch } from 'utils/request';

import get from 'lodash/get';
import omit from 'lodash/omit';

import { resetStore } from '../App/sagas';
import { setCurrentCompany } from '../App/actions';

import {
  setLoading,
  setNotification,
  setCompanyTypes,
  setCompanyDetails

} from './actions';

import {
  RESET_STORE,
  UPDATE_COMPANY,
  GET_COMPANY_TYPES
} from './constants';

/**
 * handle initial form data
 */
export function* getCompanyDetails() {
    try {
        yield put( setLoading( true ) );

        const companyTypes = yield call( Fetch, '/philippine/company_types', { method: 'GET' });
        const formattedTypes = companyTypes.data.map( ( data ) => ({ value: data, label: data }) );

        yield put( setCompanyTypes( formattedTypes ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * handle update company by id
 */
export function* updateCompany({ payload }) {
    const { id, validatedForm, originalForm } = payload;

    try {
        yield put( setLoading( true ) );
        const data = new FormData();
        const cloneData = omit( validatedForm, [ 'logo', 'address' ]);

        for ( const key of Object.keys( cloneData ) ) {
            if ( originalForm[ key ] !== validatedForm[ key ]) {
                data.append( key, validatedForm[ key ]);
            }
        }

        if ( validatedForm.logo && typeof validatedForm.logo !== 'string' ) {
            data.append( 'logo', validatedForm.logo );
        }

        const response = yield call( Fetch, `/company/${id}`, {
            method: 'POST',
            data
        });

        yield put( setCompanyDetails( response ) );
        yield put( setCurrentCompany( response ) );
    } catch ( error ) {
        let errorMessage = errorMessage = get( error, 'response.data.message', error.statusText );

        if ( !errorMessage ) {
            errorMessage = Object.values( get( error, 'response.data', []) ).toString();
        }

        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: errorMessage,
            type: 'error'
        });
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * watch for reset store
 */
export function* watchForResetStore() {
    yield takeEvery( RESET_STORE, resetStore );
}

/**
 * watch for fetching company details
 */
export function* watchForGetCompanyDetails() {
    yield takeEvery( GET_COMPANY_TYPES, getCompanyDetails );
}
/**
 * watch for update company
 */
export function* watchForUpdateCompany() {
    yield takeLatest( UPDATE_COMPANY, updateCompany );
}

// All sagas to be loaded
export default [
    watchForResetStore,
    watchForUpdateCompany,
    watchForGetCompanyDetails
];
