/* eslint-disable no-confusing-arrow */
import styled from 'styled-components';
import { Container } from 'reactstrap';

export const StyledContainer = styled( Container )`
    padding: 30px 20px;
    padding-top: 100px;
`;
export const ActionContainer = styled( Container )`
    height: 100%;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    padding: 0;
`;

export const HeaderTitle = styled.h1`
    font-size: 24px;
    font-weight: bold;
    text-align: center;
`;
export const SubTitle = styled.p`
    font-size: 14px;
    text-align: center;
    margin-top: 0;
`;
export const SectionTitle = styled.p`
    font-size: 16px;
    text-align: left;
    margin-top: 36px;
    font-weight: 600;
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const ComponentWrapper = styled.div`
    display: block;
`;
