import {
  RESET_STORE,
  GET_COMPANY_TYPES,
  UPDATE_COMPANY,
  SET_LOADING,
  SET_NOTIFICATION,
  SET_COMPANY_TYPES,
  SET_COMPANY_DETAILS
} from './constants';

/**
 *
 * Companies actions
 *
 */

/**
 * Sends request to fetch company types
 */
export function getCompanyTypes() {
    return {
        type: GET_COMPANY_TYPES
    };
}

/**
 * Sets company types
 */
export function setCompanyTypes( payload ) {
    return {
        type: SET_COMPANY_TYPES,
        payload
    };
}

/**
 * Sets company detailS
 */
export function setCompanyDetails( payload ) {
    return {
        type: SET_COMPANY_DETAILS,
        payload
    };
}

/**
 * Sets page loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object}
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets notification
 * @param {Object} payload - Notification config
 * @returns {Object}
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 *
 * Dashboard actions
 *
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}

/**
 * update Company Details
 */
export function updateCompany( payload ) {
    return {
        type: UPDATE_COMPANY,
        payload
    };
}
