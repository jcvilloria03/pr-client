/* eslint-disable react/prop-types */
import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import Button from 'components/Button';
import Modal from 'components/Modal';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import { H2, H3 } from 'components/Typography';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';

import { subscriptionService } from 'utils/SubscriptionService';
import companyValidationSchema from './utils/companyDetailsValidationSchema';

import CompanyDetailsForm from './templates/CompanyDetailsForm';
import {
  makeSelectLoading,
  makeSelectNotification,
  makeSelectCompanyTypes,
  makeSelectCompanyDetails,
  makeSelectCurrentCompanyState
} from './selectors';

import * as companyDetailsActions from './actions';

import {
    ActionContainer,
    HeaderTitle,
    LoadingStyles,
    SectionTitle,
    StyledContainer,
    SubTitle
} from './styles';
/**
 *
 * CompanyDetails
 *
 */
export class CompanyDetails extends React.Component {
    // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        resetStore: React.PropTypes.func,
        getCompanyTypes: React.PropTypes.func,
        updateCompany: React.PropTypes.func,
        currentCompany: React.PropTypes.object,
        companyTypes: React.PropTypes.array,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    }

    constructor( props ) {
        super( props );

        this.state = {
            error: {},
            formBody: {
                name: '',
                type: '',
                tin: '',
                rdo: '',
                sss: '',
                hdmf: '',
                philhealth: '',
                email: '',
                website: '',
                mobile_number: '',
                telephone_number: '',
                telephone_extension: '',
                fax_number: '',
                logo: null
            }
        };

        this.cancelModal = null;
    }

    componentDidMount() {
        this.props.getCompanyTypes();
    }

    componentWillReceiveProps( nextProps ) {
        if ( Object.keys( nextProps.companyDetails ).length ) {
            this.setState({ formBody: nextProps.companyDetails });
        } else {
            this.setState({ formBody: nextProps.currentCompany });
        }
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    setFile = ( file ) => {
        this.setState( ( prev ) => ({
            ...prev,
            formBody: {
                ...prev.formBody,
                logo: file
            }
        }) );
    };
    handleInputChange = ( name, val ) => {
        this.setState( ( prev ) => ({
            ...prev,
            formBody: {
                ...prev.formBody,
                [ name ]: val
            }
        }) );
    };

    handleCancel = () => {
        this.setState( ( prev ) => ({
            ...prev,
            error: {},
            formBody: { ...this.props.currentCompany }
        }) );

        this.cancelModal.close();
    };

    handleSubmit = () => {
        companyValidationSchema( this.state.formBody )
          .then( ( validatedForm ) => {
              const id = this.props.currentCompany.id;
              const originalForm = this.props.currentCompany;
              this.props.updateCompany({ id, validatedForm, originalForm });
          })
          .catch( ( err ) => {
              const _error = {};
              err.inner.forEach( ( e ) => {
                  _error[ e.path ] = e.message;
              });

              this.setState({ error: _error });
          });
    };

    render() {
        const {
          loading,
          products,
          notification,
          companyTypes,
          currentCompany
        } = this.props;

        const {
          error,
          formBody
        } = this.state;

        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                products &&
                subscriptionService.isSubscribedToPayroll( products ),
            isSubscribedToTA:
                products &&
                subscriptionService.isSubscribedToTA( products )
        });
        return (
            <div>
                <Helmet
                    title="Company Details"
                    meta={ [
                        {
                            name: 'description',
                            content: 'Description of CompanyDetails'
                        }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <StyledContainer>
                    <Sidebar items={ sidebarLinks } />
                    { currentCompany && loading
                      ? (
                          <LoadingStyles>
                              <H2>Loading Company Details</H2>
                              <br />
                              <H3>Please wait...</H3>
                          </LoadingStyles>
                        )
                      : (
                          <div>
                              <HeaderTitle>Company Details</HeaderTitle>
                              <SubTitle>
                                Supply with your basic company information such
                                as comapany name and contact information
                            </SubTitle>
                              <SectionTitle>Company Information</SectionTitle>
                              <CompanyDetailsForm
                                  setFile={ this.setFile }
                                  handleInputChange={ this.handleInputChange }
                                  formBody={ formBody }
                                  error={ error }
                                  companyTypes={ companyTypes }
                              />
                              <div style={ { height: 100 } }>
                                  <ActionContainer>
                                      <Button
                                          label="Cancel"
                                          type="grey"
                                          onClick={ () => {
                                              this.cancelModal && this.cancelModal.toggle();
                                          } }
                                      />
                                      <Button
                                          label="Update"
                                          type="action"
                                          onClick={ () => this.handleSubmit() }
                                      />
                                  </ActionContainer>
                              </div>
                          </div>
                      )}
                </StyledContainer>

                <Modal
                    ref={ ( ref ) => {
                        this.cancelModal = ref;
                    } }
                    title="Discard changes"
                    body={ <p>Discard changes?</p> }
                    buttons={ [
                        {
                            label: 'Ok',
                            type: 'danger',
                            onClick: this.handleCancel
                        },
                        {
                            label: 'Cancel',
                            type: 'grey',
                            onClick: () => this.cancelModal.close()
                        }
                    ] }
                />
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    companyTypes: makeSelectCompanyTypes(),
    companyDetails: makeSelectCompanyDetails(),
    currentCompany: makeSelectCurrentCompanyState()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        companyDetailsActions,
        dispatch
   );
}

export default connect( mapStateToProps, mapDispatchToProps )( CompanyDetails );
