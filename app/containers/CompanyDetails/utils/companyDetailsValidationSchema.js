import * as yup from 'yup';

/** validation schema  */
export default function companyValidationSchema( form ) {
    const schema = yup.object().shape({
        name: yup.string().required( 'Company Name is a required field.' ),
        type: yup.string().required( 'Company Type is a required field.' ),
        tin: yup
            .string()
            .required( 'Tax Identification Number (TIN) is a required field.' )
            .matches(
                /^(?:\d{3}-\d{3}-\d{3}-\d{3})$/,
                'Invalid format, Format is (XXX-XXX-XXX-XXX)'
            ),
        rdo: yup.string().required( 'RDO is a required field.' ),
        sss: yup
            .string()
            .required( 'Social Security System (SSS) is a required field.' )
            .matches(
                /^(?:\d{2}-\d{7}-\d{1})$/,
                'Invalid format, Format is (XX-XXXXXXX-X)'
            ),
        hdmf: yup
            .string()
            .required(
                'Home Development Mutual Fund (HDMF) Number is a required field.'
            )
            .matches(
                /^(?:\d{4}-\d{4}-\d{4})$/,
                'Invalid format, Format is (XXXX-XXXX-XXXX)'
            ),
        philhealth: yup
            .string()
            .required( 'PhilHealth Number is a required field.' )
            .matches(
                /^(?:\d{2}-\d{9}-\d{1})$/,
                'Invalid format, Format is (XX-XXXXXXXXX-X)'
            )
    });
    return schema.validate( form, { abortEarly: false });
}
