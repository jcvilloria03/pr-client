import { fromJS } from 'immutable';
import {
  SET_LOADING,
  SET_NOTIFICATION,
  SET_COMPANY_TYPES,
  SET_COMPANY_DETAILS
} from './constants';

const initialState = fromJS({
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    companyTypes: [],
    companyDetails: {}
});

/**
 *
 * CompanyDetails reducer
 *
 */
function companyDetailsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_COMPANY_TYPES:
            return state.set( 'companyTypes', fromJS( action.payload ) );
        case SET_COMPANY_DETAILS:
            return state.set( 'companyDetails', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default companyDetailsReducer;
