import { createSelector } from 'reselect';

/**
 * Direct selector to the companies state domain
 */
const selectCompanyDetailsDomain = () => ( state ) => state.get( 'companyDetails' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

const makeSelectCurrentCompanyState = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'currentCompany' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectCompanyDetailsDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectCompanyDetails = () => createSelector(
    selectCompanyDetailsDomain(),
  ( substate ) => substate.get( 'companyDetails' ).toJS()
);

const makeSelectCompanyTypes = () => createSelector(
    selectCompanyDetailsDomain(),
  ( substate ) => substate.get( 'companyTypes' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectCompanyDetailsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectCompanyTypes,
    makeSelectNotification,
    makeSelectCompanyDetails,
    makeSelectCurrentCompanyState
};
