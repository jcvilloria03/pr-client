/*
 *
 * Edit Company constants
 *
 */

export const LOADING = 'app/CompanyDetails/LOADING';
export const RESET_STORE = 'app/CompanyDetails/RESET_STORE';
export const NOTIFICATION = 'app/CompanyDetails/NOTIFICATION';

export const GET_COMPANY_TYPES = 'app/CompanyDetails/GET_COMPANY_TYPES';
export const UPDATE_COMPANY = 'app/CompanyDetails/UPDATE_COMPANY';

export const SET_LOADING = 'app/CompanyDetails/SET_LOADING';
export const SET_NOTIFICATION = 'app/CompanyDetails/SET_NOTIFICATION';
export const SET_COMPANY_TYPES = 'app/CompanyDetails/SET_COMPANY_TYPES';
export const SET_COMPANY_DETAILS = 'app/CompanyDetails/SET_COMPANY_DETAILS';

