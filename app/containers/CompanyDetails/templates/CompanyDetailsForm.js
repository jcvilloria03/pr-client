/* eslint-disable react/prop-types */
/* eslint-disable camelcase */
import React from 'react';

import { Col, Row } from 'reactstrap';
import Input from 'components/Input';
import Select from 'components/Select';

import {
    formatRDO,
    formatSSS,
    formatTIN,
    formatHDMF,
    stripNonNumeric,
    formatPhilHealth
  } from 'utils/functions';

import ImageUpload from './ImageUpload';
import { SectionTitle } from '../styles';

const CompanyDetailsForm = ({
    setFile,
    handleInputChange,
    formBody,
    error,
    companyTypes
}) => {
    const {
        name,
        type,
        tin,
        rdo,
        sss,
        hdmf,
        philhealth,
        email,
        website,
        mobile_number,
        telephone_number,
        telephone_extension,
        fax_number,
        logo
    } = formBody;

    return (
        <div>
            <Row>
                <Col xs={ 4 }>
                    <Input
                        label="Company Name"
                        id="name"
                        name="name"
                        value={ name || '' }
                        placeholder="Company Name"
                        onChange={ ( val ) => {
                            handleInputChange( 'name', val );
                        } }
                        required
                        clientError={ error.name }
                    />
                </Col>
                <Col xs={ 4 }>
                    <Select
                        label="Company Type"
                        id="type"
                        name="type"
                        value={ type || '' }
                        onChange={ ({ value }) => {
                            handleInputChange( 'type', value );
                        } }
                        data={ companyTypes }
                        required
                        clientError={ error.type }
                    />
                </Col>
                <Col xs={ 4 }>
                    <ImageUpload
                        label="Company Logo"
                        setFile={ setFile }
                        file={ logo }
                    />
                </Col>
            </Row>
            <SectionTitle>Goverment Issued ID no.</SectionTitle>
            <Row>
                <Col xs={ 4 }>
                    <Input
                        label="Tax Identification Number (TIN)"
                        id="tin"
                        name="tin"
                        placeholder="000-000-000-000"
                        value={ tin || '' }
                        ref={ ( ref ) => { this.tin = ref; } }
                        onChange={ ( val ) => {
                            const formatted = formatTIN( val );
                            this.tin.setState({ value: formatted }, handleInputChange( 'tin', formatted ) );
                        } }
                        required
                        clientError={ error.tin }
                    />
                </Col>
                <Col xs={ 4 }>
                    <Input
                        label="RDO"
                        id="rdo"
                        name="rdo"
                        type="number"
                        value={ rdo || '' }
                        placeholder="RDO"
                        ref={ ( ref ) => { this.rdo = ref; } }
                        onChange={ ( val ) => {
                            const formatted = formatRDO( val );
                            this.rdo.setState({ value: formatted }, handleInputChange( 'rdo', formatted ) );
                        } }
                        required
                        clientError={ error.rdo }
                    />
                </Col>
                <Col xs={ 4 }>
                    <Input
                        label="Social Security System (SSS)"
                        id="sss"
                        name="sss"
                        value={ sss || '' }
                        placeholder="00-0000000-0"
                        ref={ ( ref ) => { this.sss = ref; } }
                        onChange={ ( val ) => {
                            const formatted = formatSSS( val );
                            this.sss.setState({ value: formatted }, handleInputChange( 'sss', formatted ) );
                        } }
                        required
                        clientError={ error.sss }
                    />
                </Col>
            </Row>
            <Row>
                <Col xs={ 4 }>
                    <Input
                        label="Home Development Mutual Fund (HDMF) Number"
                        id="hdmf"
                        name="hdmf"
                        value={ hdmf || '' }
                        placeholder="0000-0000-0000"
                        ref={ ( ref ) => { this.hdmf = ref; } }
                        onChange={ ( val ) => {
                            const formatted = formatHDMF( val );
                            this.hdmf.setState({ value: formatted }, handleInputChange( 'hdmf', formatted ) );
                        } }
                        required
                        clientError={ error.hdmf }
                    />
                </Col>
                <Col xs={ 4 }>
                    <Input
                        label="PhilHealth Number"
                        id="philhealth"
                        name="philhealth"
                        value={ philhealth || '' }
                        placeholder="00-000000000-0"
                        ref={ ( ref ) => { this.philhealth = ref; } }
                        onChange={ ( val ) => {
                            const formatted = formatPhilHealth( val );
                            this.philhealth.setState({ value: formatted }, handleInputChange( 'philhealth', formatted ) );
                        } }
                        required
                        clientError={ error.philhealth }
                    />
                </Col>
            </Row>
            <SectionTitle>Contact Information</SectionTitle>
            <Row>
                <Col xs={ 4 }>
                    <Input
                        label="Email Address"
                        id="email"
                        name="email"
                        type="email"
                        value={ email || '' }
                        placeholder={ 'Email Address' }
                        onChange={ ( val ) => {
                            handleInputChange( 'email', val );
                        } }
                    />
                </Col>
                <Col xs={ 4 }>
                    <Input
                        label="Website"
                        id="website"
                        name="website"
                        value={ website || '' }
                        placeholder={ 'Website' }
                        onChange={ ( val ) => {
                            handleInputChange( 'website', val );
                        } }
                    />
                </Col>
                <Col xs={ 4 }>
                    <Input
                        label="Mobile Number"
                        id="mobile_number"
                        name="mobile_number"
                        type="number"
                        value={ mobile_number || '' }
                        placeholder={ 'Mobile Number' }
                        onChange={ ( val ) => {
                            handleInputChange( 'mobile_number', val );
                        } }
                    />
                </Col>
            </Row>
            <Row>
                <Col xs={ 4 }>
                    <Input
                        label="Telephone Number"
                        id="telephone_number"
                        type="number"
                        name="telephone_number"
                        value={ telephone_number || '' }
                        placeholder={ 'Telephone Number' }
                        ref={ ( ref ) => { this.telephone_number = ref; } }
                        onChange={ ( val ) => {
                            const formatted = stripNonNumeric( val );
                            this.telephone_number.setState({ value: formatted }, handleInputChange( 'telephone_number', formatted ) );
                        } }
                    />
                </Col>
                <Col xs={ 4 }>
                    <Input
                        label="Telephone Extension"
                        id="telephone_extension"
                        type="number"
                        name="telephone_extension"
                        value={ telephone_extension || '' }
                        placeholder={ 'Telephone Extension' }
                        onChange={ ( val ) => {
                            handleInputChange( 'telephone_extension', val );
                        } }
                    />
                </Col>
                <Col xs={ 4 }>
                    <Input
                        label="Fax"
                        id="fax_number"
                        type="number"
                        name="fax_number"
                        value={ fax_number || '' }
                        placeholder={ 'Fax' }
                        onChange={ ( val ) => {
                            handleInputChange( 'fax_number', val );
                        } }
                    />
                </Col>
            </Row>
        </div>
    );
};

export default CompanyDetailsForm;
