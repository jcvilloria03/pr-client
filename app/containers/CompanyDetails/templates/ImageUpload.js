/* eslint-disable no-return-assign */
/* eslint-disable react/prop-types */
/* eslint-disable consistent-return */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable require-jsdoc */
import React from 'react';
import Dropzone from 'react-dropzone';
import Button from '../../../components/Button';

export class ImageUpload extends React.Component {
    static propTypes = {
        file: React.PropTypes.any,
        setFile: React.PropTypes.func
        // label: React.PropTypes.string,
    };
    constructor( props ) {
        super( props );

        this.state = {
            currentFile: null,
            preview: null
        };

        this.dropZoneEl = null;
    }

    setImagePreview = ( props ) => {
        if ( props.file ) {
            if ( typeof props.file === 'string' ) {
                return this.setState({ preview: props.file });
            }
            this.setState({
                currentFile: props.file,
                preview: props.file.preview
            });
        }
    }

    componentDidMount() {
        this.setImagePreview( this.props );
    }

    componentWillReceiveProps( nextProps ) {
        this.setImagePreview( nextProps );
    }

    handleDrop = ( file ) => {
        this.setState({ preview: file[ 0 ].preview, currentFile: file[ 0 ] });
        this.props.setFile( file[ 0 ]);
    };

    handleReplace = () => {
        this.dropZoneEl.fileInputEl.click();
        // this.setState({ preview: null, currentFile: null });
        this.props.setFile( null );
    };

    render() {
        const { preview } = this.state;

        return (
            <div >
                <p style={ { fontSize: 14, marginBottom: 0 } }>
                    {this.props.label}
                </p>
                <div style={ { position: 'relative' } }>
                    {preview && (
                        <Button
                            label="Replace"
                            onClick={ this.handleReplace }
                            style={ { position: 'absolute', right: 8, top: 8 } }
                        />
                    )}
                    <Dropzone
                        id="drop-zone-component"
                        ref={ ( el ) => this.dropZoneEl = el }
                        onDrop={ this.handleDrop }
                        multiple={ false }
                        accept="image/*"
                        style={ {
                            width: '100%',
                            height: 200,
                            border: '1px dashed gray',
                            borderRadius: 4
                        } }
                    ></Dropzone>

                    {preview ? (
                        <img
                            src={ preview }
                            alt="preview"
                            height="100"
                            style={ {
                                position: 'absolute',
                                top: '50%',
                                left: '50%',
                                transform: 'translate(-50%, -50%)'
                            } }
                        />
                    ) : (
                        <p
                            style={ {
                                position: 'absolute',
                                top: '50%',
                                left: '50%',
                                transform: 'translate(-50%, -50%)'
                            } }
                        >
                            Drop an image
                        </p>
                    )}
                </div>
            </div>
        );
    }
}

export default ImageUpload;
