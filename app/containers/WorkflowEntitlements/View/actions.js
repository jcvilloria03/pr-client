import {
    INITIALIZE,
    NOTIFICATION,
    GET_WORKFLOW_ENTITLEMENTS,
    DELETE_WORKFLOW_ENTITLEMENTS,
    UPDATE_WORKFLOW_ENTITLEMENT,
    GET_EMPLOYEE_REQUEST_MODULES,
    SET_CURRENTLY_EDITING_WORKFLOW
} from './constants';

/**
 * Initialize page
 * @param {Object} data
 */
export function initialize( payload ) {
    return {
        type: INITIALIZE,
        payload
    };
}

/**
 * Fetch workflow entitlements
 * @param {Array} payload
 */
export function getRequestModules( payload = []) {
    return {
        type: GET_EMPLOYEE_REQUEST_MODULES,
        payload
    };
}

/**
 * Fetch workflow entitlements
 * @param {Object} data
 */
export function getWorkflowEntitlements( data ) {
    return {
        type: GET_WORKFLOW_ENTITLEMENTS,
        payload: data
    };
}

/**
 * Delete workflow entitlements
 * @param {Array} IDs
 */
export function deleteWorkflowEntitlements( IDs ) {
    return {
        type: DELETE_WORKFLOW_ENTITLEMENTS,
        payload: IDs
    };
}

/**
 * Update workflow entitlements
 * @param {Object} payload
 */
export function updateWorkflowEntitlement( payload, callback = () => null ) {
    return {
        type: UPDATE_WORKFLOW_ENTITLEMENT,
        payload,
        callback
    };
}

/**
 * Display a notification in page
 * @param {Boolean} show
 * @param {String} title
 * @param {String} message
 * @param {String} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Set inline edit workflow data on save via pending request modal
 * @param {Object} data
 */
export function setCurrentlyEditingWorkflow( payload ) {
    return {
        type: SET_CURRENTLY_EDITING_WORKFLOW,
        payload
    };
}
