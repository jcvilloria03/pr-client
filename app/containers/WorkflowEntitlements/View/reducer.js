import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_LOADING_WORKFLOW_ENTITLEMENTS,
    SET_WORKFLOW_ENTITLEMENTS,
    SET_WORKFLOW_ENTITLEMENTS_PAGINATION,
    SET_FILTER_DATA,
    SET_HAS_PENDING_REQUESTS,
    SET_EMPLOYEE_REQUEST_MODULES,
    NOTIFICATION_SAGA,
    SET_CURRENTLY_EDITING_WORKFLOW
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    loadingWorkflowEntitlements: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    workflowEntitlements: [],
    workflowEntitlementsPagination: {},
    filterData: {},
    hasPendingRequests: false,
    requestModules: [],
    workflow: {}
});

/**
 * Workflow View reducer
 */
export default function ( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_LOADING_WORKFLOW_ENTITLEMENTS:
            return state.set( 'loadingWorkflowEntitlements', action.payload );
        case SET_WORKFLOW_ENTITLEMENTS:
            return state.set( 'workflowEntitlements', fromJS( action.payload ) );
        case SET_WORKFLOW_ENTITLEMENTS_PAGINATION:
            return state.set( 'workflowEntitlementsPagination', fromJS( action.payload ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case SET_EMPLOYEE_REQUEST_MODULES:
            return state.set( 'requestModules', fromJS( action.payload ) );
        case SET_HAS_PENDING_REQUESTS:
            return state.set( 'hasPendingRequests', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_CURRENTLY_EDITING_WORKFLOW:
            return state.set( 'workflow', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}
