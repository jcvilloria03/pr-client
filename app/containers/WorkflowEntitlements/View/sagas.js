import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import {
    INITIALIZE,
    SET_LOADING,
    SET_LOADING_WORKFLOW_ENTITLEMENTS,
    GET_WORKFLOW_ENTITLEMENTS,
    SET_WORKFLOW_ENTITLEMENTS,
    SET_WORKFLOW_ENTITLEMENTS_PAGINATION,
    SET_EMPLOYEE_REQUEST_MODULES,
    SET_HAS_PENDING_REQUESTS,
    DELETE_WORKFLOW_ENTITLEMENTS,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    UPDATE_WORKFLOW_ENTITLEMENT
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

/**
 * Intialize data
 */
export function* initializeData() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();

        const workflowEntitlements = yield call( Fetch, `/company/${companyId}/workflow_entitlements?page=1&per_page=10`, { method: 'POST' });
        const requestModules = yield call( Fetch, '/employee_request_module', { method: 'GET' });

        const parsedRequestModules = requestModules.data.map( ({ name }) => ({ value: name, label: name }) );

        yield put({
            type: SET_WORKFLOW_ENTITLEMENTS,
            payload: workflowEntitlements.data
        });

        yield put({
            type: SET_WORKFLOW_ENTITLEMENTS_PAGINATION,
            payload: workflowEntitlements.meta.pagination
        });

        yield put({
            type: SET_EMPLOYEE_REQUEST_MODULES,
            payload: parsedRequestModules
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Get workflow entitlements for table
 * @param {Object} payload
 */
export function* getWorkflowEntitlements({ payload }) {
    try {
        yield put({
            type: SET_LOADING_WORKFLOW_ENTITLEMENTS,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();

        const page = payload && payload.page || 1;
        const perPage = payload && payload.per_page || 10;
        const term = payload && payload.term || '';

        const data = {
            sort_by: payload && payload.sort_by || null,
            sort_order: payload && payload.sort_order || null,
            workflows_ids: payload.workflows_ids || [],
            request_types_ids: payload.request_types_ids || [],
            employees_ids: payload.employees_ids || []
        };

        const response = yield call( Fetch, `/company/${companyId}/workflow_entitlements?term=${term}&page=${page}&per_page=${perPage}`,
            {
                method: 'POST',
                data
            });

        yield put({
            type: SET_WORKFLOW_ENTITLEMENTS,
            payload: response.data
        });

        yield put({
            type: SET_WORKFLOW_ENTITLEMENTS_PAGINATION,
            payload: response.meta.pagination
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING_WORKFLOW_ENTITLEMENTS,
            payload: false
        });
    }
}

/**
 * Batch delete workflow entitlements
 */
export function* deleteWorkflowEntitlements({ payload }) {
    try {
        yield call( Fetch, '/workflow_entitlement/bulk_delete', {
            method: 'DELETE',
            data: {
                workflow_entitlements_ids: payload,
                company_id: company.getLastActiveCompanyId()
            }
        });
        yield call( getWorkflowEntitlements, { payload: {}});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Check if workflow entitlement has any pending requests and update workflow entitlement
 */
export function* checkAndUpdateWorkflowEntitlement({ payload, callback }) {
    try {
        yield put({
            type: SET_LOADING_WORKFLOW_ENTITLEMENTS,
            payload: true
        });

        yield put({
            type: SET_HAS_PENDING_REQUESTS,
            payload: false
        });

        if ( payload.skipPendingRequestsCheck ) {
            yield call( updateWorkflowEntitlement, payload, callback );
        } else {
            const { has_pending_requests: hasPendingRequests } = yield call( Fetch, `/workflow_entitlement/${payload.id}/has_pending_requests`, {
                method: 'POST',
                data: {
                    ...payload,
                    company_id: company.getLastActiveCompanyId()
                }
            });

            if ( hasPendingRequests ) {
                yield put({
                    type: SET_HAS_PENDING_REQUESTS,
                    payload: hasPendingRequests
                });
            } else {
                yield call( updateWorkflowEntitlement, payload, callback );
            }
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING_WORKFLOW_ENTITLEMENTS,
            payload: false
        });
    }
}

/**
 * Update workflow entitlement
 */
export function* updateWorkflowEntitlement( payload, callback ) {
    yield call( Fetch, `/workflow_entitlement/${payload.id}`, {
        method: 'PUT',
        data: {
            ...payload,
            company_id: company.getLastActiveCompanyId()
        }
    });

    if ( callback ) {
        yield call( callback );
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watcher for GET leaves
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_WORKFLOW_ENTITLEMENTS
 */
export function* watchForGetWorkflowEntitlements() {
    const watcher = yield takeEvery( GET_WORKFLOW_ENTITLEMENTS, getWorkflowEntitlements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE_WORKFLOW_ENTITLEMENTS
 */
export function* watchForDeleteWorkflowEntitlements() {
    const watcher = yield takeEvery( DELETE_WORKFLOW_ENTITLEMENTS, deleteWorkflowEntitlements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for UPDATE_WORKFLOW_ENTITLEMENT
 */
export function* watchForUpdateWorkflowEntitlement() {
    const watcher = yield takeEvery( UPDATE_WORKFLOW_ENTITLEMENT, checkAndUpdateWorkflowEntitlement );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForGetWorkflowEntitlements,
    watchForDeleteWorkflowEntitlements,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForUpdateWorkflowEntitlement
];
