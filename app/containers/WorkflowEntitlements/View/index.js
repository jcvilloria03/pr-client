import React from 'react';
import debounce from 'lodash/debounce';
import first from 'lodash/first';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { auth } from 'utils/AuthService';

import SubHeader from '../../SubHeader';
import SnackBar from '../../../components/SnackBar';

import {
    makeSelectLoading,
    makeSelectLoadingWorkflowEntitlements,
    makeSelectWorkflowEntitlements,
    makeSelectWorkflowEntitlementsPagination,
    makeSelectNotification,
    makeHasPendingRequests,
    makeSelectRequestModules,
    makeCurrentlyEditingWorkflow
} from './selectors';
import * as workflowEntitlementsViewActions from './actions';

import { PageWrapper, ConfirmBodyWrapperStyle, LoadingStyles } from './styles';

import SalConfirm from '../../../components/SalConfirm';
import { H2, H3, H5, P } from '../../../components/Typography';
import Button from '../../../components/Button';
import Search from '../../../components/Search';
import SalDropdown from '../../../components/SalDropdown';
import AsyncTable from '../../../components/AsyncTable';
import Select from '../../../components/Select';

import { formatPaginationLabel } from '../../../utils/functions';
import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

const EditButton = ({
        rows,
        isCurrentlyEditing,
        removeCurrentEditingRows,
        setCurrentlyEditingRow,
        updateWorkflow
    }) => {
    const { id } = rows;

    return ( <div
        style={ {
            display: 'flex',
            width: '100%'
        } }
    >
        { isCurrentlyEditing( id ) ?
            <div
                style={ {
                    display: 'flex',
                    width: '100%',
                    justifyContent: 'center'
                } }
            >
                <Button
                    label="Cancel"
                    type="neutral"
                    size="small"
                    onClick={ () => {
                        removeCurrentEditingRows( id );
                    } }
                />
                <Button
                    label="Save"
                    type="action"
                    size="small"
                    onClick={ () => updateWorkflow( rows ) }
                />
            </div>
            :
            <Button
                style={ { margin: 'auto', marginRight: '8px' } }
                label="Edit"
                type="grey"
                size="small"
                onClick={ () => { setCurrentlyEditingRow( rows ); } }
            />
            }

    </div> );
};

EditButton.propTypes = {
    rows: React.PropTypes.object,
    isCurrentlyEditing: React.PropTypes.func,
    removeCurrentEditingRows: React.PropTypes.func,
    setCurrentlyEditingRow: React.PropTypes.func,
    updateWorkflow: React.PropTypes.func
};

/**
 * View Workflow Entitlements Component
 */
export class View extends React.Component {
    static propTypes = {
        deleteWorkflowEntitlements: React.PropTypes.func,
        workflowEntitlements: React.PropTypes.array,
        pagination: React.PropTypes.object,
        initialize: React.PropTypes.func,
        getWorkflowEntitlements: React.PropTypes.func,
        updateWorkflowEntitlement: React.PropTypes.func,
        loading: React.PropTypes.bool,
        loadingWorkflowEntitlements: React.PropTypes.bool,
        hasPendingRequests: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        requestModules: React.PropTypes.array,
        currentlyEditingWorkflow: React.PropTypes.object,
        setCurrentlyEditingWorkflow: React.PropTypes.func
    };

    static defaultProps = {
        loading: true,
        page: 1
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            showDelete: false,
            deleteModalLabel: '',
            term: '',
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            showModal: false,
            moreActionsVisible: false,
            showFilter: false,
            hasFiltersApplied: false,
            currentPage: this.props.pagination.current_page,
            pagination: {
                page: 0,
                pageSize: 10,
                totalPages: 0,
                dataLength: 0
            },
            sorting: {
                by: null,
                order: null
            },
            filter: {},
            selectedValues: [],
            currentlyEditingRow: null

        };

        this.onSortingChange = this.onSortingChange.bind( this );
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.workflow_entitlement',
            'create.workflow_entitlement',
            'delete.workflow_entitlement',
            'edit.workflow_entitlement'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.workflow_entitlement' ];

            if ( authorized ) {
                this.setState({ permission: {
                    view: authorization[ 'view.workflow_entitlement' ],
                    create: authorization[ 'create.workflow_entitlement' ],
                    delete: authorization[ 'delete.workflow_entitlement' ],
                    edit: authorization[ 'edit.workflow_entitlement' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.initialize( this.props.products );
    }

    componentWillReceiveProps( newProps ) {
        if ( !isEmpty( newProps.pagination ) ) {
            this.setPagination( newProps.pagination );
        }
    }

    onSortingChange( column, additive ) {
        // clone current sorting sorting
        let newSorting = JSON.parse( JSON.stringify( this.table.state.sorting, ( key, value ) => {
            if ( typeof value === 'function' ) {
                return value.toString();
            }
            return value;
        }) );

        // check if already sorted by same column
        const existingIndex = newSorting.findIndex( ( d ) => d.id === column.id );

        let isDesc = false;
        if ( existingIndex > -1 ) {
            const existing = newSorting[ existingIndex ];
            if ( existing.desc ) {
                if ( additive ) {
                    newSorting.splice( existingIndex, 1 );
                } else {
                    existing.desc = false;
                    newSorting = [existing];
                }
            } else {
                existing.desc = true;
                isDesc = true;
                if ( !additive ) {
                    newSorting = [existing];
                }
            }
        } else if ( additive ) {
            newSorting.push({
                id: column.id,
                desc: false
            });
        } else {
            newSorting = [{
                id: column.id,
                desc: false
            }];
        }

        const orderDir = isDesc ? 'desc' : 'asc';

        // set new sorting states
        this.setState({
            sorting: {
                by: column.id,
                order: orderDir
            }
        }, () => {
            // Fetch sorted data
            this.getWorkflowEntitlements();
        });

        this.table.setState({
            sorting: newSorting
        });
        this.table.tableComponent.setState({
            sorting: newSorting
        });
    }

    setCurrentlyEditingRow = ({ id }) => {
        this.setState({
            currentlyEditingRow: id
        });
    }

    getSelectedValuesIds = () => this.state.selectedValues.map( ( selectedValue ) => selectedValue.id )

    setSelectedValues = () => {
        const selectedValues = [];

        this.table.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedValues.push( this.table.props.data[ index ]);
            }
        });

        this.setState({ selectedValues });

        return this.state.selectedValues.map( ( selectedValue ) => selectedValue.id );
    }

    setPagination = ( workflowPagination ) => {
        const pagination = {
            page: workflowPagination.current_page,
            pageSize: workflowPagination.per_page,
            totalPages: workflowPagination.total_pages,
            dataLength: workflowPagination.total
        };

        this.setState({
            pagination,
            label: formatPaginationLabel({
                page: workflowPagination.current_page - 1,
                pageSize: workflowPagination.per_page,
                dataLength: workflowPagination.total
            })
        });
    }

    getTableColumns() {
        return [
            {
                header: 'Employee Name',
                accessor: 'employee_name',
                minWidth: 200,
                sortable: true,
                getProps: ( state, rowInfo ) => ({
                    style: {
                        overflow: ( rowInfo && rowInfo.row && this.isCurrentlyEdited( rowInfo.row ) ) ? 'visible' : 'hidden'
                    }
                }),
                render: ({ row }) => (
                    <div>
                        {row.employee.full_name}
                    </div>
                )
            },
            {
                header: 'Employee ID',
                accessor: 'employee_id',
                minWidth: 150,
                sortable: true,
                getProps: ( state, rowInfo ) => ({
                    style: {
                        overflow: ( rowInfo && rowInfo.row && this.isCurrentlyEdited( rowInfo.row ) ) ? 'visible' : 'hidden'
                    }
                }),
                render: ({ row }) => (
                    <div>
                        {row.employee.employee_uid}
                    </div>
                )
            },
            {
                header: 'Request Type',
                accessor: 'request_type',
                minWidth: 150,
                sortable: true,
                getProps: ( state, rowInfo ) => ({
                    style: {
                        overflow: ( rowInfo && rowInfo.row && this.isCurrentlyEditing( rowInfo.row.id ) ) ? 'visible' : 'hidden'
                    }
                }),
                render: ({ row }) => (
                    <div style={ { width: '100%' } }>
                        { this.isCurrentlyEditing( row.id ) ?
                            <Select
                                id="selectRequestType"
                                required
                                placeholder="Type in RequestType"
                                data={ this.props.requestModules }
                                value={ {
                                    disabled: false,
                                    label: row.request_type,
                                    value: row.request_type
                                } }
                                ref={ ( ref ) => { this.selectRequestType = ref; } }

                            /> : row.request_type}
                    </div>
                )
            },
            {
                header: 'Workflow',
                accessor: 'workflow_name',
                minWidth: 350,
                sortable: true,
                getProps: ( state, rowInfo ) => ({
                    style: {
                        overflow: ( rowInfo && rowInfo.row && this.isCurrentlyEditing( rowInfo.row.id ) ) ? 'visible' : 'hidden'
                    }
                }),
                render: ({ row }) => (
                    <div style={ { width: '100%' } }>
                        { this.isCurrentlyEditing( row.id ) ?
                            <Select
                                async
                                id="selectWorkflow"
                                required
                                placeholder="Type in workflows"
                                loadOptions={ this.loadWorkflows }
                                value={ {
                                    disabled: false,
                                    label: row.workflow.name,
                                    value: row.workflow.id
                                } }
                                ref={ ( ref ) => { this.selectWorkflow = ref; } }
                            /> : row.workflow_label}
                    </div>

                )
            },

            {
                header: '',
                accessor: '',
                minWidth: 100,
                sortable: false,
                getProps: ( state, rowInfo ) => ({
                    style: {
                        overflow: ( rowInfo && rowInfo.row && this.isCurrentlyEditing( rowInfo.row.id ) ) ? 'visible' : 'hidden'
                    }
                }),
                render: ({ row }) =>
                     ( <EditButton
                         rows={ row }
                         isCurrentlyEditing={ this.isCurrentlyEditing }
                         removeCurrentEditingRows={ this.removeCurrentEditingRows }
                         setCurrentlyEditingRow={ this.setCurrentlyEditingRow }
                         updateWorkflow={ this.updateWorkflow }
                         editRequestTypeValue={ this.state.editRequestTypeValue }
                         editWorkflowValue={ this.selectWorkflow }
                     /> ) }
        ];
    }

    setDeleteModalLabel = () => {
        this.setState({
            deleteModalLabel: this.getDeleteLabelText()
        });
    }

    getDeleteLabelText() {
        return `
            You are about to delete ${this.state.selectedValues.length}
            workflow entitlement${this.state.selectedValues.length > 1 ? 's' : ''}.
            Do you want to proceed?
        `;
    }

    getWorkflowEntitlements() {
        this.props.getWorkflowEntitlements({
            term: this.state.term,
            page: this.state.pagination.page,
            per_page: this.state.pagination.pageSize,
            sort_by: this.state.sorting.by,
            sort_order: this.state.sorting.order,
            ...this.state.filter
        });
    }

    redirectRoute = ( route ) => {
        browserHistory.push( route, true );
    }

    loadWorkflows = ( keyword, callback ) => {
        const companyId = company.getLastActiveCompanyId();

        // 'http://gw-api.local.salarium.test/company/12074/workflows?term=Multiple%20Approver%20in%201%20Level'
        Fetch( `/company/${companyId}/workflows?term=${keyword}`, { method: 'GET' })
          .then( ( result ) => {
              const employees = result.data.map( ({ id, name }) => ({
                  value: id,
                  label: name,
                  disabled: false
              }) );
              callback( null, { options: employees });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    isCurrentlyEditing = ( id ) => this.state.currentlyEditingRow === id

    startEditingRow( row ) {
        this.setState({
            edited: {
                id: row.id,
                employee_id: row.employee_id,
                workflow_id: row.workflow.id,
                request_type: row.request_type
            }
        });
    }

    stopEditing = ( callback ) => {
        this.setState({
            edited: null,
            currentlyEditingRow: null
        }, callback );
    }

    validateInlineEdit() {
        let valid = true;

        if ( !this.employeeName._checkRequire( this.employeeName.state.value ) ) {
            valid = false;
        }

        if ( !this.employeeID._checkRequire( this.employeeID.state.value ) ) {
            valid = false;
        }

        if ( !this.workflow._checkRequire( this.workflow.state.value ) ) {
            valid = false;
        }

        if ( !this.requestType._checkRequire( this.requestType.state.value ) ) {
            valid = false;
        }

        return valid;
    }

    saveEdited = ( skipPendingRequestsCheck ) => {
        const workflowData = this.props.currentlyEditingWorkflow;

        this.props.updateWorkflowEntitlement({ ...this.state.edited, skipPendingRequestsCheck, ...workflowData }, () => {
            this.stopEditing( this.getWorkflowEntitlements );
        });
    }

    isCurrentlyEdited = ( rowId ) => this.state.edited && this.state.edited.id === rowId;

    removeCurrentEditingRows = () => {
        this.setState({
            currentlyEditingRow: null
        });
    }

    tableIsNotMounted() {
        return !this.table ||
            !this.table.tableComponent ||
            !this.table.tableComponent.state;
    }

    handleDeleteModal = () => {
        this.setSelectedValues();
        this.setDeleteModalLabel();

        if ( !this.state.selectedValues || !this.state.selectedValues.length ) {
            return;
        }

        this.setState({ showModal: false }, () => {
            this.setState({ showModal: true });
        });
    }

    tableHasSelectedRows() {
        if ( this.tableIsNotMounted() ) {
            return false;
        }

        return !!this.table.state.selected.filter( ( item ) => item ).length;
    }

    deleteWorkflowEntitlements = () => {
        const valueIds = this.getSelectedValuesIds();

        if ( !valueIds || !valueIds.length ) {
            return;
        }

        this.props.deleteWorkflowEntitlements( valueIds );
    }

    handleSearch = ( term ) => {
        if ( term !== this.state.term ) {
            const page = 1;
            this.setState({
                term,
                pagination: {
                    ...this.state.pagination,
                    page
                }
            }, () => {
                this.getWorkflowEntitlements();
            });
        }
    }

    handleDataChange = ( tableProps ) => {
        if ( !tableProps.sorting || !tableProps.sorting.length ) return;

        const sortCondition = first( tableProps.sorting );

        this.setState({
            sorting: {
                by: sortCondition.id,
                order: sortCondition.desc ? 'desc' : 'asc'
            }
        }, () => {
            this.getWorkflowEntitlements();
        });
    }

    handlePageChange = ( page ) => {
        this.setState({
            pagination: {
                ...this.state.pagination,
                page
            }
        }, () => {
            this.getWorkflowEntitlements();
        });
    }

    handlePageSizeChange = ( pageSize ) => {
        if ( !pageSize ) {
            return;
        }
        const page = 1;
        this.setState({
            pagination: {
                ...this.state.pagination,
                page,
                pageSize
            }
        }, () => {
            this.getWorkflowEntitlements();
        });
    }

    updateWorkflowEntitlement( property, value ) {
        this.setState({
            edited: {
                ...this.state.edited,
                [ property ]: value
            }
        });
    }

    updateWorkflow = ({ id, company_id, employee_id }) => {
        const workflowId = this.selectWorkflow ? this.selectWorkflow.state.value.value : null;
        const requestType = this.selectRequestType ? this.selectRequestType.state.value.value : null;

        const editedPayload = {
            id,
            company_id,
            employee_id,
            user_id: auth.getUser().account_id,
            workflow_id: workflowId,
            request_type: requestType,
            skipPendingRequestsCheck: false
        };

        this.props.setCurrentlyEditingWorkflow({
            id,
            company_id,
            employee_id,
            user_id: auth.getUser().account_id,
            workflow_id: workflowId,
            request_type: requestType
        });

        this.props.updateWorkflowEntitlement( editedPayload, () => {
            this.setState({
                currentlyEditingRow: null
            }, this.getWorkflowEntitlements );
        });
    }

    render() {
        const dataForDisplay = this.props.workflowEntitlements.map( ( data ) => ({
            ...data,
            workflow_label: `${data.workflow.levels_count} Level ${data.workflow.name}`
        }) );

        return (
            <div>
                <Helmet
                    title="Assigned Workflows"
                    meta={ [
                        { name: 'description', content: 'View Leaves' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <SalConfirm
                                onConfirm={ this.deleteWorkflowEntitlements }
                                body={
                                    <ConfirmBodyWrapperStyle>
                                        <div className="message">
                                            { this.state.deleteModalLabel }
                                        </div>
                                    </ConfirmBodyWrapperStyle>
                                }
                                title="Delete Workflow Assignments"
                                buttonStyle="danger"
                                showCancel
                                confirmText="Delete"
                                cancelText="Cancel"
                                visible={ this.state.showModal }
                            />
                            <SalConfirm
                                onConfirm={ () => this.saveEdited( true ) }
                                body={
                                    <ConfirmBodyWrapperStyle>
                                        <div className="message">
                                            Updating workflow assignment will reset pending requests.
                                        </div>
                                    </ConfirmBodyWrapperStyle>
                                }
                                title="Update Workflow Assignment"
                                buttonStyle="danger"
                                showCancel
                                confirmText="Confirm"
                                cancelText="Cancel"
                                visible={ this.props.hasPendingRequests }
                            />
                            <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Loading Assigned Workflows.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                            { !this.props.loading && (
                                <div className="content">
                                    <div className="heading">
                                        <H3>Assigned Workflows</H3>
                                        <P>You may view assigned workflows, add batch assigned workflow and assign workflow individually or by department.</P>
                                        <div style={ { textAlign: 'center', marginTop: '20px' } } >
                                            <div className="main">
                                                <Button
                                                    className={ this.state.permission.create ? '' : 'hide' }
                                                    label="Batch Assign Workflows"
                                                    type="neutral"
                                                    onClick={ () => this.redirectRoute( '/time/workflow-entitlements/add?batch' ) }
                                                />
                                                <Button
                                                    className={ this.state.permission.create ? '' : 'hide' }
                                                    label="Assign Workflow"
                                                    type="action"
                                                    onClick={ () => this.redirectRoute( '/time/workflow-entitlements/add' ) }
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="title">
                                        <H5>Assigned Workflow List</H5>
                                        <div className="search-wrapper">
                                            <Search
                                                ref={ ( ref ) => { this.search = ref; } }
                                                handleSearch={ debounce( this.handleSearch, 1000 ) }
                                            />
                                        </div>
                                        {this.tableHasSelectedRows() &&
                                            <span className="sl-u-gap-left--sm">
                                                <SalDropdown
                                                    dropdownItems={ [
                                                        {
                                                            label: 'Delete',
                                                            children: <div>Delete</div>,
                                                            onClick: this.handleDeleteModal
                                                        }
                                                    ] }
                                                />
                                            </span>
                                        }
                                    </div>

                                    <AsyncTable
                                        data={ dataForDisplay }
                                        columns={ this.getTableColumns() }
                                        onDataChange={ this.handleDataChange }
                                        onPageChange={ this.handlePageChange }
                                        ref={ ( ref ) => { this.table = ref; } }
                                        pagination
                                        loading={ this.props.loadingWorkflowEntitlements }
                                        pages={ this.state.pagination.totalPages }
                                        onPageSizeChange={ this.handlePageSizeChange }
                                        pageSize={ this.state.pagination.pageSize }
                                        tablePage={ this.state.pagination.page - 1 }
                                        selectable
                                        onSelectionChange={ () => {
                                            this.setSelectedValues();
                                        } }
                                        onSortingChange={ this.onSortingChange }
                                    />
                                </div>
                            )}
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    loadingWorkflowEntitlements: makeSelectLoadingWorkflowEntitlements(),
    workflowEntitlements: makeSelectWorkflowEntitlements(),
    pagination: makeSelectWorkflowEntitlementsPagination(),
    notification: makeSelectNotification(),
    hasPendingRequests: makeHasPendingRequests(),
    requestModules: makeSelectRequestModules(),
    currentlyEditingWorkflow: makeCurrentlyEditingWorkflow()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        workflowEntitlementsViewActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
