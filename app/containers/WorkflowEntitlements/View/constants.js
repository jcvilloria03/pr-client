export const INITIALIZE = 'app/WorkflowEntitlements/View/INTIALIZE';
export const NOTIFICATION = 'app/WorkflowEntitlements/View/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/WorkflowEntitlements/View/NOTIFICATION_SAGA';
export const GET_WORKFLOW_ENTITLEMENTS = 'app/WorkflowEntitlements/View/GET_WORKFLOW_ENTITLEMENTS';
export const DELETE_WORKFLOW_ENTITLEMENTS = 'app/WorkflowEntitlements/View/DELETE_WORKFLOW_ENTITLEMENTS';
export const SET_LOADING = 'app/WorkflowEntitlements/View/SET_LOADING';
export const SET_LOADING_WORKFLOW_ENTITLEMENTS = 'app/WorkflowEntitlements/View/SET_LOADING_WORKFLOW_ENTITLEMENTS';
export const SET_WORKFLOW_ENTITLEMENTS = 'app/WorkflowEntitlements/View/SET_WORKFLOW_ENTITLEMENTS';
export const SET_WORKFLOW_ENTITLEMENTS_PAGINATION = 'app/WorkflowEntitlements/View/SET_WORKFLOW_ENTITLEMENTS_PAGINATION';
export const SET_FILTER_DATA = 'app/WorkflowEntitlements/View/SET_FILTER_DATA';
export const UPDATE_WORKFLOW_ENTITLEMENT = 'app/WorkflowEntitlements/View/UPDATE_WORKFLOW_ENTITLEMENT';
export const SET_HAS_PENDING_REQUESTS = 'app/WorkflowEntitlements/View/SET_HAS_PENDING_REQUESTS';
export const GET_EMPLOYEE_REQUEST_MODULES = 'app/WorkflowEntitlements/View/GET_EMPLOYEE_REQUEST_MODULES';
export const SET_EMPLOYEE_REQUEST_MODULES = 'app/WorkflowEntitlements/View/SET_EMPLOYEE_REQUEST_MODULES';
export const SET_CURRENTLY_EDITING_WORKFLOW = 'app/WorkflowEntitlements/View/SET_CURRENTLY_EDITING_WORKFLOW';

export const REQUEST_TYPES = {
    LOAN: 'Loan'
};
