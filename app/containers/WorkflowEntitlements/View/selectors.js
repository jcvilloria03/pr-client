import { createSelector } from 'reselect';

const selectWorkflowEntitlementsViewDomain = () => ( state ) => state.get( 'workflowEntitlementsView' );

const makeSelectLoading = () => createSelector(
    selectWorkflowEntitlementsViewDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectLoadingWorkflowEntitlements = () => createSelector(
    selectWorkflowEntitlementsViewDomain(),
    ( substate ) => substate.get( 'loadingWorkflowEntitlements' )
);

const makeSelectWorkflowEntitlements = () => createSelector(
    selectWorkflowEntitlementsViewDomain(),
    ( substate ) => substate.get( 'workflowEntitlements' ).toJS()
);

const makeSelectWorkflowEntitlementsPagination = () => createSelector(
    selectWorkflowEntitlementsViewDomain(),
    ( substate ) => substate.get( 'workflowEntitlementsPagination' ).toJS()
);

const makeSelectRequestModules = () => createSelector(
    selectWorkflowEntitlementsViewDomain(),
    ( substate ) => substate.get( 'requestModules' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectWorkflowEntitlementsViewDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeHasPendingRequests = () => createSelector(
    selectWorkflowEntitlementsViewDomain(),
    ( substate ) => substate.get( 'hasPendingRequests' )
);

const makeCurrentlyEditingWorkflow = () => createSelector(
    selectWorkflowEntitlementsViewDomain(),
    ( substate ) => substate.get( 'workflow' )
);

export {
    makeSelectLoading,
    makeSelectLoadingWorkflowEntitlements,
    makeSelectWorkflowEntitlements,
    makeSelectWorkflowEntitlementsPagination,
    makeSelectNotification,
    makeHasPendingRequests,
    makeSelectRequestModules,
    makeCurrentlyEditingWorkflow
};
