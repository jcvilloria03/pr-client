import React from 'react';
import MultiSelect from '../../../../components/MultiSelect';
import Button from '../../../../components/Button';

import { FilterWrapper } from './styles';

import { FILTER_TYPES } from './constants';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {

    static propTypes = {
        filterData: React.PropTypes.shape({
            workflows: React.PropTypes.array,
            requestTypes: React.PropTypes.array,
            employees: React.PropTypes.array
        }),
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    onApply = () => {
        const filters = {
            workflows: [],
            requestTypes: [],
            employees: []
        };

        this.workflows.state.value && this.workflows.state.value.forEach( ( workflow ) => {
            filters.workflows.push( Object.assign( workflow, { type: FILTER_TYPES.WORKFLOW }) );
        });
        this.requestTypes.state.value && this.requestTypes.state.value.forEach( ( requestType ) => {
            filters.requestTypes.push( Object.assign( requestType, { type: FILTER_TYPES.REQUEST_TYPE }) );
        });
        this.employees.state.value && this.employees.state.value.forEach( ( employee ) => {
            filters.employees.push( Object.assign( employee, { type: FILTER_TYPES.EMPLOYEE }) );
        });

        this.props.onApply( filters );
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    getFilterPropsValues( filterValues ) {
        if ( !this.props.filterData[ filterValues ]) {
            return [];
        }

        return this.props.filterData[ filterValues ].map( ( filterValue ) => (
            this.formatDataForMultiselect( filterValue )
        ) );
    }

    resetFilters = () => {
        this.workflows.setState({ value: null });
        this.employees.setState({ value: null });
        this.requestTypes.setState({ value: null }, () => {
            this.onApply();
        });
    }

    formatDataForMultiselect = ( data ) => (
        {
            value: data.id,
            label: data.name || `${data.first_name} ${data.last_name} ${data.employee_id}`,
            disabled: false
        }
    )

    render() {
        const filterParamsNames = {
            workflows: 'workflows',
            requestTypes: 'requestTypes',
            employees: 'employees'

        };
        return (
            <FilterWrapper>
                <div className="row">
                    <div className="col-xs-4">
                        <MultiSelect
                            id="employees"
                            label={
                                <span>Employee</span>
                            }
                            ref={ ( ref ) => { this.employees = ref; } }
                            data={ this.getFilterPropsValues( filterParamsNames.employees ) }
                            placeholder="Search employee name or ID"
                        />
                    </div>
                    <div className="col-xs-4">
                        <MultiSelect
                            id="workflows"
                            label={
                                <span>Workflow</span>
                            }
                            ref={ ( ref ) => { this.workflows = ref; } }
                            data={ this.getFilterPropsValues( filterParamsNames.workflows ) }
                            placeholder="Select workflows"
                        />
                    </div>
                    <div className="col-xs-4">
                        <MultiSelect
                            id="requestTypes"
                            label={
                                <span>Request Type</span>
                            }
                            ref={ ( ref ) => { this.requestTypes = ref; } }
                            data={ this.getFilterPropsValues( filterParamsNames.requestTypes ) }
                            placeholder="Select request type"
                        />
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
