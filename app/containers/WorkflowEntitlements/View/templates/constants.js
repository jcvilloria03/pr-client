export const FILTER_TYPES = {
    WORKFLOW: 'workflow',
    REQUEST_TYPE: 'requestType',
    EMPLOYEE: 'employee'
};
