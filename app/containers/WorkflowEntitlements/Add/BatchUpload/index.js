import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../../components/A';
import Table from '../../../../components/Table';
import Button from '../../../../components/Button';
import Clipboard from '../../../../components/Clipboard';
import FileInput from '../../../../components/FileInput';
import { H4, P } from '../../../../components/Typography';
import SalConfirm from '../../../../components/SalConfirm';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { formatPaginationLabel } from '../../../../utils/functions';

import { BASE_PATH_NAME } from '../../../../constants';

import * as actions from '../actions';

import {
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectWorkflowEntitlementsPreview,
    makeSelectHasPendingRequests,
    makeSelectSaving
} from '../selectors';

import {
    StyledLoader,
    PageWrapper,
    Footer,
    ConfirmBodyWrapperStyle
} from './styles';

/**
 * WorkflowEntitlements Batch Upload Component
 */
class BatchUpload extends React.Component {
    static propTypes = {
        batchUploadJobId: React.PropTypes.string,
        uploadWorkflowEntitlements: React.PropTypes.func,
        batchUploadStatus: React.PropTypes.string,
        batchUploadErrors: React.PropTypes.oneOfType([
            React.PropTypes.object,
            React.PropTypes.array
        ]),
        workflowEntitlementsPreview: React.PropTypes.shape({
            status: React.PropTypes.string,
            data: React.PropTypes.array
        }),
        saving: React.PropTypes.shape({
            status: React.PropTypes.string,
            errors: React.PropTypes.object
        }),
        saveWorkflowEntitlements: React.PropTypes.func,
        checkHasPendingRequestsBatchUpload: React.PropTypes.func,
        hasPendingRequests: React.PropTypes.bool,
        setIsLoading: React.PropTypes.func,
        isLoading: React.PropTypes.bool
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            files: [],
            status: null,
            errors: {},
            clipboard: '',
            submit: false,
            workflowEntitlementsTableLabel: 'Showing 0-0 of 0 entries',
            errorsTableLabel: 'Showing 0-0 of 0 entries',
            workflowEntitlementsPreview: {
                data: [],
                status: 'ready'
            },
            savingStatus: '',
            savingErrors: {}
        };
    }

    componentDidMount() {
        this.props.setIsLoading( this.props.isLoading );
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.batchUploadStatus !== this.props.batchUploadStatus && this.setState({
            status: nextProps.batchUploadStatus
        }, () => {
            this.validateButton.setState({
                disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.batchUploadStatus )
            });
        });

        nextProps.batchUploadErrors !== this.props.batchUploadErrors && this.setState({
            errors: nextProps.batchUploadErrors
        }, () => {
            let clipboard = '';
            const rows = Object.keys( nextProps.batchUploadErrors );

            if ( rows.length > 0 ) {
                rows.forEach( ( row ) => {
                    Object.values( nextProps.batchUploadErrors[ row ]).forEach( ( value ) => {
                        clipboard += `Row ${row}, ${value}\n`;
                    });
                });
            }

            this.handleErrorsTableChanges();
            this.setState({ clipboard });
        });

        nextProps.workflowEntitlementsPreview.data !== this.props.workflowEntitlementsPreview.data && this.setState({
            workflowEntitlementsPreview: { ...this.state.workflowEntitlementsPreview, data: nextProps.workflowEntitlementsPreview.data }
        }, () => {
            this.handleWorkflowEntitlementsTableChanges();
        });

        nextProps.workflowEntitlementsPreview.status !== this.props.workflowEntitlementsPreview.status && this.setState({
            workflowEntitlementsPreview: { ...this.state.workflowEntitlementsPreview, status: nextProps.workflowEntitlementsPreview.status }
        });

        nextProps.saving.status !== this.props.saving.status && this.setState({
            savingStatus: nextProps.saving.status
        }, () => {
            this.submitButton.setState({ disabled: nextProps.saving.status !== 'save_failed' });
        });
    }

    handleErrorsTableChanges = () => {
        if ( !this.errorsTable ) {
            return;
        }

        this.setState({
            errorsTableLabel: formatPaginationLabel( this.errorsTable.tableComponent.state )
        });
    };

    handleWorkflowEntitlementsTableChanges = () => {
        if ( !this.workflowEntitlementsTable ) {
            return;
        }

        this.setState({
            workflowEntitlementsTableLabel: formatPaginationLabel( this.workflowEntitlementsTable.tableComponent.state )
        });
    };

    preparePreviewData() {
        return this.state.workflowEntitlementsPreview.data.map( ( entitlement ) => ({
            employee_id: entitlement.employee.employee_uid,
            employee_name: entitlement.employee.full_name,
            workflow_name: entitlement.workflow.name,
            request_type: entitlement.request_type
        }) );
    }

    saveWorkflowEntitlements = () => {
        this.setState({ savingStatus: 'save_queued' });
        this.props.saveWorkflowEntitlements({ jobId: this.props.batchUploadJobId });
    }

    checkHasPendingRequestsAndSubmit = () => {
        this.submitButton.setState({ disabled: true });

        this.props.checkHasPendingRequestsBatchUpload({
            jobId: this.props.batchUploadJobId
        }, this.saveWorkflowEntitlements );
    }

    renderWorkflowEntitlementsSection = () => {
        const tableColumns = [
            {
                header: 'Employee Name',
                accessor: 'employee_name',
                minWidth: 250,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.employee_name}
                    </div>
                )
            },
            {
                header: 'Employee ID',
                accessor: 'employee_id',
                minWidth: 100,
                render: ({ row }) => (
                    <div>
                        {row.employee_id}
                    </div>
                )
            },
            {
                header: 'Workflow',
                accessor: 'workflow_name',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.workflow_name}
                    </div>
                )
            },
            {
                header: 'Request Type',
                accessor: 'request_type',
                minWidth: 260,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.request_type}
                    </div>
                )
            }
        ];

        const dataForDisplay = this.preparePreviewData();

        return this.state.workflowEntitlementsPreview.data.length === 0
            ? null
            : (
                <div>
                    <H4>Workflow Entitlements List:</H4>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.workflowEntitlementsTableLabel }
                        </span>
                    </div>
                    <Table
                        columns={ tableColumns }
                        data={ dataForDisplay }
                        pagination
                        ref={ ( ref ) => { this.workflowEntitlementsTable = ref; } }
                        onDataChange={ this.handleWorkflowEntitlementsTableChanges }
                    />
                </div>
            );
    };

    renderErrorsSection = () => {
        const errorDisplay = [];
        const errorList = this.state.errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'Row Number',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'Error Type',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            const keys = Object.keys( errorList );

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row {key}</H4>,
                    error: <ul>{Object.values( errorList[ key ]).map( ( value ) => <li key={ value } style={ { margin: '0' } } >{value}</li> )}</ul>
                });
            });

            return (
                <div className="errors">
                    <H4>Data field errors</H4>
                    <P>There is an error in the file you&#39;ve just uploaded. Correct the errors and upload the file again.</P>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.errorsTableLabel }
                        </span>
                        <Clipboard value={ this.state.clipboard } />
                    </div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                        ref={ ( ref ) => { this.errorsTable = ref; } }
                        onDataChange={ this.handleErrorsTableChanges }
                    />
                </div>
            );
        }

        return null;
    }

    render() {
        return (
            <PageWrapper>
                <Container>
                    <P className="sl-u-gap-top--lg sl-u-gap-bottom--lg">
                        This page allows you to add batch assigned workflows. A template will be made available for you through this page.
                    </P>
                    <div className="steps">
                        <div className="step">
                            <div className="template">
                                <H4>Step 1:</H4>
                                <P>Download and fill-out batch upload template.</P>
                                <P><A target="_blank" href={ `${BASE_PATH_NAME}/guides/workflow-entitlements/batch-upload` }>Click here to view the upload guide.</A></P>
                                <A className="sl-c-btn--wide" href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/workflow-entitlements.csv" download>Download Template</A>
                            </div>
                        </div>
                        <div className="step">
                            <div className="upload">
                                <H4>Step 2:</H4>
                                <P>After completely filling out the template, choose and upload it here.</P>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'none' : 'block' } } >
                                    <FileInput
                                        accept=".csv"
                                        onDrop={ ( files ) => {
                                            const { acceptedFiles } = files;

                                            this.setState({
                                                files: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null,
                                                errors: {},
                                                status: null
                                            }, () => {
                                                this.validateButton.setState({ disabled: acceptedFiles.length <= 0 });
                                            });
                                        } }
                                        ref={ ( ref ) => { this.fileInput = ref; } }
                                    />
                                </div>
                                <div style={ { display: this.state.status === 'validated' ? 'block' : 'none' } }>
                                    <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                                </div>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'validated' ? 'none' : 'block' } } >
                                    <Button
                                        label={
                                            this.state.submit ? (
                                                <StyledLoader className="animation">
                                                    Uploading <div className="anim3"></div>
                                                </StyledLoader>
                                            ) : (
                                                'Upload'
                                            )
                                        }
                                        disabled={ this.state.files ? this.state.files.length <= 0 : true }
                                        type="neutral"
                                        ref={ ( ref ) => { this.validateButton = ref; } }
                                        onClick={ () => {
                                            this.setState({
                                                errors: {},
                                                status: null,
                                                submit: true
                                            }, () => {
                                                this.validateButton.setState({ disabled: true }, () => {
                                                    this.props.uploadWorkflowEntitlements({ file: this.state.files });

                                                    setTimeout( () => {
                                                        this.validateButton.setState({ disabled: false });
                                                        this.setState({ submit: false });
                                                    }, 5000 );
                                                });
                                            });
                                        } }
                                    />
                                </div>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'block' : 'none' } } >
                                    <StyledLoader className="animation">
                                        <H4 style={ { margin: '0' } }>{ this.state.status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                    </StyledLoader>
                                </div>
                            </div>
                        </div>
                    </div>
                    { this.renderWorkflowEntitlementsSection() }
                    { this.renderErrorsSection() }
                </Container>

                <Footer>
                    <Container>
                        <Button
                            label="Cancel"
                            type="neutral"
                            size="large"
                            onClick={ () => {
                                browserHistory.push( '/time/workflow-entitlements', true );
                            } }
                        />
                        <Button
                            ref={ ( ref ) => { this.submitButton = ref; } }
                            type="action"
                            disabled={ this.state.status !== 'validated' }
                            label={
                                this.state.savingStatus === 'save_queued' || this.state.savingStatus === 'saving' ?
                                    (
                                        <StyledLoader className="animation">
                                            <span>Saving</span> <div className="anim3"></div>
                                        </StyledLoader>
                                    ) : 'Submit'
                            }
                            size="large"
                            onClick={ this.checkHasPendingRequestsAndSubmit }
                        />
                    </Container>
                </Footer>

                <SalConfirm
                    onConfirm={ this.saveWorkflowEntitlements }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Saving changes. Unapproved requests in this changed workflows will be reset.
                                Their requesters and first-level approvers will be notified of the changes.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ this.props.hasPendingRequests }
                />
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    batchUploadJobId: makeSelectBatchUploadJobId(),
    batchUploadStatus: makeSelectBatchUploadStatus(),
    batchUploadErrors: makeSelectBatchUploadErrors(),
    workflowEntitlementsPreview: makeSelectWorkflowEntitlementsPreview(),
    hasPendingRequests: makeSelectHasPendingRequests(),
    saving: makeSelectSaving()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( BatchUpload );
