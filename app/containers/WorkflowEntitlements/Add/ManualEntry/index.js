import React from 'react';
import find from 'lodash/find';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { auth } from 'utils/AuthService';

import { browserHistory } from '../../../../utils/BrowserHistory';
import SnackBar from '../../../../components/SnackBar';
import SalConfirm from '../../../../components/SalConfirm';
import Loader from '../../../../components/Loader';
import Button from '../../../../components/Button';
import SalSelect from '../../../../components/Select';
import MultiSelect from '../../../../components/MultiSelect';

import { getEmployeeFullName } from '../../../../utils/functions';

import {
    MainWrapper,
    ConfirmBodyWrapperStyle,
    Footer
} from './styles';

import {
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectLoading,
    makeSelectHasPendingRequests
} from '../selectors';

import * as createWorkflowEntitlementActions from '../actions';

import { isAuthorized } from '../../../../utils/Authorization';

/**
 * Helper for persisting filters
 */
export const GetAccountAndCompanyId = () => ({
    accountId: auth.getUser().account_id,
    companyId: company.getLastActiveCompanyId()
});

/**
 * ManualEntry Container
 */
export class ManualEntry extends React.Component {
    static propTypes = {
        initializeData: React.PropTypes.func,
        formOptions: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitForm: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        hasPendingRequests: React.PropTypes.bool,
        checkHasPendingRequestsManual: React.PropTypes.func,
        products: React.PropTypes.array,
        setIsLoading: React.PropTypes.func
    }

    static defaultProps = {
        loading: true,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            workflowEntitlements: [],
            affectedEmployees: [],
            workflowEntitlementForm: {
                workflow_id: null,
                request_type: null
            },
            showWarningModal: false,
            errors: {},
            showConfirmSubmitModal: false,
            companyId: null,
            selectedWorkflowName: '',
            workflowLabel: ''
        };

        this.entitledEmployees = null;
        this.workflowEntitlementForms = [];
        this.addWorkflowEntitlementForm = {
            workflow_id: null,
            request_type: null
        };
    }

    componentWillMount() {
        isAuthorized(['create.workflow_entitlement'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        const { companyId } = GetAccountAndCompanyId();

        this.setState({
            companyId
        });

        this.props.initializeData( this.props.products );
    }

    componentDidUpdate( prevProps ) {
        if ( this.props.loading !== prevProps.loading ) {
            this.props.setIsLoading( this.state.loading );
        }
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    getRequestTypesOptions = () => {
        if ( Object.keys( this.props.formOptions ).length === 0 ) {
            return [];
        }

        return this.props.formOptions.requestTypes.map( ({ name }) => ({
            value: name, label: name
        }) );
    }

    getWorkflowsOptions = () => {
        if ( Object.keys( this.props.formOptions ).length === 0 ) {
            return [];
        }

        return this.props.formOptions.workflows.map( ({ id, name }) => ({
            value: id, label: name
        }) );
    }

    getAffectedEmployeesOptions = () => {
        const { formOptions } = this.props;

        if ( Object.keys( formOptions ).length === 0 ) {
            return [];
        }

        const departments = formOptions.departments.map( ( department ) => ({
            value: department.id, label: department.name, field: 'department'
        }) );
        const employees = formOptions.employees.map( ( employee ) => ({
            value: employee.id,
            label: `${employee.employee_id} - ${getEmployeeFullName( employee )}`,
            field: 'employee'
        }) );
        const positions = formOptions.positions.map( ( position ) => ({
            value: position.id, label: position.name, field: 'position'
        }) );
        const locations = formOptions.locations.map( ( location ) => ({
            value: location.id, label: location.name, field: 'location'
        }) );

        // Push options to entitle all employees, locations, departments or positions.
        return departments.concat( employees, positions, locations, [
            {
                label: 'All Departments',
                field: 'department',
                value: null
            },
            {
                label: 'All Employees',
                field: 'employee',
                value: null
            },
            {
                label: 'All Positions',
                field: 'position',
                value: null
            },
            {
                label: 'All Locations',
                field: 'location',
                value: null
            }
        ]);
    }

    validateWorkflowEntitlementData() {
        let valid = true;

        if ( !this.affectedEmployees._checkRequire( this.affectedEmployees.state.value ) ) {
            valid = false;
        }

        this.workflowEntitlementForms.forEach( ( form, index ) => {
            if ( !this.validateWorkflowEntitlementForm( form, index ) ) {
                valid = false;
            }
        });

        return valid;
    }

    validateWorkflowEntitlementForm( form, ignoreIndex = null ) {
        let valid = true;

        const {
            workflow_id,
            request_type
        } = form;

        if ( !workflow_id._checkRequire( workflow_id.state.value ) ) {
            valid = false;
        }

        if ( !request_type._checkRequire( request_type.state.value ) ) {
            valid = false;
        }

        const foundRequestType = find( this.state.workflowEntitlements, ( entitlement, i ) =>
            i !== ignoreIndex && entitlement.request_type === request_type.state.value
        );

        // Validate that there are no duplicate request types
        if ( foundRequestType ) {
            request_type.setState({
                error: true,
                errorMessage: 'Workflow for this request type has already been chosen.'
            });

            valid = false;
        }

        return valid;
    }

    updateNewWorkflowEntitlement( field, value ) {
        this.setState({
            workflowEntitlementForm: {
                ...this.state.workflowEntitlementForm,
                [ field ]: value
            }
        });
    }

    updateWorkflowEntitlementAtIndex = ( index, field, value, onSetStateFinish ) => {
        this.setState({
            workflowEntitlements: [
                ...this.state.workflowEntitlements.slice( 0, index ),
                {
                    ...this.state.workflowEntitlements[ index ],
                    [ field ]: value
                },
                ...this.state.workflowEntitlements.slice( index + 1 )
            ]
        }, onSetStateFinish );
    }

    removeWorkflowEntitlementAtIndex( index ) {
        this.setState({
            workflowEntitlements: [
                ...this.state.workflowEntitlements.slice( 0, index ),
                ...this.state.workflowEntitlements.slice( index + 1 )
            ]
        });
    }

    handleAffectedEmployeesChange = ( affectedEmployees ) => {
        this.setState({
            affectedEmployees
        });
    }

    loadEmployees = ( keyword, callback ) => {
        const { companyId } = this.state;

        // 'http://gw-api.local.salarium.test/company/12074/employees?keyword=JOHN%20BENEDICT'
        Fetch( `/company/${companyId}/employees?keyword=${keyword}`, { method: 'GET' })
          .then( ( result ) => {
              const employees = result.data.map( ({ id, full_name }) => ({
                  value: id,
                  label: full_name,
                  field: 'employee',
                  disabled: false
              }) );
              callback( null, { options: employees });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    loadWorkflows = ( keyword, callback ) => {
        const { companyId, selectedWorkflowName } = this.state;

        const term = keyword || selectedWorkflowName;

        // 'http://gw-api.local.salarium.test/company/12074/workflows?term=Multiple%20Approver%20in%201%20Level'

        Fetch( `/company/${companyId}/workflows?term=${term}`, { method: 'GET' })
          .then( ( result ) => {
              const workflow = result.data.map( ({ id, name }) => ({
                  value: id,
                  label: name,
                  disabled: false
              }) );
              callback( null, { options: workflow });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    addWorkflowEntitlement = () => {
        if ( this.validateWorkflowEntitlementForm( this.addWorkflowEntitlementForm ) ) {
            this.setState({
                workflowEntitlements: this.state.workflowEntitlements.concat([
                    this.state.workflowEntitlementForm
                ]),
                workflowEntitlementForm: {
                    workflow_id: null,
                    request_type: null
                },
                workflowLabel: null
            });
        }
    }

    prepareAffectedEmployees() {
        return this.state.affectedEmployees.map( ( affectedEmployee ) => ({
            id: affectedEmployee.value,
            type: affectedEmployee.field
        }) );
    }

    prepareDataForSubmit() {
        return {
            affected_employees: this.prepareAffectedEmployees(),
            workflow_request_type_pairs: this.state.workflowEntitlements
        };
    }

    checkForPendingRequests = () => {
        if ( this.validateWorkflowEntitlementData() ) {
            this.submitButton.setState({ disabled: true });

            this.props.checkHasPendingRequestsManual(
                this.prepareDataForSubmit(),
                this.submitForm
            );
        }
    }

    submitForm = () => {
        if ( this.validateWorkflowEntitlementData() ) {
            this.props.submitForm( this.prepareDataForSubmit() );
        }
    }

    renderAddedWorkflowEntitlements() {
        this.workflowEntitlementForms = this.state.workflowEntitlements.map( () => ({
            workflow_id: null,
            request_type: null
        }) );

        return this.state.workflowEntitlements.map( ( workflowEntitlement, index ) => (
            <div className="row" key={ `workflowEntitlementForms-${index}` }>
                <div className="col-xs-6">
                    <SalSelect
                        async
                        id="workflow_id"
                        label="Workflow"
                        required
                        key="name"
                        loadOptions={ this.loadWorkflows }
                        value={ workflowEntitlement.workflow_id }
                        placeholder="Select your desired workflow."
                        ref={ ( ref ) => {
                            if ( this.workflowEntitlementForms[ index ]) {
                                this.workflowEntitlementForms[ index ].workflow_id = ref;
                            }
                        } }
                        onChange={ ({ value }) => { this.updateWorkflowEntitlementAtIndex( index, 'workflow_id', value ); } }
                    />
                </div>
                <div className="col-xs-4">
                    <SalSelect
                        id="request_type"
                        label="Request Type"
                        required
                        key="name"
                        data={ this.getRequestTypesOptions() }
                        value={ workflowEntitlement.request_type }
                        placeholder="Select Request Type"
                        ref={ ( ref ) => {
                            if ( this.workflowEntitlementForms[ index ]) {
                                this.workflowEntitlementForms[ index ].request_type = ref;
                            }
                        } }
                        onChange={ ({ value }) => { this.updateWorkflowEntitlementAtIndex( index, 'request_type', value ); } }
                    />
                </div>
                <div className="col-xs-2 action remove-action">
                    <Button
                        label="Remove"
                        type="neutral"
                        size="large"
                        onClick={ () => {
                            this.removeWorkflowEntitlementAtIndex( index );
                        } }
                    />
                </div>
            </div>
            ) );
    }

    renderNewWorkflowEntitlementForm() {
        const workflowName = this.state.workflowLabel;

        return (
            <div className="row">
                <div className="col-xs-6">
                    {/* <SalSelect
                        id="workflow_id"
                        label="Workflow"
                        required
                        key="name"
                        data={ this.getWorkflowsOptions() }
                        value={ this.state.workflowEntitlementForm.workflow_id }
                        placeholder="Select your desired workflow."
                        ref={ ( ref ) => { this.addWorkflowEntitlementForm.workflow_id = ref; } }
                        onChange={ ({ value }) => { this.updateNewWorkflowEntitlement( 'workflow_id', value ); } }
                    /> */}
                    <SalSelect
                        async
                        id="workflow_id"
                        label="Workflow"
                        required
                        key="name"
                        loadOptions={ this.loadWorkflows }
                        value={ workflowName ? { label: workflowName, value: this.state.workflowEntitlementForm.workflow_id } : null }
                        placeholder="Select your desired workflow."
                        ref={ ( ref ) => { this.addWorkflowEntitlementForm.workflow_id = ref; } }
                        onChange={ ({ value, label }) => { this.updateNewWorkflowEntitlement( 'workflow_id', value ); this.setState({ selectedWorkflowName: label, workflowLabel: label }); } }
                    />
                </div>
                <div className="col-xs-4">
                    <SalSelect
                        id="request_type"
                        label="Request Type"
                        required
                        key="name"
                        data={ this.getRequestTypesOptions() }
                        value={ this.state.workflowEntitlementForm.request_type }
                        placeholder="Select Request Type"
                        ref={ ( ref ) => { this.addWorkflowEntitlementForm.request_type = ref; } }
                        onChange={ ({ value }) => { this.updateNewWorkflowEntitlement( 'request_type', value ); } }
                    />
                </div>
                <div className="col-xs-2 action">
                    <Button
                        label="Add"
                        type="neutral"
                        size="large"
                        onClick={ this.addWorkflowEntitlement }
                    />
                </div>
            </div>
        );
    }

    renderFooter() {
        return (
            <Footer>
                <Button
                    label="Cancel"
                    type="neutral"
                    size="large"
                    onClick={ () => {
                        browserHistory.push( '/time/workflow-entitlements', true );
                    } }
                />
                <Button
                    label={ this.props.submitted ? <Loader /> : 'Submit' }
                    type="action"
                    size="large"
                    onClick={ this.checkForPendingRequests }
                    ref={ ( ref ) => { this.submitButton = ref; } }
                />
            </Footer>
        );
    }

    render() {
        return (
            <div>
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <MainWrapper>
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/time/workflow-entitlements/add?batch', true ); } }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showBatchModal }
                    />
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/time/workflow-entitlements', true ); } }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showWarningModal }
                    />
                    <SalConfirm
                        onConfirm={ this.submitForm }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    Saving changes. Pending requests in this changed workflows will be reset.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.props.hasPendingRequests }
                    />
                    <Container>
                        <div className="row">
                            <div className="col-xs-12">
                                <MultiSelect
                                    async
                                    id="employees"
                                    label="Employees"
                                    required
                                    placeholder="Type in employee name"
                                    loadOptions={ this.loadEmployees }
                                    value={ this.state.affectedEmployees }
                                    onChange={ this.handleAffectedEmployeesChange }
                                    ref={ ( ref ) => { this.affectedEmployees = ref; } }
                                />
                            </div>
                        </div>

                        { this.renderAddedWorkflowEntitlements() }

                        { this.renderNewWorkflowEntitlementForm() }
                    </Container>

                    { this.renderFooter() }
                </MainWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    formOptions: makeSelectFormOptions(),
    submitted: makeSelectSubmitted(),
    errors: makeSelectErrors(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    hasPendingRequests: makeSelectHasPendingRequests()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createWorkflowEntitlementActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ManualEntry );
