import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';

import { subscriptionService } from '../../../utils/SubscriptionService';

import {
    GET_WORKFLOW_ENTITLEMENTS_PREVIEW,
    GET_HAS_PENDING_REQUESTS_BATCH,
    GET_HAS_PENDING_REQUESTS_MANUAL,
    SET_HAS_PENDING_REQUESTS,
    INITIALIZE,
    LOADING,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SAVE_WORKFLOW_ENTITLEMENTS,
    SET_BATCH_UPLOAD_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_WORKFLOW_ENTITLEMENTS_PREVIEW_DATA,
    SET_WORKFLOW_ENTITLEMENTS_PREVIEW_STATUS,
    SET_ERRORS,
    SET_FORM_OPTIONS,
    SET_SAVING_ERRORS,
    SET_SAVING_STATUS,
    SUBMITTED,
    SUBMIT_FORM,
    UPLOAD_WORKFLOW_ENTITLEMENTS,
    REQUEST_TYPES,
    IS_LOADING
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formOptions = {
            workflows: [],
            requestTypes: []
        };

        const requestTypesResponse = yield call( Fetch, '/employee_request_module', { method: 'GET' });
        const departmentsResponse = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });
        const positionsResponse = yield call( Fetch, `/company/${companyId}/positions`, { method: 'GET' });
        const locationsResponse = yield call( Fetch, `/philippine/company/${companyId}/locations`, { method: 'GET' });

        if ( subscriptionService.isSubscribedToTAOnly( payload ) ) {
            formOptions.requestTypes = requestTypesResponse.data.filter( ( type ) => (
                type.name !== REQUEST_TYPES.LOAN
            ) );
        } else {
            formOptions.requestTypes = requestTypesResponse.data;
        }

        formOptions.departments = departmentsResponse.data;
        formOptions.positions = positionsResponse.data;
        formOptions.locations = locationsResponse.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield put({
            type: SET_HAS_PENDING_REQUESTS,
            payload: false
        });

        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        const data = Object.assign({}, payload );
        data.company_id = company.getLastActiveCompanyId();

        yield call( Fetch, '/workflow_entitlement/bulk_create_or_update', { method: 'POST', data });
        yield call( delay, 500 );

        yield call( browserHistory.push, '/time/workflow-entitlements', true );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Check if there are pending requests for workflows that are being updated
 */
export function* checkHasPendingRequestsManual({ payload, callback }) {
    try {
        yield put({
            type: SET_HAS_PENDING_REQUESTS,
            payload: false
        });

        const companyId = company.getLastActiveCompanyId();
        const url = '/workflow_entitlement/has_pending_requests';
        const data = {
            company_id: companyId,
            ...payload
        };

        const response = yield call( Fetch, url, { method: 'POST', data });

        if ( response.has_pending_requests ) {
            yield put({
                type: SET_HAS_PENDING_REQUESTS,
                payload: true
            });
        } else {
            yield call( callback );
        }
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Uploads the CSV of workfloe entitlements to add and starts the validation process
 */
export function* uploadWorkflowEntitlements({ payload }) {
    try {
        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: {}
        });

        yield put({
            type: SET_WORKFLOW_ENTITLEMENTS_PREVIEW_DATA,
            payload: []
        });

        const data = new FormData();
        const companyId = company.getLastActiveCompanyId();

        data.append( 'file', payload.file );
        data.append( 'company_id', companyId );

        const upload = yield call( Fetch, '/workflow_entitlement/upload', {
            method: 'POST',
            data
        });

        if ( upload.id ) {
            yield put({
                type: SET_BATCH_UPLOAD_JOB_ID,
                payload: upload.id
            });

            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: 'validation_queued'
            });

            yield call( checkValidation, { payload: { jobId: upload.id, step: 'validation' }});
        }
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();
        const check = yield call(
            Fetch,
            `/workflow_entitlement/upload/status?job_id=${payload.jobId}&step=${payload.step}&company_id=${companyId}`,
            { method: 'GET' }
        );

        if ( payload.step === 'validation' ) {
            if ( check.status === 'validation_failed' ) {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_WORKFLOW_ENTITLEMENTS_PREVIEW_DATA,
                    payload: []
                });

                yield put({
                    type: SET_BATCH_UPLOAD_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'validated' ) {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_BATCH_UPLOAD_ERRORS,
                    payload: {}
                });

                yield call( getWorkflowEntitlementsPreview, { payload: { jobId: payload.jobId }});
            } else {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield call( delay, 2000 );
                yield call( checkValidation, { payload: { jobId: payload.jobId, step: 'validation' }});
            }
        } else if ( payload.step === 'save' ) {
            if ( check.status === 'save_failed' ) {
                yield put({
                    type: SET_SAVING_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_SAVING_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'saved' ) {
                yield put({
                    type: SET_SAVING_STATUS,
                    payload: check.status
                });

                yield call( browserHistory.push, '/time/workflow-entitlements', true );
            } else {
                yield call( delay, 2000 );
                yield call( checkValidation, { payload });
            }
        }
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Fetches preview for uploaded workflow entitlements
 */
export function* getWorkflowEntitlementsPreview({ payload }) {
    try {
        yield put({
            type: SET_WORKFLOW_ENTITLEMENTS_PREVIEW_DATA,
            payload: []
        });

        yield put({
            type: SET_WORKFLOW_ENTITLEMENTS_PREVIEW_STATUS,
            payload: 'busy'
        });

        const preview = yield call(
            Fetch,
            `/workflow_entitlement/upload/preview?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}`,
            { method: 'GET' }
        );

        yield put({
            type: SET_WORKFLOW_ENTITLEMENTS_PREVIEW_DATA,
            payload: preview
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_WORKFLOW_ENTITLEMENTS_PREVIEW_STATUS,
            payload: 'ready'
        });
    }
}

/**
 * Saves validated workflow entitlements
 */
export function* saveWorkflowEntitlements({ payload }) {
    try {
        yield put({
            type: SET_HAS_PENDING_REQUESTS,
            payload: false
        });

        const upload = yield call( Fetch, '/workflow_entitlement/upload/save', {
            method: 'POST',
            data: {
                company_id: company.getLastActiveCompanyId(),
                job_id: payload.jobId
            }
        });

        if ( upload.id ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: 'save_queued'
            });

            yield call( checkValidation, { payload: {
                step: 'save',
                jobId: payload.jobId
            }});
        }
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Check if there are pending requests for workflows that are being updated
 */
export function* checkHasPendingRequestsBatchUpload({ payload, callback }) {
    try {
        yield put({
            type: SET_HAS_PENDING_REQUESTS,
            payload: false
        });

        const companyId = company.getLastActiveCompanyId();
        const url = `/workflow_entitlement/upload/has_pending_requests?company_id=${companyId}&job_id=${payload.jobId}`;

        const response = yield call( Fetch, url, {
            method: 'GET'
        });

        if ( response.has_pending_requests ) {
            yield put({
                type: SET_HAS_PENDING_REQUESTS,
                payload: true
            });
        } else {
            yield call( callback );
        }
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

// eslint-disable-next-line require-jsdoc
export function setIsLoading( payload ) {
    return {
        type: IS_LOADING,
        payload
    };
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPLOAD_WORKFLOW_ENTITLEMENTS
 */
export function* watchForUploadWorkflowEntitlements() {
    const watcher = yield takeEvery( UPLOAD_WORKFLOW_ENTITLEMENTS, uploadWorkflowEntitlements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_WORKFLOW_ENTITLEMENTS_PREVIEW
 */
export function* watchForGetWorkflowEntitlementsPreview() {
    const watcher = yield takeEvery( GET_WORKFLOW_ENTITLEMENTS_PREVIEW, getWorkflowEntitlementsPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SAVE_WORKFLOW_ENTITLEMENTS
 */
export function* watchForSaveWorkflowEntitlements() {
    const watcher = yield takeEvery( SAVE_WORKFLOW_ENTITLEMENTS, saveWorkflowEntitlements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_HAS_PENDING_REQUESTS
 */
export function* watchForCheckHasPendingRequestsBatchUpload() {
    const watcher = yield takeEvery( GET_HAS_PENDING_REQUESTS_BATCH, checkHasPendingRequestsBatchUpload );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_HAS_PENDING_REQUESTS
 */
export function* watchForCheckHasPendingRequestsManual() {
    const watcher = yield takeEvery( GET_HAS_PENDING_REQUESTS_MANUAL, checkHasPendingRequestsManual );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForGetWorkflowEntitlementsPreview,
    watchForUploadWorkflowEntitlements,
    watchForCheckHasPendingRequestsBatchUpload,
    watchForCheckHasPendingRequestsManual,
    watchForInitializeData,
    watchForReinitializePage,
    watchForSaveWorkflowEntitlements,
    watchNotify
];
