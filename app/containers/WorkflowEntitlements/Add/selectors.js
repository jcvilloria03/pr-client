import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectAddWorkflowEntitlementDomain = () => ( state ) => state.get( 'addWorkflowEntitlement' );

const makeSelectLoading = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectIsLoading = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => substate.get( 'isLoading' )
);

const makeSelectFormOptions = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectWorkflowEntitlementsPreview = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => substate.get( 'workflowEntitlementsPreview' ).toJS()
);

const makeSelectSaving = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

const makeSelectHasPendingRequests = () => createSelector(
    selectAddWorkflowEntitlementDomain(),
    ( substate ) => substate.get( 'hasPendingRequests' )
);

export {
    makeSelectBatchUploadErrors,
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectWorkflowEntitlementsPreview,
    makeSelectErrors,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectIsLoading,
    makeSelectNotification,
    makeSelectSaving,
    makeSelectSubmitted,
    makeSelectHasPendingRequests
};
