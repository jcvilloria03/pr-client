import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../components/A';
import SnackBar from '../../../components/SnackBar';
import { H2, H3 } from '../../../components/Typography';
import SubHeader from '../../../containers/SubHeader';

import { browserHistory } from '../../../utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import BatchUpload from './BatchUpload';
import ManualEntry from './ManualEntry';

import { makeSelectNotification, makeSelectIsLoading } from './selectors';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper,
    LoadingStyles
} from './styles';
import { setIsLoading } from './actions';

const TOGGLE_OPTIONS = {
    BATCH: 'BATCH',
    MANUAL: 'MANUAL'
};

/**
 * Add Loans Component
 */
class Add extends Component {
    static propTypes = {
        location: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        isLoading: React.PropTypes.bool,
        setIsLoading: React.PropTypes.func
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        const selectedToggleOption = props.location.query.batch !== undefined
            ? TOGGLE_OPTIONS.BATCH
            : TOGGLE_OPTIONS.MANUAL;

        this.state = {
            selectedToggleOption
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    componentDidUpdate( prevProps ) {
        if ( this.props.isLoading !== prevProps.isLoading ) {
            this.props.setIsLoading( this.state.loading );
        }
    }

    isBatchAdd() {
        return this.state.selectedToggleOption === TOGGLE_OPTIONS.BATCH;
    }

    /**
     * Component Render Method
     */
    render() {
        const { selectedToggleOption } = this.state;

        return (
            <PageWrapper>
                <Helmet
                    title={ this.isBatchAdd() ? 'Batch Assign Workflows' : 'Assign Workflow' }
                    meta={ [
                        { name: 'description', content: this.isBatchAdd() ? 'Batch Assign Workflows' : 'Assign Workflow' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/workflow-entitlements', true );
                            } }
                        >
                            &#8592; Back to Workflows
                        </A>
                    </Container>
                </NavWrapper>
                { this.props.isLoading ? <LoadingStyles>
                    <H2>Loading Assigned Workflows.</H2>
                    <br />
                    <H3>Please wait...</H3>
                </LoadingStyles> : null }
                <div className={ this.props.isLoading ? 'hide' : '' }>
                    <Container>
                        <HeadingWrapper>
                            <h3>{ this.isBatchAdd() ? 'Add Batch Assigned Workflows' : 'Assign Workflow' }</h3>
                        </HeadingWrapper>
                    </Container>
                    <FormWrapper>
                        { selectedToggleOption === TOGGLE_OPTIONS.BATCH
                            ? <BatchUpload />
                            : <ManualEntry setIsLoading={ setIsLoading } products={ this.props.products } />
                        }
                    </FormWrapper>
                </div>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    isLoading: makeSelectIsLoading()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        {},
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Add );
