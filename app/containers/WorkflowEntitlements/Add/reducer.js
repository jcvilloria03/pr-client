import { fromJS } from 'immutable';
import {
    LOADING,
    SET_FORM_OPTIONS,
    SET_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    RESET_WORKFLOW_ENTITLEMENTS_PREVIEW,
    SET_WORKFLOW_ENTITLEMENTS_PREVIEW_STATUS,
    SET_WORKFLOW_ENTITLEMENTS_PREVIEW_DATA,
    SET_SAVING_STATUS,
    SET_SAVING_ERRORS,
    SET_HAS_PENDING_REQUESTS,
    SUBMITTED,
    NOTIFICATION_SAGA,
    IS_LOADING
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    errors: {},
    submitted: false,
    batchUploadJobId: null,
    batchUploadStatus: '',
    batchUploadErrors: {},
    hasPendingRequests: false,
    workflowEntitlementsPreview: {
        status: 'ready',
        data: []
    },
    saving: {
        status: '',
        errors: {}
    },
    formOptions: {
        workflows: [],
        requestTypes: [],
        departments: [],
        employees: [],
        positions: [],
        locations: []
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    isLoading: true
});

/**
 *
 * Manual or batch assign workflowEntitlements
 *
 */
function addWorkflowEntitlement( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case IS_LOADING:
            return state.set( 'isLoading', action.payload );
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_BATCH_UPLOAD_JOB_ID:
            return state.set( 'batchUploadJobId', action.payload );
        case SET_BATCH_UPLOAD_STATUS:
            return state.set( 'batchUploadStatus', action.payload );
        case SET_BATCH_UPLOAD_ERRORS:
            return state.set( 'batchUploadErrors', action.payload );
        case SET_WORKFLOW_ENTITLEMENTS_PREVIEW_STATUS:
            return state.setIn([ 'workflowEntitlementsPreview', 'status' ], action.payload );
        case SET_WORKFLOW_ENTITLEMENTS_PREVIEW_DATA:
            return state.setIn([ 'workflowEntitlementsPreview', 'data' ], fromJS( action.payload ) );
        case SET_SAVING_STATUS:
            return state.setIn([ 'saving', 'status' ], action.payload );
        case SET_SAVING_ERRORS:
            return state.setIn([ 'saving', 'errors' ], action.payload );
        case SET_HAS_PENDING_REQUESTS:
            return state.set( 'hasPendingRequests', action.payload );
        case RESET_WORKFLOW_ENTITLEMENTS_PREVIEW:
            return state.set( 'preview', fromJS({
                status: 'ready',
                data: []
            }) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addWorkflowEntitlement;
