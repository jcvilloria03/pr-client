import {
    INITIALIZE,
    SUBMIT_FORM,
    UPLOAD_WORKFLOW_ENTITLEMENTS,
    SAVE_WORKFLOW_ENTITLEMENTS,
    GET_HAS_PENDING_REQUESTS_BATCH,
    GET_HAS_PENDING_REQUESTS_MANUAL,
    NOTIFICATION,
    IS_LOADING
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData( payload ) {
    return {
        type: INITIALIZE,
        payload
    };
}

/**
 * Display notification in page
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Submit a new workflow entitlement
 * @param {object} payload
 */
export function submitForm( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}

/**
 * Upload workflow entitlements
 * @param {object} payload
 */
export function uploadWorkflowEntitlements( payload ) {
    return {
        type: UPLOAD_WORKFLOW_ENTITLEMENTS,
        payload
    };
}

/**
 * Save workflow entitlements
 * @param {object} payload
 */
export function saveWorkflowEntitlements( payload ) {
    return {
        type: SAVE_WORKFLOW_ENTITLEMENTS,
        payload
    };
}

/**
 * Check if there would be some pending requests that for updated workflows
 * @param {object} payload
 * @param {function} callback
 */
export function checkHasPendingRequestsBatchUpload( payload, callback ) {
    return {
        type: GET_HAS_PENDING_REQUESTS_BATCH,
        payload,
        callback
    };
}

/**
 * Check if there would be some pending requests that for submitting form.
 * @param {object} payload
 * @param {function} callback
 */
export function checkHasPendingRequestsManual( payload, callback ) {
    return {
        type: GET_HAS_PENDING_REQUESTS_MANUAL,
        payload,
        callback
    };
}

// eslint-disable-next-line require-jsdoc
export function setIsLoading( payload ) {
    return {
        type: IS_LOADING,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
