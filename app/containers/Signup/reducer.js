import { fromJS } from 'immutable';

import {
    SET_LOADING_SUBSCRIPTION_OPTIONS,
    SET_SUBSCRIPTION_OPTIONS,
    SET_NOTIFICATION,
    SET_SUBMITTING,
    SET_LOADING
} from './constants';

const initialState = fromJS({
    loading: false,
    submitting: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    loadingSubscriptionOptions: false,
    subscriptionOptions: []
});

/**
 * transforms data from API to accomodate structure in app
 * @param {Array} data - Subscription options from endpoint
 */
function prepareSubscriptionOptions( data ) {
    return data.map( ( option ) => ({
        value: option.id,
        label: option.name
    }) );
}

/**
 * Sign Up Reducer
 */
function SignUpReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING_SUBSCRIPTION_OPTIONS:
            return state.set( 'loadingSubscriptionOptions', action.payload );
        case SET_SUBSCRIPTION_OPTIONS:
            return state.set( 'subscriptionOptions', fromJS( prepareSubscriptionOptions( action.payload ) ) );
        case SET_SUBMITTING:
            return state.set( 'submitting', action.payload );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default SignUpReducer;
