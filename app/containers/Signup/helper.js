import React from 'react';
import {
  StyledPasswordWrapper
} from './styles';

export const passwordChecker = ( value ) => {
    const valueCount = value.length;
    const capsCount = ( value.match( /[A-Z]/g ) || []).length;
    const smallCount = ( value.match( /[a-z]/g ) || []).length;
    const numberCount = ( value.match( /[0-9]/g ) || []).length;
    return (
        <StyledPasswordWrapper>
            <p>
                <span style={ { border: `2px solid ${valueCount > 7 ? 'green' : 'red'}` } } />
                At least 8 characters
            </p>
            <p>
                <span style={ { border: `2px solid ${smallCount ? 'green' : 'red'}` } } />
                Contain at least 1 lower letter (a-z)
            </p>
            <p>
                <span style={ { border: `2px solid ${capsCount ? 'green' : 'red'}` } } />
                Contain at least 1 upper case letter (A-Z)
            </p>
            <p>
                <span style={ { border: `2px solid ${numberCount ? 'green' : 'red'}` } } />
                Contain at least 1 number (0-9)
            </p>
        </StyledPasswordWrapper>
    );
};
