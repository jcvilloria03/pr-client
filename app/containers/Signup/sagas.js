import { takeEvery, delay } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';

import {
    GET_SUBSCRIPTION_OPTIONS,
    SUBMIT_FORM
} from './constants';
import {
    setLoadingSubscriptionOptions,
    setSubscriptionOptions,
    setNotification,
    setSubmitting,
    setLoading
} from './actions';

/**
 * Fetches subscription options
 */
export function* getSubscriptionOptions() {
    try {
        yield put( setLoadingSubscriptionOptions( true ) );
        const { data } = yield call( Fetch, '/subscriptions/plans' );

        yield put( setSubscriptionOptions( data ) );
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put( setLoadingSubscriptionOptions( false ) );
    }
}

/**
 * Submits form
 * @param {Object} action.payload - Sign Up form
 */
export function* submitForm({ payload }) {
    yield put( setSubmitting( true ) );
    yield put( setLoading( true ) );

    try {
        yield call( auth.signup, payload );
    } catch ( error ) {
        yield [
            call( notifyUser, error ),
            put( setSubmitting( false ) ),
            put( setLoading( false ) )
        ];
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    let message = error.response.data.message;
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: 'error'
    };

    if ( message === 'Account already exists' ) {
        message = 'The email address you have entered is already in use.';
    } else {
        message = get( error, 'message', get( error, 'response.data.message', error.statusText ) );
    }

    yield put( setNotification( emptyNotification ) );

    const payload = {
        show: true,
        title: get( error, 'title', get( error, 'response.statusText', 'Error' ) ),
        message,
        type: 'error'
    };
    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Watcher for GET_SUBSCRIPTION_OPTIONS
 */
export function* watchForGetSubscriptionOptions() {
    const watcher = yield takeEvery( GET_SUBSCRIPTION_OPTIONS, getSubscriptionOptions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetSubscriptionOptions,
    watchForSubmitForm
];
