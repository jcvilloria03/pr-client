import styled from 'styled-components';

export const StyledWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: center;
`;

export const StyledSliderWrapper = styled.div`
    margin: auto 0;
    position: sticky;
    top: 1px;
    width: 50%;
    padding: 20px;

    @media (max-width: 480px) {
        display: none;
    }
`;

export const StyledFormWrapper = styled.div`
    min-height: 100vh;
    width: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    color: black;
    padding: 20px;
    background-position: bottom;
    background-size: contain;

    @media (max-width: 480px) {
        width: 100%;
        min-width: 0;
    }

    #brand {
        width: 360px;
        text-align: center;

        img {
            width: 246px;
            height: 72px;
        }

        h5 {
            color: #212E57;
            font-size: 36px;
            letter-spacing: 1.25px;
            line-height: 175%;
        }
    }
`;

export const StyledForm = styled.form`
    width: 400px;

    @media (max-width: 480px) {
        width: 100%;
        min-width: 0;

        .form-input {
            width: max-conten;
        }
    }

    .label-text {
        display: block;
        color: black;
        font-size: 14px;
        line-height: 175%;
        letter-spacing: 1.3px;
    }

    .form-input {
        background: #FFFFFF;
        border: 1px solid #48A0DE;
        box-sizing: border-box;
        border-radius: 4px;
        height: 48px;
        width: 400px;
        margin-bottom: 20px;
        padding: 12px 16px;
        color: #2d2d2d;

        @media (max-width: 480px) {
            width: 100%;
        }
    }

    .form-error {
        color: #f21108;
        font-size: 13px;
    }

    .row {
        margin-bottom: 0;
    }
    
    .form-names {
        width: 185px;

        @media (max-width: 480px) {
            width: 100%;
        }
    }

    .captcha-container {
        margin-top: 24px;
    }

    .select-container {
        > div > .Select {
            > .Select-control {
                border-radius: 4px !important;
                border: 1px solid #48A0DE;
            }
        }
    }

    .footer-container {
        display: flex;
        justify-content:center;
        align-items: center;
        flex-direction: column;
        text-align: left;

        .policy-container {
            display: inherit;
            flex-direction: row;
            justify-content: flex-start;
            align-items: flex-start;

            .checkbox {
                margin: .5em
            }
        }
    }

    .signup-btn {
        margin: 2em;
    }

    .password-container {
        position: relative;
    }

    .reveal-password {
        right: 0;
        position: absolute;
        margin-right: 18px;
        margin-top: 10px;
    }

    .captcha-container {
        display: block;
    }

    .sl-c-card__header {
        margin: 0 1rem;
        padding: 1rem 0;
        border-bottom: 1px solid #ddd;
    }

    .sl-c-card__body {
        width: 400px;

        @media (max-width: 480px) {
            width: 100%;
            padding: 0;
        }
    }
    
    .sl-c-card__footer {
        padding: 0 3rem 1rem 3rem;
        text-align: right;

        .btn {
            padding-right: 2.75rem;
            padding-left: 2.75rem;
        }
    }

    .sl-c-form-group {
        &:not(:last-child) {
            margin-bottom: 2rem;
        }
    }

    .form-control, .Select > .Select-control {
        border-color: #ddd;
    }

    #submit {
        text-align: center;

        button {
            padding: 12px 32px;
            font-size: 18px;
            border-radius: 40px;
            font-weight: 400;
            display: block;
            margin: 0 auto 2rem;
        }

        p {
            font-size: 12px;
            color: #999;
            margin: 0;
        }
    }
`;

export const StyledPasswordWrapper = styled.div`
    border: 1px solid #48A0DE; 
    border-radius: 4px; 
    padding: 1rem; 
    margin-bottom: 20px;

    .color-text {
        color: green;
    }

    p {
        display: flex; 
        align-items: center ;
        margin-bottom: 0;
    }

    span {
        margin-right: 1rem; 
        font-size: 10px;
        height: 15px;
    }
`;
