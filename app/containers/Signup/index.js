/* eslint-disable react/sort-comp */
/* eslint-disable import/first */
import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import ReCAPTCHA from 'react-google-recaptcha';

import { browserHistory } from 'utils/BrowserHistory';
import { auth } from 'utils/AuthService';
import FeatureSlides from 'components/FeatureSlides';
import A from 'components/A';
import Button from 'components/Button';
import SnackBar from 'components/SnackBar';
import Icon from 'components/Icon';
import Loader from 'components/Loader';
import { passwordChecker } from './helper';
import TagManager from 'react-gtm-module';
import ReactGA from 'react-ga';

import {
    TRACKING_ID,
    TAG_MANAGER_ARGS
} from '../../constants';

import {
    StyledWrapper,
    StyledSliderWrapper,
    StyledFormWrapper,
    StyledForm
} from './styles';
import {
    getSubscriptionOptions,
    submitForm
} from './actions';
import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSubmitting,
    makeSelectLoadingSubscriptionOptions,
    makeSelectSubscriptionOptions
} from './selectors';

/**
 *
 * Signup
 *
 */
export class Signup extends React.PureComponent {
    static propTypes = {
        loading: PropTypes.bool,
        getSubscriptionOptions: PropTypes.func.isRequired,
        submitForm: PropTypes.func.isRequired,
        notification: PropTypes.object.isRequired,
        subscriptionOptions: PropTypes.array.isRequired
    }

    /**
     * Component constructor method
     */
    constructor( props ) {
        super( props );
        this.state = {
            disabledSubmit: false,
            submit: false,
            showPassword: false,
            email: '',
            firstName: '',
            lastName: '',
            accountName: '',
            password: '',
            errorMessage: {
                firstName: '',
                lastName: ''
            },
            showPasswordChecker: false,
            submitted: false,
            captcha: false,
            checked: false,
            isValid: true,
            featuredSlides: [
                {
                    image: 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/Asset+1200.png',
                    title: 'No strings attached, just a free trial',
                    description: 'It’s totally free for 30 days. No credit card needed.'
                },
                {
                    image: 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/Asset%2B2200-1.png',
                    title: 'One Payroll Solution to Rule Them All',
                    description: 'Find everything you need from clock-in to payout.'
                },
                {
                    image: 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/Asset%2B3200-1.png',
                    title: 'Not everything in life is free but we are for 30 days',
                    description: 'Unlock the full potential of our payroll system without breaking the bank. '
                }
            ]
        };
        this._handleSubmit = this._handleSubmit.bind( this );
        this.setShowPassword = this.setShowPassword.bind( this );
        this.validateInput = this.validateInput.bind( this );
        this.handleCaptcha = this.handleCaptcha.bind( this );
        this.handleCheckbox = this.handleCheckbox.bind( this );
    }

    /**
     * checks if there are any existing token and redirects to dashboard if its still valid
     */
    componentWillMount() {
        const hasValidToken = auth.loggedIn();
        hasValidToken && browserHistory.replace( '/dashboard', true );
    }

    /**
     * Fetch subscription options
     */
    componentDidMount() {
        window.location.replace( 'https://www.salarium.com/sign-up' );
        this.initGTags();
        this.props.getSubscriptionOptions();
    }

    componentWillReceiveProps( nextProps ) {
        /**
         * Assign default selected value for subscription
         */
        if ( this.props.subscriptionOptions.length !== nextProps.subscriptionOptions.length ) {
            this.subscriptionSelect && this.subscriptionSelect.setState({
                value: nextProps.subscriptionOptions.find( ( option ) => option.value === 3 )
            });
        }
    }

    initGTags = () => {
        TagManager.initialize( TAG_MANAGER_ARGS );
        ReactGA.initialize( TRACKING_ID );
        ReactGA.pageview( window.location.pathname );
    };

    /**
     * Fetches value of Form fields
     * @returns {Object}
     */
    getSignUpForm() {
        const { accountName, email, password, firstName, lastName } = this.state;

        const plan = this.props.subscriptionOptions
            && this.props.subscriptionOptions.find( ( option ) => option.value === 3 );

        return {
            name: accountName,
            first_name: firstName,
            last_name: lastName,
            company: accountName,
            email,
            password,
            plan_id: plan && plan.value
        };
    }

    /**
     * This validates the login form and enables or disables the submit button.
     * @private
     */

    setShowPassword( e ) {
        e.stopPropagation();
        this.setState({ showPassword: !this.state.showPassword });
    }

    _handleSubmit() {
        const fields = [ 'accountName', 'email', 'password', 'firstName', 'lastName' ];

        this.setState({ submitted: true }, () => {
            fields.map( ( field ) => this.validateInput( field ) );
        });

        if ( this._checkForm( fields ) === -1 ) {
            this.props.submitForm( this.getSignUpForm() );
        }
    }

    /**
     * This validates the form fields and enables or disables the submit button.
     * @private
     */
    _checkForm = ( fields ) => (
        fields.findIndex( ( field ) => !this.state[ field ])
    )

    handleChange( value, id, state ) {
        this.validateInput( id );
        this.setState({ [ state ]: value });
    }

    validateInput( id ) {
        const el = document.getElementById( id );
        const inputRegex = {
            email: /\S+@\S+\.\S+/,
            password: /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}$/,
            name: /^[a-zA-Z\s'-.]+$/
        };

        if ( el ) {
            if ( el.value && [ 'password', 'email' ].includes( id ) ) {
                if ( inputRegex[ id ].test( el.value ) ) {
                    if ( id === 'password' && el.value ) this.setState({ showPasswordChecker: false });
                    el.style.borderColor = '#00A5E5';
                } else {
                    if ( id === 'password' && el.value ) this.setState({ showPasswordChecker: true });
                    el.style.borderColor = 'red';
                }
            } else if ( el.value && [ 'firstName', 'lastName' ].includes( id ) ) {
                if ( !inputRegex.name.test( el.value ) ) {
                    el.style.borderColor = 'red';

                    const name = id.charAt( 0 ).toUpperCase() + id.slice( 1 );
                    this.setState( ( prevState ) => ({
                        errorMessage: {
                            ...prevState.errorMessage,
                            [ id ]: `${name.replace( /([a-z])([A-Z])/g, '$1 $2' )} accepts letters, hypens, periods, apostrophes, and spaces only`
                        }
                    }) );
                } else {
                    el.style.borderColor = '#00A5E5';
                    this.setState( ( prevState ) => ({
                        errorMessage: {
                            ...prevState.errorMessage,
                            [ id ]: ''
                        }
                    }) );
                }
            } else if ( !el.value ) {
                if ( id === 'password' ) this.setState({ showPasswordChecker: false });
                el.style.borderColor = 'red';
            } else {
                el.style.borderColor = '#00A5E5';
            }
        }
    }

    handleCaptcha( value ) {
        if ( value ) this.setState({ captcha: true });
    }

    handleCheckbox( value ) {
        const { checked } = value.target;
        this.setState({ checked });
    }

    /**
     *
     * Signup render method
     *
     */
    render() {
        const { notification } = this.props;

        return (
            <div></div>
/*            
            <StyledWrapper>
                <StyledSliderWrapper>
                    <FeatureSlides
                        slides={ this.state.featuredSlides }
                    />
                </StyledSliderWrapper>
                <StyledFormWrapper>
                    <Helmet
                        title="Signup"
                        meta={ [
                            { name: 'Signup Page', content: 'Create a Salarium Account' }
                        ] }
                    />
                    <SnackBar
                        message={ notification.message }
                        title={ notification.title }
                        type={ notification.type }
                        show={ notification.show }
                        delay={ 5000 }
                    />
                    <div id="brand">
                        <img src="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/SALARIUM-BLUE-LOGO.png" alt="Salarium" />
                        <h5>Create an Account</h5>
                    </div>
                    <p>Already have an account?&nbsp;&nbsp;<A href="/login">Sign in</A></p>

                    <StyledForm>
                        <div className="sl-c-card__body">
                            <div className="row">
                                <div className="col-lg-6">
                                    <label className="label-text" htmlFor="firstName">FIRST NAME</label>
                                    <input
                                        id="firstName"
                                        className="form-input form-names"
                                        type="search"
                                        placeholder="first name"
                                        onChange={ ( e ) => this.handleChange( e.target.value, 'firstName', 'firstName' ) }
                                        autoComplete="off"
                                    />
                                    { this.state.errorMessage.firstName && <p className="form-error">{this.state.errorMessage.firstName}</p>}
                                </div>
                                <div className="col-lg-6">
                                    <label className="label-text" htmlFor="lastName">LAST NAME</label>
                                    <input
                                        id="lastName"
                                        className="form-input form-names"
                                        type="search"
                                        placeholder="last name"
                                        onChange={ ( e ) => this.handleChange( e.target.value, 'lastName', 'lastName' ) }
                                        autoComplete="off"
                                    />
                                    { this.state.errorMessage.lastName && <p className="form-error">{this.state.errorMessage.lastName}</p>}
                                </div>
                            </div>

                            <label className="label-text" htmlFor="accountName">ACCOUNT NAME</label>
                            <input
                                type="search"
                                id="accountName"
                                className="form-input"
                                placeholder="Please enter your account name"
                                onChange={ ( e ) => this.handleChange( e.target.value, 'accountName', 'accountName' ) }
                                autoComplete="off"
                            />

                            <label className="label-text" htmlFor="email">EMAIL ADDRESS</label>
                            <input
                                id="email"
                                type="text"
                                className="form-input"
                                placeholder="Please enter your valid email address"
                                onChange={ ( e ) => this.handleChange( e.target.value, 'email', 'email' ) }
                                autoComplete="no"
                            />

                            <label className="label-text" htmlFor="password">PASSWORD</label>
                            <div className="password-container">
                                <input
                                    id="password"
                                    className="form-input"
                                    type={ this.state.showPassword ? 'search' : 'password' }
                                    placeholder="Please enter your desired password"
                                    onChange={ ( e ) => this.handleChange( e.target.value, 'password', 'password' ) }
                                    autoComplete={ this.state.showPassword ? 'off' : 'new-password' }
                                />
                                <button
                                    className="reveal-password"
                                    onClick={ this.setShowPassword }
                                    type="button"
                                >
                                    <Icon name="revealPw" className="icon" />
                                </button>
                            </div>
                            { this.state.showPasswordChecker && passwordChecker( this.state.password ) }
                        </div>

                        <footer className="footer-container">
                            <div className="policy-container">
                                <input
                                    className="checkbox"
                                    onChange={ ( e ) => this.handleCheckbox( e ) }
                                    value={ this.state.checked }
                                    type="checkbox"
                                />
                                <p>By creating an account, you agree to the <a target="_blank" rel="noopener noreferrer" href="https://www.salarium.com/terms-of-service/">Terms and Service</a>, and <a target="_blank" rel="noopener noreferrer" href="https://www.salarium.com/privacy-policy/">Privacy Policy</a>
                                </p>
                            </div>
                            <div className="captcha-container">
                                <ReCAPTCHA
                                    ref={ ( ref ) => { this.recaptcha = ref; } }
                                    sitekey={ process.env.RECAPTCHA_SITE_KEY }
                                    onChange={ this.handleCaptcha }
                                />
                            </div>
                            <Button
                                block
                                className="signup-btn"
                                label={ this.props.loading ? <Loader /> : 'Sign Up' }
                                size="large"
                                disabled={ !this.state.captcha || !this.state.checked || !Object.values( this.state.errorMessage ).every( ( x ) => x === '' ) }
                                onClick={ this._handleSubmit }
                            />
                        </footer>
                    </StyledForm>
                </StyledFormWrapper>
            </StyledWrapper>
*/
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    submitting: makeSelectSubmitting(),
    loadingSubscriptionOptions: makeSelectLoadingSubscriptionOptions(),
    subscriptionOptions: makeSelectSubscriptionOptions()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { getSubscriptionOptions, submitForm },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Signup );
