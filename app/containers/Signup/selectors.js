import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'signUp' );

const makeSelectLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectSubmitting = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'submitting' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLoadingSubscriptionOptions = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loadingSubscriptionOptions' )
);

const makeSelectSubscriptionOptions = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'subscriptionOptions' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectSubmitting,
    makeSelectNotification,
    makeSelectLoadingSubscriptionOptions,
    makeSelectSubscriptionOptions
};
