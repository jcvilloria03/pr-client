import {
    GET_SUBSCRIPTION_OPTIONS,
    SET_LOADING_SUBSCRIPTION_OPTIONS,
    SET_SUBSCRIPTION_OPTIONS,
    SET_NOTIFICATION,
    SET_SUBMITTING,
    SUBMIT_FORM,
    SET_LOADING
} from './constants';

/**
 * Get subscription options action
 */
export function getSubscriptionOptions() {
    return {
        type: GET_SUBSCRIPTION_OPTIONS
    };
}

/**
 * Set loading status of Subscription field action
 * @param {Boolean} payload - Loading status
 */
export function setLoadingSubscriptionOptions( payload ) {
    return {
        type: SET_LOADING_SUBSCRIPTION_OPTIONS,
        payload
    };
}

/**
 * Get subscription options action
 * @param {Object[]} payload - List of subscriptions
 */
export function setSubscriptionOptions( payload ) {
    return {
        type: SET_SUBSCRIPTION_OPTIONS,
        payload
    };
}

/**
 * Submit form action
 * @param {Object} payload - New account details
 */
export function submitForm( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}

/**
 * loading
 * @param {Boolean} payload - Loading state
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Set submitting action
 * @param {Boolean} payload - Submit status
 */
export function setSubmitting( payload ) {
    return {
        type: SET_SUBMITTING,
        payload
    };
}

/**
 * Set notification action
 * @param {Object} payload - Notification object
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}
