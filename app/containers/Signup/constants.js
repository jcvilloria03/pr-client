/**
 * Sign Up constants
 */

const namespace = 'app/containers/SignUp';

export const GET_SUBSCRIPTION_OPTIONS = `${namespace}/GET_SUBSCRIPTION_OPTIONS`;
export const SET_SUBSCRIPTION_OPTIONS = `${namespace}/SET_SUBSCRIPTION_OPTIONS`;
export const SET_LOADING_SUBSCRIPTION_OPTIONS = `${namespace}/SET_LOADING_SUBSCRIPTION_OPTIONS`;
export const SUBMIT_FORM = `${namespace}/SUBMIT_FORM`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_SUBMITTING = `${namespace}/SET_SUBMITTING`;
export const SET_LOADING = `${namespace}/SET_LOADING`;

export const FIELDS = {
    inputs: [ 'companyInput', 'nameInput', 'firstNameInput', 'lastNameInput', 'emailInput', 'passwordInput' ],
    selects: ['subscriptionSelect']
};
