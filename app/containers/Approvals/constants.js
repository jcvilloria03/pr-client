/*
 *
 * Approvals constants
 *
 */
import { template } from 'utils/taggedStringTemplate';

const namespace = 'app/Approvals';

export const LOADING = `${namespace}/LOADING`;
export const SET_LOADING = `${namespace}/SET_LOADING`;

export const DETAILS_LOADING = `${namespace}/DETAILS_LOADING`;
export const SET_DETAILS_LOADING = `${namespace}/SET_DETAILS_LOADING`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;

export const GET_APPROVALS = `${namespace}/GET_APPROVALS`;
export const SET_APPROVALS = `${namespace}/SET_APPROVALS`;

export const GET_REQUEST_DETAILS = `${namespace}/GET_REQUEST_DETAILS`;
export const SET_REQUEST_DETAILS = `${namespace}/SET_REQUEST_DETAILS`;

export const APPROVE = `${namespace}/APPROVE`;
export const DECLINE = `${namespace}/DECLINE`;
export const CANCEL = `${namespace}/CANCEL`;

export const DONE_REQUESTS = `${namespace}/DONE_REQUESTS`;

export const SEND_MESSAGE = `${namespace}/SEND_MESSAGE`;
export const SET_MESSAGE = `${namespace}/SET_MESSAGE`;

export const SET_PAGINATION = `${namespace}/SET_PAGINATION`;
export const PER_PAGE_DEFAULT = 10;

export const GET_DEFAULT_SCHEDULES = `${namespace}/GET_DEFAULT_SCHEDULES`;
export const SET_DEFAULT_SCHEDULES = `${namespace}/SET_DEFAULT_SCHEDULES`;

export const PROFILE_PICTURE = 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/placeholder.png';

export const REQUEST_STATUS_PENDING = 'pending';
export const REQUEST_STATUS_APPROVED = 'approved';
export const REQUEST_STATUS_APPROVING = 'approving';
export const REQUEST_STATUS_DECLINED = 'declined';
export const REQUEST_STATUS_DECLINING = 'declining';
export const REQUEST_STATUS_CANCELLED = 'cancelled';
export const REQUEST_STATUS_CANCELING = 'cancelling';
export const REQUEST_STATUS_IN_PROGRESS = 'in_progress';

export const FILTER_STATUS = [
    REQUEST_STATUS_PENDING,
    REQUEST_STATUS_APPROVED,
    REQUEST_STATUS_DECLINED,
    REQUEST_STATUS_CANCELLED,
    REQUEST_STATUS_IN_PROGRESS
];

export const STATUS_PENDING = [
    REQUEST_STATUS_PENDING
];

export const STATUS_APPROVE = [
    REQUEST_STATUS_APPROVED,
    REQUEST_STATUS_APPROVING
];

export const STATUS_DECLINE = [
    REQUEST_STATUS_DECLINED,
    REQUEST_STATUS_DECLINING
];

export const STATUS_CANCEL = [
    REQUEST_STATUS_CANCELING,
    REQUEST_STATUS_CANCELLED
];

export const FILTER_REQUEST_TYPES = [
    {
        label: 'Leaves',
        value: 'leaves'
    },
    {
        label: 'Overtime',
        value: 'overtime'
    },
    {
        label: 'Shift Change',
        value: 'shift_change'
    },
    {
        label: 'Time Correction',
        value: 'time_dispute'
    },
    {
        label: 'Undertime',
        value: 'undertime'
    }
];

export const REQUEST_TYPE_NAME = {
    leave_request: 'Leave',
    loan_request: 'Loan',
    overtime_request: 'Overtime',
    shift_change_request: 'Shift Change',
    time_dispute_request: 'Time Correction',
    undertime_request: 'Undertime'
};

export const APPROVAL_REQUEST_TYPE = {
    LEAVE: 'leave_request',
    LOAN: 'loan_request',
    TIME_DISPUTE: 'time_dispute_request',
    OVERTIME: 'overtime_request',
    SHIFT_CHANGE: 'shift_change_request',
    UNDERTIME: 'undertime_request'
};

export const NEW_DATE_FORMAT = 'ddd D MMM YYYY';
export const NEW_DATE_NO_YEAR_FORMAT = 'ddd D MMM';

export const SIDEPANEL_FILTER = 'filter';
export const SIDEPANEL_DETAILS = 'details';
export const SIDEPANEL_MESSAGES = 'messages';

export const SIDE_PANELS = [
    SIDEPANEL_FILTER,
    SIDEPANEL_DETAILS,
    SIDEPANEL_MESSAGES
];

export const SELECT_ALL_EMPLOYEES_OPTIONS = [
    {
        id: null,
        type: 'department',
        uid: 'all_department',
        value: 'department',
        field: 'department',
        label: 'All departments',
        disabled: false
    },
    {
        id: null,
        type: 'position',
        uid: 'all_position',
        value: 'position',
        field: 'position',
        label: 'All positions',
        disabled: false
    },
    {
        id: null,
        type: 'location',
        uid: 'all_location',
        value: 'location',
        field: 'location',
        label: 'All locations',
        disabled: false
    },
    {
        id: null,
        type: 'employee',
        uid: 'all_employee',
        value: 'employee',
        field: 'employee',
        label: 'All employee',
        disabled: false
    }
];

export const COMPLEX_ACTIVITIES = [
    'request_created_by_admin'
];

export const REQUEST_TRAIL_DATA = {
    request_created: {
        message_template: template`${0} submitted ${1} request`,
        icon: 'fa fa-flag text-success',
        employee: 'owner',
        timestamp: true,
        default_inputs: ['You']
    },
    request_created_by_admin: {
        message_template: template`${0} created a leave request on ${1} behalf.`,
        icon: 'fa fa-flag text-success',
        employee: 'owner',
        timestamp: true,
        default_inputs: [ 'You', 'your' ]
    },
    request_updated: {
        message_template: template`${0} updated a leave request.`,
        icon: 'fa fa-flag text-success',
        employee: 'admin',
        timestamp: true,
        default_inputs: ['You']
    },
    request_pending_approval: {
        message_template: template`Pending approval from ${0}`,
        icon: 'fa fa-circle text-warning',
        employee: 'approver',
        default_inputs: ['you']
    },
    message_sent: {
        message_template: template`${0} send a message`,
        icon: 'fa fa-reply text-disabled',
        employee: 'sender',
        timestamp: true,
        default_inputs: ['You']
    },
    request_approval_approved: {
        message_template: template`${0} approved the request`,
        icon: 'fa fa-check text-success',
        employee: 'approver',
        timestamp: true,
        default_inputs: ['You']
    },
    request_approval_rejected: {
        message_template: template`${0} rejected the request`,
        icon: 'fa fa-times text-danger',
        employee: 'approver',
        timestamp: true,
        default_inputs: ['You']
    },
    request_approved: {
        message_template: template`${0} has been approved!`,
        icon: 'fa fa-flag text-success',
        employee: 'owner',
        timestamp: true,
        default_inputs: [ 'Your request', 'Request' ]
    },
    request_rejected: {
        message_template: template`${0} has been rejected!`,
        icon: 'fa fa-flag text-danger',
        employee: 'owner',
        timestamp: true,
        default_inputs: [ 'Your request', 'Request' ]
    },
    request_auto_rejected: {
        message_template: template`Auto-rejected due to shift change.`,
        icon: 'fa fa-flag text-danger',
        employee: 'owner',
        timestamp: true,
        default_inputs: []
    },
    request_cancelled: {
        message_template: template`${0} has been cancelled!`,
        icon: 'fa fa-ban text-cancel',
        employee: 'owner',
        timestamp: true,
        default_inputs: [ 'Your request', 'Request' ]
    },
    request_reset: {
        message_template: template`${0} has been reset!`,
        icon: 'fa fa-flag text-success',
        employee: 'owner',
        timestamp: true,
        default_inputs: [ 'Your request', 'Request' ]
    },
    request_approval_no_action: {
        message_template: template`No action is required from ${0}`,
        icon: 'fa fa-circle text-warning',
        employee: 'approver',
        default_inputs: ['you']
    }
};

export const TIMESTAMP_FORMAT = 'HH:mm MMMM D, YYYY';

export const UPLOAD_ATTACHMENT = `${namespace}/UPLOAD_ATTACHMENT`;
export const REPLACE_ATTACHMENT = `${namespace}/REPLACE_ATTACHMENT`;
export const SET_ATTACHMENT = `${namespace}/SET_ATTACHMENT`;
export const REMOVE_ATTACHMENT = `${namespace}/REMOVE_ATTACHMENT`;
export const SET_ATTACHMENTS = `${namespace}/SET_ATTACHMENTS`;
export const DOWNLOAD_ATTACHMENTS = `${namespace}/DOWNLOAD_ATTACHMENTS`;

export const FILE_TYPES = [
    'application/pdf',
    'text/plain',
    'image/jpg',
    'image/jpeg',
    'image/gif',
    'application/msword',
    'doc',
    'image/png'
];
export const FILE_UPLOAD_VALIDATION_MESSAGE = {
    invalidType: 'Invalid file type',
    fileSize: 'File size should not be more than 10 MB'
};

export const SIZE_OF_10_MB_IN_BYTES = 10485760;
export const SIZE_OF_KB = 1024;
