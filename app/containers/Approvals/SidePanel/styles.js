import styled from 'styled-components';

import { H6 as BaseH6 } from 'components/Typography';

export const H6 = styled( BaseH6 )`
    font-size: 14px;
    line-height: unset;
`;

export const HeaderContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

export const SidePanelContainer = styled.div`
    margin-bottom: 1rem;
    border-radius: 5px;
    width: 25rem;
    margin: 0;
    background-color: white;
    border-radius: 0;
    transition: 0.3s;

    .header {
        padding: 0 1rem 1rem;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        font-size: 1.2rem;
        font-weight: 600;

        .fa-spin {
            font-size: 1rem;
        }

        h4 {
            font-weight: bold;
            margin-bottom: 0;
        }

        button {
            &:focus {
                outline: 0;
            }

            .icon {
                font-size: 15px;
                color: black;
                display: inline-flex;
                width: 14px;

                > i {
                    align-self: center;
                }
            }
        }
    }

    .header-details {
        padding: 0 1rem 1rem;
        display: grid;
        flex-direction: row;
        align-items: center;
        grid-template-columns: 3fr 1fr;
        justify-content: space-between;
        font-size: 1.2rem;
        font-weight: 600;
        justify-items: end;

        .fa-spin {
            font-size: 1rem;
        }

        h4 {
            font-weight: bold;
            margin-bottom: 0;
        }

        .details {
            font-size: 14px;
            font-weight: 400;
            font-style: italic;
            line-height: 1;
            text-align: center;
        }

        button {
            &:focus {
                outline: 0;
            }

            .icon {
                font-size: 15px;
                color: black;
                display: inline-flex;
                width: 14px;

                > i {
                    align-self: center;
                }
            }
        }
    }

    .Select-placeholder {
        font-size: 12px;
        touch-action: manipulation;
    }
`;

export const SectionContainer = styled.section`
    display: flex;
    flex-direction: column;
    gap: 24px;
`;

export const SectionContainerContentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 8px;

    .content-title {
        font-size: 14px;
        font-weight: 600;
        margin-bottom: 0.5rem;
        margin-top: 0.5rem;
    }

    .details-dates {
        display: grid;
        grid-auto-flow: row;
        grid-template-columns: repeat(2, 1fr);
        font-size: 14px;

        svg {
            width: 14px;
            color: #a4a1a1;
        }
    }

    .btn-wrapper {
        text-align: center;
    }

    .reset-btn {
        width: 50%;
    }

    .checkbox {
        display: flex;
        gap: 1px;
        font-size: 14px;

        .rc-checkbox-inner {
            width: 1rem;
            height: 1rem;
            &:after{
                width: 7px;
                height: 10px;
            }
        }
    }

    .datepicker {
        border: 1px solid rgba(0,0,0,.15);
        border-color: #95989a;
        border-radius: 0;
        line-height: 28px;
        background-color: #fdfdfd;
        height: 35px;
        padding-left: 10px;
        padding-right: 9px;
        width: 100%;
    }

    .btn-group {
        padding: 0;
        margin: 0;
        width: 100%;
        display: flex;
        flex-direction: row;
    }

    .btn-group button {
        padding: 0.5rem 1.5rem;
        color: #474747;
        text-align: center;
        line-height: 1.2;
        vertical-align: middle;
        border: solid 1px #adadad;
        -webkit-transition: all .2s ease-in-out;
        cursor: pointer;
        display: inline-block;
        -webkit-box-flex: 1;
        -webkit-flex-grow: 1;
        -ms-flex-positive: 1;
        -webkit-box-flex: 1;
        -webkit-flex-grow: 1;
        -ms-flex-positive: 1;
        flex-grow: 1;
        font-size: .9rem;
    }

    .btn-group button:last-child {
        border-top-right-radius: 0.25rem;
        border-bottom-right-radius: 0.25rem;
    }

    .btn-group button:first-child {
        border-top-left-radius: 0.25rem;
        border-bottom-left-radius: 0.25rem;
    }

    .btn-group button:focus,
    .btn-group button:active,
    .btn-group button.active {
        color: #fff;
        background-color: #00a5e5;
    }

    .sl-c-card__body {
        padding: 0.3rem;
        margin-bottom: 1rem;
        border: 1px solid #f0f4f6;
        border-radius: 0.5rem;
        word-break: break-all;
        font-size: 14px;

        .text-silver {
            color: silver;
        }

        .text-success {
            color: #83d24b;
        }

        .text-warning {
            color: #f4983e;
        }

        .text-danger {
            color: #EB7575;
        }

        .text-disabled {
            color: #ccc;
        }

        .text-cancel {
            color: #999999;
        }

        .media {
            display: flex;
            margin: 0;
        }

        .icon {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
            margin-right: 1rem;
        }

        .description {
            margin-bottom: 1rem;
            word-break: break-word;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;

            p {
                margin: 0;
            }
        }

        .media:not(:last-child) .icon {
            position: relative;
        }

        .media:not(:last-child) .icon:after {
            position: absolute;
            top: 1.75rem;
            bottom: 0.25rem;
            left: 50%;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%);
            content: "";
            border-left: 1px solid #ccc;
        }

        .media .icon {
            width: 1.5rem;
            text-align: center;
        }
    }
`;

export const DetailsWrapper = styled.div`
    border-radius: 0.2rem;
    padding: .5rem;
    background-color: #f0f4f6;

    * {
        font-size: 14px;
    }

    .text-strong {
        font-weight: 600;
    }

    .attachment-buttons {
        button {
            font-size: .9rem;
            border-radius: 2rem;
        }
    }

    .header {
        display: grid;
        grid-auto-flow: row;
        grid-template-columns: repeat(2, 1fr);
        padding: 0;
        font-weight: 500;

        .title {
            font-size: 14px;
            font-weight: 600;
        }

        .schedule-label {
            font-weight: 400;
            font-size: 14px;
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding: 0.25rem;
            white-space: nowrap;
            background-color: #bcdce5;
            border-radius: 1rem;
            width: fit-content;
            margin-bottom: 2px;

            svg {
                width: 14px;
                color: #00a5e5;
                align-items: center;
            }
        }
    }

    hr {
        margin-top: .3rem;
        border-top: 2px solid rgba(255,255,255,.5);
    }


    .attachments {
        margin-right: 10px;
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
        align-items: center;
        align-content: center;
        flex-wrap: wrap;
        gap: 10px;
    }

    .mr-5 {
        margin-right: 5px;
    }

    .record-wrapper {
        .record-body {
            display: grid;
            grid-auto-flow: row;
            grid-template-columns: repeat(2, 1fr);
            margin-bottom: 3px;
            align-items: center;

            .fa-icon {
                font-size: 10px;
                margin-right: 5px;
            }
        }
    }
`;

export const ContentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 24px;
    padding: 1rem;
`;

export const ContentContainer = styled.div`
    .button-icon {
        display: flex;
        align-items: center;

        &:focus {
            outline: 0;
        }

        span {
            color: #83d24b;
            font-weight: 600;
        }

        .icon {
            font-size: 20px;
            color: #83d24b;
            display: inline-flex;
            margin-right: .5rem;
            width: 14px;

            > i {
                align-self: center;
            }
        }
    }
`;

export const SectionContainerHeader = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 1rem;
`;

export const MessageReplySection = styled.div`
    justify-content: space-between;
    flex-direction: column;
    display: flex;

    .reply-field input {
        border-radius: 20px !important;
        border: 1px solid darkgrey;
        width: 100%;
        line-height: 2;
        font-size: 14px;
        padding: 5px;
        justify-content: flex-end;
        display: flex;
    }
`;

export const FooterSection = styled( SectionContainer )``;

export const FooterActions = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    flex-grow: 1;
    align-items: center;
    gap: 8px;
`;

export const Tabs = styled.div`
    box-shadow: 0 1px 2px 0 #cccccc;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-evenly;
`;

export const Tab = styled.button`
    display: inline-block;
    width: 100%;
    padding: .5rem;
    color: black !important;
    border-bottom: 3px solid #fff;
    font-size: 14px;
    text-align: center;
    cursor: pointer;

    &:hover {
        color: #00A5E5 !important;
        border-bottom-color: #00A5E5;
    }

    &:focus {
        outline: 0;
    }

    ${( props ) => props.active && `
        color: #00A5E5 !important;
        border-bottom-color: #00A5E5;
    `}
`;

export const DateDetailsContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    :first-child {
        h4 {
            margin-bottom: 0;
        }

        p {
            color: #00A5E5;
        }
    }

    .rc-switch {
        margin-left: .5rem;
    }
`;

export const DateDetails = styled.div`
    display: flex;
    flex-direction: column;
    gap: 4px;
`;

export const UserDetailsContainer = styled.div`
    display: flex;
    flex-direction: column;
    gap: 4px;

    h4 {
        font-weight: bold;
        margin-bottom: 0;
    }

    p {
        margin-bottom: 0;
    }

    .shiftName {
        color: #00A5E5;
        font-size: 14px;
    }
`;

export const ButtonIcon = styled.button`
    cursor: pointer;

    .icon {
        width: 16px;
    }

    span {
        font-size: 14px;
    }
`;

export const ChatDetails = styled.div`
    .sender {
        display: flex;
        justify-content: flex-end;
        align-items: center;
        flex-direction: row;
        font-size: 13px;

        .details {
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            align-items: flex-end;
            width: 100%;
        }
    }

    .receiver {
        display: flex;
        justify-content: flex-start;
        align-items: center;
        flex-direction: row;
        font-size: 13px;

        .details {
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            align-items: flex-start;
            width: 100%;
        }
    }

    .info {
        margin: 0;
        color: #ccc;
    }

    .message {
        padding: 0.5rem;
        border: 1px solid #f0f4f6;
        word-break: break-all;
        width: 100%
    }
`;

export const UserPhoto = styled.img`
    width: 50px;
    height: 50px;
    object-fit: cover;
    object-position: center;
    border: 1px solid #999999;
    border-radius: 50%;
    background-color: #ccc;
    margin: 0.5rem;

`;
