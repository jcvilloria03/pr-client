import React, { Component } from 'react';
import moment from 'moment';

import { auth } from 'utils/AuthService';

import {
    REQUEST_TYPE_NAME,
    REQUEST_TRAIL_DATA,
    COMPLEX_ACTIVITIES,
    TIMESTAMP_FORMAT
} from '../../constants';

import {
    SectionContainer,
    SectionContainerContentWrapper
} from '../styles';

/**
 * Activities Side Panel Component
 */
class Activities extends Component {
    static propTypes = {
        requestDetails: React.PropTypes.object
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
        };
    }

    activityIcon( activity ) {
        return REQUEST_TRAIL_DATA[ activity.type ].icon;
    }

    activityMessage( activity, requestType ) {
        if ( COMPLEX_ACTIVITIES.includes( activity.type ) ) {
            const adminInput = activity.params.approver.user_id === auth.getUser().id ? 'You' : activity.params.approver.name;
            const employeeInput = activity.params.owner.user_id === auth.getUser().id ? 'your' : activity.params.owner.name;
            const parseMessage = REQUEST_TRAIL_DATA[ activity.type ].message_template;

            return parseMessage( adminInput, employeeInput );
        }

        const defaultInputs = REQUEST_TRAIL_DATA[ activity.type ].default_inputs;
        const [ firstInput, secondInput ] = defaultInputs;

        const activityOwner = this.activityOwner( activity );

        const input = this.isCurrentUserOwnerOfActivity( activity ) ?
                    firstInput : ( secondInput || activityOwner.name );
        const parseMessage = REQUEST_TRAIL_DATA[ activity.type ].message_template;

        return parseMessage( input, requestType );
    }

    activityTimestamp( activity ) {
        return this.isActivityWithTimestamp( activity )
            ? moment( activity.created_at, 'YYYY-MM-DD HH:mm:ss' ).format( TIMESTAMP_FORMAT )
            : '';
    }

    isActivityWithTimestamp( activity ) {
        return REQUEST_TRAIL_DATA[ activity.type ].timestamp;
    }

    activityOwner( activity ) {
        return activity.params[ REQUEST_TRAIL_DATA[ activity.type ].employee ];
    }

    isCurrentUserOwnerOfActivity( activity ) {
        const activityOwner = this.activityOwner( activity );

        return activityOwner.user_id === auth.getUser().id;
    }

    render() {
        const {
            request,
            activities
        } = this.props.requestDetails;

        const {
            request_type: requestType
        } = request;

        return (
            <div>
                <SectionContainer>
                    <SectionContainerContentWrapper>
                        <p className="content-title">
                            Request Trail
                        </p>
                        <div className="sl-c-card__body">
                            { activities.map( ( item, index ) => (
                                <div className="media" key={ index }>
                                    <div className="icon">

                                        <i className={ `fa-lg ${this.activityIcon( item )}` }></i>
                                    </div>
                                    <div className="description">
                                        <p>{ this.activityMessage( item, REQUEST_TYPE_NAME[ requestType ]) }</p>
                                        <p className="text-disabled">{ this.activityTimestamp( item ) }</p>
                                    </div>
                                </div>
                            ) ) }
                        </div>
                    </SectionContainerContentWrapper>
                </SectionContainer>
            </div>
        );
    }
}

export default Activities;
