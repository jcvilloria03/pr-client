/* eslint-disable no-confusing-arrow */
/* eslint-disable no-plusplus */
import React, { Component } from 'react';
import groupBy from 'lodash/groupBy';
import orderBy from 'lodash/orderBy';
import unary from 'lodash/unary';
import partialRight from 'lodash/partialRight';
import some from 'lodash/some';
import includes from 'lodash/includes';

import moment from 'moment';

import A from 'components/A';
import Modal from 'components/Modal';
import Button from 'components/Button';

import { shiftsService } from 'utils/ShiftsService';

import {
    SectionContainer,
    DetailsWrapper,
    SectionContainerContentWrapper
} from '../styles';

import {
    FILE_TYPES,
    FILE_UPLOAD_VALIDATION_MESSAGE,
    SIZE_OF_10_MB_IN_BYTES
} from '../../constants';

/**
 * Leave Side Panel Component
 */
class Leave extends Component {
    static propTypes = {
        requestDetails: React.PropTypes.object,
        defaultSchedules: React.PropTypes.array,
        uploadAttachment: React.PropTypes.func,
        replaceAttachment: React.PropTypes.func,
        setNotification: React.PropTypes.func,
        attachments: React.PropTypes.array,
        downloadAttachment: React.PropTypes.func
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            currentAttachmentId: null,
            modalTitle: '',
            modalType: 'action',
            actionType: ''
        };

        this.handleFileInput = this.handleFileInput.bind( this );
        this.confirmModal = null;
    }

    getTime( leave ) {
        return `${leave.schedule.start_time} to ${leave.schedule.end_time}`;
    }

    getDefaultScheduleTime( date ) {
        if ( !this.props.defaultSchedules.length ) {
            return false;
        }

        const defaultSchedule =
                    shiftsService.parseDefaultScheduleForDisplay(
                        shiftsService.getDefaultScheduleForDate(
                            { defaultScheduleData: this.props.defaultSchedules },
                            date
                        ),
                        date
                    );

        return `${defaultSchedule.start} to ${defaultSchedule.end}`;
    }

    groupRequestsByDate( requests ) {
        return groupBy( this.orderNotificationsByDateDesc( requests ),
            ( request ) => moment( request.date ).format( 'MMMM D, YYYY' )
        );
    }

    orderNotificationsByDateDesc( requests ) {
        return orderBy( requests, 'created_at', 'desc' );
    }

    roundValueToTwoDecimals( value ) {
        return Number( value ).toFixed( 2 );
    }

    isFileTypeAcceptable( fileType ) {
        return fileType !== '' ? some( FILE_TYPES, unary( partialRight( includes, fileType ) ) ) : false;
    }
    isFilesSizeValid( fileSize ) {
        if ( fileSize > SIZE_OF_10_MB_IN_BYTES ) {
            return false;
        }
        return true;
    }

    async handleFileInput( event ) {
        let error = null;
        const selectedFile = event.target.files[ 0 ];

        if ( !this.isFileTypeAcceptable( selectedFile.type ) ) {
            error = FILE_UPLOAD_VALIDATION_MESSAGE.invalidType;
        }

        if ( !this.isFilesSizeValid( selectedFile.type ) ) {
            error = FILE_UPLOAD_VALIDATION_MESSAGE.fileSize;
        }

        if ( error ) {
            this.setState({ currentAttachmentId: null }, () => {
                this.props.setNotification({
                    message: error,
                    show: true,
                    title: 'Error',
                    type: 'error'
                });
            });

            return;
        }

        const payload = {
            id: this.props.requestDetails.request.id,
            file: selectedFile
        };

        if ( this.state.currentAttachmentId ) {
            payload.attachmentId = this.state.currentAttachmentId;

            this.props.replaceAttachment( payload );
        } else {
            this.props.uploadAttachment( payload );
        }
    }

    handleAction() {
        if ( this.state.actionType === 'replace' ) {
            this.fileInput.click();
        }

        this.setState({
            modalTitle: '',
            modalType: 'action',
            actionType: '',
            currentAttachmentId: null
        });

        this.confirmModal.close();
    }

    render() {
        const {
            unit,
            leaves,
            request,
            leave_type: leaveType,
            leave_credit: leaveCredit
        } = this.props.requestDetails;
        const groupedRequests = this.groupRequestsByDate( leaves );

        return (
            <div>
                <SectionContainer>
                    <SectionContainerContentWrapper>
                        { Object.values( groupedRequests ).map( ( leaveGroup, index ) => (
                            <DetailsWrapper key={ index }>
                                <div className="header">
                                    <div className="title">
                                        { moment( Object.keys( groupedRequests )[ index ]).format( 'LL' ) }
                                    </div>
                                    <div> </div>
                                </div>
                                <hr />
                                <div className="record-wrapper">
                                    { leaveGroup.map( ( leave, i ) => (
                                        <div key={ i }>
                                            <div className="record-body">
                                                <div>Schedule: </div>
                                                <div>{ leave.schedule ? leave.schedule.name : 'Default Schedule' }</div>
                                            </div>
                                            <div className="record-body">
                                                <div>Time: </div>
                                                <div>{ leave.schedule ? this.getTime( leave, Object.keys( groupedRequests )[ index ]) : this.getDefaultScheduleTime( Object.keys( groupedRequests )[ index ]) }</div>
                                            </div>
                                            <div className="record-body">
                                                <div>Leave Start Time: </div>
                                                <div>{ leave.start_time }</div>
                                            </div>
                                            <div className="record-body">
                                                <div>Leave End Time: </div>
                                                <div>{ leave.end_time }</div>
                                            </div>
                                            <div className="record-body">
                                                <div className="text-strong">Filed Leave Units: </div>
                                                <div className="text-strong">{ this.roundValueToTwoDecimals( leave.value ) } { unit }</div>
                                            </div>
                                        </div>
                                    ) ) }
                                </div>
                            </DetailsWrapper>
                        ) ) }

                        <DetailsWrapper>
                            <div className="header">
                                <div className="title">
                                    Attachment
                                </div>
                                <div className="attachment-buttons">
                                    <Button
                                        className="upload-button"
                                        label={ 'Upload' }
                                        type="grey"
                                        size="mini"
                                        onClick={ () => this.fileInput.click() }
                                    />
                                    <input
                                        id="file-upload"
                                        type="file"
                                        onChange={ this.handleFileInput }
                                        ref={ ( ref ) => { this.fileInput = ref; } }
                                        style={ { display: 'none' } }
                                    />
                                    <Button
                                        className="download-button"
                                        label={ 'Download all' }
                                        type="action"
                                        size="mini"
                                        disabled={ !this.props.attachments.length }
                                        onClick={ () => this.props.downloadAttachment({ id: request.id }) }
                                    />
                                </div>
                            </div>
                            <hr />
                            <div className="attachments">
                                { this.props.attachments.map( ( item, i ) => (
                                    item && item.id && (
                                        <div key={ i }>
                                            <A
                                                disabled="true"
                                                className="mr-5"
                                            >
                                                <i className="fa fa-times fa-icon" />
                                            </A>
                                            <A
                                                onClick={ () => this.setState({
                                                    modalTitle: 'Replace attachment',
                                                    modalType: 'danger',
                                                    actionType: 'replace',
                                                    currentAttachmentId: item.id
                                                }, () => {
                                                    this.confirmModal.toggle();
                                                }) }
                                                className="mr-5"
                                            >
                                                { item.original_name }
                                            </A>
                                        </div>
                                    )
                                ) ) }
                            </div>
                        </DetailsWrapper>

                        <DetailsWrapper>
                            <div className="header">
                                <div className="title">
                                    Leave count
                                </div>
                                <div> </div>
                            </div>
                            <hr />
                            <div className="record-wrapper">
                                <div className="record-body">
                                    <div>Total leave units used:</div>
                                    <div>{ this.roundValueToTwoDecimals( leaveCredit.used )} {leaveCredit.unit}</div>
                                </div>
                                <div className="record-body">
                                    <div>Remaining { leaveType.name } units: </div>
                                    <div className="text-strong">
                                        {
                                            leaveType.leave_credit_required
                                            ? `${this.roundValueToTwoDecimals( leaveCredit.value )} ${leaveCredit.unit}`
                                            : 'Unlimited Leave Credits'
                                        }
                                    </div>
                                </div>

                            </div>
                        </DetailsWrapper>
                    </SectionContainerContentWrapper>
                </SectionContainer>

                {/* modal */}
                <Modal
                    ref={ ( ref ) => { this.confirmModal = ref; } }
                    title={ this.state.modalTitle }
                    body={
                        <p>
                            Changing a pending request's attachment will bring the request back to the start of the workflow. Do you wish to proceed?
                        </p>
                    }
                    backdrop={ false }
                    keyboard={ false }
                    showClose={ false }
                    buttons={ [
                        {
                            label: 'Cancel',
                            type: 'grey',
                            onClick: () => {
                                this.confirmModal.close();
                                this.actionModal = null;
                                this.setState({
                                    modalTitle: '',
                                    modalType: 'neutral',
                                    currentAttachmentId: null
                                });
                            }
                        },
                        {
                            label: this.state.modalTitle,
                            type: this.state.modalType,
                            onClick: () => { this.handleAction(); }
                        }
                    ] }
                />
                {/* end of modal */}
            </div>
        );
    }
}

export default Leave;
