import React, { Component } from 'react';
import moment from 'moment';
import startCase from 'lodash/startCase';

import Input from 'components/Input';

import { auth } from 'utils/AuthService';

import {
    ContentContainer,
    SectionContainer,
    SectionContainerContentWrapper,
    UserPhoto,
    ChatDetails,
    MessageReplySection
} from '../styles';

/**
 * Messages Side Panel Messages Component
 */
class Messages extends Component {
    static propTypes = {
        requestDetails: React.PropTypes.object,
        sendMessage: React.PropTypes.func,
        messages: React.PropTypes.array
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            messageRequestId: null,
            messageRequestContent: ''
        };
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.messages !== nextProps.messages ) {
            this.setState({ messageRequestContent: '' });

            this.myFormRef && this.myFormRef.reset();
        }
    }

    handleSubmit = ( ev ) => {
        ev.preventDefault();

        this.props.sendMessage({
            id: this.state.messageRequestId,
            message: this.state.messageRequestContent
        });
    }

    render() {
        const { request } = this.props.requestDetails;

        const { id: userId } = auth.getUser();

        return (
            <div>
                { this.props.messages && (
                <div>
                    <ContentContainer>
                        <div>
                            <SectionContainer>
                                <SectionContainerContentWrapper>
                                    { this.props.messages.map( ( item, index ) => (
                                        <div key={ index }>
                                            <ChatDetails>
                                                { userId === item.sender_id ? (
                                                    <div className="sender">
                                                        <span className="details">
                                                            <p className="info">You, {moment( item.created_at ).format( 'HH:MM MMMM DD, YYYY' )}</p>
                                                            <p className="message">{item.content}</p>
                                                        </span>
                                                        <UserPhoto src={ item.sender_picture } />
                                                    </div>
                                        ) : (
                                            <div className="receiver">
                                                <UserPhoto src={ item.sender_picture } />
                                                <span className="details">
                                                    <p className="info">{startCase( item.sender_name.toLowerCase() )}, {moment( item.created_at ).format( 'HH:MM MMMM DD, YYYY' )}</p>
                                                    <p className="message">{ item.content }</p>
                                                </span>
                                            </div>
                                        )}
                                            </ChatDetails>
                                        </div>
                                ) ) }
                                </SectionContainerContentWrapper>
                            </SectionContainer>
                        </div>
                    </ContentContainer>

                    <MessageReplySection>
                        <div>
                            <hr />
                            <form onSubmit={ this.handleSubmit } ref={ ( ref ) => { this.myFormRef = ref; } }>
                                <Input
                                    className="reply-field"
                                    placeholder="Write a reply"
                                    type="text"
                                    id="reply"
                                    value={ this.state.messageRequestContent }
                                    ref={ ( ref ) => { this.replyInput = ref; } }
                                    onChange={ ( value ) => {
                                        this.setState({
                                            messageRequestContent: value,
                                            messageRequestId: request.id
                                        });
                                    } }
                                />
                            </form>
                        </div>
                    </MessageReplySection>
                </div>
            )}
            </div>
        );
    }
}

export default Messages;
