/* eslint-disable no-param-reassign */
import React, { Component } from 'react';
import startsWith from 'lodash/startsWith';
import isEmpty from 'lodash/isEmpty';
import findIndex from 'lodash/findIndex';
import map from 'lodash/map';
import startCase from 'lodash/startCase';

import moment from 'moment';

import Icon from 'components/Icon';

import { shiftsService } from 'utils/ShiftsService';
import { timeClockService } from 'utils/TimeClockService';

import {
    SectionContainer,
    DetailsWrapper,
    SectionContainerContentWrapper
} from '../styles';

/**
 * Time DisputeSide Panel Component
 */
class TimeDispute extends Component {
    static propTypes = {
        requestDetails: React.PropTypes.object,
        defaultSchedules: React.PropTypes.array
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            defaultSchedules: [],
            shiftsPerDates: []
        };
    }

    componentWillMount() {
        this.setShiftsPerDates();
    }

    setShiftsPerDates() {
        const details = this.props.requestDetails;

        if ( details && details.params ) {
            const shiftsPer = this.state.shiftsPerDates;
            details.params.shifts.forEach( ( shiftData ) => {
                const schedule = this.getScheduleForShift( shiftData );
                const updatedShift = shiftData;

                updatedShift.schedule = schedule;
                updatedShift.schedule.expected_hours = shiftsService.getExpectedHours( updatedShift );
                if ( !shiftsPer[ updatedShift.date ]) {
                    shiftsPer[ updatedShift.date ] = [];
                }

                shiftsPer[ updatedShift.date ].push( this.addShiftToDayShifts( updatedShift ) );
            });

            this.setState({
                shiftsPerDates: shiftsPer
            });
        }
    }

    getScheduleForShift( shiftData ) {
        if ( shiftData.shift_id === 'rd' || startsWith( shiftData.shift_id, 'ds_' ) ) {
            const defaultSchedule = shiftsService.parseDefaultScheduleForDisplay(
                this.getDefaultScheduleForDate( shiftData.date ),
                shiftData.date
            );

            if ( defaultSchedule ) {
                return defaultSchedule.schedule;
            }
        }

        return this.findShiftsScheduleById( shiftData );
    }

    getDefaultScheduleForDate( date ) {
        return shiftsService.getDefaultScheduleForDate({ defaultScheduleData: this.props.defaultSchedules }, date );
    }

    getTimeRecordsForIndication( timesheet ) {
        return this.getTimeDataForIndication( timesheet, true );
    }

    getTimeDataForIndication( data, isTimesheet ) {
        let indicationItems = [];

        const oldDataItems = [...data.old_state];
        let newDataItems = [...data.new_state];

        oldDataItems.forEach( ( oldItem ) => {
            const searchByType = { type: oldItem.type };

            // search by time or timestamp value and by type
            const searchByValue = {
                ...searchByType,
                ...( isTimesheet ? { timestamp: oldItem.timestamp } : { time: oldItem.time })
            };

            let index = findIndex( newDataItems, searchByValue );
            let item = newDataItems[ index ];

            if ( item ) {
                indicationItems.push( item );
            } else {
                // not found by time or timestamp value
                oldItem.action = 'old';
                indicationItems.push( oldItem );

                // search by type
                index = findIndex( newDataItems, searchByType );
                item = newDataItems[ index ];

                if ( item ) {
                    item.action = 'new';
                } else {
                    item = {
                        ...oldItem,
                        action: 'new',
                        ...( isTimesheet ? { timestamp: 0 } : { time: '00:00' })
                    };
                }

                indicationItems.push( item );
            }

            if ( index !== -1 ) {
                newDataItems.splice( index, 1 );
            }
        });

        newDataItems = map( newDataItems, ( item ) => {
            item.action = 'added';
            return item;
        });

        // merge remaining added time records
        indicationItems = [ ...indicationItems, ...newDataItems ];

        return indicationItems;
    }

    getComputedHoursWorked( hoursWorked ) {
        return !!hoursWorked && this.getTimeDataForIndication( hoursWorked, false );
    }

    getTime( shift ) {
        return `${shift.schedule.start_time || '--'} to ${shift.schedule.end_time || '--'}`;
    }

    getDefaultScheduleTime( date ) {
        if ( !this.props.defaultSchedules.length ) {
            return false;
        }

        const defaultSchedule =
                    shiftsService.parseDefaultScheduleForDisplay(
                        shiftsService.getDefaultScheduleForDate(
                            { defaultScheduleData: this.props.defaultSchedules },
                            date
                        ),
                        date
                    );

        return `${defaultSchedule.start || '--'} to ${defaultSchedule.end || '--'}`;
    }

    getMinutesWorked( hoursWorked ) {
        return hoursWorked.reduce( ( acc, el ) => acc + timeClockService.timeToMinutes( el.time, 'HH:mm' ), 0 );
    }

    findShiftsScheduleById( shiftData ) {
        const foundShift = this.props.requestDetails.shifts.find( ( shift ) => shift.id === shiftData.shift_id );

        return isEmpty( foundShift ) ? {} : foundShift.schedule;
    }

    addShiftToDayShifts( shiftData ) {
        return {
            ...shiftData,
            timeRecords: this.getTimeRecordsForIndication( shiftData.timesheet ),
            workedHours: this.getComputedHoursWorked( shiftData.hours_worked )
        };
    }

    isRestDay( shift ) {
        const defaultRestDayName = 'rest_day';

        if ( isEmpty( shift ) ) {
            return false;
        }

        if ( shift.shift_id ) {
            return shift.shift_id === 'rd';
        }

        if ( !isEmpty( shift.schedule ) ) {
            return shift.schedule.day_type === defaultRestDayName;
        }

        if ( !isEmpty( shift.day_type ) ) {
            return shift.day_type === defaultRestDayName;
        }

        return false;
    }

    timeRecordLabel( timeRecord ) {
        return `${timeRecord.action ? startCase( timeRecord.action ) : ''}
                    ${startCase( timeRecord.type.replace( /_/g, ' ' ) )}:`;
    }

    workedHoursLabel( hoursWorked ) {
        const type = `${startCase( hoursWorked.type.replace( /_/g, ' ' ) )}`;

        return `${hoursWorked.action === 'old' ? startCase( hoursWorked.action ) : ''}
                    ${type.trim()}${hoursWorked.type === 'regular' ? ' hours' : ''}:`;
    }

    modificationClass( timeRecord ) {
        return ( timeRecord.action && timeRecord.action !== 'old' )
                    ? 'text-danger' : 'text-silver';
    }

    formatTimestamp( shift, timestamp, offset ) {
        if ( !timestamp ) return 'none';

        let dateTime = moment( timestamp, 'X' );
        // We will use moment's isDST() feature here
        // This detects if DST is applicable based on client timezone and also checks if
        // timestamp is within range of DST for that year
        // DST adjusts always in 1 hour, to complete 23-hour day in late winter or early spring and one 25-hour day in the autumn.
        // Only applies in US and/or Canada TZ
        if ( offset && !dateTime.isDST() ) {
            dateTime = dateTime.utc().utcOffset( offset );
        } else if ( offset && dateTime.isDST() ) {
            // We add +0800 by default this is due so that time displays will be in Asia/Singapore TZ
            // Once DST takes effect we will need to add 1 more hour to match the original pre dst timestamp,
            // which is defaulting to Asia/Manila now (+0800)
            dateTime = dateTime.utc().utcOffset( '+0900' );
        }

        return dateTime.format( 'MMM DD, YYYY - HH:mm:ss' );
    }

    minutesWorked( shift ) {
        let clockPairs = [];

        if ( shift.timesheet ) {
            clockPairs = timeClockService.getClockPairsFromShiftTimesheet( shift.timesheet.new_state );
        }

        return timeClockService.getMinutesWorked( clockPairs, shift );
    }

    formatMinutesToTime( minutes ) {
        return timeClockService.formatMinutesToTime( minutes );
    }

    totalMinutesWorked( shift ) {
        return this.getMinutesWorked( shift.hours_worked.new_state );
    }

    render() {
        return (
            <div>
                <SectionContainer>
                    <SectionContainerContentWrapper>
                        { Object.values( this.state.shiftsPerDates ).map( ( shifts, index ) => (
                            <div key={ index }>
                                <p className="content-title">
                                    { moment( Object.keys( this.state.shiftsPerDates )[ index ]).format( 'LL' ) }
                                </p>
                                <DetailsWrapper>
                                    { shifts.map( ( item, i ) => (
                                        <div key={ i }>
                                            { !this.isRestDay( item )
                                            ? (
                                                <div>
                                                    <div className="header">
                                                        <div> { item.shift_id ? item.schedule.name : 'Default Schedule' }</div>
                                                        <div className="schedule-label">
                                                            <Icon name="dot" />
                                                            { item.shift_id ? this.getTime( item ) : this.getDefaultScheduleTime( Object.keys( this.state.shiftsPerDates )[ index ]) }
                                                        </div>
                                                        <div>
                                                            <i className="fa fa-clock-o" />&nbsp; Expected hours:
                                                        </div>
                                                        <div>
                                                            <i className="fa fa-clock-o fa-icon" />&nbsp; { item.schedule.expected_hours }
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    { item.timeRecords && item.timeRecords.map( ( timeRecord, j ) => (
                                                        <div className="record-wrapper" key={ j }>
                                                            <div className="record-body">
                                                                <div>
                                                                    <i className="fa fa-clock-o" />&nbsp; { this.timeRecordLabel( timeRecord ) }
                                                                </div>
                                                                <div>
                                                                    <i className={ `fa fa-circle fa-icon ${this.modificationClass( timeRecord )}` } ></i>
                                                                    { this.formatTimestamp( item, timeRecord.timestamp, timeRecord.offset ) }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    ) )}

                                                    { item.workedHours && item.workedHours.map( ( workedHours, j ) => (
                                                        <div className="record-wrapper" key={ j }>
                                                            <div className="record-body">
                                                                <div>
                                                                    <i className="fa fa-clock-o" />&nbsp; { this.workedHoursLabel( workedHours ) }
                                                                </div>
                                                                <div>
                                                                    <i className={ `fa fa-circle fa-icon ${this.modificationClass( workedHours )}` } ></i>
                                                                    { workedHours.time }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    ) )}

                                                    { !item.workedHours && item.timeRecords && (
                                                        <div className="record-wrapper">
                                                            <div className="record-body">
                                                                <div>
                                                                    <i className="fa fa-clock-o" />&nbsp; Hours worked:
                                                                </div>
                                                                <div>
                                                                    <i className={ 'fa fa-circle fa-icon text-silver' } ></i>
                                                                    { this.formatMinutesToTime( this.minutesWorked( item ) ) }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    ) }

                                                    { item.workedHours && (
                                                        <div className="record-wrapper">
                                                            <div className="record-body">
                                                                <div>
                                                                    <i className="fa fa-clock-o" />&nbsp; Total Hours worked:
                                                                </div>
                                                                <div>
                                                                    <i className={ 'fa fa-circle fa-icon text-silver' } ></i>
                                                                    { this.formatMinutesToTime( this.totalMinutesWorked( item ) ) }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    ) }
                                                </div>
                                            )
                                            : (
                                                <div>
                                                    <div className="header">
                                                        <div> { item.shift_id ? 'Rest Day' : 'Default Schedule - Rest Day' }</div>
                                                        <div className="schedule-label">
                                                            <Icon name="dot" />
                                                            N/A to N/A
                                                        </div>

                                                        <div>
                                                            <i className="fa fa-clock-o" />&nbsp;
                                                                Expected hours:
                                                            </div>
                                                        <div>
                                                            <i className="fa fa-clock-o" />&nbsp;
                                                            <span>N/A</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            )}
                                        </div>
                                    ) ) }
                                </DetailsWrapper>
                            </div>
                        ) ) }

                    </SectionContainerContentWrapper>
                </SectionContainer>
            </div>
        );
    }
}

export default TimeDispute;
