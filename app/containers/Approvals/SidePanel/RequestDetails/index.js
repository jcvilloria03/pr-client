import React, { Component } from 'react';
import moment from 'moment';
import startCase from 'lodash/startCase';

import Icon from 'components/Icon';

import Messages from './messages';
import Activities from './activities';
import Overtime from './overtime';
import Undertime from './undertime';
import ShiftChange from './shiftChange';
import TimeDispute from './timeDispute';
import Leave from './leave';

import {
    APPROVAL_REQUEST_TYPE,
    SIDEPANEL_MESSAGES,
    SIDEPANEL_DETAILS,
    REQUEST_TYPE_NAME,
    STATUS_PENDING,
    STATUS_APPROVE,
    STATUS_DECLINE,
    STATUS_CANCEL
} from '../../constants';

import {
    Tabs,
    Tab,
    ContentWrapper,
    SidePanelContainer,
    ContentContainer,
    SectionContainer,
    SectionContainerContentWrapper
} from '../styles';

/**
 * Details Side Panel Component
 */
class Details extends Component {
    static propTypes = {
        toggleSidePanel: React.PropTypes.func,
        requestDetails: React.PropTypes.object,
        selectedRequest: React.PropTypes.object,
        loading: React.PropTypes.bool,
        selectedPanel: React.PropTypes.string,
        sendMessage: React.PropTypes.func,
        messages: React.PropTypes.array,
        defaultSchedules: React.PropTypes.array,
        uploadAttachment: React.PropTypes.func,
        replaceAttachment: React.PropTypes.func,
        setNotification: React.PropTypes.func,
        attachments: React.PropTypes.array,
        downloadAttachment: React.PropTypes.func
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            selectedPanel: this.props.selectedPanel,
            pendingColor: '#f6bb42',
            cancelColor: '#999999',
            declineColor: '#df5640',
            approveColor: '#70ca63'
        };
    }

    componentWillReceiveProps( nextProps ) {
        this.setState({
            selectedPanel: nextProps.selectedPanel
        });
    }

    getDotIconStatusColor( status ) {
        if ( this.isInCancelStatus( status ) ) {
            return this.state.cancelColor;
        }

        if ( this.isInDeclineStatus( status ) ) {
            return this.state.declineColor;
        }

        if ( this.isInApproveStatus( status ) ) {
            return this.state.approveColor;
        }

        return this.state.pendingColor;
    }

    isInPendingStatus( status ) {
        return STATUS_PENDING.includes( status );
    }

    isInCancelStatus( status ) {
        return STATUS_CANCEL.includes( status );
    }

    isInDeclineStatus( status ) {
        return STATUS_DECLINE.includes( status );
    }

    isInApproveStatus( status ) {
        return STATUS_APPROVE.includes( status );
    }

    renderOvertimeDetails() {
        return (
            <Overtime
                requestDetails={ this.props.requestDetails }
                defaultSchedules={ this.props.defaultSchedules }
            />
        );
    }

    renderUndertimeDetails() {
        return (
            <Undertime
                requestDetails={ this.props.requestDetails }
                defaultSchedules={ this.props.defaultSchedules }
            />
        );
    }

    renderShiftChangeDetails() {
        return (
            <ShiftChange
                requestDetails={ this.props.requestDetails }
                defaultSchedules={ this.props.defaultSchedules }
            />
        );
    }

    renderTimeDisputeDetails() {
        return (
            <TimeDispute
                requestDetails={ this.props.requestDetails }
                defaultSchedules={ this.props.defaultSchedules }
            />
        );
    }

    renderLeaveDetails() {
        return (
            <Leave
                requestDetails={ this.props.requestDetails }
                defaultSchedules={ this.props.defaultSchedules }
                uploadAttachment={ ( payload ) => this.props.uploadAttachment( payload ) }
                replaceAttachment={ ( payload ) => this.props.replaceAttachment( payload ) }
                setNotification={ ( payload ) => this.props.setNotification( payload ) }
                downloadAttachment={ ( payload ) => this.props.downloadAttachment( payload ) }
                attachments={ this.props.attachments }
            />
        );
    }

    renderDetails() {
        const {
            request,
            date,
            start_date: startDate,
            end_date: endDate
        } = this.props.requestDetails;

        const requestStartDate = date || startDate;
        const requestEndDate = date || endDate;

        return (
            <div>
                <ContentContainer>
                    <div>
                        <SectionContainer>
                            <SectionContainerContentWrapper>
                                <div className="details-dates">
                                    <span>
                                        <Icon name="calendar" /> &nbsp; From:<br />
                                        { moment( requestStartDate ).format( 'LL' ) }
                                    </span>
                                    <span>
                                        <Icon name="calendar" /> &nbsp; To:<br />
                                        { moment( requestEndDate ).format( 'LL' ) }
                                    </span>
                                </div>
                            </SectionContainerContentWrapper>
                            { APPROVAL_REQUEST_TYPE.LEAVE === request.request_type && this.renderLeaveDetails() }
                            { APPROVAL_REQUEST_TYPE.OVERTIME === request.request_type && this.renderOvertimeDetails() }
                            { APPROVAL_REQUEST_TYPE.UNDERTIME === request.request_type && this.renderUndertimeDetails() }
                            { APPROVAL_REQUEST_TYPE.SHIFT_CHANGE === request.request_type && this.renderShiftChangeDetails() }
                            { APPROVAL_REQUEST_TYPE.TIME_DISPUTE === request.request_type && this.renderTimeDisputeDetails() }

                            <Activities
                                requestDetails={ this.props.requestDetails }
                            />
                        </SectionContainer>
                    </div>
                </ContentContainer>
            </div>
        );
    }

    render() {
        const { status, params, request, request_type: requestType } = this.props.selectedRequest;
        const { date } = request;

        return (
            <SidePanelContainer>
                { this.props.selectedRequest && (
                    <div className="header-details">
                        <div className="details">
                            <p>{ request && REQUEST_TYPE_NAME[ requestType ] }</p>
                            <p style={ { color: this.getDotIconStatusColor( status.toLowerCase() ) } }>{ status }</p>
                            { date && ( <p>{ moment( date ).format( 'dddd MMM DD, YYYY' ) }</p> )}
                            <p>Submitted By { startCase( params.owner.name.toLowerCase() ) }</p>
                        </div>

                        <button
                            onClick={ () => {
                                this.props.toggleSidePanel();
                            } }
                            type="button"
                        >
                            <Icon name="remove" className="icon" />
                        </button>
                    </div>
                )}

                <Tabs className="sub-header">
                    <Tab
                        active={ this.state.selectedPanel === SIDEPANEL_MESSAGES }
                        type="button"
                        onClick={ () => {
                            this.setState({
                                selectedPanel: SIDEPANEL_MESSAGES
                            });
                        } }
                    >
                        <span>Message</span>
                    </Tab>
                    <Tab
                        active={ this.state.selectedPanel === SIDEPANEL_DETAILS }
                        type="button"
                        onClick={ () => {
                            this.setState({
                                selectedPanel: SIDEPANEL_DETAILS
                            });
                        } }
                    >
                        <span>Details</span>
                    </Tab>
                </Tabs>
                <ContentWrapper>
                    {
                        this.state.selectedPanel === SIDEPANEL_MESSAGES
                        && !this.props.loading && (
                            <Messages
                                sendMessage={ ( payload ) => this.props.sendMessage( payload ) }
                                messages={ this.props.messages }
                                requestDetails={ this.props.requestDetails }
                            />
                        )
                    }
                    {
                        this.state.selectedPanel === SIDEPANEL_DETAILS
                        && !this.props.loading && this.renderDetails()
                    }
                </ContentWrapper>
            </SidePanelContainer>
        );
    }
}

export default Details;
