import React, { Component } from 'react';
import filter from 'lodash/filter';
import find from 'lodash/find';
import map from 'lodash/map';

import moment from 'moment';

import Icon from 'components/Icon';

import { shiftsService } from 'utils/ShiftsService';
import { timeClockService } from 'utils/TimeClockService';

import {
    DEFAULT_SHIFT_DATETIME_FORMAT,
    NOT_AVAILABLE,
    REST_DAY,
    REST_DAY_TYPE
} from 'utils/constants';

import {
    SectionContainer,
    DetailsWrapper,
    SectionContainerContentWrapper
} from '../styles';

/**
 * Overtime Side Panel Component
 */
class Overtime extends Component {
    static propTypes = {
        requestDetails: React.PropTypes.object,
        defaultSchedules: React.PropTypes.array
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            defaultSchedules: [],
            shifts: [],
            timesheet: [],
            timeClockPairs: [],
            isRestDay: false
        };
    }

    componentWillMount() {
        this.initializeData();
    }

    getDefaultScheduleForDate() {
        return shiftsService.getDefaultScheduleForDate(
            { defaultScheduleData: this.props.defaultSchedules },
            this.props.requestDetails.date
        );
    }

    setDataForDefaultScheduleOrRestDay() {
        const defaultScheduleForDate = this.getDefaultScheduleForDate();
        const isRestDay = defaultScheduleForDate && defaultScheduleForDate.day_type === REST_DAY_TYPE;

        this.setState({ defaultScheduleForDate, isRestDay }, () => {
            const shifts = this.props.requestDetails.params.shifts;
            this.mapShifts( shifts );
        });
    }

    initializeData() {
        const overtime = this.props.requestDetails;
        const overtimeTimesheet = overtime.timesheet || [];

        const timesheet = timeClockService.getAttendancePairs( overtimeTimesheet );
        const timeClockPairs = filter( timesheet, ( pair ) => // eslint-disable-line no-confusing-arrow
            pair && pair.clockIn
                ? moment( pair.clockIn.timestamp, 'X' ).isSame( overtime.date, 'day' )
                : false
        );

        this.setState({
            timesheet,
            timeClockPairs
        }, () => {
            const defaultShiftRegex = new RegExp( /^ds/ );
            const shiftId = overtime && overtime.params.shifts[ 0 ].shift_id;

            if ( shiftId && !defaultShiftRegex.test( shiftId ) ) {
                const filteredShifts = filter( overtime.shifts, ( shift ) =>
                    find( overtime.params.shifts, ( overtimeShift ) => overtimeShift.shift_id.toString() === shift.id.toString() )
                );

                this.mapShifts( filteredShifts );
            } else {
                this.setDataForDefaultScheduleOrRestDay();
            }
        });
    }

    mapShifts( shiftsData ) {
        const overtime = this.props.requestDetails;

        const shifts = map( shiftsData, ( shift ) => {
            const shiftData = find( overtime.params.shifts, ( overtimeShift ) =>
                    parseInt( overtimeShift.shift_id, 10 ) === parseInt( shift.id, 10 )
                );

            let updatedShift = shift;

            if ( shiftData ) {
                updatedShift.start_time = shiftData.start_time;
                updatedShift.hours = shiftData.hours;
            }

            if ( this.state.defaultScheduleForDate ) {
                const isRestDay = this.state.defaultScheduleForDate.day_type === REST_DAY_TYPE;
                const startTime = isRestDay ? NOT_AVAILABLE : moment( this.state.defaultScheduleForDate.work_start, 'HH:mm:ss' ).format( 'HH:mm' );
                const endTime = isRestDay ? NOT_AVAILABLE : moment( this.state.defaultScheduleForDate.work_end, 'HH:mm:ss' ).format( 'HH:mm' );

                updatedShift.schedule = {
                    start_time: startTime,
                    end_time: endTime,
                    name: `Default Schedule${isRestDay ? ` - ${REST_DAY}` : ''}`
                };
            }

            updatedShift = this.addShiftStartAndEnd( updatedShift );

            updatedShift.clockPair = find( this.state.timeClockPairs, ( clockPair ) =>
                    timeClockService.isTimeclockOverlapingWithShift( clockPair, updatedShift )
                );

            return updatedShift;
        });

        this.setState({ shifts });
    }

    addShiftStartAndEnd( shift ) {
        const date = this.props.requestDetails.date;
        const updatedShift = shift;

        if ( updatedShift.schedule ) {
            const start = moment(
                `${date} ${updatedShift.schedule.start_time}`, DEFAULT_SHIFT_DATETIME_FORMAT
            );
            const end = moment(
                `${date} ${updatedShift.schedule.end_time}`, DEFAULT_SHIFT_DATETIME_FORMAT
            );

            if ( end.isBefore( start ) ) {
                end.add( 1, 'days' );
            }

            updatedShift.start = start;
            updatedShift.end = end;
        }
        return updatedShift;
    }

    hasClockIn( shift ) {
        return shift.clockPair && shift.clockPair.clockIn;
    }

    hasClockOut( shift ) {
        return shift.clockPair && shift.clockPair.clockOut;
    }

    render() {
        const { date } = this.props.requestDetails;

        return (
            <div>
                <SectionContainer>
                    <SectionContainerContentWrapper>
                        { this.state.shifts.map( ( item, index ) => (
                            <DetailsWrapper key={ index }>
                                <div className="header">
                                    <div className="title">
                                        { moment( date ).format( 'LL' ) }
                                    </div>
                                    <div className="schedule-label">
                                        <Icon name="dot" />
                                        { item.schedule.start_time } - { item.schedule.end_time }
                                    </div>
                                </div>
                                <hr />
                                <div className="record-wrapper">
                                    <div className="record-body">
                                        <div>Schedule: </div>
                                        <div>{ item.schedule.name }</div>
                                    </div>

                                    <div className="record-body">
                                        <div>Time: </div>
                                        <div>{ this.state.isRestDay
                                        ? ''
                                        : `${item.schedule.start_time} to ${item.schedule.end_time}` }</div>
                                    </div>

                                    <div className="record-body">
                                        <div>Clock in: </div>
                                        <div>{ this.hasClockIn( item )
                                        ? moment( item.clockPair.clockIn.timestamp, 'X' ).format( 'HH:mm' )
                                        : ''}</div>
                                    </div>

                                    <div className="record-body">
                                        <div>Clock out: </div>
                                        <div>{ this.hasClockOut( item )
                                        ? moment( item.clockPair.clockOut.timestamp, 'X' ).format( 'HH:mm' )
                                        : ''}</div>
                                    </div>

                                    <div className="record-body">
                                        <div>Overtime Start Time: </div>
                                        <div>{ item.start_time }</div>
                                    </div>

                                    <div className="record-body">
                                        <div>Overtime Hours: </div>
                                        <div>{ item.hours }</div>
                                    </div>
                                </div>
                            </DetailsWrapper>
                        ) ) }
                    </SectionContainerContentWrapper>
                </SectionContainer>
            </div>
        );
    }
}

export default Overtime;
