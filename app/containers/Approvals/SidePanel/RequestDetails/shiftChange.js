import React, { Component } from 'react';
import difference from 'lodash/filter';
import find from 'lodash/find';
import intersection from 'lodash/map';

import moment from 'moment';

import { shiftsService } from 'utils/ShiftsService';

import {
    SectionContainer,
    DetailsWrapper,
    SectionContainerContentWrapper
} from '../styles';

/**
 * Shift Change Side Panel Component
 */
class ShiftChange extends Component {
    static propTypes = {
        requestDetails: React.PropTypes.object,
        defaultSchedules: React.PropTypes.array
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
        };
    }

    getDefaultScheduleForDate() {
        return shiftsService.getDefaultScheduleForDate(
            { defaultScheduleData: this.props.defaultSchedules },
            this.props.requestDetails.date
        );
    }

    getScheduleByShiftId( shiftId, key = null ) {
        const shift = this.getShift( shiftId );

        if ( !shift ) {
            return '';
        }

        if ( key ) {
            return shift && shift.schedule[ key ];
        }

        return shift && shift.schedule;
    }

    getScheduleById( id, key = null ) {
        const details = this.props.requestDetails;

        if ( !details.schedules ) {
            return '';
        }

        const schedule = find( details.schedules, { id });

        if ( key && schedule ) {
            return schedule[ key ];
        }

        return schedule;
    }

    getShift( id ) {
        const details = this.props.requestDetails;

        if ( !details.shifts ) {
            return null;
        }

        return find( details.shifts, { id });
    }

    getDefaultScheduleDayForDisplay( date ) {
        if ( !this.props.defaultSchedules.length ) {
            return '';
        }

        const defaultSchedule = this.getDefaultScheduleForDate( date );
        const shiftDate = `${this.formatTime( defaultSchedule.work_start )} - ${this.formatTime( defaultSchedule.work_end )}`;

        return `Default Schedule - ${defaultSchedule.day_type === 'rest_day' ? 'Rest Day' : 'Regular ' + shiftDate}`; // eslint-disable-line prefer-template
    }

    getAddedShiftIds( oldIds, newIds ) {
        return difference( newIds, oldIds );
    }

    getOldShiftIs( oldIds, newIds ) {
        return intersection( oldIds, newIds );
    }

    hasRestDays( change ) {
        return change.old_state.rest_days_ids && change.old_state.rest_days_ids.length;
    }

    newShiftsAndRestDays( change ) {
        return change.new_state.rest_day || change.new_state.shifts_ids.length || change.new_state.schedules_ids.length;
    }

    oldShiftsAndRestDays( change ) {
        return change.old_state.shifts_ids.length || this.hasRestDays( change );
    }

    formatTime( time ) {
        return moment( time, 'HH:mm:ss' ).format( 'HH:mm' );
    }

    render() {
        const details = this.props.requestDetails;

        return (
            <div>
                <SectionContainer>
                    <SectionContainerContentWrapper>
                        { details.params.shift_change_dates.map( ( item, index ) => (
                            <DetailsWrapper key={ index }>
                                <div className="header">
                                    <div className="title">
                                        { moment( item.date ).format( 'LL' ) }
                                    </div>
                                    <div>
                                    </div>
                                </div>
                                <hr />
                                {/* OLD */}
                                <div className="record-wrapper">
                                    <span className="text-disabled">OLD</span>
                                    { item.old_state.shifts_ids.map( ( shiftId, i ) => (
                                        <div key={ i }>
                                            <div className="record-body">
                                                <div>Schedule: </div>
                                                <div>{ this.getScheduleByShiftId( shiftId, 'name' ) }</div>
                                            </div>
                                            <div className="record-body">
                                                <div>Time: </div>
                                                <div>{ `${this.getScheduleByShiftId( shiftId, 'start' )} to ${this.getScheduleByShiftId( shiftId, 'end' )}` }</div>
                                            </div>
                                        </div>
                                    ) ) }

                                    { this.hasRestDays( item ) ? (
                                        <div className="record-body">
                                            <div>Schedule </div>
                                            <div>Rest Day</div>
                                        </div>
                                    ) : ''}

                                    { !this.oldShiftsAndRestDays( item ) ? (
                                        <div className="record-body">
                                            <div>{ this.getDefaultScheduleDayForDisplay( item.date ) }</div>
                                        </div>
                                    ) : ''}
                                </div>

                                <br />

                                {/* NEW */}
                                <div className="record-wrapper">
                                    <span className="text-success">NEW</span>
                                    {/* Old shifts */}
                                    { ( item.old_state.shifts_ids || item.new_state.shifts_ids )
                                        && this.getOldShiftIs( item.old_state.shifts_ids, item.new_state.shifts_ids ).map( ( shiftId, i ) => (
                                            <div key={ i }>
                                                <div className="record-body">
                                                    <div>Schedule: </div>
                                                    <div>{ this.getScheduleByShiftId( shiftId, 'name' ) }</div>
                                                </div>
                                                <div className="record-body">
                                                    <div>Time: </div>
                                                    <div>{ `${this.getScheduleByShiftId( shiftId, 'start' )} to ${this.getScheduleByShiftId( shiftId, 'end' )}` }</div>
                                                </div>
                                            </div>
                                    ) ) }

                                    {/* Added shifts */}
                                    { ( item.old_state.shifts_ids || item.new_state.shifts_ids )
                                        && this.getAddedShiftIds( item.old_state.shifts_ids, item.new_state.shifts_ids ).map( ( shiftId, i ) => (
                                            <div key={ i }>
                                                <div className="record-body">
                                                    <div>Schedule: </div>
                                                    <div>{ this.getScheduleByShiftId( shiftId, 'name' ) }</div>
                                                </div>
                                                <div className="record-body">
                                                    <div>Time: </div>
                                                    <div>{ `${this.getScheduleByShiftId( shiftId, 'start' )} to ${this.getScheduleByShiftId( shiftId, 'end' )}` }</div>
                                                </div>
                                            </div>
                                    ) ) }

                                    {/* Added schedules */}
                                    { item.new_state.schedules_ids.map( ( scheduleId, i ) => (
                                        <div key={ i }>
                                            <div className="record-body">
                                                <div>Schedule: </div>
                                                <div>{ this.getScheduleById( scheduleId, 'name' ) }</div>
                                            </div>
                                            <div className="record-body">
                                                <div>Time: </div>
                                                <div>{ `${this.getScheduleById( scheduleId, 'start' )} to ${this.getScheduleById( scheduleId, 'end' )}` }</div>
                                            </div>
                                        </div>
                                    ) ) }

                                    { item.new_state.rest_day ? (
                                        <div className="record-body">
                                            <div>Schedule </div>
                                            <div>Rest Day</div>
                                        </div>
                                    ) : ''}

                                    { !this.newShiftsAndRestDays( item ) ? (
                                        <div className="record-body">
                                            <div>{ this.getDefaultScheduleDayForDisplay( item.date ) }</div>
                                        </div>
                                    ) : ''}
                                </div>

                            </DetailsWrapper>
                        ) ) }
                    </SectionContainerContentWrapper>
                </SectionContainer>
            </div>
        );
    }
}

export default ShiftChange;
