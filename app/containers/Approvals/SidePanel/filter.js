import React, { Component } from 'react';
import Flatpickr from 'react-flatpickr';
import moment from 'moment';
import { debounce } from 'lodash';
import startCase from 'lodash/startCase';

import Icon from 'components/Icon';
import Button from 'components/Button';
import SalCheckbox from 'components/SalCheckbox/index';
import MultiSelect from 'components/MultiSelect';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { DATE_FORMATS } from 'utils/constants';

import {
    FILTER_REQUEST_TYPES,
    FILTER_STATUS,
    SELECT_ALL_EMPLOYEES_OPTIONS,
    PER_PAGE_DEFAULT,
    REQUEST_STATUS_IN_PROGRESS,
    REQUEST_STATUS_APPROVING,
    REQUEST_STATUS_DECLINING,
    REQUEST_STATUS_CANCELING
} from '../constants';

import {
    ContentContainer,
    SectionContainerHeader,
    ContentWrapper,
    SectionContainer,
    SidePanelContainer,
    SectionContainerContentWrapper
} from './styles';

/**
 * Edit Side Panel Component
 */
class Filter extends Component {
    static propTypes = {
        toggleSidePanel: React.PropTypes.func,
        getApprovals: React.PropTypes.func,
        loading: React.PropTypes.bool,
        filter: React.PropTypes.object
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            duration: null,
            start_date: null,
            end_date: null,
            filtering: false,
            processing: [
                REQUEST_STATUS_IN_PROGRESS,
                REQUEST_STATUS_APPROVING,
                REQUEST_STATUS_DECLINING,
                REQUEST_STATUS_CANCELING
            ],
            filter: this.props.filter
        };
    }

    componentWillMount() {
    }

    getAffectedValue() {
        const { filter } = this.props;

        if ( filter.affected_employees === undefined ) {
            return null;
        }

        return filter.affected_employees.map( ( data ) => ({
            id: data.id ? data.id : null,
            value: data.id,
            label: data.name,
            type: data.type,
            uid: data.uid ? data.uid : `${data.type}${data.id}`
        }) );
    }

    resetFilter() {
        this.setState({
            filter: {
                affected_employees: [],
                my_level: 1,
                statuses: ['pending'],
                types: Object.values( FILTER_REQUEST_TYPES ).map( ( item ) => ( item.value ) ),
                start_date: '',
                end_date: '',
                page: 1,
                per_page: PER_PAGE_DEFAULT
            },
            duration: null,
            start_date: null,
            end_date: null,
            filtering: true
        });

        this.submitFilter();
        this.setState({ filtering: false });
    }

    assignedEntities = ( keyword, callback ) => {
        const companyId = company.getLastActiveCompanyId();
        Fetch( `/company/${companyId}/affected_employees/search?term=${keyword}&limit=10&without_users=yes`, { method: 'GET' })
        .then( ( result ) => {
            const list = result.map( ( data ) => ({
                id: data.id ? data.id : null,
                value: data.id,
                label: data.name,
                type: data.type,
                uid: data.uid ? data.uid : `${data.type}${data.id}`
            }) );
            callback( null, { options: SELECT_ALL_EMPLOYEES_OPTIONS.concat( list ) });
        })
        .catch( ( error ) => callback( error, null ) );
    }

    submitFilter = debounce( () => {
        const { filter } = this.state;

        if ( !this.state.filtering ) {
            this.props.getApprovals({
                ...filter
            });
        }
    }, 500 )

    filterArrayValue( array, field, value ) {
        if ( array[ field ].includes( value ) ) {
            if ( array[ field ].length > 1 ) {
                return array[ field ].filter( ( item ) => item !== value );
            }
            return array[ field ];
        }
        return [ ...array[ field ], value ];
    }

    filterChanged( field, value ) {
        let filteredValue = value;

        if ([ 'statuses', 'types' ].includes( field ) ) {
            filteredValue = this.filterArrayValue( this.props.filter, field, value );
        }

        if ( field === 'affected_employees' ) {
            filteredValue = value.map( ( data ) => ({
                id: data.id,
                name: data.label,
                type: data.type,
                uid: data.uid
            }) );
        }

        const updatedFilter = {
            filtering: true
        };

        if ( field === 'duration' ) {
            updatedFilter.filter = {
                ...this.props.filter,
                start_date: this.state.start_date,
                end_date: this.state.end_date
            };
        } else {
            updatedFilter.filter = {
                ...this.props.filter,
                [ field ]: filteredValue
            };
        }

        this.setState( updatedFilter );
        this.submitFilter();
        this.setState({ filtering: false });
    }

    render() {
        const { duration } = this.state;
        const { filter } = this.props;

        return (
            <SidePanelContainer>
                <div className="header">
                    <div>
                        Filter &nbsp;
                        { this.props.loading && (
                            <i className="fa fa-spinner fa-spin fa-sm"></i>
                        )}
                    </div>

                    <button
                        onClick={ () => {
                            this.props.toggleSidePanel();
                        } }
                        type="button"
                    >
                        <Icon name="remove" className="icon" />
                    </button>
                </div>

                <ContentWrapper>
                    <ContentContainer>
                        <SectionContainer>
                            <SectionContainerContentWrapper>
                                <SectionContainerHeader>
                                    Show requests from
                                </SectionContainerHeader>
                                <MultiSelect
                                    id="search"
                                    async
                                    loadOptions={ debounce( this.assignedEntities, 1000 ) }
                                    ref={ ( ref ) => { this.search = ref; } }
                                    placeholder="Enter location, department name, position, employee name"
                                    onChange={ ( eve ) => {
                                        this.filterChanged( 'affected_employees', eve );
                                    } }
                                    value={ this.getAffectedValue() }
                                />

                            </SectionContainerContentWrapper>

                            <SectionContainerContentWrapper>
                                <SectionContainerHeader>
                                    Approval status
                                </SectionContainerHeader>
                                { FILTER_STATUS.map( ( row, i ) => (
                                    <div key={ i } className="checkbox">
                                        <SalCheckbox
                                            id={ row + i }
                                            name={ row + i }
                                            checked={ this.state.filter.statuses.includes( row ) }
                                            ref={ ( ref ) => { this.filterStatus = ref; } }
                                            onChange={ () => {
                                                this.filterChanged( 'statuses', row );
                                            } }
                                        />&nbsp;
                                        { this.state.processing.includes( row )
                                        ? 'Approving / Declining / Canceling'
                                        : startCase( row ) }
                                    </div>
                                ) ) }
                            </SectionContainerContentWrapper>

                            <SectionContainerContentWrapper>
                                <SectionContainerHeader>
                                    Approval types
                                </SectionContainerHeader>
                                { FILTER_REQUEST_TYPES.map( ( row ) => (
                                    <div key={ row.value } className="checkbox">
                                        <SalCheckbox
                                            id={ row.value }
                                            name={ row.value }
                                            checked={ filter.types.includes( row.value ) }
                                            ref={ ( ref ) => { this.filterTypes = ref; } }
                                            onChange={ () => {
                                                this.filterChanged( 'types', row.value );
                                            } }
                                        />&nbsp;
                                        { row.label }
                                    </div>
                                ) ) }
                            </SectionContainerContentWrapper>

                            <SectionContainerContentWrapper>
                                <SectionContainerHeader>
                                    Approval level
                                </SectionContainerHeader>

                                <div className="btn-group">
                                    <button
                                        className={ filter.my_level ? 'active' : '' }
                                        onClick={ () => {
                                            this.filterChanged( 'my_level', 1 );
                                        } }
                                        type="button"
                                    >
                                        My level only
                                    </button>
                                    <button
                                        className={ !filter.my_level ? 'active' : '' }
                                        onClick={ () => {
                                            this.filterChanged( 'my_level', 0 );
                                        } }
                                        type="button"
                                    >
                                        My level and below
                                    </button>
                                </div>
                            </SectionContainerContentWrapper>

                            <SectionContainerContentWrapper>
                                <SectionContainerHeader>
                                    Show filed requests
                                </SectionContainerHeader>

                                <div className="btn-group">
                                    <button
                                        className={ duration === 7 ? 'active' : '' }
                                        onClick={ () => {
                                            this.setState({
                                                duration: 7,
                                                start_date: moment().subtract( 7, 'days' ).format( DATE_FORMATS.API ),
                                                end_date: moment().format( DATE_FORMATS.API )
                                            }, () => {
                                                this.filterChanged( 'duration', null );
                                            });
                                        } }
                                        type="button"
                                    >
                                        Past 7 days
                                    </button>
                                    <button
                                        className={ duration === 30 ? 'active' : '' }
                                        onClick={ () => {
                                            this.setState({
                                                duration: 30,
                                                start_date: moment().subtract( 30, 'days' ).format( DATE_FORMATS.API ),
                                                end_date: moment().format( DATE_FORMATS.API )
                                            }, () => {
                                                this.filterChanged( 'duration', null );
                                            });
                                        } }
                                        type="button"
                                    >
                                        Past 30 days
                                    </button>
                                    <button
                                        className={ duration === 'custom' ? 'active' : '' }
                                        onClick={ () => {
                                            this.setState({ duration: 'custom' });
                                        } }
                                        type="button"
                                    >
                                        Custom
                                    </button>
                                </div>
                            </SectionContainerContentWrapper>
                            {
                                duration === 'custom' && (
                                <SectionContainerContentWrapper>
                                    <Flatpickr
                                        placeholder="Select Date Period"
                                        id="dates"
                                        className="datepicker"
                                        name="dates"
                                        ref={ ( ref ) => { this.dates = ref; } }
                                        options={ {
                                            mode: 'range',
                                            dateFormat: 'Y-m-d'
                                        } }
                                        // eslint-disable-next-line camelcase
                                        onChange={ ([ start_date, end_date ]) => {
                                            this.setState({
                                                duration: 'custom',
                                                start_date: moment( start_date ).format( DATE_FORMATS.API ),
                                                end_date: moment( end_date ).format( DATE_FORMATS.API )
                                            }, () => {
                                                this.filterChanged( 'duration', null );
                                            });
                                        } }
                                    />
                                </SectionContainerContentWrapper>
                                ) }
                            <SectionContainerContentWrapper>
                                <div className="btn-wrapper">
                                    <Button
                                        className="reset-btn"
                                        label={ 'Reset' }
                                        type="neutral"
                                        size="default"
                                        onClick={ () => this.resetFilter() }
                                    />
                                </div>
                            </SectionContainerContentWrapper>
                        </SectionContainer>
                    </ContentContainer>
                </ContentWrapper>
            </SidePanelContainer>
        );
    }
}

export default Filter;
