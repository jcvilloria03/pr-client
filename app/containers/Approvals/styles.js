import styled from 'styled-components';
import { Container } from 'reactstrap';

export const PageWrapper = styled.div`

    .container {
        position: fixed;
        height: 100%;
        padding: 6rem 5rem 0;
        margin: 0;
        width: 0;
        transition: width .3s;

        overflow-y: auto;
        scrollbar-width: thin;

        /* width */
        ::-webkit-scrollbar {
            width: 5px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #bec4c4;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }
    }

    .main-header {
        width: 100%;
        position: relative;
        transition: width .3s;
        display: grid;
        grid-auto-flow: row;
        grid-template-columns: repeat(2, 1fr);
        align-content: center;
        align-items: center;
        justify-content: center;
    }

    .main-body {
        transition: width .3s;
        position: relative;
        margin-top: 2rem;
        width: 100%;
        min-height: 100vh;

        .request-wrapper {
            display: grid;
            grid-auto-flow: row;
            grid-template-columns: 3fr 1fr;
        }

        .details-header {display: grid;
            grid-auto-flow: row;
            grid-template-columns: repeat(2,1fr);
            justify-items: stretch;
            align-items: center;
            justify-content: space-around;
            align-content: center;

            .pagination-label {
                text-align: right;
                margin-bottom: 0.5rem;
            }

            .selected-label {
                text-align: left;
                margin-bottom: 0.5rem;
                margin-left: 1.2rem;
            }
        }
    }

    #bottom-spacer {
        height: 50px;
    }

    .side-panel {
        height: 100%;
        width: 0;
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        overflow-x: hidden;
        transition: 0.3s;
        padding-top: 6rem;
        margin: 0;
        background-color: white;
        box-shadow: 0 0 14px 0 #2b2b2b4f;
        border-radius: 0;

        overflow-y: auto;
    }

    .title {
        font-weight: 600;
        font-size: 24px;
    }

    .action-buttons {
        text-align: end;
        min-width: 440px;
    }

    @media (min-width: 1281px) {
        .main-header {
            justify-items: end
        }
    }

    @media (max-width: 1280px) {
        .title {
            text-align: start;
        }
    }

    .approve-button {
        background-color: #70ca63;
        color: white;
    }

    .decline-button {
        background-color: #df5640;
        color: white;
    }

    .cancel-button {
        background-color: #999999;
        color: white;
    }

    .filter-button {
        margin-left: 3rem;

        svg {
            width: 1.3em;
            height: auto;
        }
    }

    .disabled {
        cursor: not-allowed !important;
    }

    .no-approvals {
        text-align: center
    }
`;

export const RequestWrapper = styled.div`
    letter-spacing: 0.12px;
    background-color: #fff;
    padding: 20px;
    box-shadow: 0 0 2px 0 #2b2b2b4f;

    .details {
        display: grid;
        grid-auto-flow: row;
        grid-template-columns: repeat(3, 1fr);
        justify-items: start;
        align-items: center;
        justify-content: center;
        align-content: center;

        .request-checkbox {
            height: 25px;
            width: 25px;
        }
    }

    .action-icons {
        min-width: 200px;
        text-align: end;
        color: #bebebe;
        margin-top: 10px;

        a {
            padding: 7px;
        }

        svg {
            width: 1.3em;
            height: auto;
        }

        a:hover .message-icon {
            color: #29D;
        }

        a:hover .cancel-icon {
            color: #999999;
        }

        a:hover .decline-icon {
            color: #df5640;
        }

        a:hover .approve-icon {
            color: #70ca63;
        }
    }

    :hover {
        background-color: #f0f4f6;
        border-right: solid 4px #29D;
        box-shadow: 0 0 3px 0 #2b2b2b4f;
    }

    .disable-link {
        a {
            pointer-events: none;
            cursor: ban;
        }
    }

    .decline-color {
        color: #df5640;
    }

    .approve-color {
        color: #70ca63;
    }

    .cancel-color {
        color: #999999;
    }

    .remark {
        font-weight: 600;
        font-size: 12px;
        padding-top: 12px;
    }
`;

export const EmployeeName = styled.span`
    display: flex;
    gap: 0.5rem;
    align-items: center;
    font-weight: 500;

    .link {
        text-decoration: underline !important;
    }
`;

export const EmployeePhoto = styled.img`
    width: 36px;
    height: 36px;
    object-fit: cover;
    object-position: center;
    border: 2px solid #323232;
    border-radius: 50%;
    background-color: #ccc;
`;

export const StyledContainer = styled( Container )`
    min-height: calc(100vh - 80px);
    padding: 10px 0;
    padding-bottom: 50px;
`;
