/* eslint-disable no-prototype-builtins */
/* eslint-disable camelcase */
/* eslint-disable no-inner-declarations */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import fileDownload from 'js-file-download';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import { REINITIALIZE_PAGE } from '../App/constants';
import { resetStore } from '../App/sagas';

import {
    setLoading,
    setDetailsLoading,
    setNotification
} from './actions';

import {
    GET_APPROVALS,
    GET_REQUEST_DETAILS,
    NOTIFICATION,
    SET_APPROVALS,
    SET_REQUEST_DETAILS,
    SET_PAGINATION,
    SEND_MESSAGE,
    SET_MESSAGE,
    GET_DEFAULT_SCHEDULES,
    SET_DEFAULT_SCHEDULES,
    APPROVE,
    DECLINE,
    CANCEL,
    DONE_REQUESTS,
    UPLOAD_ATTACHMENT,
    REPLACE_ATTACHMENT,
    SET_ATTACHMENT,
    REMOVE_ATTACHMENT,
    DOWNLOAD_ATTACHMENTS,
    REQUEST_STATUS_IN_PROGRESS,
    REQUEST_STATUS_APPROVING,
    REQUEST_STATUS_DECLINING,
    REQUEST_STATUS_CANCELING
} from './constants';

/**
 * Get requests for approvals
 */
export function* getApprovals({ payload }) {
    try {
        yield put( setLoading( true ) );
        const companyId = company.getLastActiveCompanyId();

        let formatedStatus = payload.statuses;
        if ( payload.statuses.includes( REQUEST_STATUS_IN_PROGRESS ) ) {
            formatedStatus = formatedStatus.filter( ( e ) => e !== REQUEST_STATUS_IN_PROGRESS );
            formatedStatus = formatedStatus.concat([
                REQUEST_STATUS_APPROVING,
                REQUEST_STATUS_DECLINING,
                REQUEST_STATUS_CANCELING
            ]);
        }

        const response = yield call( Fetch, '/user/approvals', {
            method: 'POST',
            data: {
                ...payload,
                statuses: formatedStatus,
                company_id: companyId
            }
        });

        yield put({
            type: SET_APPROVALS,
            payload: response.data || []
        });

        yield put({
            type: SET_PAGINATION,
            payload: response.meta.pagination || []
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Get request
 */
export function* getRequest({ payload }) {
    try {
        const { id, type } = payload;
        yield put( setDetailsLoading( true ) );

        const response = yield call( Fetch, `/${type}/${id}`, { method: 'GET' });

        yield put({
            type: SET_REQUEST_DETAILS,
            payload: response || {}
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setDetailsLoading( false ) );
    }
}

/**
 * Send message
 */
export function* sendMessage({ payload }) {
    try {
        const { id, message } = payload;

        const response = yield call( Fetch, '/request/send_message', {
            method: 'POST',
            data: {
                content: message,
                request_id: id
            }
        });

        yield put({
            type: SET_MESSAGE,
            payload: response || {}
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Upload leave request attachment
 */
export function* uploadAttachment({ payload }) {
    try {
        const { id, file } = payload;
        const companyId = company.getLastActiveCompanyId();

        const data = new FormData();
        data.append( 'attachment', file );
        data.append( 'request_id', id );

        const response = yield call( Fetch, '/ess/attachments', {
            method: 'POST',
            data,
            headers: {
                'X-Authz-Entities': 'ess.requests.leaves',
                'X-Authz-Company-Id': companyId
            }
        });

        yield put({
            type: SET_ATTACHMENT,
            payload: response
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Replace leave request attachment
 */
export function* replaceAttachment({ payload }) {
    try {
        const { id, file, attachmentId } = payload;

        const data = new FormData();
        data.append( 'attachment', file );
        data.append( 'request_id', id );
        data.append( 'replacing_attachment_id', attachmentId );

        const response = yield call( Fetch, '/ess/attachments/replace', {
            method: 'POST',
            data
        });

        yield put({
            type: SET_ATTACHMENT,
            payload: response
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Remove leave request attachment
 */
export function* removeAttachment({ payload }) {
    try {
        const { id, attachmentId } = payload;
        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, `ess/employee_request/${id}/attachments/${attachmentId}`, {
            method: 'DELETE',
            headers: {
                'X-Authz-Entities': 'ess.requests.leaves',
                'X-Authz-Company-Id': companyId
            }
        });

        yield put({
            type: SET_ATTACHMENT,
            payload: {}
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Download leave request attachment
 */
export function* downloadAttachment({ payload }) {
    try {
        const { id } = payload;
        const companyId = company.getLastActiveCompanyId();

        const response = yield call( Fetch, `ess/employee_request/${id}/download_attachments`, {
            method: 'GET',
            responseType: 'arraybuffer',
            headers: {
                'X-Authz-Entities': 'ess.requests.leaves',
                'X-Authz-Company-Id': companyId
            }
        });
        fileDownload( response, `${new Date().getTime()}-attachments.zip`, 'application/zip' );
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Get Default Schedules
 */
export function* getDefaultSchedules() {
    try {
        const companyId = company.getLastActiveCompanyId();

        const defaultSchedules = yield call( Fetch, `/default_schedule?company_id=${companyId}`, {
            method: 'GET'
        });

        yield put({
            type: SET_DEFAULT_SCHEDULES,
            payload: defaultSchedules.data || []
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Approve bulk/single requests
 */
export function* approveRequests({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, '/request/bulk_approve', {
            method: 'POST',
            data: {
                ...payload,
                company_id: companyId
            }
        });

        yield put({ type: DONE_REQUESTS, payload: payload.employee_requests_ids });

        yield call( notifyUser, {
            title: '',
            message: 'Requests approved successfully',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Decline bulk/single requests
 */
export function* declineRequests({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, '/request/bulk_decline', {
            method: 'POST',
            data: {
                ...payload,
                company_id: companyId
            }
        });

        yield put({ type: DONE_REQUESTS, payload: payload.employee_requests_ids });

        yield call( notifyUser, {
            title: '',
            message: 'Requests declined successfully',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Cancel bulk/single requests
 */
export function* cancelRequests({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, '/request/bulk_cancel', {
            method: 'POST',
            data: {
                ...payload,
                company_id: companyId
            }
        });

        yield put({ type: DONE_REQUESTS, payload: payload.employee_requests_ids });

        yield call( notifyUser, {
            title: '',
            message: 'Requests canceled successfully',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const title = get( error, 'response.statusText' ) || 'Error';
    const message = get( error, 'response.data.message' ) ||
        get( error, 'response.data.Message' ) ||
        error.statusText;
    const payload = {
        show: true,
        title,
        message,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getApprovals );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_APPROVALS
 */
export function* watchForGetApprovals() {
    const watcher = yield takeLatest( GET_APPROVALS, getApprovals );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_REQUEST_DETAILS
 */
export function* watchForGetRequest() {
    const watcher = yield takeLatest( GET_REQUEST_DETAILS, getRequest );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SEND_REQUEST
 */
export function* watchForSendMessage() {
    const watcher = yield takeLatest( SEND_MESSAGE, sendMessage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_DEFAULT_SCHEDULES
 */
export function* watchForGetDefaultSchedules() {
    const watcher = yield takeLatest( GET_DEFAULT_SCHEDULES, getDefaultSchedules );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for APPROVE
 */
export function* watchForApproveRequests() {
    const watcher = yield takeLatest( APPROVE, approveRequests );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DECLINE
 */
export function* watchForDeclineRequests() {
    const watcher = yield takeLatest( DECLINE, declineRequests );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for CANCEL
 */
export function* watchForCancelRequests() {
    const watcher = yield takeLatest( CANCEL, cancelRequests );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for UPLOAD_ATTACHMENT
 */
export function* watchForUploadAttachment() {
    const watcher = yield takeLatest( UPLOAD_ATTACHMENT, uploadAttachment );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REPLACE_ATTACHMENT
 */
export function* watchForReplaceAttachment() {
    const watcher = yield takeLatest( REPLACE_ATTACHMENT, replaceAttachment );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REMOVE_ATTACHMENT
 */
export function* watchForRemoveAttachment() {
    const watcher = yield takeLatest( REMOVE_ATTACHMENT, removeAttachment );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DOWNLOAD_ATTACHMENTS
 */
export function* watchForDownloadAttachment() {
    const watcher = yield takeLatest( DOWNLOAD_ATTACHMENTS, downloadAttachment );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForGetApprovals,
    watchForGetRequest,
    watchForSendMessage,
    watchForReinitializePage,
    watchForGetDefaultSchedules,
    watchForApproveRequests,
    watchForDeclineRequests,
    watchForCancelRequests,
    watchForUploadAttachment,
    watchForReplaceAttachment,
    watchForRemoveAttachment,
    watchForDownloadAttachment
];
