import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_DETAILS_LOADING,
    SET_NOTIFICATION,
    SET_APPROVALS,
    SET_PAGINATION,
    SET_REQUEST_DETAILS,
    SET_MESSAGE,
    SET_DEFAULT_SCHEDULES,
    DONE_REQUESTS,
    SET_ATTACHMENT,
    SET_ATTACHMENTS
} from './constants';

const initialState = fromJS({
    loading: false,
    detailsLoading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    message: {},
    approvals: [],
    requestDetails: {},
    pagination: {
        count: 0,
        current_page: 1,
        per_page: 6,
        total: 0,
        total_pages: 1
    },
    defaultSchedules: [],
    doneRequests: null,
    attachment: {},
    attachments: []
});

/**
 *
 * Approvals reducer
 *
 */
function approvals( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_DETAILS_LOADING:
            return state.set( 'detailsLoading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_APPROVALS:
            return state.set( 'approvals', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_REQUEST_DETAILS:
            return state.set( 'requestDetails', fromJS( action.payload ) );
        case SET_MESSAGE:
            return state.set( 'message', fromJS( action.payload ) );
        case SET_DEFAULT_SCHEDULES:
            return state.set( 'defaultSchedules', fromJS( action.payload ) );
        case DONE_REQUESTS:
            return state.set( 'doneRequests', action.payload );
        case SET_ATTACHMENT:
            return state.set( 'attachment', fromJS( action.payload ) );
        case SET_ATTACHMENTS:
            return state.set( 'attachments', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default approvals;
