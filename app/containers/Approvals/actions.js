import {
    SET_LOADING,
    SET_DETAILS_LOADING,
    SET_NOTIFICATION,
    SET_APPROVALS,
    GET_APPROVALS,
    GET_REQUEST_DETAILS,
    SEND_MESSAGE,
    GET_DEFAULT_SCHEDULES,
    APPROVE,
    DECLINE,
    CANCEL,
    UPLOAD_ATTACHMENT,
    REPLACE_ATTACHMENT,
    SET_ATTACHMENTS,
    REMOVE_ATTACHMENT,
    DOWNLOAD_ATTACHMENTS
} from './constants';

import { RESET_STORE } from '../App/constants';

/**
 * Get approvals
 */
export function getApprovals( payload ) {
    return {
        type: GET_APPROVALS,
        payload
    };
}

/**
 * Set approvals
 */
export function setApprovals( payload ) {
    return {
        type: SET_APPROVALS,
        payload
    };
}

/**
 * Get request
 */
export function getRequest( payload ) {
    return {
        type: GET_REQUEST_DETAILS,
        payload
    };
}

/**
 * Send message
 */
export function sendMessage( payload ) {
    return {
        type: SEND_MESSAGE,
        payload
    };
}

/**
 * Upload leave request attachment
 */
export function uploadAttachment( payload ) {
    return {
        type: UPLOAD_ATTACHMENT,
        payload
    };
}

/**
 * Replace leave request attachment
 */
export function replaceAttachment( payload ) {
    return {
        type: REPLACE_ATTACHMENT,
        payload
    };
}

/**
 * Remove leave request attachment
 */
export function removeAttachment( payload ) {
    return {
        type: REMOVE_ATTACHMENT,
        payload
    };
}

/**
 * Download leave request attachment
 */
export function downloadAttachment( payload ) {
    return {
        type: DOWNLOAD_ATTACHMENTS,
        payload
    };
}

/**
 * Replace leave request attachment
 */
export function setAttachments( payload ) {
    return {
        type: SET_ATTACHMENTS,
        payload
    };
}

/**
 *
 * Approve bulk/single request
 */
export function approveRequests( payload ) {
    return {
        type: APPROVE,
        payload
    };
}

/**
 *
 * Decline bulk/single request
 */
export function declineRequests( payload ) {
    return {
        type: DECLINE,
        payload
    };
}

/**
 *
 * Cancel bulk/single request
 */
export function cancelRequests( payload ) {
    return {
        type: CANCEL,
        payload
    };
}

/**
 * Get Default Schedules
 */
export function getDefaultSchedules() {
    return {
        type: GET_DEFAULT_SCHEDULES
    };
}

/**
 * Sets approval request loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object}
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets request detail loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object}
 */
export function setDetailsLoading( payload ) {
    return {
        type: SET_DETAILS_LOADING,
        payload
    };
}

/**
 * Sets notification
 * @param {Object} payload - Notification config
 * @returns {Object}
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 *
 * Approvals actions
 *
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
