/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable array-callback-return */
/* eslint-disable react/sort-comp */
/* eslint-disable no-confusing-arrow */
/* eslint-disable camelcase */
import React from 'react';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import moment from 'moment';
import startCase from 'lodash/startCase';
import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';

import A from 'components/A';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import Button from 'components/Button';
import SnackBar from 'components/SnackBar';
import SalCheckbox from 'components/SalCheckbox';

import * as approvalActions from './actions';

import Filter from './SidePanel/filter';
import Details from './SidePanel/RequestDetails/index';

import {
    STATUS_PENDING,
    STATUS_APPROVE,
    STATUS_DECLINE,
    STATUS_CANCEL,
    REQUEST_TYPE_NAME,
    APPROVAL_REQUEST_TYPE,
    NEW_DATE_FORMAT,
    NEW_DATE_NO_YEAR_FORMAT,
    SIDEPANEL_FILTER,
    SIDEPANEL_DETAILS,
    SIDEPANEL_MESSAGES,
    PER_PAGE_DEFAULT,
    FILTER_REQUEST_TYPES,
    REQUEST_STATUS_APPROVED,
    REQUEST_STATUS_PENDING
} from './constants';

import {
    PageWrapper,
    RequestWrapper,
    EmployeeName,
    EmployeePhoto
} from './styles';

import {
    makeSelectLoading,
    makeSelectDetailsLoading,
    makeSelectNotification,
    makeSelectApprovals,
    makeSelectPagination,
    makeSelectRequest,
    makeSelectMessage,
    makeSelectDefaultSchedules,
    makeSelectDoneRequests,
    makeSelectAttachment,
    makeSelectAttachments
} from './selectors';

/**
 * Announcement Section
 */
export class approvalList extends React.PureComponent {
    static propTypes = {
        loading: React.PropTypes.bool,
        detailsLoading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        approvals: React.PropTypes.array,
        requestDetails: React.PropTypes.object,
        getApprovals: React.PropTypes.func,
        pagination: React.PropTypes.object,
        getRequest: React.PropTypes.func,
        sendMessage: React.PropTypes.func,
        message: React.PropTypes.object,
        getDefaultSchedules: React.PropTypes.func,
        defaultSchedules: React.PropTypes.array,
        approveRequests: React.PropTypes.func,
        declineRequests: React.PropTypes.func,
        cancelRequests: React.PropTypes.func,
        doneRequests: React.PropTypes.array,
        attachment: React.PropTypes.object,
        uploadAttachment: React.PropTypes.func,
        replaceAttachment: React.PropTypes.func,
        setNotification: React.PropTypes.func,
        downloadAttachment: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        this.state = {
            selectedPanel: null,
            selectedRequest: null,
            defaultColor: '#bebebe',
            pendingColor: '#f6bb42',
            cancelColor: '#999999',
            declineColor: '#df5640',
            approveColor: '#70ca63',
            messageColor: '#29D',
            filter: {
                page: 1,
                per_page: PER_PAGE_DEFAULT,
                my_level: 1,
                statuses: ['pending'],
                types: Object.values( FILTER_REQUEST_TYPES ).map( ( item ) => ( item.value ) )
            },
            messages: [],
            modalTitle: '',
            modalMessage: '',
            modalType: 'action',
            actionType: '',
            requestIdInAction: null,
            attachments: [],
            currentAttachmentId: [],
            isRemoveAttachment: false,
            requestIds: [],
            checkAll: false
        };

        this.actionModal = null;

        this.spacerObserver = new IntersectionObserver( ( e ) => {
            const element = e[ 0 ];
            // eslint-disable-next-line no-useless-return
            if ( !element.isIntersecting ) return;
            this.getMoreApprovals();
        });
    }

    componentDidMount() {
        // for infinite scroll
        this.spacerObserver.observe( document.querySelector( '#bottom-spacer' ) );
    }

    componentWillMount() {
        this.props.getApprovals( this.state.filter );
        this.props.getDefaultSchedules();
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( this.props.approvals, nextProps.approvals ) ) {
            if ( nextProps.approvals.length > this.props.approvals.length ) {
                this.scrollToTop();
            }

            if ( this.state.checkAll ) {
                this.setState({
                    requestIds: nextProps.approvals.map( ( item ) => item.id )
                });
            }
        }

        if ( nextProps.requestDetails && nextProps.requestDetails.messages ) {
            this.formatMessages( nextProps.requestDetails.messages );
        }

        if ( this.props.message !== nextProps.message && nextProps.message.params ) {
            this.handleNewMessage( nextProps.message );
        }

        if ( nextProps.requestDetails && nextProps.requestDetails.attachments ) {
            this.setState({ attachments: nextProps.requestDetails.attachments });
        }

        if ( this.props.attachment !== nextProps.attachment && nextProps.attachment.id ) {
            this.handleNewAttachment( nextProps.attachment );
        }

        if ( this.props.attachment && isEmpty( nextProps.attachment ) && this.state.isRemoveAttachment ) {
            this.updateAttachments();
        }

        if ( !isEqual( this.props.doneRequests, nextProps.doneRequests ) ) {
            this.setState({
                messages: [],
                requestIds: [],
                checkAll: false
            }, () => {
                this.props.getApprovals({
                    ...this.state.filter,
                    page: this.props.pagination.page,
                    per_page: this.props.pagination.per_page
                });
            });
        }
    }

    getSingleApproveCssClass( approval ) {
        let style = '';

        if ( this.isInApproveStatus( approval.status.toLowerCase() ) ) {
            style = 'approve-color';
        }

        if ( approval.status.toLowerCase() !== REQUEST_STATUS_PENDING || approval.approver_position !== approval.approval_position ) {
            style += ' disable-link';
        }

        return style.trim();
    }

    getSingleDeclineCssClass( approval ) {
        let style = '';

        if ( this.isInDeclineStatus( approval.status.toLowerCase() ) ) {
            style = 'decline-color';
        }

        if ( approval.status.toLowerCase() !== REQUEST_STATUS_PENDING || approval.approver_position !== approval.approval_position ) {
            style += ' disable-link';
        }

        return style.trim();
    }

    getSingleCancelCssClass( approval ) {
        const status = approval.status.toLowerCase();
        let style = '';

        if ( this.isInCancelStatus( approval.status.toLowerCase() ) ) {
            style = 'cancel-color';
        }

        if ( ![ REQUEST_STATUS_PENDING, REQUEST_STATUS_APPROVED ].includes( status ) || approval.approver_position !== approval.approval_position ) {
            style += ' disable-link';
        }

        return style.trim();
    }

    handleNewMessage( message ) {
        const newMessage = {
            id: message.id,
            request_id: message.request_id,
            sender_id: message.sender_id,
            sender_picture: message.sender_picture,
            sender_name: message.params.sender.name,
            content: message.content,
            created_at: message.created_at
        };

        if ( this.props.requestDetails && message.request_id === this.props.requestDetails.request.id ) {
            const newMessages = [newMessage];
            const updatedMessages = this.state.messages.concat(
                    newMessages.filter(
                        ( itemCurrent ) => !this.state.messages.find(
                        ( itemNew ) => itemNew.id === itemCurrent.id ),
                    ),
                );

            this.setState({ messages: updatedMessages, selectedPanel: SIDEPANEL_MESSAGES });
        }
    }

    handleNewAttachment( attachment ) {
        const { currentAttachmentId, attachments } = this.state;
        let updatedAttachments = attachments;

        if ( attachment && attachment !== this.props.attachment ) {
            if ( currentAttachmentId ) {
                if ( attachments && attachments.length > 0 ) {
                    updatedAttachments = attachments.map( ( row ) => row.id !== currentAttachmentId ? row : attachment );
                } else {
                    updatedAttachments = [attachment];
                }
            }

            if ( !currentAttachmentId ) {
                const newAttachments = [attachment];
                updatedAttachments = attachments.concat(
                    newAttachments.filter(
                        ( itemCurrent ) => !attachments.find(
                        ( itemNew ) => itemNew.id === itemCurrent.id ),
                    ),
                );
            }

            this.setState({ attachments: updatedAttachments }, () => {
                this.setState({ currentAttachmentId: null });
            });
        }
    }

    updateAttachments() {
        const updatedAttachments = this.state.attachments.filter(
            ( item ) => item.id !== this.state.currentAttachmentId
        );

        this.setState({ attachments: updatedAttachments }, () => {
            this.setState({
                currentAttachmentId: null,
                isRemoveAttachment: false
            });
        });
    }

    formatMessages( messages ) {
        const updatedMessages = [];

        messages.forEach( ( row ) => {
            const message = {
                id: row.id,
                request_id: row.request_id,
                sender_id: row.sender_id,
                sender_picture: row.sender_picture,
                sender_name: row.params.sender.name,
                content: row.content,
                created_at: row.created_at
            };

            message && updatedMessages.push( message );
        });

        this.setState({ messages: updatedMessages });
    }

    toggleSidePanel( type, request = null ) {
        const { requestId } = this.props.requestDetails;
        const checkRequestIds = ( !request || ( request && request.request && request.request.id === requestId ) );

        if ( this.state.selectedPanel && this.state.selectedPanel === type && checkRequestIds ) {
            this.setState({ selectedPanel: null, selectedRequest: null });
        } else {
            this.setState({ selectedPanel: type, selectedRequest: request, messages: []});

            if ( request && request.request.id ) {
                if ( request.request.id !== requestId ) {
                    this.props.getRequest({ id: request.request.id, type: request.request_type });
                }
            }
        }
    }

    isInPendingStatus( status ) {
        return STATUS_PENDING.includes( status );
    }

    isInCancelStatus( status ) {
        return STATUS_CANCEL.includes( status );
    }

    isInDeclineStatus( status ) {
        return STATUS_DECLINE.includes( status );
    }

    isInApproveStatus( status ) {
        return STATUS_APPROVE.includes( status );
    }

    toggleBulkAction( actionType, modalType ) {
        const total = this.state.requestIds.length;
        const filterStatus = this.state.filter.statuses;

        let actionStatusMessage = `You are about to ${actionType} [${total}] requests that are pending your approval`;

        if ( actionType === 'cancel' ) {
            let totalPending = 0;
            let totalApproved = 0;

            if ( filterStatus.includes( REQUEST_STATUS_PENDING ) ) {
                totalPending = this.props.approvals
                    .filter( ( arr ) => this.state.requestIds.includes( arr.id ) && arr.status.toLowerCase() === REQUEST_STATUS_PENDING )
                    .length;
            }

            if ( filterStatus.includes( REQUEST_STATUS_APPROVED ) ) {
                totalApproved = this.props.approvals
                    .filter( ( arr ) => this.state.requestIds.includes( arr.id ) && arr.status.toLowerCase() === REQUEST_STATUS_APPROVED )
                    .length;
            }

            actionStatusMessage = `You are about to ${actionType} [${totalPending}] requests that are pending your approval `
                + `and [${totalApproved}] requests that are approved`;
        }

        this.setState({
            modalTitle: `${startCase( actionType )} Requests`,
            modalMessage: `${actionStatusMessage}. Confirm your action.`,
            modalType,
            actionType,
            requestIdInAction: null
        }, () => {
            this.actionModal.toggle();
        });
    }

    toggleSingleAction( requestIdInAction, actionType, modalType ) {
        this.setState({
            modalTitle: `${startCase( actionType )} Request`,
            modalMessage: `You are about to ${actionType} this request. Confirm your action.`,
            modalType,
            actionType,
            requestIdInAction
        }, () => {
            this.actionModal.toggle();
        });
    }

    handleAction() {
        let payload = null;

        if ( this.state.requestIdInAction ) {
            payload = {
                employee_requests_ids: [this.state.requestIdInAction]
            };
        } else {
            payload = {
                employee_requests_ids: this.state.requestIds
            };
        }

        if ( payload && this.state.actionType === 'approve' ) {
            this.props.approveRequests( payload );
        }

        if ( payload && this.state.actionType === 'decline' ) {
            this.props.declineRequests( payload );
        }

        if ( payload && this.state.actionType === 'cancel' ) {
            this.props.cancelRequests( payload );
        }

        this.setState({
            modalTitle: '',
            modalMessage: '',
            modalType: 'action',
            actionType: '',
            requestIdInAction: null
        });

        this.actionModal.close();
    }

    filterRequestApprovals( filter ) {
        this.setState({
            filter: {
                ...this.state.filter,
                statuses: filter.statuses,
                types: filter.types,
                affected_employees: filter.affected_employees,
                my_level: filter.my_level,
                start_date: filter.start_date,
                end_date: filter.end_date
            }
        }, () => {
            this.props.getApprovals({
                ...this.state.filter,
                per_page: this.props.pagination.per_page
            });
        });
    }

    scrollToTop = () => {
        if ( this.props.approvals.length > this.state.filter.per_page ) {
            window.scrollTo({
                top: window.scrollY - 300,
                behavior: 'smooth'
            });
        }
    }

    getStatusColor( status ) {
        if ( this.isInCancelStatus( status ) ) {
            return this.state.cancelColor;
        }

        if ( this.isInDeclineStatus( status ) ) {
            return this.state.declineColor;
        }

        if ( this.isInApproveStatus( status ) ) {
            return this.state.approveColor;
        }

        return this.state.pendingColor;
    }

    isBottom() {
        const container = document.querySelector( '#scrollable' );
        if ( container.scrollTop + container.clientHeight >= container.scrollHeight ) {
            return true;
        }

        return false;
    }

    hasMessages( approval ) {
        const { message } = this.props;
        const { messages_count: count, id } = approval;

        if ( count > 0 ) {
            return true;
        }

        if ( message && message.request_id === id ) {
            return true;
        }

        return false;
    }

    getMoreApprovals() {
        if ( !this.props.loading ) {
            const {
                per_page: currentPerPage,
                total
            } = this.props.pagination;

            if ( currentPerPage < total ) {
                this.props.getApprovals({
                    ...this.state.filter,
                    per_page: currentPerPage + PER_PAGE_DEFAULT
                });
            }
        }
    }

    formatRequestType( type ) {
        return REQUEST_TYPE_NAME[ type ];
    }

    getDateLabel( start, end = null ) {
        if ( end ) {
            let startDate = moment( start ).format( NEW_DATE_FORMAT );
            const endDate = moment( end ).format( NEW_DATE_FORMAT );

            if ( startDate === endDate ) {
                return startDate;
            }

            const startYear = moment( start ).year();
            const endYear = moment( end ).year();

            if ( startYear === endYear ) {
                startDate = moment( start ).format( NEW_DATE_NO_YEAR_FORMAT );
            }

            return `${startDate} - ${endDate}`;
        }

        return moment( start ).format( NEW_DATE_FORMAT );
    }

    getPaginationLabel() {
        const { pagination, approvals } = this.props;

        const from = 1;
        const to = approvals.length;
        let total = 0;

        if ( pagination && pagination.total ) {
            total = pagination.total;
        }

        return `Showing ${from} - ${to} of ${total} entries`;
    }

    canApproveOrDeclineAll() {
        const { requestIds } = this.state;
        const { approvals } = this.props;

        if ( !requestIds.length ) {
            return false;
        }

        // all request should be in pending state
        const nonPendingRequests = approvals
                .filter( ( arr ) => requestIds.includes( arr.id ) && ( arr.status.toLowerCase() !== REQUEST_STATUS_PENDING || arr.approver_position !== arr.approval_position ) );

        if ( nonPendingRequests.length > 0 ) {
            return false;
        }

        return true;
    }

    canCancelAll() {
        const { requestIds } = this.state;
        const { approvals } = this.props;

        if ( !requestIds.length ) {
            return false;
        }

        // all request should be in pending/approved state
        const cancellableStatus = [ REQUEST_STATUS_PENDING, REQUEST_STATUS_APPROVED ];
        const nonCancelableRequests = approvals
                .filter( ( arr ) => requestIds.includes( arr.id ) && !cancellableStatus.includes( arr.status.toLowerCase() ) );

        if ( nonCancelableRequests.length > 0 ) {
            return false;
        }

        const nonApproverRequests = approvals
                .filter( ( arr ) => requestIds.includes( arr.id ) && arr.approver_position !== arr.approval_position );
        if ( nonApproverRequests.length > 0 ) {
            return false;
        }

        return true;
    }

    renderMainHeader() {
        return (
            <div className="main-header">
                <div className="title">
                    Approvals
                </div>
                <div className="action-buttons">
                    <Button
                        className="approve-button"
                        label={ 'Approve all' }
                        type="action"
                        disabled={ !this.canApproveOrDeclineAll() }
                        onClick={ () => this.toggleBulkAction( 'approve', 'action' ) }
                    />
                    <Button
                        className="decline-button"
                        label={ 'Decline all' }
                        type="danger"
                        disabled={ !this.canApproveOrDeclineAll() }
                        onClick={ () => this.toggleBulkAction( 'decline', 'danger' ) }
                    />
                    <Button
                        className="cancel-button"
                        label={ 'Cancel all' }
                        type="grey"
                        disabled={ !this.canCancelAll() }
                        onClick={ () => this.toggleBulkAction( 'cancel', 'cancel' ) }
                    />
                    <Button
                        className="filter-button"
                        label={ ( <Icon name="funnel" /> ) }
                        type="grey"
                        onClick={ () => this.toggleSidePanel( SIDEPANEL_FILTER ) }
                    />
                </div>
            </div>
        );
    }

    renderRequestSummary( approval ) {
        if ( approval.request_type === APPROVAL_REQUEST_TYPE.LEAVE ) {
            return approval.request && approval.request.leave_type && (
                <div>
                    <div>
                        <strong>{ approval.request.total_value } { approval.request.unit } </strong>
                        { approval.request.leave_type.name }
                    </div>
                    <div> { this.getDateLabel( approval.request.start_date, approval.request.end_date ) }</div>
                </div>
            );
        }

        if ( approval.request_type === APPROVAL_REQUEST_TYPE.UNDERTIME ) {
            return approval.request && approval.request.params.shifts && (
                <div>
                    <div>
                        <strong>{ approval.request.params.shifts[ 0 ].hours } Hours </strong> of Undertime
                    </div>
                    <div> { this.getDateLabel( approval.request.date ) }</div>
                </div>
            );
        }

        if ( approval.request_type === APPROVAL_REQUEST_TYPE.OVERTIME ) {
            return approval.request && approval.request.params.shifts && (
                <div>
                    <div>
                        <strong>{ approval.request.params.shifts[ 0 ].hours } Hours </strong> of Overtime
                    </div>
                    <div> { this.getDateLabel( approval.request.date ) }</div>
                </div>
            );
        }

        if ( approval.request_type === APPROVAL_REQUEST_TYPE.TIME_DISPUTE ) {
            return approval.request && approval.request.start_date && (
                <div>
                    <div>
                        <strong>Times In and Out</strong>
                    </div>
                    <div> for { this.getDateLabel( approval.request.start_date, approval.request.end_date ) }</div>
                </div>
            );
        }

        if ( approval.request_type === APPROVAL_REQUEST_TYPE.SHIFT_CHANGE ) {
            return approval.request && approval.request.schedules && (
                <div>
                    <div>
                        <strong>{ approval.request.schedules[ 0 ].name }</strong> Schedule
                    </div>
                    <div> { this.getDateLabel( approval.request.start_date, approval.request.end_date ) }</div>
                </div>
            );
        }

        return (
            <div>
            </div>
        );
    }

    renderMainBody() {
        const {
            defaultColor,
            messageColor,
            requestIds,
            checkAll
        } = this.state;
        const { approvals } = this.props;

        return (
            <div className="main-body">
                <div className="details-header">
                    <div className="selected-label">
                        <SalCheckbox
                            id={ 'check-all' }
                            checked={ checkAll }
                            onChange={ () => {
                                this.setState({
                                    checkAll: !checkAll
                                }, () => {
                                    if ( this.state.checkAll ) {
                                        this.setState({ requestIds: approvals.map( ( item ) => item.id ) });
                                    } else {
                                        this.setState({ requestIds: []});
                                    }
                                });
                            } }
                        >&nbsp;
                            { requestIds.length ? requestIds.length : 'No' } entries selected
                    </SalCheckbox>
                    </div>
                    <div className="pagination-label">
                        { this.getPaginationLabel() }
                    </div>
                </div>
                { approvals && approvals.map( ( approval ) => (
                    <RequestWrapper key={ approval.id } className="request-wrapper">
                        <div
                            className="details"
                            onClick={ () => this.toggleSidePanel( SIDEPANEL_DETAILS, approval ) }
                        >
                            <div>
                                <EmployeeName>
                                    <SalCheckbox
                                        id={ `${approval.id}` }
                                        checked={ requestIds.includes( approval.id ) }
                                        onChange={ () => {
                                            requestIds.includes( approval.id )
                                                ? this.setState({ requestIds: requestIds.filter( ( val ) => val !== approval.id ) })
                                                : this.setState({ requestIds: [ ...requestIds, approval.id ]});
                                        } }
                                    /> &nbsp;
                                    <EmployeePhoto src={ approval.employee_picture } />

                                    { startCase( approval.params.owner.name.toLowerCase() ) }
                                </EmployeeName>
                            </div>
                            <div>
                                <Icon name="dot" style={ { color: this.getStatusColor( approval.status.toLowerCase() ) } } />
                                { this.formatRequestType( approval.request_type ) }
                            </div>
                            <div>
                                { this.renderRequestSummary( approval ) }
                            </div>
                        </div>
                        <div className="action-icons">
                            <A
                                onClick={ () => {
                                    this.toggleSidePanel( SIDEPANEL_MESSAGES, approval );
                                } }
                                style={ { color: this.hasMessages( approval ) > 0 ? messageColor : defaultColor } }
                            >
                                <Icon name="commenting" className="message-icon" />
                            </A>
                            <span className={ this.getSingleCancelCssClass( approval ) } >
                                <A onClick={ () => { this.toggleSingleAction( approval.id, 'cancel', 'cancel' ); } } >
                                    <Icon name="ban" className="cancel-icon" />
                                </A>
                            </span>
                            <span className={ this.getSingleDeclineCssClass( approval ) } >
                                <A onClick={ () => { this.toggleSingleAction( approval.id, 'decline', 'danger' ); } }>
                                    <Icon name="timesCircle" className="decline-icon" />
                                </A>
                            </span>
                            <span className={ this.getSingleApproveCssClass( approval ) } >
                                <A onClick={ () => { this.toggleSingleAction( approval.id, 'approve', 'action' ); } } >
                                    <Icon name="tickCircle" className="approve-icon" />
                                </A>
                            </span>
                            { approval.approver_position !== approval.approval_position && this.isInPendingStatus( approval.status.toLowerCase() ) && (
                                <div className="remark">
                                    <Icon name="dot" style={ { color: this.getStatusColor( approval.status.toLowerCase() ) } } />
                                    Pending approval from { approval.current_approver_name }
                                </div>
                            ) }
                        </div>
                    </RequestWrapper>
                ) ) }
                { !this.props.loading && isEmpty( approvals ) && (
                    <div className="no-approvals">
                        <span>
                            No approvals.
                        </span>
                    </div>
                ) }
            </div>
        );
    }

    renderSidePanel() {
        const {
            selectedPanel,
            selectedRequest
        } = this.state;

        return (
            <div className="side-panel" style={ { width: selectedPanel ? '25rem' : '0' } }>
                { selectedPanel === SIDEPANEL_FILTER && (
                    <Filter
                        ref={ ( ref ) => { this.sidePanelFilter = ref; } }
                        toggleSidePanel={ () => this.toggleSidePanel( SIDEPANEL_FILTER ) }
                        loading={ this.props.loading }
                        filter={ this.state.filter }
                        getApprovals={ ( filter ) => this.filterRequestApprovals( filter ) }
                    />
                )}
                {[ SIDEPANEL_DETAILS, SIDEPANEL_MESSAGES ].includes( selectedPanel ) && selectedRequest && selectedRequest.request && (
                    <Details
                        ref={ ( ref ) => { this.sidePanelMessages = ref; } }
                        toggleSidePanel={ () => this.toggleSidePanel( null ) }
                        loading={ this.props.detailsLoading }
                        requestDetails={ this.props.requestDetails }
                        selectedRequest={ this.state.selectedRequest }
                        selectedPanel={ this.state.selectedPanel }
                        sendMessage={ ( payload ) => this.props.sendMessage( payload ) }
                        messages={ this.state.messages }
                        defaultSchedules={ this.props.defaultSchedules }
                        uploadAttachment={ ( val ) => this.setState({ currentAttachmentId: null }, () => this.props.uploadAttachment( val ) ) }
                        replaceAttachment={ ( val ) => this.setState({ currentAttachmentId: val.attachmentId }, () => this.props.replaceAttachment( val ) ) }
                        downloadAttachment={ ( val ) => this.props.downloadAttachment( val ) }
                        attachments={ this.state.attachments }
                        setNotification={ ( payload ) => this.props.setNotification( payload ) }

                    />
                )}
            </div>
        );
    }

    render() {
        return (
            <div ref={ ( ref ) => { this.main = ref; } } id="approvals-page">
                <Helmet
                    title="Approvals"
                    meta={ [
                        { name: 'description', content: 'Approval List' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <PageWrapper>
                    <Container id="scrollable" style={ { width: this.state.selectedPanel ? 'calc(100% - 25rem)' : '100%' } }>
                        { this.renderMainHeader() }
                        { this.renderMainBody() }
                        <div id="bottom-spacer"></div>
                    </Container>
                    { this.renderSidePanel() }
                </PageWrapper>

                {/* modal */}
                <Modal
                    ref={ ( ref ) => { this.actionModal = ref; } }
                    title={ this.state.modalTitle }
                    body={
                        <p>
                            { this.state.modalMessage }
                        </p>
                    }
                    backdrop={ false }
                    keyboard={ false }
                    showClose={ false }
                    buttons={ [
                        {
                            label: 'Cancel',
                            type: 'grey',
                            onClick: () => {
                                this.actionModal.close();
                                this.setState({
                                    modalTitle: '',
                                    modalMessage: '',
                                    modalType: 'neutral',
                                    requestIdInAction: null
                                });
                            }
                        },
                        {
                            label: 'Proceed',
                            type: this.state.modalType,
                            onClick: () => { this.handleAction(); }
                        }
                    ] }
                />
                {/* end of modal */}
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    detailsLoading: makeSelectDetailsLoading(),
    notification: makeSelectNotification(),
    approvals: makeSelectApprovals(),
    pagination: makeSelectPagination(),
    requestDetails: makeSelectRequest(),
    message: makeSelectMessage(),
    defaultSchedules: makeSelectDefaultSchedules(),
    doneRequests: makeSelectDoneRequests(),
    attachment: makeSelectAttachment(),
    attachments: makeSelectAttachments()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        approvalActions,
        dispatch,
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( approvalList );
