import { createSelector } from 'reselect';

/**
 * Direct selector to the approvals state domain
 */
const selectDomain = () => ( state ) => state.get( 'approvals' );

const makeSelectLoading = () => createSelector(
    selectDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectDetailsLoading = () => createSelector(
    selectDomain(),
  ( substate ) => substate.get( 'detailsLoading' )
);

const makeSelectApprovals = () => createSelector(
    selectDomain(),
  ( substate ) => substate.get( 'approvals' ).toJS()
);

const makeSelectRequest = () => createSelector(
    selectDomain(),
  ( substate ) => substate.get( 'requestDetails' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectPagination = () => createSelector(
    selectDomain(),
    ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectMessage = () => createSelector(
    selectDomain(),
    ( substate ) => substate.get( 'message' ).toJS()
);

const makeSelectDefaultSchedules = () => createSelector(
    selectDomain(),
    ( substate ) => substate.get( 'defaultSchedules' ).toJS()
);

const makeSelectDoneRequests = () => createSelector(
    selectDomain(),
    ( substate ) => substate.get( 'doneRequests' )
);

const makeSelectAttachment = () => createSelector(
    selectDomain(),
    ( substate ) => substate.get( 'attachment' ).toJS()
);

const makeSelectAttachments = () => createSelector(
    selectDomain(),
    ( substate ) => substate.get( 'attachments' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectDetailsLoading,
  makeSelectNotification,
  makeSelectApprovals,
  makeSelectRequest,
  makeSelectPagination,
  makeSelectMessage,
  makeSelectDefaultSchedules,
  makeSelectDoneRequests,
  makeSelectAttachment,
  makeSelectAttachments
};
