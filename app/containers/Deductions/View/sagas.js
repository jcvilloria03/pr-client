import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import fileDownload from 'js-file-download';

import {
    SET_LOADING,
    GET_PAGINATED_DEDUCTIONS,
    SET_PAGINATION,
    GET_DEDUCTIONS,
    SET_DEDUCTIONS,
    SET_FILTER_DATA,
    DELETE_DEDUCTIONS,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    EXPORT_DEDUCTIONS,
    SET_DOWNLOADING
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

/**
 * Get deductions for table
 */
export function* getDeductions({ filters }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        yield call( getPaginatedDeductions, {
            payload: {
                page: 1,
                perPage: 10,
                showLoading: true
            },
            filters
        });

        const filterData = {};
        const deductionTypes = yield call( Fetch, `/company/${companyId}/other_income_types/deduction_type`, { method: 'GET' });
        const locations = yield call( Fetch, `/philippine/company/${companyId}/locations`, { method: 'GET' });
        const departments = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });
        const positions = yield call( Fetch, `/company/${companyId}/positions`, { method: 'GET' });

        filterData.deductionTypes = deductionTypes.data;
        filterData.locations = locations.data;
        filterData.departments = departments.data;
        filterData.positions = positions.data;

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * get paginated deductions
 */
export function* getPaginatedDeductions({ payload, filters }) {
    const { page, perPage, showLoading } = payload;

    try {
        if ( showLoading ) {
            yield put({
                type: SET_LOADING,
                payload: true
            });
        }

        let filtersQueryParams = '';
        if ( filters !== undefined && filters.length > 0 ) {
            const filtersIds = {
                other_income_type_ids: [],
                location_ids: [],
                department_ids: [],
                position_ids: []
            };

            filters.map( ( filter ) => {
                if ( filter.type !== 'keyword' ) {
                    const filterFieldName = filter.type === 'deduction_type' ? 'other_income_type_ids' : `${filter.type}_ids`;
                    if ( filter.disabled === false ) {
                        filtersIds[ filterFieldName ].push( filter.value );
                    }
                } else {
                    filtersQueryParams += `&filter[keyword]=${filter.value}`;
                }

                return null;
            });

            for ( const [ field, ids ] of Object.entries( filtersIds ) ) {
                if ( ids.length > 0 ) {
                    for ( const filterId of ids ) {
                        filtersQueryParams += `&filter[${field}][]=${filterId}`;
                    }
                }
            }
        }
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_incomes/deduction_type?page=${page}&per_page=${perPage}${filtersQueryParams}`, { method: 'GET' });
        const { data, ...others } = response;
        let deductionsData = [];
        if ( data.length > 0 ) {
            const employeeIdFiltersQueryParams = [];
            data.map( ( deduction ) => {
                employeeIdFiltersQueryParams.push( `filter[employee_ids][]=${deduction.recipients.employees[ 0 ].recipient_id}` );
                return null;
            });
            const employeeIdsQueryString = employeeIdFiltersQueryParams.filter( ( v, i, a ) => a.indexOf( v ) === i ).join( '&' );
            const employees = yield call( Fetch, `/company/${companyId}/employees?${employeeIdsQueryString}`, { method: 'GET' });
            deductionsData = data.map( ( deduction ) => {
                const employee = employees.data.find( ( e ) => e.id === deduction.recipients.employees[ 0 ].recipient_id );
                delete employee.recipients;
                return { ...deduction,
                    employee_name: `${employee.first_name} ${employee.last_name}`,
                    employee_location_id: employee.location_id,
                    employee_position_id: employee.position_id,
                    employee_department_id: employee.department_id
                };
            });
        }

        yield put({
            type: SET_PAGINATION,
            payload: others
        });

        yield put({
            type: SET_DEDUCTIONS,
            payload: deductionsData
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        if ( showLoading ) {
            yield put({
                type: SET_LOADING,
                payload: false
            });
        }
    }
}

/**
 * Batch delete deductions
 */
export function* deleteDeductions({ payload }) {
    try {
        yield call( Fetch, `/company/${company.getLastActiveCompanyId()}/other_income`, {
            method: 'DELETE',
            data: { ids: payload }
        });
        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully deleted',
            show: true,
            type: 'success'
        });
        yield call( getDeductions, {
            payload: {
                showLoading: true
            },
            filters: []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully deleted',
            show: true,
            type: 'success'
        });
    }
}

/**
 * Export deductions
 */
export function* exportDeductions({ payload }) {
    try {
        yield put({
            type: SET_DOWNLOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_incomes/deduction_type/download`, {
            method: 'POST',
            data: {
                ids: payload
            }
        });

        fileDownload( response, 'deductions.csv' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DOWNLOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getDeductions );
}

/**
 * Watcher for GET deductions
 */
export function* watchForGetDeductions() {
    const watcher = yield takeEvery( GET_DEDUCTIONS, getDeductions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Paginated deductions
 */
export function* watchForPaginatedDeductions() {
    const watcher = yield takeEvery( GET_PAGINATED_DEDUCTIONS, getPaginatedDeductions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE deductions
 */
export function* watchForDeleteDeductions() {
    const watcher = yield takeEvery( DELETE_DEDUCTIONS, deleteDeductions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for EXPORT deductions
 */
export function* watchForExportDeductions() {
    const watcher = yield takeEvery( EXPORT_DEDUCTIONS, exportDeductions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetDeductions,
    watchForDeleteDeductions,
    watchForExportDeductions,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForPaginatedDeductions
];
