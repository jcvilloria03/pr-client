import { PAYMENT_SCHEME_LABELS } from '../../Loans/constants';

export const SET_LOADING = 'app/Deductions/View/SET_LOADING';
export const SET_DOWNLOADING = 'app/Deductions/View/SET_DOWNLOADING';
export const GET_DEDUCTIONS = 'app/Deductions/View/GET_DEDUCTIONS';
export const SET_DEDUCTIONS = 'app/Deductions/View/SET_DEDUCTIONS';
export const GET_PAGINATED_DEDUCTIONS = 'app/Deductions/View/GET_PAGINATED_DEDUCTIONS';
export const SET_PAGINATION = 'app/Deductions/View/SET_PAGINATION';
export const SET_FILTER_DATA = 'app/Deductions/View/SET_FILTER_DATA';
export const DELETE_DEDUCTIONS = 'app/Deductions/View/DELETE_DEDUCTIONS';
export const NOTIFICATION = 'app/Deductions/View/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/Deductions/View/NOTIFICATION_SAGA';
export const EXPORT_DEDUCTIONS = 'app/Deductions/View/EXPORT_DEDUCTIONS';

export const PAY_FREQUENCIES = PAYMENT_SCHEME_LABELS;
