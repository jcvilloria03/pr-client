import React from 'react';
import orderBy from 'lodash/orderBy';
import isEqual from 'lodash/isEqual';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import { PAY_FREQUENCIES } from './constants';

import {
    makeSelectLoading,
    makeSelectDeductions,
    makeSelectFilterData,
    makeSelectNotification,
    makeSelectDownloading,
    makeSelectPagination
} from './selectors';
import * as deductionsActions from './actions';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';

import SubHeader from '../../SubHeader';
import Loader from '../../../components/Loader';
import SnackBar from '../../../components/SnackBar';
import Table from '../../../components/Table';
import { H2, H3, H5 } from '../../../components/Typography';
import Button from '../../../components/Button';
import Input from '../../../components/Input';
import SalConfirm from '../../../components/SalConfirm';
import Icon from '../../../components/Icon';
import Filter from './templates/filter';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate
} from '../../../utils/functions';

/**
 * View Deductions Component
 */
export class View extends React.Component {
    static propTypes = {
        getDeductions: React.PropTypes.func,
        getPaginatedDeductions: React.PropTypes.func,
        deleteDeductions: React.PropTypes.func,
        exportDeductions: React.PropTypes.func,
        deductions: React.PropTypes.array,
        pagination: React.PropTypes.object,
        filterData: React.PropTypes.shape({
            deductionTypes: React.PropTypes.array,
            positions: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        loading: React.PropTypes.bool,
        downloading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    };

    static defaultProps = {
        loading: true,
        downloading: false,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: this.props.deductions,
            showDelete: false,
            deleteLabel: '',
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            showModal: false,
            showFilter: false,
            hasFiltersApplied: false,
            previousQuery: '',
            filtersApplied: []
        };

        this.searchInput = null;
        this.searchTypingTimeout = 0;
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.deductions',
            'create.deductions',
            'delete.deductions',
            'edit.deductions'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.deductions' ];

            if ( authorized ) {
                this.setState({ permission: {
                    view: authorization[ 'view.deductions' ],
                    create: authorization[ 'create.deductions' ],
                    delete: authorization[ 'delete.deductions' ],
                    edit: authorization[ 'edit.deductions' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.getDeductions();
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.deductions, this.props.deductions ) ) {
            this.handleSearch( nextProps.deductions );
        }
    }

    handleTableChanges = ( tableProps = this.deductionsTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });

        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    handleSearch = ( deductions = this.props.deductions ) => {
        let searchQuery = null;
        const dataToDisplay = deductions;

        if ( this.searchInput ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery || ( this.searchInput && searchQuery === '' ) ) {
            const currentFilters = this.state.filtersApplied.filter( ( filter ) => filter.type !== 'keyword' );
            const filtersToApply = [
                ...currentFilters,
                {
                    disabled: false,
                    type: 'keyword',
                    value: searchQuery
                }
            ];
            if ( this.searchTypingTimeout !== 0 ) {
                clearTimeout( this.searchTypingTimeout );
            }
            this.searchTypingTimeout = setTimeout( () => {
                if ( searchQuery !== '' || this.state.previousQuery !== '' ) {
                    this.props.getPaginatedDeductions({ page: 1, perPage: this.props.pagination.per_page, showLoading: false }, filtersToApply );
                }

                this.setState({
                    previousQuery: searchQuery,
                    filtersApplied: filtersToApply,
                    displayedData: this.props.deductions,
                    hasFiltersApplied: true
                }, () => {
                    this.handleTableChanges();
                });
            }, 500 );
        } else {
            this.setState({ displayedData: dataToDisplay }, () => {
                this.handleTableChanges();
            });
        }
    }

    deleteDeductions = () => {
        const deductionIDs = [];
        this.deductionsTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                deductionIDs.push( this.deductionsTable.props.data[ index ].id );
            }
        });
        this.props.deleteDeductions( deductionIDs );
        this.setState({ showDelete: false });
    }

    exportDeductions = () => {
        const selectedDeductions = [];

        const sortingOptions = this.deductionsTable.state.sorting;

        this.deductionsTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedDeductions.push( this.deductionsTable.props.data[ index ]);
            }
        });

        const sortedDeductions = orderBy(
            selectedDeductions,
            sortingOptions.map( ( o ) => o.id ),
            sortingOptions.map( ( o ) => ( o.desc ? 'desc' : 'asc' ) )
        );

        this.props.exportDeductions( sortedDeductions.map( ( deduction ) => deduction.id ) );
    };

    toggleFilter = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    applyFilters = ( filters ) => {
        this.props.getDeductions( filters );
        this.setState({
            filtersApplied: filters,
            hasFiltersApplied: !!filters.length
        }, () => {
            this.handleTableChanges();
        });
    }

    resetFilters = () => {
        this.setState({
            showFilter: false,
            displayedData: this.props.deductions,
            hasFiltersApplied: false
        });
    }

    /**
     * Component Render Method
     */
    render() {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'employee_name',
                header: 'Employee Name',
                sortable: false,
                accessor: ( row ) => row.employee_name,
                minWidth: 200
            },
            {
                id: 'type_name',
                accessor: ( row ) => row.type.name,
                sortable: false,
                header: 'Deduction Type',
                minWidth: 260
            },
            {
                id: 'created_at',
                header: 'Date Created',
                sortable: false,
                accessor: ( row ) => formatDate( row.created_at, DATE_FORMATS.DISPLAY ),
                minWidth: 260
            },
            {
                header: 'Amount Deducted',
                accessor: 'total_amount',
                sortable: false,
                minWidth: 260,
                render: ({ row }) => (
                    <div>
                        Php { formatCurrency( row.amount ) }
                    </div>
                )
            },
            {
                id: 'frequency',
                header: 'Frequency',
                sortable: false,
                accessor: ( row ) => ( PAY_FREQUENCIES[ row.frequency ] ? PAY_FREQUENCIES[ row.frequency ] : 'N/A' ),
                minWidth: 300
            },
            {
                id: 'valid_from',
                header: 'Start Date',
                sortable: false,
                accessor: ( row ) => formatDate( row.valid_from, DATE_FORMATS.DISPLAY ),
                minWidth: 200
            },
            {
                id: 'valid_to',
                header: 'Valid To',
                sortable: false,
                accessor: ( row ) => ( row.valid_to ? formatDate( row.valid_to, DATE_FORMATS.DISPLAY ) : 'N/A' ),
                minWidth: 200
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.state.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        to={ `/deductions/${row.id}/edit` }
                    />
                )
            }
        ];

        const dataForDisplay = this.state.displayedData;

        return (
            <div>
                <Helmet
                    title="View Deductions"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.deleteDeductions }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Are you sure you want to delete the record?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Deductions"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Deductions.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Deductions</H3>
                                <p>Set your employees&apos; deduction details through this page. Deductions can be set as one time or recurring e.g. Gym Membership</p>
                                <div style={ { textAlign: 'center' } } >
                                    <div className="main">
                                        <Button
                                            className={ this.state.permission.create ? '' : 'hide' }
                                            label="Add Deductions"
                                            size="large"
                                            type="action"
                                            to="/deductions/add"
                                        />
                                        <Button
                                            className={ this.state.permission.create ? '' : 'hide' }
                                            label="Add Deduction Types"
                                            size="large"
                                            type="neutral"
                                            onClick={ () => {
                                                window.location.href = '/company-settings/payroll/deduction-types/add';
                                            } }
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="title">
                                <H5>Deductions List</H5>
                                <div className="search-wrapper">
                                    <Input
                                        className="search"
                                        id="search"
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                        onChange={ () => { this.handleSearch(); } }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                    />
                                </div>
                                <span>
                                    {this.state.showDelete ? (
                                        <div>
                                            { this.state.deleteLabel }
                                            &nbsp;

                                            <Button
                                                className={ this.state.permission.view ? '' : 'hide' }
                                                label={ this.props.downloading ? <Loader /> : <span>Download</span> }
                                                type="neutral"
                                                onClick={ this.exportDeductions }
                                            />
                                            <Button
                                                className={ this.state.permission.delete ? '' : 'hide' }
                                                label={ <span>Delete</span> }
                                                type="danger"
                                                onClick={ () => {
                                                    this.setState({ showModal: false }, () => {
                                                        this.setState({ showModal: true });
                                                    });
                                                } }
                                            />
                                        </div>
                                    ) : (
                                        <div>
                                            { this.state.label }
                                            &nbsp;
                                            <Button
                                                label={ <span><Icon className="filter-icon" name="filter" /> Filter</span> }
                                                type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                                onClick={ this.toggleFilter }
                                            />
                                        </div>
                                    )}
                                </span>
                            </div>
                            <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                                <Filter
                                    filterData={ this.props.filterData }
                                    onCancel={ () => { this.resetFilters(); } }
                                    onApply={ ( values ) => { this.applyFilters( values ); } }
                                />
                            </div>
                            <Table
                                data={ dataForDisplay }
                                columns={ tableColumns }
                                pagination
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.deductionsTable = ref; } }
                                selectable
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;

                                    this.setState({
                                        showDelete: selectionLength > 0,
                                        deleteLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                                onPageChange={ ( data ) => this.props.getPaginatedDeductions({ page: data + 1, perPage: this.props.pagination.per_page, showLoading: true }, this.state.filtersApplied ) }
                                onPageSizeChange={ ( data ) => this.props.getPaginatedDeductions({ page: 1, perPage: data, showLoading: true }, this.state.filtersApplied ) }
                                page={ this.props.pagination.current_page - 1 }
                                pageSize={ this.props.pagination.per_page }
                                pages={ this.props.pagination.last_page }
                                manual
                            />
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    downloading: makeSelectDownloading(),
    deductions: makeSelectDeductions(),
    filterData: makeSelectFilterData(),
    notification: makeSelectNotification(),
    pagination: makeSelectPagination()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        deductionsActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
