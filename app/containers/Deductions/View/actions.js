import {
    GET_DEDUCTIONS,
    GET_PAGINATED_DEDUCTIONS,
    DELETE_DEDUCTIONS,
    NOTIFICATION,
    EXPORT_DEDUCTIONS
} from './constants';

/**
 * Fetch list of deductions
 */
export function getDeductions( filters ) {
    return {
        type: GET_DEDUCTIONS,
        filters
    };
}

/**
 * Fetch list of deductions
 */
export function getPaginatedDeductions( payload, filters ) {
    return {
        type: GET_PAGINATED_DEDUCTIONS,
        filters,
        payload,
        showLoading: true
    };
}

/**
 * Delete deductions
 * @param {array} IDs
 */
export function deleteDeductions( IDs ) {
    return {
        type: DELETE_DEDUCTIONS,
        payload: IDs
    };
}

/**
 * Export deductions
 * @param {array} IDs
 */
export function exportDeductions( IDs ) {
    return {
        type: EXPORT_DEDUCTIONS,
        payload: IDs
    };
}

/**
 * Display a notification to a user
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
