import { createSelector } from 'reselect';

/**
 * Direct selector to the deductions state domain
 */
const selectViewDeductionsDomain = () => ( state ) => state.get( 'deductions' );

const makeSelectLoading = () => createSelector(
    selectViewDeductionsDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectDownloading = () => createSelector(
    selectViewDeductionsDomain(),
    ( substate ) => substate.get( 'downloading' )
);

const makeSelectDeductions = () => createSelector(
    selectViewDeductionsDomain(),
  ( substate ) => substate.get( 'deductions' ).toJS()
);

const makeSelectFilterData = () => createSelector(
    selectViewDeductionsDomain(),
  ( substate ) => substate.get( 'filterData' ).toJS()
);

const makeSelectPagination = () => createSelector(
  selectViewDeductionsDomain(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewDeductionsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectDownloading,
  makeSelectDeductions,
  makeSelectFilterData,
  makeSelectNotification,
  makeSelectPagination
};
