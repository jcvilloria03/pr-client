export const FILTER_TYPES = {
    DEDUCTION_TYPE: 'deduction_type',
    LOCATION: 'location',
    POSITION: 'position',
    DEPARTMENT: 'department'
};
