import { take, call, put, cancel } from 'redux-saga/effects';
import { takeLatest, takeEvery, delay } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { browserHistory } from '../../../utils/BrowserHistory';
import { formatFeedbackMessage } from '../../../utils/functions';
import { RECORD_UPDATED_MESSAGE } from '../../../utils/constants';

import {
    INITIALIZE,
    SET_LOADING,
    SET_SUBMITTED,
    SET_API_ERRORS,
    SET_FORM_OPTIONS,
    SET_DEDUCTION_DATA,
    EDIT_DEDUCTION,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { PAYMENT_SCHEME_OPTIONS } from '../../Loans/constants';
import { resetStore } from '../../App/sagas';

const TYPE = 'deduction_type';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formOptions = {};
        const response = yield call( Fetch, `/other_income/${payload.id}` );
        const deductionTypes = yield call( Fetch, `/company/${companyId}/other_income_types/${TYPE}?term=`, { method: 'GET' });
        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&keyword=`, { method: 'GET' });

        const recipientId = response.recipients.employees[ 0 ].recipient_id;
        const currentEmployee = yield call( Fetch, `/company/${companyId}/employees/search?id=${recipientId}`, { method: 'GET' });

        formOptions.currentEmployee = currentEmployee.data[ 0 ];
        formOptions.paymentSchemeOptions = PAYMENT_SCHEME_OPTIONS;
        formOptions.employees = employees.data.filter( ( employee ) => employee.payroll );

        formOptions.deductionTypes = deductionTypes.data
            .filter( ( type ) => !type.deleted_at || response.type.id === type.id )
            .map( ( value ) => ({ label: value.name, value: value.id }) );

        yield put({
            type: SET_DEDUCTION_DATA,
            payload: response
        });

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Edit deduction
 */
export function* editDeduction({ payload }) {
    try {
        yield put({
            type: SET_SUBMITTED,
            payload: true
        });

        yield put({
            type: SET_API_ERRORS,
            payload: {
                editDeduction: false
            }
        });

        // Transform data to remove null keys

        yield call( Fetch, `/philippine/deduction/${payload.id}`, {
            method: 'PATCH',
            data: payload.data
        });

        yield call( showSuccessMessage );
        yield call( delay, 500 );
        yield call( resetStore );

        payload.previosRouteName ? yield call( browserHistory.goBack ) : yield call( browserHistory.push, '/deductions' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });

        yield put({
            type: SET_API_ERRORS,
            payload: {
                editDeduction: true
            }
        });
    } finally {
        yield put({
            type: SET_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    yield call( resetStore );
    yield call( initializeData );
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Watcher for INITIALIZE
 */
export function* watcherForInitializeData() {
    const watcher = yield takeLatest( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for edit deduction
 */
export function* watchForEditDeduction() {
    const watcher = yield takeLatest( EDIT_DEDUCTION, editDeduction );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for reinitialize page
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export default [
    watcherForInitializeData,
    watchForEditDeduction,
    watchForNotifyUser,
    watchForReinitializePage
];
