import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import { Fetch } from 'utils/request';
import A from '../../../components/A';
import Input from '../../../components/Input';
import Loader from '../../../components/Loader';
import Button from '../../../components/Button';
import SalSelect from '../../../components/Select';
import Switch from '../../../components/Switch';
import SnackBar from '../../../components/SnackBar';
import SalConfirm from '../../../components/SalConfirm';
import Typeahead from '../../../components/Typeahead';
import DatePicker from '../../../components/DatePicker';
import { H2, H3 } from '../../../components/Typography';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { company } from '../../../utils/CompanyService';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';
import {
    formatCurrency,
    stripNonDigit,
    isAfter,
    getFormDataWithFormattedDates,
    isBeforeToday,
    areSame,
    formatDate
} from '../../../utils/functions';

import SubHeader from '../../../containers/SubHeader';

import {
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectApiErrors,
    makeSelectFormOptions,
    makeSelectDeductionData,
    makeSelectNotification
} from './selectors';

import * as actions from './actions';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    LoaderWrapper,
    AlignRight,
    ConfirmBodyWrapperStyle
} from './styles';

const inputTypes = {
    input: ['amount'],
    select: ['frequency'],
    datePicker: [ 'valid_to', 'valid_from' ]
};

/**
 * Edit Deduction Component
 */
class Edit extends Component {
    static propTypes = {
        params: React.PropTypes.object,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        errors: React.PropTypes.object,
        formOptions: React.PropTypes.object,
        deductionData: React.PropTypes.object, // eslint-disable-line
        initializeData: React.PropTypes.func,
        editDeduction: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            deductionData: null,
            permission: {
                edit: false
            },
            errors: {},
            prepopulatedEmployeeId: '',
            showModal: false,
            companyId: '',
            employees: [],
            isLoading: false
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.deductions',
            'edit.deductions'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.deductions' ];

            if ( authorized ) {
                this.setState({ permission: {
                    edit: authorization[ 'edit.deductions' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });

        this.props.initializeData( this.props.params.id );

        const prepopulatedEmployeeId = JSON.parse( localStorage.getItem( 'employeeIdForEditDeduction' ) );
        prepopulatedEmployeeId && this.setState({ prepopulatedEmployeeId });
    }

    componentWillReceiveProps( nextProps ) {
        if ( !this.state.deductionData ) {
            this.getInitialStateFromProps( nextProps );
        }

        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });

        nextProps.submitted !== this.props.submitted && this.submitButton.setState({ disabled: nextProps.submitted });

        const companyId = company.getLastActiveCompanyId();

        this.setState({ companyId });
    }

    componentWillUnmount() {
        localStorage.removeItem( 'employeeIdForEditDeduction' );
    }

    /**
     * Extract initial state from props
     */
    getInitialStateFromProps = ({ deductionData }) => {
        const initialData = {
            ...deductionData,
            amount: formatCurrency( deductionData.amount ),
            employee_id: deductionData.recipients.employees[ 0 ].recipient_id,
            valid_to: deductionData.valid_to ? formatDate( deductionData.valid_to, DATE_FORMATS.DISPLAY ) : null,
            valid_from: deductionData.valid_from ? formatDate( deductionData.valid_from, DATE_FORMATS.DISPLAY ) : null,
            recurring: !!deductionData.recurring
        };

        if ( this.props.errors.editDeduction ) {
            return;
        }

        this.setState({ deductionData: initialData });
    }

    getEmployeeFullName = () => {
        if ( !this.state.deductionData ) return '';

        const employee = this.props.formOptions.currentEmployee;

        return employee ? `${employee.first_name} ${employee.last_name} ${employee.employee_id}` : '';
    }

    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.input.includes( key ) || inputTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, label: this.state.errors[ key ][ 0 ] });
                } else if ( inputTypes.datePicker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    toggleRecurring = ( value ) => {
        this.updateDeductionData( 'recurring', value );
        if ( !value ) {
            this.updateDeductionData( 'frequency', '' );
            this.updateDeductionData( 'valid_to', null );
        }
    }

    /**
     * Sends api request for editing deduction
     */
    editDeduction = () => {
        if ( this.validateDeductionData() ) {
            let data = {
                ...this.state.deductionData,
                recipients: {
                    employees: [
                        this.state.deductionData.employee_id
                    ]
                },
                amount: Number( stripNonDigit( this.state.deductionData.amount ) )
            };

            data = getFormDataWithFormattedDates( data, DATE_FORMATS.DISPLAY, DATE_FORMATS.API );
            this.props.editDeduction({
                data: { ...data, valid_to: data.valid_to ? data.valid_to : null },
                id: this.props.params.id,
                previousRouteName: this.props.previousRoute.name
            });
        }
    };

    checkValidFrom = () => {
        if ( !this.validateDeductionData() ) return false;

        if ( areSame( this.state.deductionData.valid_from, this.props.deductionData.valid_from, DATE_FORMATS.DISPLAY, DATE_FORMATS.API ) ) {
            return true;
        }

        if ( this.state.deductionData.valid_from && isBeforeToday( this.state.deductionData.valid_from, DATE_FORMATS.DISPLAY ) ) {
            this.setState({ showModal: false }, () => {
                this.setState({ showModal: true });
            });
            return false;
        }

        return true;
    }

    validateDeductionData() {
        let valid = true;

        if ( this.employee_id._validate( this.employee_id.state.value ) ) {
            valid = false;
        }

        if ( !this.type_id._checkRequire(
            typeof this.type_id.state.value === 'object'
            && this.type_id.state.value !== null ? this.type_id.state.value.value : this.type_id.state.value )
        ) {
            valid = false;
        }

        if ( this.amount._validate( stripNonDigit( this.amount.state.value ) ) ) {
            valid = false;
        }

        if ( !this.frequency._checkRequire(
            typeof this.frequency.state.value === 'object' &&
            this.frequency.state.value !== null ? this.frequency.state.value.value : this.frequency.state.value )
        ) {
            valid = false;
        }

        if ( this.valid_from.checkRequired() ) {
            valid = false;
        }

        if ( this.state.deductionData.valid_to
            && this.state.deductionData.valid_from
            && !isAfter( this.state.deductionData.valid_to, this.state.deductionData.valid_from, [ DATE_FORMATS.DISPLAY, DATE_FORMATS.API ])
        ) {
            const message = 'The valid to date must be a date after or equal to valid from date.';
            this.valid_to.setState({ error: true, message });

            valid = false;
        }

        return valid;
    }

    updateDeductionData( key, value ) {
        if ( typeof value === 'undefined' ) {
            return;
        }

        if ( inputTypes.datePicker.includes( key ) ) {
            this.state.deductionData[ key ] = value;
            return;
        }

        const dataClone = { ...this.state.deductionData };
        dataClone[ key ] = value !== '' && value !== null ? value : '';

        this.setState({ deductionData: Object.assign( this.state.deductionData, dataClone ) });
    }

    validateTypeahead = ( value ) => {
        if ( typeof value === 'string' && value !== this.getEmployeeFullName() ) {
            this.employee_id.setState({ value, error: true, errorMessage: 'This field is required' });
        }
    }

    loadEmployees = ( keyword ) => {
        this.setState({ isLoading: true });
        const { companyId } = this.state;

        Fetch( `/company/${companyId}/employees?include=payroll&keyword=${keyword}`, { method: 'GET' })
          .then( ( result ) => {
              this.setState({ employees: result.data, isLoading: false });
          })
          .catch( ( error ) => error );
    }

    loadDeductionTypes = ( keyword, callback ) => {
        const { companyId } = this.state;

        Fetch( `/company/${companyId}/other_income_types/deduction_type?term=${keyword}`, { method: 'GET' })
          .then( ( result ) => {
              const deductionTypes = result.data.map( ({ id, name }) => ({
                  value: id,
                  label: name,
                  disabled: false
              }) );
              callback( null, { options: deductionTypes });
          })
          .catch( ( error ) => error );
    }

    renderForm = () => {
        if ( this.state.deductionData && this.state.deductionData.employee_id && this.state.deductionData.type_id ) {
            const payrollFrequency = this.props.formOptions.currentEmployee.payroll_group.payroll_frequency;

            return (
                <div>
                    <h2 className="sl-c-section-title">
                        <span className="sl-c-circle">2</span>
                        Deduction Details
                    </h2>
                    <div className="row">
                        <div className="col-xs-4">
                            <Input
                                id="amount"
                                label="Amount Deducted"
                                required
                                key="amount"
                                minNumber={ 1 }
                                ref={ ( ref ) => { this.amount = ref; } }
                                value={ this.state.deductionData.amount }
                                onChange={ ( value ) => { this.amount.setState({ value: stripNonDigit( value ) }); } }
                                onFocus={ () => { this.amount.setState({ value: stripNonDigit( this.amount.state.value ) }); } }
                                onBlur={ ( value ) => {
                                    this.amount.setState({ value: formatCurrency( value ) }, () => {
                                        this.updateDeductionData( 'amount', formatCurrency( value ) );
                                    });
                                } }
                            />
                            <span style={ { marginRight: 10 } }>* Recurring?</span>
                            <Switch
                                ref={ ( ref ) => { this.recurring = ref; } }
                                defaultChecked={ this.state.deductionData.recurring }
                                onChange={ this.toggleRecurring }
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-4">
                            <div style={ { marginTop: '20px' } }>
                                <SalSelect
                                    id="type_id"
                                    label="Frequency"
                                    required={ !!this.state.deductionData.recurring }
                                    disabled={ !this.state.deductionData.recurring }
                                    key="frequency"
                                    data={ this.props.formOptions.paymentSchemeOptions[ payrollFrequency ] }
                                    value={ this.state.deductionData.frequency }
                                    placeholder="Select a frequency option"
                                    ref={ ( ref ) => { this.frequency = ref; } }
                                    onChange={ ({ value }) => { this.updateDeductionData( 'frequency', value ); } }
                                />
                            </div>
                        </div>
                        <div className="col-xs-4 date" style={ { marginTop: '20px' } }>
                            <DatePicker
                                label="Start date"
                                dayFormat={ DATE_FORMATS.DISPLAY }
                                required
                                selectedDay={ this.state.deductionData.valid_from }
                                ref={ ( ref ) => { this.valid_from = ref; } }
                                onChange={ ( value ) => { this.updateDeductionData( 'valid_from', value ); } }
                            />
                        </div>
                        <div className="col-xs-4 date" style={ { marginTop: '20px' } }>
                            <DatePicker
                                label="Valid To"
                                dayFormat={ DATE_FORMATS.DISPLAY }
                                disabled={ !this.state.deductionData.recurring }
                                selectedDay={ this.state.deductionData.valid_to }
                                ref={ ( ref ) => { this.valid_to = ref; } }
                                onChange={ ( value ) => { this.updateDeductionData( 'valid_to', value ); } }
                            />
                        </div>
                    </div>
                </div>
            );
        }
        return null;
    }

    /**
     * Component render method
     */
    render() {
        const { loading } = this.props;
        return (
            <PageWrapper>
                <Helmet
                    title="Edit Deduction"
                    meta={ [
                        { name: 'description', content: 'Edit a Deduction' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();

                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/deductions' );
                            } }
                        >
                            &#8592; Back to { this.props.previousRoute.name ? this.props.previousRoute.name : 'Deductions' }
                        </A>
                    </Container>
                </NavWrapper>
                { loading
                    ? (
                        <LoaderWrapper>
                            <H2>Loading</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoaderWrapper>
                    ) : (
                        <div>
                            <HeadingWrapper>
                                <h3>Edit Deduction</h3>
                                <p>
                                    You may edit and manage the employee deduction through this page.
                                </p>
                            </HeadingWrapper>
                            <Container>
                                <SalConfirm
                                    onConfirm={ this.editDeduction }
                                    body={
                                        <ConfirmBodyWrapperStyle>
                                            <div className="message">
                                                Changing date may impact deductions.
                                                Deductions records may not match payslip records unless payslips that have been generated within deduction date range are regenerated.
                                                <br />
                                                Are you sure you want to proceed?
                                            </div>
                                        </ConfirmBodyWrapperStyle>
                                    }
                                    title="Warning!"
                                    visible={ this.state.showModal }
                                />
                                <h2 className="sl-c-section-title">
                                    <span className="sl-c-circle">1</span>
                                    Choose the deduction type
                                </h2>

                                <div className="row">
                                    <div className="col-xs-4">
                                        {this.props.loading ? null : (
                                            <Typeahead
                                                async
                                                id="employee_id"
                                                defaultValue={ this.getEmployeeFullName() }
                                                ref={ ( ref ) => { this.employee_id = ref; } }
                                                value={ this.getEmployeeFullName() }
                                                label="Employee name or Employee ID"
                                                required
                                                disabled={ !!this.state.prepopulatedEmployeeId }
                                                labelKey={ ( option ) => `${option.first_name} ${option.last_name} ${option.employee_id}` }
                                                filterBy={ [ 'first_name', 'last_name', 'employee_id' ] }
                                                placeholder="Start typing to search for an employee"
                                                options={ this.state.employees }
                                                onChange={ ( value ) => {
                                                    this.updateDeductionData( 'employee_id', value[ 0 ] ? value[ 0 ].id : null );
                                                    this.updateDeductionData( 'frequency', '' );
                                                } }
                                                onInputChange={ this.validateTypeahead }
                                                onBlur={ this.validateTypeahead }
                                                isLoading={ this.state.isLoading }
                                                onSearch={ this.loadEmployees }
                                            />
                                        ) }
                                    </div>
                                    <div className="col-xs-4">
                                        <SalSelect
                                            async
                                            id="type_id"
                                            label="Deduction type"
                                            required
                                            key="type_id"
                                            loadOptions={ this.loadDeductionTypes }
                                            defaultSelected={ this.state.deductionData ? this.state.deductionData.type_id : '' }
                                            data={ this.props.formOptions.deductionTypes }
                                            value={ this.state.deductionData ? this.state.deductionData.type_id : '' }
                                            placeholder="Choose a deduction type"
                                            ref={ ( ref ) => { this.type_id = ref; } }
                                            onChange={ ({ value }) => { this.updateDeductionData( 'type_id', value ); } }
                                        />
                                    </div>
                                </div>
                                { this.renderForm() }
                            </Container>
                            <AlignRight>
                                <div className="foot">
                                    <Button
                                        label="Cancel"
                                        type="neutral"
                                        size="large"
                                        onClick={ () => {
                                            this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/deductions' );
                                        } }
                                    />
                                    <Button
                                        className={ this.state.permission.edit ? '' : 'hide' }
                                        ref={ ( ref ) => { this.submitButton = ref; } }
                                        label={ this.props.submitted ? <Loader /> : 'Update' }
                                        type="action"
                                        size="large"
                                        onClick={ () => {
                                            if ( this.checkValidFrom() ) {
                                                this.editDeduction();
                                            }
                                        } }
                                        disabled={ this.props.submitted }
                                    />
                                </div>
                            </AlignRight>
                        </div>
                    )
                }
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    submitted: makeSelectSubmitted(),
    errors: makeSelectApiErrors(),
    formOptions: makeSelectFormOptions(),
    deductionData: makeSelectDeductionData(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Edit );
