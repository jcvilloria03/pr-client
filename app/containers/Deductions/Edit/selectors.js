import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectEditDeductionDomain = () => ( state ) => state.get( 'editDeduction' );

const makeSelectLoading = () => createSelector(
    selectEditDeductionDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectSubmitted = () => createSelector(
    selectEditDeductionDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectApiErrors = () => createSelector(
    selectEditDeductionDomain(),
    ( substate ) => substate.get( 'apiErrors' ).toJS()
);

const makeSelectFormOptions = () => createSelector(
    selectEditDeductionDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectDeductionData = () => createSelector(
    selectEditDeductionDomain(),
    ( substate ) => substate.get( 'deductionData' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectEditDeductionDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectApiErrors,
    makeSelectFormOptions,
    makeSelectDeductionData,
    makeSelectNotification
};
