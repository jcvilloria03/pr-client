import {
    INITIALIZE,
    EDIT_DEDUCTION,
    NOTIFICATION
} from './constants';

/**
 * Initialize data
 */
export function initializeData( id ) {
    return {
        type: INITIALIZE,
        payload: {
            id
        }
    };
}

/**
 * Update deduction preview
 */
export function editDeduction( payload ) {
    return {
        type: EDIT_DEDUCTION,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
