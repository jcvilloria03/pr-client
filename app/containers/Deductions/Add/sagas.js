import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { PAYMENT_SCHEME_OPTIONS } from '../../Loans/constants';
import { transformData, formatFeedbackMessage } from '../../../utils/functions';

import {
    GET_DEDUCTIONS_PREVIEW,
    INITIALIZE,
    GET_COMPANY_EMPLOYEES,
    LOADING,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SAVE_DEDUCTIONS,
    SET_BATCH_UPLOAD_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_DEDUCTIONS_PREVIEW_DATA,
    SET_DEDUCTIONS_PREVIEW_STATUS,
    SET_ERRORS,
    SET_FORM_OPTIONS,
    SET_SAVING_STATUS,
    SUBMITTED,
    SUBMIT_FORM,
    UPLOAD_DEDUCTIONS
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

const TYPE = 'deduction_type';

/**
 * Initialize data
 */
export function* initializeData() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formOptions = {};

        const deductionTypes = yield call( Fetch, `/company/${companyId}/other_income_types/${TYPE}`, { method: 'GET' });

        formOptions.deductionTypes = deductionTypes.data
            .filter( ( type ) => !type.deleted_at )
            .map( ( value ) => ({ label: value.name, value: value.id }) );

        formOptions.paymentSchemeOptions = PAYMENT_SCHEME_OPTIONS;

        const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddDeduction' ) );
        if ( prepopulatedEmployee ) {
            const filterEmployeeIdsQueryString = `&filter[employee_ids][]=${prepopulatedEmployee.id}`;
            const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL${filterEmployeeIdsQueryString}`, { method: 'GET' });
            formOptions.employees = employees.data;
        }

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Get Company Employees
 */
export function* getCompanyEmployees({ payload }) {
    try {
        const keyword = payload.keyword ? payload.keyword : '';
        const companyId = company.getLastActiveCompanyId();
        const formOptions = payload.formOptions;
        let filterEmployeeIdsQueryString = '';

        if ( payload.filterEmployeeIds ) {
            const filterEmployeeIds = payload.filterEmployeeIds;
            if ( filterEmployeeIds.length > 0 ) {
                for ( const employeeId of filterEmployeeIds ) {
                    filterEmployeeIdsQueryString += `&filter[employee_ids][]=${employeeId}`;
                }
            }
        }

        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL&keyword=${keyword}${filterEmployeeIdsQueryString}`, { method: 'GET' });

        formOptions.employees = employees.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        // Transform data to remove null keys
        const data = transformData({ ...payload });

        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, `/philippine/company/${companyId}/deduction/bulk_create`, { method: 'POST', data: [data]});
        yield call( showSuccessMessage );
        yield call( delay, 500 );

        payload.previousRouteName ? yield call( browserHistory.goBack ) : yield call( browserHistory.push, '/deductions' );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage()
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Uploads the CSV of deductions to add and starts the validation process
 */
export function* uploadDeductions({ payload }) {
    try {
        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: {}
        });

        const data = new FormData();
        const companyId = company.getLastActiveCompanyId();

        data.append( 'file', payload.file );
        data.append( 'company_id', companyId );

        const upload = yield call( Fetch, `/company/${companyId}/other_income/deduction/upload`, {
            method: 'POST',
            data
        });

        if ( upload.id ) {
            yield put({
                type: SET_BATCH_UPLOAD_JOB_ID,
                payload: upload.id
            });

            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: 'validation_queued'
            });

            yield call( checkValidation, { payload: { jobId: upload.id }});
        }
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();
        const check = yield call(
            Fetch,
            `/company/${companyId}/other_income/deduction/upload/status?job_id=${payload.jobId}`,
            { method: 'GET' }
        );

        if ( check.status === 'validation_failed' ) {
            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: check.status
            });

            yield put({
                type: SET_BATCH_UPLOAD_ERRORS,
                payload: check.errors
            });
        } else if ( check.status === 'saved' ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: check.status
            });

            yield call( browserHistory.push, '/deductions' );
        } else {
            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: check.status
            });

            yield call( delay, 2000 );
            yield call( checkValidation, { payload: { jobId: payload.jobId }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Fetches preview for uploaded deductions
 */
export function* getDeductionsPreview({ payload }) {
    try {
        yield put({
            type: SET_DEDUCTIONS_PREVIEW_DATA,
            payload: []
        });

        yield put({
            type: SET_DEDUCTIONS_PREVIEW_STATUS,
            payload: 'busy'
        });

        const preview = yield call(
            Fetch,
            `/company/other_income/deduction/upload/preview?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}`,
            { method: 'GET' }
        );

        yield put({
            type: SET_DEDUCTIONS_PREVIEW_DATA,
            payload: preview
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DEDUCTIONS_PREVIEW_STATUS,
            payload: 'ready'
        });
    }
}

/**
 * Saves validated deductions
 */
export function* saveDeductions({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const upload = yield call( Fetch, `/company/${companyId}/other_income/deduction/upload/save`, {
            method: 'POST',
            data: {
                company_id: company.getLastActiveCompanyId(),
                job_id: payload.jobId
            }
        });

        if ( upload.id ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: 'save_queued'
            });

            yield call( checkValidation, { payload: {
                step: 'save',
                jobId: payload.jobId
            }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPLOAD_DEDUCTIONS
 */
export function* watchForUploadDeductions() {
    const watcher = yield takeEvery( UPLOAD_DEDUCTIONS, uploadDeductions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_DEDUCTIONS_PREVIEW
 */
export function* watchForGetDeductionsPreview() {
    const watcher = yield takeEvery( GET_DEDUCTIONS_PREVIEW, getDeductionsPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SAVE_DEDUCTIONS
 */
export function* watchForSaveDeductions() {
    const watcher = yield takeEvery( SAVE_DEDUCTIONS, saveDeductions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_COMPANY_EMPLOYEES
 */
export function* watchForGetCompanyEmployees() {
    const watcher = yield takeEvery( GET_COMPANY_EMPLOYEES, getCompanyEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForGetDeductionsPreview,
    watchForUploadDeductions,
    watchForInitializeData,
    watchForGetCompanyEmployees,
    watchForReinitializePage,
    watchForSaveDeductions,
    watchNotify
];
