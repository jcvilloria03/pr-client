import {
    INITIALIZE,
    GET_COMPANY_EMPLOYEES,
    SUBMIT_FORM,
    UPLOAD_DEDUCTIONS,
    SAVE_DEDUCTIONS,
    NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData() {
    return {
        type: INITIALIZE
    };
}

/**
 * Display notification in page
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Get Company Employees
 */
export function getCompanyEmployees( payload ) {
    return {
        type: GET_COMPANY_EMPLOYEES,
        payload
    };
}

/**
 * Submit a new deduction
 * @param {object} payload
 */
export function submitForm( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}

/**
 * Upload deductions
 * @param {object} payload
 */
export function uploadDeductions( payload ) {
    return {
        type: UPLOAD_DEDUCTIONS,
        payload
    };
}

/**
 * Save deductions
 * @param {object} payload
 */
export function saveDeductions( payload ) {
    return {
        type: SAVE_DEDUCTIONS,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
