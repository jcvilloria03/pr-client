import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectAddDeductionDomain = () => ( state ) => state.get( 'addDeduction' );

const makeSelectDeduction = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => substate.get( 'deduction' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectFormOptions = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectDeductionsPreview = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => substate.get( 'deductionsPreview' ).toJS()
);

const makeSelectSaving = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectDeductionTypes = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => substate.get( 'deductionTypes' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectAddDeductionDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectBatchUploadErrors,
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectDeduction,
    makeSelectDeductionTypes,
    makeSelectDeductionsPreview,
    makeSelectErrors,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSaving,
    makeSelectSubmitted
};
