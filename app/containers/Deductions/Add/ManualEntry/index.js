import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import Typeahead from '../../../../components/Typeahead';
import SnackBar from '../../../../components/SnackBar';
import SalConfirm from '../../../../components/SalConfirm';
import Loader from '../../../../components/Loader';
import Button from '../../../../components/Button';
import SalSelect from '../../../../components/Select';
import Switch from '../../../../components/Switch';
import Input from '../../../../components/Input';
import DatePicker from '../../../../components/DatePicker';
import {
    formatCurrency,
    stripNonDigit,
    getFormDataWithFormattedDates,
    isSameOrAfter
} from '../../../../utils/functions';
import { browserHistory } from '../../../../utils/BrowserHistory';
import { isAuthorized } from '../../../../utils/Authorization';
import { DATE_FORMATS } from '../../../../utils/constants';

import {
    MainWrapper,
    ConfirmBodyWrapperStyle
} from './styles';

import {
    makeSelectDeductionTypes,
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectLoading
} from '../selectors';

import * as createDeductionActions from '../actions';

const inputTypes = {
    input: ['amount'],
    select: ['frequency'],
    datePicker: [ 'valid_to', 'valid_from' ]
};

/**
 * ManualEntry Container
 */
export class ManualEntry extends React.Component {
    static propTypes = {
        initializeData: React.PropTypes.func,
        getCompanyEmployees: React.PropTypes.func,
        formOptions: React.PropTypes.object,
        deductionTypes: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitForm: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        errors: React.PropTypes.object,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        previousRoute: React.PropTypes.object
    }

    static defaultProps = {
        loading: true,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deductionData: {
                type_id: null,
                amount: '',
                employee_id: null,
                frequency: '',
                recurring: false,
                valid_from: null,
                valid_to: null
            },
            showBatchModal: false,
            showWarningModal: false,
            errors: {},
            prepopulatedEmployee: null,
            typeaheadTimeout: null
        };
    }

    componentWillMount() {
        isAuthorized(['create.deductions'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        this.props.initializeData();

        const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddDeduction' ) );
        this.setState({ prepopulatedEmployee });
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });

        nextProps.submitted !== this.props.submitted && this.submitButton.setState({ disabled: nextProps.submitted });
    }

    componentDidUpdate() {
        if ( this.frequency && this.state.payroll_group_id === null ) {
            this.frequency.setState({
                error: true,
                errorMessage: 'Please add payroll group to this employee.'
            });
        }
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    getEmployeeFullName = () => {
        if ( this.props.formOptions.employees ) {
            const employee = this.props.formOptions.employees.find( ( e ) => e.id === parseInt(
                this.state.prepopulatedEmployee ? this.state.prepopulatedEmployee.id : this.state.deductionData.employee_id, 10
            ) );

            return employee ? `${employee.first_name} ${employee.last_name} ${employee.employee_id}` : '';
        }

        return '';
    }

    validateDeductionData() {
        let valid = true;

        if ( this.employee_id._validate( this.employee_id.state.value ) ) {
            valid = !!this.state.prepopulatedEmployee;
            if ( valid ) {
                this.employee_id.setState({
                    error: false,
                    errorMessage: '&nbsp;'
                });
            }
        }

        if ( !this.type_id._checkRequire(
            typeof this.type_id.state.value === 'object'
            && this.type_id.state.value !== null ? this.type_id.state.value.value : this.type_id.state.value )
        ) {
            valid = false;
        }

        if ( this.amount._validate( stripNonDigit( this.amount.state.value ) ) ) {
            valid = false;
        }

        if ( !this.frequency._checkRequire(
            typeof this.frequency.state.value === 'object' &&
            this.frequency.state.value !== null ? this.frequency.state.value.value : this.frequency.state.value )
        ) {
            valid = false;
        }

        if ( this.valid_from.checkRequired() ) {
            valid = false;
        }

        if ( this.state.deductionData.valid_to
            && this.state.deductionData.valid_from
            && !isSameOrAfter( this.state.deductionData.valid_to, this.state.deductionData.valid_from, [DATE_FORMATS.DISPLAY])
        ) {
            const message = 'The valid to date must be a date same or after valid from date.';
            this.valid_to.setState({ error: true, message });

            valid = false;
        }

        return valid;
    }

    updateDeductionData( key, value ) {
        if ( typeof value === 'undefined' ) {
            return;
        }

        if ( inputTypes.datePicker.includes( key ) ) {
            this.state.deductionData[ key ] = value;
            return;
        }

        const dataClone = { ...this.state.deductionData };
        dataClone[ key ] = value !== '' && value !== null ? value : '';
        if ( key === 'type_id' ) {
            this.deductionType = this.props.formOptions.deductionTypes.find( ( deductionType ) => deductionType.id === value );
        }
        this.setState({ deductionData: Object.assign( this.state.deductionData, dataClone ) });
    }

    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.input.includes( key ) || inputTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, label: this.state.errors[ key ][ 0 ] });
                } else if ( inputTypes.datePicker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    toggleRecurring = ( value ) => {
        this.updateDeductionData( 'recurring', value );
        if ( !value ) {
            this.updateDeductionData( 'frequency', '' );
            this.updateDeductionData( 'valid_to', null );
        }
    }

    submitForm = () => {
        if ( this.validateDeductionData() ) {
            const data = getFormDataWithFormattedDates( this.state.deductionData, DATE_FORMATS.DISPLAY, DATE_FORMATS.API );

            this.props.submitForm({
                ...data,
                amount: Number( stripNonDigit( data.amount ) ),
                recipients: { employees: [data.employee_id]},
                previousRouteName: this.props.previousRoute.name
            });

            delete data.employee_id;
        }
    }

    renderForm = () => {
        if ( this.state.prepopulatedEmployee ) {
            this.state.deductionData.employee_id = this.state.prepopulatedEmployee.id;
        }

        if ( this.state.deductionData.employee_id && this.state.deductionData.type_id ) {
            const employeeId = parseInt( this.state.deductionData.employee_id, 10 );
            const employee = this.props.formOptions.employees.find( ( e ) => e.id === employeeId );
            const payrollFrequency = employee.payroll_group.payroll_frequency;

            return (
                <div>
                    <h2 className="sl-c-section-title">
                        <span className="sl-c-circle">2</span>
                        Deduction Details
                    </h2>
                    <div className="row">
                        <div className="col-xs-4">
                            <Input
                                id="amount"
                                label="Amount Deducted"
                                required
                                key="amount"
                                minNumber={ 1 }
                                ref={ ( ref ) => { this.amount = ref; } }
                                onChange={ ( value ) => { this.amount.setState({ value: stripNonDigit( value ) }); } }
                                onFocus={ () => { this.amount.setState({ value: stripNonDigit( this.amount.state.value ) }); } }
                                onBlur={ ( value ) => {
                                    this.amount.setState({ value: formatCurrency( value ) }, () => {
                                        this.updateDeductionData( 'amount', formatCurrency( value ) );
                                    });
                                } }
                            />
                            <span style={ { marginRight: '10px' } }>* Recurring?</span>
                            <Switch
                                ref={ ( ref ) => { this.recurring = ref; } }
                                defaultChecked={ !!this.state.deductionData.recurring }
                                onChange={ this.toggleRecurring }
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-4">
                            <div style={ { marginTop: '20px' } }>
                                <SalSelect
                                    id="type_id"
                                    label="Frequency"
                                    required={ !!this.state.deductionData.recurring }
                                    disabled={ !this.state.deductionData.recurring }
                                    key="frequency"
                                    data={ this.props.formOptions.paymentSchemeOptions[ payrollFrequency ] }
                                    value={ this.state.deductionData.frequency }
                                    placeholder="Select a frequency option"
                                    ref={ ( ref ) => { this.frequency = ref; } }
                                    onChange={ ({ value }) => { this.updateDeductionData( 'frequency', value ); } }
                                />
                            </div>
                        </div>
                        <div className="col-xs-4 date" style={ { marginTop: '20px' } }>
                            <DatePicker
                                label="Start date"
                                dayFormat={ DATE_FORMATS.DISPLAY }
                                required
                                selectedDay={ this.state.deductionData.valid_from }
                                ref={ ( ref ) => { this.valid_from = ref; } }
                                onChange={ ( value ) => { this.updateDeductionData( 'valid_from', value ); } }
                            />
                        </div>
                        <div className="col-xs-4 date" style={ { marginTop: '20px' } }>
                            <DatePicker
                                label="Valid To"
                                dayFormat={ DATE_FORMATS.DISPLAY }
                                disabled={ !this.state.deductionData.recurring }
                                selectedDay={ this.state.deductionData.valid_to }
                                ref={ ( ref ) => { this.valid_to = ref; } }
                                onChange={ ( value ) => { this.updateDeductionData( 'valid_to', value ); } }
                            />
                        </div>
                    </div>
                </div>
            );
        }
        return null;
    }

    render() {
        return (
            <div>
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <MainWrapper>
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/deductions/batch-add' ); } }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showBatchModal }
                    />
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/employees', true ); } }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showWarningModal }
                    />
                    <div className={ `loader ${this.props.loading ? '' : 'hide'}` }>
                        <Loader />
                    </div>

                    <div className={ this.props.loading ? 'hide' : '' }>
                        <Container>
                            <h2 className="sl-c-section-title">
                                <span className="sl-c-circle">1</span>
                                Choose the deduction type
                            </h2>

                            <div className="row">
                                <div className="col-xs-4">
                                    {this.props.loading ? null : (
                                        <Typeahead
                                            id="employee_id"
                                            ref={ ( ref ) => { this.employee_id = ref; } }
                                            defaultValue={ this.getEmployeeFullName() }
                                            disabled={ !!this.state.prepopulatedEmployee }
                                            label="Employee name"
                                            required
                                            labelKey={ ( option ) => `${option.first_name} ${option.last_name} ${option.employee_id}` }
                                            filterBy={ [ 'first_name', 'last_name', 'employee_id' ] }
                                            placeholder="Start typing to search for an employee"
                                            options={ this.props.formOptions.employees }
                                            onInputChange={ ( value ) => {
                                                if ( value.trim() === '' ) {
                                                    return false;
                                                }

                                                if ( this.state.typeaheadTimeout ) {
                                                    clearTimeout( this.state.typeaheadTimeout );
                                                }

                                                this.state.typeaheadTimeout = setTimeout( () => {
                                                    if ( value.length >= 3 ) {
                                                        this.props.getCompanyEmployees({
                                                            keyword: value,
                                                            formOptions: this.props.formOptions
                                                        });
                                                    }
                                                }, 1000 );

                                                return true;
                                            } }
                                            onChange={ ( value ) => {
                                                this.updateDeductionData( 'employee_id', value[ 0 ] ? value[ 0 ].id : null );
                                                this.setState({ payroll_group_id: value[ 0 ] ? value[ 0 ].payroll.payroll_group_id : null });
                                            } }
                                        />
                                    ) }
                                </div>
                                <div className="col-xs-4">
                                    <SalSelect
                                        id="type_id"
                                        label="Deduction type"
                                        required
                                        key="type_id"
                                        data={ this.props.formOptions.deductionTypes }
                                        value={ this.state.deductionData.type_id }
                                        placeholder="Choose a deduction type"
                                        ref={ ( ref ) => { this.type_id = ref; } }
                                        onChange={ ({ value }) => { this.updateDeductionData( 'type_id', value ); } }
                                    />
                                </div>
                            </div>
                            { this.renderForm() }
                        </Container>
                        <div className="foot">
                            <Button
                                label="Cancel"
                                type="neutral"
                                size="large"
                                onClick={ () => {
                                    this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/deductions' );
                                } }
                            />
                            <Button
                                label={ this.props.submitted ? <Loader /> : 'Submit' }
                                disabled={ !this.state.deductionData.employee_id || !this.state.deductionData.type_id || this.props.submitted }
                                type="action"
                                size="large"
                                onClick={ () => { this.submitForm(); } }
                                ref={ ( ref ) => { this.submitButton = ref; } }
                            />
                        </div>
                    </div>
                </MainWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    deductionTypes: makeSelectDeductionTypes(),
    formOptions: makeSelectFormOptions(),
    submitted: makeSelectSubmitted(),
    errors: makeSelectErrors(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createDeductionActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ManualEntry );
