/*
 *
 * SALPay Administrators
 *
 */
const namespace = 'app/SalPayIntegration/Administrators';

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_MODAL = `${namespace}/SET_MODAL`;
export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const SET_ADMINISTRATORS = `${namespace}/SET_ADMINISTRATORS `;
export const GET_ADMINISTRATORS = `${namespace}/GET_ADMINISTRATORS`;
export const GET_PAGINATED_ADMINISTRATORS = `${namespace}/GET_PAGINATED_ADMINISTRATORS`;
export const SET_PAGINATION = `${namespace}/SET_PAGINATION`;
export const SET_FILTER_DATA = `${namespace}/SET_FILTER_DATA `;
export const SET_SALPAY_ADMINISTRATORS = `${namespace}/SET_SALPAY_ADMINISTRATORS `;
export const GET_SALPAY_ADMINISTRATORS = `${namespace}/GET_ADMINISTRATORS`;
export const SET_COMPANIES = `${namespace}/SET_COMPANIES`;
export const GET_COMPANIES = `${namespace}/GET_COMPANIES `;
export const ADD_ADMINISTRATOR = `${namespace}/ADD_ADMINISTRATOR `;
export const REMOVE_ADMINISTRATOR = `${namespace}/REMOVE_ADMINISTRATOR `;
export const SET_COMPANY_DETAILS = `${namespace}/SET_COMPANY_DETAILS `;
export const NOTIFICATION = 'app/Allowances/View/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/Allowances/View/NOTIFICATION_SAGA';
export const SET_ADMIN_LIST_LOADING = `${namespace}/SET_ADMIN_LIST_LOADING`;
export const SET_SALPAY_ADMIN_LIST_LOADING = `${namespace}/SET_SALPAY_ADMIN_LIST_LOADING`;
export const ALLOW_PAYEE_INVITE = `${namespace}/ALLOW_PAYEE_INVITE`;
export const SET_ALLOW_PAYEE_INVITE_STATUS = `${namespace}/SET_ALLOW_PAYEE_INVITE_STATUS`;
