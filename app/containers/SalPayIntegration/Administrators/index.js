/* eslint-disable import/first */
import React from 'react';
import Helmet from 'react-helmet';
import isEqual from 'lodash/isEqual';
import toLower from 'lodash/toLower';
import startCase from 'lodash/startCase';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { isAuthorized } from 'utils/Authorization';
import {
    SALPAY_INTEGRATION_SUBHEADER_ITEMS
} from 'utils/constants';
import { browserHistory } from 'utils/BrowserHistory';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { salpayAdminService } from 'utils/SalpayAdminService';
import {
    formatPaginationLabel,
    formatDeleteLabel
} from 'utils/functions';
import { H2, H3, H4, H5 } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Table from 'components/Table';
import { isAnyRowSelected } from 'components/Table/helpers';
import Button from 'components/Button';
import Modal from 'components/Modal';
import SnackBar from 'components/SnackBar';
import SalConfirm from 'components/SalConfirm';

import Loader from 'components/Loader';
import SubHeader from 'containers/SubHeader';
import {
    makeSelectUserInformationState
} from 'containers/App/selectors';
import Switch from '../../../components/Switch';

import * as actions from './actions';
import {
    PageWrapper,
    LoadingStyles,
    CompanyEntryWrapper,
    TableTitleWrapper,
    SwitchWrapper
} from './styles';
import {
    makeSelectCompanies,
    makeSelectAdministrators,
    makeSelectLoading,
    makeSelectCompanyDetails,
    makeSelectPagination,
    makeSelectNotification,
    makeSelectAdminListLoading,
    makeSelectSalpayAccountAdmins,
    makeSelectSalpayAdminListLoading,
    makeSelectModal,
    makeSelectAllowPayeeInviteStatus
} from './selectors';

/**
 * SALPay Company Details Component
 */
export class SalpayAdministrators extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        userInformation: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        getPaginatedAdministrators: React.PropTypes.func,
        loading: React.PropTypes.bool,
        adminListLoading: React.PropTypes.bool,
        salpayAdminListLoading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        companyDetails: React.PropTypes.object,
        administrators: React.PropTypes.array,
        pagination: React.PropTypes.object,
        addAdministrator: React.PropTypes.func,
        removeAdministrator: React.PropTypes.func,
        showModal: React.PropTypes.bool,
        setModal: React.PropTypes.func,
        allowPayeeInviteStatus: React.PropTypes.bool,
        allowPayeeInvite: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: this.props.administrators,
            accountViewPermission: false,
            salpayPermission: {
                view: false,
                create: false,
                delete: false,
                edit: false
            },
            action_user: {
                show: false,
                message: '',
                title: '',
                onConfirm: () => {}
            },
            delete_label: '',
            show_modal: false,
            displayedSalpayAdminsData: [],
            allow_invite: false,
            is_default_value: true
        };

        this.administratorsTable = null;
        this.salpayAdministratorsTable = null;
        this.salpayBusinessAdministratorsModal = null;
    }

    componentWillMount() {
        this.props.products
            && !subscriptionService.isSubscribedToPayroll( this.props.products )
            && browserHistory.replace( '/unauthorized' );

        isAuthorized([
            'view.account',
            'create.salpay_integration',
            'view.salpay_integration',
            'edit.salpay_integration',
            'delete.salpay_integration'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.salpay_integration' ];

            if ( authorized ) {
                this.setState({
                    accountViewPermission: authorization[ 'view.account' ],
                    salpayPermission: {
                        view: authorization[ 'view.salpay_integration' ],
                        create: authorization[ 'create.salpay_integration' ],
                        delete: authorization[ 'delete.salpay_integration' ],
                        edit: authorization[ 'edit.salpay_integration' ]
                    }
                });
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.initializeData();
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.administrators, this.props.administrators ) ) {
            this.handleSearch( nextProps.administrators );
        }

        if ( !isEqual( this.props.notification, nextProps.notification ) ) {
            this.onConfirmActionUserClose();
        }

        if ( nextProps.salpayAdministrators ) {
            const filteredUser = nextProps.salpayAdministrators.filter( ( k ) => !this.props.administrators.some( ( j ) => k.user_id === j.user_id ) );
            this.setState({ displayedSalpayAdminsData: filteredUser });
        }

        if ( !nextProps.showModal ) {
            this.salpayBusinessAdministratorsModal.close();
        }

        if ( !isEqual( this.props.allowPayeeInviteStatus, nextProps.allowPayeeInviteStatus ) ) {
            this.setState({
                allow_invite: nextProps.allowPayeeInviteStatus,
                is_default_value: false
            });
            this.onConfirmActionUserClose();
        }
    }

    onConfirmRemoveUser = ( value ) => {
        const message = 'You are about to remove this user as a Salarium SALPay Integeration admin. Please confirm if you wish to proceed?';
        const title = 'Remove User?';

        const onConfirm = () => this.props.removeAdministrator( value );

        this.setState({
            action_user: {
                show: true,
                message,
                title,
                onConfirm,
                confirmText: 'Confirm',
                buttonStyle: 'primary'
            }
        });
    }

    onConfirmAddUser = () => {
        const data = this.getSelectedEntries( this.salpayAdministratorsTable );

        const message = 'You are about to add this user as a Salarium SALPay Integeration admin. Please confirm if you wish to proceed?';
        const title = 'Add Users?';

        const onConfirm = () => this.props.addAdministrator( this.props.companyDetails.id, data );

        this.setState({
            action_user: {
                show: true,
                message,
                title,
                onConfirm,
                confirmText: 'Confirm',
                buttonStyle: 'primary'
            }
        });
    }

    onConfirmActionUserClose() {
        this.setState({
            action_user: {
                show: false,
                title: '',
                onConfirm: () => {}
            }
        });
    }

    /**
     * Constructs payload using the selected employees
     *
     * @param {Object} table - Table ref
     * @returns {Object[]}
     */
    getSelectedEntries( table ) {
        const data = [];

        table.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                const {
                    salpay_user_id,
                    user_id,
                    mobile_number,
                    email,
                    salpay_type
                } = table.props.data[ index ];

                data.push({
                    salpay_user_id,
                    user_id,
                    mobile_number,
                    email,
                    salpay_type
                });
            }
        });

        return data;
    }

    getSalpayTableColumns() {
        return [
            {
                header: 'user_id',
                accessor: 'user_id',
                show: false
            },
            {
                header: 'Name',
                accessor: 'name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        { startCase( toLower( row.name ) ) }
                    </div>
                )
            },
            {
                header: 'Email',
                accessor: 'email',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.email}
                    </div>
                )
            },
            {
                header: 'Mobile',
                accessor: 'mobile_number',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.mobile_number}
                    </div>
                )
            },
            {
                header: 'Type',
                accessor: 'salpay_type',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.salpay_type}
                    </div>
                )
            }
        ];
    }

    getTableColumns = ( value ) => {
        const columns = [
            {
                header: 'Name',
                accessor: 'user_name',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        { startCase( toLower( row.user_name ) ) }
                    </div>
                )
            },
            {
                header: 'Email',
                accessor: 'user_email',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.user_email}
                    </div>
                )
            },
            {
                header: 'Mobile',
                accessor: 'user_mobile_number',
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.user_mobile_number}
                    </div>
                )
            },
            {
                header: 'Type',
                accessor: 'is_owner',
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.is_owner ? 'owner' : 'admin'}
                    </div>
                )
            },
            {
                header: '',
                accessor: 'actions',
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        label={ <span>Remove</span> }
                        type="danger"
                        disabled={ row.is_owner || ( this.props.administrators && this.props.administrators.length === 1 ) }
                        onClick={ () => {
                            this.onConfirmRemoveUser( row );
                        } }
                    />
                )
            }
        ];

        if ( !value ) {
            return columns.filter( ( item ) => item.accessor !== 'actions' );
        }

        return columns;
    }

    handleInvitePayeePermission = ( value ) => {
        let message = 'You are about to disable employee invitation to all Non SALPay Salarium Integration Administrator. Please confirm if you wish to proceed?';

        if ( value ) {
            message = 'You are about to allow all Salarium Administrator to invite employees. Please confirm if you wish to proceed?';
        }

        const title = 'Employee Invitation Permission';
        const onConfirm = () => this.props.allowPayeeInvite(
            this.props.companyDetails.id,
            {
                payee_invite: value,
                salpay_account_number: this.props.companyDetails.salpay_account_number
            }
        );

        this.setState({
            action_user: {
                show: true,
                message,
                title,
                onConfirm,
                confirmText: 'Confirm',
                buttonStyle: 'primary'
            }
        });
    }

    handleSearch = ( administrators = this.props.administrators ) => {
        const dataToDisplay = administrators;

        this.setState({ displayedData: dataToDisplay });
    }

    handleTableChanges = ( tableProps = this.administratorsTable.tableComponent.state ) => {
        Object.assign(
            tableProps,
            {
                dataLength: this.props.pagination.total
            }
        );

        this.setState({
            label: formatPaginationLabel( tableProps ),
            delete_label: formatDeleteLabel()
        });
    }

    isSalpayAdmin() {
        if ( this.props.companyDetails && this.props.userInformation ) {
            return salpayAdminService.isSalpayAdmin(
                this.props.companyDetails.id,
                this.props.userInformation.salpay_settings
            );
        }
        return false;
    }

    isAllowedToInvite() {
        if ( this.props.companyDetails ) {
            return salpayAdminService.isAllowedToInvite( this.props.companyDetails.id );
        }
        return false;
    }

    /**
     * Component Render Method
     */
    render() {
        const { accountViewPermission } = this.state;
        const inviteStatus = this.state.is_default_value ? this.isAllowedToInvite() : this.state.allow_invite;

        return (
            <main>
                <Helmet
                    title="SALPay Integration"
                    meta={ [
                        { name: 'SALPay Integration - Administrators', content: 'SALPay Integration - Administrators' }
                    ] }
                />
                <SubHeader
                    items={ SALPAY_INTEGRATION_SUBHEADER_ITEMS }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.state.action_user.onConfirm }
                    onClose={ () => this.onConfirmActionUserClose() }
                    body={
                        <div style={ { display: 'flex', padding: '0 20px' } }>
                            <div style={ { alignSelf: 'center' } } >
                                { this.state.action_user.message }
                            </div>
                        </div>
                    }
                    title={ this.state.action_user.title }
                    buttonStyle={ this.state.action_user.buttonStyle }
                    visible={ this.state.action_user.show }
                    confirmText={ this.state.action_user.confirmText }
                    showCancel={ this.state.salpayPermission.delete }
                />
                <Modal
                    ref={ ( ref ) => { this.salpayBusinessAdministratorsModal = ref; } }
                    size="lg"
                    title="SALPay Administrators"
                    backdrop={ 'static' }
                    showClose={ false }
                    keyboard={ false }
                    body={ this.props.showModal && (
                        <Table
                            data={ this.state.displayedSalpayAdminsData }
                            columns={ this.getSalpayTableColumns() }
                            onDataChange={ this.handlesSalpayAdministratorsTableChanges }
                            selectable
                            onSelectionChange={ ({ selected }) => {
                                const selectionLength = selected.filter( ( row ) => row ).length;
                                this.setState({ delete_label: formatDeleteLabel( selectionLength ) });
                            } }
                            pagination
                            ref={ ( ref ) => { this.salpayAdministratorsTable = ref; } }
                        />
                        )
                    }
                    buttons={ [
                        {
                            label: this.props.salpayAdminListLoading ? <Loader /> : 'Add',
                            type: 'action',
                            disabled: !isAnyRowSelected( this.salpayAdministratorsTable )
                                || this.props.salpayAdminListLoading || this.state.displayedSalpayAdminsData.length === 0,
                            onClick: () => this.onConfirmAddUser()
                        },
                        {
                            label: 'Cancel',
                            type: 'grey',
                            onClick: () => this.props.setModal( false )
                        }
                    ] }
                />
                <PageWrapper>
                    <Sidebar
                        items={ getControlPanelSidebarLinks({
                            salpayViewPermission: true,
                            isSubscribedToPayroll: true,
                            accountViewPermission
                        }) }
                    />
                    { this.props.loading && !this.props.administrators && (
                        <div className="loader">
                            <LoadingStyles>
                                <H2>Loading SALPay Integration Administrator List</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                    ) }

                    { !this.props.loading && (
                        <section className="content">
                            <header className="heading">
                                <H3>SALPay Integration Company Administrators</H3>
                                <p>You can identify who are authorized to make SALPay related transactions</p>
                                <div style={ { textAlign: 'center' } } >
                                    <div className="main">
                                        <Button
                                            className={ this.state.salpayPermission.create ? '' : 'hide' }
                                            label={ this.props.salpayAdminListLoading ? <Loader /> : 'Add SALPay Administrator' }
                                            size="large"
                                            type="action"
                                            disabled={
                                                !this.isSalpayAdmin()
                                                || !this.props.companyDetails.linked
                                                || this.props.adminListLoading
                                                || this.props.salpayAdminListLoading
                                            }
                                            onClick={ () => {
                                                this.props.setModal( true );
                                                this.salpayBusinessAdministratorsModal.toggle();
                                            } }
                                        />
                                    </div>
                                </div>
                            </header>

                            <CompanyEntryWrapper>
                                <H4>Company Account Details</H4>
                                <div>
                                    <div className="details">
                                        <div>
                                            <H5>SALPay Account</H5>
                                            <div>{ this.props.companyDetails.salpay_account_number || '-' }</div>
                                        </div>
                                        <div>
                                            <H5>SALPay Company</H5>
                                            <div>{ this.props.companyDetails.salpay_account_name || '-' }</div>
                                        </div>
                                        <div>
                                            <H5>Salarium Company</H5>
                                            <div>{ this.props.companyDetails.name || '-' }</div>
                                        </div>
                                        <div>
                                            <H5>Status</H5>
                                            <div className={ this.props.companyDetails.linked ? 'linked' : 'unlinked' }>{ this.props.companyDetails.linked ? 'Linked' : 'Unlinked' }</div>
                                        </div>
                                    </div>
                                </div>
                            </CompanyEntryWrapper>

                            <SwitchWrapper>
                                <div className="details">
                                    <div>
                                        <Switch
                                            checked={ inviteStatus }
                                            onChange={ ( value ) => {
                                                this.handleInvitePayeePermission( value );
                                            } }
                                            disabled={ !this.isSalpayAdmin() || this.props.adminListLoading }
                                        />
                                        <span style={ { marginLeft: '12px' } }> Allow all Salarium Administrators to invite and re-invite employees </span>
                                        <div style={ { marginTop: '15px', marginBottom: '15px', fontWeight: 600 } }> Note: Only below SALPay Integration Administrators may run disbursement </div>
                                    </div>
                                </div>
                            </SwitchWrapper>

                            <TableTitleWrapper>
                                <H5>Administrator List</H5>
                            </TableTitleWrapper>
                            <Table
                                data={ this.state.displayedData }
                                columns={ this.getTableColumns( this.isSalpayAdmin() ) }
                                pagination
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.administratorsTable = ref; } }
                                loading={ this.props.adminListLoading }
                                onPageChange={ ( data ) => this.props.getPaginatedAdministrators( this.props.companyDetails.id, { page: data + 1, perPage: this.props.pagination.per_page, showLoading: true }) }
                                onPageSizeChange={ ( data ) => this.props.getPaginatedAdministrators( this.props.companyDetails.id, { page: 1, perPage: data, showLoading: true }) }
                                page={ this.props.pagination.current_page - 1 }
                                pageSize={ this.props.pagination.per_page }
                                pages={ this.props.pagination.last_page }
                                manual

                            />
                        </section>
                    ) }
                </PageWrapper>
            </main>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    companyDetails: makeSelectCompanyDetails(),
    pagination: makeSelectPagination(),
    companies: makeSelectCompanies(),
    userInformation: makeSelectUserInformationState(),
    administrators: makeSelectAdministrators(),
    adminListLoading: makeSelectAdminListLoading(),
    salpayAdministrators: makeSelectSalpayAccountAdmins(),
    salpayAdminListLoading: makeSelectSalpayAdminListLoading(),
    showModal: makeSelectModal(),
    allowPayeeInviteStatus: makeSelectAllowPayeeInviteStatus()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( SalpayAdministrators );
