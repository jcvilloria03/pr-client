import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    margin-left: 91px;
    display: flex;
    flex-direction: column;
    align-items: center;

    .content {
        margin-top: 40px;
        padding: 0 20px;
        width: 100%;
        max-width: 1140px;
        display: flex;
        flex-direction: column;
        align-items: center;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 30px auto;

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .row-wrapper {
            width: 100%;
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;

            .link-icon {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                margin: 0 30px;

                .fa {
                    margin: 5px 0;
                }
            }
        }

        .section-footer {
            width: 100%;
            display: flex;
            justify-content: flex-end;
        }

        & > div {
            width: 100%;
        }

        .filter-icon > svg {
            height: 10px;
        }
    }
`;

export const CompanyEntryWrapper = styled.div`
    border-radius: 5px;
    letter-spacing: 0.12px;
    border: solid 1px #adadad;
    background-color: #fff;
    padding: 20px;
    margin-bottom: 40px;
    width: 100%;

    & > div {
        display: flex;
        flex-direction: row;
        align-items: flex-start;
        width: 100%;
        padding-left: 20px;

        .details {
            display: flex;
            flex-grow: 1;
            flex-direction: row;
            align-items: flex-start;
            justify-content: space-between;
            margin-right: 20px;
        }

        .action {
            min-width: 195px;
            text-align: center;
        }
    }

    .linked {
        color: #83d24b;
    }

    .unlinked {
        color: #F21108;
    }
`;

export const NavWrapper = styled.div`
    margin-top: 70px;
    padding: 10px 20px;
    background: #f0f4f6;
`;

export const TableTitleWrapper = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 20px;

    h5 {
        margin: 0;
        margin-right: 20px;
        font-weight: 600;
        font-size: 22px;
    }

    .search-wrapper {
        flex-grow: 1;

        .search {
            width: 300px;
            border: 1px solid #333;
            border-radius: 30px;

            input {
                border: none;
            }
        }

        p {
            display: none;
        }

        .input-group,
        .form-control {
            background-color: transparent;
        }

        .input-group-addon {
            background-color: transparent;
            border: none;
        }

        .isvg {
            display: inline-block;
            width: 1rem;
        }
    }
`;

export const SwitchWrapper = styled.div`
    width: 100%;
    display: flex;
    justify-content: left;
    margin-bottom: 20px;

    & > div {
        display: flex;
        flex-direction: row;
        align-items: flex-start;
        width: 100%;

        .details {
            display: flex;
            flex-grow: 1;
            flex-direction: row;
            align-items: flex-start;
            justify-content: left;
        }
    }
`;
