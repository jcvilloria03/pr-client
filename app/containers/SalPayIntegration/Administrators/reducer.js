import { fromJS } from 'immutable';
import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_COMPANIES,
    SET_ADMINISTRATORS,
    SET_PAGINATION,
    SET_FILTER_DATA,
    SET_SALPAY_ADMINISTRATORS,
    NOTIFICATION_SAGA,
    SET_COMPANY_DETAILS,
    SET_ADMIN_LIST_LOADING,
    SET_SALPAY_ADMIN_LIST_LOADING,
    SET_MODAL,
    SET_ALLOW_PAYEE_INVITE_STATUS
} from './constants';

const initialState = fromJS({
    loading: false,
    show_modal: false,
    allow_payee_invite: false,
    company_details: {},
    administrators: [],
    companies: [],
    salpay_administrators: [],
    admin_list_loading: false,
    salpay_admin_list_loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    pagination: {
        current_page: 0,
        from: 0,
        last_page: 0,
        per_page: 0,
        to: 0,
        total: 0
    }
});

/**
 *
 * SALPay Company Details
 *
 */
function salpayPayees( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_MODAL:
            return state.set( 'show_modal', action.payload );
        case SET_COMPANIES:
            return state.set( 'companies', fromJS( action.payload ) );
        case SET_ADMIN_LIST_LOADING:
            return state.set( 'admin_list_loading', action.payload );
        case SET_SALPAY_ADMIN_LIST_LOADING:
            return state.set( 'salpay_admin_list_loading', action.payload );
        case SET_FILTER_DATA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_COMPANY_DETAILS:
            return state.set( 'company_details', fromJS( action.payload ) );
        case SET_ADMINISTRATORS:
            return state.set( 'administrators', fromJS( action.payload ) );
        case SET_SALPAY_ADMINISTRATORS:
            return state.set( 'salpay_administrators', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_ALLOW_PAYEE_INVITE_STATUS:
            return state.set( 'allow_payee_invite', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default salpayPayees;
