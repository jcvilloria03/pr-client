import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageAdministratorsDomain = () => ( state ) => state.get( 'salpayAdministrators' );

/**
 * Default selector used by SALPay Administrators
 */
const makeSelectLoading = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectCompanies = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'companies' ).toJS()
);

const makeSelectPagination = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectAdministrators = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'administrators' ).toJS()
);

const makeSelectAdminListLoading = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'admin_list_loading' )
);

const makeSelectSalpayAdminListLoading = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'salpay_admin_list_loading' )
);

const makeSelectFilterData = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'filter_data' ).toJS()
);

const makeSelectSalpayAccountAdmins = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'salpay_administrators' ).toJS()
);

const makeSelectCompanyDetails = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'company_details' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectModal = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'show_modal' )
);

const makeSelectAllowPayeeInviteStatus = () => createSelector(
    selectPageAdministratorsDomain(),
    ( substate ) => substate.get( 'allow_payee_invite' )
);

export {
    makeSelectCompanyDetails,
    makeSelectAdministrators,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectPagination,
    makeSelectFilterData,
    makeSelectSalpayAccountAdmins,
    makeSelectCompanies,
    makeSelectAdminListLoading,
    makeSelectSalpayAdminListLoading,
    makeSelectModal,
    makeSelectAllowPayeeInviteStatus
};
