import {
    SET_LOADING,
    SET_MODAL,
    SET_ADMIN_LIST_LOADING,
    SET_SALPAY_ADMIN_LIST_LOADING,
    INITIALIZE_DATA,
    SET_ADMINISTRATORS,
    GET_ADMINISTRATORS,
    SET_COMPANIES,
    GET_PAGINATED_ADMINISTRATORS,
    SET_COMPANY_DETAILS,
    ADD_ADMINISTRATOR,
    REMOVE_ADMINISTRATOR,
    SET_SALPAY_ADMINISTRATORS,
    GET_SALPAY_ADMINISTRATORS,
    SET_PAGINATION,
    NOTIFICATION,
    ALLOW_PAYEE_INVITE,
    SET_ALLOW_PAYEE_INVITE_STATUS
} from './constants';

/**
 * Set loading status of the page
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Set status of the modal
 * @param {Boolean} payload - modal status
 * @returns {Object} action
 */
export function setModal( payload ) {
    return {
        type: SET_MODAL,
        payload
    };
}

/**
 * Allow payee invite
 * @param {Integer} companyId - Company ID
 * @param {Boolean} payload - payee invite permission status
 * @returns {Object} action
 */
export function allowPayeeInvite( companyId, payload ) {
    return {
        type: ALLOW_PAYEE_INVITE,
        companyId,
        payload
    };
}

/**
 * Set allow payee invite status
 * @param {Boolean} payload - payee invite permission status
 * @returns {Object} action
 */
export function setAllowPayeeInviteStatus( payload ) {
    return {
        type: SET_ALLOW_PAYEE_INVITE_STATUS,
        payload
    };
}

/**
 * Initialize data
 * @returns {Object} action
 */
export function initializeData() {
    return {
        type: INITIALIZE_DATA
    };
}

/**
 * Sets dministrators
 * @param {Boolean} payload - administrators
 * @returns {Object} action
 */
export function setAdministrators( payload ) {
    return {
        type: SET_ADMINISTRATORS,
        payload
    };
}

/**
 * Sets SALPay dministrators
 * @param {Boolean} payload - administrators
 * @returns {Object} action
 */
export function setSalpayAdministrators( payload ) {
    return {
        type: SET_SALPAY_ADMINISTRATORS,
        payload
    };
}

/**
 * Get SALPay administrators
 */
export function getSalpayAdministrators() {
    return {
        type: GET_SALPAY_ADMINISTRATORS
    };
}

/**
 * Get administrators
 */
export function getAdministrators( companyId ) {
    return {
        type: GET_ADMINISTRATORS,
        companyId
    };
}

/**
 * Sets pagination
 * @param {Boolean} payload - pagination details
 * @returns {Object} action
 */
export function setPagination( payload ) {
    return {
        type: SET_PAGINATION,
        payload
    };
}

/**
 * Fetch list of admins
 */
export function getPaginatedAdministrators( companyId, payload ) {
    return {
        type: GET_PAGINATED_ADMINISTRATORS,
        companyId,
        payload
    };
}

/**
 * Sets company details
 * @param {Object} payload
 * @returns {Object} action
 */
export function setCompanyDetails( payload ) {
    return {
        type: SET_COMPANY_DETAILS,
        payload
    };
}

/**
 * Sets loading status of Admin List table
 * @param {Boolean} payload - Loading Status
 * @returns {Object} action
 */
export function setAdminListLoading( payload ) {
    return {
        type: SET_ADMIN_LIST_LOADING,
        payload
    };
}

/**
 * Sets loading status of SALPay admin List table
 * @param {Boolean} payload - Loading Status
 * @returns {Object} action
 */
export function setSalpayAdminListLoading( payload ) {
    return {
        type: SET_SALPAY_ADMIN_LIST_LOADING,
        payload
    };
}

/**
 * Sets companies
 * @param {Boolean} payload - companies
 * @returns {Object} action
 */
export function setCompanies( payload ) {
    return {
        type: SET_COMPANIES,
        payload
    };
}

/**
 * Sends request to add user as an admin
 * @param {Integer} companyId - Company ID
 * @param {Integer} salariumUserId - Salarium User ID
 * @param {String} email - Email
 * @returns {Object} action
 */
export function addAdministrator( companyId, payload ) {
    return {
        type: ADD_ADMINISTRATOR,
        companyId,
        payload
    };
}

/**
 * Sends request to remove user as an admin
 * @param {Integer} companyId - Company ID
 * @param {Integer} adminRecordId - Admin Record ID
 * @returns {Object} action
 */
export function removeAdministrator( payload ) {
    return {
        type: REMOVE_ADMINISTRATOR,
        payload
    };
}

/**
 * Display a notification to a user
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
