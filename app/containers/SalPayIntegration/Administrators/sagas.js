import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { Fetch } from 'utils/request';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';
import { auth } from 'utils/AuthService';

import {
    INITIALIZE_DATA,
    GET_ADMINISTRATORS,
    GET_PAGINATED_ADMINISTRATORS,
    ADD_ADMINISTRATOR,
    REMOVE_ADMINISTRATOR,
    GET_SALPAY_ADMINISTRATORS,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    ALLOW_PAYEE_INVITE
} from './constants';

import {
    setLoading,
    setAdminListLoading,
    setSalpayAdminListLoading,
    setPagination,
    setAdministrators,
    setSalpayAdministrators,
    setCompanies,
    setCompanyDetails,
    setModal,
    setAllowPayeeInviteStatus
} from './actions';

import { company } from '../../../utils/CompanyService';

/**
 * Initialize data
 */
export function* initializeData() {
    yield put( setLoading( true ) );

    try {
        const userId = auth.getUser().id;
        const companies = yield call( Fetch, `/salpay/users/${userId}/companies`, { method: 'GET' });
        const companyDetails = companies.data.find( ( c ) => c.id === company.getLastActiveCompanyId() );

        yield put( setCompanies( companies.data ) );
        yield put( setCompanyDetails( companyDetails ) );

        yield put( setLoading( false ) );

        const companyId = companyDetails.id;
        yield call( getPaginatedAdministrators, {
            companyId,
            payload: {
                page: 1,
                perPage: 10,
                showLoading: true
            }
        });

        yield call( getSalpayAdministrators );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * get admin list
 */
export function* getAdministrators( companyId ) {
    try {
        yield put( setAdminListLoading( true ) );
        const response = yield call(
            Fetch,
            `/salpay/companies/${companyId}/salpay-account/administrators`,
            { method: 'GET' }
        );

        const { data } = response;
        yield put( setAdministrators( data ) );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setAdminListLoading( false ) );
    }
}

/**
 * get paginated admin list
 */
export function* getPaginatedAdministrators({ companyId, payload }) {
    const { page, perPage, showLoading } = payload;

    try {
        yield put( setAdminListLoading( showLoading ) );

        const response = yield call(
                Fetch,
                `/salpay/companies/${companyId}/salpay-account/administrators?page=${page}&per_page=${perPage}`,
                { method: 'GET' }
            );

        const { data, ...others } = response;

        yield put( setPagination( others ) );
        yield put( setAdministrators( data ) );
    } catch ( error ) {
        yield put( setAdminListLoading( false ) );
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setAdminListLoading( false ) );
    }
}

/**
 * get salpay business admin list
 */
export function* getSalpayAdministrators() {
    try {
        yield put( setSalpayAdminListLoading( true ) );

        const companyId = company.getLastActiveCompanyId();
        const salpayAdmins = yield call( Fetch, `/salpay/companies/${companyId}/salpay-business/administrators`, { method: 'GET' });

        const salpayAdminsData = salpayAdmins.data ? salpayAdmins.data : [];
        yield put( setSalpayAdministrators( salpayAdminsData ) );
    } catch ( error ) {
        yield call(
            notifyUser,
            {
                show: true,
                title: error.response ? error.response.statusText : 'Error',
                message: error.response ? error.response.data.message : error.statusText,
                type: 'error'
            }
        );
    } finally {
        yield put( setSalpayAdminListLoading( false ) );
    }
}

/**
 * Add user as an admin
 */
export function* addAdministrator({ companyId, payload }) {
    yield put( setSalpayAdminListLoading( true ) );
    try {
        yield call( Fetch, `/salpay/companies/${companyId}/salpay-account/administrators`, {
            method: 'POST',
            data: {
                company_id: companyId,
                administrators: payload
            }
        });

        yield call( getAdministrators, companyId );

        yield put( setSalpayAdminListLoading( false ) );
        yield put( setModal( false ) );

        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully deleted',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield put( setSalpayAdminListLoading( false ) );
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Remove user as an admin
 */
export function* removeAdministrator({ payload }) {
    const companyId = payload.company_id;
    const adminId = payload.id;
    try {
        yield call( Fetch, `/salpay/companies/${companyId}/salpay-account/administrators/${adminId}`, {
            method: 'DELETE'
        });

        yield call( getAdministrators, companyId );

        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully deleted',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Update salarium admin payee invite permission
 */
export function* allowPayeeInvite({ companyId, payload }) {
    const { payee_invite, salpay_account_number } = payload;

    yield put( setSalpayAdminListLoading( true ) );
    try {
        yield call( Fetch, `/salpay/companies/${companyId}/salpay-settings`, {
            method: 'Patch',
            data: {
                payee_invite,
                salpay_account_number
            }
        });

        yield put( setAllowPayeeInviteStatus( payee_invite ) );
        yield put( setSalpayAdminListLoading( false ) );
    } catch ( error ) {
        yield put( setSalpayAdminListLoading( false ) );
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
    yield call( initializeData );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for INITIALIZE_DATA
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_PAGINATED_ADMINISTRATORS
 */
export function* watchForGetPaginatedAdministrators() {
    const watcher = yield takeEvery( GET_PAGINATED_ADMINISTRATORS, getPaginatedAdministrators );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_ADMINISTRATORS
 */
export function* watchForGetAdministrators() {
    const watcher = yield takeEvery( GET_ADMINISTRATORS, getAdministrators );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_SALPAY_ADMINISTRATORS
 */
export function* watchForSalpayAdministrators() {
    const watcher = yield takeEvery( GET_SALPAY_ADMINISTRATORS, getSalpayAdministrators );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REMOVE_ADMINISTRATOR
 */
export function* watchForRemoveAdministrator() {
    const watcher = yield takeEvery( REMOVE_ADMINISTRATOR, removeAdministrator );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for ADD_ADMINISTRATOR
 */
export function* watchForAddAdministrator() {
    const watcher = yield takeEvery( ADD_ADMINISTRATOR, addAdministrator );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for ALLOW_PAYEE_INVITE
 */
export function* watchAllowPayeeInvite() {
    const watcher = yield takeEvery( ALLOW_PAYEE_INVITE, allowPayeeInvite );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForReinitializePage,
    watchForGetPaginatedAdministrators,
    watchForGetAdministrators,
    watchForSalpayAdministrators,
    watchForAddAdministrator,
    watchForRemoveAdministrator,
    watchForNotifyUser,
    watchAllowPayeeInvite
];
