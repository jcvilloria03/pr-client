import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'salpayCompanyDetails' );

/**
 * Default selector used by SALPay Company Details
 */

const makeSelectCompanyDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'company_details' ).toJS()
);

const makeSelectEmployees = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'employees' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectEmployeeListLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'employee_list_loading' )
);

const makeSelectSalpayShippingDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'salpay_shipping_details' ).toJS()
);

const makeSelectSalpayShippingDetailsLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'salpay_shipping_details_loading' )
);

const makeSelectSalpayShippingDetailsErrors = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'salpay_validated_shipping_errors' ).toJS()
);

const makeSelectValidateSalpayShippingDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'validate_salpay_shipping_details' )
);

const makeValidatedSelectSalpayShippingDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'salpay_validated_shipping_details' ).toJS()
);

const makeSelectCountries = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'countries' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectPagination = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'pagination' ).toJS()
  );

export {
    makeSelectCompanyDetails,
    makeSelectEmployees,
    makeSelectLoading,
    makeSelectEmployeeListLoading,
    makeSelectSalpayShippingDetails,
    makeSelectSalpayShippingDetailsLoading,
    makeSelectSalpayShippingDetailsErrors,
    makeSelectValidateSalpayShippingDetails,
    makeValidatedSelectSalpayShippingDetails,
    makeSelectCountries,
    makeSelectNotification,
    makeSelectPagination
};
