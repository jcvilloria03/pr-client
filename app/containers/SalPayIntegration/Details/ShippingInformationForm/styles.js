import styled from 'styled-components';

export const FieldWrapper = styled.div`
    margin-bottom: 14px;
`;

export const LoaderWrapper = styled.div`
    padding: 40px 20px;
`;
