import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';
import { browserHistory } from 'utils/BrowserHistory';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import { company } from '../../../utils/CompanyService';

import {
    INITIALIZE_DATA,
    INVITE_PAYEES,
    REINVITE_PAYEES,
    UNLINK_PAYEES,
    GET_SALPAY_SHIPPING_DETAILS,
    GET_COUNTRIES,
    NOTIFY,
    SET_PAGINATION,
    GET_PAGINATED_EMPLOYEES,
    GET_EMPLOYEES
} from './constants';
import {
    setLoading,
    setNotification,
    setEmployees,
    setCompanyDetails,
    setEmployeeListLoading,
    setSalpayShippingDetails,
    setSalpayShippingDetailsLoading,
    setSalpayShippingDetailsErrors,
    setValidatedSalpayShippingDetails,
    setCountries
} from './actions';

/**
 * Initialize data
 * @param {Integer} payload - Company ID
 */
export function* initializeData({ payload }) {
    yield put( setLoading( true ) );

    try {
        const userId = auth.getUser().id;
        const [companies] = yield [
            call( Fetch, `/salpay/users/${userId}/companies`, { method: 'GET' }),
            call( getEmployees, { id: payload })
        ];

        const companyDetails = companies.data.find( ( c ) => c.id === Number.parseInt( payload, 10 ) );
        yield put( setCompanyDetails( companyDetails ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Get employees
 * @param {Integer} id - Company ID
 */
export function* getEmployees( payload ) {
    yield put( setEmployeeListLoading( true ) );
    try {
        yield call( getPaginatedEmployees, {
            id: payload.id,
            payload: {
                page: 1,
                perPage: 10,
                showLoading: true,
                filters: payload.filters,
                orderBy: 'last_name',
                orderDir: 'asc'
            }
        });

        yield put( setEmployeeListLoading( false ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setEmployeeListLoading( false ) );
    }
}

/**
 * Sends request to get paginated employees
 * @param {Integer} id - Company ID
 */
export function* getPaginatedEmployees({ id, payload }) {
    const { page, perPage, showLoading, filters, orderBy, orderDir } = payload;
    yield put( setEmployeeListLoading( showLoading ) );

    // Breakdown filters
    let filtersQueryParams = '';
    if ( filters !== undefined && filters.length > 0 ) {
        const filtersIds = {
            status: []
        };

        filters.map( ( filter ) => {
            if ( filter.type !== 'keyword' ) {
                const filterFieldName = 'status';
                if ( filter.disabled === false ) {
                    filtersIds[ filterFieldName ].push( filter.value );
                }
                filtersIds[ filterFieldName ].push( filter.value );
            } else {
                filtersQueryParams += `&filter[keyword]=${filter.value}`;
            }

            return null;
        });

        for ( const [ field, ids ] of Object.entries( filtersIds ) ) {
            if ( ids.length > 0 ) {
                for ( const filterId of ids ) {
                    filtersQueryParams += `&filter[${field}][]=${filterId}`;
                }
            }
        }
    }

    try {
        const response = yield call( Fetch, `/salpay/companies/${id}/employees?page=${page}&per_page=${perPage}&order_by=${orderBy}&order_dir=${orderDir}${filtersQueryParams}`, { method: 'GET' });
        const { employees, ...others } = response;
        yield put( setEmployees( employees ) );

        yield put({
            type: SET_PAGINATION,
            payload: others
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setEmployeeListLoading( false ) );
    }
}

/**
 * Sends request to invite payees
 * @param {Integer} payload.company_id - Company ID
 * @param {Integer{}} payload.employees - List of Employee IDs to invite as payee
 */
export function* invitePayees({ payload }) {
    if ( payload.shippingDetails !== null ) {
        yield put( setSalpayShippingDetailsLoading( true ) );
    } else {
        yield put( setEmployeeListLoading( true ) );
    }

    yield put( setSalpayShippingDetailsErrors([]) );

    try {
        yield call( Fetch, `/salpay/companies/${payload.company_id}/employee-invitations`, {
            method: 'POST',
            data: {
                invites: payload.employees,
                shipping_details: payload.shippingDetails || null
            }
        });

        // Reset validation status
        yield put( setValidatedSalpayShippingDetails({
            isValid: false,
            data: null,
            shouldCloseModal: true
        }) );

        yield call( getEmployees, { id: payload.company_id });

        yield call( notifyUser, {
            payload: {
                title: 'Payee Invitation Sent',
                message: "We have successfully sent an invitation to the payee's registered email address",
                show: true,
                type: 'success'
            }
        });
    } catch ( error ) {
        if ( payload.shippingDetails !== null && error.response && error.response.status === 422 ) {
            yield put( setSalpayShippingDetailsLoading( false ) );
            yield put( setSalpayShippingDetailsErrors( error.response.data.errors ) );
        } else {
            yield call( notifyError, error );
        }
    } finally {
        if ( payload.shippingDetails !== null ) {
            yield put( setSalpayShippingDetailsLoading( false ) );
        } else {
            yield put( setEmployeeListLoading( false ) );
        }
    }
}

/**
 * Sends request to invite payees
 * @param {Integer} employeeId - Employee
 */
export function* reInvitePayees({ payload }) {
    const employeeId = payload.employee_id;
    const withCard = payload.with_card;
    const companyId = company.getLastActiveCompanyId();

    try {
        yield call( Fetch, `/salpay/companies/${companyId}/employee-reinvitations`, {
            method: 'PUT',
            data: {
                invites: [
                    {
                        employee_id: employeeId,
                        with_card: withCard
                    }
                ]
            }
        });

        yield call( notifyUser, {
            payload: {
                title: 'Payee Invitation Sent',
                message: "We have successfully sent an invitation to the payee's registered email address",
                show: true,
                type: 'success'
            }
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Sends request to unlink payees
 * @param {Integer} employeeId - Employee
 */
export function* unlinkPayees({ payload }) {
    const employeeId = payload.employee_id;
    const linkStatus = payload.link_status;
    const companyId = company.getLastActiveCompanyId();
    try {
        yield call( Fetch, `/salpay/companies/${companyId}/employee-unlink-invitation`, {
            method: 'DELETE',
            data: { employee_id: employeeId }
        });

        yield call( getEmployees, { id: companyId });

        if ( linkStatus === 'INVITE_SENT' ) {
            yield call( notifyUser, {
                payload: {
                    title: 'Remove Payee Invitation',
                    message: "We have successfully removed the payee's SALPay invitation",
                    show: true,
                    type: 'success'
                }
            });
        } else {
            yield call( notifyUser, {
                payload: {
                    title: 'Unlink SALPay Account',
                    message: "We have successfully unlinked the payee's SALPay Account",
                    show: true,
                    type: 'success'
                }
            });
        }
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
    }
}

/**
 * Get countries
 */
export function* getSalpayShippingDetails({ companyId }) {
    yield put( setSalpayShippingDetailsLoading( true ) );

    try {
        const response = yield call( Fetch, `/salpay/companies/${companyId}/invite-shipping-details`, { method: 'GET' });

        yield put( setSalpayShippingDetails( response.shipping_details ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setSalpayShippingDetailsLoading( false ) );
    }
}

/**
 * Get countries
 */
export function* getCountries() {
    try {
        const { data } = yield call( Fetch, '/countries', { method: 'GET' });

        yield put( setCountries( data ) );
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, { payload });
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser({ payload }) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( browserHistory.push, '/control-panel/salpay-integration', true );
}

/**
 * Watcher for INITIALIZE_DATA
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for INVITE_PAYEES
 */
export function* watchForInvitePayees() {
    const watcher = yield takeEvery( INVITE_PAYEES, invitePayees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for INVITE_PAYEES
 */
export function* watchForReinvitePayees() {
    const watcher = yield takeEvery( REINVITE_PAYEES, reInvitePayees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for UNLINK_PAYEES
 */
export function* watchForUnlinkPayees() {
    const watcher = yield takeEvery( UNLINK_PAYEES, unlinkPayees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_SALPAY_SHIPPING_DETAILS
 */
export function* watchForGetSalpayShippingDetails() {
    const watcher = yield takeEvery( GET_SALPAY_SHIPPING_DETAILS, getSalpayShippingDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_COUNTRIES
 */
export function* watchForGetCountries() {
    const watcher = yield takeEvery( GET_COUNTRIES, getCountries );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFY
 */
export function* watchForNotify() {
    const watcher = yield takeEvery( NOTIFY, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Paginated employees
 */
export function* watchForPaginatedEmployees() {
    const watcher = yield takeEvery( GET_PAGINATED_EMPLOYEES, getPaginatedEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Paginated employees
 */
export function* watchForEmployees() {
    const watcher = yield takeEvery( GET_EMPLOYEES, getEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForInvitePayees,
    watchForReinvitePayees,
    watchForUnlinkPayees,
    watchForGetSalpayShippingDetails,
    watchForGetCountries,
    watchForNotify,
    watchForReinitializePage,
    watchForPaginatedEmployees,
    watchForEmployees
];
