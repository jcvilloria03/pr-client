import { fromJS } from 'immutable';
import lowerCase from 'lodash/lowerCase';
import capitalize from 'lodash/capitalize';
import get from 'lodash/get';

import { formatFullName } from 'utils/functions';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_NOTIFICATION,
    SET_COMPANY_DETAILS,
    SET_EMPLOYEES,
    SET_EMPLOYEE_LIST_LOADING,
    SET_SHIPPING_INFORMATION,
    SET_SALPAY_SHIPPING_DETAILS,
    SET_SALPAY_SHIPPING_DETAILS_LOADING,
    SET_SALPAY_SHIPPING_DETAILS_ERRORS,
    VALIDATE_SALPAY_SHIPPING_DETAILS,
    SET_VALIDATED_SALPAY_SHIPPING_DETAILS,
    SET_COUNTRIES,
    SET_PAGINATION
} from './constants';

const initialState = fromJS({
    loading: false,
    company_details: {},
    employees: [],
    employee_list_loading: false,
    salpay_shipping_details: [],
    salpay_shipping_details_loading: false,
    salpay_validated_shipping_details: {
        isValid: false,
        data: null
    },
    salpay_validated_shipping_errors: {},
    countries: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    pagination: {
        current_page: 0,
        from: 0,
        last_page: 0,
        per_page: 0,
        to: 0,
        total: 0
    }
});

/**
 * Formats list of employees
 * @param {Array} employees - List of employees
 * @returns {Array}
 */
function processEmployees( employees ) {
    return employees.map( ( employee ) => ({
        ...employee,
        employee: formatFullName({
            first_name: employee.first_name,
            middle_name: employee.middle_name,
            last_name: employee.last_name
        }),
        status: capitalize( lowerCase( employee.link_status ) ) || 'Not yet invited',
        with_card: employee.link_status === null || get( employee, 'with_card', true ),
        noselect: employee.link_status !== null
    }) );
}

/**
 *
 * SALPay Company Details
 *
 */
function salpayCompanyDetails( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_EMPLOYEE_LIST_LOADING:
            return state.set( 'employee_list_loading', action.payload );
        case SET_EMPLOYEES:
            return state.set( 'employees', fromJS( processEmployees( action.payload ) ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_COMPANY_DETAILS:
            return state.set( 'company_details', fromJS( action.payload ) );
        case SET_SHIPPING_INFORMATION:
            return state.set( 'shipping_information', fromJS( action.payload ) );
        case SET_SALPAY_SHIPPING_DETAILS:
            return state.set( 'salpay_shipping_details', fromJS( action.payload ) );
        case SET_SALPAY_SHIPPING_DETAILS_LOADING:
            return state.set( 'salpay_shipping_details_loading', action.payload );
        case VALIDATE_SALPAY_SHIPPING_DETAILS:
            return state.set( 'validate_salpay_shipping_details', action.payload );
        case SET_VALIDATED_SALPAY_SHIPPING_DETAILS:
            return state.set( 'salpay_validated_shipping_details', fromJS( action.payload ) );
        case SET_SALPAY_SHIPPING_DETAILS_ERRORS:
            return state.set( 'salpay_validated_shipping_errors', fromJS( action.payload ) );
        case SET_COUNTRIES:
            return state.set( 'countries', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default salpayCompanyDetails;
