import styled from 'styled-components';

export const OtpWrapper = styled.div`
    max-width: 300px;
    text-align: center;

    button {
        color: #00A5E5;
        font-weight: 500;

        &:hover {
            cursor: pointer;
        }
    }

    .expired {
        display: flex;
        flex-direction: column;
    }
`;

export const InputMask = styled.article`
    position: relative;
    padding-top: 30px;
    margin-bottom: 20px;
    display: ${( props ) => ( props.expired ? 'none' : 'block' )};

    .code {
        display: flex;
        justify-content: center;
        align-items: center;

        .char {
            height: ${( props ) => ( props.desktop ? '16vw' : '40px' )};
            width: ${( props ) => ( props.desktop ? '12%' : '30px' )};
            border: 1px solid #ececec;
            border-radius: ${( props ) => ( props.desktop ? '6px' : '3px' )};
            display: flex;
            justify-content: center;
            align-items: center;
            font-size: ${( props ) => ( props.desktop ? '36px' : '22px' )};
            color: #474747 !important;

            &:not(:last-of-type) {
                margin-right: 10px;
            }

            &.active {
                border: 1px solid #c7c7c7;
            }
        }
    }

    input {
        opacity: 0;
        margin-left: -100vw;
    }

    .error {
        margin-top: ${( props ) => ( props.desktop ? '10px' : '-20px' )};
        font-size: ${( props ) => ( props.desktop ? '16px' : '12px' )};
        color: #e40000;
        text-align: center;
    }
`;
