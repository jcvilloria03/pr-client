import React from 'react';

import { OtpWrapper, InputMask } from './styles';

import OtpBlocked from '../OtpBlocked';

/**
* Otp
*/
class Otp extends React.PureComponent {
    static propTypes = {
        error: React.PropTypes.string,
        triesLeft: React.PropTypes.number,
        mobile: React.PropTypes.string,
        onRef: React.PropTypes.func,
        resendOtp: React.PropTypes.func,
        onChange: React.PropTypes.func,
        blocked: React.PropTypes.bool,
        remainingDuration: React.PropTypes.number,
        actionLocked: React.PropTypes.bool
    }

    static defaultProps = {
        mobile: '',
        error: '',
        triesLeft: 0,
        remainingDuration: 0
    }

    constructor( props ) {
        super( props );

        this.state = {
            code: '',
            countdown: null,
            timer: null
        };

        this.checkResend = this.checkResend.bind( this );
        this.resetTimer = this.resetTimer.bind( this );
    }

    componentDidMount() {
        this.props.onRef( this );
        this.input && this.input.focus();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.error.message !== this.props.error.message
            && this.resetTimer( false );
    }

    componentWillUnmount() {
        this.state.timer && clearInterval( this.state.timer );
        this.props.onRef( null );
    }

    resetTimer( resendCode = true ) {
        clearInterval( this.state.timer );
        this.setState({
            code: '',
            countdown: this.props.blocked ? 0 : 60,
            timer: this.props.blocked ? null : setInterval( this.checkResend, 1000 )
        }, () => {
            if ( resendCode ) {
                this.props.resendOtp();
            }
        });
    }

    checkResend() {
        const { countdown } = this.state;
        if ( countdown > 0 ) {
            this.setState({ countdown: countdown - 1 });
        } else {
            clearInterval( this.state.timer );
            this.setState({ timer: null });
        }
    }

    renderChars() {
        const { code } = this.state;
        const inputs = [];
        for ( let i = 0; i < 4; i += 1 ) {
            inputs.push(
                <div
                    key={ i }
                    className={ `char${code.length === i ? ' active' : ''}` }
                >
                    { code[ i ] || '' }
                </div>
            );
        }

        return (
            <label htmlFor="code" className="code" >
                { inputs }
            </label>
        );
    }

    /**
    * OTP render method.
    */
    render() {
        const {
            error,
            mobile,
            blocked,
            remainingDuration,
            triesLeft,
            actionLocked
        } = this.props;

        if ( blocked ) {
            return (
                <OtpBlocked
                    mobile={ mobile }
                    remainingDuration={ remainingDuration }
                    actionLocked={ actionLocked }
                />
            );
        }

        return (
            <OtpWrapper className="otp">
                <p className="message">Enter the OTP that was sent through the registered mobile number</p>
                <InputMask expired={ blocked }>
                    { this.renderChars() }
                    <input
                        id="code"
                        name="code"
                        type="number"
                        pattern="[0-9]*"
                        onChange={ ( event ) => {
                            let value = event.target.value;
                            value = value.replace( /\D/g, '' );
                            if ( value.length <= 4 ) {
                                this.setState({
                                    code: value
                                }, () => this.props.onChange( this.state.code ) );
                            }
                        } }
                        maxLength="4"
                        max={ 9999 }
                        value={ this.state.code }
                        ref={ ( ref ) => { this.input = ref; } }
                        autoComplete="off"
                    />
                    <div className="error" style={ { opacity: error ? 1 : 0 } } >
                        { error ? (
                            <span>
                                You have entered an invalid OTP.
                                <br />
                                Number of attempts left: { triesLeft } out of 3
                            </span>
                        ) : '' }
                    </div>
                </InputMask>
            </OtpWrapper>
        );
    }
}

export default Otp;
