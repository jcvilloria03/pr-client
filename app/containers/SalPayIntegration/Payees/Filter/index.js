import React from 'react';

import Button from 'components/Button';
import MultiSelect from 'components/MultiSelect';

import { FilterWrapper } from './styles';

import { FILTER_TYPES } from './constants';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.PureComponent {
    static propTypes = {
        filterData: React.PropTypes.shape({
            status: React.PropTypes.array
        }).isRequired,
        onCancel: React.PropTypes.func.isRequired,
        onApply: React.PropTypes.func.isRequired
    };

    onApply = () => {
        const filters = [];
        this.status.state.value && this.status.state.value.forEach( ( status ) => {
            filters.push({ ...status, type: FILTER_TYPES.STATUS });
        });

        this.props.onApply( filters );
    }

    getStatuses = () => {
        if ( !this.props.filterData.status ) {
            return [];
        }
        const statuses = this.props.filterData.status.map( ( status ) => ({
            value: status,
            label: status
        }) );

        return statuses;
    }

    resetFilters = () => {
        this.status.setState({ value: []}, () => {
            this.onApply();
        });
    }

    render() {
        return (
            <FilterWrapper>
                <div className="row">
                    <div className="col-xs-3">
                        <MultiSelect
                            id="status"
                            label="Status"
                            data={ this.getStatuses() }
                            placeholder="All status"
                            ref={ ( ref ) => { this.status = ref; } }
                        />
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ this.props.onCancel }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
