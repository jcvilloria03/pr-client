import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import isEqual from 'lodash/isEqual';
import toLower from 'lodash/toLower';
import get from 'lodash/get';

import { isAuthorized } from 'utils/Authorization';
import {
    SALPAY_INTEGRATION_SUBHEADER_ITEMS
} from 'utils/constants';
import { browserHistory } from 'utils/BrowserHistory';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { salpayAdminService } from 'utils/SalpayAdminService';
import {
    formatPaginationLabel,
    formatDeleteLabel
} from 'utils/functions';
import { H2, H3, H4, H5 } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Search from 'components/Search';
import Table from 'components/Table';
import { isAnyRowSelected } from 'components/Table/helpers';
import SalDropdown from 'components/SalDropdown';
import Button from 'components/Button';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import SnackBar from 'components/SnackBar';
import SalConfirm from 'components/SalConfirm';
import Switch from 'components/Switch';

import SubHeader from 'containers/SubHeader';
import {
    makeSelectUserInformationState
} from 'containers/App/selectors';

import ShippingInformationForm from './ShippingInformationForm';

import Filter from './Filter';
import * as actions from './actions';
import {
    PageWrapper,
    LoadingStyles,
    CompanyEntryWrapper,
    TableTitleWrapper,
    SwitchWrapper
} from './styles';
import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectCompanyDetails,
    makeSelectEmployees,
    makeSelectEmployeeListLoading,
    makeSelectSalpayShippingDetailsErrors,
    makeValidatedSelectSalpayShippingDetails,
    makeSelectPagination,
    makeSelectFilterData
} from './selectors';

/**
 * SALPay Company Details Component
 */
export class SalpayPayees extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        initializeData: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.object,
        employeeListLoading: React.PropTypes.bool,
        companyDetails: React.PropTypes.object,
        employees: React.PropTypes.array,
        invitePayees: React.PropTypes.func,
        reInvitePayees: React.PropTypes.func,
        unlinkPayees: React.PropTypes.func,
        notify: React.PropTypes.func,
        salpayShippingDetailsErrors: React.PropTypes.object,
        validateSalpayShippingDetails: React.PropTypes.func,
        getPaginatedEmployees: React.PropTypes.func,
        pagination: React.PropTypes.object,
        filterData: React.PropTypes.shape({
            companies: React.PropTypes.array,
            status: React.PropTypes.array
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            accountViewPermission: false,
            salpayPermission: {
                view: false,
                create: false,
                delete: false,
                edit: false
            },
            label: 'Showing 0-0 of 0 entries',
            delete_label: '',
            displayed_data: [],
            show_modal: false,
            dropdown_items: [
                {
                    label: 'Invite Payees',
                    children: <div>Invite Payees</div>,
                    onClick: this.onClickInvite
                }
            ],
            show_filter: false,
            has_filters_applied: false,
            filters: [],
            previous_query: '',
            invite_payees: {
                show: false,
                message: '',
                onConfirm: () => {}
            },
            unlink_payees: {
                show: false,
                message: '',
                title: '',
                onConfirm: () => {}
            },
            shipping_information: null,
            orderBy: 'last_name',
            orderDir: 'asc'
        };

        this.search = null;
        this.employeeListTable = null;
        this.shippingDetailsModal = null;
        this.onSortingChange = this.onSortingChange.bind( this );
        this.reInvitePayees = this.reInvitePayees.bind( this );
    }

    componentWillMount() {
        this.props.products
            && !subscriptionService.isSubscribedToPayroll( this.props.products )
            && browserHistory.replace( '/unauthorized' );

        isAuthorized([
            'view.account',
            'create.salpay_integration',
            'view.salpay_integration',
            'edit.salpay_integration',
            'delete.salpay_integration'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.salpay_integration' ];

            if ( authorized ) {
                this.setState({
                    accountViewPermission: authorization[ 'view.account' ],
                    salpayPermission: {
                        view: authorization[ 'view.salpay_integration' ],
                        create: authorization[ 'create.salpay_integration' ],
                        delete: authorization[ 'delete.salpay_integration' ],
                        edit: authorization[ 'edit.salpay_integration' ]
                    }
                });
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.initializeData();
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( this.props.employees, nextProps.employees ) ) {
            this.handleSearch( nextProps.employees );
        }

        if ( nextProps.validatedSalpayShippingDetails.isValid ) {
            this.confirmInvite( nextProps.validatedSalpayShippingDetails.data );
        }

        if ( nextProps.validatedSalpayShippingDetails.shouldCloseModal ) {
            this.shippingDetailsModal.close();
        }

        if ( !isEqual( this.props.salpayShippingDetailsErrors, nextProps.salpayShippingDetailsErrors ) ) {
            this.onConfirmInviteClose();
        }

        if ( !isEqual( this.props.notification, nextProps.notification ) ) {
            this.onConfirmUnlinkClose();
        }
    }

    onSortingChange( column ) {
        let isDesc = false;

        if ( column.id === this.state.orderBy ) {
            if ( this.state.orderDir === 'asc' ) {
                isDesc = true;
            }
        }

        const setOrderDir = isDesc ? 'desc' : 'asc';

        // set new sorting states
        this.setState({
            orderBy: column.id,
            orderDir: setOrderDir
        });

        // Apply current filters
        const searchQuery = toLower( get( this.search, 'searchInput.state.value', '' ) );
        const currentFilters = this.state.filters.filter( ( filter ) => filter.type !== 'keyword' );
        const filtersToApply = [
            ...currentFilters,
            {
                type: 'keyword',
                value: searchQuery
            }
        ];

        // Perform request to apply sort
        if ( this.props.companyDetails ) {
            this.props.getPaginatedEmployees( this.props.companyDetails.id, { page: this.props.pagination.current_page, perPage: this.props.pagination.per_page, showLoading: true, filters: this.state.filters, orderBy: column.id, orderDir: setOrderDir });
        }

        this.setState({
            displayed_data: this.props.employees,
            has_filters_applied: this.state.filters.length !== 0,
            filters: filtersToApply
        }, () =>
            this.handleTableChanges()
        );
    }

    onClickInvite = () => {
        const employees = this.getSelectedEntries( this.employeeListTable );

        if ( employees.some( ( employee ) => employee.link_status === 'LINK_SUCCESS' ) ) {
            this.props.notify({
                show: true,
                title: 'Error',
                message: 'Payee is already linked to a SALPay Account. Please unlink the employee first via SALPay for Business',
                type: 'error'
            });
            return;
        }

        const hasEmployeeWithCard = employees.some( ( employee ) => employee.with_card );

        if ( hasEmployeeWithCard ) {
            this.shippingDetailsModal.toggle();
        } else {
            this.confirmInvite();
        }
    }

    onConfirmInviteClose() {
        this.setState({
            invite_payees: {
                show: false,
                onConfirm: () => {}
            }
        });
    }

    onConfirmUnlinkPayees( employeeId, linkStatus ) {
        const message = 'You are about to unlink this payee on SALPay Integeration. Please confirm if you wish to proceed?';
        const title = 'Unlink Payee?';

        const onConfirm = () => this.props.unlinkPayees( employeeId, linkStatus );

        this.setState({
            unlink_payees: {
                show: true,
                message,
                title,
                onConfirm,
                confirmText: 'Confirm',
                buttonStyle: 'primary'
            }
        });
    }

    onConfirmUnlinkClose() {
        this.setState({
            unlink_payees: {
                show: false,
                title: '',
                onConfirm: () => { }
            }
        });
    }

    /**
     * Constructs payload using the selected employees
     *
     * @param {Object} table - Table ref
     * @returns {Object[]}
     */
    getSelectedEntries( table ) {
        const employees = [];

        table.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                const {
                    id: employee_id,
                    with_card,
                    link_status
                } = table.props.data[ index ];

                employees.push({
                    employee_id,
                    with_card,
                    link_status
                });
            }
        });

        return employees;
    }

    getTableColumns( isAllowedToInvite ) {
        return [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Employees',
                accessor: 'employee'
            },
            {
                header: 'Email',
                accessor: 'email'
            },
            {
                header: 'SALPay Account Number',
                accessor: 'salpay_account_number',
                minWidth: 130,
                sortable: false,
                render: ({ row }) => row.salpay_account_number || '-'
            },
            {
                header: 'Status',
                accessor: 'status',
                sortable: false
            },
            {
                header: '',
                accessor: '',
                minWidth: 130,
                sortable: false,
                render: ({ row }) => ( row.link_status === 'INVITE_SENT' && isAllowedToInvite && (
                    <div>
                        <Button label="Reinvite" type="primary" onClick={ () => this.reInvitePayees( row.id, row.with_card ) } />
                        <Button label="Remove Invite" type="danger" onClick={ () => this.onConfirmUnlinkPayees( row.id, row.link_status ) } />
                    </div>

                ) ) || ( row.link_status === 'LINK_SUCCESS' && (
                    <div>
                        <Button label="Unlink" type="danger" onClick={ () => this.onConfirmUnlinkPayees( row.id, row.link_status ) } />
                    </div>
                ) )
            },
            {
                header: 'Require a SALPay Card?',
                accessor: 'with_card',
                minWidth: 110,
                sortable: false,
                render: ({ row }) => row.link_status !== 'LINK_SUCCESS' && (
                    <SwitchWrapper>
                        <Switch
                            id={ `switch-salpay-card-${row.id}` }
                            checked={ row.with_card }
                            onChange={ ( value ) => this.handleSalpayCardToggle( row.id, value ) }
                        />
                    </SwitchWrapper>
                )
            }
        ];
    }

    reInvitePayees( employeeId, withCard ) {
        this.props.reInvitePayees( employeeId, withCard );
    }

    confirmInvite = ( shippingDetails = null ) => {
        const employees = this.getSelectedEntries( this.employeeListTable );

        let message;

        if ( employees.some( ( employee ) => !employee.with_card ) ) {
            message = 'You are attempting to invite payee(s) with no reserved SALPay Card. Are you sure you want to proceed?';
        } else {
            message = 'You are about to invite these employees as payees in your SALPay Business Account. Please confirm if you wish to proceed?';
        }

        const onConfirm = () => this.props.invitePayees(
            this.props.companyDetails.id,
            employees.map( ({ employee_id, with_card }) => ({ employee_id, with_card }) ),
            shippingDetails
        );

        this.setState({
            invite_payees: {
                show: true,
                message,
                onConfirm,
                confirmText: 'Confirm',
                buttonStyle: 'primary'
            }
        });
    }

    /**
     * Updates table data state on click toggle
     *
     * @param {Number} id - Employee ID
     * @param {Boolean} value
     */
    handleSalpayCardToggle( id, value ) {
        const employees = Array.from( this.state.displayed_data );
        const employeeIndex = employees.findIndex( ( employee ) => employee.id === id );

        if ( employeeIndex > -1 ) {
            employees.splice(
                employeeIndex,
                1,
                {
                    ...employees[ employeeIndex ],
                    with_card: value
                }
            );

            this.setState({ displayed_data: employees });
        }
    }

    handleTableChanges = ( tableProps = this.employeeListTable && this.employeeListTable.tableComponent.state ) => {
        if ( tableProps ) {
            Object.assign(
                tableProps,
                {
                    dataLength: this.props.pagination.total
                }
            );

            this.setState({
                label: formatPaginationLabel( tableProps ),
                delete_label: formatDeleteLabel()
            });
        }
    }

    handleSearch = ( employees = this.props.employees ) => {
        const dataToDisplay = employees;
        let searchQuery = '';

        if ( this.search ) {
            searchQuery = toLower( get( this.search, 'searchInput.state.value', '' ) );
        }
        if ( searchQuery || ( this.search && searchQuery === '' ) ) {
            const currentFilters = this.state.filters.filter( ( filter ) => filter.type !== 'keyword' );
            const filtersToApply = [
                ...currentFilters,
                {
                    disabled: false,
                    type: 'keyword',
                    value: searchQuery
                }
            ];
            if ( this.searchTypingTimeout !== 0 ) {
                clearTimeout( this.searchTypingTimeout );
            }

            this.searchTypingTimeout = setTimeout( () => {
                if ( searchQuery !== '' || this.state.previous_query !== '' ) {
                    if ( this.state.previous_query !== searchQuery ) {
                        this.props.getPaginatedEmployees( this.props.companyDetails.id, { page: 1, perPage: this.props.pagination.per_page, showLoading: true, filters: filtersToApply, orderBy: this.state.orderBy, orderDir: this.state.orderDir });
                    }
                }
                this.setState({
                    displayed_data: this.props.employees,
                    previous_query: searchQuery,
                    filters: filtersToApply
                }, () => {
                    this.handleTableChanges();
                });
            }, 500 );
        } else {
            this.setState({ displayed_data: dataToDisplay }, () => {
                this.employeeListTable && this.handleTableChanges();
            });
        }
    }

    toggleFilter = () => this.setState( ( prevState ) => ({ show_filter: !prevState.show_filter }) );

    applyFilters = ( filters ) => {
        const searchQuery = toLower( get( this.search, 'searchInput.state.value', '' ) );
        const currentFilters = filters.filter( ( filter ) => filter.type !== 'keyword' && filter.type !== 'company' );
        const filtersToApply = [
            ...currentFilters,
            {
                type: 'keyword',
                value: searchQuery
            }
        ];

        // Perform filter request on current pagination settings
        if ( this.props.companyDetails ) {
            this.props.getPaginatedEmployees( this.props.companyDetails.id, { page: this.props.pagination.current_page, perPage: this.props.pagination.per_page, showLoading: true, filters: filtersToApply, orderBy: this.state.orderBy, orderDir: this.state.orderDir });
        }

        this.setState({
            displayed_data: this.props.employees,
            has_filters_applied: filters.length !== 0,
            filters: filtersToApply
        }, () =>
            this.handleTableChanges()
        );
    }

    resetFilters = () => {
        this.setState({
            show_filter: true,
            displayed_data: this.props.employees,
            has_filters_applied: false
        });
    }

    isAllowedToInvite = () => {
        const isSalpayAdmin = salpayAdminService.isSalpayAdmin( this.props.companyDetails.id );

        const isAllowedToInvite = salpayAdminService.isAllowedToInvite( this.props.companyDetails.id );

        return ( isAllowedToInvite || isSalpayAdmin );
    }

    /**
     * Component Render Method
     */
    render() {
        const { accountViewPermission } = this.state;
        const isAllowedToInvite = this.isAllowedToInvite();

        return (
            <main>
                <Helmet
                    title="SALPay Integration"
                    meta={ [
                        { name: 'SALPay Integration - Payees', content: 'SALPay Integration - Payees' }
                    ] }
                />
                <SubHeader
                    items={ SALPAY_INTEGRATION_SUBHEADER_ITEMS }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.state.invite_payees.onConfirm }
                    onClose={ () => this.onConfirmInviteClose() }
                    body={
                        <div style={ { display: 'flex', padding: '0 20px' } }>
                            <div style={ { alignSelf: 'center' } } >
                                { this.state.invite_payees.message }
                            </div>
                        </div>
                    }
                    title="Invite Payees?"
                    buttonStyle={ this.state.invite_payees.buttonStyle }
                    visible={ this.state.invite_payees.show }
                    confirmText={ this.state.invite_payees.confirmText }
                    showCancel={ this.state.salpayPermission.edit }
                />

                <SalConfirm
                    onConfirm={ this.state.unlink_payees.onConfirm }
                    onClose={ () => this.onConfirmUnlinkClose() }
                    body={
                        <div style={ { display: 'flex', padding: '0 20px' } }>
                            <div style={ { alignSelf: 'center' } } >
                                {this.state.unlink_payees.message}
                            </div>
                        </div>
                    }
                    title={ this.state.unlink_payees.title }
                    buttonStyle={ this.state.unlink_payees.buttonStyle }
                    visible={ this.state.unlink_payees.show }
                    confirmText={ this.state.unlink_payees.confirmText }
                    showCancel={ this.state.salpayPermission.delete }
                />

                <Modal
                    ref={ ( ref ) => { this.shippingDetailsModal = ref; } }
                    title="Shipping Information"
                    body={
                        <ShippingInformationForm
                            companyDetails={ this.props.companyDetails }
                        />
                    }
                    buttons={ [
                        {
                            label: 'Invite',
                            type: 'primary',
                            onClick: () => this.props.validateSalpayShippingDetails()
                        },
                        {
                            label: 'Cancel',
                            type: 'grey',
                            onClick: () => this.shippingDetailsModal.close()
                        }
                    ] }
                />
                <PageWrapper>
                    <Sidebar
                        items={ getControlPanelSidebarLinks({
                            salpayViewPermission: true,
                            isSubscribedToPayroll: true,
                            accountViewPermission
                        }) }
                    />
                    { this.props.loading && (
                        <div className="loader">
                            <LoadingStyles>
                                <H2>Loading SALPay Integration Payee List.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                    ) }

                    { !this.props.loading && (
                        <section className="content">
                            <header className="heading">
                                <H3>SALPay Integration Company Payees</H3>
                                <p>You can view the list of payees, view their linking status and invite them to link their SALPay Wallet account</p>
                            </header>

                            <CompanyEntryWrapper>
                                <H4>Integration Details</H4>
                                <div>
                                    <div className="details">
                                        <div>
                                            <H5>SALPay Account</H5>
                                            <div>{ this.props.companyDetails.salpay_account_number || '-' }</div>
                                        </div>
                                        <div>
                                            <H5>SALPay Company</H5>
                                            <div>{ this.props.companyDetails.salpay_account_name || '-' }</div>
                                        </div>
                                        <div>
                                            <H5>Salarium Company</H5>
                                            <div>{ this.props.companyDetails.name || '-' }</div>
                                        </div>
                                        <div>
                                            <H5>Status</H5>
                                            <div className={ this.props.companyDetails.linked ? 'linked' : 'unlinked' }>{ this.props.companyDetails.linked ? 'Linked' : 'Unlinked' }</div>
                                        </div>
                                    </div>
                                </div>
                            </CompanyEntryWrapper>

                            <TableTitleWrapper>
                                <H5>Employee List</H5>
                                <div className="search-wrapper">
                                    <Search
                                        ref={ ( ref ) => { this.search = ref; } }
                                        handleSearch={ () => this.handleSearch() }
                                    />
                                </div>
                                <span>
                                    <div>
                                        { isAnyRowSelected( this.disbursementsTable ) ? this.state.delete_label : this.state.label }
                                        &nbsp;
                                        <Button
                                            label={ <span><Icon name="filter" className="filter-icon" /> Filter</span> }
                                            type={ this.state.has_filters_applied ? 'primary' : 'neutral' }
                                            onClick={ this.toggleFilter }
                                        />
                                    </div>
                                </span>
                                <span className="sl-u-gap-left--sm">
                                    { isAllowedToInvite && isAnyRowSelected( this.employeeListTable ) && (
                                        <SalDropdown dropdownItems={ this.state.dropdown_items } /> )
                                    }
                                </span>
                            </TableTitleWrapper>
                            <div style={ { display: this.state.show_filter ? 'block' : 'none' } }>
                                <Filter
                                    filterData={ this.props.filterData }
                                    onCancel={ this.toggleFilter }
                                    onApply={ ( values ) => { this.applyFilters( values ); } }
                                />
                            </div>
                            <Table
                                data={ this.state.displayed_data }
                                columns={ this.getTableColumns( isAllowedToInvite ) }
                                pagination
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.employeeListTable = ref; } }
                                selectable
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    this.setState({ delete_label: formatDeleteLabel( selectionLength ) });
                                } }
                                loading={ this.props.employeeListLoading }
                                onPageChange={ ( data ) => this.props.getPaginatedEmployees( this.props.companyDetails.id, { page: data + 1, perPage: this.props.pagination.per_page, showLoading: true, filters: this.state.filters, orderBy: this.state.orderBy, orderDir: this.state.orderDir }) }
                                onPageSizeChange={ ( data ) => this.props.getPaginatedEmployees( this.props.companyDetails.id, { page: 1, perPage: data, showLoading: true, filters: this.state.filters, orderBy: this.state.orderBy, orderDir: this.state.orderDir }) }
                                page={ this.props.pagination.current_page - 1 }
                                pageSize={ this.props.pagination.per_page }
                                pages={ this.props.pagination.last_page }
                                onSortingChange={ this.onSortingChange }
                                manual

                            />
                        </section>
                    ) }
                </PageWrapper>
            </main>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    employeeListLoading: makeSelectEmployeeListLoading(),
    notification: makeSelectNotification(),
    companyDetails: makeSelectCompanyDetails(),
    employees: makeSelectEmployees(),
    salpayShippingDetailsErrors: makeSelectSalpayShippingDetailsErrors(),
    validatedSalpayShippingDetails: makeValidatedSelectSalpayShippingDetails(),
    filterData: makeSelectFilterData(),
    pagination: makeSelectPagination(),
    userInformation: makeSelectUserInformationState()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( SalpayPayees );
