import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import isEqual from 'lodash/isEqual';
import Loader from 'components/Loader';
import { renderField } from 'utils/form';

import {
    stripNonNumeric,
    formatMobileNumber
} from 'utils/functions';

import {
    FieldWrapper,
    LoaderWrapper
} from './styles';

import {
    getCountries,
    getSalpayShippingDetails,
    setValidatedSalpayShippingDetails
} from '../actions';

import {
    makeSelectCountries,
    makeSelectSalpayShippingDetails,
    makeSelectSalpayShippingDetailsLoading,
    makeSelectSalpayShippingDetailsErrors,
    makeSelectValidateSalpayShippingDetails
} from '../selectors';

import {
    SHIPPING_INFORMATION_OPTION_FIELDS,
    SHIPPING_INFORMATION_FORM_FIELDS
} from './constants';

/**
 * SALPay ShippingInformationForm Component
 */
export class ShippingInformationForm extends React.PureComponent {
    static propTypes = {
        getCountries: React.PropTypes.func,
        getSalpayShippingDetails: React.PropTypes.func,
        companyDetails: React.PropTypes.object,
        countries: React.PropTypes.array,
        salpayShippingDetails: React.PropTypes.array,
        salpayShippingDetailsErrors: React.PropTypes.object,
        salpayShippingDetailsLoading: React.PropTypes.bool,
        setValidatedSalpayShippingDetails: React.PropTypes.func,
        validateSalpayShippingDetails: React.PropTypes.any
    }

    constructor( props ) {
        super( props );

        this.state = {
            shipping_information: {
                shipping_location: 'manual_entry',
                saved_shipping_detail: null,
                contact_person: null,
                email: null,
                phone: null,
                address_line1: null,
                address_line2: null,
                city: null,
                state: null,
                zip_code: null,
                country: null,
                delivery_note: null
            },

            showSavedShippingDetails: false
        };

        this.fields = {};
    }

    componentWillMount() {
        this.setState({
            shipping_information: {
                shipping_location: 'manual_entry',
                saved_shipping_detail: null,
                contact_person: null,
                email: null,
                phone: null,
                address_line1: null,
                address_line2: null,
                city: null,
                state: null,
                zip_code: null,
                country: null,
                delivery_note: null
            },

            showSavedShippingDetails: false
        });
    }

    componentDidMount() {
        if ( this.props.countries.length === 0 ) {
            this.props.getCountries();
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( this.props.salpayShippingDetails, nextProps.salpayShippingDetails ) ) {
            if ( nextProps.salpayShippingDetails.length === 1 ) {
                this.setShippingInformation( nextProps.salpayShippingDetails[ 0 ]);
            } else if ( this.state.shipping_information.shipping_location === 'salpay_shipping_details' ) {
                this.setState( ( prevState ) => ({
                    ...prevState,
                    showSavedShippingDetails: true
                }) );
            }
        }

        if ( !isEqual( this.props.validateSalpayShippingDetails, nextProps.validateSalpayShippingDetails ) ) {
            this.validate();
        }

        if ( !isEqual( this.props.salpayShippingDetailsErrors, nextProps.salpayShippingDetailsErrors ) ) {
            this.onApiValidationErrors( nextProps.salpayShippingDetailsErrors );
        }
    }

    onShippingLocationChange( shippingLocation ) {
        if ( shippingLocation === 'salpay_shipping_details' ) {
            this.props.getSalpayShippingDetails( this.props.companyDetails.id );
        }

        if ( this.props.salpayShippingDetails.length === 1 ) {
            this.setShippingInformation( this.props.salpayShippingDetails[ 0 ]);
        } else {
            const index = this.state.shipping_information.saved_shipping_detail;

            this.setShippingInformation( this.props.salpayShippingDetails[ index ]);
        }

        this.props.setValidatedSalpayShippingDetails({
            isValid: false,
            data: null
        });
    }

    onSavedShippingDetailsChange( index ) {
        this.setShippingInformation( this.props.salpayShippingDetails[ index ]);

        this.props.setValidatedSalpayShippingDetails({
            isValid: false,
            data: null
        });
    }

    onApiValidationErrors( errors ) {
        Object.keys( errors ).forEach( ( fieldName ) => {
            const key = fieldName.replace( 'shipping_details.', '' );
            const messages = errors[ fieldName ];

            if ( !this.fields[ key ]) {
                return;
            }

            this.fields[ key ].setState({
                error: true,
                errorMessage: messages.join( ' ' )
            });
        });
    }

    setShippingInformation( shippingInformation ) {
        this.setState( ( prevState ) => ({
            shipping_information: {
                ...prevState.shipping_information,
                ...shippingInformation
            }
        }) );
    }

    getFieldData( field ) {
        const labelMaxLength = 40;

        switch ( field.id ) {
            case 'country':
                if ( !this.props.countries ) {
                    return [];
                }

                return this.props.countries.map( ({ name }) => ({
                    label: name,
                    value: name
                }) );

            case 'shipping_location':
                return [
                    {
                        label: 'Manual Entry',
                        value: 'manual_entry'
                    },
                    {
                        label: 'SALPay Shipping Details',
                        value: 'salpay_shipping_details'
                    }
                ];

            case 'saved_shipping_detail':
                return this.props.salpayShippingDetails.map( ( entry, index ) => {
                    let label = `${entry.contact_person} - ${entry.address_line1}`;

                    if ( label.length > labelMaxLength ) {
                        label = `${label.substring( 0, labelMaxLength - 3 )}...`;
                    }

                    return {
                        label,
                        value: index
                    };
                });

            default:
                return [];
        }
    }

    getOnChangeHandler( field ) {
        const formatter = ( value ) => {
            // Define formatter function based on the field id
            switch ( field.id ) {
                case 'zip_code':
                    return stripNonNumeric( value );

                case 'phone':
                    return formatMobileNumber( value );

                case 'country':
                case 'shipping_location':
                case 'saved_shipping_detail':
                    return value.value;

                default:
                    return value;
            }
        };

        return ( value ) => {
            const formatted = formatter( value );

            this.fields[ field.id ].setState({ value: formatted }, () => {
                this.setState( ( prevState ) => ({
                    shipping_information: {
                        ...prevState.shipping_information,
                        [ field.id ]: formatter( value )
                    }
                }) );

                if ( field.id === 'shipping_location' ) {
                    this.onShippingLocationChange( value.value );
                }

                if ( field.id === 'saved_shipping_detail' ) {
                    this.onSavedShippingDetailsChange( value.value );
                }
            });
        };
    }

    getDisabled( field ) {
        if ( this.props.salpayShippingDetailsLoading ) {
            return true;
        }

        const editableInSavedShippingOption = [
            'shipping_location',
            'saved_shipping_detail'
        ];

        if ( this.state.shipping_information.shipping_location === 'salpay_shipping_details'
            && !editableInSavedShippingOption.includes( field.id ) ) {
            return true;
        }

        return false;
    }

    validate() {
        let isValid = true;

        SHIPPING_INFORMATION_FORM_FIELDS.forEach( ( fieldDefinition ) => {
            const field = this.fields[ fieldDefinition.id ];

            switch ( fieldDefinition.field_params.field_type ) {
                case 'select':
                    if ( !field._checkRequire(
                        typeof field.state.value === 'object' && field.state.value !== null
                            ? field.state.value.value
                            : field.state.value
                        ) ) {
                        isValid = false;
                    }

                    break;

                default:
                    if ( field._validate( field.state.value ) ) {
                        isValid = false;
                    }
            }
        });

        this.props.setValidatedSalpayShippingDetails({
            isValid,
            data: this.state.shipping_information
        });
    }

    renderFields( fields ) {
        return fields.map( ( field, index ) => {
            const shouldShowSavedShippingDetails = field.id === 'saved_shipping_detail'
                && this.state.shipping_information.shipping_location === 'salpay_shipping_details'
                && this.state.showSavedShippingDetails
                && !this.props.salpayShippingDetailsLoading;

            return ( field.id !== 'saved_shipping_detail' || shouldShowSavedShippingDetails ) && (
                <FieldWrapper key={ index }>
                    { renderField({
                        id: field.id,
                        name: field.name,
                        label: field.label,
                        value: this.state.shipping_information[ field.id ] || '',
                        data: this.getFieldData( field ),
                        onChange: this.getOnChangeHandler( field ),
                        disabled: this.getDisabled( field ),
                        ref: ( ref ) => { this.fields[ field.id ] = ref; },
                        ...field.field_params
                    }) }
                </FieldWrapper>
            );
        });
    }

    render() {
        return (
            <div>
                { this.renderFields( SHIPPING_INFORMATION_OPTION_FIELDS ) }

                { this.props.salpayShippingDetailsLoading
                    ? (
                        <LoaderWrapper>
                            <Loader />
                        </LoaderWrapper>
                    )
                    : this.renderFields( SHIPPING_INFORMATION_FORM_FIELDS )
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    countries: makeSelectCountries(),
    salpayShippingDetails: makeSelectSalpayShippingDetails(),
    salpayShippingDetailsLoading: makeSelectSalpayShippingDetailsLoading(),
    salpayShippingDetailsErrors: makeSelectSalpayShippingDetailsErrors(),
    validateSalpayShippingDetails: makeSelectValidateSalpayShippingDetails()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators({
        getCountries,
        getSalpayShippingDetails,
        setValidatedSalpayShippingDetails
    }, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( ShippingInformationForm );
