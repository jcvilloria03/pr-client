export const SHIPPING_INFORMATION_OPTION_FIELDS = [
    {
        id: 'shipping_location',
        label: 'Shipping Location Options',
        field_params: {
            field_type: 'select',
            validations: {
                required: true
            }
        }
    },
    {
        id: 'saved_shipping_detail',
        label: 'Saved SALPay Shipping Details',
        field_params: {
            field_type: 'select',
            validations: {
                required: true
            }
        }
    }
];

export const SHIPPING_INFORMATION_FORM_FIELDS = [
    {
        id: 'contact_person',
        label: 'Contact Person',
        field_params: {
            validations: {
                required: true
            }
        }
    },
    {
        id: 'email',
        label: 'Contact Email',
        field_params: {
            validations: {
                required: true
            }
        }
    },
    {
        id: 'phone',
        label: 'Contact Phone Number',
        field_params: {
            validations: {
                required: true
            }
        }
    },
    {
        id: 'address_line1',
        label: 'Contact Address Line 1',
        field_params: {
            validations: {
                required: true
            }
        }
    },
    {
        id: 'address_line2',
        label: 'Contact Address Line 2',
        field_params: {
            validations: {
                required: false
            }
        }
    },
    {
        id: 'city',
        label: 'Contact Address City',
        field_params: {
            validations: {
                required: true
            }
        }
    },
    {
        id: 'state',
        label: 'Contact Address State',
        field_params: {
            validations: {
                required: true
            }
        }
    },
    {
        id: 'zip_code',
        label: 'Contact Address ZIP Code',
        field_params: {
            validations: {
                required: true
            }
        }
    },
    {
        id: 'country',
        label: 'Contact Address Country',
        field_params: {
            field_type: 'select',
            validations: {
                required: true
            }
        }
    },
    {
        id: 'delivery_note',
        label: 'Delivery Notes',
        field_params: {
            type: 'textarea',
            validations: {
                required: false
            }
        }
    }
];
