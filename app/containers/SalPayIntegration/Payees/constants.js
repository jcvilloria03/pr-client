/*
 *
 * SALPay Payees
 *
 */
const namespace = 'app/SalPayIntegration/Payees';

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const SET_EMPLOYEES = `${namespace}/SET_EMPLOYEES`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const NOTIFY = `${namespace}/NOTIFY`;
export const SET_COMPANY_DETAILS = `${namespace}/SET_COMPANY_DETAILS`;
export const SET_EMPLOYEE_LIST_LOADING = `${namespace}/SET_EMPLOYEE_LIST_LOADING`;
export const INVITE_PAYEES = `${namespace}/INVITE_PAYEES`;
export const REINVITE_PAYEES = `${namespace}/REINVITE_PAYEES`;
export const UNLINK_PAYEES = `${namespace}/UNLINK_PAYEES`;
export const SET_SHIPPING_INFORMATION = `${namespace}/SET_SHIPPING_INFORMATION`;
export const GET_SALPAY_SHIPPING_DETAILS = `${namespace}/GET_SALPAY_SHIPPING_DETAILS`;
export const SET_SALPAY_SHIPPING_DETAILS = `${namespace}/SET_SALPAY_SHIPPING_DETAILS`;
export const SET_SALPAY_SHIPPING_DETAILS_LOADING = `${namespace}/SET_SALPAY_SHIPPING_DETAILS_LOADING`;
export const SET_SALPAY_SHIPPING_DETAILS_ERRORS = `${namespace}/SET_SALPAY_SHIPPING_DETAILS_ERRORS`;
export const VALIDATE_SALPAY_SHIPPING_DETAILS = `${namespace}/VALIDATE_SALPAY_SHIPPING_DETAILS`;
export const SET_VALIDATED_SALPAY_SHIPPING_DETAILS = `${namespace}/SET_VALIDATED_SALPAY_SHIPPING_DETAILS`;
export const SET_COUNTRIES = `${namespace}/SET_COUNTRIES`;
export const GET_COUNTRIES = `${namespace}/GET_COUNTRIES`;
export const SET_PAGINATION = `${namespace}/SET_PAGINATION`;
export const GET_PAGINATED_EMPLOYEES = `${namespace}/GET_PAGINATED_EMPLOYEES`;
export const GET_EMPLOYEES = `${namespace}/GET_EMPLOYEES`;
export const SET_FILTER_DATA = `${namespace}/SET_FILTER_DATA`;
