
import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    INITIALIZE_DATA,
    SET_NOTIFICATION,
    SET_EMPLOYEES,
    SET_COMPANY_DETAILS,
    INVITE_PAYEES,
    REINVITE_PAYEES,
    UNLINK_PAYEES,
    SET_EMPLOYEE_LIST_LOADING,
    SET_SHIPPING_INFORMATION,
    GET_SALPAY_SHIPPING_DETAILS,
    SET_SALPAY_SHIPPING_DETAILS,
    SET_SALPAY_SHIPPING_DETAILS_LOADING,
    SET_SALPAY_SHIPPING_DETAILS_ERRORS,
    VALIDATE_SALPAY_SHIPPING_DETAILS,
    SET_VALIDATED_SALPAY_SHIPPING_DETAILS,
    SET_COUNTRIES,
    GET_COUNTRIES,
    NOTIFY,
    GET_PAGINATED_EMPLOYEES,
    GET_EMPLOYEES
} from './constants';

/**
 * Set loading status of the page
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Initialize data
 * @param {Integer} payload - Company ID
 * @returns {Object} action
 */
export function initializeData() {
    return {
        type: INITIALIZE_DATA
    };
}

/**
 * Sets company details
 * @param {Object} payload
 * @returns {Object} action
 */
export function setCompanyDetails( payload ) {
    return {
        type: SET_COMPANY_DETAILS,
        payload
    };
}

/**
 * Sets employees
 * @param {Array} payload - Employee List
 * @returns {Object} action
 */
export function setEmployees( payload ) {
    return {
        type: SET_EMPLOYEES,
        payload
    };
}

/**
 * Fetch list of employees
 */
export function getEmployees( id, filters ) {
    return {
        type: GET_EMPLOYEES,
        id,
        filters
    };
}

/**
 * Fetch list of employees
 */
export function getPaginatedEmployees( id, payload ) {
    return {
        type: GET_PAGINATED_EMPLOYEES,
        id,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Dispatches a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 * @returns {Object} action
 */
export function notify( payload ) {
    return {
        type: NOTIFY,
        payload
    };
}

/**
 * Sets loading status of Employee List table
 * @param {Boolean} payload - Loading Status
 * @returns {Object} action
 */
export function setEmployeeListLoading( payload ) {
    return {
        type: SET_EMPLOYEE_LIST_LOADING,
        payload
    };
}

/**
 * Sets shipping information
 * @param {Array} payload - Shipping information data
 * @returns {Object} action
 */
export function setShippingInformation( payload ) {
    return {
        type: SET_SHIPPING_INFORMATION,
        payload
    };
}

/**
 * Sets list of SALPay shipping details
 * @param {Array} payload - List of SALPay shipping details
 * @returns {Object} action
 */
export function setSalpayShippingDetails( payload ) {
    return {
        type: SET_SALPAY_SHIPPING_DETAILS,
        payload
    };
}

/**
 * Gets list of SALPay shipping details
 * @returns {Object} action
 */
export function getSalpayShippingDetails( companyId ) {
    return {
        type: GET_SALPAY_SHIPPING_DETAILS,
        companyId
    };
}

/**
 * Sets the loading status of SALPay shipping details
 * @returns {Object} action
 */
export function setSalpayShippingDetailsLoading( payload ) {
    return {
        type: SET_SALPAY_SHIPPING_DETAILS_LOADING,
        payload
    };
}

/**
 * Sets the validation errors of SALPay shipping details
 * @returns {Object} action
 */
export function setSalpayShippingDetailsErrors( payload ) {
    return {
        type: SET_SALPAY_SHIPPING_DETAILS_ERRORS,
        payload
    };
}

/**
 * Trigger salpay shipping details validation
 * @returns {Object} action
 */
export function validateSalpayShippingDetails() {
    return {
        type: VALIDATE_SALPAY_SHIPPING_DETAILS,
        payload: Math.random()
    };
}

/**
 * Sets the salpay shipping details validation status
 * @returns {Object} action
 */
export function setValidatedSalpayShippingDetails( payload ) {
    return {
        type: SET_VALIDATED_SALPAY_SHIPPING_DETAILS,
        payload
    };
}

/**
 * Sets list of countries
 * @param {Array} payload - List of countries
 * @returns {Object} action
 */
export function setCountries( payload ) {
    return {
        type: SET_COUNTRIES,
        payload
    };
}

/**
 * Gets list of countries
 * @returns {Object} action
 */
export function getCountries() {
    return {
        type: GET_COUNTRIES
    };
}

/**
 * Sends request to invite selected users as payee
 * @param {Integer} companyId - Company ID
 * @param {Integer[]} employees - List of Employee IDs
 * @returns {Object} action
 */
export function invitePayees( companyId, employees, shippingDetails ) {
    return {
        type: INVITE_PAYEES,
        payload: {
            company_id: companyId,
            employees,
            shippingDetails
        }
    };
}

/**
 * Sends request to reinvite selected employees as payee
 * @param {Integer} employeeId - Employee ID
 * @param {Boolean} withCard - with_card
 * @returns {Object} action
 */
export function reInvitePayees( employeeId, withCard ) {
    return {
        type: REINVITE_PAYEES,
        payload: {
            employee_id: employeeId,
            with_card: withCard
        }
    };
}

/**
 * Sends request to unlink selected employees as payee
 * @param {Integer} employeeId - Employee ID
 * @returns {Object} action
 */
export function unlinkPayees( employeeId, linkStatus ) {
    return {
        type: UNLINK_PAYEES,
        payload: { employee_id: employeeId, link_status: linkStatus }
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
