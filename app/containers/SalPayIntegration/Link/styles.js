import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    margin-left: 91px;
    display: flex;
    flex-direction: column;
    align-items: center;

    .content {
        margin: 40px 0;
        padding: 0 20px;
        width: 100%;
        max-width: 1140px;
        display: flex;
        flex-direction: column;
        align-items: center;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .row-wrapper {
            width: 100%;
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;

            .link-icon {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                margin: 0 30px;

                .fa {
                    margin: 5px 0;
                }
            }
        }

        .section-footer {
            width: 100%;
            display: flex;
            justify-content: flex-end;
        }
    }
`;

export const CompanyEntryWrapper = styled.div`
    border-radius: 5px;
    letter-spacing: 0.12px;
    border: solid 1px #adadad;
    background-color: #fff;
    width: 45%;
    padding: 20px 30px;
    margin-bottom: 20px;
    min-height: 200px;

    & > div {
        display: flex;
        flex-direction: row;
        align-items: flex-start;
        width: 100%;
        padding-left: 20px;

        .details {
            display: flex;
            flex-grow: 1;
            flex-direction: row;
            justify-content: space-between;

            & > div {
                &:first-child {
                    margin-right: 20px;
                }
            }

            .pad-bottom {
                h5 {
                    margin-bottom: 30px;
                }
            }
        }
    }
`;

export const NavWrapper = styled.div`
    margin-top: 70px;
    padding: 10px 20px;
    background: #f0f4f6;
`;
