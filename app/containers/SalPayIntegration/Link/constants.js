/*
 *
 * SALPay Company Link constants
 *
 */
const namespace = 'app/SalPayIntegration/Link';

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_LINKING = `${namespace}/SET_LINKING`;
export const REQUEST_LINK = `${namespace}/REQUEST_LINK`;
