import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    REQUEST_LINK
} from './constants';
import {
    setNotification,
    setLinking
} from './actions';

/**
 * Sends request to link accounts
 * @param {Integer} payload.company_id - Company ID
 * @param {Object} payload.salpay_company - Selected SALPay company to link
 * @param {Integer} payload.link_id - Link ID from the OTP verification
 */
export function* requestLink({ payload }) {
    yield put( setLinking( true ) );

    try {
        yield call( Fetch, '/salpay/link/company-integrations', {
            method: 'POST',
            data: {
                link_id: payload.link_id,
                account_number: payload.salpay_company.account_number,
                account_name: payload.salpay_company.name
            }
        });

        yield call( browserHistory.push, `/control-panel/salpay-integration/companies/${payload.company_id}/details`, true );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLinking( false ) );
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( browserHistory.replace, '/control-panel/salpay-integration/companies', true );
}

/**
 * Watcher for REQUEST_LINK
 */
export function* watchForRequestResendOtp() {
    const watcher = yield takeEvery( REQUEST_LINK, requestLink );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForRequestResendOtp,
    watchForReinitializePage
];
