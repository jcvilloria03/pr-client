import React from 'react';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { isAuthorized } from 'utils/Authorization';
import { browserHistory } from 'utils/BrowserHistory';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { H2, H3, H4, H5 } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Select from 'components/Select';
import Button from 'components/Button';
import A from 'components/A';

import * as actions from './actions';

import {
    PageWrapper,
    LoadingStyles,
    CompanyEntryWrapper,
    NavWrapper
} from './styles';
import {
    makeSelectLoading,
    makeSelectLinkingStatus,
    makeSelectNotification
} from './selectors';

/**
 * SALPay Company Link Component
 */
export class CompanyLink extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        loading: React.PropTypes.bool,
        location: React.PropTypes.shape({
            state: React.PropTypes.shape({
                account_details: React.PropTypes.object,
                company: React.PropTypes.object,
                salpay_companies: React.PropTypes.array,
                link_id: React.PropTypes.number
            })
        }),
        params: React.PropTypes.object,
        linking: React.PropTypes.bool,
        requestLink: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        this.state = {
            accountViewPermission: false,
            salpayPermission: {
                view: false,
                create: false,
                delete: false,
                edit: false
            },
            selected_salpay_company: {}
        };
    }

    componentWillMount() {
        this.props.products
            && !subscriptionService.isSubscribedToPayroll( this.props.products )
            && browserHistory.replace( '/unauthorized' );

        isAuthorized([
            'view.account',
            'create.salpay_integration',
            'view.salpay_integration',
            'edit.salpay_integration',
            'delete.salpay_integration'
        ], ( authorization ) => {
            const authorized = authorization[ 'create.salpay_integration' ];

            if ( authorized ) {
                this.setState({
                    accountViewPermission: authorization[ 'view.account' ],
                    salpayPermission: {
                        view: authorization[ 'view.salpay_integration' ],
                        create: authorization[ 'create.salpay_integration' ],
                        delete: authorization[ 'delete.salpay_integration' ],
                        edit: authorization[ 'edit.salpay_integration' ]
                    }
                });
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    /**
     * Component Render Method
     */
    render() {
        const { accountViewPermission } = this.state;

        return (
            <main>
                <Helmet
                    title="SALPay Integration"
                    meta={ [
                        { name: 'SALPay Integration - Link Company', content: 'SALPay Integration - Link Company' }
                    ] }
                />
                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        salpayViewPermission: true,
                        isSubscribedToPayroll: true,
                        accountViewPermission
                    }) }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => { e.preventDefault(); browserHistory.push( '/control-panel/salpay-integration/summary', true ); } }
                        >
                            &#8592; Back to SALPay Integration
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    { this.props.loading && (
                        <div className="loader">
                            <LoadingStyles>
                                <H2>Loading SALPay Integration.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                    ) }

                    { !this.props.loading && (
                        <section className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <header className="heading">
                                <H3>SALPay Integration</H3>
                                <p>Link your Salarium payroll account with your SALPay Business Account to enable payroll disbursement via SALPay</p>
                            </header>

                            <div className="row-wrapper">
                                <CompanyEntryWrapper>
                                    <H4>Salarium Details</H4>
                                    <div>
                                        <div className="details">
                                            <div className="pad-bottom">
                                                <H5>Salarium Account</H5>
                                                <span>{ this.props.location.state.account_details.name }</span>
                                            </div>
                                            <div className="pad-bottom">
                                                <H5>Salarium Company</H5>
                                                <span>{ this.props.location.state.company.name }</span>
                                            </div>
                                        </div>
                                    </div>
                                </CompanyEntryWrapper>
                                <div className="link-icon">
                                    <i className="fa fa-arrow-right fa-3x" />
                                    <i className="fa fa-arrow-left fa-3x" />
                                </div>
                                <CompanyEntryWrapper>
                                    <H4>SALPay Details</H4>
                                    <div>
                                        <div className="details">
                                            <div className="pad-bottom">
                                                <H5>SALPay Account</H5>
                                                <span>{ this.state.selected_salpay_company.account_number || '-' }</span>
                                            </div>
                                            <div>
                                                <H5>Select SALPay Company</H5>
                                                <Select
                                                    id="salpay-company-select"
                                                    placeholder="Select company"
                                                    data={ this.props.location.state.salpay_companies }
                                                    value={ this.state.selected_salpay_company.account_number }
                                                    ref={ ( ref ) => { this.salpayCompany = ref; } }
                                                    onChange={ ( value ) => this.setState({ selected_salpay_company: value || {}}) }
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </CompanyEntryWrapper>
                            </div>
                            <div className="section-footer">
                                <Button
                                    id="link-account-button"
                                    label={ this.props.linking ? 'Linking...' : 'Link Accounts' }
                                    type="action"
                                    size="large"
                                    disabled={ this.props.linking || this.state.selected_salpay_company.account_number === undefined }
                                    onClick={ () => this.props.requestLink({
                                        company_id: this.props.params.id,
                                        salpay_company: this.state.selected_salpay_company,
                                        link_id: this.props.location.state.link_id
                                    }) }
                                />
                            </div>
                        </section>
                    ) }
                </PageWrapper>
            </main>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    linking: makeSelectLinkingStatus(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( CompanyLink );

