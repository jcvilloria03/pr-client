import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'salpayCompanyLink' );

/**
 * Default selector used by SALPay Link
 */
const makeSelectLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLinkingStatus = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'linking' )
);

export {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectLinkingStatus
};
