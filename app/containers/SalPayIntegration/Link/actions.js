
import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_NOTIFICATION,
    REQUEST_LINK,
    SET_LINKING
} from './constants';

/**
 * Set loading status of the page
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Send request to link SALPay company to Salarium Company
 * @param {Integer} payload.company_id - Company ID
 * @param {Object} payload.salpay_company - Selected SALPay company to link
 * @param {Integer} payload.link_id - Link ID from the OTP verification
 * @returns {Object} action
 */
export function requestLink( payload ) {
    return {
        type: REQUEST_LINK,
        payload
    };
}

/**
 * Sets button loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setLinking( payload ) {
    return {
        type: SET_LINKING,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
