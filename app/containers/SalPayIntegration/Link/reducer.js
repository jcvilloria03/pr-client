import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_NOTIFICATION,
    SET_LINKING
} from './constants';

const initialState = fromJS({
    loading: false,
    salpay_companies: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    linking: false
});

/**
 *
 * SALPay Link Company reducer
 *
 */
function salpayLinkReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_LINKING:
            return state.set( 'linking', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default salpayLinkReducer;
