import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'salpayIntegration' );

/**
 * Default selector used by SALPay Integration
 */

const makeSelectAccountDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'account_details' ).toJS()
);

const makeSelectAccountCompanies = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'account_companies' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectModalButtonLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'modal_button_loading' )
);

const makeSelectOtpDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'otp_details' ).toJS()
);

export {
    makeSelectAccountDetails,
    makeSelectAccountCompanies,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectModalButtonLoading,
    makeSelectOtpDetails
};
