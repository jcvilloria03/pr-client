import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_ACCOUNT_DETAILS,
    SET_ACCOUNT_COMPANIES,
    SET_NOTIFICATION,
    SET_MODAL_BUTTON_LOADING,
    SET_OTP_DETAILS,
    SET_OTP_ERROR,
    SET_OTP_REMAINING_DURATION,
    SET_OTP_TRIES_LEFT,
    SET_OTP_MOBILE_NUMBER
} from './constants';

const initialState = fromJS({
    loading: false,
    account_details: {},
    account_companies: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    modal_button_loading: false,
    otp_details: {
        mobile_number: '',
        error: '',
        link_id: null,
        tries_left: null,
        remaining: null
    }
});

/**
 *
 * SALPay Integration reducer
 *
 */
function salpayIntegrationReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_ACCOUNT_DETAILS:
            return state.set( 'account_details', fromJS( action.payload ) );
        case SET_ACCOUNT_COMPANIES:
            return state.set( 'account_companies', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_MODAL_BUTTON_LOADING:
            return state.set( 'modal_button_loading', action.payload );
        case SET_OTP_DETAILS:
            return state.set( 'otp_details', fromJS( action.payload ) );
        case SET_OTP_ERROR:
            return state.setIn([ 'otp_details', 'error' ], action.payload );
        case SET_OTP_TRIES_LEFT:
            return state.setIn([ 'otp_details', 'tries_left' ], action.payload );
        case SET_OTP_REMAINING_DURATION:
            return state.setIn([ 'otp_details', 'remaining' ], action.payload );
        case SET_OTP_MOBILE_NUMBER:
            return state.setIn([ 'otp_details', 'mobile_number' ], action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default salpayIntegrationReducer;
