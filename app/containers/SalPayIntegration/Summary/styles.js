import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    margin-left: 91px;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 76px;

    .content {
        margin: 40px 0;
        padding: 0 20px;
        width: 100%;
        max-width: 1140px;
        display: flex;
        flex-direction: column;
        align-items: center;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
                max-width: 800px;
            }

            .no-linked {
                margin-top: 40px;
                display: flex;
                flex-direction: column;
                align-items: center;

                .icon-wrapper {
                    width: 9rem;
                    height: 9rem;
                    background-color: #00a4e4;
                    color: #fff;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    border-radius: 50%;
                    margin-bottom: 10px;
                }
            }
        }

        ul {
            width: 100%;
            margin: 0;
            padding: 0;

            li {
                list-style: none;
            }
        }
    }
`;

export const CompanyEntryWrapper = styled.div`
    border-radius: 5px;
    letter-spacing: 0.12px;
    border: solid 1px #adadad;
    background-color: #fff;
    padding: 20px;
    margin-bottom: 20px;

    & > div {
        display: flex;
        flex-direction: row;
        align-items: flex-start;
        width: 100%;
        padding-left: 20px;

        .details {
            display: flex;
            flex-grow: 1;
            flex-direction: row;
            align-items: center;
            justify-content: space-between;
            margin-right: 20px;
        }

        .action {
            min-width: 195px;
            text-align: center;
        }
    }

    .linked {
        color: #83d24b;
    }

    .unlinked {
        color: #F21108;
    }
`;

export const ModalBody = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;

    .mobile-number {
        min-height: 100px;
        width: 200px;
    }
`;

export const SpinnerWrapper = styled.span`
    padding: 0 20px;
`;

export const StyledForm = styled.form`
    width: 500px;

    @media (max-width: 480px) {
        width: 100%;
        min-width: 0;
    }

    .sl-c-card__header {
        margin: 0 1rem;
        padding: 1rem 0;
        border-bottom: 1px solid #ddd;
    }

    .sl-c-card__body {
        padding: 1rem 3rem;
    }

    .sl-c-card__footer {
        padding: 0 0 1rem 3rem;
        text-align: left;
        font-size: 14px;
    }

    .sl-c-form-group {
        &:not(:last-child) {
            margin-bottom: 2rem;
        }
    }

    #submit {
        text-align: center;

        button {
            padding: 12px 32px;
            font-size: 18px;
            border-radius: 40px;
            font-weight: 400;
            display: block;
            margin: 0 auto 2rem;
        }

        p {
            font-size: 12px;
            color: #999;
            margin: 0;
        }
    }
`;
