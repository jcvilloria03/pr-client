import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { isAuthorized } from 'utils/Authorization';
import {
    SALPAY_INTEGRATION_SUBHEADER_ITEMS
} from 'utils/constants';
import { browserHistory } from 'utils/BrowserHistory';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { salpayAdminService } from 'utils/SalpayAdminService';
import { H2, H3, H4, H5 } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Modal from 'components/Modal';
import Input from 'components/Input';
import SnackBar from 'components/SnackBar';

import SubHeader from 'containers/SubHeader';
import { makeSelectUserInformationState } from 'containers/App/selectors';
import { auth } from 'utils/AuthService';

import {
    makeSelectAccountCompanies,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectOtpDetails,
    makeSelectModalButtonLoading
} from './selectors';
import * as actions from './actions';
import Otp from '../Otp';

import {
    PageWrapper,
    LoadingStyles,
    CompanyEntryWrapper,
    ModalBody,
    SpinnerWrapper,
    StyledForm
} from './styles';

/**
 * SALPay Integration Component
 */
export class SALPayIntegration extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        accountCompanies: React.PropTypes.array,
        getOtpDetails: React.PropTypes.func,
        setOtpDetails: React.PropTypes.func,
        modalButtonLoading: React.PropTypes.bool,
        otpDetails: React.PropTypes.object,
        requestResendOtp: React.PropTypes.func,
        sendOtpCode: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        this.state = {
            disableButton: true,
            accountViewPermission: false,
            salpayPermission: {
                view: false,
                create: false,
                delete: false,
                edit: false
            },
            mobile_number: '',
            email: '',
            otp_code: '',
            company: null,
            render_otp: false
        };
        this._checkForm = this._checkForm.bind( this );
    }

    componentWillMount() {
        isAuthorized([
            'view.account',
            'create.salpay_integration',
            'view.salpay_integration',
            'edit.salpay_integration',
            'delete.salpay_integration'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.salpay_integration' ];

            if ( authorized ) {
                this.setState({
                    accountViewPermission: authorization[ 'view.account' ],
                    salpayPermission: {
                        view: authorization[ 'view.salpay_integration' ],
                        create: authorization[ 'create.salpay_integration' ],
                        delete: authorization[ 'delete.salpay_integration' ],
                        edit: authorization[ 'edit.salpay_integration' ]
                    }
                });
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.initializeData();
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.otpDetails.mobile_number !== nextProps.otpDetails.mobile_number ) {
            this.setState({ render_otp: nextProps.otpDetails.mobile_number !== '' });
        }
    }

    getModalButtonsConfig() {
        let label;
        let disabled = false;
        let onClick;

        if ( this.props.modalButtonLoading ) {
            label = <SpinnerWrapper><i className="fa fa-circle-o-notch fa-spin fa-fw" /></SpinnerWrapper>;
            disabled = true;
            onClick = () => { };
        } else if ( this.props.otpDetails.tries_left === 0 ) {
            label = 'Close';
            onClick = () => this.salpayIntegrationModal.toggle();
        } else if ( this.state.render_otp ) {
            label = 'Confirm';
            disabled = this.state.otp_code.length !== 4;
            onClick = () => this.state.salpayPermission.create && this.props.sendOtpCode({
                mobile: this.state.mobile_number,
                code: this.state.otp_code,
                company: this.state.company
            });
        } else {
            label = 'Submit';
            disabled = this.state.disableButton;
            onClick = () => {
                const mobileError = this.mobileNumberRef._validate( this.state.mobile_number );
                if ( this.state.salpayPermission.create && !mobileError ) {
                    this.props.getOtpDetails({
                        mobile: this.state.mobile_number,
                        email: this.state.email,
                        company: this.state.company
                    });
                }
            };
        }

        return [{
            id: 'submit',
            type: 'action',
            label,
            size: 'large',
            disabled,
            onClick
        }];
    }

    handleOtpInputChange = ( code ) => {
        this.setState({ otp_code: code });
    }

    isAccountOwner = () => salpayAdminService.isAccountOwner()

    requestResendOtp = () => {
        this.props.requestResendOtp({
            mobile: this.props.otpDetails.mobile_number,
            company: this.state.company,
            account_id: auth.getUser().account_id
        });
    }
    _checkMobile = ( value ) => {
        const match = value.match( new RegExp( /^\+63\d{10}$/ ) );
        let error = false;
        let errorMessage = '';
        if ( !match ) {
            error = true;
            errorMessage = 'Enter a valid mobile number';
        }

        this.mobileNumberRef.setState({ error, errorMessage });
    }

    _checkForm() {
        const emailError = !this.state.email || this.emailRef.state.error === true;
        const mobileError = this.mobileNumberRef.state.error === true;
        const disableButton = emailError === true || mobileError === true;

        this.setState({ disableButton });
    }

    renderPreIntegrationModalBody() {
        return (
            <div>
                <StyledForm>
                    <div className="sl-c-card__header">
                        Enter the registered mobile number
                        and email address of the company primary account owner.
                        <br />
                    </div>
                    <div className="sl-c-card__body">
                        <Input
                            id="input-email"
                            label="SALPay Corporate Account Owner Email"
                            className="sl-c-form-group"
                            placeholder="Type the email here"
                            type="email"
                            value={ this.state.email }
                            required
                            ref={ ( ref ) => { this.emailRef = ref; } }
                            onChange={ ( value ) => {
                                this.setState({ email: value });
                                this._checkForm();
                            } }
                            onBlur={ this._checkForm }
                        />

                        <Input
                            id="input-mobile-number"
                            label="SALPay Corporate Account Owner Mobile Number"
                            placeholder="+639XXXXXXXXX"
                            className="sl-c-form-group"
                            required
                            value={ this.state.mobile_number }
                            onChange={ ( value ) => {
                                const clean = value
                                    .replace( new RegExp( /[^+\d]/, 'gmi' ), '' )
                                    .substring( 0, 13 );

                                this.mobileNumberRef.setState({ value: clean }, () =>
                                    this.setState({ mobile_number: clean })
                                );
                                this._checkMobile( value );
                                this._checkForm();
                            } }
                            onBlur={ ( value ) => {
                                this._checkMobile( value );
                                this._checkForm();
                            } }
                            ref={ ( ref ) => { this.mobileNumberRef = ref; } }
                        />
                    </div>
                    <div className="sl-c-card__footer">
                        Note: Please make sure that the email address and mobile number matches the information of a SALPay Administrator. The OTP will be sent to the inputted mobile number.
                    </div>
                </StyledForm>
            </div>
        );
    }

    renderModalBody() {
        return (
            <ModalBody>
                { this.state.render_otp ? (
                    <Otp
                        mobile={ this.props.otpDetails.mobile_number }
                        error={ this.props.otpDetails.error }
                        triesLeft={ this.props.otpDetails.tries_left }
                        remainingDuration={ this.props.otpDetails.remaining }
                        blocked={ this.props.otpDetails.tries_left === 0 }
                        resendOtp={ this.requestResendOtp }
                        onChange={ this.handleOtpInputChange }
                        actionLocked={ this.props.otpDetails.link_id === null }
                        onRef={ ( ref ) => { this.otpRef = ref; } }
                    />
                ) : this.renderPreIntegrationModalBody()
                }
            </ModalBody>
        );
    }

    /**
     * Component Render Method
     */
    render() {
        const { accountViewPermission } = this.state;

        return (
            <main>
                <Helmet
                    title="SALPay Integration"
                    meta={ [
                        { name: 'SALPay Integration - Summary', content: 'SALPay Integration - Summary' }
                    ] }
                />
                <SubHeader
                    items={ SALPAY_INTEGRATION_SUBHEADER_ITEMS }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="SALPay Integration"
                    buttons={ this.getModalButtonsConfig() }
                    onClose={ () => {
                        this.setState({
                            mobile_number: '',
                            otp_code: '',
                            company: null,
                            render_otp: false
                        });
                        this.props.setOtpDetails({
                            mobile_number: '',
                            error: '',
                            link_id: null,
                            tries_left: null,
                            remaining: null
                        });
                    } }
                    body={ this.renderModalBody() }
                    ref={ ( ref ) => { this.salpayIntegrationModal = ref; } }
                />
                <PageWrapper>
                    <Sidebar
                        items={ getControlPanelSidebarLinks({
                            salpayViewPermission: true,
                            accountViewPermission,
                            isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products )
                        }) }
                    />
                    <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                        <LoadingStyles>
                            <H2>Loading SALPay Integration Summary.</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    </div>

                    <section className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                        <header className="heading">
                            <H3>SALPay Integration</H3>
                            <p>Link your Salarium payroll account with your SALPay Business Account to enable payroll disbursement via SALPay</p>
                        </header>

                        <ul>
                            { this.props.accountCompanies.map( ( company ) => (
                                <li key={ company.id }>
                                    <CompanyEntryWrapper>
                                        <H4>Integration Details</H4>
                                        <div>
                                            <div className="details">
                                                <div>
                                                    <H5>Salarium Company</H5>
                                                    <div>{ company.name }</div>
                                                </div>
                                                <div>
                                                    <H5>SALPay Account</H5>
                                                    <div>{ company.salpay_account_number || '-' }</div>
                                                </div>
                                                <div>
                                                    <H5>SALPay Company</H5>
                                                    <div>{ company.salpay_account_name || '-' }</div>
                                                </div>
                                                <div>
                                                    <H5>Status</H5>
                                                    <div className={ company.linked ? 'linked' : 'unlinked' }>{ company.linked ? 'Linked' : 'Unlinked' }</div>
                                                </div>
                                            </div>
                                            <div className="action">
                                                { ( company.linked || this.state.salpayPermission.create ) && (
                                                    <Button
                                                        id={ `${company.linked ? 'view-details-' : 'link-salpay-button-'}${company.id}` }
                                                        label={ company.linked ? 'View Details' : 'Link Account to SALPay' }
                                                        type={ company.linked ? 'primary' : 'action' }
                                                        alt={ company.linked }
                                                        onClick={ () => this.setState({
                                                            mobile_number: '',
                                                            company: {
                                                                id: company.id,
                                                                name: company.name
                                                            }
                                                        }, () => {
                                                            if ( company.linked ) {
                                                                browserHistory.push( `/control-panel/salpay-integration/companies/${company.id}/details`, true );
                                                            } else {
                                                                this.salpayIntegrationModal.toggle();
                                                            }
                                                        }) }
                                                        disabled={ !this.isAccountOwner() }
                                                        ref={ ( ref ) => { this.editButton = ref; } }
                                                    />
                                                ) }
                                            </div>
                                        </div>
                                    </CompanyEntryWrapper>
                                </li>
                            ) ) }
                        </ul>
                    </section>
                </PageWrapper>
            </main>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    accountCompanies: makeSelectAccountCompanies(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    modalButtonLoading: makeSelectModalButtonLoading(),
    otpDetails: makeSelectOtpDetails(),
    userInformation: makeSelectUserInformationState()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( SALPayIntegration );

