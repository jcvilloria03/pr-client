
import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    INITIALIZE_DATA,
    GET_ACCOUNT_DETAILS,
    SET_ACCOUNT_DETAILS,
    GET_ACCOUNT_COMPANIES,
    SET_ACCOUNT_COMPANIES,
    SET_NOTIFICATION,
    GET_OTP_DETAILS,
    SET_OTP_DETAILS,
    SET_MODAL_BUTTON_LOADING,
    REQUEST_RESEND_OTP,
    SEND_OTP_CODE,
    SET_OTP_ERROR,
    SET_OTP_REMAINING_DURATION,
    SET_OTP_TRIES_LEFT,
    SET_OTP_MOBILE_NUMBER
} from './constants';

/**
 * Set loading status of the page
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Initialize page data
 * @returns {Object} action
 */
export function initializeData() {
    return {
        type: INITIALIZE_DATA
    };
}

/**
 * Request to fetch account details
 * @returns {Object} action
 */
export function getAccountDetails() {
    return {
        type: GET_ACCOUNT_DETAILS
    };
}

/**
 * Sets account details
 * @param {Object} payload - Account details
 * @returns {Object} action
 */
export function setAccountDetails( payload ) {
    return {
        type: SET_ACCOUNT_DETAILS,
        payload
    };
}

/**
 * Request to fetch companies of account
 * @returns {Object} action
 */
export function getAccountCompanies() {
    return {
        type: GET_ACCOUNT_COMPANIES
    };
}

/**
 * Sets account companies from API
 * @param {Object} payload - Account companies
 * @returns {Object} action
 */
export function setAccountCompanies( payload ) {
    return {
        type: SET_ACCOUNT_COMPANIES,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Sets modal button loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setModalButtonLoading( payload ) {
    return {
        type: SET_MODAL_BUTTON_LOADING,
        payload
    };
}

/**
 * Gets OTP details
 * @param {String} payload.mobile - Mobile number
 * @param {Object} payload.company - Company to link
 * @returns {Object} action
 */
export function getOtpDetails( payload ) {
    return {
        type: GET_OTP_DETAILS,
        payload
    };
}

/**
 * Sends request to resend OTP
 * @param {String} payload.mobile - Mobile number
 * @param {Object} payload.company - Company to link
 * @param {Integer} payload.account_id - Account ID of Company
 * @returns {Object} action
 */
export function requestResendOtp( payload ) {
    return {
        type: REQUEST_RESEND_OTP,
        payload
    };
}

/**
 * Sets OTP details
 * @param {Object} payload - OTP Details
 * @returns {Object} action
 */
export function setOtpDetails( payload ) {
    return {
        type: SET_OTP_DETAILS,
        payload
    };
}

/**
 * Sets OTP error message
 * @param {String} payload - Error message
 * @returns {Object} action
 */
export function setOtpError( payload ) {
    return {
        type: SET_OTP_ERROR,
        payload
    };
}

/**
 * Sets OTP tries left
 * @param {Integer} payload - Tries left
 * @returns {Object} action
 */
export function setOtpTriesLeft( payload ) {
    return {
        type: SET_OTP_TRIES_LEFT,
        payload
    };
}

/**
 * Sets OTP max tries remaining duration
 * @param {Integer} payload - Remaining duration
 * @returns {Object} action
 */
export function setOtpRemainingDuration( payload ) {
    return {
        type: SET_OTP_REMAINING_DURATION,
        payload
    };
}

/**
 * Sets mobile number used for the OTP
 * @param {String} payload - Mobile number
 * @returns {Object} action
 */
export function setOtpMobileNumber( payload ) {
    return {
        type: SET_OTP_MOBILE_NUMBER,
        payload
    };
}

/**
 * Sends OTP Code
 * @param {String} payload.code - OTP Code
 * @param {String} payload.mobile - Mobile Number
 * @param {Object} payload.company - Company details
 * @returns {Object} action
 */
export function sendOtpCode( payload ) {
    return {
        type: SEND_OTP_CODE,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
