import { take, call, put, cancel, select } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
import get from 'lodash/get';
import has from 'lodash/has';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';
import { makeSelectUserInformationState } from 'containers/App/selectors';
import { auth } from 'utils/AuthService';

import {
    INITIALIZE_DATA,
    GET_OTP_DETAILS,
    REQUEST_RESEND_OTP,
    SEND_OTP_CODE
} from './constants';
import {
    setLoading,
    setAccountCompanies,
    setNotification,
    setModalButtonLoading,
    setOtpDetails,
    setOtpRemainingDuration,
    setOtpError,
    setOtpTriesLeft,
    setOtpMobileNumber
} from './actions';
import {
    makeSelectOtpDetails,
    makeSelectAccountDetails
} from './selectors';

/**
 * Initialize page data
 */
export function* initializeData() {
    yield put( setLoading( true ) );

    try {
        const userId = auth.getUser().id;

        const accountCompanies = yield call( Fetch, `/salpay/users/${userId}/companies` );

        const companyIds = accountCompanies.data.map( ( company ) => company.id );
        const paramCompanyIds = companyIds.join();
        const salpaySettings = yield call( Fetch, `/salpay/companies/salpay-settings/${userId}?company_ids=${paramCompanyIds}` );

        let user = JSON.parse( localStorage.getItem( 'user' ) );
        user = Object.assign({}, user, { salpay_settings: salpaySettings.data });
        localStorage.setItem( 'user', JSON.stringify( user ) );

        yield put( setAccountCompanies( accountCompanies.data ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Get OTP details
 * @param {String} payload.mobile - Mobile number
 * @param {Object} payload.company - Company to link
 * @param {Integer} payload.account_id - Account ID of Company
 */
export function* getOtpDetails({ payload }) {
    yield put( setModalButtonLoading( true ) );
    const {
        mobile,
        email,
        company
    } = payload;

    yield put( setOtpDetails({
        mobile_number: '',
        error: '',
        link_id: null,
        tries_left: null,
        remaining: null
    }) );

    try {
        const userDetails = yield select( makeSelectUserInformationState() );
        const response = yield call( Fetch, '/salpay/link/company-authorizations', {
            method: 'POST',
            data: {
                owner_mobile: mobile,
                owner_email: email,
                company_id: company.id,
                user_id: userDetails.id
            }
        });

        yield put( setOtpDetails({
            link_id: response.link_id,
            mobile_number: mobile,
            error: '',
            tries_left: 3
        }) );
    } catch ( error ) {
        if ( has( error, 'response.data.message' ) ) {
            let errorData;
            try {
                errorData = JSON.parse( error.response.data.message );
            } catch ( _error ) {
                errorData = null;
            }

            if ( errorData ) {
                yield [
                    put( setOtpError( get( errorData, 'error.message', 'Error' ) ) ),
                    put( setOtpRemainingDuration( get( errorData, 'errors.remaining_duration', 0 ) ) )
                ];

                const errorCode = get( errorData, 'error.code' );
                if ( errorCode === 401 ) {
                    yield call( notifyUser, {
                        show: true,
                        title: 'Error',
                        message: 'You have entered a mobile number which is not authorized in SALPay for Business. Please contact your company administrator for assistance',
                        type: 'error'
                    });
                } else if ( errorCode === 423 ) {
                    yield [
                        put( setOtpMobileNumber( mobile ) ),
                        put( setOtpTriesLeft( 0 ) )
                    ];
                }
            } else {
                yield call( notifyError, error );
            }
        } else {
            yield call( notifyError, error );
        }
    } finally {
        yield put( setModalButtonLoading( false ) );
    }
}

/**
 * Sends request to resend OTP
 * @param {String} payload.mobile - Mobile number
 * @param {Object} payload.company - Company to link
 * @param {Integer} payload.account_id - Account ID of Company
 */
export function* requestResendOtp({ payload }) {
    yield put( setModalButtonLoading( true ) );

    try {
        const { mobile } = payload;

        /**
         * TODO: Start mock
         */
        const response = {
            mobile_number: mobile,
            errorMessage: '',
            tries_left: 3,
            expired: true
        };

        // yield call( delay, 2000 );
        /**
         * TODO: End mock
         */

        yield put( setOtpDetails( response ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setModalButtonLoading( false ) );
    }
}

/**
 * Sends OTP code for verification
 * @param {String} payload.code - Mobile number
 * @param {Object} payload.company - Company to link
 */
export function* sendOtpCode({ payload }) {
    yield put( setModalButtonLoading( true ) );

    try {
        const { code, company } = payload;
        const accountDetails = yield select( makeSelectAccountDetails() );
        const { link_id: linkId } = yield select( makeSelectOtpDetails() );

        const response = yield call( Fetch, '/salpay/link/company-otp-verifications', {
            method: 'POST',
            data: {
                link_id: linkId,
                otp: code
            }
        });

        yield call( browserHistory.pushWithParam, `/control-panel/salpay-integration/companies/${company.id}/link`, true, {
            account_details: accountDetails,
            company,
            salpay_companies: response.map( ( salpayCompany ) => ({
                ...salpayCompany,
                label: salpayCompany.name,
                value: salpayCompany.account_number
            }) ),
            link_id: linkId
        });
    } catch ( error ) {
        if ( has( error, 'response.data.message' ) ) {
            let errorData;
            try {
                errorData = JSON.parse( error.response.data.message );
            } catch ( _error ) {
                errorData = null;
            }

            if ( errorData ) {
                yield [
                    put( setOtpError( get( errorData, 'error.message', 'Error' ) ) ),
                    put( setOtpTriesLeft( get( errorData, 'errors.otp.tries_left', 0 ) ) ),
                    put( setOtpRemainingDuration( get( errorData, 'errors.otp.remaining_duration', 0 ) ) )
                ];
            } else {
                yield call( notifyError, error );
            }
        } else {
            yield call( notifyError, error );
        }
    } finally {
        yield put( setModalButtonLoading( false ) );
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watcher for INITIALIZE_DATA
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for INITIALIZE_DATA
 */
export function* watchForGetOtpDetails() {
    const watcher = yield takeEvery( GET_OTP_DETAILS, getOtpDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REQUEST_RESEND_OTP
 */
export function* watchForRequestResendOtp() {
    const watcher = yield takeEvery( REQUEST_RESEND_OTP, requestResendOtp );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SEND_OTP_CODE
 */
export function* watchForSendOtpCode() {
    const watcher = yield takeEvery( SEND_OTP_CODE, sendOtpCode );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForGetOtpDetails,
    watchForRequestResendOtp,
    watchForSendOtpCode,
    watchForReinitializePage
];
