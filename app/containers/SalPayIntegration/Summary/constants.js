/*
 *
 * SALPay Integration Companies constants
 *
 */
const namespace = 'app/SalPayIntegration/Companies';

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const GET_ACCOUNT_DETAILS = `${namespace}/GET_ACCOUNT_DETAILS`;
export const SET_ACCOUNT_DETAILS = `${namespace}/SET_ACCOUNT_DETAILS`;
export const GET_ACCOUNT_COMPANIES = `${namespace}/GET_ACCOUNT_COMPANIES`;
export const SET_ACCOUNT_COMPANIES = `${namespace}/SET_ACCOUNT_COMPANIES`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_MODAL_BUTTON_LOADING = `${namespace}/SET_MODAL_BUTTON_LOADING`;

export const GET_OTP_DETAILS = `${namespace}/GET_OTP_DETAILS`;
export const SET_OTP_DETAILS = `${namespace}/SET_OTP_DETAILS`;
export const SET_OTP_ERROR = `${namespace}/SET_OTP_ERROR`;
export const SET_OTP_TRIES_LEFT = `${namespace}/SET_OTP_TRIES_LEFT`;
export const SET_OTP_REMAINING_DURATION = `${namespace}/SET_OTP_REMAINING_DURATION`;
export const SET_OTP_MOBILE_NUMBER = `${namespace}/SET_OTP_MOBILE_NUMBER`;

export const SEND_OTP_CODE = `${namespace}/SEND_OTP_CODE`;
export const REQUEST_RESEND_OTP = `${namespace}/REQUEST_RESEND_OTP`;
