import { createSelector } from 'reselect';

/**
 * Direct selector to the nightShift state domain
 */
const selectNightShiftDomain = () => ( state ) => state.get( 'nightShift' );

/**
 * Other specific selectors
 */

const makeSelectNightShift = () => createSelector(
  selectNightShiftDomain(),
( substate ) => substate && substate.get( 'nightShift' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectNightShiftDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectNightShiftDomain(),
  ( substate ) => substate && substate.get( 'notification' ).toJS()
);

const makeSelectBtnLoad = () => createSelector(
  selectNightShiftDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

export {
  makeSelectNightShift,
  makeSelectLoading,
  makeSelectNotification,
  makeSelectBtnLoad
};
