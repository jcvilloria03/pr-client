/* eslint-disable import/first */
/* eslint-disable import/no-unresolved */
/* eslint-disable import/extensions */
/* eslint-disable require-jsdoc */
import { take, call, put, cancel } from 'redux-saga/effects';
import {
    SET_LOADING,
    GET_NIGHT_SHIFT,
    GET_UPDATESHIFT,
    SET_NIGHT_SHIFT,
    SET_UPDATESHIFT,
    SET_NOTIFICATION,
    GET_NOTIFICATION,
    BTN_LOADING
} from './constants';
import { Fetch } from 'utils/request';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import { company } from 'utils/CompanyService';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* getNightShift() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/night_shift`, { method: 'GET' });
        yield put({
            type: SET_NIGHT_SHIFT,
            payload: response
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* updateNightShift({ payload }) {
    try {
        yield put({
            type: BTN_LOADING,
            payload: true
        });
        yield call( Fetch, `/night_shift/${payload.id}`, { method: 'PUT', data: payload });
        yield put({
            type: SET_UPDATESHIFT,
            payload: false
        });
        yield call( getNightShift );
        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Record Updated Successfully',
            type: 'success'
        });
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}

export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 2000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

export function* watchForGetNightShift() {
    const watcher = yield takeEvery( GET_NIGHT_SHIFT, getNightShift );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForUpdateNightShift() {
    const watcher = yield takeEvery( GET_UPDATESHIFT, updateNightShift );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchNotify() {
    const watcher = yield takeEvery( GET_NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetNightShift,
    watchForUpdateNightShift,
    watchNotify
];
