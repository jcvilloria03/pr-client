import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import Flatpickr from 'react-flatpickr';
import moment from 'moment';
import { cloneDeep } from 'lodash';

import Sidebar from 'components/Sidebar';
import { H2, H3, H1, P } from 'components/Typography';
import RadioGroup from 'components/RadioGroup';
import Radio from 'components/Radio';
import Icon from 'components/Icon';
import Toggle from 'components/Toggle';
import Modal from 'components/Modal';
import SnackBar from 'components/SnackBar';
import Button from 'components/Button';
import Loader from 'components/Loader';
import { Spinner } from 'components/Spinner';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { company } from 'utils/CompanyService';

import {
    PageWrapper,
    LoadingStyles
} from './styles';
import {
    makeSelectNightShift,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectBtnLoad
} from './selectors';
import * as nightShiftAction from './actions';
import {
    TIME_SHIFT_OPTIONS
} from './constants';

/**
 *
 * NightShift
 *
 */
export class NightShift extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        loading: React.PropTypes.bool,
        nightShift: React.PropTypes.object,
        getNightShift: React.PropTypes.func,
        updateNightShift: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        btnLoading: React.PropTypes.bool
    }

    constructor( props ) {
        super( props );

        this.state = {
            type: '',
            start_time: '',
            end_time: '',
            time_shift: '',
            company_id: '',
            id: ''
        };
        this.setState = this.setState.bind( this );
    }

    componentDidMount() {
        this.props.getNightShift();
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.nightShift ) {
            const value = cloneDeep( nextProps.nightShift );
            this.setState({
                type: value.activated.toString(),
                start_time: value.start,
                end_time: value.end,
                time_shift: value.time_shift,
                id: value.id,
                company_id: value.company_id
            });
        }
    }

    handleSubmit = () => {
        const companyId = company.getLastActiveCompanyId();
        const data = {
            activated: this.state.type.toString() === 'true',
            company_id: companyId,
            end: this.state.end_time,
            id: this.state.id,
            start: this.state.start_time,
            time_shift: this.state.time_shift
        };
        this.props.updateNightShift( data );
    }

    handleDiscard = () => {
        const value = cloneDeep( this.props.nightShift );
        this.setState({
            type: value.activated.toString(),
            start_time: value.start,
            end_time: value.end,
            time_shift: value.time_shift,
            id: value.id,
            company_id: value.company_id
        });
        this.toggle.defaultSelected = value.time_shift;
        this.DiscardModal.close();
    }
    /**
     *
     * NightShift render method
     *
     */
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const { type } = this.state;
        const { notification, loading, btnLoading } = this.props;

        return (
            <div>
                { btnLoading && <Spinner /> }
                <Helmet
                    title="Night Shift"
                    meta={ [
                        { name: 'description', content: 'Description of Night Shift' }
                    ] }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <SnackBar
                    message={ notification && notification.message }
                    title={ notification && notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification && notification.show }
                    delay={ 5000 }
                    type={ notification && notification.type }
                />
                <Modal
                    title="Discard Changes"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.DiscardModal = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'Do nothing',
                            onClick: () => this.DiscardModal.toggle()
                        },
                        {
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: this.handleDiscard
                        }
                    ] }
                />
                <PageWrapper>
                    <div className="loader" style={ { display: loading ? '' : 'none' } }>
                        <LoadingStyles>
                            <H2>Loading Night Shift</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    </div>
                    <div style={ { display: loading ? 'none' : '' } }>
                        <Container>
                            <div className="main_shift">
                                <div className="content">
                                    <div className="heading">
                                        <H1 noBottomMargin>Night Shift</H1>
                                    </div>
                                </div>
                                <div className="shift_night">
                                    <div className="schedule_type">
                                        <P noBottomMargin>* Night shift schedule</P>
                                        <div className="schedule_radio">
                                            <RadioGroup
                                                id="radioAmendmentReturn"
                                                name="type"
                                                value={ type }
                                                horizontal
                                                className="radio-group"
                                                onChange={ ( value ) => {
                                                    this.setState({ type: value });
                                                } }
                                            >
                                                <Radio value="true">Activated</Radio>
                                                <Radio value="false">Deactivated</Radio>
                                            </RadioGroup>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6 schedule_main">
                                            <P noBottomMargin>* Night shift start time</P>
                                            <div className="schedule_timeSect timeIcon">
                                                <Flatpickr
                                                    data-enable-time
                                                    name="start_time"
                                                    value={ this.state.start_time }
                                                    options={ {
                                                        noCalendar: true,
                                                        enableTime: true,
                                                        dateFormat: 'H:i',
                                                        time_24hr: true
                                                    } }
                                                    onChange={ ( value ) => {
                                                        this.setState({ start_time: moment( new Date( value ) ).format( 'HH:mm' ) });
                                                    } }
                                                    disabled={ type === 'false' }
                                                />
                                                <Icon name="time2" />
                                            </div>
                                        </div>
                                        <div className="col-md-6 schedule_main">
                                            <P noBottomMargin>* Night shift end time</P>
                                            <div className="schedule_timeSect timeIcon">
                                                <Flatpickr
                                                    data-enable-time
                                                    name="end_time"
                                                    value={ this.state.end_time }
                                                    options={ {
                                                        noCalendar: true,
                                                        enableTime: true,
                                                        dateFormat: 'H:i',
                                                        time_24hr: true
                                                    } }
                                                    onChange={ ( value ) => {
                                                        this.setState({ end_time: moment( new Date( value ) ).format( 'HH:mm' ) });
                                                    } }
                                                    disabled={ type === 'false' }
                                                />
                                                <Icon name="time2" />
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <P noBottomMargin>* Time shift</P>
                                        <div className={ type === 'false' ? 'toggle_deactive' : 'toggle' }>
                                            <Toggle
                                                options={ TIME_SHIFT_OPTIONS }
                                                defaultSelected={ this.state.time_shift }
                                                onChange={ ( value ) => {
                                                    this.setState({ time_shift: value });
                                                } }
                                                ref={ ( ref ) => { this.toggle = ref; } }
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Container>
                        <div className="from_footer">
                            <Container>
                                <div className="footer_button">
                                    <Button
                                        label={ <span>Cancel</span> }
                                        type="action"
                                        size="large"
                                        alt
                                        onClick={ () => this.DiscardModal.toggle() }
                                    />
                                    <Button
                                        label={ btnLoading ? <Loader /> : 'Update' }
                                        type="action"
                                        size="large"
                                        disabled={ btnLoading }
                                        onClick={ () => this.handleSubmit() }
                                    />
                                </div>
                            </Container>
                        </div>
                    </div>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    nightShift: makeSelectNightShift(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    btnLoading: makeSelectBtnLoad()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        nightShiftAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( NightShift );
