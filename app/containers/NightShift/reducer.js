import { fromJS } from 'immutable';
import {
    SET_LOADING,
    DEFAULT_ACTION,
    SET_NIGHT_SHIFT,
    SET_NOTIFICATION,
    BTN_LOADING
} from './constants';

const initialState = fromJS({
    loading: true,
    btnLoading: false,
    nightShift: {
        activated: true,
        company_id: '',
        end: '',
        id: '',
        start: '',
        time_shift: ''
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * NightShift reducer
 *
 */
function nightShiftReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_NIGHT_SHIFT:
            return state.set( 'nightShift', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default nightShiftReducer;
