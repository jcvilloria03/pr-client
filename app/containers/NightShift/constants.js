/*
 *
 * NightShift constants
 *
 */

export const DEFAULT_ACTION = 'app/NightShift/DEFAULT_ACTION';
export const SET_LOADING = 'app/NightShift/SET_LOADING';
export const SET_NIGHT_SHIFT = 'app/NightShift/SET_NIGHT_SHIFT';
export const GET_NIGHT_SHIFT = 'app/NightShift/GET_NIGHT_SHIFT';
export const SET_UPDATESHIFT = 'app/NightShift/SET_UPDATESHIFT';
export const GET_UPDATESHIFT = 'app/NightShift/GET_UPDATESHIFT';
export const SET_NOTIFICATION = 'app/NightShift/SET_NOTIFICATION';
export const GET_NOTIFICATION = 'app/NightShift/GET_NOTIFICATION';
export const BTN_LOADING = 'app/NightShift/BTN_LOADING';

export const TIME_SHIFT_OPTIONS = [
    {
        title: 'Carry over',
        subtext: 'The night shift hours computation will be based on the night shift start time holiday type.',
        value: 'Carry over'
    },
    {
        title: 'Counter per hour',
        subtext: 'The night shift hour/s computation is inclusive only to the holiday type per hour of each day.',
        value: 'Counter per hour'
    }
];
