/* eslint-disable require-jsdoc */
import {
    DEFAULT_ACTION,
    GET_NIGHT_SHIFT,
    GET_UPDATESHIFT
} from './constants';

/**
 *
 * NightShift actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function getNightShift() {
    return {
        type: GET_NIGHT_SHIFT
    };
}

export function updateNightShift( payload ) {
    return {
        type: GET_UPDATESHIFT,
        payload
    };
}
