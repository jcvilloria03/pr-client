import styled from 'styled-components';

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    justify-content: center;
    min-height: 100%;
`;

export const LoadingStyles = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 200px;
    height:100vh;
`;

export const PageWrapper = styled.div`
    .hvYuOH .ReactTable .rt-td {
        overflow: visible;
    }

    .day {
        background-color: #e5f6fc;
    }

    .start_Time {
        border:1px solid #474747;
        width:100%;
        border-radius: 0px !important;
    }

    .ReactTable {
        .rt-table {
            border:0;
        }

        .rt-thead{
            background-color: transparent;
            .rt-resizable-header{
                background-color: #00A5E5;
                &:first-child{
                    background-color: transparent;
                }
            }
        }
        .rt-tbody{
            .selected{
                background-color: rgba(131, 210, 75, 0.15);
                &:hover{
                    background-color: rgba(131, 210, 75, 0.15) !important;
                }
            }
        }
    }

    .main_shift{
        max-width:55vw;
        margin: auto;
        .content {
            margin-top: 110px;
            .heading {
                display: flex;
                align-items: center;
                flex-direction: column;
                margin: 0 auto 56px auto;

                h1 {
                    font-size: 36px;
                    font-weight: 700;
                    line-height: 57.6px;
                }
            }

            .main {
                .btn {
                    min-width: 140px;
                }
            }

            .tableAction button {
                width: 130px;
            }
            .foot {
                text-align: right;
                padding: 10px 10vw;
                margin-top: 20px;

                button {
                    min-width: 120px;
                }
            }
            .schedule_main{
                padding: 0px 7px 14px;
                .night_input{
                    border: 1px solid black;
                    padding: 5px 0;
                    input{
                        width: 100%;
                        height: 46px;
                        border: 1px solid #95989a;
                        padding: 0 0.75rem;
                        color: #000;
                        &:focus {
                            outline:none;
                        }
                    }
                }
                .timeIcon{
                    position: relative;
                    span{
                        position: absolute;
                        top: 10px;
                        right: 12px;
                    }
                }
                .coreTimeIcon{
                    position: relative;
                    span{
                        position: absolute;
                        top: 33px;
                        right: 12px;
                    }
                }
                .errorTimeIconcore{
                    position: relative;
                    span{
                        position: absolute;
                        top: 33px;
                        right: 12px;
                        color: red;
                    }
                    div{
                        input{
                            border:1px solid red !important;
                        }
                    }
                }
                .errorTimeIcon{
                    position: relative;
                    span{
                        position: absolute;
                        top: 10px;
                        right: 12px;
                        color: red;
                    }
                    div{
                        input{
                            border:1px solid red !important;
                        }
                    }
                }
                .start_Time{
                    background: #f8f8f8;
                    cursor: not-allowed;
                }
            }
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;

            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 600;
                font-size: 22px;
            }

            .search-wrapper {
                flex-grow: 1;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 30px;

                    input {
                        border: none;
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }
        }
    }
    .button{
        margin-left:20rem;
    }
    .buttons{
        background-color:#9fdc74;
        cursor: pointer;
        color:#FFFFFF
    }

    .buttons : hover{
        background-color:#9fdc73
    }
    .action{
        margin-left:66rem;
    }
    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }

    .no-license-error {
        color: #e13232;
    }

    .from_footer{
        margin: 14px 0px 0px;
        padding: 14px 0px;
        background: #F0F4F6;
        position: fixed;
        bottom: 0;
        width: 100%;
    }
    .from_footer .footer_button{
        width: 100%;
        text-align: end;
    }
    .from_footer .footer_button button{
        padding: 14px 28px;
        border-radius: 2rem;
        &:focus{
            outline:none;
        }
    }
    .from_footer .footer_button .btn_cancel{
        color: #474747;
        border: 1px solid #83D24B;
        background-color: #ffffff;
        margin-right: 5px;
    }

    .schedule_timeSect {
        margin-top: 4px;

        p {
            margin-bottom: 0;
            margin-top: 12px;
            font-size: 14px;
        }

        input {
            width: 100%;
            height: 46px;
            border: 1px solid #95989a;
            padding: 0 0.75rem;
            color: #000;
            &:focus {
                outline: none;
            }
            font-size: 14px;

            &:disabled {
                border-color: #c7c7c7;
                background: #f8f8f8;
                color: #999;

                & + .input-group-addon {
                    border-color: #c7c7c7;
                }

                &~label {
                    color: #9da0a1;
                }
            }
        }
    }

    .timeIcon{
        position: relative;
        span {
            position: absolute;
            top: 10px;
            right: 12px;
        }

        svg {
            width: 18px;
        }
    }
    .schedule_type{
        margin-bottom: 14px;
        label{
            margin: 0px;
        }
        .schedule_radio{
            margin-top: 4px;
            .radio-group{
                >div{
                    font-size: 14px;
                    padding: 0 !important;
                    >div{
                        display: flex;
                        align-items: center;
                    }
                }
            }
        }
    }
    .toggle .rockerSwitch{
        border-radius: 32px;
        border-color: #F6F6F6;
        color: #474747;
    }
    .toggle .dcdjqM .rockerSwitch > a:hover{
        backgroundColor: rgb(0,165,226);
        color: #fff !important;
        border: none;
    }
    .toggle .dcdjqM .rockerSwitch > a:focus{
        backgroundColor: rgb(0,165,226);
        color: #fff !important;
        border: none;
    }
    .first{
        backgroundColor: #red;
    }
    .toggle_deactive{
        cursor:default !important;
    }
    .toggle_deactive .rockerSwitch > a.selected {
        background: transparent !important;
    }
    .toggle_deactive >div .rockerSwitch > a{
        cursor:default !important;
    }
    .toggle_deactive >div .rockerSwitch > a:hover{
        backgroundColor: transparent !important;
        color:#adadad !important;
        border: none;
    }
    .toggle_deactive >div .rockerSwitch > a:focus{
        backgroundColor: transparent !important;
        color:#adadad !important;
        border: none;
    }
    .toggle_deactive >div .rockerSwitch > selected{
        backgroundColor: transparent !important;
    }
    .toggle_deactive >div .rockerSwitch > selected:hover{
        backgroundColor: transparent !important;
        color:#adadad !important;
    }
    .toggle_deactive >div .rockerSwitch > selected:focus{
        backgroundColor: transparent !important;
        color:#adadad !important;
    }
    .toggle_deactive >div .rockerSwitch .title{
        color:#adadad !important;
    }
    .toggle_deactive >div .rockerSwitch .sub{
        color:#adadad !important;
    }
    .toggle_deactive .rockerSwitch{
        border:1px solid #F6F6F6 !important;
        &:after{
            border:1px solid #f6f6f6 !important;
        }
    }
    .toggle_deactive{
        a{
            &:last-child{
                border-left:1px solid #F6F6F6 !important;
                &:hover{
                    border-left:1px solid #F6F6F6 !important;
                }
            }
        }
    }

    .toggle,
    .toggle_deactive {
        margin-top: 4px;

        .rockerSwitch > a {
            padding: 14px 20px;

            .title {
                margin-bottom: 4px;
                font-size: 14px;
            }
        }
    }

    .toggle_deactive {
        .rockerSwitch::after {
            color:#adadad !important;
        }

        .rockerSwitch > a:hover,
        .rockerSwitch > a:focus {
            background: #FFFFFF;
        }
    }
`;
