/* eslint-disable import/first */
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';
import {
    EDIT_DAYHOUR,
    GET_DAYHOUR,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_DAYHOUR,
    SET_EDIT_DAYHOUR,
    SET_LOADING
} from './constants';

import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';

import { REINITIALIZE_PAGE } from '../../App/constants';

/**
 * GET Day/Hour
 */
export function* getDayhour() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/day_hour_rates`, { method: 'GET' });

        yield put({
            type: SET_DAYHOUR,
            payload: res.data || []
        });

        yield put({
            type: SET_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize Page
 */
export function* reinitializePage() {
    yield call( getDayhour );
}

/**
 * GET Day/Hour
 */
export function* editDayhour({ payload }) {
    const { id, data, dayHourRates } = payload;
    try {
        yield put({
            type: SET_EDIT_DAYHOUR,
            payload: true
        });

        const res = yield call( Fetch, `/day_hour_rate/${id}`, { method: 'PATCH', data });

        yield put({
            type: SET_DAYHOUR,
            payload: dayHourRates.map( ( item ) => {
                if ( item.id === res.id ) {
                    return { ...item, rate: res.rate };
                }

                return item;
            })
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Changes successfully saved!',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EDIT_DAYHOUR,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Watcher for GET Day/hour
 */
export function* watchForGetDayhour() {
    const watcher = yield takeEvery( GET_DAYHOUR, getDayhour );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For ReinitializePage
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for EDIT Day/hour
 */
export function* watchForEditDayhour() {
    const watcher = yield takeEvery( EDIT_DAYHOUR, editDayhour );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Notification
 */
export function* watchFroNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetDayhour,
    watchForReinitializePage,
    watchFroNotification,
    watchForEditDayhour
];
