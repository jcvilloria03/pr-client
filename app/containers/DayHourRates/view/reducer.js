import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    NOTIFICATION_SAGA,
    SET_DAYHOUR,
    SET_EDIT_DAYHOUR,
    SET_LOADING
} from './constants';

const initialState = fromJS({
    getData: [],
    loading: false,
    isEditing: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    }
});

/**
 *
 * DayHourRates reducer
 *
 */
function dayHourRatesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_DAYHOUR:
            return state.set( 'getData', fromJS( action.payload ) );
        case SET_EDIT_DAYHOUR:
            return state.set( 'isEditing', action.payload );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default dayHourRatesReducer;
