import {
    DEFAULT_ACTION,
    EDIT_DAYHOUR,
    GET_DAYHOUR,
    NOTIFICATION
} from './constants';

/**
 *
 * DayHourRates actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Get Day/hour actions
 *
 */
export function getDayhour() {
    return {
        type: GET_DAYHOUR
    };
}

/**
 *
 * Edit Day/hour actions
 *
 */
export function editDayhour( payload ) {
    return {
        type: EDIT_DAYHOUR,
        payload
    };
}

/**
 *
 * Notification
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
