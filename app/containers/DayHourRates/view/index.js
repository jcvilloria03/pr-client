/* eslint-disable react/prop-types */
/* eslint-disable import/first */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import makeSelectDayHourRates, {
    makeSelectGetDayHourRates,
    makeSelectLoading,
    makeSelectIsEditing,
    makeSelectNotification,
    makeSelectProducts
} from './selectors';
import * as getDayhourActions from './actions';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';

import { H3, H5, H2 } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import Table from 'components/Table';
import Button from 'components/Button';
import Input from 'components/Input';

import {
    LoadingStyles,
    PageWrapper
} from './styles';

/**
 *
 * DayHourRates
 *
 */
export class DayHourRates extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    static proptypes = {
        getDayhourList: React.PropTypes.array,
        getDayhour: React.PropTypes.func,
        loading: React.PropTypes.bool,
        isEditing: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            showButtons: false,
            editedData: null
        };
    }

    componentDidMount() {
        this.props.getDayhour();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.getDayhourList !== this.props.getDayhourList && this.setState({
            label: formatPaginationLabel( this.dayhourTable.tableComponent.state )
        });
    }

    handleTableChanges = ( tableProps = this.dayhourTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    showtextBox( row ) {
        return this.state.editedData && this.state.editedData.id === row.id;
    }

    changeData() {
        this.setState({ showButtons: false });

        this.props.editDayhour({
            id: this.state.editedData.id,
            data: this.state.editedData,
            dayHourRates: this.props.DayHourRates.getData
        });
    }

    /**
     *
     * DayHourRates render method
     *
     */
    render() {
        const tableColumn = [
            {
                id: 'type',
                header: 'Day Type',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>{row.day_type}</div>
                )
            },
            {
                id: 'holiday',
                header: 'Holiday Type',
                sortable: false,
                minWidth: 300,
                render: ({ row }) => (
                    <div>{row.holiday_type}</div>
                )
            },
            {
                id: 'timeType',
                header: 'Time Type',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>{row.time_type}</div>
                )
            },
            {
                id: 'abbreviation',
                header: 'Abbreviation',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>{row.abbreviation}</div>
                )
            },
            {
                id: 'rate',
                header: 'Rate',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => ( this.showtextBox( row ) && this.state.showButtons
                    ? <div style={ { width: '100%' } }>
                        <Input
                            id="name" type="text"
                            value={ this.state.editedData.rate }
                            onChange={ ( event ) => this.setState({
                                editedData: { ...this.state.editedData, rate: event }
                            }) }
                        />
                    </div>
                    : <div>{row.rate}</div>
                )
            },
            {
                header: '',
                sortable: false,
                minWidth: 150,
                style: { justifyContent: 'right' },
                render: ({ row }) => ( this.showtextBox( row ) && this.state.showButtons
                    ? <div>
                        <Button
                            label={ <span>Cancel</span> }
                            type="neutral"
                            size="small"
                            onClick={ () => this.setState({ showButtons: false }) }
                        ></Button>
                        <Button
                            label={ <span>Save</span> }
                            type="action"
                            size="small"
                            onClick={ () => this.changeData() }
                        ></Button>
                    </div>
                    : <Button
                        label={ ( !!this.state.editedData && this.state.editedData.id === row.id && this.props.isEditing ) ? <Loader /> : 'Edit' }
                        type="grey"
                        size="small"
                        onClick={ () => this.setState({ showButtons: true, editedData: row }) }
                    ></Button>
                )
            }
        ];

        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });

        const { notification, loading, getDayhourList } = this.props;
        const { label } = this.state;

        return (
            <div>
                <Helmet
                    title="Day Hour Rates"
                    meta={ [
                        { name: 'description', content: 'Description of DayHourRates' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Sidebar items={ sidebarLinks } />
                <PageWrapper>
                    <div className="loader" style={ { display: loading ? '' : 'none' } }>
                        <LoadingStyles>
                            <H2>Loading Day/hour Rates.</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    </div>
                    <div className="content" style={ { display: loading ? 'none' : '' } }>
                        <Container>
                            <div className="content">
                                <div className="heading">
                                    <H3>Day/hour Rates</H3>
                                    <p>Update the types of workday in your company and they pay rates they call for.</p>
                                </div>

                                <div className="title">
                                    <H5>Day/hour Rates List</H5>
                                    <span>
                                        <div>
                                            {label}
                                        </div>
                                    </span>
                                </div>
                                <Table
                                    data={ getDayhourList }
                                    columns={ tableColumn }
                                    onDataChange={ this.handleTableChanges }
                                    pagination
                                    ref={ ( ref ) => { this.dayhourTable = ref; } }
                                />
                            </div>
                        </Container>
                    </div>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    DayHourRates: makeSelectDayHourRates(),
    products: makeSelectProducts(),
    getDayhourList: makeSelectGetDayHourRates(),
    loading: makeSelectLoading(),
    isEditing: makeSelectIsEditing(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        getDayhourActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( DayHourRates );
