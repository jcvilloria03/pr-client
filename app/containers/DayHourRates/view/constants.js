/*
 *
 * DayHourRates constants
 *
 */
export const nameSpace = 'app/DayHourRates/view';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const GET_DAYHOUR = `${nameSpace}/GET_DAYHOUR`;
export const SET_DAYHOUR = `${nameSpace}/SET_DAYHOUR`;
export const SET_LOADING = `${nameSpace}/SET_LOADING`;
export const EDIT_DAYHOUR = `${nameSpace}/EDIT_DAYHOUR`;
export const SET_EDIT_DAYHOUR = `${nameSpace}/SET_EDIT_DAYHOUR`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
