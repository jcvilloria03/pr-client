import { createSelector } from 'reselect';

/**
 * Direct selector to the dayHourRates state domain
 */
const selectDayHourRatesDomain = () => ( state ) => state.get( 'dayHourRates' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

/**
 * Default selector used by DayHourRates
 */

const makeSelectDayHourRates = () => createSelector(
  selectDayHourRatesDomain(),
  ( substate ) => substate.toJS()
);

const makeSelectGetDayHourRates = () => createSelector(
  selectDayHourRatesDomain(),
  ( substate ) => substate.get( 'getData' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectDayHourRatesDomain(),
( substate ) => substate.get( 'loading' )
);

const makeSelectIsEditing = () => createSelector(
  selectDayHourRatesDomain(),
( substate ) => substate.get( 'isEditing' )
);

const makeSelectNotification = () => createSelector(
  selectDayHourRatesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export default makeSelectDayHourRates;
export {
  selectDayHourRatesDomain,
  makeSelectProducts,
  makeSelectIsEditing,
  makeSelectGetDayHourRates,
  makeSelectLoading,
  makeSelectNotification
};
