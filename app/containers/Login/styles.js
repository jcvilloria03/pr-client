import styled from 'styled-components';

export const StyledWrapper = styled.div`
    background-color: #f0f4f6;
    min-height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    color: #444;
    padding: 20px;
    margin-top: -75px;
`;

export const StyledLink = styled.a`
    background: #fff;
    box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.5);
    border-radius: 40px;
    cursor: pointer;
    padding: .5rem 1rem;
    position: absolute;;
    top: 25px;
    left: 25px;

    &:hover {
        background: #F6F6F6;
        text-decoration: none;
    }
`;

export const StyledForm = styled.form`
    background-color: #FFF;
    width: 30%;
    min-width: 400px;
    max-width: 500px;
    height: 100%;
    box-shadow: 0px 0px 20px 15px rgba(204,204,204,.2);
    height: 100%;
    padding: 15px 50px 30px;
    margin-bottom: 3rem;

    @media (max-width: 480px) {
        padding: 15px;
        width: 100%;
        min-width: 0;
    }

    #brand {
        text-align: center;
        margin-bottom: 2rem;

        img {
            width: 80%;
        }
    }

    label > span > span {
        display: none;
    }

    #submit {
        text-align: center;

        button {
            padding: 12px 32px;
            font-size: 18px;
            border-radius: 40px;
            font-weight: 400;
            display: block;
            margin: 0 auto 2rem;
        }

        p {
            font-size: 12px;
            color: #999;
            margin: 0;
        }
    }
`;
