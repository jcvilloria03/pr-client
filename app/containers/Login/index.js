import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { isMobile } from 'react-device-detect';

import { browserHistory } from 'utils/BrowserHistory';
import { auth } from 'utils/AuthService';

import A from 'components/A';
import Button from 'components/Button';
import Input from 'components/Input';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';

import logo from '../../assets/logo-salarium-blue.png';

import {
    LOGIN_PAGE_SNACKBAR_MESSAGE,
    MAIN_PAGE_PERMISSION
} from '../../constants';

import {
    StyledWrapper,
    StyledLink,
    StyledForm
} from './styles';
import { company } from '../../utils/CompanyService';

/**
 *
 * Login
 *
 */
export class Login extends React.Component { // eslint-disable-line react/prefer-stateless-function
    /**
     * Component constructor method.
     */
    constructor( props ) {
        super( props );
        this.state = {
            disabledSubmit: false,
            notification: {
                title: '',
                message: '',
                type: 'error',
                show: false
            },
            submit: false,
            attempt: 0,
            unblockTimer: 0,
            unblockCountdown: null
        };
        this._checkForm = this._checkForm.bind( this );
        this._handleSubmit = this._handleSubmit.bind( this );
        this._login = this._login.bind( this );
        this._loginCallback = this._loginCallback.bind( this );
        this.updateUnblockCountdown = this.updateUnblockCountdown.bind( this );
    }

    /**
     * checks if there are any existing token and redirects to dashboard if its still valid
     */
    componentWillMount() {
        this.redirectIfLoggedIn();
    }

    componentDidMount() {
        const message = JSON.parse( localStorage.getItem( LOGIN_PAGE_SNACKBAR_MESSAGE ) );

        if ( message ) {
            this.notify( message );
            localStorage.removeItem( LOGIN_PAGE_SNACKBAR_MESSAGE );
        }
    }

    /** Development
     * Fetches value of Form fields
     * @returns {{email, password}}
     */
    getAuthParams() {
        return {
            email: this.emailInput.state.value,
            password: this.passwordInput.state.value
        };
    }

    notify( error ) {
        this.setState({
            notification: {
                title: 'Error',
                message: error.message || 'Invalid Credentials',
                type: 'error',
                show: true
            }
        });
        setTimeout( () => {
            this.setState({
                notification: {
                    title: '',
                    message: '',
                    type: 'error',
                    show: false
                }
            });
        }, 5500 );
    }

    /**
     * Redirect authenticated user to appropriate page.
     */
    redirectIfLoggedIn() {
        const hasValidToken = auth.loggedIn();

        if ( !hasValidToken ) {
            return;
        }

        if ( !auth.emailVerified() ) {
            localStorage.clear();
            return;
        }

        if ( auth.isNonAdmin() || ( auth.isEmployee() && isMobile ) ) {
            window.location.href = '/ess/';
            return;
        }

        let url = '/dashboard/';

        const redirect = localStorage.getItem( 'login_redirect' );
        if ( redirect ) {
            url = redirect;
            localStorage.removeItem( 'login_redirect' );
        }

        if ( auth.isExpired() ) {
            url = '/control-panel/subscriptions/summary';
        }

        window.location.href = url;
    }

    /**
     * Sends a login request.
     * @param e
     */
    async _login() {
        !this.state.submit && this.setState({ submit: true });
        const { email, password } = this.getAuthParams();
        const loginErrorResponse = await auth.login( email, password );
        if ( auth.loggedIn() && auth.isOwner() ) {
            await company.setToDemoCompany();
        }
        this._loginCallback( loginErrorResponse );
    }

    /**
     * this is called after login attemp and displays notification.
     */
    async _loginCallback( error ) {
        if ( error ) {
            this.notify( error );
            this.setState({ submit: false, attempt: this.state.attempt + 1, disabledSubmit: false });
            if ( this.state.attempt >= 5 ) {
                clearInterval( this.state.unblockTimer );
                this.setState({ unblockTimer: null }, () => {
                    this.setState({
                        unblockCountdown: 5,
                        unblockTimer: setInterval( this.updateUnblockCountdown, 1000 )
                    });
                });
            }
        }

        const profile = auth.getUser();
        if ( auth.isFreshAccount() ) {
            browserHistory.push( '/change-password', true );
        } else if ( auth.isForAuthnMigration() ) {
            browserHistory.push( '/forgot-password', true );
        } else if ( profile ) {
            const redirectPath = localStorage.getItem( `login_redirect:${profile.user_id}` );
            if ( redirectPath ) {
                browserHistory.push( redirectPath );
                localStorage.removeItem( 'login_redirect' );
            } else {
                this.redirectIfLoggedIn();
            }
        }
    }

    updateUnblockCountdown() {
        const { unblockCountdown } = this.state;
        if ( unblockCountdown > 0 ) {
            this.setState({ unblockCountdown: unblockCountdown - 1 });
        } else {
            clearInterval( this.state.unblockTimer );
            this.setState({ unblockTimer: null, disabledSubmit: false });
        }
    }

    /**
     * This validates the login form and enables or disables the submit button.
     * @private
     */
    _checkForm() {
        const invalidEmail = this.emailInput.state.error === true;
        const invalidPassword = this.passwordInput.state.error === true;
        const disabledSubmit = invalidEmail === true || invalidPassword === true;
        this.setState({ disabledSubmit });
    }

    /**
     * This validates the login form and enables or disables the submit button.
     * @private
     */
    async _handleSubmit() {
        const invalidEmail = this.emailInput._validate( this.emailInput.state.value );
        const invalidPassword = this.passwordInput._validate( this.passwordInput.state.value );
        !invalidEmail && !invalidPassword && await this._login();
    }
    /**
     * Login render method
     */
    render() {
        const isBlocked = ( this.state.unblockCountdown > 0 && this.state.unblockTimer >= 0 );
        return (
            <StyledWrapper>
                <Helmet
                    title="Login"
                    meta={ [
                        { name: 'Login Page', content: 'Login to your Account' }
                    ] }
                />
                <SnackBar
                    message={ this.state.notification.message }
                    title={ this.state.notification.title }
                    type={ this.state.notification.type }
                    show={ this.state.notification.show }
                    delay={ 5000 }
                    ref={ ( notification ) => { this.notification = notification; } }
                />
                <StyledLink
                    href="https://app.salarium.com/users/login"
                >
                    Sign-in on the Old Version
                </StyledLink>
                <StyledForm>
                    <div id="brand">
                        <img src={ logo } alt="Salarium" />
                    </div>
                    <Input
                        id="email"
                        label="Email"
                        type="email"
                        className="sl-c-form-group"
                        required
                        ref={ ( input ) => { this.emailInput = input; } }
                        onChange={ this._checkForm }
                    />
                    <Input
                        id="password"
                        label="Password"
                        type="password"
                        className="sl-c-form-group"
                        min={ 6 }
                        required
                        ref={ ( input ) => { this.passwordInput = input; } }
                        onChange={ this._checkForm }
                    />
                    <div id="submit">
                        <Button
                            label={ this.state.submit ? <Loader /> : isBlocked ? `LOGIN (${this.state.unblockCountdown})` : 'LOGIN' }
                            size="large"
                            disabled={ this.state.disabledSubmit || isBlocked }
                            onClick={ () => {
                                this._handleSubmit();
                                this.setState({ disabledSubmit: true });
                            } }
                        />
                        <p>Trouble signing in?&nbsp;&nbsp;<A href="/forgot-password">Forgot password</A></p>
                    </div>
                </StyledForm>
                <p><A href={ process.env.OPEN_SIGNUP_URL }>Not using Salarium? Get Started today!</A></p>
                © { ( new Date().getFullYear() ) } Salarium LTD. ALL RIGHTS RESERVED
            </StyledWrapper>
        );
    }
}

Login.propTypes = {
    dispatch: PropTypes.func.isRequired, // eslint-disable-line react/no-unused-prop-types
    router: PropTypes.object
};

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return {
        dispatch
    };
}

export default connect( null, mapDispatchToProps )( Login );
