import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import get from 'lodash/get';

import { browserHistory } from '../../utils/BrowserHistory';
import { auth } from '../../utils/AuthService';

import {
    SUBMIT,
    SET_NOTIFICATION,
    NOTIFICATION,
    SET_SUBMITTING
} from './constants';

/**
 * Submits form
 *
 */
export function* submitForm({ payload }) {
    const {
        token,
        newPassword,
        newPassConfirm
    } = payload;

    yield put({
        type: SET_SUBMITTING,
        payload: true
    });

    try {
        const response = yield call( auth.resetPassword, {
            token,
            newPassword,
            newPassConfirm
        });

        if ( response ) {
            yield put({
                type: SET_NOTIFICATION,
                payload: {
                    title: 'Request Ticket Sent',
                    message: 'You have successfully changed your password. Logging out in 5 seconds.',
                    type: 'success',
                    show: true
                }
            });

            yield call( delay, 5000 );
            localStorage.clear();
            yield call( browserHistory.replace, '/login', true );
        }
    } catch ( error ) {
        let newError;
        if ( get( error, 'response.status' ) === 406 ) {
            newError = {
                title: 'Not Acceptable',
                message: get( error, 'response.data.token.0', '' )
            };
        } else {
            newError = error;
        }

        yield call( notifyUser, newError );
    } finally {
        yield put({
            type: SET_SUBMITTING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: get( error, 'title', get( error, 'response.statusText', 'Error' ) ),
        message: get( error, 'message', get( error, 'response.data.message', error.statusText ) ),
        type: 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Individual exports for testing
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchNotify
];
