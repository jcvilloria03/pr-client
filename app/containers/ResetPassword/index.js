import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import logo from '../../assets/logo-salarium-blue.png';

import SnackBar from '../../components/SnackBar';
import { P } from '../../components/Typography';
import Input from '../../components/Input';
import Button from '../../components/Button';
import A from '../../components/A';

import {
    StyledWrapper,
    StyledForm
} from './styles';
import * as actions from './actions';
import {
    makeSelectSubmitting,
    makeSelectNotification
} from './selectors';

/**
 *
 * ResetPassword
 *
 */
export class ResetPassword extends React.PureComponent {
    static propTypes = {
        notification: PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitRequest: PropTypes.func,
        submitting: PropTypes.bool,
        params: PropTypes.object
    }

    constructor( props ) {
        super( props );

        /**
         * References for password and password confirm inputs
         */
        this.password = null;
        this.confirm = null;
    }

    /**
     * Form Validation
     */
    validateFields = () => {
        let valid = true;

        const password = this.password.state;
        const confirm = this.confirm.state;

        if ( password.value && !password.error && confirm.value && !confirm.error ) {
            valid = password.value === confirm.value;
            this.confirm.setState({
                error: !valid,
                errorMessage: valid ? '' : 'Your password does not match'
            });
        } else {
            valid = false;
        }
        return valid;
    }

    /**
     * sends a request to reset password
     */
    submitRequest() {
        const valid = this.validateFields();
        if ( valid ) {
            this.props.submitRequest({
                token: this.props.params.token,
                newPassword: this.password.state.value,
                newPassConfirm: this.confirm.state.value
            });
        }
    }

    /**
     *
     * ResetPassword render method
     *
     */
    render() {
        const { notification, submitting } = this.props;

        return (
            <div>
                <StyledWrapper>
                    <Helmet
                        title="Reset Password"
                        meta={ [
                            { name: 'description', content: 'Description of ResetPassword' }
                        ] }
                    />
                    <SnackBar
                        message={ notification.message }
                        title={ notification.title }
                        offset={ { top: 0 } }
                        type={ notification.type }
                        show={ notification.show }
                        delay={ 5000 }
                        ref={ ( ref ) => { this.notification = ref; } }
                    />
                    <div className="brand">
                        <img src={ logo } alt="Salarium" />
                    </div>
                    <StyledForm>
                        <Input
                            required
                            id="new"
                            type="password"
                            label="New Password"
                            onBlur={ this.validateFields }
                            min={ 8 }
                            ref={ ( ref ) => { this.password = ref; } }
                            className="sl-u-gap-bottom"
                            disabled={ submitting }
                        />
                        <Input
                            required
                            id="confirm"
                            type="password"
                            label="Confirm New Password"
                            onBlur={ this.validateFields }
                            min={ 8 }
                            ref={ ( ref ) => { this.confirm = ref; } }
                            disabled={ submitting }
                        />
                        <div id="submit">
                            <Button
                                label="Submit"
                                size="large"
                                disabled={ submitting }
                                onClick={ () => {
                                    this.password._validate( this.password.state.value );
                                    this.confirm._validate( this.confirm.state.value );
                                    this.submitRequest();
                                } }
                            />
                        </div>
                    </StyledForm>
                    <P>Back to <A href="/login">Login</A></P>
                </StyledWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    submitting: makeSelectSubmitting(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ResetPassword );
