import {
    SUBMIT
} from './constants';

/**
 * Submit request for reset password
 * @param {Object} payload - Contains token, password and confirm password
 */
export function submitRequest( payload ) {
    return {
        type: SUBMIT,
        payload
    };
}
