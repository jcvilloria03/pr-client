import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'resetPassword' );

const makeSelectSubmitting = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'submitting' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectSubmitting,
    makeSelectNotification
};
