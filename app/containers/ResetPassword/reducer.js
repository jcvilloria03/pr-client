import { fromJS } from 'immutable';

import {
    SET_NOTIFICATION,
    SET_SUBMITTING
} from './constants';

const initialState = fromJS({
    submitting: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 * Reset Password Reducer
 */
function ResetPasswordReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_SUBMITTING:
            return state.set( 'submitting', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default ResetPasswordReducer;
