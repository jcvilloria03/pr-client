/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-restricted-syntax */
import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import Sidebar from 'components/Sidebar';
import { getIdsOfSelectedRows, isAnyRowSelected } from 'components/Table/helpers';
import SalDropdown from 'components/SalDropdown';
import Modal from 'components/Modal';
import Table from 'components/Table';
import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';
import { H1, H2, H3, H5 } from 'components/Typography';
import Button from 'components/Button';
import SnackBar from 'components/SnackBar';

import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import makeSelectTardinessRules, {
    makeSelectListTardinessRules,
    makeSelectLoading,
    makeSelectNotification
} from './selectors';

import * as tardinessRulesAction from './actions';

import { LoadingStyles, PageWrapper } from './styles';
/**
 *
 * TardinessRules
 *
 */
export class TardinessRules extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        ListtardinessRules: React.PropTypes.array,
        getTardinessRules: React.PropTypes.func,
        deleteTardinessRules: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    static defaultProps = {
        loading: true
    };
    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: [],
            deleteLabel: '',
            totalSelected: 0,
            flag: true,
            selectPage: false,
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            }
        };
    }
    componentDidMount() {
        this.props.getTardinessRules();
    }
    componentWillReceiveProps( nextProps ) {
        // if ( nextProps.ListtardinessRules !== this.props.ListtardinessRules ) {
        this.setState({
            displayedData: nextProps.ListtardinessRules,
            label: formatPaginationLabel( this.tardinessRulesTable.tableComponent.state ),
            pagination: {
                total: nextProps.ListtardinessRules.length,
                per_page: 10,
                current_page: 1,
                last_page: Math.ceil( nextProps.ListtardinessRules.length / 10 ),
                to: 0
            }
        });
        // }
    }
    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.tardinessRulesTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.tardinessRulesTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }
    getDropdownItems() {
        return [
            {
                children: <div>Delete</div>,
                onClick: () => this.DeleteModel.toggle()
            }
        ];
    }
    AffectedEmployeesListdata = ( row ) => {
        if ( ( !row || row === '' ) && ( row.length > 0 ) ) {
            return '';
        }
        if ( row.length === 1 ) {
            return row[ 0 ];
        }
        if ( row.length <= 0 ) {
            return '';
        }
        return row.length > 2 ? `${row[ 0 ]} and ${row.length - 1} more` : `${row[ 0 ]} and ${row[ 1 ]}`;
    }
    checkBoxchanged=() => {
        this.tardinessRulesTable.state.selected = this.tardinessRulesTable.state.selected.map( ( value ) => {
            this.setState({ flag: false });
            return ( value || !value );
        });
        const selectionLength = this.tardinessRulesTable.state.selected.length;
        this.setState({ selectPage: false, deleteLabel: formatDeleteLabel( selectionLength ) });
    }
    handleTableChanges = ( tableProps = this.tardinessRulesTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            delete_label: formatDeleteLabel()
        });
        if ( this.tardinessRulesTable ) {
            this.setState({ selectPage: this.pageSelectOne( this.tardinessRulesTable.state.pageSelected ) || false });
        }
    }
    pageSelectOne=( pageObj ) => {
        let isTrue;
        for ( const key in pageObj ) {
            if ( ( this.tardinessRulesTable.tableComponent.state.page === +key ) && ( pageObj[ key ] === true ) ) {
                isTrue = pageObj[ key ];
            }
        }
        return isTrue;
    }
    hanldeDelete = () => {
        const ids = getIdsOfSelectedRows( this.tardinessRulesTable );
        this.props.deleteTardinessRules({ tardiness_rule_ids: ids, company_id: company.getLastActiveCompanyId() });
        this.DeleteModel.toggle();
    }
    /**
     *
     * TardinessRules render method
     *
     */
    tableColumns() {
        return [
            {
                id: 'name',
                header: 'Tardiness Rule Name ',
                minWidth: 200,
                style: { fontSize: '14px' },
                sortable: false,
                render: ({ row }) => (
                    <div>{row.name}</div>
                )
            },
            {
                id: 'affectedEmployees',
                header: 'Affected Employees',
                minWidth: 200,
                style: { fontSize: '14px' },
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {this.AffectedEmployeesListdata( row.affected_employees.map( ( value ) => value.name ) )}
                    </div>
                )
            },
            {
                id: 'gracePeriod',
                header: 'Grace Period',
                width: 200,
                style: { fontSize: '14px' },
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.minutes_tardy}
                    </div>
                )
            },
            {
                id: 'startDeducting',
                header: 'Start Deducting after ______ Minutes',
                minWidth: 250,
                style: { fontSize: '14px' },
                sortable: false,
                render: ({ row }) => (
                    <div>
                        {row.minutes_to_deduct}
                    </div>
                )
            },
            {
                id: 'id',
                header: ' ',
                accessor: 'actions',
                width: 100,
                sortable: false,
                style: { justifyContent: 'end' },
                render: ({ row }) => (
                    <Button
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => browserHistory.push( `/company-settings/schedule-settings/tardiness-rules/${row.id}/edit`, true ) }
                    />

                )

            }
        ];
    }
    render() {
        const { loading } = this.props;
        const { displayedData, label, deleteLabel, flag, selectPage, totalSelected } = this.state;
        const hasSelectedItems = isAnyRowSelected( this.tardinessRulesTable );
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        return (
            <div>
                <Helmet
                    title="Tardiness Rules"
                    meta={ [
                        { name: 'description', content: 'Description of Tardiness Rules' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="Confirm Your Action"
                    className="modal-md modal-commission-type"
                    center
                    ref={ ( ref ) => {
                        this.DeleteModel = ref;
                    } }
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.DeleteModel.toggle()
                        },
                        {
                            type: 'darkRed',
                            label: 'Yes',
                            onClick: this.hanldeDelete
                        }
                    ] }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <PageWrapper>
                    <div className="loader" style={ { display: loading ? '' : 'none' } }>
                        <LoadingStyles>
                            <H2>Loading Tardiness Rules.</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    </div>
                    <div className="content" style={ { display: loading ? 'none' : '' } }>
                        <div className="contentData">
                            <H1 noBottomMargin>Tardiness Rules</H1>
                        </div>
                        <div className="tardiness_section">
                            <H5 noBottomMargin>Tardiness Rule List</H5>
                            <div className="selected_more">
                                <span>
                                    <div>
                                        { hasSelectedItems ? deleteLabel : (
                                            <div className="show_contain">
                                                <span>{label}</span>
                                                <Button
                                                    label="Add Tardiness rule"
                                                    size="large"
                                                    type="action"
                                                    onClick={ () => browserHistory.push( '/company-settings/schedule-settings/tardiness-rules/add', true ) }
                                                />
                                            </div>
                                            )
                                        }
                                    </div>
                                </span>
                                {hasSelectedItems && (
                                    <span className="sl-u-gap-left--sm">
                                        <SalDropdown dropdownItems={ this.getDropdownItems() } />
                                    </span>
                                )}
                            </div>
                        </div>
                        {selectPage === true && !flag ?
                            <div className="selectPage">
                                <p>{`All ${totalSelected} entries on this page are selected.`}<a onClick={ () => this.checkBoxchanged() }> {`Select all ${this.props.ListtardinessRules.length} entries from all pages`}</a></p>
                            </div>
                            : ''
                        }
                        <div className="tardiness_table">
                            <Table
                                data={ displayedData }
                                columns={ this.tableColumns() }
                                onDataChange={ this.handleTableChanges }
                                sizeOptions={ [ 10, 25, 50, 100 ] }
                                ref={ ( ref ) => { this.tardinessRulesTable = ref; } }
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    const isSelected = this.props.ListtardinessRules.length === selectionLength ? 'true' : 'false';
                                    this.setState({
                                        totalSelected: selectionLength,
                                        selectPage: this.pageSelectOne( this.tardinessRulesTable.state.pageSelected ),
                                        flag: JSON.parse( isSelected ),
                                        deleteLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                                page={ this.state.pagination.current_page - 1 }
                                pageSize={ this.state.pagination.per_page }
                                pages={ this.state.pagination.total }
                                external
                                selectable
                            />
                        </div>
                        <div>
                            <FooterTablePaginationV2
                                page={ this.state.pagination.current_page }
                                pageSize={ this.state.pagination.per_page }
                                pagination={ this.state.pagination }
                                onPageChange={ this.onPageChange }
                                onPageSizeChange={ this.onPageSizeChange }
                                paginationLabel={ this.state.label }
                                fluid
                            />
                        </div>
                    </div>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    TardinessRules: makeSelectTardinessRules(),
    ListtardinessRules: makeSelectListTardinessRules(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        tardinessRulesAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( TardinessRules );
