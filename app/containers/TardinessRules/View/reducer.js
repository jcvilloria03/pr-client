import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, DELETED_TARDINESS_RULES, LOADING, NOTIFICATION_SAGA, SET_TARDINESS_RULES
} from './constants';

const initialState = fromJS({
    tardinessRules: [],
    loading: true,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * TardinessRules reducer
 *
 */
function tardinessRulesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_TARDINESS_RULES:
            return state.set( 'tardinessRules', fromJS( action.payload ) );
        case DELETED_TARDINESS_RULES:
            return state.set( 'deleteData', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default tardinessRulesReducer;
