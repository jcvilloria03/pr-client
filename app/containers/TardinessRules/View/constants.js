/*
 *
 * TardinessRules constants
 *
 */
const namespace = 'app/LeaveTypes/View';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;

export const SET_TARDINESS_RULES = `${namespace}/SET_TARDINESS_RULES`;
export const GET_TARDINESS_RULES = `${namespace}/GET_TARDINESS_RULES`;

export const DELETE_TARDINESS_RULES = `${namespace}/DELETE_TARDINESS_RULES`;
export const DELETED_TARDINESS_RULES = `${namespace}/DELETED_TARDINESS_RULES`;

export const LOADING = `${namespace}/LOADING`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;

