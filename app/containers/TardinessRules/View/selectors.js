import { createSelector } from 'reselect';

/**
 * Direct selector to the tardinessRules state domain
 */
const selectTardinessRulesDomain = () => ( state ) => state.get( 'tardinessRules' );

/**
 * Default selector used by TardinessRules
 */
const makeSelectTardinessRules = () => createSelector(
  selectTardinessRulesDomain(),
  ( substate ) => substate.toJS()
);

/**
 * Other specific selectors
 */
const makeSelectListTardinessRules = () => createSelector(
  selectTardinessRulesDomain(),
  ( substate ) => substate.get( 'tardinessRules' ).toJS()
);
const makeSelectLoading = () => createSelector(
  selectTardinessRulesDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectNotification = () => createSelector(
  selectTardinessRulesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);
export default makeSelectTardinessRules;
export {
  selectTardinessRulesDomain,
  makeSelectListTardinessRules,
  makeSelectLoading,
  makeSelectNotification
};
