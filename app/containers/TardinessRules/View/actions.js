import {
    DEFAULT_ACTION, DELETE_TARDINESS_RULES, GET_TARDINESS_RULES
} from './constants';

/**
 *
 * TardinessRules actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Get tardiness rules actions
 *
 */
export function getTardinessRules() {
    return {
        type: GET_TARDINESS_RULES
    };
}

/**
 *
 *Delete TardinessRules actions
 *
 */
export function deleteTardinessRules( IDs ) {
    return {
        type: DELETE_TARDINESS_RULES,
        payload: IDs
    };
}
