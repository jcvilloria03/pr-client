import { get } from 'lodash';
import { call, cancel, put, take } from 'redux-saga/effects';
import { delay, takeEvery } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';
import { DELETED_TARDINESS_RULES, DELETE_TARDINESS_RULES, GET_TARDINESS_RULES, LOADING, NOTIFICATION, NOTIFICATION_SAGA, SET_TARDINESS_RULES } from './constants';

/**
 *  get TardinessRules
 */
export function* getTardinessRules() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/tardiness_rules`, { method: 'GET' });
        yield put({
            type: SET_TARDINESS_RULES,
            payload: res.data || []
        });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message || `Request failed with statuscode ${error.response.status}` : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}
/**
 * Delete Tardiness Rules
 */
export function* DeleteTardinessRules({ payload }) {
    try {
        yield put({
            type: DELETED_TARDINESS_RULES,
            payload: true
        });

        const isUse = yield call( Fetch, '/tardiness_rule/check_in_use', { method: 'POST', data: payload });
        if ( isUse && isUse.in_use === 0 ) {
            yield call( Fetch, '/tardiness_rule/bulk_delete', {
                method: 'DELETE',
                data: {
                    company_id: payload.company_id,
                    tardiness_rules_ids: payload.tardiness_rule_ids
                }
            });

            yield call( notifyUser, {
                title: 'Success',
                message: 'Record successfully deleted',
                show: true,
                type: 'success'
            });

            yield call( getTardinessRules );
        } else {
            yield call( notifyError, {
                show: true,
                title: 'Error',
                message: 'Tardiness rule is in use.',
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message || `Request failed with statuscode ${error.response.status}` : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: DELETED_TARDINESS_RULES,
            payload: false
        });
    }
}
/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}
/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });
}
/**
* watchForNotifyUser
*/
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * watchForgetTardinessRule
 */
export function* watchForgetTardinessRule() {
    const watcher = yield takeEvery( GET_TARDINESS_RULES, getTardinessRules );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
 /**
 * watchFordeltebonusTypes
 */
export function* watchForDeleteTardinessRules() {
    const watcher = yield takeEvery( DELETE_TARDINESS_RULES, DeleteTardinessRules );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
// All sagas to be loaded
export default [
    watchForgetTardinessRule,
    watchForDeleteTardinessRules,
    watchForNotifyUser
];
