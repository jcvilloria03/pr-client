import styled from 'styled-components';
export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh ;
    padding: 0;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
    overflow: hidden;
`;

export const PageWrapper = styled.div`
    padding-left: 91px;
    display: flex;
    flex-direction: column;
    align-items: center;
    .content{
        margin-top: 120px;
        padding-bottom: 80px;
        width: 84vw;
        .contentData{
            text-align: center;
            color: #474747;
            margin-bottom: 4rem;
            margin-top: -1.5rem;
            h1{
                font-weight: bold;
                font-size: 36px;
                line-height: 1.6;
                margin: 0;
            }
            p{
                font-size: 14px;
            }
        }
        .tardiness_section{
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 1rem;
            h2{
                font-size: 20px;
                font-weight: 600;
                margin-bottom: 0;
            }
            .show_contain{
                display: flex;
                align-items: center;
                gap: 15px;
                span{
                    font-size: 15px;
                }
                button{
                    padding: 0.5rem 1.5rem;
                }
            }
            .selected_more{
                display: flex;
                align-items: center;
                gap: 10px;

                span{
                    font-size: 14px;
                }

                .dropdown,
                .dropdown-item {
                    font-size: 14px;
                }

                .dropdown-menu {
                    min-width: 122px;
                }
            }
        }
        .tardiness_table {
            .ReactTable{
                .rt-tbody{
                    .selected{
                        background-color: rgba(131, 210, 75, 0.15);
                        &:hover{
                            background-color: rgba(131, 210, 75, 0.15) !important;
                        }
                    }
                }
            }
            .ReactTable .rt-tbody .rt-td {
                padding: 14px;
                font-size: 14px;
            }
        }
        ${'' /* .tardiness_table{
            .ReactTable{
                .rt-table{
                    .rt-thead{
                        @media(max-width:1640px){
                            min-width: 1200px !important;
                        }
                        .rt-tr{
                            height: 63px;
                            padding: 5px 0px;
                            .rt-resizable-header-content{
                                font-size: 14px;
                            }
                            @media(max-width:1640px){
                                justify-content: space-between !important;
                            }
                            .rt-th{
                                @media(max-width:1640px){
                                    flex: initial !important;
                                }
                                &:not(:first-child):not(:last-child){
                                    @media(max-width:1640px){
                                        width: 250px !important;
                                    }
                                }
                            }
                        }
                    }
                }
                .rt-tbody{
                    @media(max-width:1640px){
                        min-width: 1200px !important;
                        .rt-tr-group{
                            .rt-tr{
                                justify-content: space-between !important;
                            }
                        }
                    }
                    .rt-td{
                        height: 63px;
                        padding: 1rem;
                        @media(max-width:1640px){
                            flex: initial !important;
                        }
                        &:not(:first-child):not(:last-child){
                            @media(max-width:1640px){
                                width: 250px !important;
                            }
                        }
                    }
                }
            }
        } */}
        .selectPage{
            padding: 1rem;
            background-color: #e5f6fc;
            text-align: center;
            p{
                margin-bottom: 0;
                font-size: 14px;
                font-weight: 700;
                a{
                   color: #00A5E5;
                }
            }
        }
    }
`;
