/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';

import A from 'components/A';
import Sidebar from 'components/Sidebar';
import Input from 'components/Input';
import MultiSelect from 'components/MultiSelect';
import Button from 'components/Button';
import Modal from 'components/Modal';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import Icon from 'components/Icon';
import { H1 } from 'components/Typography';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';

import { SELECT_ALL_EMPLOYEES_OPTIONS } from '../../Dashboard/constants';

import makeSelectEditTardinessRules, {
    makeSelecteditTardinessRules,
    makeSelectgetEditTardinessRules,
    makeSelectLoading,
    makeSelectNotification
} from './selectors';

import * as TardinessRulesEditAction from './actions';

import { Footer, Header, PageWrapper } from './styles';
/**
 *
 * EditTardinessRules
 *
 */
export class EditTardinessRules extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        params: React.PropTypes.object,
        getEditTardinessRules: React.PropTypes.func,
        editTardinessRules: React.PropTypes.func,
        isEdit: React.PropTypes.object,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    static defaultProps = {
        loading: false
    };
    constructor( props ) {
        super( props );

        this.state = {
            profile: null,
            formData: {},
            validEditName: 'name',
            validMinutesTardy: '1',
            gracePosVali: false,
            deductPosVali: false,
            validminutesToDeduct: '1',
            validexistName: false,
            validexistNameLoading: false
        };
        this.discardModal = null;
    }
    componentWillMount() {
        const profile = auth.getUser();

        this.setState({ profile });
    }
    componentDidMount() {
        this.props.getEditTardinessRules( this.props.params.id );
    }
    componentWillReceiveProps( nextProps ) {
        this.setState({
            formData: { ...nextProps.isEdit,
                affected_employees: nextProps.isEdit.affected_employees && nextProps.isEdit.affected_employees.map( ( data ) => ({
                    value: data.id,
                    label: data.name,
                    type: data.type
                }) )
            }
        });
    }

    gettardinessRules = ( keyword, callback ) => {
        Fetch( `/company/${this.state.profile.last_active_company_id}/affected_employees/search?term=${keyword}&limit=10&without_users=yes`, { method: 'GET' })
            .then( ( result ) => {
                const tardinessRules = result.map( ({ id, name, type }) => ({
                    id,
                    type,
                    uid: `${type}${id}`,
                    value: id,
                    label: name,
                    field: type
                }) );
                callback( null, { options: SELECT_ALL_EMPLOYEES_OPTIONS.concat( tardinessRules ) });
            })
            .catch( ( error ) => callback( error, null ) );
    }

    validateFields = () => {
        let valid = true;
        if ( this.state.formData.name === '' ) {
            this.setState({
                validAddName: ''
            });
            valid = false;
        }
        if ( this.state.formData.minutesTardy === '' ) {
            this.setState({
                validMinutesTardy: ''
            });
            valid = false;
        }

        if ( +this.state.formData.minutesTardy < 0 ) {
            this.setState({
                gracePosVali: true
            });
            valid = false;
        }

        if ( this.state.formData.minutesToDeduct !== '' && this.state.formData.minutesToDeduct < 0 ) {
            this.setState({
                deductPosVali: true
            });
            valid = false;
        }

        return valid;
    }

    submitEditForm = async () => {
        const companyId = company.getLastActiveCompanyId();
        if ( this.state.formData.name === '' ) {
            this.setState({
                validEditName: ''
            });
        }

        if ( this.state.formData.minutes_tardy === '' ) {
            this.setState({
                validMinutesTardy: ''
            });
        }
        if ( this.state.formData.name !== '' ) {
            const data = {
                ...this.state.formData,
                company_id: companyId,
                id: this.props.params.id,
                tardiness_rule_id: this.props.params.id,
                affected_employees: this.state.formData.affected_employees.map( ( value ) => ({
                    id: value.id,
                    name: value.name,
                    type: value.type,
                    uid: `${value.type}${value.id}`
                }) )
            };

            this.setState({ validexistNameLoading: true });
            await Fetch( `/company/${companyId}/tardiness_rule/is_name_available`, { method: 'POST', data })
                .then( ( response ) => {
                    this.setState({ validexistNameLoading: false });
                    if ( response.available ) {
                        if ( this.validateFields() ) {
                            this.props.editTardinessRules( data );
                        }
                    } else {
                        this.setState({ validexistName: true });
                    }
                });
        }
    }

    handleInteger=( value ) => {
        const pattern = /^\d*[0-9]\d*$/;
        const result = pattern.test( value );

        return !result;
    }

    /**
     *
     * EditTardinessRules render method
     *
     */
    render() {
        const { loading } = this.props;
        const { formData, validEditName, validMinutesTardy, validexistName, validminutesToDeduct, gracePosVali, validexistNameLoading } = this.state;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        return (
            <div>
                <Helmet
                    title="Edit Tardiness Rule"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Tardiness Rule' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Discard Changes"
                    body={
                        <p>
                           Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?
                        </p>
                    }
                    buttons={ [
                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.discardModal.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/company-settings/schedule-settings/tardiness-rules', true );
                                this.discardModal.toggle();
                            }
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Header>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/schedule-settings/tardiness-rules', true );
                                } }
                            >
                                <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Tardiness Rules</span>
                            </A>
                        </Container>
                    </div>
                </Header>
                <PageWrapper>
                    <div className="content">
                        <div className="contentData">
                            <H1 noBottomMargin>Edit Tardiness Rule</H1>
                        </div>
                        <div>
                            <div className="row">
                                <div className={ validEditName === '' || validexistName ? 'col-xs-4 input_pay' : 'col-xs-4 add_tardiness' }>
                                    <Input
                                        id="tardiness_Rule_Name"
                                        label="* Tardiness Rule Name"
                                        type="text"
                                        value={ formData.name }
                                        onChange={ ( value ) => this.setState({
                                            formData: { ...formData, name: value },
                                            validEditName: value,
                                            validexistName: false
                                        }) }
                                    />
                                    {validEditName === '' ? <p>This field is required</p> : ''}
                                    {validexistName ? <p>Record name already exists</p> : ''}
                                </div>
                            </div>
                            <div className="row">
                                <div className={ validMinutesTardy === '' || ( isNaN( validMinutesTardy ) || gracePosVali ) ? 'col-xs-4 input_pay pr-0' : 'col-xs-4 add_tardiness pr-0' }>
                                    <Input
                                        id="grace_Period"
                                        label="* Grace Period"
                                        type="text"
                                        value={ formData.minutes_tardy }
                                        onChange={ ( value ) => {
                                            this.setState({
                                                formData: { ...formData, minutes_tardy: value },
                                                validMinutesTardy: value,
                                                gracePosVali: this.handleInteger( value )
                                            });
                                        } }
                                    />
                                    {validMinutesTardy === '' ? <p>This field is required</p> : ( isNaN( validMinutesTardy ) || gracePosVali ) ? <p>Please input numeric value only</p> : ''}
                                </div>
                                <div className={ ( isNaN( validminutesToDeduct ) || this.state.deductPosVali ) ? 'col-xs-4 input_pay' : 'col-xs-4 add_tardiness' }>
                                    <Input
                                        id="start_Deducting"
                                        label=" Start Deducting after ______ Minutes"
                                        type="text"
                                        value={ formData.minutes_to_deduct }
                                        onChange={ ( value ) => this.setState({
                                            formData: { ...formData, minutes_to_deduct: value },
                                            validminutesToDeduct: value,
                                            deductPosVali: this.handleInteger( value )
                                        }) }
                                    />
                                    {( isNaN( validminutesToDeduct ) || this.state.deductPosVali ) ? <p>Please input numeric value only</p> : ''}
                                </div>
                            </div>
                            <div className="row">
                                <div className="affected_Employee add_tardiness">
                                    <MultiSelect
                                        id="affected_Employee/s"
                                        async
                                        label="Affected Employee/s"
                                        loadOptions={ this.gettardinessRules }
                                        value={ formData.affected_employees }
                                        placeholder="Enter location,department name,position,employee name"
                                        onChange={ ( value ) => {
                                            this.setState({
                                                formData: { ...formData, affected_employees: value }
                                            });
                                        } }
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </PageWrapper>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                label={ ( loading || validexistNameLoading ) ? <Loader /> : 'Update' }
                                type="action"
                                size="large"
                                onClick={ () => this.submitEditForm() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}
const mapStateToProps = createStructuredSelector({
    EditTardinessRules: makeSelectEditTardinessRules(),
    isEdit: makeSelectgetEditTardinessRules(),
    editTardinessRules: makeSelecteditTardinessRules(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        TardinessRulesEditAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EditTardinessRules );
