import { createSelector } from 'reselect';

/**
 * Direct selector to the editTardinessRules state domain
 */
const selectEditTardinessRulesDomain = () => ( state ) => state.get( 'editTardinessRules' );

/**
 * Default selector used by EditTardinessRules
 */
const makeSelectEditTardinessRules = () => createSelector(
  selectEditTardinessRulesDomain(),
  ( substate ) => substate.toJS()
);

/**
 * Other specific selectors
 */
const makeSelectgetEditTardinessRules = () => createSelector(
  selectEditTardinessRulesDomain(),
  ( substate ) => substate.get( 'isEdit' ).toJS()
);
const makeSelecteditTardinessRules = () => createSelector(
  selectEditTardinessRulesDomain(),
  ( substate ) => substate.get( 'editTardinessRules' )
);
const makeSelectLoading = () => createSelector(
  selectEditTardinessRulesDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectNotification = () => createSelector(
  selectEditTardinessRulesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);
export default makeSelectEditTardinessRules;
export {
  selectEditTardinessRulesDomain,
  makeSelectgetEditTardinessRules,
  makeSelecteditTardinessRules,
  makeSelectLoading,
  makeSelectNotification
};
