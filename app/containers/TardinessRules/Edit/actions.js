import {
    DEFAULT_ACTION, GET_EDIT, GET_EDIT_TARDINESSRULES
} from './constants';

/**
 *
 * EditTardinessRules actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
/**
 * get TardinessRules action
 * @param {object} payload
 */
export function getEditTardinessRules( payload ) {
    return {
        type: GET_EDIT,
        payload
    };
}
/**
 *
 * Edit TardinessRules action
 *
 */
export function editTardinessRules( payload ) {
    return {
        type: GET_EDIT_TARDINESSRULES,
        payload
    };
}
