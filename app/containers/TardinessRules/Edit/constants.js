/*
 *
 * EditTardinessRules constants
 *
 */
const namespace = 'app/EditTardinessRules/Edit';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;

export const GET_EDIT = `${namespace}/GET_EDIT`;
export const SET_EDIT = `${namespace}/SET_EDIT`;

export const GET_EDIT_TARDINESSRULES = `${namespace}/GET_EDIT_TARDINESSRULES`;
export const SET_EDIT_TARDINESSRULES = `${namespace}/SET_EDIT_TARDINESSRULES`;

export const LOADING = `${namespace}/LOADING`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
