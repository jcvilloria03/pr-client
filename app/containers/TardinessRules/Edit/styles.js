import styled from 'styled-components';

export const Header = styled.div`
    .nav{
        padding:10px 10px;
        background:#f0f4f6;
        margin-bottom:10px;
        position: fixed;
        top: 76px;
        right: 0;
        left: 0;
        width: 100%;
        z-index:9;
    }

    .icon-arrow {
        width: 14px;
        font-size: 14px;
        display: inline-block;
        margin-right: 4px;

        > i {
            align-self: center;
        }
    }

    .back-text {
        font-size: 14px;
    }
`;
export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh ;
    padding: 0;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;
export const PageWrapper = styled.div`
    padding-left: 91px;
    display: flex;
    flex-direction: column;
    align-items: center;
    .content{
        margin-top: 120px;
        width: 80vw;
        margin-right: auto;
        margin-left: auto;
        max-width: 100%;
        padding-top: 2rem;
        .contentData{
            text-align: center;
            color: #474747;
            margin-bottom: 56px;
            h1{
                font-weight: bold;
                font-size: 36px;
                line-height: 1.6;
                margin: 0;
            }
            p{
                font-size: 14px;
                margin-bottom: 0px;
            }
        }
        .input_pay{
            input{
                border-color:#eb7575 !important;
            }
            label{
                color:#eb7575 !important;
            }
            span{
                color:#eb7575;
            }
            p{
                color:#eb7575;
                fontSize: 14px;
                margin-bottom: 0;
            }
        }
        .affected_Employee{
            padding: 0px 15px;
        }
        .add_tardiness{
            label{
                color: #474747 !important;
            }
        }
    }
    .form-control {
        font-size: 14px;
    }
`;
export const Footer = styled.div`
    text-align: right;
    padding: 10px 6vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;
