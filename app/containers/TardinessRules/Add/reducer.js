import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, LOADING, NOTIFICATION_SAGA, SET_TARDINESS_RULES
} from './constants';

const initialState = fromJS({
    AddTardinessRules: {},
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * AddTardinessRules reducer
 *
 */
function addTardinessRulesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_TARDINESS_RULES:
            return state.set( 'AddTardiness', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default addTardinessRulesReducer;
