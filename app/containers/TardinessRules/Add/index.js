import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';

import A from 'components/A';
import Input from 'components/Input';
import MultiSelect from 'components/MultiSelect';
import Button from 'components/Button';
import Modal from 'components/Modal';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import Icon from 'components/Icon';
import { H1 } from 'components/Typography';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
import { auth } from 'utils/AuthService';

import { SELECT_ALL_EMPLOYEES_OPTIONS } from '../../Dashboard/constants';

import makeSelectAddTardinessRules, {
    makeSelectLoading,
    makeSelectNotification
} from './selectors';

import * as TardinessRulesAction from './actions';

import { Footer, Header, PageWrapper } from './styles';
/**
 *
 * AddTardinessRules
 *
 */
export class AddTardinessRules extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        getaddTardinessRules: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    static defaultProps = {
        loading: false,
        btnLoading: false
    };
    constructor( props ) {
        super( props );
        this.state = {
            profile: null,
            formData: {
                name: '',
                minutesTardy: '',
                minutesToDeduct: '',
                affectedEmployees: []
            },
            validAddName: 'name',
            validMinutesTardy: '1',
            gracePosVali: false,
            deductPosVali: false,
            validminutesToDeduct: '1',
            validexistName: false,
            validexistNameLoading: false
        };
        this.discardModal = null;
    }
    componentWillMount() {
        const profile = auth.getUser();

        this.setState({ profile });
    }
    gettardinessRules = ( keyword, callback ) => {
        Fetch( `/company/${this.state.profile.last_active_company_id}/affected_employees/search?term=${keyword}&limit=10&without_users=yes`, { method: 'GET' })
            .then( ( result ) => {
                const tardinessRules = result.map( ({ id, name, type }) => ({
                    id,
                    type,
                    uid: `${type}${id}`,
                    value: id,
                    label: name,
                    field: type
                }) );
                callback( null, { options: SELECT_ALL_EMPLOYEES_OPTIONS.concat( tardinessRules ) });
            });
    }

    validateFields = () => {
        let valid = true;
        if ( this.state.formData.name === '' ) {
            this.setState({
                validAddName: ''
            });
            valid = false;
        }
        if ( this.state.formData.minutesTardy === '' ) {
            this.setState({
                validMinutesTardy: ''
            });
            valid = false;
        }

        if ( +this.state.formData.minutesTardy < 0 ) {
            this.setState({
                gracePosVali: true
            });
            valid = false;
        }

        if ( this.state.formData.minutesToDeduct !== '' && this.state.formData.minutesToDeduct < 0 ) {
            this.setState({
                deductPosVali: true
            });
            valid = false;
        }

        return valid;
    }

    submitAddForm = async () => {
        const companyId = company.getLastActiveCompanyId();

        if ( this.state.formData.name === '' ) {
            this.setState({
                validAddName: ''
            });
        }

        if ( this.state.formData.minutesTardy === '' ) {
            this.setState({
                validMinutesTardy: ''
            });
        }

        if ( this.state.formData.name !== '' ) {
            const dataList = {
                name: this.state.formData.name,
                minutes_tardy: this.state.formData.minutesTardy,
                minutes_to_deduct: this.state.formData.minutesToDeduct,
                affected_employees: this.state.formData.affectedEmployees
                && this.state.formData.affectedEmployees.map( ( value ) => ({
                    id: value.id,
                    name: value.label,
                    type: value.field,
                    uid: `${value.type}${value.id}`
                }) )
            };

            this.setState({ validexistNameLoading: true });
            await Fetch( `/company/${companyId}/tardiness_rule/is_name_available`, { method: 'POST', data: dataList })
                .then( ( response ) => {
                    this.setState({ validexistNameLoading: false });
                    if ( response.available ) {
                        if ( this.validateFields() ) {
                            this.props.getaddTardinessRules( dataList );
                        }
                    } else {
                        this.setState({ validexistName: true });
                    }
                });
        }
    }

    handleInteger=( value ) => {
        const pattern = /^\d*[0-9]\d*$/;
        const result = pattern.test( value );

        return !result;
    }

    /**
     *
     * AddTardinessRules render method
     *
     */
    render() {
        const { loading } = this.props;
        const { name, minutesTardy, minutesToDeduct, affectedEmployees } = this.state.formData;
        const { validAddName, validMinutesTardy, validminutesToDeduct, validexistName, gracePosVali, deductPosVali, validexistNameLoading } = this.state;
        const { formData } = this.state;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        return (
            <div>
                <Helmet
                    title="Add a Tardiness Rule"
                    meta={ [
                        { name: 'description', content: 'Description of Add a Tardiness Rule' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="Discard Changes"
                    body={
                        <p>
                           Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?
                        </p>
                    }
                    buttons={ [
                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.discardModal.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/company-settings/schedule-settings/tardiness-rules', true );
                                this.discardModal.toggle();
                            }
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Header>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/schedule-settings/tardiness-rules', true );
                                } }
                            >
                                <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Tardiness Rules</span>
                            </A>
                        </Container>
                    </div>
                </Header>
                <PageWrapper>
                    <Sidebar
                        items={ sidebarLinks }
                    />
                    <div className="content">
                        <div className="contentData">
                            <H1 noBottomMargin>Add a Tardiness Rule</H1>
                        </div>
                        <div>
                            <div className="row">
                                <div className={ validAddName === '' || validexistName ? 'col-xs-4 input_pay' : 'col-xs-4 add_tardiness' }>
                                    <Input
                                        id="tardiness_Rule_Name"
                                        label="* Tardiness Rule Name"
                                        type="text"
                                        value={ name }
                                        onChange={ ( value ) => {
                                            this.setState({
                                                formData: { ...formData, name: value },
                                                validAddName: value,
                                                validexistName: false });
                                        } }
                                    />
                                    {validAddName === '' ? <p>This field is required</p> : ''}
                                    { validexistName ? <p>Record name already exists</p> : ''}
                                </div>
                            </div>
                            <div className="row">
                                <div className={ validMinutesTardy === '' || ( isNaN( validMinutesTardy ) || gracePosVali ) ? 'col-xs-4 input_pay pr-0' : 'col-xs-4 add_tardiness pr-0' }>
                                    <Input
                                        id="grace_Period"
                                        label="* Grace Period"
                                        type="text"
                                        value={ minutesTardy }
                                        onChange={ ( value ) => this.setState({ formData: { ...formData, minutesTardy: value }, validMinutesTardy: value, gracePosVali: this.handleInteger( value ) }) }
                                    />
                                    {validMinutesTardy === '' ? <p>This field is required</p> : ( isNaN( validMinutesTardy ) || gracePosVali ) ? <p>Please input numeric value only</p> : ''}
                                </div>
                                <div className={ ( isNaN( validminutesToDeduct ) || deductPosVali ) ? 'col-xs-4 input_pay' : 'col-xs-4 add_tardiness' }>
                                    <Input
                                        id="start_Deducting"
                                        label=" Start Deducting after ______ Minutes"
                                        type="text"
                                        value={ minutesToDeduct }
                                        onChange={ ( value ) => { this.setState({ formData: { ...formData, minutesToDeduct: value }, validminutesToDeduct: value, deductPosVali: this.handleInteger( value ) }); } }
                                    />
                                    {( isNaN( validminutesToDeduct ) || deductPosVali ) ? <p>Please input numeric value only</p> : ''}
                                </div>
                            </div>
                            <div className="row">
                                <div className="affected_Employee add_tardiness">
                                    <MultiSelect
                                        id="affected_Employee/s"
                                        async
                                        label="Affected Employee/s"
                                        loadOptions={ this.gettardinessRules }
                                        placeholder="Enter location,department name,position,employee name"
                                        value={ affectedEmployees }
                                        onChange={ ( value ) => { this.setState({ formData: { ...formData, affectedEmployees: value }}); } }
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </PageWrapper>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                label={ ( loading || validexistNameLoading ) ? <Loader /> : 'Submit' }
                                type="action"
                                size="large"
                                onClick={ () => this.submitAddForm() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    AddTardinessRules: makeSelectAddTardinessRules(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        TardinessRulesAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddTardinessRules );
