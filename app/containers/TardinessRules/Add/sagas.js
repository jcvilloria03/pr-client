import { get } from 'lodash';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery } from 'redux-saga';
import { call, cancel, put, take } from 'redux-saga/effects';
import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { GET_TARDINESS_RULES, LOADING, NOTIFICATION, NOTIFICATION_SAGA, SET_TARDINESS_RULES } from './constants';

/**
 * Submit form
 * @param payload
 */
export function* getaddTardinessRules({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const data = {
            company_id: companyId,
            ...payload
        };
        const addData = yield call( Fetch, '/tardiness_rule', { method: 'POST', data });
        yield put({ type: SET_TARDINESS_RULES, payload: addData });
        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully added',
            show: true,
            type: 'success'
        });
        browserHistory.push( '/company-settings/schedule-settings/tardiness-rules', true );
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message || `Request failed with statuscode ${error.response.status}` : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}
/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });
}
/**
* watchForNotifyUser
*/
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * getsubmitForm
 */
export function* watchForGetSubmitForm() {
    const watcher = yield takeEvery( GET_TARDINESS_RULES, getaddTardinessRules );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
// All sagas to be loaded
export default [
    watchForGetSubmitForm,
    watchForNotifyUser
];
