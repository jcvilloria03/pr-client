/*
 *
 * AddTardinessRules constants
 *
 */
const namespace = 'app/TardinessRules/Add';
export const DEFAULT_ACTION = `${namespace}s/DEFAULT_ACTION`;

export const SET_TARDINESS_RULES = `${namespace}/SET_TARDINESS_RULES`;
export const GET_TARDINESS_RULES = `${namespace}/GET_TARDINESS_RULES`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;

export const LOADING = `${namespace}/LOADING`;

