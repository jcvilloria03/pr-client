import {
    DEFAULT_ACTION, GET_TARDINESS_RULES
} from './constants';

/**
 *
 * AddTardinessRules actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
/**
 * payroll Request Type
 * @param {object} payload
 */
export function getaddTardinessRules( payload ) {
    return {
        type: GET_TARDINESS_RULES,
        payload
    };
}
