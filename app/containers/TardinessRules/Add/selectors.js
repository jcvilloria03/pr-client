import { createSelector } from 'reselect';

/**
 * Direct selector to the addTardinessRules state domain
 */
const selectAddTardinessRulesDomain = () => ( state ) => state.get( 'addTardinessRules' );

/**
 * Default selector used by AddTardinessRules
 */
const makeSelectAddTardinessRules = () => createSelector(
  selectAddTardinessRulesDomain(),
  ( substate ) => substate.toJS()
);

/**
 * Other specific selectors
 */
const makeSelectaddTardinessRules = () => createSelector(
  selectAddTardinessRulesDomain(),
  ( substate ) => substate.get( 'AddTardiness' )
);
const makeSelectLoading = () => createSelector(
  selectAddTardinessRulesDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectNotification = () => createSelector(
  selectAddTardinessRulesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);
export default makeSelectAddTardinessRules;
export {
  selectAddTardinessRulesDomain,
  makeSelectaddTardinessRules,
  makeSelectLoading,
  makeSelectNotification
};
