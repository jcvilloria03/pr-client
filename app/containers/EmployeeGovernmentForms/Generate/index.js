import React from 'react';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import get from 'lodash/get';

import A from 'components/A';
import SnackBar from 'components/SnackBar';
import Stepper from 'components/Stepper';
import { H3 } from 'components/Typography';

import SubHeader from 'containers/SubHeader';
import AgencyView from 'containers/GovernmentForms/templates/agency';
import PeriodSelectionView from 'containers/GovernmentForms/templates/periodPayment';

import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';
import { EMPLOYEE_SUBHEADER_ITEMS } from 'utils/constants';
import { company } from 'utils/CompanyService';
import { isAuthorized } from 'utils/Authorization';

import EmployeeList from './EmployeeList';
import {
    HeaderWrapper,
    NavWrapper,
    GeneratingWrapper
} from './styles';
import { GOVERNMENT_AGENCIES, PAGE_STATUSES } from './constants';
import * as actions from './actions';
import {
    makeSelectPageStatus,
    makeSelectPeriods,
    makeSelectEmployees,
    makeSelectNotification
} from './selectors';

/**
 *
 * Generate Employee Government Forms Component
 *
 */
class GenerateEmployeeGovtForms extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        resetStore: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        pageStatus: React.PropTypes.string,
        getPeriods: React.PropTypes.func,
        getEmployees: React.PropTypes.func,
        periods: React.PropTypes.array,
        employees: React.PropTypes.array,
        generateForms: React.PropTypes.func
    };

    constructor( props ) {
        super( props );

        this.state = {
            step: 0,
            agency: '',
            selection: {
                period: null,
                paymentMethod: '',
                channel: ''
            }
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['create.government_form'], ( authorized ) => {
            if ( !authorized ) {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * Generates config for Stepper
     *
     * @returns {Object[]}
     */
    getSteps() {
        return [
            {
                label: 'Choose a Government Agency',
                onClick: () => this.switchStep( 0, this.state.agency, this.state.selection )
            },
            {
                label: 'Select the Period',
                onClick: () => this.switchStep( 1, this.state.agency, this.state.selection )
            },
            {
                label: 'Select Employees'
            },
            {
                label: 'Generate Form'
            }
        ];
    }

    /**
     * Dispatches request to get employees for the selected period
     *
     * @param {Object} period - Selected period
     */
    getEmployees( period ) {
        const payload = typeof period === 'number' ? {
            month_from: 1,
            year_from: period,
            month_to: 12,
            year_to: period
        } : {
            month_from: period.month,
            year_from: period.year,
            month_to: period.month,
            year_to: period.year
        };

        this.props.getEmployees( payload );
    }

    /**
     * Handler for switching steps
     *
     * @param {number} step
     * @param {string} agency
     * @param {Object} selection
     */
    switchStep = ( step, agency, selection ) => {
        if ( step === 1 && agency !== this.state.agency ) {
            this.props.getPeriods( agency );
        } else if ( step === 2 ) {
            const period = JSON.parse( get( selection, 'period.value', '{}' ) );
            this.getEmployees( period );
        }

        this.setState({
            step,
            agency,
            selection
        });
    }

    /**
     * Dispatches request to generate forms
     *
     * @param {number[]} employees
     */
    generateForms = ( employees ) => {
        const companyId = company.getLastActiveCompanyId();
        const year = JSON.parse( get( this.state.selection, 'period.value', '{}' ) );

        const payload = {
            data: {
                type: 'govt_form_bir_2316',
                attributes: {
                    year,
                    company_id: companyId,
                    employees_id: employees
                }
            }
        };

        this.props.generateForms( payload );
    }

    renderSection() {
        let section;
        const commonProps = {
            loading: this.props.pageStatus === PAGE_STATUSES.LOADING,
            switchStep: this.switchStep,
            agency: this.state.agency,
            selection: this.state.selection
        };

        switch ( this.state.step ) {
            case 1:
                section = (
                    <PeriodSelectionView
                        pageType="employee"
                        periods={ this.props.periods }
                        paymentMethods={ [] }
                        birForms={ GOVERNMENT_AGENCIES.BIR.forms }
                        { ...commonProps }
                    />
                );
                break;
            case 2:
                section = (
                    <EmployeeList
                        employees={ this.props.employees }
                        generateForms={ this.generateForms }
                        { ...commonProps }
                    />
                );
                break;
            default:
                section = (
                    <AgencyView
                        pageType="employee"
                        selectAgency={ ( agency ) => this.switchStep( 1, agency, this.state.selection ) }
                    />
                );
        }

        return section;
    }

    renderGenerating() {
        return (
            <GeneratingWrapper>
                <i className="fa fa-circle-o-notch fa-spin fa-fw spinner" />
                <div className="description">
                    <H3>Generating BIR Employee 2316. Please wait...</H3>
                </div>
            </GeneratingWrapper>
        );
    }

    /**
     * { render the application }
     */
    render() {
        const isGenerating = this.props.pageStatus === PAGE_STATUSES.GENERATING;
        return (
            <div>
                <Helmet
                    title="Employee Government Forms"
                    meta={ [
                        { name: 'description', content: 'Generate Employee Government Forms' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                { !isGenerating && (
                    <NavWrapper>
                        <Container>
                            <A
                                href="/employee-government-forms"
                                onClick={ ( event ) => {
                                    event.preventDefault();
                                    browserHistory.push( '/employee-government-forms', true );
                                } }
                            >
                                &#8592; Back to Employee Government Forms List
                            </A>
                        </Container>
                    </NavWrapper>
                ) }

                { isGenerating
                    ? this.renderGenerating()
                    : (
                        <Container>
                            <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                            <HeaderWrapper>
                                <h4>Employee Government Forms</h4>
                                <p>Generate employee government form such as Employee 2316 for submission to Government Agencies and issuance to Employees.</p>
                                <Stepper steps={ this.getSteps() } activeStep={ this.state.step } />
                            </HeaderWrapper>

                            { this.renderSection() }

                        </Container>
                    )
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    pageStatus: makeSelectPageStatus(),
    periods: makeSelectPeriods(),
    employees: makeSelectEmployees(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators( actions, dispatch )
);

export default connect( mapStateToProps, mapDispatchToProps )( GenerateEmployeeGovtForms );

