/**
 *
 * Generate Employee Government Forms constants
 *
 */
const namespace = 'app/EmployeeGovernmentForms/Generate';
export const NOTIFY_USER = `${namespace}/NOTIFY_USER`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;
export const GET_PERIODS = `${namespace}/GET_PERIODS`;
export const SET_PERIODS = `${namespace}/SET_PERIODS`;
export const SET_YEAR_PERIODS = `${namespace}/SET_YEAR_PERIODS`;
export const GET_EMPLOYEES = `${namespace}/GET_EMPLOYEES`;
export const SET_EMPLOYEES = `${namespace}/SET_EMPLOYEES`;
export const GENERATE_FORMS = `${namespace}/GENERATE_FORMS`;

export const PAGE_STATUSES = {
    LOADING: 'LOADING',
    GENERATING: 'GENERATING',
    READY: 'READY'
};

export const GOVERNMENT_AGENCIES = {
    BIR: {
        name: 'Bureau of Internal Revenue (BIR)',
        forms: [
            {
                value: 'BIR2316',
                label: 'BIR Employee 2316'
            }
        ]
    }
};
