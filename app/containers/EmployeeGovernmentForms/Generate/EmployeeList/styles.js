import styled from 'styled-components';

export const Wrapper = styled.section`
    display: flex;
    justify-content: center;
    margin: 0 auto;
    padding-bottom: 40px;

    h4 {
        font-weight: bold;
    }

    & > div {
        display: flex;
        flex-direction: column;
        position: relative;
        width: 100%;
    }

    .content, .actions {
        display: flex;
    }

    .content {
        flex-direction: column;
        justify-content: space-around;
        background-color: #fff;
        margin-top: 40px;

        .title {
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin-bottom: 20px;

            h4 {
                margin: 0;
                margin-right: 20px;
                font-weight: 600;
                font-size: 20px;
            }
        }
    }

    .actions {
        flex-direction: row;
        justify-content: space-around;
        flex-wrap: wrap;
        padding-top: 30px;

        button {
            padding-left: 40px;
            padding-right: 40px;

            &.previous {
                margin-right: auto;
            }
        }
    }

    .employees-table {
        .empty-table {
            padding: 20px;
        }
    }
`;

export const LoadingWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 120px 0 40px;

    .spinner {
        font-size: 7em;
        align-self: center;
    }

    .description {
        padding-top: 40px;
        text-align: center;

        h3 {
            font-weight: 600;
        }
    }
`;
