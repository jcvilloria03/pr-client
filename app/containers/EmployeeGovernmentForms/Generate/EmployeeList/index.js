import React from 'react';

import { H3, H4 } from 'components/Typography';
import Table from 'components/Table';
import { isAnyRowSelected, getIdsOfSelectedRows } from 'components/Table/helpers';
import Button from 'components/Button';

import { formatDate, formatDeleteLabel } from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import { GOVERNMENT_AGENCIES } from '../constants';

import {
    Wrapper,
    LoadingWrapper
} from './styles';

/**
 * Employee List
 */
class EmployeeList extends React.PureComponent {
    static propTypes = {
        switchStep: React.PropTypes.func,
        loading: React.PropTypes.bool.isRequired,
        agency: React.PropTypes.string,
        employees: React.PropTypes.array,
        selection: React.PropTypes.object,
        generateForms: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        this.state = {
            selection_label: ''
        };
    }

    /**
     * Handler for Previous button onClick
     */
    onClickPrevious = () => {
        this.props.switchStep( 1, this.props.agency, {
            ...this.props.selection,
            employees: []
        });
    }

    /**
     * Handler for Continue button onClick
     */
    onClickContinue = () => {
        const payload = getIdsOfSelectedRows( this.employeeTable );
        this.props.generateForms( payload );
    }

    /**
     * Generates config for table columns
     * @returns {Object[]}
     */
    getTableColumns() {
        return [
            {
                id: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'employee_name',
                header: 'Employee',
                accessor: 'employee_name'
            },
            {
                id: 'department_name',
                header: 'Department',
                accessor: 'department_name'
            },
            {
                id: 'date_hired',
                header: 'Date Hired',
                accessor: 'date_hired',
                render: ({ value }) => formatDate( value, DATE_FORMATS.DISPLAY )
            },
            {
                id: 'date_ended',
                header: 'Date Separated',
                accessor: 'date_ended',
                render: ({ value }) => ( value ? formatDate( value, DATE_FORMATS.DISPLAY ) : '-' )
            }
        ];
    }

    /**
     * Handles table selection changes
     * @param {object} args
     * @param {boolean[]} args.selected
     */
    handleSelectionChanges = ({ selected }) => {
        const selectionLength = selected.filter( ( row ) => row ).length;
        this.setState({ selection_label: formatDeleteLabel( selectionLength ) });
    }

    /**
     * Renders loading screen
     * @returns {element}
     */
    renderLoadingScreen() {
        return (
            <LoadingWrapper>
                <i className="fa fa-circle-o-notch fa-spin fa-fw spinner" />
                <div className="description">
                    <H3>Loading data. Please wait...</H3>
                </div>
            </LoadingWrapper>
        );
    }

    /**
     * Renders component to DOM
     */
    render() {
        const hasSelectedItems = isAnyRowSelected( this.employeeTable );

        return (
            <Wrapper>
                {
                    this.props.loading
                        ? this.renderLoadingScreen()
                        : (
                            <div>
                                <section className="content">
                                    <div className="title">
                                        <H4>{ GOVERNMENT_AGENCIES[ this.props.agency ].name }</H4>
                                        <span>{ hasSelectedItems && this.state.selection_label }</span>
                                    </div>

                                    <Table
                                        className="employees-table"
                                        data={ this.props.employees }
                                        pageSize={ this.props.employees.length }
                                        columns={ this.getTableColumns() }
                                        selectable
                                        onSelectionChange={ this.handleSelectionChanges }
                                        emptyTableComponent={ () => (
                                            <div className="empty-table">
                                                There are no employees to display
                                            </div>
                                        ) }
                                        ref={ ( ref ) => { this.employeeTable = ref; } }
                                    />
                                </section>

                                <section className="actions">
                                    <Button
                                        id="button-previous"
                                        className="previous"
                                        size="large"
                                        type="neutral"
                                        label="Previous"
                                        onClick={ this.onClickPrevious }
                                    />
                                    <Button
                                        id="button-continue"
                                        size="large"
                                        type="action"
                                        label="Continue"
                                        onClick={ this.onClickContinue }
                                        disabled={ !hasSelectedItems }
                                    />
                                </section>
                            </div>
                        )
                }
            </Wrapper>
        );
    }
}

export default EmployeeList;
