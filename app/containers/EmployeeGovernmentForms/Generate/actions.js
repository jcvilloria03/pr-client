import { RESET_STORE } from 'containers/App/constants';

import {
    GET_PERIODS,
    GET_EMPLOYEES,
    SET_NOTIFICATION,
    GENERATE_FORMS
} from './constants';

/**
 * Sends request to fetch government form periods
 * @param {string} payload - Selected agency
 * @returns {Object} action
 */
export function getPeriods( payload ) {
    return {
        type: GET_PERIODS,
        payload
    };
}

/**
 * Sends request to fetch employees
 * @param {Object} payload
 * @param {number} payload.month_from
 * @param {number} payload.year_from
 * @param {number} payload.month_to
 * @param {number} payload.year_to
 * @returns {Object} action
 */
export function getEmployees( payload ) {
    return {
        type: GET_EMPLOYEES,
        payload
    };
}

/**
 * Sends request to generate forms for the
 * selected employees and period
 * @param {object} payload
 * @returns {object} action
 */
export function generateForms( payload ) {
    return {
        type: GENERATE_FORMS,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Resets the state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
