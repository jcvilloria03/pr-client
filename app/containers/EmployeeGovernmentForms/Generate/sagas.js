import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';
import { getCurrentYear, getRange } from 'utils/functions';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    PAGE_STATUSES,
    SET_PAGE_STATUS,
    NOTIFY_USER,
    GET_PERIODS,
    SET_PERIODS,
    SET_YEAR_PERIODS,
    GET_EMPLOYEES,
    SET_EMPLOYEES,
    GENERATE_FORMS
} from './constants';
import { setNotification } from './actions';

/**
 * Sends request to fetch government form periods
 * @param {string} payload - Selected agency
 */
export function* getPeriods({ payload }) {
    yield put({
        type: SET_PAGE_STATUS,
        payload: PAGE_STATUSES.LOADING
    });

    try {
        if ( payload === 'BIR' ) {
            const currentYear = getCurrentYear();
            const years = getRange( currentYear - 5, currentYear );

            yield put({
                type: SET_YEAR_PERIODS,
                payload: years
            });
        } else {
            const companyId = company.getLastActiveCompanyId();

            const { data } = yield call( Fetch, `/philippine/company/${companyId}/government_form_periods` );

            yield put({
                type: SET_PERIODS,
                payload: data
            });
        }
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.READY
        });
    }
}

/**
 * Sends request to fetch employees
 * @param {Object} payload
 * @param {number} payload.month_from
 * @param {number} payload.year_from
 * @param {number} payload.month_to
 * @param {number} payload.year_to
 */
export function* getEmployees({ payload }) {
    yield put({
        type: SET_PAGE_STATUS,
        payload: PAGE_STATUSES.LOADING
    });

    try {
        const companyId = company.getLastActiveCompanyId();

        const { data } = yield call( Fetch, `/company/${companyId}/government_forms/bir_2316/employees`, {
            method: 'GET',
            params: payload
        });

        yield put({
            type: SET_EMPLOYEES,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.READY
        });
    }
}

/**
 * Sends request to generate forms for the
 * selected employees and period
 * @param {object} payload
 */
export function* generateForms({ payload }) {
    yield put({
        type: SET_PAGE_STATUS,
        payload: PAGE_STATUSES.GENERATING
    });

    try {
        yield [
            call( Fetch, '/employee/government_forms/bir_2316/bulk/generate', {
                method: 'POST',
                data: payload
            }),
            call( delay, 3000 )
        ];

        yield call( browserHistory.pushWithParam, '/employee-government-forms', true, { generate_success: true });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.READY
        });
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error.response, 'statusText', 'Error' ),
        message: get( error.response, 'data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, { payload });
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser({ payload }) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
    // yield call( browserHistory.push, '/employee-government-forms', true );
}

/**
 * Watcher for GET_PERIODS
 */
export function* watchForGetPeriods() {
    const watcher = yield takeEvery( GET_PERIODS, getPeriods );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEES
 */
export function* watchForGetEmployees() {
    const watcher = yield takeEvery( GET_EMPLOYEES, getEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GENERATE_FORMS
 */
export function* watchForGenerateForms() {
    const watcher = yield takeEvery( GENERATE_FORMS, generateForms );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFY_USER
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFY_USER, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetPeriods,
    watchForGetEmployees,
    watchForGenerateForms,
    watchForNotifyUser,
    watchForReinitializePage
];
