import { createSelector } from 'reselect';

/**
 * Direct selector to the Generate Employee Government Forms state domain
 */
const makeSelectPageDomain = () => (
    ( state ) => ( state.get( 'employeeGovernmentFormsGenerate' ) )
);

const makeSelectPageStatus = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'page_status' )
);

const makeSelectPeriods = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'periods' ).toJS()
);

const makeSelectEmployees = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'employees' ).toJS()
);

const makeSelectNotification = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'notification' ).toJS()
);

export {
    makeSelectPageStatus,
    makeSelectPeriods,
    makeSelectEmployees,
    makeSelectNotification
};
