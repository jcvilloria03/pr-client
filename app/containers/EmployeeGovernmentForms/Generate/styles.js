import styled from 'styled-components';

export const HeaderWrapper = styled.header`
    text-align: center;

    & > div {
        text-align: left;
    }

    h4 {
        font-weight: 600;
    }
`;

export const NavWrapper = styled.nav`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

export const GeneratingWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 90vh;
    padding-top: 76px;

    .spinner {
        font-size: 7em;
        align-self: center;
    }

    .description {
        padding-top: 40px;
        text-align: center;

        h3 {
            font-weight: 600;
        }
    }
`;
