import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';

import { formatFullName } from 'utils/functions';

import {
    SET_PAGE_STATUS,
    SET_NOTIFICATION,
    PAGE_STATUSES,
    SET_PERIODS,
    SET_YEAR_PERIODS,
    SET_EMPLOYEES
} from './constants';

const initialState = fromJS({
    page_status: PAGE_STATUSES.LOADING,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    periods: [],
    employees: []
});

/**
 * Prepares government form periods for Select component
 *
 * @param {object[]} periods - Government form periods
 * @returns {object[]}
 */
function preparePeriodsData( periods ) {
    return periods.map( ({ month, year, label }) => ({
        value: JSON.stringify({ month, year }),
        label
    }) );
}

/**
 * Prepares government form periods for Select component
 *
 * @param {number[]} years - Years range
 * @returns {object[]}
 */
function prepareYearPeriodsData( years ) {
    return years
        .reverse()
        .map( ( year ) => ({
            label: `${year}`,
            value: `${year}`
        }) );
}

/**
 * Prepares Employee list for table
 *
 * @param {Object[]} data - Employees list
 * @returns {Object[]}
 */
function prepareEmployeesData( data ) {
    return data.map( ({
        employee_id: id,
        first_name,
        middle_name,
        last_name,
        department_name,
        date_hired,
        date_ended
    }) => ({
        id,
        employee_name: formatFullName({ first_name, middle_name, last_name }),
        department_name,
        date_hired,
        date_ended
    }) );
}

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case SET_PAGE_STATUS:
            return state.set( 'page_status', action.payload );
        case SET_PERIODS:
            return state.set( 'periods', fromJS( preparePeriodsData( action.payload ) ) );
        case SET_YEAR_PERIODS:
            return state.set( 'periods', fromJS( prepareYearPeriodsData( action.payload ) ) );
        case SET_EMPLOYEES:
            return state.set( 'employees', fromJS( prepareEmployeesData( action.payload ) ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
};
