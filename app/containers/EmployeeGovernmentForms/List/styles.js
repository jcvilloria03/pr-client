import styled, { css } from 'styled-components';

const commonFlexCenterStyle = css`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

export const PageWrapper = styled.div`
    .content {
        margin-top: 40px;

        .heading {
            ${commonFlexCenterStyle}
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
            }

            div {
                text-align: center;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;

            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 600;
                font-size: 20px;
            }
        }
    }

    .search-wrapper {
        flex-grow: 1;

        .search {
            width: 300px;
            border: 1px solid #333;
            border-radius: 30px;

            input {
                border: none;
            }
        }

        p {
            display: none;
        }

        .input-group,
        .form-control {
            background-color: transparent;
        }

        .input-group-addon {
            background-color: transparent;
            border: none;
        }

        .isvg {
            display: inline-block;
            width: 1rem;
        }
    }

    .filter-icon > svg {
        height: 10px;
    }

    .rt-table {
        .empty-table {
            padding: 20px;
        }
    }
`;

export const LoadingStyles = styled.div`
    ${commonFlexCenterStyle}
    min-height: 200px;
    padding: 140px 0;
`;

export const MessageWrapperStyles = styled.div`
    ${commonFlexCenterStyle}
    min-height: 200px;
    padding-top: 90px;
`;
