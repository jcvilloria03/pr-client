import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import moment from 'moment/moment';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { DATE_FORMATS } from 'utils/constants';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    INITIALIZE_DATA,
    SET_PAGE_STATUS,
    SET_FILTER_DATA,
    GET_EMPLOYEE_GOVERNMENT_FORMS,
    SET_EMPLOYEE_GOVERNMENT_FORMS,
    SET_EMPLOYEE_GOVERNMENT_FORMS_TABLE_LOADING,
    REGENERATE_FORMS,
    DOWNLOAD_FORMS,
    NOTIFY_USER,
    PAGE_STATUSES,
    SET_DOWNLOAD_URL
} from './constants';
import { setNotification } from './actions';

/**
 * Initialize data
 */
export function* initializeData() {
    try {
        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.LOADING
        });

        const companyId = company.getLastActiveCompanyId();

        const [ locations, departments, payrollGroups, employees ] = yield [
            call( Fetch, `/philippine/company/${companyId}/locations` ),
            call( Fetch, `/company/${companyId}/departments` ),
            call( Fetch, `/philippine/company/${companyId}/payroll_groups` ),
            call( Fetch, `/company/${companyId}/employees` ),
            call( getEmployeeGovernmentForms, {
                payload: {
                    coverage_start_date: moment().startOf( 'year' ).format( DATE_FORMATS.API ),
                    coverage_end_date: moment().endOf( 'year' ).format( DATE_FORMATS.API ),
                    page: 1,
                    limit: 10
                }
            })
        ];

        const filterData = {
            locations: locations.data,
            departments: departments.data,
            payroll_groups: payrollGroups.data,
            employees: employees.data
        };

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.READY
        });
    }
}

/**
 * Sends request to fetch employee government forms
 * @param {Object} payload
 * @param {string} payload.coverage_start_date
 * @param {string} payload.coverage_end_date
 * @param {number} payload.page
 * @param {number} payload.limit
 * @param {number} [payload.employee_id]
 * @param {number} [payload.payroll_group_id]
 * @param {number} [payload.department_id]
 * @param {number} [payload.location_id]
 */
export function* getEmployeeGovernmentForms({ payload }) {
    yield put({
        type: SET_EMPLOYEE_GOVERNMENT_FORMS_TABLE_LOADING,
        payload: true
    });

    try {
        const companyId = company.getLastActiveCompanyId();
        const forms = yield call( Fetch, `/company/${companyId}/government_forms/bir_2316`, {
            method: 'GET',
            params: payload
        });

        yield put({
            type: SET_EMPLOYEE_GOVERNMENT_FORMS,
            payload: forms
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_EMPLOYEE_GOVERNMENT_FORMS_TABLE_LOADING,
            payload: false
        });
    }
}

/**
 * Sends request to regenerate selected employee government forms
 * @param {number[]} ids - Employee Government Form IDs
 */
export function* regenerateForms({ payload }) {
    yield put({
        type: SET_PAGE_STATUS,
        payload: PAGE_STATUSES.REGENERATING
    });

    try {
        const companyId = company.getLastActiveCompanyId();
        yield call( Fetch, `/company/${companyId}/government_forms/bir_2316/regenerate`, {
            method: 'POST',
            data: {
                data: payload
            }
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.READY
        });
    }
}

/**
 * Sends request to download selected employee government forms
 * @param {number[]} ids - Employee Government Form IDs
 */
export function* downloadForms({ payload }) {
    yield put({
        type: SET_PAGE_STATUS,
        payload: PAGE_STATUSES.DOWNLOADING
    });

    try {
        const companyId = company.getLastActiveCompanyId();
        const { data } = yield call( Fetch, `/company/${companyId}/government_forms/bir_2316/download`, {
            method: 'POST',
            data: {
                data: {
                    ids: payload
                }
            }
        });

        yield put({
            type: SET_DOWNLOAD_URL,
            payload: data.uri
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.READY
        });
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error.response, 'statusText', 'Error' ),
        message: get( error.response, 'data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, { payload });
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser({ payload }) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
    yield call( initializeData );
}

/**
 * Watcher for INITIALIZE_DATA
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_GOVERNMENT_FORMS
 */
export function* watchForGetEmployeeGovernmentForms() {
    const watcher = yield takeEvery( GET_EMPLOYEE_GOVERNMENT_FORMS, getEmployeeGovernmentForms );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REGENERATE_FORMS
 */
export function* watchForRegenerateForms() {
    const watcher = yield takeEvery( REGENERATE_FORMS, regenerateForms );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DOWNLOAD_FORMS
 */
export function* watchForDownloadForms() {
    const watcher = yield takeEvery( DOWNLOAD_FORMS, downloadForms );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFY_USER
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFY_USER, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForGetEmployeeGovernmentForms,
    watchForRegenerateForms,
    watchForDownloadForms,
    watchForNotifyUser,
    watchForReinitializePage
];
