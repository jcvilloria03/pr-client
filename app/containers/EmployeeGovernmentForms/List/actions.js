import { RESET_STORE } from 'containers/App/constants';

import {
    INITIALIZE_DATA,
    NOTIFY_USER,
    SET_NOTIFICATION,
    GET_EMPLOYEE_GOVERNMENT_FORMS,
    REGENERATE_FORMS,
    DOWNLOAD_FORMS
} from './constants';

/**
 * Initialize data
 */
export function initializeData() {
    return {
        type: INITIALIZE_DATA
    };
}

/**
 * Sends request to fetch employee government forms
 * @param {Object} payload
 * @param {string} payload.coverage_start_date
 * @param {string} payload.coverage_end_date
 * @param {number} payload.page
 * @param {number} payload.limit
 * @param {number} [payload.employee_id]
 * @param {number} [payload.payroll_group_id]
 * @param {number} [payload.department_id]
 * @param {number} [payload.location_id]
 * @returns {Object} action
 */
export function getEmployeeGovernmentForms( payload ) {
    return {
        type: GET_EMPLOYEE_GOVERNMENT_FORMS,
        payload
    };
}

/**
 * Sends request to regenerate selected employee government forms
 * @param {number[]} ids - Employee Government Form IDs
 * @returns {Object} action
 */
export function regenerateForms( ids ) {
    return {
        type: REGENERATE_FORMS,
        payload: ids
    };
}

/**
 * Sends request to download selected employee government forms
 * @param {number[]} ids - Employee Government Form IDs
 * @returns {Object} action
 */
export function downloadForms( ids ) {
    return {
        type: DOWNLOAD_FORMS,
        payload: ids
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Dispatches request to show notification in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function notify( payload ) {
    return {
        type: NOTIFY_USER,
        payload
    };
}

/**
 * Resets the state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
