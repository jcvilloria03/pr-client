/**
 *
 * Employee Government Forms List constants
 *
 */
const namespace = 'app/EmployeeGovernmentForms/List';
export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const NOTIFY_USER = `${namespace}/NOTIFY_USER`;

export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;
export const SET_FILTER_DATA = `${namespace}/SET_FILTER_DATA`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const GET_EMPLOYEE_GOVERNMENT_FORMS = `${namespace}/GET_EMPLOYEE_GOVERNMENT_FORMS`;
export const SET_EMPLOYEE_GOVERNMENT_FORMS = `${namespace}/SET_EMPLOYEE_GOVERNMENT_FORMS`;
export const SET_EMPLOYEE_GOVERNMENT_FORMS_TABLE_LOADING = `${namespace}/SET_EMPLOYEE_GOVERNMENT_FORMS_TABLE_LOADING`;
export const REGENERATE_FORMS = `${namespace}/REGENERATE_FORMS`;
export const DOWNLOAD_FORMS = `${namespace}/DOWNLOAD_FORMS`;
export const SET_DOWNLOAD_URL = `${namespace}/SET_DOWNLOAD_URL`;

export const PAGE_STATUSES = {
    LOADING: 'LOADING',
    REGENERATING: 'REGENERATING',
    DOWNLOADING: 'DOWNLOADING',
    READY: 'READY'
};
