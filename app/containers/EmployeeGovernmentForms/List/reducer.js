import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_PAGE_STATUS,
    SET_FILTER_DATA,
    SET_EMPLOYEE_GOVERNMENT_FORMS,
    SET_EMPLOYEE_GOVERNMENT_FORMS_TABLE_LOADING,
    SET_NOTIFICATION,
    PAGE_STATUSES,
    SET_DOWNLOAD_URL
} from './constants';

const initialState = fromJS({
    page_status: PAGE_STATUSES.LOADING,
    employee_government_forms_table_loading: false,
    filter_data: {},
    employee_government_forms: [],
    total_forms: 0,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    download_url: ''
});

/**
 * Prepares Employee Government Forms list for table
 *
 * @param {Object[]} data - Employee Government Forms list
 * @returns {Object[]}
 */
function prepareEmployeeGovtFormsData( data ) {
    return data.map( ({ id, type, attributes }) => ({
        id,
        type,
        ...attributes
    }) );
}

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case SET_PAGE_STATUS:
            return state.set( 'page_status', action.payload );
        case SET_EMPLOYEE_GOVERNMENT_FORMS_TABLE_LOADING:
            return state.set( 'employee_government_forms_table_loading', action.payload );
        case SET_FILTER_DATA:
            return state.set( 'filter_data', fromJS( action.payload ) );
        case SET_EMPLOYEE_GOVERNMENT_FORMS:
            return state
                .set( 'employee_government_forms', fromJS( prepareEmployeeGovtFormsData( action.payload.data ) ) )
                .set( 'total_forms', action.payload.pagination.total );
        case SET_DOWNLOAD_URL:
            return state.set( 'download_url', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
};
