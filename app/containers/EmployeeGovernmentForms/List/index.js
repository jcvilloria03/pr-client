import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment/moment';
import get from 'lodash/get';

import SubHeader from 'containers/SubHeader';

import Icon from 'components/Icon';
import Input from 'components/Input';
import AsyncTable from 'components/AsyncTable';
import { isAnyRowSelected, getIdsOfSelectedRows } from 'components/Table/helpers';
import Button from 'components/Button';
import SnackBar from 'components/SnackBar';
import { H2, H3, H5 } from 'components/Typography';
import SalDropdown from 'components/SalDropdown';
import Loader from 'components/Loader';
import A from 'components/A';

import { browserHistory } from 'utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from 'utils/constants';
import { formatDeleteLabel } from 'utils/functions';
import { subscriptionService } from 'utils/SubscriptionService';
import { isAuthorized } from 'utils/Authorization';

import Filter from '../Filter';

import {
    PageWrapper,
    LoadingStyles
} from './styles';

import {
    makeSelectPageStatus,
    makeSelectEmployeeGovtFormsTableLoading,
    makeSelectEmployeeGovernmentForms,
    makeSelectTotalForms,
    makeSelectNotification,
    makeSelectFilterData,
    makeSelectDownloadUrl
} from './selectors';

import * as actions from './actions';
import { PAGE_STATUSES } from './constants';

/**
 * Employees' Government Forms
 */
class EmployeeGovernmentForms extends React.PureComponent {
    static propTypes = {
        location: React.PropTypes.object,
        notify: React.PropTypes.func,
        pageStatus: React.PropTypes.string,
        employeeGovtFormsTableLoading: React.PropTypes.bool,
        filterData: React.PropTypes.object,
        employeeGovernmentForms: React.PropTypes.array,
        totalForms: React.PropTypes.number,
        resetStore: React.PropTypes.func,
        initializeData: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        getEmployeeGovernmentForms: React.PropTypes.func,
        regenerateForms: React.PropTypes.func,
        downloadForms: React.PropTypes.func,
        downloadUrl: React.PropTypes.string
    }

    constructor( props ) {
        super( props );

        this.state = {
            permissions: {
                create: false,
                edit: false
            },
            label: 'Showing 0-0 of 0 entries',
            selection_label: '',
            displayed_data: [],
            show_filter: true,
            has_filters_applied: false,
            coverage_start_date_filter: moment().startOf( 'year' ).format( DATE_FORMATS.API ),
            coverage_end_date_filter: moment().endOf( 'year' ).format( DATE_FORMATS.API ),
            page: 1,
            page_size: 10,
            pages: 0
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToAnyProduct( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
            return;
        }

        isAuthorized([
            'view.government_form',
            'create.government_form',
            'edit.government_form'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.government_form' ];

            if ( authorized ) {
                this.setState({
                    permissions: {
                        create: authorization[ 'create.government_form' ],
                        edit: authorization[ 'edit.government_form' ]
                    }
                });
                this.props.initializeData();
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.checkForSuccessfulFormGenerate();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.employeeGovernmentForms !== this.props.employeeGovernmentForms
            && this.setState({
                displayed_data: nextProps.employeeGovernmentForms,
                pages: Math.ceil( nextProps.totalForms / this.state.page_size )
            });

        nextProps.downloadUrl !== this.props.downloadUrl
            && nextProps.downloadUrl
            && this.downloadFile( nextProps.downloadUrl );
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * Generates config for table columns
     * @returns {Object[]}
     */
    getTableColumns() {
        return [
            {
                id: 'employee_full_name',
                header: 'Employee',
                accessor: 'employee_full_name'
            },
            {
                id: 'agency',
                header: 'Government Agency',
                accessor: 'agency'
            },
            {
                id: 'form_name',
                header: 'Government Form',
                accessor: 'form_name'
            },
            {
                id: 'payroll_period',
                header: 'Payroll Period',
                accessor: 'payroll_period'
            },
            {
                header: ' ',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => this.state.permissions.edit && (
                    <Button
                        label={ <span>View</span> }
                        type="grey"
                        size="small"
                        to={ [ `/employee/${row.employee_id}/government-forms/BIR/2316/${row.id}`, true ] }
                    />
                )
            }
        ];
    }

    /**
     * Generates config for dropdown items
     * @returns {Object[]}
     */
    getDropdownItems() {
        return [
            {
                children: this.props.pageStatus === PAGE_STATUSES.REGENERATING
                    ? <Loader />
                    : <div>Regenerate Employee 2316</div>,
                onClick: this.regenerateSelectedForms,
                disabled: this.state.permissions.edit
            },
            {
                children: this.props.pageStatus === PAGE_STATUSES.DOWNLOADING
                    ? <Loader />
                    : <div>Download Employee 2316</div>,
                onClick: this.downloadSelectedForms
            }
        ];
    }

    /**
     * Dispatches request to fetch government forms
     * @param {object} [filters]
     */
    getEmployeeGovernmentForms = ( filters = this.filter.state ) => {
        const {
            page,
            page_size: pageSize
        } = this.state;

        const appliedFilters = Object.assign({}, filters );
        delete appliedFilters.is_expanded;

        this.props.getEmployeeGovernmentForms({
            ...appliedFilters,
            page,
            limit: pageSize
        });
    }

    /**
    * Formats the pagination label
    *
    * @returns {string}
    */
    formatPaginationLabel() {
        const {
            page,
            page_size: pageSize
        } = this.state;
        const { totalForms } = this.props;

        const pageCeiling = page * pageSize;
        const shownRange = `${totalForms ? ( pageCeiling + 1 ) - pageSize : 0} - ${pageCeiling > totalForms ? totalForms : pageCeiling}`;

        return `Showing ${shownRange} of ${totalForms} entr${totalForms > 1 || totalForms === 0 ? 'ies' : 'y'}`;
    }

    /**
     * Download file to browser
     *
     * @param {string} url
     */
    downloadFile( url ) {
        const linkElement = document.getElementById( 'downloadLink' );
        linkElement.target = '_blank';
        linkElement.download = '';
        linkElement.href = url;
        linkElement.click();
        linkElement.href = '';
    }

    checkForSuccessfulFormGenerate() {
        const showSuccess = get( this.props.location, 'state.generate_success', false );
        showSuccess && this.props.notify({
            title: 'Success',
            message: 'Employee government forms successfully generated',
            show: true,
            type: 'success'
        });
    }

    /**
     * Handles search input
     * @param {string} [term = ''] - Search term
     */
    handleSearch = ( term = '' ) => {
        let dataToDisplay = this.props.employeeGovernmentForms;

        if ( term ) {
            const regex = new RegExp( term, 'i' );
            dataToDisplay = dataToDisplay.filter( ( row ) => ( regex.test( row.employee_full_name ) ) );
        }

        this.setState({ displayed_data: dataToDisplay }, () => {
            this.handleTableChanges();
        });
    }

    /**
     * Clears search input
     */
    clearSearchInput() {
        this.searchInput && this.searchInput.setState({ value: '' });
    }

    /**
     * Handles table changes
     */
    handleTableChanges = () => {
        this.setState({
            label: this.formatPaginationLabel(),
            selection_label: formatDeleteLabel()
        });
    }

    /**
     * Handles table selection changes
     * @param {object} args
     * @param {boolean[]} args.selected
     */
    handleSelectionChanges = ({ selected }) => {
        const selectionLength = selected.filter( ( row ) => row ).length;
        this.setState({ selection_label: formatDeleteLabel( selectionLength ) });
    }

    /**
     * Handles table page changes
     * @param {number} page
     */
    handlePageChange = ( page ) => (
        this.setState({ page }, this.getEmployeeGovernmentForms )
    )

    /**
     * Handles table page changes
     * @param {number} page
     */
    handlePageSizeChange = ( pageSize ) => (
        this.setState({
            page_size: pageSize,
            page: 1
        }, this.getEmployeeGovernmentForms )
    )

    /**
     * Checks if a filter is applied
     * @param {object} filters
     * @returns {boolean}
     */
    hasFiltersApplied( filters ) {
        return [ 'employee_id', 'payroll_group_id', 'department_id', 'location_id' ].some( ( filter ) => Boolean( filters[ filter ]) );
    }

    /**
     * Handles applying of filters
     * @param {object} filters
     */
    applyFilters = ( filters ) => {
        this.clearSearchInput();
        this.getEmployeeGovernmentForms( filters );
        this.setState({ has_filters_applied: this.hasFiltersApplied( filters ) });
    }

    /**
     * Resets applied filters
     */
    resetFilters = () => {
        this.setState({
            show_filter: false,
            has_filters_applied: false
        });
    }

    /**
     * Toggles filter show state
     */
    toggleFilter = () => {
        this.setState( ( state ) => ({
            show_filter: !state.show_filter
        }) );
    }

    /**
     * Handles dispatching of request to regenerate selected forms
     */
    regenerateSelectedForms = () => {
        const payload = getIdsOfSelectedRows( this.employeeGovernmentFormsTable );
        this.props.regenerateForms( payload );
    }

    /**
     * Handles dispatching of request to download selected forms
     */
    downloadSelectedForms = () => {
        const payload = getIdsOfSelectedRows( this.employeeGovernmentFormsTable );
        this.props.downloadForms( payload );
    }

    render() {
        const isLoading = this.props.pageStatus === PAGE_STATUSES.LOADING;
        const hasSelectedItems = isAnyRowSelected( this.employeeGovernmentFormsTable );

        return (
            <div>
                <Helmet
                    title="Employee Government Forms"
                    meta={ [
                        { name: 'description', content: 'Employee Government Forms List' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <Container>
                        { isLoading ? (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading Employee Government Forms.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        ) : (
                            <div className="content">
                                <div className="heading">
                                    <H3>Employee Government Forms</H3>
                                    <p>Generate employee government form such as Employee 2316 for submission to Government Agencies and issuance to Employees.</p>
                                    { this.state.permissions.create && (
                                        <div>
                                            <Button
                                                id="button-generate-employee-govt-form"
                                                label="Generate Employee Government Form"
                                                size="large"
                                                type="action"
                                                to={ [ '/employee-government-forms/generate', true ] }
                                            />
                                        </div>
                                    ) }
                                </div>
                                <div className="title">
                                    <H5>Employee Government Forms List</H5>
                                    <div className="search-wrapper">
                                        <Input
                                            id="search"
                                            className="search"
                                            onChange={ ( value ) => this.handleSearch( value.toLowerCase() ) }
                                            addon={ {
                                                content: <Icon name="search" />,
                                                placement: 'right'
                                            } }
                                            ref={ ( ref ) => { this.searchInput = ref; } }
                                        />
                                    </div>
                                    <span>
                                        <div>
                                            { hasSelectedItems ? this.state.selection_label : this.state.label }
                                            &nbsp;
                                            <Button
                                                id="button-filter"
                                                label={ <span><Icon name="filter" className="filter-icon" /> Filter</span> }
                                                type={ this.state.has_filters_applied ? 'primary' : 'neutral' }
                                                onClick={ this.toggleFilter }
                                            />
                                        </div>
                                    </span>
                                    { hasSelectedItems && (
                                        <span className="sl-u-gap-left--sm">
                                            <SalDropdown dropdownItems={ this.getDropdownItems() } />
                                        </span>
                                    ) }
                                </div>
                                <div style={ { display: this.state.show_filter ? 'block' : 'none' } }>
                                    <Filter
                                        filterData={ this.props.filterData }
                                        onCancel={ this.resetFilters }
                                        onApply={ this.applyFilters }
                                        loading={ this.props.employeeGovtFormsTableLoading }
                                        ref={ ( ref ) => { this.filter = ref; } }
                                    />
                                </div>
                                <AsyncTable
                                    loading={ this.props.employeeGovtFormsTableLoading }
                                    data={ this.state.displayed_data }
                                    columns={ this.getTableColumns() }
                                    showFilters={ false }
                                    pagination
                                    selectable
                                    pages={ this.state.pages }
                                    onDataChange={ this.handleTableChanges }
                                    onSelectionChange={ this.handleSelectionChanges }
                                    pageSize={ this.state.page_size }
                                    onPageSizeChange={ this.handlePageSizeChange }
                                    onPageChange={ this.handlePageChange }
                                    emptyTableComponent={ () => (
                                        <div className="empty-table">
                                            There are no employee government forms available to display
                                        </div>
                                    ) }
                                    ref={ ( ref ) => { this.employeeGovernmentFormsTable = ref; } }
                                />
                            </div>
                        ) }
                        <A download id="downloadLink" />
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    pageStatus: makeSelectPageStatus(),
    employeeGovtFormsTableLoading: makeSelectEmployeeGovtFormsTableLoading(),
    filterData: makeSelectFilterData(),
    employeeGovernmentForms: makeSelectEmployeeGovernmentForms(),
    totalForms: makeSelectTotalForms(),
    downloadUrl: makeSelectDownloadUrl(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators( actions, dispatch )
);

export default connect( mapStateToProps, mapDispatchToProps )( EmployeeGovernmentForms );
