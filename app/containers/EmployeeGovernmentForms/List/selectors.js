import { createSelector } from 'reselect';

/**
 * Direct selector to the Employee Government Forms List state domain
 */
const makeSelectPageDomain = () => (
    ( state ) => ( state.get( 'employeeGovernmentFormsList' ) )
);

const makeSelectPageStatus = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'page_status' )
);

const makeSelectEmployeeGovtFormsTableLoading = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'employee_government_forms_table_loading' )
);

const makeSelectFilterData = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'filter_data' ).toJS()
);

const makeSelectEmployeeGovernmentForms = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'employee_government_forms' ).toJS()
);

const makeSelectTotalForms = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'total_forms' )
);

const makeSelectDownloadUrl = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'download_url' )
);

const makeSelectNotification = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'notification' ).toJS()
);

export {
    makeSelectPageStatus,
    makeSelectEmployeeGovtFormsTableLoading,
    makeSelectFilterData,
    makeSelectEmployeeGovernmentForms,
    makeSelectTotalForms,
    makeSelectDownloadUrl,
    makeSelectNotification
};
