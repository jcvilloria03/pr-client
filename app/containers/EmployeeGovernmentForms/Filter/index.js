import React from 'react';
import moment from 'moment/moment';

import Button from 'components/Button';
import Select from 'components/Select';
import DatePicker from 'components/DatePicker';
import Loader from 'components/Loader';

import { formatDate } from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import { FilterWrapper } from './styles';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.PureComponent {
    static propTypes = {
        filterData: React.PropTypes.shape({
            employees: React.PropTypes.array,
            payroll_groups: React.PropTypes.array,
            departments: React.PropTypes.array,
            locations: React.PropTypes.array
        }),
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func,
        loading: React.PropTypes.bool
    };

    static initialState = {
        coverage_start_date: moment().startOf( 'year' ).format( DATE_FORMATS.API ),
        coverage_end_date: moment().endOf( 'year' ).format( DATE_FORMATS.API ),
        employee_id: null,
        payroll_group_id: null,
        department_id: null,
        location_id: null,
        is_expanded: false
    };

    constructor( props ) {
        super( props );

        this.state = Filter.initialState;
    }

    onApply = () => {
        this.props.onApply( this.state );
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    getEmployees() {
        return this.formatDataForSelect( this.props.filterData.employees );
    }

    getPayrollGroups() {
        return this.formatDataForSelect( this.props.filterData.payroll_groups );
    }

    getDepartments() {
        return this.formatDataForSelect( this.props.filterData.departments );
    }

    getLocations() {
        return this.formatDataForSelect( this.props.filterData.locations );
    }

    formatDataForSelect( array ) {
        return array.map( ( item ) => ({
            value: item.id,
            label: item.name || item.full_name
        }) );
    }

    toggleExpanded = () => {
        this.setState( ( state ) => ({ is_expanded: !state.is_expanded }) );
    }

    resetFilters = () => {
        this.setState( Filter.initialState, this.onApply );
    }

    handleDateChange = ( key ) => ( value ) => {
        this.setState({ [ key ]: formatDate( value, DATE_FORMATS.API ) });
    }

    handleSelectChange = ( key ) => ( value ) => {
        this.setState({ [ key ]: value ? value.value : null });
    }

    render() {
        return (
            <FilterWrapper>
                <div className="row">
                    <div className="col-xs-3 date-picker">
                        <DatePicker
                            label="Coverage Start Date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            ref={ ( ref ) => { this.coverage_start_date = ref; } }
                            selectedDay={ this.state.coverage_start_date }
                            onChange={ this.handleDateChange( 'coverage_start_date' ) }
                            disabledDays={ this.state.coverage_end_date
                                ? [{ after: new Date( this.state.coverage_end_date ) }]
                                : []
                            }
                        />
                    </div>
                    <div className="col-xs-3 date-picker">
                        <DatePicker
                            label="Coverage End Date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            ref={ ( ref ) => { this.coverage_end_date = ref; } }
                            selectedDay={ this.state.coverage_end_date }
                            onChange={ this.handleDateChange( 'coverage_end_date' ) }
                            disabled={ !this.state.coverage_start_date }
                            disabledDays={ this.state.coverage_start_date
                                ? [{ before: new Date( this.state.coverage_start_date ) }]
                                : []
                            }
                        />
                    </div>
                    { !this.state.is_expanded && (
                        <div className="col-xs-3">
                            <div className="sl-c-filter-actions-no-border">
                                <div className="sl-c-filter-toggle">
                                    <Button
                                        id="button-show-other-filters"
                                        label="Show other filters"
                                        size="large"
                                        type="neutral"
                                        onClick={ this.toggleExpanded }
                                    />
                                </div>
                            </div>
                        </div>
                    ) }
                    { this.state.is_expanded && [
                        <div key="employee" className="col-xs-3">
                            <Select
                                id="employee"
                                label="Employee"
                                placeholder="All Employees"
                                data={ this.getEmployees() }
                                value={ this.state.employee_id }
                                onChange={ this.handleSelectChange( 'employee_id' ) }
                                ref={ ( ref ) => { this.employees = ref; } }
                            />
                        </div>,
                        <div key="payroll-group" className="col-xs-3">
                            <Select
                                id="payroll-group"
                                label="Payroll Group"
                                placeholder="All Payroll Groups"
                                data={ this.getPayrollGroups() }
                                value={ this.state.payroll_group_id }
                                onChange={ this.handleSelectChange( 'payroll_group_id' ) }
                                ref={ ( ref ) => { this.payroll_groups = ref; } }
                            />
                        </div>
                    ] }
                </div>
                { this.state.is_expanded && (
                    <div className="row">
                        <div className="col-xs-3">
                            <Select
                                id="department"
                                label="Department"
                                placeholder="All Departments"
                                data={ this.getDepartments() }
                                value={ this.state.department_id }
                                onChange={ this.handleSelectChange( 'department_id' ) }
                                ref={ ( ref ) => { this.departments = ref; } }
                            />
                        </div>
                        <div className="col-xs-3">
                            <Select
                                id="location"
                                label="Location"
                                placeholder="All Locations"
                                data={ this.getLocations() }
                                value={ this.state.location_id }
                                onChange={ this.handleSelectChange( 'location_id' ) }
                                ref={ ( ref ) => { this.locations = ref; } }
                            />
                        </div>
                        <div className="col-xs-3">
                            <div className="sl-c-filter-actions-no-border">
                                <div className="sl-c-filter-toggle">
                                    <Button
                                        id="button-hide-other-filters"
                                        label="Hide other filters"
                                        size="large"
                                        type="neutral"
                                        onClick={ this.toggleExpanded }
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                ) }
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            id="button-reset-filter"
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            disabled={ this.props.loading }
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            id="button-cancel-filter"
                            label="Cancel"
                            size="large"
                            type="neutral"
                            disabled={ this.props.loading }
                            onClick={ this.onCancel }
                        />
                        <Button
                            id="button-apply-filter"
                            label={ this.props.loading ? <Loader /> : 'Apply' }
                            size="large"
                            type="action"
                            disabled={ !( this.state.coverage_start_date && this.state.coverage_end_date ) || this.props.loading }
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
