import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import get from 'lodash/get';

import SnackBar from 'components/SnackBar';

import SubHeader from 'containers/SubHeader';
import { H2, H3, H5 } from 'components/Typography';
import Input from 'components/Input';
import DatePicker from 'components/DatePicker';
import Button from 'components/Button';
import A from 'components/A';
import Loader from 'components/Loader';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from 'utils/constants';
import {
    stripNonDigit,
    formatTIN,
    formatRDO,
    formatDate,
    formatCurrency
} from 'utils/functions';
import { renderField } from 'utils/form';

import * as actions from './actions';
import {
    makeSelectPageStatus,
    makeSelectData,
    makeSelectNotification,
    makeSelectDownloadUrl,
    makeSelectFormErrors
} from './selectors';

import {
    LoadingWrapper,
    NavWrapper,
    FormWrapper,
    PageHeaderWrapper,
    FormTitleWrapper,
    FormHeaderWrapper,
    SectionWrapper,
    SectionHeadingWrapper,
    LineWrapper,
    InlineDateWrapper,
    InputWrapper,
    SpecifyInputWrapper
} from './styles';

import {
    PAGE_STATUSES,
    FORM_CONFIG,
    BIR_2316_FORM_FIELDS
} from './constants';

/**
 * BIR Form 2316 component
 */
export class BIR2316Form extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        resetStore: React.PropTypes.func,
        pageStatus: React.PropTypes.string,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        initializeData: React.PropTypes.func,
        saveAndDownloadForm: React.PropTypes.func,
        regenerateForm: React.PropTypes.func,
        data: React.PropTypes.object,
        downloadUrl: React.PropTypes.string,
        routeParams: React.PropTypes.object,
        formErrors: React.PropTypes.object
    };

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            is_mwe_exempt: false,
            substituted_filing: false,
            permissions: {
                edit: false
            }
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.government_form',
            'edit.government_form'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.government_form' ];

            if ( authorized ) {
                this.setState({
                    permissions: {
                        edit: authorization[ 'edit.government_form' ]
                    }
                });
                this.props.initializeData( this.props.routeParams );
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    /**
     * Perform actions when component props changes
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.data !== this.props.data
            && this.setState({
                is_mwe_exempt: nextProps.data.is_mwe_exempt,
                substituted_filing: nextProps.data.substituted_filing
            });

        nextProps.downloadUrl !== this.props.downloadUrl
            && nextProps.downloadUrl
            && this.downloadFile( nextProps.downloadUrl );

        nextProps.formErrors !== this.props.formErrors
            && this.setErrors( nextProps.formErrors );
        this.setState({ substituted_filing: true });
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    getSpecificFieldConfig( field ) {
        const value = this.props.data[ field.id ];

        let config = {};
        if ( BIR_2316_FORM_FIELDS.AMOUNTS.includes( field.id ) ) {
            config = {
                onChange: ( newValue ) => this[ field.id ].setState({ value: stripNonDigit( newValue ) }),
                onFocus: () => this[ field.id ].setState( ( state ) => ({ value: stripNonDigit( state.value ) }) ),
                onBlur: () => this[ field.id ].setState( ( state ) => ({ value: formatCurrency( state.value ) }) )
            };
        } else if ( BIR_2316_FORM_FIELDS.DATES.includes( field.id ) ) {
            config = {
                selectedDay: value
            };
        } else if ( BIR_2316_FORM_FIELDS.SWITCHES.includes( field.id ) ) {
            if ( field.id === 'is_mwe_exempt' ) {
                config = {
                    checked: this.state.is_mwe_exempt,
                    onChange: ( checked ) => this.setState({ [ field.id ]: checked })
                };
            }
            if ( field.id === 'substituted_filing' ) {
                config = {
                    checked: this.state.substituted_filing,
                    onChange: ( checked ) => this.setState({ [ field.id ]: checked })
                };
            }
        } else if ( field.type === 'tin' ) {
            config = {
                onChange: ( newValue ) => this[ field.id ].setState({ value: formatTIN( newValue ) })
            };
        } else if ( field.type === 'rdo' ) {
            config = {
                onChange: ( newValue ) => this[ field.id ].setState({ value: formatRDO( newValue ) })
            };
        }

        return config;
    }

    /**
     * Sets field errors
     * @param {object} errors - Error object
     */
    setErrors( errors ) {
        this.clearErrors();

        const fields = Object.keys( errors );

        fields.forEach( ( field ) => {
            const errorMessageKey = BIR_2316_FORM_FIELDS.DATES.includes( field ) ? 'message' : 'errorMessage';

            this[ field ] && this[ field ].setState({
                [ errorMessageKey ]: get( errors, [ field, '0' ], 'Invalid value' ),
                error: true
            });
        });
    }

    /**
     * Clears field errors
     */
    clearErrors() {
        const { INPUTS, DATES, AMOUNTS } = BIR_2316_FORM_FIELDS;
        const inputs = INPUTS.concat( AMOUNTS );

        inputs.forEach( ( field ) =>
            this[ field ] && this[ field ].setState({
                errorMessage: '',
                error: false
            })
        );

        DATES.forEach( ( field ) =>
            this[ field ] && this[ field ].setState({
                message: '',
                error: false
            })
        );
    }

    /**
     * Validates form before saving and downloading
     * @returns {Boolean}
     */
    validateForm() {
        let valid = true;

        const inputs = BIR_2316_FORM_FIELDS.INPUTS.concat( BIR_2316_FORM_FIELDS.AMOUNTS );

        inputs.forEach( ( input ) => {
            if ( this[ input ] && this[ input ]._validate( this[ input ].state.value ) ) {
                valid = false;
            }
        });

        BIR_2316_FORM_FIELDS.DATES.forEach( ( date ) => {
            if ( this[ date ] && this[ date ].checkRequired() ) {
                valid = false;
            }
        });

        return valid;
    }

    regenerateForm = () => {
        const { id, employeeId } = this.props.routeParams;
        const payload = {
            id,
            employeeId
        };
        this.props.regenerateForm( payload );
    }

    saveAndDownloadForm = () => {
        if ( !this.validateForm() ) {
            return;
        }

        const data = {
            current_employer_type: this.current_employer_type.state.value,
            is_mwe_exempt: this.state.is_mwe_exempt,
            substitute_filing: this.state.substituted_filing,
            coverage_start_day: formatDate( this.coverage_start_day.state.selectedDay, 'MM/DD' ) || '',
            coverage_end_day: formatDate( this.coverage_end_day.state.selectedDay, 'MM/DD' ) || '',
            employee_name: this.employee_name.state.value
        };

        BIR_2316_FORM_FIELDS.INPUTS.forEach( ( field ) => {
            if ( this[ field ]) {
                data[ field ] = this[ field ].state.value;
            }
        });

        BIR_2316_FORM_FIELDS.DATES.forEach( ( field ) => {
            if ( this[ field ]) {
                data[ field ] = formatDate( this[ field ].state.selectedDay, DATE_FORMATS.API ) || '';
            }
        });

        BIR_2316_FORM_FIELDS.AMOUNTS.forEach( ( field ) => {
            if ( this[ field ]) {
                data[ field ] = stripNonDigit( this[ field ].state.value );
            }
        });

        const payload = {
            id: this.props.routeParams.id,
            employeeId: this.props.routeParams.employeeId,
            data
        };

        this.props.saveAndDownloadForm( payload );
    }

    /**
     * Download file to browser
     *
     * @param {string} url
     */
    downloadFile( url ) {
        const linkElement = document.getElementById( 'downloadLink' );
        linkElement.target = '_blank';
        linkElement.download = '';
        linkElement.href = url;
        linkElement.click();
        linkElement.href = '';
    }

    /**
     * Renders a loading screen
     *
     * @param {boolean} regenerating - Is regenerating flag
     */
    renderLoadingPage( regenerating = false ) {
        return (
            <LoadingWrapper>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw spinner" />
                    <div className="description">
                        <H3>{ regenerating ? 'Regenerating.' : 'Loading data.' } Please wait...</H3>
                    </div>
                </div>
            </LoadingWrapper>
        );
    }

    /**
     * Renders header section
    */
    renderHeaderSection() {
        return (
            <SectionWrapper>
                <FormTitleWrapper>
                    <H2>Certificate of Compensation</H2>
                    <H2>Payment / Tax Withheld</H2>
                    <H3>(BIR Form No. 2316)</H3>
                </FormTitleWrapper>
            </SectionWrapper>
        );
    }

    renderPeriodSection() {
        const { data } = this.props;

        return (
            <SectionWrapper>
                <LineWrapper>
                    <InputWrapper>
                        <Input
                            id="input-year"
                            className="input-text-center"
                            label={ <span><span className="item-number">1.</span> For the Year</span> }
                            value={ data.year }
                            disabled
                        />
                    </InputWrapper>
                    <InputWrapper className="columns2">
                        <div className="period">
                            <span>
                                <span className="item-number">2.</span> For the Period
                            </span>
                            <div>
                                <InlineDateWrapper horizontal>
                                    <DatePicker
                                        id="date-picker-coverage-start"
                                        label="From (MM/DD)"
                                        selectedDay={ data.coverage_start_day }
                                        dayFormat="MM/DD"
                                        disabled
                                        ref={ ( ref ) => { this.coverage_start_day = ref; } }
                                    />
                                </InlineDateWrapper>
                                <InlineDateWrapper horizontal>
                                    <DatePicker
                                        id="date-picker-coverage-end"
                                        className="horizontal"
                                        label="To (MM/DD)"
                                        selectedDay={ data.coverage_end_day }
                                        dayFormat="MM/DD"
                                        placeholder="Select date"
                                        disabledDays={ [
                                            { before: new Date( data.coverage_start_day ) },
                                            { after: new Date( data.year, 11, 31 ) }
                                        ] }
                                        ref={ ( ref ) => { this.coverage_end_day = ref; } }
                                    />
                                </InlineDateWrapper>
                            </div>
                        </div>
                    </InputWrapper>
                </LineWrapper>
            </SectionWrapper>
        );
    }

    /**
     * Renders field for type `specify-input`
     *
     * @param {object} field - Field config
     */
    renderSpecifyInput( field ) {
        const { id, label } = field;

        const specifyId = `${id}_specify`;
        const amountId = `${id}_amount`;
        const specifyValue = this.props.data[ specifyId ];
        const amountValue = this.props.data[ amountId ];

        return (
            <SpecifyInputWrapper key={ id }>
                <div>
                    <Input
                        id={ specifyId }
                        label={ <span className="item-number">{ label }</span> }
                        value={ specifyValue }
                        ref={ ( ref ) => { this[ specifyId ] = ref; } }
                    />
                </div>
                <div>
                    <Input
                        id={ amountId }
                        className="number"
                        value={ amountValue }
                        onChange={ ( value ) => this[ amountId ].setState({ value: stripNonDigit( value ) }) }
                        onBlur={ () => this[ amountId ].setState( ( state ) => ({ value: formatCurrency( state.value ) }) ) }
                        onFocus={ () => this[ amountId ].setState( ( state ) => ({ value: stripNonDigit( state.value ) }) ) }
                        ref={ ( ref ) => { this[ amountId ] = ref; } }
                    />
                </div>
            </SpecifyInputWrapper>
        );
    }

    renderField = ( field ) => {
        if ( field.field_type === 'specify-input' ) {
            return this.renderSpecifyInput( field );
        }

        const {
            class_name: className,
            item_no: itemNum,
            label,
            ...rest
        } = field;

        let value = this.props.data[ field.id ];

        const specificFieldConfig = this.getSpecificFieldConfig( field );

        if ( BIR_2316_FORM_FIELDS.SWITCHES.includes( field.id ) ) {
            value = this.state[ field.id ];
        }

        const formattedLabel = itemNum
            ? ( <span><span className="item-number">{ itemNum }</span> { label }</span> )
            : label;

        const config = {
            ...rest,
            ...specificFieldConfig,
            label: formattedLabel,
            value,
            ref: ( ref ) => { this[ field.id ] = ref; }
        };

        return (
            <InputWrapper className={ className } key={ field.id }>
                { field.field_type
                    ? renderField( config )
                    : formattedLabel
                }
            </InputWrapper>
        );
    };

    renderFormSection( sectionName ) {
        const section = FORM_CONFIG[ sectionName ];

        return (
            <SectionWrapper>
                <SectionHeadingWrapper>
                    <H5>{ section.TITLE }</H5>
                </SectionHeadingWrapper>
                { section.ROWS.map( ( row, index ) => (
                    <LineWrapper key={ `${sectionName}-${index}` }>
                        { row.map( this.renderField ) }
                    </LineWrapper>
                ) ) }
            </SectionWrapper>
        );
    }

    /**
     * renders component to DOM
     */
    render() {
        const isRegenerating = this.props.pageStatus === PAGE_STATUSES.REGENERATING;
        const renderLoading = isRegenerating || this.props.pageStatus === PAGE_STATUSES.LOADING;
        const isDownloading = this.props.pageStatus === PAGE_STATUSES.DOWNLOADING;

        return (
            <div>
                <Helmet
                    title="Employee Government Forms: BIR 2316"
                    meta={ [
                        { name: 'description', content: 'Employee BIR 2316 Form' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                { !renderLoading && (
                    <NavWrapper>
                        <Container>
                            <A
                                href="/employee-government-forms"
                                onClick={ ( event ) => {
                                    event.preventDefault();
                                    browserHistory.push( '/employee-government-forms', true );
                                } }
                            >
                                &#8592; Back to Employee Government Forms List
                            </A>
                        </Container>
                    </NavWrapper>
                ) }

                { renderLoading
                    ? this.renderLoadingPage( isRegenerating )
                    : (
                        <Container>
                            <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                            <PageHeaderWrapper>
                                <H3>Employee Government Forms</H3>
                                <p>Generate employee government form such as Employee 2316 for submission to Government Agencies and issuance to Employees.</p>
                            </PageHeaderWrapper>

                            <FormHeaderWrapper>
                                <div>
                                    <H3>Bureau of Internal Revenue Form No. 2316</H3>
                                    <p>To accomplish your BIR 2316 Form, follow the steps below:</p>
                                    <ol>
                                        <li>Fill out the fields required below.</li>
                                        <li>Generate and print the BIR Form No. 2316 PDF File in triplicate copies.</li>
                                        <li>Ask the employee to sign all three (3) copies.</li>
                                        <li>Bring the signed copies to the Employer&apos;s BIR RDO branch for acknowledgement stamp.</li>
                                        <li>Issue one (1) copy (with stamp) to employee.</li>
                                    </ol>
                                </div>
                                { this.state.permissions.edit && (
                                    <div className="actions">
                                        <Button
                                            id="button-regenerate-form"
                                            alt
                                            label="Regenerate Form"
                                            type="action"
                                            size="large"
                                            onClick={ this.regenerateForm }
                                            disabled={ isDownloading }
                                        />
                                        <Button
                                            id="button-save-and-download-form"
                                            label={ isDownloading ? <Loader /> : 'Save and Download Form' }
                                            type="action"
                                            size="large"
                                            onClick={ this.saveAndDownloadForm }
                                            disabled={ isDownloading || !this.validateForm() }
                                        />
                                        <A download id="downloadLink" />
                                    </div>
                                ) }
                            </FormHeaderWrapper>

                            <FormWrapper>
                                { this.renderHeaderSection() }
                                { this.renderPeriodSection() }
                                { this.renderFormSection( 'PART_1' ) }
                                { this.renderFormSection( 'PART_2' ) }
                                { this.renderFormSection( 'PART_3' ) }
                                { this.renderFormSection( 'PART_4A' ) }
                                { this.renderFormSection( 'PART_4B' ) }
                                { this.renderFormSection( 'DECLARATION' ) }
                            </FormWrapper>

                        </Container>
                    )
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    pageStatus: makeSelectPageStatus(),
    data: makeSelectData(),
    downloadUrl: makeSelectDownloadUrl(),
    notification: makeSelectNotification(),
    formErrors: makeSelectFormErrors()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( BIR2316Form );
