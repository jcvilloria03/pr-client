import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import get from 'lodash/get';
import has from 'lodash/has';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    PAGE_STATUSES,
    SET_PAGE_STATUS,
    NOTIFY_USER,
    SET_NOTIFICATION,
    INITIALIZE_DATA,
    SET_DATA,
    SAVE_AND_DOWNLOAD_FORM,
    SET_DOWNLOAD_URL,
    REGENERATE_FORM,
    SET_ERRORS
} from './constants';

/**
 * Initialize data
 * @param {number} payload - Government form ID
 */
export function* initializeData({ payload }) {
    yield put({
        type: SET_PAGE_STATUS,
        payload: PAGE_STATUSES.LOADING
    });

    try {
        yield call( getData, payload );
    } catch ( error ) {
        yield call( notifyError, error );
        yield call( browserHistory.push, '/employee-government-forms', true );
    } finally {
        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.READY
        });
    }
}

/**
 * Sends request to fetch government form record details
 *
 * @param {object} payload
 * @param {number} payload.id - Government form ID
 * @param {number} payload.employeeId - Employee ID
 */
export function* getData( payload ) {
    try {
        const { id, employeeId } = payload;
        const { data } = yield call( Fetch, `/employee/${employeeId}/government_forms/bir_2316/${id}` );

        yield put({
            type: SET_DATA,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyError, error );
        yield call( browserHistory.push, '/employee-government-forms', true );
    }
}

/**
 * Save and download form
 *
 * @param {object} payload
 */
export function* saveAndDownloadForm({ payload }) {
    yield put({
        type: SET_PAGE_STATUS,
        payload: PAGE_STATUSES.DOWNLOADING
    });
    yield put({
        type: SET_ERRORS,
        payload: {}
    });

    try {
        const { id, employeeId, data } = payload;
        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, `/employee/${employeeId}/government_forms/bir_2316/${id}`, {
            method: 'PATCH',
            data: {
                data: {
                    attributes: {
                        company_id: companyId,
                        employee_id: employeeId,
                        ...data
                    }
                }
            }
        });

        const { data: { uri }} = yield call( Fetch, `/company/${companyId}/government_forms/bir_2316/download`, {
            method: 'POST',
            data: {
                data: {
                    ids: [id]
                }
            }
        });

        yield put({
            type: SET_DOWNLOAD_URL,
            payload: uri
        });
    } catch ( error ) {
        if ( has( error, 'response.data.errors' ) ) {
            yield put({
                type: SET_ERRORS,
                payload: error.response.data.errors
            });
        }
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.READY
        });
    }
}

/**
 * Regenerate form
 *
 * @param {number} payload.id - Government Form ID
 * @param {number} payload.employeeId - Employee ID
 */
export function* regenerateForm({ payload }) {
    yield put({
        type: SET_PAGE_STATUS,
        payload: PAGE_STATUSES.REGENERATING
    });

    try {
        const companyId = company.getLastActiveCompanyId();
        const { data } = yield call( Fetch, `/company/${companyId}/government_forms/bir_2316/regenerate`, {
            method: 'POST',
            data: {
                data: [payload.id]
            }
        });
        const { id } = data[ 0 ];

        yield put({
            type: SET_DATA,
            payload: data[ 0 ]
        });
        yield call( browserHistory.replace, `/employee/${payload.employeeId}/government-forms/BIR/2316/${id}/`, true );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_PAGE_STATUS,
            payload: PAGE_STATUSES.READY
        });
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error.response, 'statusText', 'Error' ),
        message: get( error.response, 'data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, { payload });
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser({ payload }) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Watcher for INITIALIZE_DATA
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SAVE_AND_DOWNLOAD_FORM
 */
export function* watchForSaveAndDownloadForm() {
    const watcher = yield takeEvery( SAVE_AND_DOWNLOAD_FORM, saveAndDownloadForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REGENERATE_FORM
 */
export function* watchForRegenerateForm() {
    const watcher = yield takeEvery( REGENERATE_FORM, regenerateForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFY_USER
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFY_USER, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForSaveAndDownloadForm,
    watchForRegenerateForm,
    watchForNotifyUser,
    watchForReinitializePage
];
