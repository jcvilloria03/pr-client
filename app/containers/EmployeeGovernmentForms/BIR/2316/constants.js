import { DATE_FORMATS } from 'utils/constants';

/**
 *
 * Employee Government Form BIR 2316 constants
 *
 */
const namespace = 'app/EmployeeGovernmentForms/BIR/2316';
export const NOTIFY_USER = `${namespace}/NOTIFY_USER`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;
export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const SET_DATA = `${namespace}/SET_DATA`;
export const SAVE_AND_DOWNLOAD_FORM = `${namespace}/SAVE_AND_DOWNLOAD_FORM`;
export const SET_DOWNLOAD_URL = `${namespace}/SET_DOWNLOAD_URL`;
export const REGENERATE_FORM = `${namespace}/REGENERATE_FORM`;
export const SET_ERRORS = `${namespace}/SET_ERRORS`;

export const PAGE_STATUSES = {
    LOADING: 'LOADING',
    DOWNLOADING: 'DOWNLOADING',
    REGENERATING: 'REGENERATING',
    READY: 'READY'
};

const OPTIONS = {
    TYPE_OF_EMPLOYER: [
        {
            label: 'Main Employer',
            value: 'main'
        },
        {
            label: 'Secondary Employer',
            value: 'secondary'
        }
    ]
};

export const FORM_CONFIG = {
    PART_1: {
        TITLE: 'Part I: Employee Information',
        ROWS: [
            [
                {
                    field_type: 'input',
                    class_name: 'input-text-center',
                    id: 'employee_tin',
                    item_no: '3.',
                    label: 'Tax Identification Number (TIN)',
                    placeholder: '000-000-000-000',
                    validations: {
                        required: true
                    },
                    type: 'tin'
                },
                {
                    field_type: 'input',
                    class_name: '',
                    id: 'employee_name',
                    item_no: '4.',
                    label: 'Employee\'s Name',
                    placeholder: 'Enter employee\'s name',
                    validations: {
                        required: true
                    }
                },
                {
                    field_type: 'input',
                    class_name: 'input-text-center',
                    id: 'employee_rdo_code',
                    item_no: '5.',
                    label: 'RDO Code',
                    placeholder: 'Enter RDO code',
                    validations: {
                        required: true
                    },
                    type: 'rdo'
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns2',
                    id: 'employee_registered_address',
                    item_no: '6.',
                    label: 'Registered Address',
                    placeholder: 'Enter address',
                    validations: {
                        required: true
                    }
                },
                {
                    field_type: 'input',
                    class_name: 'input-text-center',
                    id: 'employee_registered_zip_code',
                    item_no: '6A.',
                    label: 'ZIP Code',
                    placeholder: 'Enter ZIP code',
                    validations: {
                        required: false
                    },
                    type: 'number'
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns2',
                    id: 'employee_local_home_address',
                    item_no: '6B.',
                    label: 'Local Home Address',
                    placeholder: 'Enter address',
                    validations: {
                        required: false
                    }
                },
                {
                    field_type: 'input',
                    class_name: 'input-text-center',
                    id: 'employee_local_zip_code',
                    item_no: '6C.',
                    label: 'ZIP Code',
                    placeholder: 'Enter ZIP code',
                    validations: {
                        required: false
                    },
                    type: 'number'
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns2',
                    id: 'employee_foreign_address',
                    item_no: '6D.',
                    label: 'Foreign Address',
                    placeholder: 'Enter address',
                    validations: {
                        required: false
                    }
                },
                {
                    field_type: 'date-picker',
                    class_name: '',
                    id: 'birth_date',
                    item_no: '7.',
                    label: 'Date of Birth',
                    placeholder: 'Select date',
                    validations: {
                        required: false
                    },
                    dayFormat: DATE_FORMATS.DISPLAY
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: '',
                    id: 'employee_contact_number',
                    item_no: '8.',
                    label: 'Contact Number',
                    placeholder: 'Enter contact number',
                    validations: {
                        required: false
                    }
                },
                {
                    field_type: 'input',
                    class_name: 'number',
                    id: 'statutory_mwe_per_day',
                    item_no: '9.',
                    label: 'Statutory Minimum Wage rate per day',
                    placeholder: 'Enter rate',
                    validations: {
                        required: false
                    }
                },
                {
                    field_type: 'input',
                    class_name: 'number',
                    id: 'statutory_mwe_per_month',
                    item_no: '10.',
                    label: 'Statutory Minimum Wage rate per month',
                    placeholder: 'Enter rate',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'switch',
                    class_name: 'columns3 switch',
                    id: 'is_mwe_exempt',
                    item_no: '11.',
                    label: 'Minimum Wage Earner (MWE) whose compensation is exempt from withholding tax and not subject to income tax'
                }
            ]
        ]
    },
    PART_2: {
        TITLE: 'Part II: Employer Information (Present)',
        ROWS: [
            [
                {
                    field_type: 'input',
                    class_name: 'input-text-center',
                    id: 'current_employer_tin',
                    item_no: '12.',
                    label: 'Tax Identification Number (TIN)',
                    placeholder: '000-000-000-000',
                    validations: {
                        required: true
                    },
                    type: 'tin'
                },
                {
                    field_type: 'input',
                    class_name: 'columns2',
                    id: 'current_employer_name',
                    item_no: '13.',
                    label: 'Employer\'s Name',
                    placeholder: 'Enter employer\'s name',
                    validations: {
                        required: true
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns2',
                    id: 'current_employer_address',
                    item_no: '14.',
                    label: 'Registered Address',
                    placeholder: 'Enter address',
                    validations: {
                        required: true
                    }
                },
                {
                    field_type: 'input',
                    class_name: 'input-text-center',
                    id: 'current_employer_zip_code',
                    item_no: '14A.',
                    label: 'ZIP Code',
                    placeholder: 'Enter ZIP code',
                    validations: {
                        required: false
                    },
                    type: 'number'
                }
            ],
            [
                {
                    field_type: 'radio-group',
                    class_name: 'columns3',
                    id: 'current_employer_type',
                    item_no: '15.',
                    label: 'Type of Employer',
                    horizontal: true,
                    options: OPTIONS.TYPE_OF_EMPLOYER
                }
            ]
        ]
    },
    PART_3: {
        TITLE: 'Part III: Employer Information (Previous)',
        ROWS: [
            [
                {
                    field_type: 'input',
                    class_name: 'input-text-center',
                    id: 'previous_employer_tin',
                    item_no: '16.',
                    label: 'Tax Identification Number (TIN)',
                    placeholder: '000-000-000-000',
                    validations: {
                        required: false
                    },
                    type: 'tin'
                },
                {
                    field_type: 'input',
                    class_name: 'columns2',
                    id: 'previous_employer_name',
                    item_no: '17.',
                    label: 'Employer\'s Name',
                    placeholder: 'Enter employer\'s name',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns2',
                    id: 'previous_employer_address',
                    item_no: '18.',
                    label: 'Registered Address',
                    placeholder: 'Enter address',
                    validations: {
                        required: false
                    }
                },
                {
                    field_type: 'input',
                    class_name: 'input-text-center',
                    id: 'previous_employer_zip_code',
                    item_no: '18A.',
                    label: 'ZIP Code',
                    placeholder: 'Enter ZIP code',
                    validations: {
                        required: false
                    },
                    type: 'number'
                }
            ]
        ]
    },
    PART_4A: {
        TITLE: 'Part IV-A: SUMMARY',
        ROWS: [
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'gross_compensation_income_present_employer',
                    item_no: '19.',
                    label: 'Gross Compensation Incom from Present Employer (Sum of items 36 and 50)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'total_non_taxable_exempt',
                    item_no: '20.',
                    label: 'Less: Total Non-Taxable / Exempt Compensation Income from Present Employer (From Item 36)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_compensation_income_present_employer',
                    item_no: '21.',
                    label: 'Taxable Compensation Income from Present Employer (Item 19 Less Item 20) (From Item 50)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_compensation_income_previous_employer',
                    item_no: '22.',
                    label: 'Add: Taxable Compensation Income from Previous Employer, if applicable',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'gross_taxable_compensation_income',
                    item_no: '23.',
                    label: 'Gross Taxable Compensation Income (Sum of Items 21 and 22)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'tax_due',
                    item_no: '24.',
                    label: 'Tax Due',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: null,
                    class_name: 'columns3',
                    id: '25.',
                    item_no: '25.',
                    label: 'Amount of Taxes Withheld'
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full indented number',
                    id: 'amount_of_taxes_withheld_present_employer',
                    item_no: '25A.',
                    label: 'Present Employer',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full indented number',
                    id: 'amount_of_taxes_withheld_previous_employer',
                    item_no: '25B.',
                    label: 'Previous Employer, if applicable',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'total_amount_of_taxes_withheld_as_adjusted',
                    item_no: '26.',
                    label: 'Total Amount of Taxes Withheld as Adjusted (Sum of Items 25A and 25B)',
                    validations: {
                        required: false
                    }
                }
            ]
        ]
    },
    PART_4B: {
        TITLE: 'Part IV-B : DETAILS OF COMPENSATION INCOME AND TAX WITHHELD FROM PRESENT EMPLOYER',
        ROWS: [
            [
                {
                    field_type: null,
                    class_name: 'columns3 section-title',
                    id: '4B-A',
                    label: 'A. NON-TAXABLE / EXEMPT COMPENSATION INCOME'
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'nontax_basic_salary',
                    item_no: '27.',
                    label: 'Basic Salary (including the exempt P250,000 & below) or the Statutory Minimum Wage of the MWE',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'nontax_holiday_pay',
                    item_no: '28.',
                    label: 'Holiday Pay (MWE)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'nontax_overtime_pay',
                    item_no: '29.',
                    label: 'Overtime Pay (MWE)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'nontax_night_shift_differential',
                    item_no: '30.',
                    label: 'Night Shift Differential (MWE)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'nontax_hazard_pay',
                    item_no: '31.',
                    label: 'Hazard Pay (MWE)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'nontax_13th_month_pay_and_other_benefits',
                    item_no: '32.',
                    label: '13th Month Pay and Other Benefits (maximum of P90,000)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'nontax_deminimis_benefits',
                    item_no: '33.',
                    label: 'De Minimis Benefits',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'nontax_contributions_ee_shares',
                    item_no: '34.',
                    label: 'SSS, GSIS, PHIC & PAG-IBIG Contributions & Union Dues (employee share only)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'nontax_salaries_other_form_of_compensation',
                    item_no: '35.',
                    label: 'Salaries & Other Forms of Compensation',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'nontax_total_non_taxable_exempt_compensation_income',
                    item_no: '36.',
                    label: 'Total Non-Taxable / Exempt Compensation Income (Sum of Items 27 to 35)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: null,
                    class_name: 'columns3 section-title',
                    id: '4B-B',
                    label: 'B. TAXABLE COMPENSATION INCOME'
                }
            ],
            [
                {
                    field_type: null,
                    class_name: 'columns3 section-title pad-left',
                    id: '4B-REGULAR',
                    label: 'REGULAR'
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_basic_salary',
                    item_no: '37.',
                    label: 'Basic Salary',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_representation',
                    item_no: '38.',
                    label: 'Representation',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_transportation',
                    item_no: '39.',
                    label: 'Transportation',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_cola',
                    item_no: '40.',
                    label: 'Cost of Living Allowance (COLA)',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_fixed_housing_allowance',
                    item_no: '41.',
                    label: 'Fixed Housing Allowance',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: null,
                    class_name: 'columns3',
                    id: '42.',
                    item_no: '42.',
                    label: 'Others (Specify)'
                }
            ],
            [
                {
                    field_type: 'specify-input',
                    id: 'taxable_reg_others_a',
                    item_no: '42A.',
                    label: '42A.',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'specify-input',
                    class_name: 'number',
                    id: 'taxable_reg_others_b',
                    item_no: '42B.',
                    label: '42B.',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: null,
                    class_name: 'columns3 section-title',
                    id: '4B-SUPPLEMENTARY',
                    label: 'SUPPLEMENTARY'
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_commission',
                    item_no: '43.',
                    label: 'Commission',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_profit_sharing',
                    item_no: '44.',
                    label: 'Profit Sharing',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_fees',
                    item_no: '45.',
                    label: 'Fees Including Director\'s Fees',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_13th_month_benefits',
                    item_no: '46.',
                    label: 'Taxable 13th Month Pay Benefits',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_hazard_pay',
                    item_no: '47.',
                    label: 'Hazard Pay',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'taxable_overtime_pay',
                    item_no: '48.',
                    label: 'Overtime Pay',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: null,
                    class_name: 'columns3',
                    id: '49.',
                    item_no: '49.',
                    label: 'Others (Specify)'
                }
            ],
            [
                {
                    field_type: 'specify-input',
                    id: 'taxable_supp_others_a',
                    item_no: '49A.',
                    label: '49A.',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'specify-input',
                    class_name: 'number',
                    id: 'taxable_supp_others_b',
                    item_no: '49B.',
                    label: '49B.',
                    validations: {
                        required: false
                    }
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'columns3 horizontal full number',
                    id: 'total_taxable_compensation',
                    item_no: '50.',
                    label: 'Total Taxable Compensation Income (Sum of Items 37 to 49B)',
                    validations: {
                        required: false
                    }
                }
            ]
        ]
    },
    DECLARATION: {
        TITLE: 'DECLARATION',
        ROWS: [
            [
                {
                    field_type: 'input',
                    class_name: '',
                    id: 'present_employer_authorized_agent_name',
                    item_no: '51.',
                    label: 'Present Employer / Authorized Agent Signatory',
                    validations: {
                        required: true
                    }
                },
                {
                    field_type: 'date-picker',
                    class_name: '',
                    id: 'present_employer_authorized_agent_date_signed',
                    label: 'Date Signed',
                    validations: {
                        required: false
                    },
                    dayFormat: DATE_FORMATS.DISPLAY
                },
                {
                    field_type: 'switch',
                    class_name: 'switch',
                    id: 'substituted_filing',
                    label: 'Substituted Filing?'
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: '',
                    id: 'ctc_valid_id_no_employee',
                    item_no: '52.',
                    label: 'CTC/Valid ID No. of Employee',
                    validations: {
                        required: false
                    }
                },
                {
                    field_type: 'input',
                    class_name: '',
                    id: 'place_of_issue',
                    label: 'Place of Issue',
                    validations: {
                        required: false
                    }
                },
                {
                    field_type: 'date-picker',
                    class_name: '',
                    id: 'ctc_valid_id_no_employee_date_signed',
                    label: 'Date Signed',
                    validations: {
                        required: false
                    },
                    dayFormat: DATE_FORMATS.DISPLAY
                }
            ],
            [
                {
                    field_type: 'input',
                    class_name: 'number',
                    id: 'ctc_amount_paid',
                    label: 'Amount paid, if CTC',
                    validations: {
                        required: false
                    }
                }
            ]
        ]
    }
};

export const BIR_2316_FORM_FIELDS = {
    INPUTS: [
        'employee_tin',
        'employee_last_name',
        'employee_first_name',
        'employee_middle_name',
        'employee_rdo_code',
        'employee_registered_address',
        'employee_registered_zip_code',
        'employee_local_home_address',
        'employee_local_zip_code',
        'employee_foreign_address',
        'employee_contact_number',
        'current_employer_tin',
        'current_employer_name',
        'current_employer_address',
        'current_employer_zip_code',
        'previous_employer_tin',
        'previous_employer_name',
        'previous_employer_address',
        'previous_employer_zip_code',
        'taxable_reg_others_a_specify',
        'taxable_reg_others_b_specify',
        'taxable_supp_others_a_specify',
        'taxable_supp_others_b_specify',
        'present_employer_authorized_agent_name',
        'ctc_valid_id_no_employee',
        'place_of_issue'
    ],
    DATES: [
        'birth_date',
        'present_employer_authorized_agent_date_signed',
        'ctc_valid_id_no_employee_date_signed'
    ],
    SWITCHES: [
        'is_mwe_exempt',
        'substituted_filing'
    ],
    AMOUNTS: [
        'statutory_mwe_per_day',
        'statutory_mwe_per_month',
        'gross_compensation_income_present_employer',
        'total_non_taxable_exempt',
        'taxable_compensation_income_present_employer',
        'taxable_compensation_income_previous_employer',
        'gross_taxable_compensation_income',
        'tax_due',
        'amount_of_taxes_withheld_present_employer',
        'amount_of_taxes_withheld_previous_employer',
        'total_amount_of_taxes_withheld_as_adjusted',
        'nontax_basic_salary',
        'nontax_holiday_pay',
        'nontax_overtime_pay',
        'nontax_night_shift_differential',
        'nontax_hazard_pay',
        'nontax_13th_month_pay_and_other_benefits',
        'nontax_deminimis_benefits',
        'nontax_contributions_ee_shares',
        'nontax_salaries_other_form_of_compensation',
        'nontax_total_non_taxable_exempt_compensation_income',
        'taxable_basic_salary',
        'taxable_representation',
        'taxable_transportation',
        'taxable_cola',
        'taxable_fixed_housing_allowance',
        'taxable_reg_others_a_amount',
        'taxable_reg_others_b_amount',
        'taxable_commission',
        'taxable_profit_sharing',
        'taxable_fees',
        'taxable_13th_month_benefits',
        'taxable_hazard_pay',
        'taxable_overtime_pay',
        'taxable_supp_others_a_amount',
        'taxable_supp_others_b_amount',
        'total_taxable_compensation',
        'ctc_amount_paid'
    ]
};
