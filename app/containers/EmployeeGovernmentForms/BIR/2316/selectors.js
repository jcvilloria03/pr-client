import { createSelector } from 'reselect';

/**
 * Direct selector to the Employee Government Form BIR 2316 state domain
 */
const makeSelectPageDomain = () => (
    ( state ) => ( state.get( 'employeeGovernmentFormsBIR2316' ) )
);

const makeSelectPageStatus = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'page_status' )
);

const makeSelectData = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'data' ).toJS()
);

const makeSelectDownloadUrl = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'download_url' )
);

const makeSelectNotification = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'notification' ).toJS()
);

const makeSelectFormErrors = () => createSelector(
    makeSelectPageDomain(),
    ( state ) => state.get( 'errors' ).toJS()
);

export {
    makeSelectPageStatus,
    makeSelectData,
    makeSelectDownloadUrl,
    makeSelectNotification,
    makeSelectFormErrors
};
