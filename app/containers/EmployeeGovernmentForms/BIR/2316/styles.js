import styled, { css } from 'styled-components';

export const LoadingWrapper = styled.div`
    display: flex;
    justify-content: center;
    height: 100vh;

    & > div {
        display: flex;
        flex-direction: column;
        justify-content: center;

        .spinner {
            font-size: 7em;
            align-self: center;
        }

        .description {
            padding-top: 40px;
            text-align: center;

            h3 {
                font-weight: 600;
            }
        }
    }
`;

export const FormWrapper = styled.div`
    margin-bottom: 80px;
`;

export const FormTitleWrapper = styled.div`
    width: 100%;
    text-align: center;

    h2, h3 {
        font-weight: 600;
        margin: 0;
    }
`;

export const SectionWrapper = styled.section`
    display: flex;
    flex-wrap: wrap;
    background: #fff;
    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
    padding: 10px;
    margin: 0;
    width: 100%;

    .line-heading {
        padding: 0 15px;
        margin-bottom: 10px;

        h3 {
            margin-bottom: 20px;
        }
    }

    .specify-input {
        justify-content: space-between;
        padding-right: 15px;

        & > div:first-child {
            flex-direction: row;
            width: 50%;

            label {
                margin-right: 10px;
            }
        }
    }

    .pad-bottom {
        padding-bottom: 24px;
    }

    .item-number {
        color: #a1a1a1;
        margin-right: 0;
    }
`;

export const SectionHeadingWrapper = styled.div`
    width: 100%;
    text-align: center;

    h5 {
        font-size: 18px;
    }
`;

export const LineWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    align-items: flex-start;
    margin-bottom: 10px;

    label, p.label {
        font-size: 14px;
        font-weight: 400;
    }

    label {
        margin-bottom: 4px;
    }

    input {
        text-transform: uppercase;
    }

    .DayPickerInput {
        width: 100%;

        input {
            width: 100%;
            text-transform: unset;
            padding-top: 0;
        }
    }

    .period {
        display: flex;
        flex: 1;
        flex-direction: column;

        & > div {
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: space-between;

            span {
                margin-right: 5px;
            }

            & > div {
                width: 50%;
                margin-left: 16px;
            }
        }

        & > span {
            font-size: 14px;
        }
    }

    .input-text-center input {
        text-align: center;
    }

    .number input {
        text-align: right;
    }
`;

export const InputWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    width: 33%;
    padding: 0 15px;

    & > span {
        font-size: 14px;
        font-weight: 400;
    }

    &.columns2{
        width: 66%;
    }

    &.columns3{
        width: 100%;
    }

    &.section-title {
        padding: 0;
        font-weight: 600;
    }

    &.pad-left {
        padding-left: 18px;
    }

    &.switch {
        height: 60px;
        display: flex;
        flex-direction: column;
        justify-content: center;

        & > div {
            display: flex;
        }

        .switch-label {
            margin-right: 20px;
        }
    }

    &.horizontal {
        width: 100%;

        &.full > div {
            align-items: flex-end;
            justify-content: center;
        }

        & > div {
            display: flex;
            position: relative;
            padding-right: 15px;
            align-items: center;
        }

        input {
            width: calc(33% - 15px);
        }

        label {
            width: 67%;
            position: absolute;
            left: 0;
            padding-right: 20px;
            align-self: flex-start;
        }

        p {
            width: 33%;
            padding-left: 15px;
        }

        &.indented label {
            left: 20px;
        }
    }
`;

export const SpecifyInputWrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 0 30px;

    & > div:first-child {
        flex-direction: row;
        width: 67%;

        input {
            width: 50%;
        }

        label {
            margin-right: 10px;
        }
    }

    & > div:last-child {
        width: calc(33% - 10px);
    }
`;

const horizontalDateCss = css`
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
`;

export const InlineDateWrapper = styled.div`
    & > div {
        display: flex;
        flex-direction: column;
        ${({ horizontal }) => horizontal && horizontalDateCss}
        width: 100%;
    }

    .DayPickerInput > input {
        width: 100%;
        text-align: center;
        padding-top: 0;
    }
`;

export const PageHeaderWrapper = styled.header`
    text-align: center;
    margin-bottom: 30px;

    & > div {
        text-align: left;
    }

    h3 {
        font-weight: 600;
        margin-bottom: 10px;
    }
`;

export const FormHeaderWrapper = styled.header`
    margin-bottom: 10px;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    justify-content: space-between;

    span {
        display: block;
    }

    ol {
        padding-left: 16px;
    }

    h3 {
        font-weight: 600;
    }
`;

export const NavWrapper = styled.nav`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 30px;
`;
