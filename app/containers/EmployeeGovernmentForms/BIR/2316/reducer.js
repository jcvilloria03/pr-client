import { fromJS } from 'immutable';
import moment from 'moment/moment';

import { RESET_STORE } from 'containers/App/constants';

import { formatFullName, formatNumber } from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import {
    SET_PAGE_STATUS,
    SET_NOTIFICATION,
    PAGE_STATUSES,
    SET_DATA,
    SET_DOWNLOAD_URL,
    BIR_2316_FORM_FIELDS,
    SET_ERRORS
} from './constants';

const initialState = fromJS({
    page_status: PAGE_STATUSES.LOADING,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    data: {},
    download_url: '',
    errors: {}
});

/**
 * Prepares data for form
 *
 * @param {object} data - Employee 2316 data
 * @returns {object}
 */
function prepareData({
    attributes,
    ...restOfArgs
}) {
    const {
        employee_first_name: first_name,
        employee_middle_name: middle_name,
        employee_last_name: last_name,
        coverage_start_day: start,
        coverage_end_day: end,
        ...restOfAttributes
    } = attributes;

    const employeeName = formatFullName({ first_name, middle_name, last_name });

    const year = attributes.year;
    const coverageStart = moment( `${year}/${start}`, 'YYYY/MM/DD' ).format( DATE_FORMATS.API );
    const coverageEnd = moment( `${year}/${end}`, 'YYYY/MM/DD' ).format( DATE_FORMATS.API );

    const data = {
        ...restOfArgs,
        ...restOfAttributes,
        employee_name: employeeName,
        coverage_start_day: coverageStart,
        coverage_end_day: coverageEnd
    };

    BIR_2316_FORM_FIELDS.AMOUNTS.forEach( ( field ) => {
        data[ field ] = formatNumber( attributes[ field ]);
    });

    BIR_2316_FORM_FIELDS.SWITCHES.forEach( ( field ) => {
        data[ field ] = Boolean( attributes[ field ]);
    });

    return data;
}

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case SET_PAGE_STATUS:
            return state.set( 'page_status', action.payload );
        case SET_DATA:
            return state.set( 'data', fromJS( prepareData( action.payload ) ) );
        case SET_DOWNLOAD_URL:
            return state.set( 'download_url', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
};
