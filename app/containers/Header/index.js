import React from 'react';
import { Container, Dropdown, DropdownMenu, DropdownToggle } from 'reactstrap';
import find from 'lodash/find';

import SalCompanySwitcher from 'components/SalCompanySwitcher';
import Icon from 'components/Icon';
import A from 'components/A';
import HeaderLink from 'components/HeaderLink';
import SalUserMenu from 'components/SalUserMenu';
import { browserHistory } from 'utils/BrowserHistory';
import { auth } from 'utils/AuthService';
import { isAuthorized } from 'utils/Authorization';
import { sockets } from 'utils/Sockets';
import { ANNOUNCEMENT_NOTIFICATIONS } from 'utils/notifications';
import { PRODUCTS } from 'utils/constants';
import { subscriptionService } from 'utils/SubscriptionService';

import { Wrapper, DropdownWrapper, ScrollableDropdownWrapper } from './styles';

import NotificationItem from './templates/NotificationItem';
import AnnouncementItem from './templates/AnnouncementItem';

/**
* Header
*/
class Header extends React.Component {

    static propTypes = {
        onSetCompany: React.PropTypes.func,
        currentCompany: React.PropTypes.object,
        companies: React.PropTypes.array,
        userInformation: React.PropTypes.object,
        unreadAnnouncements: React.PropTypes.array,
        markAnnouncementAsRead: React.PropTypes.func,
        markReplyAsRead: React.PropTypes.func,
        userNotifications: React.PropTypes.object,
        loadMoreNotifications: React.PropTypes.func,
        pushNotification: React.PropTypes.func,
        updateNotificationClickedStatus: React.PropTypes.func,
        hasApprovals: React.PropTypes.bool
    };

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            loggedIn: false,
            permission: {
                home: true,
                payroll: false,
                employees: false,
                reports: false,
                schedules: true
            },
            menu: {
                employees: false,
                reports: false
            },
            unreadAnnouncements: [],
            announcementDropdownOpen: false,
            notificationDropdownOpen: false,
            shouldShowNotificationBadge: false,
            notificationsPage: 2,
            hasEssPermissions: auth.getAuthzPermissions().includes( 'root.ess' )
        };
    }

    /**
     * called before mount and checks if header should be painted in the DOM.
     */
    componentWillMount() {
        if ( auth.loggedIn() ) {
            sockets.init();
        }
        // check if user is logged in
        this.setState({
            loggedIn: auth.loggedIn()
        });
    }

    /**
     * checks main nav links permissions
     */
    componentDidMount() {
        // check permission to view pages
        isAuthorized([
            'view.payroll',
            'view.government_form',
            'view.employee'
        ], ( authorization ) => {
            this.setState({ permission: Object.assign( this.state.permission, {
                payroll: authorization[ 'view.payroll' ],
                forms: authorization[ 'view.government_form' ],
                employees: authorization[ 'view.employee' ]
            }) });
        });
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.unreadAnnouncements !== nextProps.unreadAnnouncements ) {
            this.setState({
                unreadAnnouncements: nextProps.unreadAnnouncements
            });
        }
        if ( !Object.keys( this.props.userInformation ).length && Object.keys( nextProps.userInformation ).length ) {
            this.listenUserChannel( nextProps.userInformation );
        }

        if ( this.props.userNotifications.data.length !== nextProps.userNotifications.data.length ) {
            const lastNotificationId = localStorage.getItem( 'lastNotificationId' );
            this.setState({
                shouldShowNotificationBadge: nextProps.userNotifications.data[ 0 ].id !== parseInt( lastNotificationId, 10 )
            });
        }
    }

    /**
     * Gets a list of main navigation menu items
     */
    getMainNavigationMenuItems() {
        const isVisible = !auth.isExpired();
        return [
            {
                id: 'home',
                icon: 'home',
                title: 'Home',
                links: ['dashboard'],
                visible: isVisible,
                onClick: () => { window.location.href = '/dashboard/'; }

            },
            {
                id: 'schedules',
                icon: 'clock',
                title: 'Time and Attendance',
                links: [ 'attendance-computations', 'shifts', 'schedules' ],
                visible: isVisible && this.hasTimeAttendanceProduct(),
                onClick: () => this.navigate( '/time/attendance-computations', true )
            },
            {
                id: 'payroll',
                icon: 'payroll',
                title: 'Payroll',
                links: [ 'payroll/payroll', 'payslip', '/forms', 'bonuses', 'commissions', 'final-pay-run' ],
                visible: isVisible && this.hasPayrollProduct(),
                onClick: () => this.navigate( '/payroll' )
            },
            {
                id: 'employees',
                icon: 'employees',
                title: 'Employees',
                links: [ 'employees', 'employee', 'loans', 'deductions', 'adjustments', 'allowances', 'leaves', 'leave-credits', 'annual-earnings', 'workflow-entitlements', 'views' ],
                visible: isVisible,
                onClick: () => this.navigate( '/employees', true )
            }
        ];
    }

    /**
     * Gets a list of sub-navigation menu
     */
    getSubNavigationMenuItems() {
        const isVisible = !auth.isExpired();
        return [
            {
                id: 'user',
                icon: 'userCircle',
                label: 'Profile information',
                visible: isVisible,
                onClick: () => this.navigateToProfile()
            },
            {
                id: 'control-panel',
                icon: 'globe',
                label: 'Control Panel',
                visible: true,
                onClick: () => this.navigate( '/control-panel/subscriptions', true )
            },
            {
                id: 'ess',
                icon: 'userCircle', // added just to avoid warnings in console
                label: 'Employee Page',
                visible: isVisible && this.isEssUrlVisible() && auth.isEmployee(),
                onClick: () => this.navigateToESS()
            },
            {
                id: 'knowledge-base',
                icon: 'infoCircle', // added just to avoid warnings in console
                label: 'Knowledge Base',
                visible: true,
                onClick: () => this.navigateToKnowledgeBase()
            },
            {
                id: 'signout',
                icon: 'signOut',
                label: 'Logout',
                visible: true,
                onClick: () => auth.logout()
            }
        ];
    }

    getNavBrand() {
        return (
            <SalCompanySwitcher
                onSetCompany={ this.props.onSetCompany }
                onManageCompanies={ this.handleOnManageCompanies }
                currentCompany={ this.props.currentCompany }
                companies={ this.props.companies }
            />
        );
    }

    /**
     * Listens users channel for AnnouncementPublished event.
     */
    listenUserChannel( user ) {
        window.Echo.private( `user.${user.id}` )
            .listen( 'UserNotificationReceived', ({ notification }) => {
                if (
                    notification.activity && (
                        notification.activity.type === ANNOUNCEMENT_NOTIFICATIONS.REPLY_SENT ||
                        notification.activity.type === ANNOUNCEMENT_NOTIFICATIONS.PUBLISHED
                    )
                ) {
                    if ( this.state.unreadAnnouncements ) {
                        const announcements = [...this.state.unreadAnnouncements];
                        announcements.unshift( notification );
                        this.setState({
                            unreadAnnouncements: announcements.slice( 0, 3 )
                        });

                        return;
                    }
                }

                this.props.pushNotification( this.props.userNotifications.data, notification );
            });
    }

    toggleAnnouncementDropdown = () => {
        this.setState({
            announcementDropdownOpen: !this.state.announcementDropdownOpen
        });
    }

    toggleNotificationDropdown = () => {
        // if ( this.props.userNotifications.data.length ) {
        //     localStorage.setItem( 'lastNotificationId', this.props.userNotifications.data[ 0 ].id );
        //     this.shouldShowBadge = false;
        // }

        this.setState({
            notificationDropdownOpen: !this.state.notificationDropdownOpen
        });
    }

    hasPayrollProduct() {
        const products = auth.getProducts();

        if ( products ) {
            return ( products.includes( PRODUCTS.PAYROLL ) || products.includes( PRODUCTS.BOTH ) );
        }

        return false;
    }

    hasTimeAttendanceProduct() {
        const products = auth.getProducts();

        if ( products ) {
            return ( products.includes( PRODUCTS.TIME_AND_ATTENDANCE ) || products.includes( PRODUCTS.BOTH ) );
        }

        return false;
    }

    /**
     * Navigate to manage companies page
     */
    handleOnManageCompanies() {
        browserHistory.push( '/control-panel/companies', true );
    }

    /**
     * Navigate to company settings
     */
    navigateToSettings() {
        browserHistory.push( '/company-settings/company-structure/company-details', true );
    }

    /**
     * Navigate to approvals
     */
    navigateToApprovals() {
        browserHistory.push( '/time/company/approvals/list', true );
    }

    /**
     * Navigate to announcements
     */
    navigateToAnnouncements() {
        browserHistory.push( '/time/company/announcements/received', true );
    }

    /**
      * Navigate to announcements
     */
    navigateToNotifications() {
        window.location.href = '/time/company/approvals/list';
    }

    /**
     * Navigate to profile page
     */
    navigateToProfile() {
        browserHistory.push( '/profile', true );
    }

    /**
     * Navigate to ESS
     */
    navigateToESS() {
        window.location.href = '/ess/';
    }

    /**
     * Navigate to KnowledgeBase
     */
    navigateToKnowledgeBase() {
        window.location.href = 'https://support.salarium.com/';
    }

    /**
     * Navigate to shifts
     */
    navigateToShifts() {
        browserHistory.push( '/time/shifts/calendar', true );
    }

    /**
>>>>>>> 9db1ee00c19ab9b076bc8f6b5dc4090c7ec04684
     * Navigate to dashboard
     */
    navigateToDashboard() {
        window.location.href = '/dashboard/';
    }

    /**
     * this handles navigation
     * @param location = page to navigate to.
     * @param withoutBasePath = whether to prepend base path.
     */
    navigate( location, withoutBasePath = false ) {
        browserHistory.push( location, withoutBasePath );
    }

    /**
     * Check if Ess Url is visible.
     * @returns {Object}
     */
    isEssUrlVisible() {
        const user = auth.getUser();
        if ( !user ) {
            return false;
        }
        const companyId = user.company_id;
        const { hasEssPermissions } = this.state;

        const employeeCompany = find( user.companies, ( userCompany ) => userCompany.id === companyId );

        return employeeCompany && hasEssPermissions;
    }

    loadMoreNotifications = () => {
        const { data } = this.props.userNotifications;
        const page = this.state.notificationsPage;

        this.props.loadMoreNotifications( data, page );

        this.setState({
            notificationsPage: this.state.notificationsPage + 1
        });
    }

    hasMoreNotifications = () => (
        this.state.notificationsPage - 1 !== this.props.userNotifications.totalPages
    )

    removeNotificationBadge = () => {
        if ( this.props.userNotifications.data && this.props.userNotifications.data.length ) {
            localStorage.setItem( 'lastNotificationId', this.props.userNotifications.data[ 0 ].id );
            this.setState({
                shouldShowNotificationBadge: false
            });
        }
    }

    markReplyAsRead = ( announcement ) => {
        const { announcement_id: announcementId, reply_id: replyId, owner } = announcement.activity.params;
        const iscurrentUserAnnouncementOwner = this.props.userInformation.id === owner.id ? 'sent' : 'received';

        this.props.markReplyAsRead( announcementId, replyId, iscurrentUserAnnouncementOwner, announcement.id );
    }

    /**
     * Render Announcements Dropdown
     */
    renderAnnouncementsDropdown = () => {
        if ( this.state.unreadAnnouncements.length && ( this.hasTimeAttendanceProduct() || this.hasPayrollProduct() ) ) {
            return (
                <DropdownWrapper>
                    <Dropdown isOpen={ this.state.announcementDropdownOpen } toggle={ this.toggleAnnouncementDropdown }>
                        <DropdownToggle
                            tag="span"
                            onClick={ this.toggleAnnouncementDropdown }
                            data-toggle="dropdown"
                            aria-expanded={ this.state.announcementDropdownOpen }
                        >
                            <span className="menu-item">
                                <HeaderLink
                                    title="Announcements"
                                    links={ ['announcements'] }
                                    className="menu-link"
                                >
                                    <Icon name="bullhorn" />
                                    <span className="sl-c-icon-badge"></span>
                                </HeaderLink>
                            </span>
                        </DropdownToggle>
                        <DropdownMenu>
                            { this.state.unreadAnnouncements.map( ( announcement ) => (
                                <AnnouncementItem
                                    key={ announcement.id }
                                    announcement={ announcement }
                                    markAnnouncementAsRead={ this.props.markAnnouncementAsRead }
                                    markReplyAsRead={ this.markReplyAsRead }
                                />
                            ) )}
                        </DropdownMenu>
                    </Dropdown>
                </DropdownWrapper>
            );
        }

        return null;
    }

    /**
     * Render Notifications Dropdown
     */
    renderNotificationDropdown = () => {
        const { userNotifications } = this.props;
        if ( this.hasTimeAttendanceProduct() || this.hasPayrollProduct() ) {
            return (
                <ScrollableDropdownWrapper>
                    <Dropdown isOpen={ this.state.notificationDropdownOpen } toggle={ this.toggleNotificationDropdown }>
                        <DropdownToggle
                            tag="span"
                            onClick={ this.toggleNotificationDropdown }
                            data-toggle="dropdown"
                            aria-expanded={ this.state.notificationDropdownOpen }
                        >
                            <span className="menu-item">
                                <HeaderLink
                                    title="Notifications"
                                    links={ ['notifications'] }
                                    className="menu-link"
                                    onClick={ this.removeNotificationBadge }
                                >
                                    <Icon name="bell" />
                                    { this.state.shouldShowNotificationBadge ? (
                                        <span className="sl-c-icon-badge"></span>
                                    ) : null}
                                </HeaderLink>
                            </span>
                        </DropdownToggle>
                        { userNotifications.data && userNotifications.data.length > 0 && (
                            <DropdownMenu>
                                { this.props.userNotifications.data.map( ( notification ) => (
                                    <span key={ notification.id }>
                                        <NotificationItem
                                            notification={ notification }
                                            userId={ this.props.userInformation.id }
                                            updateNotificationClickedStatus={ this.props.updateNotificationClickedStatus }
                                        />
                                    </span>
                                ) )}
                                <span style={ { textAlign: 'center' } }>
                                    { !userNotifications.loadingMore && this.hasMoreNotifications() && (
                                        <A onClick={ this.loadMoreNotifications } >
                                            Show more
                                        </A>
                                    ) }
                                    { userNotifications.loadingMore && (
                                        <span style={ { padding: '10px' } }>
                                            <i className="fa fa-circle-o-notch fa-spin fa-fw"></i>
                                        </span>
                                    ) }
                                </span>
                            </DropdownMenu>
                        ) }
                    </Dropdown>
                </ScrollableDropdownWrapper>
            );
        }

        return null;
    }

    /**
    * Header render method.
    */
    render() {
        const { loggedIn, permission } = this.state;

        const isVisible = !auth.isExpired();

        const isSubscribedToTA = subscriptionService.isSubscribedToTA( auth.getProducts() );

        return loggedIn ? (
            <Wrapper>
                <Container>
                    { this.getNavBrand() }

                    <div className="menu-nav">
                        <ul className="menu">
                            {
                                this.getMainNavigationMenuItems().filter( ( item ) => item.visible ).map( ( item, index ) => {
                                    const hasPermission = permission[ item.id ];
                                    return hasPermission ? (
                                        <li key={ index } className="menu-item">
                                            <HeaderLink
                                                showTooltip
                                                title={ item.title }
                                                links={ item.links }
                                                className="menu-link"
                                                onClick={ item.onClick }
                                            >
                                                <Icon name={ item.icon } />
                                            </HeaderLink>
                                        </li>
                                    ) : false;
                                })
                            }
                        </ul>
                    </div>

                    <div className="sub-menu-nav">
                        <div className="sub-menu-nav">
                            <ul className="menu">
                                {
                                    this.state.permission.reports ?
                                        <li className={ `menu-item ${this.state.menu.reports ? 'active' : ''}` }>
                                            <A className="menu-link" href="/reports">Reports</A>
                                        </li>
                                        : false
                                }
                            </ul>
                        </div>
                        <div className="sub-menu-nav">
                            <div className="sub-menu-nav">
                                <ul className="menu">
                                    { isVisible && !this.state.unreadAnnouncements.length && ( this.hasTimeAttendanceProduct() || this.hasPayrollProduct() ) ? (
                                        <li className="menu-item">
                                            <HeaderLink
                                                title="Settings"
                                                links={ ['announcements'] }
                                                className="menu-link"
                                                onClick={ () => this.navigateToAnnouncements() }
                                            >
                                                <Icon name="bullhorn" />
                                            </HeaderLink>
                                        </li>
                                    ) : null }
                                    { isVisible && [
                                        <li className="menu-item" key="announcements">
                                            { this.renderAnnouncementsDropdown() }
                                        </li>,
                                        <li className="menu-item" key="notifications">
                                            { this.renderNotificationDropdown() }
                                        </li>
                                    ]}
                                    { isVisible && isSubscribedToTA && (
                                        <li className="menu-item" key="approvals">
                                            <HeaderLink
                                                title="Approvals"
                                                links={ ['approvals'] }
                                                className="menu-link"
                                                onClick={ () => this.navigateToApprovals() }
                                            >
                                                <Icon name="checkCircle" />
                                                { this.props.hasApprovals ? (
                                                    <span className="sl-c-icon-badge"></span>
                                                        ) : null }
                                            </HeaderLink>
                                        </li>
                                    )}
                                    { isVisible && [
                                        <li className="menu-item" key="company-settings">
                                            <HeaderLink
                                                showTooltip
                                                title="Settings"
                                                links={ ['settings'] }
                                                className="menu-link sl-c-icon-badge"
                                                onClick={ () => this.navigateToSettings() }
                                            >
                                                <Icon name="cog" />
                                            </HeaderLink>
                                        </li>
                                    ]}
                                    <li className="menu-item">
                                        <SalUserMenu dropdownItems={ this.getSubNavigationMenuItems().filter( ( item ) => item.visible ) } />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </Container>
            </Wrapper>
        ) : false;
    }
}

export default Header;
