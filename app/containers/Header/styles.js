import styled from 'styled-components';

export const SalCompanySwitcher = styled.div`
    border-right: 1px solid #ffffff;
    margin-right: 1rem;
`;

export const Wrapper = styled.div`
    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    z-index: 1000;
    width: 100%;
    background-color: #00A5E5;

    > .container {
        display: flex;
        height: 76px;

        width: 100vw;
        padding: 0 1rem;
    }

    .brand {
        img {
            width: 190px;
            height: auto;
        }
    }

    .menu-nav,
    .sub-menu-nav {
        display: inline-flex;

        .menu {
            display: inline-flex;
            list-style: none;
            padding: 0;
            margin: 0 0;
            margin-left: 1rem;

            .menu-item {
                display: inline-flex;
                align-items: center;

                > div, > div > .dropdown, > div > .dropdown > span, > div > .dropdown > span > .menu-item {
                    display: inline-flex;
                    align-items: center;
                    height: 100%;
                }

                > div > a {
                    display: inline-flex;
                    align-items: center;
                    height: 100%;
                    padding-right: 1.2rem;
                    padding-left: 1.2rem;
                    color: #fff;
                    text-decoration: none;
                    outline: none;
                    border-bottom: 3px solid transparent;

                    &:hover {
                        background-color: #0077a5;
                    }

                    &.active {
                        border-bottom-color: #ffffff;
                    }

                    svg {
                        width: 28px;
                        height: 28px;
                    }
                }

                &.active,
                &:hover {
                    cursor: pointer;

                    .sub-menu {
                        display: inline-flex;
                    }
                }

                .sl-c-icon-badge {
                    width: 12px;
                    background-color: #EB7575;
                    height: 12px;
                    border-radius: 50%;
                    position: relative;
                    right: 7px;
                    bottom: 10px;
                }
            }
        }
    }

    .menu-nav {
        margin-right: auto;
    }
`;

export const DropdownWrapper = styled.div`
    .dropdown-menu {
        position: absolute;
        top: 85%;
        left: auto;
        right: 0;
        z-index: 10;
        min-width: 11.7rem;
        font-size: 14px;
        background-color: #F0F4F6;
        border: none;
        box-shadow: 0 1px 2px 0 #cccccc;
        padding: 0;

        &:before {
            position: absolute;
            top: -.5rem;
            left: auto;
            right: 1.4rem;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 0 .5rem .5rem .5rem;
            border-color: transparent transparent #F0F4F6 transparent;
            content: '';
        }

        > span {
            &:first-child {
                a {
                    border-top-right-radius: .25rem;
                    border-top-left-radius: .25rem;
                }
            }

            &:last-child {
                a {
                    border-bottom-right-radius: .25rem;
                    border-bottom-left-radius: .25rem;
                }
            }

            &:not(:last-child) {
                a {
                    border-bottom: 1px solid #eee;
                }
            }
        }

        a {
            display: flex;
            padding: .5rem 1rem .5rem .5rem;
            color: #474747;
            border-left: .25rem solid transparent;
            font-weight: 400;
            align-items: center;

            &.active,
            &:hover {
                border-left-color: #00A5E5;
                color: #0077A5;
                border-bottom: none;
                box-shadow: none;
            }
        }

        .item-icon > svg {
            display: inline-flex;
            align-items: center;
            justify-content: center;
            width: 1rem;
            height: 1rem;
            fill: currentColor;
            vertical-align: -.2rem;
            margin-right: 1rem;
        }
    }

    .dropdown > a {
        color: #ffffff;
        padding: 1rem;
    }

    img {
        border-radius: 50%
    }

    .sl-c-icon-badge {
        width: 12px;
        background-color: #EB7575;
        height: 12px;
        border-radius: 50%;
        position: relative;
        right: 7px;
        bottom: 10px;
    }
`;

export const ScrollableDropdownWrapper = styled.div`
    height: 100%;

    .dropdown-menu {
        position: absolute;
        top: 85%;
        left: auto;
        right: 0;
        z-index: 10;
        font-size: 14px;
        background-color: #F0F4F6;
        border: none;
        box-shadow: 0 1px 2px 0 #cccccc;
        padding: 0;

        max-height: 300px;
        overflow:hidden;
        overflow-y:scroll;

        min-width: 400px !important;
        background: white;
        text-decoration: underline;
        border: 1px solid #00A5E5;
        border-radius: 0;

        .show-more-section {
            text-align: center;
        }

        &:before {
            position: absolute;
            top: -.5rem;
            left: auto;
            right: 1.4rem;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 0 .5rem .5rem .5rem;
            border-color: transparent transparent #F0F4F6 transparent;
            content: '';
        }

        > span {
            &:first-child {
                a {
                    border-top-right-radius: .25rem;
                    border-top-left-radius: .25rem;
                }
            }

            &:last-child {
                a {
                    border-bottom-right-radius: .25rem;
                    border-bottom-left-radius: .25rem;
                }
            }

            &:not(:last-child) {
                a {
                    border-bottom: 1px solid #eee;
                }
            }
        }

        a {
            display: flex;
            padding: 10px;
            color: #00A5E5;
            border-left: .25rem solid transparent;
            font-weight: 400;
            align-items: center;
            border: none;

            &.active,
            &:hover {
                border-left-color: #00A5E5;
                color: #0077A5;
                border-bottom: none;
                box-shadow: none;
            }
        }

        .item-icon > svg {
            display: inline-flex;
            align-items: center;
            justify-content: center;
            width: 1rem;
            height: 1rem;
            fill: currentColor;
            vertical-align: -.2rem;
            margin-right: 1rem;
        }
    }

    .dropdown {
        height: 100%;

        & > a {
            color: #ffffff;
            padding: 1rem;
        }

        & > span {
            height: 100%;

            & > span {
                height: 100%;
            }
        }
    }

    img {
        border-radius: 50%
    }

    .sl-c-icon-badge {
        width: 12px;
        background-color: #EB7575;
        height: 12px;
        border-radius: 50%;
        position: relative;
        right: 7px;
        bottom: 10px;
    }

    .sl-u-text--bold {
        font-weight: 600 !important;
    }

    .sl-u-background--lightgrey {
        background: #f7f8f9;
    }
`;

