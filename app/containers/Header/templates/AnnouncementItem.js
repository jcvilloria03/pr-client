import React, { Component, PropTypes } from 'react';

import A from '../../../components/A';

import { ANNOUNCEMENT_NOTIFICATIONS } from '../../../utils/notifications';

/**
 * Announcement Item
 */
export default class AnnouncementItem extends Component {
    static propTypes = {
        announcement: PropTypes.object,
        markAnnouncementAsRead: PropTypes.func,
        markReplyAsRead: PropTypes.func
    }

    /**
     * Determine if announcement is actually a reply to announcement
     * @param {Object} announcement
     *
     * @returns {Boolean}
     */
    announcementIsReply = () => {
        const { announcement } = this.props;

        return announcement.activity && announcement.activity.type && announcement.activity.type === ANNOUNCEMENT_NOTIFICATIONS.REPLY_SENT;
    }

    /**
     * Render reply
     */
    renderReply = () => {
        const { announcement } = this.props;

        return (
            <span>
                <A
                    onClick={ () => {
                        this.props.markReplyAsRead( announcement );
                    } }
                >
                    Reply from { announcement.activity.params.reply_sender.name }
                </A>
            </span>
        );
    }

    /**
     * Render announcement
     */
    renderAnnouncement = () => {
        const { announcement } = this.props;

        return (
            <span>
                <A
                    onClick={ () => {
                        this.props.markAnnouncementAsRead( announcement.activity.params.announcement_id, announcement.id );
                    } }
                >
                    Announcement from { announcement.activity.params.employee_name }
                </A>
            </span>
        );
    }

    /**
     * Render method
     */
    render() {
        return this.announcementIsReply() ? this.renderReply() : this.renderAnnouncement();
    }
}
