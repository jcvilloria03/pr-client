import React from 'react';

import get from 'lodash/get';
import values from 'lodash/values';
import keys from 'lodash/keys';
import isEmpty from 'lodash/isEmpty';

import A from 'components/A';
import NOTIFICATIONS from 'utils/notifications';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

/**
 * Notification item
 */
export default class NotificatinItem extends React.Component {
    static propTypes = {
        notification: React.PropTypes.object,
        updateNotificationClickedStatus: React.PropTypes.func,
        userId: React.PropTypes.number
    };

    constructor( props ) {
        super( props );

        this.state = {};
    }

    onMessageClick = () => {
        if ( !this.props.notification.clicked ) {
            this.props.updateNotificationClickedStatus( this.props.notification.id, this.followLink );
        } else {
            this.followLink();
        }
    }

    getActivity = () => (
        this.props.notification.activity
    )

    getTemplate = () => {
        const template = NOTIFICATIONS[ this.getActivity().type ];

        if ( !template ) {
            return null;
        }

        return template.data || template[ this.isCurrentUserOwnerOfActivity() ? 'own' : 'other' ];
    }

    getMessage = () => {
        const template = this.getTemplate();

        if ( template === null ) {
            return null;
        }

        const messageTemplate = template.messageTemplate;

        if ( typeof messageTemplate === 'string' ) {
            return messageTemplate;
        }

        return messageTemplate( ...this.getTemplateTagValues() );
    }

    getTemplateTagValues = () => (
        values( this.getTemplate().templateTags.map( ( param, index ) => (
            this.extractParam( param ) || this.getTemplate().defaultValues[ index ]
        ) ) )
    )

    getNotificationRoute = () => {
        const routeType = this.getTemplate().multipleRoutes
            ? this.extractParam( this.getTemplate().routeTypeParam )
            : ( this.getTemplate().isSubscription
                ? 'subscription'
                : 'default'
            );

        const route = this.getTemplate().routes[ routeType ];

        if ( typeof route === 'function' ) {
            return route( this.getActivity() );
        }
        return {
            name: route.name,
            params: this.mapRouteParams( route.params )
        };
    }

    isCurrentUserOwnerOfActivity = () => (
        this.getActivity().owner_id === this.props.userId
    )

    followLink = () => {
        if ( this.getTemplate().link ) {
            const { name, params } = this.getNotificationRoute();

            if ( isEmpty( params ) && [ '/control-panel/subscriptions', '/control-panel/subscriptions/invoices' ].includes( name ) ) {
                browserHistory.push( name, true );
            } else {
                window.location.href = '/time/company/approvals/list';
            }
        }
    }

    mapRouteParams = ( routeParams ) => {
        if ( !routeParams ) {
            return {};
        }

        const params = {};

        keys( routeParams ).forEach( ( param ) => {
            params[ param ] = this.extractParam( routeParams[ param ]);
        });

        if ( !params.companyId ) {
            params.companyId = company.getLastActiveCompanyId();
        }

        return params;
    }

    extractParam = ( param ) => {
        if ( typeof param === 'function' ) {
            return param( this.getActivity() );
        }

        if ( typeof param === 'string' ) {
            if ( param.split( '.' ).shift() === 'params' && typeof this.getActivity().params === 'string' ) {
                const paramsObject = JSON.parse( this.getActivity().params );
                const selectedItem = param.split( '.' ).pop();

                return get( paramsObject, selectedItem );
            }

            return get( this.getActivity(), param );
        }

        return null;
    }

    render() {
        const { notification } = this.props;
        const message = this.getMessage();
        return message && (
            <A
                className={ !notification.clicked ? 'sl-u-text--bold sl-u-background--lightgrey' : '' }
                onClick={ this.onMessageClick }
            >
                { message }
            </A>
        );
    }
}
