import {
    INITIALIZE,
    GET_COMPANY_EMPLOYEES,
    EDIT_ADJUSTMENT,
    NOTIFICATION
} from './constants';

/**
 * Initialize data
 */
export function initializeData( id ) {
    return {
        type: INITIALIZE,
        payload: {
            id
        }
    };
}

/**
 * Get Company Employees
 */
export function getCompanyEmployees( payload ) {
    return {
        type: GET_COMPANY_EMPLOYEES,
        payload
    };
}

/**
 * Update adjustment preview
 */
export function editAdjustment( payload ) {
    return {
        type: EDIT_ADJUSTMENT,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
