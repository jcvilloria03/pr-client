/* eslint-disable consistent-return */
import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import get from 'lodash/get';

import A from '../../../components/A';
import Input from '../../../components/Input';
import Loader from '../../../components/Loader';
import Button from '../../../components/Button';
import SalSelect from '../../../components/Select';
import Switch from '../../../components/Switch';
import SnackBar from '../../../components/SnackBar';
import Typeahead from '../../../components/Typeahead';
import DatePicker from '../../../components/DatePicker';
import { H2, H3 } from '../../../components/Typography';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { subscriptionService } from '../../../utils/SubscriptionService';
import {
    formatCurrency,
    stripNonDigit,
    formatDate
} from '../../../utils/functions';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';

import SubHeader from '../../../containers/SubHeader';

import {
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectApiErrors,
    makeSelectFormOptions,
    makeSelectAdjustmentData,
    makeSelectNotification
} from './selectors';

import * as actions from './actions';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    LoaderWrapper,
    AlignRight
} from './styles';

const inputTypes = {
    input: [ 'amount', 'reason' ],
    select: ['type_id'],
    datePicker: ['date']
};

/**
 * Edit Adjustment Component
 */
class Edit extends Component {
    static propTypes = {
        params: React.PropTypes.object,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        errors: React.PropTypes.object,
        formOptions: React.PropTypes.object,
        adjustmentData: React.PropTypes.object, // eslint-disable-line
        initializeData: React.PropTypes.func,
        getCompanyEmployees: React.PropTypes.func,
        editAdjustment: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            adjustmentData: null,
            permission: {
                edit: false
            },
            errors: {},
            prepopulatedEmployeeId: '',
            typeaheadTimeout: null
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.adjustments',
            'edit.adjustments'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.adjustments' ];

            if ( authorized ) {
                this.setState({ permission: {
                    edit: authorization[ 'edit.adjustments' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });

        this.props.initializeData( this.props.params.id );

        const prepopulatedEmployeeId = JSON.parse( localStorage.getItem( 'employeeIdForEditAdjustment' ) );
        prepopulatedEmployeeId && this.setState({ prepopulatedEmployeeId });
    }

    componentWillReceiveProps( nextProps ) {
        if ( !this.state.adjustmentData ) {
            this.getInitialStateFromProps( nextProps );
        }

        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });

        nextProps.submitted !== this.props.submitted && this.submitButton.setState({ disabled: nextProps.submitted });
    }

    componentWillUnmount() {
        localStorage.removeItem( 'employeeIdForEditAdjustment' );
    }

    /**
     * Extract initial state from props
     */
    getInitialStateFromProps = ({ adjustmentData }) => {
        if ( !Object.keys( adjustmentData ).length ) return;
        const initialData = {
            ...adjustmentData,
            amount: formatCurrency( adjustmentData.amount ),
            employee_id: adjustmentData.recipients.employees[ 0 ].recipient_id,
            date: adjustmentData.release_details[ 0 ].date ? formatDate( adjustmentData.release_details[ 0 ].date, DATE_FORMATS.DISPLAY ) : null,
            disburse_through_special_pay_run: !!adjustmentData.release_details[ 0 ].disburse_through_special_pay_run
        };

        if ( this.props.errors.editAdjustment ) {
            return;
        }

        this.setState({ adjustmentData: initialData });
    }

    getEmployeeFullName = () => {
        if ( this.props.formOptions.employees ) {
            const employee = this.props.formOptions.employees.find( ( e ) => e.id === parseInt(
                this.state.prepopulatedEmployee ? this.state.prepopulatedEmployee.id : this.state.adjustmentData.employee_id, 10
            ) );

            return employee ? `${employee.first_name} ${employee.last_name} ${employee.employee_id}` : '';
        }
    }

    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.input.includes( key ) || inputTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, label: this.state.errors[ key ][ 0 ] });
                } else if ( inputTypes.datePicker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    validateTypeahead = ( value ) => {
        if ( this.state.typeaheadTimeout ) {
            clearTimeout( this.state.typeaheadTimeout );
        }

        this.state.typeaheadTimeout = setTimeout( () => {
            if ( value.length >= 3 ) {
                this.props.getCompanyEmployees({
                    keyword: value.replace( /\d+/g, '' ),
                    formOptions: this.props.formOptions
                });
            }
        }, 1000 );

        if ( typeof value === 'string' && value !== this.getEmployeeFullName() ) {
            this.employee_id.setState({ value, error: true, errorMessage: 'This field is required' });
        }

        return true;
    }

    /**
     * Sends api request for editing adjustment
     */
    editAdjustment = () => {
        if ( this.validateAdjustmentData() ) {
            const data = {
                ...this.state.adjustmentData,
                recipients: {
                    employees: [
                        this.state.adjustmentData.employee_id
                    ]
                },
                release_details: [
                    {
                        id: this.state.adjustmentData.release_details[ 0 ].id,
                        date: formatDate( this.state.adjustmentData.date, DATE_FORMATS.API ),
                        disburse_through_special_pay_run: this.state.adjustmentData.disburse_through_special_pay_run,
                        status: this.state.adjustmentData.release_details[ 0 ].status
                    }
                ],
                amount: Number( stripNonDigit( this.state.adjustmentData.amount ) )
            };

            const { date, disburse_through_special_pay_run, employee_id, ...rest } = data; // eslint-disable-line
            this.props.editAdjustment({
                data: rest,
                id: this.props.params.id,
                prepopulatedEmployeeId: this.state.prepopulatedEmployeeId ? this.state.prepopulatedEmployeeId : '',
                previousRouteName: this.props.previousRoute.name
            });
        }
    };

    validateAdjustmentData() {
        let valid = true;

        if ( this.employee_id._validate( this.employee_id.state.value ) ) {
            valid = false;
        }

        if ( !this.type_id._checkRequire(
            typeof this.type_id.state.value === 'object'
            && this.type_id.state.value !== null ? this.type_id.state.value.value : this.type_id.state.value )
        ) {
            valid = false;
        }

        if ( this.amount._validate( stripNonDigit( this.amount.state.value ) ) ) {
            valid = false;
        }

        if ( stripNonDigit( this.amount.state.value ) === '0.00' ) {
            this.amount.setState({ error: true, errorMessage: 'Adjustment amount cannot be 0.' });

            valid = false;
        }

        if ( this.date.checkRequired() ) {
            valid = false;
        }

        if ( this.reason._validate( this.reason.state.value ) ) {
            valid = false;
        }

        return valid;
    }

    updateAdjustmentData( key, value ) {
        if ( typeof value === 'undefined' ) {
            return;
        }

        if ( inputTypes.datePicker.includes( key ) ) {
            this.state.adjustmentData[ key ] = value;
            return;
        }

        const dataClone = { ...this.state.adjustmentData };
        dataClone[ key ] = value !== '' && value !== null ? value : '';

        this.setState({ adjustmentData: Object.assign( this.state.adjustmentData, dataClone ) });
    }

    renderForm = () => {
        const disburseThroughSpecialPayRun = get( this.state.adjustmentData, 'release_details[0].disburse_through_special_pay_run', false );
        const includedInPayroll = Boolean( get( this.props.adjustmentData, 'release_details[0].payroll_id' ) );
        const disableEdit = disburseThroughSpecialPayRun && includedInPayroll;

        return this.state.adjustmentData && (
            <div>
                <div className="row">
                    <div className="col-xs-1" />
                    <div className="col-xs-5">
                        { !this.props.loading && (
                            <Typeahead
                                id="employee_id"
                                ref={ ( ref ) => { this.employee_id = ref; } }
                                defaultValue={ this.getEmployeeFullName() }
                                value={ this.getEmployeeFullName() }
                                label="Employee name or Employee ID"
                                required
                                disabled={ !!this.state.prepopulatedEmployeeId || disableEdit }
                                labelKey={ ( option ) => `${option.first_name} ${option.last_name} ${option.employee_id}` }
                                filterBy={ [ 'first_name', 'last_name', 'employee_id' ] }
                                placeholder="Start typing to search for an employee"
                                options={ this.props.formOptions.employees }
                                onChange={ ( value ) => {
                                    this.updateAdjustmentData( 'employee_id', value[ 0 ] ? value[ 0 ].id : null );
                                } }
                                onInputChange={ this.validateTypeahead }
                                onBlur={ ( value ) => {
                                    if ( typeof value === 'string' && value !== this.getEmployeeFullName() ) {
                                        this.employee_id.setState({ value, error: true, errorMessage: 'This field is required' });
                                    }
                                } }
                            />
                        ) }
                    </div>
                    <div className="col-xs-5">
                        <SalSelect
                            id="type_id"
                            label="Adjustment type"
                            required
                            key="type_id"
                            defaultSelected={ this.state.adjustmentData ? this.state.adjustmentData.type_id : '' }
                            data={ this.props.formOptions.adjustmentTypes }
                            value={ this.state.adjustmentData ? this.state.adjustmentData.type_id : '' }
                            placeholder="Choose a adjustment type"
                            ref={ ( ref ) => { this.type_id = ref; } }
                            onChange={ ({ value }) => { this.updateAdjustmentData( 'type_id', value ); } }
                            disabled={ disableEdit }
                        />
                    </div>
                    <div className="col-xs-1"></div>
                </div>
                <div className="row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-1"></div>
                    <div className="col-xs-5">
                        <Input
                            id="amount"
                            placeholder="Type Amount"
                            label="Amount"
                            required
                            ref={ ( ref ) => { this.amount = ref; } }
                            value={ this.state.adjustmentData.amount }
                            onChange={ ( value ) => { this.amount.setState({ value: stripNonDigit( value ) }); } }
                            onFocus={ () => { this.amount.setState({ value: stripNonDigit( this.amount.state.value ) }); } }
                            onBlur={ ( value ) => {
                                this.amount.setState({ value: formatCurrency( value ) }, () => {
                                    this.updateAdjustmentData( 'amount', formatCurrency( value ) );
                                });
                            } }
                        />
                    </div>
                    <div className="col-xs-5 date">
                        <DatePicker
                            label="Payroll Date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            required
                            disabled={ disableEdit }
                            selectedDay={ this.state.adjustmentData.date }
                            ref={ ( ref ) => { this.date = ref; } }
                            onChange={ ( value ) => { this.updateAdjustmentData( 'date', value ); } }
                        />
                        <Switch
                            ref={ ( ref ) => { this.disburse_through_special_pay_run = ref; } }
                            defaultChecked={ !!this.state.adjustmentData.disburse_through_special_pay_run }
                            onChange={ ( value ) => { this.updateAdjustmentData( 'disburse_through_special_pay_run', value ); } }
                            disabled={ disableEdit }
                        />
                        <span style={ { marginLeft: '10px' } }>Include to special pay run?</span>
                    </div>
                    <div className="col-xs-1"></div>
                </div>
                <div className="row">
                    <div className="col-xs-1"></div>
                    <div className="col-xs-5">
                        <Input
                            id="reason"
                            placeholder="Type your reason here"
                            label="Reason"
                            required
                            value={ this.state.adjustmentData.reason }
                            ref={ ( ref ) => { this.reason = ref; } }
                            onChange={ ( value ) => { this.updateAdjustmentData( 'reason', value ); } }
                        />
                    </div>
                </div>
            </div>
        );
    }

    /**
     * Component render method
     */
    render() {
        const { loading } = this.props;
        return (
            <PageWrapper>
                <Helmet
                    title="Edit Adjustment"
                    meta={ [
                        { name: 'description', content: 'Edit a Adjustment' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/adjustments' );
                            } }
                        >
                            &#8592; Back to { this.props.previousRoute.name ? this.props.previousRoute.name : 'Adjustments' }
                        </A>
                    </Container>
                </NavWrapper>
                { loading
                    ? (
                        <LoaderWrapper>
                            <H2>Loading</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoaderWrapper>
                    ) : (
                        <div>
                            <HeadingWrapper>
                                <h3>Edit Adjustment</h3>
                                <p>
                                    You may edit and manage the employee adjustment through this page.
                                </p>
                            </HeadingWrapper>
                            <Container>
                                { this.renderForm() }
                            </Container>
                            <AlignRight>
                                <div className="foot">
                                    <Button
                                        label="Cancel"
                                        type="neutral"
                                        size="large"
                                        onClick={ () => {
                                            this.props.previousRoute.name
                                            ? browserHistory.goBack()
                                            : browserHistory.push( '/adjustments' );
                                        } }
                                    />
                                    <Button
                                        className={ this.state.permission.edit ? '' : 'hide' }
                                        ref={ ( ref ) => { this.submitButton = ref; } }
                                        label={ this.props.submitted ? <Loader /> : 'Update' }
                                        type="action"
                                        size="large"
                                        onClick={ this.editAdjustment }
                                        disabled={ this.props.submitted }
                                    />
                                </div>
                            </AlignRight>
                        </div>
                    )
                }
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    submitted: makeSelectSubmitted(),
    errors: makeSelectApiErrors(),
    formOptions: makeSelectFormOptions(),
    adjustmentData: makeSelectAdjustmentData(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Edit );
