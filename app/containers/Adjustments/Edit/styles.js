import styled from 'styled-components';

const PageWrapper = styled.div`
    .date {
        .DayPickerInput {
            width: 100%;
        }

        input {
            width: 100%;
            padding-top: 0px !important;
        }
    }

    .title {
        display: flex;
        align-items: center;
        margin-bottom: 20px;

        h5 {
            margin: 0;
            margin-right: 20px;
            font-weight: 600;
            font-size: 22px;
        }

        .search-wrapper {
            flex-grow: 1;


            .search {
                width: 300px;
                border: 1px solid #333;
                border-radius: 30px;

                input {
                    border: none;
                }
            }

            p {
                display: none;
            }

            .input-group,
            .form-control {
                background-color: transparent;
            }

            .input-group-addon {
                background-color: transparent;
                border: none;
            }

            .isvg {
                display: inline-block;
                width: 1rem;
            }
        }
    }
`;

const NavWrapper = styled.div`
    margin-bottom: 50px;
    padding: 10px 0;
    background: #f0f4f6;

    svg {
        height: 15px;
        margin-right: 10px;
    }
`;

const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;

    h3 {
        font-weight: 600;
    }

    p {
        text-align: center;
        max-width: 800px;
    }
`;

const LoaderWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

const AlignRight = styled.div`
    text-align: right;

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        margin-top: 20px;

        button {
            min-width: 120px;
        }
    }
`;

export {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    LoaderWrapper,
    AlignRight
};
