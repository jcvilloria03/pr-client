import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectEditAdjustmentDomain = () => ( state ) => state.get( 'editAdjustment' );

const makeSelectLoading = () => createSelector(
    selectEditAdjustmentDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectSubmitted = () => createSelector(
    selectEditAdjustmentDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectApiErrors = () => createSelector(
    selectEditAdjustmentDomain(),
    ( substate ) => substate.get( 'apiErrors' ).toJS()
);

const makeSelectFormOptions = () => createSelector(
    selectEditAdjustmentDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectAdjustmentData = () => createSelector(
    selectEditAdjustmentDomain(),
    ( substate ) => substate.get( 'adjustmentData' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectEditAdjustmentDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectApiErrors,
    makeSelectFormOptions,
    makeSelectAdjustmentData,
    makeSelectNotification
};
