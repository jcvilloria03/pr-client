import startCase from 'lodash/startCase';
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeLatest, takeEvery, delay } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { browserHistory } from '../../../utils/BrowserHistory';
import { transformData, formatFeedbackMessage } from '../../../utils/functions';
import { RECORD_UPDATED_MESSAGE } from '../../../utils/constants';

import {
    INITIALIZE,
    GET_COMPANY_EMPLOYEES,
    SET_LOADING,
    SET_SUBMITTED,
    SET_API_ERRORS,
    SET_FORM_OPTIONS,
    SET_ADJUSTMENT_DATA,
    EDIT_ADJUSTMENT,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formOptions = {};
        const response = yield call( Fetch, `/other_income/${payload.id}` );
        const adjustmentTypes = yield call( Fetch, `/company/${companyId}/other_income_types/adjustment_type`, { method: 'GET' });
        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&filter[employee_ids][]=${response.recipients.employees[ 0 ].recipient_id}`, { method: 'GET' });

        formOptions.employees = employees.data.filter( ( employee ) => employee.payroll );
        formOptions.adjustmentTypes = adjustmentTypes.data
            .filter( ( type ) => !type.deleted_at || response.type.id === type.id )
            .map( ( value ) => ({ label: startCase( value.name.toLowerCase() ), value: value.id }) );

        yield put({
            type: SET_ADJUSTMENT_DATA,
            payload: response
        });

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Get Company Employees
 */
export function* getCompanyEmployees({ payload }) {
    try {
        const keyword = payload.keyword ? payload.keyword : '';
        const companyId = company.getLastActiveCompanyId();
        const formOptions = payload.formOptions;
        let filterEmployeeIdsQueryString = '';

        if ( payload.filterEmployeeIds ) {
            const filterEmployeeIds = payload.filterEmployeeIds;
            if ( filterEmployeeIds.length > 0 ) {
                for ( const employeeId of filterEmployeeIds ) {
                    filterEmployeeIdsQueryString += `&filter[employee_ids][]=${employeeId}`;
                }
            }
        }

        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&keyword=${keyword}${filterEmployeeIdsQueryString}`, { method: 'GET' });

        formOptions.employees = employees.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}
/**
 * Edit adjustment
 */
export function* editAdjustment({ payload }) {
    try {
        yield put({
            type: SET_SUBMITTED,
            payload: true
        });

        yield put({
            type: SET_API_ERRORS,
            payload: {
                editAdjustment: false
            }
        });

        // Transform data to remove null keys
        const data = transformData( payload.data );

        yield call( Fetch, `/philippine/adjustment/${payload.id}`, {
            method: 'PATCH',
            data
        });

        yield call( showSuccessMessage );
        yield call( delay, 500 );
        yield call( resetStore );

        payload.previousRouteName ? yield call( browserHistory.goBack ) : yield call( browserHistory.push, '/adjustments' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });

        yield put({
            type: SET_API_ERRORS,
            payload: {
                editAdjustment: true
            }
        });
    } finally {
        yield put({
            type: SET_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    yield call( resetStore );
    yield call( initializeData );
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Watcher for INITIALIZE
 */
export function* watcherForInitializeData() {
    const watcher = yield takeLatest( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for edit adjustment
 */
export function* watchForEditAdjustment() {
    const watcher = yield takeLatest( EDIT_ADJUSTMENT, editAdjustment );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for reinitialize page
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_COMPANY_EMPLOYEES
 */
export function* watchForGetCompanyEmployees() {
    const watcher = yield takeEvery( GET_COMPANY_EMPLOYEES, getCompanyEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export default [
    watcherForInitializeData,
    watchForGetCompanyEmployees,
    watchForEditAdjustment,
    watchForNotifyUser,
    watchForReinitializePage
];
