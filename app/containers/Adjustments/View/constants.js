import { PAYMENT_SCHEME_LABELS } from '../../Loans/constants';

export const SET_LOADING = 'app/Adjustments/View/SET_LOADING';
export const SET_DOWNLOADING = 'app/Adjustments/View/SET_DOWNLOADING';
export const GET_ADJUSTMENTS = 'app/Adjustments/View/GET_ADJUSTMENTS';
export const GET_PAGINATED_ADJUSTMENTS = 'app/Adjustments/View/GET_PAGINATED_ADJUSTMENTS';
export const SET_PAGINATION = 'app/Adjustments/View/SET_PAGINATION';
export const SET_ADJUSTMENTS = 'app/Adjustments/View/SET_ADJUSTMENTS';
export const SET_FILTER_DATA = 'app/Adjustments/View/SET_FILTER_DATA';
export const DELETE_ADJUSTMENTS = 'app/Adjustments/View/DELETE_ADJUSTMENTS';
export const NOTIFICATION = 'app/Adjustments/View/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/Adjustments/View/NOTIFICATION_SAGA';
export const EXPORT_ADJUSTMENTS = 'app/Adjustments/View/EXPORT_ADJUSTMENTS';

export const PAY_FREQUENCIES = PAYMENT_SCHEME_LABELS;
