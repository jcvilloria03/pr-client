import React from 'react';
import orderBy from 'lodash/orderBy';
import isEqual from 'lodash/isEqual';
import startCase from 'lodash/startCase';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { subscriptionService } from '../../../utils/SubscriptionService';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate
} from '../../../utils/functions';

import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';

import {
    makeSelectLoading,
    makeSelectAdjustments,
    makeSelectFilterData,
    makeSelectNotification,
    makeSelectDownloading,
    makeSelectPagination
} from './selectors';
import * as adjustmentsActions from './actions';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';

import SubHeader from '../../SubHeader';
import Loader from '../../../components/Loader';
import SnackBar from '../../../components/SnackBar';
import Table from '../../../components/Table';
import { H2, H3, H5 } from '../../../components/Typography';
import Button from '../../../components/Button';
import Input from '../../../components/Input';
import SalConfirm from '../../../components/SalConfirm';
import Icon from '../../../components/Icon';
import Filter from './templates/filter';

/**
 * View Adjustments Component
 */
export class View extends React.Component {
    static propTypes = {
        getAdjustments: React.PropTypes.func,
        getPaginatedAdjustments: React.PropTypes.func,
        deleteAdjustments: React.PropTypes.func,
        exportAdjustments: React.PropTypes.func,
        adjustments: React.PropTypes.array,
        pagination: React.PropTypes.object,
        filterData: React.PropTypes.shape({
            adjustmentTypes: React.PropTypes.array,
            positions: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        loading: React.PropTypes.bool,
        downloading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    };

    static defaultProps = {
        loading: true,
        downloading: false,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: this.props.adjustments,
            showDelete: false,
            deleteLabel: '',
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            showModal: false,
            showFilter: false,
            hasFiltersApplied: false,
            previousQuery: '',
            filtersApplied: []
        };

        this.searchInput = null;
        this.searchTypingTimeout = 0;
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.adjustments',
            'create.adjustments',
            'delete.adjustments',
            'edit.adjustments'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.adjustments' ];

            if ( authorized ) {
                this.setState({ permission: {
                    view: authorization[ 'view.adjustments' ],
                    create: authorization[ 'create.adjustments' ],
                    delete: authorization[ 'delete.adjustments' ],
                    edit: authorization[ 'edit.adjustments' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.getAdjustments();
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.adjustments, this.props.adjustments ) ) {
            this.handleSearch( nextProps.adjustments );
        }
    }

    handleTableChanges = ( tableProps = this.adjustmentsTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });

        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    handleSearch = ( adjustments = this.props.adjustments ) => {
        let searchQuery = null;

        const dataToDisplay = adjustments;

        if ( this.searchInput ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery || ( this.searchInput && searchQuery === '' ) ) {
            const currentFilters = this.state.filtersApplied.filter( ( filter ) => filter.type !== 'keyword' );
            const filtersToApply = [
                ...currentFilters,
                {
                    disabled: false,
                    type: 'keyword',
                    value: searchQuery
                }
            ];
            if ( this.searchTypingTimeout !== 0 ) {
                clearTimeout( this.searchTypingTimeout );
            }
            this.searchTypingTimeout = setTimeout( () => {
                if ( searchQuery !== '' || this.state.previousQuery !== '' ) {
                    this.props.getPaginatedAdjustments({ page: 1, perPage: this.props.pagination.per_page, showLoading: false }, filtersToApply );
                }

                this.setState({
                    previousQuery: searchQuery,
                    filtersApplied: filtersToApply,
                    displayedData: this.props.adjustments,
                    hasFiltersApplied: true
                }, () => {
                    this.handleTableChanges();
                });
            }, 500 );
        } else {
            this.setState({ displayedData: dataToDisplay }, () => {
                this.handleTableChanges();
            });
        }
    }

    deleteAdjustments = () => {
        const adjustmentIDs = [];
        this.adjustmentsTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                adjustmentIDs.push( this.adjustmentsTable.props.data[ index ].id );
            }
        });
        this.props.deleteAdjustments( adjustmentIDs );
        this.setState({ showDelete: false });
    }

    exportAdjustments = () => {
        const selectedAdjustments = [];

        const sortingOptions = this.adjustmentsTable.state.sorting;

        this.adjustmentsTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedAdjustments.push( this.adjustmentsTable.props.data[ index ]);
            }
        });

        const sortedAdjustments = orderBy(
            selectedAdjustments,
            sortingOptions.map( ( o ) => o.id ),
            sortingOptions.map( ( o ) => ( o.desc ? 'desc' : 'asc' ) )
        );

        this.props.exportAdjustments( sortedAdjustments.map( ( adjustment ) => adjustment.id ) );
    };

    toggleFilter = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    applyFilters = ( filters ) => {
        this.props.getAdjustments( filters );
        this.setState({
            filtersApplied: filters,
            hasFiltersApplied: !!filters.length
        }, () => {
            this.handleTableChanges();
        });
    }

    resetFilters = () => {
        this.setState({
            showFilter: false,
            displayedData: this.props.adjustments,
            hasFiltersApplied: false
        });
    }

    /**
     * Component Render Method
     */
    render() {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'employee_name',
                header: 'Employee Name',
                sortable: false,
                accessor: ( row ) => row.employee_name,
                minWidth: 200
            },
            {
                id: 'employee_id',
                header: 'Employee ID',
                sortable: false,
                accessor: ( row ) => row.employee_id,
                minWidth: 200
            },
            {
                id: 'reason',
                header: 'Reason',
                sortable: false,
                accessor: ( row ) => row.reason,
                minWidth: 200
            },
            {
                id: 'type_name',
                sortable: false,
                accessor: ( row ) => startCase( row.type.name.toLowerCase() ),
                header: 'Type',
                minWidth: 260
            },
            {
                header: 'Amount',
                accessor: 'amount',
                sortable: false,
                minWidth: 260,
                render: ({ row }) => (
                    <div>
                        Php { formatCurrency( row.amount ) }
                    </div>
                )
            },
            {
                id: 'payroll_date',
                header: 'Payroll Date',
                sortable: false,
                accessor: ( row ) => ( row.release_details.length > 0 && row.release_details[ 0 ].date ? formatDate( row.release_details[ 0 ].date, DATE_FORMATS.DISPLAY ) : 'N/A' ),
                minWidth: 200
            },
            {
                id: 'disburse_through_special_pay_run',
                header: 'Special Pay Run',
                sortable: false,
                accessor: ( row ) => ( row.release_details.length > 0 && row.release_details[ 0 ].disburse_through_special_pay_run ? 'Yes' : 'No' ),
                minWidth: 180
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    row.release_details.length > 0 && !row.release_details[ 0 ].status ? (
                        <Button
                            className={ this.state.permission.edit ? '' : 'hide' }
                            label={ <span>Edit</span> }
                            type="grey"
                            size="small"
                            to={ `/adjustments/${row.id}/edit` }
                        />
                    ) : null
                )
            }
        ];

        const dataForDisplay = this.state.displayedData;

        return (
            <div>
                <Helmet
                    title="View Adjustments"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.deleteAdjustments }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Are you sure you want to delete these adjustments?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Adjustments"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Adjustments.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Adjustments</H3>
                                <p>Add employee adjustments through this page. Remember that adjustments added here will only take effect on the payroll date indicated.</p>
                                <div style={ { textAlign: 'center' } } >
                                    <div className="main">
                                        <Button
                                            className={ this.state.permission.create ? '' : 'hide' }
                                            label="Add Adjustments"
                                            size="large"
                                            type="action"
                                            to="/adjustments/add"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="title">
                                <H5>Adjustments List</H5>
                                <div className="search-wrapper">
                                    <Input
                                        className="search"
                                        id="search"
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                        onChange={ () => { this.handleSearch(); } }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                    />
                                </div>
                                <span>
                                    {this.state.showDelete ? (
                                        <div>
                                            { this.state.deleteLabel }
                                            &nbsp;

                                            <Button
                                                className={ this.state.permission.view ? '' : 'hide' }
                                                label={ this.props.downloading ? <Loader /> : <span>Download</span> }
                                                type="neutral"
                                                onClick={ this.exportAdjustments }
                                            />
                                            <Button
                                                className={ this.state.permission.delete ? '' : 'hide' }
                                                label={ <span>Delete</span> }
                                                type="danger"
                                                onClick={ () => {
                                                    this.setState({ showModal: false }, () => {
                                                        this.setState({ showModal: true });
                                                    });
                                                } }
                                            />
                                        </div>
                                    ) : (
                                        <div>
                                            { this.state.label }
                                            &nbsp;
                                            <Button
                                                label={ <span><Icon className="filter-icon" name="filter" /> Filter</span> }
                                                type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                                onClick={ this.toggleFilter }
                                            />
                                        </div>
                                    )}
                                </span>
                            </div>
                            <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                                <Filter
                                    filterData={ this.props.filterData }
                                    onCancel={ () => { this.resetFilters(); } }
                                    onApply={ ( values ) => { this.applyFilters( values ); } }
                                />
                            </div>
                            <Table
                                data={ dataForDisplay }
                                columns={ tableColumns }
                                pagination
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.adjustmentsTable = ref; } }
                                selectable
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;

                                    this.setState({
                                        showDelete: selectionLength > 0,
                                        deleteLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                                onPageChange={ ( data ) => this.props.getPaginatedAdjustments({ page: data + 1, perPage: this.props.pagination.per_page, showLoading: true }, this.state.filtersApplied ) }
                                onPageSizeChange={ ( data ) => this.props.getPaginatedAdjustments({ page: 1, perPage: data, showLoading: true }, this.state.filtersApplied ) }
                                page={ this.props.pagination.current_page - 1 }
                                pageSize={ this.props.pagination.per_page }
                                pages={ this.props.pagination.last_page }
                                manual
                            />
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    downloading: makeSelectDownloading(),
    adjustments: makeSelectAdjustments(),
    filterData: makeSelectFilterData(),
    notification: makeSelectNotification(),
    pagination: makeSelectPagination()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        adjustmentsActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
