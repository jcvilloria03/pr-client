export const FILTER_TYPES = {
    ADJUSTMENT_TYPE: 'adjustment_type',
    LOCATION: 'location',
    POSITION: 'position',
    DEPARTMENT: 'department'
};
