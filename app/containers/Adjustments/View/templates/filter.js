import React from 'react';
import startCase from 'lodash/startCase';
import MultiSelect from '../../../../components/MultiSelect';
import Button from '../../../../components/Button';

import { FilterWrapper } from './styles';

import { FILTER_TYPES } from './constants';
import { DATE_FORMATS } from '../../../../utils/constants';
import DatePicker from '../../../../components/DatePicker';
import { formatDate } from '../../../../utils/functions';
/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {

    static propTypes = {
        filterData: React.PropTypes.shape({
            adjustmentTypes: React.PropTypes.arr,
            payrollDates: React.PropTypes.instanceOf( Date ),
            positions: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    onApply = () => {
        const filters = [];
        this.adjustmentTypes.state.value && this.adjustmentTypes.state.value.forEach( ( adjustmentType ) => {
            filters.push( Object.assign( adjustmentType, { type: FILTER_TYPES.ADJUSTMENT_TYPE }) );
        });
        this.locations.state.value && this.locations.state.value.forEach( ( location ) => {
            filters.push( Object.assign( location, { type: FILTER_TYPES.LOCATION }) );
        });
        this.departments.state.value && this.departments.state.value.forEach( ( department ) => {
            filters.push( Object.assign( department, { type: FILTER_TYPES.DEPARTMENT }) );
        });
        this.positions.state.value && this.positions.state.value.forEach( ( position ) => {
            filters.push( Object.assign( position, { type: FILTER_TYPES.POSITION }) );
        });
        this.payrollDates.state.selectedDay && filters.push({
            disabled: true,
            label: 'Payroll Date',
            type: 'payroll_date',
            value: formatDate( this.payrollDates.state.selectedDay, DATE_FORMATS.API )
        });
        this.props.onApply( filters );
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    getAdjustmentTypes = () => {
        if ( !this.props.filterData.adjustmentTypes ) {
            return [];
        }

        const adjustmentTypes = this.props.filterData.adjustmentTypes.map( ( adjustmentType ) => (
            this.formatDataForMultiselect( Object.assign( adjustmentType, { name: startCase( adjustmentType.name.toLowerCase() ) }) )
        ) );

        return adjustmentTypes;
    }

    getLocations = () => {
        if ( !this.props.filterData.locations ) {
            return [];
        }

        const locations = this.props.filterData.locations.map( ( location ) => (
            this.formatDataForMultiselect( location )
        ) );

        return locations;
    }

    getDepartments = () => {
        if ( !this.props.filterData.departments ) {
            return [];
        }

        const departments = this.props.filterData.departments.map( ( department ) => (
            this.formatDataForMultiselect( department )
        ) );

        return departments;
    }

    getPositions = () => {
        if ( !this.props.filterData.positions ) {
            return [];
        }

        const positions = this.props.filterData.positions.map( ( position ) => (
            this.formatDataForMultiselect( position )
        ) );

        return positions;
    }

    resetFilters = () => {
        this.payrollDates.state.selectedDay = '';
        this.adjustmentTypes.setState({ value: null });
        this.locations.setState({ value: null });
        this.departments.setState({ value: null });
        this.positions.setState({ value: null }, () => {
            this.onApply();
        });
    }

    formatDataForMultiselect = ( data ) => (
        {
            value: data.id,
            label: data.name,
            disabled: false
        }
    )

    render() {
        return (
            <FilterWrapper>
                <div className="row">
                    <div className="col-xs-3">
                        <MultiSelect
                            id="adjustment_types"
                            label={
                                <span>Adjustment Types</span>
                            }
                            ref={ ( ref ) => { this.adjustmentTypes = ref; } }
                            data={ this.getAdjustmentTypes() }
                            placeholder="All adjustment types"
                        />
                    </div>
                    <div className="col-xs-3 payroll_date">
                        <DatePicker
                            id="payroll_date"
                            label={
                                <span>Payroll Date</span>
                            }
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            selectedDay={ this.payrollDates }
                            ref={ ( ref ) => { this.payrollDates = ref; } }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-3">
                        <MultiSelect
                            id="locations"
                            label={
                                <span>Locations</span>
                            }
                            ref={ ( ref ) => { this.locations = ref; } }
                            data={ this.getLocations() }
                            placeholder="All locations"
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            id="departments"
                            label={
                                <span>Departments</span>
                            }
                            ref={ ( ref ) => { this.departments = ref; } }
                            data={ this.getDepartments() }
                            placeholder="All departments"
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            id="positions"
                            label={
                                <span>Positions</span>
                            }
                            ref={ ( ref ) => { this.positions = ref; } }
                            data={ this.getPositions() }
                            placeholder="All positions"
                        />
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
