import { createSelector } from 'reselect';

/**
 * Direct selector to the adjustments state domain
 */
const selectViewAdjustmentsDomain = () => ( state ) => state.get( 'adjustments' );

const makeSelectLoading = () => createSelector(
    selectViewAdjustmentsDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectDownloading = () => createSelector(
    selectViewAdjustmentsDomain(),
    ( substate ) => substate.get( 'downloading' )
);

const makeSelectAdjustments = () => createSelector(
    selectViewAdjustmentsDomain(),
  ( substate ) => substate.get( 'adjustments' ).toJS()
);

const makeSelectFilterData = () => createSelector(
    selectViewAdjustmentsDomain(),
  ( substate ) => substate.get( 'filterData' ).toJS()
);

const makeSelectPagination = () => createSelector(
  selectViewAdjustmentsDomain(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewAdjustmentsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectDownloading,
  makeSelectAdjustments,
  makeSelectFilterData,
  makeSelectNotification,
  makeSelectPagination
};
