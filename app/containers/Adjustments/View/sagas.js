import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import fileDownload from 'js-file-download';

import {
    SET_LOADING,
    GET_PAGINATED_ADJUSTMENTS,
    SET_PAGINATION,
    GET_ADJUSTMENTS,
    SET_ADJUSTMENTS,
    SET_FILTER_DATA,
    DELETE_ADJUSTMENTS,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    EXPORT_ADJUSTMENTS,
    SET_DOWNLOADING
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

/**
 * Get adjustments for table
 */
export function* getAdjustments({ filters }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        yield call( getPaginatedAdjustments, {
            payload: {
                page: 1,
                perPage: 10,
                showLoading: true
            },
            filters
        });

        const filterData = {};
        const adjustmentTypes = yield call( Fetch, `/company/${companyId}/other_income_types/adjustment_type`, { method: 'GET' });
        const locations = yield call( Fetch, `/philippine/company/${companyId}/locations`, { method: 'GET' });
        const departments = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });
        const positions = yield call( Fetch, `/company/${companyId}/positions`, { method: 'GET' });

        filterData.adjustmentTypes = adjustmentTypes.data;
        filterData.locations = locations.data;
        filterData.departments = departments.data;
        filterData.positions = positions.data;

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * get paginated loans
 */
export function* getPaginatedAdjustments({ payload, filters }) {
    const { page, perPage, showLoading } = payload;

    try {
        if ( showLoading ) {
            yield put({
                type: SET_LOADING,
                payload: true
            });
        }

        let filtersQueryParams = '';
        if ( filters !== undefined && filters.length > 0 ) {
            const filtersIds = {
                other_income_type_ids: [],
                location_ids: [],
                department_ids: [],
                position_ids: []
            };

            filters.map( ( filter ) => {
                if ( filter.type !== 'keyword' ) {
                    const filterFieldName = filter.type === 'adjustment_type' ? 'other_income_type_ids' : `${filter.type}_ids`;
                    if ( filter.disabled === false ) {
                        filtersIds[ filterFieldName ].push( filter.value );
                    }
                } else if ( filter.value ) {
                    filtersQueryParams += `&filter[keyword]=${filter.value}`;
                } else {
                    filtersQueryParams += `&filter[payroll_date]=${filters[ 0 ].value}`;
                }

                return null;
            });

            for ( const [ field, ids ] of Object.entries( filtersIds ) ) {
                if ( ids.length > 0 ) {
                    for ( const filterId of ids ) {
                        filtersQueryParams += `&filter[${field}][]=${filterId}`;
                    }
                }
                if ( filters[ filters.length - 1 ].type === 'payroll_date' ) {
                    filtersQueryParams += `&filter[payroll_date]=${filters[ filters.length - 1 ].value}`;
                }
            }
        }

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_incomes/adjustment_type?page=${page}&per_page=${perPage}${filtersQueryParams}` );
        const { data, ...others } = response;
        let adjustmentsData = [];
        if ( data.length > 0 ) {
            const employeeIdFiltersQueryParams = [];
            data.map( ( adjustment ) => {
                employeeIdFiltersQueryParams.push( `filter[employee_ids][]=${adjustment.recipients.employees[ 0 ].recipient_id}` );
                return null;
            });
            const employeeIdsQueryString = employeeIdFiltersQueryParams.filter( ( v, i, a ) => a.indexOf( v ) === i ).join( '&' );
            const employees = yield call( Fetch, `/company/${companyId}/employees?${employeeIdsQueryString}`, { method: 'GET' });
            adjustmentsData = data.map( ( adjustment ) => {
                const employee = employees.data.find( ( e ) => e.id === adjustment.recipients.employees[ 0 ].recipient_id );
                delete employee.recipients;
                return { ...adjustment,
                    employee_id: employee.employee_id,
                    employee_name: `${employee.first_name} ${employee.last_name}`,
                    employee_location_id: employee.location_id,
                    employee_position_id: employee.position_id,
                    employee_department_id: employee.department_id
                };
            });
        }

        yield put({
            type: SET_PAGINATION,
            payload: others
        });

        yield put({
            type: SET_ADJUSTMENTS,
            payload: adjustmentsData
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        if ( showLoading ) {
            yield put({
                type: SET_LOADING,
                payload: false
            });
        }
    }
}

/**
 * Batch delete adjustments
 */
export function* deleteAdjustments({ payload }) {
    try {
        yield call( Fetch, `/company/${company.getLastActiveCompanyId()}/other_income`, {
            method: 'DELETE',
            data: { ids: payload }
        });
        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully deleted',
            show: true,
            type: 'success'
        });
        yield call( getAdjustments, {
            payload: {
                showLoading: true
            },
            filters: []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Export adjustments
 */
export function* exportAdjustments({ payload }) {
    try {
        yield put({
            type: SET_DOWNLOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_incomes/adjustment_type/download`, {
            method: 'POST',
            data: { ids: payload }
        });

        fileDownload( response, 'adjustments.csv' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DOWNLOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getAdjustments );
}

/**
 * Watcher for GET adjustments
 */
export function* watchForGetAdjustments() {
    const watcher = yield takeEvery( GET_ADJUSTMENTS, getAdjustments );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Paginated adjustments
 */
export function* watchForPaginatedAdjustments() {
    const watcher = yield takeEvery( GET_PAGINATED_ADJUSTMENTS, getPaginatedAdjustments );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE adjustments
 */
export function* watchForDeleteAdjustments() {
    const watcher = yield takeEvery( DELETE_ADJUSTMENTS, deleteAdjustments );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for EXPORT adjustments
 */
export function* watchForExportAdjustments() {
    const watcher = yield takeEvery( EXPORT_ADJUSTMENTS, exportAdjustments );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetAdjustments,
    watchForDeleteAdjustments,
    watchForExportAdjustments,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForPaginatedAdjustments
];
