import {
    GET_ADJUSTMENTS,
    GET_PAGINATED_ADJUSTMENTS,
    DELETE_ADJUSTMENTS,
    NOTIFICATION,
    EXPORT_ADJUSTMENTS
} from './constants';

/**
 * Fetch list of adjustments
 */
export function getAdjustments( filters ) {
    return {
        type: GET_ADJUSTMENTS,
        filters
    };
}

/**
 * Fetch list of adjustments
 */
export function getPaginatedAdjustments( payload, filters ) {
    return {
        type: GET_PAGINATED_ADJUSTMENTS,
        filters,
        payload,
        showLoading: true
    };
}

/**
 * Delete adjustments
 * @param {array} IDs
 */
export function deleteAdjustments( IDs ) {
    return {
        type: DELETE_ADJUSTMENTS,
        payload: IDs
    };
}

/**
 * Export adjustments
 * @param {array} IDs
 */
export function exportAdjustments( IDs ) {
    return {
        type: EXPORT_ADJUSTMENTS,
        payload: IDs
    };
}

/**
 * Display a notification to a user
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
