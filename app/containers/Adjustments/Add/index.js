import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../components/A';
import Icon from '../../../components/Icon';
import Toggle from '../../../components/Toggle';
import SnackBar from '../../../components/SnackBar';
import SubHeader from '../../../containers/SubHeader';

import BatchUpload from './BatchUpload';
import ManualEntry from './ManualEntry';

import { makeSelectNotification } from './selectors';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper
} from './styles';

import { browserHistory } from '../../../utils/BrowserHistory';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';

const TOGGLE_OPTIONS = {
    BATCH: 'BATCH',
    MANUAL: 'MANUAL'
};

/**
 * Add Loans Component
 */
class Add extends Component {
    static propTypes = {
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            selectedToggleOption: TOGGLE_OPTIONS.MANUAL
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    componentWillUnmount() {
        localStorage.removeItem( 'prepopulatedEmployeeForAddAdjustment' );
    }

    /**
     * Returns the list of toggle options
     */
    getToggleOptions = () => ([
        {
            title: 'Batch Upload',
            subtext: 'Upload multiple entries at once using the template.',
            value: TOGGLE_OPTIONS.BATCH,
            icon: <Icon name="batchUpload" />
        },
        {
            title: 'Manual Entry',
            subtext: 'Create entries one by one.',
            value: TOGGLE_OPTIONS.MANUAL,
            icon: <Icon name="manualEntry" />
        }
    ]);

    /**
     * Component Render Method
     */
    render() {
        const { selectedToggleOption } = this.state;

        return (
            <PageWrapper>
                <Helmet
                    title="Add Adjustments"
                    meta={ [
                        { name: 'description', content: 'Add adjustments' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/adjustments' );
                            } }
                        >
                            &#8592; Back to { this.props.previousRoute.name ? this.props.previousRoute.name : 'Adjustments' }
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <HeadingWrapper>
                        <h3>Add Adjustments</h3>
                        <p>
                            You may add adjustments via manual entry and batch upload.
                        </p>
                    </HeadingWrapper>
                    { !localStorage.getItem( 'prepopulatedEmployeeForAddAdjustment' ) ? (
                        <Toggle
                            options={ this.getToggleOptions() }
                            defaultSelected={ TOGGLE_OPTIONS.MANUAL }
                            onChange={ ( value ) => {
                                this.setState({
                                    selectedToggleOption: value
                                });
                            } }
                        /> ) : null
                    }
                </Container>
                <FormWrapper>
                    { selectedToggleOption === TOGGLE_OPTIONS.BATCH
                        ? <BatchUpload />
                        : <ManualEntry previousRoute={ this.props.previousRoute } />
                    }
                </FormWrapper>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        {},
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Add );
