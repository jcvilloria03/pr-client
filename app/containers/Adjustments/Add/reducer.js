import { fromJS } from 'immutable';
import {
    LOADING,
    SET_FORM_OPTIONS,
    SET_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    RESET_ADJUSTMENTS_PREVIEW,
    SET_ADJUSTMENTS_PREVIEW_STATUS,
    SET_ADJUSTMENTS_PREVIEW_DATA,
    SET_SAVING_STATUS,
    SET_SAVING_ERRORS,
    SUBMITTED,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    errors: {},
    submitted: false,
    batchUploadJobId: null,
    batchUploadStatus: '',
    batchUploadErrors: {},
    adjustmentsPreview: {
        status: 'ready',
        data: []
    },
    saving: {
        status: '',
        errors: {}
    },
    adjustment: {
        type_id: null
    },
    formOptions: {
        adjustmentTypes: [],
        employees: [],
        payrollGroups: []
    },
    adjustmentTypes: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Manual or batch assign adjustments
 *
 */
function addAdjustment( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_BATCH_UPLOAD_JOB_ID:
            return state.set( 'batchUploadJobId', action.payload );
        case SET_BATCH_UPLOAD_STATUS:
            return state.set( 'batchUploadStatus', action.payload );
        case SET_BATCH_UPLOAD_ERRORS:
            return state.set( 'batchUploadErrors', action.payload );
        case SET_ADJUSTMENTS_PREVIEW_STATUS:
            return state.setIn([ 'adjustmentsPreview', 'status' ], action.payload );
        case SET_ADJUSTMENTS_PREVIEW_DATA:
            return state.setIn([ 'adjustmentsPreview', 'data' ], fromJS( action.payload ) );
        case SET_SAVING_STATUS:
            return state.setIn([ 'saving', 'status' ], action.payload );
        case SET_SAVING_ERRORS:
            return state.setIn([ 'saving', 'errors' ], action.payload );
        case RESET_ADJUSTMENTS_PREVIEW:
            return state.set( 'preview', fromJS({
                status: 'ready',
                data: []
            }) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addAdjustment;
