import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectAddAdjustmentDomain = () => ( state ) => state.get( 'addAdjustment' );

const makeSelectAdjustment = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => substate.get( 'adjustment' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectFormOptions = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectAdjustmentsPreview = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => substate.get( 'adjustmentsPreview' ).toJS()
);

const makeSelectSaving = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectAdjustmentTypes = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => substate.get( 'adjustmentTypes' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectAddAdjustmentDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectBatchUploadErrors,
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectAdjustment,
    makeSelectAdjustmentTypes,
    makeSelectAdjustmentsPreview,
    makeSelectErrors,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSaving,
    makeSelectSubmitted
};
