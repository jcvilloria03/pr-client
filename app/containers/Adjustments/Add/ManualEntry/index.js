import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import Typeahead from '../../../../components/Typeahead';
import SnackBar from '../../../../components/SnackBar';
import SalConfirm from '../../../../components/SalConfirm';
import Loader from '../../../../components/Loader';
import Button from '../../../../components/Button';
import SalSelect from '../../../../components/Select';
import Switch from '../../../../components/Switch';
import Input from '../../../../components/Input';
import DatePicker from '../../../../components/DatePicker';

import {
    formatCurrency,
    stripNonDigit,
    getFormDataWithFormattedDates
} from '../../../../utils/functions';
import { browserHistory } from '../../../../utils/BrowserHistory';
import { DATE_FORMATS } from '../../../../utils/constants';

import {
    MainWrapper,
    ConfirmBodyWrapperStyle
} from './styles';

import {
    makeSelectAdjustmentTypes,
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectLoading
} from '../selectors';

import * as createAdjustmentActions from '../actions';

import { isAuthorized } from '../../../../utils/Authorization';

const inputTypes = {
    input: [ 'amount', 'reason' ],
    select: ['type_id'],
    datePicker: ['date']
};

/**
 * ManualEntry Container
 */
export class ManualEntry extends React.Component {
    static propTypes = {
        initializeData: React.PropTypes.func,
        getCompanyEmployees: React.PropTypes.func,
        formOptions: React.PropTypes.object,
        adjustmentTypes: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitForm: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        errors: React.PropTypes.object,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        previousRoute: React.PropTypes.object
    }

    static defaultProps = {
        loading: true,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            adjustmentData: {
                employee_id: null,
                type_id: null,
                amount: '',
                reason: '',
                date: null,
                disburse_through_special_pay_run: false
            },
            showModal: false,
            showBatchModal: false,
            errors: {},
            prepopulatedEmployee: null,
            typeaheadTimeout: null
        };
    }

    componentWillMount() {
        isAuthorized(['create.adjustments'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        this.props.initializeData();

        const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddAdjustment' ) );
        this.setState({ prepopulatedEmployee });
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });

        nextProps.submitted !== this.props.submitted && this.submitButton.setState({ disabled: nextProps.submitted });
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    getEmployeeFullName = () => {
        if ( this.props.formOptions.employees ) {
            const employee = this.props.formOptions.employees.find( ( e ) => e.id === parseInt(
                this.state.prepopulatedEmployee ? this.state.prepopulatedEmployee.id : this.state.adjustmentData.employee_id, 10
            ) );

            return employee ? `${employee.first_name} ${employee.last_name} ${employee.employee_id}` : '';
        }

        return '';
    }

    validateAdjustmentData() {
        let valid = true;

        if ( this.employee_id._validate( this.employee_id.state.value ) ) {
            valid = !!this.state.prepopulatedEmployee;
            if ( valid ) {
                this.employee_id.setState({
                    error: false,
                    errorMessage: '&nbsp;'
                });
            }
        }

        if ( !this.type_id._checkRequire(
            typeof this.type_id.state.value === 'object'
            && this.type_id.state.value !== null ? this.type_id.state.value.value : this.type_id.state.value )
        ) {
            valid = false;
        }

        if ( this.amount._validate( stripNonDigit( this.amount.state.value ) ) ) {
            valid = false;
        }

        if ( stripNonDigit( this.amount.state.value ) === '0.00' ) {
            this.amount.setState({ error: true, errorMessage: 'Adjustment amount cannot be 0.' });

            valid = false;
        }

        if ( this.date.checkRequired() ) {
            valid = false;
        }

        if ( this.reason._validate( this.reason.state.value ) ) {
            valid = false;
        }

        return valid;
    }

    updateAdjustmentData( key, value ) {
        if ( typeof value === 'undefined' ) {
            return;
        }

        if ( inputTypes.datePicker.includes( key ) ) {
            this.state.adjustmentData[ key ] = value;
            return;
        }

        const dataClone = { ...this.state.adjustmentData };
        dataClone[ key ] = value !== '' && value !== null ? value : '';
        if ( key === 'type_id' ) {
            this.adjustmentType = this.props.formOptions.adjustmentTypes.find( ( adjustmentType ) => adjustmentType.id === value );
        }
        this.setState({ adjustmentData: Object.assign( this.state.adjustmentData, dataClone ) });
    }

    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.input.includes( key ) || inputTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, label: this.state.errors[ key ][ 0 ] });
                } else if ( inputTypes.datePicker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    submitForm = () => {
        if ( this.validateAdjustmentData() ) {
            const data = getFormDataWithFormattedDates( this.state.adjustmentData, DATE_FORMATS.DISPLAY, DATE_FORMATS.API );
            const { employee_id: employeeId, date, disburse_through_special_pay_run, ...rest } = data;

            this.props.submitForm({
                ...rest,
                previousRouteName: this.props.previousRoute.name,
                amount: Number( stripNonDigit( data.amount ) ),
                recipients: { employees: [employeeId]},
                release_details: [{
                    date,
                    disburse_through_special_pay_run,
                    status: 0
                }]
            });

            delete data.employee_id;
        }
    }

    renderForm = () => {
        if ( this.state.prepopulatedEmployee ) {
            this.state.adjustmentData.employee_id = this.state.prepopulatedEmployee.id;
        }

        return (
            <div>
                <div className="row">
                    <div className="col-xs-1"></div>
                    <div className="col-xs-5">
                        {this.props.loading ? null : (
                            <Typeahead
                                id="employee_id"
                                ref={ ( ref ) => { this.employee_id = ref; } }
                                defaultValue={ this.getEmployeeFullName() }
                                disabled={ !!this.state.prepopulatedEmployee }
                                label="Employee name or Employee ID"
                                required
                                labelKey={ ( option ) => `${option.first_name} ${option.last_name} ${option.employee_id}` }
                                filterBy={ [ 'first_name', 'last_name', 'employee_id' ] }
                                placeholder="Start typing to search for an employee"
                                options={ this.props.formOptions.employees }
                                onInputChange={ ( value ) => {
                                    if ( value.trim() === '' ) {
                                        return false;
                                    }

                                    if ( this.state.typeaheadTimeout ) {
                                        clearTimeout( this.state.typeaheadTimeout );
                                    }

                                    this.state.typeaheadTimeout = setTimeout( () => {
                                        if ( value.length >= 3 ) {
                                            this.props.getCompanyEmployees({
                                                keyword: value,
                                                formOptions: this.props.formOptions
                                            });
                                        }
                                    }, 1000 );

                                    return true;
                                } }
                                onChange={ ( value ) => {
                                    this.updateAdjustmentData( 'employee_id', value[ 0 ] ? value[ 0 ].id : null );
                                } }
                            />
                        ) }
                    </div>
                    <div className="col-xs-5">
                        <SalSelect
                            id="type_id"
                            label="Adjustment type"
                            required
                            key="type_id"
                            data={ this.props.formOptions.adjustmentTypes }
                            value={ this.state.adjustmentData.type_id }
                            placeholder="Select adjustment type"
                            ref={ ( ref ) => { this.type_id = ref; } }
                            onChange={ ({ value }) => { this.updateAdjustmentData( 'type_id', value ); } }
                        />
                    </div>
                    <div className="col-xs-1"></div>
                </div>
                <div className="row" style={ { marginTop: '20px' } }>
                    <div className="col-xs-1"></div>
                    <div className="col-xs-5">
                        <Input
                            id="amount"
                            placeholder="Type Amount"
                            label="Amount"
                            required
                            ref={ ( ref ) => { this.amount = ref; } }
                            onChange={ ( value ) => { this.amount.setState({ value: stripNonDigit( value ) }); } }
                            onFocus={ () => { this.amount.setState({ value: stripNonDigit( this.amount.state.value ) }); } }
                            onBlur={ ( value ) => {
                                this.amount.setState({ value: formatCurrency( value ) }, () => {
                                    this.updateAdjustmentData( 'amount', formatCurrency( value ) );
                                });
                            } }
                        />
                    </div>
                    <div className="col-xs-5 date">
                        <DatePicker
                            label="Payroll Date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            required
                            selectedDay={ this.state.adjustmentData.date }
                            ref={ ( ref ) => { this.date = ref; } }
                            onChange={ ( value ) => { this.updateAdjustmentData( 'date', value ); } }
                        />
                        <Switch
                            ref={ ( ref ) => { this.disburse_through_special_pay_run = ref; } }
                            defaultChecked={ !!this.state.adjustmentData.disburse_through_special_pay_run }
                            onChange={ ( value ) => { this.updateAdjustmentData( 'disburse_through_special_pay_run', value ); } }
                        />
                        <span style={ { marginLeft: '10px' } }>Include to special pay run?</span>
                    </div>
                    <div className="col-xs-1"></div>
                </div>
                <div className="row">
                    <div className="col-xs-1"></div>
                    <div className="col-xs-5">
                        <Input
                            id="reason"
                            placeholder="Type your reason here"
                            label="Reason"
                            required
                            ref={ ( ref ) => { this.reason = ref; } }
                            onChange={ ( value ) => { this.updateAdjustmentData( 'reason', value ); } }
                        />
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div>
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <MainWrapper>
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/adjustments' ); } }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showModal }
                    />
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/adjustments/batch-add' ); } }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showBatchModal }
                    />

                    <div className={ `loader ${this.props.loading ? '' : 'hide'}` }>
                        <Loader />
                    </div>

                    <div className={ this.props.loading ? 'hide' : '' }>
                        <Container>
                            { this.renderForm() }
                        </Container>
                        <div className="foot">
                            <Button
                                label="Cancel"
                                type="neutral"
                                size="large"
                                onClick={ () => {
                                    this.props.previousRoute.name
                                    ? browserHistory.goBack()
                                    : browserHistory.push( '/adjustments' );
                                } }
                            />
                            <Button
                                label={ this.props.submitted ? <Loader /> : 'Submit' }
                                disabled={ this.props.submitted }
                                type="action"
                                size="large"
                                onClick={ this.submitForm }
                                ref={ ( ref ) => { this.submitButton = ref; } }
                            />
                        </div>
                    </div>
                </MainWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    adjustmentTypes: makeSelectAdjustmentTypes(),
    formOptions: makeSelectFormOptions(),
    submitted: makeSelectSubmitted(),
    errors: makeSelectErrors(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createAdjustmentActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ManualEntry );
