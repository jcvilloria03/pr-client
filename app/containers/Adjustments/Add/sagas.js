import startCase from 'lodash/startCase';
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { transformData, formatFeedbackMessage } from '../../../utils/functions';

import {
    GET_ADJUSTMENTS_PREVIEW,
    INITIALIZE,
    GET_COMPANY_EMPLOYEES,
    LOADING,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SAVE_ADJUSTMENTS,
    SET_BATCH_UPLOAD_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_ADJUSTMENTS_PREVIEW_DATA,
    SET_ADJUSTMENTS_PREVIEW_STATUS,
    SET_ERRORS,
    SET_FORM_OPTIONS,
    SET_SAVING_STATUS,
    SUBMITTED,
    SUBMIT_FORM,
    UPLOAD_ADJUSTMENTS
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formOptions = {};

        const adjustmentTypes = yield call( Fetch, `/company/${companyId}/other_income_types/adjustment_type`, { method: 'GET' });

        formOptions.adjustmentTypes = adjustmentTypes.data
            .filter( ( type ) => !type.deleted_at )
            .map( ( value ) => ({ label: startCase( value.name.toLowerCase() ), value: value.id }) );

        const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddAdjustment' ) );
        if ( prepopulatedEmployee ) {
            const filterEmployeeIdsQueryString = `&filter[employee_ids][]=${prepopulatedEmployee.id}`;
            const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL${filterEmployeeIdsQueryString}`, { method: 'GET' });
            formOptions.employees = employees.data;
        }

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Get Company Employees
 */
export function* getCompanyEmployees({ payload }) {
    try {
        const keyword = payload.keyword ? payload.keyword : '';
        const companyId = company.getLastActiveCompanyId();
        const formOptions = payload.formOptions;
        let filterEmployeeIdsQueryString = '';

        if ( payload.filterEmployeeIds ) {
            const filterEmployeeIds = payload.filterEmployeeIds;
            if ( filterEmployeeIds.length > 0 ) {
                for ( const employeeId of filterEmployeeIds ) {
                    filterEmployeeIdsQueryString += `&filter[employee_ids][]=${employeeId}`;
                }
            }
        }

        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL&keyword=${keyword}${filterEmployeeIdsQueryString}`, { method: 'GET' });

        formOptions.employees = employees.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        // Transform data to remove null keys
        const data = transformData({ ...payload });

        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, `/philippine/company/${companyId}/adjustment/bulk_create`, { method: 'POST', data: [data]});
        yield call( showSuccessMessage );
        yield call( delay, 500 );

        payload.previousRouteName ? yield call( browserHistory.goBack ) : yield call( browserHistory.push, '/adjustments' );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( notification ) {
    let payload = {};

    if ( notification.type === 'success' ) {
        payload = {
            show: true,
            title: notification.title || 'Success',
            message: notification.message || 'Record successfully updated',
            type: 'success'
        };
    } else {
        payload = {
            show: true,
            title: notification.response ? notification.response.statusText : 'Error',
            message: notification.response ? notification.response.data.message : notification.statusText,
            type: 'error'
        };
    }

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'success'
        }
    });
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage()
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Uploads the CSV of adjustments to add and starts the validation process
 */
export function* uploadAdjustments({ payload }) {
    try {
        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: {}
        });

        const data = new FormData();
        const companyId = company.getLastActiveCompanyId();

        data.append( 'file', payload.file );
        data.append( 'company_id', companyId );

        const upload = yield call( Fetch, `/company/${companyId}/other_income/adjustment/upload`, {
            method: 'POST',
            data
        });

        if ( upload.id ) {
            yield put({
                type: SET_BATCH_UPLOAD_JOB_ID,
                payload: upload.id
            });

            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: 'validation_queued'
            });

            yield call( checkValidation, { payload: { jobId: upload.id }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();
        const check = yield call(
            Fetch,
            `/company/${companyId}/other_income/adjustment/upload/status?job_id=${payload.jobId}`,
            { method: 'GET' }
        );

        if ( check.status === 'validation_failed' ) {
            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: check.status
            });

            yield put({
                type: SET_BATCH_UPLOAD_ERRORS,
                payload: check.errors
            });
        } else if ( check.status === 'saved' ) {
            yield call( notifyUser, {
                title: 'Success',
                message: 'Record successfully updated',
                show: true,
                type: 'success'
            });
            yield put({
                type: SET_SAVING_STATUS,
                payload: check.status
            });
            yield call( delay, 2000 );
            yield call( browserHistory.push, '/adjustments' );
        } else {
            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: check.status
            });

            yield call( checkValidation, { payload: { jobId: payload.jobId }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Fetches preview for uploaded adjustments
 */
export function* getAdjustmentsPreview({ payload }) {
    try {
        yield put({
            type: SET_ADJUSTMENTS_PREVIEW_DATA,
            payload: []
        });

        yield put({
            type: SET_ADJUSTMENTS_PREVIEW_STATUS,
            payload: 'busy'
        });

        const preview = yield call(
            Fetch,
            `/company/other_income/adjustment/upload/preview?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}`,
            { method: 'GET' }
        );

        yield put({
            type: SET_ADJUSTMENTS_PREVIEW_DATA,
            payload: preview
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_ADJUSTMENTS_PREVIEW_STATUS,
            payload: 'ready'
        });
    }
}

/**
 * Saves validated adjustments
 */
export function* saveAdjustments({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const upload = yield call( Fetch, `/company/${companyId}/other_income/adjustment/upload/save`, {
            method: 'POST',
            data: {
                company_id: company.getLastActiveCompanyId(),
                job_id: payload.jobId
            }
        });

        if ( upload.id ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: 'save_queued'
            });

            yield call( checkValidation, { payload: {
                step: 'save',
                jobId: payload.jobId
            }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPLOAD_ADJUSTMENTS
 */
export function* watchForUploadAdjustments() {
    const watcher = yield takeEvery( UPLOAD_ADJUSTMENTS, uploadAdjustments );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_ADJUSTMENTS_PREVIEW
 */
export function* watchForGetAdjustmentsPreview() {
    const watcher = yield takeEvery( GET_ADJUSTMENTS_PREVIEW, getAdjustmentsPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SAVE_ADJUSTMENTS
 */
export function* watchForSaveAdjustments() {
    const watcher = yield takeEvery( SAVE_ADJUSTMENTS, saveAdjustments );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_COMPANY_EMPLOYEES
 */
export function* watchForGetCompanyEmployees() {
    const watcher = yield takeEvery( GET_COMPANY_EMPLOYEES, getCompanyEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForGetAdjustmentsPreview,
    watchForUploadAdjustments,
    watchForInitializeData,
    watchForGetCompanyEmployees,
    watchForReinitializePage,
    watchForSaveAdjustments,
    watchNotify
];
