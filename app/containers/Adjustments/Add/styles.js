import styled from 'styled-components';

const PageWrapper = styled.div`
    .selected .fill {
        fill: #fff;
    }

    .selected .stroke {
        stroke: #fff;
    }

    .fill {
        fill: rgb(0,165,226);
    }

    .stroke {
        stroke: rgb(0,165,226);
    }
`;

const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;

    h3 {
        font-weight: 600;
    }

    p {
        text-align: center;
        max-width: 800px;
    }
`;

const FormWrapper = styled.div`
    > div {
        & > h4 {
            text-align: left;
        }

        & > button {
            padding: 12px 40px;
            margin-top: 2rem;
        }

        .steps {
            display: flex;
            align-items: stretch;
            justify-content: space-between;
            margin: 1rem 0;

            > .step {
                width: 49%;
            }
        }

        .template,
        .upload {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
            width: 100%;
            height: 100%;
            padding: 2rem;
            text-align: center;
            border: 2px dashed #ccc;
            border-radius: 12px;

            .sl-c-btn--wide {
                min-width: 12rem;
            }
        }

        .upload > div:first-of-type > div > div {
            margin: 0 auto;
        }

        .errors {
            .rt-thead .rt-tr {
                background: #F21108;
                color: #fff;
            }
            .rt-tbody .rt-tr {
                background: rgba(242, 17, 8, .1)
            }
            .ReactTable.-striped .rt-tr.-odd {
                background: rgba(242, 17, 8, .2)
            }

            .ReactTable .rt-th,
            .ReactTable .rt-td {
                flex: 1 0 0px;
                white-space: initial;
                text-overflow: ellipsis;
                padding: 7px 20px;
                overflow: hidden;
                transition: 0.3s ease;
                transition-property: width, min-width, padding, opacity;
            }

            .react-bs-container-body tr {
                background: rgba(249, 210, 210, 0.8);

                &:hover {
                    background: rgba(249, 210, 210, 1);
                }
            }
        }
    }
`;

export {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper
};
