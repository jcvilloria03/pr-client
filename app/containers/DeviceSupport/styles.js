import styled from 'styled-components';

export const Wrapper = styled.div`
    padding: 1rem;
    min-height: 100vh;
    color: #2c3e50;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    background-color: #F0F4F6;
    text-align: center;

    h1 {
        font-size: 28px;
        font-weight: bold;
    }
`;
