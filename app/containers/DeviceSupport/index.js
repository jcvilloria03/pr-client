import React from 'react';
import Media from 'react-media';

import { MOBILE_SCREEN_WIDTH } from './constants';

import { Wrapper } from './styles';

import image from '../../assets/logo-salarium-blue.png';

/**
 * Display "Device Not Supported" screen.
 */
const DeviceNotSupported = () => (
    <Wrapper>
        <img src={ image } alt="Salarium - logo" />
        <h1>Device Not Supported</h1>
    </Wrapper>
);

/**
 * Wrap passed component with MediaQuery to display only for supported devices.
 */
export default function withSupportedDevice( WrappedComponent ) {
    return class SupportedDevice extends React.PureComponent {
        static propTypes = WrappedComponent.propTypes;

        constructor( props ) {
            super( props );
            this.state = {
                currentRoute: props.router.routes[ 1 ]
            };
        }

        componentWillReceiveProps( nextProps ) {
            if ( this.state.currentRoute.path !== nextProps.router.routes[ 1 ].path ) {
                this.setState({
                    currentRoute: nextProps.router.routes[ 1 ]
                });
            }
        }

        render() {
            const currentRoute = this.state.currentRoute;
            const onlySupportedDevice = currentRoute && !currentRoute.anyDevice;

            if ( onlySupportedDevice ) {
                return (
                    <Media query={ `(min-width: ${MOBILE_SCREEN_WIDTH}px)` }>
                        { ( matches ) => {
                            if ( matches ) {
                                return <WrappedComponent { ...this.props } />;
                            }

                            return <DeviceNotSupported />;
                        } }
                    </Media>
                );
            }

            return <WrappedComponent { ...this.props } />;
        }
    };
}
