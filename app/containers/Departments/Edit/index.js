/* eslint-disable react/no-did-update-set-state */
/* eslint-disable require-jsdoc */
/* eslint-disable consistent-return */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import _ from 'lodash';
import Sidebar from 'components/Sidebar';
import { H3 } from 'components/Typography';
import SnackBar from 'components/SnackBar';
import Button from 'components/Button';
import Input from 'components/Input';
import A from 'components/A';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import Loader from 'components/Loader';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
import * as editDepartmentAction from './actions';
import {
    makeSelectBtnLoad,
    makeSelectNotification,
    makeSelectDepartments
} from './selectors';
import { PageWrapper, Footer, ModalAction, Department } from './styles';
import { LoadingStyles } from '../View/styles';
import { H2 } from '../../../components/Typography';

export class EditDepartment extends React.Component {
    static propTypes = {
        btnLoading: React.PropTypes.bool,
        resetStore: React.PropTypes.func,
        getDepartments: React.PropTypes.func,
        updateDepartment: React.PropTypes.func,
        departments: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        routeParams: React.PropTypes.object
    }
    constructor( props ) {
        super( props );
        this.state = {
            department: [],
            isLoading: true
        };
        this.discardModal = null;
    }

    componentDidMount() {
        this.props.getDepartments();
    }

    componentDidUpdate( prevProps ) {
        if ( this.props.departments !== prevProps.departments ) {
            const { departmentId } = this.props.routeParams;
            const department = this.props.departments.find( ( item ) => String( item.id ) === departmentId );
            if ( department ) {
                this.setState({
                    department: {
                        ...department,
                        ...this.state.department
                    },
                    isLoading: false
                });
            }
        }
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    handleChange( value ) {
        const { department } = this.state;
        department.currentValue = value;
        this.setState({ department });
    }

    handleSubmit() {
        const { department } = this.state;
        const error = this.validateDepartment( department.currentValue );

        if ( error.hasError ) {
            department.hasError = true;
            department.errorMessage = error.errorMessage;
            this.setState({ department });
        } else {
            department.hasError = false;
            department.errorMessage = '';
            this.setState({ department });
            this.updateDepartment();
        }
    }

    updateDepartment() {
        const { department } = this.state;
        const companyId = company.getLastActiveCompanyId();
        this.props.updateDepartment({
            company_id: companyId,
            data: [
                {
                    id: department.id,
                    name: department.currentValue,
                    company_id: companyId
                }
            ]
        });
    }

    validateDepartment = async ( value ) => {
        if ( !value || value.length < 3 ) {
            return {
                hasError: true,
                errorMessage: 'Please input valid Department name.'
            };
        }
        const companyId = company.getLastActiveCompanyId();
        const isNameAvailable = await Fetch( `/company/${companyId}/department/is_name_available`, { method: 'POST', data: { name: value }});
        const isNameExists = _.find( this.state.departments, { value });
        if ( !isNameAvailable || isNameExists ) {
            return {
                hasError: true,
                errorMessage: 'Department name already exists'
            };
        }
        return {
            hasError: false,
            errorMessage: ''
        };
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { btnLoading } = this.props;
        const { department } = this.state;
        return (
            <div>
                <Helmet
                    title="Edit Departments"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Departments' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p></ModalAction> }
                    buttons={ [
                        {
                            label: 'Stay on this page',
                            type: 'grey',
                            onClick: () => this.discardModal.toggle()
                        },
                        {
                            label: 'Discard',
                            type: 'darkRed',
                            onClick: () => browserHistory.push( '/company-settings/company-structure/departments', true )
                        }
                    ] }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <PageWrapper>
                    <Container className="add-dpt-container">
                        <div className="loader" style={ { display: this.state.isLoading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Departments.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        { !this.state.isLoading ? <div className="nav">
                            <Container>
                                <A
                                    href
                                    onClick={ ( e ) => {
                                        e.preventDefault();
                                        browserHistory.push( '/company-settings/company-structure/departments', true );
                                    } }
                                >
                                    <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Departments</span>
                                </A>
                            </Container>
                        </div> : null }
                        { !this.state.isLoading ? <div className="main_container">
                            <section className="content">
                                <header className="heading">
                                    <H3>Edit Departments</H3>
                                    <p>A department should have a reporting to department. In this manner, organizational hierarchy structure will be determined.</p>
                                </header>
                            </section>
                            <section className="content">
                                <Department>
                                    <div className="department-label">
                                        <span></span>
                                        <span>Department Name</span>
                                        <span></span>
                                    </div>
                                    <div className="department-number">
                                        <span>Department 1</span>
                                    </div>
                                    <div className={ `department-name-input ${department.hasError && 'department-name-input--error'}` }>
                                        <Input
                                            value={ department.currentValue || department.name }
                                            onChange={ ( value ) => this.handleChange( value ) }
                                        />
                                        { department.hasError && <span className="department-name-input--error-message">{ department.errorMessage}</span> }
                                    </div>
                                    <div className="department-btns"></div>
                                </Department>
                            </section>
                        </div> : null }
                    </Container>
                </PageWrapper>
                { !this.state.isLoading ? <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                type="action"
                                size="large"
                                label={ btnLoading ? <Loader /> : 'Submit' }
                                disabled={ btnLoading || this.state.isAddLoader }
                                onClick={ () => this.handleSubmit() }
                            />
                        </div>
                    </div>
                </Footer> : null }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    btnLoading: makeSelectBtnLoad(),
    departments: makeSelectDepartments()
});

function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { ...editDepartmentAction },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EditDepartment );
