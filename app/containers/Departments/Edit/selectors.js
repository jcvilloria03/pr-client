import { createSelector } from 'reselect';

const selectEditDepartmentDomain = () => ( state ) => state.get( 'departmentsEdit' );

const makeSelectNotification = () => createSelector(
  selectEditDepartmentDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectDepartments = () => createSelector(
  selectEditDepartmentDomain(),
  ( substate ) => substate.get( 'departments' ).toJS()
);

const makeSelectBtnLoad = () => createSelector(
  selectEditDepartmentDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

export {
  makeSelectDepartments,
  makeSelectNotification,
  makeSelectBtnLoad
};
