/* eslint-disable require-jsdoc */
import { RESET_STORE } from '../../App/constants';
import {
    DEFAULT_ACTION,
    NOTIFICATION,
    GET_DEPARTMENTS,
    UPDATE_DEPARTMENT
} from './constants';

export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function updateDepartment( payload ) {
    return {
        type: UPDATE_DEPARTMENT,
        payload
    };
}

export function getDepartments() {
    return {
        type: GET_DEPARTMENTS
    };
}

export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

export function resetStore() {
    return {
        type: RESET_STORE
    };
}
