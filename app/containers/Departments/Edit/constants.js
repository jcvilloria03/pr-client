export const namespace = 'app/containers/Departments/Add';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const BTN_LOADING = `${namespace}/BTN_LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;

export const UPDATE_DEPARTMENT = `${namespace}/UPDATE_DEPARTMENTS`;
export const GET_DEPARTMENTS = `${namespace}/GET_DEPARTMENTS`;
export const SET_DEPARTMENTS = `${namespace}/SET_DEPARTMENTS`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const SET_ERRORS = `${namespace}/SET_ERRORS`;