/* eslint-disable require-jsdoc */
import { fromJS } from 'immutable';
import { RESET_STORE } from '../../App/constants';
import {
    BTN_LOADING,
    DEFAULT_ACTION,
    NOTIFICATION_SAGA,
    SET_DEPARTMENTS,
    SET_ERRORS
} from './constants';

const initialState = fromJS({
    btnLoading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    errors: {},
    departments: []
});

function addDepartmentReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_DEPARTMENTS:
            return state.set( 'departments', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case BTN_LOADING:
            return state.set( 'btnLoading', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addDepartmentReducer;
