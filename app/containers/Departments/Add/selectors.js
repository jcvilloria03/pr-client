import { createSelector } from 'reselect';

const selectAddDepartmentDomain = () => ( state ) => state.get( 'departmentsAdd' );

const makeSelectNotification = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBtnLoad = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

const makeSelectAddDepartment = () => createSelector(
  selectAddDepartmentDomain(),
  ( substate ) => substate.toJS()
);

export {
  makeSelectAddDepartment,
  makeSelectNotification,
  makeSelectBtnLoad
};
