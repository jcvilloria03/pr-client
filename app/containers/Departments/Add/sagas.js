/* eslint-disable require-jsdoc */
/* eslint-disable consistent-return */

import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import { take, call, put, cancel } from 'redux-saga/effects';
import { get } from 'lodash';
import { Fetch } from '../../../utils/request';
import { CREATE_DEPARTMENTS, NOTIFICATION, NOTIFICATION_SAGA, SET_ERRORS, BTN_LOADING } from './constants';
import { RECORD_UPDATED_MESSAGE } from '../../../utils/constants';
import { formatFeedbackMessage } from '../../../utils/functions';
import { resetStore } from '../../App/sagas';
import { REINITIALIZE_PAGE } from '../../App/constants';
import { browserHistory } from '../../../utils/BrowserHistory';

export function* createDepartments({ payload }) {
    try {
        yield put({
            type: BTN_LOADING,
            payload: true
        });
        yield call( Fetch, '/department/bulk_create', { method: 'POST', data: payload });
        yield call( showSuccessMessage );
        browserHistory.push( '/company-settings/company-structure/departments', true );
    } catch ( error ) {
        yield call( notifyError, error.response );
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}

export function* reinitializePage() {
    yield call( resetStore );
}

export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.data.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

export function* watchForCreateDepartments() {
    const watcher = yield takeEvery( CREATE_DEPARTMENTS, createDepartments );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export default [
    watchForCreateDepartments,
    watchForNotifyUser,
    watchForReinitializePage
];
