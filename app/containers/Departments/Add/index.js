/* eslint-disable require-jsdoc */
/* eslint-disable consistent-return */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import _ from 'lodash';
import Sidebar from 'components/Sidebar';
import { H3 } from 'components/Typography';
import SnackBar from 'components/SnackBar';
import Button from 'components/Button';
import Input from 'components/Input';
import A from 'components/A';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import Loader from 'components/Loader';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
import * as addDepartmentAction from './actions';
import { DEFAULT_DEPARTMENT } from './constants';
import {
    makeSelectBtnLoad,
    makeSelectNotification
} from './selectors';
import { PageWrapper, Footer, ModalAction, Department } from './styles';

export class AddDepartment extends React.Component {
    static propTypes = {
        btnLoading: React.PropTypes.bool,
        resetStore: React.PropTypes.func,
        createDepartments: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    constructor( props ) {
        super( props );
        this.state = {
            departments: [
                { ...DEFAULT_DEPARTMENT }
            ],
            departmentToDelete: null,
            isAddLoader: false
        };
        this.confirmOpenModal = null;
        this.successModal = null;
        this.cancelOption = '';
        this.discardModal = null;
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    async handleAddDepartment( value, index ) {
        const { departments } = this.state;
        const error = await this.validateDepartment( value );
        departments[ index ].hasError = error.hasError;
        departments[ index ].errorMessage = error.errorMessage;

        if ( !error.hasError ) {
            departments[ index ].value = value;
            departments[ index ].isNew = false;
            departments[ index ].isEditable = true;
            departments.push({ ...DEFAULT_DEPARTMENT });
        }
        this.setState({ departments });
    }

    handleEditDepartment( department, index ) {
        const { departments } = this.state;
        departments[ index ].isEditable = false;
        departments[ index ].isEditing = true;
        departments[ index ].currentValue = department.value;
        this.setState({ departments });
    }

    handleSaveDepartment( department, index ) {
        const { departments } = this.state;
        departments[ index ].isEditing = false;
        departments[ index ].isEditable = true;
        departments[ index ].value = department.currentValue;
        this.setState({ departments });
    }

    handleCancelEditingDepartment( index ) {
        const { departments } = this.state;
        departments[ index ].isEditing = false;
        departments[ index ].isEditable = true;
        departments[ index ].currentValue = departments[ index ].value;
        this.setState({ departments });
    }

    handleDeleteDepartment( index ) {
        this.setState({ departmentToDelete: index });
        this.confirmOpenModal.toggle();
    }

    handleConfirmDeleteDepartment() {
        const { departments, departmentToDelete } = this.state;
        departments.splice( departmentToDelete, 1 );
        this.setState({ departments, departmentToDelete: null });
        this.confirmOpenModal.toggle();
    }

    handleCancelDeleteDepartment() {
        this.setState({ departmentToDelete: null });
        this.confirmOpenModal.toggle();
    }

    handleChange( value, index ) {
        const { departments } = this.state;
        departments[ index ].currentValue = value;
        this.setState({ departments });
    }

    handleSubmit() {
        const companyId = company.getLastActiveCompanyId();
        const data = this.state.departments.map( ( department ) => ({
            name: department.value,
            parent: null
        }) ).filter( ( department ) => department.name );
        this.props.createDepartments({
            company_id: companyId,
            data
        });
    }

    validateDepartment = async ( value ) => {
        if ( !value || value.length < 3 ) {
            return {
                hasError: true,
                errorMessage: 'Please input valid Department name.'
            };
        }
        const companyId = company.getLastActiveCompanyId();
        const isNameAvailable = await Fetch( `/company/${companyId}/department/is_name_available`, { method: 'POST', data: { name: value }});
        const isNameExists = _.find( this.state.departments, { value });
        if ( !isNameAvailable || isNameExists ) {
            return {
                hasError: true,
                errorMessage: 'Department name already exists'
            };
        }
        return {
            hasError: false,
            errorMessage: ''
        };
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { btnLoading } = this.props;
        return (
            <div>
                <Helmet
                    title="Add Departments"
                    meta={ [
                        { name: 'description', content: 'Description of Add Departments' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p></ModalAction> }
                    buttons={ [
                        {
                            label: 'Stay on this page',
                            type: 'grey',
                            onClick: () => this.discardModal.toggle()
                        },
                        {
                            label: 'Discard',
                            type: 'darkRed',
                            onClick: () => browserHistory.push( '/company-settings/company-structure/departments', true )
                        }
                    ] }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Modal
                    size="md"
                    title="Confirm Your Action"
                    body={ <ModalAction>
                        Do you wish to remove this record?
                    </ModalAction> }
                    buttons={ [
                        {
                            label: 'No',
                            type: 'grey',
                            onClick: () => this.handleCancelDeleteDepartment()
                        },
                        {
                            label: 'Yes',
                            type: 'action',
                            onClick: () => this.handleConfirmDeleteDepartment()
                        }
                    ] }
                    ref={ ( ref ) => { this.confirmOpenModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <Modal
                    title="Removed"
                    size="md"
                    body={ <ModalAction><p>Record successfully removed.</p></ModalAction> }
                    buttons={ [
                        {
                            label: 'Okay',
                            type: 'action',
                            onClick: () => this.successModal.toggle()
                        }
                    ] }
                    ref={ ( ref ) => { this.successModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <PageWrapper>
                    <Container className="add-dpt-container">
                        <div className="nav">
                            <Container>
                                <A
                                    href
                                    onClick={ ( e ) => {
                                        e.preventDefault();
                                        browserHistory.push( '/company-settings/company-structure/departments', true );
                                    } }
                                >
                                    <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Departments</span>
                                </A>
                            </Container>
                        </div>
                        <div className="main_container">
                            <section className="content">
                                <header className="heading">
                                    <H3>Add Departments</H3>
                                    <p>A department should have a reporting to department. In this manner, organizational hierarchy structure will be determined.</p>
                                </header>
                            </section>
                            <section className="content">
                                { this.state.departments.map( ( department, index ) => (
                                    <Department key={ `department_${index}` }>
                                        <div className="department-label">
                                            <span></span>
                                            <span>Department Name</span>
                                            <span></span>
                                        </div>
                                        <div className="department-number">
                                            <span>Department { index + 1 }</span>
                                        </div>
                                        { department.isNew || department.isEditing ? (
                                            <div className={ `department-name-input ${department.hasError && 'department-name-input--error'}` }>
                                                <Input
                                                    key={ `department_name_${index}` }
                                                    value={ department.currentValue }
                                                    onChange={ ( value ) => this.handleChange( value, index ) }
                                                />
                                                { department.hasError && <span className="department-name-input--error-message">{ department.errorMessage}</span> }
                                            </div> ) : (
                                                <div className="department-name">
                                                    <span>{ department.value }</span>
                                                </div>
                                            )
                                        }
                                        { department.isNew ? (
                                            <div className="department-btns">
                                                <Button
                                                    type="neutral"
                                                    className="add-btn"
                                                    label={ <span><Icon name="plus" className="svg-icon" /><span>Add Department</span></span> }
                                                    onClick={ () => this.handleAddDepartment( department.currentValue, index ) }
                                                />
                                            </div>
                                        ) : null }
                                        { !department.isNew && department.isEditable ? (
                                            <div className="department-btns">
                                                <Button
                                                    type="neutral"
                                                    label={ <span><Icon name="pencil" className="svg-icon" /><span>Edit</span></span> }
                                                    onClick={ () => this.handleEditDepartment( department, index ) }
                                                />
                                                <Button
                                                    type="darkRed"
                                                    alt
                                                    label={ <span><Icon name="trash" className="svg-icon" /><span>Delete</span></span> }
                                                    onClick={ () => this.handleDeleteDepartment( index ) }
                                                />
                                            </div>
                                        ) : null }
                                        { !department.isNew && department.isEditing ? (
                                            <div className="department-btns">
                                                <Button
                                                    type="action"
                                                    label="Save"
                                                    onClick={ () => this.handleSaveDepartment( department, index ) }
                                                />
                                                <Button
                                                    type="darkRed"
                                                    alt
                                                    label="Cancel"
                                                    onClick={ () => this.handleCancelEditingDepartment( index ) }
                                                />
                                            </div>
                                        ) : null }
                                    </Department>
                                ) ) }
                            </section>
                        </div>
                    </Container>
                </PageWrapper>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                type="action"
                                size="large"
                                label={ btnLoading ? <Loader /> : 'Submit' }
                                disabled={ btnLoading || this.state.isAddLoader }
                                onClick={ () => this.handleSubmit() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    btnLoading: makeSelectBtnLoad()
});

function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { ...addDepartmentAction },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddDepartment );
