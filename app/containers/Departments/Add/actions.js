/* eslint-disable require-jsdoc */
import { RESET_STORE } from '../../App/constants';
import {
    DEFAULT_ACTION,
    NOTIFICATION,
    CREATE_DEPARTMENTS
} from './constants';

export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function createDepartments( payload ) {
    return {
        type: CREATE_DEPARTMENTS,
        payload
    };
}

export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

export function resetStore() {
    return {
        type: RESET_STORE
    };
}
