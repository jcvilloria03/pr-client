/* eslint-disable require-jsdoc */
import {
    NOTIFICATION,
    DELETE_DEPARTMENTS,
    GET_DEPARTMENTS

} from './constants';

export function getDepartments( payload ) {
    return {
        type: GET_DEPARTMENTS,
        payload
    };
}

export function deleteDepartments( IDs ) {
    return {
        type: DELETE_DEPARTMENTS,
        payload: IDs
    };
}

export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
