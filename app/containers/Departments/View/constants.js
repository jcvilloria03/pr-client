const namespace = 'app/Departments/View';

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const TABLE_LOADING = `${namespace}/TABLE_LOADING`;
export const GET_DEPARTMENTS = `${namespace}/GET_DEPARTMENTS`;
export const SET_DEPARTMENTS = `${namespace}/SET_DEPARTMENTS`;
export const DELETE_DEPARTMENTS = `${namespace}/DELETE_DEPARTMENTS`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const SET_DEPARTMENTS_PAGINATION = `${namespace}/SET_DEPARTMENTS_PAGINATION`;

export const DEFAULT_ORDER_BY = 'name';
export const DEFAULT_ORDER_DIR = 'asc';
