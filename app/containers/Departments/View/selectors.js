import { createSelector } from 'reselect';

const selectViewDepartmentsDomain = () => ( state ) => state.get( 'departments' );

const makeSelectLoading = () => createSelector(
    selectViewDepartmentsDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectTableLoading = () => createSelector(
    selectViewDepartmentsDomain(),
  ( substate ) => substate.get( 'tableLoading' )
);

const makeSelectDepartments = () => createSelector(
    selectViewDepartmentsDomain(),
  ( substate ) => substate.get( 'departments' ).toJS()
);

const makeSelectPagination = () => createSelector(
    selectViewDepartmentsDomain(),
  ( substate ) => substate.get( 'pagination' )
);

const makeSelectNotification = () => createSelector(
    selectViewDepartmentsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectTableLoading,
  makeSelectDepartments,
  makeSelectPagination,
  makeSelectNotification
};
