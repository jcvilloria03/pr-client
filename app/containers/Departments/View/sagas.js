/* eslint-disable require-jsdoc */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import {
    SET_LOADING,
    TABLE_LOADING,
    GET_DEPARTMENTS,
    DELETE_DEPARTMENTS,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_DEPARTMENTS,
    SET_DEPARTMENTS_PAGINATION
} from './constants';
import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';
import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

export function* getDepartments({ payload }) {
    //
    try {
        yield put({
            type: TABLE_LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const parsedInitialParams = `departments?mode=DEFAULT_MODE&page=${payload.pagination.current_page}&per_page=${payload.pagination.per_page}&sort_by=${payload.orderBy}&sort_order=${payload.orderDir}`;
        // Check if searchKeyword is not empty, and send keyword value for search
        const parsedKeyword = ( payload.searchTerm.trim() ) ? `&keyword=${payload.searchTerm}` : '';
        const urlSuffix = parsedInitialParams + parsedKeyword;
        const departments = yield call( Fetch, `/company/${companyId}/${urlSuffix}`, { method: 'GET' });

        if ( departments.total ) {
            const paginationData = {
                from: departments.from || 1,
                to: departments.to || 1,
                total: departments.total || 1,
                current_page: departments.current_page || 1,
                last_page: departments.last_page || 1,
                per_page: departments.per_page
            };
            yield put({
                type: SET_DEPARTMENTS_PAGINATION,
                payload: paginationData
            });
        }
        yield put({
            type: SET_DEPARTMENTS,
            payload: departments.data || []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: TABLE_LOADING,
            payload: false
        });
    }
}

export function* deleteDepartments({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const isUse = yield call( Fetch, '/company/department/is_in_use', { method: 'POST',
            data: {
                company_id: company.getLastActiveCompanyId(),
                ids: payload,
                type: 'department'
            }});
        if ( isUse && isUse.in_use === 0 ) {
            const promises = payload.map( ( id ) => call( Fetch, `/department/${id}`, { method: 'DELETE' }) );
            yield promises;
            yield call( notifyUser, {
                show: true,
                title: 'Success',
                message: 'Record(s) successfully deleted!',
                type: 'success'
            });
            yield call( getDepartments );
        } else {
            const error = {
                message: "One or more of the departments selected is currently in use, and can't be deleted."
            };
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: error.message,
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

export function* reinitializePage() {
    yield call( resetStore );
    yield call( getDepartments );
}

export function* watchForGetDepartments() {
    const watcher = yield takeEvery( GET_DEPARTMENTS, getDepartments );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForDeleteDepartments() {
    const watcher = yield takeEvery( DELETE_DEPARTMENTS, deleteDepartments );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export default [
    watchForGetDepartments,
    watchForDeleteDepartments,
    watchForNotifyUser,
    watchForReinitializePage
];
