import { createSelector } from 'reselect';

const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

const makeSelectProductsState = () => createSelector(
    selectAppDomain(),
    ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

export {
  makeSelectProductsState
};
