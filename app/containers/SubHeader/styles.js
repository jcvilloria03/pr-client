import styled from 'styled-components';

const Wrapper = styled.div`
    position: fixed;
    top: 76px;
    left: 0;
    right: 0;
    z-index: 10;
    width: 100%;
    max-width: 100% !important;
    background-color: #02688f;
    box-shadow: 0 1px 2px 0 #cccccc;

    > .container {
        max-width: 80vw;
        margin-right: auto;
        margin-left: auto;
        padding-left: 0;
        padding-right: 0;
    }

    .sub-menu-link {
        color: #fff !important;
    }

    ul {
        margin-bottom: 0;
        padding: 0;
    }

    .active {
        color: #fff !important;
        border-bottom: 3px solid #fff;
    }

    a {
        display: inline-block;
        min-width: 100px;
        padding: 14px 20px;
        color: #fff !important;
        border-bottom: 3px solid transparent;
        font-size: 14px;
        text-align: center;
        vertical-align: middle;
        text-decoration: none;

        &:hover, &:active {
            border-bottom-color: #fff;
        }
    }
`;

export default Wrapper;
