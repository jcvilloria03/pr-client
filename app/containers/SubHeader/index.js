import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';
import {
    makeSelectProductsState
} from './selectors';
import Wrapper from './styles';
import HeaderLink from '../../components/HeaderLink';

import { subscriptionService } from '../../utils/SubscriptionService';

/**
 * SubHeader Component
 */
class SubHeader extends Component {
    static propTypes = {
        items: PropTypes.arrayOf( PropTypes.shape({
            links: PropTypes.array,
            title: PropTypes.string.isRequired,
            onClick: PropTypes.function
        }) ),
        products: React.PropTypes.array
    }

    componentWillMount() {
        document.body.classList.add( 'sl-is-subnav-visible' );
    }

    componentWillUnmount() {
        document.body.classList.remove( 'sl-is-subnav-visible' );
    }

    shouldRenderItem( item ) {
        switch ( item.product ) {
            case 'payroll':
                return this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products );
            case 'time and attendance':
                return this.props.products && subscriptionService.isSubscribedToTA( this.props.products );
            default:
                return true;
        }
    }

    render() {
        return (
            <Wrapper>
                <Container>
                    {
                        this.props.items && (
                            <div>
                                { this.props.items.map( ( item, index ) => this.shouldRenderItem( item ) && (
                                    <HeaderLink
                                        key={ index }
                                        links={ item.links }
                                        onClick={ item.onClick }
                                        className={ `sub-menu-link ${window.location.href.includes( item.links ) ? 'active' : ''}` }
                                    >
                                        { item.title }
                                    </HeaderLink>
                                ) ) }
                            </div>
                        )
                    }
                </Container>
            </Wrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    products: makeSelectProductsState()
});

export default connect( mapStateToProps )( SubHeader );
