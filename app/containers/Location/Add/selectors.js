import { createSelector } from 'reselect';

/**
 * Direct selector to the addLocation state domain
 */
const selectAddLocationDomain = () => ( state ) => state.get( 'addLocation' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

/**
 * Default selector used by AddLocation
 */

const makeSelectAddLocation = () => createSelector(
  selectAddLocationDomain(),
  ( substate ) => substate.toJS()
);

const makeSelectSetAddLocation = () => createSelector(
  selectAddLocationDomain(),
  ( substate ) => substate.get( 'addLocation' )
);

const makeSelecGetLocation = () => createSelector(
  selectAddLocationDomain(),
  ( substate ) => substate.get( 'getlocation' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectAddLocationDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectAddLocationDomain(),
  ( substate ) => substate.get( 'loading' )
);

export default makeSelectAddLocation;
export {
  selectAddLocationDomain,
  makeSelectProducts,
  makeSelectSetAddLocation,
  makeSelectNotification,
  makeSelectLoading,
  makeSelecGetLocation
};
