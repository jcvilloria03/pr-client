import {
    ADD_LOCATION,
    DEFAULT_ACTION,
    GET_LOCATION,
    NOTIFICATION
} from './constants';

/**
 *
 * AddLocation actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Add Location action
 *
 */
export function addLocation( payload ) {
    return {
        type: ADD_LOCATION,
        payload
    };
}

/**
 *
 * Get location action
 *
 */
export function getLocation() {
    return {
        type: GET_LOCATION
    };
}

/**
 *
 * Notification action
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
