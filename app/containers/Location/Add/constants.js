/*
 *
 * AddLocation constants
 *
 */
export const nameSpace = 'app/Location/Add';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const ADD_LOCATION = `${nameSpace}/ADD_LOCATION`;
export const SET_ADD_LOCATION = `${nameSpace}/SET_ADD_LOCATION`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
export const LOADING = `${nameSpace}/LOADING`;
export const GET_LOCATION = `${nameSpace}/GET_LOCATION`;
export const SET_LOCATION = `${nameSpace}/SET_LOCATION`;
