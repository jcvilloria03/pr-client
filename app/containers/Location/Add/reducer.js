import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    NOTIFICATION_SAGA,
    SET_ADD_LOCATION,
    SET_LOCATION
} from './constants';

const initialState = fromJS({
    addLocation: '',
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    },
    loading: false,
    getlocation: []
});

/**
 *
 * AddLocation reducer
 *
 */
function addLocationReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_ADD_LOCATION:
            return state.set( 'addLocation', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_LOCATION:
            return state.set( 'getlocation', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default addLocationReducer;
