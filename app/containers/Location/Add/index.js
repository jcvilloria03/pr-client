import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import makeSelectAddLocation, {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectProducts,
    makeSelectSetAddLocation,
    makeSelecGetLocation
} from './selectors';
import * as addLocationActions from './actions';

import { getCompanySettingsSidebarLinks } from '../../../utils/sidebarHelper';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { browserHistory } from '../../../utils/BrowserHistory';
import { timezones, defaultTimezone } from '../../../utils/timezones';

import Input from '../../../components/Input';
import A from '../../../components/A';
import { H3, H4, H6 } from '../../../components/Typography';
import Sidebar from '../../../components/Sidebar';
import SnackBar from '../../../components/SnackBar';
import Button from '../../../components/Button';
import Icon from '../../../components/Icon';
import Loader from '../../../components/Loader';
import Radio from '../../../components/Radio';
import RadioGroup from '../../../components/RadioGroup';
import Select from '../../../components/Select';

import {
    PageWrapper,
    NavWrapper
} from './styles';
import { DEFAULT_NO_OPTION, getCountries, getStates, getCities } from '../../../utils/Locations/locations';

/**
 *
 * AddLocation
 *
 */
export class AddLocation extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
        addLocation: React.PropTypes.func,
        products: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool,
        getlocationList: React.PropTypes.array,
        getLocation: React.PropTypes.func
    }

    static defaultProps = {
        loading: false
    };

    constructor( props ) {
        super( props );

        this.state = {
            formData: {
                locationName: '',
                timeZone: '',
                search: 'Searched data',
                address1: '',
                address2: ' ',
                country: '',
                region: '',
                city: '',
                zip: '',
                ipAddressList: [],
                headquart: 'no'
            },
            ipAddress: '',
            showButtons: false,
            editIP: null,
            validationHeadQ: false,
            validationLocationName: 'name',
            validationtimeZone: 'timezone',
            validationAddress1: 'address',
            validationAddress2: 'address',
            validationCountry: 'country',
            validationRegion: 'region',
            validationCity: 'city',
            validationZip: 'zip',
            validationIP: 'ip',
            nameExists: 'name',
            countries: [],
            states: [],
            cities: [],
            country: {},
            state: {},
            city: {},
        };
    }

    componentDidMount() {
        this.props.getLocation();
        this.setTimezone();
        this.setState({
            countries: getCountries(),
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if ( this.state.formData.timeZone !== prevState.formData.timeZone ) {
            this.setTimezone();
        }

        if ( this.state.formData.country !== prevState.formData.country ) {
            let country = this.state.countries.filter( ( value ) => value.label === this.state.formData.country );
            country = country[ 0 ] ? country[ 0 ] : { label: this.state.formData.country, value: this.state.formData.country };
            this.setState({ country, states: this.getStates() });
        }

        if ( ( this.state.formData.region !== prevState.formData.region ) ) {
            let state = getStates( this.state.formData.country.value ).filter( ( value ) => value.label === this.state.formData.region );
            state = state[ 0 ] ? state[ 0 ] : { label: this.state.formData.region, value: this.state.formData.region };
            this.setState({ state, cities: this.getCities() });
        }

        if ( ( this.state.formData.city !== prevState.formData.city ) ) {
            let city = getCities( this.state.formData.country.value, this.state.formData.region.value ).filter( ( value ) => value.label === this.state.formData.city );
            city = city[ 0 ] ? city[ 0 ] : { label: this.state.formData.city, value: this.state.formData.city };
            this.setState({ city });
        }
    }

    setTimezone() {
        const tzExists = timezones.find( ( tz ) => tz.value === this.state.formData.timeZone );
        if ( this.state.formData.timeZone === '' || !tzExists ) {
            this.setState({
                formData: {
                    ...this.state.formData,
                    timeZone: defaultTimezone.value
                }
            });
        }
    }

    getStates() {
        if ( this.state.formData.country === '' ) {
            return [];
        }

        const country = getCountries().filter( ( value ) => value.label === this.state.formData.country );
        if ( country[ 0 ]) {
            const states = getStates( country[ 0 ].value );
            return states.length > 0 ? states : [DEFAULT_NO_OPTION];
        }

        return [DEFAULT_NO_OPTION];
    }

    getCities() {
        if ( this.state.formData.country === '' ) {
            return [];
        }

        const country = getCountries().filter( ( value ) => value.label === this.state.formData.country );
        const state = getStates( country[ 0 ].value ).filter( ( value ) => value.label === this.state.formData.region );
        if ( country[ 0 ]) {
            const cities = getCities( country[ 0 ].value, ( state && state[ 0 ]) ? state[ 0 ].value : null );
            return cities.length > 0 ? cities : [DEFAULT_NO_OPTION];
        }

        return [DEFAULT_NO_OPTION];
    }

    handleCountryChange( country ) {
        this.setState({
            formData: {
                ...this.state.formData,
                country: country.label || '',
                region: '',
                city: ''
            }
        });
    }

    handleRegionChange( region ) {
        const { formData } = this.state;
        this.setState({ validationRegion: region.label,
            formData: {
                ...formData,
                region: region.label || '',
                city: ''
            }});
    }

    addToList() {
        if ( this.validateIP() ) {
            this.setState({
                formData: {
                    ...this.state.formData,
                    ipAddressList: [
                        ...this.state.formData.ipAddressList,
                        this.state.ipAddress
                    ]
                }
            });
            this.setState({
                ipAddress: ''
            });
        }
    }

    deleteList( id ) {
        this.setState({
            formData: {
                ...this.state.formData,
                ipAddressList: this.state.formData.ipAddressList.filter( ( arr, i ) => i !== id )
            }
        });
    }

    showTextbox( IpId ) {
        return this.state.editIP && this.state.editIP.id === IpId;
    }

    editList() {
        this.state.formData.ipAddressList.map( ( elem, i ) => {
            if ( i === this.state.editIP.id ) {
                const value = this.state.formData.ipAddressList;
                value[ i ] = this.state.editIP.ip;
                this.setState({
                    formData: {
                        ...this.state.formData,
                        ipAddressList: value
                    },
                    showButtons: false
                });
            }
            return !elem;
        });
    }

    validateIP = () => {
        let valid = true;
        const regexExp = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/gi;
        if ( regexExp.test( this.state.ipAddress ) === false ) {
            this.setState({
                validationIP: ''
            });
            valid = false;
        }
        return valid;
    }

    validateFields = () => {
        let valid = true;
        if ( this.state.formData.locationName === '' ) {
            this.setState({
                validationLocationName: ''
            });
            valid = false;
        } else if ( this.props.getlocationList.map( ( value ) => value.name ).includes( this.state.formData.locationName ) ) {
            this.setState({
                nameExists: ''
            });
            valid = false;
        }
        if ( this.state.formData.timeZone === '' ) {
            this.setState({
                validationtimeZone: ''
            });
            valid = false;
        }
        if ( this.state.formData.address1 === '' ) {
            this.setState({
                validationAddress1: ''
            });
            valid = false;
        }
        if ( this.state.formData.country === '' ) {
            this.setState({
                validationCountry: ''
            });
            valid = false;
        }
        if ( this.state.formData.region === '' ) {
            this.setState({
                validationRegion: ''
            });
            valid = false;
        }
        if ( this.state.formData.city === '' ) {
            this.setState({
                validationCity: ''
            });
            valid = false;
        }
        if ( this.state.formData.zip === '' ) {
            this.setState({
                validationZip: ''
            });
            valid = false;
        }
        return valid;
    }

    addLocationData() {
        const data = this.state.formData;
        if ( this.validateFields() ) {
            this.props.addLocation( data );
        }
    }
    /**
     *
     * AddLocation render method
     *
     */
    render() {
        const headquarterList = this.props.getlocationList.map( ( value ) => value.is_headquarters );
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        const { notification, loading } = this.props;
        const { value, formData, showButtons, editIP, ipAddress, validationRegion, nameExists, validationCity, validationHeadQ, validationIP, validationZip, validationLocationName, validationtimeZone, validationAddress1, validationAddress2, validationCountry } = this.state;

        return (
            <div>
                <Helmet
                    title="Add Location"
                    meta={ [
                        { name: 'description', content: 'Description of Add Location' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/company-settings/company-structure/locations', true );
                            } }
                        >
                            &#8592; Back to Locations
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="heading">
                                <H3>Add Location</H3>
                                <p>For multiple branch or work location company, fill-in the company location addresses with the valid IP for the employees' clock ins/outs.</p>
                            </div>
                            <div className="w-100 position-relative">
                                <H3>Location</H3>
                                <div className="row">
                                    <div className="col-md-3">
                                        <H4 className={ validationLocationName === '' ? 'input-head-error' : nameExists === '' ? 'input-head-error' : 'input-head' }>*Location name</H4>
                                        <Input
                                            className={ validationLocationName === '' ? 'content-input-error' : nameExists === '' ? 'content-input-error' : 'content-input' }
                                            id="location name"
                                            value={ value }
                                            onChange={ ( eve ) => this.setState({ validationLocationName: eve, formData: { ...formData, locationName: eve }}) }
                                        />
                                        {validationLocationName === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : nameExists === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Record name already exists</p> : ''}
                                    </div>
                                    <div className="col-md-3">
                                        <H4 className={ validationtimeZone === '' ? 'input-head-error' : 'input-head' }>*Time Zone</H4>
                                        <Select
                                            id="timeZone"
                                            value={ formData.timeZone || defaultTimezone }
                                            data={ timezones }
                                            onChange={ ( tz ) => {
                                                this.setState({ validationtimeZone: tz.value, formData: { ...formData, timeZone: tz.value }});
                                            } }
                                        />
                                        {validationtimeZone === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-3">
                                        <H4 className={ validationAddress1 === '' ? 'input-head-error' : 'input-head' }>* Address line 1</H4>
                                        <Input
                                            className={ validationAddress1 === '' ? 'content-input-error' : 'content-input' }
                                            id="address1"
                                            value={ value }
                                            onChange={ ( eve ) => this.setState({ validationAddress1: eve, formData: { ...formData, address1: eve }}) }
                                        />
                                        {validationAddress1 === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                    </div>
                                    <div className="col-md-3">
                                        <H4 className={ validationAddress2 === '' ? 'input-head-error' : 'input-head' }>Address line 2</H4>
                                        <Input
                                            className={ validationAddress2 === '' ? 'content-input-error' : 'content-input' }
                                            id="address2"
                                            value={ value }
                                            onChange={ ( eve ) => this.setState({ validationAddress2: eve, formData: { ...formData, address2: eve }}) }
                                        />
                                        {validationAddress2 === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-3">
                                        <H4 className={ validationCountry === '' ? 'input-head-error' : 'input-head' }>* Country</H4>
                                        <Select
                                            id="country"
                                            name="country"
                                            data={ this.state.countries }
                                            value={ this.state.country }
                                            onChange={ ( country ) => this.handleCountryChange( country ) }
                                        />
                                        {validationCountry === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                    </div>
                                    <div className="col-md-3">
                                        <H4 className={ validationRegion === '' ? 'input-head-error' : 'input-head' }>* Region</H4>
                                        <Select
                                            id="region"
                                            name="region"
                                            data={ this.state.states }
                                            value={ this.state.state }
                                            onChange={ ( region ) => this.handleRegionChange( region ) }
                                            disabled={ formData.country === '' }
                                        />
                                        {validationRegion === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-3">
                                        <H4 className={ validationCity === '' ? 'input-head-error' : 'input-head' }>* City</H4>
                                        <Select
                                            id="city"
                                            name="city"
                                            data={ this.state.cities }
                                            value={ this.state.city }
                                            onChange={ ( city ) => this.setState({ validationCity: city.label, formData: { ...formData, city: city.label }}) }
                                            disabled={ formData.country === '' || formData.region === '' }
                                        />
                                        {validationCity === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                    </div>
                                    <div className="col-md-3">
                                        <H4 className={ validationZip === '' ? 'input-head-error' : 'input-head' }>* Zip code</H4>
                                        <Input
                                            className={ validationZip === '' ? 'content-input-error' : 'content-input' }
                                            id="zip"
                                            value={ value }
                                            onChange={ ( eve ) => this.setState({ validationZip: eve, formData: { ...formData, zip: eve }}) }
                                        />
                                        {validationZip === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <p className={ headquarterList.includes( true ) && validationHeadQ ? 'schdule_tooltip input-head-error' : 'schdule_tooltip input-head' }>* Set as headquarters
                                        <div className="bg_tooltip downloadTooltip">
                                            <label htmlFor="download">?</label>
                                            <span className="tooltiptext">The address of the location set as a Headquarters will be reflected on the government forms generated through Salarium.<br />
                                            </span>
                                        </div>
                                        </p>
                                        <RadioGroup
                                            horizontal
                                            value={ formData.headquart }
                                            onChange={ ( eve ) => {
                                                this.setState({ validationHeadQ: eve === 'yes', formData: { ...formData, headquart: eve }});
                                            } }
                                        >
                                            <Radio value="yes" checked={ formData.headquart === 'yes' } pointColor={ headquarterList.includes( true ) && validationHeadQ && '#eb7575' }>Yes</Radio>
                                            <Radio value="no" checked={ formData.headquart === 'no' } rootColor={ headquarterList.includes( true ) && validationHeadQ && '#eb7575' }>No</Radio>
                                        </RadioGroup>
                                        { headquarterList.includes( true ) && validationHeadQ ? <p className="headquarters-warn" >There is already an existing location tagged as headquarters.</p> : ''}
                                    </div>
                                </div>
                                <H3>IP Address</H3>
                                <p>You can nominate multiple IP addresses in one location. This will become the basis of valid time logs (clock ins/outs) to be processed in Time and Attendance module. In the event that a location has no specified IP address, this will mean that all time logs from any device and locations are valid.</p>
                                <span>
                                    {formData.ipAddressList.map( ( IpValue, i ) => (
                                        <div className="col-md-12" key={ i } style={ { marginBottom: '30px', marginTop: '30px' } }>
                                            <div className="col-md-3">
                                                <H6 style={ { fontWeight: 400 } }>IP address</H6>
                                            </div>
                                            <div className="col-md-3">
                                                { this.showTextbox( i ) && showButtons === true
                                                ? <div style={ { width: '100%' } }>
                                                    <Input
                                                        id="name" type="text"
                                                        value={ editIP.ip }
                                                        onChange={ ( eve ) => this.setState({ editIP: { ...editIP, ip: eve }}) }
                                                    />
                                                </div>
                                                : <div>{ IpValue }</div>
                                            }
                                            </div>
                                            <div className="col-md-3">
                                                { this.showTextbox( i ) && showButtons === true
                                            ? <div>
                                                <Button
                                                    className="savebtn2"
                                                    label={ <span>Save</span> }
                                                    type="action"
                                                    size="small"
                                                    onClick={ () => this.editList() }
                                                ></Button>
                                                <Button
                                                    className="cancelbtn"
                                                    label={ <span>Cancel</span> }
                                                    type="neutral"
                                                    size="small"
                                                    onClick={ () => this.setState({ showButtons: false }) }
                                                ></Button>
                                            </div>
                                            : <div>
                                                <Button
                                                    className="savebtn"
                                                    label={ <span><Icon name="pencil" className="icon" /> Edit</span> }
                                                    type="neutral"
                                                    size="small"
                                                    onClick={ () => this.setState({ showButtons: true, editIP: { id: i, ip: IpValue }}) }
                                                >
                                                </Button>
                                                <Button
                                                    className="deletebtn"
                                                    label={ <span><Icon name="trash" className="icon" /> Delete</span> }
                                                    type="danger"
                                                    size="small"
                                                    onClick={ () => this.deleteList( i ) }
                                                ></Button>
                                            </div>
                                            }
                                            </div>
                                        </div>
                                ) )}
                                    <div className="col-md-12 ip-margin">
                                        <div className="col-md-3">
                                            <H6 style={ { fontWeight: 400 } }>IP address</H6>
                                        </div>
                                        <div className="col-md-3">
                                            <Input
                                                className={ validationIP === '' ? 'content-input-error' : 'content-input' }
                                                id="Ipaddress"
                                                value={ ipAddress }
                                                onChange={ ( eve ) => this.setState({ validationIP: eve, ipAddress: eve }) }
                                            />
                                            {validationIP === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Invalid IP Address</p> : ''}
                                        </div>
                                        <div className="col-md-3">
                                            <Button
                                                className="addIP"
                                                label={ <span><Icon name="plus" className="icon" /> Add IP address</span> }
                                                type="neutral"
                                                size="default"
                                                onClick={ () => this.addToList() }
                                            ></Button>
                                        </div>
                                    </div>
                                </span>
                            </div>
                            <div className="row submit">
                                <div className="col-xs-12">
                                    <Button
                                        label="Cancel"
                                        type="action"
                                        size="large"
                                        alt
                                        onClick={ () => browserHistory.push( '/company-settings/company-structure/locations', true ) }
                                    />
                                    <Button
                                        label={ loading ? <Loader /> : 'Submit' }
                                        type="action"
                                        size="large"
                                        onClick={ () => this.addLocationData() }
                                    />
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    AddLocation: makeSelectAddLocation(),
    products: makeSelectProducts(),
    locationAdd: makeSelectSetAddLocation(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    getlocationList: makeSelecGetLocation()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        addLocationActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddLocation );
