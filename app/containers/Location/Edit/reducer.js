import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    NOTIFICATION_SAGA,
    SET_EDIT_LOCATION,
    SET_LOCATION
} from './constants';

const initialState = fromJS({
    editLocation: '',
    getlocation: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    },
    loading: false
});

/**
 *
 * EditLocation reducer
 *
 */
function editLocationReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_EDIT_LOCATION:
            return state.set( 'editLocation', action.payload );
        case SET_LOCATION:
            return state.set( 'getlocation', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        default:
            return state;
    }
}

export default editLocationReducer;
