import { take, call, put, cancel } from 'redux-saga/effects';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import {
    EDIT_LOCATION,
    GET_LOCATION,
    LOADING,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_EDIT_LOCATION,
    SET_LOCATION
} from './constants';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';
import { browserHistory } from '../../../utils/BrowserHistory';

import { REINITIALIZE_PAGE } from '../../App/constants';

/**
 * GET location
 */
export function* getLocation() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/time_attendance_locations?mode=DEFAULT_MODE&name=`, { method: 'GET' });

        yield put({
            type: SET_LOCATION,
            payload: res.data || []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize Page
 */
export function* reinitializePage() {
    yield call( getLocation );
}

/**
 * Add location
 */
export function* EditLocation({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        yield put({
            type: SET_EDIT_LOCATION,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/location/is_name_available`, {
            method: 'POST',
            data: { name: payload.name, location_id: payload.id }
        });

        if ( res.available === true ) {
            yield call( Fetch, `/time_attendance_locations/${payload.id}`, {
                method: 'PUT',
                data: {
                    account_id: 7815,
                    address_bar: payload.address_bar,
                    city: payload.city,
                    company_id: companyId,
                    country: payload.country,
                    first_address_line: payload.first_address_line,
                    id: payload.id,
                    ip_addresses: payload.ip_addresses.map( ( val ) => val.ip_address ),
                    is_headquarters: payload.is_headquarters,
                    location_pin: '36.0472454,14.2691021',
                    name: payload.name,
                    region: payload.region,
                    second_address_line: payload.second_address_line,
                    timezone: payload.timezone,
                    zip_code: payload.zip_code
                }
            });

            yield call( notifyUser, {
                title: 'Success',
                message: 'Location Updated succesfully..',
                show: 'true',
                type: 'success'
            });
            yield call( browserHistory.push( '/company-settings/company-structure/locations', true ) );
        } else {
            yield call( notifyUser, {
                title: 'Error',
                message: 'Location name is already exist..',
                show: 'true',
                type: 'error'
            });
        }
        yield put({
            type: LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EDIT_LOCATION,
            payload: false
        });
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Watch for get Location
 */
export function* watchForGetLocation() {
    const watcher = yield takeEvery( GET_LOCATION, getLocation );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For ReinitializePage
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watcher For Add location
 */
export function* watchForEditLocation() {
    const watcher = yield takeEvery( EDIT_LOCATION, EditLocation );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Notification
 */
export function* watchFroNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetLocation,
    watchForReinitializePage,
    watchFroNotification,
    watchForEditLocation
];
