import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;
`;

export const PageWrapper = styled.div`
    padding-top: 120px;
    .content {
        margin-top: 40px;
        .headquarters-warn{
            color: #EB7575;
            font-size: 14px;
            width: 52%;
        }
        .field-warn{
            color: #EB7575;
        }
        .entriTitle{
            text-align:right;
        }
        .position-relative{
            position:relative;
        }
        .map-box{
            top: 18.5%;
            left: 50%;
            height: 340px;
            width: 500px;
            background-color: #e5e3df;
            position: absolute;
        }
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: bold;
            }
            p{
                margin-bottom:0.25rem;
            }
        }
        h3 {
            font-weight: bold;
        }
        h4 {
            font-weight: 300;
        }
        .content-input{
            height:55px;
            input{
                border: 1px solid black;
                min-height: 46px;
            }
        }
        .content-input-error{
            height:55px;
            input{
                border: 1px solid #eb7575;
                min-height: 46px;
            }
        }

        .schdule_tooltip{
        display: flex;
        align-items: center;
        font-size: 14px;
        margin-bottom:5px;
        button{
            width: 20px;
            height: 20px;
            padding: 0;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
        }
        a{
            padding: 0;
            border-bottom: none;
            padding-left: 6px;
        }
        .bg_tooltip {
            position: relative;
            display: inline-block;
            width:20px;
            height:20px;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
            margin-left: 6px;
            text-align: center;
            label{
                font-size: 15px;
                font-weight: bold;
            }
        }

        .bg_tooltip .tooltiptext {
            visibility: hidden;
            width: 242px;
            background-color: #fff;
            color: #03a9f4;
            border: 2px solid #03a9f4;
            text-align: left;
            border-radius: 6px;
            padding: 5px 16px;
            position: absolute;
            left: 26px;
            top: -50%;
            transform: translateY(-38%);
            z-index: 1;
        }
        .downloadTooltip .tooltiptext{
            transform: translateY(-24%);
        }
        .bg_tooltip .tooltiptextStart{
            transform: translateY(-26%);
        }

        .bg_tooltip .tooltiptextEnd{
            transform: translateY(-20%);
            width: 262px;
        }
        .bg_tooltip .tooltiptextHours{
            transform: translateY(-17%);
            width: 255px;
        }
        .bg_tooltip:hover .tooltiptext {
            visibility: visible;
        }
        .bg_tooltip .tooltiptext::after {
            content: " ";
            position: absolute;
            top: 50%;
            right: 99%;
            margin-top: -5px;
            border: solid #03a9f4;
            border-width: 0 2px 2px 0;
            display: inline-block;
            padding: 4px;
            transform: rotate(135deg);
            background-color: #ffff;
        }
    }
        .input-head{
            font-size: 14px;
            font-weight:500;
            margin-bottom:8px;
        }
        .input-head-error{
            font-size: 14px;
            font-weight:500;
            margin-bottom:8px;
            color: #eb7575;
        }
        .submit {
            & > div {
                text-align: right;
                margin-top: 50px;

                & > button {
                    min-width: 160px;
                    margin-bottom: 50px;
                }
            }
        }
        .ip-margin{
            margin-top:40px;
        }
        .addIP{
            height: 38px;
            border-radius: 50px;
            width: 226px;
            font-size:15px;
            svg{
                width:13px;
                margin-top:-2px;
                margin-right:3px;
            }
        }
        .savebtn{
            height: 38px;
            border-radius: 50px;
            width: 86px;
            color:#474747;
            border:1px solid #83d24b;
            font-size:15px;
            svg{
                width:13px;
                margin-top:-2px;
                margin-right:3px;
            }
        }
        .savebtn2{
            height: 38px;
            border-radius: 50px;
            width: 86px;
            background-color: #83d24b;
            color:white;
            font-size:15px;
        }
        .cancelbtn{
            width:87px;
            height:38px;
            border: 1px solid #eb7575; 
            font-size:15px;
            border-radius:50px;
        }
        .deletebtn{
            height: 38px;
            border-radius: 50px;
            width: 102px;
            font-size:15px;
            border: 1px solid #eb7575;
            background-color:unset !important;
            color:#474747;
            svg{
                width:13px;
                margin-top:-2px;
                margin-right:3px;
            }
        }
    }

    .select-wrapper .Select .Select-control {
        border: 1px solid black;
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;
