/*
 *
 * EditLocation constants
 *
 */
export const nameSpace = 'app/Location/Edit';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const GET_LOCATION = `${nameSpace}/GET_LOCATION`;
export const SET_LOCATION = `${nameSpace}/SET_LOCATION`;
export const EDIT_LOCATION = `${nameSpace}/EDIT_LOCATION`;
export const SET_EDIT_LOCATION = `${nameSpace}/SET_EDIT_LOCATION`;
export const LOADING = `${nameSpace}/LOADING`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
