/* eslint-disable no-param-reassign */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';

import makeSelectEditLocation, {
    makeSelecGetLocation,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectProducts
} from './selectors';
import * as getLocationActions from './actions';

import { getCompanySettingsSidebarLinks } from '../../../utils/sidebarHelper';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { browserHistory } from '../../../utils/BrowserHistory';

import Input from '../../../components/Input';
import A from '../../../components/A';
import { H2, H3, H4, H6 } from '../../../components/Typography';
import Sidebar from '../../../components/Sidebar';
import SnackBar from '../../../components/SnackBar';
import Button from '../../../components/Button';
import Icon from '../../../components/Icon';
import Loader from '../../../components/Loader';
import Radio from '../../../components/Radio';
import RadioGroup from '../../../components/RadioGroup';
import Select from '../../../components/Select';

import {
    PageWrapper,
    NavWrapper,
    LoadingStyles
} from './styles';
import { defaultTimezone, timezones } from '../../../utils/timezones';
import { DEFAULT_NO_OPTION, getCities, getCountries, getStates } from '../../../utils/Locations/locations';

/**
 *
 * EditLocation
 *
 */
export class EditLocation extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
        getlocationList: React.PropTypes.array,
        products: React.PropTypes.array,
        getLocation: React.PropTypes.func,
        params: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        editLocation: React.PropTypes.func,
        loading: React.PropTypes.bool
    }

    static defaultProps = {
        loading: false
    };

    constructor( props ) {
        super( props );

        this.state = {
            ipAddress: '',
            showButtons: false,
            editIP: null,
            fillData: [],
            data: {},
            countries: [],
            states: [],
            cities: [],
            country: {},
            state: {},
            city: {},
            validationHeadQ: false,
            validationLocationName: 'name',
            validationtimeZone: 'timezone',
            validationAddress1: 'address',
            validationAddress2: 'address',
            validationCountry: 'country',
            validationRegion: 'region',
            validationCity: 'city',
            validationZip: 'zip',
            validateIP: 'ip',
            nameExists: 'name',
            headquarter: ''
        };
    }

    componentDidMount() {
        this.props.getLocation();
        this.setState({
            countries: getCountries(),
        });
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.getlocationList !== this.props.getlocationList &&
        nextProps.getlocationList.map( ( value ) => value.id.toString() === this.props.params.id && this.setState({ data: value, headquarter: value.is_headquarters }) );
    }

    componentDidUpdate(prevProps, prevState) {
        if ( this.state.data.timezone !== prevState.data.timezone ) {
            const tzExists = timezones.find( ( tz ) => tz.value === this.state.data.timezone );
            if ( this.state.data.timezone === '' || !tzExists ) {
                this.setState({
                    data: {
                        ...this.state.data,
                        timezone: defaultTimezone.value
                    }
                });
            }
        }

        if ( this.state.data.country !== prevState.data.country ) {
            let country = this.state.countries.filter( ( value ) => value.label === this.state.data.country );
            country = country[ 0 ] ? country[ 0 ] : { label: this.state.data.country, value: this.state.data.country };
            this.setState({ country, states: this.getStates() });
        }

        if ( ( this.state.data.region !== prevState.data.region ) ) {
            let state = getStates( this.state.data.country.value ).filter( ( value ) => value.label === this.state.data.region );
            state = state[ 0 ] ? state[ 0 ] : { label: this.state.data.region, value: this.state.data.region };
            this.setState({ state, cities: this.getCities() });
        }

        if ( ( this.state.data.city !== prevState.data.city ) ) {
            let city = getCities( this.state.data.country.value, this.state.data.region.value ).filter( ( value ) => value.label === this.state.data.city );
            city = city[ 0 ] ? city[ 0 ] : { label: this.state.data.city, value: this.state.data.city };
            this.setState({ city });
        }
    }

    getStates() {
        if ( this.state.data.country === '' ) {
            return [];
        }

        const country = getCountries().filter( ( value ) => value.label === this.state.data.country );
        if ( country[ 0 ]) {
            const states = getStates( country[ 0 ].value );
            return states.length > 0 ? states : [{ value: this.state.data.country, label: this.state.data.country }];
        }

        return this.state.data.region ? [{ value: this.state.data.region, label: this.state.data.region }] : [DEFAULT_NO_OPTION];
    }

    getCities() {
        if ( this.state.data.country === '' || this.state.data.region === '' ) {
            return [];
        }

        const country = getCountries().filter( ( value ) => value.label === this.state.data.country );
        if ( country[ 0 ]) {
            const state = getStates( country[ 0 ].value ).filter( ( value ) => value.label === this.state.data.region );
            const cities = getCities( country[ 0 ].value, ( state && state[ 0 ]) ? state[ 0 ].value : null );
            return cities.length > 0 ? cities : [DEFAULT_NO_OPTION];
        }

        return this.state.data.city ? [{ value: this.state.data.city, label: this.state.data.city }] : [DEFAULT_NO_OPTION];
    }

    handleCountryChange( country ) {
        this.setState({
            data: {
                ...this.state.data,
                country: country.label || '',
                region: '',
                city: ''
            }
        });
    }

    handleRegionChange( region ) {
        const { data } = this.state;
        this.setState({ validationRegion: region.label,
            data: {
                ...data,
                region: region.label || '',
                city: ''
            }});
    }

    handleCityChange( city ) {
        const { data } = this.state;
        this.setState({ validationCity: city.label,
            data: {
                ...data,
                city: city.label || ''
            }});
    }

    addToList() {
        if ( this.validateIP() ) {
            this.setState({
                data: {
                    ...this.state.data,
                    ip_addresses: [
                        ...this.state.data.ip_addresses,
                        {
                            ip_address: this.state.ipAddress
                        }
                    ]
                }
            });
            this.setState({
                ipAddress: ''
            });
        }
    }

    deleteList( id ) {
        this.setState({
            data: {
                ...this.state.data,
                ip_addresses: this.state.data.ip_addresses.filter( ( arr, i ) => i !== id )
            }
        });
    }

    showTextbox( IpId ) {
        return this.state.editIP && this.state.editIP.id === IpId;
    }

    editList() {
        this.state.data.ip_addresses.map( ( elem, i ) => {
            if ( i === this.state.editIP.id ) {
                const value = this.state.data.ip_addresses;
                value[ i ] = { ip_address: this.state.editIP.ip };
                this.setState({
                    data: {
                        ...this.state.data,
                        ip_addresses: value
                    },
                    showButtons: false
                });
            }
            return !elem;
        });
    }

    handleSubmit = ( event ) => {
        event && event.preventDefault();

        const {
            name, timezone, address_bar, first_address_line, second_address_line, country, region, city, zip_code, ip_addresses, is_headquarters
        } = this.state.data;

        const updatedLocation = {
            id: this.props.params.id,
            name,
            timezone,
            address_bar,
            first_address_line,
            second_address_line,
            country,
            region,
            city,
            zip_code,
            ip_addresses,
            is_headquarters
        };

        if ( this.validateFields() ) {
            this.props.editLocation( updatedLocation );
        }
    }

    validateIP = () => {
        let valid = true;
        const regexExp = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/gi;
        if ( regexExp.test( this.state.ipAddress ) === false ) {
            this.setState({
                validationIP: ''
            });
            valid = false;
        }
        return valid;
    }

    validateFields = () => {
        let valid = true;
        const validatenames = this.props.getlocationList.filter( ( arr ) => arr.id !== this.state.data.id );
        if ( this.state.data.name === '' ) {
            this.setState({
                validationLocationName: ''
            });
            valid = false;
        } else if ( validatenames.map( ( value ) => value.name ).includes( this.state.data.name ) ) {
            this.setState({
                nameExists: ''
            });
            valid = false;
        }

        if ( this.state.data.timezone === '' ) {
            this.setState({
                validationtimeZone: ''
            });
            valid = false;
        }
        if ( this.state.data.first_address_line === '' ) {
            this.setState({
                validationAddress1: ''
            });
            valid = false;
        }
        if ( this.state.data.country === '' ) {
            this.setState({
                validationCountry: ''
            });
            valid = false;
        }
        if ( this.state.data.region === '' ) {
            this.setState({
                validationRegion: ''
            });
            valid = false;
        }
        if ( this.state.data.city === '' ) {
            this.setState({
                validationCity: ''
            });
            valid = false;
        }
        if ( this.state.data.zip_code === '' ) {
            this.setState({
                validationZip: ''
            });
            valid = false;
        }
        return valid;
    }

    /**
     *
     * EditLocation render method
     *
     */
    render() {
        const headquarterList = this.props.getlocationList.map( ( value ) => value.is_headquarters );
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });

        const { notification, loading } = this.props;
        const { data, showButtons, editIP, ipAddress, validationRegion, validationCity, nameExists, headquarter, validationIP, validationHeadQ, validationZip, validationLocationName, validationtimeZone, validationAddress1, validationAddress2, validationCountry } = this.state;
        return (
            <div>
                <Helmet
                    title="Edit Location"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Location' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/company-settings/company-structure/locations', true );
                            } }
                        >
                            &#8592; Back to Locations
                        </A>
                    </Container>
                </NavWrapper>
                <Sidebar items={ sidebarLinks } />
                <PageWrapper>
                    <Container>
                        {loading ?
                            <div className="loader" style={ { display: loading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Please wait...</H2>
                                </LoadingStyles>
                            </div> : ''}
                        {!loading ?
                            <div className="content">
                                <div className="heading">
                                    <H3>Edit Location</H3>
                                    <p>For multiple branch or work location company, fill-in the company location addresses with the valid IP for the employees' clock ins/outs.</p>
                                </div>
                                <div className="w-100 position-relative">
                                    <H3>Location</H3>
                                    <div className="row">
                                        <div className="col-md-3">
                                            <H4 className={ validationLocationName === '' ? 'input-head-error' : nameExists === '' ? 'input-head-error' : 'input-head' }>*Location name</H4>
                                            <Input
                                                className={ validationLocationName === '' ? 'content-input-error' : nameExists === '' ? 'content-input-error' : 'content-input' }
                                                id="location name"
                                                value={ data.name }
                                                name="name"
                                                onChange={ ( eve ) => this.setState({ validationLocationName: eve, data: { ...data, name: eve, address_bar: 'Searched data' }}) }
                                            />
                                            {validationLocationName === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : nameExists === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Record name already exists</p> : ''}
                                        </div>
                                        <div className="col-md-3">
                                            <H4 className={ validationtimeZone === '' ? 'input-head-error' : 'input-head' }>*Time Zone</H4>
                                            <Select
                                                id="timezone"
                                                value={ data.timezone || defaultTimezone }
                                                data={ timezones }
                                                onChange={ ( tz ) => {
                                                    this.setState({ validationtimeZone: tz.value, data: { ...data, timezone: tz.value }});
                                                } }
                                            />
                                            {validationtimeZone === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-3">
                                            <H4 className={ validationAddress1 === '' ? 'input-head-error' : 'input-head' }>* Address line 1</H4>
                                            <Input
                                                className={ validationAddress1 === '' ? 'content-input-error' : 'content-input' }
                                                id="address1"
                                                value={ data.first_address_line }
                                                name="first_address_line"
                                                onChange={ ( eve ) => this.setState({ validationAddress1: eve, data: { ...data, first_address_line: eve }}) }
                                            />
                                            {validationAddress1 === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                        </div>
                                        <div className="col-md-3">
                                            <H4 className={ validationAddress2 === '' ? 'input-head-error' : 'input-head' }>Address line 2</H4>
                                            <Input
                                                className={ validationAddress2 === '' ? 'content-input-error' : 'content-input' }
                                                id="address2"
                                                value={ data.second_address_line }
                                                name="second_address_line"
                                                onChange={ ( eve ) => this.setState({ validationAddress2: eve, data: { ...data, second_address_line: eve }}) }
                                            />
                                            {validationAddress2 === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-3">
                                            <H4 className={ validationCountry === '' ? 'input-head-error' : 'input-head' }>* Country</H4>
                                            <Select
                                                id="country"
                                                name="country"
                                                data={ this.state.countries }
                                                value={ this.state.country }
                                                onChange={ ( country ) => this.handleCountryChange( country ) }
                                            />
                                            {validationCountry === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                        </div>
                                        <div className="col-md-3">
                                            <H4 className={ validationRegion === '' ? 'input-head-error' : 'input-head' }>* Region</H4>
                                            <Select
                                                id="region"
                                                name="region"
                                                data={ this.state.states }
                                                value={ this.state.state }
                                                onChange={ ( region ) => this.handleRegionChange( region ) }
                                                disabled={ data.country === '' }
                                            />
                                            {validationRegion === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-3">
                                            <H4 className={ validationCity === '' ? 'input-head-error' : 'input-head' }>* City</H4>
                                            <Select
                                                id="city"
                                                name="city"
                                                data={ this.state.cities }
                                                value={ this.state.city }
                                                onChange={ ( city ) => this.handleCityChange( city ) }
                                                disabled={ data.country === '' || data.region === '' }
                                            />
                                            {validationCity === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                        </div>
                                        <div className="col-md-3">
                                            <H4 className={ validationZip === '' ? 'input-head-error' : 'input-head' }>* Zip code</H4>
                                            <Input
                                                className={ validationZip === '' ? 'content-input-error' : 'content-input' }
                                                id="zip"
                                                value={ data.zip_code }
                                                name="zip_code"
                                                onChange={ ( eve ) => this.setState({ validationZip: eve, data: { ...data, zip_code: eve }}) }
                                            />
                                            {validationZip === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <p className={ headquarterList.includes( true ) && headquarter === false && validationHeadQ ? 'schdule_tooltip input-head-error' : 'schdule_tooltip input-head' }>* Set as headquarters
                                        <div className="bg_tooltip downloadTooltip">
                                            <label htmlFor="download">?</label>
                                            <span className="tooltiptext">The address of the location set as a Headquarters will be reflected on the government forms generated through Salarium.<br />
                                            </span>
                                        </div>
                                            </p>
                                            <RadioGroup
                                                horizontal
                                                value={ `${data.is_headquarters}` }
                                                onChange={ ( value ) => {
                                                    this.setState({
                                                        validationHeadQ: value === 'true',
                                                        data: { ...data, is_headquarters: value }});
                                                } }
                                            >
                                                <Radio value="true" pointColor={ headquarterList.includes( true ) && headquarter === false && validationHeadQ && '#eb7575' }>Yes</Radio>
                                                <Radio value="false" rootColor={ headquarterList.includes( true ) && headquarter === false && validationHeadQ && '#eb7575' }>No</Radio>
                                            </RadioGroup>
                                            { headquarterList.includes( true ) && headquarter === false && this.state.validationHeadQ ? <p className="headquarters-warn" >There is already an existing location tagged as headquarters.</p> : ''}
                                        </div>
                                    </div>
                                    <H3>IP Address</H3>
                                    <p>You can nominate multiple IP addresses in one location. This will become the basis of valid time logs (clock ins/outs) to be processed in Time and Attendance module. In the event that a location has no specified IP address, this will mean that all time logs from any device and locations are valid.</p>
                                    <span>
                                        {data.ip_addresses && data.ip_addresses.map( ( IpValue, i ) => (

                                            <div className="col-md-12" key={ i } style={ { marginBottom: '30px', marginTop: '30px' } }>
                                                <div className="col-md-3">
                                                    <H6 style={ { fontWeight: 400 } }>IP address</H6>
                                                </div>
                                                <div className="col-md-3">
                                                    { this.showTextbox( i ) && showButtons === true
                                                ? <div style={ { width: '100%' } }>
                                                    <Input
                                                        id="name" type="text"
                                                        value={ editIP.ip }
                                                        onChange={ ( eve ) => this.setState({ editIP: { ...editIP, ip: eve }}) }
                                                    />
                                                </div>
                                                : <div>{ IpValue.ip_address }</div>
                                            }
                                                </div>
                                                <div className="col-md-3">
                                                    { this.showTextbox( i ) && showButtons === true
                                            ? <div>
                                                <Button
                                                    className="savebtn2"
                                                    label={ <span>Save</span> }
                                                    type="action"
                                                    size="small"
                                                    onClick={ () => this.editList() }
                                                ></Button>
                                                <Button
                                                    className="cancelbtn"
                                                    label={ <span>Cancel</span> }
                                                    type="neutral"
                                                    size="small"
                                                    onClick={ () => this.setState({ showButtons: false }) }
                                                ></Button>
                                            </div>
                                            : <div>
                                                <Button
                                                    className="savebtn"
                                                    label={ <span><Icon name="pencil" className="icon" /> Edit</span> }
                                                    type="neutral"
                                                    size="small"
                                                    onClick={ () => this.setState({ showButtons: true, editIP: { id: i, ip: IpValue.ip_address }}) }
                                                ></Button>
                                                <Button
                                                    className="deletebtn"
                                                    label={ <span><Icon name="trash" className="icon" /> Delete</span> }
                                                    type="danger"
                                                    size="small"
                                                    onClick={ () => this.deleteList( i ) }
                                                ></Button>
                                            </div>
                                            }
                                                </div>
                                            </div>
                                ) )}
                                        <div className="col-md-12 ip-margin">
                                            <div className="col-md-3">
                                                <H6 style={ { fontWeight: 400 } }>IP address</H6>
                                            </div>
                                            <div className="col-md-3">
                                                <Input
                                                    className={ validationIP === '' ? 'content-input-error' : 'content-input' }
                                                    id="Ipaddress"
                                                    value={ ipAddress }
                                                    onChange={ ( eve ) => this.setState({ validationIP: eve, ipAddress: eve }) }
                                                />
                                                {validationIP === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Invalid IP Address</p> : ''}
                                            </div>
                                            <div className="col-md-3">
                                                <Button
                                                    className="addIP"
                                                    label={ <span><Icon name="plus" className="icon" /> Add IP address</span> }
                                                    type="neutral"
                                                    size="default"
                                                    onClick={ () => this.addToList() }
                                                ></Button>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div className="row submit">
                                    <div className="col-xs-12">
                                        <Button
                                            label="Cancel"
                                            type="action"
                                            size="large"
                                            alt
                                            onClick={ () => browserHistory.push( '/company-settings/company-structure/locations', true ) }
                                        />
                                        <Button
                                            label={ loading ? <Loader /> : 'Update' }
                                            type="action"
                                            size="large"
                                            onClick={ ( eve ) => this.handleSubmit( eve ) }
                                        />
                                    </div>
                                </div>
                            </div> : ''}
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    EditLocation: makeSelectEditLocation(),
    products: makeSelectProducts(),
    getlocationList: makeSelecGetLocation(),
    locationEdit: makeSelectEditLocation(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        getLocationActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EditLocation );
