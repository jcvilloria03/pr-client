import {
    DEFAULT_ACTION,
    EDIT_LOCATION,
    GET_LOCATION,
    NOTIFICATION
} from './constants';

/**
 *
 * EditLocation actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Add Location action
 *
 */
export function editLocation( payload ) {
    return {
        type: EDIT_LOCATION,
        payload
    };
}

/**
 *
 * Get location action
 *
 */
export function getLocation() {
    return {
        type: GET_LOCATION
    };
}

/**
 *
 * Notification action
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
