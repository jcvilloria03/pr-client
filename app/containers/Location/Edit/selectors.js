import { createSelector } from 'reselect';

/**
 * Direct selector to the editLocation state domain
 */
const selectEditLocationDomain = () => ( state ) => state.get( 'editLocation' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

/**
 * Default selector used by EditLocation
 */

const makeSelectEditLocation = () => createSelector(
  selectEditLocationDomain(),
  ( substate ) => substate.toJS()
);

const makeSelecGetLocation = () => createSelector(
  selectEditLocationDomain(),
  ( substate ) => substate.get( 'getlocation' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectEditLocationDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectSetEditLocation = () => createSelector(
  selectEditLocationDomain(),
  ( substate ) => substate.get( 'editLocation' )
);

const makeSelectLoading = () => createSelector(
  selectEditLocationDomain(),
  ( substate ) => substate.get( 'loading' )
);

export default makeSelectEditLocation;
export {
  selectEditLocationDomain,
  makeSelectProducts,
  makeSelecGetLocation,
  makeSelectNotification,
  makeSelectSetEditLocation,
  makeSelectLoading
};
