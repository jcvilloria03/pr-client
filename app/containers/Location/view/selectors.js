import { createSelector } from 'reselect';

/**
 * Direct selector to the location state domain
 */
const selectLocationDomain = () => ( state ) => state.get( 'location' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

/**
 * Default selector used by Location
 */

const makeSelectLocation = () => createSelector(
  selectLocationDomain(),
  ( substate ) => substate.toJS()
);

const makeSelecGetLocation = () => createSelector(
  selectLocationDomain(),
  ( substate ) => substate.get( 'getlocation' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectLocationDomain(),
( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectLocationDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export default makeSelectLocation;
export {
  selectLocationDomain,
  makeSelectProducts,
  makeSelecGetLocation,
  makeSelectLoading,
  makeSelectNotification
};
