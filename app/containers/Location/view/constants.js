/*
 *
 * Location constants
 *
 */
export const nameSpace = 'app/Location/view';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const GET_LOCATION = `${nameSpace}/GET_LOCATION`;
export const SET_LOCATION = `${nameSpace}/SET_LOCATION`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
export const SET_LOADING = `${nameSpace}/SET_LOADING`;
export const DELETE_LOCATION = `${nameSpace}/DELETE_LOCATION`;
export const SET_DELETE_LOCATION = `${nameSpace}/SET_DELETE_LOCATION`;
