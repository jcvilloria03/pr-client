import {
    DEFAULT_ACTION,
    DELETE_LOCATION,
    GET_LOCATION,
    NOTIFICATION
} from './constants';

/**
 *
 * Location actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Get location action
 *
 */
export function getLocation() {
    return {
        type: GET_LOCATION
    };
}

/**
 *
 * Delete location action
 *
 */
export function deleteLocation( payload ) {
    return {
        type: DELETE_LOCATION,
        payload
    };
}

/**
 *
 * Notification action
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

