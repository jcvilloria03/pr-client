import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    NOTIFICATION_SAGA,
    SET_DELETE_LOCATION,
    SET_LOADING,
    SET_LOCATION
} from './constants';

const initialState = fromJS({
    getlocation: [],
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    }
});

/**
 *
 * Location reducer
 *
 */
function locationReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LOCATION:
            return state.set( 'getlocation', fromJS( action.payload ) );
        case SET_DELETE_LOCATION:
            return state.set( action.payload );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default locationReducer;
