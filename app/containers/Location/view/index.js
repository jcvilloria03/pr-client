import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';

import makeSelectLocation, {
    makeSelecGetLocation,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectProducts
} from './selectors';
import * as getLocationActions from './actions';

import { browserHistory } from '../../../utils/BrowserHistory';
import { formatDeleteLabel, formatPaginationLabel } from '../../../utils/functions';
import { getCompanySettingsSidebarLinks } from '../../../utils/sidebarHelper';
import { subscriptionService } from '../../../utils/SubscriptionService';

import Button from '../../../components/Button';
import { getIdsOfSelectedRows } from '../../../components/Table/helpers';
import Sidebar from '../../../components/Sidebar';
import SnackBar from '../../../components/SnackBar';
import SalConfirm from '../../../components/SalConfirm';
import SalDropdown from '../../../components/SalDropdown';
import Table from '../../../components/Table';
import { H3, H5, H2 } from '../../../components/Typography';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';
import { defaultTimezone, timezones } from '../../../utils/timezones';

/**
 *
 * Location
 *
 */
export class Location extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
        getlocationList: React.PropTypes.array,
        products: React.PropTypes.array,
        getLocation: React.PropTypes.func,
        deleteLocation: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            show: false,
            showModel: false
        };
    }

    componentDidMount() {
        this.props.getLocation();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.getlocationList !== this.props.getlocationList && this.setState({ label: formatPaginationLabel( this.locationTable.tableComponent.state ) });
    }

    handleTableChanges = ( tableProps = this.locationTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    locationDelete = () => {
        const payload = getIdsOfSelectedRows( this.locationTable );
        this.props.deleteLocation( payload );
        setTimeout( () => {
            this.props.getLocation();
        }, 4000 );
        this.setState({ show: false });
    }

    getTimezoneLabel = ( timezone ) => {
        const tz = timezones.find( ( tz ) => tz.value === timezone );
        return tz ? tz.label : defaultTimezone.label;
    }
    /**
     *
     * Location render method
     *
     */
    render() {
        const tableColumn = [
            {
                id: 'name',
                header: 'Location Name',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>{row.name}{row.is_headquarters === true ? ' (Headquarters)' : ''}</div>
                )
            },
            {
                id: 'name',
                header: 'Time Zone',
                sortable: false,
                minWidth: 100,
                render: ({ row }) => (
                    <div>{this.getTimezoneLabel( row.timezone )}</div>
                )
            },
            {
                id: 'name',
                header: 'Address',
                sortable: false,
                minWidth: 500,
                render: ({ row }) => (
                    <div>{`${row.first_address_line}, ${row.city}, ${row.region}`}</div>
                )
            },
            {
                id: 'name',
                header: '',
                sortable: false,
                minWidth: 100,
                render: ({ row }) => (
                    <Button
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => browserHistory.push( `/company-settings/company-structure/locations/${row.id}/edit`, true ) }
                    ></Button>
                )
            }
        ];

        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });

        return (
            <div>
                <Helmet
                    title="Location"
                    meta={ [
                        { name: 'description', content: 'Description of Location' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.locationDelete }
                    onClose={ () => this.setState({ showModel: false }) }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Are you sure you want to delete these Location?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Location"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModel }
                />
                <Sidebar items={ sidebarLinks } />
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Loading Locations</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                            <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                                <div className="heading">
                                    <H3>Locations</H3>
                                    <p>The address of the location set as a Headquarters will be reflected on the government forms generated through Salarium.</p>
                                </div>

                                <div className="title">
                                    <H5>Locations List</H5>
                                    <span className="title-Content">
                                        { this.state.show
                                        ? <p className="mb-0 mr-1">{this.state.deleteLabel}</p>
                                        : <p className="mb-0 mr-1">{this.state.label}</p>
                                    }
                                        <div>
                                            {this.state.show ? (
                                                <SalDropdown
                                                    dropdownItems={ [
                                                        {
                                                            label: 'Delete',
                                                            children: <div>Delete</div>,
                                                            onClick: () => this.setState({ showModel: true })
                                                        }
                                                    ] }
                                                />
                                            ) :
                                                ( <Button
                                                    id="button-filter"
                                                    label="Add Location"
                                                    type="action"
                                                    onClick={ () => browserHistory.push( '/company-settings/company-structure/locations/add', true ) }
                                                /> )
                                            }
                                        </div>
                                    </span>
                                </div>

                                <Table
                                    data={ this.props.getlocationList }
                                    columns={ tableColumn }
                                    pagination
                                    onDataChange={ this.handleTableChanges }
                                    selectable
                                    onSelectionChange={ ({ selected }) => {
                                        const selectionLength = selected.filter( ( row ) => row ).length;
                                        this.setState({
                                            show: selectionLength > 0,
                                            deleteLabel: formatDeleteLabel( selectionLength )
                                        });
                                    } }
                                    ref={ ( ref ) => { this.locationTable = ref; } }
                                />
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    Location: makeSelectLocation(),
    products: makeSelectProducts(),
    getlocationList: makeSelecGetLocation(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        getLocationActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Location );
