import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import {
    DELETE_LOCATION,
    GET_LOCATION,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_DELETE_LOCATION,
    SET_LOADING,
    SET_LOCATION
} from './constants';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

import { REINITIALIZE_PAGE } from '../../App/constants';

/**
 * GET location
 */
export function* getLocation() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/time_attendance_locations?mode=DEFAULT_MODE&name=`, { method: 'GET' });

        yield put({
            type: SET_LOCATION,
            payload: res.data || []
        });
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize Page
 */
export function* reinitializePage() {
    yield call( getLocation );
}

/**
 * Delete Projects
 */
export function* deleteLocation({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();
        yield put({
            type: SET_DELETE_LOCATION,
            payload: true
        });

        const res = yield call( Fetch, '/company/location/is_in_use', {
            method: 'POST',
            data: { company_id: companyId, ids: payload }
        });
        if ( res.in_use === 0 ) {
            yield call( Fetch, '/time_attendance_locations/bulk_delete', {
                method: 'DELETE',
                data: { company_id: companyId, location_ids: payload }
            });

            yield call( notifyUser, {
                title: 'Deleted',
                message: 'Remove content from Location',
                show: 'true',
                type: 'success'
            });
        } else {
            yield call( notifyUser, {
                title: 'Deleted',
                message: 'This location id is not in use..',
                show: 'true',
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DELETE_LOCATION,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Watch for get Location
 */
export function* watchForGetLocation() {
    const watcher = yield takeEvery( GET_LOCATION, getLocation );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For ReinitializePage
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For Delete Project
 */
export function* watchForDeleteLocation() {
    const watcher = yield takeEvery( DELETE_LOCATION, deleteLocation );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Notification
 */
export function* watchFroNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetLocation,
    watchFroNotification,
    watchForDeleteLocation
];
