import { WITHHOLDING_TAX_TYPE } from './constants';

export const getLabelForWithholdingTaxType = ( withholdingTaxType ) => {
    switch ( withholdingTaxType ) {
        case WITHHOLDING_TAX_TYPE.COMPENSATION:
            return 'Compensation';
        case WITHHOLDING_TAX_TYPE.MIN_WAGE_EARNER:
            return 'Min Wage Earner';
        case WITHHOLDING_TAX_TYPE.EXPANDED:
            return 'Expanded';
        case WITHHOLDING_TAX_TYPE.SPECIAL:
            return 'Special';
        case WITHHOLDING_TAX_TYPE.NONE:
            return 'None';
        default:
            return 'N/A';
    }
};
