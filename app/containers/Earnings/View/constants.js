export const INITIALIZE_DATA = 'app/Earnings/View/INITIALIZE_DATA';
export const NOTIFY_USER = 'app/Earnings/View/NOTIFY_USER';

export const SET_LOADING = 'app/Earnings/View/SET_LOADING';
export const SET_FILTER_DATA = 'app/Earnings/View/SET_FILTER_DATA';
export const SET_NOTIFICATION = 'app/Earnings/View/SET_NOTIFICATION';
export const SET_EARNINGS = 'app/Earnings/View/SET_EARNINGS';
