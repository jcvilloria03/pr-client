import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_FILTER_DATA,
    SET_EARNINGS,
    SET_NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    filterData: {},
    earnings: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case SET_EARNINGS:
            return state.set( 'earnings', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
};
