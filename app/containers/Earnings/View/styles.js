import styled from 'styled-components';

export const PageWrapper = styled.div`
    .content {
        margin-top: 40px;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;

            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 600;
                font-size: 20px;
            }
        }
    }
    .search-wrapper {
        flex-grow: 1;

        .search {
            width: 300px;
            border: 1px solid #333;
            border-radius: 30px;

            input {
                border: none;
            }
        }

        p {
            display: none;
        }

        .input-group,
        .form-control {
            background-color: transparent;
        }

        .input-group-addon {
            background-color: transparent;
            border: none;
        }

        .isvg {
            display: inline-block;
            width: 1rem;
        }
    }

    .leave-types {
        clear:both;
    }

    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }

    .editing {
        input{
            height: 25px;
        }

        p {
            display:none;
        }
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
    z-index: 10;
`;

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

export const MessageWrapperStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding-top: 90px;
`;

export const AlignRight = styled.div`
    text-align: right;

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        margin-top: 20px;

        button {
            min-width: 120px;
        }
    }
`;
