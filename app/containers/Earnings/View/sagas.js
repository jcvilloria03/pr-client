import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';

import {
    SET_LOADING,
    SET_FILTER_DATA,
    SET_EARNINGS,
    SET_NOTIFICATION,
    INITIALIZE_DATA,
    NOTIFY_USER
} from './constants';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();

        const filterData = {};
        const locations = yield call( Fetch, `/philippine/company/${companyId}/locations`, { method: 'GET' });
        const departments = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });
        const positions = yield call( Fetch, `/company/${companyId}/positions`, { method: 'GET' });
        const teams = yield call( Fetch, `/company/${companyId}/teams`, { method: 'GET' });

        const earnings = yield call( Fetch, `/company/${companyId}/earnings`, { method: 'GET' });

        filterData.locations = locations.data;
        filterData.departments = departments.data;
        filterData.positions = positions.data;
        filterData.teams = teams.data;

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });

        yield put({
            type: SET_EARNINGS,
            payload: earnings.data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Watcher for INITIALIZE_DATA
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFY_USER
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFY_USER, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForNotifyUser,
    watchForReinitializePage
];
