import { createSelector } from 'reselect';

/**
 * Direct selector to the view earnings state domain
 */
const makeSelectViewEarningsDomain = () => (
    ( state ) => ( state.get( 'viewEarnings' ) )
);

const makeSelectLoading = () => createSelector(
    makeSelectViewEarningsDomain(),
    ( state ) => state.get( 'loading' )
);

const makeSelectFilterData = () => createSelector(
    makeSelectViewEarningsDomain(),
    ( state ) => state.get( 'filterData' ).toJS()
);

const makeSelectEarnings = () => createSelector(
    makeSelectViewEarningsDomain(),
    ( state ) => state.get( 'earnings' ).toJS()
);

const makeSelectNotification = () => createSelector(
    makeSelectViewEarningsDomain(),
    ( state ) => state.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectFilterData,
    makeSelectEarnings,
    makeSelectNotification
};
