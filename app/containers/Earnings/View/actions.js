import {
    INITIALIZE_DATA,
    SET_NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData() {
    return {
        type: INITIALIZE_DATA
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: SET_NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Resets the state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
