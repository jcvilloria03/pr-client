import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import SubHeader from '../../SubHeader';

import Filter from '../Filter';

import Icon from '../../../components/Icon';
import Input from '../../../components/Input';
import Table from '../../../components/Table';
import Button from '../../../components/Button';
import SnackBar from '../../../components/SnackBar';
import { H2, H3, H5, P } from '../../../components/Typography';

import { getLabelForWithholdingTaxType } from '../utils';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { formatPaginationLabel, getEmployeeFullName } from '../../../utils/functions';
import { subscriptionService } from '../../../utils/SubscriptionService';

import {
    PageWrapper,
    LoadingStyles
} from './styles';

import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectFilterData,
    makeSelectEarnings
} from './selectors';

import * as actions from './actions';

/**
 * View Earnings
 */
class ViewEarnings extends React.Component {
    static propTypes = {
        loading: React.PropTypes.bool,
        earnings: React.PropTypes.array,
        filterData: React.PropTypes.object,
        resetStore: React.PropTypes.func,
        initializeData: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    }

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: this.props.earnings,
            showFilter: false,
            hasFiltersApplied: false,
            permission: {
                view: false,
                create: false,
                edit: false
            }
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.earnings',
            'create.earnings',
            'edit.earnings'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.earnings' ];

            if ( authorized ) {
                this.setState({ permission: Object.assign( this.state.permission, {
                    view: authorization[ 'view.earnings' ],
                    create: authorization[ 'create.earnings' ],
                    edit: authorization[ 'edit.earnings' ]
                }) });
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });

        this.props.initializeData();
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.earnings.length !== 0 ) {
            this.setState({
                displayedData: nextProps.earnings
            }, () => {
                this.handleSearch();
            });
        }
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    getTableColumns = () => (
        [
            {
                id: 'id',
                show: false
            },
            {
                id: 'employee_name',
                header: 'Employee Name',
                sortable: true,
                minWidth: 180,
                accessor: ( row ) => (
                    getEmployeeFullName( row.employee )
                )
            },
            {
                id: 'basic_pay',
                header: 'Basic Pay',
                sortable: true,
                minWidth: 130,
                accessor: ( row ) => (
                    row.active_basic_pay ? `Php ${row.active_basic_pay.amount}` : 'N/A'
                )
            },
            {
                id: 'withholding_tax_type',
                header: 'Withholding Tax Type',
                sortable: true,
                minWidth: 220,
                accessor: ( row ) => (
                    getLabelForWithholdingTaxType( row.with_holding_tax_type )
                )
            },
            {
                id: 'attendance',
                header: 'Attendance',
                sortable: true,
                minWidth: 140,
                accessor: ( row ) => (
                    row.attendance_data_required ? 'Yes' : 'No'
                )
            },
            {
                id: 'urh_pay',
                header: 'URH Pay',
                sortable: true,
                minWidth: 120,
                accessor: ( row ) => (
                    row.entitled_unworked_regular_holiday_pay !== null
                        ? row.entitled_unworked_regular_holiday_pay ? 'Yes' : 'No'
                        : 'N/A'
                )
            },
            {
                id: 'ush_pay',
                header: 'USH Pay',
                sortable: true,
                minWidth: 120,
                accessor: ( row ) => (
                    row.entitled_unworked_special_holiday_pay !== null
                    ? row.entitled_unworked_special_holiday_pay ? 'Yes' : 'No'
                    : 'N/A'
                )
            },
            {
                id: 'overtime',
                header: 'Overtime',
                sortable: true,
                minWidth: 120,
                accessor: ( row ) => (
                    row.entitled_overtime ? 'Yes' : 'No'
                )
            },
            {
                header: ' ',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.state.permission.view ? '' : 'hide' }
                        label={ <span>View</span> }
                        type="grey"
                        size="small"
                        to={ `/earnings/${row.id}/detail` }
                    />
                )
            }
        ]
    )

    tableIsNotMounted = () => (
        !this.earningsTable || !this.earningsTable.tableComponent || !this.earningsTable.tableComponent.state
    )

    tableHasSelectedRows = () => {
        if ( this.tableIsNotMounted() ) {
            return false;
        }

        return this.earningsTable.state.selected.filter( ( item ) => item ).length !== 0;
    }

    handleSearch = ( term = '' ) => {
        const matchingRows = this.earningsTable.getFilteredData( this.props.earnings, term );

        this.setState({
            displayedData: term ? matchingRows : this.props.earnings
        }, () => {
            this.handleTableChanges();
        });
    }

    handleTableChanges = ( tableProps = this.earningsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    applyFilters = ( filters ) => {
        const withholdingTaxTypeFiltersValues =
            filters
                .filter( ( f ) => f.type === 'withholding_tax_type' )
                .map( ( f ) => f.value );

        const locationFiltersValues =
            filters
                .filter( ( f ) => f.type === 'location' )
                .map( ( f ) => f.value );

        const departmentFiltersValues =
            filters
                .filter( ( f ) => f.type === 'department' )
                .map( ( f ) => f.value );

        const positionFiltersValues =
            filters
                .filter( ( f ) => f.type === 'position' )
                .map( ( f ) => f.value );

        const filteredData = this.props.earnings.filter( ( item ) => {
            const withholdingTaxTypeFilterMatching =
                withholdingTaxTypeFiltersValues.length !== 0
                    ? withholdingTaxTypeFiltersValues.includes( item.with_holding_tax_type )
                    : true;

            const locationFiltersMatching =
                locationFiltersValues.length !== 0
                    ? locationFiltersValues.includes( item.employee.location_id )
                    : true;

            const departmentFiltersMatching =
                departmentFiltersValues.length !== 0
                    ? departmentFiltersValues.includes( item.employee.department_id )
                    : true;

            const positionFiltersMatching =
                positionFiltersValues.length !== 0
                    ? positionFiltersValues.includes( item.employee.position_id )
                    : true;

            return (
                withholdingTaxTypeFilterMatching && locationFiltersMatching && departmentFiltersMatching && positionFiltersMatching
            );
        });

        this.setState({
            displayedData: filters.length !== 0 ? filteredData : this.props.earnings,
            hasFiltersApplied: filters.length !== 0
        }, () => {
            this.handleTableChanges();
        });
    }

    resetFilters = () => {
        this.setState({
            showFilter: false,
            displayedData: this.props.earnings,
            hasFiltersApplied: false
        }, () => {
            this.handleSearch();
        });
    }

    toggleFilter = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    render() {
        return (
            <div>
                <Helmet
                    title="Earnings"
                    meta={ [
                        { name: 'description', content: 'Description of Earnings' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Earnings.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Earnings</H3>
                                <P>You may view a complete list of employees&#39; earnings through this page.</P>
                                <Button
                                    label="Batch Update"
                                    size="large"
                                    type="action"
                                    onClick={ () => { browserHistory.push( '/payroll/earnings/batch-update', true ); } }
                                />
                            </div>
                            <div className="title">
                                <H5>Employee List</H5>
                                <div className="search-wrapper">
                                    <Input
                                        className="search"
                                        id="search"
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                        onChange={ () => {
                                            this.handleSearch(
                                                this.searchInput.state.value.toLowerCase()
                                            );
                                        } }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                        placeholder="Search for an employee"
                                    />
                                </div>
                                <span>
                                    <div>
                                        { this.state.label }
                                        &nbsp;
                                        <Button
                                            label={ <span><Icon name="filter" className="filter-icon" /> Filter</span> }
                                            type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                            onClick={ this.toggleFilter }
                                        />
                                    </div>
                                </span>
                            </div>
                            <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                                <Filter
                                    filterData={ this.props.filterData }
                                    onCancel={ () => { this.resetFilters(); } }
                                    onApply={ ( values ) => { this.applyFilters( values ); } }
                                />
                            </div>
                            <Table
                                data={ this.state.displayedData }
                                columns={ this.getTableColumns() }
                                pagination
                                ref={ ( ref ) => { this.earningsTable = ref; } }
                                selectable
                                onDataChange={ this.handleTableChanges }
                                onSelectionChange={ () => {
                                    this.setState({
                                        isAddToSpecialPayRunButtonDisabled: !this.tableHasSelectedRows()
                                    });
                                } }
                            />
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    filterData: makeSelectFilterData(),
    earnings: makeSelectEarnings(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators( actions, dispatch )
);

export default connect( mapStateToProps, mapDispatchToProps )( ViewEarnings );
