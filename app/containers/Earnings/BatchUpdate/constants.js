/*
 *
 * Batch Update constants
 *
 */

export const SUBMIT_FORM = 'app/Earnings/BatchUpdate/SUBMIT_FORM';

export const UPLOAD_EARNINGS = 'app/Earnings/BatchUpdate/UPLOAD_EARNINGS';
export const SET_BATCH_UPLOAD_JOB_ID = 'app/Earnings/BatchUpdate/SET_BATCH_UPLOAD_JOB_ID';
export const SET_BATCH_UPLOAD_STATUS = 'app/Earnings/BatchUpdate/SET_BATCH_UPLOAD_STATUS';
export const SET_BATCH_UPLOAD_ERRORS = 'app/Earnings/BatchUpdate/SET_BATCH_UPLOAD_ERRORS';
export const GET_EARNINGS_PREVIEW = 'app/Earnings/BatchUpdate/GET_EARNINGS_PREVIEW';
export const SET_SAVING_STATUS = 'app/Earnings/BatchUpdate/SET_SAVING_STATUS';
export const SET_SAVING_ERRORS = 'app/Earnings/BatchUpdate/SET_SAVING_ERRORS';
export const SAVE_EARNINGS = 'app/Earnings/BatchUpdate/SAVE_EARNINGS';
export const SET_EARNINGS_PREVIEW_STATUS = 'app/Earnings/BatchUpdate/SET_EARNINGS_PREVIEW_STATUS';
export const SET_EARNINGS_PREVIEW_DATA = 'app/Earnings/BatchUpdate/SET_EARNINGS_PREVIEW_DATA';
export const RESET_EARNINGS_PREVIEW = 'app/Earnings/BatchUpdate/RESET_EARNINGS_PREVIEW';

export const NOTIFICATION_SAGA = 'app/Earnings/BatchUpdate/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Earnings/BatchUpdate/NOTIFICATION';
