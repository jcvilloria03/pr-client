import { createSelector } from 'reselect';

/**
 * Direct selector to the batch update earnings state domain
 */
const selectBatchUpdateEarningsDomain = () => ( state ) => state.get( 'batchUpdateEarnings' );

const makeSelectLoading = () => createSelector(
    selectBatchUpdateEarningsDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectFormOptions = () => createSelector(
    selectBatchUpdateEarningsDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectBatchUpdateEarningsDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectBatchUpdateEarningsDomain(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectBatchUpdateEarningsDomain(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectBatchUpdateEarningsDomain(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectEarningsPreview = () => createSelector(
    selectBatchUpdateEarningsDomain(),
    ( substate ) => substate.get( 'earningsPreview' ).toJS()
);

const makeSelectSaving = () => createSelector(
    selectBatchUpdateEarningsDomain(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectBatchUpdateEarningsDomain(),
    ( substate ) => substate.get( 'submitted' )
);

export {
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectEarningsPreview,
    makeSelectSaving,
    makeSelectSubmitted,
    makeSelectNotification
};
