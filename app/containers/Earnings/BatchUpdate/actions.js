import {
    UPLOAD_EARNINGS,
    SAVE_EARNINGS,
    NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Upload earnings
 */
export function uploadEarnings( payload ) {
    return {
        type: UPLOAD_EARNINGS,
        payload
    };
}

/**
 * Save earnings
 */
export function saveEarnings( payload ) {
    return {
        type: SAVE_EARNINGS,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
