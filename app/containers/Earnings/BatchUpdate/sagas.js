import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { formatFeedbackMessage } from '../../../utils/functions';

import {
    UPLOAD_EARNINGS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    GET_EARNINGS_PREVIEW,
    SET_SAVING_STATUS,
    SET_SAVING_ERRORS,
    SAVE_EARNINGS,
    SET_EARNINGS_PREVIEW_STATUS,
    SET_EARNINGS_PREVIEW_DATA,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

/**
 * Uploads the CSV of earnings to add and starts the validation process
 */
export function* uploadEarnings({ payload }) {
    try {
        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: {}
        });

        const companyId = company.getLastActiveCompanyId();

        const data = new FormData();

        data.append( 'company_id', companyId );
        data.append( 'file', payload.file );

        const upload = yield call( Fetch, '/earning/upload', {
            method: 'POST',
            data
        });

        if ( upload.id ) {
            yield put({
                type: SET_BATCH_UPLOAD_JOB_ID,
                payload: upload.id
            });

            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: 'validation_queued'
            });

            yield call( checkValidation, { payload: { jobId: upload.id, step: 'validation' }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const check = yield call(
            Fetch,
            `/earning/upload/status?job_id=${payload.jobId}&company_id=${companyId}&step=${payload.step}`,
            { method: 'GET' }
        );

        if ( payload.step === 'validation' ) {
            if ( check.status === 'validation_failed' ) {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_EARNINGS_PREVIEW_DATA,
                    payload: []
                });

                yield put({
                    type: SET_BATCH_UPLOAD_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'validated' ) {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_BATCH_UPLOAD_ERRORS,
                    payload: {}
                });

                yield call( getEarningsPreview, { payload: { jobId: payload.jobId }});
            } else {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield call( delay, 2000 );
                yield call( checkValidation, { payload: { jobId: payload.jobId, step: 'validation' }});
            }
        } else if ( payload.step === 'save' ) {
            if ( check.status === 'save_failed' ) {
                yield put({
                    type: SET_SAVING_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_SAVING_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'saved' ) {
                yield put({
                    type: SET_SAVING_STATUS,
                    payload: check.status
                });

                browserHistory.push( '/earnings' );
            } else {
                yield call( delay, 2000 );
                yield call( checkValidation, { payload });
            }
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Fetches preview for uploaded earnings
 */
export function* getEarningsPreview({ payload }) {
    try {
        yield put({
            type: SET_EARNINGS_PREVIEW_DATA,
            payload: []
        });

        yield put({
            type: SET_EARNINGS_PREVIEW_STATUS,
            payload: 'busy'
        });

        const preview = yield call(
            Fetch,
            `/earning/upload/preview?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}`,
            { method: 'GET' }
        );

        yield put({
            type: SET_EARNINGS_PREVIEW_DATA,
            payload: preview
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EARNINGS_PREVIEW_STATUS,
            payload: 'ready'
        });
    }
}

/**
 * Saves validated earnings
 */
export function* saveEarnings({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const upload = yield call( Fetch, '/earning/upload/save', {
            method: 'POST',
            data: {
                company_id: companyId,
                job_id: payload.jobId
            }
        });

        if ( upload.id ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: 'save_queued'
            });

            yield call( checkValidation, { payload: {
                step: 'save',
                jobId: payload.jobId
            }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage()
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Watch for UPLOAD_EARNINGS
 */
export function* watchForUploadEarnings() {
    const watcher = yield takeEvery( UPLOAD_EARNINGS, uploadEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_EARNINGS_PREVIEW
 */
export function* watchForGetEarningsPreview() {
    const watcher = yield takeEvery( GET_EARNINGS_PREVIEW, getEarningsPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SAVE_EARNINGS
 */
export function* watchForSaveEarnings() {
    const watcher = yield takeEvery( SAVE_EARNINGS, saveEarnings );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetEarningsPreview,
    watchForUploadEarnings,
    watchForSaveEarnings,
    watchNotify
];
