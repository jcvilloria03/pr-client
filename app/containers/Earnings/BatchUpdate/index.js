import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import SubHeader from '../../SubHeader';
import SnackBar from '../../../components/SnackBar';
import A from '../../../components/A';
import Table from '../../../components/Table';
import Clipboard from '../../../components/Clipboard';
import { H4, P } from '../../../components/Typography';
import FileInput from '../../../components/FileInput';
import Button from '../../../components/Button';

import { browserHistory } from '../../../utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { getLabelForWithholdingTaxType } from '../utils';
import { formatPaginationLabel } from '../../../utils/functions';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { BASE_PATH_NAME } from '../../../constants';

import * as batchUpdateActions from './actions';
import {
    PageWrapper,
    StyledLoader,
    Footer
} from './styles';
import {
    makeSelectNotification,
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectEarningsPreview,
    makeSelectSaving
} from './selectors';

/**
 * EarningsBatchUpdate Container
 */
export class EarningsBatchUpdate extends React.Component {
    static propTypes = {
        batchUploadJobId: React.PropTypes.string,
        uploadEarnings: React.PropTypes.func,
        batchUploadStatus: React.PropTypes.string,
        batchUploadErrors: React.PropTypes.oneOfType([
            React.PropTypes.object,
            React.PropTypes.array
        ]),
        earningsPreview: React.PropTypes.shape({
            status: React.PropTypes.string,
            data: React.PropTypes.array
        }),
        saving: React.PropTypes.shape({
            status: React.PropTypes.string,
            errors: React.PropTypes.object
        }),
        saveEarnings: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    }

    /**
     * component's constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            files: [],
            status: null,
            errors: {},
            clipboard: '',
            submit: false,
            annualEarningsTableLabel: 'Showing 0-0 of 0 entries',
            errorsTableLabel: 'Showing 0-0 of 0 entries',
            earningsPreview: {
                data: [],
                status: 'ready'
            },
            savingStatus: '',
            savingErrors: {}
        };

        const { last_active_company_id: companyId } = JSON.parse( localStorage.getItem( 'user' ) );
        this.company_id = companyId;
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.batchUploadStatus !== this.props.batchUploadStatus && this.setState({
            status: nextProps.batchUploadStatus
        }, () => {
            this.validateButton.setState({
                disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.batchUploadStatus )
            });
        });

        nextProps.batchUploadErrors !== this.props.batchUploadErrors && this.setState({
            errors: nextProps.batchUploadErrors
        }, () => {
            let clipboard = '';
            const rows = Object.keys( nextProps.batchUploadErrors );

            if ( rows.length > 0 ) {
                rows.forEach( ( row ) => {
                    Object.values( nextProps.batchUploadErrors[ row ]).forEach( ( value ) => {
                        clipboard += `Row ${row}, ${value}\n`;
                    });
                });
            }

            this.handleErrorsTableChanges();
            this.setState({ clipboard });
        });

        nextProps.earningsPreview.data !== this.props.earningsPreview.data && this.setState({
            earningsPreview: { ...this.state.earningsPreview, data: nextProps.earningsPreview.data }
        }, () => {
            this.handleEarningsTableChanges();
        });

        nextProps.earningsPreview.status !== this.props.earningsPreview.status && this.setState({
            earningsPreview: { ...this.state.earningsPreview, status: nextProps.earningsPreview.status }
        });

        nextProps.saving.status !== this.props.saving.status && this.setState({
            savingStatus: nextProps.saving.status
        }, () => {
            this.submitButton.setState({ disabled: nextProps.saving.status !== 'save_failed' });
        });
    }

    setSubmitButtonStatus( file, uploadStatus ) {
        return file ? ( file.length <= 0 || !!uploadStatus.length ) : true;
    }

    handleErrorsTableChanges = () => {
        if ( !this.errorsTable ) {
            return;
        }

        this.setState({
            errorsTableLabel: formatPaginationLabel( this.errorsTable.tableComponent.state )
        });
    };

    handleEarningsTableChanges = () => {
        if ( !this.earningsTable ) {
            return;
        }

        this.setState({
            annualEarningsTableLabel: formatPaginationLabel( this.earningsTable.tableComponent.state )
        });
    };

    renderEarningsSection = () => {
        const tableColumns = [
            {
                id: 'id',
                show: false
            },
            {
                id: 'employee_id',
                header: 'Employee ID',
                sortable: true,
                minWidth: 180,
                accessor: ( row ) => row.employee_id
            },
            {
                id: 'withholding_tax_type',
                header: 'Withholding Tax Type',
                sortable: true,
                minWidth: 220,
                accessor: ( row ) => (
                    getLabelForWithholdingTaxType( row.with_holding_tax_type )
                )
            },
            {
                id: 'expanded_with_holding_tax_rate',
                header: 'Withholding Tax Rate',
                sortable: true,
                minWidth: 220,
                accessor: ( row ) => (
                    row.expanded_with_holding_tax_rate || ''
                )
            },
            {
                id: 'attendance_data_required',
                header: 'Attendance Data Required',
                sortable: true,
                minWidth: 130,
                accessor: ( row ) => (
                    row.attendance_data_required
                )
            },
            {
                id: 'entitled_unworked_regular_holiday_pay',
                header: 'Entitled Unworked Regular Holiday Pay',
                sortable: true,
                minWidth: 140,
                accessor: ( row ) => (
                    row.entitled_unworked_regular_holiday_pay
                )
            },
            {
                id: 'entitled_unworked_special_holiday_pay',
                header: 'Entitled Unworked Special Holiday Pay',
                sortable: true,
                minWidth: 140,
                accessor: ( row ) => (
                    row.entitled_unworked_special_holiday_pay
                )
            },
            {
                id: 'entitled_holiday_premium_pay',
                header: 'Entitled Holiday Premium Pay',
                sortable: true,
                minWidth: 140,
                accessor: ( row ) => (
                    row.entitled_holiday_premium_pay
                )
            },
            {
                id: 'entitled_rest_day_pay',
                header: 'Entitled Rest Day Pay',
                sortable: true,
                minWidth: 120,
                accessor: ( row ) => (
                    row.entitled_rest_day_pay
                )
            },
            {
                id: 'entitled_overtime',
                header: 'Entitled Overtime',
                sortable: true,
                minWidth: 120,
                accessor: ( row ) => (
                    row.entitled_overtime
                )
            },
            {
                id: 'entitled_night_differential',
                header: 'Entitled Night Differential',
                sortable: true,
                minWidth: 120,
                accessor: ( row ) => (
                    row.entitled_night_differential
                )
            }
        ];

        const dataForDisplay = this.state.earningsPreview.data;

        return this.state.earningsPreview.data.length === 0
            ? null
            : (
                <div>
                    <H4>Earnings List:</H4>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.earningsTableLabel }
                        </span>
                    </div>
                    <Table
                        columns={ tableColumns }
                        data={ dataForDisplay }
                        pagination
                        ref={ ( ref ) => { this.earningsTable = ref; } }
                        onDataChange={ this.handleEarningsTableChanges }
                    />
                </div>
            );
    };

    renderErrorsSection = () => {
        const errorDisplay = [];
        const errorList = this.state.errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'Row Number',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'Error Type',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            const keys = Object.keys( errorList );

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row {key}</H4>,
                    error: <ul>{Object.values( errorList[ key ]).map( ( value ) => <li key={ value } style={ { margin: '0' } } >{value}</li> )}</ul>
                });
            });

            return (
                <div className="errors">
                    <H4>Data field errors</H4>
                    <p>There seems to be an error in the file you uploaded. Review the list below, correct the errors and upload the file again.</p>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.errorsTableLabel }
                        </span>
                        <Clipboard value={ this.state.clipboard } />
                    </div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                        ref={ ( ref ) => { this.errorsTable = ref; } }
                        onDataChange={ this.handleErrorsTableChanges }
                    />
                </div>
            );
        }

        return null;
    }

    /**
     * renders component to DOM
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Batch Upload"
                    meta={ [
                        { name: 'description', content: 'Earnings Batch Upload' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => { e.preventDefault(); browserHistory.push( '/payroll/earnings', true ); } }
                            >
                                &#8592; Back to Earnings
                            </A>
                        </Container>
                    </div>
                    <div className="heading">
                        <h3>Batch Update</h3>
                        <p>Make multiple changes on your existing employee records. Download our batch update templates, fill them out, and upload them once you are done.</p>
                    </div>

                    <div className="earnings-form">
                        <div className="steps">
                            <div className="step">
                                <div className="template">
                                    <H4>Step 1:</H4>
                                    <P>Download and fill-out the Employee Payroll Information Template.</P>
                                    <P><A target="_blank" href={ `${BASE_PATH_NAME}/guides/earnings/batch-update` }>Click here to view the upload guide.</A></P>
                                    <A className="sl-c-btn--wide" href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/guides/earnings.csv" download>Download Template</A>
                                </div>
                            </div>
                            <div className="step">
                                <div className="upload">
                                    <H4>Step 2:</H4>
                                    <P>After completely filling out the template, choose and upload it here.</P>
                                    <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'none' : 'block' } } >
                                        <FileInput
                                            accept=".csv"
                                            onDrop={ ( files ) => {
                                                const { acceptedFiles } = files;

                                                this.setState({
                                                    files: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null,
                                                    errors: {},
                                                    status: null
                                                }, () => {
                                                    this.validateButton.setState({ disabled: acceptedFiles.length <= 0 });
                                                });
                                            } }
                                            ref={ ( ref ) => { this.fileInput = ref; } }
                                        />
                                    </div>
                                    <div style={ { display: this.state.status === 'validated' ? 'block' : 'none' } }>
                                        <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                                    </div>
                                    <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'validated' ? 'none' : 'block' } } >
                                        <Button
                                            label={
                                            this.state.submit ? (
                                                <StyledLoader className="animation">
                                                    Uploading <div className="anim3"></div>
                                                </StyledLoader>
                                            ) : (
                                                'Upload'
                                            )
                                        }
                                            disabled={ this.state.files ? this.state.files.length <= 0 : true }
                                            type="neutral"
                                            ref={ ( ref ) => { this.validateButton = ref; } }
                                            onClick={ () => {
                                                this.setState({
                                                    errors: {},
                                                    status: null,
                                                    submit: true
                                                }, () => {
                                                    this.validateButton.setState({ disabled: true }, () => {
                                                        this.props.uploadEarnings({ file: this.state.files });

                                                        setTimeout( () => {
                                                            this.validateButton.setState({ disabled: false });
                                                            this.setState({ submit: false });
                                                        }, 5000 );
                                                    });
                                                });
                                            } }
                                        />
                                    </div>
                                    <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'block' : 'none' } } >
                                        <StyledLoader className="animation">
                                            <H4 style={ { margin: '0' } }>{ this.state.status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                        </StyledLoader>
                                    </div>
                                </div>
                            </div>
                        </div>
                        { this.renderEarningsSection() }
                        { this.renderErrorsSection() }
                    </div>
                </PageWrapper>
                <Footer>
                    <Button
                        label="Cancel"
                        type="neutral"
                        size="large"
                        onClick={ () => {
                            browserHistory.push( '/earnings' );
                        } }
                    />
                    <Button
                        ref={ ( ref ) => { this.submitButton = ref; } }
                        type="action"
                        disabled={ this.state.status !== 'validated' }
                        label={
                            this.state.savingStatus === 'save_queued' || this.state.savingStatus === 'saving' ?
                                (
                                    <StyledLoader className="animation">
                                        <span>Saving</span> <div className="anim3"></div>
                                    </StyledLoader>
                                ) : 'Submit'
                        }
                        size="large"
                        onClick={ () => {
                            this.submitButton.setState({ disabled: true });
                            this.setState({ savingStatus: 'save_queued' });
                            this.props.saveEarnings({ jobId: this.props.batchUploadJobId });
                        } }
                    />
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    batchUploadJobId: makeSelectBatchUploadJobId(),
    batchUploadStatus: makeSelectBatchUploadStatus(),
    batchUploadErrors: makeSelectBatchUploadErrors(),
    earningsPreview: makeSelectEarningsPreview(),
    saving: makeSelectSaving()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        batchUpdateActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EarningsBatchUpdate );
