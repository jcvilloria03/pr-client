import { fromJS } from 'immutable';
import {
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    RESET_EARNINGS_PREVIEW,
    SET_EARNINGS_PREVIEW_DATA,
    SET_EARNINGS_PREVIEW_STATUS,
    SET_SAVING_STATUS,
    SET_SAVING_ERRORS,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    errors: {},
    batchUploadJobId: null,
    batchUploadStatus: '',
    batchUploadErrors: {},
    earningsPreview: {
        status: 'ready',
        data: []
    },
    saving: {
        status: '',
        errors: {}
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Batch update earnings reducer
 *
 */
function manualEntryAllowanceReducer( state = initialState, action ) {
    switch ( action.type ) {
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_BATCH_UPLOAD_JOB_ID:
            return state.set( 'batchUploadJobId', action.payload );
        case SET_BATCH_UPLOAD_STATUS:
            return state.set( 'batchUploadStatus', action.payload );
        case SET_BATCH_UPLOAD_ERRORS:
            return state.set( 'batchUploadErrors', action.payload );
        case SET_EARNINGS_PREVIEW_STATUS:
            return state.setIn([ 'earningsPreview', 'status' ], action.payload );
        case SET_EARNINGS_PREVIEW_DATA:
            return state.setIn([ 'earningsPreview', 'data' ], fromJS( action.payload ) );
        case SET_SAVING_STATUS:
            return state.setIn([ 'saving', 'status' ], action.payload );
        case SET_SAVING_ERRORS:
            return state.setIn([ 'saving', 'errors' ], action.payload );
        case RESET_EARNINGS_PREVIEW:
            return state.set( 'earnings', fromJS({
                status: 'ready',
                data: []
            }) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default manualEntryAllowanceReducer;
