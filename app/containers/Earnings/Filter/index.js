import React from 'react';

import Button from '../../../components/Button';
import MultiSelect from '../../../components/MultiSelect';

import { FilterWrapper } from './styles';

import { FILTER_TYPES } from './constants';
import { WITHHOLDING_TAX_TYPE } from '../constants';
import { getLabelForWithholdingTaxType } from '../utils';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {
    static propTypes = {
        filterData: React.PropTypes.shape({
            locations: React.PropTypes.array,
            departments: React.PropTypes.array,
            positions: React.PropTypes.array
        }),
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    onApply = () => {
        const filters = [];

        this.withholding_tax_type.state.value && this.withholding_tax_type.state.value.forEach( ( taxType ) => {
            filters.push({ ...taxType, type: FILTER_TYPES.WITHHOLDING_TAX_TYPE });
        });

        this.location.state.value && this.location.state.value.forEach( ( location ) => {
            filters.push({ ...location, type: FILTER_TYPES.LOCATION });
        });

        this.department.state.value && this.department.state.value.forEach( ( department ) => {
            filters.push({ ...department, type: FILTER_TYPES.DEPARTMENT });
        });

        this.position.state.value && this.position.state.value.forEach( ( position ) => {
            filters.push({ ...position, type: FILTER_TYPES.POSITION });
        });

        this.props.onApply( filters );
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    getWithholdingTaxTypes = () => (
        Object.keys( WITHHOLDING_TAX_TYPE ).map( ( key ) => ({
            value: WITHHOLDING_TAX_TYPE[ key ],
            label: getLabelForWithholdingTaxType( WITHHOLDING_TAX_TYPE[ key ]),
            disabled: false
        }) )
    )

    getLocations = () => {
        if ( !this.props.filterData.locations ) {
            return [];
        }

        const locations = this.props.filterData.locations.map( ( location ) => ({
            value: location.id,
            label: location.name,
            disabled: false
        }) );

        return locations;
    }

    getDepartments = () => {
        if ( !this.props.filterData.departments ) {
            return [];
        }

        const departments = this.props.filterData.departments.map( ( department ) => ({
            value: department.id,
            label: department.name,
            disabled: false
        }) );

        return departments;
    }

    getPositions = () => {
        if ( !this.props.filterData.positions ) {
            return [];
        }

        const positions = this.props.filterData.positions.map( ( position ) => ({
            value: position.id,
            label: position.name,
            disabled: false
        }) );

        return positions;
    }

    resetFilters = () => {
        this.withholding_tax_type.setState({ value: null }, () => {
            this.onApply();
        });

        this.location.setState({ value: null }, () => {
            this.onApply();
        });

        this.department.setState({ value: null }, () => {
            this.onApply();
        });

        this.position.setState({ value: null }, () => {
            this.onApply();
        });
    }

    render() {
        return (
            <FilterWrapper>
                <div className="row" key="1">
                    <div className="col-xs-3">
                        <MultiSelect
                            id="withholding_tax_type"
                            label="Withholding Tax Type"
                            data={ this.getWithholdingTaxTypes() }
                            placeholder="All withholding tax types"
                            ref={ ( ref ) => { this.withholding_tax_type = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            id="location"
                            label="Location"
                            key="location"
                            data={ this.getLocations() }
                            placeholder="All locations"
                            ref={ ( ref ) => { this.location = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            id="department"
                            label="Department"
                            data={ this.getDepartments() }
                            placeholder="All departments"
                            ref={ ( ref ) => { this.department = ref; } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            id="position"
                            label="Position"
                            placeholder="All positions"
                            data={ this.getPositions() }
                            ref={ ( ref ) => { this.position = ref; } }
                        />
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
