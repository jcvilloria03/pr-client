export const FILTER_TYPES = {
    WITHHOLDING_TAX_TYPE: 'withholding_tax_type',
    LOCATION: 'location',
    DEPARTMENT: 'department',
    POSITION: 'position'
};
