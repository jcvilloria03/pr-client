import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import SubHeader from '../../SubHeader';

import A from '../../../components/A';
import Button from '../../../components/Button';
import SnackBar from '../../../components/SnackBar';
import { H2, H3, P } from '../../../components/Typography';

import { WITHHOLDING_TAX_TYPE } from '../constants';
import { getLabelForWithholdingTaxType } from '../utils';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { getEmployeeFullName } from '../../../utils/functions';
import { subscriptionService } from '../../../utils/SubscriptionService';

import {
    NavWrapper,
    ValueWrapper,
    MainWrapper,
    LoadingStyles
} from './styles';

import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectEarning
} from './selectors';

import * as actions from './actions';

/**
 * Earnings Detail
 */
class EarningsDetail extends React.Component {
    static propTypes = {
        loading: React.PropTypes.bool,
        earning: React.PropTypes.object,
        params: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    }

    constructor( props ) {
        super( props );

        this.state = {
            permission: {
                view: false,
                edit: false
            }
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.earnings',
            'edit.earnings'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.earnings' ];

            if ( authorized ) {
                this.setState({ permission: Object.assign( this.state.permission, {
                    view: authorization[ 'view.earnings' ],
                    edit: authorization[ 'edit.earnings' ]
                }) });
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });

        this.props.initializeData({ earningId: this.props.params.earningId });
    }

    render() {
        return (
            <div>
                <Helmet
                    title="Earning Details"
                    meta={ [
                        { name: 'description', content: 'Description of Earning Details' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/earnings' );
                            } }
                        >
                            &#8592; Back to Earnings
                        </A>
                    </Container>
                </NavWrapper>
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <MainWrapper>
                    { this.props.loading
                        ? (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading Earning.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        )
                        : (
                            <div>
                                <Container>
                                    <div className="content">
                                        <div className="heading">
                                            <H3>Earning Details</H3>
                                            <P>You may view the employee&#39;s earning details through this page.</P>
                                        </div>
                                        <div>
                                            <div className="row">
                                                <div className="col-xs-12">
                                                    <h5>
                                                        <b>{ getEmployeeFullName( this.props.earning.employee )}</b>
                                                    </h5>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xs-12">
                                                    <strong>Earning Info</strong>
                                                </div>
                                            </div>
                                            <div className="row" style={ { marginTop: '20px' } }>
                                                <div className="col-xs-3">
                                                    <div>Basic Pay:</div>
                                                    <ValueWrapper>
                                                        { `Php ${this.props.earning.active_basic_pay ? this.props.earning.active_basic_pay.basic_pay : '0.00'}` }
                                                        <div style={ { marginTop: '10px' } }>
                                                            <A
                                                                href
                                                                onClick={ ( e ) => {
                                                                    e.preventDefault();
                                                                    browserHistory.push( `/employee/${this.props.earning.employee.id}`, true );
                                                                } }
                                                            >
                                                                Basic Pay History
                                                            </A>
                                                        </div>
                                                    </ValueWrapper>
                                                </div>
                                                <div className="col-xs-3">
                                                    <div>Withholding Tax Type:</div>
                                                    <ValueWrapper>
                                                        { getLabelForWithholdingTaxType( this.props.earning.with_holding_tax_type ) }
                                                    </ValueWrapper>
                                                </div>
                                                { this.props.earning.with_holding_tax_type === WITHHOLDING_TAX_TYPE.EXPANDED
                                                    ? (
                                                        <div className="col-xs-3">
                                                            <div>Expanded Withholding Tax Rate:</div>
                                                            <ValueWrapper>
                                                                { `${this.props.earning.expanded_with_holding_tax_rate}%` }
                                                            </ValueWrapper>
                                                        </div>
                                                    )
                                                    : null
                                                }
                                            </div>
                                            <div className="row">
                                                <div className="col-xs-12">
                                                    <strong>Payroll Rules</strong>
                                                </div>
                                            </div>
                                            <div className="row" style={ { marginTop: '20px' } }>
                                                <div className="col-xs-3">
                                                    <div>Attendance Data Required:</div>
                                                    <ValueWrapper>
                                                        { this.props.earning.attendance_data_required ? 'Yes' : 'No' }
                                                    </ValueWrapper>
                                                </div>
                                                <div className="col-xs-4">
                                                    <div>Entitled Unworked Regular Holiday Pay:</div>
                                                    <ValueWrapper>
                                                        {
                                                            this.props.earning.entitled_unworked_regular_holiday_pay !== null
                                                            ? this.props.earning.entitled_unworked_regular_holiday_pay ? 'Yes' : 'No'
                                                            : 'N/A'
                                                        }
                                                    </ValueWrapper>
                                                </div>
                                                <div className="col-xs-3">
                                                    <div>Entitled Overtime Pay:</div>
                                                    <ValueWrapper>
                                                        { this.props.earning.entitled_overtime ? 'Yes' : 'No' }
                                                    </ValueWrapper>
                                                </div>
                                                <div className="col-xs-2">
                                                    <div>Entitled Rest Day Pay:</div>
                                                    <ValueWrapper>
                                                        { this.props.earning.entitled_rest_day_pay ? 'Yes' : 'No' }
                                                    </ValueWrapper>
                                                </div>
                                            </div>
                                            <div className="row" style={ { marginTop: '20px' } }>
                                                <div className="col-xs-3">
                                                    <div>Entitled Night Differential:</div>
                                                    <ValueWrapper>
                                                        { this.props.earning.entitled_night_differential ? 'Yes' : 'No' }
                                                    </ValueWrapper>
                                                </div>
                                                <div className="col-xs-4">
                                                    <div>Entitled Unworked Special Holiday Pay:</div>
                                                    <ValueWrapper>
                                                        {
                                                            this.props.earning.entitled_unworked_special_holiday_pay !== null
                                                                ? this.props.earning.entitled_unworked_special_holiday_pay ? 'Yes' : 'No'
                                                                : 'N/A'
                                                        }
                                                    </ValueWrapper>
                                                </div>
                                                <div className="col-xs-3">
                                                    <div>Entitled Holiday Premium Pay:</div>
                                                    <ValueWrapper>
                                                        { this.props.earning.entitled_holiday_premium_pay ? 'Yes' : 'No' }
                                                    </ValueWrapper>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Container>
                                <div className="foot">
                                    <Button
                                        label="Cancel"
                                        type="neutral"
                                        size="large"
                                        onClick={ () => {
                                            browserHistory.push( '/earnings' );
                                        } }
                                    />
                                    <Button
                                        label="Edit"
                                        type="action"
                                        size="large"
                                        onClick={ () => {
                                            browserHistory.push( `/earnings/${this.props.earning.id}/edit` );
                                        } }
                                    />
                                </div>
                            </div>
                        ) }
                </MainWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    earning: makeSelectEarning(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators( actions, dispatch )
);

export default connect( mapStateToProps, mapDispatchToProps )( EarningsDetail );
