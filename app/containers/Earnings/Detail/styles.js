import styled from 'styled-components';

export const MainWrapper = styled.div`
    padding-top: 76px;
    background: #fff;

    .nav {
        padding: 10px 20px;
        background: #f0f4f6;
        margin-bottom: 50px;
    }

    .loader {
        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .date {
        & > div > span {
            display: block;
        }
        .DayPickerInput {
            display: block;

            input {
                width: 100%;
            }
        }
    }

    .heading {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 0 auto 30px auto;

        h3 {
            font-weight: 600;
        }

        p {
            text-align: center;
            max-width: 800px;
        }
    }

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        margin-top: 37px;
        position: absolute;
        width: 100%;

        button {
            min-width: 120px;
        }
    }

    .hide {
        display: none;
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
    z-index: 10;
`;

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

export const MessageWrapperStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding-top: 90px;
`;

export const AlignRight = styled.div`
    text-align: right;

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        margin-top: 20px;

        button {
            min-width: 120px;
        }
    }
`;

export const ValueWrapper = styled.div`
    padding-top: 15px;
    padding-left: 12.5px;
    padding-bottom: 30px;

    .Select-control {
        border: none;
        color: #373a3c !important;
        background: #ffffff !important;
        cursor: default !important;
        vertical-align: top;
    }

    .Select.is-disabled {
        .Select-value-label {
            color: #373a3c !important;
        }
    }

    .Select-arrow-zone {
        display: none;
    }
`;
