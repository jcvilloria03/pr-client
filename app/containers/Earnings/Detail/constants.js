export const INITIALIZE_DATA = 'app/Earnings/Detail/INITIALIZE_DATA';
export const NOTIFY_USER = 'app/Earnings/Detail/NOTIFY_USER';

export const SET_LOADING = 'app/Earnings/Detail/SET_LOADING';
export const SET_EARNING = 'app/Earnings/Detail/SET_EARNING';
export const SET_NOTIFICATION = 'app/Earnings/Detail/SET_NOTIFICATION';
