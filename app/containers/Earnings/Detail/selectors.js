import { createSelector } from 'reselect';

/**
 * Direct selector to the earnings detail state domain
 */
const makeSelectEarningsDetailDomain = () => (
    ( state ) => ( state.get( 'earningsDetail' ) )
);

const makeSelectLoading = () => createSelector(
    makeSelectEarningsDetailDomain(),
    ( state ) => state.get( 'loading' )
);

const makeSelectEarning = () => createSelector(
    makeSelectEarningsDetailDomain(),
    ( state ) => state.get( 'earning' ).toJS()
);

const makeSelectNotification = () => createSelector(
    makeSelectEarningsDetailDomain(),
    ( state ) => state.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectEarning,
    makeSelectNotification
};
