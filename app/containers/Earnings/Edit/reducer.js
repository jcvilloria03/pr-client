import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_SUBMITTED,
    SET_EARNING,
    SET_NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    submitted: false,
    earning: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_EARNING:
            return state.set( 'earning', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
};
