import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import decimal from 'js-big-decimal';

import SubHeader from '../../SubHeader';

import A from '../../../components/A';
import Input from '../../../components/Input';
import Loader from '../../../components/Loader';
import Button from '../../../components/Button';
import Switch from '../../../components/Switch';
import SalSelect from '../../../components/Select';
import SnackBar from '../../../components/SnackBar';
import { H2, H3, P } from '../../../components/Typography';

import { WITHHOLDING_TAX_TYPE } from '../constants';
import { getLabelForWithholdingTaxType } from '../utils';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { stripNonDigit, formatCurrency, getEmployeeFullName } from '../../../utils/functions';
import { subscriptionService } from '../../../utils/SubscriptionService';

import {
    NavWrapper,
    ValueWrapper,
    MainWrapper,
    LoadingStyles
} from './styles';

import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectEarning,
    makeSelectSubmitted
} from './selectors';

import * as actions from './actions';

const BASE_PAY_UNIT = [
    { value: 'per_hour', label: 'Per Hour' },
    { value: 'per_day', label: 'Per Day' },
    { value: 'per_month', label: 'Per Month' },
    { value: 'per_year', label: 'Per Year' }
];

const getDataForWithholdingTaxTypeSelect = () => (
    Object.keys( WITHHOLDING_TAX_TYPE ).map( ( key ) => ({
        value: WITHHOLDING_TAX_TYPE[ key ],
        label: getLabelForWithholdingTaxType( WITHHOLDING_TAX_TYPE[ key ])
    }) )
);

/**
 * Edit Earnings
 */
class EditEarnings extends React.Component {
    static propTypes = {
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        earning: React.PropTypes.object,
        params: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        submitForm: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    }

    constructor( props ) {
        super( props );

        this.state = {
            didSetInitialEarningState: false,
            earning: {
                employee_id: null,
                amount: null,
                unit: null,
                with_holding_tax_type: null,
                expanded_with_holding_tax_rate: null,
                attendance_data_required: null,
                entitled_unworked_regular_holiday_pay: null,
                entitled_unworked_special_holiday_pay: null,
                entitled_holiday_premium_pay: null,
                entitled_rest_day_pay: null,
                entitled_overtime: null,
                entitled_night_differential: null
            },
            permission: {
                view: false,
                edit: false
            }
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.earnings',
            'edit.earnings'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.earnings' ];

            if ( authorized ) {
                this.setState({ permission: Object.assign( this.state.permission, {
                    view: authorization[ 'view.earnings' ],
                    edit: authorization[ 'edit.earnings' ]
                }) });
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });

        this.props.initializeData({ earningId: this.props.params.earningId });
    }

    componentWillReceiveProps( nextProps ) {
        if ( Object.keys( nextProps.earning ).length !== 0 && !this.state.didSetInitialEarningState ) {
            this.setInitialEarningStateFromProps( nextProps.earning );
        }
    }

    setInitialEarningStateFromProps = ( props ) => {
        const initialEarningState = {};

        Object.keys( this.state.earning ).forEach( ( key ) => {
            if ( key === 'employee_id' ) {
                initialEarningState.employee_id = props.employee.id;
            } else if ( key === 'amount' ) {
                initialEarningState.amount = props.active_basic_pay.amount;
            } else if ( key === 'unit' ) {
                initialEarningState.unit = props.active_basic_pay.unit;
            } else {
                initialEarningState[ key ] = props[ key ];
            }
        });

        this.setState({
            earning: initialEarningState,
            didSetInitialEarningState: true
        });
    }

    updateEarning = ( field, value ) => {
        this.setState({
            earning: {
                ...this.state.earning,
                [ field ]: value
            }
        });
    }

    validateForm = () => {
        let valid = true;

        if ( this.amount._validate( stripNonDigit( this.amount.state.value ) ) ) {
            valid = false;
        }

        if ( !this.unit._checkRequire( this.unit.state.value ) ) {
            valid = false;
        }

        if ( !this.with_holding_tax_type._checkRequire( this.with_holding_tax_type.state.value ) ) {
            valid = false;
        }

        if ( this.with_holding_tax_type.state.value === WITHHOLDING_TAX_TYPE.EXPANDED ) {
            if ( this.expanded_with_holding_tax_rate._validate( stripNonDigit( this.expanded_with_holding_tax_rate.state.value ) ) ) {
                valid = false;
            }
        }

        return valid;
    }

    checkExpandedWithHoldingTaxRate = ( onSetStateFinish ) => {
        if ( this.state.earning.with_holding_tax_type !== WITHHOLDING_TAX_TYPE.EXPANDED ) {
            this.setState({
                earning: {
                    ...this.state.earning,
                    expanded_with_holding_tax_rate: null
                }
            }, onSetStateFinish );
        } else {
            onSetStateFinish();
        }
    }

    submitForm = () => {
        if ( this.validateForm() ) {
            this.checkExpandedWithHoldingTaxRate( () => {
                this.props.submitForm({
                    ...this.state.earning,
                    amount: decimal.round( `${this.state.earning.amount}`.replace( /[,\s]/g, '' ), 2 ),
                    expanded_with_holding_tax_rate:
                        this.state.earning.expanded_with_holding_tax_rate
                            ? decimal.round( `${this.state.earning.expanded_with_holding_tax_rate}`.replace( /[,\s]/g, '' ), 2 )
                            : null,
                    earningId: this.props.earning.id,
                    previousPath: this.props.previousRoute.path
                });
            });
        }
    }

    render() {
        return (
            <div>
                <Helmet
                    title="Edit Earnings"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Earnings' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/earnings' );
                            } }
                        >
                            &#8592; Back to Earnings
                        </A>
                    </Container>
                </NavWrapper>
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <MainWrapper>
                    { this.props.loading
                        ? (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading Earning.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        )
                        : (
                            <div>
                                <Container>
                                    <div className="content">
                                        <div className="heading">
                                            <H3>Edit Earnings</H3>
                                            <P>You may edit the employee&#39;s basic pay and withholding tax type through this page. You may also tick on and off certain payroll rules through this page.</P>
                                        </div>
                                        <div>
                                            <div className="row">
                                                <div className="col-xs-12">
                                                    <h5>
                                                        <b>{ getEmployeeFullName( this.props.earning.employee )}</b>
                                                    </h5>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xs-12">
                                                    <strong>Earning Info</strong>
                                                </div>
                                            </div>
                                            <div className="row" style={ { marginTop: '20px' } }>
                                                <div className="col-xs-3">
                                                    <div>Basic Pay Amount:</div>
                                                    <div style={ { paddingLeft: '12.5px', paddingTop: '15px' } }>
                                                        <Input
                                                            id="amount"
                                                            placeholder="Enter basic pay amount"
                                                            required
                                                            value={ this.state.earning.amount }
                                                            minNumber={ 1 }
                                                            ref={ ( ref ) => { this.amount = ref; } }
                                                            onChange={ ( value ) => { this.amount.setState({ value: stripNonDigit( value ) }); } }
                                                            onFocus={ () => { this.amount.setState({ value: stripNonDigit( this.amount.state.value ) }); } }
                                                            onBlur={ ( value ) => {
                                                                this.amount.setState({
                                                                    value: formatCurrency( value )
                                                                }, () => {
                                                                    this.updateEarning( 'amount', formatCurrency( value ) );
                                                                });
                                                            } }
                                                        />
                                                        <div style={ { marginTop: '10px' } }>
                                                            <A
                                                                href
                                                                onClick={ ( e ) => {
                                                                    e.preventDefault();
                                                                    browserHistory.push( `/employee/${this.props.earning.employee.id}`, true );
                                                                } }
                                                            >
                                                                Basic Pay History
                                                            </A>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-3">
                                                    <div>Basic Pay Unit:</div>
                                                    <div style={ { paddingLeft: '12.5px', paddingTop: '15px' } }>
                                                        <SalSelect
                                                            id="unit"
                                                            required
                                                            key="unit"
                                                            data={ BASE_PAY_UNIT }
                                                            value={ this.state.earning.unit }
                                                            placeholder="Select basic pay unit"
                                                            ref={ ( ref ) => { this.unit = ref; } }
                                                            onChange={ ({ value }) => {
                                                                this.updateEarning( 'unit', value );
                                                            } }
                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-xs-3">
                                                    <div>Withholding Tax Type:</div>
                                                    <div style={ { paddingLeft: '12.5px', paddingTop: '15px' } }>
                                                        <SalSelect
                                                            id="with_holding_tax_type"
                                                            required
                                                            key="with_holding_tax_type"
                                                            data={ getDataForWithholdingTaxTypeSelect() }
                                                            value={ this.state.earning.with_holding_tax_type }
                                                            placeholder="Select withholding tax type"
                                                            ref={ ( ref ) => { this.with_holding_tax_type = ref; } }
                                                            onChange={ ({ value }) => {
                                                                this.updateEarning( 'with_holding_tax_type', value );
                                                            } }
                                                        />
                                                    </div>
                                                </div>
                                                { this.state.earning.with_holding_tax_type === WITHHOLDING_TAX_TYPE.EXPANDED
                                                    ? (
                                                        <div className="col-xs-3">
                                                            <div>Expanded Withholding Tax Rate:</div>
                                                            <div style={ { paddingLeft: '12.5px', paddingTop: '15px' } }>
                                                                <Input
                                                                    id="expanded_with_holding_tax_rate"
                                                                    placeholder="Enter expanded withholding tax rate"
                                                                    required
                                                                    value={ this.state.earning.expanded_with_holding_tax_rate }
                                                                    minNumber={ 0 }
                                                                    ref={ ( ref ) => { this.expanded_with_holding_tax_rate = ref; } }
                                                                    onChange={ ( value ) => { this.expanded_with_holding_tax_rate.setState({ value: stripNonDigit( value ) }); } }
                                                                    onFocus={ () => { this.expanded_with_holding_tax_rate.setState({ value: stripNonDigit( this.expanded_with_holding_tax_rate.state.value ) }); } }
                                                                    onBlur={ ( value ) => {
                                                                        this.expanded_with_holding_tax_rate.setState({
                                                                            value: formatCurrency( value )
                                                                        }, () => {
                                                                            this.updateEarning( 'expanded_with_holding_tax_rate', formatCurrency( value ) );
                                                                        });
                                                                    } }
                                                                />
                                                            </div>
                                                        </div>
                                                    )
                                                    : null
                                                }
                                            </div>
                                            <div className="row">
                                                <div className="col-xs-12">
                                                    <strong>Payroll Rules</strong>
                                                </div>
                                            </div>
                                            <div className="row" style={ { marginTop: '20px' } }>
                                                <div className="col-xs-3">
                                                    <div>Attendance Data Required:</div>
                                                    <ValueWrapper>
                                                        <Switch
                                                            checked={ this.state.earning.attendance_data_required }
                                                            onChange={ ( value ) => {
                                                                this.setState({
                                                                    earning: {
                                                                        ...this.state.earning,
                                                                        attendance_data_required: value,
                                                                        entitled_unworked_regular_holiday_pay: value ? false : null,
                                                                        entitled_unworked_special_holiday_pay: value ? false : null
                                                                    }
                                                                });
                                                            } }
                                                        />
                                                    </ValueWrapper>
                                                </div>
                                                <div className={ this.state.earning.attendance_data_required ? 'col-xs-4 ' : 'hidden' }>
                                                    <div>Entitled Unworked Regular Holiday Pay:</div>
                                                    <ValueWrapper>
                                                        <Switch
                                                            checked={ this.state.earning.entitled_unworked_regular_holiday_pay }
                                                            onChange={ ( value ) => {
                                                                this.updateEarning( 'entitled_unworked_regular_holiday_pay', value );
                                                            } }
                                                        />
                                                    </ValueWrapper>
                                                </div>
                                                <div className="col-xs-3">
                                                    <div>Entitled Overtime Pay:</div>
                                                    <ValueWrapper>
                                                        <Switch
                                                            checked={ this.state.earning.entitled_overtime }
                                                            onChange={ ( value ) => {
                                                                this.updateEarning( 'entitled_overtime', value );
                                                            } }
                                                        />
                                                    </ValueWrapper>
                                                </div>
                                                <div className="col-xs-2">
                                                    <div>Entitled Rest Day Pay:</div>
                                                    <ValueWrapper>
                                                        <Switch
                                                            checked={ this.state.earning.entitled_rest_day_pay }
                                                            onChange={ ( value ) => {
                                                                this.updateEarning( 'entitled_rest_day_pay', value );
                                                            } }
                                                        />
                                                    </ValueWrapper>
                                                </div>
                                            </div>
                                            <div className="row" style={ { marginTop: '20px' } }>
                                                <div className="col-xs-3">
                                                    <div>Entitled Night Differential:</div>
                                                    <ValueWrapper>
                                                        <Switch
                                                            checked={ this.state.earning.entitled_night_differential }
                                                            onChange={ ( value ) => {
                                                                this.updateEarning( 'entitled_night_differential', value );
                                                            } }
                                                        />
                                                    </ValueWrapper>
                                                </div>
                                                <div className={ this.state.earning.attendance_data_required ? 'col-xs-4 ' : 'hidden' }>
                                                    <div>Entitled Unworked Special Holiday Pay:</div>
                                                    <ValueWrapper>
                                                        <Switch
                                                            checked={ this.state.earning.entitled_unworked_special_holiday_pay }
                                                            onChange={ ( value ) => {
                                                                this.updateEarning( 'entitled_unworked_special_holiday_pay', value );
                                                            } }
                                                        />
                                                    </ValueWrapper>
                                                </div>
                                                <div className="col-xs-3">
                                                    <div>Entitled Holiday Premium Pay:</div>
                                                    <ValueWrapper>
                                                        <Switch
                                                            checked={ this.state.earning.entitled_holiday_premium_pay }
                                                            onChange={ ( value ) => {
                                                                this.updateEarning( 'entitled_holiday_premium_pay', value );
                                                            } }
                                                        />
                                                    </ValueWrapper>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Container>
                                <div className="foot">
                                    <Button
                                        label="Cancel"
                                        type="neutral"
                                        size="large"
                                        onClick={ () => {
                                            !this.props.previousRoute.path || this.props.previousRoute.path === '/payroll/earnings/:earningId/detail'
                                                ? browserHistory.push( '/earnings' )
                                                : browserHistory.goBack();
                                        } }
                                    />
                                    <Button
                                        label={ this.props.submitted ? <Loader /> : 'Update' }
                                        type="action"
                                        size="large"
                                        onClick={ this.submitForm }
                                    />
                                </div>
                            </div>
                        ) }
                </MainWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    submitted: makeSelectSubmitted(),
    earning: makeSelectEarning(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators( actions, dispatch )
);

export default connect( mapStateToProps, mapDispatchToProps )( EditEarnings );
