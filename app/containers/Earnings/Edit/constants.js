export const INITIALIZE_DATA = 'app/Earnings/Edit/INITIALIZE_DATA';
export const NOTIFY_USER = 'app/Earnings/Edit/NOTIFY_USER';
export const SUBMIT_FORM = 'app/Earnings/Edit/SUBMIT_FORM';

export const SET_LOADING = 'app/Earnings/Edit/SET_LOADING';
export const SET_SUBMITTED = 'app/Earnings/Edit/SET_SUBMITTED';
export const SET_EARNING = 'app/Earnings/Edit/SET_EARNING';
export const SET_NOTIFICATION = 'app/Earnings/Edit/SET_NOTIFICATION';
