import { createSelector } from 'reselect';

/**
 * Direct selector to the edit earnings state domain
 */
const makeSelectEditEarningsDomain = () => (
    ( state ) => ( state.get( 'editEarnings' ) )
);

const makeSelectLoading = () => createSelector(
    makeSelectEditEarningsDomain(),
    ( state ) => state.get( 'loading' )
);

const makeSelectSubmitted = () => createSelector(
    makeSelectEditEarningsDomain(),
    ( state ) => state.get( 'submitted' )
);

const makeSelectEarning = () => createSelector(
    makeSelectEditEarningsDomain(),
    ( state ) => state.get( 'earning' ).toJS()
);

const makeSelectNotification = () => createSelector(
    makeSelectEditEarningsDomain(),
    ( state ) => state.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectEarning,
    makeSelectNotification
};
