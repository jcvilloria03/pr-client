export const WITHHOLDING_TAX_TYPE = {
    COMPENSATION: 'compensation',
    MIN_WAGE_EARNER: 'min_wage_earner',
    EXPANDED: 'expanded',
    SPECIAL: 'special',
    NONE: 'none'
};

