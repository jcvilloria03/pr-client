/* eslint-disable no-undef */
/* eslint-disable import/first */
/* eslint-disable require-jsdoc */
 import { take, call, put, cancel } from 'redux-saga/effects';
 import { LOCATION_CHANGE } from 'react-router-redux';
 import {
    GET_DEFAULT_SCHEDULE,
    SET_DEFAULT_SCHEDULE,
    DEFAULT_SCHEDULE_UPDATE,
    SET_NOTIFICATION,
    GET_NOTIFICATION,
    SET_LOADING
} from './constants';
 import { Fetch } from '../../utils/request';
 import { takeEvery, takeLatest, delay } from 'redux-saga';
 import { REINITIALIZE_PAGE } from '../App/constants';
 import { company } from 'utils/CompanyService';
/**
 * Individual exports for testing
 */
 export function* getDefaultSchedule() {
     try {
         yield put({
             type: SET_LOADING,
             payload: true
         });
         const companyId = company.getLastActiveCompanyId();
         const response = yield call( Fetch, `/default_schedule?company_id=${companyId}`, { method: 'GET' });

         yield put({
             type: SET_DEFAULT_SCHEDULE,
             payload: response.data || []
         });
     } catch ( err ) {
         yield call( notifyUser, {
             show: true,
             title: error.response ? error.response.statusText : 'Error',
             message: error.response ? error.response.data.message : error.statusText,
             type: 'error'
         });
     } finally {
         yield put({
             type: SET_LOADING,
             payload: false
         });
     }
 }
 export function* updateDefaultSchedule( payload ) {
     try {
         const companyId = company.getLastActiveCompanyId();
         yield call( Fetch, '/default_schedule/related_requests_to_updated_days_of_week', {
             method: 'POST',
             data: {
                 company_id: companyId,
                 updated_default_schedule: payload.payload
             }
         });

         yield call( Fetch, '/default_schedule/bulk_update', {
             method: 'PUT',
             data: {
                 company_id: companyId,
                 default_schedules: payload.payload
             }
         });
         yield call( getDefaultSchedule );
         yield call( notifyUser, {
             show: true,
             title: 'Success',
             message: 'Updated Successfully',
             type: 'success'
         });
     } catch ( error ) {
         yield call( notifyUser, {
             show: true,
             title: error.response ? error.response.statusText : 'Error',
             message: error.response ? error.response.data.message : error.statusText,
             type: 'error'
         });
     }
 }

/**
 * Display a notification to user
 */
 export function* notifyUser( payload ) {
     const emptyNotification = {
         title: ' ',
         message: ' ',
         show: false,
         type: payload.type || 'error'
     };

     yield put({
         type: SET_NOTIFICATION,
         payload: emptyNotification
     });

     yield put({
         type: SET_NOTIFICATION,
         payload
     });

     yield call( delay, 5000 );

     yield put({
         type: SET_NOTIFICATION,
         payload: emptyNotification
     });
 }

 export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
     yield call( getDefaultSchedule );
 }

 export function* watchForGetDefaultSchedule() {
     const watcher = yield takeEvery( GET_DEFAULT_SCHEDULE, getDefaultSchedule );
     yield take( LOCATION_CHANGE );
     yield cancel( watcher );
 }
 export function* watchForReinitializePage() {
     const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

     yield take( LOCATION_CHANGE );
     yield cancel( watcher );
 }

 export function* watchFordefaultSchedulesUpdate() {
     const watcher = yield takeEvery( DEFAULT_SCHEDULE_UPDATE, updateDefaultSchedule );

     yield take( LOCATION_CHANGE );
     yield cancel( watcher );
 }

 export function* watchNotify() {
     const watcher = yield takeEvery( GET_NOTIFICATION, notifyUser );

     yield take( LOCATION_CHANGE );
     yield cancel( watcher );
 }

// All sagas to be loaded
 export default [
     watchForGetDefaultSchedule,
     watchForReinitializePage,
     watchFordefaultSchedulesUpdate
 ];
