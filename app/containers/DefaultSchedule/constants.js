/*
 *
 * DefaultSchedule constants
 *
 */

export const DEFAULT_ACTION = 'app/DefaultSchedule/DEFAULT_ACTION';
export const GET_DEFAULT_SCHEDULE = 'app/DefaultSchedule/GET_DEFAULT_SCHEDULE';
export const SET_DEFAULT_SCHEDULE = 'app/DefaultSchedule/SET_DEFAULT_SCHEDULE';
export const UPDATE_DEFAULT_SCHEDULE = 'app/DefaultSchedule/UPDATE_DEFAULT_SCHEDULE';
export const UPDATEED_DEFAULT_SCHEDULE = 'app/DefaultSchedule/UPDATE_DEFAULT_SCHEDULE';
export const DEFAULT_SCHEDULE_UPDATE = 'app/DefaultSchedule/DEFAULT_SCHEDULE_UPDATE';
export const SET_NOTIFICATION = 'app/DefaultSchedule/SET_NOTIFICATION';
export const GET_NOTIFICATION = 'app/DefaultSchedule/GET_NOTIFICATION';
export const SET_UPDATE = 'app/DefaultSchedule/SET_UPDATE';
export const GET_UPDATE = 'app/DefaultSchedule/GET_UPDATE';
export const SET_LOADING = 'app/DefaultSchedule/SET_LOADING';
