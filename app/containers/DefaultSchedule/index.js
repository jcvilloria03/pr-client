/* eslint-disable import/first */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';
import Flatpickr from 'react-flatpickr';
import { makeSelectDefaultList, makeSelectNotification, makeSelectLoading } from './selectors';
import { bindActionCreators } from 'redux';
import * as defaultscheduleAction from './actions';
import Radio from '../../components/Radio';
import { H2, H3 } from 'components/Typography';
import Button from '../../components/Button';
import Table from 'components/Table';
import RadioGroup from '../../components/RadioGroup';
import {
    PageWrapper,
    ConfirmBodyWrapperStyle,
    LoadingStyles,
    Footer
} from './styles';
import moment from 'moment';
import SnackBar from 'components/SnackBar';
import Sidebar from 'components/Sidebar';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import SalConfirm from 'components/SalConfirm';
import includes from 'lodash/includes';
import 'flatpickr/dist/themes/material_green.css';

/**
 * DefaultSchedule
 */
export class DefaultSchedule extends React.PureComponent {
    static propTypes = {
        loading: React.PropTypes.bool,
        listdefault: React.PropTypes.array,
        defaultSchedules: React.PropTypes.func,
        updateDefaultSchedule: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    }
    /**
     *
     * DefaultSchedule render method
     *
     */
    constructor( props ) {
        super( props );

        this.state = {
            checked: false,
            editData: this.props.listdefault,
            data: [],
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            }
        };
    }

    componentWillMount() {
        this.props.defaultSchedules();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.listdefault !== this.props.listdefault &&
            this.setState({
                data: nextProps.listdefault
            });
    }

    getTotalHoursForSchedule( schedule ) {
        if ( includes( schedule, 'Invalid date' ) ) {
            return '00:00';
        }

        const workStart = moment( schedule.work_start, 'HH:mm:ss' );
        const workBreakStart = moment( schedule.work_break_start, 'HH:mm:ss' );
        const workBreakEnd = moment( schedule.work_break_end, 'HH:mm:ss' );
        const workEnd = moment( schedule.work_end, 'HH:mm:ss' );

        if ( workEnd.isSameOrBefore( workStart ) ) {
            workEnd.add( 1, 'day' );
        }

        if ( workBreakStart.isBefore( workStart ) ) {
            workBreakStart.add( 1, 'day' );
        }

        if ( workBreakEnd.isBefore( workBreakStart ) ) {
            workBreakEnd.add( 1, 'day' );
        }

        const diff = moment.duration( workEnd.diff( workStart ) )
            .subtract( workBreakEnd.diff( workBreakStart ) );
        const momentDiff = moment( '00:00:00', 'HH:mm:ss' )
            .add( diff );

        return momentDiff.isValid() ? momentDiff.format( 'HH:mm' ) : '00:00';
    }

    saveEditing=() => {
        const data = this.props.listdefault;
        this.props.updateDefaultSchedule( data );
    }

    discard = () => {
        window.location.replace( '/time/schedules' );
    }

    render() {
        const {
            products
        } = this.props;
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                accessor: 'day_of_week',
                className: 'day',
                sortable: false,
                render: ({ row }) => (
                    <div className="day">{row.day_of_week === 1 ?
                    'Monday' : row.day_of_week === 2 ?
                    'Tuesday' : row.day_of_week === 3 ?
                    'Wednesday' : row.day_of_week === 4 ?
                    'Thursday' : row.day_of_week === 5 ?
                    'Friday' : row.day_of_week === 6 ?
                    'Saturday' : row.day_of_week === 7 ?
                    'Sunday' : ''
                     }</div>
                )
            },
            {
                header: 'Type',
                accessor: 'day_type',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        <RadioGroup
                            horizontal
                            id="radioAmendmentReturn"
                            name="type"
                            value={ row.day_type }
                            ref={ ( ref ) => { this.day_type = ref; } }
                            onChange={ ( value ) => {
                                const updatedCustomers = this.state.data.map(
                                    ( customer ) => {
                                        if ( customer.id === row.id ) {
                                            const data = row;
                                            data.day_type = value;
                                            return row;
                                        } return customer;
                                    }
                                  );
                                this.setState({ data: updatedCustomers });
                            } }
                        >
                            <Radio value="regular">Regular</Radio>
                            <Radio value="rest_day">Rest</Radio>
                        </RadioGroup>
                    </div>
                )
            },
            {
                header: 'Work start',
                accessor: 'work_start',
                sortable: false,
                minWidth: 84,
                render: ({ row }) => (
                    row.day_type === 'regular' &&
                    <div>
                        <Flatpickr
                            id="startWork"
                            data-enable-time
                            className="timeInput"
                            value={ row.work_start }
                            name="start"
                            ref={ ( ref ) => { this.workStart = ref; } }
                            options={ {
                                allowInput: false,
                                noCalendar: true,
                                enableTime: true,
                                dateFormat: 'h:i K'
                            } }
                            onChange={ ( value ) => {
                                const updatedCustomers = this.state.data.map(
                                    ( customer ) => {
                                        if ( customer.id === row.id ) {
                                            const data = row;
                                            data.work_start = moment( new Date( value ), ['h:mm A']).format( 'HH:mm:ss' );
                                            return row;
                                        } return customer;
                                    }
                                  );
                                this.setState({ data: updatedCustomers });
                            } }
                        />
                    </div>

                )
            },
            {
                header: 'Work break start',
                accessor: 'work_break_start',
                sortable: false,
                minWidth: 84,
                render: ({ row }) => (
                        row.day_type === 'regular' &&
                        <div>
                            <Flatpickr
                                data-enable-time
                                className="timeInput"
                                value={ row.work_break_start }
                                options={ {
                                    allowInput: false,
                                    noCalendar: true,
                                    enableTime: true,
                                    dateFormat: 'h:i K'
                                } }
                                onChange={ ( value ) => {
                                    const updatedCustomers = this.state.data.map(
                                        ( customer ) => {
                                            if ( customer.id === row.id ) {
                                                const data = row;
                                                data.work_break_start = moment( new Date( value ) ).format( 'HH:mm' );
                                                return row;
                                            } return customer;
                                        }
                                      );
                                    this.setState({ data: updatedCustomers });
                                } }
                            />
                        </div>
                )
            },
            {
                header: 'Work break end',
                accessor: 'work_break_end',
                sortable: false,
                minWidth: 84,
                render: ({ row }) => (
                    row.day_type === 'regular' &&
                    <div>
                        <Flatpickr
                            data-enable-time
                            className="timeInput"
                            value={ row.work_break_end }
                            options={ {
                                allowInput: false,
                                noCalendar: true,
                                enableTime: true,
                                dateFormat: 'h:i K'
                            } }
                            onChange={ ( value ) => {
                                const updatedCustomers = this.state.data.map(
                                    ( customer ) => {
                                        if ( customer.id === row.id ) {
                                            const data = row;
                                            data.work_break_end = moment( new Date( value ) ).format( 'HH:mm' );
                                            return row;
                                        } return customer;
                                    }
                                  );
                                this.setState({ data: updatedCustomers });
                            } }
                        />
                    </div>
                )
            },
            {
                header: 'Work end',
                accessor: 'work_end',
                sortable: false,
                minWidth: 84,
                render: ({ row }) => (
                    row.day_type === 'regular' &&
                    <div>
                        <Flatpickr
                            data-enable-time
                            className="timeInput"
                            value={ row.work_end }
                            options={ {
                                allowInput: false,
                                noCalendar: true,
                                enableTime: true,
                                dateFormat: 'h:i K'
                            } }
                            onChange={ ( value ) => {
                                const updatedCustomers = this.state.data.map(
                                    ( customer ) => {
                                        if ( customer.id === row.id ) {
                                            const data = row;
                                            data.work_end = moment( new Date( value ) ).format( 'HH:mm' );
                                            return row;
                                        } return customer;
                                    }
                                  );
                                this.setState({ data: updatedCustomers });
                            } }
                        />
                    </div>
                )
            },
            {
                header: 'Total',
                accessor: 'total_hours',
                sortable: false,
                minWidth: 100,
                render: ({ row }) => (
                    row.day_type === 'regular' &&
                    <div>{this.getTotalHoursForSchedule( row )}</div>
                )
            }
        ];

        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                products &&
                subscriptionService.isSubscribedToPayroll( products ),
            isSubscribedToTA:
                products &&
                subscriptionService.isSubscribedToTA( products )
        });

        return (
            <div>
                <Helmet
                    title="Default Schedule"
                    meta={ [
                        { name: 'description', content: 'Description of DefaultSchedule' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ this.props.notification && this.props.notification.message }
                    title={ this.props.notification && this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification && this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification && this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ () => this.discard() }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                            Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Discard Changes"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Discard"
                    cancelText="Stay on this page"
                    visible={ this.state.showModal }
                />
                <PageWrapper>
                    {
                        this.props.loading ?
                        (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading Default Schedule</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        ) : (
                            <div>
                                <Container>
                                    <div className="content">
                                        <div className="heading">
                                            <H3>Default Schedule</H3>
                                            <p>Update the fixed schedule of employees who are not assigned to a shift.</p>
                                        </div>
                                    </div>
                                    <Table
                                        data={ this.state.data }
                                        columns={ tableColumns }
                                        ref={ ( ref ) => { this.scheduleTable = ref; } }
                                    />
                                </Container>
                            </div>
                        )}
                </PageWrapper>
                <Footer>
                    <div className="from_footer">
                        <Container>
                            <div className="footer_button">
                                <Button
                                    label={ <span>Cancel</span> }
                                    type="grey"
                                    onClick={ () => {
                                        this.setState({ showModal: false }, () => {
                                            this.setState({ showModal: true });
                                        });
                                    } }
                                />
                                <Button
                                    label="Update"
                                    type="action"
                                    size="large"
                                    onClick={ () => this.saveEditing() }
                                />
                            </div>
                        </Container>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    listdefault: makeSelectDefaultList(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        defaultscheduleAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( DefaultSchedule );
