import styled from 'styled-components';

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    justify-content: center;
    min-height: 100%;
`;

export const LoadingStyles = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 200px;
    margin-top: 100px;
`;

export const PageWrapper = styled.div`

.container{
    overflow: hidden;
    margin: auto;
    padding-bottom: 80px;
}

.hvYuOH .ReactTable .rt-td{
    overflow: visible;
}

.hvYuOH .ReactTable {
    margin-bottom: 100px;
}

.day{
    background-color: #e5f6fc;
}

.timeInput{
    border:1px solid #474747;
    width:100%;
    border-radius: 0px !important;
}

.ReactTable{
    .rt-table{
        border:0;
    }
    .rt-thead{
        background-color: transparent;
        .rt-resizable-header{
            background-color: #00A5E5;
            &:first-child{
                background-color: transparent;
            }
        }
    }
    .rt-tbody{
        .selected{
            background-color: rgba(131, 210, 75, 0.15);
            &:hover{
                background-color: rgba(131, 210, 75, 0.15) !important;
            }
        }
    }

}
    .content {
        margin-top: 110px;
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 90px auto;

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
                max-width: 800px;
                margin-bottom: -5rem;
            }
        }

        .main {
            .btn {
                min-width: 140px;
            }
        }

        .tableAction button {
            width: 130px;
        }
        .foot {
        text-align: right;
        padding: 10px 10vw;
        margin-top: 20px;

        button {
            min-width: 120px;
        }
    }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;

            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 600;
                font-size: 22px;
            }

            .search-wrapper {
                flex-grow: 1;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 30px;

                    input {
                        border: none;
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }
        }
    }
.button{
    margin-left:20rem;
}
.buttons{
    background-color:#9fdc74;
    cursor: pointer;
    color:#FFFFFF
}

.buttons : hover{
    background-color:#9fdc73
}
 .action{
     margin-left:66rem;
 }
    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }

    .no-license-error {
        color: #e13232;
    }
`;
export const Footer = styled.div`
    .from_footer{
        margin: 14px 0px 0px;
        padding: 14px 0px;
        background: #F0F4F6;
        bottom: 0;
        position: fixed;
        width: 100%;
    }
    .from_footer .footer_button{
        width: 100%;
        text-align: end;
    }
    .from_footer .footer_button button{
        padding: 14px 28px;
        border-radius: 2rem;
        &:focus{
            outline:none;
        }
    }
    .from_footer .footer_button .btn_cancel{
        color: #474747;
        border: 1px solid #83D24B;
        background-color: #ffffff;
        margin-right: 5px;
    }
`;
