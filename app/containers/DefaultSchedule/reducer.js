import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_DEFAULT_SCHEDULE,
    UPDATEED_DEFAULT_SCHEDULE,
    SET_NOTIFICATION,
    SET_LOADING
} from './constants';

const initialState = fromJS({
    loading: true,
    defaultschedule: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * DefaultSchedule reducer
 *
 */
function defaultScheduleReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_DEFAULT_SCHEDULE:
            return state.set( 'defaultschedule', fromJS( action.payload ) );
        case UPDATEED_DEFAULT_SCHEDULE:
            return state.set( 'updateDefaulData', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default defaultScheduleReducer;
