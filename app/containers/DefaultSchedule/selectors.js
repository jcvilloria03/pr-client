import { createSelector } from 'reselect';

/**
 * Direct selector to the defaultSchedule state domain
 */
const selectDefaultScheduleDomain = () => ( state ) => state.get( 'defaultSchedule' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by DefaultSchedule
 */

const makeSelectDefaultList = () => createSelector(
  selectDefaultScheduleDomain(),
  ( substate ) => substate.get( 'defaultschedule' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectDefaultScheduleDomain(),
  ( substate ) => substate && substate.get( 'notification' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectDefaultScheduleDomain(),
  ( substate ) => substate.get( 'loading' )
);

export {
  makeSelectDefaultList,
  makeSelectNotification,
  makeSelectLoading
};
