/* eslint-disable require-jsdoc */
import {
    DEFAULT_ACTION,
    GET_DEFAULT_SCHEDULE,
    UPDATE_DEFAULT_SCHEDULE,
    DEFAULT_SCHEDULE_UPDATE,
    GET_UPDATE
} from './constants';

/**
 *
 * DefaultSchedule actions
 *
 */
export function defaultSchedules() {
    return {
        type: GET_DEFAULT_SCHEDULE
    };
}
export function updateDefault( payload ) {
    return {
        type: UPDATE_DEFAULT_SCHEDULE,
        payload
    };
}
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function updateDefaultSchedule( payload ) {
    return {
        type: DEFAULT_SCHEDULE_UPDATE,
        payload
    };
}

export function updateForm( payload ) {
    return {
        type: GET_UPDATE,
        payload
    };
}
