import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'forgotPassword' );

const makeSelectSubmitting = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'submitting' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectError = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'error' )
);

export {
    makeSelectSubmitting,
    makeSelectNotification,
    makeSelectError
};
