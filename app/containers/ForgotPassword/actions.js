import {
    SUBMIT
} from './constants';

/**
 * Submit request for reset password
 * @param {String} payload - Email to request for reset password
 */
export function submitRequest( payload ) {
    return {
        type: SUBMIT,
        payload
    };
}
