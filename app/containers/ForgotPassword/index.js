import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import logo from '../../assets/logo-salarium-blue.png';

import SnackBar from '../../components/SnackBar';
import Input from '../../components/Input';
import Button from '../../components/Button';
import A from '../../components/A';
import { P } from '../../components/Typography';

import {
    StyledWrapper,
    StyledForm
} from './styles';
import * as actions from './actions';
import {
    makeSelectSubmitting,
    makeSelectNotification,
    makeSelectError
} from './selectors';

/**
 *
 * ForgotPassword
 *
 */
export class ForgotPassword extends React.PureComponent {
    static propTypes = {
        notification: PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitRequest: PropTypes.func,
        submitting: PropTypes.bool,
        error: PropTypes.bool
    }

    constructor( props ) {
        super( props );

        /**
         * Reference for email input
         */
        this.email = null;
    }

    /**
     * called when component is mounted.
     */
    componentDidMount() {
        const forgotPasswordEmail = localStorage.getItem( 'authn-forgot-password-email' );
        forgotPasswordEmail && this.email && this.email.setState({
            value: forgotPasswordEmail
        });
    }

    componentDidUpdate( prevProps ) {
        if ( prevProps.error !== this.props.error && this.props.error ) {
            this.email && this.email.setState({
                error: this.props.error,
                errorMessage: 'This email address does not exist'
            });
        }
    }

    /**
     * sends a request to reset password
     */
    submitRequest() {
        const email = this.email.state;
        const valid = email.value && !email.error;

        if ( valid ) {
            this.props.submitRequest( email.value );
        }
    }

    /**
     *
     * ForgotPassword render method
     *
     */
    render() {
        const { notification, submitting } = this.props;

        return (
            <div>
                <StyledWrapper>
                    <Helmet
                        title="Forgot Password"
                        meta={ [
                            { name: 'description', content: 'Description of ForgotPassword' }
                        ] }
                    />
                    <SnackBar
                        message={ notification.message }
                        title={ notification.title }
                        offset={ { top: 0 } }
                        type={ notification.type }
                        show={ notification.show }
                        delay={ 5000 }
                        ref={ ( ref ) => { this.notification = ref; } }
                    />
                    <div className="brand">
                        <img src={ logo } alt="Salarium" />
                    </div>
                    <StyledForm>
                        <Input
                            required
                            id="email"
                            type="email"
                            label="Email"
                            ref={ ( ref ) => { this.email = ref; } }
                            className="sl-u-gap-bottom"
                            disabled={ submitting }
                        />
                        <div id="submit">
                            <Button
                                label="Reset Password"
                                size="large"
                                disabled={ submitting }
                                onClick={ () => {
                                    this.email._validate( this.email.state.value );
                                    this.submitRequest();
                                } }
                            />
                        </div>
                    </StyledForm>
                    <P>Back to <A href="/login">Login</A></P>
                </StyledWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    submitting: makeSelectSubmitting(),
    notification: makeSelectNotification(),
    error: makeSelectError()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ForgotPassword );
