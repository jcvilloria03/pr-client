/*
 *
 * Forgot Password constants
 *
 */
export const NAMESPACE = 'app/containers/ForgotPassword/';

export const SUBMIT = `${NAMESPACE}SUBMIT`;
export const NOTIFICATION = `${NAMESPACE}NOTIFICATION`;
export const SET_NOTIFICATION = `${NAMESPACE}SET_NOTIFICATION`;
export const SET_SUBMITTING = `${NAMESPACE}SET_SUBMITTING`;
export const SET_ERROR = `${NAMESPACE}SET_ERROR`;
