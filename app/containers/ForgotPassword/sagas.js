import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import get from 'lodash/get';

import { browserHistory } from '../../utils/BrowserHistory';
import { auth } from '../../utils/AuthService';

import {
    SUBMIT,
    SET_NOTIFICATION,
    NOTIFICATION,
    SET_SUBMITTING,
    SET_ERROR
} from './constants';

/**
 * Submits form
 *
 */
export function* submitForm({ payload }) {
    yield put({
        type: SET_SUBMITTING,
        payload: true
    });

    try {
        const response = yield call( auth.requestResetPassword, payload );

        if ( response ) {
            yield put({
                type: SET_NOTIFICATION,
                payload: {
                    title: 'Request Ticket Sent',
                    message: response.data.message,
                    show: true,
                    type: 'success'
                }
            });

            yield call( delay, 5000 );
            localStorage.clear();
            yield call( browserHistory.replace, '/login', true );
        }
    } catch ( error ) {
        if ( get( error, 'response.status' ) === 404 ) {
            yield put({
                type: SET_ERROR,
                payload: true
            });
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SET_ERROR,
            payload: false
        });

        yield put({
            type: SET_SUBMITTING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Individual exports for testing
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchNotify
];
