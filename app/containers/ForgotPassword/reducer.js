import { fromJS } from 'immutable';

import {
    SET_NOTIFICATION,
    SET_SUBMITTING,
    SET_ERROR
} from './constants';

const initialState = fromJS({
    submitting: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    error: false
});

/**
 * Forgot Password Reducer
 */
function ForgotPasswordReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_SUBMITTING:
            return state.set( 'submitting', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ERROR:
            return state.set( 'error', action.payload );
        default:
            return state;
    }
}

export default ForgotPasswordReducer;
