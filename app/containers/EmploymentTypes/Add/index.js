/* eslint-disable react/prop-types */
/* eslint-disable import/first */
import React from 'react';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';

import { makeSelectProducts } from '../View/selectors';
import makeSelectAddEmploymentType, {
    makeSelectSubmitted,
    makeSelectNotification
} from './selectors';
import * as employmentAction from './actions';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';

import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import { H3 } from 'components/Typography';
import Button from 'components/Button';
import A from 'components/A';
import Input from 'components/Input';
import Loader from 'components/Loader';

import {
    NavWrapper,
    PageWrapper,
    FootWrapper
} from './styles';

/**
 *
 * AddEmploymentType
 *
 */
export class AddEmploymentType extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        addEmployment: React.PropTypes.func,
        submitted: React.PropTypes.bool,
        products: React.PropTypes.array
    };

    constructor( props ) {
        super( props );
        this.state = {
            names: '',
            counter: 0
        };
    }

    /**
     *
     * AddRanks render method
     *
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.addEmployment !== this.props.addEmployment && this.submitButton.setState({ disabled: nextProps.addEmployment });
    }

    updateCounter( value ) {
        this.setState({
            counter: value.split( ',' ).length
        });
    }

    handleClick = () => {
        this.props.addEmployment({ company_id: company.getLastActiveCompanyId(), names: this.state.names });
    }

    /**
     *
     * AddEmploymentType render method
     *
     */
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        return (
            <div>
                <Helmet
                    title="Add Employment Types"
                    meta={ [
                        { name: 'description', content: 'Description of AddEmploymentType' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/company-settings/company-structure/employment-types', true );
                            } }
                        >
                            &#8592; Back to Employment Types
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="heading">
                                <H3>Add Employment Types</H3>
                                <p>An employment type is a category that determines an  employee&apos; s entitlements.</p>
                            </div>
                            <p className="entriTitle">{this.state.counter} entries added</p>
                            <div className="w-100">
                                <div className="row">
                                    <div className="col-md-2">
                                        <p>Use comma to separate values.<em>Example: Regular, Probationary, Consultant</em></p>
                                    </div>
                                    <div className="col-md-10">
                                        <Input
                                            id="name"
                                            name="name"
                                            type="textarea"
                                            max={ 260 }
                                            value={ this.state.names }
                                            ref={ ( ref ) => { this.names = ref; } }
                                            onChange={ ( value ) => {
                                                this.setState({ names: value, counter: value.split( ',' ).length });
                                            } }
                                            required
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <FootWrapper>
                    <div className="pull-right">
                        <Button
                            label="Cancel"
                            type="action"
                            size="large"
                            alt
                            onClick={ () => { this.setState({ names: '' }); } }
                        />
                        <Button
                            id="submitButton"
                            disabled={ this.props.submitted }
                            label={ this.props.submitted ? <Loader /> : 'Submit' }
                            type="action"
                            size="large"
                            ref={ ( ref ) => { this.submitButton = ref; } }
                            onClick={ this.handleClick }
                        />
                    </div>
                </FootWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    AddEmploymentType: makeSelectAddEmploymentType(),
    products: makeSelectProducts(),
    notification: makeSelectNotification(),
    submitted: makeSelectSubmitted()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        employmentAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddEmploymentType );
