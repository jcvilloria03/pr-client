import { createSelector } from 'reselect';

/**
 * Direct selector to the addEmploymentType state domain
 */
const selectAddEmploymentTypeDomain = () => ( state ) => state.get( 'addEmploymentType' );

/**
 * Other specific selectors
 */
const makeSelectSubmitted = () => createSelector(
  selectAddEmploymentTypeDomain(),
  ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
  selectAddEmploymentTypeDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectErrors = () => createSelector(
  selectAddEmploymentTypeDomain(),
  ( substate ) => {
      let error;
      try {
          error = substate.get( 'errors' ).toJS();
      } catch ( err ) {
          error = substate.get( 'errors' );
      }

      return error;
  }
);
/**
 * Default selector used by AddEmploymentType
 */

const makeSelectAddEmploymentType = () => createSelector(
  selectAddEmploymentTypeDomain(),
  ( substate ) => substate.toJS()
);

export default makeSelectAddEmploymentType;
export {
  selectAddEmploymentTypeDomain,
  makeSelectSubmitted,
  makeSelectNotification,
  makeSelectErrors
};
