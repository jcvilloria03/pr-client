import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_ADD_EMPLOYMENT_TYPE,
    SET_ERRORS,
    NOTIFICATION_SAGA
} from './constants';

const initialState = fromJS({
    errors: {},
    submitted: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * AddEmploymentType reducer
 *
 */
function addEmploymentTypeReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_ADD_EMPLOYMENT_TYPE:
            return state.set( 'submitted', action.payload );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default addEmploymentTypeReducer;
