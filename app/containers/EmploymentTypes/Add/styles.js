import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;
`;

export const PageWrapper = styled.div`
    padding-top: 120px;
    .content {
        margin-top: 40px;
        .entriTitle{
            text-align:right;
        }
         
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: bold;
            }
            p{
                margin-bottom:0.25rem;
            }
        }
    }
`;

export const FootWrapper = styled.div`
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        margin-top: 20px;
        position: fixed;
        bottom: 0;
        left: 0;
        right: 0;
        button {
            min-width: 120px;
        }
`;
