import { LOCATION_CHANGE } from 'react-router-redux';
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';

import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';

import { resetStore } from '../../App/sagas';
import { REINITIALIZE_PAGE } from '../../App/constants';

import {
    SET_ADD_EMPLOYMENT_TYPE,
    GET_ADD_EMPLOYMENT_TYPE,
    SET_ERRORS,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';

/**
 * isNameAvalibility saga
 */
export function* addEmploymentType({ payload }) {
    try {
        yield [
            put({ type: SET_ADD_EMPLOYMENT_TYPE, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        let isAnyUnavailable = false;
        let response;

        const checkName = payload.names.includes( ',' );
        if ( checkName === true ) {
            const match = payload.names.split( ',' );
            for ( const prop of match ) {
                const name = prop.trim();
                response = yield call( Fetch, `/company/${payload.company_id}/employment_type/is_name_available`,
                    {
                        method: 'POST', data: { name }
                    });

                if ( !response.available ) {
                    isAnyUnavailable = true;
                    break;
                }
            }
            if ( !isAnyUnavailable ) {
                try {
                    const names = match.map( ( name ) => name.trim() );
                    yield call( Fetch, '/employment_type/bulk_create',
                        {
                            method: 'POST', data: { company_id: payload.company_id, names }
                        });
                    yield put({
                        type: SET_ADD_EMPLOYMENT_TYPE,
                        payload: false
                    });
                    yield call( notifyUser, {
                        show: true,
                        title: 'Success',
                        message: 'Employment Types successfully added.',
                        type: 'success'
                    });
                    yield call( browserHistory.push, '/company-settings/company-structure/employment-types', true );
                } catch ( error ) {
                    yield call( notifyUser, {
                        show: true,
                        title: error.response ? error.response.statusText : 'Error',
                        message: error.response ? error.response.data.message : 'Employment Type name already exists on the given company',
                        type: 'error'
                    });
                }
            } else {
                yield call( notifyUser, {
                    show: true,
                    title: 'Error',
                    message: 'Employment Type name already exists on the given company.',
                    type: 'error'
                });
                yield put({
                    type: SET_ADD_EMPLOYMENT_TYPE,
                    payload: false
                });
            }
        } else {
            const name = payload.names.trim();
            const res = yield call( Fetch, `/company/${payload.company_id}/employment_type/is_name_available`,
                {
                    method: 'POST', data: { name }
                });

            if ( res.available === true ) {
                try {
                    yield call( Fetch, '/employment_type/bulk_create',
                        {
                            method: 'POST', data: { company_id: payload.company_id, names: [name]}
                        });
                    yield put({
                        type: SET_ADD_EMPLOYMENT_TYPE,
                        payload: false
                    });
                    yield call( notifyUser, {
                        show: true,
                        title: 'Success',
                        message: 'Employment Type successfully added.',
                        type: 'success'
                    });
                    yield call( browserHistory.push, '/company-settings/company-structure/employment-types', true );
                } catch ( error ) {
                    if ( error.response && error.response.status === 406 ) {
                        yield call( setErrors, error.response.data.message );
                    } else {
                        yield call( notifyUser, {
                            show: true,
                            title: error.response ? error.response.statusText : 'Error',
                            message: error.response ? error.response.data.message : '',
                            type: 'error'
                        });
                    }
                }
            } else {
                yield call( notifyUser, {
                    show: true,
                    title: 'Error',
                    message: 'Employment Type name already exists on the given company.',
                    type: 'error'
                });
                yield put({
                    type: SET_ADD_EMPLOYMENT_TYPE,
                    payload: false
                });
            }
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : 'Employment Type name already exists on the given company',
            type: 'error'
        });
        yield put({
            type: SET_ADD_EMPLOYMENT_TYPE,
            payload: false
        });
    }
}

/**
 * changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'general'
        }
    });

    const payload = {
        show: true,
        title: error.title || 'general',
        message: error.message || '',
        type: error.type || 'general'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'general'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    yield call( resetStore );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( GET_ADD_EMPLOYMENT_TYPE, addEmploymentType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchNotify,
    watchForReinitializePage
];
