/*
 *
 * AddEmploymentType constants
 *
 */

export const namespace = 'app/AddEmploymentType';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const SET_ERRORS = `${namespace}/SET_ERRORS`;
export const SET_ADD_EMPLOYMENT_TYPE = `${namespace}/SET_ADD_EMPLOYMENT_TYPE`;
export const GET_ADD_EMPLOYMENT_TYPE = `${namespace}/GET_ADD_EMPLOYMENT_TYPE`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
