import {
    GET_ADD_EMPLOYMENT_TYPE,
    NOTIFICATION
} from './constants';

/**
 *
 * add Employee actions
 *
 */
export function addEmployment( payload ) {
    return {
        type: GET_ADD_EMPLOYMENT_TYPE,
        payload
    };
}
/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
