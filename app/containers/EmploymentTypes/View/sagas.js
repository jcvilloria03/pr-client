import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    LOADING,
    GET_EMPLOYEE_TYPES,
    SET_EMPLOYEE_TYPES,
    SET_EDITING,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_EMPLOYEE_LENGTH,
    GET_EDIT_EMPLOYEE_TYPES,
    SET_EDIT_EMPLOYEE_TYPES,
    GET_DELETE_EMPLOYEE_TYPES,
    SET_DELETE_EMPLOYEE_TYPES
} from './constants';

/**
 * rank Saga functions
 */
export function* getEmployeeTypes() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        
        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                show: false
            }
        });

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/employment_types`, { method: 'Get' });

        yield put({
            type: SET_EMPLOYEE_TYPES,
            payload: response && response.data || []
        });
        yield put({
            type: SET_EMPLOYEE_LENGTH,
            payload: response && response.data.length || []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield call( delay, 5000 );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Edit data EmpolyeType
 */
export function* editEmploymentTyes({ payload }) {
    const { id, data, employmentTypes } = payload;
    try {
        yield put({
            type: SET_EDITING,
            payload: true
        });

        const res = yield call( Fetch, `/employment_type/${id}`,
            {
                method: 'PUT', data
            });

        yield put({
            type: SET_EMPLOYEE_TYPES,
            payload: employmentTypes.map( ( item ) => {
                if ( item.id === res.id ) {
                    return { ...item, name: res.name };
                }

                return item;
            })
        });

        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Changes successfully saved!',
            type: 'success'
        });
        yield call( delay, 5000 );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 ) {
            yield call( notifyUser, {
                show: true,
                title: error.response ? error.response.statusText : 'Error',
                message: error.response ? error.response.data.message : error.statusText,
                type: 'error'
            });
            yield call( delay, 5000 );
        } else {
            yield call( notifyUser, error );
            yield call( delay, 5000 );
        }
        yield put({
            type: SET_EDIT_EMPLOYEE_TYPES,
            payload: false
        });
    } finally {
        yield put({
            type: SET_EDITING,
            payload: false
        });
    }
}

/**
 * Delete employementType saga
 */
export function* deleteEmploymentType({ payload }) {
    try {
        const res = yield call( Fetch, '/company/employment_type/is_in_use',
            {
                method: 'POST', data: payload
            });

        if ( !res.in_use ) {
            const formData = new FormData();

            formData.append( '_method', 'DELETE' );
            formData.append( 'company_id', payload.company_id );

            payload.ids.forEach( ( id ) => {
                formData.append( 'employment_type_ids[]', id );
            });

            yield call( Fetch, '/employment_type/bulk_delete',
                {
                    method: 'POST', data: formData
                });

            yield call( getEmployeeTypes );

            yield call( notifyUser, {
                show: true,
                title: 'Success',
                message: 'Employment Type(s) successfully deleted.',
                type: 'success'
            });
        } else {
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: 'Cannot delete, employment type is in use!',
                type: 'error'
            });

            yield call( notifyUser, {
                show: false
            });
        }
    } catch ( error ) {
        if ( error.response && error.response.status === 406 ) {
            yield call( notifyUser, {
                show: true,
                title: error.response ? error.response.statusText : 'Error',
                message: error.response ? error.response.data.message : error.statusText,
                type: 'error'
            });
            yield call( delay, 5000 );
        } else {
            yield call( notifyUser, error );
            yield call( delay, 5000 );
        }
        yield put({
            type: SET_DELETE_EMPLOYEE_TYPES,
            payload: false
        });
    }
}
/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
}

/**
 * Display a notification to user
 */
export function* notifyFromClient({ payload }) {
    yield call( notifyUser, payload );
}

/**
 * Watcher for NOTIFICATION
 *
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    yield call( resetStore );
    yield call( getEmployeeTypes );
}

/**
 * Watcher for SET_EMPLOYEE_TYPES
 *
 */
export function* watchForRankData() {
    const watcher = yield takeEvery( GET_EMPLOYEE_TYPES, getEmployeeTypes );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EDIT_EMPLOYEE_TYPES
 *
*/
export function* watchForEditRankData() {
    const watcher = yield takeEvery( GET_EDIT_EMPLOYEE_TYPES, editEmploymentTyes );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_DELETE_EMPLOYEE_TYPES
 *
*/
export function* watchForDeleteRankData() {
    const watcher = yield takeEvery( GET_DELETE_EMPLOYEE_TYPES, deleteEmploymentType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForNotifyUser,
    watchForRankData,
    watchForReinitializePage,
    watchForEditRankData,
    watchForDeleteRankData
];
