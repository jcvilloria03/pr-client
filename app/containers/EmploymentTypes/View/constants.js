/*
 *
 * EmploymentTypes constants
 *
 */

export const namespace = 'app/EmploymentTypes';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const LOADING = `${namespace}/LOADING`;
export const GET_EMPLOYEE_TYPES = `${namespace}/GET_EMPLOYEE_TYPES`;
export const SET_EMPLOYEE_TYPES = `${namespace}/SET_EMPLOYEE_TYPES`;
export const SET_EDITING = `${namespace}/SET_EDITING`;
export const SET_EMPLOYEE_LENGTH = `${namespace}/SET_EMPLOYEE_LENGTH`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const GET_EDIT_EMPLOYEE_TYPES = `${namespace}/GET_EDIT_EMPLOYEE_TYPES`;
export const SET_EDIT_EMPLOYEE_TYPES = `${namespace}/SET_EDIT_EMPLOYEE_TYPES`;
export const GET_DELETE_EMPLOYEE_TYPES = `${namespace}/GET_DELETE_EMPLOYEE_TYPES`;
export const SET_DELETE_EMPLOYEE_TYPES = `${namespace}/SET_DELETE_EMPLOYEE_TYPES`;
