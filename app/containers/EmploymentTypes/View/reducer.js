import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    SET_EDITING,
    NOTIFICATION_SAGA,
    SET_EMPLOYEE_LENGTH,
    SET_EMPLOYEE_TYPES
} from './constants';

const initialState = fromJS({
    loading: true,
    isEditing: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    employeeTypeList: [],
    length: 0
});

/**
 *
 * EmploymentTypes reducer
 *
 */
function employmentTypesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_EMPLOYEE_TYPES:
            return state.set( 'employeeTypeList', fromJS( action.payload ) );
        case SET_EDITING:
            return state.set( 'isEditing', action.payload );
        case SET_EMPLOYEE_LENGTH:
            return state.set( 'length', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default employmentTypesReducer;
