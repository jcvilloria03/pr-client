import { createSelector } from 'reselect';

/**
 * Direct selector to the employmentTypes state domain
 */
const selectEmploymentTypesDomain = () => ( state ) => state.get( 'employmentTypes' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
  selectEmploymentTypesDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectEmplomentList = () => createSelector(
  selectEmploymentTypesDomain(),
  ( substate ) => substate.get( 'employeeTypeList' ).toJS()
);

const makeSelectIsEditing = () => createSelector(
  selectEmploymentTypesDomain(),
( substate ) => substate.get( 'isEditing' )
);

const makeSelectEmpTypeLength = () => createSelector(
  selectEmploymentTypesDomain(),
  ( substate ) => substate.get( 'length' )
);

const makeSelectNotification = () => createSelector(
  selectEmploymentTypesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);
/**
 * Default selector used by EmploymentTypes
 */

const makeSelectEmploymentTypes = () => createSelector(
  selectEmploymentTypesDomain(),
  ( substate ) => substate.toJS()
);

export default makeSelectEmploymentTypes;
export {
  selectEmploymentTypesDomain,
  makeSelectProducts,
  makeSelectLoading,
  makeSelectIsEditing,
  makeSelectEmplomentList,
  makeSelectEmpTypeLength,
  makeSelectNotification
};
