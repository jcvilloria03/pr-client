/* eslint-disable react/sort-comp */
/* eslint-disable react/prop-types */
/* eslint-disable import/first */
import React from 'react';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import clone from 'lodash/clone';

import makeSelectEmploymentTypes, {
    makeSelectProducts,
    makeSelectEmplomentList,
    makeSelectLoading,
    makeSelectIsEditing,
    makeSelectNotification,
    makeSelectEmpTypeLength
} from './selectors';
import * as EmployeeTypeAction from './actions';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';

import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import { H2, H3, H5 } from 'components/Typography';
import Loader from 'components/Loader';
import Button from 'components/Button';
import Table from 'components/Table';
import Input from 'components/Input';
import Modal from 'components/Modal';
import SalDropdown from 'components/SalDropdown';
import { isAnyRowSelected, getIdsOfSelectedRows } from 'components/Table/helpers';

import {
    PageWrapper,
    LoadingStyles
} from './styles';

/**
 *
 * EmploymentTypes
 *
 */
export class EmploymentTypes extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        loading: React.PropTypes.bool,
        employmentTypeList: React.PropTypes.array,
        getEmploymentTyes: React.PropTypes.func,
        editEmploymentTyes: React.PropTypes.func,
        deleteEmploymentTyes: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            delete_label: '',
            selection_label: '',
            notification: {
                title: '',
                message: '',
                show: false,
                type: 'error'
            },
            editableCell: false,
            edited: null
        };
    }

    componentWillMount() {
        this.props.getEmploymentTyes();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.notification !== this.props.notification && this.setState({ notification: nextProps.notification });

        nextProps.employmentTypeList !== this.props.employmentTypeList
            && this.setState({
                displayedData: nextProps.employmentTypeList,
                label: formatPaginationLabel( this.employmentFormsTable.tableComponent.state )
            });
    }

    /**
     * isCurrent Edit
     */
    isCurrentlyEdited( row ) {
        return this.state.edited && this.state.edited.id === row.id;
    }

    startEditingRow( row ) {
        this.setState({
            edited: clone( row )
        });
    }

    editedRowState( property, value ) {
        this.rankName.setState({ property: value });

        this.setState({
            edited: {
                ...this.state.edited,
                [ property ]: value
            }
        });
    }

    handleTableChanges = ( tableProps = this.employmentFormsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            delete_label: formatDeleteLabel()
        });
    };

    stopEditing = ( callback ) => {
        this.setState({
            edited: null
        }, callback );
        this.setState({ editableCell: false });
    }

    saveEdited() {
        this.setState({ editableCell: false });
        this.props.editEmploymentTyes({
            id: this.state.edited.id,
            data: this.state.edited,
            employmentTypes: this.props.employmentTypeList
        });
    }

    getDropdownItems() {
        return [
            {
                children: <div>Delete</div>,
                onClick: () => this.DeleteModel.toggle()
            }
        ];
    }

    handleDelete = () => {
        const payload = getIdsOfSelectedRows( this.employmentFormsTable );
        this.props.deleteEmploymentTyes({ ids: payload, company_id: company.getLastActiveCompanyId() });
        this.DeleteModel.toggle();
    }

    getTableColumns() {
        return [
            {
                id: 'name',
                header: 'Employment Type Name',
                minWidth: 600,
                sortable: false,
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) && this.state.editableCell === true ? (
                        <div style={ { width: '100%' } }>
                            <Input
                                id="name"
                                type="text"
                                required
                                value={ this.state.edited.name }
                                ref={ ( ref ) => { this.rankName = ref; } }
                                onChange={ ( value ) => {
                                    this.editedRowState( 'name', value );
                                } }
                            />
                        </div>
                    ) : (
                        <div>{row.name}</div>
                    )
                )
            },
            {
                header: ' ',
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) && this.state.editableCell === true ? (
                        <div>
                            <Button
                                label={ <span>Cancel</span> }
                                type="grey"
                                size="small"
                                onClick={ this.stopEditing }
                            />

                            <Button
                                label={ <span>Save</span> }
                                type="action"
                                size="small"
                                onClick={ () => this.saveEdited() }
                            />
                        </div>
                    ) : (
                        <Button
                            label={ ( !!this.state.edited && this.state.edited.id === row.id && this.props.isEditing ) ? <Loader /> : 'Edit' }
                            type="grey"
                            size="small"
                            onClick={ () => {
                                this.startEditingRow( row );
                                this.setState({ editableCell: !this.state.editableCell });
                            } }
                        />
                    )
                )
            }
        ];
    }

    /**
     *
     * EmploymentTypes render method
     *
     */
    render() {
        const { employmentTypeList, loading } = this.props;
        const { label } = this.state;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        const selectedItems = isAnyRowSelected( this.employmentFormsTable );
        return (
            <div>
                <Helmet
                    title="Employment Types"
                    meta={ [
                        { name: 'description', content: 'Description of Employment Types' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="Confirm Your Action"
                    ref={ ( ref ) => {
                        this.DeleteModel = ref;
                    } }
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.DeleteModel.toggle()
                        },
                        {
                            type: 'danger',
                            label: 'Yes',
                            onClick: this.handleDelete
                        }
                    ] }
                />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Employment Types</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Employment Types</H3>
                                <p>An employment type is a category that determines an  employee&apos; s entitlements.</p>
                            </div>
                            <div className="title">
                                <H5>Employment Types List</H5>
                                <span className="title-Content">
                                    {selectedItems ? <p className="mb-0 mr-1">{this.state.delete_label}</p> : <p className="mb-0 mr-1">{label}</p>}
                                    <div>
                                        {selectedItems ? (
                                            <SalDropdown dropdownItems={ this.getDropdownItems() } />
                                        ) :
                                            ( <Button
                                                id="button-filter"
                                                label="Add Employment Types"
                                                type="action"
                                                onClick={ () => browserHistory.push( '/company-settings/company-structure/employment-types/add', true ) }
                                            /> )
                                        }
                                    </div>
                                </span>
                            </div>
                            <Table
                                loading={ this.props.employmentFormsTable }
                                data={ employmentTypeList }
                                columns={ this.getTableColumns() }
                                pagination
                                selectable
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.employmentFormsTable = ref; } }
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    this.setState({ delete_label: formatDeleteLabel( selectionLength ) });
                                } }
                            />
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    EmploymentTypes: makeSelectEmploymentTypes(),
    products: makeSelectProducts(),
    employmentTypeList: makeSelectEmplomentList(),
    loading: makeSelectLoading(),
    isEditing: makeSelectIsEditing(),
    notification: makeSelectNotification(),
    length: makeSelectEmpTypeLength()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        EmployeeTypeAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EmploymentTypes );
