import {
    DEFAULT_ACTION,
    GET_EMPLOYEE_TYPES,
    GET_EDIT_EMPLOYEE_TYPES,
    GET_DELETE_EMPLOYEE_TYPES
} from './constants';

/**
 * EmploymentTypes get data
 */
export function getEmploymentTyes() {
    return {
        type: GET_EMPLOYEE_TYPES
    };
}

/**
 * Edit data for EmploymentTypes
 */
export function editEmploymentTyes( payload ) {
    return {
        type: GET_EDIT_EMPLOYEE_TYPES,
        payload
    };
}
/**
 * Delete data for EmploymentTypes
 */
export function deleteEmploymentTyes( payload ) {
    return {
        type: GET_DELETE_EMPLOYEE_TYPES,
        payload
    };
}
/**
 *
 * EmploymentTypes actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
