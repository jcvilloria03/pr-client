/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { browserHistory } from '../../utils/BrowserHistory';
import { Wrapper } from './styles';
import A from '../../components/A';
import { SUB } from '../../components/Typography';

/**
 * Component for 404 pages.
 */
export default class NotFound extends React.Component { // eslint-disable-line react/prefer-stateless-function
    /**
    * Component render method
    */
    render() {
        return (
            <div>
                <Wrapper className="container" style={ { overflow: 'hidden', paddingTop: '70px' } }>
                    <div className="boo-wrapper">
                        <div className="boo">
                            <div className="face"></div>
                        </div>
                        <div className="shadow"></div>
                        <h1>404 : PAGE NOT FOUND</h1>
                        <p>
                            The page you were looking for appears to have been moved, deleted, or does not exist.
                            <br />
                            You could go back to <A onClick={ () => { browserHistory.goBack(); } }>where you were</A> or head straight to the <A onClick={ () => { window.location.href = '/dashboard/'; } }>main dashboard</A>.
                        </p>
                        <SUB>If you typed in the address, kindly double check the spellings. If you were redirected here, please let us know. Our <A>Support team</A> would be glad to help you.</SUB>
                    </div>
                </Wrapper>
            </div>
        );
    }
}
