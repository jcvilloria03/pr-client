import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    padding-top: 70px;
    background: #fff;

    .nav {
        padding: 10px 20px;
        background: #f0f4f6;
        margin-bottom: 50px;
    }

    .content {
        & > h2 {
            text-align: center;
            font-weight: 500;
            padding-top: 40px;
        }

        & > p {
            text-align: center;
        }

        .task-table-header {
            margin-top: 20px;

            & > div {
                background: #00A5E5;
                color: #fff;
                padding: 20px;
                font-size: 18px;
            }
        }

        .task-table-module {
            border-bottom: 1px solid #ccc;

            & > div {
                padding: 20px;

                a {
                    margin-right: 20px;

                    i {
                        color: #00A5E5;
                    }
                }
            }
        }

        .task-table-submodule {
            border-bottom: 1px solid #ccc;

            &> div {
                padding: 20px;

                &:first-of-type {
                    padding-left: 60px;
                }

                a {
                    margin-right: 20px;

                    i {
                        color: #00A5E5;
                    }
                }
            }
        }

        .task-table-task {
            border-bottom: 1px solid #ccc;
            display: flex;
                align-items: center;

            &> div {
                &:first-of-type {
                    padding-left: 80px;
                }

                &:last-of-type {
                    padding-top: 20px;
                }

                &:nth-of-type(3) {
                    & > span {
                        margin-right: 20px;
                    }
                }

                a {
                    margin-right: 20px;

                    i {
                        color: #00A5E5;
                    }
                }
            }

            &.summary {
                &> div {
                    &:last-of-type {
                        padding: 20px 0;
                    }
                }
            }
        }
    }

    .hide {
        display: none;
    }

    .submit {
        & > div {
            text-align: right;
            margin-top: 50px;

            & > button {
                min-width: 160px;
                margin-bottom: 50px;
            }
        }
    }

    .errors {
        color: #f21108;
        margin-top: 30px;
    }

    .tagged {
        padding: 4px 10px;
        background: #f0f4f6;
        margin: 0 4px;
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;
