import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectCreateRoleDomain = () => ( state ) => state.get( 'createRole' );

const makeSelectLoading = () => createSelector(
  selectCreateRoleDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectCompanies = () => createSelector(
  selectCreateRoleDomain(),
  ( substate ) => substate.get( 'companies' ).toJS()
);

const makeSelectPayrollGroups = () => createSelector(
  selectCreateRoleDomain(),
  ( substate ) => substate.get( 'payroll_groups' ).toJS()
);

const makeSelectTasks = () => createSelector(
  selectCreateRoleDomain(),
  ( substate ) => substate.get( 'tasks' ).toJS()
);

const makeSelectName = () => createSelector(
  selectCreateRoleDomain(),
  ( substate ) => substate.get( 'name' )
);

const makeSelectNotification = () => createSelector(
  selectCreateRoleDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectCompanies,
  makeSelectPayrollGroups,
  makeSelectTasks,
  makeSelectName,
  makeSelectNotification
};
