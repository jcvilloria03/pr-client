/*
 *
 * Create Roles constants
 *
 */

export const INITIALIZE = 'app/Roles/Create/INITIALIZE';
export const RESET_STORE = 'app/Roles/Create/RESET_STORE';
export const SET_LOADING = 'app/Roles/Create/SET_LOADING';
export const LOAD_COMPANIES = 'app/Roles/Create/LOAD_COMPANIES';
export const LOAD_PAYROLL_GROUPS = 'app/Roles/Create/LOAD_PAYROLL_GROUPS';
export const LOAD_TASKS = 'app/Roles/Create/LOAD_TASKS';
export const CREATE_ROLE = 'app/Roles/Create/CREATE_ROLE';
export const EDIT_ROLE = 'app/Roles/Create/EDIT_ROLE';
export const SET_NAME = 'app/Roles/Create/SET_NAME';
export const NOTIFICATION = 'app/Roles/Create/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/Roles/Create/NOTIFICATION_SAGA';
