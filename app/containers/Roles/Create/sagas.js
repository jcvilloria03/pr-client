import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import {
    INITIALIZE,
    SET_LOADING,
    LOAD_COMPANIES,
    LOAD_PAYROLL_GROUPS,
    LOAD_TASKS,
    SET_NAME,
    CREATE_ROLE,
    EDIT_ROLE,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    RESET_STORE
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';

import { browserHistory } from '../../../utils/BrowserHistory';
import { Fetch } from '../../../utils/request';

/**
 * fetch necessary data for the page
 */
export function* initialize({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        yield call( getCompanies );
        yield call( getPayrollGroups );
        yield call( getTasks, payload );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * fetch all companies in account from API
 */
export function* getCompanies() {
    try {
        const response = yield call( Fetch, '/account/philippine/companies', { method: 'GET' });
        yield put({
            type: LOAD_COMPANIES,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * fetch all payroll groups in account from API
 */
export function* getPayrollGroups() {
    try {
        const response = yield call( Fetch, '/account/payroll_groups', { method: 'GET' });
        yield put({
            type: LOAD_PAYROLL_GROUPS,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * fetch all payroll groups in account from API
 */
export function* getTasks( id ) {
    try {
        const response = yield call( Fetch, '/account/roles/tasks', { method: 'GET' });
        if ( id ) {
            const roleData = yield call( Fetch, `/account/role/${id}`, { method: 'GET' });
            const data = response;
            data.forEach( ( responseModule, moduleIndex ) => {
                responseModule.module.submodules.forEach( ( responseSubmodule, submoduleIndex ) => {
                    responseSubmodule.tasks.forEach( ( responseTask, taskIndex ) => {
                        data[ moduleIndex ].module.submodules[ submoduleIndex ].tasks[ taskIndex ].selected_scopes = {
                            Account: [],
                            Company: [],
                            'Payroll Group': []
                        };
                        data[ moduleIndex ].module.submodules[ submoduleIndex ].tasks[ taskIndex ].active = false;
                        roleData.permissions.forEach( ({ module }) => {
                            module.submodules.forEach( ( submodule ) => {
                                submodule.tasks.forEach( ( task ) => {
                                    if ( data[ moduleIndex ].module.submodules[ submoduleIndex ].tasks[ taskIndex ].id === task.id ) {
                                        data[ moduleIndex ].module.submodules[ submoduleIndex ].tasks[ taskIndex ].active = true;
                                        const keys = Object.keys( task.scopes );
                                        keys.forEach( ( scopeType ) => {
                                            data[ moduleIndex ].module.submodules[ submoduleIndex ].tasks[ taskIndex ].selected_scopes[ scopeType ] = task.scopes[ scopeType ];
                                        });
                                    }
                                });
                            });
                        });
                    });
                });
            });
            yield put({
                type: SET_NAME,
                payload: roleData.name
            });
            yield put({
                type: LOAD_TASKS,
                payload: data
            });
        } else {
            response.forEach( ({ module }, moduleIndex ) => {
                module.submodules.forEach( ( submodule, submoduleIndex ) => {
                    submodule.tasks.forEach( ( task, taskIndex ) => {
                        response[ moduleIndex ].module.submodules[ submoduleIndex ].tasks[ taskIndex ].selected_scopes = {
                            Account: [],
                            Company: [],
                            'Payroll Group': []
                        };
                    });
                });
            });
            yield put({
                type: LOAD_TASKS,
                payload: response
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * fetch all payroll groups in account from API
 */
export function* createRole({ payload }) {
    try {
        const response = yield call( Fetch, '/account/role', { method: 'POST', data: payload });
        if ( response.id ) {
            browserHistory.push( '/roles' );
        } else {
            let errorMessage;
            if ( response.name ) {
                errorMessage = response.name[ 0 ];
            } else if ( response.employee_company_id ) {
                errorMessage = response.employee_company_id[ 0 ];
            } else {
                errorMessage = 'Encountered a problem while saving role.';
            }
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: errorMessage,
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * fetch all payroll groups in account from API
 */
export function* editRole({ payload }) {
    try {
        const response = yield call( Fetch, `/account/role/${payload.id}`, { method: 'PATCH', data: payload.data });
        if ( response.role ) {
            browserHistory.push( '/roles' );
        } else {
            let errorMessage;
            if ( response.name ) {
                errorMessage = response.name[ 0 ];
            } else if ( response.employee_company_id ) {
                errorMessage = response.employee_company_id[ 0 ];
            } else {
                errorMessage = 'Encountered a problem while saving role.';
            }
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: errorMessage,
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * resets the setup store to initial values
 */
export function* resetStore() {
    yield put({
        type: RESET_STORE
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initialize );
}

/**
 * Watcher for initial fetching of necessary data for the page
 */
export function* watchForInitialLoad() {
    const watcher = yield takeEvery( INITIALIZE, initialize );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for create role requests
 */
export function* watchForCreateRole() {
    const watcher = yield takeEvery( CREATE_ROLE, createRole );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for create role requests
 */
export function* watchForEditRole() {
    const watcher = yield takeEvery( EDIT_ROLE, editRole );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitialLoad,
    watchForCreateRole,
    watchForEditRole,
    watchForNotifyUser,
    watchForReinitializePage
];
