import {
    INITIALIZE,
    CREATE_ROLE,
    EDIT_ROLE,
    RESET_STORE,
    NOTIFICATION
} from './constants';

/**
 * resets the store to initial state
 */
export function initialize( payload ) {
    return {
        type: INITIALIZE,
        payload
    };
}

/**
 * sends a request to API to create a new role
 */
export function createRole( payload ) {
    return {
        type: CREATE_ROLE,
        payload
    };
}

/**
 * sends a request to API to create a new role
 */
export function editRole( payload ) {
    return {
        type: EDIT_ROLE,
        payload
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
