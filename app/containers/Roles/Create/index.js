import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import {
    makeSelectLoading,
    makeSelectCompanies,
    makeSelectPayrollGroups,
    makeSelectTasks,
    makeSelectName,
    makeSelectNotification
} from './selectors';
import * as createRoleActions from './actions';
import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';

import SnackBar from '../../../components/SnackBar';

import Icon from '../../../components/Icon';
import A from '../../../components/A';
import { H2, H3, P } from '../../../components/Typography';
import Stepper from '../../../components/Stepper';
import Input from '../../../components/Input';
import MultiSelect from '../../../components/MultiSelect';
import Button from '../../../components/Button';
import ToolTip from '../../../components/ToolTip';
import Switch from '../../../components/Switch';
import SalSelect from '../../../components/Select';
import SalConfirm from '../../../components/SalConfirm';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';

/**
 *
 * Users
 *
 */
export class CreateRole extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        initialize: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        createRole: React.PropTypes.func,
        editRole: React.PropTypes.func,
        params: React.PropTypes.object,
        location: React.PropTypes.object,
        name: React.PropTypes.string,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        companies: React.PropTypes.arrayOf(
            React.PropTypes.object
        ),
        payroll_groups: React.PropTypes.arrayOf(
            React.PropTypes.object
        ),
        tasks: React.PropTypes.array,
        loading: React.PropTypes.bool
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            step: 0,
            modules: [],
            employee_company_id: 0,
            errors: [],
            name: '',
            showModal: false,
            preload: false
        };

        this.batchAssignScope = this.batchAssignScope.bind( this );
        this.validateForm = this.validateForm.bind( this );
        this.createRole = this.createRole.bind( this );
        this.pristineModules = [];
    }

    /**
     * Checks permission to view this page
     */
    componentWillMount() {
        isAuthorized([
            'create.role'
        ], ( authorized ) => {
            if ( !authorized ) {
                browserHistory.replace( '/unauthorized' );
            } else {
                let id = false;
                if ( this.props.params.id || this.props.location.query.from ) {
                    id = this.props.params.id || this.props.location.query.from;
                    this.setState({ preload: true });
                }
                this.props.initialize( id );
            }
        });
    }

    /**
     * handles changes in task list
     * @param {*array} tasks = new list of tasks
     */
    componentWillReceiveProps( nextProps ) {
        const nextTasks = nextProps.tasks;
        if ( nextTasks !== this.props.tasks ) {
            const modules = nextTasks.map( ({ module }) => {
                const moduleScopes = {
                    Account: [],
                    Company: [],
                    'Payroll Group': []
                };
                const { name, ess, submodules } = module;
                let totalModuleTasks = 0;

                submodules.forEach( ( subModule ) => {
                    totalModuleTasks += subModule.tasks.length;
                });

                const payload = {
                    name,
                    ess,
                    open: true,
                    active_tasks: 0,
                    total_tasks: totalModuleTasks,
                    selected_scopes: {
                        Account: 0,
                        Company: 0,
                        'Payroll Group': 0
                    },
                    submodules: submodules.map( ( submodule ) => {
                        const subModuleScopes = {
                            Account: [],
                            Company: [],
                            'Payroll Group': []
                        };
                        const submodulePayload = {
                            name: submodule.name,
                            open: true,
                            active_tasks: 0,
                            total_tasks: submodule.tasks.length,
                            selected_scopes: {
                                Account: 0,
                                Company: 0,
                                'Payroll Group': 0
                            },
                            tasks: submodule.tasks.map( ( task ) => ({
                                id: task.id,
                                name: task.name,
                                description: '',
                                active: task.active,
                                scope_types: task.scopes,
                                selected_scopes: task.selected_scopes
                            }) )
                        };
                        submodulePayload.active_tasks = submodulePayload.tasks.filter( ( task ) => task.active ).length;
                        submodulePayload.tasks.forEach( ( task ) => {
                            const toAdd = [];
                            if ( task.selected_scopes.Account.length ) {
                                task.selected_scopes.Account.forEach( ( selectedAccountId ) => {
                                    subModuleScopes.Account.push( selectedAccountId );
                                    moduleScopes.Account.push( selectedAccountId );

                                    toAdd.push({
                                        label: 'Account',
                                        id: selectedAccountId,
                                        value: `Account-${selectedAccountId}`,
                                        type: 'Account'
                                    });
                                });
                            }

                            if ( task.selected_scopes.Company.length ) {
                                task.selected_scopes.Company.forEach( ( selectedCompanyId ) => {
                                    const selectedCompany = this.props.companies.find( ( company ) => selectedCompanyId === company.id );

                                    subModuleScopes.Company.push( selectedCompanyId );
                                    moduleScopes.Company.push( selectedCompanyId );
                                    toAdd.push( selectedCompanyId === 'all' ? {
                                        label: 'All Companies',
                                        value: 'Company-all',
                                        id: 'all',
                                        type: 'Company'
                                    } : {
                                        label: selectedCompany.name,
                                        id: selectedCompanyId,
                                        value: `Company-${selectedCompanyId}`,
                                        type: 'Company'
                                    });
                                });
                            }

                            if ( task.selected_scopes[ 'Payroll Group' ].length ) {
                                task.selected_scopes[ 'Payroll Group' ].forEach( ( selectedPayrollGroupId ) => {
                                    subModuleScopes[ 'Payroll Group' ].push( selectedPayrollGroupId );
                                    moduleScopes[ 'Payroll Group' ].push( selectedPayrollGroupId );

                                    if ( selectedPayrollGroupId === 'all' ) {
                                        toAdd.push({
                                            label: 'All Payroll Groups',
                                            value: 'Payroll_Group-all',
                                            id: 'all',
                                            type: 'Payroll Group'
                                        });
                                    } else {
                                        const selectedPayrollGroup = this.props.payroll_groups.find( ( payrollGroup ) => selectedPayrollGroupId === payrollGroup.id );
                                        toAdd.push({
                                            label: selectedPayrollGroup.name,
                                            id: selectedPayrollGroupId,
                                            value: `Company-${selectedPayrollGroupId}`,
                                            type: 'Company'
                                        });
                                    }
                                });
                            }

                            this[ `task_${task.id}` ] && this[ `task_${task.id}` ].setState({
                                value: this[ `task_${task.id}` ].state.value ? this[ `task_${task.id}` ].state.value.concat( this.removeDuplicate( toAdd ) ) : this.removeDuplicate( toAdd )
                            });
                        });
                        submodulePayload.selected_scopes = {
                            Account: this.removeDuplicate( subModuleScopes.Account ).length,
                            Company: this.removeDuplicate( subModuleScopes.Company ).length,
                            'Payroll Group': this.removeDuplicate( subModuleScopes[ 'Payroll Group' ]).length
                        };
                        return submodulePayload;
                    })
                };

                payload.selected_scopes = {
                    Account: this.removeDuplicate( moduleScopes.Account ).length,
                    Company: this.removeDuplicate( moduleScopes.Company ).length,
                    'Payroll Group': this.removeDuplicate( moduleScopes[ 'Payroll Group' ]).length
                };

                payload.submodules.forEach( ( submodule ) => {
                    payload.active_tasks += submodule.active_tasks;
                });

                return payload;
            });

            this.setState({ modules });
            this.pristineModules = modules;
            if ( this.props.params.id ) {
                this.role_name.setState({ value: this.props.name });
            }
        }
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    getOptions( scope = []) {
        const options = [];
        if ( scope.includes( 'Account' ) ) {
            options.push({
                label: 'Account',
                id: JSON.parse( localStorage.getItem( 'user' ) ).account_id,
                value: `Account-${JSON.parse( localStorage.getItem( 'user' ) ).account_id}`,
                type: 'Account'
            });
        }

        if ( scope.includes( 'Company' ) ) {
            if ( this.props.companies.length ) {
                options.push({
                    label: 'Companies',
                    value: 'company title',
                    type: 'title',
                    disabled: true
                });
                options.push({
                    label: 'All Companies',
                    value: 'Company-all',
                    id: 'all',
                    type: 'Company'
                });
                this.props.companies.forEach( ( company ) => {
                    options.push({
                        label: company.name,
                        id: company.id,
                        value: `Company-${company.id}`,
                        type: 'Company'
                    });
                });
            }
        }

        if ( scope.includes( 'Payroll Group' ) ) {
            if ( this.props.payroll_groups.length ) {
                options.push({
                    label: 'Payroll Groups',
                    value: 'payroll group title',
                    type: 'title',
                    disabled: true
                });
                options.push({
                    label: 'All Payroll Groups',
                    value: 'Payroll_Group-all',
                    id: 'all',
                    type: 'Payroll Group'
                });

                this.props.payroll_groups.forEach( ( payrollGroup ) => {
                    options.push({
                        label: payrollGroup.name,
                        value: `Payrol_Group-${payrollGroup.id}`,
                        id: payrollGroup.id,
                        type: 'Payroll Group'
                    });
                });
            }
        }

        return options;
    }

    batchAssignScope() {
        if ( this.batch_assign.state.value && this.batch_assign.state.value.length ) {
            const moduleCopy = [].concat( this.state.modules );
            const list = this.batch_assign.state.value;
            this.state.modules.forEach( ( module, moduleIndex ) => {
                if ( !module.ess && module.active_tasks > 0 ) {
                    const moduleScopes = {
                        Account: [],
                        Company: [],
                        'Payroll Group': []
                    };
                    module.submodules.forEach( ( submodule, submoduleIndex ) => {
                        if ( submodule.active_tasks > 0 ) {
                            const subModuleScopes = {
                                Account: [],
                                Company: [],
                                'Payroll Group': []
                            };
                            submodule.tasks.forEach( ( task, taskIndex ) => {
                                const currentValue = this[ `task_${task.id}` ].state.value;
                                list.forEach( ( toAdd ) => {
                                    // CHECK IF TASK INCLUDES THE SCOPE TYPE OF "toAdd" AND IS NOT ALREADY IN THE SELECTED SCOPES
                                    if ( task.active && task.scope_types.includes( toAdd.type ) && !task.selected_scopes[ toAdd.type ].includes( toAdd.id ) ) {
                                        // ADD SCOPE TO TASK STATE
                                        moduleCopy[ moduleIndex ].submodules[ submoduleIndex ].tasks[ taskIndex ].selected_scopes[ toAdd.type ].push( toAdd.id );
                                        // ADD SCOPE TO SCOPE UI IN TASK ROW
                                        currentValue.push( toAdd );
                                        this[ `task_${task.id}` ].setState({
                                            value: currentValue,
                                            error: false
                                        });
                                    }
                                });

                                Object.keys( moduleCopy[ moduleIndex ].submodules[ submoduleIndex ].tasks[ taskIndex ].selected_scopes ).forEach( ( scope ) => {
                                    if ( moduleCopy[ moduleIndex ].submodules[ submoduleIndex ].tasks[ taskIndex ].selected_scopes[ scope ].length ) {
                                        moduleCopy[ moduleIndex ].submodules[ submoduleIndex ].tasks[ taskIndex ].selected_scopes[ scope ].forEach( ( scopeId ) => {
                                            moduleScopes[ scope ].push( scopeId );
                                            subModuleScopes[ scope ].push( scopeId );
                                        });
                                    }
                                });
                            });
                            moduleCopy[ moduleIndex ].submodules[ submoduleIndex ].selected_scopes = {
                                Account: this.removeDuplicate( subModuleScopes.Account ).length,
                                Company: this.removeDuplicate( subModuleScopes.Company ).length,
                                'Payroll Group': this.removeDuplicate( subModuleScopes[ 'Payroll Group' ]).length
                            };
                        }
                    });
                    moduleCopy[ moduleIndex ].selected_scopes = {
                        Account: this.removeDuplicate( moduleScopes.Account ).length,
                        Company: this.removeDuplicate( moduleScopes.Company ).length,
                        'Payroll Group': this.removeDuplicate( moduleScopes[ 'Payroll Group' ]).length
                    };
                }
            });

            // CLEAR BATCH ASSIGN FORM
            this.setState({ modules: moduleCopy }, () => {
                this.batch_assign.setState({ value: []});
            });
        }
    }

    validateForm() {
        this.setState({ errors: []}, () => {
            let pass = true;

            // CHECK IF IT HAS A VALID ROLE NAME
            if ( !this.role_name.state.value ) {
                const errors = this.state.errors;
                this.role_name._validate( this.role_name.state.value );
                errors.push( 'A valid role name is required.' );
                this.setState({ errors });
                pass = false;
            }

            // CHECK IF THERE IS ATLEAST ONE ACTIVE PERMISSION
            if ( this.state.modules.some( ( module ) => module.active_tasks > 0 ) ) {
                // CHECK IF ALL ACTIVE ADMIN TASKS HAS SCOPES
                this.state.modules.filter( ( module ) => !module.ess && module.active_tasks > 0 ).forEach( ( module ) => {
                    module.submodules.filter( ( ( submodule ) => submodule.active_tasks > 0 ) ).forEach( ( submodule ) => {
                        submodule.tasks.filter( ( task ) => task.active ).forEach( ( task ) => {
                            const scopeKeys = Object.keys( task.selected_scopes );
                            let hasScope = false;

                            scopeKeys.forEach( ( key ) => {
                                if ( task.selected_scopes[ key ].length ) {
                                    hasScope = true;
                                }
                            });

                            if ( !hasScope ) {
                                pass = false;
                                // ADD GENERAL MESSAGE
                                const errors = this.state.errors;
                                if ( !errors.includes( 'There are active permission/s without scopes.' ) ) {
                                    errors.push( 'There are active permission/s without scopes.' );
                                    this.setState({ errors });
                                }

                                // DISPLAY ERROR IN SCOPE INPUT
                                this[ `task_${task.id}` ].setState({ error: true });
                            }
                        });
                    });
                });

                // CHECK IF IT HAS EMPLOYEE COMPANY ID IF ATLEAST ONE ESS TASK IS ACTIVE
                this.state.modules.filter( ( module ) => module.ess ).forEach( ( module ) => {
                    if ( module.active_tasks > 0 && this.state.employee_company_id === 0 ) {
                        pass = false;
                        const errors = this.state.errors;
                        errors.push( 'Employee Company is required if any employee permission is active.' );
                        this.setState({ errors });
                    }
                });
            } else {
                pass = false;
                const errors = this.state.errors;
                errors.push( 'Atleast one(1) active permission is required.' );
                this.setState({ errors });
            }

            if ( pass ) {
                const moduleCopy = [].concat( this.state.modules );
                moduleCopy.forEach( ( module, moduleIndex ) => {
                    moduleCopy[ moduleIndex ].open = false;
                    moduleCopy[ moduleIndex ].submodules.forEach( ( submodule, submoduleIndex ) => {
                        moduleCopy[ moduleIndex ].submodules[ submoduleIndex ].open = false;
                    });
                });
                this.setState({ step: 1, modules: moduleCopy, name: this.role_name.state.value });
            } else {
                window.scrollTo( 0, 0 );
            }
        });
    }

    removeDuplicate( arr ) {
        return arr.filter( ( elem, index, self ) => index === self.indexOf( elem ) );
    }

    createRole() {
        // Create request payload
        const { name, modules } = this.state;
        const permissions = [];

        modules.filter( ( module ) => module.active_tasks > 0 ).forEach( ( module ) => {
            module.submodules.filter( ( submodule ) => submodule.active_tasks > 0 ).forEach( ( submodule ) => {
                submodule.tasks.filter( ( task ) => task.active ).forEach( ( task ) => {
                    const scopes = task.scope_types.filter( ( scope ) => scope !== 'Team' && task.selected_scopes[ scope ].length > 0 ).map( ( scope ) => ({
                        type: scope,
                        targets: task.selected_scopes[ scope ]
                    }) );

                    permissions.push( module.ess ? {
                        task_id: task.id
                    } : {
                        task_id: task.id,
                        scopes
                    });
                });
            });
        });

        const payload = { name, permissions };

        if ( this.state.employee_company_id !== 0 ) {
            payload.employee_company_id = this.state.employee_company_id;
        }

        if ( this.props.params.id ) {
            // DISPATCH EDIT ACTION
            this.props.editRole({ id: this.props.params.id, data: payload });
        } else {
            this.props.createRole( payload );
        }
    }

    /**
     *
     * Users render method
     *
     */
    render() {
        return (
            <div>
                <Helmet
                    title={ this.props.params.id ? 'Edit Role' : 'Create New Role' }
                    meta={ [
                        { name: 'description', content: 'Create a new role for your salarium account' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ () => { browserHistory.push( '/roles' ); } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="icon"><i className="fa fa-exclamation-circle" /></div>
                            <div className="message">
                                You are about to leave the page with unsaved changes.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ this.state.showModal }
                />
                <PageWrapper>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    if ( this.pristineModules !== this.state.modules ) {
                                        this.setState({ showModal: false }, () => {
                                            this.setState({ showModal: true });
                                        });
                                    } else {
                                        browserHistory.push( '/roles' );
                                    }
                                } }
                            >
                                &#8592; Back to Employees
                            </A>
                        </Container>
                    </div>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>LOADING</H2>
                                <i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" />
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: this.props.loading || this.state.step === 1 ? 'none' : '' } }>
                            <H2>{this.props.params.id ? 'Edit Role' : 'Create New Role'}</H2>
                            <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lobortis pulvinar sapien</P>
                            <Stepper steps={ [{ label: 'Create New Role' }, { label: 'Summary' }] } activeStep={ this.state.step } />
                            {
                                this.state.errors.length ? (
                                    <div className="row errors">
                                        <div className="col-xs-12">
                                            <ol>
                                                {
                                                    this.state.errors.map( ( error, errorIndex ) => ( <li key={ errorIndex }>{error}</li> ) )
                                                }
                                            </ol>
                                        </div>
                                    </div>
                                ) : false
                            }
                            <div className="row" style={ { marginTop: '50px' } } >
                                <div className="col-xs-4">
                                    <Input
                                        id="role_name"
                                        label="Role name"
                                        required
                                        ref={ ( ref ) => { this.role_name = ref; } }
                                    />
                                </div>
                            </div>
                            <div className="row" style={ { display: 'flex', alignItems: 'center' } }>
                                <div className="col-xs-11">
                                    <MultiSelect
                                        id="batch_assign"
                                        label={
                                            <span>
                                                Batch assign Admin related tasks to account, companies, and payroll groups &nbsp;
                                                <ToolTip
                                                    id="tooltipBasicInformation"
                                                    target={ <i className="fa fa-question-circle" /> }
                                                    placement="right"
                                                >
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lobortis pulvinar sapien
                                                </ToolTip>
                                            </span>
                                        }
                                        ref={ ( ref ) => { this.batch_assign = ref; } }
                                        data={ this.getOptions([ 'Account', 'Company', 'Payroll Group' ]) }
                                    />
                                </div>
                                <div className="col-xs-1">
                                    <Button type="action" size="large" label="apply" onClick={ this.batchAssignScope } />
                                </div>
                            </div>
                            <div className="row task-table-header">
                                <div className="col-xs-3">Modules</div>
                                <div className="col-xs-3">Description</div>
                                <div className="col-xs-2">Permissions</div>
                                <div className="col-xs-4">Scopes</div>
                            </div>
                            {
                                this.state.modules.map( ( module, moduleIndex ) => (
                                    <div key={ moduleIndex }>
                                        <div className="row task-table-module">
                                            <div className="col-xs-3">
                                                <a
                                                    href=""
                                                    onClick={ ( e ) => {
                                                        e.preventDefault();
                                                        const moduleCopy = [].concat( this.state.modules );
                                                        moduleCopy[ moduleIndex ].open = !moduleCopy[ moduleIndex ].open;
                                                        this.setState({ modules: moduleCopy });
                                                    } }
                                                >
                                                    <Icon class={ `fa fa-${module.open ? 'minus' : 'plus'}-square-o` } />
                                                </a>
                                                { module.name }
                                            </div>
                                            <div className="col-xs-3"></div>
                                            <div className="col-xs-2">{ module.active_tasks } of { `${module.total_tasks} task${module.total_tasks > 1 ? 's' : ''}` } activated</div>
                                            {
                                                module.ess ? (
                                                    <div className="col-xs-4">
                                                        <SalSelect
                                                            id="ess_company"
                                                            data={ this.getOptions(['Company']) }
                                                            placeholder="Choose a company"
                                                            onChange={ ( value ) => {
                                                                this.setState({ employee_company_id: value ? value.id : 0 });
                                                            } }
                                                        />
                                                    </div>
                                                ) : (
                                                    <div className="col-xs-4">
                                                        { module.selected_scopes.Account } Account,
                                                        { ` ${module.selected_scopes.Company} Compan${module.selected_scopes.Company > 1 ? 'ies' : 'y'}` },
                                                        { ` ${module.selected_scopes[ 'Payroll Group' ]} Payroll Group${module.selected_scopes[ 'Payroll Group' ] > 1 ? 's' : ''}` }
                                                    </div>
                                                )
                                            }
                                        </div>
                                        <div className={ `col-xs-12${module.open ? '' : ' hide'}` }>
                                            {
                                                module.submodules.map( ( subModule, subModuleIndex ) => (
                                                    <div key={ subModuleIndex }>
                                                        <div className="row task-table-submodule">
                                                            <div className="col-xs-3">
                                                                <a
                                                                    href=""
                                                                    onClick={ ( e ) => {
                                                                        e.preventDefault();
                                                                        const moduleCopy = [].concat( this.state.modules );
                                                                        moduleCopy[ moduleIndex ].submodules[ subModuleIndex ].open = !moduleCopy[ moduleIndex ].submodules[ subModuleIndex ].open;
                                                                        this.setState({ modules: moduleCopy });
                                                                    } }
                                                                >
                                                                    <Icon class={ `fa fa-${subModule.open ? 'minus' : 'plus'}-square-o` } />
                                                                </a>
                                                                { subModule.name }
                                                            </div>
                                                            <div className="col-xs-3"></div>
                                                            <div className="col-xs-2">{ subModule.active_tasks } of { `${subModule.total_tasks} task${subModule.total_tasks > 1 ? 's' : ''}` } activated</div>
                                                            {
                                                                module.ess ? (
                                                                    <div className="col-xs-4"></div>
                                                                ) : (
                                                                    <div className="col-xs-4">
                                                                        { subModule.selected_scopes.Account } Account,
                                                                        { ` ${subModule.selected_scopes.Company} Compan${subModule.selected_scopes.Company > 1 ? 'ies' : 'y'}` },
                                                                        { ` ${subModule.selected_scopes[ 'Payroll Group' ]} Payroll Group${subModule.selected_scopes[ 'Payroll Group' ] > 1 ? 's' : ''}` }
                                                                    </div>
                                                                )
                                                            }
                                                        </div>
                                                        <div className={ `col-xs-12${subModule.open ? '' : ' hide'}` }>
                                                            {
                                                                subModule.tasks.map( ( task, taskIndex ) => (
                                                                    <div key={ taskIndex } className="row task-table-task">
                                                                        <div className="col-xs-3">{taskIndex + 1}. {task.name}</div>
                                                                        <div className="col-xs-3">{task.description}</div>
                                                                        <div className="col-xs-2">
                                                                            <Switch
                                                                                defaultChecked={ task.active }
                                                                                ref={ ( ref ) => { this[ `task_${task.id}_switch` ] = ref; } }
                                                                                onChange={ ( active ) => {
                                                                                    const moduleCopy = [].concat( this.state.modules );
                                                                                    moduleCopy[ moduleIndex ].submodules[ subModuleIndex ].tasks[ taskIndex ].active = active;
                                                                                    moduleCopy[ moduleIndex ].submodules[ subModuleIndex ].active_tasks += active ? 1 : -1;
                                                                                    moduleCopy[ moduleIndex ].active_tasks += active ? 1 : -1;
                                                                                    this.setState({ modules: moduleCopy });
                                                                                } }
                                                                            />
                                                                            { task.active ? 'Active' : 'Inactive' }
                                                                        </div>
                                                                        <div className="col-xs-4" style={ { opacity: task.active && !module.ess ? '1' : '0' } } >
                                                                            <MultiSelect
                                                                                id={ `task_${task.id}` }
                                                                                data={ this.getOptions( task.scope_types ) }
                                                                                onChange={ ( value ) => {
                                                                                    const moduleCopy = [].concat( this.state.modules );

                                                                                    // UPDATE TASK LEVEL STATE WITH NEW SCOPES
                                                                                    const newScope = {
                                                                                        Account: [],
                                                                                        Company: [],
                                                                                        'Payroll Group': []
                                                                                    };

                                                                                    if ( value.length ) {
                                                                                        value.forEach( ( scope ) => {
                                                                                            newScope[ scope.type ].push( scope.id );
                                                                                        });
                                                                                    }

                                                                                    moduleCopy[ moduleIndex ].submodules[ subModuleIndex ].tasks[ taskIndex ].selected_scopes = newScope;

                                                                                    // UPDATE SUBMODULE LEVEL STATE OF CHANGES
                                                                                    const newSubModuleScopes = {
                                                                                        Account: [],
                                                                                        Company: [],
                                                                                        'Payroll Group': []
                                                                                    };

                                                                                    moduleCopy[ moduleIndex ].submodules[ subModuleIndex ].tasks.forEach( ( tsk ) => {
                                                                                        newSubModuleScopes.Account = newSubModuleScopes.Account.concat( tsk.selected_scopes.Account );
                                                                                        newSubModuleScopes.Company = newSubModuleScopes.Company.concat( tsk.selected_scopes.Company );
                                                                                        newSubModuleScopes[ 'Payroll Group' ] = newSubModuleScopes[ 'Payroll Group' ].concat( tsk.selected_scopes[ 'Payroll Group' ]);
                                                                                    });

                                                                                    newSubModuleScopes.Account = this.removeDuplicate( newSubModuleScopes.Account );
                                                                                    newSubModuleScopes.Company = this.removeDuplicate( newSubModuleScopes.Company );
                                                                                    newSubModuleScopes[ 'Payroll Group' ] = this.removeDuplicate( newSubModuleScopes[ 'Payroll Group' ]);

                                                                                    moduleCopy[ moduleIndex ].submodules[ subModuleIndex ].selected_scopes = {
                                                                                        Account: newSubModuleScopes.Account.length,
                                                                                        Company: newSubModuleScopes.Company.length,
                                                                                        'Payroll Group': newSubModuleScopes[ 'Payroll Group' ].length
                                                                                    };

                                                                                    // UPDATE MODULE LEVEL STATE OF CHANGES
                                                                                    const newModuleScopes = {
                                                                                        Account: [],
                                                                                        Company: [],
                                                                                        'Payroll Group': []
                                                                                    };

                                                                                    moduleCopy[ moduleIndex ].submodules.forEach( ( sub ) => {
                                                                                        sub.tasks.forEach( ( tsk ) => {
                                                                                            newModuleScopes.Account = newModuleScopes.Account.concat( tsk.selected_scopes.Account );
                                                                                            newModuleScopes.Company = newModuleScopes.Company.concat( tsk.selected_scopes.Company );
                                                                                            newModuleScopes[ 'Payroll Group' ] = newModuleScopes[ 'Payroll Group' ].concat( tsk.selected_scopes[ 'Payroll Group' ]);
                                                                                        });
                                                                                    });

                                                                                    newModuleScopes.Account = this.removeDuplicate( newModuleScopes.Account );
                                                                                    newModuleScopes.Company = this.removeDuplicate( newModuleScopes.Company );
                                                                                    newModuleScopes[ 'Payroll Group' ] = this.removeDuplicate( newModuleScopes[ 'Payroll Group' ]);

                                                                                    moduleCopy[ moduleIndex ].selected_scopes = {
                                                                                        Account: newModuleScopes.Account.length,
                                                                                        Company: newModuleScopes.Company.length,
                                                                                        'Payroll Group': newModuleScopes[ 'Payroll Group' ].length
                                                                                    };
                                                                                    // UPDATE COMPONENT STATE OF CHANGES
                                                                                    this.setState({ modules: moduleCopy });
                                                                                } }
                                                                                required
                                                                                ref={ ( ref ) => { this[ `task_${task.id}` ] = ref; } }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                ) )
                                                            }
                                                        </div>
                                                    </div>
                                                ) )
                                            }
                                        </div>
                                    </div>
                                ) )
                            }
                            <div className="row submit">
                                <div className="col-xs-12">
                                    <Button
                                        label="Next"
                                        type="action"
                                        size="large"
                                        onClick={ this.validateForm }
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="content" style={ { display: this.props.loading || this.state.step === 0 ? 'none' : '' } }>
                            <H2>Create New Role</H2>
                            <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lobortis pulvinar sapien</P>
                            <Stepper steps={ [{ label: 'Create New Role' }, { label: 'Summary' }] } activeStep={ this.state.step } />
                            <H2>{ this.state.name }</H2>
                            <div className="row task-table-header">
                                <div className="col-xs-3">Modules</div>
                                <div className="col-xs-3">Description</div>
                                <div className="col-xs-2">Permissions</div>
                                <div className="col-xs-4">Scopes</div>
                            </div>
                            {
                                this.state.modules.map( ( module, moduleIndex ) => (
                                    <div key={ moduleIndex }>
                                        <div className="row task-table-module">
                                            <div className="col-xs-3">
                                                <a
                                                    href=""
                                                    onClick={ ( e ) => {
                                                        e.preventDefault();
                                                        const moduleCopy = [].concat( this.state.modules );
                                                        moduleCopy[ moduleIndex ].open = !moduleCopy[ moduleIndex ].open;
                                                        this.setState({ modules: moduleCopy });
                                                    } }
                                                >
                                                    <Icon class={ `fa fa-${module.open ? 'minus' : 'plus'}-square-o` } />
                                                </a>
                                                { module.name }
                                            </div>
                                            <div className="col-xs-3"></div>
                                            <div className="col-xs-2">{ module.active_tasks } of { `${module.total_tasks} task${module.total_tasks > 1 ? 's' : ''}` } activated</div>
                                            {
                                                module.ess ? (
                                                    <div className="col-xs-4">
                                                        {
                                                            this.props.companies.filter( ( company ) => company.id === this.state.employee_company_id ).map( ( company, companyIndex ) => (
                                                                <span key={ companyIndex }>{ company.name }</span>
                                                            ) )
                                                        }
                                                    </div>
                                                ) : (
                                                    <div className="col-xs-4">
                                                        { module.selected_scopes.Account } Account,
                                                        { ` ${module.selected_scopes.Company} Compan${module.selected_scopes.Company > 1 ? 'ies' : 'y'}` },
                                                        { ` ${module.selected_scopes[ 'Payroll Group' ]} Payroll Group${module.selected_scopes[ 'Payroll Group' ] > 1 ? 's' : ''}` }
                                                    </div>
                                                )
                                            }
                                        </div>
                                        <div className={ `col-xs-12${module.open ? '' : ' hide'}` }>
                                            {
                                                module.submodules.map( ( subModule, subModuleIndex ) => (
                                                    <div key={ subModuleIndex }>
                                                        <div className="row task-table-submodule">
                                                            <div className="col-xs-3">
                                                                <a
                                                                    href=""
                                                                    onClick={ ( e ) => {
                                                                        e.preventDefault();
                                                                        const moduleCopy = [].concat( this.state.modules );
                                                                        moduleCopy[ moduleIndex ].submodules[ subModuleIndex ].open = !moduleCopy[ moduleIndex ].submodules[ subModuleIndex ].open;
                                                                        this.setState({ modules: moduleCopy });
                                                                    } }
                                                                >
                                                                    <Icon class={ `fa fa-${subModule.open ? 'minus' : 'plus'}-square-o` } />
                                                                </a>
                                                                { subModule.name }
                                                            </div>
                                                            <div className="col-xs-3"></div>
                                                            <div className="col-xs-2">{ subModule.active_tasks } of { `${subModule.total_tasks} task${subModule.total_tasks > 1 ? 's' : ''}` } activated</div>
                                                            {
                                                                module.ess ? (
                                                                    <div className="col-xs-4"></div>
                                                                ) : (
                                                                    <div className="col-xs-4">
                                                                        { subModule.selected_scopes.Account } Account,
                                                                        { ` ${subModule.selected_scopes.Company} Compan${subModule.selected_scopes.Company > 1 ? 'ies' : 'y'}` },
                                                                        { ` ${subModule.selected_scopes[ 'Payroll Group' ]} Payroll Group${subModule.selected_scopes[ 'Payroll Group' ] > 1 ? 's' : ''}` }
                                                                    </div>
                                                                )
                                                            }
                                                        </div>
                                                        <div className={ `col-xs-12${subModule.open ? '' : ' hide'}` }>
                                                            {
                                                                subModule.tasks.map( ( task, taskIndex ) => (
                                                                    <div key={ taskIndex } className="row task-table-task summary">
                                                                        <div className="col-xs-3">{taskIndex + 1}. {task.name}</div>
                                                                        <div className="col-xs-3">{task.description}</div>
                                                                        <div className="col-xs-2">
                                                                            { task.active ? 'Active' : 'Inactive' }
                                                                        </div>
                                                                        <div className="col-xs-4">
                                                                            {
                                                                                task.selected_scopes.Account.length ? <span className="tagged">Account </span> : false
                                                                            }
                                                                            {
                                                                                task.selected_scopes.Company.length ? task.selected_scopes.Company.map( ( companyId, companyIndex ) => (
                                                                                    <span className="tagged" key={ companyIndex }>
                                                                                        {
                                                                                            companyId === 'all' ? 'All Companies' :
                                                                                            this.props.companies.find( ( company ) => company.id === companyId ).name
                                                                                        }
                                                                                    </span>
                                                                                ) ) : false
                                                                            }
                                                                            {
                                                                                task.selected_scopes[ 'Payroll Group' ].length ? task.selected_scopes[ 'Payroll Group' ].map( ( payrollGroupID, payrollGroupIndex ) => (
                                                                                    <span className="tagged" key={ payrollGroupIndex }>
                                                                                        {
                                                                                            payrollGroupID === 'all' ? 'All Payroll Groups' :
                                                                                            this.props.payroll_groups.find( ( payrollGroup ) => payrollGroup.id === payrollGroupID ).name
                                                                                        }
                                                                                    </span>
                                                                                ) ) : false
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                ) )
                                                            }
                                                        </div>
                                                    </div>
                                                ) )
                                            }
                                        </div>
                                    </div>
                                ) )
                            }
                            <div className="row submit">
                                <div className="col-xs-12">
                                    <Button
                                        label="Previous"
                                        type="action"
                                        size="large"
                                        alt
                                        onClick={ () => {
                                            const moduleCopy = [].concat( this.state.modules );
                                            moduleCopy.forEach( ( module, moduleIndex ) => {
                                                moduleCopy[ moduleIndex ].open = true;
                                                moduleCopy[ moduleIndex ].submodules.forEach( ( submodule, submoduleIndex ) => {
                                                    moduleCopy[ moduleIndex ].submodules[ submoduleIndex ].open = true;
                                                });
                                            });
                                            this.setState({ step: 0, modules: moduleCopy });
                                        } }
                                    />
                                    <Button
                                        label="SUBMIT"
                                        type="action"
                                        size="large"
                                        onClick={ this.createRole }
                                        ref={ ( ref ) => { this.submitRole = ref; } }
                                    />
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    companies: makeSelectCompanies(),
    payroll_groups: makeSelectPayrollGroups(),
    tasks: makeSelectTasks(),
    name: makeSelectName(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createRoleActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( CreateRole );
