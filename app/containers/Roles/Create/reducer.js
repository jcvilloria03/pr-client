import { fromJS } from 'immutable';
import {
    SET_LOADING,
    NOTIFICATION_SAGA,
    LOAD_COMPANIES,
    LOAD_PAYROLL_GROUPS,
    LOAD_TASKS,
    SET_NAME,
    RESET_STORE
} from './constants';

const initialState = fromJS({
    loading: true,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    name: '',
    companies: [],
    payroll_groups: [],
    tasks: []
});

/**
 * Reduce data fetched from API to expose only those needed for page.
 * @param {Array} companies = array of all companies in account
 */
function reduceData( list ) {
    return list.map( ({ name, id }) => ({ name, id }) );
}

/**
 *
 * Create Role reducer
 *
 */
function createRoleReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case LOAD_COMPANIES:
            return state.set( 'companies', fromJS( reduceData( action.payload ) ) );
        case LOAD_PAYROLL_GROUPS:
            return state.set( 'payroll_groups', fromJS( reduceData( action.payload ) ) );
        case LOAD_TASKS:
            return state.set( 'tasks', fromJS( action.payload ) );
        case SET_NAME:
            return state.set( 'name', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default createRoleReducer;
