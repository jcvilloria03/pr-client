import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import {
    SET_LOADING,
    GET_ROLES,
    SET_ROLES,
    DELETE_ROLES,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';

import { REINITIALIZE_PAGE } from '../App/constants';
import { resetStore } from '../App/sagas';

import { Fetch } from '../../utils/request';

/**
 * Individual exports for testing
 */
export function* getRoles() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const response = yield call( Fetch, '/account/roles/all', { method: 'GET' });
        yield put({
            type: SET_ROLES,
            payload: response.data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Individual exports for testing
 */
export function* deleteRoles({ payload }) {
    try {
        yield call( Fetch, '/account/roles/bulk_delete', {
            method: 'DELETE',
            data: {
                role_ids: payload
            }
        });
        yield call( getRoles );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getRoles );
}

/**
 * Watcher for GET Roles
 */
export function* watchForGetRoles() {
    const watcher = yield takeEvery( GET_ROLES, getRoles );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE roles
 */
export function* watchForDeleteRoles() {
    const watcher = yield takeEvery( DELETE_ROLES, deleteRoles );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetRoles,
    watchForDeleteRoles,
    watchForNotifyUser,
    watchForReinitializePage
];
