import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import {
    makeSelectLoading,
    makeSelectRoles,
    makeSelectNotification
} from './selectors';
import * as rolesActions from './actions';
import { browserHistory } from '../../utils/BrowserHistory';
import { isAuthorized } from '../../utils/Authorization';

import SnackBar from '../../components/SnackBar';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';

import { H2, H3, H5, P } from '../../components/Typography';
import Button from '../../components/Button';
import Table from '../../components/Table';
import Input from '../../components/Input';
import SalConfirm from '../../components/SalConfirm';
import Icon from '../../components/Icon';

/**
 *
 * Users
 *
 */
export class Roles extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        getRoles: React.PropTypes.func,
        deleteRoles: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool,
        roles: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                value: React.PropTypes.any,
                label: React.PropTypes.string
            })
        )
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: this.props.roles,
            showDelete: false,
            deleteLabel: '',
            permission: {
                view: false,
                create: false,
                delete: false,
                edit: false
            },
            showModal: false
        };

        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.handleSearch = this.handleSearch.bind( this );
        this.searchInput = null;
        this.deleteRoles = this.deleteRoles.bind( this );
    }

    /**
     * Checks permission to view this page
     */
    componentWillMount() {
        isAuthorized([
            'view.role',
            'create.role',
            'delete.role',
            'edit.role'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.role' ];

            if ( authorized ) {
                this.setState({ permission: {
                    view: authorization[ 'view.role' ],
                    create: authorization[ 'create.role' ],
                    delete: authorization[ 'delete.role' ],
                    edit: authorization[ 'edit.role' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.getRoles();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.roles !== this.props.roles && this.handleSearch();
    }

    /**
     * handles changes to the entries label when table data changes
     * @param {object} tableProps table properties after changes
     */
    handleTableChanges( tableProps = this.rolesTable.tableComponent.state ) {
        const page = tableProps.page + 1;
        const size = tableProps.pageSize;
        const dataLength = tableProps.data.length;
        this.setState({
            label: `Showing ${( ( page * size ) + 1 ) - size} - ${( page * size ) > dataLength ? dataLength : ( page * size )} of ${dataLength} entr${dataLength > 1 ? 'ies' : 'y'}`
        });
    }

    /**
     * handles filters and search inputs
     */
    handleSearch() {
        let searchQuery = null;

        let dataToDisplay = this.props.roles;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery ) {
            dataToDisplay = dataToDisplay.filter( ( role ) => {
                let match = false;
                const { name, users } = role;
                if ( name.toLowerCase().indexOf( searchQuery ) >= 0 ) {
                    match = true;
                } else {
                    users.forEach( ( user ) => {
                        if ( user.toLowerCase().indexOf( searchQuery ) >= 0 ) {
                            match = true;
                        }
                    });
                }

                return match;
            });
        }

        this.setState({ displayedData: dataToDisplay, showDelete: false }, () => {
            this.handleTableChanges();
        });
    }

    /**
     * send a request to delete roles
     */
    deleteRoles() {
        // Get role IDs of selected rows
        const roleIDs = [];
        this.rolesTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                roleIDs.push( this.rolesTable.props.data[ index ].id );
            }
        });
        // dispatch an action to send a request to API
        this.props.deleteRoles( roleIDs );
    }

    /**
     *
     * Users render method
     *
     */
    render() {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Roles',
                accessor: 'name',
                minWidth: 200
            },
            {
                header: 'Entitled Users',
                accessor: 'users',
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.users[ 0 ]}
                        {
                            row.users.length > 1 ? ` +${row.users.length - 1} more` : ''
                        }
                    </div>
                )
            },
            {
                header: 'Permission',
                accessor: 'permission',
                minWidth: 120,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <a className={ this.state.permission.view ? '' : 'hide' } href={ `/payroll/role/${row.id}` }>View</a>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 300,
                sortable: false,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    <div className="tableAction">
                        <Button
                            className={ row.custom_role && this.state.permission.edit ? '' : 'hide' }
                            label={ <span><i className="fa fa-edit" /> Edit</span> }
                            alt
                            to={ `/role/${row.id}/edit` }
                        />
                        <Button
                            className={ this.state.permission.create ? '' : 'hide' }
                            label={ <span><i className="fa fa-copy" /> Make a copy</span> }
                            type="neutral"
                            to={ `/role/create?from=${row.id}` }
                        />
                    </div>
                )
            }
        ];
        return (
            <div>
                <Helmet
                    title="Roles"
                    meta={ [
                        { name: 'description', content: 'List of Roles in your Salarium Account' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.deleteRoles }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="icon"><i className="fa fa-exclamation-circle" /></div>
                            <div className="message">
                                This process cannot be undone.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ this.state.showModal }
                />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Roles.</H2>
                                <i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" />
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <H2>Roles</H2>
                            <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lobortis pulvinar sapien</P>
                            <div id="create">
                                <Button
                                    className={ this.state.permission.create ? '' : 'hide' }
                                    label={ <span><i className="fa fa-plus" /> Create New Role</span> }
                                    size="large"
                                    type="action"
                                    to="/role/create"
                                />
                            </div>
                            <div className="title">
                                <H5>Roles and Permissions List</H5>
                                <div className="search-wrapper">
                                    <Input
                                        className="search"
                                        id="search"
                                        placeholder="Search for role name or users"
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                        onChange={ this.handleSearch }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                    />
                                </div>
                                <span>
                                    {
                                        this.state.showDelete ?
                                            (
                                                <div>
                                                    { this.state.deleteLabel }
                                                    &nbsp;
                                                    <Button
                                                        className={ this.state.permission.delete ? '' : 'hide' }
                                                        label={ <span><i className="fa fa-trash" /> Delete</span> }
                                                        type="danger"
                                                        onClick={ () => {
                                                            this.setState({ showModal: false }, () => {
                                                                this.setState({ showModal: true });
                                                            });
                                                        } }
                                                    />
                                                </div>
                                            )
                                            :
                                            this.state.label
                                    }
                                </span>
                            </div>
                            <Table
                                data={ this.state.displayedData }
                                columns={ tableColumns }
                                pagination
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.rolesTable = ref; } }
                                selectable
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    this.setState({
                                        showDelete: selectionLength > 0,
                                        deleteLabel: `${selectionLength} entr${selectionLength > 1 ? 'ies' : 'y'} selected`
                                    });
                                } }
                            />
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    roles: makeSelectRoles(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        rolesActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Roles );
