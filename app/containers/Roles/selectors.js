import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectRolesDomain = () => ( state ) => state.get( 'roles' );

const makeSelectLoading = () => createSelector(
  selectRolesDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectRoles = () => createSelector(
  selectRolesDomain(),
  ( substate ) => substate.get( 'roles' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectRolesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectRoles,
  makeSelectNotification
};
