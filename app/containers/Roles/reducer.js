import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_ROLES,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../App/constants';

const initialState = fromJS({
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    roles: []
});

/**
 * prepared list of roles fetched from API to accomodate FE needs
 * @param {*array} data array of roles
 */
function prepareRolesData( data ) {
    const newData = [].concat( data );
    data.forEach( ( role, index ) => {
        newData[ index ].users = data[ index ].users || [];
        if ( newData[ index ].users && newData[ index ].users.length > 0 ) {
            newData[ index ].noselect = true;
        } else if ( !data[ index ].custom_role ) {
            newData[ index ].noselect = true;
        } else {
            newData[ index ].noselect = false;
        }
    });

    return newData;
}

/**
 *
 * Roles reducer
 *
 */
function rolesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_ROLES:
            return state.set( 'roles', fromJS( prepareRolesData( action.payload ) ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default rolesReducer;
