import {
    GET_ROLES,
    DELETE_ROLES,
    NOTIFICATION
} from './constants';

/**
 * Fetch List of roles
 */
export function getRoles() {
    return {
        type: GET_ROLES
    };
}

/**
 * delete roles
 */
export function deleteRoles( IDs ) {
    return {
        type: DELETE_ROLES,
        payload: IDs
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
