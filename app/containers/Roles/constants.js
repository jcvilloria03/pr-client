/*
 *
 * Roles constants
 *
 */

export const SET_LOADING = 'app/Roles/SET_LOADING';
export const NOTIFICATION = 'app/Roles/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/Roles/NOTIFICATION_SAGA';
export const GET_ROLES = 'app/Roles/GET_ROLES';
export const SET_ROLES = 'app/Roles/SET_ROLES';
export const DELETE_ROLES = 'app/Roles/DELETE_ROLES';
