import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import fileDownload from 'js-file-download';

import {
    SET_LOADING,
    SET_DOWNLOADING,
    GET_ALLOWANCES,
    GET_PAGINATED_ALLOWANCES,
    SET_PAGINATION,
    SET_ALLOWANCES,
    SET_FILTER_DATA,
    SET_EMPLOYEES,
    DELETE_ALLOWANCES,
    EXPORT_ALLOWANCES,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

/**
 * Get allowances for table
 */
export function* getAllowances({ filters }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const filterData = {};
        const companyId = company.getLastActiveCompanyId();

        yield call( getPaginatedAllowances, {
            payload: {
                page: 1,
                perPage: 10,
                showLoading: true
            },
            filters
        });

        // Get Filters
        const allowanceTypes = yield call( Fetch, `/company/${companyId}/other_income_types/allowance_type?group_by=name&exclude_trashed=1`, { method: 'GET' });
        const locations = yield call( Fetch, `/philippine/company/${companyId}/locations`, { method: 'GET' });
        const departments = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });
        const positions = yield call( Fetch, `/company/${companyId}/positions`, { method: 'GET' });

        filterData.allowanceTypes = allowanceTypes.data;
        filterData.locations = locations.data;
        filterData.departments = departments.data;
        filterData.positions = positions.data;

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });

        yield put({
            type: SET_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * get paginated allowances
 */
export function* getPaginatedAllowances({ payload, filters }) {
    try {
        const { page, perPage, showLoading } = payload;

        if ( showLoading ) {
            yield put({
                type: SET_LOADING,
                payload: true
            });
        }

        let filtersQueryParams = '';
        if ( filters !== undefined && filters.length > 0 ) {
            const filtersIds = {
                other_income_type_ids: [],
                location_ids: [],
                department_ids: [],
                position_ids: []
            };

            filters.map( ( filter ) => {
                if ( filter.type !== 'keyword' ) {
                    const filterFieldName = filter.type === 'allowance_type' ? 'other_income_type_ids' : `${filter.type}_ids`;
                    if ( filter.disabled === false ) {
                        filtersIds[ filterFieldName ].push( filter.value );
                    }
                } else {
                    filtersQueryParams += `&filter[keyword]=${filter.value}`;
                }

                return null;
            });

            for ( const [ field, ids ] of Object.entries( filtersIds ) ) {
                if ( ids.length > 0 ) {
                    for ( const filterId of ids ) {
                        filtersQueryParams += `&filter[${field}][]=${filterId}`;
                    }
                }
            }
        }

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_incomes/allowance_type?page=${page}&per_page=${perPage}${filtersQueryParams}` );
        const { data, ...others } = response;
        // Get employee data from allowances received
        const employeeIdFiltersQueryParams = [];
        data.map( ( allowances ) => {
            employeeIdFiltersQueryParams.push( `filter[employee_ids][]=${allowances.recipients.employees[ 0 ].recipient_id}` );
            return null;
        });
        const employeeIdsQueryString = employeeIdFiltersQueryParams.filter( ( v, i, a ) => a.indexOf( v ) === i ).join( '&' );
        let filteredEmployees = { data: []};
        if ( employeeIdsQueryString ) {
            filteredEmployees = yield call( Fetch, `/company/${companyId}/employees?${employeeIdsQueryString}`, { method: 'GET' });
        }

        const preparedAllowances = data.map( ( allowance ) => {
            const employee = filteredEmployees.data.find( ( e ) => e.id === allowance.recipients.employees[ 0 ].recipient_id );
            return {
                ...allowance,
                employee_location_id: employee.location_id,
                employee_position_id: employee.position_id,
                employee_department_id: employee.department_id
            };
        });

        yield put({
            type: SET_EMPLOYEES,
            payload: filteredEmployees.data
        });

        yield put({
            type: SET_PAGINATION,
            payload: others
        });

        yield put({
            type: SET_ALLOWANCES,
            payload: preparedAllowances
        });

        yield put({
            type: SET_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Batch delete allowances
 */
export function* deleteAllowances({ payload }) {
    try {
        yield call( Fetch, `/company/${company.getLastActiveCompanyId()}/other_income`, {
            method: 'DELETE',
            data: { ids: payload }
        });
        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully deleted',
            show: true,
            type: 'success'
        });
        yield call( getAllowances, {
            payload: {
                showLoading: true
            },
            filters: []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Export allowances
 */
export function* exportAllowances({ payload }) {
    try {
        yield put({
            type: SET_DOWNLOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_incomes/allowance_type/download`, {
            method: 'POST',
            data: { ids: payload }
        });

        fileDownload( response, 'allowances.csv' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DOWNLOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getAllowances );
}

/**
 * Watcher for GET_ALLOWANCES
 */
export function* watchForGetAllowances() {
    const watcher = yield takeEvery( GET_ALLOWANCES, getAllowances );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for DELETE_ALLOWANCES
 */
export function* watchForDeleteAllowances() {
    const watcher = yield takeEvery( DELETE_ALLOWANCES, deleteAllowances );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Paginated adjustments
 */
export function* watchForPaginatedAllowances() {
    const watcher = yield takeEvery( GET_PAGINATED_ALLOWANCES, getPaginatedAllowances );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for EXPORT_ALLOWANCES
 */
export function* watchForExportAllowances() {
    const watcher = yield takeEvery( EXPORT_ALLOWANCES, exportAllowances );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetAllowances,
    watchForDeleteAllowances,
    watchForExportAllowances,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForPaginatedAllowances
];
