import { createSelector } from 'reselect';

/**
 * Direct selector to the allowances state domain
 */
const selectViewAllowancesDomain = () => ( state ) => state.get( 'allowances' );

const makeSelectPagination = () => createSelector(
  selectViewAllowancesDomain(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectViewAllowancesDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectDownloading = () => createSelector(
    selectViewAllowancesDomain(),
    ( substate ) => substate.get( 'downloading' )
);

const makeSelectAllowances = () => createSelector(
    selectViewAllowancesDomain(),
  ( substate ) => substate.get( 'allowances' ).toJS()
);

const makeSelectFilterData = () => createSelector(
    selectViewAllowancesDomain(),
  ( substate ) => substate.get( 'filterData' ).toJS()
);

const makeSelectEmployees = () => createSelector(
  selectViewAllowancesDomain(),
( substate ) => substate.get( 'employees' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewAllowancesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectDownloading,
  makeSelectAllowances,
  makeSelectFilterData,
  makeSelectNotification,
  makeSelectPagination,
  makeSelectEmployees
};
