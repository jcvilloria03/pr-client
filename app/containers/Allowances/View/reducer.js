import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_ALLOWANCES,
    SET_PAGINATION,
    SET_FILTER_DATA,
    NOTIFICATION_SAGA,
    SET_DOWNLOADING,
    SET_EMPLOYEES
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: false,
    downloading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    employees: [],
    allowances: [],
    filterData: {},
    pagination: {
        current_page: 0,
        from: 0,
        last_page: 0,
        per_page: 0,
        to: 0,
        total: 0
    }
});

/**
 * Allowances view reducer
 * @param {object} state
 * @param {object} action
 */
function allowancesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_DOWNLOADING:
            return state.set( 'downloading', action.payload );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_ALLOWANCES:
            return state.set( 'allowances', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case SET_EMPLOYEES:
            return state.set( 'employees', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default allowancesReducer;
