import {
    GET_ALLOWANCES,
    GET_PAGINATED_ALLOWANCES,
    DELETE_ALLOWANCES,
    EXPORT_ALLOWANCES,
    NOTIFICATION,
    GET_EMPLOYEES
} from './constants';

/**
 * Fetch list of allowances
 */
export function getAllowances( filters ) {
    return {
        type: GET_ALLOWANCES,
        filters
    };
}

/**
 * Fetch list of allowances
 */
export function getEmployees() {
    return {
        type: GET_EMPLOYEES
    };
}

/**
 * Fetch list of employees
 */
export function getPaginatedAllowances( payload, filters ) {
    return {
        type: GET_PAGINATED_ALLOWANCES,
        payload,
        filters
    };
}

/**
 * Delete allowances
 * @param {array} IDs
 */
export function deleteAllowances( IDs ) {
    return {
        type: DELETE_ALLOWANCES,
        payload: IDs
    };
}

/**
 * Export allowances
 * @param {array} IDs
 */
export function exportAllowances( IDs ) {
    return {
        type: EXPORT_ALLOWANCES,
        payload: IDs
    };
}

/**
 * Display a notification to a user
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
