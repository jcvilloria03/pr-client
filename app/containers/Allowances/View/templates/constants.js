export const FILTER_TYPES = {
    ALLOWANCE_TYPE: 'allowance_type',
    LOCATION: 'location',
    POSITION: 'position',
    DEPARTMENT: 'department'
};
