import styled from 'styled-components';

export const FilterWrapper = styled.div`
    display: flex;
    flex-direction: column;
    border: 1px solid #ccc;
    padding: 30px 15px;
    padding-bottom: 10px;
    margin-bottom: 25px;
    border-radius: .5rem;
    .sl-c-filter-actions {
        display: flex;
        justify-content: space-between;
        border-top: 1px solid lightgrey;
        padding-top: 20px;
        align-items: center;
        .sl-c-filter-reset > .btn {
            margin-left: 0;
            color: #00A5E5;
            cursor: pointer;
            border: none;
        }
        .sl-c-filter-buttons > .btn {
            width: 100px;
        }
    }
`;
