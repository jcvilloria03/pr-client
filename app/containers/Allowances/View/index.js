import React from 'react';
import orderBy from 'lodash/orderBy';
import isEqual from 'lodash/isEqual';
import Helmet from 'react-helmet';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { subscriptionService } from 'utils/SubscriptionService';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    getEmployeeFullName,
    isBeforeToday
} from 'utils/functions';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from 'utils/constants';

import Loader from 'components/Loader';
import SnackBar from 'components/SnackBar';
import Table from 'components/Table';
import { H2, H3, H5 } from 'components/Typography';
import Button from 'components/Button';
import Input from 'components/Input';
import SalConfirm from 'components/SalConfirm';
import Icon from 'components/Icon';

import SubHeader from 'containers/SubHeader';

import Filter from './templates/filter';

import {
    makeSelectLoading,
    makeSelectAllowances,
    makeSelectFilterData,
    makeSelectNotification,
    makeSelectDownloading,
    makeSelectPagination,
    makeSelectEmployees
} from './selectors';

import * as allowancesActions from './actions';

import {
    CREDIT_FREQUENCY
} from '../constants';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';

/**
 * View Allowances Component
 */
export class View extends React.Component {
    static propTypes = {
        getAllowances: React.PropTypes.func,
        getPaginatedAllowances: React.PropTypes.func,
        deleteAllowances: React.PropTypes.func,
        exportAllowances: React.PropTypes.func,
        allowances: React.PropTypes.array,
        employees: React.PropTypes.array,
        pagination: React.PropTypes.object,
        filterData: React.PropTypes.shape({
            employees: React.PropTypes.array,
            allowanceTypes: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array,
            positions: React.PropTypes.array
        }),
        loading: React.PropTypes.bool,
        downloading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    };

    static defaultProps = {
        loading: true,
        downloading: false,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: this.props.allowances,
            showDelete: false,
            deleteLabel: '',
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            showModal: false,
            showFilter: false,
            hasFiltersApplied: false,
            filtersApplied: []
        };

        this.searchInput = null;
        this.searchTypingTimeout = 0;
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.allowances',
            'create.allowances',
            'delete.allowances',
            'edit.allowances'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.allowances' ];

            if ( authorized ) {
                this.setState({ permission: {
                    view: authorization[ 'view.allowances' ],
                    create: authorization[ 'create.allowances' ],
                    delete: authorization[ 'delete.allowances' ],
                    edit: authorization[ 'edit.allowances' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.getAllowances();
    }

    componentWillReceiveProps( nextProps ) {
        if ( !this.props.allowances.length || !isEqual( nextProps.allowances, this.props.allowances ) ) {
            this.handleSearch( nextProps.allowances );
        }
    }

    handleTableChanges = ( tableProps = this.allowancesTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    handleSearch = ( allowances = this.props.allowances ) => {
        let searchQuery = null;

        let dataToDisplay = allowances;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( searchQuery ) {
            dataToDisplay = dataToDisplay.filter( ( allowance ) => {
                let match = false;
                const employee = this.props.employees.find( ( e ) => e.id === allowance.recipients.employees[ 0 ].recipient_id );

                if ( ( employee ? getEmployeeFullName( employee ) : '' ).toLowerCase().indexOf( searchQuery ) >= 0 ) {
                    match = true;
                }

                return match;
            });
        }

        this.setState({ displayedData: dataToDisplay }, () => {
            this.handleTableChanges();
        });
    }

    deleteAllowances = () => {
        const allowancesIDs = [];

        this.allowancesTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                allowancesIDs.push( this.allowancesTable.props.data[ index ].id );
            }
        });

        this.props.deleteAllowances( allowancesIDs );
        this.setState({ showDelete: false });
    }

    checkIfAnySelectedAllowanceIsInThePast = () => {
        if ( !this.allowancesTable ) return false;

        return this.allowancesTable.state.selected.some( ( selected, index ) => (
            selected && isBeforeToday( this.allowancesTable.props.data[ index ].valid_from, DATE_FORMATS.API )
        ) );
    }

    exportAllowances = () => {
        const selectedAllowances = [];

        const sortingOptions = this.allowancesTable.state.sorting;

        this.allowancesTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedAllowances.push( this.allowancesTable.props.data[ index ]);
            }
        });

        const sortedAllowances = orderBy(
            selectedAllowances,
            sortingOptions.map( ( o ) => o.id ),
            sortingOptions.map( ( o ) => ( o.desc ? 'desc' : 'asc' ) )
        );

        this.props.exportAllowances( sortedAllowances.map( ( allowance ) => allowance.id ) );
    };

    toggleFilter = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    applyFilters = ( filters ) => {
        this.props.getAllowances( filters );
        this.setState({
            filtersApplied: filters,
            hasFiltersApplied: !!filters.length
        }, () => {
            this.handleTableChanges();
        });
    }

    resetFilters = () => {
        this.setState({
            showFilter: false,
            displayedData: this.props.allowances,
            hasFiltersApplied: false
        });
    }

    /**
     * Component Render Method
     */
    render() {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Employee Name',
                sortable: false,
                accessor: 'employee_name',
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.employee_name}
                    </div>
                )
            },
            {
                header: 'Amount',
                sortable: false,
                accessor: 'amount',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {`Php ${formatCurrency( row.amount )}`}
                    </div>
                )
            },
            {
                header: 'Recurring?',
                sortable: false,
                accessor: 'recurring',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.recurring}
                    </div>
                )
            },
            {
                header: 'Frequency',
                sortable: false,
                accessor: 'credit_frequency',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.credit_frequency}
                    </div>
                )
            },
            {
                header: 'Allowance Type',
                sortable: false,
                accessor: 'type_name',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.type_name}
                    </div>
                )
            },
            {
                header: 'Prorated',
                sortable: false,
                accessor: 'prorated',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.prorated}
                    </div>
                )
            },
            {
                header: 'Special Pay Run?',
                sortable: false,
                accessor: 'special_pay_run',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.special_pay_run}
                    </div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.state.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        to={ `/allowances/${row.id}/edit` }
                    />
                )
            }
        ];

        const dataForDisplay = this.state.displayedData.map( ( data ) => {
            if ( this.props.employees ) {
                const employee = this.props.employees.find( ( e ) => e.id === data.recipients.employees[ 0 ].recipient_id );
                const creditFrequency = CREDIT_FREQUENCY.find( ( f ) => f.value === data.credit_frequency );
                return {
                    ...data,
                    employee_name: employee ? getEmployeeFullName( employee ) : '',
                    recurring: data.recurring ? 'Yes' : 'No',
                    credit_frequency: creditFrequency ? creditFrequency.label : '-',
                    type_name: data.type.name,
                    prorated: data.prorated ? 'Yes' : 'No',
                    special_pay_run: data.release_details.length === 0 // there will be no need for this check after backend is fixed
                        ? ''
                        : data.release_details[ 0 ].disburse_through_special_pay_run ? 'Yes' : 'No'
                };
            }
            return {};
        });

        return (
            <div>
                <Helmet
                    title="View Allowances"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.deleteAllowances }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Deleting the allowance will erase all details related to it. Do you want to proceed?
                                {
                                    this.checkIfAnySelectedAllowanceIsInThePast() ? (
                                        <span>
                                            <br />
                                            Previously generated pay runs within the allowance pay pay period will not be affected unless re-opened and regenerated.
                                        </span>
                                    ) : null
                                }
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Allowances"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Allowances.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Allowances</H3>
                                <p>
                                    You may add employee allowances and allowance types through this page.
                                    Allowances can be set as fixed or recurring allowances. You can also specify
                                    the crediting frequency and the proration rule for recurring allowances.
                                </p>
                                <div style={ { textAlign: 'center' } } >
                                    <div className="main">
                                        <Button
                                            className={ this.state.permission.create ? '' : 'hide' }
                                            label="Add Allowances"
                                            size="large"
                                            type="action"
                                            to="/allowances/add"
                                        />
                                        <Button
                                            className={ this.state.permission.create ? '' : 'hide' }
                                            label="Add Allowance Types"
                                            size="large"
                                            type="neutral"
                                            onClick={ () => {
                                                window.location.href = '/company-settings/payroll/allowance-types/add';
                                            } }
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="title">
                                <H5>Allowances List</H5>
                                <div className="search-wrapper">
                                    <Input
                                        className="search"
                                        id="search"
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                        onChange={ () => { this.handleSearch(); } }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                    />
                                </div>
                                <span>
                                    {
                                        this.state.showDelete ?
                                            (
                                                <div>
                                                    { this.state.deleteLabel }
                                                    &nbsp;

                                                    <Button
                                                        className={ this.state.permission.view ? '' : 'hide' }
                                                        label={ this.props.downloading ? <Loader /> : <span>Download</span> }
                                                        type="neutral"
                                                        onClick={ this.exportAllowances }
                                                    />
                                                    <Button
                                                        className={ this.state.permission.delete ? '' : 'hide' }
                                                        label={ <span>Delete</span> }
                                                        type="danger"
                                                        onClick={ () => {
                                                            this.setState({ showModal: false }, () => {
                                                                this.setState({ showModal: true });
                                                            });
                                                        } }
                                                    />
                                                </div>
                                            )
                                            :
                                            (
                                                <div>
                                                    { this.state.label }
                                                    &nbsp;
                                                    <Button
                                                        label={ <span><Icon className="filter-icon" name="filter" /> Filter</span> }
                                                        type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                                        onClick={ this.toggleFilter }
                                                    />
                                                </div>

                                            )
                                    }
                                </span>
                            </div>
                            <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                                <Filter
                                    filterData={ this.props.filterData }
                                    onCancel={ () => { this.resetFilters(); } }
                                    onApply={ ( values ) => { this.applyFilters( values ); } }
                                />
                            </div>
                            <Table
                                data={ dataForDisplay }
                                columns={ tableColumns }
                                pagination
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.allowancesTable = ref; } }
                                selectable
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;

                                    this.setState({
                                        showDelete: selectionLength > 0,
                                        deleteLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                                onPageChange={ ( data ) => this.props.getPaginatedAllowances({ page: data + 1, perPage: this.props.pagination.per_page, showLoading: true }, this.state.filtersApplied ) }
                                onPageSizeChange={ ( data ) => this.props.getPaginatedAllowances({ page: 1, perPage: data, showLoading: true }, this.state.filtersApplied ) }
                                page={ this.props.pagination.current_page - 1 }
                                pageSize={ this.props.pagination.per_page }
                                pages={ this.props.pagination.last_page }
                                manual

                            />
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    downloading: makeSelectDownloading(),
    employees: makeSelectEmployees(),
    allowances: makeSelectAllowances(),
    filterData: makeSelectFilterData(),
    notification: makeSelectNotification(),
    pagination: makeSelectPagination()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        allowancesActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
