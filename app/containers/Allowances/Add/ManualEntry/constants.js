export const PRORATED_BY = [
    { label: 'Prorated by Hour', value: 'PRORATED_BY_HOUR' },
    { label: 'Prorated by Day', value: 'PRORATED_BY_DAY' }
];

export const ENTITLED_WHEN = {
    ON_PAID_LEAVE: 'ON_PAID_LEAVE',
    ON_UNPAID_LEAVE: 'ON_UNPAID_LEAVE',
    ON_PAID_AND_UNPAID_LEAVE: 'ON_PAID_AND_UNPAID_LEAVE'
};
