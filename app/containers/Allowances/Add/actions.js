import {
    INITIALIZE,
    GET_COMPANY_EMPLOYEES,
    SUBMIT_FORM,
    UPLOAD_ALLOWANCES,
    SAVE_ALLOWANCES,
    NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData() {
    return {
        type: INITIALIZE
    };
}

/**
 * Get Company Employees
 */
export function getCompanyEmployees( payload ) {
    return {
        type: GET_COMPANY_EMPLOYEES,
        payload
    };
}

/**
 * Submit allowance
 */
export function submitAllowance( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}

/**
 * Upload allowances
 */
export function uploadAllowances( payload ) {
    return {
        type: UPLOAD_ALLOWANCES,
        payload
    };
}

/**
 * Save allowances
 */
export function saveAllowances( payload ) {
    return {
        type: SAVE_ALLOWANCES,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}

