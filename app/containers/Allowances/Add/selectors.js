import { createSelector } from 'reselect';

/**
 * Direct selector to the add allowances state domain
 */
const selectAddAllowancesDomain = () => ( state ) => state.get( 'addAllowances' );

const makeSelectLoading = () => createSelector(
    selectAddAllowancesDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectFormOptions = () => createSelector(
    selectAddAllowancesDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectAddAllowancesDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectAddAllowancesDomain(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectAddAllowancesDomain(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectAddAllowancesDomain(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectAllowancesPreview = () => createSelector(
    selectAddAllowancesDomain(),
    ( substate ) => substate.get( 'allowancesPreview' ).toJS()
);

const makeSelectSaving = () => createSelector(
    selectAddAllowancesDomain(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectAddAllowancesDomain(),
    ( substate ) => substate.get( 'submitted' )
);

export {
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectAllowancesPreview,
    makeSelectSaving,
    makeSelectSubmitted,
    makeSelectNotification
};
