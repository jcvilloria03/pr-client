export const INITIALIZE = 'app/Allowances/Add/INITIALIZE';
export const GET_COMPANY_EMPLOYEES = 'app/Adjustments/Add/GET_COMPANY_EMPLOYEES';
export const LOADING = 'app/Allowances/Add/LOADING';
export const SET_SUBMITTED = 'app/Allowances/Add/SET_SUBMITTED';
export const SET_FORM_OPTIONS = 'app/Allowances/Add/SET_FORM_OPTIONS';

export const SUBMIT_FORM = 'app/Allowances/Add/SUBMIT_FORM';

export const UPLOAD_ALLOWANCES = 'app/Allowances/Add/UPLOAD_ALLOWANCES';
export const SET_BATCH_UPLOAD_JOB_ID = 'app/Allowances/Add/SET_BATCH_UPLOAD_JOB_ID';
export const SET_BATCH_UPLOAD_STATUS = 'app/Allowances/Add/SET_BATCH_UPLOAD_STATUS';
export const SET_BATCH_UPLOAD_ERRORS = 'app/Allowances/Add/SET_BATCH_UPLOAD_ERRORS';
export const GET_ALLOWANCES_PREVIEW = 'app/Allowances/Add/GET_ALLOWANCES_PREVIEW';
export const SET_SAVING_STATUS = 'app/Allowances/Add/SET_SAVING_STATUS';
export const SET_SAVING_ERRORS = 'app/Allowances/Add/SET_SAVING_ERRORS';
export const SAVE_ALLOWANCES = 'app/Allowances/Add/SAVE_ALLOWANCES';
export const SET_ALLOWANCES_PREVIEW_STATUS = 'app/Allowances/Add/SET_ALLOWANCES_PREVIEW_STATUS';
export const SET_ALLOWANCES_PREVIEW_DATA = 'app/Allowances/Add/SET_ALLOWANCES_PREVIEW_DATA';
export const RESET_ALLOWANCES_PREVIEW = 'app/Allowances/Add/RESET_ALLOWANCES_PREVIEW';

export const NOTIFICATION_SAGA = 'app/Allowances/Add/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Allowances/Add/NOTIFICATION';
