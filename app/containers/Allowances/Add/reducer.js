import { fromJS } from 'immutable';
import {
    LOADING,
    SET_FORM_OPTIONS,
    SET_SUBMITTED,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    RESET_ALLOWANCES_PREVIEW,
    SET_ALLOWANCES_PREVIEW_DATA,
    SET_ALLOWANCES_PREVIEW_STATUS,
    SET_SAVING_STATUS,
    SET_SAVING_ERRORS,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    submitted: false,
    batchUploadJobId: null,
    batchUploadStatus: '',
    batchUploadErrors: {},
    allowancesPreview: {
        status: 'ready',
        data: []
    },
    saving: {
        status: '',
        errors: {}
    },
    formOptions: {
        employees: []
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Manual entry allowance reducer
 *
 */
function manualEntryAllowanceReducer( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_BATCH_UPLOAD_JOB_ID:
            return state.set( 'batchUploadJobId', action.payload );
        case SET_BATCH_UPLOAD_STATUS:
            return state.set( 'batchUploadStatus', action.payload );
        case SET_BATCH_UPLOAD_ERRORS:
            return state.set( 'batchUploadErrors', action.payload );
        case SET_ALLOWANCES_PREVIEW_STATUS:
            return state.setIn([ 'allowancesPreview', 'status' ], action.payload );
        case SET_ALLOWANCES_PREVIEW_DATA:
            return state.setIn([ 'allowancesPreview', 'data' ], fromJS( action.payload ) );
        case SET_SAVING_STATUS:
            return state.setIn([ 'saving', 'status' ], action.payload );
        case SET_SAVING_ERRORS:
            return state.setIn([ 'saving', 'errors' ], action.payload );
        case RESET_ALLOWANCES_PREVIEW:
            return state.set( 'allowancesPreview', fromJS({
                status: 'ready',
                data: []
            }) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default manualEntryAllowanceReducer;
