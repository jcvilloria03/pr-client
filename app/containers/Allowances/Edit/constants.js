export const INITIALIZE = 'app/Allowances/Edit/INITIALIZE';
export const LOADING = 'app/Allowances/Edit/LOADING';
export const GET_COMPANY_EMPLOYEES = 'app/Adjustments/Add/GET_COMPANY_EMPLOYEES';

export const SET_SUBMITTED = 'app/Allowances/Edit/SET_SUBMITTED';
export const SET_FORM_OPTIONS = 'app/Allowances/Edit/SET_FORM_OPTIONS';
export const SET_ALLOWANCE = 'app/Allowances/Edit/SET_ALLOWANCE';

export const UPDATE_ALLOWANCE = 'app/Allowances/Edit/UPDATE_ALLOWANCE';

export const NOTIFICATION_SAGA = 'app/Allowances/Edit/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Allowances/Edit/NOTIFICATION';
