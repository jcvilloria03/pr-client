import {
    INITIALIZE,
    GET_COMPANY_EMPLOYEES,
    UPDATE_ALLOWANCE
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData( payload ) {
    return {
        type: INITIALIZE,
        payload
    };
}

/**
 * Get Company Employees
 */
export function getCompanyEmployees( payload ) {
    return {
        type: GET_COMPANY_EMPLOYEES,
        payload
    };
}

/**
 * Update allowance
 */
export function updateAllowance( payload ) {
    return {
        type: UPDATE_ALLOWANCE,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
