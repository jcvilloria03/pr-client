import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { formatFeedbackMessage } from '../../../utils/functions';
import { RECORD_UPDATED_MESSAGE } from '../../../utils/constants';

import {
    INITIALIZE,
    LOADING,
    GET_COMPANY_EMPLOYEES,
    SET_SUBMITTED,
    SET_FORM_OPTIONS,
    SET_ALLOWANCE,
    UPDATE_ALLOWANCE,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();

        const allowance = yield call( Fetch, `/other_income/${payload.allowanceId}`, { method: 'GET' });

        const allowanceTypes = yield call( Fetch, `/company/${companyId}/other_income_types/allowance_type`, { method: 'GET' });
        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&filter[employee_ids][]=${allowance.recipients.employees[ 0 ].recipient_id}`, { method: 'GET' });

        const formOptions = {};
        formOptions.allowanceTypes = allowanceTypes.data
            .filter( ( type ) => !type.deleted_at || allowance.type.id === type.id );

        formOptions.employees = employees.data.filter( ( employee ) => employee.payroll );

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });

        yield put({
            type: SET_ALLOWANCE,
            payload: allowance
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Get Company Employees
 */
export function* getCompanyEmployees({ payload }) {
    try {
        const keyword = payload.keyword ? payload.keyword : '';
        const companyId = company.getLastActiveCompanyId();
        const formOptions = payload.formOptions;
        let filterEmployeeIdsQueryString = '';

        if ( payload.filterEmployeeIds ) {
            const filterEmployeeIds = payload.filterEmployeeIds;
            if ( filterEmployeeIds.length > 0 ) {
                for ( const employeeId of filterEmployeeIds ) {
                    filterEmployeeIdsQueryString += `&filter[employee_ids][]=${employeeId}`;
                }
            }
        }

        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&keyword=${keyword}${filterEmployeeIdsQueryString}`, { method: 'GET' });

        formOptions.employees = employees.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Update allowance
 */
export function* updateAllowance({ payload }) {
    try {
        yield put({
            type: SET_SUBMITTED,
            payload: true
        });

        yield call( Fetch, `/philippine/allowance/${payload.allowanceId}`, { method: 'PATCH', data: payload });

        yield call( showSuccessMessage );
        yield call( delay, 500 );

        payload.previousRouteName ? yield call( browserHistory.goBack ) : yield call( browserHistory.push, '/allowances' );
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForUpdateAllowance() {
    const watcher = yield takeEvery( UPDATE_ALLOWANCE, updateAllowance );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_COMPANY_EMPLOYEES
 */
export function* watchForGetCompanyEmployees() {
    const watcher = yield takeEvery( GET_COMPANY_EMPLOYEES, getCompanyEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForUpdateAllowance,
    watchForGetCompanyEmployees,
    watchForInitializeData,
    watchForReinitializePage,
    watchNotify
];
