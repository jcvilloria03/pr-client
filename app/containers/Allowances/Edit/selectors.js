import { createSelector } from 'reselect';

/**
 * Direct selector to the edit commission state domain
 */
const selectEditAllowanceDomain = () => ( state ) => state.get( 'editAllowance' );

const makeSelectLoading = () => createSelector(
    selectEditAllowanceDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectSubmitted = () => createSelector(
    selectEditAllowanceDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectFormOptions = () => createSelector(
    selectEditAllowanceDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectAllowance = () => createSelector(
    selectEditAllowanceDomain(),
    ( substate ) => substate.get( 'allowance' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectEditAllowanceDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectAllowance,
    makeSelectNotification
};
