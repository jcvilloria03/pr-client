import { fromJS } from 'immutable';
import {
    LOADING,
    SET_FORM_OPTIONS,
    SET_SUBMITTED,
    NOTIFICATION_SAGA,
    SET_ALLOWANCE
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    submitted: false,
    formOptions: {},
    allowance: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Edit allowance reducer
 *
 */
function editAllowanceReducer( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_ALLOWANCE:
            return state.set( 'allowance', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default editAllowanceReducer;
