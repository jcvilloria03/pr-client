import styled from 'styled-components';

const PageWrapper = styled.div`
    .selected .fill {
        fill: #fff;
    }

    .selected .stroke {
        stroke: #fff;
    }

    .fill {
        fill: rgb(0,165,226);
    }

    .stroke {
        stroke: rgb(0,165,226);
    }
`;

const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;

    h3 {
        font-weight: 600;
    }

    p {
        text-align: center;
        max-width: 800px;
    }
`;

const FormWrapper = styled.div`
    > div {
        & > h4 {
            text-align: left;
        }

        & > button {
            padding: 12px 40px;
            margin-top: 2rem;
        }

        .steps {
            display: flex;
            align-items: stretch;
            justify-content: space-between;
            margin: 1rem 0;

            > .step {
                width: 49%;
            }
        }

        .template,
        .upload {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
            width: 100%;
            height: 100%;
            padding: 2rem;
            text-align: center;
            border: 2px dashed #ccc;
            border-radius: 12px;

            .sl-c-btn--wide {
                min-width: 12rem;
            }
        }

        .upload > div:first-of-type > div > div {
            margin: 0 auto;
        }

        .errors {
            .rt-thead .rt-tr {
                background: #F21108;
                color: #fff;
            }
            .rt-tbody .rt-tr {
                background: rgba(242, 17, 8, .1)
            }
            .ReactTable.-striped .rt-tr.-odd {
                background: rgba(242, 17, 8, .2)
            }

            .ReactTable .rt-th,
            .ReactTable .rt-td {
                flex: 1 0 0px;
                white-space: initial;
                text-overflow: ellipsis;
                padding: 7px 20px;
                overflow: hidden;
                transition: 0.3s ease;
                transition-property: width, min-width, padding, opacity;
            }

            .react-bs-container-body tr {
                background: rgba(249, 210, 210, 0.8);

                &:hover {
                    background: rgba(249, 210, 210, 1);
                }
            }
        }
    }
`;

const MainWrapper = styled.div`
    padding-top: 76px;
    background: #fff;

    .tableAction button {
        width: 130px;
    }

    .action {
        margin-top: 20px;
    }

    .remove-action {
        button {
            border-color: #f21108;
        }
    }

    .switch {
        font-size: 14px;
        margin-bottom: 4px;
    }

    .date-picker {
        .DayPickerInput {
            width: 100%;
        }
        span {
            display: block;
        }
        input {
            width: 100%;
            padding-top: 0px !important;
        }
    }

    .title {
        display: flex;
        align-items: center;
        margin-bottom: 20px;

        h5 {
            margin: 0;
            margin-right: 20px;
            font-weight: 600;
            font-size: 22px;
        }

        .search-wrapper {
            flex-grow: 1;

            .search {
                width: 300px;
                border: 1px solid #333;
                border-radius: 30px;

                input {
                    border: none;
                }
            }

            p {
                display: none;
            }

            .input-group,
            .form-control {
                background-color: transparent;
            }

            .input-group-addon {
                background-color: transparent;
                border: none;
            }

            .isvg {
                display: inline-block;
                width: 1rem;
            }
        }
    }

    .nav {
        padding: 10px 20px;
        background: #f0f4f6;
        margin-bottom: 50px;
    }

    .loader {
        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .money {
        input {
            text-align: right;
        }
    }

    .stepper {
        padding: 20px 10vw;
    }

    .radiogroup {
        padding: 0;
        & > span {
            padding: 0 15px;
            color: #5b5b5b;
            font-size: 14px;
            margin-bottom: 4px;
            margin-right: 1rem;
            font-weight: 400;
        }
    }

    .basic, .employment, .payroll {
        padding: 20px 10vw;

        h4 {
            font-weight: 600;
        }
    }

    .employment .DayPickerInput-Overlay {
        bottom: 47px;
    }

    .date {
        & > div > span {
            display: block;
        }
        .DayPickerInput {
            display: block;

            input {
                width: 100%;
            }
        }
    }

    .heading {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 0 auto 30px auto;

        h3 {
            font-weight: 600;
        }

        p {
            text-align: center;
            max-width: 800px;
        }
    }

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;

        button {
            min-width: 120px;
        }
    }

    .hide {
        display: none;
    }

    .selected .fill {
      fill: #fff;
    }

    .selected .stroke {
      stroke: #fff;
    }

    .fill {
      fill: rgb(0,165,226);
    }

    .stroke {
      stroke: rgb(0,165,226);
    }
`;

const LoaderWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper,
    MainWrapper,
    LoaderWrapper
};
