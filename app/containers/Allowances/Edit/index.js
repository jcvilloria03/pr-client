import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment';
import Helmet from 'react-helmet';
import decimal from 'js-big-decimal';
import get from 'lodash/get';

import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';
import { getEmployeeFullName, formatDate } from 'utils/functions';
import { isAuthorized } from 'utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from 'utils/constants';

import A from 'components/A';
import Typeahead from 'components/Typeahead';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import Input from 'components/Input';
import SalSelect from 'components/Select';
import Button from 'components/Button';
import Switch from 'components/Switch';
import DatePicker from 'components/DatePicker';
import SubHeader from 'containers/SubHeader';
import { H2, H3 } from 'components/Typography';

import {
    PRORATED_BY,
    ENTITLED_WHEN,
    CREDIT_FREQUENCY
} from '../constants';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper,
    MainWrapper,
    LoaderWrapper
} from './styles';

import {
    makeSelectLoading,
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectNotification,
    makeSelectAllowance
} from './selectors';

import * as editAllowanceActions from './actions';

/**
 * Edit Allowance Component
 */
export class Edit extends React.Component {
    static propTypes = {
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        allowance: React.PropTypes.object,
        formOptions: React.PropTypes.object,
        getCompanyEmployees: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        params: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        updateAllowance: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    }

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            allowanceForm: {
                type_id: '',
                recipients: {
                    employees: []
                },
                amount: '',
                recurring: false,
                credit_frequency: '',
                prorated: false,
                prorated_by: '',
                entitled_when: null,
                valid_from: '',
                valid_to: '',
                release_date: '',
                never_expires: false,
                release_details: [
                    {
                        disburse_through_special_pay_run: false
                    }
                ]
            },
            typeaheadTimeout: null
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['edit.allowances'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        const { id: allowanceId } = this.props.params;
        this.props.initializeData({ allowanceId });
    }

    componentWillReceiveProps( nextProps ) {
        if ( Object.keys( nextProps.allowance ).length !== Object.keys( this.props.allowance ).length ) {
            this.setInitialAllowanceFormFromProps( nextProps );
        }
    }

    componentWillUnmount() {
        localStorage.removeItem( 'employeeIdForEditAllowance' );
        this.props.resetStore();
    }

    setInitialAllowanceFormFromProps = ( props ) => {
        const { formOptions, allowance } = props;

        if ( Object.keys( formOptions ).length === 0 ) {
            return;
        }

        this.setState({
            allowanceForm: {
                type_id: allowance.type_id,
                recipients: {
                    employees: [
                        allowance.recipients.employees[ 0 ].recipient_id
                    ]
                },
                amount: `${allowance.amount}`,
                recurring: !!allowance.recurring,
                credit_frequency: allowance.credit_frequency,
                prorated: !!allowance.prorated,
                prorated_by: allowance.prorated_by,
                entitled_when: allowance.entitled_when,
                valid_from: allowance.release_details[ 0 ].disburse_through_special_pay_run ? '' : allowance.valid_from,
                valid_to: allowance.valid_to,
                release_date: allowance.release_details[ 0 ].disburse_through_special_pay_run ? allowance.valid_from : '',
                never_expires: allowance.valid_to === null,
                release_details: [
                    {
                        id: allowance.release_details[ 0 ].id,
                        disburse_through_special_pay_run: allowance.release_details[ 0 ].disburse_through_special_pay_run
                    }
                ]
            }
        });
    }

    getEmployeeFullName = () => {
        const employee = this.props.formOptions.employees.find( ( e ) => e.id === this.state.allowanceForm.recipients.employees[ 0 ], 10 );
        return employee ? getEmployeeFullName( employee ) : '';
    }

    getAllowanceTypesOptions = () => (
        this.props.formOptions.allowanceTypes.map( ( allowanceType ) => ({
            value: allowanceType.id, label: allowanceType.name
        }) )
    )

    validateTypeahead = ( value ) => {
        if ( this.state.typeaheadTimeout ) {
            clearTimeout( this.state.typeaheadTimeout );
        }

        this.state.typeaheadTimeout = setTimeout( () => {
            if ( value.length >= 3 ) {
                this.props.getCompanyEmployees({
                    keyword: value.replace( /\d+/g, '' ),
                    formOptions: this.props.formOptions
                });
            }
        }, 1000 );

        if ( typeof value === 'string' && value !== this.getEmployeeFullName() ) {
            this.employee_id.setState({ value, error: true, errorMessage: 'This field is required' });
        }

        return true;
    }

    handleProratedChange = ( value ) => {
        this.setState({
            allowanceForm: {
                ...this.state.allowanceForm,
                prorated: value,
                prorated_by: value ? this.state.allowanceForm.prorated_by : '',
                entitled_when: null
            }
        });
    }

    handleSpecialPayRunToggle = ( value ) => {
        this.updateAllowanceFormState( 'release_details', [{
            disburse_through_special_pay_run: value
        }], () => {
            const defaultValues = value
                ? {
                    prorated: false,
                    recurring: false,
                    never_expires: null,
                    prorated_by: '',
                    entitled_when: null,
                    valid_from: '',
                    valid_to: ''
                } : {
                    release_date: ''
                };

            this.setState({
                allowanceForm: {
                    ...this.state.allowanceForm,
                    ...defaultValues
                }
            });
        });
    }

    updateAllowanceFormState = ( field, value, callback ) => {
        this.setState({
            allowanceForm: {
                ...this.state.allowanceForm,
                [ field ]: value
            }
        }, () => {
            callback && callback();
        });
    }

    validateForm = () => {
        let valid = true;

        if ( !this.type_id._checkRequire( this.type_id.state.value ) ) {
            valid = false;
        }

        if ( this.amount._validate( this.amount.state.value ) ) {
            valid = false;
        }

        if ( !this.credit_frequency._checkRequire( this.credit_frequency.state.value ) ) {
            valid = false;
        }

        if ( this.valid_from.checkRequired() ) {
            valid = false;
        }

        if ( this.valid_to.checkRequired() ) {
            valid = false;
        } else if ( moment( this.state.allowanceForm.valid_to ) < moment( this.state.allowanceForm.valid_from ) ) {
            this.valid_to.setState({ error: true, message: 'Valid until date must be after or equal to valid from date' });
            valid = false;
        }

        if ( !this.prorated_by._checkRequire( this.prorated_by.state.value ) ) {
            valid = false;
        }

        if ( this.release_date && this.release_date.checkRequired() ) {
            valid = false;
        }

        return valid;
    }

    submitAllowance = () => {
        if ( this.validateForm() ) {
            let allowanceFormAdjusment = {};

            if ( this.state.allowanceForm.release_details[ 0 ].disburse_through_special_pay_run ) {
                // when disburse only on special payrun, we use the release_date instead of valid_from
                allowanceFormAdjusment = {
                    valid_from: this.state.allowanceForm.release_date,
                    release_details: this.state.allowanceForm.release_details
                };
                // we also need to add a date to release_details
                allowanceFormAdjusment.release_details[ 0 ].date = this.state.allowanceForm.release_date;
            }

            const allowanceForm = Object.assign({}, this.state.allowanceForm, allowanceFormAdjusment );

            const { prorated_by, entitled_when, ...rest } = allowanceForm; // eslint-disable-line
            const form = rest.prorated ? allowanceForm : rest;

            this.props.updateAllowance({
                ...form,
                allowanceId: this.props.params.id,
                previousRouteName: this.props.previousRoute.name
            });
        }
    }

    /**
     * Component Render Method
     */
    render() {
        const disburseThroughSpecialPayRun = get( this.state.allowanceForm, 'release_details[0].disburse_through_special_pay_run', false );
        const includedInPayroll = Boolean( get( this.props.allowance, 'release_details[0].payroll_id' ) );
        const disableEdit = disburseThroughSpecialPayRun && includedInPayroll;

        return (
            <PageWrapper>
                <Helmet
                    title="Update Allowance"
                    meta={ [
                        { name: 'description', content: 'Create a new Allowance' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/allowances' );
                            } }
                        >
                         &#8592; Back to { this.props.previousRoute.name ? this.props.previousRoute.name : 'Allowances' }
                        </A>
                    </Container>
                </NavWrapper>
                { this.props.loading ? (
                    <LoaderWrapper>
                        <H2>Loading</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoaderWrapper>
                ) : (
                    <div>
                        <Container>
                            <HeadingWrapper>
                                <h3>Edit Allowance</h3>
                            </HeadingWrapper>
                        </Container>
                        <FormWrapper>
                            <MainWrapper>
                                <div>
                                    <Container>
                                        <div className="row">
                                            <div className="col-xs-6">
                                                <Typeahead
                                                    id="employee_id"
                                                    label="Employee name or ID"
                                                    required
                                                    disabled={ disableEdit }
                                                    defaultValue={ this.getEmployeeFullName() }
                                                    labelKey={ ( employee ) => `${employee.first_name} ${employee.last_name} ${employee.employee_id}` }
                                                    filterBy={ [ 'first_name', 'last_name', 'employee_id' ] }
                                                    placeholder="Search employee name or ID"
                                                    options={ this.props.formOptions.employees }
                                                    ref={ ( ref ) => { this.employee_id = ref; } }
                                                    onInputChange={ this.validateTypeahead }
                                                    onChange={ ( value ) => {
                                                        this.updateAllowanceFormState( 'recipients', {
                                                            employees: [value[ 0 ] ? value[ 0 ].id : null]
                                                        });
                                                    } }
                                                />
                                            </div>
                                            <div className="col-xs-6">
                                                <SalSelect
                                                    id="allowance_type"
                                                    label="Allowance Type"
                                                    placeholder="Select desired allowance type"
                                                    required
                                                    disabled={ disableEdit }
                                                    value={ this.state.allowanceForm.type_id }
                                                    data={ this.getAllowanceTypesOptions() }
                                                    onChange={ ({ value }) => {
                                                        this.updateAllowanceFormState( 'type_id', value );
                                                    } }
                                                    ref={ ( ref ) => { this.type_id = ref; } }
                                                />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-6">
                                                <div className="switch">
                                                    Special Pay Run?
                                                </div>
                                                <Switch
                                                    checked={ disburseThroughSpecialPayRun }
                                                    onChange={ this.handleSpecialPayRunToggle }
                                                    disabled={ disableEdit }
                                                />
                                            </div>
                                            { disburseThroughSpecialPayRun && (
                                                <div className="col-xs-6 date-picker">
                                                    <DatePicker
                                                        label="Release Date"
                                                        dayFormat={ DATE_FORMATS.DISPLAY }
                                                        placeholder="Select desired date"
                                                        required
                                                        disabled={ disableEdit }
                                                        selectedDay={ this.state.allowanceForm.release_date }
                                                        onChange={ ( value ) => {
                                                            const selectedDay = formatDate( value, DATE_FORMATS.API );

                                                            if ( selectedDay !== this.state.allowanceForm.release_date ) {
                                                                this.updateAllowanceFormState( 'release_date', selectedDay );
                                                            }
                                                        } }
                                                        ref={ ( ref ) => { this.release_date = ref; } }
                                                    />
                                                </div>
                                            ) }
                                        </div>
                                        <br />
                                        <div className="row">
                                            <div className="col-xs-6">
                                                <Input
                                                    id="amount"
                                                    label="Amount"
                                                    placeholder="Type desired amount"
                                                    type="number"
                                                    required
                                                    minNumber={ 1 }
                                                    value={ this.state.allowanceForm.amount }
                                                    onChange={ ( value ) => {
                                                        this.updateAllowanceFormState( 'amount', value );
                                                    } }
                                                    onBlur={ ( value ) => {
                                                        this.amount.setState({ value: decimal.round( value, 2 ) });
                                                        this.updateAllowanceFormState( 'amount', decimal.round( value, 2 ) );
                                                    } }
                                                    ref={ ( ref ) => { this.amount = ref; } }
                                                />
                                            </div>
                                            <div className="col-xs-6">
                                                <SalSelect
                                                    id="credit_frequency"
                                                    label="Crediting frequency"
                                                    placeholder="Select frequency"
                                                    required={ this.state.allowanceForm.recurring }
                                                    disabled={ !this.state.allowanceForm.recurring }
                                                    value={ this.state.allowanceForm.credit_frequency }
                                                    data={ CREDIT_FREQUENCY }
                                                    onChange={ ({ value }) => {
                                                        this.updateAllowanceFormState( 'credit_frequency', value );
                                                    } }
                                                    ref={ ( ref ) => { this.credit_frequency = ref; } }
                                                />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-4">
                                                <div className="switch">
                                                    Recurring
                                                </div>
                                                <Switch
                                                    disabled={ disburseThroughSpecialPayRun }
                                                    checked={ this.state.allowanceForm.recurring }
                                                    onChange={ ( value ) => {
                                                        this.setState({
                                                            allowanceForm: {
                                                                ...this.state.allowanceForm,
                                                                recurring: value,
                                                                credit_frequency: value ? this.state.allowanceForm.credit_frequency : null
                                                            }
                                                        });
                                                    } }
                                                />
                                            </div>
                                        </div>
                                        <br />
                                        <div className="row">
                                            <div className="col-xs-6 date-picker">
                                                <DatePicker
                                                    label="Valid from"
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    placeholder="Select desired date"
                                                    required={ !disburseThroughSpecialPayRun }
                                                    disabled={ disburseThroughSpecialPayRun }
                                                    selectedDay={ this.state.allowanceForm.valid_from }
                                                    onChange={ ( value ) => {
                                                        const selectedDay = formatDate( value, DATE_FORMATS.API );

                                                        if ( selectedDay !== this.state.allowanceForm.valid_from ) {
                                                            this.updateAllowanceFormState( 'valid_from', selectedDay );
                                                        }
                                                    } }
                                                    ref={ ( ref ) => { this.valid_from = ref; } }
                                                />
                                            </div>
                                            <div className="col-xs-6 date-picker">
                                                <DatePicker
                                                    label="Valid until"
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    placeholder="Select desired date"
                                                    required={ !( this.state.allowanceForm.never_expires || !this.state.allowanceForm.recurring ) }
                                                    disabled={ this.state.allowanceForm.never_expires || !this.state.allowanceForm.recurring }
                                                    selectedDay={ this.state.allowanceForm.valid_to }
                                                    onChange={ ( value ) => {
                                                        const selectedDay = formatDate( value, DATE_FORMATS.API );

                                                        if ( selectedDay !== this.state.allowanceForm.valid_to ) {
                                                            this.updateAllowanceFormState( 'valid_to', selectedDay );
                                                        }
                                                    } }
                                                    ref={ ( ref ) => { this.valid_to = ref; } }
                                                />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-4">
                                                <div className="switch">
                                                    Never expires
                                                </div>
                                                <Switch
                                                    disabled={ disburseThroughSpecialPayRun || !this.state.allowanceForm.recurring }
                                                    checked={ this.state.allowanceForm.never_expires }
                                                    onChange={ ( value ) => {
                                                        this.setState({
                                                            allowanceForm: {
                                                                ...this.state.allowanceForm,
                                                                never_expires: value,
                                                                valid_to: value ? null : this.state.allowanceForm.valid_to
                                                            }
                                                        });
                                                    } }
                                                />
                                            </div>
                                        </div>
                                        <br />
                                        <div className="row">
                                            <div className="col-xs-6">
                                                <div className="switch">
                                                    Prorated
                                                </div>
                                                <Switch
                                                    disabled={ disburseThroughSpecialPayRun || !this.state.allowanceForm.recurring }
                                                    checked={ this.state.allowanceForm.prorated }
                                                    onChange={ this.handleProratedChange }
                                                />
                                            </div>
                                            <div className="col-xs-6">
                                                <SalSelect
                                                    id="prorated_by"
                                                    label="Prorated by"
                                                    placeholder="Select desired option"
                                                    disabled={ !this.state.allowanceForm.prorated }
                                                    required={ this.state.allowanceForm.prorated }
                                                    value={ this.state.allowanceForm.prorated_by }
                                                    data={ PRORATED_BY }
                                                    onChange={ ({ value }) => {
                                                        this.updateAllowanceFormState( 'prorated_by', value );
                                                    } }
                                                    ref={ ( ref ) => { this.prorated_by = ref; } }
                                                />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-6" />
                                            <div className="col-xs-6">
                                                <div className="switch">
                                                    Prorated entitlements
                                                </div>
                                                <br />
                                                <div className="switch">
                                                    On paid leave
                                                </div>
                                                <Switch
                                                    disabled={ !this.state.allowanceForm.prorated }
                                                    checked={
                                                        this.state.allowanceForm.entitled_when === ENTITLED_WHEN.ON_PAID_LEAVE ||
                                                        this.state.allowanceForm.entitled_when === ENTITLED_WHEN.ON_PAID_AND_UNPAID_LEAVE
                                                    }
                                                    onChange={ ( value ) => {
                                                        const { entitled_when: entitledWhenCurrentValue } = this.state.allowanceForm;

                                                        const entitledWhenNewValue =
                                                            value
                                                                ? (
                                                                    entitledWhenCurrentValue === ENTITLED_WHEN.ON_UNPAID_LEAVE
                                                                        ? ENTITLED_WHEN.ON_PAID_AND_UNPAID_LEAVE
                                                                        : ENTITLED_WHEN.ON_PAID_LEAVE
                                                                )
                                                                : (
                                                                    entitledWhenCurrentValue === ENTITLED_WHEN.ON_PAID_AND_UNPAID_LEAVE
                                                                        ? ENTITLED_WHEN.ON_UNPAID_LEAVE
                                                                        : null
                                                                );

                                                        this.updateAllowanceFormState( 'entitled_when', entitledWhenNewValue );
                                                    } }
                                                />
                                            </div>
                                        </div>
                                        <br />
                                        <div className="row">
                                            <div className="col-xs-6" />
                                            <div className="col-xs-6">
                                                <div className="switch">
                                                    On unpaid leave
                                                </div>
                                                <Switch
                                                    disabled={ !this.state.allowanceForm.prorated }
                                                    checked={
                                                        this.state.allowanceForm.entitled_when === ENTITLED_WHEN.ON_UNPAID_LEAVE ||
                                                        this.state.allowanceForm.entitled_when === ENTITLED_WHEN.ON_PAID_AND_UNPAID_LEAVE
                                                    }
                                                    onChange={ ( value ) => {
                                                        const { entitled_when: entitledWhenCurrentValue } = this.state.allowanceForm;

                                                        const entitledWhenNewValue =
                                                            value
                                                                ? (
                                                                    entitledWhenCurrentValue === ENTITLED_WHEN.ON_PAID_LEAVE
                                                                        ? ENTITLED_WHEN.ON_PAID_AND_UNPAID_LEAVE
                                                                        : ENTITLED_WHEN.ON_UNPAID_LEAVE
                                                                )
                                                                : (
                                                                    entitledWhenCurrentValue === ENTITLED_WHEN.ON_PAID_AND_UNPAID_LEAVE
                                                                        ? ENTITLED_WHEN.ON_PAID_LEAVE
                                                                        : null
                                                                );

                                                        this.updateAllowanceFormState( 'entitled_when', entitledWhenNewValue );
                                                    } }
                                                />
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                    </Container>
                                    <div className="foot">
                                        <Button
                                            label="Cancel"
                                            type="neutral"
                                            size="large"
                                            onClick={ () => {
                                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/allowances' );
                                            } }
                                        />
                                        <Button
                                            label={ this.props.submitted ? <Loader /> : 'Submit' }
                                            type="action"
                                            size="large"
                                            onClick={ this.submitAllowance }
                                            ref={ ( ref ) => { this.submitButton = ref; } }
                                        />
                                    </div>
                                </div>
                            </MainWrapper>
                        </FormWrapper>
                    </div>
                ) }
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    formOptions: makeSelectFormOptions(),
    submitted: makeSelectSubmitted(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    allowance: makeSelectAllowance()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        editAllowanceActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Edit );
