export const PRORATED_BY = [
    { label: 'Prorated by Hour', value: 'PRORATED_BY_HOUR' },
    { label: 'Prorated by Day', value: 'PRORATED_BY_DAY' }
];

export const ENTITLED_WHEN = {
    ON_PAID_LEAVE: 'ON_PAID_LEAVE',
    ON_UNPAID_LEAVE: 'ON_UNPAID_LEAVE',
    ON_PAID_AND_UNPAID_LEAVE: 'ON_PAID_AND_UNPAID_LEAVE'
};

export const ENTITLED_WHEN_LABELS = [
    { label: 'On paid leave', value: 'ON_PAID_LEAVE' },
    { label: 'On unpaid leave', value: 'ON_UNPAID_LEAVE' },
    { label: 'On paid and unpaid leave', value: 'ON_PAID_AND_UNPAID_LEAVE' }
];

export const CREDIT_FREQUENCY = [
    { label: 'Daily', value: 'DAILY' },
    { label: 'Per Pay', value: 'PER_PAY' },
    { label: 'First Pay of the Month', value: 'FIRST_PAY_OF_THE_MONTH' },
    { label: 'Last Pay of the Month', value: 'LAST_PAY_OF_THE_MONTH' },
    { label: 'Per Month', value: 'PER_MONTH' }
];
