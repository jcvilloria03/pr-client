import {
    DEFAULT_ACTION,
    GET_IS_NAME_AVALABILITY
} from './constants';

/**
 *
 * isNameAvalibility actions
 *
 */
export function isNameAvalibility( payload ) {
    return {
        type: GET_IS_NAME_AVALABILITY,
        payload
    };
}

/**
 * defaultAction actions
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
