import { LOCATION_CHANGE } from 'react-router-redux';
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay } from 'redux-saga';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import {
    SET_COST_DATA,
    SET_IS_NAME_AVALABILITY,
    GET_IS_NAME_AVALABILITY,
    SET_ERRORS,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * isNameAvalibility saga
 */
export function* isNameAvalibility({ payload }) {
    try {
        yield [
            put({ type: SET_COST_DATA, payload: true })
        ];
        const companyId = company.getLastActiveCompanyId();
        const payloadValue = { ...payload, company_id: companyId };
        const response = yield call( Fetch, `/company/${companyId}/cost_center/is_name_available`,
            {
                method: 'POST', data: { name: payload.name }
            });

        if ( response.available === true ) {
            try {
                yield call( Fetch, '/cost_center',
                    {
                        method: 'POST', data: payloadValue
                    });
                yield put({
                    type: SET_COST_DATA,
                    payload: false
                });

                yield call( browserHistory.push, '/company-settings/company-structure/cost-centers', true );
            } catch ( error ) {
                if ( error.response && error.response.status === 406 ) {
                    yield call( setErrors, error.response.data.data );

                    yield put({
                        type: SET_COST_DATA,
                        payload: false
                    });
                } else {
                    yield call( notifyError, error );

                    yield put({
                        type: SET_COST_DATA,
                        payload: false
                    });
                }
            }
        } else {
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: 'Cost Center Name is taken',
                type: 'error'
            });
            yield put({
                type: SET_COST_DATA,
                payload: false
            });
        }
    } catch ( error ) {
        if ( error.response && error.response.status === 406 ) {
            yield call( setErrors, error.response.data.data );
        } else {
            yield call( notifyError, error );
        }
        yield put({
            type: SET_IS_NAME_AVALABILITY,
            payload: false
        });
    }
}

/**
 * changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });
}

/**
 * Individual exports for isName available testing
 */
export function* watchForIsNameAvailableForm() {
    const watcher = yield takeEvery( GET_IS_NAME_AVALABILITY, isNameAvalibility );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForIsNameAvailableForm,
    watchNotify
];
