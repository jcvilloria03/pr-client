/*
 *
 * Add constants
 *
 */

export const nameSpace = 'app/AddCostCenters';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const SET_COST_DATA = `${nameSpace}/SET_COST_DATA`;
export const GET_COST_DATA = `${nameSpace}/GET_COST_DATA`;
export const GET_IS_NAME_AVALABILITY = `${nameSpace}/GET_IS_NAME_AVALABILITY`;
export const SET_IS_NAME_AVALABILITY = `${nameSpace}/SET_IS_NAME_AVALABILITY`;
export const NOTIFICATION = `${nameSpace}/View/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
export const SET_ERRORS = `${nameSpace}/SET_ERRORS`;
