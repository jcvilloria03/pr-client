import { createSelector } from 'reselect';

/**
 * Direct selector to the add state domain
 */
const selectAddCostCentersDomain = () => ( state ) => state.get( 'addCostCenters' );

/**
 * Other specific selectors
 */
const makeSelectSubmitted = () => createSelector(
  selectAddCostCentersDomain(),
  ( substate ) => substate.get( 'submitted' )
);

const makeSelectIsNameAvailables = () => createSelector(
  selectAddCostCentersDomain(),
  ( substate ) => substate.get( 'isNameAvailable' )
);

const makeSelectNotification = () => createSelector(
  selectAddCostCentersDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectErrors = () => createSelector(
  selectAddCostCentersDomain(),
  ( substate ) => {
      let error;
      try {
          error = substate.get( 'errors' ).toJS();
      } catch ( err ) {
          error = substate.get( 'errors' );
      }

      return error;
  }
);

/**
 * Default selector used by Add
 */

const makeSelectAdd = () => createSelector(
  selectAddCostCentersDomain(),
  ( substate ) => substate.toJS()
);

export default makeSelectAdd;
export {
  selectAddCostCentersDomain,
  makeSelectSubmitted,
  makeSelectIsNameAvailables,
  makeSelectNotification,
  makeSelectErrors
};
