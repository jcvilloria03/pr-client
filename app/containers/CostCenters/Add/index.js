/* eslint-disable react/prop-types */
/* eslint-disable import/first */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';

import makeSelectAdd, { makeSelectSubmitted, makeSelectNotification } from './selectors';
import { makeSelectProducts } from '../View/selectors';
import * as addCostCreated from './actions';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';

import Sidebar from 'components/Sidebar';
import A from 'components/A';
import Input from 'components/Input';
import Button from 'components/Button';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import { H3 } from 'components/Typography';

import {
    PageWrapper,
    NavWrapper,
    FootWrapper
} from './styles';
/**
 *
 * Add
 *
 */
export class AddCostCenters extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        isNameAvalibility: React.PropTypes.func,
        submitted: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            name: '',
            description: '',
            notification: {
                title: '',
                message: '',
                show: false,
                type: 'error'
            }
        };
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.submitted !== this.props.submitted && this.submitButton.setState({ disabled: nextProps.submitted });

        nextProps.notification !== this.props.notification && this.setState({ notification: nextProps.notification });
    }

    /**
     *
     * Add render method
     *
     */
    submitForm() {
        this.props.isNameAvalibility({ name: this.state.name, description: this.state.description, nameTaken: false, company_id: '' });
    }

    render() {
        const { name, description } = this.state;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        return (
            <div>
                <Helmet
                    title="Add Cost Center"
                    meta={ [
                        { name: 'description', content: 'Description of Add' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ this.state.notification.message }
                    title={ this.state.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.state.notification.show }
                    delay={ 5000 }
                    type={ this.state.notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/company-settings/company-structure/cost-centers', true );
                            } }
                        >
                            &#8592; Back to Cost Centers
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="heading">
                                <H3>Create Cost Center</H3>
                                <div className="w-100">
                                    <div className="row">
                                        <div className="col-md-5">
                                            <p>* Cost Center Name</p>
                                            <Input
                                                id="name"
                                                name="name"
                                                value={ name || '' }
                                                onChange={ ( event ) => {
                                                    this.setState({ name: event });
                                                } }
                                                required
                                            />
                                        </div>
                                        <div className="col-md-5">
                                            <p>* Cost Center Description</p>
                                            <Input
                                                id="description"
                                                name="description"
                                                value={ description || '' }
                                                onChange={ ( event ) => {
                                                    this.setState({ description: event });
                                                } }
                                                required
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <FootWrapper>
                    <Button
                        label="Cancel"
                        type="neutral"
                        size="large"
                        onClick={ () => {
                            browserHistory.push( '/company-settings/company-structure/cost-centers', true );
                        } }
                    />
                    <Button
                        id="submitButton"
                        label={ this.props.submitted ? <Loader /> : 'Submit' }
                        ref={ ( ref ) => { this.submitButton = ref; } }
                        type="action"
                        size="large"
                        onClick={ () => this.submitForm() }
                    />
                </FootWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    AddCostCenters: makeSelectAdd(),
    products: makeSelectProducts(),
    submitted: makeSelectSubmitted(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        addCostCreated,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddCostCenters );
