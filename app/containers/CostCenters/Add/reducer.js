import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_COST_DATA,
    SET_IS_NAME_AVALABILITY,
    SET_ERRORS,
    NOTIFICATION_SAGA
} from './constants';

const initialState = fromJS({
    errors: {},
    submitted: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    isNameAvailibility: false
});

/**
 *
 * Add reducer
 *
 */
function addReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_COST_DATA:
            return state.set( 'submitted', action.payload );
        case SET_IS_NAME_AVALABILITY:
            return state.set( 'isNameAvailibility', action.payload );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default addReducer;
