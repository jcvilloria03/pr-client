/* eslint-disable no-unused-vars */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/prop-types */
/* eslint-disable import/first */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import clone from 'lodash/clone';

import makeSelectCostCenters, {
    makeSelectProducts,
    makeSelectListCostCenters,
    makeSelectLoading,
    makeSelectIsEditing,
    makeSelectNotification,
    makeSelectLength
} from './selectors';
import * as costCreatedActions from './actions';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';

import { H2, H3, H5 } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Loader from 'components/Loader';
import Input from 'components/Input';
import Icon from 'components/Icon';
import SalDropdown from 'components/SalDropdown';
import Table from 'components/Table';
import SnackBar from 'components/SnackBar';
import Modal from 'components/Modal';
import { isAnyRowSelected, getIdsOfSelectedRows } from 'components/Table/helpers';

import {
    PageWrapper,
    LoadingStyles
} from './styles';

/**
 *
 * CostCenters
 *
 */
export class CostCenters extends React.PureComponent {
    static propTypes = {
        loading: React.PropTypes.bool,
        CostCenterListView: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        getCostCreate: React.PropTypes.func,
        editCostCreated: React.PropTypes.func,
        deleteCostCreate: React.PropTypes.func,
        notify: React.PropTypes.func,
        datalength: React.PropTypes.number
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            delete_label: '',
            displayedData: [],
            notification: {
                title: '',
                message: '',
                show: false,
                type: 'error'
            },
            editableCell: false,
            edited: null
        };
    }
    /**
     *
     * CostCenters render method
     *
     */
    componentDidMount() {
        this.props.getCostCreate();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.notification !== this.props.notification && this.setState({ notification: nextProps.notification });

        nextProps.CostCenterListView !== this.props.CostCenterListView
            && this.setState({
                displayedData: nextProps.CostCenterListView,
                label: formatPaginationLabel( this.CostCreateFormsTable.tableComponent.state )
            });
    }

    isCurrentlyEdited( row ) {
        return this.state.edited && this.state.edited.id === row.id;
    }

    startEditingRow( row ) {
        this.setState({
            edited: clone( row )
        });
    }

    editedRowState( property, value ) {
        this.costName.setState({ property: value });
        this.costDescription.setState({ property: value });

        this.setState({
            edited: {
                ...this.state.edited,
                [ property ]: value
            }
        });
    }

    stopEditing = ( callback ) => {
        this.setState({
            edited: null
        }, callback );
        this.setState({ editableCell: false });
    }

    saveEdited() {
        this.setState({ editableCell: false });
        this.props.editCostCreated({
            id: this.state.edited.id,
            data: this.state.edited,
            costCenters: this.props.CostCenterListView
        });
    }

    getTableColumns() {
        return [
            {
                id: 'name',
                header: 'Cost Center Name',
                minWidth: 100,
                sortable: false,
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) && this.state.editableCell === true ? (
                        <Input
                            id="name"
                            type="text"
                            required
                            value={ this.state.edited.name }
                            ref={ ( ref ) => { this.costName = ref; } }
                            onChange={ ( value ) => {
                                this.editedRowState( 'name', value );
                            } }
                        />
                    ) : (
                        <div>{row.name}</div>
                    )
                )
            },
            {
                id: 'description',
                header: 'Cost Center Description',
                minWidth: 300,
                sortable: false,
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) && this.state.editableCell === true ? (
                        <div style={ { width: '100%' } }>
                            <Input
                                id="description"
                                type="text"
                                required
                                value={ this.state.edited.description }
                                ref={ ( ref ) => { this.costDescription = ref; } }
                                onChange={ ( value ) => {
                                    this.editedRowState( 'description', value );
                                } }
                            />
                        </div>
                    ) : (
                        <div>{row.description}</div>
                    )
                )
            },
            {
                header: ' ',
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) && this.state.editableCell === true ? (
                        <div>
                            <Button
                                label={ <span>Cancel</span> }
                                type="grey"
                                size="small"
                                onClick={ this.stopEditing }
                            />

                            <Button
                                label={ <span>Save</span> }
                                type="action"
                                size="small"
                                onClick={ () => this.saveEdited() }
                            />
                        </div>
                    ) : (
                        <Button
                            label={ ( !!this.state.edited && this.state.edited.id === row.id && this.props.isEditing ) ? <Loader /> : 'Edit' }
                            type="grey"
                            size="small"
                            onClick={ () => {
                                this.startEditingRow( row );
                                this.setState({ editableCell: !this.state.editableCell });
                            } }
                        />
                    )
                )
            }
        ];
    }

    /**
    * Handles delete of request to regenerate selected forms
    */
    SelectedForms = () => {
        const payload = getIdsOfSelectedRows( this.CostCreateFormsTable );
        this.props.deleteCostCreate({ cost_center_ids: payload, company_id: '' });
        this.chatModal.toggle();
        setTimeout( async () => {
            await this.props.getCostCreate();
        }, 2000 );
    }

    getDropdownItems() {
        return [
            {
                children: <div>Delete</div>,
                onClick: () => this.chatModal.toggle()
            }
        ];
    }

    handleTableChanges = ( tableProps = this.CostCreateFormsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            delete_label: formatDeleteLabel()
        });
    };

    handleSearch = ( term = '' ) => {
        let displayData = this.props.CostCenterListView;

        if ( term ) {
            const regex = new RegExp( term, 'i' );
            displayData = displayData.filter( ( row ) => ( regex.test( row.name ) ) );
        }

        this.setState({ displayedData: displayData }, () => {
            this.handleTableChanges();
        });
    }

    render() {
        const { loading } = this.props;
        const { displayedData, notification, label } = this.state;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        const hasSelectedItems = isAnyRowSelected( this.CostCreateFormsTable );
        return (
            <div>
                <Helmet
                    title="Cost Centers"
                    meta={ [
                        { name: 'description', content: 'Description of CostCenters' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Modal
                    title="Confirm Your Action"
                    ref={ ( ref ) => {
                        this.chatModal = ref;
                    } }
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.chatModal.toggle()
                        },
                        {
                            type: 'danger',
                            label: 'Yes',
                            onClick: () => this.SelectedForms()
                        }
                    ] }
                />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Cost Centers</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Cost Centers</H3>
                                <p>Add and define your cost centers here. A cost center is a part of an organization that does not produce direct profit and adds to the cost of running a company. Employees can be added to Cost Centers to determine the expenditure per Cost Center. <b>You must add at least one cost center to be able to set up your employees.</b></p>
                                <div style={ { textAlign: 'center' } } >
                                    <div className="main">
                                        <Button label="Create Cost Center" size="large" type="action" onClick={ () => { browserHistory.push( '/company-settings/company-structure/cost-centers/add', true ); } } />
                                    </div>
                                </div>
                            </div>

                            <div className="title">
                                <H5>Cost Center List</H5>
                                <div className="search-wrapper">
                                    <Input
                                        id="search"
                                        className="search"
                                        onChange={ ( value ) => this.handleSearch( value.toLowerCase() ) }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                    />
                                </div>
                                <span>
                                    <div>
                                        {hasSelectedItems ? this.state.delete_label : label}
                                        &nbsp;
                                    </div>
                                </span>
                                {hasSelectedItems && (
                                    <span className="sl-u-gap-left--sm">
                                        <SalDropdown dropdownItems={ this.getDropdownItems() } />
                                    </span>
                                )}
                            </div>
                            <div>
                                <Table
                                    data={ displayedData }
                                    columns={ this.getTableColumns() }
                                    pagination
                                    selectable
                                    onDataChange={ this.handleTableChanges }
                                    ref={ ( ref ) => { this.CostCreateFormsTable = ref; } }
                                    onSelectionChange={ ({ selected }) => {
                                        const selectionLength = selected.filter( ( row ) => row ).length;
                                        this.setState({ delete_label: formatDeleteLabel( selectionLength ) });
                                    } }
                                />
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    CostCenters: makeSelectCostCenters(),
    products: makeSelectProducts(),
    loading: makeSelectLoading(),
    CostCenterListView: makeSelectListCostCenters(),
    notification: makeSelectNotification(),
    datalength: makeSelectLength(),
    isEditing: makeSelectIsEditing()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        costCreatedActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( CostCenters );
