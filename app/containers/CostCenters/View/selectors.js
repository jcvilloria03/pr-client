import { createSelector } from 'reselect';

/**
 * Direct selector to the costCenters state domain
 */
const selectCostCentersDomain = () => ( state ) => state.get( 'costCenters' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
  selectCostCentersDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectListCostCenters = () => createSelector(
  selectCostCentersDomain(),
  ( schedules ) => schedules.get( 'costCreated' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectCostCentersDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLength = () => createSelector(
  selectCostCentersDomain(),
  ( substate ) => substate.get( 'length' )
);

const makeSelectIsEditing = () => createSelector(
  selectCostCentersDomain(),
( substate ) => substate.get( 'isEditing' )
);

const makeSelectEditCostCenters = () => createSelector(
  selectCostCentersDomain(),
  ( schedules ) => schedules.get( 'editCostCreats' )
);

const makeSelectDeleteCostCenters = () => createSelector(
  selectCostCentersDomain(),
  ( schedules ) => schedules.get( 'deleteCostCreats' )
);

const makeSelectCostCenters = () => createSelector(
  selectCostCentersDomain(),
  ( substate ) => substate.toJS()
);

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

export default makeSelectCostCenters;
export {
  selectCostCentersDomain,
  makeSelectProducts,
  makeSelectListCostCenters,
  makeSelectLoading,
  makeSelectNotification,
  makeSelectLength,
  makeSelectIsEditing,
  makeSelectEditCostCenters,
  makeSelectDeleteCostCenters
};
