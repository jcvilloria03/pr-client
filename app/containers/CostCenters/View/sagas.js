import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    GET_COST_CENTER,
    SET_COST_CENTER,
    SET_COST_CENTER_LENGTH,
    SET_COST_CENTER_LOADING,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    SET_EDITING,
    GET_EDIT_COST_CENTER,
    SET_EDIT_COST_CENTER,
    GET_DELETE_COST_CENTER,
    SET_DELETE_COST_CENTER
} from './constants';

// eslint-disable-next-line require-jsdoc
export function* getCostCreate() {
    try {
        yield put({
            type: SET_COST_CENTER_LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/cost_centers`, { method: 'Get' });

        yield put({
            type: SET_COST_CENTER,
            payload: response && response.data || []
        });
        yield put({
            type: SET_COST_CENTER_LENGTH,
            payload: response && response.data.length
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_COST_CENTER_LOADING,
            payload: false
        });
    }
}

/**
 *
 * @param {*object} payload
 */
export function* editCostCreate({ payload }) {
    const { id, data, costCenters } = payload;
    try {
        yield put({
            type: SET_EDITING,
            payload: true
        });

        const response = yield call( Fetch, `/cost_center/${id}`,
            {
                method: 'PUT',
                data
            });
        if ( response ) {
            yield put({
                type: SET_EDIT_COST_CENTER,
                payload: 'Change succefully save!'
            });
            yield put({
                type: SET_COST_CENTER,
                payload: costCenters.map( ( item ) => {
                    if ( item.id === response.id ) {
                        return { ...item, name: response.name, description: response.description };
                    }

                    return item;
                })
            });
            yield call( notifyUser, {
                show: true,
                title: 'Success',
                message: 'Change succefully save!',
                type: 'success'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EDITING,
            payload: false
        });
    }
}

/**
 *
 * @param {object} payload
 */
export function* deleteCostCreates({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();
        const payloadValue = { ...payload, company_id: companyId };
        yield call( Fetch, '/cost_center/bulk_delete', { method: 'DELETE', data: payloadValue });
        yield put({
            type: SET_DELETE_COST_CENTER,
            payload: 'Recored Succefully deleted!'
        });
        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Recored Succefully deleted!',
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: 'Error',
            message: 'Cost center name is in use',
            type: 'error'
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
}

/**
 * Display a notification to user
 */
export function* notifyFromClient({ payload }) {
    yield call( notifyUser, payload );
}

/**
 * Watcher for NOTIFICATION
 *
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyFromClient );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getCostCreate );
}

/**
 * Watcher for LOAD_INITIAL_DATA
 *
 */
export function* watchForLoadInitialData() {
    const watcher = yield takeEvery( GET_COST_CENTER, getCostCreate );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for edit cost create data
 *
 */
export function* watchForEditCostData() {
    const watcher = yield takeEvery( GET_EDIT_COST_CENTER, editCostCreate );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for delete cost data
 *
 */
export function* watchForDeletCostData() {
    const watcher = yield takeEvery( GET_DELETE_COST_CENTER, deleteCostCreates );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForReinitializePage,
    watchForLoadInitialData,
    watchForNotifyUser,
    watchForEditCostData,
    watchForDeletCostData
];
