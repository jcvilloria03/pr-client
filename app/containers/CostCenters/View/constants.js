/*
 *
 * CostCenters constants
 *
 */

export const namespace = 'app/CostCenters';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const GET_COST_CENTER = `${namespace}/GET_COST_CENTER`;
export const SET_COST_CENTER = `${namespace}/SET_COST_CENTER`;
export const SET_EDITING = `${namespace}/SET_EDITING`;
export const SET_COST_CENTER_LENGTH = `${namespace}/SET_COST_CENTER_LENGTH`;
export const SET_COST_CENTER_LOADING = `${namespace}/SET_COST_CENTER_LOADING`;
export const NOTIFICATION = `${namespace}/View/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const GET_EDIT_COST_CENTER = `${namespace}/GET_EDIT_COST_CENTER`;
export const SET_EDIT_COST_CENTER = `${namespace}/SET_EDIT_COST_CENTER`;
export const GET_DELETE_COST_CENTER = `${namespace}/GET_DELETE_COST_CENTER`;
export const SET_DELETE_COST_CENTER = `${namespace}/SET_DELETE_COST_CENTER`;
export const SET_LOADING = `${namespace}/SET_LOADING`;

