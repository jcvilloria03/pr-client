import {
    DEFAULT_ACTION,
    GET_COST_CENTER,
    NOTIFICATION,
    GET_EDIT_COST_CENTER,
    GET_DELETE_COST_CENTER
} from './constants';

/**
 *
 * CostCenters actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * Fetch list of loans
 */
export function getCostCreate() {
    return {
        type: GET_COST_CENTER
    };
}

/**
 * Edit data of cost
 */
export function editCostCreated( payload ) {
    return {
        type: GET_EDIT_COST_CENTER,
        payload
    };
}

/**
 * Delete Data of const
 */
export function deleteCostCreate( payload ) {
    return {
        type: GET_DELETE_COST_CENTER,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
