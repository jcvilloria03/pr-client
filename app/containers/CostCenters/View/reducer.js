import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_COST_CENTER,
    NOTIFICATION_SAGA,
    SET_EDITING,
    SET_COST_CENTER_LOADING,
    SET_COST_CENTER_LENGTH,
    SET_EDIT_COST_CENTER,
    SET_DELETE_COST_CENTER
} from './constants';

const initialState = fromJS({
    loading: true,
    isEditing: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    costCreated: [],
    length: 0,
    editCostCreats: '',
    deleteCostCreats: ''
});

/**
 *
 * CostCenters reducer
 *
 */
function costCentersReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_COST_CENTER_LOADING:
            return state.set( 'loading', action.payload );
        case SET_COST_CENTER:
            return state.set( 'costCreated', fromJS( action.payload ) );
        case SET_COST_CENTER_LENGTH:
            return state.set( 'length', action.payload );
        case SET_EDITING:
            return state.set( 'isEditing', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_EDIT_COST_CENTER:
            return state.set( 'editCostCreats', action.payload );
        case SET_DELETE_COST_CENTER:
            return state.set( 'deleteCostCreats', action.payload );
        default:
            return state;
    }
}

export default costCentersReducer;
