import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import Modal from 'components/Modal';
import Button from 'components/Button';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import { H2, H3 } from 'components/Typography';

import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';

import companyValidationSchema from '../utils/formValidation';

import BackNavigation from '../templates/BackNavigation';
import CompanyForm from '../templates/CompanyForm';

import * as addCompanyActions from './actions';

import {
  makeSelectLoading,
  makeSelectNotification,
  makeSelectCompanyTypes
} from './selectors';

import {
    ActionContainer,
    HeaderTitle,
    LoadingStyles,
    StyledContainer,
    SubTitle
} from './styles';

/**
 *
 * Add Company
 *
 */
export class AddCompany extends React.Component {
    static propTypes = {
        resetStore: React.PropTypes.func,
        getCompanyTypes: React.PropTypes.func,
        addCompany: React.PropTypes.func,
        products: React.PropTypes.array,
        companyTypes: React.PropTypes.array,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            error: {},
            formBody: {
                name: '',
                type: '',
                tin: '',
                rdo: '',
                sss: '',
                hdmf: '',
                philhealth: '',
                email: '',
                website: '',
                mobile_number: '',
                telephone_number: '',
                telephone_extension: '',
                fax_number: '',
                logo: null
            }
        };

        this.handleInputChange = this.handleInputChange.bind( this );
        this.cancelModal = null;
    }

    componentDidMount() {
        this.props.getCompanyTypes();
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    /* end of life cycle methos */
    setFile = ( file ) => {
        this.setState( ( prev ) => ({
            ...prev,
            formBody: {
                ...prev.formBody,
                logo: file
            }
        }) );
    };

    handleInputChange = ( name, val ) => {
        this.setState( ( prev ) => ({
            ...prev,
            formBody: {
                ...prev.formBody,
                [ name ]: val
            }
        }) );
    };

    handleSave = () => {
        companyValidationSchema( this.state.formBody )
            .then( ( validatedForm ) => {
                this.props.addCompany({ validatedForm });
            })
            .catch( ( err ) => {
                const _error = {};
                err.inner.forEach( ( e ) => {
                    _error[ e.path ] = e.message;
                });

                this.setState({ error: _error });
            });
    };

    /**
     *
     * AddCompany render method
     *
     */

    render() {
        const {
          error,
          formBody
        } = this.state;

        const {
          loading,
          products,
          notification,
          companyTypes
        } = this.props;

        return (
            <div>
                <Helmet
                    title="Add Company"
                    meta={ [
                        { name: 'description', content: 'Description of Companies' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <BackNavigation
                    title="Back To Manage Companies"
                    onClick={ () => browserHistory.push( '/control-panel/companies', true ) }
                />
                <StyledContainer>
                    <Sidebar
                        items={ getControlPanelSidebarLinks({
                            salpayViewPermission: true,
                            accountViewPermission: true,
                            isSubscribedToPayroll: products && subscriptionService.isSubscribedToPayroll( products )
                        }) }
                    />
                    { loading
                      ? (
                          <LoadingStyles>
                              <H2>Loading Add Company</H2>
                              <br />
                              <H3>Please wait...</H3>
                          </LoadingStyles>
                        )
                      : (
                          <div>
                              <HeaderTitle>Add a Company</HeaderTitle>
                              <SubTitle>
                                  Provide your new company&#39;s registered trade name, logo,
                                  and contact information.
                              </SubTitle>
                              <CompanyForm
                                  setFile={ this.setFile }
                                  handleInputChange={ this.handleInputChange }
                                  formBody={ formBody }
                                  error={ error }
                                  companyTypes={ companyTypes }
                              />
                              <div style={ { height: 100 } }>
                                  <ActionContainer>
                                      <Button
                                          label="Cancel"
                                          type="grey"
                                          onClick={ () => this.cancelModal.toggle() }
                                          disabled={ loading }
                                      />
                                      <Button
                                          label={ loading ? 'Submitting' : 'Submit' }
                                          type="action"
                                          onClick={ this.handleSave }
                                          disabled={ loading }
                                      />
                                  </ActionContainer>
                              </div>
                          </div>
                      )}
                </StyledContainer>

                {/* modal */}
                <Modal
                    ref={ ( ref ) => { this.cancelModal = ref; } }
                    title="Discard changes"
                    body={
                        <p>
                            Clicking Discard will undo all changes you made on
                            this page. Are you sure you want to proceed?
                        </p>
                    }
                    buttons={ [
                        {
                            label: 'Stay on this page',
                            type: 'grey',
                            onClick: () => this.cancelModal.close()
                        },
                        {
                            label: 'Discard',
                            type: 'danger',
                            onClick: () => browserHistory.push( '/control-panel/companies', true )
                        }
                    ] }
                />
                {/* end of modal */}
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    companyTypes: makeSelectCompanyTypes()

});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        addCompanyActions,
        dispatch
   );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddCompany );
