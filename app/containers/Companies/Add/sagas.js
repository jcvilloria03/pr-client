import { delay, takeEvery, takeLatest } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
import { resetStore } from '../../App/sagas';

import {
  setLoading,
  setNotification,
  setCompanyTypes
} from './actions';

import {
  ADD_COMPANY,
  RESET_STORE,
  GET_COMPANY_TYPES
} from './constants';

/**
 * handle initial form data
 */
export function* getCompanyTypes() {
    try {
        yield put( setLoading( true ) );

        const { data } = yield call( Fetch, '/philippine/company_types', { method: 'GET' });
        const formattedTypes = data.map( ( value ) => ({ value, label: value }) );

        yield put( setCompanyTypes( formattedTypes ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * handle update company by id
 */
export function* addCompany({ payload }) {
    const { validatedForm } = payload;
    const data = new FormData();
    const country = 'philippine';

    try {
        yield put( setLoading( true ) );

        // eslint-disable-next-line no-restricted-syntax
        for ( const key in validatedForm ) {
            if ( Object.hasOwnProperty.call( validatedForm, key ) ) {
                data.append( key, validatedForm[ key ]);
            }
        }

        yield call( Fetch, `/${country}/company`, {
            method: 'POST',
            data
        });

        yield call( browserHistory.push, '/control-panel/companies', true );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * watch for reset store
 */
export function* watchForResetStore() {
    yield takeEvery( RESET_STORE, resetStore );
}

/**
 * watch for fetching company types
 */
export function* watchForGetCompanyTypes() {
    yield takeEvery( GET_COMPANY_TYPES, getCompanyTypes );
}
/**
 * watch for add company
 */
export function* watchForAddCompany() {
    yield takeLatest( ADD_COMPANY, addCompany );
}

// All sagas to be loaded
export default [
    watchForResetStore,
    watchForAddCompany,
    watchForGetCompanyTypes
];
