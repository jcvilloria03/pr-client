/*
 *
 * Add Company constants
 *
 */

export const LOADING = 'app/Companies/Add/LOADING';
export const RESET_STORE = 'app/Companies/Add/RESET_STORE';
export const NOTIFICATION = 'app/Companies/Add/NOTIFICATION';

export const GET_COMPANY_TYPES = 'app/Companies/Add/GET_COMPANY_TYPES';

export const ADD_COMPANY = 'app/Companies/Add/ADD_COMPANY';

export const SET_LOADING = 'app/Companies/Add/SET_LOADING';
export const SET_NOTIFICATION = 'app/Companies/Add/SET_NOTIFICATION';
export const SET_COMPANY_TYPES = 'app/Companies/Add/SET_COMPANY_TYPES';

