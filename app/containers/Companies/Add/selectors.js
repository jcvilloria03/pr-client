import { createSelector } from 'reselect';

/**
 * Direct selector to the companies state domain
 */
const selectEditCompaniesDomain = () => ( state ) => state.get( 'addCompany' );

const makeSelectLoading = () => createSelector(
    selectEditCompaniesDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectCompanyTypes = () => createSelector(
    selectEditCompaniesDomain(),
  ( substate ) => substate.get( 'companyTypes' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectEditCompaniesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectCompanyTypes,
    makeSelectNotification
};
