import { createSelector } from 'reselect';

/**
 * Direct selector to the companies state domain
 */
const selectEditCompaniesDomain = () => ( state ) => state.get( 'editCompany' );

const makeSelectLoading = () => createSelector(
    selectEditCompaniesDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectCompanyDetails = () => createSelector(
    selectEditCompaniesDomain(),
  ( substate ) => substate.get( 'companyDetails' ).toJS()
);

const makeSelectCompanyTypes = () => createSelector(
    selectEditCompaniesDomain(),
  ( substate ) => substate.get( 'companyTypes' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectEditCompaniesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectCompanyTypes,
    makeSelectNotification,
    makeSelectCompanyDetails
};
