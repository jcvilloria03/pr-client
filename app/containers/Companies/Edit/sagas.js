import { delay, takeEvery, takeLatest } from 'redux-saga';
import { call, put } from 'redux-saga/effects';

import { Fetch } from 'utils/request';

import get from 'lodash/get';
import omit from 'lodash/omit';

import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import { resetStore, getCompanies } from '../../App/sagas';
import { setCurrentCompany } from '../../App/actions';

import {
  setLoading,
  setNotification,
  setCompanyTypes,
  setCompanyDetails

} from './actions';

import {
  RESET_STORE,
  UPDATE_COMPANY,
  GET_COMPANY_DETAILS
} from './constants';

/**
 * handle initial form data
 */
export function* getCompanyDetails({ payload }) {
    const { companyId } = payload;
    const country = 'philippine';

    try {
        yield put( setLoading( true ) );

        const [
            companyTypes,
            companyDetails
          ] = yield [
              call( Fetch, '/philippine/company_types', { method: 'GET' }),
              call( Fetch, `/${country}/company/${companyId}`, { method: 'GET' })
          ];
        const formattedTypes = companyTypes.data.map( ( data ) => ({ value: data, label: data }) );

        yield put( setCompanyTypes( formattedTypes ) );
        yield put( setCompanyDetails( companyDetails ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * handle update company by id
 */
export function* updateCompany({ payload }) {
    const companyId = company.getLastActiveCompanyId();
    const { id, validatedForm, originalForm } = payload;

    try {
        yield put( setLoading( true ) );
        const data = new FormData();
        const cloneData = omit( validatedForm, ['logo']);

        for ( const key of Object.keys( cloneData ) ) {
            if ( originalForm[ key ] !== validatedForm[ key ]) {
                data.append( key, validatedForm[ key ]);
            }
        }

        if ( validatedForm.logo && typeof validatedForm.logo !== 'string' ) {
            data.append( 'logo', validatedForm.logo );
        }

        const response = yield call( Fetch, `/company/${id}`, {
            method: 'POST',
            data
        });

        if ( response.id === companyId ) {
            yield put( setCurrentCompany( response ) );
        } else {
            yield call( getCompanies );
        }

        yield put( setCompanyDetails( response ) );
        yield call( browserHistory.push, '/control-panel/companies', true );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * watch for reset store
 */
export function* watchForResetStore() {
    yield takeEvery( RESET_STORE, resetStore );
}

/**
 * watch for fetching company details
 */
export function* watchForGetCompanyDetails() {
    yield takeEvery( GET_COMPANY_DETAILS, getCompanyDetails );
}
/**
 * watch for update company
 */
export function* watchForUpdateCompany() {
    yield takeLatest( UPDATE_COMPANY, updateCompany );
}

// All sagas to be loaded
export default [
    watchForResetStore,
    watchForUpdateCompany,
    watchForGetCompanyDetails
];
