/*
 *
 * Edit Company constants
 *
 */

export const LOADING = 'app/Companies/Edit/LOADING';
export const RESET_STORE = 'app/Companies/Edit/RESET_STORE';
export const NOTIFICATION = 'app/Companies/Edit/NOTIFICATION';

export const GET_COMPANY_DETAILS = 'app/Companies/Edit/GET_COMPANY_DETAILS';

export const UPDATE_COMPANY = 'app/Companies/Edit/UPDATE_COMPANY';

export const SET_LOADING = 'app/Companies/Edit/SET_LOADING';
export const SET_COMPANY_DETAILS = 'app/Companies/Edit/SET_COMPANY_DETAILS';
export const SET_NOTIFICATION = 'app/Companies/Edit/SET_NOTIFICATION';
export const SET_COMPANY_TYPES = 'app/Companies/Edit/SET_COMPANY_TYPES';

