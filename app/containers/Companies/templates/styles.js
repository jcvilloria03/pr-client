/* eslint-disable no-confusing-arrow */
import styled, { keyframes } from 'styled-components';
import { Container } from 'reactstrap';

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

export const Spinner = styled.div`
    animation: ${rotate360} 1s linear infinite;
    transform: translateZ(0);

    border-top: 2px solid grey;
    border-right: 2px solid grey;
    border-bottom: 2px solid grey;
    border-left: 4px solid black;
    background: transparent;
    width: 5rem;
    height: 5rem;
    border-radius: 50%;
`;

export const SpinnerContainer = styled.div`
    position: absolute;
    height: 100vh;
    background-color: rgba(0, 0, 0, 0.35);
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100vw;
    z-index: 999;
`;

export const StyledContainer = styled( Container )`
    padding: 80px 0 20px 20px;
`;
export const ActionContainer = styled( Container )`
    height: 100%;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    padding: 0;
`;

export const HeaderTitle = styled.h1`
    font-size: 24px;
    font-weight: bold;
    text-align: center;
`;
export const SubTitle = styled.p`
    font-size: 14px;
    text-align: center;
    margin-top: 0;
`;
export const SectionTitle = styled.p`
    font-size: 16px;
    text-align: left;
    margin-top: 36px;
    font-weight: 600;
`;

export const LoadingStyles = styled.div`
    display: ${( props ) => ( props.status === 'pending' ? 'flex' : 'none' )};
    flex-direction: column;
    align-items: center;
    min-height: 80vh;
    justify-content: center;
    padding: 140px 0;
`;

export const ComponentWrapper = styled.div`
    display: ${( props ) => ( props.status === 'pending' ? 'none' : 'block' )};
`;
