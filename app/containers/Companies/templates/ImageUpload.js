/* eslint-disable no-return-assign */
/* eslint-disable react/prop-types */
/* eslint-disable consistent-return */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable require-jsdoc */
import React from 'react';
import Dropzone from 'react-dropzone';
import Button from '../../../components/Button';

const FILE_SIZE_LIMIT = 2000000;
const ERROR_COLOR = '#eb7575';

function isFileValid( file ) {
    if ( !file ) return false;
    return (
        file.size < FILE_SIZE_LIMIT &&
        ( file.type === 'image/jpeg' || file.type === 'image/png' )
    );
}
export class ImageUpload extends React.Component {
    static propTypes = {
        file: React.PropTypes.any,
        setFile: React.PropTypes.func
        // label: React.PropTypes.string,
    };
    constructor( props ) {
        super( props );

        this.state = {
            currentFile: null,
            preview: null,
            error: null
        };

        this.dropZoneEl = null;
    }

    setImagePreview = ( props ) => {
        if ( props.file ) {
            if ( typeof props.file === 'string' ) {
                return this.setState({ preview: props.file });
            }
            this.setState({
                currentFile: props.file,
                preview: props.file.preview
            });
        }
    };

    componentDidMount() {
        this.setImagePreview( this.props );
    }

    componentWillReceiveProps( nextProps ) {
        this.setImagePreview( nextProps );
    }

    handleDrop = ( file ) => {
        if ( !isFileValid( file[ 0 ]) ) {
            return this.setState({
                error: 'Please upload image file only not exceeding 2MB in file size.'
            });
        }
        this.setState({
            preview: file[ 0 ].preview,
            currentFile: file[ 0 ],
            error: null
        });
        this.props.setFile( file[ 0 ]);
    };

    handleReplace = () => {
        this.dropZoneEl.fileInputEl.click();
        // this.setState({ preview: null, currentFile: null });
        this.props.setFile( null );
    };

    render() {
        const { preview } = this.state;

        return (
            <div>
                <p
                    style={ {
                        fontSize: 14,
                        marginBottom: 0,
                        color: this.state.error ? ERROR_COLOR : null
                    } }
                >
                    {this.props.label}
                </p>
                <div style={ { position: 'relative' } }>
                    {preview && (
                        <Button
                            label="Replace"
                            onClick={ this.handleReplace }
                            style={ { position: 'absolute', right: 8, top: 8 } }
                        />
                    )}
                    <Dropzone
                        id="drop-zone-component"
                        ref={ ( el ) => ( this.dropZoneEl = el ) }
                        onDrop={ this.handleDrop }
                        multiple={ false }
                        accept="image/*"
                        style={ {
                            width: '100%',
                            height: 200,
                            border: this.state.error
                                ? `1px dashed ${ERROR_COLOR}`
                                : '1px dashed gray',
                            borderRadius: 4
                        } }
                    ></Dropzone>

                    {preview ? (
                        <img
                            src={ preview }
                            alt="preview"
                            height="100"
                            style={ {
                                position: 'absolute',
                                top: '50%',
                                left: '50%',
                                transform: 'translate(-50%, -50%)'
                            } }
                        />
                    ) : (
                        <div
                            style={ {
                                position: 'absolute',
                                top: '50%',
                                left: '50%',
                                transform: 'translate(-50%, -50%)'
                            } }
                        >
                            <p
                                style={ {
                                    color: this.state.error
                                        ? ERROR_COLOR
                                        : null
                                } }
                            >
                                Maximum file size is 2MB. File types accepted
                                are: JPEG and PNG.
                            </p>
                            <Button
                                label="Choose File"
                                onClick={ this.handleReplace }
                            />
                        </div>
                    )}
                </div>
                <p style={ { color: ERROR_COLOR } }>
                    <small>{this.state.error}</small>
                </p>
            </div>
        );
    }
}

export default ImageUpload;
