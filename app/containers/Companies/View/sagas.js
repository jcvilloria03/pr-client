import { call, put } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { resetStore } from '../../App/sagas';

import {
  RESET_STORE,
  BULK_DELETE,
  GET_COMPANIES
} from './constants';

import {
  setLoading,
  setCompanies,
  setNotification
} from './actions';

/** validate if bulk delete is valid */
function* isBulkDeleteValid( numOfInUseCompanies ) {
    return new Promise( ( res, rej ) => {
        if ( numOfInUseCompanies <= 0 ) {
            return res( true );
        }
        const isMany = numOfInUseCompanies > 1;
        return rej(
            new Error(
                `There ${isMany ? 'are' : 'is'} ${numOfInUseCompanies} compan${
                    isMany ? 'ies' : 'y'
                } currently in use`
            )
        );
    });
}

/**
 * handles fetching of companies
 *
 *   */
export function* getCompanies() {
    const country = 'philippine';

    try {
        yield put( setLoading( true ) );
        const { data } = yield call( Fetch, `/account/${country}/companies`, { method: 'GET' });

        yield put( setCompanies( data ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * handles bulk delete of companies
 *
 *   */
export function* bulkDelete({ payload }) {
    const companyId = company.getLastActiveCompanyId();

    try {
        const response = yield call( Fetch, '/company/company/is_in_use', {
            method: 'POST',
            data: {
                company_id: companyId,
                ids: payload
            }
        });

        const valid = yield call( isBulkDeleteValid, response.in_use );
        if ( valid ) {
            yield call( Fetch, '/company/bulk_delete', {
                method: 'DELETE',
                data: {
                    company_ids: payload
                }
            });

            yield call( getCompanies );
        }
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * watch for fetching companies
 */
export function* watchForGetCompanies() {
    yield takeEvery( GET_COMPANIES, getCompanies );
}
/**
 * watch for bulk delete
 */
export function* watchForBulkDelete() {
    yield takeLatest( BULK_DELETE, bulkDelete );
}

/**
 * watch for reset store
 */
export function* watchForResetStore() {
    yield takeEvery( RESET_STORE, resetStore );
}

// All sagas to be loaded
export default [
    watchForResetStore,
    watchForBulkDelete,
    watchForGetCompanies
];
