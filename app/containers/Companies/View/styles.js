/* eslint-disable no-confusing-arrow */
import styled from 'styled-components';
import { Container } from 'reactstrap';

export const StyledContainer = styled( Container )`
    padding: 80px 0 20px 20px;
    margin-top: 40px;

    .heading {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 0 auto 50px auto;

        h3 {
            font-weight: 600;
        }

        p {
            text-align: center;
            max-width: 800px;
        }
    }

    .table-header {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;

        h5 {
          margin: 0;
          margin-right: 20px;
          font-weight: 600;
          font-size: 20px;
        }

        .table-header-info {
          display: flex;
          flex-direction: row;

          span {
            margin-right: 5px
          }

          .table-select {
            margin-bottom: 10px;
          }
        }

      }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;
