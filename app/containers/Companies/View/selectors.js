import { createSelector } from 'reselect';

/**
 * Direct selector to the companies state domain
 */
const selectCompaniesDomain = () => ( state ) => state.get( 'companies' );

const makeSelectLoading = () => createSelector(
    selectCompaniesDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectCompanies = () => createSelector(

    selectCompaniesDomain(),
  ( substate ) => substate.get( 'companies' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectCompaniesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectCompanies,
    makeSelectNotification
};
