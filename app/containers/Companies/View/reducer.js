import { fromJS } from 'immutable';
import {
  SET_LOADING,
  SET_COMPANIES,
  SET_NOTIFICATION
} from './constants';

const initialState = fromJS({
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    companies: []
});

/**
 *
 * Companies reducer
 *
 */
function companiesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_COMPANIES:
            return state.set( 'companies', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default companiesReducer;
