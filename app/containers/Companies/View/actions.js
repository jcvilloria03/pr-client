import {
  GET_COMPANIES,
  RESET_STORE,
  BULK_DELETE,
  SET_LOADING,
  SET_COMPANIES,
  SET_NOTIFICATION
} from './constants';

/**
 *
 * Companies actions
 *
 */

/**
 * Sends request to fetch companies
 */
export function getCompanies() {
    return {
        type: GET_COMPANIES
    };
}

/**
 * Sets companies table data
 */
export function setCompanies( payload ) {
    return {
        type: SET_COMPANIES,
        payload
    };
}

/**
 * Sets page loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object}
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets notification
 * @param {Object} payload - Notification config
 * @returns {Object}
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 *
 * Dashboard actions
 *
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}

/**
 * bulk Delete
 */
export function bulkDelete( payload ) {
    return {
        type: BULK_DELETE,
        payload
    };
}
