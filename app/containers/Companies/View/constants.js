/*
 *
 * Companies constants
 *
 */

export const LOADING = 'app/Companies/View/LOADING';
export const RESET_STORE = 'app/Companies/View/RESET_STORE';
export const NOTIFICATION = 'app/Companies/View/NOTIFICATION';
export const BULK_DELETE = 'app/Companies/View/BULK_DELETE';

export const GET_COMPANIES = 'app/Companies/View/GET_COMPANIES';

export const SET_LOADING = 'app/Companies/View/SET_LOADING';
export const SET_COMPANIES = 'app/Companies/View/SET_COMPANIES';
export const SET_NOTIFICATION = 'app/Companies/View/SET_NOTIFICATION';

