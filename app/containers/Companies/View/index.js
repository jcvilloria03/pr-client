/* eslint-disable camelcase */
import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import Modal from 'components/Modal';
import Table from 'components/Table';
import Button from 'components/Button';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import SalDropdown from 'components/SalDropdown';
import { H2, H3, H5 } from 'components/Typography';
import {
  isAnyRowSelected,
  getIdsOfSelectedRows
} from 'components/Table/helpers';

import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';

import * as companiesActions from './actions';

import {
    makeSelectLoading,
    makeSelectCompanies,
    makeSelectNotification
} from './selectors';

import {
  StyledContainer,
  LoadingStyles
} from './styles';
/**
 *
 * Companies
 *
 */
export class Companies extends React.Component {
    static propTypes = {
        resetStore: React.PropTypes.func,
        getCompanies: React.PropTypes.func,
        bulkDelete: React.PropTypes.func,
        companies: React.PropTypes.array,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    }

    constructor( props ) {
        super( props );

        this.state = {
            dropdownItems: [
                {
                    label: 'Delete',
                    children: <div>Delete</div>,
                    onClick: () => this.confirmDeleteModal.toggle()
                }
            ],
            label: 'Showing 0-0 of 0 entries',
            delete_label: ''
        };
    }

    componentDidMount() {
        this.props.getCompanies();
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    handleTableChanges = ( tableProps = this.companiesTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    };

    bulkDelete = () => {
        const selected = getIdsOfSelectedRows( this.companiesTable );

        this.props.bulkDelete( selected );
        this.confirmDeleteModal.close();
    }

    /**
     *
     * Companies render method
     *
     */
    render() {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Company Name',
                accessor: 'name',
                minWidth: 200
            },
            {
                header: 'Email Address',
                accessor: 'email',
                minWidth: 200
            },

            {
                header: 'Website',
                accessor: 'website',
                minWidth: 200
            },
            {
                header: 'Mobile Number',
                accessor: 'mobile_number',
                minWidth: 200
            },
            {
                header: 'Telephone Number',
                accessor: 'telephone_number',
                minWidth: 200
            },
            {
                header: 'Fax',
                accessor: 'fax_number',
                minWidth: 200
            },

            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => browserHistory.push( `/control-panel/companies/${row.id}/edit`, true ) }
                    />
                )
            }
        ];

        const {
          label,
          delete_label,
          dropdownItems
        } = this.state;

        const {
          loading,
          products,
          companies,
          notification
        } = this.props;

        return (
            <div>
                <Helmet
                    title="Companies"
                    meta={ [
                        { name: 'description', content: 'Description of Companies' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <StyledContainer>
                    <Sidebar
                        items={ getControlPanelSidebarLinks({
                            salpayViewPermission: true,
                            accountViewPermission: true,
                            isSubscribedToPayroll: products && subscriptionService.isSubscribedToPayroll( products )
                        }) }
                    />
                    { loading
                      ? (
                          <LoadingStyles>
                              <H2>Loading Companies</H2>
                              <br />
                              <H3>Please wait...</H3>
                          </LoadingStyles>
                        )
                      : (
                          <div>
                              <div className="heading">
                                  <H3>Companies</H3>
                                  <p>
                                  View and update your company records. You can add or
                                  delete a company as well as make changes to its name,
                                  logo, and contact information.
                                </p>
                              </div>
                              <div className="table-header mb-1">
                                  <H5>Companies List</H5>
                                  <div className="table-header-info">
                                      <div>
                                          { isAnyRowSelected( this.companiesTable )
                                            ? <span>{delete_label}</span>
                                            : (
                                                <div>
                                                    <span>{label} </span>
                                                    <Button
                                                        label="Add a Company"
                                                        type="action"
                                                        onClick={ () => browserHistory.push( '/control-panel/companies/add', true ) }
                                                    />
                                                </div>
                                              )
                                          }
                                      </div>
                                            &nbsp;
                                      { isAnyRowSelected( this.companiesTable ) && (
                                          <SalDropdown
                                              className="table-select"
                                              dropdownItems={ dropdownItems }
                                          />
                                      ) }
                                  </div>
                              </div>
                              <Table
                                  data={ companies }
                                  columns={ tableColumns }
                                  pagination
                                  selectable
                                  onDataChange={ this.handleTableChanges }
                                  ref={ ( ref ) => { this.companiesTable = ref; } }
                                  onSelectionChange={ ({ selected }) => {
                                      const selectionLength = selected.filter( ( row ) => row ).length;
                                      this.setState({ delete_label: formatDeleteLabel( selectionLength ) });
                                  } }
                              />
                          </div>
                        )
                      }
                </StyledContainer>

                {/* Delete Confimation modal */}
                <Modal
                    ref={ ( ref ) => {
                        this.confirmDeleteModal = ref;
                    } }
                    title="Confirm Your Action"
                    body={ <p>Proceed in removing the record?</p> }
                    buttons={ [
                        {
                            label: 'No',
                            type: 'grey',
                            onClick: () => this.confirmDeleteModal.close()
                        },
                        {
                            label: 'Yes',
                            type: 'danger',
                            onClick: this.bulkDelete
                        }
                    ] }
                />
                {/* end of modal */}
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    companies: makeSelectCompanies(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
      companiesActions,
      dispatch
    )
    ;
}
export default connect( mapStateToProps, mapDispatchToProps )( Companies );
