/* eslint-disable no-prototype-builtins */
/* eslint-disable no-inner-declarations */
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';

import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';

import {
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_BATCH_UPLOAD_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_ERRORS,
    SET_SAVING_STATUS,
    UPLOAD_ATTENDANCE
} from './constants';

/**
 * Uploads the CSV of Attendance Computation to add and starts the validation process
 */
export function* uploadAttendance({ payload }) {
    const { file, dates, employee_ids } = payload;
    try {
        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: {}
        });

        const data = new FormData();
        const companyId = company.getLastActiveCompanyId();

        data.append( 'file', file );
        data.append( 'company_id', companyId );

        const upload = yield call( Fetch, '/attendance/records/upload', {
            method: 'POST',
            data
        });

        if ( upload.data.job_id ) {
            yield put({
                type: SET_BATCH_UPLOAD_JOB_ID,
                payload: upload.data.job_id
            });

            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: 'validation_queued'
            });

            yield call( checkValidation, { payload: { jobId: upload.data.job_id, dates, employee_ids }});
        }
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const { data } = yield call( Fetch, `/attendance/job/${payload.jobId}`, {
            method: 'GET'
        });

        switch ( data.status ) {
            case 'FAILED':
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: data.status
                });
                yield call( getUploadErrors, {
                    jobId: payload.jobId
                });

                break;
            case 'FINISHED':
                yield put({
                    type: SET_SAVING_STATUS,
                    payload: 'success'
                });
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: 'success'
                });
                yield call( notifyUser, {
                    title: 'Success',
                    message: 'Upload successful',
                    show: true,
                    type: 'success'
                });
                yield call( regenerate, payload );

                break;
            default:
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: 'validating'
                });
                yield call( delay, 2000 );
                yield call( checkValidation, { payload });
        }
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Get errors of the uploaded data
 */
export function* getUploadErrors({ jobId }) {
    try {
        const { data } = yield call( Fetch, `/attendance/job/${jobId}/errors`, {
            method: 'GET'
        });

        const errors = {};

        data.forEach( ( error ) => {
            const { message } = error;
            try {
                const parsedMessage = JSON.parse( message );
                if ( parsedMessage && typeof parsedMessage === 'object' ) {
                    errors[ parsedMessage.row ] = parsedMessage.error;
                }
            } catch ( e ) {
                errors[ message.row ] = message;
            }
        });

        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: errors
        });
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}

/**
 * Regenerate attendance computation
 *
 * @returns {Promise}
 */
export function* regenerate( payload ) {
    const { dates, employee_ids } = payload;
    try {
        const companyId = company.getLastActiveCompanyId();

        const data = {
            company_id: companyId,
            filter: { employee_status: ['active']},
            employee_ids,
            search_terms: '',
            dates
        };
        let jobID = null;
        let employeeIds = null;

        /**
         * Get employee id using the field employee_id
         */
        function* getEmployeeIds() {
            const response = yield call( Fetch, `/company/${companyId}/employees_by_ids`, {
                method: 'POST',
                data: {
                    uids: employee_ids,
                    status: 'active,semi-active',
                    mode: 'TA_BASIC'
                }
            });

            if ( response.data ) {
                employeeIds = response.data.map( ( employee ) => employee.id );
                data.employee_ids = employeeIds;
            }
        }

        /**
         * calculate attendance
         */
        function* calculate() {
            const response = yield call( Fetch, '/attendance/calculate/bulk', {
                method: 'POST',
                data
            });

            if ( response.data ) {
                jobID = response.data.job_id;
                yield call( fetchJobStatus, response.data.job_id );
            } else {
                yield call( calculate, data );
            }
        }

        /**
         * Get the calculation status
         */
        function* fetchJobStatus( jobId ) {
            const response = yield call( Fetch, `/attendance/job/${jobId}`, { method: 'GET' });

            if ( response.data.status === 'FINISHED' ) {
                yield call( browserHistory.push, '/time/attendance-computations', true );
            } else if ( response.data.status === 'FAILED' ) {
                yield call( displayJobErrors, jobId );
            } else {
                yield call( delay, 5000 );
                yield call( fetchJobStatus, jobId );
            }

            return true;
        }

        /**
         * Get the job errors
         */
        function* displayJobErrors( jobId ) {
            const response = yield call( Fetch, `/attendance/job/${jobId}/errors`, { method: 'GET' });

            const uniqueErrors = new Set( response.data.map( ({ message }) => message ) );
            const errorMessages = Array.from( uniqueErrors );

            yield call( delay, 5000 );
            yield call( notifyUser, errorMessages );
        }

        if ( !employeeIds ) yield call( getEmployeeIds, data );
        if ( !jobID ) yield call( calculate, data );
    } catch ( error ) {
        yield call( notifyUser, toErrorPayload( error ) );
    }
}
/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * converts api error to notification payload
 */
function toErrorPayload( error ) {
    return {
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });

    const notification = Object.assign({}, payload, { show: true });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: notification
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });
}

/**
 * Watch for UPLOAD_ATTENDANCE
 */
export function* watchForUploadAttendance() {
    const watcher = yield takeEvery( UPLOAD_ATTENDANCE, uploadAttendance );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForUploadAttendance,
    watchNotify
];
