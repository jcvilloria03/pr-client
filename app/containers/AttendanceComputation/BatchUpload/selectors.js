import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectBatchAddAttendanceComputation = () => ( state ) => state.get( 'attendanceComputationBatchUpload' );

const makeSelectNotification = () => createSelector(
    selectBatchAddAttendanceComputation(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectBatchAddAttendanceComputation(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectBatchAddAttendanceComputation(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectBatchAddAttendanceComputation(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectSaving = () => createSelector(
    selectBatchAddAttendanceComputation(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectBatchAddAttendanceComputation(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectBatchUploadErrors,
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectSaving
};
