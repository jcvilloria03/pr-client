import { fromJS } from 'immutable';
import {
    SET_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    SET_SAVING_STATUS,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    errors: {},
    batchUploadJobId: null,
    batchUploadStatus: '',
    batchUploadErrors: {},
    saving: {
        status: '',
        errors: {}
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Manual or batch assign Annual Earnings
 *
 */
function attendanceComputationBatchUpload( state = initialState, action ) {
    switch ( action.type ) {
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_BATCH_UPLOAD_JOB_ID:
            return state.set( 'batchUploadJobId', action.payload );
        case SET_BATCH_UPLOAD_STATUS:
            return state.set( 'batchUploadStatus', action.payload );
        case SET_BATCH_UPLOAD_ERRORS:
            return state.set( 'batchUploadErrors', action.payload );
        case SET_SAVING_STATUS:
            return state.setIn([ 'saving', 'status' ], action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default attendanceComputationBatchUpload;
