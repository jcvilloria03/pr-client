/* eslint-disable no-plusplus */
import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from 'components/A';
import Table from 'components/Table';
import Button from 'components/Button';
import SnackBar from 'components/SnackBar';
import FileInput from 'components/FileInput';
import { H4, P } from 'components/Typography';
import FooterTablePagination from 'components/FooterTablePagination';

import { browserHistory } from 'utils/BrowserHistory';
import { formatPaginationLabel } from 'utils/functions';
import { subscriptionService } from 'utils/SubscriptionService';

import * as actions from './actions';

import {
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectSaving,
    makeSelectNotification
} from './selectors';

import {
    StyledLoader,
    PageWrapper,
    FormWrapper,
    NavWrapper,
    HeadingWrapper
} from './styles';

/**
 * Annual Earnings Batch Upload Component
 */
class BatchUpload extends React.Component {
    static propTypes = {
        uploadAttendance: React.PropTypes.func,
        batchUploadStatus: React.PropTypes.string,
        batchUploadErrors: React.PropTypes.oneOfType([
            React.PropTypes.object,
            React.PropTypes.array
        ]),
        saving: React.PropTypes.shape({
            status: React.PropTypes.string,
            errors: React.PropTypes.object
        }),
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            files: [],
            status: null,
            errors: {},
            clipboard: '',
            submit: false,
            label: 'Showing 0-0 of 0 entries',
            errorsTableLabel: 'Showing 0-0 of 0 entries',
            savingStatus: '',
            savingErrors: {},
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            }
        };

        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.batchUploadStatus !== this.props.batchUploadStatus && this.setState({
            status: nextProps.batchUploadStatus
        }, () => {
            this.validateButton && this.validateButton.setState({
                disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.batchUploadStatus )
            });
        });

        nextProps.batchUploadErrors !== this.props.batchUploadErrors && this.setState({
            errors: nextProps.batchUploadErrors,
            pagination: {
                total: Object.keys( nextProps.batchUploadErrors ).length,
                per_page: 10,
                current_page: 1,
                last_page: Math.ceil( Object.keys( nextProps.batchUploadErrors ).length / 10 ),
                to: 0
            }
        });

        nextProps.saving.status !== this.props.saving.status && this.setState({
            savingStatus: nextProps.saving.status
        }, () => {
            this.submitButton && this.submitButton.setState({ disabled: nextProps.saving.status !== 'save_failed' });
        });
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.errorsTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.errorsTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    handleTableChanges = ( tableProps = this.errorsTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: Object.keys( this.state.errors ).length });
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    processData( allText ) {
        const allTextLines = allText.split( /\r\n|\n/ );
        const headers = allTextLines[ 0 ].split( ',' );
        const IDList = [];
        const dateList = [];

      // Get CSV data
        for ( let i = 1; i < allTextLines.length; i++ ) {
            const data = allTextLines[ i ].split( ',' );
            if ( data.length === headers.length ) {
                for ( let j = 0; j < headers.length; j++ ) {
                    switch ( headers[ j ]) {
                        case 'Employee Id':
                            IDList.push( data[ j ]);
                            break;
                        case 'Timesheet Date':
                            dateList.push( data[ j ].replace(
                              /(\d\d)\/(\d\d)\/(\d{4})/,
                              '$3-$1-$2'
                            ) );
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        this.props.uploadAttendance({
            file: this.state.files,
            dates: [...new Set( dateList )],
            employee_ids: [...new Set( IDList )]
        });
    }

    renderErrorsSection = () => {
        const errorDisplay = [];
        const errorList = this.state.errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'Row Number',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'Error Type',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            Object.keys( errorList ).forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', marginLeft: '1rem', textAlign: 'center' } }>{ key === 'undefined' ? '' : `Row ${key}` }</H4>,
                    error: <ul><li key={ key } style={ { margin: '0' } } >{errorList[ key ]}</li></ul>
                });
            });

            return (
                <div className="errors">
                    <H4>Data field errors</H4>
                    <p>
                        There seems to be an error in the file you uploaded. Review the list below, correct the errors and upload the file again.
                    </p>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        ref={ ( ref ) => { this.errorsTable = ref; } }
                        onDataChange={ this.handleTableChanges }
                        page={ this.state.pagination.current_page - 1 }
                        pageSize={ this.state.pagination.per_page }
                        pages={ this.state.pagination.total }
                        external
                    />
                </div>
            );
        }

        return null;
    }

    render() {
        return (
            <PageWrapper>
                <Helmet
                    title="Batch Add Attendance Computation"
                    meta={ [
                            { name: 'description', content: 'Batch Add Attendance Computation' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/attendance-computations', true );
                            } }
                        >
                                &#8592; Back to Attendance Computation
                        </A>
                    </Container>
                </NavWrapper>
                <HeadingWrapper>
                    <h3>Batch Add Attendance</h3>
                    <p>To make it easier, you may upload employees&apos; attendance by batch. A template will be made available for you through this page.</p>
                </HeadingWrapper>
                <FormWrapper>
                    <Container className="sl-u-gap-bottom--xlg">
                        <p className="sl-u-gap-top--lg sl-u-gap-bottom--lg">
                                Follow the steps below for adding employee attendance via batch upload. A template will be available for you to fill out.
                            </p>

                        <div className="steps">
                            <div className="step">
                                <div className="template">
                                    <H4>Step 1:</H4>
                                    <P>Download and fill-out the Employee Attendance Template.</P>
                                    <A className="sl-c-btn--wide" href="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/add-attendance-template.csv" download>Download Template</A>
                                </div>
                            </div>
                            <div className="step">
                                <div className="upload">
                                    <H4>Step 2:</H4>
                                    <P>After completely filling out the template, choose and upload it here.</P>
                                    <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.savingStatus === 'success' ? 'none' : 'block' } } >
                                        <FileInput
                                            accept=".csv"
                                            onDrop={ ( files ) => {
                                                const { acceptedFiles } = files;

                                                this.setState({
                                                    files: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null,
                                                    errors: {},
                                                    status: null
                                                }, () => {
                                                    this.validateButton && this.validateButton.setState({ disabled: acceptedFiles.length <= 0 });
                                                });
                                            } }
                                            ref={ ( ref ) => { this.fileInput = ref; } }
                                        />
                                    </div>
                                    <div style={ { display: this.state.status === 'validated' ? 'block' : 'none' } }>
                                        <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                                    </div>
                                    <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'validated' || this.state.savingStatus === 'success' ? 'none' : 'block' } } >
                                        <Button
                                            label={
                                                    this.state.submit ? (
                                                        <StyledLoader className="animation">
                                                            Uploading <div className="anim3"></div>
                                                        </StyledLoader>
                                                    ) : (
                                                        'Upload'
                                                    )
                                                }
                                            disabled={ this.state.files ? this.state.files.length <= 0 : true }
                                            type="neutral"
                                            ref={ ( ref ) => { this.validateButton = ref; } }
                                            onClick={ () => {
                                                this.setState({
                                                    errors: {},
                                                    status: null,
                                                    submit: true
                                                }, () => {
                                                    this.validateButton && this.validateButton.setState({ disabled: true }, () => {
                                                        const file = this.state.files;
                                                        const reader = new FileReader();
                                                        reader.onload = () => {
                                                            this.processData( reader.result );
                                                        };
                                                        reader.readAsText( file );

                                                        setTimeout( () => {
                                                            this.validateButton.setState({ disabled: false });
                                                            this.setState({ submit: false });
                                                        }, 5000 );
                                                    });
                                                });
                                            } }
                                        />
                                    </div>
                                    <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'block' : 'none' } } >
                                        <StyledLoader className="animation">
                                            <H4 style={ { margin: '0' } }>{ this.state.status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                        </StyledLoader>
                                    </div>
                                    <div style={ { display: this.state.savingStatus === 'success' ? 'block' : 'none' } } >
                                        <StyledLoader className="animation">
                                            <H4 style={ { margin: '0' } }>Uploaded Successfully</H4>
                                        </StyledLoader>
                                    </div>
                                </div>
                            </div>
                        </div>
                        { this.renderErrorsSection() }
                    </Container>
                </FormWrapper>
                <div
                    className="loader"
                    style={ { display: Object.keys( this.state.errors ).length ? 'block' : 'none', position: 'fixed', bottom: '0', background: '#fff' } }
                >
                    <FooterTablePagination
                        page={ this.state.pagination.current_page }
                        pageSize={ this.state.pagination.per_page }
                        pagination={ this.state.pagination }
                        onPageChange={ this.onPageChange }
                        onPageSizeChange={ this.onPageSizeChange }
                        paginationLabel={ this.state.label }
                    />
                </div>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    batchUploadJobId: makeSelectBatchUploadJobId(),
    batchUploadStatus: makeSelectBatchUploadStatus(),
    batchUploadErrors: makeSelectBatchUploadErrors(),
    saving: makeSelectSaving(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( BatchUpload );
