import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_LOADING_TABLE,
    SET_PAGINATION,
    SET_NOTIFICATION,
    SET_ATTENDANCE,
    SET_SHIFTS,
    SET_LEAVES,
    SET_LEAVE_TYPES
} from './constants';

const initialState = fromJS({
    loading: false,
    loadingTable: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    attendance: [],
    shifts: [],
    leaves: [],
    leaveTypes: [],
    pagination: {
        item_count: 10,
        page: 1,
        total_pages: 0,
        total_records: 0
    }
});

/**
 *
 * Dashboard reducer
 *
 */
function attendanceComputation( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_LOADING_TABLE:
            return state.set( 'loadingTable', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_SHIFTS:
            return state.set( 'shifts', fromJS( action.payload ) );
        case SET_ATTENDANCE:
            return state.set( 'attendance', fromJS( action.payload ) );
        case SET_LEAVES:
            return state.set( 'leaves', fromJS( action.payload ) );
        case SET_LEAVE_TYPES:
            return state.set( 'leaveTypes', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default attendanceComputation;
