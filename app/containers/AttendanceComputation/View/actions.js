import {
    INITIALIZE,
    REGENERATE,
    RESET_STORE,
    DOWNLOAD,
    GET_SIDEPANEL_DATA,
    GET_ATTENDANCE,
    SET_LOADING,
    SET_LOADING_TABLE,
    SET_PAGINATION,
    SET_NOTIFICATION,
    SET_LEAVES,
    SET_LEAVE_TYPES,
    SET_ATTENDANCE,
    LOCK_ATTENDANCE,
    SET_SHIFTS,
    BULK_CREATE_DELETE_TIME_RECORDS,
BULK_CREATE_UPDATE_DELETE_LEAVES,
EDIT_COMPUTED_ATTENDANCE
} from './constants';

/**
 * initialize page
 */
export function initializePage() {
    return {
        type: INITIALIZE
    };
}

/**
 * initialize page
 */
export function regenerate( payload ) {
    return {
        type: REGENERATE,
        payload
    };
}

/**
 * download data
 */
export function download( payload ) {
    return {
        type: DOWNLOAD,
        payload
    };
}

/**
 * Sends request to fetch Attendance data
 * @param {Array} payload - pagination, table details
 *
 */
export function getAttendance( payload ) {
    return {
        type: GET_ATTENDANCE,
        payload
    };
}

/**
 * Sends request to fetch Employee sidepanel data
 * @param {Array} payload - leave details
 *
 */
export function getSidePanelData( payload ) {
    return {
        type: GET_SIDEPANEL_DATA,
        payload
    };
}
/**
 * Sets Leaves data
 * @param {Array} payload - leave details
 *
 */
export function setLeaves( payload ) {
    return {
        type: SET_LEAVES,
        payload
    };
}
/**
 * Sets Leave Types data
 * @param {Array} payload - leave types details
 *
 */
export function setLeaveTypes( payload ) {
    return {
        type: SET_LEAVE_TYPES,
        payload
    };
}

/**
 * Sets pagination
 * @param {Boolean} payload - pagination details
 * @returns {Object} action
 */
export function setPagination( payload ) {
    return {
        type: SET_PAGINATION,
        payload
    };
}

/**
 * Sets attendance data
 */
export function setAttendance( payload ) {
    return {
        type: SET_ATTENDANCE,
        payload
    };
}

/**
 * locks attendance data
 */
export function lockAttendance( payload ) {
    return {
        type: LOCK_ATTENDANCE,
        payload
    };
}

/**
 * Sets attendance data
 */
export function setShifts( payload ) {
    return {
        type: SET_SHIFTS,
        payload
    };
}
/**
 * Bulk Create/Delete Time Records
 */
export function bulkCreateDeleteTimeRecords( payload ) {
    return {
        type: BULK_CREATE_DELETE_TIME_RECORDS,
        payload
    };
}
/**
 * Bulk Create/Update/Delete Leaves
 */
export function bulkCreateUpdateDeleteLeaves( payload ) {
    return {
        type: BULK_CREATE_UPDATE_DELETE_LEAVES,
        payload
    };
}
/**
 * Edit Computed Attendance
 */
export function editComputedAttendance( payload ) {
    return {
        type: EDIT_COMPUTED_ATTENDANCE,
        payload
    };
}

/**
 * Sets page loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object}
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets page table loading status
 * @param {Boolean} payload - Loading table status
 * @returns {Object}
 */
export function setLoadingTable( payload ) {
    return {
        type: SET_LOADING_TABLE,
        payload
    };
}

/**
 * Sets notification
 * @param {Object} payload - Notification config
 * @returns {Object}
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 *
 * Dashboard actions
 *
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
