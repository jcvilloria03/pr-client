/*
 *
 * Dashboard constants
 *
 */

export const LOADING = 'app/AttendanceComputation/LOADING';
export const DOWNLOAD = 'app/AttendanceComputation/DOWNLOAD';
export const INITIALIZE = 'app/AttendanceComputation/INITIALIZE';
export const REGENERATE = 'app/AttendanceComputation/REGENERATE';
export const RESET_STORE = 'app/AttendanceComputation/RESET_STORE';
export const NOTIFICATION = 'app/AttendanceComputation/NOTIFICATION';

export const SET_LOADING = 'app/AttendanceComputation/SET_LOADING';
export const SET_LOADING_TABLE = 'app/AttendanceComputation/SET_LOADING_TABLE';
export const SET_PAGINATION = 'app/AttendanceComputation/SET_PAGINATION';
export const SET_NOTIFICATION = 'app/AttendanceComputation/SET_NOTIFICATION';
export const SET_ATTENDANCE = 'app/AttendanceComputation/SET_ATTENDANCE';
export const SET_SHIFTS = 'app/AttendanceComputation/SET_SHIFTS';
export const SET_LEAVES = 'app/AttendanceComputation/SET_LEAVES';
export const SET_LEAVE_TYPES = 'app/AttendanceComputation/SET_LEAVE_TYPES';

export const GET_ATTENDANCE = 'app/AttendanceComputation/GET_ATTENDANCE';
export const GET_SIDEPANEL_DATA = 'app/AttendanceComputation/GET_SIDEPANEL_DATA';

export const BULK_CREATE_DELETE_TIME_RECORDS = 'app/AttendanceComputation/BULK_CREATE_DELETE_TIME_RECORDS';
export const BULK_CREATE_UPDATE_DELETE_LEAVES = 'app/AttendanceComputation/BULK_CREATE_UPDATE_DELETE_LEAVES';
export const EDIT_COMPUTED_ATTENDANCE = 'app/AttendanceComputation/EDIT_COMPUTED_ATTENDANCE';
export const LOCK_ATTENDANCE = 'app/AttendanceComputation/LOCK_ATTENDANCE';

export const EMPLOYMENT_STATUS = [
    {
        value: 'active',
        label: 'Active',
        disabled: false
    },
    {
        value: 'semi-active',
        label: 'Semi-Active',
        disabled: false
    },
    {
        value: 'inactive',
        label: 'Inactive',
        disabled: false
    }
];

export const HOURS_WORKED_TYPES = {
    regular: 'Regular',
    night_shift: 'Night',
    overtime: 'Overtime',
    overtime_night_shift: 'Night, Overtime',
    undertime: 'Undertime'
};

export const DAY_HOUR_RATE_TYPES = [
    'Regular', 'Paid Leave', 'Unpaid Leave', 'OT', 'NT', 'NT+OT', 'Undertime',
    'UH', 'Tardy', 'Absent', 'RH', 'RH+NT', 'RH+OT', 'RH+NT+OT', 'SH', 'SH+NT',
    'SH+OT', 'SH+NT+OT', '2RH', '2RH+NT', '2RH+OT', '2RH+NT+OT', 'RD', 'RD+SH',
    'RD+RH', 'RD+2RH', 'RD+NT', 'RD+SH+NT', 'RD+RH+NT', 'RD+2RH+NT', 'RD+OT',
    'RD+SH+OT', 'RD+RH+OT', 'RD+2RH+OT', 'RD+NT+OT', 'RD+SH+NT+OT', 'RD+RH+NT+OT',
    'RD+2RH+NT+OT', 'OVERBREAK'
];
