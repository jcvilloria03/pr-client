/* eslint-disable camelcase */
import React from 'react';
import moment from 'moment';

import Button from 'components/Button';
import Select from 'components/Select';
import DatePicker from 'components/DatePicker';
import MultiSelect from 'components/MultiSelect';

import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';
import { formatDate } from 'utils/functions';
import { company } from 'utils/CompanyService';
import { DATE_FORMATS } from 'utils/constants';

import { EMPLOYMENT_STATUS } from '../../constants';
import { FilterWrapper } from './styles';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {
    static propTypes = {
        onFilter: React.PropTypes.func,
        toggleFilter: React.PropTypes.func
    };

    constructor( props ) {
        super( props );

        this.state = {
            account: null,
            companyId: null,
            status: EMPLOYMENT_STATUS[ 0 ].value,
            start_date: moment().subtract( 1, 'months' ).format( DATE_FORMATS.API ),
            end_date: moment().format( DATE_FORMATS.API ),
            employee_ids: [],
            department_ids: [],
            location_ids: []
        };
    }

    componentWillMount() {
        this.setState({
            account: auth.getUser(),
            companyId: company.getLastActiveCompanyId()
        });
    }

    onApply = () => {
        const { start_date, end_date, status, employee_ids, department_ids, location_ids } = this.state;
        this.props.toggleFilter();
        this.props.onFilter( 1, 10, {
            start_date,
            end_date,
            employee_ids,
            department_ids,
            location_ids,
            sort_by: { date: 'ASC', employee_name: 'ASC' },
            status: [status]
        });
    }

    getEmployees = ( keyword, callback ) => {
        const { companyId } = this.state;

        Fetch( `/company/${companyId}/ta_employees?q=1&response_size=minimal&keyword=${keyword}&page=1&per_page=10`, { method: 'GET' })
          .then( ( result ) => {
              const employees = result.data.map( ({ id, full_name }) => ({
                  value: id,
                  label: full_name,
                  disabled: false
              }) );
              callback( null, { options: employees });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    getDepartments = ( keyword, callback ) => {
        const { companyId } = this.state;

        Fetch( `/company/${companyId}/departments?mode=FILTER_MODE${keyword ? `&keyword=${keyword}` : ''}`, { method: 'GET' })
          .then( ( result ) => {
              const departments = result.data.map( ({ id, name }) => ({
                  value: id,
                  label: name,
                  disabled: false
              }) );
              callback( null, { options: departments });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    getLocations = ( keyword, callback ) => {
        const { companyId } = this.state;

        Fetch( `/company/${companyId}/time_attendance_locations?mode=FILTER_MODE&name=${keyword}`, { method: 'GET' })
          .then( ( result ) => {
              const locations = result.data.map( ({ id, name }) => ({
                  value: id,
                  label: name,
                  disabled: false
              }) );
              callback( null, { options: locations });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    resetFilters = () => {
        this.setState({
            status: EMPLOYMENT_STATUS[ 0 ].value,
            start_date: moment().subtract( 1, 'months' ).format( DATE_FORMATS.API ),
            end_date: moment().format( DATE_FORMATS.API ),
            employee_ids: [],
            department_ids: [],
            location_ids: []
        });

        this.employee.setState({ value: null });
        this.department.setState({ value: null });
        this.location.setState({ value: null });

        this.props.onFilter( 1, 10, {
            start_date: moment().subtract( 1, 'months' ).format( DATE_FORMATS.API ),
            end_date: moment().format( DATE_FORMATS.API ),
            employee_ids: [],
            sort_by: { date: 'ASC', employee_name: 'ASC' },
            status: ['active']
        });
    }

    render() {
        const { status, start_date, end_date } = this.state;
        return (
            <FilterWrapper>
                <div className="row">
                    <div className="col-xs-3">
                        <Select
                            id="employee-status"
                            label="Employee Status"
                            data={ EMPLOYMENT_STATUS }
                            value={ status }
                            onChange={ ({ value }) => {
                                this.setState({ status: value });
                            } }
                        />
                    </div>
                    <div className="col-xs-3 date">
                        <DatePicker
                            id="start-date"
                            label={
                                <span>Start Date</span>
                            }
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            selectedDay={ start_date }
                            onChange={ ( value ) => {
                                this.setState({ start_date: formatDate( value, DATE_FORMATS.API ) });
                            } }
                        />
                    </div>
                    <div className="col-xs-3 date">
                        <DatePicker
                            id="end-date"
                            label={
                                <span>End Date</span>
                            }
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            selectedDay={ end_date }
                            onChange={ ( value ) => {
                                this.setState({ end_date: formatDate( value, DATE_FORMATS.API ) });
                            } }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-3">
                        <MultiSelect
                            async
                            id="employees"
                            label="Employees"
                            placeholder="Search for employee"
                            loadOptions={ this.getEmployees }
                            multi={ false }
                            ref={ ( ref ) => { this.employee = ref; } }
                            onChange={
                                ({ value }) => {
                                    this.setState({ employee_ids: [value]});
                                }
                             }
                            autoload={ false }
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            async
                            id="departments"
                            label="Departments"
                            placeholder="Search for department"
                            loadOptions={ this.getDepartments }
                            multi={ false }
                            ref={ ( ref ) => { this.department = ref; } }
                            onChange={ ({ value }) => {
                                this.setState({ department_ids: [value]});
                            } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            async
                            id="locations"
                            label="Locations"
                            placeholder="Search for location"
                            loadOptions={ this.getLocations }
                            multi={ false }
                            ref={ ( ref ) => { this.location = ref; } }
                            onChange={ ({ value }) => {
                                this.setState({ location_ids: [value]});
                            } }
                        />
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ () => {
                                this.props.toggleFilter();
                            } }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
