import styled from 'styled-components';

import { P } from 'components/Typography';

export const LeavesWrapper = styled.div`
    border-radius: 0.5rem;
    padding: .5rem 1rem;
    background-color: #f0f4f6;

    * {
        font-size: 14px;
    }

    .title {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
        margin: 0.5rem 0;

        p {
            margin-bottom: 0;
        }

        .button-edit {
            cursor: pointer;
            margin-left: 1rem;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;

            &:focus {
                outline: 0;
            }

            span {
                color: black
            }

            .icon {
                width: 12px;
                font-size: 12px;
                color: #00A5E5;
                margin-right: 5px;

                > i {
                    align-self: center;
                }
            }
        }

        .button-remove {
            cursor: pointer;
            margin-left: 1rem;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;

            &:focus {
                outline: 0;
            }

            span {
                color: #f2130a;
                font-size: 14px;
            }

            .icon-remove {
                width: 12px;
                font-size: 12px;
                color: #f2130a;
                margin-right: 5px;

                > i {
                    align-self: center;
                }
            }
        }
    }

    hr {
        margin-top: 0;
        border-top: 3px solid rgba(255,255,255,.5);
    }

    .text-error {
        color: #f2130a;
        font-size: 14px;
        margin-top: .5rem;
        margin-bottom: 0;
        font-style: italic;
    }

    .record-body {
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        align-items: flex-start;

        .select-leave-wrapper {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 1rem;
            width: -webkit-fill-available;
            width: -moz-available;

            p {
                margin-bottom: 0;
                margin-right: 1.5rem;
            }

            .select-type {
                width: -webkit-fill-available;
                width: -moz-available;
            }

            .Select {
                width: 100%;
            }
        }

        .time-picker-wrapper {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 1rem;
            width: -webkit-fill-available;
            width: -moz-available;

            p {
                display: flex;
                flex-direction: row;
                justify-content: flex-start;
                align-items: center;
                margin-bottom: 0;

                .icon-clock {
                    font-size: 14px;
                    color: #d1d1d1;

                    > i {
                        align-self: center;
                    }
                }
            }

            .start_Time, .end_Time{
                border: 1px solid rgba(0,0,0,.15);
                border-color: #95989a;
                border-radius: 0;
                line-height: 28px;
                background-color: #fdfdfd;
                height: 46px;
                padding-left: 10px;
                margin-left: 1rem;
                width: 60%;
            }
        }

        .time-result-wrapper {
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;
            margin-bottom: 1rem;
            margin-top: 2rem;
            width: -webkit-fill-available;
            width: -moz-available;

            p {
                margin-bottom: 0;

                &:first-child {
                    margin-right: 3rem;
                }
            }
        }
    }
`;

export const ActionButton = styled.button`
    font-size: 14px;
`;

export const TimePickerLabel = styled( P )`
    display: flex;
    gap: 4px;
    align-items: center;
`;
