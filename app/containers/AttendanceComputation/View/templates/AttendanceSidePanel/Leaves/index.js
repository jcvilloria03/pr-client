/* eslint-disable no-plusplus */
/* eslint-disable react/sort-comp */
/* eslint-disable camelcase */
import React, { Component } from 'react';
import moment from 'moment';
import Flatpickr from 'react-flatpickr';
import each from 'lodash/each';
import sum from 'lodash/sum';
import map from 'lodash/map';
import isEmpty from 'lodash/isEmpty';

import 'flatpickr/dist/themes/material_green.css';

import Icon from 'components/Icon';
import Select from 'components/Select';
import { P } from 'components/Typography';

import { ActionButton, LeavesWrapper, TimePickerLabel } from './styles';
import { H6 } from '../styles';

/**
 *
 * Leaves Component
 *
 */
class Leaves extends Component {
    static propTypes = {
        locked: React.PropTypes.bool,
        record: React.PropTypes.object,
        expectedShifts: React.PropTypes.array,
        leaveTypes: React.PropTypes.array,
        recordCount: React.PropTypes.number,
        removeLeave: React.PropTypes.func,
        onEdit: React.PropTypes.func,
        onChangeLeave: React.PropTypes.func,
        readOnly: React.PropTypes.bool
    }

    static defaultProps = {
        leave: {},
        leaveTypes: []
    };

    constructor( props ) {
        super( props );

        this.state = {
            leaveOptions: [],
            timeDifference: 'N/A',
            start_time: '',
            end_time: '',
            error: null
        };
    }

    componentDidMount() {
        this.initializeData( this.props.leaveTypes, this.props.record );
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEmpty( nextProps.leaveTypes ) || !isEmpty( nextProps.record ) ) {
            this.initializeData( nextProps.leaveTypes, nextProps.record );
        }

        if ( this.state.timeDifference === 'N/A' ) {
            this.setState({
                start_time: nextProps.record.start_time,
                end_time: nextProps.record.end_time
            }, () => this.getTimeDifference() );
        }
    }

    initializeData = ( leaveTypes = [], record = {}) => {
        const leaveOptions = leaveTypes.map( ( leaveType ) => ({
            value: leaveType.id,
            label: leaveType.name,
            type: leaveType.payable ? 'paid_leave' : 'unpaid_leave'
        }) );

        if ( this.state.timeDifference === 'N/A' ) {
            this.setState({
                start_time: record.start_time,
                end_time: record.end_time
            }, () => this.getTimeDifference() );
        }

        this.setState({ leaveOptions });
    }

    getStartAndEndDateTime( startTime, endTime ) {
        const startDate = moment().format( 'YYYY-MM-DD' );
        let endDate = startDate;
        const start = moment( `${startDate} ${startTime}` );

        if ( startTime > endTime ) {
            endDate = moment( endDate ).add( 1, 'days' ).format( 'YYYY-MM-DD' );
        }
        const end = moment( `${endDate} ${endTime}` );

        return [ moment( start ), moment( end ) ];
    }

    isTimeWithinTimeInterval = ( datetime, interval ) => {
        const [ start, end ] = this.getStartAndEndDateTime( interval.start, interval.end );

        if ( start.isSame( end ) ) return false;

        if ( start.isBefore( end ) ) {
            if ( datetime.isBefore( start ) || datetime.isAfter( end ) ) return false;

            return true;
        }

        if ( datetime.isSameOrAfter( start ) || datetime.isSameOrBefore( end ) ) return true;

        return false;
    }

    /**
       * check Leaves
       */
    checkLeavesInsideOfShift = () => {
        const { start_time, end_time } = this.state;
        const { expectedShifts } = this.props;
        let isInInterval = false;

        for ( let j = 0; j < expectedShifts.length; j++ ) {
            const shiftInterval = {
                start: expectedShifts[ j ].schedule.start_time,
                end: expectedShifts[ j ].schedule.end_time
            };

            const [ start, end ] = this.getStartAndEndDateTime( start_time, end_time );
            if (
                this.isTimeWithinTimeInterval( start, shiftInterval ) &&
                this.isTimeWithinTimeInterval( end, shiftInterval )
            ) {
                isInInterval = true;
                break;
            }
        }

        if ( !isInInterval ) {
            this.setState({ error: 'Leave is outside of shift' });
        } else {
            this.setState({ error: null });
        }
    }

    getTimeDifference = () => {
        const { start_time, end_time } = this.state;
        const { expectedShifts } = this.props;
        let timeDifference = 'N/A';

        if ( start_time && end_time ) {
            const [ start, end ] = this.getStartAndEndDateTime( start_time, end_time );
            let diff = end.diff( start, 'hours', true );
            each( expectedShifts, ( shift ) => {
                const breakTotalHours = sum( map( shift.schedule.breaks, ( item ) => {
                    if ( item.is_paid ) return 0;

                    if ( item.type === 'flexi' ) {
                        return moment.duration( item.break_hours ).asMinutes();
                    }
                    return 0;
                }) );

                if ( diff < 0 ) diff = 24 + diff;

                diff = Math.round( diff * 100 ) / 100 - ( breakTotalHours / 60 );
                timeDifference = diff > 1 ? `${diff} Hours` : `${diff} Hour`;
            });
        }

        this.setState({ timeDifference });
    }

    onTimechange = ( value, key ) => {
        this.setState({ [ key ]: moment( value[ 0 ]).format( 'HH:mm' ) });
        this.getTimeDifference();
        this.checkLeavesInsideOfShift();
        this.props.onChangeLeave(
          this.props.record.id,
          key,
          moment( value[ 0 ]).format( 'HH:mm' )
        );
    }

    render() {
        const {
          onEdit,
          locked,
          record,
          readOnly,
          recordCount,
          removeLeave
        } = this.props;

        const {
          error,
          start_time,
          end_time,
          leaveOptions,
          timeDifference
        } = this.state;

        return (
            <LeavesWrapper>
                <div className="title">
                    <H6 noBottomMargin>Record {recordCount}</H6>
                    { readOnly
                      ? !locked && (
                      <ActionButton
                          onClick={ onEdit }
                          type="button"
                          className="button-edit"
                          disabled={ locked }
                      >
                          <Icon name="pencil" className="icon" />
                          <span>Edit</span>
                      </ActionButton>
                    ) : (
                        <ActionButton
                            onClick={ () => removeLeave( record.id ) }
                            type="button"
                            className="button-remove"
                        >
                            <Icon name="remove" className="icon-remove" />
                            <span>Remove</span>
                        </ActionButton>
                    ) }
                </div>
                { record.error || error &&
                <P className="text-error">{record.error || error}</P>
                }
                <hr />
                <div className="record-body">
                    <div className="select-leave-wrapper">
                        <P>Type:</P>
                        <div className="select-type">
                            <Select
                                id="type-picker"
                                required
                                value={ record.type.value }
                                disabled={ readOnly }
                                data={ leaveOptions }
                                onChange={ ( value ) => this.props.onChangeLeave( record.id, 'type', value ) }
                                placeholder="Select Leave Type"
                                ref={ ( ref ) => { this.year = ref; } }
                            />
                        </div>
                    </div>
                    <div className="time-picker-wrapper">
                        <TimePickerLabel>
                            <Icon name="time2" className="icon-clock" />

                            <span>Start Time: </span>
                        </TimePickerLabel>

                        <Flatpickr
                            id="startWork"
                            data-enable-time
                            className="start_Time"
                            value={ start_time || '00:00' }
                            disabled={ readOnly }
                            name="start"
                            options={ {
                                allowInput: true,
                                noCalendar: true,
                                enableTime: true,
                                dateFormat: 'H:i',
                                time_24hr: true
                            } }
                            onChange={ ( value ) => this.onTimechange( value, 'start_time' ) }
                        />
                    </div>
                    <div className="time-picker-wrapper">
                        <TimePickerLabel>
                            <Icon name="time2" className="icon-clock" />

                            <span>End Time:</span>
                        </TimePickerLabel>

                        <Flatpickr
                            id="endWork"
                            data-enable-time
                            className="end_Time"
                            value={ end_time || '00:00' }
                            disabled={ readOnly }
                            name="end"
                            options={ {
                                allowInput: true,
                                noCalendar: true,
                                enableTime: true,
                                dateFormat: 'H:i',
                                time_24hr: true
                            } }
                            onChange={ ( value, ) => this.onTimechange( value, 'end_time' ) }
                        />
                    </div>
                    <div className="time-result-wrapper">
                        <P>Total Hours:</P>
                        <P>{timeDifference}</P>
                    </div>
                </div>
            </LeavesWrapper>
        );
    }
}

export default Leaves;
