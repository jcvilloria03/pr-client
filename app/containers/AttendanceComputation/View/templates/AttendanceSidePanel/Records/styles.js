import styled from 'styled-components';

export const TimeRecordsWrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 8px;

    * {
        font-size: 14px;
    }
`;

export const TimeRecord = styled.div`
    border-radius: 0.5rem;
    padding: .5rem 1rem;
    background-color: #f0f4f6;

    .title {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 1rem;

        p {
            margin-bottom: 0;
        }

        .edit {
            cursor: pointer;

            .icon {
                color: #00A5E5;
                font-size: 15px;
                width: 15px;
            }

            span {
                color: black;
                font-size: 14px;
            }
        }

        .remove {
            cursor: pointer;
            margin-left: .5rem;

            span {
                color: #f2130a;
                font-size: 14px;
            }

            .icon-remove {
                width: 12px;
                font-size: 12px;
                color: #f2130a;
                margin-right: 5px;

                > i {
                    align-self: center;
                }
            }
        }
    }

    .records {
        width: 100%;

        .record-head {
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;

            .icon {
                font-size: 20px;
                color: #d1d1d1;
                width: 20px;
                margin-right: 1rem;
            }

            .react-tagsinput {
                width: 100%;
                border: 1px solid #95989a;

                .react-tagsinput-tag {
                    background-color: #f0f4f6;
                    border: 1px solid #95989a;
                    border-radius: 0.25rem;
                    padding: 0.25rem 0.5rem;
                    color: #373a3c;
                    font-size: 14px;
                    cursor: pointer;
                }
            }
        }

        hr {
            border-top: 3px solid rgba(255,255,255,.5);
        }

        .record-body {
            .date-picker-wrapper {
                display: flex;
                flex-direction: row;
                justify-content: flex-start;
                align-items: center;
                margin-bottom: 1rem;

                .icon-calendar {
                    font-size: 20px;
                    color: #d1d1d1;
                    margin-right: 1rem;
                    width: 20px;

                    > i {
                        align-self: center;
                    }
                }

                .DayPickerInput {
                    input {
                        width: 15.8rem;
                        padding-top: 0;
                    }
                }

                .error-message, .label {
                    display: none;
                }
            }

            .time-picker-wrapper {
                display: flex;
                flex-direction: row;
                justify-content: flex-start;
                align-items: center;
                margin-bottom: 1rem;

                .icon-clock {
                    font-size: 20px;
                    color: #d1d1d1;
                    width: 20px;
                    margin-right: 1rem;

                    > i {
                        align-self: center;
                    }
                }

                .time-select {
                    width: 100%;
                }

                .start_Time {
                    border: 1px solid rgba(0,0,0,.15);
                    border-color: #95989a;
                    border-radius: 0;
                    line-height: 28px;
                    background-color: #fdfdfd;
                    height: 46px;
                    padding-left: 10px;
                    margin-left: .5rem;
                    width: 30%;
                }
            }

            .max-clockout-wrapper {
                .icon {
                    font-size: 20px;
                    margin-right: 1rem;
                    width: 20px;

                    > i {
                        align-self: center;
                    }

                    svg {
                        padding: .75rem 0;
                        width: 20px;
                    }
                }
            }

            .show-details {
                display: flex;
                flex-direction: row;
                justify-content: flex-start;
                align-items: center;
                margin: 1rem 0;
                margin-bottom: 0;
                cursor: pointer;

                .icon {
                    margin-right: .5rem;
                    color: #00A5E5;
                    font-size: 20px;
                    width: 20px;
                }

                span {
                    color: #00A5E5;
                    font-size: 14px;
                }

                &:focus {
                    outline: 0;
                }

                &.rotate {
                    transform: translateX(0);
                }
            }
        }
    }
`;
