/* eslint-disable no-prototype-builtins */
/* eslint-disable array-callback-return */
/* eslint-disable react/sort-comp */
/* eslint-disable react/jsx-no-bind */
import React, { Component } from 'react';

import moment from 'moment';
import Geocode from 'react-geocode';
import Flatpickr from 'react-flatpickr';
import TagsInput from 'react-tagsinput';
import sortBy from 'lodash/sortBy';
import isEmpty from 'lodash/isEmpty';

import 'flatpickr/dist/themes/material_green.css';
import 'react-tagsinput/react-tagsinput.css';

import Icon from 'components/Icon';
import Select from 'components/Select';
import DatePicker from 'components/DatePicker';

import { DATE_FORMATS, PHT_UTC_OFFSET } from 'utils/constants';

import { TimeRecord, TimeRecordsWrapper } from './styles';

import Map from '../maps';

import { GMAPS_KEY } from '../../../../../../constants';

Geocode.setApiKey( GMAPS_KEY );

/**
 *
 * Records Component
 *
 */
class Records extends Component {
    static propTypes = {
        locked: React.PropTypes.bool,
        readOnly: React.PropTypes.bool,
        shift: React.PropTypes.object,
        selectedDay: React.PropTypes.string,
        timeRecords: React.PropTypes.array,
        onEdit: React.PropTypes.func,
        editRecord: React.PropTypes.func,
        removeRecord: React.PropTypes.func
    }

    static defaultProps = {
        readOnly: false
    };

    constructor( props ) {
        super( props );
        this.state = {
            timeOptions: [
              { id: 1, label: 'Clock In', value: 'clock_in' },
              { id: 2, label: 'Clock Out', value: 'clock_out' }
            ],
            timeRecordsPairing: [],
            isCollapsed: true
        };
    }

    componentDidMount() {
        this.handleTimeRecordsPairing( this.props.timeRecords );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.timeRecords !== this.props.timeRecords ) {
            this.handleTimeRecordsPairing( nextProps.timeRecords );
        }
    }

    handleTimeRecordsPairing = ( data ) => {
        let previousIn = false;
        let previousOut = false;
        let workingPair = null;
        const timePairs = [];

        sortBy( data, 'timestamp' ).forEach( ( record ) => {
            const timestamp = this.timestampFormatting( record.timestamp );
            const timeRecord = Object.assign({}, record, { timestamp });

            if ( record.state ) {
                if ( previousIn ) {
                    timePairs.push( workingPair );
                }

                workingPair = { clockIn: timeRecord };
            } else {
                if ( previousIn ) {
                    workingPair = Object.assign({}, workingPair, { clockOut: timeRecord });
                } else if ( previousOut || workingPair === null ) {
                    workingPair = { clockOut: timeRecord };
                }

                timePairs.push( workingPair );
                workingPair = null;
            }

            previousIn = record.state;
            previousOut = !record.state;
        });

        this.setState({ timeRecordsPairing: workingPair !== null ? timePairs.concat( workingPair ) : timePairs });
    }

    timestampFormatting = ( timestamp ) => moment( moment( timestamp, 'X' ).format( 'YYYY-MM-DD HH:mm:ss' ) ).format( 'X' )

    getRecordProperties = ( record ) => {
        const dateFormat = moment( record.timestamp, 'X' ).utcOffset( PHT_UTC_OFFSET ).format( DATE_FORMATS.DISPLAY );
        const timeFormat = moment( record.timestamp, 'X' ).utcOffset( PHT_UTC_OFFSET ).format( 'HH:mm:ss' );
        const hasMaxClockoutProperties = record.hasOwnProperty( 'gps' )
            && record.hasOwnProperty( 'ip' )
            && record.hasOwnProperty( 'origin' );

        const hasMaxClockoutSettings = hasMaxClockoutProperties
            && record.gps !== 'None'
            && record.ip !== 'None'
            && record.gps !== 'None';

        const coordinates = hasMaxClockoutProperties && record.gps !== 'None'
            ? { lat: record.gps.split( '|' )[ 0 ],
                lng: record.gps.split( '|' )[ 1 ] }
            : {};

        let address = '';
        hasMaxClockoutProperties
        && record.gps !== 'None'
        && Geocode.fromLatLng( coordinates.lat, coordinates.lng ).then(
          ( response ) => { address = response.results[ 0 ].formatted_address; }
        );

        return {
            dateFormat,
            timeFormat,
            hasMaxClockoutProperties,
            hasMaxClockoutSettings,
            coordinates,
            address
        };
    }

    render() {
        const {
          onEdit,
          locked,
          readOnly,
          shift,
          editRecord,
          selectedDay,
          timeRecords,
          removeRecord
        } = this.props;

        const {
          isCollapsed,
          timeOptions,
          timeRecordsPairing
        } = this.state;

        const onDisplayRecords = timeRecordsPairing.map( ( record, i ) => {
            const clockInProps = record.hasOwnProperty( 'clockIn' ) &&
                this.getRecordProperties( record.clockIn );
            const clockOutProps = record.hasOwnProperty( 'clockOut' ) &&
                this.getRecordProperties( record.clockOut );

            return (
                <TimeRecord key={ i }>
                    { Boolean( clockInProps ) && (
                        <div>
                            <div className="title">
                                <p>Record {i + 1}</p>

                                { !locked && (
                                    <button
                                        className="button-icon edit"
                                        type="button"
                                        onClick={ () => onEdit() }
                                        disabled={ locked }
                                    >
                                        <Icon name="pencil" className="icon" />

                                        <span>Edit</span>
                                    </button>
                                ) }
                            </div>

                            <div className="records">
                                <div className="record-head">
                                    <Icon name="tag" className="icon" />

                                    { !isEmpty( record.clockIn.tags )
                                        ? record.clockIn.tags.reduce( ( accumulator, currentValue ) => `${accumulator}, ${currentValue}` )
                                        : 'None'
                                    }
                                </div>

                                <hr />

                                <div className="record-body">
                                    <div className="date-picker-wrapper">
                                        <Icon name="calendarO" className="icon-calendar" />

                                        <span>{clockInProps.dateFormat}</span>
                                    </div>

                                    <div className="time-picker-wrapper">
                                        <Icon name="time2" className="icon-clock" />

                                        <span>{record.clockIn.state ? 'Clock In:' : 'Clock out:'}</span>

                                        <span className="ml-1">{clockInProps.timeFormat}</span>
                                    </div>

                                    <div className="max-clockout-wrapper">
                                        <Icon name="fingerprint" className="icon" />

                                        <span>{record.clockIn.origin || 'None'}</span>
                                    </div>

                                    { clockInProps.hasMaxClockoutSettings && !isCollapsed && (
                                        <div className="max-clockout-wrapper">
                                            <div>
                                                <Icon name="worldGrid" className="icon" />
                                                <span>{clockInProps.address}</span>
                                            </div>

                                            <div>
                                                <Icon name="ipAddress" className="icon" />
                                                <span>{record.clockIn.ip}</span>
                                            </div>

                                            <Map
                                                coordinates={ clockInProps.coordinates }
                                            />
                                        </div>
                                    ) }

                                    { clockInProps.hasMaxClockoutSettings && (
                                        <button
                                            onClick={ () => this.setState({ isCollapsed: !isCollapsed }) }
                                            type="button"
                                            className="show-details"
                                        >
                                            <Icon
                                                name="caretCircle"
                                                className="icon"
                                                style={ { transform: !isCollapsed ? '' : 'rotate(180deg)' } }
                                            />

                                            <span>Show more</span>
                                        </button>
                                    ) }
                                </div>
                            </div>
                        </div>
                    ) }

                    { Boolean( clockOutProps ) && (
                        <div>
                            {clockInProps && (
                                <hr style={ { margin: '2rem 0', border: '1px solid  #95989a' } } />
                            ) }

                            { !clockInProps && (
                                <div className="title">
                                    <p>Record {i + 1}</p>
                                    <button
                                        className="button-icon edit"
                                        type="button"
                                        onClick={ () => onEdit() }
                                    >
                                        <Icon name="pencil" className="icon" />
                                        <span>Edit</span>
                                    </button>
                                </div>
                            ) }

                            <div className="records">
                                <div className="record-head">
                                    <Icon name="tag" className="icon" />

                                    { !isEmpty( record.clockOut.tags )
                                        ? record.clockOut.tags.reduce( ( accumulator, currentValue ) => `${accumulator}, ${currentValue}` )
                                        : 'None'
                                    }
                                </div>

                                <hr />

                                <div className="record-body">
                                    <div className="date-picker-wrapper">
                                        <Icon name="calendarO" className="icon-calendar" />

                                        <span>{clockOutProps.dateFormat}</span>
                                    </div>

                                    <div className="time-picker-wrapper">
                                        <Icon name="time2" className="icon-clock" />

                                        <span>{record.clockOut.state ? 'Clock In:' : 'Clock out:'}</span>

                                        <span className="ml-1">{clockOutProps.timeFormat}</span>
                                    </div>

                                    <div className="max-clockout-wrapper">
                                        <Icon name="fingerprint" className="icon" />

                                        <span>{record.clockOut.origin || 'None'}</span>
                                    </div>

                                    { clockOutProps.hasMaxClockoutSettings && !isCollapsed && (
                                        <div className="max-clockout-wrapper">
                                            <div>
                                                <Icon name="worldGrid" className="icon" />

                                                <span>{clockOutProps.address}</span>
                                            </div>

                                            <div>
                                                <Icon name="ipAddress" className="icon" />

                                                <span>{record.clockOut.ip}</span>
                                            </div>

                                            <Map coordinates={ clockOutProps.coordinates } />
                                        </div>
                                    ) }

                                    { clockOutProps.hasMaxClockoutSettings && (
                                        <button
                                            onClick={ () => this.setState({ isCollapsed: !isCollapsed }) }
                                            type="button"
                                            className="show-details"
                                        >
                                            <Icon
                                                name="caretCircle"
                                                className="icon"
                                                style={ { transform: !isCollapsed ? '' : 'rotate(180deg)' } }
                                            />

                                            <span>Show more</span>
                                        </button>
                                    ) }
                                </div>
                            </div>
                        </div>
                    ) }
                </TimeRecord>
            );
        });

        const onEditRecords = timeRecords.map( ( record, i ) => {
            const properties = this.getRecordProperties( record );

            return (
                <TimeRecord key={ i }>
                    <div className="title">
                        <p>Record {i + 1}</p>

                        <button
                            onClick={ () => removeRecord( record.id, shift.id ) }
                            type="button"
                            className="button-icon remove"
                        >
                            <Icon name="remove" className="icon-remove" />

                            <span>Remove</span>
                        </button>
                    </div>

                    <div className="records">
                        <div className="record-head">
                            <Icon name="tag" className="icon" />

                            <TagsInput
                                value={ record.tags }
                                onChange={ ( value ) => editRecord(
                                    record.id,
                                    shift.id,
                                    'tags',
                                    value
                                ) }
                            />
                        </div>

                        <hr />

                        <div className="record-body">
                            <div className="date-picker-wrapper">
                                <Icon name="calendarO" className="icon-calendar" />

                                <DatePicker
                                    label={ '' }
                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                    placeholder="Select desired date"
                                    selectedDay={ properties.dateFormat }
                                    disabledDays={ {
                                        before: new Date( moment( selectedDay ) ),
                                        after: new Date( moment( selectedDay ).add( 1, 'days' ) )
                                    } }
                                    onChange={ ( value ) => editRecord(
                                        record.id,
                                        shift.id,
                                        'timestamp',
                                        moment(
                                            `${moment( value ).format( 'YYYY-MM-DD' )} ${properties.timeFormat}`,
                                            'YYYY-MM-DD HH:mm'
                                        ).format( 'X' )
                                    ) }
                                    ref={ ( ref ) => { this.release_date = ref; } }
                                />
                            </div>

                            <div className="time-picker-wrapper">
                                <Icon name="time2" className="icon-clock" />

                                <div className="time-select">
                                    <Select
                                        id="time-picker"
                                        value={ record.type }
                                        data={ timeOptions }
                                        placeholder="Select Time Type"
                                        ref={ ( ref ) => { this.year = ref; } }
                                        onChange={ ( item ) => editRecord(
                                            record.id,
                                            shift.id,
                                            'type',
                                            item.value
                                        ) }
                                    />
                                </div>

                                <Flatpickr
                                    id="startWork"
                                    data-enable-time
                                    className="start_Time"
                                    name="start"
                                    ref={ ( ref ) => { this.workStart = ref; } }
                                    options={ {
                                        allowInput: true,
                                        noCalendar: true,
                                        enableTime: true,
                                        dateFormat: 'H:i',
                                        time_24hr: true,
                                        defaultDate: properties.timeFormat
                                    } }
                                    onChange={ ( value ) => editRecord(
                                        record.id,
                                        shift.id,
                                        'timestamp',
                                        moment(
                                            `${properties.dateFormat} ${moment( value[ 0 ]).format( 'HH:mm' )}${PHT_UTC_OFFSET}`
                                        ).format( 'X' )
                                    ) }
                                />
                            </div>

                            { properties.hasMaxClockoutSettings && !isCollapsed && (
                                <div className="max-clockout-wrapper">
                                    <div>
                                        <Icon name="fingerprint" className="icon" />

                                        <span>{record.origin}</span>
                                    </div>

                                    <div>
                                        <Icon name="worldGrid" className="icon" />

                                        <span>{properties.address}</span>
                                    </div>

                                    <div>
                                        <Icon name="ipAddress" className="icon" />

                                        <span>{record.ip}</span>
                                    </div>

                                    <Map coordinates={ properties.coordinates } />
                                </div>
                            ) }

                            { properties.hasMaxClockoutSettings && (
                                <button
                                    onClick={ () => this.setState({ isCollapsed: !isCollapsed }) }
                                    type="button"
                                    className="show-details"
                                >
                                    <Icon
                                        name="caretCircle"
                                        className="icon"
                                        style={ { transform: !isCollapsed ? '' : 'rotate(180deg)' } }
                                    />

                                    <span>Show more</span>
                                </button>
                            ) }
                        </div>
                    </div>
                </TimeRecord>
            );
        });

        return (
            <TimeRecordsWrapper>
                { !readOnly
                  ? onEditRecords
                  : onDisplayRecords
                }
            </TimeRecordsWrapper>
        );
    }
}

export default Records;
