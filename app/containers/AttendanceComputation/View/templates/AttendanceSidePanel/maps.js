/* eslint-disable import/first */
import React from 'react';
const { compose, withProps, withHandlers } = require( 'recompose' );
import isEmpty from 'lodash/isEmpty';

const {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} = require( 'react-google-maps' );

import { GMAPS_KEY } from '../../../../../constants';

const Map = compose(
  withProps({
      googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${GMAPS_KEY}&v=3.exp&libraries=geometry,drawing,places`,
      loadingElement: <div style={ { height: '100%' } } />,
      containerElement: <div style={ { height: '400px' } } />,
      mapElement: <div style={ { height: '100%' } } />
  }),
  withHandlers( () => {
      const refs = {
          map: undefined
      };

      return {
          onMapMounted: () => ( ref ) => {
              refs.map = ref;
          }
      };
  }),
  withScriptjs,
  withGoogleMap
)( ( props ) => {
    const { coordinates } = props;
    const defaultCenter = { lat: 14.5547, lng: 121.0244 };

    return (
        <GoogleMap
            defaultZoom={ 15 }
            defaultCenter={ defaultCenter }
            center={ !isEmpty( coordinates )
              ? coordinates
              : defaultCenter
            }
            ref={ props.onMapMounted }
        >
            <Marker
                position={ !isEmpty( coordinates )
                  ? coordinates
                  : defaultCenter
                }
            />
        </GoogleMap>
    );
});

export default Map;
