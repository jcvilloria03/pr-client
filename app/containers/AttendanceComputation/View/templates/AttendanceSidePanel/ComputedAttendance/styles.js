import styled from 'styled-components';

export const ComputedAttendanceWrapper = styled.div`
    background-color: #f0f4f6;
    border-radius: 0.5rem;
    padding: .5rem 1rem;

    * {
        font-size: 14px;
    }

    .title {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;

        p {
            margin-bottom: 0;
        }

        .button-icon {
            cursor: pointer;

            .icon {
                color: #00A5E5;
                font-size: 15px;
                width: 15px;
            }

            span {
                color: black
            }
        }
    }

    hr {
        margin-top: .5rem;
        border-top: 3px solid rgba(255,255,255,.5);
    }

    .records {
        width: 100%;

        .record-head {
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;

            .icon {
                font-size: 20px;
                color: #d1d1d1;
                width: 14px;
                margin-right: 1rem;
            }
        }

        hr {
            border-top: 3px solid rgba(255,255,255,.5);
        }

        .record-body {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 1rem;

            .icon-calendar {
                font-size: 38px;
                color: #d1d1d1;
                margin-right: 1rem;
                width: 38px;

                > i {
                    align-self: center;
                }
            }

            .record-type {
                width: 100%;
                margin-right: .5rem;
            }

            .DayPickerInput {
                width: 100%;

                input {
                    padding-top: 0;
                }
            }

            .error-message, .label {
                display: none;
            }

            .button-icon {
                cursor: pointer;

                span {
                    color: #f2130a;
                    font-size: 14px;
                }

                .icon-remove {
                    width: 12px;
                    font-size: 12px;
                    color: #f2130a;

                    > i {
                        align-self: center;
                    }
                }
            }

            .start_Time{
                border: 1px solid rgba(0,0,0,.15);
                border-color: #95989a;
                border-radius: 0;
                line-height: 28px;
                background-color: #fdfdfd;
                height: 46px;
                padding-left: 10px;
                width: 30%;
                margin-right: .5rem;
                padding-right: 9px;
            }
        }
    }
`;

