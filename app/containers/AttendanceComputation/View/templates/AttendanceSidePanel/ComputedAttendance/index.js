/* eslint-disable array-callback-return */
import React, { Component } from 'react';
import Flatpickr from 'react-flatpickr';

import 'flatpickr/dist/themes/material_green.css';

import Icon from 'components/Icon';
import Select from 'components/Select';

import { DAY_HOUR_RATE_TYPES } from '../../../constants';

import { ComputedAttendanceWrapper } from './styles';
import { H6 } from '../styles';

/**
 *
 * ComputedAttendance Component
 *
 */
class ComputedAttendance extends Component {
    static propTypes = {
        locked: React.PropTypes.bool,
        onEdit: React.PropTypes.func,
        readOnly: React.PropTypes.bool,
        expectedShifts: React.PropTypes.array,
        editAttendance: React.PropTypes.func,
        removeAttendance: React.PropTypes.func,
        computedAttendance: React.PropTypes.object
    }

    static defaultProps = {
        readOnly: false,
        expectedShifts: [],
        computedAttendance: [],
        dayHourTypeOptions: []
    };

    constructor( props ) {
        super( props );
        this.state = {

        };
    }

    getDayHourTypeOptions = ( recordType ) => {
        const { expectedShifts, computedAttendance } = this.props;
        const dayHourTypeOptions = expectedShifts.map( ( shift ) =>
          DAY_HOUR_RATE_TYPES.map( ( item ) => ({
              label: item,
              value: item.toUpperCase()
          }) ).filter( ( item ) => !computedAttendance[ shift.id ].map( ( data ) => data.type !== recordType && data.type ).includes( item.value ) )
        );

        return dayHourTypeOptions.reduce( ( options, dayHourType ) => options.concat( dayHourType ), []);
    }

    render() {
        const {
          onEdit,
          locked,
          readOnly,
          expectedShifts,
          editAttendance,
          removeAttendance,
          computedAttendance
        } = this.props;

        return (
            <ComputedAttendanceWrapper>
                <div className="title">
                    <H6 noBottomMargin>Record</H6>

                    {Boolean( readOnly && !locked ) && (
                        <button
                            onClick={ () => onEdit() }
                            disabled={ locked }
                            type="button"
                            className="button-icon"
                        >
                            <Icon name="pencil" className="icon" />

                            <H6 noBottomMargin>Edit</H6>
                        </button>
                    ) }
                </div>

                <hr />

                <div className="records">
                    {expectedShifts.map( ( shift ) =>
                        computedAttendance[ shift.id ].map( ( record, index ) => (
                            <div className="record-body" key={ index }>
                                <Icon name="calendarO" className="icon-calendar" />
                                <div className="record-type">
                                    <Select
                                        id="time-picker"
                                        value={ record.type }
                                        data={ this.getDayHourTypeOptions( record.type ) }
                                        placeholder="Select an hour type"
                                        ref={ ( ref ) => { this.year = ref; } }
                                        disabled={ readOnly }
                                        onChange={ ( item ) => {
                                            editAttendance( record.id, shift.id, 'type', item.value );
                                        } }
                                    />
                                </div>
                                <Flatpickr
                                    id="startWork"
                                    data-enable-time
                                    className="start_Time"
                                    value={ record.time }
                                    name="start"
                                    ref={ ( ref ) => { this.workStart = ref; } }
                                    options={ {
                                        allowInput: true,
                                        noCalendar: true,
                                        enableTime: true,
                                        dateFormat: 'H:i',
                                        time_24hr: true
                                    } }
                                    disabled={ readOnly }
                                    onClose={ ( time ) => {
                                        editAttendance( record.id, shift.id, 'time', time[ 0 ]);
                                    } }
                                />
                                {!readOnly &&
                                <button
                                    onClick={ () => removeAttendance( shift.id, record.id ) }
                                    type="button"
                                    className="button-icon"
                                >
                                    <Icon name="remove" className="icon-remove" />
                                </button>
                                }
                            </div>
                      ) )
                    ) }
                </div>
            </ComputedAttendanceWrapper>
        );
    }
}

export default ComputedAttendance;
