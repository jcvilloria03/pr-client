import React from 'react';

import { Container, ErrorIcon, ErrorText } from './styles';

/**
 *
 * MaximumClockOutNotice component
 *
 */
export function MaximumClockOutNotice() {
    return (
        <Container>
            <ErrorIcon>!</ErrorIcon>

            <ErrorText>Time log is beyond maximum clock-out rule</ErrorText>
        </Container>
    );
}

export default MaximumClockOutNotice;
