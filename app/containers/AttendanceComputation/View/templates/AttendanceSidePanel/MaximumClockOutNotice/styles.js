import styled from 'styled-components';

export const Container = styled.div`
    align-items: center;
    display: flex;
    gap: 16px;
    color: #f2130a;
    padding: 0px 2px 10px;
`;

export const ErrorIcon = styled.span`
    width: 20px;
    height: 20px;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    background: #f2130a;
    color: #fff;
    font-size: 14px;
    font-weight: bold;
`;

export const ErrorText = styled.p`
    margin: 0;
    flex: 1;
    line-height: 16px;
    font-size: 14px;
`;
