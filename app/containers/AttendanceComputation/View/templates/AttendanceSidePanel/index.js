/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable no-param-reassign */
/* eslint-disable camelcase */
/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import moment from 'moment';

import get from 'lodash/get';
import sum from 'lodash/sum';
import map from 'lodash/map';
import each from 'lodash/each';
import sortBy from 'lodash/sortBy';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import startsWith from 'lodash/startsWith';

import Icon from 'components/Icon';
import Switch from 'components/Switch';
import Button from 'components/Button';
import { H5, P } from 'components/Typography';

import { formatDate } from 'utils/functions';
import { DATE_FORMATS, PHT_UTC_OFFSET } from 'utils/constants';

import Leaves from './Leaves';
import ComputedAttendance from './ComputedAttendance';
import Records from './Records';

import {
    ButtonIcon,
    ContentContainer,
    SectionContainerHeader,
    ContentWrapper,
    DateDetails,
    DateDetailsContainer,
    FooterActions,
    FooterSection,
    H6,
    LockUnlock,
    SectionContainer,
    SidePanelContainer,
    Tab,
    Tabs,
    UserDetailsContainer,
    SectionContainerContentWrapper
} from './styles';

import { DAY_HOUR_RATE_TYPES } from '../../constants';

/**
 *
 * Attendance Panel
 *
 */
class AttendanceSidePanel extends Component {
    static propTypes = {
        open: React.PropTypes.bool,
        loading: React.PropTypes.bool,
        leaves: React.PropTypes.array,
        leaveTypes: React.PropTypes.array,
        lockAttendance: React.PropTypes.func,
        toggleSidePanel: React.PropTypes.func,
        selectedAttendance: React.PropTypes.object,
        storeSidePanelAttendance: React.PropTypes.func
        // pendingAttendance: React.PropTypes.array,
        // shifts: React.PropTypes.array
    }

    constructor( props ) {
        super( props );
        this.state = {
            selectedPanel: 'records',
            selectedDay: '',
            employeeId: null,
            locked: false,
            readOnly: false,
            lockUnlock: false,
            expectedShifts: [],
            computedAttendance: {},
            timeRecords: [],
            deletedTimeRecords: {},
            leaveRecords: [],
            removedLeaveRecords: []
        };
    }

    componentWillReceiveProps( nextProps ) {
        const hasPendingRecords = !isEmpty( nextProps.pendingAttendance ) && nextProps.pendingAttendance.find( ( attendance ) => attendance.id === nextProps.selectedAttendance.id );

        if ( !isEmpty( nextProps.selectedAttendance ) && ( isEmpty( nextProps.pendingAttendance ) || !hasPendingRecords ) ) {
            this.initializeData( nextProps.shifts, nextProps.selectedAttendance );
        }

        if ( !isEqual( nextProps.selectedAttendance, this.props.selectedAttendance ) ) {
            this.setState({
                leaveRecords: []
            });
        }

        if ( isEmpty( this.state.leaveRecords ) ) {
            if ( !isEmpty( nextProps.leaves ) ) {
                const { selectedAttendance } = this.props;

                const formattedRecords = nextProps.leaves.map( ( leave, i ) => ({
                    id: `${selectedAttendance.id}_${i + 1}`,
                    date: leave.date,
                    employee_id: leave.employee_id,
                    edited: false,
                    error: null,
                    type: {
                        label: leave.leave_type.name,
                        type: leave.type,
                        value: leave.leave_type_id
                    },
                    start_time: moment( leave.start_datetime ).format( 'HH:mm' ),
                    end_time: moment( leave.end_datetime ).format( 'HH:mm' ),
                    time_difference: leave.value
                }) );
                this.setState({
                    leaveRecords: formattedRecords
                });
            } else {
                this.setState({
                    leaveRecords: []
                });
            }
        }
    }

    initializeData( shifts, attendance ) {
        const { scheduled_shifts } = attendance;
        const expectedShifts = this.filterExpectedShifts( shifts, scheduled_shifts );
        const computedAttendance = this.getEmployeeComputedAttendancePerShift( attendance );
        const timeRecords = this.getEmployeeTimeSheetPerShift( attendance );

        this.setState({
            timeRecords,
            expectedShifts,
            computedAttendance,
            locked: attendance.locked,
            readOnly: true,
            lockUnlock: false,
            selectedDay: moment( attendance.date ).format( DATE_FORMATS.API ),
            employeeId: attendance.employee.uid,
            deletedTimeRecords: {}
        });
    }

    augmentDefaultShiftId( defaultShiftId ) {
        if ( !startsWith( defaultShiftId, 'ds_' ) ) {
            return `ds_${defaultShiftId}`;
        }
        return defaultShiftId;
    }

    filterExpectedShifts( shifts, scheduled_shifts ) {
        // Just in case we don't have anything in `scheduled_shifts`
        if ( isEmpty( scheduled_shifts ) ) {
            return [];
        }

        const assignedShifts = !isEmpty( scheduled_shifts.assigned_shifts ) ? scheduled_shifts.assigned_shifts[ 0 ] : null;
        const defaultShift = get( scheduled_shifts, 'default_shift', null );
        const isDefaultRegular = assignedShifts && assignedShifts.name === 'default' && get( assignedShifts, 'day_type' ) === 'regular';
        const isRestDay = assignedShifts && assignedShifts.name === 'RD' && get( assignedShifts, 'day_type' ) === 'rest_day';

        if ( isDefaultRegular ) {
            return [{
                id: this.augmentDefaultShiftId( assignedShifts.shift_id ),
                schedule: {
                    name: 'Default Schedule',
                    start_time: moment( defaultShift.start_time, 'HH:mm:ss' ).format( 'HH:mm' ),
                    end_time: moment( defaultShift.end_time, 'HH:mm:ss' ).format( 'HH:mm' ),
                    total_hours: assignedShifts.total_hours
                }
            }];
        } else if (
          isEmpty( scheduled_shifts.assigned_shifts ) &&
          !isEmpty( defaultShift ) && defaultShift.day_type === 'rest_day'
        ) {
            return [{
                id: this.augmentDefaultShiftId( defaultShift.id ),
                schedule: {
                    rest_day_id: defaultShift.id,
                    name: 'Rest Day',
                    start_time: defaultShift.work_start,
                    end_time: defaultShift.work_end,
                    total_hours: defaultShift.total_hours || null
                }
            }];
        } else if ( isRestDay ) {
            return [{
                id: this.augmentDefaultShiftId( assignedShifts.id ),
                schedule: {
                    rest_day_id: assignedShifts.id,
                    name: 'Rest Day',
                    start_time: assignedShifts.work_start,
                    end_time: assignedShifts.work_end,
                    total_hours: assignedShifts.total_hours || null
                }
            }];
        }

        const filteredShifts = shifts.filter( ( shift ) =>
               scheduled_shifts.assigned_shifts.find( ( scheduledShift ) =>
                 parseInt( scheduledShift.shift_id, 10 ) === parseInt( shift.id, 10 )
               ) );

        return sortBy( filteredShifts, ( data ) => data.schedule.start_time );
    }

    getShiftNameDescription = ( shiftName ) => {
        switch ( shiftName ) {
            case 'default':
                return 'Default Schedule';
            case 'RD':
                return 'Rest Day';
            default:
                return shiftName;
        }
    }

    /**
       * Get expected shifts
       *
       * @param {Array} scheduledShifts
       *
       * @returns {String} name.
       */
    getExpectedShifts = ( scheduled_shifts ) => {
        let isRestDay = false;

        if ( isEmpty( scheduled_shifts.assigned_shifts ) ) {
            const shiftDateIsRestDay = !isEmpty( scheduled_shifts.shift_dates ) &&
              Object.keys( scheduled_shifts.shift_dates ).some( ( shiftDate ) => scheduled_shifts.shift_dates[ shiftDate ].rest_day );

            const defaultShiftIsRestDay = !isEmpty( scheduled_shifts.default_shift ) &&
              scheduled_shifts.default_shift.day_type === 'rest_day';

            if ( shiftDateIsRestDay || defaultShiftIsRestDay ) {
                isRestDay = true;
            }
        }

        const restDayContent = isRestDay
            ? (
                <P noBottomMargin>
                    Schedule: <span className="shiftName">Rest Day</span>{' '}
                    ({scheduled_shifts.default_shift.start_time} - {scheduled_shifts.default_shift.end_time})
                </P>
            ) : null;

        return !isEmpty( scheduled_shifts.assigned_shifts )
            ? scheduled_shifts.assigned_shifts.map( ( shift, i ) => (
                <P noBottomMargin key={ i }>
                    <span className="shiftName">
                        {this.getShiftNameDescription( shift.name )}
                    </span>
                    {' '}
                    ({shift.start_time} - {shift.end_time})
                </P>
            ) ) : restDayContent;
    }

    dateType = ( scheduled_shifts ) => {
        if ( isEmpty( scheduled_shifts ) && isEmpty( scheduled_shifts.shift_dates ) ) return;

        const day = scheduled_shifts.shift_dates[ this.state.selectedDay ];
        let type = '';

        if ( !day.rest_day && !day.holiday_type ) {
            type = 'Regular, Non-Holiday';
        }

        if ( day.rest_day && !day.holiday_type ) {
            type = 'Rest Day, Non-Holiday';
        }

        if ( !day.rest_day && day.holiday_type ) {
            type = `Regular, ${day.holiday_type}`;
        }

        if ( day.rest_day && day.holiday_type ) {
            type = `Rest Day, ${day.holiday_type}`;
        }

        return type;
    }

    getExpectedHoursWork( scheduled_shifts ) {
        let expectedHoursWork = '0:00';
        const expectedShift = isEmpty( scheduled_shifts.assigned_shifts )
            ? 'Rest Day'
            : scheduled_shifts.assigned_shifts[ 0 ].name;

        if ( expectedShift === 'Rest Day' ) {
            return '0:00';
        }

        each( this.state.expectedShifts, ( shift ) => {
            const totalHours = moment( shift.schedule.total_hours, 'HH:mm' );
            const breakTotalMinutes = sum( map( shift.schedule.breaks, ( item ) => {
                if ( item.is_paid ) return 0;

                const start = moment( item.start, 'HH:mm' );
                const end = moment( item.end, 'HH:mm' );
                if ( item.type === 'flexi' || item.type === 'floating' ) {
                    return moment.duration( item.break_hours ).asMinutes();
                }
                return moment.duration( end.diff( start ) ).asMinutes();
            }) );

            expectedHoursWork = totalHours.subtract( breakTotalMinutes, 'minutes' ).format( 'HH:mm' );
        });

        return expectedHoursWork;
    }

    /**
       * Gets Employee's Computed Attendance per shift
       * @returns {Object}
       */
    getEmployeeComputedAttendancePerShift = ( attendance ) => {
        const defaultComputedAttendance = {};
        let defaultShiftId = null;
        let dayType = null;
        const employeeComputedAttendance = Object.keys( attendance.attendance )
          .reduce( ( attendanceObject, shiftId ) => {
              const shiftObject = attendance.attendance[ shiftId ];

              attendanceObject[ shiftId ] = Object.keys( shiftObject )
              .map( ( type, i ) => this.makeAttendanceObject( type, shiftObject[ type ], shiftId, i + 1 ) );

              return attendanceObject;
          }, {});

        const assignedShifts = attendance.scheduled_shifts.assigned_shifts;
        const defaultShift = attendance.scheduled_shifts.default_shift;

        if ( isEmpty( employeeComputedAttendance ) && isEmpty( assignedShifts ) && isEmpty( defaultShift ) ) {
            return { RD: []};
        } else if ( isEmpty( employeeComputedAttendance ) && isEmpty( assignedShifts ) && !isEmpty( defaultShift ) ) {
            defaultShiftId = defaultShift.id;
            dayType = defaultShift.day_type === 'rest_day' ? 'RD' : 'REGULAR';
            const shiftObject = this.makeAttendanceObject( dayType, 0, defaultShiftId, 1 );
            defaultComputedAttendance[ defaultShiftId ] = [shiftObject];
            return defaultComputedAttendance;
        }

        return employeeComputedAttendance;
    }

    makeAttendanceObject = ( type, value, shiftId, index, timeValue = null ) => {
        const time = timeValue || moment().startOf( 'day' ).add( value, 'minutes' ).format( 'HH:mm' );

        return {
            id: `${shiftId}_${index}`,
            edited: false,
            time,
            value,
            type,
            shiftId
        };
    }

    getEmployeeTimeSheetPerShift( attendance ) {
        const employeeTimesheet = {};

        if ( isEmpty( attendance.scheduled_shifts.assigned_shifts ) && isEmpty( attendance.scheduled_shifts.default_shift ) ) {
            employeeTimesheet.RD = [];
        } else if ( isEmpty( attendance.scheduled_shifts.assigned_shifts ) && !isEmpty( attendance.scheduled_shifts.default_shift ) ) {
            const defaultShiftId = this.augmentDefaultShiftId( attendance.scheduled_shifts.default_shift.id );
            employeeTimesheet[ defaultShiftId ] = [];
        } else {
            each( attendance.scheduled_shifts.assigned_shifts, ( shift ) => {
                let rawTimesheet = [];

                //  Get from raw_timesheet otherwise timesheet
                if ( !isEmpty( shift.raw_timesheet ) ) {
                    rawTimesheet = shift.raw_timesheet;
                } else if ( !isEmpty( shift.timesheet ) ) {
                    rawTimesheet = shift.timesheet;
                }

                employeeTimesheet[ `${shift.shift_id}` ] = rawTimesheet.map( ( timesheet, i ) => {
                    timesheet.id = `${shift.shift_id}_${i + 1}`;
                    timesheet.edited = false;
                    timesheet.new = false;
                    timesheet.tags = timesheet.tags || [];
                    timesheet.employee_id = timesheet.employee_uid;
                    timesheet.type = timesheet.state ? 'clock_in' : 'clock_out';
                    timesheet.shift_id = shift.shift_id;

                    const timestampString = timesheet.timestamp.toString();
                    timesheet.temp_key = timestampString;
                    timesheet.timestamp = timestampString;
                    return timesheet;
                });
            });
        }

        return employeeTimesheet;
    }

    addRecord = ( shift_id ) => {
        const cloneTimeRecords = [...this.state.timeRecords[ shift_id ]];

        const timestamp = moment( `${this.state.selectedDay} 00:00${PHT_UTC_OFFSET}` ).format( 'X' );
        cloneTimeRecords.push({
            id: `${shift_id}_${this.state.timeRecords[ shift_id ].length + 1}`,
            date: this.state.selectedDay,
            edited: false,
            new: true,
            employee_id: this.state.employeeId,
            shift_id,
            state: true,
            tags: [],
            temp_key: moment().format( 'x' ),
            timestamp,
            type: 'clock_in'
        });
        this.setState({
            readOnly: false,
            timeRecords: {
                ...this.state.timeRecords,
                [ shift_id ]: cloneTimeRecords
            }
        });
    }

    removeRecord = ( recordId, shiftId ) => {
        const { timeRecords, deletedTimeRecords } = this.state;
        const cloneTimeRecords = [...timeRecords[ shiftId ]];
        const clonedDeletedTimeRecords = !isEmpty( deletedTimeRecords ) ? [...deletedTimeRecords[ shiftId ]] : [];
        const selectedRecord = cloneTimeRecords.find( ( record ) => record.id === recordId );

        if ( !selectedRecord.new ) {
            const findRecord = clonedDeletedTimeRecords.find( ( item ) => item.id === recordId );

            if ( !findRecord ) {
                clonedDeletedTimeRecords.push( selectedRecord );
            }

            this.setState({
                deletedTimeRecords: {
                    ...deletedTimeRecords,
                    [ shiftId ]: clonedDeletedTimeRecords
                }
            });
        }

        this.setState({
            timeRecords: {
                ...timeRecords,
                [ shiftId ]: cloneTimeRecords.filter( ( record ) => record.id !== recordId )
            }
        });
    }

    editRecord = ( recordId, shiftId, type, value ) => {
        const { timeRecords, deletedTimeRecords } = this.state;

        const cloneTimeRecords = [...timeRecords[ shiftId ]];
        const clonedDeletedTimeRecords = !isEmpty( deletedTimeRecords ) ? [...deletedTimeRecords[ shiftId ]] : [];
        const record = cloneTimeRecords.find( ( item ) => item.id === recordId );

        if ( !record.new ) {
            const findRecord = clonedDeletedTimeRecords.find( ( item ) => item.id === recordId );

            if ( !findRecord ) {
                clonedDeletedTimeRecords.push( record );
            }

            this.setState({
                deletedTimeRecords: {
                    ...deletedTimeRecords,
                    [ shiftId ]: clonedDeletedTimeRecords
                }
            });
        }

        if ( type === 'type' ) {
            record[ type ] = value;
            record.state = value === 'clock_in';
            record.edited = true;
        } else {
            record[ type ] = value;
            record.edited = true;
        }

        this.setState({
            readOnly: false,
            timeRecords: {
                ...timeRecords,
                [ shiftId ]: cloneTimeRecords
            }
        });
    }

    getDayHourTypeOptions = ( expectedShifts, computedAttendance ) => {
        const dayHourTypeOptions = expectedShifts.map( ( shift ) =>
          DAY_HOUR_RATE_TYPES.map( ( item ) => ({
              label: item,
              value: item.toUpperCase()
          }) ).filter( ( item ) => !computedAttendance[ shift.id ].map( ( data ) => data.type ).includes( item.value ) )
        );

        return dayHourTypeOptions.reduce( ( options, dayHourType ) => options.concat( dayHourType ), []);
    }

    addAttendance = () => {
        const { computedAttendance, expectedShifts } = this.state;
        const dayHourTypeOptions = this.getDayHourTypeOptions( expectedShifts, computedAttendance );

        expectedShifts.map( ( shift ) => {
            const shiftId = shift.id;
            const computedAttendanceClone = computedAttendance;
            const shiftValue = dayHourTypeOptions[ 0 ];

            computedAttendanceClone[ shiftId ].push({
                id: `${shiftId}_${computedAttendanceClone[ shiftId ].length + 1}`,
                edited: true,
                time: '00:00',
                value: 0,
                type: shiftValue.value,
                shiftId
            });

            this.setState({
                readOnly: false,
                computedAttendance: computedAttendanceClone
            });
        });
    }

    editAttendance = ( id, shiftId, type, value ) => {
        const computedAttendanceClone = this.state.computedAttendance;

        const index = computedAttendanceClone[ shiftId ].findIndex( ( data ) => data.id === id );

        if ( type === 'time' ) {
            const formatedTime = moment( value ).format( 'HH:mm' );
            const minutes = moment( formatedTime, 'HH:mm' ).minutes();
            const hours = moment( formatedTime, 'HH:mm' ).hours();
            const totalMinutes = hours * 60 + minutes;

            computedAttendanceClone[ shiftId ][ index ].edited = true;
            computedAttendanceClone[ shiftId ][ index ].time = formatedTime;
            computedAttendanceClone[ shiftId ][ index ].value = totalMinutes;
        } else {
            computedAttendanceClone[ shiftId ][ index ].type = value;
            computedAttendanceClone[ shiftId ][ index ].edited = true;
        }

        this.setState({
            computedAttendance: computedAttendanceClone
        });
    }

    removeAttendance = ( shiftId, id ) => {
        const clonedComputedAttendance = this.state.computedAttendance;

        Object.assign( clonedComputedAttendance, {
            [ shiftId ]: clonedComputedAttendance[ shiftId ].filter( ( data ) => data.id !== id )
        });

        clonedComputedAttendance[ shiftId ].map( ( data ) => {
            Object.assign( data, { edited: true });
        });

        this.setState({ computedAttendance: clonedComputedAttendance });
    }

    addLeave = () => {
        const { selectedAttendance } = this.props;
        const { leaveRecords } = this.state;
        const cloneLeaveRecords = [...leaveRecords];

        cloneLeaveRecords.push({
            id: `${selectedAttendance.id}_${leaveRecords.length}`,
            date: selectedAttendance.date,
            employee_id: selectedAttendance.employee.uid,
            edited: true,
            error: null,
            type: '',
            start_time: '',
            end_time: '',
            time_difference: ''
        });
        this.setState({
            readOnly: false,
            leaveRecords: cloneLeaveRecords
        });
    }

    onChangeLeave = ( id, key, value ) => {
        const cloneLeaveRecords = [...this.state.leaveRecords];
        const record = cloneLeaveRecords.find( ( item ) => item.id === id );
        record[ key ] = value;
        record.edited = true;
        this.setState({
            leaveRecords: cloneLeaveRecords
        });
    }

    removeLeave = ( id ) => {
        const { leaveRecords, removedLeaveRecords } = this.state;
        const clonedLeaveRecords = [...leaveRecords];
        const clonedRemovedLeaveRecords = !isEmpty( removedLeaveRecords ) ? removedLeaveRecords : [];
        const selectedRecord = clonedLeaveRecords.find( ( record ) => record.id === id );

        if ( !selectedRecord.new ) {
            const findRecord = clonedRemovedLeaveRecords.find( ( item ) => item.id === id );

            if ( !findRecord ) {
                clonedRemovedLeaveRecords.push( selectedRecord );
            }

            this.setState({
                removedLeaveRecords: clonedRemovedLeaveRecords
            });
        }

        this.setState({
            leaveRecords: clonedLeaveRecords.filter( ( record ) => record.id !== id )
        }, () => {
            this.props.storeSidePanelAttendance( this.state );
        });
    }

    onEdit = () => {
        this.setState({
            readOnly: false
        });
    }

    render() {
        const {
          locked,
          readOnly,
          lockUnlock,
          selectedDay,
          timeRecords,
          leaveRecords,
          selectedPanel,
          expectedShifts,
          computedAttendance
        } = this.state;

        const {
          open,
          leaves,
          loading,
          leaveTypes,
          toggleSidePanel,
          selectedAttendance,
          storeSidePanelAttendance
        } = this.props;

        return (
            <SidePanelContainer className={ open && !loading ? 'slide-in' : '' }>
                <div className="header">
                    <H5 noBottomMargin>Attendance</H5>

                    <button
                        onClick={ () => {
                            this.setState({
                                timeRecords: [],
                                leaveRecords: [],
                                selectedPanel: 'records'
                            });
                            toggleSidePanel();
                        } }
                        type="button"
                    >
                        <Icon name="remove" className="icon" />
                    </button>
                </div>

                <Tabs className="sub-header">
                    <Tab
                        active={ selectedPanel === 'records' }
                        onClick={ () => this.setState({
                            selectedPanel: 'records',
                            readOnly: true
                        }) }
                        type="button"
                    >
                        <span>Time Records</span>
                    </Tab>

                    <Tab
                        active={ selectedPanel === 'leaves' }
                        onClick={ () => this.setState({
                            selectedPanel: 'leaves',
                            readOnly: true
                        }) }
                        type="button"
                    >
                        <span>Leaves</span>
                    </Tab>
                </Tabs>

                <ContentWrapper>
                    <ContentContainer>
                        <SectionContainer>
                            { Object.keys( selectedAttendance ).length > 0 && (
                                <SectionContainer>
                                    <DateDetailsContainer>
                                        <DateDetails>
                                            <H6 noBottomMargin>
                                                {formatDate( selectedAttendance.date, DATE_FORMATS.DISPLAY )}
                                            </H6>
                                            <P noBottomMargin>
                                                {this.dateType( selectedAttendance.scheduled_shifts )}
                                            </P>
                                        </DateDetails>

                                        <LockUnlock>
                                            {locked ? 'Unlock' : 'Lock'}

                                            <Switch
                                                checked={ locked }
                                                onChange={ ( value ) => {
                                                    this.setState({
                                                        locked: value,
                                                        lockUnlock: true
                                                    });
                                                } }
                                            />
                                        </LockUnlock>
                                    </DateDetailsContainer>

                                    <UserDetailsContainer>
                                        <H6 noBottomMargin>{selectedAttendance.employee.full_name}</H6>

                                        {this.getExpectedShifts( selectedAttendance.scheduled_shifts )}

                                        <P noBottomMargin>
                                            Expected Work Hours{' '}
                                            <span>({this.getExpectedHoursWork( selectedAttendance.scheduled_shifts )})</span>
                                        </P>
                                    </UserDetailsContainer>

                                    {selectedPanel === 'records' && (
                                        <SectionContainer>
                                            { expectedShifts.map( ( shift, i ) =>
                                                <SectionContainerContentWrapper key={ i }>
                                                    <SectionContainerHeader>
                                                        <H6 noBottomMargin>Time Records</H6>

                                                        {!locked && (
                                                            <ButtonIcon
                                                                onClick={ () => this.addRecord( shift.id ) }
                                                                className="button-icon"
                                                                type="button"
                                                                disabled={ locked }
                                                            >
                                                                <Icon name="plusCircle" className="icon" />

                                                                <span>Add Record</span>
                                                            </ButtonIcon>
                                                        )}
                                                    </SectionContainerHeader>

                                                    { !isEmpty( timeRecords ) && timeRecords[ shift.id ].length > 0
                                                        ? (
                                                            <Records
                                                                shift={ shift }
                                                                locked={ locked }
                                                                selectedDay={ selectedDay }
                                                                readOnly={ readOnly }
                                                                timeRecords={ timeRecords[ shift.id ] }
                                                                onEdit={ this.onEdit }
                                                                editRecord={ this.editRecord }
                                                                removeRecord={ this.removeRecord }
                                                            />
                                                        ) : <strong><P noBottomMargin italicized>No record found</P></strong>
                                                    }
                                                </SectionContainerContentWrapper>
                                            )}

                                            <SectionContainerContentWrapper>
                                                <SectionContainerHeader>
                                                    <H6 noBottomMargin>Computed Attendance</H6>

                                                    {!locked && (
                                                        <ButtonIcon
                                                            onClick={ () => this.addAttendance() }
                                                            className="button-icon"
                                                            type="button"
                                                            disabled={ locked }
                                                        >
                                                            <Icon name="plusCircle" className="icon" />
                                                            <span>Add Attendance</span>
                                                        </ButtonIcon>
                                                    )}
                                                </SectionContainerHeader>

                                                <ComputedAttendance
                                                    readOnly={ readOnly }
                                                    locked={ locked }
                                                    expectedShifts={ expectedShifts }
                                                    computedAttendance={ computedAttendance }
                                                    onEdit={ this.onEdit }
                                                    editAttendance={ this.editAttendance }
                                                    removeAttendance={ this.removeAttendance }
                                                />
                                            </SectionContainerContentWrapper>
                                        </SectionContainer>
                                    )}
                                </SectionContainer>
                            )}

                            { selectedPanel === 'leaves' && (
                                <SectionContainerContentWrapper>
                                    <SectionContainerHeader>
                                        <H6 noBottomMargin>Leaves</H6>

                                        { !locked && (
                                            <ButtonIcon
                                                onClick={ () => this.addLeave() }
                                                className="button-icon"
                                                type="button"
                                                disabled={ locked }
                                            >
                                                <Icon name="plusCircle" className="icon" />

                                                <span>Add Leave</span>
                                            </ButtonIcon>
                                        ) }
                                    </SectionContainerHeader>

                                    { leaveRecords.length > 0
                                        ? leaveRecords.map( ( record, i ) => (
                                            <Leaves
                                                leaves={ leaves }
                                                locked={ locked }
                                                record={ record }
                                                readOnly={ readOnly }
                                                leaveTypes={ leaveTypes }
                                                expectedShifts={ expectedShifts }
                                                onEdit={ this.onEdit }
                                                removeLeave={ this.removeLeave }
                                                onChangeLeave={ this.onChangeLeave }
                                                recordCount={ i + 1 }
                                                key={ i }
                                            />
                                        ) )
                                        : <P noBottomMargin italicized>No record found</P>
                                    }
                                </SectionContainerContentWrapper>
                            ) }
                        </SectionContainer>
                    </ContentContainer>

                    { ( !readOnly || lockUnlock ) && (
                        <FooterSection>
                            <FooterActions className="button-wrapper">
                                <Button
                                    block
                                    label="Cancel"
                                    type="neutral"
                                    onClick={ () => {
                                        this.setState({
                                            timeRecords: [],
                                            leaveRecords: [],
                                            selectedPanel: 'records'
                                        });
                                        toggleSidePanel();
                                    } }
                                />

                                <Button
                                    label="Update"
                                    type="action"
                                    block
                                    onClick={ () => {
                                        this.setState({
                                            readOnly: true,
                                            lockUnlock: false
                                        });

                                        storeSidePanelAttendance( this.state );
                                    } }
                                />
                            </FooterActions>

                            <P noBottomMargin>
                                If you click <strong>Update</strong>, your changes will only be displayed on the table temporarily.
                                {' '}
                                <strong>To save your changes permanently in the system, click &quot;Save and Submit&quot;.</strong>
                            </P>
                        </FooterSection>
                    ) }
                </ContentWrapper>
            </SidePanelContainer>
        );
    }
}
export default AttendanceSidePanel;
