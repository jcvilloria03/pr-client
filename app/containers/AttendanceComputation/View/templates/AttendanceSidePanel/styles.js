import styled from 'styled-components';

import { H6 as BaseH6 } from 'components/Typography';

export const H6 = styled( BaseH6 )`
    font-size: 14px;
    line-height: unset;
`;

export const HeaderContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

export const SidePanelContainer = styled.div`
    margin-bottom: 1rem;
    border-radius: 5px;
    position: fixed;
    top: 4.5rem;
    right: 0;
    transform: translateX(110%);
    bottom: 0;
    z-index: 10;
    width: 23rem;
    margin: 0;
    overflow-y: auto;
    background-color: white;
    box-shadow: 0 0 14px 0 #2b2b2b4f;
    border-radius: 0;
    transition: all .2s ease-in-out;

    &.slide-in {
        transform: translateX(0);
    }

    .header {
        padding: 1rem 1rem;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;

        h4 {
            font-weight: bold;
            margin-bottom: 0;
        }

        button {
            &:focus {
                outline: 0;
            }

            .icon {
                font-size: 15px;
                color: black;
                display: inline-flex;
                width: 14px;

                > i {
                    align-self: center;
                }
            }
        }
    }
`;

export const SectionContainer = styled.section`
    display: flex;
    flex-direction: column;
    gap: 24px;
`;

export const SectionContainerContentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 8px;
`;

export const ContentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 24px;
    padding: 1rem;
`;

export const ContentContainer = styled.div`
    .button-icon {
        display: flex;
        align-items: center;

        &:focus {
            outline: 0;
        }

        span {
            color: #83d24b;
            font-weight: 600;
        }

        .icon {
            font-size: 20px;
            color: #83d24b;
            display: inline-flex;
            margin-right: .5rem;
            width: 14px;

            > i {
                align-self: center;
            }
        }
    }
`;

export const SectionContainerHeader = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

export const FooterSection = styled( SectionContainer )``;

export const FooterActions = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    flex-grow: 1;
    align-items: center;
    gap: 8px;

    && {
        > button {
            margin: 0;
            margin-top: 0;
        }
    }
`;

export const Tabs = styled.div`
    box-shadow: 0 1px 2px 0 #cccccc;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-evenly;
`;

export const Tab = styled.button`
    display: inline-block;
    width: 100%;
    padding: .5rem;
    color: black !important;
    border-bottom: 3px solid #fff;
    font-size: 14px;
    text-align: center;
    cursor: pointer;

    &:hover {
        color: #00A5E5 !important;
        border-bottom-color: #00A5E5;
    }

    &:focus {
        outline: 0;
    }

    ${( props ) => props.active && `
        color: #00A5E5 !important;
        border-bottom-color: #00A5E5;
    `}
`;

export const LockUnlock = styled.span`
    font-size: 14px;
    font-weight: 600;
`;

export const DateDetailsContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    :first-child {
        h4 {
            margin-bottom: 0;
        }

        p {
            color: #00A5E5;
        }
    }

    .rc-switch {
        margin-left: .5rem;
    }
`;

export const DateDetails = styled.div`
    display: flex;
    flex-direction: column;
    gap: 4px;
`;

export const UserDetailsContainer = styled.div`
    display: flex;
    flex-direction: column;
    gap: 4px;

    h4 {
        font-weight: bold;
        margin-bottom: 0;
    }

    p {
        margin-bottom: 0;
    }

    .shiftName {
        color: #00A5E5;
        font-size: 14px;
    }
`;

export const ButtonIcon = styled.button`
    cursor: pointer;

    .icon {
        width: 16px;
    }

    span {
        font-size: 14px;
    }
`;
