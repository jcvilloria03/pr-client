import { createSelector } from 'reselect';

/**
 * Direct selector to the dashboard state domain
 */
const selectAttendanceComputationDomain = () => ( state ) => state.get( 'attendanceComputation' );

const makeSelectLoading = () => createSelector(
    selectAttendanceComputationDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectLoadingTable = () => createSelector(
    selectAttendanceComputationDomain(),
  ( substate ) => substate.get( 'loadingTable' )
);

const makeSelectNotification = () => createSelector(
    selectAttendanceComputationDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);
const makeSelectAttendance = () => createSelector(
    selectAttendanceComputationDomain(),
  ( substate ) => substate.get( 'attendance' ).toJS()
);

const makeSelectPagination = () => createSelector(
    selectAttendanceComputationDomain(),
    ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectLeaves = () => createSelector(
    selectAttendanceComputationDomain(),
    ( substate ) => substate.get( 'leaves' ).toJS()
);

const makeSelectLeaveTypes = () => createSelector(
    selectAttendanceComputationDomain(),
    ( substate ) => substate.get( 'leaveTypes' ).toJS()
);

const makeSelectShifts = () => createSelector(
  selectAttendanceComputationDomain(),
  ( substate ) => substate.get( 'shifts' ).toJS()
);

export {
  makeSelectShifts,
  makeSelectLeaveTypes,
  makeSelectLeaves,
  makeSelectLoading,
  makeSelectLoadingTable,
  makeSelectPagination,
  makeSelectAttendance,
  makeSelectNotification
};
