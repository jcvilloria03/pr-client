/* eslint-disable camelcase */
import React from 'react';
import moment from 'moment';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import has from 'lodash/has';
import each from 'lodash/each';
import merge from 'lodash/merge';
import isEqual from 'lodash/isEqual';
import flatten from 'lodash/flatten';
import isEmpty from 'lodash/isEmpty';
import compact from 'lodash/compact';
import findLast from 'lodash/findLast';
import flattenDeep from 'lodash/flattenDeep';

import Icon from 'components/Icon';
import Table from 'components/Table';
import Input from 'components/Input';
import Button from 'components/Button';
import SnackBar from 'components/SnackBar';
import SalDropdown from 'components/SalDropdown';
import { H2, H3 } from 'components/Typography';
import FooterTablePagination from 'components/FooterTablePagination';
import { getIdsOfSelectedRows } from 'components/Table/helpers';

import SubHeader from 'containers/SubHeader';

import { auth } from 'utils/AuthService';
import { browserHistory } from 'utils/BrowserHistory';
import { formatDate, formatPaginationLabel } from 'utils/functions';
import { DATE_FORMATS, TIME_AND_ATTENDANCE_SUBHEADER_ITEMS } from 'utils/constants';

import Filter from './templates/Filter';
import AttendanceSidePanel from './templates/AttendanceSidePanel';
import * as attendanceComputationActions from './actions';

import {
  PageWrapper,
  LoadingStyles,
  StyledContainer,
  NotificationSubheader,
  AttendanceComputationWrapper
} from './styles';

import {
  makeSelectShifts,
  makeSelectLeaveTypes,
  makeSelectLeaves,
  makeSelectLoading,
  makeSelectLoadingTable,
  makeSelectPagination,
  makeSelectAttendance,
  makeSelectNotification
} from './selectors';

/**
 * Announcement Section
 */
export class attendanceComputation extends React.PureComponent {
    static propTypes = {
        loading: React.PropTypes.bool,
        loadingTable: React.PropTypes.bool,
        regenerate: React.PropTypes.func,
        download: React.PropTypes.func,
        getSidePanelData: React.PropTypes.func,
        getAttendance: React.PropTypes.func,
        lockAttendance: React.PropTypes.func,
        initializePage: React.PropTypes.func,
        bulkCreateDeleteTimeRecords: React.PropTypes.func,
        bulkCreateUpdateDeleteLeaves: React.PropTypes.func,
        editComputedAttendance: React.PropTypes.func,
        attendance: React.PropTypes.array,
        shifts: React.PropTypes.array,
        leaves: React.PropTypes.array,
        leaveTypes: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        pagination: React.PropTypes.shape({
            count: React.PropTypes.number,
            page: React.PropTypes.number,
            total: React.PropTypes.number,
            page_size: React.PropTypes.number
        })
    }

    static defaultProps = {
        attendance: []
    };

    constructor( props ) {
        super( props );

        this.state = {
            readyToRender: false,
            label: 'Showing 0-0 of 0 entries',
            delete_label: '',
            show_filter: false,
            sort_by: { date: 'DESC', employee_name: 'ASC' },
            hasFiltersApplied: false,
            selectedAttendance: {},
            filtersApplied: [],
            dropdownItems: [
                {
                    label: 'Regenerate',
                    children: <div>Regenerate</div>,
                    onClick: () => {
                        const selectedRows = this.attendanceTable.state.selectedRows;
                        const selectedIds = selectedRows.map( ( row ) => row.employee.uid );

                        this.regenerateOrDownloadAttendance( 'regenerate', selectedIds );
                    }
                },
                {
                    label: 'Lock',
                    children: <div>Lock</div>,
                    onClick: () => {
                        const employee_ids = getIdsOfSelectedRows( this.attendanceTable );

                        this.handleLockAttendance({
                            employee_ids,
                            lock: true
                        });

                        this.attendanceTable.setState({
                            selectedRows: [],
                            selected: new Array( this.attendanceTable.state.selected.length ).fill( false )
                        });
                    }
                },
                {
                    label: 'Unlock',
                    children: <div>Unlock</div>,
                    onClick: () => {
                        const employee_ids = getIdsOfSelectedRows( this.attendanceTable );

                        this.handleLockAttendance({
                            employee_ids,
                            lock: false
                        });

                        this.attendanceTable.setState({
                            selectedRows: [],
                            selected: new Array( this.attendanceTable.state.selected.length ).fill( false )
                        });
                    }
                },
                {
                    label: 'Download',
                    children: <div>Download</div>,
                    onClick: () => {
                        const selectedRows = this.attendanceTable.state.selectedRows;
                        const selectedIds = selectedRows.map( ( row ) => row.employee.uid );

                        this.regenerateOrDownloadAttendance( 'download', selectedIds );
                    }
                }
            ],
            pending_attendance: [],
            showModal: false,
            profile: null,
            saving: false,
            open: false
        };

        this.onSortingChange = this.onSortingChange.bind( this );
    }
    componentWillMount() {
        const profile = auth.getUser();

        this.setState({ profile });
    }

    /* eslint-disable react/no-did-mount-set-state */
    /**
     * Already tried moving this into componentWillMount(), but errors were received during pagination.
     */
    componentDidMount() {
        this.props.initializePage();
        this.setState({ readyToRender: true });
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.pagination, this.props.pagination ) ) {
            this.setState({
                pagination: nextProps.pagination,
                label: formatPaginationLabel({
                    page: nextProps.pagination.page - 1,
                    pageSize: nextProps.pagination.page_size,
                    dataLength: nextProps.pagination.total
                })
            });
        }

        if ( !isEqual( nextProps.attendance, this.props.attendance )
              || this.state.saving ) {
            this.setState({
                attendance: nextProps.attendance,
                selectedAttendance: {},
                pending_attendance: [],
                showModal: false,
                saving: false,
                open: false
            });
        }
    }

    onSortingChange = async ( column, additive ) => {
        // clone current sorting sorting
        let newSorting = JSON.parse( JSON.stringify( this.attendanceTable.state.sorting, ( key, value ) => {
            if ( typeof value === 'function' ) {
                return value.toString();
            }
            return value;
        }) );

        // check if already sorted by same column
        const existingIndex = newSorting.findIndex( ( d ) => d.id === column.id );

        let isDesc = false;
        if ( existingIndex > -1 ) {
            const existing = newSorting[ existingIndex ];
            if ( existing.desc ) {
                if ( additive ) {
                    newSorting.splice( existingIndex, 1 );
                } else {
                    existing.desc = false;
                    newSorting = [existing];
                }
            } else {
                existing.desc = true;
                isDesc = true;
                if ( !additive ) {
                    newSorting = [existing];
                }
            }
        } else if ( additive ) {
            newSorting.push({
                id: column.id,
                desc: false
            });
        } else {
            newSorting = [{
                id: column.id,
                desc: false
            }];
        }

        let searchQuery = null;

        const orderDir = isDesc ? 'DESC' : 'ASC';
        const { page, page_size } = this.props.pagination;
        const { status,
          start_date,
          end_date,
          employee_ids,
          department_ids,
          location_ids
        } = this.filter.state;

        const filter = {
            page,
            page_size,
            sort_by: { [ column.id ]: orderDir },
            start_date,
            end_date,
            employee_ids: [...new Set([...employee_ids])],
            department_ids,
            location_ids,
            status: [status],
            search_terms: searchQuery
        };

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();

            filter.search_terms = {
                all: searchQuery
            };
        }

        await this.props.getAttendance({ ...filter });

        // set new sorting states
        this.setState({
            sort_by: { [ column.id ]: orderDir }
        });
        this.attendanceTable.setState({
            sorting: newSorting
        });
        this.attendanceTable.tableComponent.setState({
            sorting: newSorting
        });
    }

    getTimesheet = ( assignedShifts, type = 'timesheet' ) => {
        const timesheet = [];

        each( assignedShifts, ( shift ) => {
            merge( timesheet, shift[ type ]);
        });

        return compact( timesheet );
    }

    getTimeClocks = ( assignedShifts, param ) => {
        const timesheet = this.getTimesheet( assignedShifts );
        const rawTimesheet = this.getTimesheet( assignedShifts, 'raw_timesheet' );

        if ( !timesheet.length && !rawTimesheet.length ) return '';

        // fetch from raw timesheet first
        let clockIn = rawTimesheet.find( ( x ) => x.state );
        let clockOut = findLast( rawTimesheet, ( x ) => !x.state );

        // if no timeclocks found, fetch from timesheet
        if ( !clockIn ) {
            clockIn = timesheet.find( ( x ) => x.state );
        }

        if ( !clockOut ) {
            clockOut = findLast( timesheet, ( x ) => !x.state );
        }

        return param === 'clock_in'
          ? clockIn && this.forceTZ( clockIn.timestamp ).format( 'HH:mm' )
          : clockOut && this.forceTZ( clockOut.timestamp ).format( 'HH:mm' );
    }

    /**
     * Get the attendance tag
     * @param {Object} attendance
     *  {
     *    "107246": {
     *      "ABSENT": 480
     *    }
     *  }
     * @returns {string}
     */
    getAttendanceTag = ( attendance ) => {
        if ( !attendance || !Object.keys( attendance ).length ) {
            return [];
        }

        const ALLOWED_TAGS = [ 'unpaid leave', 'paid leave', 'absent', 'tardy', 'undertime' ];
        const key = Object.keys( attendance );
        return Object.keys( attendance[ key[ 0 ] ]).map( ( tag ) => tag.toLowerCase() ).filter( ( tag ) => ALLOWED_TAGS.includes( tag ) );
    }

    getExpectedShifts = ( scheduled_shifts ) => {
        let isRestDay = false;

        if ( isEmpty( scheduled_shifts.assigned_shifts ) ) {
            const shiftDateIsRestDay = !isEmpty( scheduled_shifts.shift_dates ) &&
              Object.keys( scheduled_shifts.shift_dates ).some( ( shiftDate ) => scheduled_shifts.shift_dates[ shiftDate ].rest_day );

            const defaultShiftIsRestDay = !isEmpty( scheduled_shifts.default_shift ) &&
              scheduled_shifts.default_shift.day_type === 'rest_day';

            if ( shiftDateIsRestDay || defaultShiftIsRestDay ) {
                isRestDay = true;
            }
        }
        return !isEmpty( scheduled_shifts.assigned_shifts )
       ? scheduled_shifts.assigned_shifts.map( ( shift ) => {
           switch ( shift.name ) {
               case 'default':
                   return 'Default Schedule';
               case 'RD':
                   return 'Rest Day';
               default:
                   return shift.name;
           }
       }).join( ', ' )
        : isRestDay && 'Rest Day';
    }

    // Create a new moment object with forced timezone
    forceTZ = ( timestamp ) => moment( timestamp, 'X' ).utcOffset( '+8000' )

    handleLockAttendance( payload ) {
        let searchQuery = null;
        const { status,
        start_date,
        end_date,
        employee_ids,
        department_ids,
        location_ids
      } = this.filter.state;
        const { sort_by } = this.state;
        const { page, page_size } = this.props.pagination;

        const filter = {
            page,
            page_size,
            sort_by,
            start_date,
            end_date,
            employee_ids: [...new Set([...employee_ids])],
            department_ids,
            location_ids,
            status: [status],
            search_terms: searchQuery
        };

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();

            filter.search_terms = {
                all: searchQuery
            };
        }

        this.props.lockAttendance({ employee_ids: payload.employee_ids, lock: payload.lock, filter });
    }

    tablePaginationAndFilter = ( page = 1, page_size = 10, filter = {}) => {
        let searchQuery = null;
        const { status,
          start_date,
          end_date,
          employee_ids,
          department_ids,
          location_ids
        } = this.filter.state;
        const { sort_by } = this.state;

        let payload = {
            page,
            page_size,
            sort_by,
            search_terms: searchQuery
        };

        if ( Object.values( filter ).length > 0 ) {
            payload = Object.assign( payload, { ...filter });
        } else {
            payload = Object.assign( payload, {
                start_date,
                end_date,
                employee_ids: [...new Set([...employee_ids])],
                department_ids,
                location_ids,
                status: [status]
            });
        }

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();

            payload.search_terms = {
                all: searchQuery
            };
        }

        this.props.getAttendance({ ...payload });
    }

    regenerateOrDownloadAttendance = ( type = '', selectedRowIds = []) => {
        let searchQuery = null;
        const { status,
          start_date,
          end_date,
          employee_ids,
          department_ids,
          location_ids
        } = this.filter.state;
        const { sort_by } = this.state;
        const { page, page_size } = this.props.pagination;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        const payload = {
            page,
            page_size,
            start_date,
            end_date,
            sort_by,
            employee_ids: [...new Set([ ...employee_ids, ...selectedRowIds ])],
            department_ids,
            location_ids,
            status: [status],
            search_terms: searchQuery
        };

        if ( type === 'download' ) {
            payload.sort_by = this.state.sort_by;
        }

        type === 'download' ? this.props.download( payload ) : this.props.regenerate( payload );
    }

    toggleFilter = () => {
        this.setState( ( state ) => ({
            show_filter: !state.show_filter
        }) );
    }

    toggleSidePanel = () => {
        this.setState( ( state ) => ({
            open: !state.open
        }) );
    }

    saveAttendance() {
        let searchQuery = null;
        const { status,
        start_date,
        end_date,
        employee_ids,
        department_ids,
        location_ids
      } = this.filter.state;
        const { pending_attendance, sort_by } = this.state;
        const { page, page_size } = this.props.pagination;

        const filter = {
            page,
            page_size,
            sort_by,
            start_date,
            end_date,
            employee_ids: [...new Set([...employee_ids])],
            department_ids,
            location_ids,
            status: [status],
            search_terms: searchQuery
        };

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();

            filter.search_terms = {
                all: searchQuery
            };
        }

        const mergedTime = {};
        const mergedAttendance = [];
        const mergedLeave = [];
        const attendancesToLock = [];
        const attendancesToUnlock = [];

        this.setState({
            saving: true
        });

        pending_attendance.forEach( ( attendance ) => {
            if ( !isEmpty( attendance.time_records ) ) {
                mergedTime.create = [ ...mergedTime.create || [], ...attendance.time_records.create ];
                mergedTime.delete = [ ...mergedTime.delete || [], ...attendance.time_records.delete ];
            }

            if ( !isEmpty( attendance.leave_records ) ) {
                mergedLeave.push( attendance.leave_records );
            }

            if ( !isEmpty( attendance.attendance_records ) ) {
                mergedAttendance.push( attendance.attendance_records );
            }

            if ( attendance.locked ) {
                attendancesToLock.push( attendance.id );
            } else {
                attendancesToUnlock.push( attendance.id );
            }
        });

        let refresh = true;

        if ( !isEmpty( mergedTime ) ) {
            refresh = false;
            this.props.bulkCreateDeleteTimeRecords({ records: mergedTime, filter });
        }

        if ( !isEmpty( mergedAttendance ) ) {
            refresh = false;
            this.props.editComputedAttendance({ records: mergedAttendance, filter });
        }

        if ( !isEmpty( mergedLeave ) ) {
            refresh = false;
            this.props.bulkCreateUpdateDeleteLeaves({ records: mergedLeave, filter });
        }

        if ( !isEmpty( attendancesToLock ) ) {
            this.props.lockAttendance({ employee_ids: attendancesToLock, lock: true, filter, refresh });
        }

        if ( !isEmpty( attendancesToUnlock ) ) {
            this.props.lockAttendance({ employee_ids: attendancesToUnlock, lock: false, filter, refresh });
        }
    }

    storeSidePanelAttendance = ( data ) => {
        const { pending_attendance, selectedAttendance } = this.state;
        const { leaveRecords, removedLeaveRecords, timeRecords, deletedTimeRecords, computedAttendance, locked } = data;

        if ( this.hasChanges( timeRecords, 'timeRecords' )
            || Object.keys( deletedTimeRecords ).length > 0
            || this.hasChanges( computedAttendance, 'attendance' )
            || this.hasChanges( leaveRecords, 'leaves' )
            || removedLeaveRecords.length > 0
            || locked !== selectedAttendance.locked
            ) {
            // scroll to top of the page
            window.scrollTo( 0, 0 );

            const payload = {
                id: selectedAttendance.id,
                time_records: this.hasChanges( timeRecords, 'timeRecords' ) || Object.keys( deletedTimeRecords ).length > 0
                    ? this.preparePayload( data, timeRecords, 'timeRecords' )
                    : [],
                attendance_records: this.hasChanges( computedAttendance, 'attendance' )
                    ? this.preparePayload( data, computedAttendance, 'attendance' )
                    : [],
                leave_records: removedLeaveRecords.length > 0 || this.hasChanges( leaveRecords, 'leaves' )
                    ? this.preparePayload( data, leaveRecords, 'leaves' )
                    : [],
                locked
            };

            const clonePendingAttendance = [...pending_attendance];
            const index = clonePendingAttendance.findIndex( ( x ) => x.id === selectedAttendance.id );
            if ( index > -1 ) {
                clonePendingAttendance[ index ] = payload;
            } else {
                clonePendingAttendance.push( payload );
            }

            this.setState( () => ({
                pending_attendance: clonePendingAttendance
            }) );
        }
    }

    hasChanges= ( records, type = '' ) => {
        if ( type === 'leaves' ) {
            return !isEmpty( records ) && records.filter( ( record ) => record.edited ).length > 0;
        }

        let hasChanges = false;
        !isEmpty( flattenDeep( Object.values( records ) ) ) && Object.keys( records ).forEach( ( shiftId ) => {
            hasChanges = records[ shiftId ].filter( ( record ) => record.edited ).length > 0;
        });

        return hasChanges;
    }

    preparePayload = ( data, records, type = '' ) => {
        switch ( type ) {
            case 'timeRecords': {
                const deletedTimeRecords = flatten( Object.values( data.deletedTimeRecords ) ).map( ( item ) =>
                    Object.assign({}, item, { timestamp: item.temp_key })
                );

                return {
                    create: flatten( Object.values( records ) ),
                    delete: deletedTimeRecords
                };
            }
            case 'leaves':
                return {
                    employee_id: data.employeeId,
                    date: data.selectedDay,
                    shift_id: data.expectedShifts[ 0 ].id,
                    leaves: records.map( ( leave ) => ({
                        start_datetime: `${records[ 0 ].date} ${leave.start_time}`,
                        end_datetime: leave.start_time > leave.end_time
                            ? `${moment( records[ 0 ].date ).add( 1, 'days' ).format( 'YYYY-MM-DD' )} ${leave.end_time}`
                            : `${records[ 0 ].date} ${leave.end_time}`,
                        type: leave.type.type,
                        leave_type_id: leave.type.value
                    }) )
                };
            case 'attendance': {
                const payload = {};
                let hasChanges = false;

                Object.keys( records ).forEach( ( shiftId ) => {
                    if ( !has( payload, shiftId ) ) {
                        payload[ shiftId ] = {};
                    }

                    hasChanges = records[ shiftId ].filter( ( record ) => record.edited ).length > 0;

                    records[ shiftId ].forEach( ( item ) => {
                        payload[ shiftId ][ item.type ] = item.value;
                    });
                });

                return hasChanges
                ? {
                    employeeId: data.employeeId,
                    selectedDay: data.selectedDay,
                    payload
                }
                : {};
            }

            default:
                return {};
        }
    }

    handleTableChanges = ( tableProps = this.attendanceTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });

        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    handleSearch = () => {
        let searchQuery = null;
        const { sort_by } = this.state;
        const { page, page_size } = this.props.pagination;
        const { status,
          start_date,
          end_date,
          employee_ids,
          department_ids,
          location_ids
        } = this.filter.state;

        const payload = {
            page_size,
            page,
            start_date,
            end_date,
            employee_ids,
            department_ids,
            location_ids,
            sort_by,
            status: [status]
        };

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();

            payload.search_terms = {
                all: searchQuery
            };
        }

        this.props.getAttendance( payload );
    };

    render() {
        const {
          selectedAttendance,
          pending_attendance,
          label,
          open,
          readyToRender
        } = this.state;

        const {
          shifts,
          leaves,
          loading,
          leaveTypes,
          attendance,
          pagination,
          loadingTable,
          notification,
          getSidePanelData
        } = this.props;

        const tableColumns = [
            {
                id: 'id',
                header: 'id',
                accessor: 'id',
                minWidth: 150,
                show: false
            },
            {
                id: 'employee_uid',
                header: 'Employee ID',
                sortable: true,
                accessor: ( row ) => row.employee.company_employee_id,
                minWidth: 125

            },
            {
                id: 'employee_name',
                header: 'Employee Name',
                sortable: true,
                accessor: ( row ) => row.employee.full_name,
                minWidth: 150

            },
            {
                id: 'date',
                header: 'Date',
                sortable: true,
                accessor: ( row ) => formatDate( row.date, DATE_FORMATS.API ) || 'N/A',
                minWidth: 100

            },
            {
                id: 'expected_shift',
                header: 'Expected Shift',
                sortable: true,
                accessor: ( row ) => this.getExpectedShifts( row.scheduled_shifts ),
                minWidth: 150

            },
            {
                id: 'clock_in',
                header: 'First Clock In',
                sortable: false,
                accessor: ( row ) => this.getTimeClocks( row.scheduled_shifts.assigned_shifts, 'clock_in' ),
                minWidth: 110
            },
            {
                id: 'clock_out',
                header: 'Last Clock Out',
                accessor: ( row ) => this.getTimeClocks( row.scheduled_shifts.assigned_shifts, 'clock_out' ),
                sortable: false,
                minWidth: 110
            },
            {
                id: 'tag',
                header: 'Tagging',
                sortable: false,
                minWidth: 110,
                render: ({ row }) => {
                    const tags = this.getAttendanceTag( row.attendance );
                    // eslint-disable-next-line no-confusing-arrow
                    const btnLabel = tags.map( ( tag ) => tag.includes( 'leave' ) ? 'on leave' : tag );
                    return tags.length ? (
                        btnLabel.map( ( tagLabel, index ) => (
                            <Button
                                key={ `${tagLabel}_${index}` }
                                disabled
                                label={ tagLabel }
                                className={ `attendance-tag ${tagLabel === 'on leave' ? 'approve-button' : 'decline-button'}` }
                                size="small"
                                type="neutral"
                            />
                        ) )
                    ) : null;
                },
                style: { flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center' }
            },
            {
                id: 'computed_attendance',
                header: 'Hours Worked',
                sortable: true,
                accessor: ( row ) => row.computed_attendance || '0:00',
                minWidth: 135
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 110,
                sortable: false,
                style: { justifyContent: 'flex-end' },
                render: ({ row }) => (
                    <div className="tableAction">
                        <button
                            className="lock-button"
                            onClick={ () => this.handleLockAttendance({
                                employee_ids: [row.id],
                                lock: !row.locked
                            }) }
                        >
                            <Icon name={ row.locked ? 'lockClose' : 'lockOpen' } className="icon" />
                        </button>
                        <Button
                            label={ <span>View</span> }
                            size="small"
                            type="neutral"
                            onClick={ () => {
                                this.setState({
                                    selectedAttendance: row
                                });
                                getSidePanelData({
                                    date: row.date,
                                    employee_id: row.employee.uid
                                });
                                this.toggleSidePanel();
                            } }
                        />
                    </div>
                )
            }];

        return (
            <PageWrapper>
                <Helmet
                    title="Attendance Computation"
                    meta={ [
                        { name: 'description', content: 'Description of Attendance Computation' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <SubHeader items={ TIME_AND_ATTENDANCE_SUBHEADER_ITEMS } />
                {
                    !loading && pending_attendance.length > 0 &&
                    <NotificationSubheader>
                        <p> <strong>There are { pending_attendance.length } unsaved changes made,</strong> These rows are highlighted. </p>
                        <Button
                            onClick={ () => this.saveAttendance() }
                            label={ <span>Save and Submit</span> }
                            size="small"
                            type="neutral"
                        />
                    </NotificationSubheader>
                }
                <StyledContainer>
                    { loading || !readyToRender
                      ? (
                          <div className="loader">
                              <LoadingStyles>
                                  <H2>Loading Attendance Computation</H2>
                                  <br />
                                  <H3>Please wait...</H3>
                              </LoadingStyles>
                          </div>
                        )
                      : (
                          <AttendanceComputationWrapper>
                              <div className="content">
                                  <div className="heading">
                                      <H3>Attendance Computation</H3>
                                  </div>
                                  <div className="header">
                                      <div className="search-wrapper">
                                          <Input
                                              className="search"
                                              id="search"
                                              ref={ ( ref ) => { this.searchInput = ref; } }
                                              onKeyPress={ ( e ) => {
                                                  e.charCode === 13 && this.handleSearch();
                                              } }
                                              placeholder="Search by Employee Name"
                                              addon={ {
                                                  content: <Icon name="search" />,
                                                  placement: 'right'
                                              } }
                                          />
                                          <Button
                                              id="button-filter"
                                              label={ <span><Icon name="filter" className="filter-icon" /> Filter</span> }
                                              type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                              onClick={ this.toggleFilter }
                                          />
                                      </div>
                                      <span className="dropdown-wrapper">
                                          <Button
                                              label="Regenerate"
                                              type="action"
                                              onClick={ () => this.regenerateOrDownloadAttendance() }
                                          />
                                          <Button
                                              alt
                                              label="Batch Add Attendance"
                                              type="action"
                                              onClick={ () => { browserHistory.push( '/time/attendance-computations/add/batch', true ); } }
                                          />
                                          <SalDropdown
                                              label="More Options"
                                              dropdownItems={ this.state.dropdownItems }
                                          />
                                      </span>
                                  </div>
                                  <div style={ { display: this.state.show_filter ? 'block' : 'none' } }>
                                      <Filter
                                          ref={ ( ref ) => { this.filter = ref; } }
                                          toggleFilter={ this.toggleFilter }
                                          onFilter={ ( data ) => this.tablePaginationAndFilter( data ) }
                                      />
                                  </div>
                                  <Table
                                      className="table"
                                      data={ attendance }
                                      loading={ loadingTable }
                                      columns={ tableColumns }
                                      selectable
                                      onDataChange={ this.handleTableChanges }
                                      ref={ ( ref ) => { this.attendanceTable = ref; } }
                                      onSortingChange={ this.onSortingChange }
                                      pages={ pagination.total }
                                      pageSize={ pagination.page_size }
                                      manual
                                  />
                              </div>
                              <AttendanceSidePanel
                                  open={ open }
                                  shifts={ shifts }
                                  leaves={ leaves }
                                  loading={ loadingTable }
                                  leaveTypes={ leaveTypes }
                                  lockAttendance={ ( data ) => this.handleLockAttendance( data ) }
                                  pendingAttendance={ pending_attendance }
                                  selectedAttendance={ selectedAttendance }
                                  ref={ ( ref ) => { this.sidePanel = ref; } }
                                  toggleSidePanel={ () => this.toggleSidePanel() }
                                  storeSidePanelAttendance={ ( data ) => this.storeSidePanelAttendance( data ) }
                              />
                          </AttendanceComputationWrapper>
                      )}
                </StyledContainer>
                <div
                    className="loader"
                    style={ { display: this.props.loading ? 'none' : 'block', position: 'fixed', bottom: '0', background: '#fff', zIndex: 1 } }
                >
                    <FooterTablePagination
                        page={ pagination.page }
                        pageSize={ pagination.page_size }
                        pagination={ pagination }
                        onPageChange={ ( data ) => this.tablePaginationAndFilter( data, pagination.page_size ) }
                        onPageSizeChange={ ( data ) => this.tablePaginationAndFilter( 1, data ) }
                        paginationLabel={ label }
                    />
                </div>

            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    shifts: makeSelectShifts(),
    leaves: makeSelectLeaves(),
    leaveTypes: makeSelectLeaveTypes(),
    loading: makeSelectLoading(),
    loadingTable: makeSelectLoadingTable(),
    attendance: makeSelectAttendance(),
    pagination: makeSelectPagination(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        attendanceComputationActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( attendanceComputation );
