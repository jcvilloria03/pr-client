import styled from 'styled-components';
import { Container } from 'reactstrap';

export const PageWrapper = styled.div`
.selected .fill {
    fill: #fff;
}

.selected .stroke {
    stroke: #fff;
}

.fill {
    fill: rgb(0,165,226);
}

.stroke {
    stroke: rgb(0,165,226);
}
`;

export const StyledContainer = styled( Container )`
    min-height: calc(100vh - 80px);
    padding: 10px 0;
    padding-bottom: 50px;
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const NotificationSubheader = styled.div`
    height: 50px;
    color: rgb(232, 230, 227);
    background-color: rgb(127, 18, 18);
    padding: 0 10%;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;

    p {
      margin-bottom: 0;
      margin-right: 1rem;
    }
`;

export const AttendanceComputationWrapper = styled.div`
  width: 100%;

  .title {
    font-weight: bold;
  }

  .header {
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    .regenerate {
      height: 40px;
      width: 40px;

      &:focus, &:active:focus {
        outline: none;
        background-color: #fff;
        border-radius: 50%;
        color: #4ABA4A;
      }
    }
  }

  .approve-button {
    background-color: #83d24b !important;
    color: white !important;
  }

  .decline-button {
    background-color: #e02020 !important;
    color: white !important;
  }

  && .attendance-tag {
    display: flex;
    justify-content: center;
    cursor: default;
    font-size: 10px;
    font-weight: 600;
    padding: 1px 5px;
    border: 0;
    width: 80px;
    text-transform: capitalize;
  }

  .content {
        background-color:#fff;
        padding: 1rem;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto;

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .header {
            width: 100%;
            display: flex;
            justify-content: space-between;
            align-items: flex-end;
            flex-direction: row;
            margin-bottom: 10px;

            button {
              border-radius: 2px;
            }

            .search-wrapper {
                flex-grow: 1;
                display: flex;
                align-items: center;
                flex-direction: row;
                justify-content: flex-start;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 2px;
                    margin-right: 2px;

                    .input-group {
                      height: 35px;

                      input {
                        height: 35px;
                        border: none;
                      }
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }

                .filter-icon > svg {
                    height: 10px;
                }
            }

            .dropdown-wrapper {
              display: flex;
              flex-direction: row;
              align-items: center;
              justify-content: flex-end;

              .sl-c-text-align-right {
                font-size: 14px;
              }

              :last-child {
                margin-left: 1px;
              }

              .dropdown-menu {
                text-align: left;
                font-size: 14px;
              }
            }
        }

        .table {
          font-size: 14px;

          .details {
            padding: 1rem;
            margin: 20px;
            background-color: #fafbfc;
            border: 1px solid #f0f4f6;

            .head {
              display: flex;
              justify-content: space-between;
            }
          }

          .tableAction {
            display: flex;
            justify-content: space-around;
            align-items: center;

            .lock-button {
              &:focus {
                outline: 0;
              }

              &:hover {
                cursor: pointer;
              }

              .icon {
                font-size: 15px;
                color: black;
                display: flex;
                width: 12px;
                margin-right: 0.5rem;

                > i {
                    align-self: center;
                }
              }
            }

          }

        }
    }
`;
