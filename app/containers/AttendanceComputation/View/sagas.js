/* eslint-disable no-prototype-builtins */
/* eslint-disable camelcase */
/* eslint-disable no-inner-declarations */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import moment from 'moment';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import {
  setShifts,
  setLoading,
  setLoadingTable,
  setAttendance,
  setLeaves,
  setLeaveTypes,
  setPagination,
  setNotification
} from './actions';

import {
    REGENERATE,
    INITIALIZE,
    NOTIFICATION,
    DOWNLOAD,
    GET_SIDEPANEL_DATA,
    GET_ATTENDANCE,
    LOCK_ATTENDANCE,
    BULK_CREATE_DELETE_TIME_RECORDS,
    BULK_CREATE_UPDATE_DELETE_LEAVES,
    EDIT_COMPUTED_ATTENDANCE
} from './constants';

/**
 * initialize page
 */
export function* initializePage() {
    try {
        yield put( setLoading( true ) );
        const companyId = company.getLastActiveCompanyId();

        const attendance = yield call( Fetch, '/attendance/records', { method: 'POST',
            data: {
                page: 1,
                ids: [],
                page_size: 10,
                sort_by: { date: 'DESC', employee_name: 'ASC' },
                company_id: companyId,
                employee_status: ['active'],
                end_date: moment().format( 'YYYY-MM-DD' ),
                start_date: moment().subtract( 1, 'months' ).format( 'YYYY-MM-DD' )
            }
        });

        const { attendance_records, ...pagination } = attendance.data;
        yield [
            put( setAttendance( attendance_records ) ),
            put( setPagination( pagination ) )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Get Attendance
 */
export function* getAttendance({ payload }) {
    const {
      page_size,
      page,
      start_date,
      end_date,
      employee_ids,
      department_ids,
      location_ids,
      search_terms,
      sort_by,
      status
  } = payload;

    try {
        yield put( setLoadingTable( true ) );
        const companyId = company.getLastActiveCompanyId();

        /**
         * check if there ia a payload else return default values
         * @param {*} param
         * @param {*} defaultValue
         * @returns
         */
        function checkPayload( type, value, defaultValue ) {
            return Object.keys( payload ).length > 0 && payload.hasOwnProperty( type )
                ? value
                : defaultValue;
        }

        const data = {
            company_id: companyId,
            page: checkPayload( 'page', page, 1 ),
            ids: checkPayload( 'employee_ids', employee_ids, []),
            filters: { employee_status: checkPayload( 'status', status, ['active']) },
            page_size: checkPayload( 'page_size', page_size, 10 ),
            employee_status: checkPayload( 'status', status, ['active']),
            sort_by: checkPayload( 'sort_by', sort_by, { date: 'ASC', employee_name: 'ASC' }),
            start_date: checkPayload( 'start_date', start_date, moment().subtract( 1, 'months' ).format( 'YYYY-MM-DD' ) ),
            end_date: checkPayload( 'end_date', end_date, moment().format( 'YYYY-MM-DD' ) )
        };

        if ( payload.hasOwnProperty( 'department_ids' ) && department_ids.length > 0 ) {
            data.filters.department = department_ids;
        }

        if ( payload.hasOwnProperty( 'location_ids' ) && location_ids.length > 0 ) {
            data.filters.location = location_ids;
        }

        if ( payload.hasOwnProperty( 'search_terms' ) && search_terms ) {
            data.search_terms = search_terms;
        }

        const attendance = yield call( Fetch, '/attendance/records', { method: 'POST', data });

        const { attendance_records, ...pagination } = attendance.data;
        yield [
            put( setAttendance( attendance_records ) ),
            put( setPagination( pagination ) )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * Get Sidepanel Data
 */
export function* getSidePanelData({ payload }) {
    const { date, employee_id } = payload;

    try {
        yield put( setLoadingTable( true ) );
        const companyId = company.getLastActiveCompanyId();

        const [ leaveTypes, leaves, shifts ] = yield [
            call( Fetch, `/company/${companyId}/leave_types/search?credit_required=true`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/hours_worked/leaves`, { method: 'POST',
                data: {
                    date,
                    employee_id
                }}),
            call( Fetch, `/company/${companyId}/shifts`, { method: 'GET',
                params: {
                    date_from: date,
                    date_to: date,
                    employee_ids: [employee_id]
                }})
        ];

        yield [
            put( setLeaves( leaves.data ) ),
            put( setLeaveTypes( leaveTypes.data ) ),
            put( setShifts( shifts.data ) )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * Bulk Create/Delete Time Records
 */
export function* bulkCreateDeleteTimeRecords({ payload }) {
    const { records, filter } = payload;
    try {
        yield put( setLoadingTable( true ) );
        const companyId = company.getLastActiveCompanyId();

        const response = yield call( Fetch, `/company/${companyId}/time_records/bulk_create_or_delete`, { method: 'POST',
            data: records
        });

        yield call( fetchJobStatus, { jobId: response.job_ids[ 0 ], type: 'calculate', filter });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * Bulk Create/Update/Delete Leaves
 * @param {*} payload
 * @returns
  */
export function* bulkCreateUpdateDeleteLeaves({ payload }) {
    const { records, filter } = payload;

    const data = {
        data: records
    };
    try {
        yield put( setLoadingTable( true ) );
        const companyId = company.getLastActiveCompanyId();

        const response = yield call( Fetch, `/company/${companyId}/hours_worked/leaves/bulk_create_or_update_or_delete`, { method: 'POST', data });

        yield call( fetchJobStatus, { jobId: response.job_ids[ 0 ], type: 'calculate', filter });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * Edit Computed Attendance
 *
 * @param {*} payload
 * @returns
  */
export function* editComputedAttendance({ payload }) {
    const { records, filter } = payload;

    try {
        yield put( setLoadingTable( true ) );

        yield records.map( ( record ) => call( Fetch, `/attendance/${record.employeeId}/${record.selectedDay}/edit`, {
            method: 'PUT', data: record.payload
        }) );

        yield call( getAttendance, { payload: { ...filter }});

        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully saved',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * locks Employee Attendance
 * @param {array} data
 */
export function* lockEmployeeAttendance({ payload }) {
    const { employee_ids, lock, filter, refresh = true } = payload;
    const companyId = company.getLastActiveCompanyId();

    const data = {
        attendance_record_ids: employee_ids,
        company_id: companyId
    };

    try {
        if ( employee_ids.length ) {
            if ( refresh ) {
                yield put( setLoadingTable( true ) );
            }

            yield call( Fetch, `/attendance/${lock ? 'lock' : 'unlock'}/bulk`, { method: 'POST', data });

            if ( refresh ) {
                yield call( getAttendance, { payload: { ...filter }});
            }
        } else {
            yield call( notifyUser, {
                show: true,
                message: 'Please select at least one employee',
                title: 'Error',
                type: 'error',
                autoDismiss: 10
            });
        }
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        if ( refresh ) {
            yield put( setLoadingTable( false ) );
        }
    }
}

/**
 * Export Employee Data
 * @param {array} data
 */
export function* downloadAttendance({ payload }) {
    const {
        start_date,
        end_date,
        employee_ids,
        department_ids,
        location_ids,
        sort_by,
        status
    } = payload;

    try {
        yield put( setLoadingTable( true ) );
        const companyId = company.getLastActiveCompanyId();

        const data = {
            company_id: companyId,
            filters: { employee_status: status },
            employee_status: status,
            ids: employee_ids,
            sort_by,
            start_date,
            end_date
        };

        if ( payload.hasOwnProperty( 'department_ids' ) && department_ids.length > 0 ) {
            data.filters.department = department_ids;
        }

        if ( payload.hasOwnProperty( 'location_ids' ) && location_ids.length > 0 ) {
            data.filters.location = location_ids;
        }

        const response = yield call( Fetch, '/attendance/records/export/job', { method: 'POST', data });
        yield call( delay, 3000 );
        yield call( fetchAttendanceExportJobStatus, { jobId: response.data.job_id });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

function* fetchAttendanceExportJobStatus( payload ) {
    const { jobId } = payload;
    const response = yield call( Fetch, `/attendance/records/export/job/${jobId}`, { method: 'GET' });
    if ( response.data.status === 'done' ) {
        window.open( response.data.result, '_self' );
    } else if ( response.data.status === 'error' ) {
        yield call( notifyUser, {
            title: 'Attendance Export',
            message: response.data.error,
            show: true,
            type: 'error'
        });
    } else {
        yield call( delay, 3000 );
        yield call( fetchAttendanceExportJobStatus, payload );
    }
}

/**
 * Regenerate attendance computation
 *
 * @returns {Promise}
 */
export function* regenerate({ payload }) {
    const {
        start_date,
        end_date,
        employee_ids,
        department_ids,
        location_ids,
        search_terms,
        status,
        page,
        sort_by,
        page_size
    } = payload;

    try {
        yield put( setLoadingTable( true ) );
        const companyId = company.getLastActiveCompanyId();
        const dates = [];

        let startDate = moment( start_date );
        const endDate = moment( end_date );

        while ( startDate.isBefore( endDate ) || startDate.isSame( moment( end_date ) ) ) {
            dates.push( startDate.format( 'YYYY-MM-DD' ) );
            startDate = startDate.add( 1, 'days' );
        }

        const data = {
            company_id: companyId,
            filter: { employee_status: status },
            employee_ids,
            search_terms,
            dates
        };
        let jobID = null;

        if ( payload.hasOwnProperty( 'department_ids' ) && department_ids.length > 0 ) {
            data.filter.department = department_ids;
        }

        if ( payload.hasOwnProperty( 'location_ids' ) && location_ids.length > 0 ) {
            data.filter.location = location_ids;
        }

        if ( payload.hasOwnProperty( 'search_terms' ) && search_terms ) {
            data.search_terms = search_terms;
        }
        /**
         * calculate attendance
         */
        function* calculate() {
            const response = yield call( Fetch, '/attendance/calculate/bulk', {
                method: 'POST',
                data
            });

            const filter = {
                start_date,
                end_date,
                employee_ids,
                department_ids,
                location_ids,
                status,
                page,
                sort_by,
                page_size
            };

            if ( search_terms ) {
                filter.search_terms = {
                    all: search_terms
                };
            }

            if ( response.data ) {
                jobID = response.data.job_id;
                yield call( fetchJobStatus, {
                    jobId: response.data.job_id,
                    type: 'regenarate',
                    filter
                });
            } else {
                yield call( calculate, data );
            }
        }

        if ( !jobID ) yield call( calculate, data );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * Get the calculation status
 */
function* fetchJobStatus( payload ) {
    const { jobId, type, filter } = payload;
    const response = yield call( Fetch, `/attendance/job/${jobId}`, { method: 'GET' });

    if ( response.data.status === 'FINISHED' ) {
        if ( type === 'calculate' ) {
            yield call( notifyUser, {
                title: 'Success',
                message: 'Record successfully saved',
                show: true,
                type: 'success'
            });
        }

        yield call( getAttendance, { payload: { ...filter }});
    } else if ( response.data.status === 'FAILED' ) {
        yield call( displayJobErrors, jobId );
    } else {
        yield call( delay, 5000 );
        yield call( fetchJobStatus, { jobId, type, filter });
    }

    return true;
}

/**
 * Get the job errors
 */
function* displayJobErrors( jobId ) {
    const response = yield call( Fetch, `/attendance/job/${jobId}/errors`, { method: 'GET' });

    const uniqueErrors = new Set( response.data.map( ({ message }) => message ) );
    const errorMessages = Array.from( uniqueErrors );

    yield call( delay, 5000 );
    yield call( notifyError, errorMessages );
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const title = get( error, 'response.statusText' ) || 'Error';
    const message = get( error, 'response.data.message' ) ||
        get( error, 'response.data.Message' ) ||
        error.statusText;
    const payload = {
        show: true,
        title,
        message,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 2000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializePage );
}

/**
 * Watcher for GET_ANNOUNCEMENTS
 */
export function* watchForGetAttendance() {
    const watcher = yield takeEvery( GET_ATTENDANCE, getAttendance );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_SIDEPANEL_DATA
 */
export function* watchForGetSidePanelData() {
    const watcher = yield takeEvery( GET_SIDEPANEL_DATA, getSidePanelData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for BULK_CREATE_DELETE_TIME_RECORDS
 */
export function* watchForBulkCreateDeleteTimeRecords() {
    const watcher = yield takeEvery( BULK_CREATE_DELETE_TIME_RECORDS, bulkCreateDeleteTimeRecords );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for BULK_CREATE_UPDATE_DELETE_LEAVES
 */
export function* watchForBulkCreateUpdateDeleteLeaves() {
    const watcher = yield takeEvery( BULK_CREATE_UPDATE_DELETE_LEAVES, bulkCreateUpdateDeleteLeaves );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for EDIT_COMPUTED_ATTENDANCE
 */
export function* watchForEditComputedAttendance() {
    const watcher = yield takeEvery( EDIT_COMPUTED_ATTENDANCE, editComputedAttendance );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for LOCK_ATTENDANCE
 */
export function* watchForLockAttendance() {
    const watcher = yield takeEvery( LOCK_ATTENDANCE, lockEmployeeAttendance );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForRegenerate() {
    const watcher = yield takeLatest( REGENERATE, regenerate );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForDownload() {
    const watcher = yield takeLatest( DOWNLOAD, downloadAttendance );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForInitializePage() {
    const watcher = yield takeLatest( INITIALIZE, initializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForDownload,
    watchForRegenerate,
    watchForNotifyUser,
    watchForGetSidePanelData,
    watchForGetAttendance,
    watchForLockAttendance,
    watchForInitializePage,
    watchForReinitializePage,
    watchForEditComputedAttendance,
    watchForBulkCreateDeleteTimeRecords,
    watchForBulkCreateUpdateDeleteLeaves
];
