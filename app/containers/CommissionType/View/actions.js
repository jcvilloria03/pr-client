import {
    DEFAULT_ACTION, DELETE_COMMISSION_TYPE, GET_COMMISSION_TYPE, NOTIFICATION, UPDATE_COMMISSION, UPDATE_GET_COMMISSION_TYPE,
    AVAILABLE
} from './constants';

/**
 *
 * CommissionType actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Get CommissionType actions
 *
 */
export function getCommissionType() {
    return {
        type: GET_COMMISSION_TYPE
    };
}
/**
 *
 * Update CommissionType actions
 *
 */
export function updateCommissionType( IDs ) {
    return {
        type: UPDATE_GET_COMMISSION_TYPE,
        payload: IDs
    };
}
/**
 *
 * Update Commission actions
 *
 */
export function updatecommission( payload ) {
    return {
        type: UPDATE_COMMISSION,
        payload
    };
}
/**
 *
 * Update Commission actions
 *
 */
export function updatecommissionvalid( payload ) {
    return {
        type: AVAILABLE,
        payload
    };
}
/**
 * Delete CommissionType
 * @param {array} IDs
 */
export function deleteCommissionType( IDs ) {
    return {
        type: DELETE_COMMISSION_TYPE,
        payload: IDs
    };
}
/**
 * Display a notification to a user
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
