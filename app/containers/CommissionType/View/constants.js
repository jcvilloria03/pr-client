/*
 *
 * CommissionType constants
 *
 */
export const DEFAULT_ACTION = 'app/CommissionType/View/DEFAULT_ACTION';
export const GET_COMMISSION_TYPE = 'app/CommissionType/View/GET_COMMISSION_TYPE';
export const SET_COMMISSION_TYPE = 'app/CommissionType/View/SET_COMMISSION_TYPE';
export const DELETE_COMMISSION_TYPE = 'app/CommissionType/View/DELETE_COMMISSION_TYPE';
export const DELETED_COMMISSION_TYPE = 'app/CommissionType/View/DELETED_COMMISSION_TYPE';
export const NOTIFICATION = 'app/CommissionType/View/NOTIFICATION ';
export const NOTIFICATION_SAGA = 'app/CommissionType/View/NOTIFICATION_SAGA ';
export const UPDATE_GET_COMMISSION_TYPE = 'app/CommissionType/View/UPDATE_GET_COMMISSION_TYPE ';
export const UPADATE_SET_COMMISSSION_TYPE = 'app/CommissionType/View/UPADATE_SET_COMMISSSION_TYPE ';
export const UPDATE_COMMISSION = 'app/CommissionType/View/UPDATE_COMMISSION';
export const UPDATED_COMMISSION = 'app/CommissionType/View/UPDATED_COMMISSION ';
export const LOADING = 'app/CommissionType/View/LOADING';
export const AVAILABLE = 'app/CommissionType/View/ AVAILABLE ';
export const SET_AVAILABLE = 'app/CommissionType/View/SET_AVAILABLE ';
export const BTN_LOADING = 'app/CommissionType/View/BTN_LOADING ';
export const EDIT_LOADING = 'app/CommissionType/View/EDIT_LOADING ';

