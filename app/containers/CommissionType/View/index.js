/* eslint-disable react/sort-comp */
/* eslint-disable no-confusing-arrow */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';

import { clone, isEqual } from 'lodash';

import { isAnyRowSelected, getIdsOfSelectedRows } from 'components/Table/helpers';
import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';
import { H5, H3, H2, H1, P } from 'components/Typography';
import SalDropdown from 'components/SalDropdown';
import SnackBar from 'components/SnackBar';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Switch from 'components/Switch2';
import Table from 'components/Table';
import Input from 'components/Input';
import Modal from 'components/Modal';
import Icon from 'components/Icon';

import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';

import {
    makeSelectListCommissionType,
    makeSelectupdatecommission,
    makeSelectNotification,
    makeSelectEditLoading,
    makeSelectupdateList,
    makeSelectbtnLoading,
    makeSelectProducts,
    makeSelectLoading,
    makeSelectupdate
} from './selectors';
import * as commissionTypeAction from './actions';

import {
    PageWrapper,
    LoadingStyles
} from './styles';

/**
 *
 * CommissionType
 *
 */
export class CommissionType extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        updatecommissionvalid: React.PropTypes.func,
        deleteCommissionType: React.PropTypes.func,
        updateCommissionType: React.PropTypes.func,
        getCommissionType: React.PropTypes.func,
        updatecommission: React.PropTypes.func,
        ListCommissionType: React.PropTypes.array,
        products: React.PropTypes.array,
        updatecommissionedit: React.PropTypes.bool,
        updateData: React.PropTypes.bool,
        btnLoading: React.PropTypes.bool,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    static defaultProps = {
        loading: false
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            },
            tableData: [],
            delete_label: '',
            click: false,
            editData: null,
            check: false,
            validName: 'name',
            validMax_on_taxable: '1',
            isEdit: false,
            searchTerm: ''
        };

        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
    }
    componentDidMount() {
        this.props.getCommissionType();
    }
    /**
     *
     * CommissionType render method
     *
     */
    componentWillReceiveProps( nextProps ) {
        const commissionTypesListChanged = !isEqual( nextProps.ListCommissionType, this.props.ListCommissionType );

        this.setState( ( prevState ) => ({
            tableData: nextProps.ListCommissionType,
            isEdit: false,
            pagination: {
                total: nextProps.ListCommissionType.length,
                per_page: 10,
                current_page: commissionTypesListChanged ? 1 : prevState.pagination.current_page,
                last_page: Math.ceil( nextProps.ListCommissionType.length / 10 ),
                to: 0
            }
        }), () => this.handleTableChanges() );
    }

    handleTableChanges = ( tableProps = this.CommissionTypeTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.state.tableData.length });
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    getDropdownItems() {
        return [
            {
                children: <div>Delete</div>,
                onClick: () => this.DeleteModel.toggle()
            }
        ];
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.CommissionTypeTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.CommissionTypeTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    handleSearch = ( term = '' ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: 10,
                current_page: 1
            },
            searchTerm: term
        }), () => {
            this.handleTableChanges();
        });
    }
    stopEditing = () => {
        this.setState({ click: false, editData: null });
    }
    saveEdited() {
        const data = {
            basis: this.state.editData.basis,
            beingEdited: true,
            company_id: company.getLastActiveCompanyId(),
            deleted_at: this.state.editData.deleted_at,
            fully_taxable: this.state.editData.fully_taxable,
            id: this.state.editData.id,
            isInUse: true,
            max_non_taxable: this.state.editData.max_non_taxable,
            name: this.state.editData.name,
            type_name: this.state.editData.type_name,
            unavailableName: false
        };
        this.props.updatecommission( data );
        setTimeout( () => {
            if ( this.validatedNameExist() ) {
                this.props.updatecommissionvalid( data );
                this.setState({ click: false, isEdit: true, editData: null });
                setTimeout( async () => {
                    await this.props.getCommissionType();
                }, 2000 );
            }
        }, 3000 );
    }
    Edited( row ) {
        return this.state.editData && this.state.editData.id === row.id;
    }
    editState( property, values ) {
        this.setState({
            editData: {
                ...this.state.editData,
                [ property ]: values
            }
        });
    }
    startEditingRow( row ) {
        this.setState({
            editData: clone( row ),
            click: true
        });
        this.props.updateCommissionType({ other_income_type_id: row });
    }
    handleDelete = () => {
        const payload = getIdsOfSelectedRows( this.CommissionTypeTable );
        this.props.deleteCommissionType({ ids: payload, company_id: company.getLastActiveCompanyId() });
        this.DeleteModel.toggle();
        setTimeout( async () => {
            await this.props.getCommissionType();
        }, 3000 );
    }
    validatedNameExist = () => {
        let valid = true;
        if ( this.props.updatecommissionedit === false ) {
            const message = <p style={ { color: 'red', 'font-size': '14px' } }>Record name already exists</p>;
            this.typeName.setState({ error: true, errorMessage: message });
            valid = false;
        }
        return valid;
    }

    /**
     *
     * CommissionType render method
     *
     */
    render() {
        const data = [
            {
                value: 'FIXED',
                label: 'Fixed'
            }
        ];
        const { loading } = this.props;
        const hasSelectedItems = isAnyRowSelected( this.CommissionTypeTable );
        const { searchTerm } = this.state;

        let displayData = this.props.ListCommissionType;

        if ( searchTerm ) {
            const regex = new RegExp( searchTerm, 'i' );
            displayData = displayData.filter( ( row ) => ( regex.test( row.name ) ) );
        }

        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'name',
                header: 'Name',
                minWidth: 100,
                sortable: true,
                render: ({ row }) => this.Edited( row ) && this.state.click === true ? (
                    <div style={ { width: '100%' } } className="input-group">
                        <Input
                            className={ this.state.validName === '' ? 'content-input-error' : '' }
                            id="name"
                            type="text"
                            value={ this.state.editData.name }
                            ref={ ( ref ) => { this.typeName = ref; } }
                            onChange={ ( values ) => {
                                this.editState( 'name', values );
                                this.setState({
                                    validName: values
                                });
                            } }
                        />
                        {this.state.validName === '' ? <p style={ { color: 'red', 'font-size': '14px' } }>Please Input valid Data</p> : ''}
                    </div>
                ) : (
                    <div>{row.name}</div>
                )
            },
            {
                id: 'basis',
                header: 'Amount Basis',
                minWidth: 80,
                sortable: true,
                render: ({ row }) => this.Edited( row ) && this.state.click === true ? (
                    <div style={ { width: '100%' } }>
                        <select
                            className="amount-basis-select"
                            name="basis"
                            id="basis"
                            disabled={ !this.props.updateData }
                        >
                            {data.map( ( basis ) => (
                                <option key={ basis.value } value={ basis.value }>{basis.label}</option>
                            ) )}
                        </select>
                    </div>
                ) : (
                    <div>{row.basis === 'FIXED' ? 'Fixed' : ''}</div>
                )
            },
            {
                id: 'fully_taxable',
                header: 'Fully Taxable',
                minWidth: 80,
                sortable: true,
                render: ({ row }) => this.Edited( row ) && this.state.click === true ? (
                    <div style={ { width: '100%' } }>
                        {this.props.updateData === true ?
                            <Switch
                                id="basis"
                                checked={ this.state.editData.fully_taxable }
                                ref={ ( ref ) => { this.fully_taxable = ref; } }
                                onChange={ ( values ) => {
                                    this.editState( 'fully_taxable', values );
                                    this.setState({
                                        validMax_on_taxable: 90000,
                                        editData: {
                                            ...this.state.editData,
                                            fully_taxable: values,
                                            max_non_taxable: values === false ?
                                            this.state.editData.max_non_taxable === null ?
                                            '90000.00' : this.state.editData.max_non_taxable : this.state.editData.max_non_taxable
                                        }
                                    });
                                } }
                            /> : <Switch
                                id="basis"
                                checked={ this.state.editData.fully_taxable }
                                ref={ ( ref ) => { this.fully_taxable = ref; } }
                                disabled
                            />
                      }
                    </div>
                ) : (
                    <div>{row.fully_taxable === true ? 'Yes' : 'No'}</div>
                )
            },
            {
                id: 'max_non_taxable',
                header: 'Maximum Non Taxable ',
                minWidth: 80,
                sortable: true,
                render: ({ row }) => this.Edited( row ) && this.state.click === true ? (
                    <div style={ { width: '100%' } } className="input-group">
                        {this.state.editData.fully_taxable !== true ?
                            <Input
                                className={ this.state.validMax_on_taxable === '' ?
                                'content-input-error' : Math.sign( this.state.validMax_on_taxable )
                                === 0 ? 'content-input-error' : isNaN( this.state.validMax_on_taxable ) ? 'content-input-error' : '' }
                                id="max_non_taxable"
                                type="text"
                                value={ this.state.editData.fully_taxable === false ? this.state.editData.max_non_taxable === null ? '90000.00' : this.state.editData.max_non_taxable : '' }
                                ref={ ( ref ) => { this.max_non_taxable = ref; } }
                                onChange={ ( valuess ) => {
                                    this.setState({
                                        validMax_on_taxable: valuess,
                                        editData: {
                                            ...this.state.editData,
                                            max_non_taxable: valuess
                                        }
                                    });
                                }
                                 }
                            />
                           : <Input
                               id="name"
                               type="text"
                               ref={ ( ref ) => { this.max_non_taxable = ref; } }
                               disabled
                           />
                        }
                        {this.state.editData.fully_taxable === false && this.state.validMax_on_taxable === '' ?
                            <p style={ { color: 'red', 'font-size': '14px' } }>Please Input valid Data</p>
                        : isNaN( this.state.validMax_on_taxable ) ?
                            <p style={ { color: 'red', 'font-size': '14px' } }>Only numeric value is allowed</p> :
                            this.state.editData.fully_taxable === false && Math.sign( this.state.validMax_on_taxable ) === 0 ?
                                <p style={ { color: 'red', 'font-size': '14px' } }>This field minimal value is 1</p> : '' }
                    </div>
                ) : (
                    <div>{row.max_non_taxable === null ? 'N/A' : row.max_non_taxable }</div>
                )
            },
            {
                id: 'id',
                header: ' ',
                accessor: 'actions',
                minWidth: 80,
                sortable: false,
                style: { justifyContent: 'end' },
                render: ({ row }) => this.Edited( row ) && this.state.click === true ? (
                    <div>
                        <Button
                            className="button"
                            label={ <span>Cancel</span> }
                            type="grey"
                            size="small"
                            onClick={ () => this.stopEditing() }
                        />
                        <Button
                            className="button"
                            label={ <span>Save</span> }
                            type="action"
                            size="small"
                            onClick={ () => this.saveEdited() }
                        />
                    </div>
                ) : (
                    <Button
                        className="button"
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => {
                            this.startEditingRow( row );
                        } }
                    />
                )
            }
        ];
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        return (
            <div style={ { display: 'flex' } }>
                <Helmet
                    title="Commission Types"
                    meta={ [
                        { name: 'description', content: 'Description of Commission Types' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="Confirm Your Action"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.DeleteModel = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.DeleteModel.toggle()
                        },
                        {
                            type: 'darkRed',
                            label: 'Yes',
                            onClick: this.handleDelete
                        }
                    ] }
                />
                <PageWrapper>
                    <div className="loader" style={ { display: loading ? '' : 'none' } }>
                        <LoadingStyles>
                            <H2>Loading Commission Types</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    </div>
                    <div className="content" style={ { display: loading ? 'none' : '' } }>
                        <div className="heading">
                            <H1 noBottomMargin>Commission Types</H1>

                            <P noBottomMargin>View and update commission types. You can add, edit, or delete commission types.</P>

                            <div className="buttons-wrapper">
                                <Button
                                    label="Add Commissions"
                                    type="neutral"
                                    onClick={ () => browserHistory.push( '/payroll/commissions/add/', true ) }
                                />
                                <Button
                                    label="Add Commission Types"
                                    type="action"
                                    onClick={ () => browserHistory.push( '/company-settings/payroll/commission-types/add', true ) }
                                />
                            </div>
                        </div>
                        <div className="title">
                            <div className="search-wrapper">
                                <H5 noBottomMargin>Commission Type List</H5>

                                <Input
                                    id="search"
                                    className="search"
                                    onChange={ ( value ) => this.handleSearch( value.toLowerCase() ) }
                                    addon={ {
                                        content: <Icon name="search" />,
                                        placement: 'right'
                                    } }
                                    ref={ ( ref ) => { this.searchInput = ref; } }
                                />
                            </div>
                            <div className="actions-wrapper">
                                <P noBottomMargin>{hasSelectedItems ? this.state.delete_label : this.state.label}</P>

                                {hasSelectedItems && (
                                    <span style={ { marginLeft: '14px' } }>
                                        <SalDropdown dropdownItems={ this.getDropdownItems() } />
                                    </span>
                                )}
                            </div>
                        </div>
                        <div>
                            <Table
                                className="commission-table"
                                onDataChange={ this.handleTableChanges }
                                loading={ this.props.btnLoading }
                                columns={ tableColumns }
                                data={ displayData }
                                ref={ ( ref ) => { this.CommissionTypeTable = ref; } }
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    this.setState({ delete_label: formatDeleteLabel( selectionLength ) });
                                } }
                                page={ this.state.pagination.current_page - 1 }
                                pageSize={ this.state.pagination.per_page }
                                pages={ this.state.pagination.total }
                                selectable
                                external
                            />

                            <div>
                                <FooterTablePaginationV2
                                    page={ this.state.pagination.current_page }
                                    pageSize={ this.state.pagination.per_page }
                                    pagination={ this.state.pagination }
                                    onPageChange={ this.onPageChange }
                                    onPageSizeChange={ this.onPageSizeChange }
                                    paginationLabel={ this.state.label }
                                    fluid
                                />
                            </div>
                        </div>
                    </div>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    ListCommissionType: makeSelectListCommissionType(),
    updatecommissionedit: makeSelectupdatecommission(),
    notification: makeSelectNotification(),
    editLoading: makeSelectEditLoading(),
    btnLoading: makeSelectbtnLoading(),
    available: makeSelectupdateList(),
    updateData: makeSelectupdate(),
    products: makeSelectProducts(),
    loading: makeSelectLoading()

});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        commissionTypeAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( CommissionType );
