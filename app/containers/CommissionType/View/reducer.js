import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_COMMISSION_TYPE,
    DELETED_COMMISSION_TYPE,
    NOTIFICATION_SAGA,
    UPADATE_SET_COMMISSSION_TYPE,
    UPDATED_COMMISSION,
    LOADING,
    SET_AVAILABLE,
    BTN_LOADING,
    EDIT_LOADING
} from './constants';

const initialState = fromJS({
    commissiontype: [],
    updateData: null,
    loading: true,
    btnLoading: false,
    editLoading: false,
    update: null,
    available: null,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});
/**
 *
 * CommissionType reducer
 *
 */
function commissionTypeReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_COMMISSION_TYPE:
            return state.set( 'commissiontype', fromJS( action.payload ) );
        case DELETED_COMMISSION_TYPE:
            return state.set( 'deleteData', action.payload );
        case UPADATE_SET_COMMISSSION_TYPE:
            return state.set( 'updateData', action.payload );
        case UPDATED_COMMISSION:
            return state.set( 'update', action.payload );
        case BTN_LOADING:
            return state.set( 'btnLoading', action.payload );
        case EDIT_LOADING:
            return state.set( 'editLoading', action.payload );
        case SET_AVAILABLE:
            return state.set( 'available', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        default:
            return state;
    }
}

export default commissionTypeReducer;
