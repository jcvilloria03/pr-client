import styled from 'styled-components';
import { Container } from 'reactstrap';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;
export const PageWrapper = styled( Container )`
    height: 100%;
    margin-bottom: 20px;
    padding-bottom: 100px;

    button {
        border-radius: 28px;
    }

    .input-group {
        height: 39px;

        input {
            height: 39px;
        }
    }

    .content {
        margin-top: 110px;
        .ReactTable{
            .rt-tbody{
                .selected{
                    background-color: rgba(131, 210, 75, 0.15);
                    &:hover{
                        background-color: rgba(131, 210, 75, 0.15) !important;
                    }
                }
            }
        }
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 56px auto;

            h1 {
                font-weight: 700;
                font-size: 36px;
                line-height: 57.6px;
            }

            p {
                text-align: center;
                line-height: 22.4px;
            }

            .buttons-wrapper {
                display: flex;
                flex-grow: 1;
                align-items: center;
                justify-content: center;
                margin-top: 28px;

                button {
                    margin: 0 5px;
                    border-radius: 28px;
                    min-width: 200px;
                }
            }
        }
        .content-input-error{
            height:55px;
            input{
                border: 1px solid #eb7575;
                min-height: 46px;
            }
        }
        .main {
            display: flex;
            justify-content: center;
            margin-bottom:50px;
            .btn {
                width: 148px;
                height:51px;
                background-color: #83d24b;
                display:flex;
                color: #ffffff;
                border-color: #83d24b;
                align-items:center;
                justify-content:center;
                line-height:0;
                margin: 0 7px;
                font-size:14px;
                &:last-child{
                    width:182px;
                }
                &:hover{
                    background-color: #9fdc74
                }
            }
        }

        .tableAction button {
            width: 130px;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 14px;

            .search-wrapper {
                display: flex;
                align-items: center;
                flex-direction: row;

                h5 {
                    margin: 0;
                    margin-right: 28px;
                    font-weight: 600;
                    font-size: 18px;
                    line-height: 28.8px;
                }

                .search {
                    width: 200px;
                    border: 1px solid #333;
                    border-radius: 28px;
                    margin-right: 3px;

                    .input-group {
                      height: 42px;

                        input {
                            height: 42px;
                            border: none;
                        }
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }

            .actions-wrapper {
                display: flex;
                flex-grow: 1;
                align-items: center;
                justify-content: flex-end;

                .dropdown {
                    span {
                        border-radius: 2px;
                    }
                }
            }
        }
    }

    .ReactTable .rt-tbody .rt-td {
        padding: 14px;
        font-size: 14px;
    }

    .rt-td {
        span {
            display: flex;
            align-items: center;
        }
    }

    .ReactTable input {
        border: 1px solid #474747;
    }

    .ReactTable select {
        width: 100%;
        appearance: auto;
        padding: 7px;
        border: 1px solid #474747;
        border-radius: 0;
        background-color: #fff;
        display: block;
        height: 39px;
    }
`;
