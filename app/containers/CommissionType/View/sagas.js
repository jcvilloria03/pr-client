import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';

import { GET_COMMISSION_TYPE,
    SET_COMMISSION_TYPE,
    DELETED_COMMISSION_TYPE,
    DELETE_COMMISSION_TYPE,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    UPADATE_SET_COMMISSSION_TYPE,
    UPDATE_GET_COMMISSION_TYPE,
    UPDATE_COMMISSION,
    UPDATED_COMMISSION,
    LOADING,
    SET_AVAILABLE,
    AVAILABLE,
    BTN_LOADING
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * GetCommissionsTypes
 */
export function* GetCommissionsTypes() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_income_types/commission_type?exclude_trashed=1`, { method: 'GET' });
        yield put({
            type: SET_COMMISSION_TYPE,
            payload: response.data || []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * UpdateCommissionType
 */
export function* UpdateCommissionType({ payload }) {
    try {
        yield put({
            type: BTN_LOADING,
            payload: true
        });
        const response = yield call( Fetch, '/other_income_type/is_edit_available', { method: 'POST',
            data: {
                other_income_type_id: payload.other_income_type_id.id }});
        yield put({
            type: UPADATE_SET_COMMISSSION_TYPE,
            payload: response.available
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}

/**
 * updatecommisssions
 */
export function* updatecommisssions({ payload }) {
    try {
        yield put({
            type: BTN_LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const updateData = yield call( Fetch, `/philippine/company/${companyId}/commission_type/is_name_available`, { method: 'POST',
            data: {
                name: payload.name,
                other_income_type_id: payload.id
            }});
        yield put({
            type: UPDATED_COMMISSION,
            payload: updateData.available
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}
/**
* updatecommisssions
*/
export function* updatecommisssionsedit({ payload }) {
    try {
        yield put({
            type: BTN_LOADING,
            payload: true
        });
        const update = yield call( Fetch, `/philippine/commission_type/${payload.id}`, { method: 'PATCH', data: payload });
        yield put({
            type: SET_AVAILABLE,
            payload: update
        });
        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully Updated',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: BTN_LOADING,
            payload: false
        });
    }
}
/**
 * deletedCommissionType
 */
export function* deletedCommissionType({ payload }) {
    try {
        yield put({
            type: DELETED_COMMISSION_TYPE,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_income_type/is_delete_available`, { method: 'POST', data: payload });
        const formData = new FormData();
        if ( response.available === true ) {
            formData.append( '_method', 'DELETE' );
            formData.append( 'company_id', companyId );
            for ( const i of payload.ids ) {
                formData.append( 'ids[]', i );
            }
            yield call( Fetch, `/company/${companyId}/other_income_type`, { method: 'POST', data: formData });
        }
        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully deleted',
            show: true,
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: DELETED_COMMISSION_TYPE,
            payload: false
        });
    }
}

/**
 * notifyUser
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * reinitializePage
 */
export function* reinitializePage() {
    yield call( GetCommissionsTypes );
}

/**
 * watchForReinitializePage
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watchForGetCommissionsTypes
 */
export function* watchForGetCommissionsTypes() {
    const watcher = yield takeEvery( GET_COMMISSION_TYPE, GetCommissionsTypes );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watchFordeletedCommissionType
 */
export function* watchFordeletedCommissionType() {
    const watcher = yield takeEvery( DELETE_COMMISSION_TYPE, deletedCommissionType );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watchForupdateCommissiones
 */
export function* watchForupdateCommissiones() {
    const watcher = yield takeEvery( UPDATE_COMMISSION, updatecommisssions );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watchForUpdateCommissionType
 */
export function* watchForUpdateCommissionType() {
    const watcher = yield takeEvery( UPDATE_GET_COMMISSION_TYPE, UpdateCommissionType );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watchForNotifyUser
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
 /**
* watchForupdateAllowance
*/
export function* watchForupdatecommissionedit() {
    const watcher = yield takeEvery( AVAILABLE, updatecommisssionsedit );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetCommissionsTypes,
    watchForReinitializePage,
    watchFordeletedCommissionType,
    watchForNotifyUser,
    watchForUpdateCommissionType,
    watchForupdateCommissiones,
    watchForupdatecommissionedit
];
