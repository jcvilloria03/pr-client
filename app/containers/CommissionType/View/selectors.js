import { createSelector } from 'reselect';

/**
 * Direct selector to the commissionType state domain
 */
const selectCommissionTypeDomain = () => ( state ) => state.get( 'CommissionType' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

const makeSelectListCommissionType = () => createSelector(
  selectCommissionTypeDomain(),
  ( substate ) => substate.get( 'commissiontype' ).toJS()
);
const makeSelectupdate = () => createSelector(
  selectCommissionTypeDomain(),
  ( substate ) => substate.get( 'updateData' )
);
const makeSelectupdatecommission = () => createSelector(
  selectCommissionTypeDomain(),
  ( substate ) => substate.get( 'update' )
);
const makeSelectupdateList = () => createSelector(
  selectCommissionTypeDomain(),
  ( substate ) => substate.get( 'available' )
);
const makeSelectNotification = () => createSelector(
  selectCommissionTypeDomain(),
( substate ) => substate.get( 'notification' ).toJS()
);
const makeSelectLoading = () => createSelector(
  selectCommissionTypeDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectbtnLoading = () => createSelector(
  selectCommissionTypeDomain(),
  ( substate ) => substate.get( 'btnLoading' )
);

const makeSelectEditLoading = () => createSelector(
  selectCommissionTypeDomain(),
  ( substate ) => substate.get( 'editLoading' )
);

export {
  makeSelectListCommissionType,
  makeSelectupdatecommission,
  selectCommissionTypeDomain,
  makeSelectNotification,
  makeSelectEditLoading,
  makeSelectupdateList,
  makeSelectbtnLoading,
  makeSelectProducts,
  makeSelectLoading,
  makeSelectupdate
};
