/*
 *
 * AddCommissionType constants
 *
 */
export const nameSpace = 'app/CommissionType/Add/';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const ADD_COMMISIONTYPE = `${nameSpace}/ADD_COMMISIONTYPE`;
export const SET_ADD_COMMISIONTYPE = `${nameSpace}/SET_ADD_COMMISIONTYPE`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
export const LOADING = `${nameSpace}/LOADING`;
export const ADD_COMMISIONTYPE_LIST = `${nameSpace}/ADD_COMMISIONTYPE_LIST`;
export const SET_ADD_COMMISIONTYPE_LIST = `${nameSpace}/SET_ADD_COMMISIONTYPE_LIST`;
