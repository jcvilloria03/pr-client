import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;

    .icon-arrow {
        width: 14px;
        font-size: 14px;
        display: inline-block;
        margin-right: 4px;

        > i {
            align-self: center;
        }
    }

    .back-text {
        font-size: 14px;
    }
`;

export const PageWrapper = styled.div`
    padding-top: 120px;
    .content {
        margin-top: 40px;
        .entriTitle{
            text-align:right;
        }
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h1 {
                font-weight: 700;
                font-size: 36px;
                line-height: 57.6px;
            }

            p {
                text-align: center;
                line-height: 22.4px;
            }
        }
        .comm-detail{
            .col-md-2{
                padding: 0 0.5rem 1rem;
            }
            h5{
                font-size: 14px;
                color: #474747;
                font-weight: 700;
            }
            input{
                padding: 7px;
                color:#474747;
                font-weight: 400;
                &::placeholder{
                    color:#cbc7c7 !important;
                    font-size:14px;
                    font-weight:400;
                }
            }
            input,.Select-control{
                border-color: rgb(71, 71, 71);
                font-size:14px;
            }
            .taxable-input input{
                border-color:#adadad;
            }
            .taxable-input-error input{
                border-color:#eb7575;
            }
            .switch-input{
                margin-top:10px;
            }
        }
        .addType{
            height: 38px;
            border-radius: 50px;
            width: 226px;
            font-size:15px;
            svg{
                width:13px;
                margin-top:-2px;
                margin-right:3px;
            }
        }

        .content-input-error{
            height:55px;
            input{
                border: 1px solid #eb7575;
                min-height: 46px;
            }
        }
        .deletebtn{
            height: 38px;
            border-radius: 50px;
            width: 226px;
            font-size:15px;
            border: 1px solid #eb7575;
            background-color:unset !important;
            color:#474747;
            svg{
                width:13px;
                margin-top:-2px;
                margin-right:3px;
            }
        }

        .submit {
            & > div {
                text-align: right;
                margin-top: 50px;

                & > button {
                    min-width: 160px;
                    margin-bottom: 50px;
                }
            }
        }
    }

    .row.submit {
        margin-bottom: 0;
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;
    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;
        > i {
            align-self: center;
        }
    }
    .message {
        display: flex;
        align-self: center;
    }
`;
export const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;
