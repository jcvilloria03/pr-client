/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';

import SnackBar from 'components/SnackBar';
import { H1, P } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Select from 'components/Select';
import Switch from 'components/Switch2';
import Input from 'components/Input';
import A from 'components/A';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import { Spinner } from 'components/Spinner';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';

import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectProducts,
    makeSelectSetAddcommissiontype,
    makeSelectSetAddcommissiontypeList
} from './selectors';
import * as allActions from './actions';

import {
    PageWrapper,
    NavWrapper,
    Footer
} from './styles';

/**
 *
 * AddCommissionType
 *
 */
export class AddCommissionType extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
        products: React.PropTypes.array,
        addCommissiontype: React.PropTypes.func,
        addCommissionListType: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        CommissionTypeAdd: React.PropTypes.bool,
        loading: React.PropTypes.bool
    }

    static defaultProps = {
        loading: false
    };

    constructor( props ) {
        super( props );
        const companyId = company.getLastActiveCompanyId();
        this.state = {
            formData: {
                id: '',
                name: '',
                basis: 'FIXED',
                fully_taxable: true,
                max_non_taxable: '',
                company_id: companyId,
                unavailableName: false
            },
            dataList: [],
            showModel: false,
            editedData: '',
            listId: '',
            NewEdit: false,
            validateNameExists: 'name',
            validateName: 'name',
            validateMaxNoTaxable: '1'
        };
    }
    getUniqueId = () => `${Math.floor( Math.random() * 1000000 )}`;

    addCommission() {
        const companyId = company.getLastActiveCompanyId();
        if ( this.validateFields() ) {
            if ( this.state.validateName !== '' && this.state.validateName !== 'name' ) {
                this.props.addCommissiontype( this.state.formData );
                setTimeout( () => {
                    if ( this.validateTypeIsExist() && this.state.formData.name !== '' ) {
                        this.setState({
                            formData: {
                                id: '',
                                name: '',
                                basis: 'FIXED',
                                fully_taxable: true,
                                max_non_taxable: '',
                                company_id: companyId,
                                unavailableName: false,
                                editError: false,
                                editErrMaxNoTax: false,
                                editIsZero: false
                            },
                            dataList: [
                                ...this.state.dataList,
                                { ...this.state.formData,
                                    id: this.getUniqueId() }
                            ]
                        });
                    }
                }, 2000 );
            }
        }
    }

    deleteList = () => {
        this.setState({
            dataList: this.state.dataList.filter( ( arr ) => arr.id !== this.state.listId ),
            showModel: false
        });
        this.DeleteModel.toggle();
    }

    addCommissionList() {
        this.props.addCommissionListType( this.state.dataList );
    }

    validateTypeIsExist = () => {
        let valid = true;
        const name = this.state.dataList.map( ( value ) => value.name ).includes( this.state.formData.name );
        if ( this.props.CommissionTypeAdd === false ) {
            const message = <p style={ { color: '#eb7575', fontSize: '14px' } }>Record name already exists</p>;
            this.name.setState({ error: true, errorMessage: message });
            this.setState({
                validateNameExists: ''
            });
            valid = false;
        } else if ( name ) {
            const message = <p style={ { color: '#eb7575', fontSize: '14px' } }>Record name already exists</p>;
            this.name.setState({ error: true, errorMessage: message });
            this.setState({
                validateNameExists: ''
            });
            valid = false;
        }
        return valid;
    }

    validateFields = () => {
        let valid = true;
        if ( this.state.formData.name === '' ) {
            this.setState({
                validateName: ''
            });
            valid = false;
        }
        return valid;
    }
    EditCommiType=( elem, id ) => {
        const data = [
            { value: 'FIXED', label: 'Fixed' }
        ];
        return (
            <div ref={ ( el ) => { this.messagesEnd = el; } } key={ id }>
                <div className="row comm-detail">
                    <div className="col-md-10 comm-row" >
                        <div className="col-md-3">
                            <Input
                                className={ elem.editError ? 'content-input-error' : '' }
                                id="name"
                                type="text"
                                name="name"
                                placeholder="Type in commission type name"
                                value={ elem.name }
                                onChange={ ( eve ) => {
                                    this.setState( ( state ) => {
                                        const newState = state.dataList.map( ( dataOne ) => {
                                            if ( dataOne.id === id ) {
                                                dataOne.name = eve;
                                                dataOne.editError = dataOne.name === '';
                                            }
                                            return dataOne;
                                        });
                                        return { ...state, editError: true, dataList: newState };
                                    });
                                } }
                            />
                            {
                             this.state.dataList.map( ( val ) => {
                                 if ( val.id === id ) {
                                     return val.editError;
                                 }
                             }).includes( true ) ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                        </div>
                        <div className="col-md-3">
                            <Select
                                id="basis"
                                required
                                name="basis"
                                value={ elem.basis }
                                data={ data }
                                onChange={ ( eve ) => {
                                    this.setState( ( state ) => {
                                        const newState = state.dataList.map( ( dataOne ) => {
                                            if ( dataOne.id === id ) {
                                                dataOne.basis = eve.value;
                                            }
                                            return dataOne;
                                        });
                                        return { ...state, dataList: newState };
                                    });
                                } }
                            />
                        </div>
                        <div className="col-md-3 switch-input">
                            <Switch
                                id="taxable"
                                checked={ elem.fully_taxable }
                                onChange={ ( eve ) => {
                                    this.setState( ( state ) => {
                                        const newState = state.dataList.map( ( dataOne ) => {
                                            if ( dataOne.id === id ) {
                                                dataOne.fully_taxable = eve;
                                                dataOne.max_non_taxable = eve === false ? dataOne.max_non_taxable === '' ? '90000.00' : dataOne.max_non_taxable : '';
                                            }
                                            return dataOne;
                                        });
                                        return { ...state, dataList: newState };
                                    });
                                } }
                            />
                        </div>
                        <div
                            className={ elem.editErrMaxNoTax === true ?
                            'col-md-3 taxable-input-error' :
                            elem.editIsZero === true ?
                            'col-md-3 taxable-input-error' :
                            'col-md-3 taxable-input' }
                        >
                            {elem.fully_taxable === false ?
                                <Input
                                    id="taxable"
                                    name="max_non_taxable"
                                    value={ elem.max_non_taxable }
                                    type="number"
                                    onChange={ ( eve ) => {
                                        this.setState( ( state ) => {
                                            const newState = state.dataList.map( ( dataOne ) => {
                                                if ( dataOne.id === id ) {
                                                    dataOne.max_non_taxable = parseFloat( eve );
                                                    dataOne.editErrMaxNoTax = isNaN( dataOne.max_non_taxable );
                                                    dataOne.editIsZero = Math.sign( dataOne.max_non_taxable ) === 0;
                                                }
                                                return dataOne;
                                            });
                                            return { ...state, dataList: newState };
                                        });
                                    } }
                                />
                        : <Input
                            id="name"
                            type="text"
                            ref={ ( ref ) => { this.max_non_taxable = ref; } }
                            disabled
                        />
                    }
                            {
                            this.state.dataList.map( ( val ) => {
                                if ( val.fully_taxable === false && val.id === id ) {
                                    return val.editErrMaxNoTax;
                                }
                            }).includes( true )
                            ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Field is required</p>
                            : this.state.dataList.map( ( val ) => {
                                if ( val.fully_taxable === false && val.id === id ) {
                                    return val.editIsZero;
                                }
                            }).includes( true )
                            ? <p style={ { color: '#eb7575', fontSize: '14px' } }>This field minimal value is 1</p>
                            : ''}
                        </div>
                    </div>
                    <div className="col-md-2">
                        <div>
                            <Button
                                className="deletebtn"
                                label="Remove"
                                type="danger"
                                size="default"
                                onClick={ () => {
                                    this.setState({
                                        showModel: true,
                                        listId: id
                                    });
                                    this.DeleteModel.toggle();
                                } }
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    /**
     *
     * AddCommissionType render method
     *
     */
    render() {
        const data = [
            { value: 'FIXED', label: 'Fixed' }
        ];
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });

        const { notification, loading } = this.props;
        const { formData, dataList, validateName, validateMaxNoTaxable, validateNameExists } = this.state;
        return (
            <div>
                <div style={ { paddingBottom: '100px' } }>
                    { loading && <Spinner /> }
                    <Helmet
                        title="Add Commission Types"
                        meta={ [
                            { name: 'description', content: 'Description of Add Commission Types' }
                        ] }
                    />
                    <Sidebar items={ sidebarLinks } />
                    <SnackBar
                        message={ notification.message }
                        title={ notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ notification.show }
                        delay={ 5000 }
                        type={ notification.type }
                    />
                    <Modal
                        title="Confirm Your Action"
                        className="modal-md modal-commission-type"
                        ref={ ( ref ) => {
                            this.DeleteModel = ref;
                        } }
                        center
                        showClose={ false }
                        body={ (
                            <div>
                                <p>Do you wish to remove this record?</p>
                            </div>
                        ) }
                        buttons={ [
                            {
                                type: 'grey',
                                label: 'No',
                                onClick: () => this.DeleteModel.toggle()
                            },
                            {
                                type: 'action',
                                label: 'Yes',
                                onClick: this.deleteList
                            }
                        ] }
                    />
                    <Modal
                        title="Discard Changes"
                        className="modal-md modal-commission-type"
                        ref={ ( ref ) => {
                            this.DiscardModal = ref;
                        } }
                        center
                        showClose={ false }
                        body={ (
                            <div>
                                <p>Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p>
                            </div>
                        ) }
                        buttons={ [
                            {
                                type: 'grey',
                                label: 'Stay on this page',
                                onClick: () => this.DiscardModal.toggle()
                            },
                            {
                                type: 'darkRed',
                                label: 'Discard',
                                onClick: () => browserHistory.push( '/company-settings/payroll/commission-types', true )
                            }
                        ] }
                    />
                    <NavWrapper>
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/payroll/commission-types', true );
                                } }
                            >
                                <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Commission Types</span>
                            </A>
                        </Container>
                    </NavWrapper>
                    <PageWrapper>
                        <Container>
                            <div className="content">
                                <div className="heading">
                                    <H1 noBottomMargin>Add Commission Types</H1>

                                    <P noBottomMargin>View and update commission types. You can add, edit, or delete commission types.</P>
                                </div>
                                <div className="row comm-detail">
                                    <div className="col-md-10">
                                        <div className="col-md-3">
                                            <h5>Commission type name</h5>
                                        </div>
                                        <div className="col-md-3">
                                            <h5>Amount Basis</h5>
                                        </div>
                                        <div className="col-md-3">
                                            <h5>Fully Taxable</h5>
                                        </div>
                                        <div className="col-md-3">
                                            <h5>If no, specify the maximum non taxable</h5>
                                        </div>
                                    </div>
                                    <div className="col-md-2"></div>
                                </div>
                                { dataList.map( ( elem ) => this.EditCommiType( elem, elem.id ) ) }
                                <div className="row comm-detail">
                                    <div className="col-md-10 comm-row">
                                        <div className="col-md-3">
                                            <Input
                                                className={ validateName === '' ? 'content-input-error' : validateNameExists === '' ? 'content-input-error' : '' }
                                                id="name"
                                                type="text"
                                                placeholder="Type in commission type name"
                                                value={ formData.name }
                                                onChange={ ( eve ) => this.setState({ validateName: eve, validateNameExists: eve, formData: { ...formData, name: eve }}) }
                                                ref={ ( ref ) => { this.name = ref; } }
                                            />
                                            {validateName === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please input valid data</p> : ''}
                                        </div>
                                        <div className="col-md-3">
                                            <Select
                                                id="basis"
                                                required
                                                value={ formData.basis }
                                                data={ data }
                                                onChange={ ( eve ) => this.setState({ formData: { ...formData, basis: eve.value }}) }
                                            />
                                        </div>
                                        <div className="col-md-3 switch-input">
                                            <Switch
                                                id="taxable"
                                                checked={ formData.fully_taxable }
                                                onChange={ ( eve ) => {
                                                    this.setState({ validateMaxNoTaxable: formData.fully_taxable === true ? 90000 : null,
                                                        formData: {
                                                            ...formData,
                                                            fully_taxable: eve === true,
                                                            max_non_taxable: eve === false ? formData.max_non_taxable === '' ? 90000.00 : formData.max_non_taxable : ''
                                                        }});
                                                } }
                                            />
                                        </div>
                                        <div
                                            className={ validateMaxNoTaxable === '' ?
                                                'col-md-3 taxable-input-error' :
                                                formData.fully_taxable === false && Math.sign( validateMaxNoTaxable ) === 0 ?
                                                 'col-md-3 taxable-input-error' :
                                                 'col-md-3 taxable-input' }
                                        >
                                            {
                                            formData.fully_taxable === true
                                            ? <Input
                                                id="taxable"
                                                disabled
                                                type="text"
                                            />
                                            : <Input
                                                id="taxable"
                                                type="number"
                                                value={ formData.max_non_taxable }
                                                onChange={ ( eve ) => this.setState({ validateMaxNoTaxable: eve,
                                                    formData: { ...formData,
                                                        max_non_taxable: parseFloat( eve )
                                                    }}) }
                                                ref={ ( ref ) => { this.max_non_taxable = ref; } }
                                            />
                                        }
                                            {validateMaxNoTaxable === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Field is required</p>
                                            : formData.fully_taxable === false && Math.sign( validateMaxNoTaxable ) === 0 ?
                                                <p style={ { color: '#eb7575', fontSize: '14px' } }>This field minimal value is 1</p> : ''}
                                        </div>
                                    </div>
                                    <div className="col-md-2">
                                        <div>
                                            <Button
                                                className="addType"
                                                label="Add"
                                                type="neutral"
                                                size="default"
                                                disabled={ loading }
                                                onClick={ () => this.addCommission() }
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Container>
                    </PageWrapper>
                </div>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.DiscardModal.toggle() }
                            />
                            {
                                dataList.length <= 0
                                ? <Button
                                    disabled
                                    label="Submit"
                                    type="action"
                                    size="large"
                                />
                                : <Button
                                    label="Submit"
                                    type="action"
                                    size="large"
                                    onClick={ () => this.addCommissionList() }
                                    disabled={ loading || ( validateName === '' ? true : validateNameExists === '' ) }
                                />
                            }
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    products: makeSelectProducts(),
    CommissionTypeAdd: makeSelectSetAddcommissiontype(),
    ListCommissionTypeAdd: makeSelectSetAddcommissiontypeList(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        allActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddCommissionType );
