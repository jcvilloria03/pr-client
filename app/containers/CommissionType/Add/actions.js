import {
    ADD_COMMISIONTYPE,
    ADD_COMMISIONTYPE_LIST,
    DEFAULT_ACTION,
    NOTIFICATION
} from './constants';

/**
 *
 * AddCommissionType actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Add commision type action
 *
 */
export function addCommissiontype( payload ) {
    return {
        type: ADD_COMMISIONTYPE,
        payload
    };
}

/**
 *
 * Add commision type list action
 *
 */
export function addCommissionListType( payload ) {
    return {
        type: ADD_COMMISIONTYPE_LIST,
        payload
    };
}

/**
 *
 * Notification action
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
