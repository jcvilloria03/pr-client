import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';
import {
    ADD_COMMISIONTYPE,
    ADD_COMMISIONTYPE_LIST,
    LOADING,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_ADD_COMMISIONTYPE,
    SET_ADD_COMMISIONTYPE_LIST
} from './constants';

import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { browserHistory } from '../../../utils/BrowserHistory';

/**
 * Add commission type
 */
export function* addCommissionType({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/philippine/company/${companyId}/commission_type/is_name_available`, {
            method: 'POST',
            data: { name: payload.name }
        });
        yield put({
            type: SET_ADD_COMMISIONTYPE,
            payload: res.available
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Add commission type list
 */
export function* addCommissionTypeList({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        yield put({
            type: SET_ADD_COMMISIONTYPE_LIST,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        yield call( Fetch, `/philippine/company/${companyId}/commission_type/bulk_create`, {
            method: 'POST',
            data: payload
        });
        yield call( notifyUser, {
            title: 'Success',
            message: 'Record saved successfully',
            show: true,
            type: 'success'
        });
        yield call( browserHistory.push( '/company-settings/payroll/commission-types', true ) );
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error || 'Something went wrong',
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
        yield put({
            type: SET_ADD_COMMISIONTYPE_LIST,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * watcher For Add commission type
 */
export function* watchForAddCommissionType() {
    const watcher = yield takeEvery( ADD_COMMISIONTYPE, addCommissionType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watcher For Add commission type List
 */
export function* watchForAddCommissionTypeList() {
    const watcher = yield takeEvery( ADD_COMMISIONTYPE_LIST, addCommissionTypeList );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Notification
 */
export function* watchFroNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForAddCommissionType,
    watchForAddCommissionTypeList,
    watchFroNotification
];
