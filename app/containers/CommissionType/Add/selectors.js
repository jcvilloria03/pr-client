import { createSelector } from 'reselect';

/**
 * Direct selector to the addCommissionType state domain
 */
const selectAddCommissionTypeDomain = () => ( state ) => state.get( 'CommissionTypeAdd' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

/**
 * Default selector used by AddCommissionType
 */

const makeSelectAddCommissionType = () => createSelector(
  selectAddCommissionTypeDomain(),
  ( substate ) => substate.toJS()
);

const makeSelectSetAddcommissiontype = () => createSelector(
  selectAddCommissionTypeDomain(),
  ( substate ) => substate.get( 'addtype' )
);

const makeSelectSetAddcommissiontypeList = () => createSelector(
  selectAddCommissionTypeDomain(),
  ( substate ) => substate.get( 'addtypelist' )
);

const makeSelectNotification = () => createSelector(
  selectAddCommissionTypeDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectAddCommissionTypeDomain(),
  ( substate ) => substate.get( 'loading' )
);

export default makeSelectAddCommissionType;
export {
  selectAddCommissionTypeDomain,
  makeSelectProducts,
  makeSelectSetAddcommissiontype,
  makeSelectNotification,
  makeSelectLoading,
  makeSelectSetAddcommissiontypeList
};
