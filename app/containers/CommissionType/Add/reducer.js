import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    NOTIFICATION_SAGA,
    SET_ADD_COMMISIONTYPE,
    SET_ADD_COMMISIONTYPE_LIST
} from './constants';

const initialState = fromJS({
    addtype: null,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    },
    loading: false,
    addtypelist: ''
});

/**
 *
 * AddCommissionType reducer
 *
 */
function addCommissionTypeReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_ADD_COMMISIONTYPE:
            return state.set( 'addtype', action.payload );
        case SET_ADD_COMMISIONTYPE_LIST:
            return state.set( 'addtypelist', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        default:
            return state;
    }
}

export default addCommissionTypeReducer;
