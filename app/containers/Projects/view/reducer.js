import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    NOTIFICATION_SAGA,
    SET_DELETE_PROJECT,
    SET_EDIT_PROJECT,
    SET_LOADING,
    SET_PROJECTS
} from './constants';

const initialState = fromJS({
    projects: [],
    loading: false,
    isEditing: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    },
    pagination: {
        current_page: 0,
        from: 0,
        last_page: 0,
        per_page: 0,
        to: 0,
        total: 0
    }
});

/**
 *
 * Projects reducer
 *
 */
function projectsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_PROJECTS:
            return state.set( 'projects', fromJS( action.payload ) );
        case SET_EDIT_PROJECT:
            return state.set( 'isEditing', action.payload );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_DELETE_PROJECT:
            return state.set( action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default projectsReducer;
