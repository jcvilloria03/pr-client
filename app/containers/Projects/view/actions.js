import {
    DEFAULT_ACTION, DELETE_PROJECT, EDIT_PROJECT, GET_PROJECTS, NOTIFICATION
} from './constants';

/**
 *
 * Projects actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Get Projects actions
 *
 */
export function getProjects() {
    return {
        type: GET_PROJECTS
    };
}

/**
 *
 * Delete Projects actions
 *
 */
export function deleteProjects( payload ) {
    return {
        type: DELETE_PROJECT,
        payload
    };
}

/**
 *
 * Edit Projects actions
 *
 */
export function editProject( payload ) {
    return {
        type: EDIT_PROJECT,
        payload
    };
}

/**
 *
 * Projects actions
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
