/*
 *
 * Projects constants
 *
 */
export const nameSpace = 'app/Projects/view';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const SET_LOADING = `${nameSpace}/SET_LOADING`;
export const GET_PROJECTS = `${nameSpace}/GET_PROJECTS`;
export const SET_PROJECTS = `${nameSpace}/SET_PROJECTS`;
export const DELETE_PROJECT = `${nameSpace}/DELETE_PROJECT`;
export const SET_DELETE_PROJECT = `${nameSpace}/SET_DELETE_PROJECT`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
export const EDIT_PROJECT = `${nameSpace}/EDIT_PROJECT`;
export const SET_EDIT_PROJECT = `${nameSpace}/SET_EDIT_PROJECT`;
