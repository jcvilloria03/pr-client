import { createSelector } from 'reselect';

/**
 * Direct selector to the projects state domain
 */
const selectProjectsDomain = () => ( state ) => state.get( 'projects' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

/**
 * Default selector used by Projects
 */

const makeSelectProjects = () => createSelector(
  selectProjectsDomain(),
  ( substate ) => substate.toJS()
);

const makeSelectGetProjects = () => createSelector(
  selectProjectsDomain(),
  ( substate ) => substate.get( 'projects' ).toJS()
);

const makeSelectIsEditing = () => createSelector(
  selectProjectsDomain(),
( substate ) => substate.get( 'isEditing' )
);

const makeSelectLoading = () => createSelector(
  selectProjectsDomain(),
( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectProjectsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export default makeSelectProjects;
export {
  selectProjectsDomain,
  makeSelectProducts,
  makeSelectGetProjects,
  makeSelectIsEditing,
  makeSelectLoading,
  makeSelectNotification
};
