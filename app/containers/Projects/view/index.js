/* eslint-disable react/prop-types */
/* eslint-disable import/first */
import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import makeSelectProjects, {
    makeSelectGetProjects,
    makeSelectLoading,
    makeSelectIsEditing,
    makeSelectNotification,
    makeSelectProducts
} from './selectors';
import * as getProjectActions from './actions';

import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';

import { H3, H5, H2 } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import SalDropdown from 'components/SalDropdown';
import { isAnyRowSelected, getIdsOfSelectedRows } from 'components/Table/helpers';
import Table from 'components/Table';
import Loader from 'components/Loader';
import Button from 'components/Button';
import Input from 'components/Input';
import SalConfirm from 'components/SalConfirm';

import {
    PageWrapper,
    ConfirmBodyWrapperStyle,
    LoadingStyles
} from './styles';

/**
 *
 * Projects
 *
 */
export class Projects extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
        getProjects: React.PropTypes.func,
        editProject: React.PropTypes.func,
        listProject: React.PropTypes.array,
        loading: React.PropTypes.bool,
        deleteProjects: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            show: false,
            showModel: false,
            showButtons: false,
            editData: null
        };
    }

    componentDidMount() {
        this.props.getProjects();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.listProject !== this.props.listProject && this.setState({ label: formatPaginationLabel( this.projectTable.tableComponent.state ) });
    }

    handleTableChanges = ( tableProps = this.projectTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    deleteProjectName = () => {
        const payload = getIdsOfSelectedRows( this.projectTable );
        this.props.deleteProjects( payload );
        setTimeout( () => {
            this.props.getProjects();
        }, 1000 );
        this.setState({ show: false, showModel: false });
    }

    showTextbox( row ) {
        return this.state.editData && this.state.editData.id === row.id;
    }

    editProjectDATA() {
        this.setState({ showButtons: false });
        this.props.editProject({
            id: this.state.editData.id,
            data: this.state.editData,
            projects: this.props.listProject
        });
    }

    /**
     *
     * Projects render method
     *
     */
    render() {
        const tableColumn = [
            {
                id: 'name',
                header: 'Project Name',
                sortable: false,
                minWidth: 500,
                render: ({ row }) => (
                    this.showTextbox( row ) && this.state.showButtons === true
                        ? <div style={ { width: '100%' } }>
                            <Input
                                id="name" type="text"
                                value={ this.state.editData.name }
                                onChange={ ( eve ) => this.setState({ editData: { ...this.state.editData, name: eve }}) }
                            />
                        </div>
                        : <div>{row.name}</div>
                )
            },
            {
                id: 'name',
                header: '',
                sortable: false,
                minWidth: 100,
                style: { justifyContent: 'right' },
                render: ({ row }) => (
                    this.showTextbox( row ) && this.state.showButtons === true
                        ? <div>

                            <Button
                                label={ <span>Cancel</span> }
                                type="neutral"
                                size="small"
                                onClick={ () => this.setState({ showButtons: false }) }
                            ></Button>
                            <Button
                                label={ <span>Save</span> }
                                type="action"
                                size="small"
                                onClick={ () => this.editProjectDATA() }
                            ></Button>
                        </div>
                        : <Button
                            label={ ( !!this.state.editData && this.state.editData.id === row.id && this.props.isEditing ) ? <Loader /> : 'Edit' }
                            type="grey"
                            size="small"
                            onClick={ () => this.setState({ showButtons: true, editData: row }) }
                        ></Button>
                )
            }
        ];

        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });

        const hasSelectedItems = isAnyRowSelected( this.projectTable );
        return (
            <div>
                <Helmet
                    title="Projects"
                    meta={ [
                        { name: 'description', content: 'Description of Projects' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.deleteProjectName }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Are you sure you want to delete these Project?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Project"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModel }
                />
                <Sidebar items={ sidebarLinks } />
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Loading Projects.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                            <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>

                                <div className="heading">
                                    <H3>Projects</H3>
                                    <p>A project is a planned set of tasks or assignments done for particular purposes or clients.</p>
                                </div>
                                <div className="title">
                                    <H5>Projects List</H5>
                                    <span className="title-Content">
                                        {hasSelectedItems ? <p className="mb-0 mr-1">{this.state.deleteLabel}</p> : <p className="mb-0 mr-1">{this.state.label}</p>}
                                        <div>
                                            {hasSelectedItems ? (
                                                <SalDropdown
                                                    dropdownItems={ [
                                                        {
                                                            label: 'Delete',
                                                            children: <div>Delete</div>,
                                                            onClick: () => this.setState({ showModel: true })
                                                        }
                                                    ] }
                                                />
                                            ) :
                                                ( <Button
                                                    id="button-filter"
                                                    label="Add Projects"
                                                    type="action"
                                                    onClick={ () => browserHistory.push( '/company-settings/company-structure/projects/add', true ) }
                                                /> )
                                            }
                                        </div>
                                    </span>
                                </div>

                                <Table
                                    data={ this.props.listProject }
                                    columns={ tableColumn }
                                    pagination
                                    onDataChange={ this.handleTableChanges }
                                    selectable
                                    onSelectionChange={ ({ selected }) => {
                                        const selectionLength = selected.filter( ( row ) => row ).length;
                                        this.setState({
                                            show: selectionLength > 0,
                                            deleteLabel: formatDeleteLabel( selectionLength )
                                        });
                                    } }
                                    ref={ ( ref ) => { this.projectTable = ref; } }
                                />
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    Projects: makeSelectProjects(),
    products: makeSelectProducts(),
    listProject: makeSelectGetProjects(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    isEditing: makeSelectIsEditing()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        getProjectActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Projects );
