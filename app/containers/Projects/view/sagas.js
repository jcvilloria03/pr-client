/* eslint-disable import/first */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import {
    DELETE_PROJECT,
    EDIT_PROJECT,
    GET_PROJECTS,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_DELETE_PROJECT,
    SET_EDIT_PROJECT,
    SET_LOADING,
    SET_PROJECTS
} from './constants';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { REINITIALIZE_PAGE } from '../../App/constants';

/**
 * GET Projects
 */
export function* getProject() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/projects`, { method: 'GET' });

        yield put({
            type: SET_PROJECTS,
            payload: res.data || []
        });
    } catch ( err ) {
        yield call( notifyUser, {
            title: 'Error occurred during API integration',
            message: `${err}`,
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize Page
 */
export function* reinitializePage() {
    yield call( getProject );
}

/**
 * Delete Projects
 */
export function* deleteProjectSaga({ payload }) {
    try {
        yield put({
            type: SET_DELETE_PROJECT,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formData = new FormData();
        formData.append( '_method', 'DELETE' );
        formData.append( 'company_id', companyId );
        for ( const i of payload ) {
            formData.append( 'project_ids[]', i );
        }
        yield call( Fetch, '/project/bulk_delete', { method: 'POST', data: formData });

        yield call( notifyUser, {
            title: 'Deleted',
            message: 'Remove content from Project',
            show: true,
            type: 'success'
        });
    } catch ( err ) {
        yield call( notifyUser, {
            title: 'Error occurred during API integration',
            message: `${err}`,
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DELETE_PROJECT,
            payload: false
        });
    }
}

/**
 * Edit Projects
 */
export function* editProject({ payload }) {
    const { id, data, projects } = payload;
    try {
        yield put({
            type: SET_EDIT_PROJECT,
            payload: true
        });

        const res = yield call( Fetch, `/project/${id}`, { method: 'PUT', data });

        yield put({
            type: SET_PROJECTS,
            payload: projects.map( ( project ) => {
                if ( project.id === res.id ) {
                    return { ...project, name: res.name };
                }

                return project;
            })
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Changes successfully saved!',
            show: true,
            type: 'success'
        });
    } catch ( err ) {
        yield call( notifyUser, {
            title: 'Error occurred during API integration',
            message: `${err}`,
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_EDIT_PROJECT,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Watcher for GET Project
 */
export function* watchForGetProjects() {
    const watcher = yield takeEvery( GET_PROJECTS, getProject );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For ReinitializePage
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For Delete Project
 */
export function* watchForDeleteProject() {
    const watcher = yield takeEvery( DELETE_PROJECT, deleteProjectSaga );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Edit Project
 */
export function* watchForEditProject() {
    const watcher = yield takeEvery( EDIT_PROJECT, editProject );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Notification
 */
export function* watchFroNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetProjects,
    watchForDeleteProject,
    watchFroNotification,
    watchForEditProject
];
