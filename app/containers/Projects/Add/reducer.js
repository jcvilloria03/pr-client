import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    NOTIFICATION_SAGA,
    SET_ADD_PROJECT
} from './constants';

const initialState = fromJS({
    addProject: '',
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    loading: false
});

/**
 *
 * AddProject reducer
 *
 */
function addProjectReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_ADD_PROJECT:
            return state.set( 'addProject', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        default:
            return state;
    }
}

export default addProjectReducer;
