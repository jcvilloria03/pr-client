/* eslint-disable react/prop-types */
/* eslint-disable import/first */
import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';

import makeSelectAddProject, {
    makeSelectNotification,
    makeSelectProducts,
    makeSelectSetAddProject,
    makeSelectLoading
} from './selectors';
import * as addProjectAction from './actions';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';

import Loader from 'components/Loader';
import { H3 } from 'components/Typography';
import SnackBar from 'components/SnackBar';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Input from 'components/Input';
import A from 'components/A';

import {
    PageWrapper,
    NavWrapper,
    FooterWrapper
} from './styles';

/**
 *
 * AddProject
 *
 */
export class AddProject extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
        addProjects: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool
    }

    static defaultProps = {
        loading: false
    };

    constructor( props ) {
        super( props );

        this.state = {
            name: '',
            length: 0,
            loading: false
        };
    }

    setValues( eve ) {
        this.setState({ name: eve, length: eve.split( ',' ).length });
    }

    addOnClick() {
        this.props.addProjects( this.state.name );
    }

    /**
     *
     * AddProject render method
     *
     */
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        return (
            <div>
                <Helmet
                    title="Add Projects"
                    meta={ [
                        { name: 'description', content: 'Description of Add Projects' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar items={ sidebarLinks } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/company-settings/company-structure/projects', true );
                            } }
                        >
                            &#8592; Back to Projects
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="heading">
                                <H3>Add Projects</H3>
                                <p>A project is a planned set of tasks or assignments done for particular purposes or clients.</p>
                            </div>

                            <p className="entriTitle">{this.state.length} entries added</p>
                            <div className="w-100">
                                <div className="row">
                                    <div className="col-md-2">
                                        <p>Use comma to separate values. Example:<em> Project 1, Project 2, Project 3.</em></p>
                                    </div>
                                    <div className="col-md-10">
                                        <Input
                                            id="name"
                                            type="textarea"
                                            value={ this.state.value }
                                            onChange={ ( eve ) => this.setValues( eve ) }
                                            required
                                        />

                                    </div>

                                </div>
                            </div>
                        </div>
                    </Container>
                    <FooterWrapper>
                        <div>
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => browserHistory.push( '/company-settings/company-structure/projects', true ) }
                            />
                            <Button
                                label={ this.props.loading ? <Loader /> : 'Submit' }
                                type="action"
                                size="large"
                                onClick={ () => this.addOnClick() }
                            />
                        </div>
                    </FooterWrapper>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    AddProject: makeSelectAddProject(),
    products: makeSelectProducts(),
    setProject: makeSelectSetAddProject(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        addProjectAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddProject );
