import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;
`;

export const PageWrapper = styled.div`
    padding-top: 120px;
    .content {
        margin-top: 40px;
        .entriTitle{
            text-align:right;
        }
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: bold;
            }
            p{
                margin-bottom:0.25rem;
            }
        }
    }
`;

export const FooterWrapper = styled.div`
    position: absolute;
    bottom: 0px;
    right: 0px;
    padding: 15px 0;
    padding-right: 10%;
    width: 100%;
    background-color: #f0f4f6;
    bottom: 0px;
    & > div {
        text-align: right;

        & > button {
            min-width: 130px;
        }
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;
