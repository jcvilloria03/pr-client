/* eslint-disable import/first */
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import { ADD_PROJECT,
    LOADING,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_ADD_PROJECT
} from './constants';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

/**
 * Add project
 */
export function* addProjects({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        if ( payload.includes( ',' ) === true ) {
            const split = payload.split( ',' );
            for ( const s of split ) {
                const res = yield call( Fetch, `/company/${companyId}/project/is_name_available`, { method: 'POST', data: { name: s }});

                if ( res.available === true ) {
                    yield call( Fetch, '/project/bulk_create', { method: 'POST', data: { company_id: companyId, names: split }});

                    yield call( browserHistory.goBack );
                } else {
                    yield call( notifyUser, {
                        title: 'Error',
                        message: 'Project name already exists',
                        show: 'true',
                        type: 'error'
                    });
                }
            }
        } else {
            const res = yield call( Fetch, `/company/${companyId}/project/is_name_available`, { method: 'POST', data: { name: payload }});

            if ( res.available === true ) {
                yield call( Fetch, '/project/bulk_create', { method: 'POST', data: { company_id: companyId, names: [payload]}});

                yield call( browserHistory.goBack );
            } else {
                yield call( notifyUser, {
                    title: 'Error',
                    message: 'Project name already exists',
                    show: 'true',
                    type: 'error'
                });
            }
        }

        yield put({
            type: SET_ADD_PROJECT,
            payload: true
        });
    } catch ( err ) {
        yield call( notifyUser, {
            title: 'Error occurred during API integration',
            message: `${err}`,
            show: 'true',
            type: 'error'
        });

        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: SET_ADD_PROJECT,
            payload: false
        });
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}
/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * watcher For Add project
 */
export function* watchForAddProjects() {
    const watcher = yield takeEvery( ADD_PROJECT, addProjects );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watcher For Notification
 */
export function* watchForNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForAddProjects,
    watchForNotification
];
