/*
 *
 * AddProject constants
 *
 */

export const nameSpace = 'app/Projects/Add';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const ADD_PROJECT = `${nameSpace}/ADD_PROJECT`;
export const SET_ADD_PROJECT = `${nameSpace}/SET_ADD_PROJECT`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
export const LOADING = `${nameSpace}/LOADING`;
