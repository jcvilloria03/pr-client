import {
    ADD_PROJECT,
    DEFAULT_ACTION,
    NOTIFICATION
} from './constants';

/**
 *
 * AddProject actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Add Project action
 *
 */
export function addProjects( payload ) {
    return {
        type: ADD_PROJECT,
        payload
    };
}

/**
 *
 * Notify user action
 *
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
