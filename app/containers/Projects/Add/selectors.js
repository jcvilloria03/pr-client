import { createSelector } from 'reselect';

/**
 * Direct selector to the addProject state domain
 */
const selectAddProjectDomain = () => ( state ) => state.get( 'addProject' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

/**
 * Default selector used by AddProject
 */

const makeSelectAddProject = () => createSelector(
  selectAddProjectDomain(),
  ( substate ) => substate.toJS()
);

const makeSelectSetAddProject = () => createSelector(
  selectAddProjectDomain(),
  ( substate ) => substate.get( 'addProject' )
);

const makeSelectNotification = () => createSelector(
  selectAddProjectDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectAddProjectDomain(),
  ( substate ) => substate.get( 'loading' )
);

export default makeSelectAddProject;
export {
  selectAddProjectDomain,
  makeSelectProducts,
  makeSelectSetAddProject,
  makeSelectNotification,
  makeSelectLoading
};
