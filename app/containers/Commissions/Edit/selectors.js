import { createSelector } from 'reselect';

/**
 * Direct selector to the edit commission state domain
 */
const selectEditCommissionDomain = () => ( state ) => state.get( 'editCommission' );

const makeSelectLoading = () => createSelector(
    selectEditCommissionDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectSubmitted = () => createSelector(
    selectEditCommissionDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectFormOptions = () => createSelector(
    selectEditCommissionDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectCommission = () => createSelector(
    selectEditCommissionDomain(),
    ( substate ) => substate.get( 'commission' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectEditCommissionDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectCommission,
    makeSelectNotification
};
