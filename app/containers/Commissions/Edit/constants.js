export const INITIALIZE = 'app/Commissions/Edit/INITIALIZE';
export const LOADING = 'app/Commissions/Edit/LOADING';

export const SET_SUBMITTED = 'app/Commissions/Edit/SET_SUBMITTED';
export const SET_FORM_OPTIONS = 'app/Commissions/Edit/SET_FORM_OPTIONS';
export const SET_COMMISSION = 'app/Commissions/Edit/SET_COMMISSION';

export const UPDATE_COMMISSION = 'app/Commissions/Edit/UPDATE_COMMISSION';

export const NOTIFICATION_SAGA = 'app/Commissions/Edit/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Commissions/Edit/NOTIFICATION';
