import {
    INITIALIZE,
    UPDATE_COMMISSION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData( payload ) {
    return {
        type: INITIALIZE,
        payload
    };
}

/**
 * Update commission
 */
export function updateCommission( payload ) {
    return {
        type: UPDATE_COMMISSION,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
