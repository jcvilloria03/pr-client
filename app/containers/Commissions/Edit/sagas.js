import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { formatFeedbackMessage } from '../../../utils/functions';
import { RECORD_UPDATED_MESSAGE } from '../../../utils/constants';

import {
    INITIALIZE,
    LOADING,
    SET_SUBMITTED,
    SET_FORM_OPTIONS,
    SET_COMMISSION,
    UPDATE_COMMISSION,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();

        const commission = yield call( Fetch, `/other_income/${payload.commissionId}`, { method: 'GET' });

        const commissionTypes = yield call( Fetch, `/company/${companyId}/other_income_types/commission_type`, { method: 'GET' });
        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL`, { method: 'GET' });
        const departments = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });
        const payrollGroups = yield call( Fetch, `/philippine/company/${companyId}/payroll_groups`, { method: 'GET' });

        const formOptions = {};
        formOptions.commissionTypes = commissionTypes.data.filter( ( type ) => !type.deleted_at || commission.type.id === type.id );
        formOptions.employees = employees.data.filter( ( employee ) => employee.payroll );
        formOptions.departments = departments.data;
        formOptions.payrollGroups = payrollGroups.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });

        yield put({
            type: SET_COMMISSION,
            payload: commission
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Update commission
 */
export function* updateComission({ payload }) {
    try {
        yield put({
            type: SET_SUBMITTED,
            payload: true
        });

        yield call( Fetch, `/philippine/commission/${payload.commissionId}`, { method: 'PATCH', data: payload });

        yield call( showSuccessMessage );
        yield call( delay, 500 );

        payload.previousRouteName ? yield call( browserHistory.goBack ) : yield call( browserHistory.push, '/commissions' );
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForUpdateCommission() {
    const watcher = yield takeEvery( UPDATE_COMMISSION, updateComission );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForUpdateCommission,
    watchForInitializeData,
    watchForReinitializePage,
    watchNotify
];
