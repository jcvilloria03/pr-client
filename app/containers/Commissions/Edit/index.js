import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import decimal from 'js-big-decimal';
import get from 'lodash/get';

import A from '../../../components/A';
import Button from '../../../components/Button';
import SnackBar from '../../../components/SnackBar';
import SalSelect from '../../../components/Select';
import MultiSelect from '../../../components/MultiSelect';
import Input from '../../../components/Input';
import Switch from '../../../components/Switch';
import Loader from '../../../components/Loader';
import DatePicker from '../../../components/DatePicker';
import SubHeader from '../../../containers/SubHeader';
import SalConfirm from '../../../components/SalConfirm';
import { H2, H3 } from '../../../components/Typography';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { getEmployeeFullName, checkIfAnyEmployeesBelongToMultipleGroups, formatDate } from '../../../utils/functions';
import { PAYROLL_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import { COMMISSION_TYPE_BASIS } from '../constants';

import {
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectFormOptions,
    makeSelectNotification,
    makeSelectCommission
} from './selectors';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper,
    MainWrapper,
    LoaderWrapper,
    ConfirmBodyWrapperStyle
} from './styles';

import * as actions from './actions';

/**
 * Edit Commission Component
 */
class Edit extends Component {
    static propTypes = {
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        commission: React.PropTypes.object,
        formOptions: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        params: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        updateCommission: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            commissionForm: {
                type_id: '',
                type_basis: null,
                percentage: '',
                amount: '',
                recipients: {
                    employees: [],
                    payroll_groups: [],
                    departments: []
                },
                release_details: [
                    {
                        disburse_through_special_pay_run: false,
                        date: ''
                    }
                ]
            },
            prepopulatedEmployeeId: '',
            isWarningMessageVisible: false
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['edit.commissions'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        const { id: commissionId } = this.props.params;
        this.props.initializeData({ commissionId });

        const prepopulatedEmployeeId = JSON.parse( localStorage.getItem( 'employeeIdForEditCommission' ) );
        prepopulatedEmployeeId && this.setState({ prepopulatedEmployeeId });
    }

    componentWillReceiveProps( nextProps ) {
        if ( Object.keys( nextProps.commission ).length !== Object.keys( this.props.commission ).length ) {
            this.setInitialCommissionFormFromProps( nextProps );
        }
    }

    componentWillUnmount() {
        localStorage.removeItem( 'employeeIdForEditCommission' );
        this.props.resetStore();
    }

    setInitialCommissionFormFromProps = ( props ) => {
        const { formOptions, commission } = props;

        if ( Object.keys( formOptions ).length === 0 ) {
            return;
        }

        const formOptionsEmployeesIds = formOptions.employees.map( ( e ) => e.id );
        const formOptionsDepartmentsIds = formOptions.departments.map( ( d ) => d.id );
        const formOptionsPayrollGroupsIds = formOptions.payrollGroups.map( ( pg ) => pg.id );

        this.setState({
            commissionForm: {
                type_id: commission.type_id,
                type_basis: commission.type.other_income_type_subtype.basis,
                amount: commission.amount ? `${commission.amount}` : '',
                percentage: commission.percentage ? `${commission.percentage}` : '',
                recipients: {
                    employees: commission.recipients.employees
                        ? commission.recipients.employees
                            .filter( ( e ) => formOptionsEmployeesIds.includes( e.recipient_id ) )
                            .map( ( e ) => {
                                const employee = formOptions.employees.find( ( f ) => f.id === e.recipient_id );

                                return {
                                    label: getEmployeeFullName( employee ),
                                    value: e.recipient_id
                                };
                            })
                        : [],
                    payroll_groups: commission.recipients.payroll_groups
                        ? commission.recipients.payroll_groups
                            .filter( ( pg ) => formOptionsPayrollGroupsIds.includes( pg.recipient_id ) )
                            .map( ( pg ) => {
                                const payrollGroup = formOptions.payrollGroups.find( ( f ) => f.id === pg.recipient_id );

                                return {
                                    label: payrollGroup.name,
                                    value: pg.recipient_id
                                };
                            })
                        : [],
                    departments: commission.recipients.departments
                        ? commission.recipients.departments
                            .filter( ( d ) => formOptionsDepartmentsIds.includes( d.recipient_id ) )
                            .map( ( d ) => {
                                const department = formOptions.departments.find( ( f ) => f.id === d.recipient_id );

                                return {
                                    label: department.name,
                                    value: d.recipient_id
                                };
                            })
                        : []
                },
                release_details: [
                    {
                        id: commission.release_details[ 0 ].id,
                        disburse_through_special_pay_run: commission.release_details[ 0 ].disburse_through_special_pay_run,
                        date: commission.release_details[ 0 ].date
                    }
                ]
            }
        });
    }

    getRecipientsOptions = () => {
        const { formOptions } = this.props;

        if ( Object.keys( formOptions ).length === 0 ) {
            return [];
        }

        const departments = formOptions.departments.map( ( department ) => ({
            value: department.id, label: department.name, field: 'departments'
        }) );
        const payrollGroups = formOptions.payrollGroups.map( ( payrollGroup ) => ({
            value: payrollGroup.id, label: payrollGroup.name, field: 'payroll_groups'
        }) );
        const employees = formOptions.employees.map( ( employee ) => ({
            value: employee.id, label: getEmployeeFullName( employee ), field: 'employees'
        }) );

        return departments.concat( payrollGroups ).concat( employees );
    }

    getCommissionTypesOptions = () => (
        Object.keys( this.props.formOptions ).length === 0
            ? []
            : this.props.formOptions.commissionTypes.map( ({ id, name, basis }) => ({
                value: id, label: name, basis
            }) )
    )

    updateCommissionFormState = ( field, value, onSetStateFinish ) => {
        this.setState({
            commissionForm: {
                ...this.state.commissionForm,
                [ field ]: value
            }
        }, onSetStateFinish );
    }

    validateForm = () => {
        let valid = true;

        if ( !this.recipients._checkRequire( this.recipients.state.value ) ) {
            valid = false;
        }

        if ( this.date.checkRequired() ) {
            valid = false;
        }

        if ( !this.type_id._checkRequire( this.type_id.state.value ) ) {
            valid = false;

            if ( this.amount._validate( this.amount.state.value ) ) {
                valid = false;
            }
        } else {
            const commissionType = this.props.formOptions.commissionTypes.find( ( t ) => t.id === this.type_id.state.value );

            if ( commissionType.basis === COMMISSION_TYPE_BASIS.FIXED ) {
                if ( this.amount._validate( this.amount.state.value ) ) {
                    valid = false;
                }
            } else if ( this.percentage._validate( this.percentage.state.value ) ) {
                valid = false;
            }
        }

        return valid;
    }

    prepareCommissionForSubmit = ( commission ) => {
        const copiedCommission = { ...commission };

        copiedCommission.type_basis === COMMISSION_TYPE_BASIS.FIXED
            ? delete copiedCommission.percentage
            : delete copiedCommission.amount;

        return {
            ...copiedCommission,
            recipients: {
                employees: commission.recipients.employees.map( ( x ) => x.value ),
                payroll_groups: commission.recipients.payroll_groups.map( ( x ) => x.value ),
                departments: commission.recipients.departments.map( ( x ) => x.value )
            }
        };
    }

    updateCommission = () => {
        if ( this.validateForm() ) {
            this.props.updateCommission({
                ...this.prepareCommissionForSubmit( this.state.commissionForm ),
                commissionId: this.props.params.id,
                previousRouteName: this.props.previousRoute.name
            });
        }
    }

    render() {
        const { isWarningMessageVisible, prepopulatedEmployeeId, commissionForm } = this.state;
        const { employees, payroll_groups, departments } = commissionForm.recipients;

        const [releaseDetails] = commissionForm.release_details;
        const {
            disburse_through_special_pay_run: disburseThroughSpecialPayRun,
            date
        } = releaseDetails;

        const currentRecipients = employees.map( ( employee ) => Object.assign( employee, { field: 'employees' }) )
            .concat( payroll_groups.map( ( group ) => Object.assign( group, { field: 'payroll_groups' }) ) )
            .concat( departments.map( ( department ) => Object.assign( department, { field: 'departments' }) ) );

        const includedInPayroll = Boolean( get( this.props.commission, 'release_details[0].payroll_id' ) );
        const disableEdit = disburseThroughSpecialPayRun && includedInPayroll;

        return (
            <PageWrapper>
                <Helmet
                    title="Edit Commission"
                    meta={ [
                        { name: 'description', content: 'Edit a Commission' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    title="Warning"
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Some employees you chose belong to several groups. However, those employees will receive the commission only once.
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    onConfirm={ () => {
                        this.setState({ isWarningMessageVisible: false });
                    } }
                    showCancel={ false }
                    buttonStyle="info"
                    confirmText="Ok"
                    visible={ isWarningMessageVisible }
                />
                <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/commissions' );
                            } }
                        >
                         &#8592; Back to { this.props.previousRoute.name ? this.props.previousRoute.name : 'Commissions' }
                        </A>
                    </Container>
                </NavWrapper>
                { this.props.loading ? (
                    <LoaderWrapper>
                        <H2>Loading</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoaderWrapper>
                ) : (
                    <div>
                        <Container>
                            <HeadingWrapper>
                                <h3>Edit Commission</h3>
                            </HeadingWrapper>
                        </Container>
                        <FormWrapper>
                            <MainWrapper>
                                <div>
                                    <Container>
                                        <div className="row">
                                            <div className="col-xs-4">
                                                <MultiSelect
                                                    id="recipients"
                                                    label={ <span>Recipients</span> }
                                                    placeholder="Search recipients"
                                                    required
                                                    disabled={ !!prepopulatedEmployeeId || disableEdit }
                                                    data={ this.getRecipientsOptions() }
                                                    value={ currentRecipients }
                                                    onChange={ ( values ) => {
                                                        const newRecipientsValue = {
                                                            employees: values.filter( ( v ) => v.field === 'employees' ),
                                                            payroll_groups: values.filter( ( v ) => v.field === 'payroll_groups' ),
                                                            departments: values.filter( ( v ) => v.field === 'departments' )
                                                        };

                                                        this.updateCommissionFormState( 'recipients', newRecipientsValue, () => {
                                                            const belongsToMultipleGroups = checkIfAnyEmployeesBelongToMultipleGroups(
                                                                {
                                                                    employeesIds: newRecipientsValue.employees.map( ( employee ) => employee.value ),
                                                                    departmentsIds: newRecipientsValue.departments.map( ( department ) => department.value ),
                                                                    payrollGroupsIds: newRecipientsValue.payroll_groups.map( ( group ) => group.value )
                                                                },
                                                                this.props.formOptions.employees
                                                            );

                                                            if ( belongsToMultipleGroups ) {
                                                                this.setState({
                                                                    isWarningMessageVisible: true
                                                                });
                                                            }
                                                        });
                                                    } }
                                                    ref={ ( ref ) => { this.recipients = ref; } }
                                                />
                                            </div>
                                            <div className="col-xs-2">
                                                <SalSelect
                                                    id="commision_type"
                                                    label="Commission Type"
                                                    placeholder="Search type"
                                                    required
                                                    disabled={ disableEdit }
                                                    value={ commissionForm.type_id }
                                                    data={ this.getCommissionTypesOptions() }
                                                    onChange={ ({ value, basis }) => {
                                                        this.updateCommissionFormState( 'type_id', value, () => {
                                                            this.updateCommissionFormState( 'type_basis', basis );
                                                        });
                                                    } }
                                                    ref={ ( ref ) => { this.type_id = ref; } }
                                                />
                                            </div>
                                            <div className="col-xs-1">
                                                <div className="switch">
                                                    Special Pay Run
                                                </div>
                                                <Switch
                                                    checked={ disburseThroughSpecialPayRun }
                                                    onChange={ ( value ) => {
                                                        this.updateCommissionFormState( 'release_details', [{
                                                            ...releaseDetails,
                                                            disburse_through_special_pay_run: value
                                                        }]);
                                                    } }
                                                    disabled={ disableEdit }
                                                />
                                            </div>
                                            <div className="col-xs-2 date-picker">
                                                <DatePicker
                                                    label="Release Date"
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    required
                                                    disabled={ disableEdit }
                                                    selectedDay={ date }
                                                    onChange={ ( value ) => {
                                                        const selectedDay = formatDate( value, DATE_FORMATS.API );

                                                        if ( selectedDay !== date ) {
                                                            this.updateCommissionFormState( 'release_details', [{
                                                                ...releaseDetails,
                                                                date: selectedDay
                                                            }]);
                                                        }
                                                    } }
                                                    ref={ ( ref ) => { this.date = ref; } }
                                                />
                                            </div>
                                            <div className="col-xs-2">
                                                { commissionForm.type_basis === COMMISSION_TYPE_BASIS.SALARY_BASED
                                                    ? (
                                                        <Input
                                                            id="percentage"
                                                            label="Percentage"
                                                            placeholder="Desired percentage"
                                                            type="number"
                                                            required
                                                            minNumber={ 1 }
                                                            value={ commissionForm.percentage }
                                                            onChange={ ( value ) => {
                                                                this.updateCommissionFormState( 'percentage', value );
                                                            } }
                                                            onBlur={ ( value ) => {
                                                                this.percentage.setState({ value: decimal.round( value, 2 ) });
                                                                this.updateCommissionFormState( 'percentage', decimal.round( value, 2 ) );
                                                            } }
                                                            ref={ ( ref ) => { this.percentage = ref; } }
                                                        />
                                                    )
                                                    : (
                                                        <Input
                                                            id="amount"
                                                            label="Amount"
                                                            placeholder="Desired amount"
                                                            type="number"
                                                            required
                                                            minNumber={ 1 }
                                                            value={ commissionForm.amount }
                                                            onChange={ ( value ) => {
                                                                this.updateCommissionFormState( 'amount', value );
                                                            } }
                                                            onBlur={ ( value ) => {
                                                                this.amount.setState({ value: decimal.round( value, 2 ) });
                                                                this.updateCommissionFormState( 'amount', decimal.round( value, 2 ) );
                                                            } }
                                                            ref={ ( ref ) => { this.amount = ref; } }
                                                        />
                                                    )
                                                }
                                            </div>
                                        </div>
                                    </Container>
                                    <div className="foot">
                                        <Button
                                            label="Cancel"
                                            type="neutral"
                                            size="large"
                                            onClick={ () => {
                                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/commissions' );
                                            } }
                                        />
                                        <Button
                                            label={ this.props.submitted ? <Loader /> : 'Update' }
                                            type="action"
                                            size="large"
                                            onClick={ this.updateCommission }
                                        />
                                    </div>
                                </div>
                            </MainWrapper>
                        </FormWrapper>
                    </div>
                ) }
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    submitted: makeSelectSubmitted(),
    formOptions: makeSelectFormOptions(),
    commission: makeSelectCommission(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators( actions, dispatch )
);

export default connect( mapStateToProps, mapDispatchToProps )( Edit );
