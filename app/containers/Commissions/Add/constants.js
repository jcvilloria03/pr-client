export const INITIALIZE = 'app/Commissions/Add/INITIALIZE';
export const LOADING = 'app/Commissions/Add/LOADING';
export const SET_SUBMITTED = 'app/Commissions/Add/SET_SUBMITTED';
export const SET_FORM_OPTIONS = 'app/Commissions/Add/SET_FORM_OPTIONS';

export const SUBMIT_FORM = 'app/Commissions/Add/SUBMIT_FORM';

export const UPLOAD_COMMISSIONS = 'app/Commissions/Add/UPLOAD_COMMISSIONS';
export const SET_BATCH_UPLOAD_JOB_ID = 'app/Commissions/Add/SET_BATCH_UPLOAD_JOB_ID';
export const SET_BATCH_UPLOAD_STATUS = 'app/Commissions/Add/SET_BATCH_UPLOAD_STATUS';
export const SET_BATCH_UPLOAD_ERRORS = 'app/Commissions/Add/SET_BATCH_UPLOAD_ERRORS';
export const GET_COMMISSIONS_PREVIEW = 'app/Commissions/Add/GET_COMMISSIONS_PREVIEW';
export const SET_SAVING_STATUS = 'app/Commissions/Add/SET_SAVING_STATUS';
export const SET_SAVING_ERRORS = 'app/Commissions/Add/SET_SAVING_ERRORS';
export const SAVE_COMMISSIONS = 'app/Commissions/Add/SAVE_COMMISSIONS';
export const SET_COMMISSIONS_PREVIEW_STATUS = 'app/Commissions/Add/SET_COMMISSIONS_PREVIEW_STATUS';
export const SET_COMMISSIONS_PREVIEW_DATA = 'app/Commissions/Add/SET_COMMISSIONS_PREVIEW_DATA';
export const RESET_COMMISSIONS_PREVIEW = 'app/Commissions/Add/RESET_COMMISSIONS_PREVIEW';

export const NOTIFICATION_SAGA = 'app/Commissions/Add/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Commissions/Add/NOTIFICATION';
