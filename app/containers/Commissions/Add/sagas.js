import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { formatFeedbackMessage } from '../../../utils/functions';

import {
    INITIALIZE,
    LOADING,
    SUBMIT_FORM,
    SET_SUBMITTED,
    UPLOAD_COMMISSIONS,
    SET_FORM_OPTIONS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    SAVE_COMMISSIONS,
    SET_SAVING_STATUS,
    GET_COMMISSIONS_PREVIEW,
    SET_COMMISSIONS_PREVIEW_STATUS,
    SET_COMMISSIONS_PREVIEW_DATA,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();

        const formOptions = {};
        const commissionTypes = yield call( Fetch, `/company/${companyId}/other_income_types/commission_type`, { method: 'GET' });
        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL`, { method: 'GET' });
        const departments = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });
        const payrollGroups = yield call( Fetch, `/philippine/company/${companyId}/payroll_groups`, { method: 'GET' });

        formOptions.commissionTypes = commissionTypes.data.filter( ( type ) => !type.deleted_at );
        formOptions.employees = employees.data.filter( ( employee ) => employee.payroll );
        formOptions.departments = departments.data;
        formOptions.payrollGroups = payrollGroups.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SET_SUBMITTED, payload: true })
        ];

        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, `/philippine/company/${companyId}/commission/bulk_create`, { method: 'POST', data: payload.commissions });
        yield call( showSuccessMessage );
        yield call( delay, 500 );

        payload.previousRouteName ? yield call( browserHistory.goBack ) : yield call( browserHistory.push, '/commissions' );
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Uploads the CSV of commissions to add and starts the validation process
 */
export function* uploadCommissions({ payload }) {
    try {
        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: {}
        });

        const companyId = company.getLastActiveCompanyId();

        const data = new FormData();

        data.append( 'company_id', company.getLastActiveCompanyId() );
        data.append( 'file', payload.file );

        const upload = yield call( Fetch, `/company/${companyId}/other_income/commission/upload`, {
            method: 'POST',
            data
        });

        if ( upload.id ) {
            yield put({
                type: SET_BATCH_UPLOAD_JOB_ID,
                payload: upload.id
            });

            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: 'validation_queued'
            });

            yield call( checkValidation, { payload: { jobId: upload.id }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const check = yield call(
            Fetch,
            `/company/${companyId}/other_income/commission/upload/status?job_id=${payload.jobId}`,
            { method: 'GET' }
        );

        if ( check.status === 'validation_failed' ) {
            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: check.status
            });

            yield put({
                type: SET_BATCH_UPLOAD_ERRORS,
                payload: check.errors
            });
        } else if ( check.status === 'saved' ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: check.status
            });

            browserHistory.push( '/commissions' );
        } else {
            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: check.status
            });

            yield call( delay, 2000 );
            yield call( checkValidation, { payload: { jobId: payload.jobId }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Fetches preview for uploaded commissions
 */
export function* getCommissionsPreview({ payload }) {
    try {
        yield put({
            type: SET_COMMISSIONS_PREVIEW_DATA,
            payload: []
        });

        yield put({
            type: SET_COMMISSIONS_PREVIEW_STATUS,
            payload: 'busy'
        });

        const preview = yield call(
            Fetch,
            `/company/other_income/commission/upload/preview?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}`,
            { method: 'GET' }
        );

        yield put({
            type: SET_COMMISSIONS_PREVIEW_DATA,
            payload: preview
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_COMMISSIONS_PREVIEW_STATUS,
            payload: 'ready'
        });
    }
}

/**
 * Saves validated commissions
 */
export function* saveCommissions({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const upload = yield call( Fetch, `/company/${companyId}/other_income/commission/upload/save`, {
            method: 'POST',
            data: {
                company_id: companyId,
                job_id: payload.jobId
            }
        });

        if ( upload.id ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: 'save_queued'
            });

            yield call( checkValidation, { payload: {
                step: 'save',
                jobId: payload.jobId
            }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage()
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPLUPLOAD_COMMISSIONSOAD_COMM
 */
export function* watchForUploadCommissions() {
    const watcher = yield takeEvery( UPLOAD_COMMISSIONS, uploadCommissions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_COMMISSIONS_PREVIEW
 */
export function* watchForGetCommissionsPreview() {
    const watcher = yield takeEvery( GET_COMMISSIONS_PREVIEW, getCommissionsPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SAVE_COMMISSIONS
 */
export function* watchForSaveCommissions() {
    const watcher = yield takeEvery( SAVE_COMMISSIONS, saveCommissions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForGetCommissionsPreview,
    watchForUploadCommissions,
    watchForInitializeData,
    watchForReinitializePage,
    watchForSaveCommissions,
    watchNotify
];
