import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import decimal from 'js-big-decimal';

import SnackBar from '../../../../components/SnackBar';
import Loader from '../../../../components/Loader';
import Input from '../../../../components/Input';
import SalSelect from '../../../../components/Select';
import Button from '../../../../components/Button';
import MultiSelect from '../../../../components/MultiSelect';
import Switch from '../../../../components/Switch';
import DatePicker from '../../../../components/DatePicker';
import SalConfirm from '../../../../components/SalConfirm';

import { COMMISSION_TYPE_BASIS } from '../../constants';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { getEmployeeFullName, checkIfAnyEmployeesBelongToMultipleGroups, formatDate } from '../../../../utils/functions';
import { DATE_FORMATS } from '../../../../utils/constants';

import { MainWrapper, ConfirmBodyWrapperStyle } from './styles';

import {
    makeSelectLoading,
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectNotification
} from '../selectors';

import * as createCommissionActions from '../actions';

/**
 * Commissions Manual Entry Component
 */
export class ManualEntry extends React.Component {
    static propTypes = {
        loading: React.PropTypes.bool,
        formOptions: React.PropTypes.object,
        submitted: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        initializeData: React.PropTypes.func,
        submitCommissions: React.PropTypes.func,
        previousRoute: React.PropTypes.object
    }

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            commissions: [],
            prepopulatedEmployee: null,
            commissionForm: {
                type_id: '',
                type_basis: null,
                amount: '',
                percentage: '',
                recipients: {
                    employees: [],
                    payroll_groups: [],
                    departments: []
                },
                release_details: [
                    {
                        disburse_through_special_pay_run: false,
                        date: ''
                    }
                ]
            },
            isWarningMessageVisible: false
        };

        this.commissionsForms = [];
        this.addCommissionForm = {
            recipients: null,
            type_id: null,
            date: null,
            amount: null,
            percentage: null
        };
    }

    componentWillMount() {
        this.props.initializeData();
        let prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddCommission' ) );
        prepopulatedEmployee = prepopulatedEmployee ? {
            value: prepopulatedEmployee.id,
            field: 'employees',
            label: `${prepopulatedEmployee.first_name}${prepopulatedEmployee.middle_name ? ` ${prepopulatedEmployee.middle_name} ` : ' '}${prepopulatedEmployee.last_name}`
        } : null;
        this.setState({ prepopulatedEmployee });
    }

    getRecipientsOptions = () => {
        const { formOptions } = this.props;

        if ( Object.keys( formOptions ).length === 0 ) {
            return [];
        }

        const departments = formOptions.departments.map( ( department ) => ({
            value: department.id, label: department.name, field: 'departments'
        }) );
        const payrollGroups = formOptions.payrollGroups.map( ( payrollGroup ) => ({
            value: payrollGroup.id, label: payrollGroup.name, field: 'payroll_groups'
        }) );
        const employees = formOptions.employees.map( ( employee ) => ({
            value: employee.id, label: getEmployeeFullName( employee ), field: 'employees'
        }) );

        return departments.concat( payrollGroups ).concat( employees );
    }

    getCommissionTypesOptions = () => {
        if ( Object.keys( this.props.formOptions ).length === 0 ) {
            return [];
        }

        return this.props.formOptions.commissionTypes.map( ({ id, name, basis }) => ({
            value: id, label: name, basis
        }) );
    }

    updateCommissionFormState = ( field, value ) => {
        this.setState({
            commissionForm: {
                ...this.state.commissionForm,
                [ field ]: value
            }
        }, () => {
            if ( field === 'recipients' ) {
                const belongsToMultipleGroups = checkIfAnyEmployeesBelongToMultipleGroups(
                    {
                        employeesIds: value.employees.map( ( employee ) => employee.value ),
                        departmentsIds: value.departments.map( ( department ) => department.value ),
                        payrollGroupsIds: value.payroll_groups.map( ( group ) => group.value )
                    },
                    this.props.formOptions.employees
                );

                if ( belongsToMultipleGroups ) {
                    this.setState({
                        isWarningMessageVisible: true
                    });
                }
            }
        });
    }

    updateCommissionAtIndex = ( index, field, value, onSetStateFinish ) => {
        this.setState({
            commissions: [
                ...this.state.commissions.slice( 0, index ),
                {
                    ...this.state.commissions[ index ],
                    [ field ]: value
                },
                ...this.state.commissions.slice( index + 1 )
            ]
        }, onSetStateFinish );
    }

    addCommission = () => {
        if ( this.validateForm( this.addCommissionForm ) ) {
            this.setState({
                commissions: this.state.commissions.concat([this.state.commissionForm]),
                commissionForm: {
                    type_id: '',
                    type_basis: null,
                    amount: '',
                    percentage: '',
                    recipients: {
                        employees: this.state.prepopulatedEmployee ? [this.state.prepopulatedEmployee] : [],
                        payroll_groups: [],
                        departments: []
                    },
                    release_details: [
                        {
                            disburse_through_special_pay_run: false,
                            date: ''
                        }
                    ]
                }
            });
        }
    }

    removeCommissionAtIndex = ( index ) => {
        this.setState({
            commissions: [ ...this.state.commissions.slice( 0, index ), ...this.state.commissions.slice( index + 1 ) ]
        });
    }

    validateForm = ( form ) => {
        let valid = true;

        const {
            recipients,
            type_id,
            date,
            amount,
            percentage
        } = form;

        if ( !recipients._checkRequire( recipients.state.value ) ) {
            valid = false;
        }

        if ( !type_id._checkRequire( type_id.state.value ) ) {
            valid = false;

            if ( amount._validate( amount.state.value ) ) {
                valid = false;
            }
        } else {
            const commissionType = this.props.formOptions.commissionTypes.find( ( t ) => t.id === type_id.state.value );

            if ( commissionType.basis === COMMISSION_TYPE_BASIS.FIXED ) {
                if ( amount._validate( amount.state.value ) ) {
                    valid = false;
                }
            } else if ( percentage._validate( percentage.state.value ) ) {
                valid = false;
            }
        }

        if ( date.checkRequired() ) {
            valid = false;
        }

        return valid;
    }

    prepareCommissionsForSubmit = ( commissions ) => (
        commissions.map( ( commission ) => {
            const copiedCommission = { ...commission };

            copiedCommission.type_basis === COMMISSION_TYPE_BASIS.FIXED
                ? delete copiedCommission.percentage
                : delete copiedCommission.amount;

            return {
                ...copiedCommission,
                recipients: {
                    employees: this.state.prepopulatedEmployee ? [this.state.prepopulatedEmployee.value] : commission.recipients.employees.map( ( x ) => x.value ),
                    payroll_groups: commission.recipients.payroll_groups.map( ( x ) => x.value ),
                    departments: commission.recipients.departments.map( ( x ) => x.value )
                }
            };
        })
    )

    submitCommissions = () => {
        let valid = true;

        this.commissionsForms.forEach( ( form ) => {
            if ( !this.validateForm( form ) ) {
                valid = false;
            }
        });

        if ( valid ) {
            this.props.submitCommissions({
                commissions: this.prepareCommissionsForSubmit( this.state.commissions ),
                previousRouteName: this.props.previousRoute.name
            });
        }
    }

    renderAddedCommissions = () => {
        this.commissionsForms = [];

        this.state.commissions.forEach( () => {
            this.commissionsForms.push(
                {
                    recipients: null,
                    type_id: null,
                    date: null,
                    amount: null,
                    percentage: null
                }
            );
        });

        return this.state.commissions.map( ( commission, index ) => (
            <div key={ index } className="row">
                <div className="col-xs-4">
                    <MultiSelect
                        id="recipients"
                        label={ <span>Recipients</span> }
                        required
                        disabled={ !!this.state.prepopulatedEmployee }
                        placeholder="Search recipients"
                        data={ this.getRecipientsOptions() }
                        value={ this.state.prepopulatedEmployee === null ?
                            commission.recipients.employees.concat(
                            commission.recipients.payroll_groups ).concat(
                            commission.recipients.departments ) : [this.state.prepopulatedEmployee]
                        }
                        onChange={ ( values ) => {
                            const newRecipientsValue = {
                                employees: values.filter( ( v ) => v.field === 'employees' ),
                                payroll_groups: values.filter( ( v ) => v.field === 'payroll_groups' ),
                                departments: values.filter( ( v ) => v.field === 'departments' )
                            };

                            this.updateCommissionAtIndex( index, 'recipients', newRecipientsValue, () => {
                                const belongsToMultipleGroups = checkIfAnyEmployeesBelongToMultipleGroups(
                                    {
                                        employeesIds: newRecipientsValue.employees.map( ( employee ) => employee.value ),
                                        departmentsIds: newRecipientsValue.departments.map( ( department ) => department.value ),
                                        payrollGroupsIds: newRecipientsValue.payroll_groups.map( ( group ) => group.value )
                                    },
                                    this.props.formOptions.employees
                                );

                                if ( belongsToMultipleGroups ) {
                                    this.setState({
                                        isWarningMessageVisible: true
                                    });
                                }
                            });
                        } }
                        ref={ ( ref ) => {
                            if ( this.commissionsForms[ index ]) {
                                this.commissionsForms[ index ].recipients = ref;
                            }
                        } }
                    />
                </div>
                <div className="col-xs-2">
                    <SalSelect
                        id="commision_type"
                        placeholder="Search type"
                        label="Commission Type"
                        required
                        data={ this.getCommissionTypesOptions() }
                        value={ commission.type_id }
                        onChange={ ({ value, basis }) => {
                            this.updateCommissionAtIndex( index, 'type_id', value, () => {
                                this.updateCommissionAtIndex( index, 'type_basis', basis );
                            });
                        } }
                        ref={ ( ref ) => {
                            if ( this.commissionsForms[ index ]) {
                                this.commissionsForms[ index ].type_id = ref;
                            }
                        } }
                    />
                </div>
                <div className="col-xs-1">
                    <div className="switch">
                        Special Pay Run
                    </div>
                    <Switch
                        checked={ commission.release_details[ 0 ].disburse_through_special_pay_run }
                        onChange={ ( value ) => {
                            this.updateCommissionAtIndex( index, 'release_details', [{
                                ...commission.release_details[ 0 ],
                                disburse_through_special_pay_run: value
                            }]);
                        } }
                    />
                </div>
                <div className="col-xs-2 date-picker">
                    <DatePicker
                        label="Release Date"
                        dayFormat={ DATE_FORMATS.DISPLAY }
                        required
                        selectedDay={ commission.release_details[ 0 ].date }
                        onChange={ ( value ) => {
                            const selectedDay = formatDate( value, DATE_FORMATS.API );

                            if ( selectedDay !== commission.release_details[ 0 ].date ) {
                                this.updateCommissionAtIndex( index, 'release_details', [{
                                    ...commission.release_details[ 0 ],
                                    date: selectedDay
                                }]);
                            }
                        } }
                        ref={ ( ref ) => {
                            if ( this.commissionsForms[ index ]) {
                                this.commissionsForms[ index ].date = ref;
                            }
                        } }
                    />
                </div>
                <div className="col-xs-2">
                    { commission.type_basis === COMMISSION_TYPE_BASIS.SALARY_BASED
                        ? (
                            <Input
                                id="percentage"
                                label="Percentage"
                                placeholder="Desired percentage"
                                type="number"
                                required
                                minNumber={ 1 }
                                value={ commission.percentage }
                                onChange={ ( value ) => {
                                    this.updateCommissionAtIndex( index, 'percentage', value );
                                } }
                                onBlur={ ( value ) => {
                                    this.commissionsForms[ index ].percentage.setState({ value: decimal.round( value, 2 ) });
                                    this.updateCommissionAtIndex( index, 'percentage', decimal.round( value, 2 ) );
                                } }
                                ref={ ( ref ) => {
                                    if ( this.commissionsForms[ index ]) {
                                        this.commissionsForms[ index ].percentage = ref;
                                    }
                                } }
                            />
                        )
                        : (
                            <Input
                                id="amount"
                                label="Amount"
                                placeholder="Desired amount"
                                type="number"
                                required
                                minNumber={ 1 }
                                value={ commission.amount }
                                onChange={ ( value ) => {
                                    this.updateCommissionAtIndex( index, 'amount', value );
                                } }
                                onBlur={ ( value ) => {
                                    this.commissionsForms[ index ].amount.setState({ value: decimal.round( value, 2 ) });
                                    this.updateCommissionAtIndex( index, 'amount', decimal.round( value, 2 ) );
                                } }
                                ref={ ( ref ) => {
                                    if ( this.commissionsForms[ index ]) {
                                        this.commissionsForms[ index ].amount = ref;
                                    }
                                } }
                            />
                        )
                    }
                </div>
                <div className="col-xs-1 action remove-action">
                    <Button
                        label="Remove"
                        type="neutral"
                        size="large"
                        onClick={ () => {
                            this.removeCommissionAtIndex( index );
                        } }
                    />
                </div>
            </div>
        ) );
    }

    renderAddNewCommissionForm = () => {
        const { employees, payroll_groups, departments } = this.state.commissionForm.recipients;

        const currentRecipients = employees.concat( payroll_groups ).concat( departments );

        return (
            <div className="row">
                <div className="col-xs-4">
                    <MultiSelect
                        id="recipients"
                        label={ <span>Recipients</span> }
                        placeholder="Search recipients"
                        required
                        disabled={ !!this.state.prepopulatedEmployee }
                        data={ this.getRecipientsOptions() }
                        value={ this.state.prepopulatedEmployee === null ?
                            currentRecipients : [this.state.prepopulatedEmployee]
                        }
                        onChange={ ( values ) => {
                            this.updateCommissionFormState( 'recipients', {
                                employees: values.filter( ( v ) => v.field === 'employees' ),
                                payroll_groups: values.filter( ( v ) => v.field === 'payroll_groups' ),
                                departments: values.filter( ( v ) => v.field === 'departments' )
                            });
                        } }
                        ref={ ( ref ) => { this.addCommissionForm.recipients = ref; } }
                    />
                </div>
                <div className="col-xs-2">
                    <SalSelect
                        id="commision_type"
                        label="Commission Type"
                        placeholder="Search type"
                        required
                        value={ this.state.commissionForm.type_id }
                        data={ this.getCommissionTypesOptions() }
                        onChange={ ({ value, basis }) => {
                            this.setState({
                                commissionForm: {
                                    ...this.state.commissionForm,
                                    type_basis: basis,
                                    type_id: value
                                }
                            });
                        } }
                        ref={ ( ref ) => { this.addCommissionForm.type_id = ref; } }
                    />
                </div>
                <div className="col-xs-1">
                    <div className="switch">
                        Special Pay Run
                    </div>
                    <Switch
                        checked={ this.state.commissionForm.release_details[ 0 ].disburse_through_special_pay_run }
                        onChange={ ( value ) => {
                            this.updateCommissionFormState( 'release_details', [{
                                ...this.state.commissionForm.release_details[ 0 ],
                                disburse_through_special_pay_run: value
                            }]);
                        } }
                    />
                </div>
                <div className="col-xs-2 date-picker">
                    <DatePicker
                        label="Release Date"
                        dayFormat={ DATE_FORMATS.DISPLAY }
                        required
                        selectedDay={ this.state.commissionForm.release_details[ 0 ].date }
                        onChange={ ( value ) => {
                            const selectedDay = formatDate( value, DATE_FORMATS.API );

                            if ( selectedDay !== this.state.commissionForm.release_details[ 0 ].date ) {
                                this.updateCommissionFormState( 'release_details', [{
                                    ...this.state.commissionForm.release_details[ 0 ],
                                    date: selectedDay
                                }]);
                            }
                        } }
                        ref={ ( ref ) => { this.addCommissionForm.date = ref; } }
                    />
                </div>
                <div className="col-xs-2">
                    { this.state.commissionForm.type_basis === COMMISSION_TYPE_BASIS.SALARY_BASED
                        ? (
                            <Input
                                id="percentage"
                                label="Percentage"
                                placeholder="Desired percentage"
                                type="number"
                                required
                                minNumber={ 1 }
                                value={ this.state.commissionForm.percentage }
                                onChange={ ( value ) => {
                                    this.updateCommissionFormState( 'percentage', value );
                                } }
                                onBlur={ ( value ) => {
                                    if ( this.state.commissionForm.percentage !== '' ) {
                                        this.addCommissionForm.percentage.setState({ value: decimal.round( value, 2 ) });
                                        this.updateCommissionFormState( 'percentage', decimal.round( value, 2 ) );
                                    }
                                } }
                                ref={ ( ref ) => { this.addCommissionForm.percentage = ref; } }
                            />
                        )
                        : (
                            <Input
                                id="amount"
                                label="Amount"
                                placeholder="Desired amount"
                                type="number"
                                required
                                minNumber={ 1 }
                                value={ this.state.commissionForm.amount }
                                onChange={ ( value ) => {
                                    this.updateCommissionFormState( 'amount', value );
                                } }
                                onBlur={ ( value ) => {
                                    if ( this.state.commissionForm.amount !== '' ) {
                                        this.addCommissionForm.amount.setState({ value: decimal.round( value, 2 ) });
                                        this.updateCommissionFormState( 'amount', decimal.round( value, 2 ) );
                                    }
                                } }
                                ref={ ( ref ) => { this.addCommissionForm.amount = ref; } }
                            />
                        )
                    }
                </div>
                <div className="col-xs-1 action">
                    <Button
                        label="Add"
                        type="neutral"
                        size="large"
                        onClick={ this.addCommission }
                    />
                </div>
            </div>
        );
    }

    /**
     * Component Render Method
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Add Commissions"
                    meta={ [
                        { name: 'description', content: 'Create a new Commission' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    title="Warning"
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Some employees you chose belong to several groups. However, those employees will receive the commission only once.
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    onConfirm={ () => {
                        this.setState({ isWarningMessageVisible: false });
                    } }
                    showCancel={ false }
                    buttonStyle="info"
                    confirmText="Ok"
                    visible={ this.state.isWarningMessageVisible }
                />
                <MainWrapper>
                    { this.props.loading ? (
                        <div className="loader">
                            <Loader />
                        </div>
                    ) : (
                        <div>
                            <Container>
                                { this.renderAddedCommissions() }
                                { this.renderAddNewCommissionForm() }
                            </Container>
                            <div className="foot">
                                <Button
                                    label="Cancel"
                                    type="neutral"
                                    size="large"
                                    onClick={ () => {
                                        this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/commissions' );
                                    } }
                                />
                                <Button
                                    label={ this.props.submitted ? <Loader /> : 'Submit' }
                                    type="action"
                                    size="large"
                                    disabled={ this.state.commissions.length === 0 }
                                    onClick={ this.submitCommissions }
                                    ref={ ( ref ) => { this.submitButton = ref; } }
                                />
                            </div>
                        </div>
                    ) }
                </MainWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    formOptions: makeSelectFormOptions(),
    submitted: makeSelectSubmitted(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createCommissionActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ManualEntry );
