import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../../components/A';
import Table from '../../../../components/Table';
import Button from '../../../../components/Button';
import Clipboard from '../../../../components/Clipboard';
import FileInput from '../../../../components/FileInput';
import { H4, P } from '../../../../components/Typography';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { formatPaginationLabel, formatCurrency, formatDate } from '../../../../utils/functions';
import { DATE_FORMATS } from '../../../../utils/constants';

import { BASE_PATH_NAME } from '../../../../constants';

import * as actions from '../actions';

import {
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectCommissionsPreview,
    makeSelectSaving
} from '../selectors';

import { StyledLoader, PageWrapper } from './styles';

/**
 * Commissions Batch Upload Component
 */
class BatchUpload extends React.Component {
    static propTypes = {
        batchUploadJobId: React.PropTypes.string,
        uploadCommissions: React.PropTypes.func,
        batchUploadStatus: React.PropTypes.string,
        batchUploadErrors: React.PropTypes.oneOfType([
            React.PropTypes.object,
            React.PropTypes.array
        ]),
        commissionsPreview: React.PropTypes.shape({
            status: React.PropTypes.string,
            data: React.PropTypes.array
        }),
        saving: React.PropTypes.shape({
            status: React.PropTypes.string,
            errors: React.PropTypes.object
        }),
        saveCommissions: React.PropTypes.func
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            files: [],
            status: null,
            errors: {},
            clipboard: '',
            submit: false,
            commissionsTableLabel: 'Showing 0-0 of 0 entries',
            errorsTableLabel: 'Showing 0-0 of 0 entries',
            commissionsPreview: {
                data: [],
                status: 'ready'
            },
            savingStatus: '',
            savingErrors: {}
        };
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.batchUploadStatus !== this.props.batchUploadStatus && this.setState({
            status: nextProps.batchUploadStatus
        }, () => {
            this.validateButton.setState({
                disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.batchUploadStatus )
            });
        });

        nextProps.batchUploadErrors !== this.props.batchUploadErrors && this.setState({
            errors: nextProps.batchUploadErrors
        }, () => {
            let clipboard = '';
            const rows = Object.keys( nextProps.batchUploadErrors );

            if ( rows.length > 0 ) {
                rows.forEach( ( row ) => {
                    Object.values( nextProps.batchUploadErrors[ row ]).forEach( ( value ) => {
                        clipboard += `Row ${row}, ${value}\n`;
                    });
                });
            }

            this.handleErrorsTableChanges();
            this.setState({ clipboard });
        });

        nextProps.commissionsPreview.data !== this.props.commissionsPreview.data && this.setState({
            commissionsPreview: { ...this.state.commissionsPreview, data: nextProps.commissionsPreview.data }
        }, () => {
            this.handleCommissionsTableChanges();
        });

        nextProps.commissionsPreview.status !== this.props.commissionsPreview.status && this.setState({
            commissionsPreview: { ...this.state.commissionsPreview, status: nextProps.commissionsPreview.status }
        });

        nextProps.saving.status !== this.props.saving.status && this.setState({
            savingStatus: nextProps.saving.status
        }, () => {
            this.submitButton.setState({ disabled: nextProps.saving.status !== 'save_failed' });
        });
    }

    handleErrorsTableChanges = () => {
        if ( !this.errorsTable ) {
            return;
        }

        this.setState({
            errorsTableLabel: formatPaginationLabel( this.errorsTable.tableComponent.state )
        });
    };

    handleCommissionsTableChanges = () => {
        if ( !this.commissionsTable ) {
            return;
        }

        this.setState({
            commissionsTableLabel: formatPaginationLabel( this.commissionsTable.tableComponent.state )
        });
    };

    renderCommissionsSection = () => {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Recipients',
                accessor: 'recipients',
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.recipients}
                    </div>
                )
            },
            {
                header: 'Amount',
                accessor: 'amount',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.amount}
                    </div>
                )
            },
            {
                header: 'Commission Type',
                accessor: 'commission_type',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.commission_type}
                    </div>
                )
            },
            {
                header: 'Release Date',
                accessor: 'release_date',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.release_date}
                    </div>
                )
            },
            {
                header: 'Special Pay Run',
                accessor: 'disburse_through_special_pay_run',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.disburse_through_special_pay_run}
                    </div>
                )
            }
        ];

        const dataForDisplay = this.state.commissionsPreview.data.map( ( data ) => ({
            ...data,
            amount: data.amount === '' ? `${formatCurrency( data.percentage )} %` : `Php ${formatCurrency( data.amount )}`,
            recipients: data.employee_id,
            release_date: formatDate( data.date, DATE_FORMATS.DISPLAY )
        }) );

        return this.state.commissionsPreview.data.length === 0
            ? null
            : (
                <div>
                    <H4>Commissions List:</H4>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.commissionsTableLabel }
                        </span>
                    </div>
                    <Table
                        columns={ tableColumns }
                        data={ dataForDisplay }
                        pagination
                        ref={ ( ref ) => { this.commissionsTable = ref; } }
                        onDataChange={ this.handleCommissionsTableChanges }
                    />
                </div>
            );
    };

    renderErrorsSection = () => {
        const errorDisplay = [];
        const errorList = this.state.errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'Row Number',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'Error Type',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            const keys = Object.keys( errorList );

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row {key}</H4>,
                    error: <ul>{Object.values( errorList[ key ]).map( ( value ) => <li key={ value } style={ { margin: '0' } } >{value}</li> )}</ul>
                });
            });

            return (
                <div className="errors">
                    <H4>Data field errors</H4>
                    <p>There seems to be an error in the file you uploaded. Review the list below, correct the errors and upload the file again.</p>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.errorsTableLabel }
                        </span>
                        <Clipboard value={ this.state.clipboard } />
                    </div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                        ref={ ( ref ) => { this.errorsTable = ref; } }
                        onDataChange={ this.handleErrorsTableChanges }
                    />
                </div>
            );
        }

        return null;
    }

    render() {
        return (
            <PageWrapper>
                <Container>
                    <p className="sl-u-gap-top--lg sl-u-gap-bottom--lg">
                        Follow the steps below for adding commissions via batch upload. A template will be available for you to fill out.
                    </p>

                    <div className="steps">
                        <div className="step">
                            <div className="template">
                                <H4>Step 1:</H4>
                                <P>Download and fill-out the Employee Payroll Information Template.</P>
                                <P><A target="_blank" href={ `${BASE_PATH_NAME}/guides/commissions/batch-upload` }>Click here to view the upload guide.</A></P>
                                <A className="sl-c-btn--wide" href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/guides/commissions_payroll_template.csv" download>Download Template</A>
                            </div>
                        </div>
                        <div className="step">
                            <div className="upload">
                                <H4>Step 2:</H4>
                                <P>After completely filling out the template, choose and upload it here.</P>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'none' : 'block' } } >
                                    <FileInput
                                        accept=".csv"
                                        onDrop={ ( files ) => {
                                            const { acceptedFiles } = files;

                                            this.setState({
                                                files: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null,
                                                errors: {},
                                                status: null
                                            }, () => {
                                                this.validateButton.setState({ disabled: acceptedFiles.length <= 0 });
                                            });
                                        } }
                                        ref={ ( ref ) => { this.fileInput = ref; } }
                                    />
                                </div>
                                <div style={ { display: this.state.status === 'validated' ? 'block' : 'none' } }>
                                    <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                                </div>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'validated' ? 'none' : 'block' } } >
                                    <Button
                                        label={
                                            this.state.submit ? (
                                                <StyledLoader className="animation">
                                                    Uploading <div className="anim3"></div>
                                                </StyledLoader>
                                            ) : (
                                                'Upload'
                                            )
                                        }
                                        disabled={ this.state.files ? this.state.files.length <= 0 : true }
                                        type="neutral"
                                        ref={ ( ref ) => { this.validateButton = ref; } }
                                        onClick={ () => {
                                            this.setState({
                                                errors: {},
                                                status: null,
                                                submit: true
                                            }, () => {
                                                this.validateButton.setState({ disabled: true }, () => {
                                                    this.props.uploadCommissions({ file: this.state.files });

                                                    setTimeout( () => {
                                                        this.validateButton.setState({ disabled: false });
                                                        this.setState({ submit: false });
                                                    }, 5000 );
                                                });
                                            });
                                        } }
                                    />
                                </div>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'block' : 'none' } } >
                                    <StyledLoader className="animation">
                                        <H4 style={ { margin: '0' } }>{ this.state.status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                    </StyledLoader>
                                </div>
                            </div>
                        </div>
                    </div>
                    { this.renderCommissionsSection() }
                    { this.renderErrorsSection() }
                </Container>
                <div className="foot">
                    <Container>
                        <Button
                            label="Cancel"
                            type="neutral"
                            size="large"
                            onClick={ () => {
                                const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddCommission' ) );
                                prepopulatedEmployee && prepopulatedEmployee.id === ''
                                    ? browserHistory.push( `/employee/${prepopulatedEmployee.id}`, true )
                                    : browserHistory.push( '/commissions' );
                            } }
                        />
                        <Button
                            ref={ ( ref ) => { this.submitButton = ref; } }
                            type="action"
                            disabled={ this.state.status !== 'validated' }
                            label={
                                this.state.savingStatus === 'save_queued' || this.state.savingStatus === 'saving' ?
                                    (
                                        <StyledLoader className="animation">
                                            <span>Saving</span> <div className="anim3"></div>
                                        </StyledLoader>
                                    ) : 'Submit'
                            }
                            size="large"
                            onClick={ () => {
                                this.submitButton.setState({ disabled: true });
                                this.setState({ savingStatus: 'save_queued' });
                                this.props.saveCommissions({ jobId: this.props.batchUploadJobId });
                            } }
                        />
                    </Container>
                </div>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    batchUploadJobId: makeSelectBatchUploadJobId(),
    batchUploadStatus: makeSelectBatchUploadStatus(),
    batchUploadErrors: makeSelectBatchUploadErrors(),
    commissionsPreview: makeSelectCommissionsPreview(),
    saving: makeSelectSaving()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( BatchUpload );
