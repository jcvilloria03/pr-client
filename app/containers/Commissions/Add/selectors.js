import { createSelector } from 'reselect';

/**
 * Direct selector to the add commissions state domain
 */
const selectAddCommissionsDomain = () => ( state ) => state.get( 'addCommissions' );

const makeSelectLoading = () => createSelector(
    selectAddCommissionsDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectFormOptions = () => createSelector(
    selectAddCommissionsDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectAddCommissionsDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectAddCommissionsDomain(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectAddCommissionsDomain(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectAddCommissionsDomain(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectCommissionsPreview = () => createSelector(
    selectAddCommissionsDomain(),
    ( substate ) => substate.get( 'commissionsPreview' ).toJS()
);

const makeSelectSaving = () => createSelector(
    selectAddCommissionsDomain(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectAddCommissionsDomain(),
    ( substate ) => substate.get( 'submitted' )
);

export {
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectCommissionsPreview,
    makeSelectSaving,
    makeSelectSubmitted,
    makeSelectNotification
};
