import {
    INITIALIZE,
    SUBMIT_FORM,
    UPLOAD_COMMISSIONS,
    SAVE_COMMISSIONS,
    NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData() {
    return {
        type: INITIALIZE
    };
}

/**
 * Submit commissions
 */
export function submitCommissions( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}

/**
 * Upload commissions
 */
export function uploadCommissions( payload ) {
    return {
        type: UPLOAD_COMMISSIONS,
        payload
    };
}

/**
 * Save commissions
 */
export function saveCommissions( payload ) {
    return {
        type: SAVE_COMMISSIONS,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}

