import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import fileDownload from 'js-file-download';

import {
    SET_LOADING,
    SET_DOWNLOADING,
    GET_COMMISSIONS,
    SET_COMMISSIONS,
    SET_FORM_OPTIONS,
    GET_PAGINATED_COMMISSIONS,
    SET_PAGINATION,
    SET_FILTER_DATA,
    DELETE_COMMISSIONS,
    IS_DELETE_AVAILABLE,
    SET_NUMBER_OF_DELETE_UNAVAILABLE,
    EXPORT_COMMISSIONS,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

/**
 * Get commissions for table
 */
export function* getCommissions( filters ) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        yield call( getPaginatedCommissions, {
            payload: {
                page: 1,
                perPage: 10,
                showLoading: true
            },
            filters
        });

        const companyId = company.getLastActiveCompanyId();
        const commissionTypes = yield call( Fetch, `/company/${companyId}/other_income_types/commission_type`, { method: 'GET' });
        const departments = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });
        const payrollGroups = yield call( Fetch, `/philippine/company/${companyId}/payroll_groups`, { method: 'GET' });

        const filterData = {};
        filterData.commissionTypes = commissionTypes.data;
        filterData.departments = departments.data;
        filterData.payrollGroups = payrollGroups.data;

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * get paginated commissions
 */
export function* getPaginatedCommissions({ payload, filters }) {
    const { page, perPage, showLoading } = payload;
    const commissionFilter = filters.filters ? filters.filters : filters;

    try {
        if ( showLoading ) {
            yield put({
                type: SET_LOADING,
                payload: true
            });
        }

        let filtersQueryParams = '';
        if ( commissionFilter !== undefined && commissionFilter.length > 0 ) {
            const filtersIds = {
                other_income_type_ids: [],
                department_ids: [],
                employee_ids: [],
                payroll_group_ids: []
            };

            commissionFilter.map( ( filter ) => {
                if ( filter.type !== 'keyword' ) {
                    const filterFieldName = filter.type === 'commission_type' ? 'other_income_type_ids' : `${filter.type}_ids`;
                    if ( filter.disabled === false ) {
                        filtersIds[ filterFieldName ].push( filter.value );
                    }
                } else {
                    filtersQueryParams += `&filter[keyword]=${filter.value}`;
                }

                return null;
            });

            for ( const [ field, ids ] of Object.entries( filtersIds ) ) {
                if ( ids.length > 0 ) {
                    for ( const filterId of ids ) {
                        filtersQueryParams += `&filter[${field}][]=${filterId}`;
                    }
                }
            }
        }

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_incomes/commission_type?page=${page}&per_page=${perPage}${filtersQueryParams}`, { method: 'GET' });
        const { data, ...others } = response;

        yield put({
            type: SET_PAGINATION,
            payload: others
        });

        yield put({
            type: SET_COMMISSIONS,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        if ( showLoading ) {
            yield put({
                type: SET_LOADING,
                payload: false
            });
        }
    }
}

/**
 * Get Company Employees
 */
export function* getCompanyEmployees({ payload }) {
    try {
        const keyword = payload.keyword ? payload.keyword : '';
        const companyId = company.getLastActiveCompanyId();
        const formOptions = payload.formOptions;
        let filterEmployeeIdsQueryString = '';

        if ( payload.filterEmployeeIds ) {
            const filterEmployeeIds = payload.filterEmployeeIds;
            if ( filterEmployeeIds.length > 0 ) {
                for ( const employeeId of filterEmployeeIds ) {
                    filterEmployeeIdsQueryString += `&filter[employee_ids][]=${employeeId}`;
                }
            }
        }

        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL&keyword=${keyword}${filterEmployeeIdsQueryString}`, { method: 'GET' });

        formOptions.employees = employees.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Batch delete commissions
 */
export function* checkIsDeleteCommissionsAvailable({ payload }) {
    try {
        yield put({
            type: SET_NUMBER_OF_DELETE_UNAVAILABLE,
            payload: null
        });

        const companyId = company.getLastActiveCompanyId();

        const response = yield call( Fetch, `/company/${companyId}/other_income/is_delete_available`, {
            method: 'POST',
            data: {
                ids: payload
            }
        });

        yield put({
            type: SET_NUMBER_OF_DELETE_UNAVAILABLE,
            payload: response.unavailable
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Batch delete commissions
 */
export function* deleteCommissions({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, `/company/${companyId}/other_income`, {
            method: 'DELETE',
            data: {
                ids: payload,
                company_id: company.getLastActiveCompanyId()
            }
        });

        yield call( getCommissions );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Export commissions
 */
export function* exportCommissions({ payload }) {
    try {
        yield put({
            type: SET_DOWNLOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_incomes/commission_type/download`, {
            method: 'POST',
            data: { ids: payload }
        });

        fileDownload( response, 'commissions.csv' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DOWNLOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getCommissions );
}

/**
 * Watcher for GET_COMMISSIONS
 */
export function* watchForGetCommissions() {
    const watcher = yield takeEvery( GET_COMMISSIONS, getCommissions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for IS_DELETE_AVAILABLE
 */
export function* watchForCheckIsDeleteCommissionsAvailable() {
    const watcher = yield takeEvery( IS_DELETE_AVAILABLE, checkIsDeleteCommissionsAvailable );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE_COMMISSIONS
 */
export function* watchForDeleteCommissions() {
    const watcher = yield takeEvery( DELETE_COMMISSIONS, deleteCommissions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for EXPORT_COMMISSIONS
 */
export function* watchForExportCommissions() {
    const watcher = yield takeEvery( EXPORT_COMMISSIONS, exportCommissions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Paginated bonuses
 */
export function* watchForPaginatedCommissions() {
    const watcher = yield takeEvery( GET_PAGINATED_COMMISSIONS, getPaginatedCommissions );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetCommissions,
    watchForPaginatedCommissions,
    watchForDeleteCommissions,
    watchForCheckIsDeleteCommissionsAvailable,
    watchForExportCommissions,
    watchForNotifyUser,
    watchForReinitializePage
];
