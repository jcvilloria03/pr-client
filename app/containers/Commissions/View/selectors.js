import { createSelector } from 'reselect';

/**
 * Direct selector to the commissions state domain
 */
const selectViewCommissionsDomain = () => ( state ) => state.get( 'commissions' );

const makeSelectLoading = () => createSelector(
    selectViewCommissionsDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectDownloading = () => createSelector(
    selectViewCommissionsDomain(),
    ( substate ) => substate.get( 'downloading' )
);

const makeSelectNumberOfDeleteUnavailable = () => createSelector(
    selectViewCommissionsDomain(),
    ( substate ) => substate.get( 'numberOfDeleteUnavailable' )
);

const makeSelectCommissions = () => createSelector(
    selectViewCommissionsDomain(),
  ( substate ) => substate.get( 'commissions' ).toJS()
);

const makeSelectFilterData = () => createSelector(
    selectViewCommissionsDomain(),
  ( substate ) => substate.get( 'filterData' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewCommissionsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectPagination = () => createSelector(
  selectViewCommissionsDomain(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectDownloading,
  makeSelectNumberOfDeleteUnavailable,
  makeSelectCommissions,
  makeSelectFilterData,
  makeSelectNotification,
  makeSelectPagination
};
