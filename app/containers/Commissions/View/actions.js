import {
    GET_COMMISSIONS,
    GET_PAGINATED_COMMISSIONS,
    DELETE_COMMISSIONS,
    EXPORT_COMMISSIONS,
    IS_DELETE_AVAILABLE,
    NOTIFICATION
} from './constants';

/**
 * Fetch list of commissions
 */
export function getCommissions( filters ) {
    return {
        type: GET_COMMISSIONS,
        filters
    };
}

/**
 * Fetch list of commissions
 */
export function getPaginatedCommissions( payload, filters ) {
    return {
        type: GET_PAGINATED_COMMISSIONS,
        filters,
        payload,
        showLoading: true
    };
}

/**
 * Check is delete available for commissions
 * @param {array} IDs
 */
export function checkIsDeleteAvailable( IDs ) {
    return {
        type: IS_DELETE_AVAILABLE,
        payload: IDs
    };
}

/**
 * Delete commissions
 * @param {array} IDs
 */
export function deleteCommissions( IDs ) {
    return {
        type: DELETE_COMMISSIONS,
        payload: IDs
    };
}

/**
 * Export commissions
 * @param {array} IDs
 */
export function exportCommissions( IDs ) {
    return {
        type: EXPORT_COMMISSIONS,
        payload: IDs
    };
}

/**
 * Display a notification to a user
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
