export const FILTER_TYPES = {
    COMMISSION_TYPE: 'commission_type',
    PAYROLL_GROUP: 'payroll_group',
    EMPLOYEE: 'employee',
    DEPARTMENT: 'department'
};
