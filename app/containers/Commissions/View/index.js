import React from 'react';
import orderBy from 'lodash/orderBy';
import isEqual from 'lodash/isEqual';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate
} from '../../../utils/functions';
import { PAYROLL_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import {
    makeSelectLoading,
    makeSelectCommissions,
    makeSelectFilterData,
    makeSelectNumberOfDeleteUnavailable,
    makeSelectNotification,
    makeSelectDownloading,
    makeSelectPagination
} from './selectors';
import * as commissionsActions from './actions';

import { COMMISSION_TYPE_BASIS } from '../constants';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';

import SubHeader from '../../SubHeader';
import Loader from '../../../components/Loader';
import SnackBar from '../../../components/SnackBar';
import Table from '../../../components/Table';
import { H2, H3, H5 } from '../../../components/Typography';
import Button from '../../../components/Button';
import Input from '../../../components/Input';
import SalConfirm from '../../../components/SalConfirm';
import Icon from '../../../components/Icon';
import Filter from './templates/filter';

/**
 * View Commissions Component
 */
export class View extends React.Component {
    static propTypes = {
        getCommissions: React.PropTypes.func,
        getPaginatedCommissions: React.PropTypes.func,
        checkIsDeleteAvailable: React.PropTypes.func,
        deleteCommissions: React.PropTypes.func,
        exportCommissions: React.PropTypes.func,
        numberOfDeleteUnavailable: React.PropTypes.number,
        commissions: React.PropTypes.array,
        filterData: React.PropTypes.shape({
            commissionTypes: React.PropTypes.array,
            departments: React.PropTypes.array,
            payrollGroups: React.PropTypes.array
        }),
        loading: React.PropTypes.bool,
        downloading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        pagination: React.PropTypes.array,
        products: React.PropTypes.array
    };

    static defaultProps = {
        loading: true,
        downloading: false,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: this.props.commissions,
            showDelete: false,
            deleteLabel: '',
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            showModal: false,
            showIsDeleteAvailableModal: false,
            showFilter: false,
            hasFiltersApplied: false,
            previousQuery: '',
            filtersApplied: []
        };

        this.searchInput = null;
        this.searchTypingTimeout = 0;
        this.deleteCommissions = this.deleteCommissions.bind( this );
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.commissions',
            'create.commissions',
            'delete.commissions',
            'edit.commissions'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.commissions' ];

            if ( authorized ) {
                this.setState({ permission: {
                    view: authorization[ 'view.commissions' ],
                    create: authorization[ 'create.commissions' ],
                    delete: authorization[ 'delete.commissions' ],
                    edit: authorization[ 'edit.commissions' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.getCommissions();
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.commissions, this.props.commissions ) ) {
            this.handleSearch( nextProps.commissions );
        }

        if ( this.props.numberOfDeleteUnavailable === null && nextProps.numberOfDeleteUnavailable !== null ) {
            if ( nextProps.numberOfDeleteUnavailable === 0 ) {
                this.deleteCommissions();
                return;
            }

            this.setState({
                showIsDeleteAvailableModal: nextProps.numberOfDeleteUnavailable !== 0
            });
        }
    }

    handleTableChanges = ( tableProps = this.commissionsTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });

        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    handleSearch = ( commissions = this.props.commissions ) => {
        let searchQuery = null;
        const dataToDisplay = commissions;

        if ( this.searchInput ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( (searchQuery != this.state.previousQuery) && searchQuery || ( this.searchInput && searchQuery === '' ) ) {
            const currentFilters = this.state.filtersApplied.filter( ( filter ) => filter.type !== 'keyword' );
            const filtersToApply = [
                ...currentFilters,
                {
                    disabled: false,
                    type: 'keyword',
                    value: searchQuery
                }
            ];

            if ( this.searchTypingTimeout !== 0 ) {
                clearTimeout( this.searchTypingTimeout );
            }
            this.searchTypingTimeout = setTimeout( () => {
                if ( searchQuery !== '' || this.state.previousQuery !== '' ) {
                    this.props.getPaginatedCommissions({ page: 1, perPage: this.props.pagination.per_page, showLoading: false }, filtersToApply );
                }

                this.setState({
                    previousQuery: searchQuery,
                    filtersApplied: filtersToApply,
                    displayedData: this.props.commissions,
                    hasFiltersApplied: true
                }, () => {
                    this.handleTableChanges();
                });
            }, 500 );
        } else {
            this.setState({ displayedData: dataToDisplay }, () => {
                this.handleTableChanges();
            });
        }
    }

    checkIsDeleteAvailable = () => {
        const commissionsIDs = [];
        this.commissionsTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                commissionsIDs.push( this.commissionsTable.props.data[ index ].id );
            }
        });

        this.props.checkIsDeleteAvailable( commissionsIDs );
    }

    deleteCommissions = () => {
        const commissionsIDs = [];
        this.commissionsTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                commissionsIDs.push( this.commissionsTable.props.data[ index ].id );
            }
        });

        this.props.deleteCommissions( commissionsIDs );
        this.setState({ showDelete: false });
    }

    exportCommissions = () => {
        const selectedCommissions = [];

        const sortingOptions = this.commissionsTable.state.sorting;

        this.commissionsTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedCommissions.push( this.commissionsTable.props.data[ index ]);
            }
        });

        const sortedCommissions = orderBy(
            selectedCommissions,
            sortingOptions.map( ( o ) => o.id ),
            sortingOptions.map( ( o ) => ( o.desc ? 'desc' : 'asc' ) )
        );

        this.props.exportCommissions( sortedCommissions.map( ( commission ) => commission.id ) );
    };

    toggleFilter = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    applyFilters = ( filters ) => {
        this.props.getCommissions( filters );
        this.setState({
            filtersApplied: filters,
            hasFiltersApplied: !!filters.length
        }, () => {
            this.handleTableChanges();
        });
    }

    resetFilters = () => {
        this.setState({
            showFilter: false,
            displayedData: this.props.commissions,
            hasFiltersApplied: false
        });
    }

    prepareRecipientsForDisplay = ( recipients ) => {
        let departmentsNames = [];
        if ( this.props.filterData.departments ) {
            const filterDataDepartmentsIds = this.props.filterData.departments.map( ( d ) => d.id );
            const departments = recipients.departments
                ? recipients.departments.filter( ( d ) => filterDataDepartmentsIds.includes( d.recipient_id ) )
                : [];

            departmentsNames = departments.map( ( d ) =>
                this.props.filterData.departments.find( ( fd ) => fd.id === d.recipient_id ).name
            );
        }

        let payrollGroupsNames = [];
        if ( this.props.filterData.payrollGroups ) {
            const filterDataPayrollGroupsIds = this.props.filterData.payrollGroups.map( ( pg ) => pg.id );
            const payrollGroups = recipients.payroll_groups
                ? recipients.payroll_groups.filter( ( pg ) => filterDataPayrollGroupsIds.includes( pg.recipient_id ) )
                : [];

            payrollGroupsNames = payrollGroups.map( ( p ) =>
                this.props.filterData.payrollGroups.find( ( fpg ) => fpg.id === p.recipient_id ).name
            );
        }

        const employeesNames = recipients.employees ? recipients.employees.map( ( e ) => {
            const firstName = e.first_name ? `${e.first_name} ` : '';
            const middleName = e.middle_name ? `${e.middle_name} ` : '';
            const lastName = e.last_name ? e.last_name : '';

            return `${firstName}${middleName}${lastName}`;
        }) : [];

        return employeesNames.concat(
            departmentsNames
        ).concat(
            payrollGroupsNames
        ).reduce( ( string, name ) => `${name}, ${string}`, '' ).slice( 0, -2 );
    }

    /**
     * Component Render Method
     */
    render() {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Recipients',
                accessor: 'recipients',
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.recipients}
                    </div>
                )
            },
            {
                header: 'Amount',
                accessor: 'amount',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        { row.type.other_income_type_subtype.basis === COMMISSION_TYPE_BASIS.FIXED
                            ? `Php ${formatCurrency( row.amount )}`
                            : `${formatCurrency( row.percentage )} %`
                        }
                    </div>
                )
            },
            {
                header: 'Commission Type',
                accessor: 'type_name',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.type_name}
                    </div>
                )
            },
            {
                header: 'Release Date',
                accessor: 'release_date',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.release_date}
                    </div>
                )
            },
            {
                header: 'Status',
                accessor: 'status',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.status}
                    </div>
                )
            },
            {
                header: 'Special Pay Run',
                accessor: 'special_pay_run',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.special_pay_run}
                    </div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.state.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        disabled={ row.status === 'Released' }
                        to={ `/commissions/${row.id}/edit` }
                    />
                )
            }
        ];

        const dataForDisplay = this.state.displayedData ? this.state.displayedData.map( ( data ) => ({
            ...data,
            recipients: this.prepareRecipientsForDisplay( data.recipients ),
            type_name: data.type.name,
            release_date: formatDate( data.release_details[ 0 ].date, DATE_FORMATS.DISPLAY ),
            status: data.release_details[ 0 ].status ? 'Released' : 'Not yet released',
            special_pay_run: data.release_details[ 0 ].disburse_through_special_pay_run ? 'Yes' : 'No'
        }) ) : [];

        return (
            <div>
                <Helmet
                    title="View Commissions"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.checkIsDeleteAvailable }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                            Are you sure you want to delete these Commission?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Commissions"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <SalConfirm
                    onConfirm={ () => { this.setState({ showIsDeleteAvailableModal: false }); } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                {`${this.props.numberOfDeleteUnavailable} commission(s) cannot be deleted.`}
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Cannot Delete Commissions"
                    showCancel={ false }
                    buttonStyle="info"
                    confirmText="Ok"
                    visible={ this.state.showIsDeleteAvailableModal }
                />
                <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Commissions.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Commissions</H3>
                                <p>Add your employees&apos; commissions through this page. You can add commissions to an individual employee or group of employees.</p>
                                <div style={ { textAlign: 'center' } } >
                                    <div className="main">
                                        <Button
                                            className={ this.state.permission.create ? '' : 'hide' }
                                            label="Add Commissions"
                                            size="large"
                                            type="action"
                                            to="/commissions/add"
                                        />
                                        <Button
                                            className={ this.state.permission.create ? '' : 'hide' }
                                            label="Add Commission Types"
                                            size="large"
                                            type="neutral"
                                            onClick={ () => {
                                                window.location.href = '/company-settings/payroll/commission-types/add';
                                            } }
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="title">
                                <H5>Commissions List</H5>
                                <div className="search-wrapper">
                                    <Input
                                        className="search"
                                        id="search"
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                        onChange={ () => { this.handleSearch(); } }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                    />
                                </div>
                                <span>
                                    {
                                        this.state.showDelete ?
                                            (
                                                <div>
                                                    { this.state.deleteLabel }
                                                    &nbsp;

                                                    <Button
                                                        className={ this.state.permission.view ? '' : 'hide' }
                                                        label={ this.props.downloading ? <Loader /> : <span>Download</span> }
                                                        type="neutral"
                                                        onClick={ this.exportCommissions }
                                                    />
                                                    <Button
                                                        className={ this.state.permission.delete ? '' : 'hide' }
                                                        label={ <span>Delete</span> }
                                                        type="danger"
                                                        onClick={ () => {
                                                            this.setState({ showModal: false }, () => {
                                                                this.setState({ showModal: true });
                                                            });
                                                        } }
                                                    />
                                                </div>
                                            )
                                            :
                                            (
                                                <div>
                                                    { this.state.label }
                                                    &nbsp;
                                                    <Button
                                                        label={ <span><Icon className="filter-icon" name="filter" /> Filter</span> }
                                                        type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                                        onClick={ this.toggleFilter }
                                                    />
                                                </div>

                                            )
                                    }
                                </span>
                            </div>
                            <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                                <Filter
                                    filterData={ this.props.filterData }
                                    onCancel={ () => { this.resetFilters(); } }
                                    onApply={ ( values ) => { this.applyFilters( values ); } }
                                />
                            </div>
                            <Table
                                data={ dataForDisplay }
                                columns={ tableColumns }
                                pagination
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.commissionsTable = ref; } }
                                selectable
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;

                                    this.setState({
                                        showDelete: selectionLength > 0,
                                        deleteLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                                onPageChange={ ( data ) => this.props.getPaginatedCommissions({ page: data + 1, perPage: this.props.pagination.per_page, showLoading: true }, this.state.filtersApplied ) }
                                onPageSizeChange={ ( data ) => this.props.getPaginatedCommissions({ page: 1, perPage: data, showLoading: true }, this.state.filtersApplied ) }
                                page={ this.props.pagination.current_page - 1 }
                                pageSize={ this.props.pagination.per_page }
                                pages={ this.props.pagination.last_page }
                                manual
                            />
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    downloading: makeSelectDownloading(),
    numberOfDeleteUnavailable: makeSelectNumberOfDeleteUnavailable(),
    commissions: makeSelectCommissions(),
    filterData: makeSelectFilterData(),
    notification: makeSelectNotification(),
    pagination: makeSelectPagination()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        commissionsActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
