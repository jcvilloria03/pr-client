/*
 *
 * License constants
 *
 */
const namespace = 'app/Licenses';

export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;
export const SET_LICENSES_DETAILS = `${namespace}/SET_LICENSES_DETAILS`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_ACCOUNT_DETAILS = `${namespace}/SET_ACCOUNT_DETAILS`;
export const UPDATE_SUBSCRIPTION = `${namespace}/UPDATE_SUBSCRIPTION`;
export const SET_SUBSCRIPTION_PLANS = `${namespace}/SET_SUBSCRIPTION_PLANS`;
export const SET_SUBSCRIPTION_DRAFT = `${namespace}/SET_SUBSCRIPTION_DRAFT`;
export const DELETE_SUBSCRIPTION_DRAFT = `${namespace}/DELETE_SUBSCRIPTION_DRAFT`;

export const PAGE_STATUSES = {
    READY: 'ready',
    LOADING: 'loading',
    SUBMITTING: 'submitting',
    DELETING_DRAFT: 'deleting_draft'
};

export const SUBSCRIPTION_STATUS_SUSPENDED = 'SUSPENDED';
export const SUBSCRIPTION_TYPE_PREPAID = 'prepaid';
export const SUBSCRIPTION_TYPE_POSTPAID = 'postpaid';
