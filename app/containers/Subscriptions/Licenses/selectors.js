import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'licenses' );

/**
 * Default selector used by License
 */

const makeSelectLicenseDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'licenses_details' ).toJS()
);

const makeSelectPageStatus = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'page_status' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectAccountDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'account_details' ).toJS()
);

const makeSelectSubscriptionOptions = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'subscription_form_options' ).toJS()
);

const makeSelectSubscriptionPlans = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'subscription_plans' ).toJS()
);

const makeSelectSubscriptionDraft = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'subscription_draft' ).toJS()
);

export {
    makeSelectLicenseDetails,
    makeSelectPageStatus,
    makeSelectNotification,
    makeSelectAccountDetails,
    makeSelectSubscriptionOptions,
    makeSelectSubscriptionPlans,
    makeSelectSubscriptionDraft
};
