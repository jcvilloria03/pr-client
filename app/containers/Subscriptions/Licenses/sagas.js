import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import get from 'lodash/get';

import { browserHistory } from 'utils/BrowserHistory';
import { Fetch } from 'utils/request';
import { subscriptionService } from 'utils/SubscriptionService';
import { auth } from 'utils/AuthService';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore, reinitializeAppData } from 'containers/App/sagas';

import {
    INITIALIZE_DATA,
    UPDATE_SUBSCRIPTION,
    PAGE_STATUSES,
    DELETE_SUBSCRIPTION_DRAFT
} from './constants';
import {
    setPageStatus,
    setLicensesDetails,
    setNotification,
    setAccountDetails,
    setSubscriptionPlans,
    setSubscriptionDraft
} from './actions';

/**
 * Initialize data for page
 */
export function* initializeData() {
    yield put( setPageStatus( PAGE_STATUSES.LOADING ) );

    try {
        const subscriptionId = yield call( subscriptionService.getSubscriptionId );

        const [ accountDetails, plans ] = yield [
            call( Fetch, '/account', { method: 'GET' }),
            call( Fetch, '/subscriptions/plans', { method: 'GET' }),
            call( getLicensesDetails, subscriptionId ),
            call( getSubscriptionDraft, subscriptionId )
        ];

        yield [
            put( setAccountDetails( accountDetails ) ),
            put( setSubscriptionPlans( plans.data ) )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Fetch licenses details
 * @param {Integer} id - Subscription ID
 */
export function* getLicensesDetails( id ) {
    try {
        const { data } = yield call( Fetch, `/subscriptions/${id}/stats`, { method: 'GET' });
        yield put( setLicensesDetails( data ) );
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Fetch subscription draft details
 * @param {Integer} id - Subscription ID
 */
export function* getSubscriptionDraft( id ) {
    try {
        const { data } = yield call( Fetch, `/subscriptions/${id}/draft`, { method: 'GET' });
        yield put( setSubscriptionDraft( data ) );
    } catch ( error ) {
        if ( get( error, 'response.status' ) === 404 ) {
            yield put( setSubscriptionDraft({}) );
        } else {
            yield call( notifyError, error );
        }
    }
}

/**
 * Sends request to update subscription
 * @param {Integer} payload.plan_id - Plan ID
 * @param {Integer} payload.convert_to_paid - Flag (0 or 1) to determine if subscription will be converted to paid
 * @param {Object[]} payload.licenses - Array of license updates per product
 */
export function* updateSubscription({ payload }) {
    yield put( setPageStatus( PAGE_STATUSES.SUBMITTING ) );

    try {
        const subscriptionId = yield call( subscriptionService.getSubscriptionId );
        const result = yield call( Fetch, `/subscriptions/${subscriptionId}/plan/update`, {
            method: 'PATCH',
            data: {
                ...payload
            }
        });

        yield put( setPageStatus( PAGE_STATUSES.READY ) );

        yield call( notifyUser, {
            title: 'Success',
            message: 'Subscription successfully updated',
            show: true,
            type: 'success'
        });

        if ( payload.convert_to_paid && result && result.data ) {
            const subscription = result.data;

            const user = Object.assign({}, auth.getUser(), {
                subscription
            });

            auth.setUserData( user );
            yield call( reinitializeAppData );
            browserHistory.push( '/control-panel/subscriptions/summary', true );
        } else {
            yield call( reinitializePage );
        }
    } catch ( error ) {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
        yield call( notifyError, error );
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
        yield call( reinitializeAppData );
    }
}

/**
 * Sends request to delete subscription draft
 */
export function* deleteSubscriptionDraft() {
    yield put( setPageStatus( PAGE_STATUSES.DELETING_DRAFT ) );

    try {
        const subscriptionId = yield call( subscriptionService.getSubscriptionId );
        yield call( Fetch, `/subscriptions/${subscriptionId}/draft`, { method: 'DELETE' });

        yield [
            call( notifyUser, {
                title: 'Success',
                message: 'Subscription draft successfully cancelled',
                show: true,
                type: 'success'
            }),
            call( reinitializePage )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watcher for INITIALIZE_DATA
 *
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for UPDATE_SUBSCRIPTION
 */
export function* watchForUpdateSubscription() {
    const watcher = yield takeEvery( UPDATE_SUBSCRIPTION, updateSubscription );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE_SUBSCRIPTION_DRAFT
 */
export function* watchForDeleteSubscriptionDraft() {
    const watcher = yield takeEvery( DELETE_SUBSCRIPTION_DRAFT, deleteSubscriptionDraft );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForUpdateSubscription,
    watchForDeleteSubscriptionDraft,
    watchForReinitializePage
];
