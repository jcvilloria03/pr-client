import { fromJS } from 'immutable';
import { subscriptionService } from 'utils/SubscriptionService';

import { RESET_STORE } from 'containers/App/constants';

import {
    PAGE_STATUSES,
    SET_PAGE_STATUS,
    SET_LICENSES_DETAILS,
    SET_NOTIFICATION,
    SET_ACCOUNT_DETAILS,
    SET_SUBSCRIPTION_PLANS,
    SET_SUBSCRIPTION_DRAFT
} from './constants';

const initialState = fromJS({
    page_status: PAGE_STATUSES.LOADING,
    licenses_details: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: ''
    },
    account_details: {
        is_trial: true,
        main_plan: null
    },
    subscription_form_options: [],
    subscription_draft: {},
    subscription_plans: {}
});

/**
 * Transforms data for form options
 * @param {Array} data - Subscription options from endpoint
 * @returns {Array}
 */
function prepareSubscriptionOptions( data ) {
    return data.map( ( plan ) => ({
        ...plan,
        value: plan.id,
        label: plan.name
    }) );
}

/**
 * Adds price details to the license details data
 * @param {Object} data - License details
 * @returns {Object}
 */
function formatLicenseDetails( data ) {
    const prices = {};
    const stats = {};
    data.products.forEach( ( product ) => {
        prices[ product.product_code ] = {
            id: product.product_id,
            unit_price: product.unit_price,
            currency: product.currency
        };

        stats[ product.product_code ] = {
            id: product.product_id,
            used: product.licenses_used,
            available: product.licenses_available,
            total: product.licenses_used + product.licenses_available
        };
    });

    return {
        ...data,
        prices,
        stats
    };
}

/**
 * Formats subscription plans array into object notation
 * @param {Array} data - Subscription plans
 * @returns {Object}
 */
function formatSubscriptionPlans( data ) {
    return data.reduce( ( plans, plan ) => ({
        ...plans,
        [ plan.id ]: plan
    }), {});
}

/**
 * Parses account details
 * @param {Object} data - Account details
 * @returns {Object}
 */
function parseAccountDetails( data ) {
    const [subscription] = data.subscriptions;
    subscriptionService.updateSubscription( subscription );

    return {
        ...data,
        is_trial: subscription.is_trial,
        main_plan: subscription.main_product_id
    };
}

/**
 *
 * License reducer
 *
 */
function licenseReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_PAGE_STATUS:
            return state.set( 'page_status', action.payload );
        case SET_LICENSES_DETAILS:
            return state.set( 'licenses_details', fromJS( formatLicenseDetails( action.payload ) ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ACCOUNT_DETAILS:
            return state.set( 'account_details', fromJS( parseAccountDetails( action.payload ) ) );
        case SET_SUBSCRIPTION_PLANS:
            return state
                .set( 'subscription_plans', fromJS( formatSubscriptionPlans( action.payload ) ) )
                .set( 'subscription_form_options', fromJS( prepareSubscriptionOptions( action.payload ) ) );
        case SET_SUBSCRIPTION_DRAFT:
            return state.set( 'subscription_draft', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default licenseReducer;
