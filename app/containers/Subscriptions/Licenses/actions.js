
import { RESET_STORE } from 'containers/App/constants';

import {
    INITIALIZE_DATA,
    SET_PAGE_STATUS,
    SET_LICENSES_DETAILS,
    SET_NOTIFICATION,
    SET_ACCOUNT_DETAILS,
    UPDATE_SUBSCRIPTION,
    SET_SUBSCRIPTION_PLANS,
    SET_SUBSCRIPTION_DRAFT,
    DELETE_SUBSCRIPTION_DRAFT
} from './constants';

/**
 * Initialize page data
 * @returns {Object} action
 */
export function initializeData() {
    return {
        type: INITIALIZE_DATA
    };
}

/**
 * Set status of the page
 * @param {String} payload - Page status
 * @returns {Object} action
 */
export function setPageStatus( payload ) {
    return {
        type: SET_PAGE_STATUS,
        payload
    };
}

/**
 * Sets licenses details from API
 * @param {Object} payload - Licenses details
 * @returns {Object} action
 */
export function setLicensesDetails( payload ) {
    return {
        type: SET_LICENSES_DETAILS,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Sets account details
 * @param {Object} payload - Account details
 * @returns {Object} action
 */
export function setAccountDetails( payload ) {
    return {
        type: SET_ACCOUNT_DETAILS,
        payload
    };
}

/**
 * Sends request to update subscription
 * @param {Integer} payload.plan_id - Plan ID
 * @param {Integer} payload.convert_to_paid - Flag (0 or 1) to determine if subscription will be converted to paid
 * @param {Object[]} payload.licenses - Array of license updates per product
 */
export function updateSubscription( payload ) {
    return {
        type: UPDATE_SUBSCRIPTION,
        payload
    };
}

/**
 * Sets subscription plans
 * @param {Array} payload - Subscription plans
 * @returns {Object} action
 */
export function setSubscriptionPlans( payload ) {
    return {
        type: SET_SUBSCRIPTION_PLANS,
        payload
    };
}

/**
 * Sets subscription draft details
 * @param {Object} payload - Subscription draft details
 * @returns {Object} action
 */
export function setSubscriptionDraft( payload ) {
    return {
        type: SET_SUBSCRIPTION_DRAFT,
        payload
    };
}

/**
 * Sends request to delete subscription draft
 * @returns {Object} action
 */
export function deleteSubscriptionDraft( payload ) {
    return {
        type: DELETE_SUBSCRIPTION_DRAFT,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
