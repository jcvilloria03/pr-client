import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import get from 'lodash/get';
import isEqual from 'lodash/isEqual';

import { isAuthorized } from 'utils/Authorization';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import {
    stripNonNumeric,
    formatCurrency
} from 'utils/functions';

import { H2, H3, H5 } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import Button from 'components/Button';
import Select from 'components/Select';
import Input from 'components/Input';
import A from 'components/A';
import Modal from 'components/Modal';
import Loader from 'components/Loader';
import Confirm from 'components/SalConfirm';

import SubHeader from 'containers/SubHeader';
import { makeSelectIsExpired } from 'containers/App/selectors';

import { BASE_PATH_NAME } from '../../../constants';

import {
    initializeData,
    updateSubscription,
    deleteSubscriptionDraft
} from './actions';
import {
    PageWrapper,
    SectionWrapper,
    LoadingStyles,
    SummaryWrapper,
    LicenseWrapper,
    QuantityWrapper,
    TotalLicenseWrapper,
    PriceWrapper,
    SubscriptionSelectWrapper,
    ModalBody,
    ConfirmBodyWrapperStyle
} from './styles';
import {
    makeSelectPageStatus,
    makeSelectNotification,
    makeSelectLicenseDetails,
    makeSelectAccountDetails,
    makeSelectSubscriptionOptions,
    makeSelectSubscriptionPlans,
    makeSelectSubscriptionDraft
} from './selectors';

import {
    PAGE_STATUSES,
    SUBSCRIPTION_STATUS_SUSPENDED,
    SUBSCRIPTION_TYPE_PREPAID
} from './constants';

/**
 * Licenses Component
 */
export class Licenses extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        pageStatus: React.PropTypes.string,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        initializeData: React.PropTypes.func,
        licenses: React.PropTypes.object,
        accountDetails: React.PropTypes.object,
        isExpired: React.PropTypes.bool,
        subscriptionOptions: React.PropTypes.array,
        updateSubscription: React.PropTypes.func,
        plans: React.PropTypes.object,
        draft: React.PropTypes.object,
        deleteSubscriptionDraft: React.PropTypes.func
    };

    static defaultProps = {
        accountDetails: {
            is_trial: false,
            subscriptions: []
        }
    };

    constructor( props ) {
        super( props );

        this.state = {
            salpay_view_permission: false,
            subscription_plan: {},
            licenses: {},
            show_cancel_confirm: false
        };
    }

    componentWillMount() {
        isAuthorized([
            'view.account',
            'view.salpay_integration'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.account' ];

            if ( authorized ) {
                this.setState({ salpay_view_permission: authorization[ 'view.salpay_integration' ] });
            } else {
                window.location.href = '/control-panel/companies';
            }
        });
    }

    componentDidMount() {
        this.props.initializeData();

        this.loadSubscriptionDetails();
    }

    componentWillReceiveProps( nextProps ) {
        !isEqual( nextProps.licenses.stats, this.props.licenses.stats )
            && nextProps.licenses.stats
            && this.setState({ licenses: nextProps.licenses.stats });

        nextProps.accountDetails.main_plan !== this.props.accountDetails.main_plan
            && this.setState({
                subscription_plan: {
                    value: nextProps.accountDetails.main_plan
                }
            });

        nextProps.pageStatus !== this.props.pageStatus
            && nextProps.pageStatus === PAGE_STATUSES.READY
            && this.updateSubscriptionSummary
            && this.updateSubscriptionSummary.state.modal
            && this.updateSubscriptionSummary.toggle();
    }

    /**
     * Computes total licenses and amount for the selected plan
     *
     * @param {Object} plan - Selected subscription plan
     * @returns {Object}
     */
    getTotalLicenseAndAmount( plan ) {
        if ( !plan ) return {};

        let licenses = 0;
        let amount = 0;

        plan.products.forEach( ( product ) => {
            const quantity = parseInt( get( this.state.licenses, [ product.code, 'total' ], 0 ), 10 );
            licenses += quantity;
            amount += ( quantity * get( this.props.licenses, [ 'prices', product.code, 'unit_price' ], 0 ) );
        });

        return {
            licenses,
            amount
        };
    }

    /**
     * Computes total selected licenses
     *
     * @returns {Object}
     */
    getSelectedPlanTotalLicenses( plans ) {
        if ( !plans ) return {};

        let licenses = 0;

        plans.forEach( ( product ) => {
            const quantity = parseInt( get( this.state.licenses, [ product.code, 'total' ], 0 ), 10 );
            licenses += quantity;
        });

        return licenses;
    }

    /**
     * Computes total licenses and amount for the subscription update draft
     *
     * @returns {Object}
     */
    getTotalLicenseAndAmountForDraft() {
        let licenses = 0;
        let amount = 0;

        this.props.draft.subscription_licenses.forEach( ( license ) => {
            licenses += license.units;
            amount += license.units * license.product.price;
        });

        return {
            licenses,
            amount
        };
    }

    /**
     * Gets difference in quantity for draft details
     *
     * @param {String} type - Type of difference ('add' or 'deduct')
     * @param {Object} license - Draft subscription license
     * @returns {Number}
     */
    getQuantityDifferenceForDraft( type, license ) {
        const difference = get( this.props.licenses, [ 'stats', license.product.code, 'total' ], 0 ) - license.units;

        return ( type === 'add' && difference < 0 || type === 'deduct' && difference > 0 )
            ? Math.abs( difference )
            : 0;
    }

    /**
     * Gets unit price string for given product
     * @param {String} productCode - Product code
     * @returns {String} Product price and unit
     */
    getProductUnitPrice( productCode ) {
        const product = get( this.props.licenses, [ 'prices', productCode ], {});
        return `${formatCurrency( product.unit_price )} ${product.currency}`;
    }

    getPreviousTotalLicenses() {
        const used = get( this.props.licenses, 'total_used', 0 );
        const remaining = get( this.props.licenses, 'total_remaining', 0 );

        return used + remaining;
    }

    showPrepaidLicenseChangeWarning() {
        const subscription = get( this.props.accountDetails, 'subscriptions.0', {});
        const subscriptionType = get( subscription, 'subscription_settings.type', null );
        const isTrial = get( subscription, 'is_trial', false );

        if ( subscriptionType !== SUBSCRIPTION_TYPE_PREPAID || isTrial ) {
            return false;
        }

        const currentSubscription = get( this.props.plans, this.props.accountDetails.main_plan );
        const selectedPlanProducts = get( this.props.plans, [ this.state.subscription_plan.value, 'products' ], []);

        const totalCurrentLicenses = this.getPreviousTotalLicenses();
        const totalSelectedLicenses = this.getSelectedPlanTotalLicenses( selectedPlanProducts );

        const currentPlanId = get( currentSubscription, 'id', null );
        const currentPlanPrice = get( this.props.plans, [ currentPlanId, 'price' ], 0 );

        const newPlanId = get( this.state.subscription_plan, 'value', null );
        const newPlanPrice = get( this.props.plans, [ newPlanId, 'price' ], 0 );

        if ( newPlanPrice > currentPlanPrice ) {
            return true;
        }

        if ( totalSelectedLicenses > totalCurrentLicenses ) {
            return true;
        }

        return false;
    }

    loadSubscriptionDetails() {
        this.setState({
            subscription_plan: {
                value: this.props.accountDetails.main_plan
            },

            licenses: this.props.licenses.stats
        });
    }

    /**
     * onClick handler for add/subtract buttons
     */
    updateTotalLicenses = ( productCode, type, isMain = false ) => () => {
        const change = type === 'add' ? 1 : -1;

        this.setState( ( state ) => {
            const total = get( state.licenses, [ productCode, 'total' ], 0 ) + change;
            return {
                licenses: {
                    ...state.licenses,
                    [ productCode ]: {
                        ...state.licenses[ productCode ],
                        total: total > 0 ? total : isMain ? 1 : 0
                    }
                }
            };
        });
    }

    /**
     * Dispatches request to save updated subscription details
     */
    updateSubscription = () => {
        const selectedPlanProducts = get( this.props.plans, [ this.state.subscription_plan.value, 'products' ], []);

        const licenses = selectedPlanProducts.reduce( ( summary, product ) =>
            summary.concat({
                product_id: get( this.state.licenses, [ product.code, 'id' ], 0 ),
                units: parseInt( get( this.state.licenses, [ product.code, 'total' ], 0 ), 10 )
            }), []);

        const account = this.props.accountDetails;
        const subscription = account && account.subscriptions && account.subscriptions[ 0 ];
        const suspendedTrialAccount = subscription && subscription.is_trial && subscription.status === 'SUSPENDED';

        this.props.updateSubscription({
            plan_id: this.state.subscription_plan.value,
            convert_to_paid: suspendedTrialAccount ? 1 : 0,
            licenses
        });
    }

    /**
     * onClick handler for Confirm button
     */
    showChangesSummary = () => {
        if ( this.subscriptionSelect._checkRequire( this.state.subscription_plan.value ) ) {
            this.updateSubscriptionSummary.toggle();
        }
    }

    /**
     * Dispatches request to delete subscription draft
     */
    deleteSubscriptionDraft = () => {
        this.props.deleteSubscriptionDraft();
    }

    renderUpdateSubscriptionSummaryBody() {
        if ( this.props.pageStatus === PAGE_STATUSES.LOADING ) {
            return (
                <ModalBody>
                    <Loader />
                </ModalBody>
            );
        }
        const currentSubscription = get( this.props.plans, this.props.accountDetails.main_plan );
        const selectedPlanProducts = get( this.props.plans, [ this.state.subscription_plan.value, 'products' ], []);
        const totalLicenses = this.getSelectedPlanTotalLicenses( selectedPlanProducts );

        return (
            <ModalBody>
                <div className="subscription current">
                    <div>Current Subscription:</div>
                    <div>
                        <H5>{ get( currentSubscription, 'name' ) }</H5>
                    </div>
                </div>

                { this.props.accountDetails.main_plan !== this.state.subscription_plan.value && (
                    <div className="subscription">
                        <div>Change Subscription To:</div>
                        <div>
                            <strong>
                                <H5>{ this.state.subscription_plan.label }</H5>
                            </strong>
                        </div>
                    </div>
                ) }

                <SummaryWrapper type="modal">
                    <div>
                        <LicenseWrapper className="header">Licenses</LicenseWrapper>
                        <QuantityWrapper className="header">Total Licenses</QuantityWrapper>
                    </div>

                    { selectedPlanProducts.map( ( product ) => (
                        <div id={ `summary-${product.code}` } key={ `summary-${product.code}` }>
                            <LicenseWrapper>{ product.name }</LicenseWrapper>
                            <QuantityWrapper>{ get( this.state.licenses, [ product.code, 'total' ], 0 ) }</QuantityWrapper>
                        </div>
                    ) ) }
                </SummaryWrapper>

                <br />
                <span>Subscription Licenses: <strong>{ totalLicenses }</strong></span>

                { this.showPrepaidLicenseChangeWarning() && (
                    <span className="warning">
                        <br />
                        You are about to receive an invoice for the added licenses to be paid within the day. Confirm your action.
                    </span>
                )}
            </ModalBody>
        );
    }

    renderNextBillingUpdate() {
        const total = this.getTotalLicenseAndAmountForDraft();

        return this.props.pageStatus !== PAGE_STATUSES.LOADING && (
            <SectionWrapper className="next-billing">
                <section className="content">
                    <header className="heading">
                        <H3>Next Billing Period Subscription Update</H3>
                        <div className="subscription">
                            <div>Subscription Plan:</div>
                            <H5>{ get( this.props.plans, [ this.props.draft.plan_id, 'name' ], '' ) }</H5>
                        </div>
                    </header>

                    <SummaryWrapper>
                        <div>
                            <LicenseWrapper className="header">Licenses</LicenseWrapper>
                            <QuantityWrapper className="header">Add</QuantityWrapper>
                            <QuantityWrapper className="header">Deduct</QuantityWrapper>
                            <QuantityWrapper className="header">Total Licenses</QuantityWrapper>
                            <PriceWrapper className="header">Unit Price</PriceWrapper>
                            <PriceWrapper className="header">Date Modified</PriceWrapper>
                            <PriceWrapper className="header">Modified By</PriceWrapper>
                        </div>

                        { this.props.draft.subscription_licenses.map( ( license ) => (
                            <div id={ `draft-${license.id}` } key={ `draft-${license.id}` }>
                                <LicenseWrapper>{ license.product.name }</LicenseWrapper>
                                <QuantityWrapper>{ this.getQuantityDifferenceForDraft( 'add', license ) }</QuantityWrapper>
                                <QuantityWrapper>{ this.getQuantityDifferenceForDraft( 'deduct', license ) }</QuantityWrapper>
                                <QuantityWrapper>{ license.units }</QuantityWrapper>
                                <PriceWrapper>{ `${formatCurrency( license.product.price )} ${license.product.currency}` }</PriceWrapper>
                                <PriceWrapper>{ /* TODO: not yet available */ }</PriceWrapper>
                                <PriceWrapper>{ /* TODO: not yet available */ }</PriceWrapper>
                            </div>
                        ) ) }

                        <div className="total">
                            <span>Total Licenses: <strong>{ total.licenses }</strong></span>
                            <span>Total Amount: <strong>{ formatCurrency( total.amount )} { get( this.props.licenses, 'products[0].currency' ) }</strong></span>
                        </div>
                    </SummaryWrapper>

                    <Button
                        id="cancel-button"
                        label="Cancel"
                        type="danger"
                        size="large"
                        onClick={ () => this.setState({ show_cancel_confirm: true }) }
                        disabled={
                            this.props.pageStatus === PAGE_STATUSES.SUBMITTING
                                || this.props.pageStatus === PAGE_STATUSES.DELETING_DRAFT
                        }
                    />
                </section>
            </SectionWrapper>
        );
    }

    /**
     * Component Render Method
     */
    render() {
        const {
            draft,
            plans,
            products,
            licenses,
            isExpired,
            pageStatus,
            accountDetails,
            subscriptionOptions
        } = this.props;

        const {
            subscription_plan
        } = this.state;

        const renderContent = pageStatus !== PAGE_STATUSES.LOADING
            && Object.keys( licenses ).length > 0
            && Object.keys( plans ).length > 0;

        const subscription = get( accountDetails, 'subscriptions.0', {});
        const selectedPlan = get( plans, subscription_plan.value );
        const total = this.getTotalLicenseAndAmount( selectedPlan );

        const isControlsDisabled = !subscription.is_trial && subscription.status === SUBSCRIPTION_STATUS_SUSPENDED;
        const isSubmitting = pageStatus === PAGE_STATUSES.SUBMITTING;

        return (
            <PageWrapper>
                <Helmet
                    title="Licenses"
                    meta={ [
                        { name: 'Licenses', content: 'Licenses Details' }
                    ] }
                />
                <SubHeader
                    items={ subscriptionService.getTabs() }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="Update Subscription Summary"
                    size="lg"
                    body={ this.renderUpdateSubscriptionSummaryBody() }
                    maxWidth="898px"
                    showClose={ false }
                    buttons={ [
                        {
                            id: 'modal-cancel-button',
                            type: 'neutral',
                            label: 'Cancel',
                            disabled: isSubmitting,
                            onClick: () => this.updateSubscriptionSummary.toggle()
                        },
                        {
                            id: 'modal-submit-button',
                            type: 'action',
                            label: isSubmitting ? 'Submitting...' : 'Submit',
                            disabled: isSubmitting,
                            onClick: this.updateSubscription
                        }
                    ] }
                    ref={ ( ref ) => { this.updateSubscriptionSummary = ref; } }
                />
                <Confirm
                    onConfirm={ this.deleteSubscriptionDraft }
                    onClose={ () => this.setState({ show_cancel_confirm: false }) }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                You are about to cancel your subscription update. You can&apos;t undo this action.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ this.state.show_cancel_confirm }
                />
                <SectionWrapper>
                    <Sidebar
                        items={ getControlPanelSidebarLinks({
                            isExpired,
                            accountViewPermission: true,
                            salpayViewPermission: this.state.salpay_view_permission,
                            isSubscribedToPayroll: products && subscriptionService.isSubscribedToPayroll( products )
                        }) }
                    />
                    { pageStatus === PAGE_STATUSES.LOADING && (
                        <div className="loader">
                            <LoadingStyles>
                                <H2>Loading Licenses.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                    ) }

                    { renderContent && (
                        <section className="content">
                            <header className="heading">
                                <H3>Subscription Summary</H3>
                                <p>
                                    Updates on your Account Subscription will affect your users assigned licenses.
                                    <br /><br />
                                    Please read the&nbsp;

                                    <A href={ `${BASE_PATH_NAME}/guides/subscriptions/update` } target="_blank">
                                        Subscription Update Guide
                                    </A>.
                                </p>

                                <SubscriptionSelectWrapper>
                                    <H5>Subscriptions Plan: </H5>
                                    <Select
                                        id="subscription-select"
                                        required
                                        autoSize
                                        placeholder="Select your subscription"
                                        value={ this.state.subscription_plan.value }
                                        data={ subscriptionOptions }
                                        ref={ ( select ) => { this.subscriptionSelect = select; } }
                                        onChange={ ( value ) => this.setState({ subscription_plan: value }) }
                                        disabled={ isControlsDisabled || isSubmitting }
                                    />
                                </SubscriptionSelectWrapper>
                            </header>

                            <SummaryWrapper>
                                <div>
                                    <LicenseWrapper className="header">Licenses</LicenseWrapper>
                                    <QuantityWrapper className="header">Used</QuantityWrapper>
                                    <QuantityWrapper className="header">Available</QuantityWrapper>
                                    <TotalLicenseWrapper className="header">Total Licenses</TotalLicenseWrapper>
                                    <PriceWrapper className="header">Unit Price</PriceWrapper>
                                </div>

                                { selectedPlan && selectedPlan.products.map( ( product ) => (
                                    <div id={ `license-${product.code}` } key={ `license-${product.code}` }>
                                        <LicenseWrapper>{ product.name }</LicenseWrapper>
                                        <QuantityWrapper>{ get( licenses, [ 'stats', product.code, 'used' ], 0 ) }</QuantityWrapper>
                                        <QuantityWrapper>{ get( licenses, [ 'stats', product.code, 'available' ], 0 ) }</QuantityWrapper>

                                        <TotalLicenseWrapper>
                                            <button
                                                disabled={ isControlsDisabled }
                                                onClick={ ( subscription.main_product_id === product.id || this.state.subscription_plan.code === product.code )
                                                    ? this.updateTotalLicenses( product.code, 'subtract', true ) : this.updateTotalLicenses( product.code, 'subtract' ) }
                                            >
                                                <i className="fa fa-minus" />
                                            </button>

                                            <Input
                                                id={ `total-license-input-${product.id}` }
                                                className="total-license-input"
                                                type="number"
                                                value={ get( this.state.licenses, [ product.code, 'total' ], 0 ) }
                                                onChange={ ( value ) => {
                                                    const formatted = parseInt( stripNonNumeric( value ), 10 ) || 0;
                                                    this[ `totalLicense${product.id}` ].setState({ value: formatted }, () => {
                                                        this.setState( ( state ) => ({
                                                            licenses: Object.assign(
                                                                {},
                                                                state.licenses,
                                                                {
                                                                    [ product.code ]: {
                                                                        ...state.licenses[ product.code ],
                                                                        total: formatted
                                                                    }
                                                                }
                                                            )
                                                        }) );
                                                    });
                                                } }
                                                ref={ ( ref ) => { this[ `totalLicense${product.id}` ] = ref; } }
                                                disabled={ isControlsDisabled }
                                            />

                                            <button
                                                disabled={ isControlsDisabled }
                                                onClick={ this.updateTotalLicenses( product.code, 'add' ) }
                                            >
                                                <i className="fa fa-plus" />
                                            </button>
                                        </TotalLicenseWrapper>

                                        <PriceWrapper>{ this.getProductUnitPrice( product.code ) }</PriceWrapper>
                                    </div>
                                ) ) }

                                <div className="total">
                                    <span>Total Licenses: <strong>{ total.licenses || 0 }</strong></span>
                                    <span>Total Amount: <strong>{ formatCurrency( total.amount || 0 )} { get( subscriptionOptions, '[0].currency' ) }</strong></span>
                                </div>
                            </SummaryWrapper>

                            <Button
                                id="confirm-button"
                                label="Confirm"
                                type="action"
                                size="large"
                                onClick={ this.showChangesSummary }
                                disabled={ isControlsDisabled || isSubmitting || pageStatus === PAGE_STATUSES.DELETING_DRAFT }
                            />
                        </section>
                    ) }
                </SectionWrapper>

                {
                    Object.keys( draft ).length > 0 && this.renderNextBillingUpdate()
                }

            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    licenses: makeSelectLicenseDetails(),
    pageStatus: makeSelectPageStatus(),
    notification: makeSelectNotification(),
    accountDetails: makeSelectAccountDetails(),
    isExpired: makeSelectIsExpired(),
    subscriptionOptions: makeSelectSubscriptionOptions(),
    plans: makeSelectSubscriptionPlans(),
    draft: makeSelectSubscriptionDraft()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        {
            initializeData,
            updateSubscription,
            deleteSubscriptionDraft
        },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Licenses );
