import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.main`
    .next-billing .content {
        margin-top: 0;
    }
`;

export const SectionWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 0 20px 0 111px;

    .content {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 40px 0;
        width: 100%;
        max-width: 1100px;
        border-radius: 5px;
        border: solid 1px #adadad;
        background-color: #fff;
        padding: 20px 0;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 30px auto;

            p {
                text-align: center;
            }

            h3, h4 {
                font-weight: 600;
            }

            a {
                text-decoration: underline !important;
            }

            .subscription {
                display: flex;

                & > div {
                    width: 170px;
                    text-align: start;
                }

                h5 {
                    margin-bottom: 0;
                }
            }
        }
    }
`;

export const SummaryWrapper = styled.div`
    width: 100%;
    text-align: center;

    & > div {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: ${({ type }) => ( type === 'modal' ? 'space-around' : 'space-between' )};
        margin: 10px 50px;
    }

    & > div:first-of-type {
        margin-bottom: 20px;
    }

    .total {
        justify-content: space-between;
        margin-top: 38px;

        strong {
            margin-left: 20px;
        }
    }

    .header {
        font-weight: 600;
    }
`;

export const LicenseWrapper = styled.div`
    width: 250px;
    text-align: start;
`;

export const QuantityWrapper = styled.div`
    width: 150px;
    text-align: center;
`;

export const TotalLicenseWrapper = styled.div`
    width: 200px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;

    .total-license-input {
        margin: 0 10px;

        input {
            text-align: center;
        }
    }

    .fa {
        margin: 0;
    }

    button {
        padding: 0;
        border: none;
        background: none;

        &:focus {
            outline: none;
        }

        &:hover {
            cursor: pointer;

            .fa {
                transform: scale( 1.1 );
            }
        }
    }
`;

export const PriceWrapper = styled.div`
    width: 150px;
    text-align: center;
`;

export const SubscriptionSelectWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    white-space: nowrap;

    h5 {
        margin-right: 20px;
    }

    & > div {
        min-width: 300px;
    }
`;

export const ModalBody = styled.section`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding-bottom: 30px;

    span > strong {
        margin-left: 20px;
    }

    .subscription {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-bottom: 20px;

        & > div {
            width: 250px;
            text-align: start;
        }

        h5 {
            margin-bottom: 0;
        }
    }

    .current {
        h5 {
            font-weight: 400;
        }
    }

    .warning {
        font-weight: 600;
    }

    .animation {
        padding: 100px 0;
        font-size: 30px;

        .anim3 {
            &:before {
                width: 24px;
                height: 24px;
                border-width: 4px;
                border-color: #444;
            }

            &:after {
                background-color: #666;
                width: 24px;
                height: 6px;
            }
        }
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .message {
        display: flex;
        align-self: center;
    }
`;
