
import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_STATUS,
    SET_REQUEST_DETAILS,
    SUBMIT_ACCOUNT_DELETION_REQUEST,
    CONFIRM_ACCOUNT_DELETION_REQUEST,
    CANCEL_ACCOUNT_DELETION_REQUEST,
    SET_OTP_DETAILS,
    INITIALIZE_DATA,
    SET_NOTIFICATION,
    VERIFY_OTP,
    RESEND_OTP,
    SET_OTP_RESEND_COUNTDOWN,
    SET_OTP_UNBLOCK_COUNTDOWN,
    NOTIFY_USER
} from './constants';

/**
 * Set deletion status of the page
 * @param {Boolean} payload - page status
 * @returns {Object} action
 */
export function setStatus( payload ) {
    return {
        type: SET_STATUS,
        payload
    };
}

/**
 * Sets request details
 * @returns {Object} action
 */
export function initializeData() {
    return {
        type: INITIALIZE_DATA
    };
}

/**
 * Set deletion request details
 * @param {Boolean} payload - Request details
 * @returns {Object} action
 */
export function setDeletionRequestDetails( payload ) {
    return {
        type: SET_REQUEST_DETAILS,
        payload
    };
}

/**
 * Set loading status of the page
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Submit deletion request
 * @param {Object} payload - Deletion details
 * @returns {Object} action
 */
export function submitDeletionRequest( payload ) {
    return {
        type: SUBMIT_ACCOUNT_DELETION_REQUEST,
        payload
    };
}

/**
 * Confirm deletion request
 * @param {Object} payload - Deletion details
 * @returns {Object} action
 */
export function confirmDeletionRequest( payload ) {
    return {
        type: CONFIRM_ACCOUNT_DELETION_REQUEST,
        payload
    };
}

/**
 * Cancel deletion request
 * @param {Object} payload - Deletion details
 * @returns {Object} action
 */
export function cancelDeletionRequest( payload ) {
    return {
        type: CANCEL_ACCOUNT_DELETION_REQUEST,
        payload
    };
}

/**
 * Verify otp
 * @param {Object} payload - otp details
 * @returns {Object} action
 */
export function verifyOtp( payload ) {
    return {
        type: VERIFY_OTP,
        payload
    };
}

/**
 * Resend otp
 * @returns {Object} action
 */
export function resendOtp( payload ) {
    return {
        type: RESEND_OTP,
        payload
    };
}

/**
 * Sets otp details
 * @param {Object} payload - OTP details
 * @returns {Object} action
 */
export function setOtpDetails( payload ) {
    return {
        type: SET_OTP_DETAILS,
        payload
    };
}

/**
 * Set OTP resend countdown
 * @param {Boolean} payload - countdown
 * @returns {Object} action
 */
export function setOtpResendCountdown( payload ) {
    return {
        type: SET_OTP_RESEND_COUNTDOWN,
        payload
    };
}

/**
 * Set OTP unblock countdown
 * @param {Boolean} payload - countdown
 * @returns {Object} action
 */
export function setOtpUnblockCountdown( payload ) {
    return {
        type: SET_OTP_UNBLOCK_COUNTDOWN,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Notify User
 */
export function notifyUser( payload ) {
    return {
        type: NOTIFY_USER,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
