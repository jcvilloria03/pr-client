/*
 *
 * Account Deletion constants
 *
 */
const namespace = 'app/Subscriptions';

export const SET_STATUS = `${namespace}/SET_STATUS`;
export const SET_LOADING = `${namespace}/SET_LOADING`;
export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;

export const GET_REQUEST_DETAILS = `${namespace}/GET_REQUEST_DETAILS`;
export const SET_REQUEST_DETAILS = `${namespace}/SET_REQUEST_DETAILS`;

export const SUBMIT_ACCOUNT_DELETION_REQUEST = `${namespace}/SUBMIT_ACCOUNT_DELETION_REQUEST`;
export const CONFIRM_ACCOUNT_DELETION_REQUEST = `${namespace}/CONFIRM_ACCOUNT_DELETION_REQUEST`;
export const CANCEL_ACCOUNT_DELETION_REQUEST = `${namespace}/CANCEL_ACCOUNT_DELETION_REQUEST`;

export const VERIFY_OTP = `${namespace}/VERIFY_OTP`;
export const RESEND_OTP = `${namespace}/RESEND_OTP`;
export const SET_OTP_DETAILS = `${namespace}/SET_OTP_DETAILS`;

export const SET_OTP_RESEND_COUNTDOWN = `${namespace}/SET_OTP_RESEND_COUNTDOWN`;
export const SET_OTP_UNBLOCK_COUNTDOWN = `${namespace}/SET_OTP_UNBLOCK_COUNTDOWN`;

export const NOTIFY_USER = `${namespace}/NOTIFY_USER`;

export const STATUS_INITIAL = `${namespace}/INITIAL_DELETE`;
export const STATUS_OTP = `${namespace}/STATUS_OTP`;
export const STATUS_CONFIRM_DELETE_ME = `${namespace}/STATUS_CONFIRM_DELETE_ME`;
export const STATUS_COUNTDOWN = `${namespace}/STATUS_COUNTDOWN`;

export const DEFAULT_RESEND_DURATION_SECONDS = 180;
export const DEFAULT_UNBLOCK_DURATION_SECONDS = 300;

export const OTP_MAX_LIMIT_REACHED_ERROR = "Unfortunately, you've got no verification attempts left for now. Please try again after 5 minutes.";
