import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_STATUS,
    SET_LOADING,
    SET_OTP_DETAILS,
    SET_REQUEST_DETAILS,
    SET_OTP_RESEND_COUNTDOWN,
    SET_OTP_UNBLOCK_COUNTDOWN,
    SET_NOTIFICATION
} from './constants';

const initialState = fromJS({
    status: null,
    loading: false,
    otp: {},
    request: {},
    otpResendCountdown: 180,
    otpUnblockCountdown: 300,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    }
});

/**
 *
 * Subscription reducer
 *
 */
function accountDeletionReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_STATUS:
            return state.set( 'status', action.payload );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_OTP_DETAILS:
            return state.set( 'otp', fromJS( action.payload ) );
        case SET_REQUEST_DETAILS:
            return state.set( 'request', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_OTP_RESEND_COUNTDOWN:
            return state.set( 'otpResendCountdown', action.payload );
        case SET_OTP_UNBLOCK_COUNTDOWN:
            return state.set( 'otpUnblockCountdown', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default accountDeletionReducer;
