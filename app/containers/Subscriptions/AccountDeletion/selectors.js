import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'accountDeletion' );

/**
 * Default selector used by Subscription
 */

const makeSelectStatus = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'status' )
);

const makeSelectLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectOtpDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'otp' ).toJS()
);

const makeSelectRequestDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'request' ).toJS()
);

const makeSelectOtpResendCountdown = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'otpResendCountdown' )
);

const makeSelectOtpUnblockCountdown = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'otpUnblockCountdown' )
);

export {
    makeSelectStatus,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectOtpDetails,
    makeSelectRequestDetails,
    makeSelectOtpResendCountdown,
    makeSelectOtpUnblockCountdown
};
