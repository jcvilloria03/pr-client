import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 200px;
`;

export const PageWrapper = styled.div`
    padding-left: 91px;
    display: flex;
    flex-direction: column;
    align-items: center;

    .loader {
        padding: 140px 0;
    }

    .content {
        margin: 40px 0;
        padding: 0 20px;
        width: 80%;
        max-width: 1140px;
        display: flex;
        flex-direction: column;
        align-items: center;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 30px auto;

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
                max-width: 800px;
            }

            a {
                text-decoration: underline !important;
            }
        }

        .expired {
            width: 100%;
            text-align: center;
        }

        .header {
            font-weight: 600;
        }

        .request-button-area {
            display: flex;
            justify-content: center;
            padding: 20px 0;
            margin-top: 40px;
            width: 50%;

            .request-delete-btn {
                border-radius: 5px;
                font-weight: 600;
            }
        }
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    justify-content: center;
    min-height: 100%;
    text-align: center;
`;

export const SpinnerWrapper = styled.span`
    padding: 0 20px;
`;

export const FormWrapper = styled.div`
    > div {
        & > h4 {
            text-align: left;
        }

        & > button {
            padding: 12px 40px;
            margin-top: 2rem;
        }

        .steps {
            display: flex;
            align-items: stretch;
            justify-content: space-between;
            margin: 1rem 0;

            > .step {
                width: 49%;
            }
        }

        .template,
        .upload {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
            width: 100%;
            height: 100%;
            padding: 2rem;
            text-align: center;
            border: 2px dashed #ccc;
            border-radius: 12px;

            .sl-c-btn--wide {
                min-width: 12rem;
            }
        }

        .upload > div:first-of-type > div > div {
            margin: 0 auto;
        }

        .errors {
            .rt-thead .rt-tr {
                background: #F21108;
                color: #fff;
            }
            .rt-tbody .rt-tr {
                background: rgba(242, 17, 8, .1)
            }
            .ReactTable.-striped .rt-tr.-odd {
                background: rgba(242, 17, 8, .2)
            }

            .ReactTable .rt-th,
            .ReactTable .rt-td {
                flex: 1 0 0px;
                white-space: initial;
                text-overflow: ellipsis;
                padding: 7px 20px;
                overflow: hidden;
                transition: 0.3s ease;
                transition-property: width, min-width, padding, opacity;
            }

            .react-bs-container-body tr {
                background: rgba(249, 210, 210, 0.8);

                &:hover {
                    background: rgba(249, 210, 210, 1);
                }
            }
        }
    }
`;

export const SectionWrapper = styled.div`
    background-color: #fff;
    border: 1px solid #e5e5e5;
    padding: 25px;

    .section-heading {
        display: flex;
        align-items: center;
        justify-content: space-between;
        border-bottom: 1px solid #e5e5e5;

        h5 {
            padding-top: 5px;
        }

        button {
            margin-bottom: 20px;
        }

        a {
            border-bottom: none;

            button {
                background-color: #4aba4a;
                border-radius: 50%;
                width: 30px;
                height: 30px;
                border: none;
                outline: none;
                cursor: pointer;

                &:disabled {
                    cursor: not-allowed;
                    opacity: .65;
                }

                i {
                    color: #fff;
                }
            }
        }
    }

    .section-body {
        padding-top: 25px;
        align-items: center;

        h4 {
            font-weight: 700;
            padding-left: 35px;
        }

        > div {
            &.section-column {
                > div {
                    margin-bottom: 25px;
                }
            }

            > div {
                .section-row {
                    display: flex;
                    flex-wrap: wrap;
                    padding-left: 35px;
                    margin-top: 20px;

                    > div {
                        margin-bottom: 25px;

                        .radio-group {
                            > div:first-child{
                                padding-left: 0 !important;
                            }
                        }
                    }

                    &.edit {
                        > div:not(:last-child) {
                            margin-bottom: 0;
                        }
                    }
                }
            }
        }

        .content {
            .heading {
                display: flex;
                align-items: center;
                flex-direction: column;
                margin: 0 auto 50px auto;

                h3 {
                    font-weight: 600;
                }

                p {
                    text-align: center;
                    max-width: 800px;
                }
            }

            .title {
                display: flex;
                align-items: center;
                margin-bottom: 20px;

                h5 {
                    margin: 0;
                    margin-right: 20px;
                    font-weight: 600;
                    font-size: 20px;
                }
            }
        }

        .section-actions {
            text-align: center;
            margin-top: 20px;
        }

        .resend-btn-area {
            text-align: center;
            margin-top: 20px;

            .resend-count {
                font-weight: 600;
                color: #00A5E5;
            }

            .unblock-count {
                font-weight: 700;
                color: #c51a07;
            }

            .resend-link-text {
                font-weight: 600;
                color: #00A5E5;
            }

            .disabled {
                font-weight: 500;
                color: #808080;
            }
        }
    }
`;

export const ValueWrapper = styled.div`
    padding-top: 15px;
    padding-left: 12.5px;
    padding-bottom: 30px;
`;

export const CountdownWrapper = styled.div`
    position: fixed;
    z-index: 10000;
    right: 0;
    min-height: 30px;
    margin: auto;
    display: flex;
    align-items: center;
    justify-content: space-between;
    transition: top 500ms cubic-bezier(0, 0, 0.30, 1);
    font-weight: 600;
    text-transform: initial;
    font-size: 16px;
    box-shadow: 0px -3px 20px 0px rgba(0,0,0,0.75);
    color: #ffffff;
    background: #ef7575;

    .delete-countdown-progress {
        position: fixed;
        height: 30px;
        background-color: red;
        z-index: 1;
        left: 91px;
    }

    .delete-countdown-text {
        position: relative;
        z-index: 2;
        padding: 0 10px;
    }
`;
export const StyledContent = styled.div`
    margin: 0;
    work-break: break-word
`;
