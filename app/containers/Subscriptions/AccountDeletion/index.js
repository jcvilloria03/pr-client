/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';

import { isAuthorized } from 'utils/Authorization';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { auth } from 'utils/AuthService';

import Button from 'components/Button';
import SalConfirm from 'components/SalConfirm';
import SalCheckbox from 'components/SalCheckbox/index';
import Input from 'components/Input';
import { H3 } from 'components/Typography';
import A from 'components/A';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';

import SubHeader from 'containers/SubHeader';

import * as actions from './actions';
import {
    STATUS_INITIAL,
    STATUS_OTP,
    STATUS_CONFIRM_DELETE_ME,
    STATUS_COUNTDOWN
} from './constants';
import {
    PageWrapper,
    ConfirmBodyWrapperStyle,
    FormWrapper,
    SectionWrapper
} from './styles';
import {
    makeSelectStatus,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectOtpDetails,
    makeSelectRequestDetails,
    makeSelectOtpResendCountdown,
    makeSelectOtpUnblockCountdown
} from './selectors';

/**
 * AccountDeletion Component
 */
export class AccountDeletion extends React.PureComponent {
    static propTypes = {
        status: React.PropTypes.string,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        otp: React.PropTypes.object,
        request: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        submitDeletionRequest: React.PropTypes.func,
        verifyOtp: React.PropTypes.func,
        resendOtp: React.PropTypes.func,
        otpResendCountdown: React.PropTypes.number,
        otpUnblockCountdown: React.PropTypes.number,
        confirmDeletionRequest: React.PropTypes.func,
        cancelDeletionRequest: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        this.state = {
            viewPermission: false,
            confirmModal: false,
            showDeletionReasonForm: false,
            reasons: [
                'Cost',
                'Features Missing',
                'Signed Up with Another Provider',
                'No Time to Setup',
                'Others'
            ],
            selectedReasons: [],
            requestReason: '',
            requestReasonMaxChars: 250,
            emailCode: '',
            deleteText: '',
            resendTimer: 0,
            resendCountdown: null,
            unblockTimer: 0,
            unblockCountdown: null,
            disableInitialButton: true
        };

        this.updateResendCountdown = this.updateResendCountdown.bind( this );
        this.updateUnblockCountdown = this.updateUnblockCountdown.bind( this );
    }

    componentWillMount() {
        isAuthorized([
            'view.account',
            'view.subscriptions'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.account' ];

            if ( authorized ) {
                this.setState({ viewPermission: authorization[ 'view.subscriptions' ] });
            } else if ( subscriptionService.isExpired() && !['Owner'].includes( auth.getUser().role ) ) {
                localStorage.clear();
            } else {
                window.location.href = '/control-panel/companies';
            }
        });

        this.props.initializeData();
    }

    componentWillReceiveProps( nextProps ) {
        if ( !this.props.request && nextProps.request || nextProps.status === STATUS_CONFIRM_DELETE_ME ) {
            this.reset();
        }

        if ( Object.keys( this.props.request ).length > 0 ) {
            this.setState({ disableInitialButton: false });
        }

        if ( ( nextProps.otpResendCountdown !== this.props.otpResendCountdown ) ) {
            clearInterval( this.state.resendTimer );
            this.setState({ resendTimer: null }, () => {
                this.setState({
                    resendCountdown: nextProps.otpResendCountdown,
                    resendTimer: setInterval( this.updateResendCountdown, 1000 )
                });
            });
        }

        if ( nextProps.otpUnblockCountdown !== this.props.otpUnblockCountdown ) {
            clearInterval( this.state.unblockTimer );
            this.setState({ unblockTimer: null }, () => {
                this.setState({
                    unblockCountdown: nextProps.otpUnblockCountdown,
                    unblockTimer: setInterval( this.updateUnblockCountdown, 1000 )
                });
            });
        }
    }

    updateResendCountdown() {
        const { resendCountdown } = this.state;
        if ( resendCountdown > 0 ) {
            this.setState({ resendCountdown: resendCountdown - 1 });
        } else {
            clearInterval( this.state.resendTimer );
            this.setState({ resendTimer: null });
        }
    }

    updateUnblockCountdown() {
        const { unblockCountdown } = this.state;
        if ( unblockCountdown > 0 ) {
            this.setState({ unblockCountdown: unblockCountdown - 1 });
        } else {
            clearInterval( this.state.unblockTimer );
            this.setState({ unblockTimer: null });
        }
    }

    reset() {
        this.setState({ showDeletionReasonForm: false }, () => {
            this.setState({
                selectedReasons: [],
                requestReason: '',
                confirmModal: false,
                emailCode: ''
            });
        });
    }

    submitAccountDeletionRequest() {
        this.props.submitDeletionRequest({
            selected_reasons: this.state.selectedReasons,
            request_reasons: this.state.requestReason
        });
    }

    resendOtp() {
        this.props.request && this.props.resendOtp({ id: this.props.request.id });
    }

    cancelAccountDeletionRequest() {
        this.props.request && this.props.cancelDeletionRequest({ id: this.props.request.id });
        this.reset();
    }

    verifyOtp() {
        this.state.emailCode &&
        this.props.verifyOtp({
            id: this.props.request.id,
            otp: this.state.emailCode
        });
    }

    confirmAccountDeletionRequest() {
        this.state.deleteText &&
        this.props.confirmDeletionRequest({
            id: this.props.request.id,
            deleteText: this.state.deleteText
        });
    }

    formatCountDown( countdown ) {
        const durationInSeconds = moment.duration( countdown, 'seconds' );
        return `${durationInSeconds.minutes()}:${durationInSeconds.seconds().toString().padStart( 2, '0' )}`;
    }

    isDisableSubmitReasonForm() {
        const hasOtherReason = this.state.selectedReasons.includes( 'Others' );
        return this.state.selectedReasons.length === 0
            || this.state.requestReason.length > this.state.requestReasonMaxChars
            || this.props.loading
            || ( hasOtherReason && !this.state.requestReason )
            || this.props.status === STATUS_OTP;
    }

    /**
     * Render Deletion Reason Form
     */
    renderReasonForm() {
        return (
            <FormWrapper>
                <SectionWrapper>
                    <div className="section-heading">
                        <p style={ { fontWeight: '500' } }>Why do you want to cancel and delete your account?</p>

                    </div>
                    <div className="section-body">
                        <div className="section-row row">
                            {
                                this.state.reasons.map( ( row, key ) => (
                                    <div className="col-xs-12" key={ `row_${key}` } >
                                        <div className="checkcontaion">
                                            <SalCheckbox
                                                id={ `reason_${key}` }
                                                name="reasonTitle"
                                                ref={ ( ref ) => { this.reasonTitle = ref; } }
                                                checked={ this.state.selectedReasons.indexOf( row ) > -1 }
                                                onChange={ ( value ) => {
                                                    if ( value ) {
                                                        this.setState({
                                                            selectedReasons: [ ...this.state.selectedReasons, row ]
                                                        });
                                                    } else {
                                                        this.setState({
                                                            selectedReasons: this.state.selectedReasons.filter(
                                                                ( reason ) => reason !== row
                                                            )
                                                        });
                                                    }
                                                } }
                                            />&nbsp;
                                            { row }
                                        </div>
                                    </div>
                                ) )
                            }
                            <div className="col-xs-12" style={ { marginTop: '20px' } }>
                                <Input
                                    id="name"
                                    type="textarea"
                                    value={ this.state.requestReason }
                                    onChange={ ( value ) => this.setState({ requestReason: value }) }
                                    max={ this.state.requestReasonMaxChars }
                                    placeholder="Type your reason here"
                                />
                            </div>
                            <div className="col-xs-12 section-actions" style={ { marginTop: '20px' } }>
                                <Button
                                    id="cancel-btn"
                                    label="Cancel"
                                    type="grey"
                                    onClick={ () => { this.reset(); } }
                                >
                                </Button>
                                <Button
                                    id="submit-btn"
                                    label={
                                        this.props.loading
                                        ? 'Loading..'
                                        : 'Submit'
                                    }
                                    type="action"
                                    disabled={ this.isDisableSubmitReasonForm() }
                                    onClick={ () => { this.submitAccountDeletionRequest(); } }
                                >
                                </Button>
                            </div>
                        </div>
                    </div>
                </SectionWrapper>
            </FormWrapper>
        );
    }

    /**
     * Render OTP Form
     */
    renderOtpForm() {
        const showResend = ( this.state.resendCountdown <= 0 && this.state.unblockCountdown <= 0 );
        const isBlocked = ( this.state.unblockCountdown > 0 && this.props.otp.remaining_attempt <= 0 );
        return (
            <FormWrapper>
                <SectionWrapper>
                    <div className="section-heading">
                        <p style={ { fontWeight: '500' } }>Enter email verification code to proceed in Account Deletion</p>
                    </div>
                    <div className="section-body">
                        <div className="section-row row">
                            <div className="col-xs-12">
                                <Input
                                    id="otp"
                                    value={ this.state.emailCode }
                                    onChange={ ( value ) => this.setState({ emailCode: value }) }
                                    placeholder="Email Verification Code"
                                />
                            </div>
                            <div className="col-xs-12 section-actions">
                                <Button
                                    id="cancel-btn"
                                    label="Cancel"
                                    type="grey"
                                    disabled={ this.state.unblockCountdown > 0 }
                                    onClick={ () => { this.cancelAccountDeletionRequest(); } }
                                >
                                </Button>
                                <Button
                                    id="submit-btn"
                                    label={
                                        this.props.loading
                                        ? 'Loading..'
                                        : 'Submit'
                                    }
                                    type="action"
                                    disabled={
                                        !this.state.emailCode || ( this.state.emailCode && this.state.emailCode.length < 6 )
                                        || this.props.loading || this.state.unblockCountdown > 0
                                    }
                                    onClick={ () => { this.verifyOtp(); } }
                                >
                                </Button>
                            </div>
                            <div className="col-xs-12 resend-btn-area">
                                <p>
                                    { showResend && (
                                        <A onClick={ () => this.resendOtp() } className="resend-link-text">
                                            Resend Verification Code
                                        </A>
                                    )}
                                    { this.state.resendCountdown > 0 && !isBlocked && (
                                        <span className="resend-count">
                                            <del className="disabled">Resend Verification Code</del> &nbsp;
                                            { this.formatCountDown( this.state.resendCountdown ) }
                                        </span>
                                    )}
                                    { isBlocked && (
                                        <span className="unblock-count">
                                            <del className="disabled">Resend Verification Code</del> &nbsp;
                                            { this.formatCountDown( this.state.unblockCountdown ) }
                                        </span>
                                    )}
                                </p>
                            </div>
                        </div>
                    </div>
                </SectionWrapper>
            </FormWrapper>
        );
    }

    /**
     * Render Confirm Delete Text Form
     */
    renderConfirmDeleteTextForm() {
        return (
            <FormWrapper>
                <SectionWrapper>
                    <div className="section-heading">
                        <p style={ { fontWeight: '500' } }>Type "<b>Delete Me</b>" to proceed the request to permanently delete your account</p>
                    </div>
                    <div className="section-body">
                        <div className="section-row row">
                            <div className="col-xs-12">
                                <Input
                                    id="deleteMe"
                                    value={ this.state.deleteText }
                                    onChange={ ( value ) => this.setState({ deleteText: value }) }
                                    placeholder=""
                                />
                            </div>
                            <div className="col-xs-12 section-actions">
                                <Button
                                    id="cancel-btn"
                                    label="Cancel"
                                    type="grey"
                                    onClick={ () => { this.cancelAccountDeletionRequest(); } }
                                >
                                </Button>
                                <Button
                                    id="submit-btn"
                                    label={
                                        this.props.loading
                                        ? 'Loading..'
                                        : 'Submit'
                                    }
                                    type="action"
                                    disabled={
                                        !this.state.deleteText || this.props.loading
                                    }
                                    onClick={ () => { this.confirmAccountDeletionRequest(); } }
                                >
                                </Button>
                            </div>
                        </div>
                    </div>
                </SectionWrapper>
            </FormWrapper>
        );
    }

    /**
     * Component Render Method
     */
    render() {
        const { viewPermission } = this.state;

        return (
            <main>
                <Helmet
                    title="Account Deletion"
                    meta={ [
                        { name: 'Subscriptions', content: 'Account Deletion' }
                    ] }
                />
                <SubHeader
                    items={ subscriptionService.getTabs() }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 4000 }
                    type={ this.props.notification.type }
                />
                <PageWrapper>
                    <Sidebar
                        items={ getControlPanelSidebarLinks({
                            isExpired: subscriptionService.isExpired(),
                            accountViewPermission: true,
                            viewPermission,
                            isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products )
                        }) }
                    />
                    <section className="content">
                        <header className="heading">
                            <H3>Account Deletion</H3>
                            <p style={ { fontWeight: '600' } }>
                                Warning: once your account has been deleted, it can never be restored again.
                            </p>
                        </header>
                        {
                            this.props.status === STATUS_INITIAL && !this.state.showDeletionReasonForm && (
                                <div className="request-button-area">
                                    <Button
                                        id="delete-btn"
                                        className="request-delete-btn"
                                        label="Delete My Account"
                                        type="danger"
                                        disabled={ this.state.disableInitialButton }
                                        block
                                        onClick={ () => {
                                            this.setState({ confirmModal: false }, () => {
                                                this.setState({ confirmModal: true });
                                            });
                                        } }
                                    >
                                    </Button>
                                </div>
                            )
                        }

                        {
                            this.props.status === STATUS_INITIAL && this.state.showDeletionReasonForm && this.renderReasonForm()
                        }

                        {
                            this.props.status === STATUS_OTP && this.renderOtpForm()
                        }

                        {
                            this.props.status === STATUS_CONFIRM_DELETE_ME && this.renderConfirmDeleteTextForm()
                        }

                        {
                            this.props.status === STATUS_COUNTDOWN && (
                                <div className="request-button-area">
                                    <Button
                                        id="cancel-delete-btn"
                                        className="request-delete-btn"
                                        label="Cancel Account Deletion"
                                        type="neutral"
                                        disabled={ this.props.loading }
                                        block
                                        onClick={ () => {
                                            this.cancelAccountDeletionRequest();
                                        } }
                                    >
                                    </Button>
                                </div>
                            )
                        }
                    </section>
                </PageWrapper>

                <SalConfirm
                    onConfirm={ () => {
                        this.setState({ confirmModal: false }, () => {
                            this.setState({ showDeletionReasonForm: true });
                        });
                    } }
                    onClose={ () => { this.reset(); } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Are you sure you want to permanently
                                <br />
                                delete your account?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Account Request"
                    visible={ this.state.confirmModal }
                    confirmText="Yes"
                    cancelText="No"
                    backdrop={ false }
                    keyboard={ false }
                />
            </main>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    status: makeSelectStatus(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    otp: makeSelectOtpDetails(),
    request: makeSelectRequestDetails(),
    otpResendCountdown: makeSelectOtpResendCountdown(),
    otpUnblockCountdown: makeSelectOtpUnblockCountdown()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AccountDeletion );

