import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore, setAccountDeleteRequestData } from 'containers/App/sagas';

import { getFormStatus, getRemainingDuration } from './utils';

import {
    STATUS_INITIAL,
    INITIALIZE_DATA,
    SUBMIT_ACCOUNT_DELETION_REQUEST,
    CONFIRM_ACCOUNT_DELETION_REQUEST,
    CANCEL_ACCOUNT_DELETION_REQUEST,
    NOTIFY_USER,
    VERIFY_OTP,
    RESEND_OTP
} from './constants';
import {
    setStatus,
    setLoading,
    setOtpDetails,
    setNotification,
    setOtpResendCountdown,
    setDeletionRequestDetails,
    setOtpUnblockCountdown
} from './actions';

/**
 * Get deletion request
 */
export function* initializeData() {
    yield put( setLoading( true ) );
    try {
        const response = yield call( Fetch, '/subscriptions/account_deletion', { method: 'GET' });

        if ( response ) {
            const resendDuration = getRemainingDuration( response.otp_expires_at );

            const status = getFormStatus( response.status );

            yield [
                put( setDeletionRequestDetails({
                    id: response.id,
                    email: response.email,
                    delete_at: response.delete_at,
                    last_otp_at: response.last_otp_at,
                    status: response.status,
                    selected_reasons: response.selected_reasons,
                    request_reasons: response.request_reasons
                }) ),
                put( setOtpDetails({
                    expires_at: response.otp_expires_at,
                    last_otp_at: response.last_otp_at,
                    remaining_attempt: response.remaining_attempt
                }) ),
                put( setOtpResendCountdown( resendDuration ) ),
                put( setStatus( status ) )
            ];

            if ( response.remaining_attempt <= 0 ) {
                const unblockDuration = getRemainingDuration( response.last_otp_at, true );
                yield put( setOtpUnblockCountdown( unblockDuration ) );
            }
        } else {
            yield put( setStatus( STATUS_INITIAL ) );
        }
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Submit deletion request
 */
export function* submitDeletionRequest({ payload }) {
    yield put( setLoading( true ) );

    try {
        const response = yield call( Fetch, '/subscriptions/account_deletion', {
            method: 'POST',
            data: payload
        });

        const resendDuration = getRemainingDuration( response.otp_expires_at );
        const status = getFormStatus( 'pending' );

        yield [
            put( setOtpDetails({
                expires_at: response.otp_expires_at,
                last_otp_at: null,
                remaining_attempt: 3
            }) ),
            put( setDeletionRequestDetails({
                id: response.id,
                email: response.email,
                delete_at: null,
                status: 'pending',
                selected_reasons: payload.selected_reasons,
                request_reasons: payload.request_reasons
            }) ),
            put( setStatus( status ) ),
            put( setOtpResendCountdown( resendDuration ) ),
            put( setLoading( false ) ),
            call( notifyUser, {
                title: 'Success',
                message: 'Verification code has been sent to your email.',
                show: true,
                type: 'success'
            })
        ];
    } catch ( error ) {
        yield put( setLoading( false ) );
        yield call( notifyError, error );
    }
}

/**
 * Submit OTP verification
 */
export function* verifyOtp({ payload }) {
    yield put( setLoading( true ) );

    try {
        const { id, otp } = payload;
        const response = yield call( Fetch, `/subscriptions/account_deletion/${id}/confirm_otp`, {
            method: 'POST',
            data: {
                otp
            }
        });

        const status = getFormStatus( response.status );

        yield [
            put( setOtpDetails({
                expires_at: response.otp_expires_at,
                last_otp_at: response.last_otp_at,
                remaining_attempt: 3
            }) ),
            put( setStatus( status ) ),
            put( setLoading( false ) ),
            call( notifyUser, {
                title: 'Success',
                message: '',
                show: true,
                type: 'success'
            })
        ];
    } catch ( error ) {
        let title = error.response ? error.response.statusText : 'Error';
        const otpData = error.response && error.response.data && error.response.data.otp ? error.response.data.otp : null;
        if ( otpData ) {
            const resendDuration = getRemainingDuration( otpData.otp_expires_at );
            const unblockDuration = getRemainingDuration( otpData.last_otp_at, true );

            yield [
                put( setOtpResendCountdown( resendDuration ) ),
                put( setOtpDetails({
                    expires_at: otpData.otp_expires_at,
                    last_otp_at: otpData.last_otp_at,
                    remaining_attempt: otpData.remaining_attempt
                }) )
            ];

            if ( otpData.remaining_attempt <= 0 ) {
                title = 'Attempts Limit Reached';
                yield put( setOtpUnblockCountdown( unblockDuration ) );
            }
        }

        yield call( notifyUser, {
            show: true,
            title,
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Resend OTP verification
 */
export function* resendOtp({ payload }) {
    yield put( setLoading( true ) );

    try {
        const response = yield call( Fetch, `/subscriptions/account_deletion/${payload.id}/resend_otp`, { method: 'POST' });

        const resendDuration = getRemainingDuration( response.otp_expires_at );

        yield [
            put( setOtpDetails({
                id: response.id,
                expires_at: response.otp_expires_at,
                last_otp_at: response.last_otp_at,
                remaining_attempt: response.remaining_attempt
            }) ),
            put( setOtpResendCountdown( resendDuration ) ),
            put( setLoading( false ) ),
            call( notifyUser, {
                title: 'Success',
                message: 'Verification code has been resent to your email.',
                show: true,
                type: 'success'
            })
        ];
    } catch ( error ) {
        yield put( setLoading( false ) );
        yield call( notifyError, error );
    }
}

/**
 * Confirm deletion request
 */
export function* confirmDeletionRequest({ payload }) {
    yield put( setLoading( true ) );

    try {
        const { id, deleteText } = payload;
        const response = yield call( Fetch, `/subscriptions/account_deletion/${id}/confirm`, {
            method: 'PATCH',
            data: {
                confirm_text: deleteText
            }
        });

        const status = getFormStatus( response.status );

        yield [
            put( setDeletionRequestDetails({
                id: response.id,
                email: response.email,
                delete_at: response.delete_at,
                last_otp_at: response.last_otp_at,
                status: response.status,
                selected_reasons: payload.selected_reasons,
                request_reasons: payload.request_reasons
            }) ),
            put( setStatus( status ) ),
            put( setLoading( false ) ),
            call( notifyUser, {
                title: 'Success',
                message: 'Account deletion request has been successfully queued!',
                show: true,
                type: 'success'
            })
        ];

        yield call( setAccountDeleteRequestData, {
            id: response.id,
            requestedAt: response.last_otp_at,
            deleteAt: response.delete_at
        });
    } catch ( error ) {
        yield put( setLoading( false ) );
        yield call( notifyError, error );
    }
}

/**
 * Cancel deletion request
 */
export function* cancelDeletionRequest({ payload }) {
    yield put( setLoading( true ) );

    try {
        yield call( Fetch, `/subscriptions/account_deletion/${payload.id}/cancel`, { method: 'DELETE' });

        yield call( setAccountDeleteRequestData, null );
        auth.setAccountDeleteRequestData( null );
        yield [
            put( setOtpDetails({}) ),
            put( setDeletionRequestDetails({}) ),
            call( notifyUser, {
                title: 'Success',
                message: 'Your account deletion request has been successfully cancelled!',
                show: true,
                type: 'success'
            })
        ];
        yield put( setStatus( STATUS_INITIAL ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const data = !payload.payload ? payload : payload.payload;

    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: data.type || 'error'
    };

    yield put( setNotification( data ) );

    yield call( delay, 4000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for INITIALIZE_DATA
 */
export function* watchForinitializeData() {
    const watcher = yield takeLatest( INITIALIZE_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SUBMIT_ACCOUNT_DELETION_REQUEST
 */
export function* watchForSubmitDeletionRequest() {
    const watcher = yield takeLatest( SUBMIT_ACCOUNT_DELETION_REQUEST, submitDeletionRequest );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for CONFIRM_ACCOUNT_DELETION_REQUEST
 */
export function* watchForConfirmDeletionRequest() {
    const watcher = yield takeLatest( CONFIRM_ACCOUNT_DELETION_REQUEST, confirmDeletionRequest );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for CANCEL_ACCOUNT_DELETION_REQUEST
 */
export function* watchForCancelDeletionRequest() {
    const watcher = yield takeLatest( CANCEL_ACCOUNT_DELETION_REQUEST, cancelDeletionRequest );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for RESEND_OTP
 */
export function* watchForResendOtp() {
    const watcher = yield takeLatest( RESEND_OTP, resendOtp );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for VERIFY_OTP
 */
export function* watchForVerifyOtp() {
    const watcher = yield takeLatest( VERIFY_OTP, verifyOtp );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFY_USER
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFY_USER, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForReinitializePage,
    watchForinitializeData,
    watchForSubmitDeletionRequest,
    watchForConfirmDeletionRequest,
    watchForCancelDeletionRequest,
    watchForVerifyOtp,
    watchForResendOtp,
    watchForNotifyUser
];
