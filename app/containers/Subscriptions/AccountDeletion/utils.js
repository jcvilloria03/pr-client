
import moment from 'moment';

import {
    STATUS_INITIAL,
    STATUS_OTP,
    STATUS_CONFIRM_DELETE_ME,
    STATUS_COUNTDOWN,
    DEFAULT_UNBLOCK_DURATION_SECONDS
} from './constants';

export const getFormStatus = ( value ) => {
    let status = value;

    switch ( value ) {
        case 'pending':
            status = STATUS_OTP;
            break;
        case 'verified':
            status = STATUS_CONFIRM_DELETE_ME;
            break;
        case 'confirmed':
            status = STATUS_COUNTDOWN;
            break;
        default:
            status = STATUS_INITIAL;
            break;
    }

    return status;
};

export const getRemainingDuration = ( dateTimeBasis, isUnblockCountdown = false ) => {
    if ( !dateTimeBasis ) {
        return 0;
    }
    const now = moment();
    const basisAt = moment( dateTimeBasis );

    if ( isUnblockCountdown ) {
        basisAt.add( DEFAULT_UNBLOCK_DURATION_SECONDS, 'seconds' );
    }

    const remainingDuration = basisAt.diff( now, 'seconds' );

    return Math.max( remainingDuration, 0 );
};
