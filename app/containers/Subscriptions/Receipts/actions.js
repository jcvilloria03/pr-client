import {
    SET_NOTIFICATION,
    INITIALIZE_DATA,
    SET_PAGE_STATUS,
    SET_ACCOUNT_DETAILS,
    SET_RECEIPTS_LIST
} from './constants';

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Sets account details
 * @param {Object} payload - Account details
 * @returns {Object} action
 */
export function setAccountDetails( payload ) {
    return {
        type: SET_ACCOUNT_DETAILS,
        payload
    };
}

/**
 * Set status of the page
 * @param {String} payload - Page status
 * @returns {Object} action
 */
export function setPageStatus( payload ) {
    return {
        type: SET_PAGE_STATUS,
        payload
    };
}

/**
 * Set list of receipts
 * @param {String} payload - Page status
 * @returns {Object} action
 */
export function setReceiptsList( payload ) {
    return {
        type: SET_RECEIPTS_LIST,
        payload
    };
}

/**
 * Initialize page data
 * @returns {Object} action
 */
export function initializeData() {
    return {
        type: INITIALIZE_DATA
    };
}
