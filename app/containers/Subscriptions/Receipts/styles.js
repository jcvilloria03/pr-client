import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.main`
    .next-billing .content {
        margin-top: 0;
    }
`;

export const SectionWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 0 20px 0 111px;

    .content {
        margin-top: 40px;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 600;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;

            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 600;
                font-size: 22px;
            }
        }
    }
`;

export const TableWrapper = styled.div`
    padding: 3rem 0;
`;
