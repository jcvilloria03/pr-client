import { fromJS } from 'immutable';
import {
    SET_NOTIFICATION,
    SET_ACCOUNT_DETAILS,
    SET_PAGE_STATUS,
    SET_RECEIPTS_LIST,
    PAGE_STATUSES
} from './constants';

const initialState = fromJS({
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    account_details: {
        is_trial: true,
        main_plan: null
    },
    page_status: PAGE_STATUSES.LOADING,
    receipt_details: {}
});

/**
 * Parses account details
 * @param {Object} data - Account details
 * @returns {Object}
 */
function parseAccountDetails( data ) {
    const [subscription] = data.subscriptions;

    return {
        ...data,
        is_trial: subscription.is_trial,
        main_plan: subscription.main_product_id
    };
}

/**
 * Filters Receipts
 * @param {Object} data - Receipts
 * @returns {Object}
 */
function filterNullReceipts( data ) {
    return {
        data: data.data.filter( ( receipt ) => receipt.payment !== null )
    };
}

/**
 *
 * SubscriptionsReceipts reducer
 *
 */
function receiptsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_PAGE_STATUS:
            return state.set( 'page_status', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ACCOUNT_DETAILS:
            return state.set( 'account_details', fromJS( parseAccountDetails( action.payload ) ) );
        case SET_RECEIPTS_LIST:
            return state.set( 'receipt_details', fromJS( filterNullReceipts( action.payload ) ) );
        default:
            return state;
    }
}

export default receiptsReducer;
