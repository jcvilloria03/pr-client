/*
 *
 * License constants
 *
 */
const namespace = 'app/Receipts';

export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_ACCOUNT_DETAILS = `${namespace}/SET_ACCOUNT_DETAILS`;
export const SET_PAGE_STATUS = `${namespace}/SET_PAGE_STATUS`;
export const SET_RECEIPTS_LIST = `${namespace}/SET_RECEIPTS_LIST`;

export const PAGE_STATUSES = {
    READY: 'ready',
    LOADING: 'loading'
};
