import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import { isAuthorized } from 'utils/Authorization';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { formatDate, formatCurrency } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';

import {
    DATE_FORMATS
} from 'utils/constants';

import Button from 'components/Button';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import { H2, H3 } from 'components/Typography';
import Table from 'components/Table';
import SubHeader from 'containers/SubHeader';

import {
    makeSelectReceipts,
    makeSelectNotification,
    makeSelectAccountDetails,
    makeSelectPageStatus
} from './selectors';

import {
    initializeData
} from './actions';

import {
    PageWrapper,
    SectionWrapper,
    LoadingStyles,
    TableWrapper
} from './styles';

import { PAGE_STATUSES } from './constants';

/**
 *
 * SubscriptionsReceipts
 *
 */
export class Receipts extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        products: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.node,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        accountDetails: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        page_status: React.PropTypes.string,
        receipt_details: React.PropTypes.object
    }

    constructor( props ) {
        super( props );

        this.state = {
            salpay_view_permission: false
        };
    }

    componentWillMount() {
        isAuthorized([
            'view.account',
            'view.salpay_integration'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.account' ];

            if ( authorized ) {
                this.setState({ salpay_view_permission: authorization[ 'view.salpay_integration' ] });
            } else {
                window.location.href = '/control-panel/companies';
            }
        });
    }

    componentDidMount() {
        this.props.initializeData();
    }

    openReceiptDetails( data ) {
        browserHistory.pushWithParam(
            `/control-panel/subscriptions/receipts/${data.id}`,
            true,
            data
        );
    }

    render() {
        const tableColumns = [
            {
                header: 'Receipt #',
                id: 'receipt_number',
                minWidth: 150,
                accessor: 'id'
            },
            {
                header: 'Payment Date',
                id: 'payment_date',
                accessor: ( row ) => (
                    row.payment && row.payment.created_at ? row.payment.created_at : ''
                ),
                render: ( row ) => (
                    formatDate( row.value, DATE_FORMATS.DISPLAY )
                ),
                minWidth: 200
            },
            {
                header: 'Description',
                id: 'description',
                minWidth: 300,
                accessor: ( row ) => (
                    row.invoice.invoice_number ? row.invoice.invoice_number : ''
                ),
                render: ( row ) => (
                    `Payment receipt for Invoice ${row.value}`
                )
            },
            {
                header: 'Payment Method',
                id: 'payment_method',
                minWidth: 180,
                accessor: ( row ) => (
                    row.payment && row.payment.payment_method ? row.payment.payment_method : ''
                )
            },
            {
                header: 'Payment Platform',
                id: 'payment_platform',
                minWidth: 200,
                accessor: ( row ) => (
                    row.payment && row.payment.source ? row.payment.source : ''
                )
            },
            {
                header: 'Amount',
                id: 'amount',
                minWidth: 150,
                accessor: ( row ) => (
                    row.invoice.total ? row.invoice.total : ''
                ),
                render: ( row ) => (
                    formatCurrency( row.value )
                )
            },
            {
                header: 'Currency',
                id: 'currency',
                minWidth: 150,
                accessor: ( row ) => (
                    row.invoice.currency ? row.invoice.currency : 'USD'
                )
            },
            {
                header: 'Action',
                sortable: false,
                filterable: false,
                minWidth: 200,
                render: ( row ) => (
                    <Button
                        label="View Receipt"
                        type="action"
                        onClick={ () => this.openReceiptDetails( row.row ) }
                        alt
                    />
                )
            }
        ];

        return (
            <PageWrapper>
                <Helmet
                    title="Receipts"
                    meta={ [
                        { name: 'Receipts', content: 'Receipts' }
                    ] }
                />

                <SubHeader
                    items={ subscriptionService.getTabs() }
                />

                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                <Container>
                    <SectionWrapper>
                        <Sidebar
                            items={ getControlPanelSidebarLinks({
                                accountViewPermission: true,
                                salpayViewPermission: this.state.salpay_view_permission,
                                isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products )
                            }) }
                        />

                        { this.props.page_status === PAGE_STATUSES.LOADING && (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading Receipts.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        ) }

                        { this.props.page_status === PAGE_STATUSES.READY && (
                            <TableWrapper>
                                <div className="content">
                                    <div className="heading">
                                        <H3>Receipts History</H3>
                                    </div>
                                    <Table
                                        data={ this.props.receipt_details.data
                                                ? this.props.receipt_details.data
                                                : []
                                        }
                                        columns={ tableColumns }
                                        pagination
                                        loading={ ( this.props.page_status === PAGE_STATUSES.LOADING ) }
                                    />
                                </div>
                            </TableWrapper>
                        ) }
                    </SectionWrapper>
                </Container>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    accountDetails: makeSelectAccountDetails(),
    page_status: makeSelectPageStatus(),
    receipt_details: makeSelectReceipts()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        {
            initializeData
        },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Receipts );
