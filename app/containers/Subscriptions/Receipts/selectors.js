import { createSelector } from 'reselect';

/**
 * Direct selector to the subscriptionsReceipts state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'receipts' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by SubscriptionsReceipts
 */

const makeSelectReceipts = () => createSelector(
  selectPageDomain(),
  ( substate ) => substate.get( 'receipt_details' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectAccountDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'account_details' ).toJS()
);

const makeSelectPageStatus = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'page_status' )
);

export default makeSelectReceipts;

export {
  makeSelectReceipts,
  makeSelectNotification,
  makeSelectAccountDetails,
  makeSelectPageStatus
};
