import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh;

    .break-word {
        word-break: break-all;
    }

    > .heading {
        padding: 10px 20px;
        position: fixed;
        background: #f0f4f6;
        z-index: 1;
        width: 100%;
    }

    > .content {
        display: flex;
        position: relative;
        height: 100%;
        margin-top: 44px;

        @media (max-width: '960px') {
            > .list-container {
                display: none;
            }
        }
    }
`;

export const SectionWrapper = styled.div`
    margin: 40px auto;
    max-width: 1000px;

    .buttons {
        display: flex;
        align-items: center;
        justify-content: flex-end;

        button {
            width: 120px;
        }
    }

    .receipt-container {
        margin-top: 20px;
        border: solid 1px #adadad;
        border-radius: 5px;
        padding: 60px;

        .logo {
            width: 100%;
        }

        .address {
            margin: 10px 0;
        }

        .details p {
            margin-bottom: 0;
        }

        .billing-details {
            margin-top: 60px;
        }

        .receipt-total {
            margin-top: 20px;
            text-align: right;
        }

        .vat-info {
            margin-top: 10px;
            text-align: right;
        }
    }
`;
