import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'receipt_view' );

/**
 * Default selector used by ReceiptDetails
 */

const makeSelectLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectAccountDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'account_details' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectAccountDetails
};
