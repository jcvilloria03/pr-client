import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';

import { isAuthorized } from 'utils/Authorization';
import { formatDate, formatCurrency } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import {
    DATE_FORMATS
} from 'utils/constants';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';

import A from 'components/A';
import Button from 'components/Button';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import Table from 'components/Table';
import { H2, H3 } from 'components/Typography';

import SubHeader from 'containers/SubHeader';
import logo from 'assets/logo-salarium-blue.png';

import { initializeData } from './actions';
import {
    PageWrapper,
    SectionWrapper,
    LoadingStyles
} from './styles';
import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectAccountDetails
} from './selectors';

/**
 *
 * ReceiptDetails
 *
 */
export class ReceiptDetails extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.node,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        location: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        accountDetails: React.PropTypes.object
    }

    constructor( props ) {
        super( props );

        this.state = { salpay_view_permission: false };
    }

    componentWillMount() {
        isAuthorized([
            'view.account',
            'view.salpay_integration'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.account' ];

            if ( authorized ) {
                this.setState({
                    salpay_view_permission: authorization[ 'view.salpay_integration' ]
                });
            } else {
                window.location.href = '/control-panel/companies';
            }
        });
    }

    componentDidMount() {
        if ( !this.props.location.state ) {
            window.location.href = '/control-panel/subscriptions/receipts';
        }

        this.props.initializeData();
    }

    downloadReceipt = () => {
        window.open( this.props.location.state.receipt_pdf );
    }

    /**
     * Billing details table
     */
    renderBillingDetailsTable() {
        const invoiceItems = get( this.props.location.state, 'invoice.invoice_items', []);
        const currency = get( this.props.location.state, 'invoice.currency', '' );
        const tableColumns = [
            {
                header: 'Description',
                id: 'description',
                sortable: false,
                width: 200,
                accessor: 'name'
            },
            {
                header: 'Quantity',
                id: 'quantity',
                sortable: false,
                accessor: 'units'
            },
            {
                header: 'Unit Price',
                id: 'unit_price',
                sortable: false,
                render: ({ row }) => `${formatCurrency( row.amount )} ${currency}`
            },
            {
                header: 'Amount',
                id: 'amount',
                sortable: false,
                render: ({ row }) => `${formatCurrency( row.subtotal )} ${currency}`
            }
        ];

        return (
            <Table
                data={ invoiceItems }
                columns={ tableColumns }
                loading={ this.props.loading }
            />
        );
    }

    renderVatInfo( vatInfo, netTotal, currency) {
        if (isEmpty(vatInfo) && !netTotal) {
            return (null)
        }

        const name = get( vatInfo, 'name' );
        const rate = get( vatInfo, 'rate' );
        const amount = get( vatInfo, 'amount' );

        return (
            <div className='vat-info'>
                <div><strong>Total Unit Price: </strong> { formatCurrency( netTotal ) } { currency }</div>
                <div><strong>{ name } ({ rate }%): </strong> { formatCurrency( amount ) } { currency }</div>
            </div>
        )
    }

    render() {
        const receiptId = get( this.props.location.state, 'id', '' );
        const billingInfo = get( this.props.location.state, 'invoice.billing_info', {});
        const paymentReceivedOn = get( this.props.location.state, 'payment.created_at', '' );
        const paymentMethod = get( this.props.location.state, 'payment.source', '' );
        const total = get( this.props.location.state, 'invoice.total', '' );
        const currency = get( this.props.location.state, 'invoice.currency', '' );
        const vatInfo = get( this.props.location.state, 'invoice.vat_info', [] );
        const netTotal = get( this.props.location.state, 'invoice.net_total', null )

        return (
            <div>
                <Helmet
                    title="Receipts"
                    meta={ [
                        { name: 'description', content: 'Receipt Details' }
                    ] }
                />

                <SubHeader
                    items={ subscriptionService.getTabs() }
                />

                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        accountViewPermission: true,
                        salpayViewPermission: this.state.salpay_view_permission,
                        isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products )
                    }) }
                />

                <PageWrapper>
                    <div className="heading">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => { e.preventDefault(); browserHistory.push( '/control-panel/subscriptions/receipts', true ); } }
                            >
                                &#8592; Back to Receipts History
                            </A>
                        </Container>
                    </div>

                    <div className="content">
                        <Container>
                            { this.props.loading && (
                                <div className="loader">
                                    <LoadingStyles>
                                        <H2>Loading Receipt Details</H2>
                                        <br />
                                        <H3>Please wait...</H3>
                                    </LoadingStyles>
                                </div>
                            ) || (
                                <SectionWrapper>
                                    <div className="buttons">
                                        <Button
                                            type="action"
                                            label="Download"
                                            onClick={ this.downloadReceipt }
                                            alt
                                        />
                                    </div>

                                    <div className="receipt-container">
                                        <div className="row justify-content-between">
                                            <div className="col-sm-5">
                                                <img className="logo" src={ logo } alt="Salarium" />
                                            </div>

                                            <div className="col-sm-3 offset-sm-4">
                                                <div className="address details">
                                                    <p>Salarium Pte Ltd.</p>
                                                    <p>36 Robinson Road #13-01</p>
                                                    <p>Singapore 068777</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-sm-3">
                                                <div className="details">
                                                    <p><strong>Billed To:</strong></p>
                                                    <p>{ get( billingInfo, 'account_name' ) }</p>
                                                    <p>{ get( billingInfo, 'first_name' ) } { get( billingInfo, 'last_name' ) }</p>
                                                    <p>{ get( billingInfo, 'address_1' ) }</p>

                                                    { get( billingInfo, 'address_2' ) && ( <p> { get( billingInfo, 'address_2' ) } </p> ) }
                                                    { get( billingInfo, 'address_3' ) && ( <p> { get( billingInfo, 'address_3' ) } </p> ) }

                                                    <p>{ get( billingInfo, 'city' ) }</p>
                                                    <p>{ get( billingInfo, 'state' ) } { get( billingInfo, 'country' ) } { get( billingInfo, 'zip_code' ) }</p>
                                                </div>
                                            </div>

                                            <div className="col-sm-6">
                                                <div className="details">
                                                    <p><strong>Receipt No.:</strong> { receiptId }</p>
                                                    <p><strong>Payment Received On:</strong> { paymentReceivedOn && formatDate( paymentReceivedOn, DATE_FORMATS.DISPLAY ) }</p>
                                                    <p><strong>Payment Method:</strong> { paymentMethod }</p>
                                                </div>
                                            </div>

                                            <div className="col-sm-3">
                                                <H3><strong>Paid Amount</strong></H3>
                                                <H2><strong>{ formatCurrency( total ) } { currency }</strong></H2>
                                            </div>
                                        </div>

                                        <p className="billing-details"><strong>Billing Details</strong></p>

                                        { this.renderBillingDetailsTable() }

                                        { this.renderVatInfo( vatInfo, netTotal, currency ) }

                                        <div className="receipt-total">
                                            <H3><strong>Total Amount: { formatCurrency( total ) } { currency }</strong></H3>
                                        </div>
                                    </div>
                                </SectionWrapper>
                            ) }
                        </Container>
                    </div>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    accountDetails: makeSelectAccountDetails()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { initializeData },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ReceiptDetails );
