
import { RESET_STORE } from 'containers/App/constants';
import {
    INITIALIZE_DATA,
    SET_ACCOUNT_DETAILS,
    SET_LOADING,
    SET_NOTIFICATION
} from './constants';

/**
 * Initialize page data
 *
 * @returns {Object} action
 */
export function initializeData() {
    return {
        type: INITIALIZE_DATA
    };
}

/**
 * Set loading state of the page
 *
 * @param {Boolean} payload - Loading state
 * @returns {Object} action
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 *
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Sets account details
 *
 * @param {Object} payload - Account details
 * @returns {Object} action
 */
export function setAccountDetails( payload ) {
    return {
        type: SET_ACCOUNT_DETAILS,
        payload
    };
}

/**
 * Resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
