import { fromJS } from 'immutable';
import { subscriptionService } from 'utils/SubscriptionService';

import { RESET_STORE } from 'containers/App/constants';
import {
    SET_LOADING,
    SET_NOTIFICATION,
    SET_ACCOUNT_DETAILS
} from './constants';

const initialState = fromJS({
    loading: true,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    account_details: {
        is_trial: true,
        main_plan: null
    }
});

/**
 * Parses account details
 *
 * @param {Object} data - Account details
 * @returns {Object}
 */
function parseAccountDetails( data ) {
    const [subscription] = data.subscriptions;
    subscriptionService.updateSubscription( subscription );

    return {
        ...data,
        is_trial: subscription.is_trial,
        main_plan: subscription.main_product_id
    };
}

/**
 *
 * ReceiptDetails reducer
 *
 */
function receiptDetailsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ACCOUNT_DETAILS:
            return state.set( 'account_details', fromJS( parseAccountDetails( action.payload ) ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default receiptDetailsReducer;
