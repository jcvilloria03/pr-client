/*
 *
 * ReceiptDetails constants
 *
 */
const namespace = 'app/Receipts/View';

export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const SET_ACCOUNT_DETAILS = `${namespace}/SET_ACCOUNT_DETAILS`;
export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
