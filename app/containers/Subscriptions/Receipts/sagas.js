import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';

import { Fetch } from 'utils/request';

import {
    INITIALIZE_DATA,
    PAGE_STATUSES
} from './constants';

import {
    setPageStatus,
    setAccountDetails,
    setReceiptsList,
    setNotification
} from './actions';

/**
 * Initialize data for page
 */
export function* initializeData() {
    yield put( setPageStatus( PAGE_STATUSES.LOADING ) );

    try {
        const [ accountDetails, receipts ] = yield [
            call( Fetch, '/account', { method: 'GET' }),
            call( Fetch, '/subscriptions/receipts', { method: 'GET' })
        ];

        yield [
            put( setAccountDetails( accountDetails ) ),
            put( setReceiptsList( receipts ) )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPageStatus( PAGE_STATUSES.READY ) );
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Watcher for INITIALIZE_DATA
 *
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData
];
