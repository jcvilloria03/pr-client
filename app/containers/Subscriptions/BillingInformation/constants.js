/*
 *
 * License constants
 *
 */
const namespace = 'app/BillingInformation';

export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_FORM_OPTIONS = `${namespace}/SET_FORM_OPTIONS`;
export const SET_ACCOUNT_DETAILS = `${namespace}/SET_ACCOUNT_DETAILS`;
export const SET_BILLING_INFORMATION_STATE = `${namespace}/SET_BILLING_INFORMATION_STATE`;
export const SET_BILLING_INFORMATION = `${namespace}/SET_BILLING_INFORMATION`;
export const SAVE_BILLING_INFORMATION = `${namespace}/SAVE_BILLING_INFORMATION`;

export const BILLING_INFORMATION_EDIT = 'edit';
export const BILLING_INFORMATION_VIEW = 'view';
export const BILLING_INFORMATION_SAVING = 'saving';

export const BILLING_INFORMATION_FORM = [
    [
        {
            id: 'account_name',
            label: 'Account Name',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'first_name',
            label: 'First Name',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'last_name',
            label: 'Last Name',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'email',
            label: 'Email',
            field_params: {
                validations: {
                    required: true
                }
            }
        }
    ],
    [
        {
            id: 'phone',
            label: 'Phone',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'address_1',
            label: 'Address Line 1',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'address_2',
            label: 'Address Line 2',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'address_3',
            label: 'Address Line 3',
            field_params: {
                validations: {
                    required: false
                }
            }
        }
    ],
    [
        {
            id: 'city',
            label: 'City',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'zip_code',
            label: 'Zip Code',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'state',
            label: 'State/Province',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'country',
            label: 'Country',
            type: 'select',
            field_params: {
                validations: {
                    required: true
                }
            }
        }
    ]
];

export const NOTIFY_USER = `${namespace}/NOTIFY_USER`;

export const SET_PAYMENT_METHOD = `${namespace}/SET_PAYMENT_METHOD`;
export const REQUEST_SETUP_INTENT = `${namespace}/REQUEST_SETUP_INTENT`;
export const REQUEST_SETUP_INTENT_LOADING = `${namespace}/REQUEST_SETUP_INTENT_LOADING`;
export const SET_CLIENT_SECRET = `${namespace}/SET_CLIENT_SECRET`;
export const SET_CARD_MODAL = `${namespace}/SET_CARD_MODAL`;
export const SET_REMOVE_CARD_LOADING = `${namespace}/SET_REMOVE_CARD_LOADING`;
export const REMOVE_CARD = `${namespace}/REMOVE_CARD`;

