
import { RESET_STORE } from 'containers/App/constants';

import {
    INITIALIZE_DATA,
    SET_LOADING,
    SET_NOTIFICATION,
    SET_FORM_OPTIONS,
    SET_ACCOUNT_DETAILS,
    SET_BILLING_INFORMATION_STATE,
    SET_BILLING_INFORMATION,
    SAVE_BILLING_INFORMATION,
    SET_PAYMENT_METHOD,
    NOTIFY_USER,
    SET_CLIENT_SECRET,
    REQUEST_SETUP_INTENT_LOADING,
    REQUEST_SETUP_INTENT,
    SET_CARD_MODAL,
    SET_REMOVE_CARD_LOADING,
    REMOVE_CARD
} from './constants';

/**
 * Initialize page data
 * @returns {Object} action
 */
export function initializeData() {
    return {
        type: INITIALIZE_DATA
    };
}

/**
 * Set loading state of the page
 * @param {Boolean} payload - Loading state
 * @returns {Object} action
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Sets form options
 * @param {Object} payload - Form options
 * @returns {Object} action
 */
export function setFormOptions( payload ) {
    return {
        type: SET_FORM_OPTIONS,
        payload
    };
}

/**
 * Sets account details
 * @param {Object} payload - Account details
 * @returns {Object} action
 */
export function setAccountDetails( payload ) {
    return {
        type: SET_ACCOUNT_DETAILS,
        payload
    };
}

/**
 * Set state of the page billing info
 * @param {String} payload - Billing information state
 * @returns {Object} action
 */
export function setBillingInformationState( payload ) {
    return {
        type: SET_BILLING_INFORMATION_STATE,
        payload
    };
}

/**
 * Sets billing information
 * @param {Object} payload - Billing information
 * @returns {Object} action
 */
export function setBillingInformation( payload ) {
    return {
        type: SET_BILLING_INFORMATION,
        payload
    };
}

/**
 * submit a new employee
 */
export function saveBillingInformation( payload ) {
    return {
        type: SAVE_BILLING_INFORMATION,
        payload
    };
}

/**
 * Sets current payment method
 * @param {Object} payload - Payment method
 * @returns {Object} action
 */
export function setPaymentMethod( payload ) {
    return {
        type: SET_PAYMENT_METHOD,
        payload
    };
}

/**
 * Notify User
 */
export function notifyUser( payload ) {
    return {
        type: NOTIFY_USER,
        payload
    };
}

/**
 * Request Setup Intent
 */
export function requestSetupIntent() {
    return {
        type: REQUEST_SETUP_INTENT
    };
}

/**
 * Request Setup Intent Loading state
 */
export function requestSetupIntentLoading( payload ) {
    return {
        type: REQUEST_SETUP_INTENT_LOADING,
        payload
    };
}

/**
 * Set client secret
 */
export function setClientSecret( payload ) {
    return {
        type: SET_CLIENT_SECRET,
        payload
    };
}

/**
 * Set card modal
 */
export function setCardModal( payload ) {
    return {
        type: SET_CARD_MODAL,
        payload
    };
}

/**
 * Set remove card loading
 */
export function setRemoveCardLoading( payload ) {
    return {
        type: SET_REMOVE_CARD_LOADING,
        payload
    };
}

/**
 * Remove card
 */
export function removeCard() {
    return {
        type: REMOVE_CARD
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
