import React from 'react';

import { injectStripe } from 'react-stripe-elements';

import Input from 'components/Input';

import {
    SpinnerWrapper,
} from '../styles';

import CardSection from './CardSection';

class CardForm extends React.Component {
    static propTypes = {
        address: React.PropTypes.object,
        clientSecret: React.PropTypes.string,
        onError: React.PropTypes.func,
        onSuccess: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        this.state = {
            cardholder_name: '',
            disable_submit_button: false,
            card_error: null,
            loading: false
        };

        this.fields = {};
    }

    validateForm() {
        let valid = true;
        let errorMessage = null;
        let disableSubmitButtom = false;
        let cardEmpty = document.querySelector('.StripeElement--empty');

        const invalidName = this.cardholderNameRef._validate( this.state.cardholder_name );

        if ( cardEmpty || invalidName ) {
            disableSubmitButtom = true;
            valid = false;
            errorMessage = 'This field is required';
        }

        this.setState({
            card_error: errorMessage,
            disable_submit_button: disableSubmitButtom
        });

        return valid;
    }

    handleCarholderNameChange = ( name ) => {
        let disableButton = Boolean( this.state.card_error );

        if ( !Boolean( name ) ) {
            disableButton = true;
        }

        this.setState({
            cardholder_name: name,
            disable_submit_button: disableButton
        });
    }

    handleCardEvent = ( event ) => {
        let errorMessage = null;
        let disableSubmitButtom = this.cardholderNameRef._validate( this.state.cardholder_name );
        let cardEmpty = document.querySelector('.StripeElement--empty');
        const cardComplete = document.querySelector('.StripeElement--complete');

        if ( event.empty || ( event == 'blur' && cardEmpty ) ) {
            errorMessage = 'This field is required';
        }

        if ( event.error && event.error.message ) {
            errorMessage = event.error.message;
        }

        if ( !event.complete ) {
            disableSubmitButtom = true;
        }

        if ( ( event.complete || cardComplete ) && this.state.cardholder_name ) {
            disableSubmitButtom = false;
            errorMessage = null;
        } else {
            disableSubmitButtom = true;
        }

        this.setState({
            card_error: errorMessage,
            disable_submit_button: disableSubmitButtom
        });
    }

    addNewCard() {
        const cardElement = this.props.elements.getElement('card');
        const address = this.props.address;

        this.props.stripe.confirmCardSetup(this.props.clientSecret, {
            payment_method: {
                card: cardElement,
                billing_details: {
                    name: this.state.cardholder_name,
                    email: address.email,
                    phone: address.phone,
                    address: {
                        line1: address.address_1,
                        line2: address.address_2 + ' ' + address.address_3,
                        city: address.city,
                        state: address.state,
                        postal_code: address.zip_code,
                        country: 'PH'
                    }
                },
            },
        })
        .then(( result ) => {
            if ( result.error ) {
                this.props.onError( result.error.message )
            } else {
                this.props.onSuccess( result.setupIntent )
            }
            this.setState({ loading: false });
        });
    }

    handleSubmit = (ev) => {
        ev.preventDefault();

        this.setState({ loading: true });
        this.validateForm() && this.props.clientSecret && this.addNewCard();
    };

    render() {
        return (
            <form onSubmit={ this.handleSubmit }>
                <div className="row card-section">
                    <div className="col-sm-12">
                        <Input
                            id="cardholder_name"
                            placeholder="Cardholder Name"
                            type="text"
                            value={ this.state.cardholder_name }
                            required
                            ref={ ( ref ) => { this.cardholderNameRef = ref; } }
                            onChange={ ( value ) => {
                                this.handleCarholderNameChange( value );
                            } }
                        />
                    </div>

                    <div className="col-sm-12 mt-10">
                        <CardSection onCardFieldEvent={ this.handleCardEvent } />
                        { this.state.card_error && (
                            <p className="card-error">{ this.state.card_error }</p>
                        )}
                    </div>

                    <div className="col-sm-12 mt-15">
                        <button disabled={ this.state.disable_submit_button || this.state.loading }>
                            {
                                this.state.loading
                                    ? <SpinnerWrapper><i className="fa fa-circle-o-notch fa-spin fa-fw" /></SpinnerWrapper>
                                    :  'USE THIS CARD'
                            }
                        </button>
                    </div>
                    <div className="col-sm-12 confirm-terms">
                        <p>
                            By adding this card, you allow <b> Salarium </b> to charge your card for future payments in accordance with Salarium's Terms of Service.
                        </p>
                    </div>
                </div>
            </form>
        );
    }
}

export default injectStripe( CardForm );
