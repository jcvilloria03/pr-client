import React from 'react';
import { CardElement } from 'react-stripe-elements';

import { CARD_ELEMENT_OPTIONS } from '../../../../constants';

class CardSection extends React.Component {
    static propTypes = {
        onCardFieldEvent: React.PropTypes.func
    }

    handleChange = ( change ) => {
        this.props.onCardFieldEvent( change );
    };

    handleBlur = () => {
        this.props.onCardFieldEvent( 'blur' );
    };

    handleFocus = () => {
        this.props.onCardFieldEvent( 'focus' );
    };

    render() {
        return (
        <div>
            <CardElement
                onChange={ this.handleChange }
                onBlur={ this.handleBlur }
                onFocus={ this.handleFocus }
                options={ CARD_ELEMENT_OPTIONS }
            />
        </div>
        );
    }
}

export default CardSection;
