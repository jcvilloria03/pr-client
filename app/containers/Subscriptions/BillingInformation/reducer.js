import { fromJS } from 'immutable';
import { subscriptionService } from 'utils/SubscriptionService';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_NOTIFICATION,
    SET_ACCOUNT_DETAILS,
    SET_FORM_OPTIONS,
    SET_BILLING_INFORMATION_STATE,
    SET_BILLING_INFORMATION,
    BILLING_INFORMATION_VIEW,
    SET_PAYMENT_METHOD,
    SET_CLIENT_SECRET,
    REQUEST_SETUP_INTENT_LOADING,
    SET_CARD_MODAL,
    SET_REMOVE_CARD_LOADING
} from './constants';

const initialState = fromJS({
    loading: true,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    form_options: {},
    account_details: {
        is_trial: true,
        main_plan: null
    },
    billing_information: {},
    billing_information_state: BILLING_INFORMATION_VIEW,
    payment_method: {},
    client_secret: null,
    setup_intent_loading: false,
    card_modal: false,
    remove_card_loading: false
});

/**
 * Parses account details
 * @param {Object} data - Account details
 * @returns {Object}
 */
function parseAccountDetails( data ) {
    const [subscription] = data.subscriptions;
    subscriptionService.updateSubscription( subscription );

    return {
        ...data,
        is_trial: subscription.is_trial,
        main_plan: subscription.main_product_id
    };
}

/**
 *
 * License reducer
 *
 */
function billingInformationReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ACCOUNT_DETAILS:
            return state.set( 'account_details', fromJS( parseAccountDetails( action.payload ) ) );
        case SET_FORM_OPTIONS:
            return state.set( 'form_options', fromJS( action.payload ) );
        case SET_BILLING_INFORMATION_STATE:
            return state.set( 'billing_information_state', action.payload );
        case SET_BILLING_INFORMATION:
            return state.set( 'billing_information', fromJS( action.payload ) );
        case SET_PAYMENT_METHOD:
            return state.set( 'payment_method', fromJS( action.payload ) );
        case SET_CLIENT_SECRET:
            return state.set( 'client_secret', action.payload );
        case REQUEST_SETUP_INTENT_LOADING:
            return state.set( 'setup_intent_loading', action.payload );
        case SET_CARD_MODAL:
            return state.set( 'card_modal', action.payload );
        case SET_REMOVE_CARD_LOADING:
            return state.set( 'remove_card_loading', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default billingInformationReducer;
