import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';

import { Fetch } from 'utils/request';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    INITIALIZE_DATA,
    SAVE_BILLING_INFORMATION,
    BILLING_INFORMATION_VIEW,
    BILLING_INFORMATION_EDIT,
    BILLING_INFORMATION_SAVING,
    REQUEST_SETUP_INTENT,
    NOTIFY_USER,
    REMOVE_CARD
} from './constants';

import {
    setLoading,
    setNotification,
    setAccountDetails,
    setFormOptions,
    setBillingInformation,
    setBillingInformationState,
    setPaymentMethod,
    setClientSecret,
    requestSetupIntentLoading,
    setCardModal,
    setRemoveCardLoading
} from './actions';

/**
 * Initialize data for page
 */
export function* initializeData() {
    yield put( setLoading( true ) );

    try {
        const formOptions = {};

        const [ accountDetails, billingInformation, countries, paymentMethod ] = yield [
            call( Fetch, '/account', { method: 'GET' }),
            call( Fetch, '/subscriptions/billing_information', { method: 'GET' }),
            call( Fetch, '/countries', { method: 'GET' }),
            call( Fetch, '/subscription-payment/payment-method/current', { method: 'GET' })
        ];

        if ( billingInformation ) {
            yield put( setBillingInformation( billingInformation.data ) );
        }

        formOptions.countries = countries.data.map( ( value ) => ({ label: value.name, value: value.name }) );
        const paymentMethodData = paymentMethod.data.constructor === Array ? {} : paymentMethod.data;

        yield [
            put( setAccountDetails( accountDetails ) ),
            put( setFormOptions( formOptions ) ),
            put( setPaymentMethod( paymentMethodData ) )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Save billing information request
 */
export function* saveBillingInformation( action ) {
    yield put( setBillingInformationState( BILLING_INFORMATION_SAVING ) );

    try {
        const billingInformation = yield call( Fetch, '/subscriptions/billing_information', {
            method: 'PATCH',
            data: action.payload
        });

        yield put( setBillingInformation( billingInformation.data ) );
        yield put( setBillingInformationState( BILLING_INFORMATION_VIEW ) );
    } catch ( error ) {
        yield put( setBillingInformationState( BILLING_INFORMATION_EDIT ) );
        yield call( notifyError, error );
    }
}

/**
 * Request New SetupIntent
 */
export function* requestSetupIntent() {
    yield put( requestSetupIntentLoading( true ) );
    try {
        const setupIntent = yield call( Fetch, '/subscription-payment/payment-method/setup-intent', {
            method: 'POST',
            data: {}
        });

        yield put( setClientSecret( setupIntent.data.client_secret ) );
        yield put( setCardModal( true ) );

    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( requestSetupIntentLoading( false ) );
    }
}

/**
 * Request New SetupIntent
 */
export function* removeCard() {
    yield put( setRemoveCardLoading( true ) );
    try {
        const response = yield call( Fetch, '/subscription-payment/payment-method/current/remove', {
            method: 'DELETE'
        });

        yield put( setPaymentMethod( {} ) )
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setRemoveCardLoading( false ) );
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const data = !payload.payload ? payload : payload.payload;

    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: data.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( data ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watcher for INITIALIZE_DATA
 *
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SAVE_BILLING_INFORMATION
 */
export function* watchForSaveBillingInformation() {
    const watcher = yield takeLatest( SAVE_BILLING_INFORMATION, saveBillingInformation );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REQUEST_SETUP_INTENT
 */
export function* watchForRequestSetupIntent() {
    const watcher = yield takeLatest( REQUEST_SETUP_INTENT, requestSetupIntent );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REMOVE_CARD
 */
export function* watchForRemoveCard() {
    const watcher = yield takeLatest( REMOVE_CARD, removeCard );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFY_USER
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFY_USER, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForReinitializePage,
    watchForSaveBillingInformation,
    watchForRequestSetupIntent,
    watchForRemoveCard,
    watchForNotifyUser
];
