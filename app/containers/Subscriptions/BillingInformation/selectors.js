import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'billingInformation' );

/**
 * Default selector used by License
 */

const makeSelectLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectAccountDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'account_details' ).toJS()
);

const makeSelectFormOptions = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'form_options' ).toJS()
);

const makeSelectBillingInformationState = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'billing_information_state' )
);

const makeSelectBillingInformation = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'billing_information' ).toJS()
);

const makeSelectPaymentMethod = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'payment_method' ).toJS()
);

const makeSelectClientSecret = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'client_secret' )
);

const makeSelectSetupIntentLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'setup_intent_loading' )
);

const makeSelectCardModal = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'card_modal' )
);

const makeSelectRemoveCardLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'remove_card_loading' )
);

export {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectAccountDetails,
    makeSelectFormOptions,
    makeSelectBillingInformationState,
    makeSelectBillingInformation,
    makeSelectPaymentMethod,
    makeSelectClientSecret,
    makeSelectSetupIntentLoading,
    makeSelectCardModal,
    makeSelectRemoveCardLoading
};
