/* eslint-disable react/no-did-mount-set-state */
import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import { StripeProvider, Elements } from 'react-stripe-elements';

import StripeLogo from 'assets/images/stripe_logo.png';
import PaynamicsLogo from 'assets/images/paynamics_logo.png';

import { isAuthorized } from 'utils/Authorization';

import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';

import { stripNonNumeric, formatMobileNumber } from 'utils/functions';
import { renderField } from 'utils/form';
import { H2, H3, H5, P } from 'components/Typography';
import A from 'components/A';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import Modal from 'components/Modal';
import Button from 'components/Button';
import Icon from 'components/Icon';
import SalConfirm from 'components/SalConfirm';

import SubHeader from 'containers/SubHeader';
import Radio from 'components/Radio';

import { STRIPE_PUB_KEY } from '../../../constants';

import {
    initializeData,
    setBillingInformationState,
    saveBillingInformation,
    requestSetupIntent,
    notifyUser,
    setClientSecret,
    setCardModal,
    removeCard
} from './actions';

import {
    SectionWrapper,
    ModalBody,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';

import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectAccountDetails,
    makeSelectFormOptions,
    makeSelectBillingInformationState,
    makeSelectBillingInformation,
    makeSelectPaymentMethod,
    makeSelectClientSecret,
    makeSelectSetupIntentLoading,
    makeSelectCardModal,
    makeSelectRemoveCardLoading
} from './selectors';

import {
    BILLING_INFORMATION_EDIT,
    BILLING_INFORMATION_VIEW,
    BILLING_INFORMATION_SAVING,
    BILLING_INFORMATION_FORM
} from './constants';

import CardForm from './CardForm';

/**
 * Billing Information Component
 */
export class BillingInformation extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        initializeData: React.PropTypes.func,
        accountDetails: React.PropTypes.object,
        paymentMethod: React.PropTypes.object,
        billing_information_state: React.PropTypes.string,
        form_options: React.PropTypes.object,
        setBillingInformationState: React.PropTypes.func,
        notifyUser: React.PropTypes.func,
        saveBillingInformation: React.PropTypes.func,
        requestSetupIntent: React.PropTypes.func,
        clientSecret: React.PropTypes.string,
        setupIntentLoading: React.PropTypes.bool,
        cardModal: React.PropTypes.bool,
        setCardModal: React.PropTypes.func,
        removeCardLoading: React.PropTypes.bool,
        removeCard: React.PropTypes.func,
        setClientSecret: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        this.state = {
            stripe: null,
            loadingAddress: true,
            data: {
                first_name: null,
                last_name: null,
                account_name: null,
                email: null,
                address_1: null,
                address_2: null,
                address_3: null,
                city: null,
                state: null,
                country: null,
                zip_code: null
            },
            showRemoveConfirm: false,
            paymentMethod: null,
            payOverTheCounter: false
        };

        this.fields = {};
    }

    componentWillMount() {
        isAuthorized([
            'view.account',
            'view.salpay_integration'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.account' ];

            if ( authorized ) {
                this.setState({ salpay_view_permission: authorization[ 'view.salpay_integration' ] });
            } else {
                window.location.href = '/control-panel/companies';
            }
        });
        this.props.initializeData();
    }

    componentDidMount() {
        if ( window.Stripe ) {
            this.setState({ stripe: window.Stripe( STRIPE_PUB_KEY ) });
        } else {
            document.querySelector( '#stripe-js' ).addEventListener( 'load', () => {
                // Create Stripe instance once Stripe.js loads
                this.setState({ stripe: window.Stripe( STRIPE_PUB_KEY ) });
            });
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.billing_information ) {
            this.setState({ data: nextProps.billing_information });
        }

        if ( nextProps.cardModal === true && !this.props.cardModal ) {
            this.paymentFormModal.toggle();
        }

        if ( nextProps.cardModal === false ) {
            this.paymentFormModal.close();
        }

        this.setState({ paymentMethod: nextProps.paymentMethod });
    }

    onAddCardError = ( message ) => {
        this.props.setCardModal( false );
        if ( message ) {
            const payload = {
                show: true,
                title: 'Unable to Add New Card',
                message,
                type: 'error'
            };

            this.props.notifyUser( payload );
        }
    }

    onAddCardSuccess = ( payment ) => {
        this.props.setClientSecret( null );
        this.props.setCardModal( false );

        if ( payment ) {
            const payload = {
                show: true,
                title: 'Success',
                message: 'New card has been added successfully',
                type: 'success'
            };

            this.props.notifyUser( payload );
            this.props.initializeData();
        }
    }

    getClientSecret() {
        this.setState({ payOverTheCounter: false });

        if ( !this.props.clientSecret ) {
            this.props.requestSetupIntent();
        } else {
            this.paymentFormModal.toggle();
        }
    }

    getFieldData( field ) {
        switch ( field.id ) {
            case 'country':
                return this.props.form_options.countries;

            default:
                return null;
        }
    }

    getOnChangeHandler( field ) {
        const formatter = ( value ) => {
            // Define formatter function based on the field id
            switch ( field.id ) {
                case 'zip_code':
                    return stripNonNumeric( value );

                case 'phone':
                    return formatMobileNumber( value );

                case 'country':
                    return value.value;

                default:
                    return value;
            }
        };

        return ( value ) => {
            const formatted = formatter( value );

            this.fields[ field.id ].setState({ value: formatted }, () => this.setState( ( prevState ) => ({
                data: {
                    ...prevState.data,
                    [ field.id ]: formatter( value )
                }
            }) ) );
        };
    }

    validateForm() {
        let valid = true;

        BILLING_INFORMATION_FORM.flat().forEach( ( fieldDefinition ) => {
            const field = this.fields[ fieldDefinition.id ];

            switch ( fieldDefinition.type ) {
                case 'select':
                    if ( !field._checkRequire( typeof field.state.value === 'object' && field.state.value !== null ? field.state.value.value : field.state.value ) ) {
                        valid = false;
                    }

                    break;

                default:
                    if ( field._validate( field.state.value ) ) {
                        valid = false;
                    }
            }
        });

        return valid;
    }

    saveBillingInformation() {
        this.validateForm() && this.props.saveBillingInformation( this.state.data );
    }

    renderBillingInformation() {
        return (
            <div className="row">
                { BILLING_INFORMATION_FORM.map( ( group, groupIndex ) => (
                    <div className="col-sm-4" key={ groupIndex }>
                        { group.map( ( field, index ) => this.renderBillingField( field, index ) ) }
                    </div>
                ) ) }
            </div>
        );
    }

    renderPaymentModalBody() {
        return (
            <ModalBody>
                <StripeProvider stripe={ this.state.stripe }>
                    <Elements>
                        <CardForm
                            clientSecret={ this.props.clientSecret }
                            address={ this.state.data }
                            onError={ this.onAddCardError }
                            onSuccess={ this.onAddCardSuccess }
                        />
                    </Elements>
                </StripeProvider>
            </ModalBody>
        );
    }

    renderBillingField( field, index ) {
        return (
            <div key={ index }>
                <label htmlFor={ `billing-${index}` }>{ field.label }</label>

                <div className="billing-information-field">
                    { [ BILLING_INFORMATION_EDIT, BILLING_INFORMATION_SAVING ].includes( this.props.billing_information_state ) && renderField({
                        id: `billing-${index}`,
                        value: this.state.data[ field.id ] || '',
                        data: this.getFieldData( field ),
                        field_type: field.type,
                        onChange: this.getOnChangeHandler( field ),
                        disabled: this.props.billing_information_state === BILLING_INFORMATION_SAVING,
                        ref: ( ref ) => { this.fields[ field.id ] = ref; },
                        ...field.field_params
                    }) || (
                        <H5>{ this.state.data[ field.id ] || 'N/A' }</H5>
                    ) }
                </div>
            </div>
        );
    }

    renderAddressSection() {
        return (
            <div className="billing-information">
                { this.props.loading
                    ? (
                        <div className="loader">
                            <LoadingStyles>
                                <H2>Loading Address Section.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                    )
                    : (
                        <div>
                            <div className="title">
                                <H5><Icon className="fa-icon" name="envelope" /> &nbsp;Billing Information</H5>

                                { this.props.billing_information_state === BILLING_INFORMATION_EDIT && (
                                    <Button
                                        type="action"
                                        style={ { marginBottom: '20px' } }
                                        label="Save Billing Information"
                                        onClick={ () => this.saveBillingInformation() }
                                    />
                                ) }

                                { this.props.billing_information_state === BILLING_INFORMATION_SAVING && (
                                    <Button
                                        type="action"
                                        style={ { marginBottom: '20px' } }
                                        label="Saving Billing Information"
                                        disabled
                                    />
                                ) }

                                { this.props.billing_information_state === BILLING_INFORMATION_VIEW && (
                                    <Button
                                        alt
                                        type="action"
                                        style={ { marginBottom: '20px', color: '#83d24b' } }
                                        label={ <span><Icon className="fa-icon" name="pencil" /></span> }
                                        onClick={ () => this.props.setBillingInformationState( BILLING_INFORMATION_EDIT ) }
                                    />
                                ) }
                            </div>
                            { this.renderBillingInformation() }
                        </div>
                    )
                }
            </div>
        );
    }

    renderCardSection() {
        const paymentMethod = Object.keys( this.props.paymentMethod ).length > 0 && this.state.paymentMethod;

        return (
            <div>
                <div className="payment-method">
                    <div className="title">
                        <div className="payment-card">
                            <H5>
                                <Icon className="fa-icon card-icon" name="creditCard" />
                                Payment Method
                            </H5>

                            <Button
                                type="action"
                                label="Add Card"
                                disabled={ this.props.setupIntentLoading }
                                onClick={ () => this.getClientSecret() }
                            />

                            <P noBottomMargin>powered by:</P>

                            <div className="stripe-logo">
                                <img alt="Stripe Logo" src={ StripeLogo } />
                            </div>
                        </div>

                        { !this.props.loading && (
                            <div className="payment-over-counter">
                                <div className="radio-wrapper">
                                    <Radio
                                        value="payOverTheCounter"
                                        disabled={ !!paymentMethod || this.props.setupIntentLoading }
                                        checked={ this.state.payOverTheCounter }
                                        onChange={ () => {
                                            this.setState({ payOverTheCounter: true });
                                        } }
                                    >
                                        <H5 noBottomMargin className="text-over-counter">Pay Over the Counter</H5>
                                    </Radio>

                                    <P noBottomMargin>powered by:</P>

                                    <div className="paynamics-logo">
                                        <img alt="Paynamics Logo" src={ PaynamicsLogo } />
                                    </div>
                                </div>

                                <div className="over-counter-info">
                                    <P noBottomMargin>Info on over the counter payment</P>
                                    <A
                                        href="https://www.paynamics.com/"
                                        target="_blank"
                                        className="guide-link"
                                    >
                                    OTC payment guide
                                </A>
                                </div>
                            </div>
                        )}
                    </div>

                    <SalConfirm
                        onConfirm={ () => {
                            this.props.removeCard();
                            this.setState({ showRemoveConfirm: false });
                        } }
                        onClose={ () => this.setState({ showRemoveConfirm: false }) }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to remove this current payment method
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showRemoveConfirm }
                    />

                    <Modal
                        title="Enter Card Information"
                        ref={ ( ref ) => { this.paymentFormModal = ref; } }
                        body={ this.renderPaymentModalBody() }
                        size="md"
                        showClose
                        onClose={ () => this.props.setCardModal( false ) }
                        keyboard={ false }
                        buttons={ [] }
                    />

                    { !this.props.loading && paymentMethod ? (
                        <div>
                            <div className="row payment-card-view">
                                <div className="col-sm-3" >
                                        Card Number <br />
                                    <b> { paymentMethod.brand } </b>  ending in <b> { paymentMethod.last4 } </b> <br />
                                </div>
                                <div className="col-sm-3" >
                                        Cardholder Name <br />
                                    <b>{ paymentMethod.name } </b> <br />
                                </div>
                                <div className="col-sm-3" >
                                        Exp. Date <br />
                                    <b>{ paymentMethod.exp_date } </b> <br />
                                </div>
                                <div className="col-sm-3" style={ { marginTop: '5px' } }>
                                    <Button
                                        type="danger"
                                        label="Remove Card"
                                        disabled={ this.props.removeCardLoading }
                                        onClick={ () => this.setState({ showRemoveConfirm: true }) }
                                    />
                                </div>
                            </div>
                        </div>
                        ) : (
                            <div>
                                <div className="row payment-card-view">
                                    <div className="col-sm-12" >
                                        No Payment Data
                                    </div>
                                </div>
                            </div>
                        )
                    }
                </div>
            </div>
        );
    }

    /**
     * Component Render Method
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Billing Information"
                    meta={ [
                        { name: 'description', content: 'Billing Information Details' }
                    ] }
                />

                <SubHeader
                    items={ subscriptionService.getTabs() }
                />

                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        accountViewPermission: true,
                        salpayViewPermission: this.state.salpay_view_permission,
                        isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products )
                    }) }
                />

                <Container>
                    <SectionWrapper>
                        <div className="content">
                            { this.renderAddressSection() }
                            { this.renderCardSection() }
                        </div>
                    </SectionWrapper>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    accountDetails: makeSelectAccountDetails(),
    form_options: makeSelectFormOptions(),
    billing_information_state: makeSelectBillingInformationState(),
    billing_information: makeSelectBillingInformation(),
    paymentMethod: makeSelectPaymentMethod(),
    clientSecret: makeSelectClientSecret(),
    setupIntentLoading: makeSelectSetupIntentLoading(),
    cardModal: makeSelectCardModal(),
    removeCardLoading: makeSelectRemoveCardLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        {
            initializeData,
            setBillingInformationState,
            saveBillingInformation,
            requestSetupIntent,
            setClientSecret,
            notifyUser,
            setCardModal,
            removeCard
        },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( BillingInformation );
