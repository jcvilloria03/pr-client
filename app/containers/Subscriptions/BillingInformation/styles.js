import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const SectionWrapper = styled.div`
    .content {
        margin: 40px 0;

        .billing-information {
            margin-bottom: 60px;
            border-radius: 5px;
            border: solid 1px #adadad;
            background-color: #fff;
            padding: 25px;

            .title {
                display: flex;
                align-items: center;
                margin-bottom: 20px;
                justify-content: space-between;
                border-bottom: solid 2px #d0e1e1;

                h5 {
                    margin: 0;
                    margin-right: 20px;
                    margin-bottom: 20px;
                    font-weight: 600;
                    font-size: 22px;
                }
            }
        }

        .payment-method {
            margin-bottom: 60px;
            border-radius: 5px;
            border: solid 1px #adadad;
            background-color: #fff;
            padding: 25px;

            .title {
                display: flex;
                align-items: center;
                margin-bottom: 20px;
                padding-bottom: 20px;
                border-bottom: solid 2px #d0e1e1;

                .payment-card {
                    flex: 1;
                    display: flex;
                    align-items: center;

                    .card-icon {
                        margin-right: 10px;
                    }

                    h5 {
                        margin: 0;
                        font-weight: 600;
                        font-size: 22px;
                        margin-right: 10px;
                    }

                    p {
                        margin: 0 12px;
                        font-weight: 600;
                        color: #000000;
                    }

                    .stripe-logo {
                        background-color: black;
                        padding: 0 6px;
                        border-radius: 4px;

                        img {
                            height: 12px;
                        }
                    }
                }

                .payment-over-counter {
                    flex: 1;

                    .radio-wrapper {
                        display: flex;
                        align-items: center;

                        .text-over-counter {
                            font-weight: 600;
                            color: #000000;
                        }

                        p {
                            margin: 0 12px;
                            font-weight: 600;
                            color: #000000;
                        }

                        .paynamics-logo {
                            position: relative;

                            img {
                                position: absolute;
                                top: -22px;
                                height: 32px;
                            }
                        }
                    }

                    .over-counter-info {
                        display: flex;
                        padding-left: 20px;

                        p {
                            color: #000000;
                        }

                        .guide-link {
                            margin-left: 12px;
                            font-size: 14px;
                        }
                    }
                }
            }
        }

        .billing-information-field {
            margin-bottom: 14px;
        }
    }

    .fa-icon > svg {
        height: 24px;
    }

    .fa-table-icon > svg {
        height: 15px;
    }

    .payment-card-view {
        font-weight: 600;
        font-size: 16px;
    }
`;

export const ModalBody = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 100%;

    form {
        width: 100%;
    }

    .mt-15 {
        margin-top: 15px;
    }

    .mt-10 {
        margin-top: 10px;
    }

    .card-section {
        .CardField-cvc > input {
            -webkit-text-security: square !important;
        }

        input,
        .StripeElement {
            display: block;
            margin: 10px 0 10px 0;
            padding: 17px 14px;
            font-size: 14px;
            box-shadow: 0 3px 6px 0 rgba(49,49,93,0.10),
                        0 1px 2px 0 rgba(0,0,0,0.08);
            border: 0;
            outline: 0;
            border-radius: 4px;
            background: white;
            height: 50px;
        }

        input:focus,
        .StripeElement--focus {
            box-shadow: rgba(50, 50, 93, 0.109804) 0px 4px 6px, rgba(0, 0, 0, 0.0784314) 0px 1px 3px;
            -webkit-transition: all 150ms ease;
            transition: all 150ms ease;
        }

        .card {
            padding-top: 11px !important;
        }

        .card-error {
            -ms-flex-order: 3;
            -webkit-order: 3;
            order: 3;
            color: #f21108;
            font-size: 13px;
            padding-left: 2px;
            margin-bottom: 6px;
        }

        ::placeholder {
            color: #808080;
        }

        :-ms-input-placeholder {
            color: #808080;
        }

        ::-ms-input-placeholder {
            color: #808080;
        }
    }

    button {
        white-space: nowrap;
        border: 0;
        outline: 0;
        display: inline-block;
        height: 40px;
        line-height: 40px;
        padding: 0 14px;
        box-shadow: 0 4px 6px rgba(50, 50, 93, .11), 0 1px 3px rgba(0, 0, 0, .08);
        color: #fff;
        border-radius: 4px;
        font-size: 15px;
        font-weight: 600;
        text-transform: uppercase;
        letter-spacing: 0.025em;
        background-color: #83d24b;
        text-decoration: none;
        -webkit-transition: all 150ms ease;
        transition: all 150ms ease;
        margin-top: 20px;
        width: 100%;
    }

    button:hover {
        color: #fff;
        cursor: pointer;
        background-color: #9ddb70;
        transform: translateY(-1px);
        box-shadow: 0 7px 14px rgba(50, 50, 93, .10), 0 3px 6px rgba(0, 0, 0, .08);
    }

    button:focus {
        background: #9ddb70;
    }

    button:active {
        background: #b9e699;
    }

    button:disabled,
    button[disabled] {
        background-color: #ade188;
        color: #ecf6fa;
        border-color: #ade188;
        opacity: 1;
        cursor: not-allowed;
        transform: none;
        box-shadow: 0;
    }

    .confirm-terms {
        font-size: 14px;
        font-weight: 550;
        color: gray;
        margin-top: 10px;
        text-align: center;
    }
`;

export const SpinnerWrapper = styled.span`
    padding: 0 20px;
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    justify-content: center;
    min-height: 100%;
    text-align: center;
`;
