
import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_ACCOUNT_DETAILS,
    GET_SUBSCRIPTION_SUMMARY,
    SET_SUBSCRIPTION_SUMMARY,
    SET_NOTIFICATION,
    GET_SUBSCRIPTION_STATS,
    SET_SUBSCRIPTION_STATS,
    SET_SUBSCRIBE_LOADING,
    SUBSCRIBE,
    SET_SUBSCRIBE_MODAL,
    SET_PAYMENT_METHOD,
    NOTIFY_USER,
    SET_CLIENT_SECRET,
    REQUEST_SETUP_INTENT_LOADING,
    REQUEST_SETUP_INTENT,
    SET_CARD_MODAL,
    SET_BILLING_INFORMATION
} from './constants';

/**
 * Set loading status of the page
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets account details
 * @param {Object} payload - Account details
 * @returns {Object} action
 */
export function setAccountDetails( payload ) {
    return {
        type: SET_ACCOUNT_DETAILS,
        payload
    };
}

/**
 * Request to fetch subscription details
 * @returns {Object} action
 */
export function getSubscriptionDetails() {
    return {
        type: GET_SUBSCRIPTION_SUMMARY
    };
}

/**
 * Sets subscription details from API
 * @param {Object} payload - Subscription details
 * @returns {Object} action
 */
export function setSubscriptionSummary( payload ) {
    return {
        type: SET_SUBSCRIPTION_SUMMARY,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Request to fetch subscription stats
 * @returns {Object} action
 */
export function getSubscriptionStats() {
    return {
        type: GET_SUBSCRIPTION_STATS
    };
}

/**
 * Sets subscription stats from API
 * @param {Object} payload - Subscription stats
 * @returns {Object} action
 */
export function setSubscriptionStats( payload ) {
    return {
        type: SET_SUBSCRIPTION_STATS,
        payload
    };
}

/**
 * Set loading status of the subscription
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setSubscribeLoading( payload ) {
    return {
        type: SET_SUBSCRIBE_LOADING,
        payload
    };
}

/**
 * Subscribe
 */
export function subscribe( payload ) {
    return {
        type: SUBSCRIBE,
        payload
    };
}

/**
 * Set Subscribe Modal
 */
export function setSubscribeModal( payload ) {
    return {
        type: SET_SUBSCRIBE_MODAL,
        payload
    };
}

/**
 * Sets current payment method
 * @param {Object} payload - Payment method
 * @returns {Object} action
 */
export function setPaymentMethod( payload ) {
    return {
        type: SET_PAYMENT_METHOD,
        payload
    };
}

/**
 * Notify User
 */
export function notifyUser( payload ) {
    return {
        type: NOTIFY_USER,
        payload
    };
}

/**
 * Request Setup Intent
 */
export function requestSetupIntent() {
    return {
        type: REQUEST_SETUP_INTENT
    };
}

/**
 * Request Setup Intent Loading state
 */
export function requestSetupIntentLoading( payload ) {
    return {
        type: REQUEST_SETUP_INTENT_LOADING,
        payload
    };
}

/**
 * Set client secret
 */
export function setClientSecret( payload ) {
    return {
        type: SET_CLIENT_SECRET,
        payload
    };
}

/**
 * Set card modal
 */
export function setCardModal( payload ) {
    return {
        type: SET_CARD_MODAL,
        payload
    };
}

/**
 * Sets billing information
 * @param {Object} payload - Billing information
 * @returns {Object} action
 */
export function setBillingInformation( payload ) {
    return {
        type: SET_BILLING_INFORMATION,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
