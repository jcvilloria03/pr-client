import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'subscriptionSummary' );

/**
 * Default selector used by Subscription
 */

const makeSelectSubscriptionSummary = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'summary' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectSubscriptionStats = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'stats' ).toJS()
);

const makeSelectAccountDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'account_details' ).toJS()
);

const makeSelectSuscribeLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'subscribe_loading' )
);

const makeSelectSubscribeModal = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'subscribe_modal' )
);

const makeSelectPaymentMethod = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'payment_method' ).toJS()
);

const makeSelectClientSecret = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'client_secret' )
);

const makeSelectSetupIntentLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'setup_intent_loading' )
);

const makeSelectCardModal = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'card_modal' )
);

const makeSelectBillingInformation = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'billing_information' ).toJS()
);

export {
    makeSelectSubscriptionSummary,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSubscriptionStats,
    makeSelectAccountDetails,
    makeSelectSuscribeLoading,
    makeSelectSubscribeModal,
    makeSelectPaymentMethod,
    makeSelectClientSecret,
    makeSelectSetupIntentLoading,
    makeSelectCardModal,
    makeSelectBillingInformation
};
