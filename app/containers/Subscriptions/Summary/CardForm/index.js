import React from 'react';

import { injectStripe } from 'react-stripe-elements';
import capitalize from 'lodash/capitalize';

import { renderField } from 'utils/form';
import { stripNonNumeric, formatMobileNumber } from 'utils/functions';
import { H3, H5 } from 'components/Typography';
import Icon from 'components/Icon';

import { BILLING_INFORMATION_FORM } from '../constants';

import {
    SpinnerWrapper,
} from '../styles';

import CardSection from './CardSection';

class CardForm extends React.Component {
    static propTypes = {
        clientSecret: React.PropTypes.string,
        onError: React.PropTypes.func,
        onConfirmSubscribe: React.PropTypes.func,
        paymentMethod: React.PropTypes.object
    }

    constructor( props ) {
        super( props );

        this.state = {
            data: {
                cardholder_name: null,
                email: null,
                phone: null,
                address_1: null,
                address_2: null,
                city: null,
                state: null,
                country: 'PH',
                zip_code: null
            },
            disable_submit_button: false,
            card_error: null,
            loading: false,
            hasPaymentMethod: false,
            card_complete: false
        };

        this.fields = {};
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.subscribeLoading === false ) {
            this.setState({ loading: nextProps.subscribeLoading });
        }

        if ( Object.keys( nextProps.paymentMethod ).length > 0 ) {
            this.setState( { hasPaymentMethod: true } );
        }
    }

    getOnChangeHandler( field ) {
        const formatter = ( value ) => {
            // Define formatter function based on the field id
            switch ( field.id ) {
                case 'zip_code':
                    return stripNonNumeric( value );

                case 'phone':
                    return formatMobileNumber( value );

                case 'country':
                    return value.value;

                default:
                    return value;
            }
        };

        return ( value ) => {
            const formatted = formatter( value );

            this.fields[ field.id ].setState({ value: formatted }, () => this.setState( ( prevState ) => ({
                data: {
                    ...prevState.data,
                    [ field.id ]: formatter( value )
                }
            }) ) );
        };
    }

    validateForm() {
        let valid = true;

        BILLING_INFORMATION_FORM.flat().forEach( ( fieldDefinition ) => {
            const field = this.fields[ fieldDefinition.id ];

            switch ( fieldDefinition.type ) {
                case 'select':
                    if ( !field._checkRequire( typeof field.state.value === 'object' && field.state.value !== null ? field.state.value.value : field.state.value ) ) {
                        valid = false;
                    }
                    break;

                default:
                    if ( field._validate( field.state.value ) ) {
                        valid = false;
                    }
            }
        });

        this.setState({
            disable_submit_button: !valid
        });

        return valid;
    }

    handleCardEvent = ( event ) => {
        let errorMessage = null;
        let validAddress = this.validateForm();
        let disableSubmitButtom = !validAddress;
        let cardComplete = false;
        const field = this.fields[ 'cardholder_name' ];

        if ( event.error && event.error.message ) {
            errorMessage = event.error.message;
        }

        if ( event.empty ) {
            field.setState({ error: false, errorMessage: null })
        }

        if ( !event.complete ) {
            disableSubmitButtom = true;
        } else {
            field._validate( field.state.value )
            cardComplete = true;
        }

        if ( event.complete && validAddress ) {
            disableSubmitButtom = false;
            errorMessage = null;
        } else {
            disableSubmitButtom = true;
        }

        this.setState({
            card_complete: cardComplete,
            card_error: errorMessage,
            disable_submit_button: disableSubmitButtom
        });
    }

    addNewCard() {
        this.setState({ loading: true });
        const cardElement = this.props.elements.getElement('card');

        this.props.stripe.confirmCardSetup(this.props.clientSecret, {
            payment_method: {
                card: cardElement,
                billing_details: {
                    name: this.state.data.cardholder_name,
                    email: this.state.data.email,
                    phone: this.state.data.phone,
                    address: {
                        line1: this.state.data.address_1,
                        line2: this.state.data.address_2 + ' ' + this.state.data.address_3,
                        city: this.state.data.city,
                        state: this.state.data.state,
                        postal_code: this.state.data.zip_code,
                        country: 'PH'
                    }
                },
            },
        })
        .then(( result ) => {
            if ( result.error ) {
                this.props.onError( result.error.message )
            } else {
                this.props.onConfirmSubscribe( true );
            }
            this.setState({ loading: false });
        });
    }

    handleSubmit = (ev) => {
        ev.preventDefault();

        this.props.onConfirmSubscribe( true );
    }

    renderAddressSection() {
        return (
            <div className="row">
                { BILLING_INFORMATION_FORM.map( ( group, groupIndex ) => (
                    <div className="col-sm-12" key={ groupIndex }>
                        <div className="row">
                            { group.map( ( field, index ) => this.renderAddressField( field, index ) ) }
                        </div>
                    </div>
                ) ) }
            </div>
        );
    }

    renderAddressField( field, index ) {
        const col12 = ['Cardholder Name', 'Address Line 1', 'Address Line 2', 'Address Line 3'];
        const classValue = col12.indexOf( field.label ) > -1 ? "col-sm-12" : "col-sm-6";

        return (
            <div className={ "mt-25 " + classValue } key={ index }>
                <label htmlFor={ `billing-${index}` }>{ field.label }</label>
                <div className="address">
                    { renderField({
                        id: `billing-${index}`,
                        value: this.state.data[ field.id ] || '',
                        field_type: field.type,
                        onChange: this.getOnChangeHandler( field ),
                        ref: ( ref ) => { this.fields[ field.id ] = ref; },
                        ...field.field_params
                    })}
                </div>
            </div>
        );
    }

    renderSubscriptionWithForm() {
        return (
            <div>
                <div>
                    <div className="col-sm-12">
                        <div className="address-section">
                            {/* { this.renderAddressSection() } */}
                        </div>
                    </div>

                    <div className="col-sm-12 mt-25 card-section">
                        <label>
                            Card details
                        </label>
                        {/* <CardSection onCardFieldEvent={ this.handleCardEvent } /> */}
                        { this.state.card_error && (
                            <p className="card-error">{ this.state.card_error }</p>
                        )}
                    </div>
                </div>
            </div>
        )
    }

    renderSubscriptionWithoutForm() {
        const paymentMethod = this.props.paymentMethod;
        return (
            <div className="row">
                <div className="title">
                    <H3>
                        <Icon className="fa-icon" name="creditCard" /> &nbsp;
                        CURRENT CARD DETAILS &nbsp;
                    </H3>
                </div>

                <div className="row payment-card-view">
                    <div className="col-sm-5">
                        Cardholder Name
                    </div>
                    <div className="col-sm-6 value">
                        <b>{ paymentMethod.name } </b> <br />
                    </div>
                    <div className="col-sm-5">
                        Card Number
                    </div>
                    <div className="col-sm-6 value">
                        <p>{ paymentMethod.brand } <span className="text-lc"> ending in </span> { paymentMethod.last4 } </p>
                    </div>
                    <div className="col-sm-5">
                        Exp. Date
                    </div>
                    <div className="col-sm-6 value">
                        { paymentMethod.exp_date }
                    </div>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div>
                { this.state.hasPaymentMethod ? (
                    <div className="row">
                    { this.renderSubscriptionWithoutForm() }
                        <div className="col-sm-12 confirm-terms">
                            <p>
                                By confirming your subscription, you allow <b> Salarium </b> to charge your card for future payments in accordance with Salarium's Terms of Service.
                            </p>
                            <br />
                        </div>
                    </div>
                ) : (
                    <div className="row">
                        <div className="col-sm-12 confirm-terms">
                            <p>
                                You are about to change you subscription from <b>Free</b> to <b>Paid</b> plan.
                                <br />
                                Do you wish to proceed?
                            </p>
                            <br /><br />
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default injectStripe( CardForm );
