import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { Fetch } from 'utils/request';
import moment from 'moment/moment';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    GET_SUBSCRIPTION_SUMMARY,
    SUBSCRIBE,
    REQUEST_SETUP_INTENT,
    NOTIFY_USER
} from './constants';
import {
    setLoading,
    setAccountDetails,
    setSubscriptionSummary,
    setNotification,
    setSubscriptionStats,
    setSubscribeLoading,
    setSubscribeModal,
    setPaymentMethod,
    setClientSecret,
    requestSetupIntentLoading,
    setBillingInformation
} from './actions';

/**
 * Fetch subscription summary details
 */
export function* getSubscriptionSummary() {
    yield put( setLoading( true ) );

    try {
        const response = yield call( Fetch, '/account', { method: 'GET' });
        yield call( getSubscriptionStats, response.subscriptions[ 0 ].id );
        yield [
            put( setAccountDetails( response ) ),
            put( setSubscriptionSummary( response ) )
        ];

        const invoicePayment = yield call( Fetch, '/subscription-payment/invoice/payment-status', { method: 'GET' });
        const paymentMethod = yield call( Fetch, '/subscription-payment/payment-method/current', { method: 'GET' });

        const billingInformation = yield call( Fetch, '/subscriptions/billing_information', { method: 'GET' });

        let invoicePaymentData = null;
        if ( invoicePayment && invoicePayment.data && invoicePayment.data.constructor ) {
            invoicePaymentData = invoicePayment.data.constructor === Array ? {} : invoicePayment.data;
        }

        let paymentMethodData = null;
        if ( paymentMethodData && paymentMethodData.data && paymentMethod.data.constructor ) {
            paymentMethodData = paymentMethod.data.constructor === Array ? {} : paymentMethod.data;
            yield put( setPaymentMethod( paymentMethodData ) );
        }

        yield put( setBillingInformation( billingInformation.data ) );

        if ( invoicePaymentData && invoicePaymentData.status === 'failed' ) {
            const message = `Your payment for billing period ${
                moment( invoicePaymentData.period_start_date ).format( 'MMMM DD, YYYY' )
                } - ${
                moment( invoicePaymentData.period_end_date ).format( 'MMMM DD, YYYY' )
                } did not succeed. You can manually settle invoice number ${invoicePaymentData.invoice_number
                } on the Invoice module.`;

            const payload = {
                show: true,
                message,
                type: 'error'
            };

            yield put( setNotification( payload ) );
        }
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Fetch Subscription stats
 * @param {Integer} id - Subscription ID
 */
export function* getSubscriptionStats( id ) {
    yield put( setLoading( true ) );

    try {
        const { data } = yield call( Fetch, `/subscriptions/${id}/stats`, { method: 'GET' });
        yield put( setSubscriptionStats( data ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const data = !payload.payload ? payload : payload.payload;

    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: data.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( data ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Subscribe
 */
export function* subscribe({ payload }) {
    yield put( setSubscribeLoading( true ) );

    try {
        yield call( Fetch, `/subscriptions/${payload.subscription_id}/plan/update`, {
            method: 'PATCH',
            data: {
                ...payload
            }
        });

        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Your subscription status has been successfully updated.',
            type: 'success'
        });

        yield put( setSubscribeModal( false ) );
        yield call( reinitializePage );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setSubscribeLoading( false ) );
    }
}

/**
 * Request New SetupIntent
 */
export function* requestSetupIntent() {
    yield put( requestSetupIntentLoading( true ) );
    try {
        const setupIntent = yield call( Fetch, '/subscription-payment/payment-method/setup-intent', {
            method: 'POST',
            data: {}
        });

        yield put( setClientSecret( setupIntent.data.client_secret ) );
        yield put( setSubscribeModal( true ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( requestSetupIntentLoading( false ) );
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getSubscriptionSummary );
}

/**
 * Watcher for GET_SUBSCRIPTION_SUMMARY
 *
 */
export function* watchForGetSubscriptionSummary() {
    const watcher = yield takeEvery( GET_SUBSCRIPTION_SUMMARY, getSubscriptionSummary );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SUBSCRIBE
 */
export function* watchForSubscribe() {
    const watcher = yield takeLatest( SUBSCRIBE, subscribe );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REQUEST_SETUP_INTENT
 */
export function* watchForRequestSetupIntent() {
    const watcher = yield takeLatest( REQUEST_SETUP_INTENT, requestSetupIntent );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFY_USER
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFY_USER, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetSubscriptionSummary,
    watchForSubscribe,
    watchForReinitializePage,
    watchForRequestSetupIntent,
    watchForNotifyUser
];
