/*
 *
 * Subscription constants
 *
 */
const namespace = 'app/Subscriptions';

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_ACCOUNT_DETAILS = `${namespace}/SET_ACCOUNT_DETAILS`;
export const GET_SUBSCRIPTION_SUMMARY = `${namespace}/GET_SUBSCRIPTION_SUMMARY`;
export const SET_SUBSCRIPTION_SUMMARY = `${namespace}/SET_SUBSCRIPTION_SUMMARY`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const GET_SUBSCRIPTION_STATS = `${namespace}/GET_SUBSCRIPTION_STATS`;
export const SET_SUBSCRIPTION_STATS = `${namespace}/SET_SUBSCRIPTION_STATS`;
export const GET_LICENSE_UNITS = `${namespace}/GET_LICENSE_UNITS`;

export const SUBSCRIBE = `${namespace}/SUBSCRIBE`;
export const SET_SUBSCRIBE_LOADING = `${namespace}/SET_SUBSCRIBE_LOADING`;
export const SET_SUBSCRIBE_MODAL = `${namespace}/SET_SUBSCRIBE_MODAL`;
export const REQUEST_SETUP_INTENT = `${namespace}/REQUEST_SETUP_INTENT`;
export const REQUEST_SETUP_INTENT_LOADING = `${namespace}/REQUEST_SETUP_INTENT_LOADING`;
export const SET_CLIENT_SECRET = `${namespace}/SET_CLIENT_SECRET`;
export const SET_CARD_MODAL = `${namespace}/SET_CARD_MODAL`;
export const SET_PAYMENT_METHOD = `${namespace}/SET_PAYMENT_METHOD`;

export const SET_BILLING_INFORMATION = `${namespace}/SET_BILLING_INFORMATION`;

export const NOTIFY_USER = `${namespace}/NOTIFY_USER`;

export const BILLING_INFORMATION_FORM = [
    [

        {
            id: 'email',
            label: 'Email',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'phone',
            label: 'Phone',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'address_1',
            label: 'Address Line 1',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'address_2',
            label: 'Address Line 2',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'city',
            label: 'City',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'zip_code',
            label: 'Zip Code',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'state',
            label: 'State/Province',
            field_params: {
                validations: {
                    required: true
                }
            }
        },
        {
            id: 'country',
            label: 'Country',
            disabled: true,
            field_params: {
                validations: {
                    required: true
                }
            }
        }
    ]
];
