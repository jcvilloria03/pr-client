/* eslint-disable array-callback-return */
/* eslint-disable no-param-reassign */
/* eslint-disable react/no-did-mount-set-state */
import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment/moment';
import get from 'lodash/get';

import { isAuthorized } from 'utils/Authorization';
import {
    DATE_FORMATS
} from 'utils/constants';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';
import { auth } from 'utils/AuthService';

import { H2, H3 } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import A from 'components/A';
import Button from 'components/Button';
import SalConfirm from 'components/SalConfirm';

import SubHeader from 'containers/SubHeader';

import { STRIPE_PUB_KEY } from '../../../constants';

import * as actions from './actions';
import {
    PageWrapper,
    LoadingStyles,
    SectionWrapper,
    ConfirmBodyWrapperStyle
} from './styles';
import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSubscriptionSummary,
    makeSelectSubscriptionStats,
    makeSelectAccountDetails,
    makeSelectSuscribeLoading,
    makeSelectSubscribeModal,
    makeSelectPaymentMethod,
    makeSelectBillingInformation
} from './selectors';

// TODO: Clean unused forms

/**
 * Subscriptions Component
 */
export class Subscriptions extends React.PureComponent {
    static propTypes = {
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        accountDetails: React.PropTypes.object,
        getSubscriptionDetails: React.PropTypes.func,
        summary: React.PropTypes.object,
        stats: React.PropTypes.object,
        products: React.PropTypes.array,
        subscribe: React.PropTypes.func,
        subscribeLoading: React.PropTypes.bool,
        subscribeModal: React.PropTypes.bool,
        billingInformation: React.PropTypes.object
    }

    constructor( props ) {
        super( props );

        this.state = {
            stripe: null,
            subscription: null,
            license_to_purchase: {},
            salpayViewPermission: false,
            hasPaymentMethod: false,
            showConfirmModal: false
        };
    }

    componentWillMount() {
        isAuthorized([
            'view.account',
            'view.salpay_integration'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.account' ];

            if ( authorized ) {
                this.props.getSubscriptionDetails();
                this.setState({ salpayViewPermission: authorization[ 'view.salpay_integration' ] });
            } else if ( subscriptionService.isExpired() && ![ 'Owner', 'Super Admin' ].includes( auth.getUser().role ) ) {
                localStorage.clear();
            } else {
                window.location.href = '/control-panel/companies';
            }
        });
    }

    componentDidMount() {
        if ( window.Stripe ) {
            this.setState({ stripe: window.Stripe( STRIPE_PUB_KEY ) });
        } else {
            document.querySelector( '#stripe-js' ).addEventListener( 'load', () => {
                // Create Stripe instance once Stripe.js loads
                this.setState({ stripe: window.Stripe( STRIPE_PUB_KEY ) });
            });
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( Object.keys( nextProps.paymentMethod ).length > 0 ) {
            this.setState({ hasPaymentMethod: true });
        }

        if ( nextProps.subscribeModal === true && !this.props.subscribeModal ) {
            this.subscriptionModal.toggle();
        }

        if ( nextProps.subscribeModal === false ) {
            this.subscriptionModal && this.subscriptionModal.close();
            this.setState({ showConfirmModal: false });
        }
    }

    getBillingDate() {
        const endDate = get( this.props.summary, 'end_date', null );
        const settings = get( this.props.accountDetails, 'subscriptions.0.subscription_settings', null );
        const nextBillingDate = get( settings, 'next_billing_date', null );

        if ( nextBillingDate && moment( nextBillingDate ).diff( moment( endDate ).add( 1, 'day' ) ) >= 0 ) {
            return moment( nextBillingDate ).format( 'dddd, MMM D, YYYY' );
        }

        return moment( endDate ).add( 1, 'day' ).format( 'dddd, MMM D, YYYY' );
    }

    navigateTo = ( to = '/' ) => ( event ) => {
        event.preventDefault();
        browserHistory.push( `/control-panel/subscriptions${to}`, true );
    }

    initializeSubscribe() {
        this.subscriptionModal.toggle();
    }

    subscribe = () => {
        const accountSubscription = this.props.accountDetails.subscriptions[ 0 ];

        accountSubscription
            .subscription_licenses
            .map( ( item ) => {
                item.product_id = item.product.id;
                delete item.product;
            });

        this.props.subscribe({
            subscription_id: accountSubscription.id,
            start_date: this.props.summary.start_date,
            end_date: moment( this.props.summary.end_date ).add( 30, 'days' ).format( 'YYYY-MM-DD' ),
            plan_id: accountSubscription.plan_id,
            convert_to_paid: 1,
            licenses: accountSubscription.subscription_licenses
        });
    }

    renderPaidContent() {
        const {
            product,
            start_date: startDate,
            end_date: endDate
        } = this.props.summary;

        if ( auth.isExpired() ) {
            return (
                <section className="content">
                    <header className="heading">
                        <H3>Your Subscription has been suspended</H3>
                        <p>
                            Please&nbsp;
                            <A
                                href="/control-panel/subscriptions/invoices"
                                onClick={ this.navigateTo( '/invoices' ) }
                            >
                                pay
                            </A>
                            &nbsp;any pending invoices to be re-activated.
                            <br /><br />
                            You can contact us if you want to re-open your account.
                        </p>
                    </header>
                    <SectionWrapper>
                        <div className="summary">
                            <i className="fa fa-user fa-2x" />
                            <span>
                                Primary owner email is <strong>{ `${this.props.accountDetails.email}` }</strong>
                            </span>
                        </div>
                    </SectionWrapper>
                </section>
            );
        }

        const renderSummary = Object.keys( this.props.stats ).length > 0 && product && startDate && !auth.isExpired();

        return (
            <section className="content">
                <header className="heading">
                    <H3>Subscription Summary</H3>
                </header>

                { renderSummary && (
                    <SectionWrapper>
                        <div className="summary">
                            <i className="fa fa-user fa-2x" />
                            <span>
                                Primary owner email is <strong>{ `${this.props.accountDetails.email}` }</strong>
                            </span>
                        </div>
                        <div className="summary">
                            <i className="fa fa-users fa-2x" />
                            <span>You currently have&nbsp;
                                <strong>
                                    { `${this.props.stats.total_used} ${this.props.stats.total_used.length > 1 ? 'licenses' : 'license'} in ${product.name}` }
                                </strong>
                            </span>

                            &nbsp;
                            <Button
                                id="cancel-button"
                                label="Add / Remove Licenses"
                                type="action"
                                onClick={ () => browserHistory.push( '/control-panel/subscriptions/licenses', true ) }
                            />
                        </div>
                        <div className="summary">
                            <i className="fa fa-calendar fa-2x" />
                            <span>Billing date is <strong>{ this.getBillingDate() }</strong></span>
                        </div>
                        <div className="summary">
                            <i className="fa fa-hourglass fa-2x" />
                            <span>Billing period is <strong>{ `${moment( startDate ).format( 'MMM D' )} - ${moment( endDate ).format( 'MMM D' )}` }</strong></span>
                        </div>
                        <div className="summary">
                            <i className="fa fa-credit-card fa-2x" />
                            <span>Your current setup would cost <strong>{ this.props.stats.current_setup_price }</strong></span>
                        </div>
                    </SectionWrapper>
                ) }
            </section>
        );
    }

    renderFreeTrialContent() {
        const {
            product,
            start_date: startDate,
            end_date: endDate,
            days_left: daysLeft,
            is_expired: isExpired
        } = this.props.summary;

        const hasBillingInformation = this.props.billingInformation && this.props.billingInformation.account_name;

        if ( isExpired ) {
            return (
                <section className="content">
                    <header className="heading">
                        <H3>Your free trial has come to an end.</H3>
                        <p>
                            We hope you experienced how Salarium can benefit your organization with time, payroll or both.
                            <br /><br />
                            You can now choose plans and licenses on the&nbsp;
                            <A
                                href="/control-panel/subscriptions/licenses"
                                onClick={ this.navigateTo( '/licenses' ) }
                            >
                                Licenses Page.
                            </A>
                            <br /><br />
                            Place your billing information in the&nbsp;
                            <A
                                href="/control-panel/subscriptions/billing-information"
                                onClick={ this.navigateTo( '/billing-information' ) }
                            >
                                Billing Page.
                            </A>
                            <br /><br />
                            For any assistance please don't hesitate to reach out to support.
                        </p>
                    </header>
                    <SectionWrapper>
                        <div className="summary">
                            <i className="fa fa-user fa-2x" />
                            <span>
                                Primary owner email: <strong>{ `${this.props.accountDetails.email}` }</strong>
                                <br />
                                Free trial start date: <strong>{ `${startDate}` }</strong>
                                <br />
                                Total number of active licenses: <strong>{ `${this.props.stats.total_remaining}` }</strong>
                                <br />
                                Total number of used licenses: <strong>{ `${this.props.stats.total_used}` }</strong>
                            </span>
                        </div>
                    </SectionWrapper>
                </section>
            );
        }

        const renderSummary = Object.keys( this.props.stats ).length > 0 && product && endDate;

        return (
            <section className="content">
                <header className="heading">
                    <H3>Subscription Summary</H3>
                    <p>We hope you enjoy using Salarium.</p>
                </header>
                { renderSummary && (
                    <SectionWrapper>
                        <div className="summary">
                            <i className="fa fa-user fa-2x" />
                            <span>
                                Primary owner email is <strong>{ `${this.props.accountDetails.email}` }</strong>
                            </span>
                        </div>
                        <div className="summary">
                            <i className="fa fa-user fa-2x" />
                            <span>You currently used&nbsp;
                                <strong>
                                    { `${this.props.stats.total_used} ${this.props.stats.total_used.length > 1 ? 'licenses' : 'license'} in ${product.name}` }
                                </strong>
                            </span>

                            &nbsp;
                            <Button
                                id="cancel-button"
                                label="Add / Remove Licenses"
                                type="action"
                                onClick={ () => browserHistory.push( '/control-panel/subscriptions/licenses', true ) }
                            />
                        </div>
                        <div className="summary">
                            <i className="fa fa-calendar fa-2x" />
                            <span>Free trial will expire on <strong>{ moment( endDate ).add( 2, 'days' ).format( DATE_FORMATS.DISPLAY ) }</strong></span>

                            &nbsp;
                            <Button
                                id="subscribe-button"
                                disabled={ !renderSummary || this.props.subscribeLoading || !Object.keys( this.props.billingInformation ).length }
                                label="Subscribe Now"
                                type="action"
                                onClick={ () => this.setState({ showConfirmModal: true }) }
                            />

                            <SalConfirm
                                onConfirm={ () => {
                                    hasBillingInformation
                                    ? this.subscribe()
                                    : browserHistory.push( '/control-panel/subscriptions/billing-information', true );
                                } }
                                confirmText={ hasBillingInformation ? 'Confirm' : 'Go to billing information form' }
                                onClose={ () => {
                                    this.setState({ showConfirmModal: false });
                                } }
                                body={
                                    <ConfirmBodyWrapperStyle>
                                        <div>
                                            { hasBillingInformation ? (
                                                <div>
                                                You are about to change your subscription from <b>Free</b> to <b>Paid</b> plan.
                                                <br />
                                                You will receive an invoice to be paid within the day.
                                                <br /><br />
                                                Confirm your action.
                                            </div>
                                        ) : (
                                            <div>
                                                Please fill in the billing information first before you can subscribe to a paid subscription.
                                            </div>
                                        ) }
                                        </div>
                                    </ConfirmBodyWrapperStyle>
                                }
                                title="Subscription Confirmation"
                                visible={ this.state.showConfirmModal }
                            />
                        </div>
                    </SectionWrapper>
                ) }
            </section>
        );
    }

    /**
     * Component Render Method
     */
    render() {
        const { salpayViewPermission } = this.state;

        return (
            <main>
                <Helmet
                    title="Subscriptions"
                    meta={ [
                        { name: 'Subscriptions', content: 'Subscription Summary' }
                    ] }
                />
                <SubHeader
                    items={ subscriptionService.getTabs() }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 7000 }
                    type={ this.props.notification.type }
                />
                <PageWrapper>
                    <Sidebar
                        items={ getControlPanelSidebarLinks({
                            isExpired: this.props.summary.is_expired,
                            accountViewPermission: true,
                            salpayViewPermission,
                            isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products )
                        }) }
                    />
                    { this.props.loading && (
                        <div className="loader">
                            <LoadingStyles>
                                <H2>Loading Subscriptions.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                    ) }

                    { !this.props.loading && Object.keys( this.props.summary ).length > 0 && (
                        this.props.summary.is_trial
                            ? this.renderFreeTrialContent()
                            : this.renderPaidContent()
                    ) }
                </PageWrapper>
            </main>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    summary: makeSelectSubscriptionSummary(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    stats: makeSelectSubscriptionStats(),
    accountDetails: makeSelectAccountDetails(),
    subscribeLoading: makeSelectSuscribeLoading(),
    subscribeModal: makeSelectSubscribeModal(),
    paymentMethod: makeSelectPaymentMethod(),
    billingInformation: makeSelectBillingInformation()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Subscriptions );

