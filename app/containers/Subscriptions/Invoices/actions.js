
import { RESET_STORE } from 'containers/App/constants';

import {
    INITIALIZE_DATA,
    SET_LOADING,
    SET_NOTIFICATION,
    SET_FORM_OPTIONS,
    SET_ACCOUNT_DETAILS,
    SET_INVOICES,
    SET_BILLING_INFORMATION_STATE,
    SET_BILLING_INFORMATION,
    SAVE_BILLING_INFORMATION
} from './constants';

/**
 * Initialize page data
 * @returns {Object} action
 */
export function initializeData() {
    return {
        type: INITIALIZE_DATA
    };
}

/**
 * Set loading state of the page
 * @param {Boolean} payload - Loading state
 * @returns {Object} action
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Sets form options
 * @param {Object} payload - Form options
 * @returns {Object} action
 */
export function setFormOptions( payload ) {
    return {
        type: SET_FORM_OPTIONS,
        payload
    };
}

/**
 * Sets account details
 * @param {Object} payload - Account details
 * @returns {Object} action
 */
export function setAccountDetails( payload ) {
    return {
        type: SET_ACCOUNT_DETAILS,
        payload
    };
}

/**
 * Sets invoices
 * @param {Object} payload - List of invoices
 * @returns {Object} action
 */
export function setInvoices( payload ) {
    return {
        type: SET_INVOICES,
        payload
    };
}

/**
 * Set state of the page billing info
 * @param {String} payload - Billing information state
 * @returns {Object} action
 */
export function setBillingInformationState( payload ) {
    return {
        type: SET_BILLING_INFORMATION_STATE,
        payload
    };
}

/**
 * Sets billing information
 * @param {Object} payload - Billing information
 * @returns {Object} action
 */
export function setBillingInformation( payload ) {
    return {
        type: SET_BILLING_INFORMATION,
        payload
    };
}

/**
 * submit a new employee
 */
export function saveBillingInformation( payload ) {
    return {
        type: SAVE_BILLING_INFORMATION,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
