import { fromJS } from 'immutable';
import { subscriptionService } from 'utils/SubscriptionService';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_NOTIFICATION,
    SET_ACCOUNT_DETAILS,
    SET_FORM_OPTIONS,
    SET_INVOICES,
    SET_BILLING_INFORMATION_STATE,
    SET_BILLING_INFORMATION,
    BILLING_INFORMATION_VIEW
} from './constants';

const initialState = fromJS({
    loading: true,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    form_options: {},
    account_details: {
        is_trial: true,
        main_plan: null
    },
    invoices: [],
    billing_information: {},
    billing_information_state: BILLING_INFORMATION_VIEW
});

/**
 * Parses account details
 * @param {Object} data - Account details
 * @returns {Object}
 */
function parseAccountDetails( data ) {
    const [subscription] = data.subscriptions;
    subscriptionService.updateSubscription( subscription );

    return {
        ...data,
        is_trial: subscription.is_trial,
        main_plan: subscription.main_product_id
    };
}

/**
 *
 * License reducer
 *
 */
function invoiceReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ACCOUNT_DETAILS:
            return state.set( 'account_details', fromJS( parseAccountDetails( action.payload ) ) );
        case SET_FORM_OPTIONS:
            return state.set( 'form_options', fromJS( action.payload ) );
        case SET_INVOICES:
            return state.set( 'invoices', fromJS( action.payload ) );
        case SET_BILLING_INFORMATION_STATE:
            return state.set( 'billing_information_state', action.payload );
        case SET_BILLING_INFORMATION:
            return state.set( 'billing_information', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default invoiceReducer;
