import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import capitalize from 'lodash/capitalize';

import { isAuthorized } from 'utils/Authorization';
import { formatDate, formatCurrency } from 'utils/functions';
import {
    DATE_FORMATS
} from 'utils/constants';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';

import { H2, H3, H5 } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import Table from 'components/Table';
import Button from 'components/Button';

import SubHeader from 'containers/SubHeader';

import {
    initializeData
} from './actions';
import {
    SectionWrapper,
    LoadingStyles
} from './styles';
import {
    makeSelectLoading,
    makeSelectInvoices,
    makeSelectNotification,
    makeSelectAccountDetails
} from './selectors';

import {

} from './constants';

/**
 * Licenses Component
 */
export class Invoices extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        initializeData: React.PropTypes.func,
        accountDetails: React.PropTypes.object,
        invoices: React.PropTypes.array
    }

    constructor( props ) {
        super( props );

        this.state = {
        };
    }

    componentWillMount() {
        isAuthorized([
            'view.account',
            'view.salpay_integration'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.account' ];

            if ( authorized ) {
                this.setState({ salpay_view_permission: authorization[ 'view.salpay_integration' ] });
            } else {
                window.location.href = '/control-panel/companies';
            }
        });
    }

    componentDidMount() {
        this.props.initializeData();
    }

    openInvoiceDetails( invoiceId ) {
        browserHistory.push( `/control-panel/subscriptions/invoices/${invoiceId}`, true );
    }

    /**
     * Component Render Method
     */
    render() {
        const tableColumns = [
            {
                header: 'Invoice Number',
                id: 'invoice_number',
                accessor: ( row ) => row.invoice_number
            },
            {
                header: 'Issue Date',
                id: 'sent_date',
                accessor: ( row ) => formatDate( row.sent_date, DATE_FORMATS.DISPLAY )
            },
            {
                header: 'Due Date',
                id: 'due_date',
                accessor: ( row ) => formatDate( row.due_date, DATE_FORMATS.DISPLAY )
            },
            {
                header: 'Description',
                id: 'description',
                accessor: ( row ) => row.description || 'N/A'
            },
            {
                header: 'Payment Date',
                id: 'payment_date',
                accessor: ( row ) => formatDate( row.payment_date, DATE_FORMATS.DISPLAY ) || 'N/A'
            },
            {
                header: 'Amount',
                id: 'amount',
                accessor: ( row ) => formatCurrency( row.total )
            },
            {
                header: 'Currency',
                id: 'currency',
                accessor: ( row ) => row.currency
            },
            {
                header: 'Status',
                id: 'status',
                accessor: ( row ) => capitalize( row.status )
            },
            {
                header: 'Action',
                sortable: false,
                filterable: false,
                render: ( row ) => (
                    <Button
                        alt
                        label="View Invoice"
                        type="action"
                        size="small"
                        onClick={ () => this.openInvoiceDetails( row.row.id ) }
                    />
                )
            }
        ];

        return (
            <div>
                <Helmet
                    title="Invoices"
                    meta={ [
                        { name: 'description', content: 'Invoices Details' }
                    ] }
                />

                <SubHeader
                    items={ subscriptionService.getTabs() }
                />

                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        accountViewPermission: true,
                        salpayViewPermission: this.state.salpay_view_permission,
                        isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products )
                    }) }
                />

                <Container>
                    <SectionWrapper>
                        { this.props.loading && (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading Invoices.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        ) || (
                            <div className="content">
                                <div className="title">
                                    <H5>Invoice History</H5>
                                </div>

                                <Table
                                    data={ this.props.invoices }
                                    columns={ tableColumns }
                                    loading={ this.props.loading }
                                    pageSize={ this.props.invoices.length }
                                    defaultSorting={ [
                                        {
                                            id: 'invoice_number',
                                            desc: true
                                        }
                                    ] }
                                    selectable
                                />
                            </div>
                        ) }
                    </SectionWrapper>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    invoices: makeSelectInvoices(),
    notification: makeSelectNotification(),
    accountDetails: makeSelectAccountDetails()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        {
            initializeData
        },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Invoices );
