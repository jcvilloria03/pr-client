import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';

import { Fetch } from 'utils/request';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    INITIALIZE_DATA,
    GET_PAYNAMICS_PAYMENT_DETAILS,
    PAYPAL_CREATE_ORDER,
    PAYPAL_CAPTURE_ORDER,
    REQUEST_PAYMENT_INTENT,
    NOTIFY_USER
} from './constants';

import {
    setLoading,
    setNotification,
    setAccountDetails,
    setInvoiceDetails,
    setPaynamicsPaymentDetails,
    setPayPalOrder,
    setPaymentDetailsLoading,
    setBilledUsers,
    setClientSecret,
    setPaymentIntentLoading,
    setStripePaymentModal
} from './actions';

/**
 * Initialize data for page
 */
export function* initializeData({ payload: { invoiceId }}) {
    yield put( setLoading( true ) );

    try {
        const [ accountDetails, invoiceDetails, billedUsers ] = yield [
            call( Fetch, '/account', { method: 'GET' }),
            call( Fetch, `/subscriptions/invoices/${invoiceId}`, { method: 'GET' }),
            call( Fetch, `/subscriptions/invoices/${invoiceId}/billed_users`, { method: 'GET' })
        ];

        yield [
            put( setAccountDetails( accountDetails ) ),
            put( setInvoiceDetails( invoiceDetails.data ) ),
            put( setBilledUsers( billedUsers.data.billed_users ) )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const data = !payload.payload ? payload : payload.payload;

    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: data.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( data ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Get Paynamics payment details
 * @param {Int} payload.invoice_id
 */
export function* getPaynamicsPaymentDetails( action ) {
    yield put( setPaynamicsPaymentDetails({ }) );
    yield put( setPaymentDetailsLoading( true ) );

    try {
        const paynamicsPaymentDetails = yield call(
            Fetch,
            `/subscriptions/invoices/${action.payload.invoice_id}/payments/generate_paynamics`, {
                method: 'GET'
            }
        );

        yield put( setPaynamicsPaymentDetails( paynamicsPaymentDetails.data ) );
    } catch ( error ) {
        yield call( notifyError, error );
        yield put( setPaymentDetailsLoading( false ) );
    }
}

/**
 * Create PayPal order
 * @param {String} payload.invoiceId
 * @param {String} payload.orderId
 */
export function* paypalCreateOrder({ payload: { invoiceId }}) {
    yield put( setPaymentDetailsLoading( true ) );

    try {
        const order = yield call( subscriptionService.createPayPalOrder, invoiceId );

        yield put( setPayPalOrder( order ) );
    } catch ( error ) {
        yield call( notifyError, error );
        yield put( setPaymentDetailsLoading( false ) );
    }
}

/**
 * Capture PayPal order
 * @param {String} payload.invoiceId
 * @param {String} payload.orderId
 */
export function* paypalCaptureOrder({ payload: { invoiceId, orderId }}) {
    yield put( setPaymentDetailsLoading( true ) );

    try {
        yield call( subscriptionService.capturePayPalOrder, invoiceId, orderId );

        browserHistory.push( '/control-panel/subscriptions/invoices', true );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setPaymentDetailsLoading( false ) );
    }
}

/**
 * Request Payment Intent
 * @param {String} payload.invoiceId
 */
export function* requestPaymentIntent({ payload: { invoiceId }}) {
    yield put( setPaymentIntentLoading( true ) );

    try {
        const paymentIntent = yield call(
            Fetch,
            `/subscription-payment/invoice/${invoiceId}/payment-intent`, {
                method: 'POST',
                data: {}
            }
        );

        yield put( setClientSecret( paymentIntent.data.client_secret ) );
        yield put( setPaymentIntentLoading( false ) );
        yield put( setStripePaymentModal( true ) );
    } catch ( error ) {
        const errorMessage = error.response.data ? error.response.data.error.message : error.statusText;

        yield put( setPaymentIntentLoading( false ) );

        yield call( notifyUser, {
            show: true,
            title: 'Error preparing payment client',
            message: errorMessage,
            type: 'error'
        });

        if ( errorMessage === 'Invoice has already been paid.' ) {
            const invoiceDetails = yield call( Fetch, `/subscriptions/invoices/${invoiceId}`, { method: 'GET' });

            yield [
                put( setClientSecret( null ) ),
                put( setInvoiceDetails( invoiceDetails.data ) ),
                put( setStripePaymentModal( false ) )
            ];
        }
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watcher for INITIALIZE_DATA
 *
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_PAYNAMICS_PAYMENT_DETAILS
 */
export function* watchForGetPaynamicsPaymentDetails() {
    const watcher = yield takeLatest( GET_PAYNAMICS_PAYMENT_DETAILS, getPaynamicsPaymentDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for PAYPAL_CREATE_ORDER
 */
export function* watchForPayPalCreateOrder() {
    const watcher = yield takeLatest( PAYPAL_CREATE_ORDER, paypalCreateOrder );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for PAYPAL_CAPTURE_ORDER
 */
export function* watchForPayPalCaptureOrder() {
    const watcher = yield takeLatest( PAYPAL_CAPTURE_ORDER, paypalCaptureOrder );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REQUEST_PAYMENT_INTENT
 */
export function* watchForRequestPaymentIntent() {
    const watcher = yield takeLatest( REQUEST_PAYMENT_INTENT, requestPaymentIntent );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFY_USER
 */
export function* watchForNotifyUser() {
    const watcher = yield takeLatest( NOTIFY_USER, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForReinitializePage,
    watchForGetPaynamicsPaymentDetails,
    watchForPayPalCreateOrder,
    watchForPayPalCaptureOrder,
    watchForRequestPaymentIntent,
    watchForNotifyUser
];
