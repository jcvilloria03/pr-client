import React from 'react';

/**
 * Invoice Details - Paynamics Payment Form Compontnent
 */
export class PaynamicsPaymentForm extends React.PureComponent {
    static propTypes = {
        method: React.PropTypes.string,
        action: React.PropTypes.string,
        paymentrequest: React.PropTypes.string
    }

    /**
     * Component Constructor
     */
    constructor( props ) {
        super( props );

        this.submit = this.submit.bind( this );
    }

    submit() {
        this.form.submit();
    }

    render() {
        return (
            <form
                id="invoice-paynamics-payment-request"
                action={ this.props.action }
                method={ this.props.method }
                ref={ ( ref ) => { this.form = ref; } }
            >
                <input
                    type="hidden"
                    name="paymentrequest"
                    value={ this.props.paymentrequest }
                />
            </form>
        );
    }
}

export default PaynamicsPaymentForm;
