import React from 'react';

import { STRIPE_PUB_KEY } from '../../../../../constants';

import {
    SpinnerWrapper
} from './styles';

/**
 * StripePaymentForm Component
 */
class StripePaymentForm extends React.Component {
    static propTypes = {
        clientSecret: React.PropTypes.string,
        amount: React.PropTypes.string
    }

    constructor( props ) {
        super( props );

        this.stripe = null;
        this.elements = null;
        this.paymentElement = null;
        this.isFormComplete = false;
        this.isFormLoading = false;
    }

    componentDidMount() {
        this.showMessage( null );
        this.showButtonText( false );
        this.handleSubmitButtonDisableState( true );

        if ( window.Stripe ) {
            this.stripe = window.Stripe( STRIPE_PUB_KEY );
        } else {
            document.querySelector( '#stripe-js' ).addEventListener( 'load', () => {
                // Create Stripe instance once Stripe.js loads
                this.stripe = window.Stripe( STRIPE_PUB_KEY );
            });
        }

        // Create and mount payment element
        if ( this.stripe && this.props.clientSecret ) {
            this.elements = this.stripe.elements({ clientSecret: this.props.clientSecret });
            this.paymentElement = this.elements.create( 'payment' );

            this.paymentElement.mount( '#payment-element' );
            this.paymentElement.on( 'change', this.paymentElementOnchange );
        }
    }

    paymentElementOnchange = ( ev ) => {
        if ( 'complete' in ev ) {
            this.isFormComplete = ev.complete;
        }

        this.showButtonText( true );
        this.handleSubmitButtonDisableState( !this.isFormComplete );
        this.showMessage( null );
    }

    handleSubmitButtonDisableState( state ) {
        document.getElementById( 'stripe-submit-button' ).disabled = state;
    }

    showButtonText( value ) {
        if ( value ) {
            document.getElementById( 'text' ).style.display = 'block';
            document.getElementById( 'spinner' ).style.display = 'none';
        } else {
            document.getElementById( 'text' ).style.display = 'none';
            document.getElementById( 'spinner' ).style.display = 'block';
        }
    }

    showMessage( value ) {
        if ( value ) {
            document.getElementById( 'message-prompt' ).style.display = 'block';
            document.getElementById( 'message-prompt' ).style.color = 'red';
            document.getElementById( 'message-prompt' ).style.textAlign = 'center';
            document.getElementById( 'message-prompt' ).style.fontSize = '12px';
            document.getElementById( 'message-prompt' ).style.marginTop = '5px';
            document.getElementById( 'message-prompt' ).innerHTML = `${value}`;
        } else {
            document.getElementById( 'message-prompt' ).style.display = 'none';
            document.getElementById( 'message-prompt' ).innerHTML = '';
        }
    }

    handleSubmit = ( ev ) => {
        ev.preventDefault();

        if ( this.isFormComplete && !this.isFormLoading ) {
            this.isFormLoading = true;
            this.showButtonText( false );
            this.handleSubmitButtonDisableState( this.isFormLoading );

            this.stripe.confirmPayment({
                elements: this.elements,
                confirmParams: {
                    // Return URL where the customer should be redirected after the PaymentIntent is confirmed.
                    return_url: window.location.href
                }
            })
            .then( ( result ) => {
                if ( result.error ) {
                    // Inform the customer that there was an error.
                    this.showMessage( result.error.message );
                } else if ( result.paymentIntent ) {
                    this.showMessage( null );
                }
                this.isFormLoading = false;
                this.showButtonText( true );
                this.handleSubmitButtonDisableState( this.isFormLoading );
            });
        } else {
            this.showMessage( 'Incomplete payment details.' );
        }
    };

    render() {
        const buttonText = this.props.amount ? `Pay ${this.props.amount}` : 'Pay';

        return (
            <div className="card-payment">
                <form onSubmit={ this.handleSubmit }>
                    <div className="row">
                        <div className="col-sm-12">
                            <div id="payment-element"></div>
                        </div>
                        <div id="message-prompt" className="col-sm-12"></div>
                        <div className="col-sm-12">
                            <button id="stripe-submit-button">
                                <span id="text">{ buttonText }</span>
                                <span id="spinner">
                                    <SpinnerWrapper><i className="fa fa-lg fa-circle-o-notch fa-spin fa-fw" /></SpinnerWrapper>
                                </span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default StripePaymentForm;
