import styled from 'styled-components';
export const ActionWrapper = styled.div`
    text-transform: uppercase;
    color: #fff;
    border: 0;
    background: transparent;
    font-size: 13px;
    outline: 0;
    display: flex;
    align-self: stretch;
    padding: 30px 10px;

    button {
        font-size: 13px;
        cursor: pointer;
        margin: auto 10px;
    }
`;
export const IconWrapper = styled.div`
    width: 100px;
    img {
        margin: 10px 20px;
    }
`;
export const Wrapper = styled.div`
    position: fixed;
    z-index: 10000;
    right: 0;
    min-height: 50px;
    margin: auto;
    display: flex;
    align-items: center;
    justify-content: space-between;
    transition: top 500ms cubic-bezier(0, 0, 0.30, 1);
    font-weight: 500;
    text-transform: initial;
    font-size: 14px;
    box-shadow: 0px -3px 20px 0px rgba(0,0,0,0.75);
    padding: 10px;
`;
export const StyledH5 = styled.h5`
    margin-bottom: 10px;
    font-weight: 500;
`;
export const StyledP = styled.p`
    margin: 0;
    work-break: break-word
`;
export const StyledContent = styled.div`
    margin: 15px;
    flex-grow: 1;
    padding-right: 30px;
`;

export const SpinnerWrapper = styled.span`
    padding: 0 20px;
`;
