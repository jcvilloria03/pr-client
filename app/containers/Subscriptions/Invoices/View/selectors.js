import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'invoice_view' );

/**
 * Default selector used by License
 */

const makeSelectLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectAccountDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'account_details' ).toJS()
);

const makeSelectInvoiceDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'invoice_details' ).toJS()
);

const makeSelectPaynamicsPaymentDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'paynamics_payment_details' ).toJS()
);

const makeSelectPaymentDetailsLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'payment_details_loading' )
);

const makeSelectPayPalOrder = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'paypal_order' ).toJS()
);

const makeSelectBilledUsers = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'billed_users' ).toJS()
);

const makeSelectClientSecret = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'client_secret' )
);

const makeSelectPaymentIntentLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'payment_intent_loading' )
);

const makeSelectStripePaymentModal = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'stripe_payment_modal' )
);

export {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectAccountDetails,
    makeSelectInvoiceDetails,
    makeSelectPaynamicsPaymentDetails,
    makeSelectPaymentDetailsLoading,
    makeSelectPayPalOrder,
    makeSelectBilledUsers,
    makeSelectClientSecret,
    makeSelectPaymentIntentLoading,
    makeSelectStripePaymentModal
};
