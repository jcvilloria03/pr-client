/*
 *
 * License constants
 *
 */
const namespace = 'app/Invoices/View';

export const INITIALIZE_DATA = `${namespace}/INITIALIZE_DATA`;
export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_ACCOUNT_DETAILS = `${namespace}/SET_ACCOUNT_DETAILS`;
export const SET_INVOICE_DETAILS = `${namespace}/SET_INVOICE_DETAILS`;
export const SET_PAYNAMICS_PAYMENT_DETAILS = `${namespace}/SET_PAYNAMICS_PAYMENT_DETAILS`;
export const GET_PAYNAMICS_PAYMENT_DETAILS = `${namespace}/GET_PAYNAMICS_PAYMENT_DETAILS`;
export const SET_PAYMENT_DETAILS_LOADING = `${namespace}/SET_PAYMENT_DETAILS_LOADING`;
export const SET_PAYPAL_ORDER = `${namespace}/SET_PAYPAL_ORDER`;
export const PAYPAL_CREATE_ORDER = `${namespace}/PAYPAL_CREATE_ORDER`;
export const PAYPAL_CAPTURE_ORDER = `${namespace}/PAYPAL_CAPTURE_ORDER`;
export const SET_BILLED_USERS = `${namespace}/SET_BILLED_USERS`;
export const VALID_UNPAID_INVOICE_STATUSES = [
    'PENDING',
    'OVERDUE'
];

export const NOTIFY_USER = `${namespace}/NOTIFY_USER`;
export const REQUEST_PAYMENT_INTENT = `${namespace}/REQUEST_PAYMENT_INTENT`;
export const SET_PAYMENT_INTENT_LOADING = `${namespace}/SET_PAYMENT_INTENT_LOADING`;
export const SET_STRIPE_PAYMENT_MODAL = `${namespace}/SET_STRIPE_PAYMENT_MODAL`;
export const SET_CLIENT_SECRET = `${namespace}/SET_CLIENT_SECRET`;
