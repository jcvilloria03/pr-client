import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh;

    .break-word {
        word-break: break-all;
    }

    > .heading {
        padding: 10px 20px;
        position: fixed;
        background: #f0f4f6;
        z-index: 1;
        width: 100%;
    }

    > .content {
        display: flex;
        position: relative;
        height: 100%;
        margin-top: 44px;

        @media (max-width: '960px') {
            > .list-container {
                display: none;
            }
        }
    }
`;

export const SectionWrapper = styled.div`
    margin: 40px auto;
    max-width: 1000px;

    .buttons {
        display: flex;
        align-items: center;
        justify-content: flex-end;

        button {
            width: 120px;
        }
    }

    .invoice-container {
        margin-top: 20px;
        border: solid 1px #adadad;
        border-radius: 5px;
        padding: 60px;

        .logo {
            width: 100%;
        }

        .address {
            margin: 10px 0;
        }

        .details p {
            margin-bottom: 0;
        }

        .billing-details {
            margin-top: 60px;
        }

        .invoice-total {
            margin-top: 20px;
            text-align: right;
        }

        .vat-info {
            margin-top: 10px;
            text-align: right;
        }
    }
`;

export const ModalBody = styled.section`
    .payment-options-container {
        display: flex;
        justify-content: space-evenly;

        .payment-option-content {
            display: flex;
            align-items: center;
            justify-content: center;
            flex: 1 1 0px;
            padding: 1rem;
        }

        .payment-option-button {
            width: 100%;

            img {
                border-style: none;
                width: inherit;
            }
        }
    }

    .card-payment {
        display: flex;
        align-items: center;
        justify-content: center;
        min-height: 100%;

        form {
            width: 100%;
        }

        .mt-15 {
            margin-top: 15px;
        }

        .mt-10 {
            margin-top: 10px;
        }

        .card-section {
            .CardField-cvc > input {
                -webkit-text-security: square !important;
            }

            input,
            .StripeElement {
                display: block;
                margin: 10px 0 10px 0;
                padding: 17px 14px;
                font-size: 14px;
                box-shadow: 0 3px 6px 0 rgba(49,49,93,0.10),
                            0 1px 2px 0 rgba(0,0,0,0.08);
                border: 0;
                outline: 0;
                border-radius: 4px;
                background: white;
                height: 50px;
            }

            input:focus,
            .StripeElement--focus {
                box-shadow: rgba(50, 50, 93, 0.109804) 0px 4px 6px, rgba(0, 0, 0, 0.0784314) 0px 1px 3px;
                -webkit-transition: all 150ms ease;
                transition: all 150ms ease;
            }

            .card {
                padding-top: 11px !important;
            }

            .card-error {
                -ms-flex-order: 3;
                -webkit-order: 3;
                order: 3;
                color: #f21108;
                font-size: 13px;
                padding-left: 2px;
                margin-bottom: 6px;
            }

            ::placeholder {
                color: #808080;
            }

            :-ms-input-placeholder {
                color: #808080;
            }

            ::-ms-input-placeholder {
                color: #808080;
            }
        }

        button {
            white-space: nowrap;
            border: 0;
            outline: 0;
            display: inline-block;
            height: 40px;
            line-height: 40px;
            padding: 0 14px;
            box-shadow: 0 4px 6px rgba(50, 50, 93, .11), 0 1px 3px rgba(0, 0, 0, .08);
            color: #fff;
            border-radius: 4px;
            font-size: 15px;
            font-weight: 600;
            text-transform: uppercase;
            letter-spacing: 0.025em;
            background-color: #83d24b;
            text-decoration: none;
            -webkit-transition: all 150ms ease;
            transition: all 150ms ease;
            margin-top: 20px;
            width: 100%;
        }

        button:hover {
            color: #fff;
            cursor: pointer;
            background-color: #9ddb70;
            transform: translateY(-1px);
            box-shadow: 0 7px 14px rgba(50, 50, 93, .10), 0 3px 6px rgba(0, 0, 0, .08);
        }

        button:focus {
            background: #9ddb70;
        }

        button:active {
            background: #b9e699;
        }

        button:disabled,
        button[disabled] {
            background-color: #ade188;
            color: #ecf6fa;
            border-color: #ade188;
            opacity: 1;
            cursor: not-allowed;
            transform: none;
            box-shadow: 0;
        }

        .confirm-terms {
            font-size: 14px;
            font-weight: 550;
            color: gray;
            margin-top: 10px;
            text-align: center;
        }
    }
`;

export const PaymentButton = styled.button`
    background: #fff;
    box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.5);
    border-radius: 4px;
    cursor: pointer;
    height: 200px;
    width: 100%;
    padding: 3rem;

    &:hover {
        background: #f2f2f2;
    }

    .fa-icon > svg {
        height: 35px;
    }

    h5 {
        margin-top: 25px;
        font-weight: 600;
        font-size: 34px;
    }
`;

export const SpinnerWrapper = styled.span`
    padding: 0 20px;
`;
