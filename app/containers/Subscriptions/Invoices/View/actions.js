
import { RESET_STORE } from 'containers/App/constants';

import {
    INITIALIZE_DATA,
    SET_LOADING,
    SET_NOTIFICATION,
    SET_ACCOUNT_DETAILS,
    SET_INVOICE_DETAILS,
    SET_PAYNAMICS_PAYMENT_DETAILS,
    GET_PAYNAMICS_PAYMENT_DETAILS,
    SET_PAYMENT_DETAILS_LOADING,
    SET_PAYPAL_ORDER,
    PAYPAL_CREATE_ORDER,
    PAYPAL_CAPTURE_ORDER,
    SET_BILLED_USERS,
    REQUEST_PAYMENT_INTENT,
    SET_CLIENT_SECRET,
    NOTIFY_USER,
    SET_PAYMENT_INTENT_LOADING,
    SET_STRIPE_PAYMENT_MODAL
} from './constants';

/**
 * Initialize page data
 * @returns {Object} action
 */
export function initializeData( invoiceId ) {
    return {
        type: INITIALIZE_DATA,
        payload: {
            invoiceId
        }
    };
}

/**
 * Set loading state of the page
 * @param {Boolean} payload - Loading state
 * @returns {Object} action
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets notification to be shown in snackbar
 * @param {Object} payload - Notification details
 * @returns {Object} action
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Sets account details
 * @param {Object} payload - Account details
 * @returns {Object} action
 */
export function setAccountDetails( payload ) {
    return {
        type: SET_ACCOUNT_DETAILS,
        payload
    };
}

/**
 * Sets invoice details
 * @param {Object} payload - Invoice details
 * @returns {Object} action
 */
export function setInvoiceDetails( payload ) {
    return {
        type: SET_INVOICE_DETAILS,
        payload
    };
}

/**
 *
 * @param {Object} payload - Paynamics payment form details
 * @returns {Object} action
 */
export function setPaynamicsPaymentDetails( payload ) {
    return {
        type: SET_PAYNAMICS_PAYMENT_DETAILS,
        payload
    };
}

/**
 *
 * @param {Object} payload - Paynamics payment form details
 * @returns {Object} action
 */
export function getPaynamicsPaymentDetails( payload ) {
    return {
        type: GET_PAYNAMICS_PAYMENT_DETAILS,
        payload
    };
}

/**
 *
 * @param {Object} payload - Payment modal loading state
 * @returns {Object} action
 */
export function setPaymentDetailsLoading( payload ) {
    return {
        type: SET_PAYMENT_DETAILS_LOADING,
        payload
    };
}

/**
 * Set current PayPal order
 * @param {Object} payload - PayPal order details
 * @return {Object} action
 */
export function setPayPalOrder( payload ) {
    return {
        type: SET_PAYPAL_ORDER,
        payload
    };
}

/**
 * PayPal create order
 * @param  {String} invoiceId - Invoice ID
 * @return {Object} action
 */
export function paypalCreateOrder( invoiceId ) {
    return {
        type: PAYPAL_CREATE_ORDER,
        payload: {
            invoiceId
        }
    };
}

/**
 * PayPal capture order
 * @param  {String} invoiceId - Invoice ID
 * @param  {String} orderId - PayPal order ID
 * @return {Object} action
 */
export function paypalCaptureOrder( invoiceId, orderId ) {
    return {
        type: PAYPAL_CAPTURE_ORDER,
        payload: {
            invoiceId,
            orderId
        }
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}

/**
 *
 * @param {Object} payload - Payment modal loading state
 * @returns {Object} action
 */
export function setBilledUsers( payload ) {
    return {
        type: SET_BILLED_USERS,
        payload
    };
}

/**
 *
 * @param {Object} payload - invoice id
 * @returns {Object} action
 */
export function requestPaymentIntent( invoiceId ) {
    return {
        type: REQUEST_PAYMENT_INTENT,
        payload: {
            invoiceId
        }
    };
}

/**
 *
 * @param {Object} payload - payment intent client secret
 * @returns {Object} action
 */
export function setClientSecret( payload ) {
    return {
        type: SET_CLIENT_SECRET,
        payload
    };
}

/**
 * Notify User
 */
export function notifyUser( payload ) {
    return {
        type: NOTIFY_USER,
        payload
    };
}

/**
 * Set loading state of payment intent request
 * @param {Boolean} payload - Loading state
 * @returns {Object} action
 */
export function setPaymentIntentLoading( payload ) {
    return {
        type: SET_PAYMENT_INTENT_LOADING,
        payload
    };
}

/**
 * Set state of payment intent modal
 * @param {Boolean} payload - Loading state
 * @returns {Object} action
 */
export function setStripePaymentModal( payload ) {
    return {
        type: SET_STRIPE_PAYMENT_MODAL,
        payload
    };
}
