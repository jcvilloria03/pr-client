import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import capitalize from 'lodash/capitalize';
import SVG from 'react-inlinesvg';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';

import { isAuthorized } from 'utils/Authorization';
import { formatDate, formatCurrency } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import {
    DATE_FORMATS
} from 'utils/constants';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';

import A from 'components/A';
import Button from 'components/Button';
import Modal from 'components/Modal';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import Table from 'components/Table';
import { H2, H3 } from 'components/Typography';

import SubHeader from 'containers/SubHeader';
import logo from 'assets/logo-salarium-blue.png';
import paynamicsLogo from 'assets/images/paynamics.svg';
import stripeLogo from 'assets/images/stripe.png';

import PaynamicsPaymentForm from './PaynamicsPaymentForm';
import StripePaymentForm from './StripePaymentForm';

import {
    initializeData,
    getPaynamicsPaymentDetails,
    paypalCreateOrder,
    paypalCaptureOrder,
    requestPaymentIntent,
    notifyUser,
    setStripePaymentModal,
    setClientSecret
} from './actions';
import {
    PageWrapper,
    SectionWrapper,
    LoadingStyles,
    ModalBody,
    PaymentButton,
    SpinnerWrapper
} from './styles';
import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectAccountDetails,
    makeSelectInvoiceDetails,
    makeSelectPaynamicsPaymentDetails,
    makeSelectPaymentDetailsLoading,
    makeSelectPayPalOrder,
    makeSelectBilledUsers,
    makeSelectClientSecret,
    makeSelectPaymentIntentLoading,
    makeSelectStripePaymentModal
} from './selectors';
import {
    VALID_UNPAID_INVOICE_STATUSES
} from './constants';

/**
 * Invoice Details Component
 */
export class InvoiceDetails extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.node,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        routeParams: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        accountDetails: React.PropTypes.object,
        invoiceDetails: React.PropTypes.object,
        paynamicsPaymentDetails: React.PropTypes.object,
        getPaynamicsPaymentDetails: React.PropTypes.func,
        paymentDetailsLoading: React.PropTypes.bool,
        paypalCreateOrder: React.PropTypes.func,
        paypalCaptureOrder: React.PropTypes.func,
        clientSecret: React.PropTypes.string,
        requestPaymentIntent: React.PropTypes.func,
        notifyUser: React.PropTypes.func,
        paymentIntentLoading: React.PropTypes.bool,
        setStripePaymentModal: React.PropTypes.func,
        setClientSecret: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        this.state = {
            billing: {},
            capturePending: false
        };
    }

    componentWillMount() {
        isAuthorized([
            'view.account',
            'view.salpay_integration'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.account' ];

            if ( authorized ) {
                this.setState({ salpay_view_permission: authorization[ 'view.salpay_integration' ] });
            } else {
                window.location.href = '/control-panel/companies';
            }
        });
    }

    componentDidMount() {
        this.props.initializeData( this.props.routeParams.id );
        this.checkUrlParams();
    }

    componentWillReceiveProps( nextProps ) {
        this.setState({
            invoiceId: nextProps.invoiceDetails.id,
            billing: nextProps.invoiceDetails.billing_info,
            number: nextProps.invoiceDetails.invoice_number,
            status: nextProps.invoiceDetails.status,
            issueDate: nextProps.invoiceDetails.sent_date,
            dueDate: nextProps.invoiceDetails.due_date,
            periodStartDate: nextProps.invoiceDetails.period_start_date,
            periodEndDate: nextProps.invoiceDetails.period_end_date,
            total: nextProps.invoiceDetails.total,
            currency: nextProps.invoiceDetails.currency,
            items: nextProps.invoiceDetails.invoice_items,
            billedUsers: nextProps.billedUsers,
            vatInfo: nextProps.invoiceDetails.vat_info,
            netTotal: nextProps.invoiceDetails.net_total
        });

        const {
            status,
            token
        } = nextProps.location.query;

        const readyToCapture = nextProps.invoiceDetails.id && status === 'success' && !this.state.capturePending;

        if ( readyToCapture ) {
            this.setState({
                capturePending: true
            });

            this.paymentModal.toggle();

            this.props.paypalCaptureOrder( nextProps.invoiceDetails.id, token );
        }

        if ( nextProps.paypalOrder.checkout_link ) {
            window.location = nextProps.paypalOrder.checkout_link;
        }

        if ( nextProps.stripePaymentModal === true ) {
            this.stripePaymentModal.toggle();
            this.paymentModal.close();
        }

        if ( nextProps.stripePaymentModal === false ) {
            this.stripePaymentModal.close();
        }

        if ( nextProps.invoiceDetails.status === 'PAID' ) {
            this.paymentModal.close();
        }
    }

    componentDidUpdate( prevProps ) {
        const {
            method,
            paynamics_url: paynamicsUrl,
            form_data: formData
        } = this.props.paynamicsPaymentDetails;

        this.props.paynamicsPaymentDetails !== prevProps.paynamicsPaymentDetails
        && !isEmpty( this.props.paynamicsPaymentDetails )
        && method
        && paynamicsUrl
        && formData
        && formData.paymentrequest
        && this.paynamicsForm.submit();
    }

    stripePaymentSuccess = () => {
        const successMessage = 'Invoice status is now being processed.';

        this.props.setStripePaymentModal( false );
        this.props.setClientSecret( null );

        const payload = {
            show: true,
            title: 'Payment Success',
            message: successMessage,
            type: 'success'
        };

        this.props.notifyUser( payload );
        this.setState({ status: 'PAID' });
    }

    checkUrlParams() {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams( queryString );
        const stripePaymentRedirectStatus = urlParams.get( 'redirect_status' );

        if ( stripePaymentRedirectStatus === 'succeeded' ) {
            window.history.pushState({}, document.title, `/control-panel/subscriptions/invoices/${this.props.routeParams.id}` );
            this.stripePaymentSuccess();
        }
    }

    /**
     * Card payment flow
     */
    stripePayment = () => {
        if ( !this.props.clientSecret ) {
            this.props.requestPaymentIntent( this.state.invoiceId );
        } else {
            this.stripePaymentModal.toggle();
            this.paymentModal.close();
        }
    }

    /**
     * Paynamics payment flow
     */
    paynamicsPayment = () => {
        this.props.getPaynamicsPaymentDetails({
            invoice_id: this.props.invoiceDetails.id
        });
    }

    /**
     * PayPal payment flow
     */
    paypalPayment = () => {
        this.props.paypalCreateOrder( this.state.invoiceId );
    }

    showPaymentModal = () => {
        this.paymentModal.toggle();
    }

    downloadInvoice = () => {
        window.open( this.props.invoiceDetails.invoice_pdf );
    }

    /**
     * Stripe payment modal body
     */
    renderPaymentCardModalBody() {
        return (
            <ModalBody>
                <StripePaymentForm
                    clientSecret={ this.props.clientSecret }
                    amount={ `${formatCurrency( this.state.total )} ${this.state.currency} ` }
                />
            </ModalBody>
        );
    }

    /**
     * Payment modal body
     */
    renderPaymentModalBody() {
        const { paymentDetailsLoading } = this.props;

        return (
            <ModalBody>
                { paymentDetailsLoading && (
                    <div className="loader">
                        <LoadingStyles>
                            <H2>Loading Payment Details</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    </div>
                ) || (
                    <div className="payment-options-container">
                        <div className="payment-option-content">
                            <div className="payment-option-button">
                                <PaymentButton
                                    onClick={ this.stripePayment }
                                    disabled={ this.props.paymentIntentLoading }
                                >
                                    {
                                        this.props.paymentIntentLoading
                                            ? <SpinnerWrapper><i className="fa fa-circle-o-notch fa-spin fa-fw" /></SpinnerWrapper>
                                            : <img alt="Stripe Logo" src={ stripeLogo } />
                                    }
                                </PaymentButton>
                            </div>
                        </div>

                        <div className="payment-option-content">
                            <div className="payment-option-button">
                                <PaymentButton
                                    onClick={ this.paynamicsPayment }
                                >
                                    <SVG
                                        src={ paynamicsLogo }
                                    />
                                </PaymentButton>
                            </div>
                        </div>
                    </div>
                ) }
            </ModalBody>
        );
    }

    /**
     * Billing Details Table
     */
    renderBillingDetailsTable() {
        const tableColumns = [
            {
                header: 'Description',
                id: 'description',
                sortable: false,
                width: 200,
                accessor: 'name'
            },
            {
                header: 'Period',
                id: 'period',
                sortable: false,
                width: 300,
                render: () => {
                    const start = formatDate( this.state.periodStartDate, DATE_FORMATS.DISPLAY );
                    const end = formatDate( this.state.periodEndDate, DATE_FORMATS.DISPLAY );

                    return `${start} - ${end}`;
                }
            },
            {
                header: 'Quantity',
                id: 'quantity',
                sortable: false,
                accessor: 'units'
            },
            {
                header: 'Unit Price',
                id: 'unit_price',
                sortable: false,
                render: ({ row }) => {
                    const amount = formatCurrency( row.amount );

                    return `${amount} ${this.state.currency}`;
                }
            },
            {
                header: 'Amount',
                id: 'amount',
                sortable: false,
                render: ({ row }) => {
                    const subtotal = formatCurrency( row.subtotal );

                    return `${subtotal} ${this.state.currency}`;
                }
            }
        ];

        return (
            <Table
                data={ this.state.items || [] }
                columns={ tableColumns }
                loading={ this.props.loading }
            />
        );
    }

    renderBilledUsersTable() {
        const tableColumns = [
            {
                header: 'Employee Name',
                id: 'employee_name',
                accessor: ( row ) => `${row.last_name}, ${row.first_name}`
            },
            {
                header: 'Email',
                id: 'email',
                accessor: ( row ) => row.email
            },
            {
                header: 'Assigned License',
                id: 'license',
                accessor: ( row ) => row.license
            },
            {
                header: 'Employee Status',
                id: 'active',
                accessor: ( row ) => ( row.active ? row.active.toUpperCase() : '' )
            }
        ];

        return (
            <Table
                data={ this.state.billedUsers }
                columns={ tableColumns }
                loading={ this.props.loading }
                pageSize={ this.state.billedUsers.length }
                defaultSorting={ [
                    {
                        id: 'first_name',
                        desc: false
                    }
                ] }
            />
        );
    }

    renderVatInfo() {
        if (isEmpty(this.state.vatInfo) && !this.state.netTotal) {
            return (null)
        }

        return (
            <div className='vat-info'>
                <div><strong>Total Unit Price: </strong>{ formatCurrency( this.state.netTotal ) } { this.state.currency }</div>
                <div><strong>{ this.state.vatInfo.name } ({ this.state.vatInfo.rate }%): </strong> { formatCurrency( this.state.vatInfo.amount ) } { this.state.currency }</div>
            </div>
        )
    }

    /**
     * Component Render Method
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Invoices"
                    meta={ [
                        { name: 'description', content: 'Invoices Details' }
                    ] }
                />

                <SubHeader
                    items={ subscriptionService.getTabs() }
                />

                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        accountViewPermission: true,
                        salpayViewPermission: this.state.salpay_view_permission,
                        isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products )
                    }) }
                />

                <Modal
                    title="Payment Options"
                    size="lg"
                    body={ this.renderPaymentModalBody() }
                    showClose={ !this.props.paymentIntentLoading }
                    keyboard={ !this.props.paymentIntentLoading }
                    buttons={ [] }
                    ref={ ( ref ) => { this.paymentModal = ref; } }
                />

                <Modal
                    title="Payment Form"
                    size="md"
                    body={ this.renderPaymentCardModalBody() }
                    showClose
                    onClose={ () => this.props.setStripePaymentModal( false ) }
                    keyboard={ false }
                    buttons={ [] }
                    ref={ ( ref ) => { this.stripePaymentModal = ref; } }
                />

                <PaynamicsPaymentForm
                    method={ this.props.paynamicsPaymentDetails.method || '' }
                    action={ this.props.paynamicsPaymentDetails.paynamics_url || '' }
                    paymentrequest={ get( this.props.paynamicsPaymentDetails, 'form_data.paymentrequest', '' ) }
                    ref={ ( ref ) => { this.paynamicsForm = ref; } }
                />

                <PageWrapper>
                    <div className="heading">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => { e.preventDefault(); browserHistory.push( '/control-panel/subscriptions/invoices', true ); } }
                            >
                                &#8592; Back to Invoice Summary
                            </A>
                        </Container>
                    </div>

                    <div className="content">
                        <Container>
                            { this.props.loading && (
                                <div className="loader">
                                    <LoadingStyles>
                                        <H2>Loading Invoice Details</H2>
                                        <br />
                                        <H3>Please wait...</H3>
                                    </LoadingStyles>
                                </div>
                            ) || (
                                <SectionWrapper>
                                    <div className="buttons">
                                        <Button
                                            type="action"
                                            label="Download"
                                            onClick={ this.downloadInvoice }
                                            alt
                                        />

                                        { VALID_UNPAID_INVOICE_STATUSES.includes( this.state.status ) && (
                                            <Button
                                                type="action"
                                                label="Pay"
                                                onClick={ this.showPaymentModal }
                                            />
                                        ) }

                                    </div>

                                    <div className="invoice-container">
                                        <div className="row justify-content-between">
                                            <div className="col-sm-5">
                                                <img className="logo" src={ logo } alt="Salarium" />
                                            </div>

                                            <div className="col-sm-3 offset-sm-4">
                                                <div className="address details">
                                                    <p>Salarium Pte Ltd.</p>
                                                    <p>36 Robinson Road #13-01</p>
                                                    <p>Singapore 068777</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-sm-3">
                                                <div className="details">
                                                    <p><strong>Billed To:</strong></p>
                                                    <p>{ this.state.billing.account_name }</p>
                                                    <p>{ this.state.billing.first_name } { this.state.billing.last_name }</p>
                                                    <p>{ this.state.billing.address_1 }</p>

                                                    { this.state.billing.address_2 && ( <p> { this.state.billing.address_2 } </p> ) }
                                                    { this.state.billing.address_3 && ( <p> { this.state.billing.address_3 } </p> ) }

                                                    <p>{ this.state.billing.city }</p>
                                                    <p>{ this.state.billing.state } { this.state.billing.country } { this.state.billing.zip_code }</p>
                                                </div>
                                            </div>

                                            <div className="col-sm-6">
                                                <div className="details">
                                                    <p><strong>Invoice No.:</strong> { this.state.number }</p>
                                                    <p><strong>Invoice Status:</strong> { capitalize( this.state.status ) }</p>
                                                    <p><strong>Issue Date:</strong> { formatDate( this.state.issueDate, DATE_FORMATS.DISPLAY ) }</p>
                                                    <p><strong>Due Date:</strong> { formatDate( this.state.dueDate, DATE_FORMATS.DISPLAY ) }</p>
                                                </div>
                                            </div>

                                            <div className="col-sm-3">
                                                <H3><strong>Invoice Amount</strong></H3>
                                                <H2><strong>{ formatCurrency( this.state.total ) } { this.state.currency }</strong></H2>
                                            </div>
                                        </div>

                                        <p className="billing-details"><strong>Billing Details</strong></p>

                                        { this.renderBillingDetailsTable() }

                                        { this.renderVatInfo() }

                                        <div className="invoice-total">
                                            <H3><strong>Total Amount: { formatCurrency( this.state.total ) } { this.state.currency }</strong></H3>
                                        </div>

                                        <p className="billing-details"><strong>Billed Users</strong></p>

                                        { this.renderBilledUsersTable() }
                                    </div>
                                </SectionWrapper>
                            ) }
                        </Container>
                    </div>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    accountDetails: makeSelectAccountDetails(),
    invoiceDetails: makeSelectInvoiceDetails(),
    paynamicsPaymentDetails: makeSelectPaynamicsPaymentDetails(),
    paymentDetailsLoading: makeSelectPaymentDetailsLoading(),
    paypalOrder: makeSelectPayPalOrder(),
    billedUsers: makeSelectBilledUsers(),
    clientSecret: makeSelectClientSecret(),
    paymentIntentLoading: makeSelectPaymentIntentLoading(),
    stripePaymentModal: makeSelectStripePaymentModal()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        {
            initializeData,
            getPaynamicsPaymentDetails,
            paypalCreateOrder,
            paypalCaptureOrder,
            requestPaymentIntent,
            notifyUser,
            setStripePaymentModal,
            setClientSecret
        },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( InvoiceDetails );
