import { fromJS } from 'immutable';
import { subscriptionService } from 'utils/SubscriptionService';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_NOTIFICATION,
    SET_ACCOUNT_DETAILS,
    SET_INVOICE_DETAILS,
    SET_PAYNAMICS_PAYMENT_DETAILS,
    SET_PAYMENT_DETAILS_LOADING,
    SET_PAYPAL_ORDER,
    SET_BILLED_USERS,
    SET_CLIENT_SECRET,
    SET_PAYMENT_INTENT_LOADING,
    SET_STRIPE_PAYMENT_MODAL
} from './constants';

const initialState = fromJS({
    loading: true,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    account_details: {
        is_trial: true,
        main_plan: null
    },
    invoice_details: {},
    paynamics_payment_details: {},
    payment_details_loading: false,
    paypal_order: {},
    billed_users: [],
    client_secret: null,
    payment_intent_loading: false,
    stripe_payment_modal: false
});

/**
 * Parses account details
 * @param {Object} data - Account details
 * @returns {Object}
 */
function parseAccountDetails( data ) {
    const [subscription] = data.subscriptions;
    subscriptionService.updateSubscription( subscription );

    return {
        ...data,
        is_trial: subscription.is_trial,
        main_plan: subscription.main_product_id
    };
}

/**
 *
 * License reducer
 *
 */
function invoiceReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ACCOUNT_DETAILS:
            return state.set( 'account_details', fromJS( parseAccountDetails( action.payload ) ) );
        case SET_INVOICE_DETAILS:
            return state.set( 'invoice_details', fromJS( action.payload ) );
        case SET_PAYNAMICS_PAYMENT_DETAILS:
            return state.set( 'paynamics_payment_details', fromJS( action.payload ) );
        case SET_PAYMENT_DETAILS_LOADING:
            return state.set( 'payment_details_loading', fromJS( action.payload ) );
        case SET_PAYPAL_ORDER:
            return state.set( 'paypal_order', fromJS( action.payload ) );
        case SET_BILLED_USERS:
            return state.set( 'billed_users', fromJS( action.payload ) );
        case SET_CLIENT_SECRET:
            return state.set( 'client_secret', action.payload );
        case SET_PAYMENT_INTENT_LOADING:
            return state.set( 'payment_intent_loading', action.payload );
        case SET_STRIPE_PAYMENT_MODAL:
            return state.set( 'stripe_payment_modal', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default invoiceReducer;
