import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = () => ( state ) => state.get( 'invoices' );

/**
 * Default selector used by License
 */

const makeSelectLoading = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectAccountDetails = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'account_details' ).toJS()
);

const makeSelectFormOptions = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'form_options' ).toJS()
);

const makeSelectInvoices = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'invoices' ).toJS()
);

const makeSelectBillingInformationState = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'billing_information_state' )
);

const makeSelectBillingInformation = () => createSelector(
    selectPageDomain(),
    ( substate ) => substate.get( 'billing_information' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectAccountDetails,
    makeSelectFormOptions,
    makeSelectInvoices,
    makeSelectBillingInformationState,
    makeSelectBillingInformation
};
