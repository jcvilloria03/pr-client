import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { renderField } from 'utils/form';
import { H5 } from 'components/Typography';
import Button from 'components/Button';

import {
    stripNonNumeric,
    formatMobileNumber
} from 'utils/functions';

import {
    initializeData,
    setBillingInformationState,
    saveBillingInformation
} from '../actions';

import {
    makeSelectFormOptions,
    makeSelectBillingInformationState,
    makeSelectBillingInformation
} from '../selectors';

import {
    BILLING_INFORMATION_EDIT,
    BILLING_INFORMATION_VIEW,
    BILLING_INFORMATION_SAVING,
    BILLING_INFORMATION_FORM
} from '../constants';

/**
 * BillingInformationForm Component
 */
export class BillingInformationForm extends React.PureComponent {
    static propTypes = {
        billing_information_state: React.PropTypes.string,
        billing_information: React.PropTypes.object,
        form_options: React.PropTypes.object,
        setBillingInformationState: React.PropTypes.func,
        saveBillingInformation: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        this.state = {
            data: {
                first_name: null,
                last_name: null,
                account_name: null,
                email: null,
                address_1: null,
                address_2: null,
                address_3: null,
                city: null,
                state: null,
                country: null,
                zip_code: null
            }
        };

        this.fields = {};
    }

    componentWillMount() {
        this.setState({
            data: this.props.billing_information
        });
    }

    getFieldData( field ) {
        switch ( field.id ) {
            case 'country':
                return this.props.form_options.countries;

            default:
                return null;
        }
    }

    getOnChangeHandler( field ) {
        const formatter = ( value ) => {
            // Define formatter function based on the field id
            switch ( field.id ) {
                case 'zip_code':
                    return stripNonNumeric( value );

                case 'phone':
                    return formatMobileNumber( value );

                case 'country':
                    return value.value;

                default:
                    return value;
            }
        };

        return ( value ) => {
            const formatted = formatter( value );

            this.fields[ field.id ].setState({ value: formatted }, () => this.setState( ( prevState ) => ({
                data: {
                    ...prevState.data,
                    [ field.id ]: formatter( value )
                }
            }) ) );
        };
    }

    validateForm() {
        let valid = true;

        BILLING_INFORMATION_FORM.flat().forEach( ( fieldDefinition ) => {
            const field = this.fields[ fieldDefinition.id ];

            switch ( fieldDefinition.type ) {
                case 'select':
                    if ( !field._checkRequire( typeof field.state.value === 'object' && field.state.value !== null ? field.state.value.value : field.state.value ) ) {
                        valid = false;
                    }

                    break;

                default:
                    if ( field._validate( field.state.value ) ) {
                        valid = false;
                    }
            }
        });

        return valid;
    }

    saveBillingInformation() {
        this.validateForm() && this.props.saveBillingInformation( this.state.data );
    }

    renderBillingInformation() {
        return (
            <div className="row">
                { BILLING_INFORMATION_FORM.map( ( group, groupIndex ) => (
                    <div className="col-sm-3" key={ groupIndex }>
                        { group.map( ( field, index ) => this.renderBillingField( field, index ) ) }
                    </div>
                ) ) }
            </div>
        );
    }

    renderBillingField( field, index ) {
        return (
            <div key={ index }>
                <label htmlFor={ `billing-${index}` }>{ field.label }</label>

                <div className="billing-information-field">
                    { [ BILLING_INFORMATION_EDIT, BILLING_INFORMATION_SAVING ].includes( this.props.billing_information_state ) && renderField({
                        id: `billing-${index}`,
                        value: this.state.data[ field.id ] || '',
                        data: this.getFieldData( field ),
                        field_type: field.type,
                        onChange: this.getOnChangeHandler( field ),
                        disabled: this.props.billing_information_state === BILLING_INFORMATION_SAVING,
                        ref: ( ref ) => { this.fields[ field.id ] = ref; },
                        ...field.field_params
                    }) || (
                        <H5>{ this.state.data[ field.id ] || 'N/A' }</H5>
                    ) }
                </div>
            </div>
        );
    }

    render() {
        return (
            <div>
                <div className="title">
                    <H5>Billing Information</H5>

                    { this.props.billing_information_state === BILLING_INFORMATION_EDIT && (
                        <Button
                            type="action"
                            label="Save Billing Information"
                            onClick={ () => this.saveBillingInformation() }
                        />
                    ) }

                    { this.props.billing_information_state === BILLING_INFORMATION_SAVING && (
                        <Button
                            type="action"
                            label="Saving Billing Information"
                            disabled
                        />
                    ) }

                    { this.props.billing_information_state === BILLING_INFORMATION_VIEW && (
                        <Button
                            alt
                            type="action"
                            label="Edit Billing Information"
                            onClick={ () => this.props.setBillingInformationState( BILLING_INFORMATION_EDIT ) }
                        />
                    ) }
                </div>

                <div className="billing-information">
                    { this.renderBillingInformation() }
                </div>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    form_options: makeSelectFormOptions(),
    billing_information_state: makeSelectBillingInformationState(),
    billing_information: makeSelectBillingInformation()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        {
            initializeData,
            setBillingInformationState,
            saveBillingInformation
        },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( BillingInformationForm );
