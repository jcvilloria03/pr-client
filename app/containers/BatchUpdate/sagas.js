import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeLatest, delay } from 'redux-saga';

import { Fetch } from '../../utils/request';

import {
    UPDATE_PERSONAL_INFO,
    UPDATE_PAYROLL_INFO,
    UPDATE_TIME_ATTENDANCE_INFO,
    PERSONAL_INFO_STATUS,
    PERSONAL_INFO_ERRORS,
    TIME_ATTENDANCE_INFO_STATUS,
    TIME_ATTENDANCE_INFO_ERRORS,
    PAYROLL_INFO_STATUS,
    PAYROLL_INFO_ERRORS,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../App/constants';
import { resetStore } from '../App/sagas';

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* uploadPersonalInfo({ payload }) {
    try {
        yield put({
            type: PERSONAL_INFO_STATUS,
            payload: 'validation_queued'
        });

        const data = new FormData();
        data.append( 'company_id', payload.company_id );
        data.append( 'file', payload.file );
        const upload = yield call( Fetch, '/employee/batch_update/personal_info', {
            method: 'POST',
            data
        });
        if ( upload.id ) {
            yield call( checkUpdateStatus, { payload: { step: 'personal', company_id: payload.company_id, job_id: upload.id }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: PERSONAL_INFO_STATUS,
            payload: ''
        });
    }
}

/**
 * Uploads the time & attendance info CSV of employees to add and starts the validation process
 */
export function* uploadTimeAttendanceInfo({ payload }) {
    try {
        yield put({
            type: TIME_ATTENDANCE_INFO_STATUS,
            payload: 'validation_queued'
        });
        const data = new FormData();
        data.append( 'company_id', payload.company_id );
        data.append( 'file', payload.file );
        const upload = yield call( Fetch, '/employee/batch_update/time_attendance_info', {
            method: 'POST',
            data
        });
        if ( upload.id ) {
            yield call( checkUpdateStatus, { payload: { step: 'time_attendance', company_id: payload.company_id, job_id: upload.id }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: TIME_ATTENDANCE_INFO_STATUS,
            payload: ''
        });
    }
}

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* uploadPayrollInfo({ payload }) {
    try {
        yield put({
            type: PAYROLL_INFO_STATUS,
            payload: 'validation_queued'
        });
        const data = new FormData();
        data.append( 'company_id', payload.company_id );
        data.append( 'file', payload.file );
        const upload = yield call( Fetch, '/employee/batch_update/payroll_info', {
            method: 'POST',
            data
        });
        if ( upload.id ) {
            yield call( checkUpdateStatus, { payload: { step: 'payroll', company_id: payload.company_id, job_id: upload.id }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: PAYROLL_INFO_STATUS,
            payload: ''
        });
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkUpdateStatus({ payload }) {
    try {
        const check = yield call( Fetch, `/employee/batch_update/status?company_id=${payload.company_id}&job_id=${payload.job_id}`, {
            method: 'GET'
        });

        // SET STATUS
        yield put({
            type: getStatusByStep( payload.step ),
            payload: check.status
        });

        // UPDATE ERRORS
        if ([ 'validation_failed', 'saved', 'save_failed' ].includes( check.status ) ) {
            yield put({
                type: getErrorsByStep( payload.step ),
                payload: [ 'validation_failed', 'save_failed' ].includes( check.status ) ? check.errors : {}
            });
        } else {
            // wait for 1 sec and check the status again
            yield call( delay, 1000 );
            yield call( checkUpdateStatus, { payload });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Individual exports for testing
 */
export function* watchForPersonalInfoUpload() {
    const watcher = yield takeLatest( UPDATE_PERSONAL_INFO, uploadPersonalInfo );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForTimeAttendanceInfoUpload() {
    const watcher = yield takeLatest( UPDATE_TIME_ATTENDANCE_INFO, uploadTimeAttendanceInfo );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForPayrollInfoUpload() {
    const watcher = yield takeLatest( UPDATE_PAYROLL_INFO, uploadPayrollInfo );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeLatest( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Get status by upload step
 */
function getStatusByStep( step ) {
    switch ( step ) {
        case 'personal':
            return PERSONAL_INFO_STATUS;
        case 'time_attendance':
            return TIME_ATTENDANCE_INFO_STATUS;
        case 'payroll':
            return PAYROLL_INFO_STATUS;
        default:
            return '';
    }
}

/**
 * Get errors by upload step
 */
function getErrorsByStep( step ) {
    switch ( step ) {
        case 'personal':
            return PERSONAL_INFO_ERRORS;
        case 'time_attendance':
            return TIME_ATTENDANCE_INFO_ERRORS;
        case 'payroll':
            return PAYROLL_INFO_ERRORS;
        default:
            return '';
    }
}

// All sagas to be loaded
export default [
    watchForPersonalInfoUpload,
    watchForTimeAttendanceInfoUpload,
    watchForPayrollInfoUpload,
    watchForNotifyUser,
    watchForReinitializePage
];
