import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import SubHeader from '../SubHeader';
import SnackBar from '../../components/SnackBar';
import A from '../../components/A';
import Toggle2 from '../../components/Toggle2';
import Table from '../../components/Table';
import Clipboard from '../../components/Clipboard';
import { H4, H6, P } from '../../components/Typography';
import FileInput from '../../components/FileInput';
import Button from '../../components/Button';

import { browserHistory } from '../../utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../utils/constants';
import { subscriptionService } from '../../utils/SubscriptionService';

import * as batchUpdateActions from './actions';
import {
    PageWrapper,
    StyledLoader
} from './styles';
import {
    makeSelectPersonalInfo,
    makeSelectTimeAttendanceInfo,
    makeSelectPayrollInfo,
    makeSelectNotification,
    makeSelectProductsState
} from './selectors';

/**
 * EmployeeBatchUpload Container
 */
export class EmployeeBatchUpdate extends React.Component {
    static propTypes = {
        updatePersonalInfo: React.PropTypes.func,
        updateTimeAttendanceInfo: React.PropTypes.func,
        updatePayrollInfo: React.PropTypes.func,
        resetPersonalInfo: React.PropTypes.func,
        resetTimeAttendanceInfo: React.PropTypes.func,
        resetPayrollInfo: React.PropTypes.func,
        personal_info: React.PropTypes.shape({
            status: React.PropTypes.string,
            errors: React.PropTypes.object
        }).isRequired,
        time_attendance_info: React.PropTypes.shape({
            status: React.PropTypes.string,
            errors: React.PropTypes.object
        }).isRequired,
        payroll_info: React.PropTypes.shape({
            status: React.PropTypes.string,
            errors: React.PropTypes.object
        }).isRequired,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    }

    /**
     * component's constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            step: 'personal',
            personal_info_clipboard: '',
            time_attendance_info_clipboard: '',
            payroll_info_clipboard: '',
            personal_info: null,
            time_attendance_info: null,
            payroll_info: null
        };

        const { last_active_company_id: companyId } = JSON.parse( localStorage.getItem( 'user' ) );
        this.company_id = companyId;
    }

    /**
     * this displays all existing locations onload
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        if ( nextProps.personal_info.errors !== this.props.personal_info.errors ) {
            let clipboard = '';
            const rows = Object.keys( nextProps.personal_info.errors );
            if ( rows.length ) {
                rows.forEach( ( row ) => {
                    nextProps.personal_info.errors[ row ].forEach( ( error ) => {
                        clipboard += `Row #${Number( row ) + 1}, ${error}\n`;
                    });
                });
            }

            this.setState({ personal_info_clipboard: clipboard });
        }

        if ( nextProps.time_attendance_info.errors !== this.props.time_attendance_info.errors ) {
            let clipboard = '';
            const rows = Object.keys( nextProps.time_attendance_info.errors );
            if ( rows.length ) {
                rows.forEach( ( row ) => {
                    nextProps.time_attendance_info.errors[ row ].forEach( ( error ) => {
                        clipboard += `Row #${Number( row ) + 1}, ${error}\n`;
                    });
                });
            }

            this.setState({ time_attendance_info_clipboard: clipboard });
        }

        if ( nextProps.payroll_info.errors !== this.props.payroll_info.errors ) {
            let clipboard = '';
            const rows = Object.keys( nextProps.payroll_info.errors );
            if ( rows.length ) {
                rows.forEach( ( row ) => {
                    nextProps.payroll_info.errors[ row ].forEach( ( error ) => {
                        clipboard += `Row #${Number( row ) + 1}, ${error}\n`;
                    });
                });
            }

            this.setState({ payroll_info_clipboard: clipboard });
        }

        nextProps.personal_info.status === 'saved' && nextProps.personal_info.status !== this.props.personal_info.status && this.personalInput.setState({ error: false, files: [], label: 'Try dropping some files' }) && this.personalInfoSubmit.setState({ disabled: true });
        nextProps.time_attendance_info.status === 'saved' && nextProps.time_attendance_info.status !== this.props.time_attendance_info.status && this.timeAttendanceInput.setState({ error: false, files: [], label: 'Try dropping some files' }) && this.timeAttendanceInfoSubmit.setState({ disabled: true });
        nextProps.payroll_info.status === 'saved' && nextProps.payroll_info.status !== this.props.payroll_info.status && this.payrollInput.setState({ error: false, files: [], label: 'Try dropping some files' }) && this.payrollInfoSubmit.setState({ disabled: true });
    }

    /**
     * this renders the personal infomation upload section of the add employees
     */
    getPersonalInfoSection() {
        let errorDisplay = [];

        const errorList = this.props.personal_info.errors;
        const keys = Object.keys( errorList );
        if ( keys.length ) {
            const columns = [
                { header: 'INDEX', accessor: 'row', minWidth: 150, sortable: false },
                { header: 'ERROR', accessor: 'error', minWidth: 550, sortable: false }
            ];

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row #{ Number( key ) + 1 }</H4>,
                    error: <ul>{errorList[ key ].map( ( error, index ) => <li key={ index } style={ { margin: '0' } } >{error}</li> )}</ul>
                });
            });

            errorDisplay = (
                <div className="errors">
                    <H4>ERROR OCCURED:</H4>
                    <div style={ { textAlign: 'right' } }><Clipboard value={ this.state.personal_info_clipboard } /></div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                    />
                </div>
            );
        }
        return (
            <div className="personal" style={ { display: this.state.step === 'personal' ? '' : 'none' } } >
                <div className="row">
                    <div className="col-xs-12">
                        <H4>Update Personal Information</H4>
                        <P>Edit your employees&#39; names, addresses, contact numbers, and other personal information.</P>
                    </div>
                </div>
                <div className="row steps">
                    <div className="step col-sm-6">
                        <div className="template">
                            <H6>Step 1:</H6>
                            <p>A batch upload template is available for you to download and fill out.</p>
                            <P><A target="_blank" href="/payroll/guides/employees/batch-upload#personal-guide">You may click here to view the upload guide.</A></P>
                            <A href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/employee_personal_info_template.csv" download>Download Template</A>
                        </div>
                    </div>
                    <div className="step col-sm-6">
                        <div className="upload">
                            <H6>Step 2:</H6>
                            <p>After completely filling out the template, choose and upload it here.</p>
                            <div style={ { display: [ '', 'validation_failed', 'saved', 'save_failed' ].includes( this.props.personal_info.status ) ? 'block' : 'none' } } >
                                <FileInput
                                    accept=".csv"
                                    onDrop={ ( files ) => {
                                        const { acceptedFiles } = files;
                                        this.props.resetPersonalInfo();
                                        this.personalInfoSubmit.setState({ disabled: !acceptedFiles.length > 0 });
                                        this.setState({
                                            personal_info: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null
                                        });
                                    } }
                                    ref={ ( ref ) => { this.personalInput = ref; } }
                                />
                            </div>
                            <div style={ { display: this.props.personal_info.status === 'saved' ? 'block' : 'none' } }>
                                <H4 style={ { color: '#4ABA4A', margin: '0' } }>SAVED <con className="fa fa-check-square-o" /></H4>
                            </div>
                            <div style={ { display: [ 'save_failed', 'validation_failed' ].includes( this.props.personal_info.status ) ? 'block' : 'none' } }>
                                <H4 style={ { color: '#F21108', margin: '0' } }>{ this.props.personal_info.status.split( '_' ).join( ' ' ).toUpperCase() } <i className="fa fa-times-circle-o" /></H4>
                            </div>
                            <div style={ { display: [ '', 'saved', 'save_failed', 'validation_failed' ].includes( this.props.personal_info.status ) ? 'none' : 'block' } } >
                                <StyledLoader className="animation">
                                    <H4 style={ { margin: '0' } }>{ this.props.personal_info.status.split( '_' ).join( ' ' ).toUpperCase() }</H4> <div className="anim3"></div>
                                </StyledLoader>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="action_button">
                    <Button
                        label="Cancel"
                        alt
                        size="large"
                        type="action"
                        onClick={ () => { browserHistory.push( '/employees', true ); } }
                    />
                    <Button
                        disabled={ this.setSubmitButtonStatus( this.state.personal_info, this.props.personal_info.status ) }
                        ref={ ( ref ) => { this.personalInfoSubmit = ref; } }
                        label="Submit"
                        size="large"
                        type="action"
                        onClick={ () => {
                            this.personalInfoSubmit.setState({ disabled: true }, () => {
                                this.props.updatePersonalInfo( this.state.personal_info, this.company_id );
                            });
                        } }
                    />
                </div>
                { errorDisplay }
            </div>
        );
    }

    /**
     * Renders the time & attendance infomation upload section of the update employees
     */
    getTimeAttendanceInfoSection() {
        let errorDisplay = [];

        const errorList = this.props.time_attendance_info.errors;
        const keys = Object.keys( errorList );
        if ( keys.length ) {
            const columns = [
                { header: 'INDEX', accessor: 'row', minWidth: 150, sortable: false },
                { header: 'ERROR', accessor: 'error', minWidth: 550, sortable: false }
            ];

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row #{ Number( key ) + 1 }</H4>,
                    error: <ul>{errorList[ key ].map( ( error, index ) => <li key={ index } style={ { margin: '0' } } >{error}</li> )}</ul>
                });
            });

            errorDisplay = (
                <div className="errors">
                    <H4>ERROR OCCURED:</H4>
                    <div style={ { textAlign: 'right' } }><Clipboard value={ this.state.time_attendance_info_clipboard } /></div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                    />
                </div>
            );
        }
        return (
            <div className="personal" style={ { display: this.state.step === 'time_attendance' ? '' : 'none' } } >
                <div className="row">
                    <div className="col-xs-12">
                        <H4>Update Time and Attendance Information</H4>
                        <P>Edit employees&#39; time and attendance data.</P>
                    </div>
                </div>
                <div className="row steps">
                    <div className="step col-sm-6">
                        <div className="template">
                            <H6>Step 1:</H6>
                            <p>A batch upload template is available for you to download and fill out.</p>
                            <P><A target="_blank" href="/payroll/guides/employees/batch-upload#time-attendance-guide">You may click here to view the upload guide.</A></P>
                            <A href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/employee_time_attendance_info_template.csv" download>Download Template</A>
                        </div>
                    </div>
                    <div className="step col-sm-6">
                        <div className="upload">
                            <H6>Step 2:</H6>
                            <p>After completely filling out the template, choose and upload it here.</p>
                            <div style={ { display: [ '', 'validation_failed', 'saved', 'save_failed' ].includes( this.props.time_attendance_info.status ) ? 'block' : 'none' } } >
                                <FileInput
                                    accept=".csv"
                                    onDrop={ ( files ) => {
                                        const { acceptedFiles } = files;
                                        this.props.resetTimeAttendanceInfo();
                                        this.timeAttendanceInfoSubmit.setState({ disabled: !acceptedFiles.length > 0 });
                                        this.setState({
                                            time_attendance_info: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null
                                        });
                                    } }
                                    ref={ ( ref ) => { this.timeAttendanceInput = ref; } }
                                />
                            </div>
                            <div style={ { display: this.props.time_attendance_info.status === 'saved' ? 'block' : 'none' } }>
                                <H4 style={ { color: '#4ABA4A', margin: '0' } }>SAVED <i className="fa fa-check-square-o" /></H4>
                            </div>
                            <div style={ { display: [ 'save_failed', 'validation_failed' ].includes( this.props.time_attendance_info.status ) ? 'block' : 'none' } }>
                                <H4 style={ { color: '#F21108', margin: '0' } }>{ this.props.time_attendance_info.status.split( '_' ).join( ' ' ).toUpperCase() } <i className="fa fa-times-circle-o" /></H4>
                            </div>
                            <div style={ { display: [ '', 'saved', 'save_failed', 'validation_failed' ].includes( this.props.time_attendance_info.status ) ? 'none' : 'block' } } >
                                <StyledLoader className="animation">
                                    <H4 style={ { margin: '0' } }>{ this.props.time_attendance_info.status.split( '_' ).join( ' ' ).toUpperCase() }</H4> <div className="anim3"></div>
                                </StyledLoader>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="action_button">
                    <Button
                        label="Cancel"
                        alt
                        size="large"
                        type="action"
                        onClick={ () => { browserHistory.push( '/employees', true ); } }
                    />
                    <Button
                        disabled={ this.setSubmitButtonStatus( this.state.time_attendance_info, this.props.time_attendance_info.status ) }
                        ref={ ( ref ) => { this.timeAttendanceInfoSubmit = ref; } }
                        label="Submit"
                        size="large"
                        type="action"
                        onClick={ () => {
                            this.timeAttendanceInfoSubmit.setState({ disabled: true }, () => {
                                this.props.updateTimeAttendanceInfo( this.state.time_attendance_info, this.company_id );
                            });
                        } }
                    />
                </div>
                { errorDisplay }
            </div>
        );
    }

    /**
     * this renders the payroll infomation upload section of the add employees
     */
    getPayrollInfoSection() {
        let errorDisplay = [];

        const errorList = this.props.payroll_info.errors;
        const keys = Object.keys( errorList );
        if ( keys.length ) {
            const columns = [
                { header: 'INDEX', accessor: 'row', minWidth: 150, sortable: false },
                { header: 'ERROR', accessor: 'error', minWidth: 550, sortable: false }
            ];

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row #{ Number( key ) + 1 }</H4>,
                    error: <ul>{errorList[ key ].map( ( error, index ) => <li key={ index } style={ { margin: '0' } } >{error}</li> )}</ul>
                });
            });

            errorDisplay = (
                <div className="errors">
                    <H4>ERROR OCCURED:</H4>
                    <div style={ { textAlign: 'right' } }><Clipboard value={ this.state.payroll_info_clipboard } /></div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                    />
                </div>
            );
        }
        return (
            <div className="personal" style={ { display: this.state.step === 'payroll' ? '' : 'none' } } >
                <div className="row">
                    <div className="col-xs-12">
                        <H4>Update Payroll Information</H4>
                        <P>Edit details like your employees&#39; basic salaries, tax statuses, payroll groups, and government numbers.</P>
                    </div>
                </div>
                <div className="row steps">
                    <div className="step col-sm-6">
                        <div className="template">
                            <H6>Step 1:</H6>
                            <p>A batch upload template is available for you to download and fill out.</p>
                            <P><A target="_blank" href="/payroll/guides/employees/batch-upload#payroll-guide">You may click here to view the upload guide.</A></P>
                            <A href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/employee_payroll_info_template.csv" download>Download Template</A>
                        </div>
                    </div>
                    <div className="step col-sm-6">
                        <div className="upload">
                            <H6>Step 2:</H6>
                            <p>After completely filling out the template, choose and upload it here.</p>
                            <div style={ { display: [ '', 'validation_failed', 'saved', 'save_failed' ].includes( this.props.payroll_info.status ) ? 'block' : 'none' } } >
                                <FileInput
                                    accept=".csv"
                                    onDrop={ ( files ) => {
                                        const { acceptedFiles } = files;
                                        this.props.resetPayrollInfo();
                                        this.payrollInfoSubmit.setState({ disabled: !acceptedFiles.length > 0 });
                                        this.setState({
                                            payroll_info: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null
                                        });
                                    } }
                                    ref={ ( ref ) => { this.payrollInput = ref; } }
                                />
                            </div>
                            <div style={ { display: this.props.payroll_info.status === 'saved' ? 'block' : 'none' } }>
                                <H4 style={ { color: '#4ABA4A', margin: '0' } }>SAVED <i className="fa fa-check-square-o" /></H4>
                            </div>
                            <div style={ { display: [ 'save_failed', 'validation_failed' ].includes( this.props.payroll_info.status ) ? 'block' : 'none' } }>
                                <H4 style={ { color: '#F21108', margin: '0' } }>{ this.props.payroll_info.status.split( '_' ).join( ' ' ).toUpperCase() } <i className="fa fa-times-circle-o" /></H4>
                            </div>
                            <div style={ { display: [ '', 'saved', 'save_failed', 'validation_failed' ].includes( this.props.payroll_info.status ) ? 'none' : 'block' } } >
                                <StyledLoader className="animation">
                                    <H4 style={ { margin: '0' } }>{ this.props.payroll_info.status.split( '_' ).join( ' ' ).toUpperCase() }</H4> <div className="anim3"></div>
                                </StyledLoader>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="action_button">
                    <Button
                        label="Cancel"
                        alt
                        size="large"
                        type="action"
                        onClick={ () => { browserHistory.push( '/employees', true ); } }
                    />
                    <Button
                        disabled={ this.setSubmitButtonStatus( this.state.payroll_info, this.props.payroll_info.status ) }
                        ref={ ( ref ) => { this.payrollInfoSubmit = ref; } }
                        label="Submit"
                        size="large"
                        type="action"
                        onClick={ () => {
                            this.payrollInfoSubmit.setState({ disabled: true }, () => {
                                this.props.updatePayrollInfo( this.state.payroll_info, this.company_id );
                            });
                        } }
                    />
                </div>
                { errorDisplay }
            </div>
        );
    }

    setSubmitButtonStatus( file, uploadStatus ) {
        return file ? ( file.length <= 0 || !!uploadStatus.length ) : true;
    }

    getUpdateSteps() {
        const steps = [
            { label: 'Personal Information', value: 'personal' }
        ];

        if ( this.isTaSubscribed() ) {
            steps.push({ label: 'Time and Attendance Information', value: 'time_attendance' });
        }

        if ( this.isPayrollSubscribed() ) {
            steps.push({ label: 'Payroll Information', value: 'payroll' });
        }

        return steps;
    }

    isTaSubscribed = () => (
        this.props.products && subscriptionService.isSubscribedToTA( this.props.products )
    )

    isPayrollSubscribed = () => (
        this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products )
    )

    /**
     * renders component to DOM
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Batch Upload"
                    meta={ [
                        { name: 'description', content: 'Employees Batch Upload' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => { e.preventDefault(); browserHistory.push( '/employees', true ); } }
                            >
                                &#8592; Back to Employees
                            </A>
                        </Container>
                    </div>
                    <div className="heading">
                        <h3>Batch Update</h3>
                        <p>Make multiple changes on your existing employee records. Download our batch update templates, fill them out, and upload them once you are done.</p>
                    </div>
                    <div className="toggle">
                        <Toggle2
                            options={ this.getUpdateSteps() }
                            defaultSelected="personal"
                            onChange={ ( step ) => {
                                const inputDefault = {
                                    error: false,
                                    files: [],
                                    label: 'Try dropping some files'
                                };

                                this.setState({ step });
                                if ( step === 'personal' ) {
                                    this.props.resetPersonalInfo();
                                    this.personalInput.setState( inputDefault );
                                } else if ( step === 'time_attendance' ) {
                                    this.props.resetTimeAttendanceInfo();
                                    this.timeAttendanceInput.setState( inputDefault );
                                } else {
                                    this.props.resetPayrollInfo();
                                    this.payrollInput.setState( inputDefault );
                                }
                            } }
                        />
                    </div>
                    <div className="employees-form">
                        <div>
                            { this.getPersonalInfoSection() }
                            { this.isTaSubscribed() ? this.getTimeAttendanceInfoSection() : null }
                            { this.isPayrollSubscribed() ? this.getPayrollInfoSection() : null }
                        </div>
                    </div>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    personal_info: makeSelectPersonalInfo(),
    time_attendance_info: makeSelectTimeAttendanceInfo(),
    payroll_info: makeSelectPayrollInfo(),
    notification: makeSelectNotification(),
    products: makeSelectProductsState()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        batchUpdateActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EmployeeBatchUpdate );
