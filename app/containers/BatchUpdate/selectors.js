import { createSelector } from 'reselect';

/**
 * Direct selector to the setup state domain
 */
const selectBatchUpdateDomain = () => ( state ) => state.get( 'batch_update' );

const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */
const makeSelectPersonalInfo = () => createSelector(
    selectBatchUpdateDomain(),
    ( substate ) => substate.get( 'personal_info' ).toJS()
);

const makeSelectTimeAttendanceInfo = () => createSelector(
    selectBatchUpdateDomain(),
    ( substate ) => substate.get( 'time_attendance_info' ).toJS()
);

const makeSelectPayrollInfo = () => createSelector(
    selectBatchUpdateDomain(),
    ( substate ) => substate.get( 'payroll_info' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectBatchUpdateDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectProductsState = () => createSelector(
    selectAppDomain(),
    ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

export {
    makeSelectPersonalInfo,
    makeSelectTimeAttendanceInfo,
    makeSelectPayrollInfo,
    makeSelectNotification,
    makeSelectProductsState
};

