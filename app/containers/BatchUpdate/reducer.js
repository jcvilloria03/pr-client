import { fromJS } from 'immutable';

import {
    PERSONAL_INFO_STATUS,
    PERSONAL_INFO_ERRORS,
    PERSONAL_INFO_RESET,
    TIME_ATTENDANCE_INFO_STATUS,
    TIME_ATTENDANCE_INFO_ERRORS,
    TIME_ATTENDANCE_RESET,
    PAYROLL_INFO_STATUS,
    PAYROLL_INFO_ERRORS,
    PAYROLL_INFO_RESET,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../App/constants';

const initialState = fromJS({
    personal_info: {
        status: '',
        errors: {}
    },
    time_attendance_info: {
        status: '',
        errors: {}
    },
    payroll_info: {
        status: '',
        errors: {}
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Batch Upload reducer
 *
 */
function batchUpdateReducer( state = initialState, action ) {
    switch ( action.type ) {
        case PERSONAL_INFO_STATUS:
            return state.setIn([ 'personal_info', 'status' ], action.payload );
        case PERSONAL_INFO_ERRORS:
            return state.setIn([ 'personal_info', 'errors' ], fromJS( action.payload ) );
        case PERSONAL_INFO_RESET:
            return state.set( 'personal_info', fromJS({ status: '', errors: {}}) );
        case TIME_ATTENDANCE_INFO_STATUS:
            return state.setIn([ 'time_attendance_info', 'status' ], action.payload );
        case TIME_ATTENDANCE_INFO_ERRORS:
            return state.setIn([ 'time_attendance_info', 'errors' ], fromJS( action.payload ) );
        case TIME_ATTENDANCE_RESET:
            return state.set( 'time_attendance_info', fromJS({ status: '', errors: {}}) );
        case PAYROLL_INFO_STATUS:
            return state.setIn([ 'payroll_info', 'status' ], action.payload );
        case PAYROLL_INFO_ERRORS:
            return state.setIn([ 'payroll_info', 'errors' ], fromJS( action.payload ) );
        case PAYROLL_INFO_RESET:
            return state.set( 'payroll_info', fromJS({ status: '', errors: {}}) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( Object.assign( state.get( 'notification' ).toJS(), action.payload ) ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default batchUpdateReducer;

