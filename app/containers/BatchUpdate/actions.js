import {
    UPDATE_PERSONAL_INFO,
    UPDATE_TIME_ATTENDANCE_INFO,
    UPDATE_PAYROLL_INFO,
    PERSONAL_INFO_RESET,
    TIME_ATTENDANCE_RESET,
    PAYROLL_INFO_RESET,
    NOTIFICATION
} from './constants';

/**
 * upload personal info
 */
export function updatePersonalInfo( file, companyId ) {
    return {
        type: UPDATE_PERSONAL_INFO,
        payload: {
            company_id: companyId,
            file
        }
    };
}

/**
 * upload time & attendance info
 */
export function updateTimeAttendanceInfo( file, companyId ) {
    return {
        type: UPDATE_TIME_ATTENDANCE_INFO,
        payload: {
            company_id: companyId,
            file
        }
    };
}

/**
 * upload payroll info
 */
export function updatePayrollInfo( file, companyId ) {
    return {
        type: UPDATE_PAYROLL_INFO,
        payload: {
            company_id: companyId,
            file
        }
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * upload payroll info
 */
export function resetPersonalInfo() {
    return {
        type: PERSONAL_INFO_RESET
    };
}

/**
 * upload payroll info
 */
export function resetTimeAttendanceInfo() {
    return {
        type: TIME_ATTENDANCE_RESET
    };
}

/**
 * upload payroll info
 */
export function resetPayrollInfo() {
    return {
        type: PAYROLL_INFO_RESET
    };
}
