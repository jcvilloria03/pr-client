/*
 *
 * Batch Update constants
 *
 */
export const UPDATE_PERSONAL_INFO = 'app/BatchUpdate/UPDATE_PERSONAL_INFO';
export const PERSONAL_INFO_STATUS = 'app/BatchUpdate/PERSONAL_INFO_STATUS';
export const PERSONAL_INFO_ERRORS = 'app/BatchUpdate/PERSONAL_INFO_ERRORS';
export const PERSONAL_INFO_RESET = 'app/BatchUpdate/PERSONAL_INFO_RESET';

export const UPDATE_TIME_ATTENDANCE_INFO = 'app/BatchUpdate/UPDATE_TIME_ATTENDANCE_INFO';
export const TIME_ATTENDANCE_INFO_STATUS = 'app/BatchUpdate/TIME_ATTENDANCE_INFO_STATUS';
export const TIME_ATTENDANCE_INFO_ERRORS = 'app/BatchUpdate/TIME_ATTENDANCE_INFO_ERRORS';
export const TIME_ATTENDANCE_RESET = 'app/BatchUpdate/TIME_ATTENDANCE_RESET';

export const UPDATE_PAYROLL_INFO = 'app/BatchUpdate/UPDATE_PAYROLL_INFO';
export const PAYROLL_INFO_STATUS = 'app/BatchUpdate/PAYROLL_INFO_STATUS';
export const PAYROLL_INFO_ERRORS = 'app/BatchUpdate/PAYROLL_INFO_ERRORS';
export const PAYROLL_INFO_RESET = 'app/BatchUpdate/PAYROLL_INFO_RESET';

export const NOTIFICATION = 'app/BatchUpdate/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/BatchUpdate/NOTIFICATION_SAGA';
