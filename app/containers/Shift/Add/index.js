/* eslint-disable react/prop-types */
/* eslint-disable no-unused-expressions, no-sequences, import/first */

import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import moment from 'moment';
import { createStructuredSelector } from 'reselect';
import { cloneDeep } from 'lodash';

import { browserHistory } from 'utils/BrowserHistory';
import { Fetch } from 'utils/request';
import Icon from 'components/Icon';
import { H3, H1, P } from 'components/Typography';
import A from 'components/A';
import Button from 'components/Button';
import Flatpickr from 'react-flatpickr';
import Toggle from 'components/Toggle';
import MultiSelect from 'components/MultiSelect';
import SalConfirm from 'components/SalConfirm';
import { company } from 'utils/CompanyService';
import SalSelect from 'components/Select';
import AsyncTable from 'components/AsyncTable';
import Loader from 'components/Loader';
import { SELECT_ALL_EMPLOYEES_OPTIONS } from './constants';
import SnackBar from 'components/SnackBar';
import {
    makeSelectLoading,
    makeSelectAssignShiftList,
    makeSelectAssignShiftcreate,
    makeSelectNotification,
    makeSelectStatusData
} from './selectors';
import {
    NavWrapper,
    PageWrapper,
    FootWrapper,
    ConfirmBodyWrapperStyle
} from './styles';
import * as ManualAction from './actions';

/**
 *
 * AddShifts
 *
 */
export class AddShifts extends React.Component { // eslint-disable-line react/prefer-stateless-function

    static PropsTypes ={
        listSchduleData: React.PropTypes.array,
        getSchedule: React.PropTypes.func,
        create: React.PropTypes.string,
        addSchedule: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool,
        statusData: React.PropTypes.object
    }
    /**
     *
     * AddShifts render method
     *
     */
    constructor( props ) {
        super( props );

        this.state = {
            addData: {
                schedule_id: '',
                start: '',
                end: '',
                affected_employees: this.props.AssignedEmployees
            },
            selectData: [],
            selectedDay: null,
            startDateOptions: {
                minDate: new Date( null ),
                maxDate: null
            },
            endDateOptions: {
                minDate: new Date( null )
            }
        };
    }

    componentDidMount() {
        this.props.getSchedule();
    }

    getlistselectData = () => {
        if ( Object.keys( this.props.listSchduleData ).length === 0 ) {
            return [];
        }
        return this.props.listSchduleData.map( ( ele ) => (
            {
                ele,
                value: ele.id,
                label: ele.name
            }) );
    }

    addScheduleData() {
        const addData = cloneDeep( this.state.addData );

        if ( !this.props.loading && addData && addData.affected_employees.length > 0 ) {
            const data = {
                company_id: company.getLastActiveCompanyId(),
                schedule_id: addData.schedule_id,
                end: moment( addData.end ).format( 'YYYY-MM-DD' ),
                start: moment( addData.start ).format( 'YYYY-MM-DD' ),
                affected_employees: addData.affected_employees.map( ( val ) => ({
                    id: val.id,
                    type: val.type,
                    name: val.label,
                    uid: val.uid
                }) )
            };
            this.props.addSchedule( data );
        }
    }

    /**
     * gets the batch upload icon
     */
    batchUploadIcon() {
        return (
            <Icon name="batchUpload" />
        );
    }

    /**
     * gets the manual entry icon
     */
    manualEntryIcon() {
        return (
            <Icon name="manualEntry" />
        );
    }

    addToList() {
        this.setState({
            formData: {
                ...this.state.formData,
                ipAddressList: [
                    ...this.state.formData.ipAddressList,
                    this.state.ipAddress
                ]
            }
        });
        this.setState({
            ipAddress: ''
        });
    }

    deleteList( id ) {
        this.setState({
            formData: {
                ipAddressList: [
                    this.state.formData.ipAddressList.filter( ( arr, i ) => i !== id )
                ]
            }
        });
    }

    showTextbox( IpId ) {
        return this.state.editIP && this.state.editIP.id === IpId;
    }

    AssignedEmployees = ( keyword, callback ) => {
        const companyId = company.getLastActiveCompanyId();
        Fetch( `/company/${companyId}/affected_employees/search?term=${keyword}&limit=10&without_users=yes`, { method: 'GET' })
        .then( ( result ) => {
            const list = result.map( ( data ) => ({
                id: data.id,
                value: data.id,
                label: data.name,
                type: data.type,
                uid: `${data.type}${data.id}`
            }) );
            callback( null, { options: SELECT_ALL_EMPLOYEES_OPTIONS.concat( list ) });
        })
        .catch( ( error ) => callback( error, null ) );
    }

    render() {
        const { statusData } = this.props;

        const showCount = statusData.status === 'validating';
        const showError = statusData.status === 'validation_failed';

        const { startDateOptions, endDateOptions } = this.state;

        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Schedule type:',
                accessor: 'type',
                sortable: false,
                minWidth: 150
            },
            {
                header: 'Start and end time:',
                sortable: false,
                minWidth: 150,
                render: ( ( row ) => (
                    <div>{row.row && row.row.start_time || row.row && row.row.end_time === true ? `${row.row && row.row.start_time} to ${row.row && row.row.end_time}` : ''}</div>
                ) )
            },
            {
                header: 'Schedule effective start date:',
                sortable: false,
                minWidth: 150,
                render: ( ( row ) => (
                    <div>{ row.row && row.row.start_date ? moment( row.row && row.row.start_date ).format( 'MMMM D, YYYY' ) : ''}</div>
                ) )
            },
            {
                header: 'Schedule effective end date:',
                sortable: false,
                minWidth: 150,
                render: ( ( row ) => (
                    <div>{row.row && row.row.end_date === undefined ? 'Never ends' : row.row && row.row.end_date}</div>
                ) )
            }
        ];
        return (
            <div>
                <Helmet
                    title="Add Shifts"
                    meta={ [
                        { name: 'description', content: 'Description of Add Shifts' }
                    ] }
                />

                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                <SalConfirm
                    onConfirm={ () => browserHistory.push( '/time/shifts/calendar', true ) }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                            Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Discard Changes"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Discard"
                    cancelText="Stay on this page"
                    visible={ this.state.showModal }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/shifts/calendar', true );
                            } }
                        >
                            &#8592; Back to Shifts
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="heading">
                                <H1 className="assign_title">Assign Shift</H1>
                            </div>
                        </div>
                        <div className="toggle">
                            <Toggle
                                options={ [
                                    { title: 'Batch Upload', subtext: 'Upload multiple entries at once using template.', value: 'batch', icon: this.batchUploadIcon() },
                                    { title: 'Manual Entry', subtext: 'Create entries one by one.', value: 'single', icon: this.manualEntryIcon() }
                                ] }
                                defaultSelected="single"
                                onChange={ ( value ) => { if ( value === 'batch' ) { browserHistory.push( '/time/shifts/assign/batch', true ); } } }
                                ref={ ( ref ) => { this.toggle = ref; } }
                            />
                        </div>
                        <div className="schedule">
                            <div>
                                <div className="schedule-head">
                                    <div className="schedule-head-title">
                                        <H3>1</H3>
                                    </div>
                                    <span>Choose a schedule</span>
                                </div>
                                <div className="row">
                                    <div className="col-xs-4">
                                        <p className="schdule_tooltip">* Schedule
                                            <span className="bg_tooltip">
                                                <span>?</span>
                                                <span className="tooltiptext">Covers working hours,
                                                    break periods, and other planned activities.</span>
                                            </span>
                                        </p>
                                        <div className="datepicker-main">
                                            <SalSelect
                                                id="name"
                                                lable="name"
                                                required
                                                data={ this.getlistselectData() }
                                                ref={ ( ref ) => { this.name = ref; } }
                                                placeholder="Choose an option"
                                                onChange={ ( value ) => {
                                                    this.setState( ( prevState ) => ({
                                                        selectData: value,
                                                        startDateOptions: { minDate: value.ele.start_date, maxDate: null },
                                                        endDateOptions: { minDate: value },
                                                        addData: { ...prevState.addData, schedule_id: value.value }
                                                    }) );
                                                } }
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="summary">
                                <div className="summary_header">
                                    <div>
                                        <p className="schdule_tooltip summary_tooltip">Schedule a summary
                                        </p>
                                    </div>
                                    <div>
                                        {this.state.selectData.ele && this.state.selectData.ele.id ? (
                                            <A
                                                href
                                                onClick={ ( e ) => {
                                                    e.preventDefault();
                                                    window.location.href = `/time/schedules/edit/${this.state.selectData.ele.id}/`;
                                                } }
                                            >
                                                View schedule details
                                            </A>
                                        ) : '' }
                                    </div>
                                </div>
                                <AsyncTable
                                    data={ [this.state.selectData.ele] }
                                    columns={ tableColumns }
                                    ref={ ( ref ) => { this.shiftTable = ref; } }
                                />
                            </div>
                        </div>
                        <div className="shift-details">
                            <div className="schedule-head">
                                <div className="schedule-head-title">
                                    <H3>2</H3>
                                </div>
                                <span>Shift details</span>
                            </div>
                            <div className="row">
                                <div className="col-xs-4">
                                    <p className="schdule_tooltip">* Shift effective start date
                                        <span className="bg_tooltip">
                                            <span>?</span>
                                            <span className="tooltiptext">Start date refers to the date when the employee should start following the selected schedule.</span>
                                        </span>
                                    </p>
                                    <div className="datepicker-main">
                                        <Flatpickr
                                            onChange={ ([eve]) => this.setState( ( prevState ) => ({
                                                endDateOptions: { minDate: eve },
                                                addData: { ...prevState.addData, start: eve }
                                            }) ) }
                                            options={ startDateOptions }
                                        />
                                    </div>
                                </div>
                                <div className="col-xs-4">
                                    <p className="schdule_tooltip">* Shift effective end date
                                        <span className="bg_tooltip">
                                            <span>?</span>
                                            <span className="tooltiptext">The employee should follow the selected schedule until the end date indicated here..</span>
                                        </span>
                                    </p>
                                    <div className="datepicker-main">
                                        <Flatpickr
                                            onChange={ ([eve]) => this.setState( ( prevState ) => ({
                                                startDateOptions: { ...startDateOptions, maxDate: eve },
                                                addData: { ...prevState.addData, end: eve }
                                            }) ) }
                                            options={ endDateOptions }
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className={ showError ? 'col-xs-12 Employees assign-employee-error' : 'col-xs-12 Employees' }>
                                    <P noBottomMargin className="mb-0">Assigned Employees</P>
                                    <MultiSelect
                                        id="assignedemployees"
                                        async
                                        required
                                        loadOptions={ this.AssignedEmployees }
                                        ref={ ( ref ) => { this.assignedemployees = ref; } }
                                        placeholder="Enter location,department name,position,employee name"
                                        onChange={ ( eve ) => this.setState( ( prevState ) => ({ addData: { ...prevState.addData, affected_employees: eve }}) ) }
                                    />
                                    {showError && <P noBottomMargin>{statusData.errors}</P>}
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <FootWrapper>
                    <Button
                        className="button"
                        label={ <span>Cancel</span> }
                        type="grey"
                        size="large"
                        onClick={ () => {
                            this.setState({ showModal: false }, () => {
                                this.setState({ showModal: true });
                            });
                        } }
                    />
                    <Button
                        label={ <span>{ !this.props.loading ? 'Submit' : <Loader /> }</span> }
                        type="action"
                        size="large"
                        onClick={ () => this.addScheduleData() }
                    />

                    {showCount && <P noBottomMargin>Total count: {statusData.employees}</P>}
                </FootWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    listSchduleData: makeSelectAssignShiftList(),
    create: makeSelectAssignShiftcreate(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    statusData: makeSelectStatusData()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        ManualAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddShifts );
