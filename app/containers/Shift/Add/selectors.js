import { createSelector } from 'reselect';

/**
 * Direct selector to the addShifts state domain
 */
const selectAddShiftsDomain = () => ( state ) => state.get( 'addShifts' );

/**
 * Other specific selectors
 */
const makeSelectAssignShiftList = () => createSelector(
  selectAddShiftsDomain(),
  ( substate ) => substate.get( 'scheduleData' ).toJS()
);

const makeSelectAssignShiftcreate = () => createSelector(
  selectAddShiftsDomain(),
  ( substate ) => substate.get( 'createSchedule' )
);

const makeSelectLoading = () => createSelector(
  selectAddShiftsDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectAddShiftsDomain(),
  ( schedules ) => schedules && schedules.get( 'notification' ).toJS()
);

const makeSelectStatusData = () => createSelector(
  selectAddShiftsDomain(),
  ( schedules ) => schedules && schedules.get( 'shiftStatusData' ).toJS()
);

export {
  selectAddShiftsDomain,
  makeSelectAssignShiftList,
  makeSelectAssignShiftcreate,
  makeSelectNotification,
  makeSelectLoading,
  makeSelectStatusData
};
