import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_SCHEDULE,
    GET_ADD_SCHEDULE,
    NOTIFICATION_SAGA,
    LOADING,
    SET_ASSIGN_SHIFT_STATUS_DATA
} from './constants';

const initialState = fromJS({
    scheduleData: [],
    createSchedule: '',
    shiftStatusData: {
        status: null,
        errors: null,
        employees: null,
        created: null,
        company_id: null
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * AddShifts reducer
 *
 */
function addShiftsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_SCHEDULE:
            return state.set( 'scheduleData', fromJS( action.payload ) );
        case GET_ADD_SCHEDULE:
            return state.set( 'createSchedule', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ASSIGN_SHIFT_STATUS_DATA:
            return state.set( 'shiftStatusData', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        default:
            return state;
    }
}

export default addShiftsReducer;
