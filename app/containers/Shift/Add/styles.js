import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    justify-content: center;
    min-height: 100%;
`;

export const PageWrapper = styled.div`
    padding-top: 90px;
    .content {
        margin-top: 60px;
        .entriTitle{
            text-align:right;
        }
         
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            p{
                margin-bottom:0.25rem;
            }
        }
    }
    .heading h1 {
        font-size: 1.5rem;
        font-weight: 700;
    }
    h1.assign_title {
        margin-bottom: 0;
    }
    .summary_tooltip{
        .bg_tooltip .tooltiptext {
            transform: translateY(13%) !important;
        }
    }

    .rt-tr.-odd .rt-td:first-child{
        text-transform: capitalize;
    }



    .schdule_tooltip{
        display: flex;
        align-items: center;
        font-size: 14px;
        margin-bottom:5px;
        button{
            width: 20px;
            height: 20px;
            padding: 0;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
        }
        a{
            padding: 0;
            border-bottom: none;
            padding-left: 6px;
        }
        .bg_tooltip {
            position: relative;
            display: inline-block;
            width:20px;
            height:20px;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
            margin-left: 6px;
            text-align: center;
            label{
                font-size: 15px;
                font-weight: bold;
            }
        }

        .bg_tooltip .tooltiptext {
            visibility: hidden;
            width: 242px;
            background-color: #fff;
            color: #03a9f4;
            border: 2px solid #03a9f4;
            text-align: left;
            border-radius: 6px;
            padding: 5px 16px;
            position: absolute;
            left: 26px;
            top: -50%;
            transform: translateY(-20%);
            z-index: 1;
        }

        .bg_tooltip .tooltiptextStart{
            transform: translateY(-26%);
        }

        .bg_tooltip .tooltiptextEnd{
            transform: translateY(-20%);
            width: 262px;
        }
        .bg_tooltip .tooltiptextHours{
            transform: translateY(-17%);
            width: 255px;
        }
        .bg_tooltip:hover .tooltiptext {
            visibility: visible;
        }
        .bg_tooltip .tooltiptext::after {
            content: " ";
            position: absolute;
            top: 50%;
            right: 99%;
            margin-top: -5px;
            border: solid #03a9f4;
            border-width: 0 2px 2px 0;
            display: inline-block;
            padding: 4px;
            transform: rotate(135deg);
            background-color: #ffff;
        }
    }
    .schedule{
        .schedule-head{
            margin-bottom:20px;
            margin-top:20px;
            display: flex;
            align-items: center;
            color: #00A5E5;
            .schedule-head-title{
                background: white;
                border: 1px solid #00A5E5;
                width: max-content;
                width: 26px;
                height: 26px;
                border-radius: 50%;
                display: flex;
                align-items: center;
                justify-content: center;
                h3{
                    margin-top: 16px;
                    font-weight: 600;
                    font-size:16px;
                }
            }
            span{
                margin-left: 5px;
                font-size:15px;
            }
        }
        .giQTap{
            .Select-control{
                width:37%;
                .Select-placeholder{
                    font-size:15px;
                }
            }
        }
        .summary{
            .summary_header{
                display:flex;
                justify-content: space-between;
                margin-top: 30px;
                p{
                    font-weight:600;
                    margin-bottom:10px;
                    color:black;
                }
                .hvYuOH{
                    .ReactTable {
                        .rt-table{
                            border-radius:0.5rem;
                            .rt-thead{
                                background-color:#E5F6FC;
                                border-color: rgba(0, 165, 229, 0.1);
                                color:black;
                            }
                        }
                    }
                }
            }
        }
    }
    .shift-details{
        .DayPickerInput,.DayPickerInput input{
            width:100%;
        }
        .Employees{
            padding-bottom:70px;
        }
        .datepicker-main{
            span{
                display:none;
            }
        }
        .schedule-head{
            margin-top:20px;
            margin-bottom:20px;
            display: flex;
            align-items: center;
            color: #00A5E5;
            .schedule-head-title{
                background: white;
                border: 1px solid #00A5E5;
                width: max-content;
                width: 26px;
                height: 26px;
                border-radius: 50%;
                display: flex;
                align-items: center;
                justify-content: center;
                h3{
                    margin-top: 16px;
                    font-weight: 600;
                    font-size:16px;
                }
            }
            span{
                margin-left: 5px;
                font-size:15px;
            }
        }

        .assign-employee-error {
            color: #eb7575;

            .Select-control {
                border-color: #eb7575;
            }
        }
    }



    .selected .fill {
        fill: #fff;
    }

    .selected .stroke {
        stroke: #fff;
    }

    .fill {
        fill: rgb(0,165,226);
    }

    .stroke {
        stroke: rgb(0,165,226);
    }

    .flatpickr-input {
        height: 46px;
        width: 368px;
        border: 1px solid #95989a !important;
        padding: 5px 12px 0;
    }
`;

export const FootWrapper = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    display: flex;
    align-items: center;
    justify-content: end;

    button {
        min-width: 120px;
    }

    p {
        margin-left: 8px;
    }
`;
