/* eslint-disable require-jsdoc */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay } from 'redux-saga';
import { Fetch } from 'utils/request';
import { LOCATION_CHANGE } from 'react-router-redux';
import {
    LOADING,
    SET_SCHEDULE,
    GET_SCHEDULE,
    ADD_SCHEDULE,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    GET_ASSIGN_SHIFT_STATUS,
    SET_ASSIGN_SHIFT_STATUS_DATA
} from './constants';
import { company } from '../../../utils/CompanyService';
import { browserHistory } from '../../../utils/BrowserHistory';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* getScheduleData() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/schedules`, { method: 'GET' });
        yield put({
            type: SET_SCHEDULE,
            payload: response.data || []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

export function* createSchedule({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const response = yield call( Fetch, '/shift/bulk_create', { method: 'POST', data: payload });

        yield call( getAssignShiftStatus, { payload: { job_id: response.id }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Get assign shift status
 */
export function* getAssignShiftStatus({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();

        const response = yield call( Fetch, `/shift/${companyId}/assign_shifts/status`, {
            method: 'POST',
            data: {
                job_id: payload.job_id
            }
        });

        yield put({
            type: SET_ASSIGN_SHIFT_STATUS_DATA,
            payload: response
        });

        if ( response.status === 'validation_failed' ) {
            yield put({
                type: LOADING,
                payload: false
            });
            yield call( notifyUser, {
                title: 'Error',
                message: response ? response.errors : 'Your request is invalid',
                show: true,
                type: 'error'
            });
        } else if ( response.status === 'completed' ) {
            yield put({
                type: LOADING,
                payload: false
            });
            yield call( notifyUser, {
                title: 'Success',
                message: 'Change Successfully save',
                show: true,
                type: 'success'
            });
            yield call( browserHistory.push, '/time/shifts/calendar', true );
        } else {
            yield call( delay, 2000 );
            yield call( getAssignShiftStatus, { payload: { job_id: payload.job_id }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: payload.type
        }
    });
    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: payload.type
        }
    });
}

export function* watchForgetScheduleData() {
    const watcher = yield takeEvery( GET_SCHEDULE, getScheduleData );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForcreateSchedule() {
    const watcher = yield takeEvery( ADD_SCHEDULE, createSchedule );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for get download url
 */
export function* watchForGetAssignShiftStatus() {
    const watcher = yield takeEvery( GET_ASSIGN_SHIFT_STATUS, getAssignShiftStatus );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForgetScheduleData,
    watchForcreateSchedule,
    watchForNotifyUser,
    watchForGetAssignShiftStatus
];
