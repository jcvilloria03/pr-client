/* eslint-disable require-jsdoc */
import {
    DEFAULT_ACTION,
    GET_SCHEDULE,
    ADD_SCHEDULE
} from './constants';

/**
 *
 * AddShifts actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function getSchedule() {
    return {
        type: GET_SCHEDULE
    };
}

export function addSchedule( payload ) {
    return {
        type: ADD_SCHEDULE,
        payload
    };
}
