/*
 *
 * AddShifts constants
 *
 */

export const DEFAULT_ACTION = 'app/AssignShift/DEFAULT_ACTION';
export const GET_SCHEDULE = 'app/AssignShift/GET_SCHEDULE';
export const SET_SCHEDULE = 'app/AssignShift/SET_SCHEDULE';
export const ADD_SCHEDULE = 'app/AssignShift/ADD_SCHEDULE';
export const GET_ADD_SCHEDULE = 'app/AssignShift/GET_ADD_SCHEDULE';
export const NOTIFICATION = 'app/AssignShift/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/AssignShift/NOTIFICATION_SAGA';
export const LOADING = 'app/AssignShift/LOADING';
export const GET_ASSIGN_SHIFT_STATUS = 'app/AssignShift/GET_ASSIGN_SHIFT_STATUS';

export const GET_ASSIGN_SHIFT_STATUS_DATA = 'app/AssignShift/GET_ASSIGN_SHIFT_STATUS_DATA';
export const SET_ASSIGN_SHIFT_STATUS_DATA = 'app/AssignShift/SET_ASSIGN_SHIFT_STATUS_DATA';

export const SELECT_ALL_EMPLOYEES_OPTIONS = [
    {
        id: null,
        type: 'department',
        uid: 'all_department',
        value: 'department',
        field: 'department',
        label: 'All departments',
        disabled: false
    },
    {
        id: null,
        type: 'position',
        uid: 'all_position',
        value: 'position',
        field: 'position',
        label: 'All positions',
        disabled: false
    },
    {
        id: null,
        type: 'location',
        uid: 'all_location',
        value: 'location',
        field: 'location',
        label: 'All locations',
        disabled: false
    },
    {
        id: null,
        type: 'employee',
        uid: 'all_employee',
        value: 'employee',
        field: 'employee',
        label: 'All employee',
        disabled: false
    }
];
