import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_LOADING,
    SET_MINI_LOADING,
    SET_DEFAULT_SCHEDULE,
    SET_SHIFTS,
    SET_NOTIFICATION,
    SET_FILTER_DATA,
    SET_SCHEDULE,
    SET_PAGINATION
} from './constants';

const initialState = fromJS({
    loading: true,
    defaultschedule: [],
    shifts: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    filterData: {
        filter: {
            location: '',
            department: '',
            status: 'active',
            week: ''
        },
        locations: '',
        departments: ''
    },
    schedule: [],
    pagination: {
        from: 1,
        to: 1,
        total: 1,
        current_page: 0,
        last_page: 1,
        per_page: 10
    },
    miniLoading: true
});

/**
 *
 * Shift reducer
 *
 */
function shiftReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_MINI_LOADING:
            return state.set( 'miniLoading', action.payload );
        case SET_DEFAULT_SCHEDULE:
            return state.set( 'defaultschedule', fromJS( action.payload ) );
        case SET_SHIFTS:
            return state.set( 'shifts', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case SET_SCHEDULE:
            return state.set( 'schedule', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default shiftReducer;
