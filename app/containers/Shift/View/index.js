/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment';
import get from 'lodash/get';

import { H1, H2, H3 } from 'components/Typography';
import Button from 'components/Button';
import Icon from 'components/Icon';
import Input from 'components/Input';
import SnackBar from 'components/SnackBar';

import SubHeader from 'containers/SubHeader';

import { makeSelectCurrentCompanyState } from 'containers/App/selectors';

import { TIME_AND_ATTENDANCE_SUBHEADER_ITEMS } from 'utils/constants';
import { browserHistory } from 'utils/BrowserHistory';
import FooterTablePagination from 'components/FooterTablePagination';
import * as shiftActions from './actions';
import {
    PageWrapper,
    LoadingStyles,
    CalendarHeader,
    CalendarHeaderDate,
    CalendarHeaderActions,
    CalendarHeaderActionGroup
} from './styles';

import Calendar from './Calendar';

import {
    makeSelectDefaultList,
    makeSelectShifts,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectFilterData,
    makeSelectSchedule,
    makeSelectPagination,
    makeSelectMiniLoading
} from './selectors';
import Filter from './templates/filterList';
import { CALENDAR_FREQUENCY } from './constants';

/**
 *
 * Shift
 *
 */
export class Shift extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static PropsTypes = {
        loading: React.PropTypes.bool,
        defaultSchedule: React.PropTypes.array,
        getDefaultSchedules: React.PropTypes.func,
        shifts: React.PropTypes.array,
        getShifts: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        filterData: React.PropTypes.shape({
            positions: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        getSchedule: React.PropTypes.func,
        scheduleList: React.PropTypes.array,
        generateCSV: React.PropTypes.func,
        pagination: React.PropTypes.object,
        addShift: React.PropTypes.func,
        addRestDay: React.PropTypes.func,
        unAssignShift: React.PropTypes.func,
        unAssignRestDay: React.PropTypes.func,
        miniLoading: React.PropTypes.bool
    }

    constructor( props ) {
        super( props );

        this.state = {
            showListFilter: false,
            hasFiltersApplied: false,
            eventData: '',
            viewName: CALENDAR_FREQUENCY.WEEK,
            date: moment( new Date() ).format( 'YYYY-MM-DD' ),
            scheduleDateRange: {
                start: null,
                end: null
            },
            currentPage: this.props.pagination.current_page || 1,
            total: this.props.pagination.total_pages,
            perPage: 10,
            weekDateFilter: null,
            currentMonth: moment().format( 'YYYY-MM' ),
            pageSelectOptions: [ 10, 25, 50, 100 ],
            label: 'Showing 0-0 of 0 entries',
            filter: {
                status: 'active',
                location: '',
                department: ''
            },
            search: '',
            shift: this.props.shifts
        };
    }

    componentDidMount() {
        this.props.getDefaultSchedules();
        this.fetchShiftsData();
    }

    componentWillReceiveProps() {
        // Set table details label
        if ( this.props.pagination.total ) {
            const page = this.props.pagination.current_page || 0;
            const size = this.props.pagination.per_page || 0;
            const dataLength = this.props.pagination.total || 0;
            this.setState({
                label: `Showing ${dataLength ? ( ( page * size ) + 1 ) - size : 0} - ${( page * size ) > dataLength ? dataLength : ( page * size )} of ${dataLength} entr${dataLength > 1 || dataLength === 0 ? 'ies' : 'y'}`
            });
        }
    }

    componentDidUpdate( prevProps ) {
        const companyId = get( this.props.currentCompany, 'id' );
        const prevCompanyId = get( prevProps.currentCompany, 'id' );

        if ( companyId && prevCompanyId && companyId !== prevCompanyId ) {
            this.reset();
        }
    }

    getWeekViewHeaderDate() {
        const { date } = this.state;
        const startOfWeek = moment( date ).startOf( CALENDAR_FREQUENCY.WEEK );
        const endOfWeek = moment( date ).endOf( CALENDAR_FREQUENCY.WEEK );

        if ( !startOfWeek.isSame( endOfWeek, 'year' ) ) {
            return `${startOfWeek.format( 'MMMM D, YYYY' )} - ${endOfWeek.format( 'MMMM D, YYYY' )}`;
        }

        if ( !startOfWeek.isSame( endOfWeek, 'month' ) ) {
            return `${startOfWeek.format( 'MMMM D' )} - ${endOfWeek.format( 'MMMM D, YYYY' )}`;
        }

        return `${startOfWeek.format( 'MMMM D' )} - ${endOfWeek.format( 'D, YYYY' )}`;
    }

    reset() {
        this.setState({
            showListFilter: false,
            hasFiltersApplied: false,
            eventData: '',
            viewName: CALENDAR_FREQUENCY.WEEK,
            date: moment( new Date() ).format( 'YYYY-MM-DD' ),
            scheduleDateRange: {
                start: null,
                end: null
            },
            currentPage: 1,
            total: 0,
            perPage: 10,
            weekDateFilter: null,
            currentMonth: moment().format( 'YYYY-MM' ),
            pageSelectOptions: [ 10, 25, 50, 100 ],
            label: 'Showing 0-0 of 0 entries',
            filter: {
                status: 'active',
                location: '',
                department: ''
            },
            search: '',
            shift: this.props.shifts
        }, () => {
            this.props.getDefaultSchedules();
            this.fetchShiftsData();
        });
    }

    navigateToManualPage() {
        browserHistory.push( '/time/shifts/assign/batch', true );
    }

    changePage( next ) {
        const {
            date,
            viewName,
            scheduleDateRange
        } = this.state;

        let newDate = moment( date );

        if ( next ) {
            newDate = newDate.add( 1, viewName );
        } else {
            newDate = newDate.subtract( 1, viewName );
        }

        newDate = newDate.format( 'YYYY-MM-DD' );

        const isWithinWeek = moment( newDate )
            .isBetween(
                moment( scheduleDateRange.start ),
                moment( scheduleDateRange.end ),
                undefined,
                '[]'
            );

        this.setState({
            date: newDate,
            weekDateFilter: null
        }, () => {
            if ( !isWithinWeek ) {
                this.fetchShiftsData();
            }
        });
    }

    headerDate() {
        const {
            viewName,
            date
        } = this.state;

        if ( viewName === CALENDAR_FREQUENCY.DAY ) {
            return moment( date ).format( 'dddd MMMM D, YYYY' );
        }

        return this.getWeekViewHeaderDate();
    }

    fetchShiftsData() {
        const {
            weekDateFilter,
            currentPage,
            perPage,
            search,
            date,
            filter
        } = this.state;

        let startDate = moment( date )
            .startOf( CALENDAR_FREQUENCY.WEEK ).format( 'YYYY-MM-DD' );
        let endDate = moment( date )
            .endOf( CALENDAR_FREQUENCY.WEEK ).format( 'YYYY-MM-DD' );

        let dateFilter = date;
        const filters = filter;

        let params = {
            filter_locations: this.props.filterData.locations,
            filter_departments: this.props.filterData.departments,
            start_date: startDate,
            end_date: endDate,
            page: currentPage,
            per_page: perPage,
            name: search,
            filters
        };

        if ( weekDateFilter ) {
            startDate = moment( weekDateFilter, 'YYYY-MM-DD' )
                .startOf( CALENDAR_FREQUENCY.WEEK )
                .format( 'YYYY-MM-DD' );
            endDate = moment( weekDateFilter, 'YYYY-MM-DD' )
                .endOf( CALENDAR_FREQUENCY.WEEK )
                .format( 'YYYY-MM-DD' );

            params = {
                ...params,
                start_date: startDate,
                end_date: endDate
            };

            dateFilter = weekDateFilter;
        }

        this.setState({
            scheduleDateRange: {
                start: startDate,
                end: endDate
            },
            date: dateFilter
        }, () => {
            this.props.getShifts( params );
        });
    }

    toggleFilter = () => {
        this.setState({
            showListFilter: !this.state.showListFilter
        });
    };

    applyFilters = ( filters ) => {
        let updatedState = { filter: filters, currentPage: 1 };

        if ( filters.week !== '' ) {
            updatedState = {
                ...updatedState,
                weekDateFilter: filters.week,
                date: filters.week
            };
        } else {
            updatedState = {
                ...updatedState,
                date: moment().format( 'YYYY-MM-DD' )
            };
        }

        this.setState({ ...updatedState }, () => {
            this.fetchShiftsData();
        });
    }

    generateMasterfile() {
        const { shifts } = this.props;
        const { currentMonth } = this.state;

        const month = moment( currentMonth, 'YYYY-MM' );
        const dateFrom = moment( month ).startOf( 'month' ).format( 'YYYY-MM-DD' );
        const dateTo = moment( month ).endOf( 'month' ).format( 'YYYY-MM-DD' );

        const shiftIDs = [];

        shifts.forEach(
            ( shiftData ) => shiftData.shifts.forEach( ( shiftId ) => shiftIDs.push( shiftId.id ) )
        );

        const generatePayload = {
            shifts_ids: shiftIDs,
            schedule_ids: [],
            location_ids: [],
            department_ids: [],
            status: ['Active'],
            date_from: dateFrom,
            date_to: dateTo
        };

        this.props.generateCSV( generatePayload );
    }

    handleSearch = ( value ) => {
        this.setState(
            { search: value, currentPage: 1 },
            () => {
                this.fetchShiftsData();
            }
        );
    }

    handlePageChange = ( value ) => {
        this.setState(
            { currentPage: value },
            () => {
                this.fetchShiftsData();
            }
        );
    }

    handlePageSizeChange = ( value ) => {
        this.setState(
            { currentPage: 1, perPage: value },
            () => {
                this.fetchShiftsData();
            }
        );
    }

    addShiftHandler = () => {
        this.fetchShiftsData();
    }

    handleChangeView = ( frequency ) => {
        this.setState({ viewName: frequency });
    }

    render() {
        const {
            search,
            hasFiltersApplied,
            showListFilter,
            label,
            viewName,
            date
        } = this.state;

        return (
            <div>
                <Helmet
                    title="Shift"
                    meta={ [
                        { name: 'description', content: 'Description of Shift' }
                    ] }
                />

                <SnackBar
                    message={ this.props.notification && this.props.notification.message }
                    title={ this.props.notification && this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification && this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification && this.props.notification.type }
                />

                <SubHeader items={ TIME_AND_ATTENDANCE_SUBHEADER_ITEMS } />

                {
                    this.props.loading
                    ?
                        <div className="loader">
                            <LoadingStyles>
                                <H2>Loading Shifts</H2>

                                <br />

                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                    : (
                        <div>
                            <PageWrapper>
                                <Container>
                                    <div className="content">
                                        <div className="heading">
                                            <H1>Shifts</H1>
                                        </div>

                                        <div>
                                            <div className="title">
                                                <div className="search-wrapper">
                                                    <Input
                                                        className="search"
                                                        id="search"
                                                        placeholder="Search for an employee"
                                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                                        value={ search }
                                                        onKeyPress={ ( e ) => {
                                                            e.charCode === 13 && this.handleSearch( e.target.value );
                                                        } }
                                                        addon={ {
                                                            content: <Icon name="search" />,
                                                            placement: 'right'
                                                        } }
                                                    />

                                                    <Button
                                                        id="button-filter"
                                                        label={ <span><Icon name="filter" className="filter-icon" /> Filter</span> }
                                                        onClick={ () => this.toggleFilter() }
                                                        className="btnFilter"
                                                    />
                                                </div>

                                                <div>
                                                    <Button
                                                        label={ <span>Export</span> }
                                                        type={ hasFiltersApplied ? 'primary' : 'neutral' }
                                                        onClick={ () => this.generateMasterfile() }
                                                    />

                                                    <Button
                                                        label="Assign Shifts"
                                                        type="action"
                                                        onClick={ () => this.navigateToManualPage() }
                                                    />
                                                </div>
                                            </div>

                                            { showListFilter && (
                                                <div>
                                                    <Filter
                                                        filterData={ this.props.filterData }
                                                        onCancel={ () => this.toggleFilter() }
                                                        onApply={ ( filters ) => { this.applyFilters( filters ); } }
                                                    />
                                                </div>
                                            ) }

                                            <div className="loading_section">
                                                { this.props.miniLoading && (
                                                    <div className="mini-loading">
                                                        Loading...
                                                    </div>
                                                ) }

                                                <CalendarHeader>
                                                    <CalendarHeaderDate>
                                                        <Button
                                                            label={ <span><Icon name="back" className="filter-icon" /></span> }
                                                            type="grey"
                                                            onClick={ () => this.changePage( false ) }
                                                        />

                                                        <Button
                                                            label={ <span><Icon name="next" className="filter-icon" /></span> }
                                                            type="grey"
                                                            onClick={ () => this.changePage( true ) }
                                                        />

                                                        <H2>{ this.headerDate() }</H2>
                                                    </CalendarHeaderDate>

                                                    <CalendarHeaderActions>
                                                        { label }

                                                        <CalendarHeaderActionGroup>
                                                            <Button
                                                                id="button-filter"
                                                                label="Day"
                                                                type="grey"
                                                                onClick={ () => this.handleChangeView( CALENDAR_FREQUENCY.DAY ) }
                                                                className={ viewName === CALENDAR_FREQUENCY.DAY ? 'active' : '' }
                                                            />

                                                            <Button
                                                                id="button-filter"
                                                                label="Week"
                                                                type="grey"
                                                                onClick={ () => this.handleChangeView( CALENDAR_FREQUENCY.WEEK ) }
                                                                className={ viewName === CALENDAR_FREQUENCY.WEEK ? 'active' : '' }
                                                            />
                                                        </CalendarHeaderActionGroup>
                                                    </CalendarHeaderActions>
                                                </CalendarHeader>

                                                <Calendar
                                                    fetchShiftsData={ () => this.addShiftHandler() }
                                                    employeeShifts={ this.props.shifts }
                                                    viewName={ viewName }
                                                    date={ date }
                                                    defaultSchedule={ this.props.defaultSchedule }
                                                    headerDate={ this.headerDate() }
                                                    list={ this.props.scheduleList }
                                                    addShift={ this.props.addShift }
                                                    addRestDay={ this.props.addRestDay }
                                                    unAssignShift={ this.props.unAssignShift }
                                                    unAssignRestDay={ this.props.unAssignRestDay }
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </Container>
                            </PageWrapper>

                            <div
                                className="loader"
                                style={ { display: this.props.loading ? 'none' : 'block', position: 'fixed', bottom: '0', background: '#fff', zIndex: '2' } }
                            >
                                <FooterTablePagination
                                    page={ this.props.pagination.current_page }
                                    pageSize={ this.props.pagination.per_page }
                                    pagination={ this.props.pagination }
                                    onPageChange={ this.handlePageChange }
                                    onPageSizeChange={ this.handlePageSizeChange }
                                    paginationLabel={ label }
                                />
                            </div>
                        </div>
                    )
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    defaultSchedule: makeSelectDefaultList(),
    shifts: makeSelectShifts(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    filterData: makeSelectFilterData(),
    scheduleList: makeSelectSchedule(),
    pagination: makeSelectPagination(),
    miniLoading: makeSelectMiniLoading(),
    currentCompany: makeSelectCurrentCompanyState()

});

// eslint-disable-next-line require-jsdoc
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        shiftActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Shift );
