/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
/* eslint-disable radix */
/* eslint-disable guard-for-in */
export const changeColorLuminance = ( color, lum ) => {
    const hex = rgb2hex( color ).replace( '#', '' );

    lum = lum || 0;

    // Convert to decimal and change luminosity
    let rgb = '#';
    let c;
    for ( let i = 0; i < 3; i++ ) {
        c = parseInt( hex.substr( i * 2, 2 ), 16 );
        c = Math.round( Math.min( Math.max( 0, c + lum ), 255 ) ).toString( 16 );
        rgb += ( `00${c}` ).substr( c.length );
    }

    return rgb;
};

const rgb2hex = ( color ) => {
    if ( color.indexOf( 'rgb' ) === -1 ) {
        let hex = String( color ).replace( /[^0-9a-f]/gi, '' );

        if ( hex.length < 6 ) {
            hex = hex[ 0 ] + hex[ 0 ] + hex[ 1 ] + hex[ 1 ] + hex[ 2 ] + hex[ 2 ];
        }

        return `#${hex}`;
    }

    const rgb = color.match( /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/ );

    return `#${hex( rgb[ 1 ])}${hex( rgb[ 2 ])}${hex( rgb[ 3 ])}`;
};

const hex = ( x ) => ( `0${parseInt( x ).toString( 16 )}` ).slice( -2 );

const colorLuminance = ( color ) => {
    const hexCode = rgb2hex( color ).replace( '#', '' );

    const colorRgb = {
        red: parseInt( hexCode.substr( 0, 2 ), 16 ),
        green: parseInt( hexCode.substr( 2, 2 ), 16 ),
        blue: parseInt( hexCode.substr( 4, 2 ), 16 )
    };

    // eslint-disable-next-line no-restricted-syntax
    for ( const name in colorRgb ) {
        let value = colorRgb[ name ] / 255;
        if ( value < 0.03928 ) {
            value /= 12.92;
        } else {
            // eslint-disable-next-line no-restricted-properties
            value = Math.pow( ( value + 0.055 ) / 1.055, 2 );
        }
        colorRgb[ name ] = value;
    }

    return colorRgb.red * 0.2126 + colorRgb.green * 0.7152 + colorRgb.blue * 0.0722;
};

export const isColorLuminant = ( color ) => colorLuminance( color ) > 0.55;
