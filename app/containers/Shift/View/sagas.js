/* eslint-disable import/no-duplicates */
/* eslint-disable no-undef */
/* eslint-disable require-jsdoc */
/* eslint-disable import/first */
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import {
    SET_LOADING,
    SET_MINI_LOADING,
    SET_DEFAULT_SCHEDULE,
    GET_DEFAULT_SCHEDULES,
    SET_SHIFTS,
    GET_SHIFTS,
    SET_NOTIFICATION,
    SET_FILTER_DATA,
    SET_SCHEDULE,
    GET_SCHEDULE,
    GENERATE_CSV,
    SET_PAGINATION,
    OVERLAP_SHIFT,
    ADD_SHIFT,
    GET_UNASSIGNED_SHIFT,
    SET_UNASSIGNED_SHIFT,
    ADD_REST_DAY,
    UNASSIGN_REST_DAY
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* getDefaultSchedules() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const response = yield call(
            Fetch,
            `/default_schedule?company_id=${companyId}`,
            { method: 'GET' }
        );

        yield put({
            type: SET_DEFAULT_SCHEDULE,
            payload: response.data || []
        });

        yield call( getSchedule );
    } catch ( err ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

export function* getShifts( payload ) {
    try {
        yield put({
            type: SET_MINI_LOADING,
            payload: true
        });

        const val = payload.payload;
        const companyId = yield call( company.getLastActiveCompanyId );

        let shiftendpoint = `/company/${companyId}/shifts_data?page=${val.page}&per_page=${val.per_page}`;
        const filter = {
            location: '',
            department: '',
            status: 'active',
            week: ''
        };

        if ( val.name ) {
            shiftendpoint += `&name=${val.name}`;
        }

        if ( val.start_date ) {
            shiftendpoint += `&start_date=${val.start_date}`;
        }

        if ( val.end_date ) {
            shiftendpoint += `&end_date=${val.end_date}`;
        }

        if ( val.filters.status ) {
            shiftendpoint += `&filter[status]=${val.filters.status}`;
            filter.status = val.filters.status;
        }

        if ( val.filters.location ) {
            for ( let l = 0; l < val.filters.location.length; l += 1 ) {
                shiftendpoint += `&filter[location][]=${val.filters.location[ l ].id}`;
            }
            filter.location = val.filters.location;
        }

        if ( val.filters.department ) {
            for ( let d = 0; d < val.filters.department.length; d += 1 ) {
                shiftendpoint += `&filter[department][]=${val.filters.department[ d ].id}`;
            }
            filter.department = val.filters.department;
        }

        if ( val.filters.week ) {
            filter.week = val.filters.week;
        }

        const response = yield call( Fetch, shiftendpoint, { method: 'GET', val });
        const { data, ...others } = response;

        yield put({
            type: SET_PAGINATION,
            payload: others
        });

        yield put({
            type: SET_SHIFTS,
            payload: data
        });

        const filterData = {
            filter,
            locations: val.filter_locations,
            departments: val.filter_departments
        };

        if ( !val.filter_locations ) {
            const [ locations, departments ] = yield [
                call( Fetch, `/company/${companyId}/time_attendance_locations?mode=DEFAULT_MODE&name=`, { method: 'GET' }),
                call( Fetch, `/company/${companyId}/departments?mode=DEFAULT_MODE`, { method: 'GET' })
            ];
            filterData.locations = locations.data;
            filterData.departments = departments.data;
        }

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });

        yield put({
            type: SET_MINI_LOADING,
            payload: false
        });
    } catch ( err ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });

        yield put({
            type: SET_MINI_LOADING,
            payload: false
        });
    }
}

export function* generateCSV({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const { file_name: fileName } = yield call( Fetch, `/company/${companyId}/shifts/generate_csv`, {
            method: 'POST',
            data: payload
        });

        const link = document.createElement( 'a' );
        link.href = fileName;
        link.setAttribute( 'download', '' );

        document.body.appendChild( link );

        link.click();

        document.body.removeChild( link );
    } catch ( error ) {
        let message = get( error, 'response.data.message' ) || '';
        message = message.toLowerCase().includes( 'the shifts ids field is required' )
            ? 'There are no shifts that can be exported.'
            : '';

        yield call( notifyUser, {
            show: true,
            title: get( error, 'response.statusText' ) || 'Error',
            message: message || error.statusText,
            type: 'error'
        });
    }
}

export function* getSchedule() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/schedules`, { method: 'GET' });

        yield put({
            type: SET_SCHEDULE,
            payload: response.data || []
        });
    } catch ( err ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* getOverLap( payload ) {
    try {
        const data = payload.payload;

        yield call( Fetch, '/shift/overlapping_requests', {
            method: 'POST',
            data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

export function* addShift({ payload, successCallback }) {
    try {
        yield put({
            type: SET_MINI_LOADING,
            payload: true
        });

        yield call( Fetch, '/shift', {
            method: 'POST',
            data: payload
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Shift successfully assigned!',
            show: true,
            type: 'success'
        });

        yield call( successCallback );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_MINI_LOADING,
            payload: false
        });

        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* addRestDay({ payload, successCallback }) {
    try {
        yield put({
            type: SET_MINI_LOADING,
            payload: true
        });

        const data = payload;

        yield call( Fetch, '/rest_day', {
            method: 'POST',
            data
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Rest Day successfully assigned!',
            show: true,
            type: 'success'
        });

        yield call( successCallback );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* unAssignShift({ payload, successCallback }) {
    try {
        yield put({
            type: SET_MINI_LOADING,
            payload: true
        });

        yield call( Fetch, `/shift/unassign/${payload.id}`, { method: 'PUT', data: payload.data });

        yield put({
            type: SET_UNASSIGNED_SHIFT,
            payload: false
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Shift unassigned!',
            show: true,
            type: 'success'
        });

        yield call( successCallback );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* unAssignRestDay({ payload, successCallback }) {
    try {
        yield put({
            type: SET_MINI_LOADING,
            payload: true
        });

        yield call( Fetch, `/rest_day/unassign/${payload.id}`, {
            method: 'POST',
            data: payload.data
        });

        yield call( notifyUser, {
            title: 'Unassigned',
            message: 'Shift unassigned!',
            show: true,
            type: 'success'
        });

        yield call( successCallback );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 3000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

export function* reinitializePage() {
    yield call( resetStore );

    yield call( getDefaultSchedules );
}

export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForGetDefaultSchedules() {
    const watcher = yield takeEvery( GET_DEFAULT_SCHEDULES, getDefaultSchedules );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForGetShifts() {
    const watcher = yield takeEvery( GET_SHIFTS, getShifts );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchNotify() {
    const watcher = yield takeEvery( GET_NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForGetSchedule() {
    const watcher = yield takeEvery( GET_SCHEDULE, getSchedule );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForGenerateCsv() {
    const watcher = yield takeEvery( GENERATE_CSV, generateCSV );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForOverLap() {
    const watcher = yield takeEvery( OVERLAP_SHIFT, getOverLap );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForAddShift() {
    const watcher = yield takeEvery( ADD_SHIFT, addShift );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForAddRestday() {
    const watcher = yield takeEvery( ADD_REST_DAY, addRestDay );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForUnassignShift() {
    const watcher = yield takeEvery( GET_UNASSIGNED_SHIFT, unAssignShift );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForUnassignRestday() {
    const watcher = yield takeEvery( UNASSIGN_REST_DAY, unAssignRestDay );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetDefaultSchedules,
    watchForGetShifts,
    watchForGetSchedule,
    watchForGenerateCsv,
    watchForOverLap,
    watchForAddShift,
    watchForAddRestday,
    watchForUnassignShift,
    watchForUnassignRestday
];
