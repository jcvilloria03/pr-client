/* eslint-disable require-jsdoc */
import 'moment-recur';
import {
    DEFAULT_ACTION,
    GET_DEFAULT_SCHEDULES,
    GET_SHIFTS,
    GENERATE_CSV,
    GET_SCHEDULE,
    OVERLAP_SHIFT,
    ADD_SHIFT,
    GET_UNASSIGNED_SHIFT,
    ADD_REST_DAY,
    UNASSIGN_REST_DAY
} from './constants';

/**
 *
 * Shift actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function getDefaultSchedules() {
    return {
        type: GET_DEFAULT_SCHEDULES
    };
}

export function getShifts( payload ) {
    return {
        type: GET_SHIFTS,
        payload
    };
}

export function generateCSV( payload ) {
    return {
        type: GENERATE_CSV,
        payload
    };
}

export function getSchedule() {
    return {
        type: GET_SCHEDULE
    };
}

export function getOverLap( payload ) {
    return {
        type: OVERLAP_SHIFT,
        payload
    };
}

export function addShift( payload, successCallback ) {
    return {
        type: ADD_SHIFT,
        payload,
        successCallback
    };
}

export function addRestDay( payload, successCallback ) {
    return {
        type: ADD_REST_DAY,
        payload,
        successCallback
    };
}

export function unAssignShift( payload, successCallback ) {
    return {
        type: GET_UNASSIGNED_SHIFT,
        payload,
        successCallback
    };
}
export function unAssignRestDay( payload, successCallback ) {
    return {
        type: UNASSIGN_REST_DAY,
        payload,
        successCallback
    };
}
