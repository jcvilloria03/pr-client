import { createSelector } from 'reselect';

/**
 * Direct selector to the shift state domain
 */
const selectShiftDomain = () => ( state ) => state.get( 'shift' );

/**
 * Default selector used by Shift
 */

const makeSelectDefaultList = () => createSelector(
  selectShiftDomain(),
  ( substate ) => substate.get( 'defaultschedule' ).toJS()
);

const makeSelectShifts = () => createSelector(
  selectShiftDomain(),
  ( substate ) => substate.get( 'shifts' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectShiftDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectMiniLoading = () => createSelector(
  selectShiftDomain(),
  ( substate ) => substate.get( 'miniLoading' )
);

const makeSelectNotification = () => createSelector(
  selectShiftDomain(),
  ( substate ) => substate && substate.get( 'notification' ).toJS()
);

const makeSelectFilterData = () => createSelector(
  selectShiftDomain(),
( substate ) => substate && substate.get( 'filterData' ).toJS()
);

const makeSelectSchedule = () => createSelector(
  selectShiftDomain(),
  ( schedules ) => schedules.get( 'schedule' ).toJS()
);

const makeSelectPagination = () => createSelector(
  selectShiftDomain(),
( substate ) => substate.get( 'pagination' ).toJS()
);

// export default makeSelectShift;
export {
  makeSelectDefaultList,
  makeSelectShifts,
  makeSelectLoading,
  makeSelectNotification,
  makeSelectFilterData,
  makeSelectSchedule,
  makeSelectPagination,
  makeSelectMiniLoading
};
