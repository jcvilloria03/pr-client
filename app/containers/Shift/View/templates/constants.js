export const FILTER_TYPES = {
    LOCATION: 'location',
    POSITION: 'position',
    DEPARTMENT: 'department',
    SCHEDULED_TYPE: 'scheduled_type'
};

export const filterData =
    {
        status: [
            { name: 'active', label: 'Active' },
            { name: 'semi-active', label: 'Semi-Active' },
            { name: 'inactive', label: 'Inactive' }
        ]
    };

