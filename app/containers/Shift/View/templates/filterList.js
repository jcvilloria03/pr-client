/* eslint-disable jsx-a11y/label-has-for, import/first,jsx-a11y/no-static-element-interactions */
import React from 'react';
import Button from 'components/Button';
import Select from 'components/Select';
import MultiSelect from 'components/MultiSelect';
import $ from 'jquery';
import { FilterWrapper } from './styles';
import { filterData } from './constants';
import Flatpickr from 'react-flatpickr';
import 'flatpickr/dist/themes/material_green.css';
/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {
    static propTypes = {
        // eslint-disable-next-line react/no-unused-prop-types
        filterData: React.PropTypes.shape({
            filter: React.PropTypes.object,
            positions: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    constructor( props ) {
        super( props );
        this.state = {
            filter: this.props.filterData.filter
        };

        this.onApply = this.onApply.bind( this );
    }

    onApply = () => {
        this.props.onApply( this.state.filter );
        this.props.onCancel();
    }

    onCancel = () => {
        this.props.onCancel();
    }

    getFilterPropsValues( filterValues ) {
        if ( !filterData[ filterValues ]) {
            return [];
        }
        return filterData[ filterValues ].map( ( filterValue ) => (
            this.formatDataForSelect( filterValue )
        ) );
    }

    getLocations = () => {
        if ( !this.props.filterData.locations ) {
            return [];
        }

        const locations = this.props.filterData.locations.map( ( location ) => (
            this.formatDataForMultiselect( location )
        ) );
        return locations;
    }

    getDepartments = () => {
        if ( !this.props.filterData.departments ) {
            return [];
        }

        const departments = this.props.filterData.departments.map( ( department ) => (
            this.formatDataForMultiselect( department )
        ) );

        return departments;
    }

    formatDataForMultiselect = ( data ) => (
        {
            id: data.id,
            value: data.name,
            label: data.name,
            disabled: false
        }
    )

    formatDataForSelect = ( data ) => (
        {
            value: data.name,
            label: data.label,
            disabled: false
        }
    )

    retrieveFilters = ( defaultFilters ) => {
        if ( defaultFilters ) {
            this.setState({ defaultFilters }, () => {
                this.onApply();
            });
        }
    }

    handleChange = ( name, value ) => {
        this.setState( ( prevState ) => ({
            ...prevState,
            filter: {
                ...prevState.filter,
                [ name ]: value
            }
        }) );
    }

    resetFilters = () => {
        const filter = {
            location: '',
            department: '',
            status: 'active',
            week: ''
        };
        this.setState({ filter });
        this.props.onApply( filter );
        this.props.onCancel();
    }

    handleDateStyle = () => {
        $( '.dayContainer' ).addClass( 'style_date_picker' );
    }

    render() {
        const filterParamsNames = {
            status: 'status'
        };
        return (
            <FilterWrapper>
                <div className="filterMain">
                    <div className="row filterRow">
                        <div className="col-xs-3 colSect">
                            <Select
                                label="Status"
                                placeholder="Status"
                                value={ this.state.filter.status }
                                data={ this.getFilterPropsValues( filterParamsNames.status ) }
                                onChange={ ( eve ) => this.handleChange( 'status', eve.value ) }
                            />
                        </div>
                        <div className="col-xs-3 colSect">
                            <MultiSelect
                                id="locations"
                                label="Locations"
                                placeholder="Select locations"
                                value={ this.state.filter.location }
                                data={ this.getLocations() }
                                onChange={ ( eve ) => this.handleChange( 'location', eve ) }
                            />
                        </div>
                        <div className="col-xs-3 colSect">
                            <MultiSelect
                                id="departments"
                                label="Departments"
                                placeholder="Select departments"
                                value={ this.state.filter.department }
                                data={ this.getDepartments() }
                                onChange={ ( eve ) => this.handleChange( 'department', eve ) }
                            />
                        </div>
                        <div className="col-xs-3 colSect date-picker filter_date" onClick={ () => this.handleDateStyle() }>
                            <label>Week</label>
                            <Flatpickr
                                label="Week"
                                placeholder="Select a date"
                                value={ this.state.filter.week }
                                selectedDay={ this.state.endDate }
                                onChange={ ( eve ) => this.handleChange( 'week', eve[ 0 ]) }
                            />
                        </div>
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
