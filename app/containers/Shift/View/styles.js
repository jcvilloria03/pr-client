import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 400px;
`;

export const PageWrapper = styled.div`
    .content {
        margin-bottom: 80px;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 10px auto;
            padding-top: 32px;

            h1 {
                font-size: 24px;
                font-weight: 600;
                margin-bottom: 0;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .pagination {
            display:flex;
            justify-content:space-between;

            .display_page {
                select {
                    margin:0 5px;
                    height: 2.5rem;
                    min-width: 5rem;
                    border: 1px solid #474747;
                    padding: 5px;
                }
            }

            .current_page {
                select {
                    margin:0 5px;
                    height: 2.5rem;
                    min-width: 5rem;
                    border: 1px solid #474747;
                    padding: 5px;
                }

                button {
                    margin-left:15px
                }
            }
        }
        .main {
            .btn {
                min-width: 140px;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;

            .btnFilter {
                background-color: transparent;
                color: #474747;
                border-color: #9fdc74;
                border-radius: 2px;
            }

            h5 {
                margin: 0;
                margin-right: 10px;
                font-weight: 600;
                font-size: 20px;
            }

            .search-wrapper {
                flex-grow: 1;
                display: flex;
                justify-content: flex-start;
                align-items: center;

                .search {
                    width: 298px;
                    border: 1px solid #adadad;
                    border-radius: 2px;
                    height: 37px;
                    line-height: 37px;
                    padding: 0 9px;

                    input {
                        border: none;
                        height: 36px;
                    }

                    .input-group-addon {
                        padding: 0;
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                    padding-right: 5px;
                    padding-left: 0;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;

                    svg {
                        margin-top: -16px;
                    }
                }
            }
        }
    }

    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }

    .no-license-error {
        color: #e13232;
    }

    button.dCEVJQ.btn.btn-secondary.btn-default {
        border-radius: 2px;
    }

    button.bAaddA.btn.btn-secondary.btn-default {
        border-radius: 2px;
    }

    span.isvg.loaded.filter-icon svg {
        margin-top: 0px !important;
    }

    .loading_section {
        position: relative;
    }

    .mini-loading {
        display: flex;
        background: rgba(255,255,255,0.8);
        justify-content: center; 
        align-items: center;
        position: absolute;
        top: 0; 
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 3;
        -webkit-transition: all 0.3s ease;
        transition: all 0.3s ease;
    }
`;

export const CalendarHeader = styled.div`
    display: flex;
    align-items: center;
    padding: 1rem;
    border: 1px solid #ccc;
    border-bottom: none;
    justify-content: space-between;
`;

export const CalendarHeaderDate = styled.div`
    display: flex;
    align-items: center;

    h2 {
        margin: 0 0 0 1rem;
        font-size: 1.3rem;
        font-weight: bold;
    }

    button {
        border-color: #ccc;
        border-radius: 0.25rem;
        background-color: transparent;
        padding: 0.25rem 1rem;
        font-size: .9rem;

        &:first-child{
            margin-right: 1rem; 
        }
    }
`;

export const CalendarHeaderActions = styled.div`
    position: relative;
`;

export const CalendarHeaderActionGroup = styled.div`
    display: inline-flex;
    justify-content: flex-start;
    align-items: stretch;
    margin-left: 1rem;

    button {
        padding: 0.25rem 1rem;
        font-size: .9rem;
        border-color: #ccc;
        background-color: transparent;
        margin: 0;
        border-radius: 0;

        &:focus {
            outline:none;
        }

        &:first-child {
            border-right: none;
            border-top-left-radius: 0.25rem;
            border-bottom-left-radius: 0.25rem;
        }

        &:last-child {
            border-left: none;
            border-top-right-radius: 0.25rem;
            border-bottom-right-radius: 0.25rem;
        }
    }

    .active {
        color: #fff;
        background-color: #00a5e5;
    }
`;
