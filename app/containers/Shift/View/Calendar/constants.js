export const SCHEDULES_DISPLAY = {
    DEFAULT: 'YYYY-MM-DD'
};

export const TIME_FORMATS = {
    DEFAULT: 'HH:mm'
};

export const SCHEDULE_TYPES = {
    flexi: 'Flexi Schedule',
    fixed: 'Fixed Schedule'
};

export const DATE_FORMATS = {
    DEFAULT: 'YYYY-MM-DD',
    HOLIDAYS_DISPLAY: 'MMMM DD, YYYY',
    REQUEST_END_DATE_DISPLAY: 'DD, YYYY',
    SCHEDULES_DISPLAY: 'MM/DD/YYYY',
    SHIFTS_DISPLAY: 'MMMM D, YYYY',
    DATE_TIME: 'MMMM DD, YYYY - HH:mm',
    MESSAGING: 'HH:mm MMMM DD, YYYY',
    CLOCK: 'HH:mm:ss',
    HOURS_AND_MINUTES: 'HH:mm'
};

export const DAYS_OF_WEEK = {
    1: 'Monday',
    2: 'Tuesday',
    3: 'Wednesday',
    4: 'Thursday',
    5: 'Friday',
    6: 'Saturday',
    7: 'Sunday'
};
