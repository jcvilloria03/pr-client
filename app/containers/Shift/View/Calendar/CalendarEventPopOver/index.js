/* eslint-disable import/first, no-undef */

import React from 'react';
import moment from 'moment';
import { browserHistory } from 'utils/BrowserHistory';
import $ from 'jquery';

import { H6, H4 } from 'components/Typography';
import Button from 'components/Button';
import Modal from 'components/Modal';
import Icon from 'components/Icon';

import {
    EventViewWrapper,
    ModalWrapper
} from './styles';

import { CloseButton } from '../styles';

/**
 *
 * CalendarEventPopOver Component
 *
 */
class CalendarEventPopOver extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        singleEvent: React.PropTypes.object,
        closeEventPopOver: React.PropTypes.func,
        unAssignShift: React.PropTypes.func,
        unAssignRestDay: React.PropTypes.func,
        fetchShiftsData: React.PropTypes.func
    };

    navigateToDefaultSchedulePage() {
        browserHistory.push( '/company-settings/schedule-settings/default-schedule', true );
    }

    navigateToEditShiftPage( id ) {
        browserHistory.push( `/time/shifts/edit-shift/${id}`, true );
    }

    handleModal() {
        this.chatModal.toggle();
        setTimeout( () => {
            $( '.change_width_of_modal' ).parents( '.modal-dialog' ).addClass( 'width_of_modal' );
        }, 300 );
    }

    unAssignOne() {
        const data = {
            start: this.props.singleEvent.val.date,
            end: this.props.singleEvent.val.date,
            target: ''
        };
        const id = this.props.singleEvent.val.id;

        this.props.unAssignShift({ data, id }, () => {
            this.props.fetchShiftsData();
        });

        this.props.closeEventPopOver();
    }

    unAssignRestDayOne() {
        const id = this.props.singleEvent.val.id;
        const data = {
            end: this.props.singleEvent.val.date,
            start: this.props.singleEvent.val.date
        };

        this.props.unAssignRestDay({ data, id }, () => {
            this.props.fetchShiftsData();
        });

        this.props.closeEventPopOver();
    }

    unAssignRestDayAllDay() {
        const id = this.props.singleEvent.val.id;
        const data = {
            target: 'all'
        };

        this.props.unAssignRestDay({ data, id }, () => {
            this.props.fetchShiftsData();
        });

        this.props.closeEventPopOver();
    }

    unAssignFollowingRestDay() {
        const id = this.props.singleEvent.val.id;
        const data = {
            start: this.props.singleEvent.val.start
        };
        this.props.unAssignRestDay({ data, id }, () => {
            this.props.fetchShiftsData();
        });

        this.props.closeEventPopOver();
    }

    unAssignTodayFollowingDay() {
        const id = this.props.singleEvent.val.id;
        const data = {
            end: this.props.singleEvent.val.end,
            start: this.props.singleEvent.val.date,
            target: 'upcoming'
        };

        this.props.unAssignShift({ data, id }, () => {
            this.props.fetchShiftsData();
        });

        this.props.closeEventPopOver();
    }

    unAssignAllDay() {
        const id = this.props.singleEvent.val.id;
        const data = {
            end: this.props.singleEvent.val.end,
            start: this.props.singleEvent.val.start,
            target: 'all'
        };

        this.props.unAssignShift({ data, id }, () => {
            this.props.fetchShiftsData();
        });

        this.props.closeEventPopOver();
    }

    render() {
        const eventValue = this.props.singleEvent.val;
        return (
            <EventViewWrapper>
                <Modal
                    className="mainchat"
                    ref={ ( ref ) => {
                        this.chatModal = ref;
                    } }
                    size="md"
                    title={ eventValue.restDay === true ? 'Unassign Rest Day' : 'Unassign Shift' }
                    body={
                        <ModalWrapper>
                            <div className="change_width_of_modal">
                                <div className="message">
                                        Would you like to unassign { eventValue.restDay === true ? 'rest day' : 'shift' } just for this day, today and all following days, or for all days?
                                    </div>
                                <div>
                                    <Button
                                        className="button"
                                        label="Just this day"
                                        type="neutral"
                                        onClick={ () => { eventValue.restDay ? this.unAssignRestDayOne() : this.unAssignOne() } }
                                    />
                                </div>
                                <div>
                                    <Button
                                        className="button"
                                        label="Today and all following days"
                                        type="neutral"
                                        onClick={ () => { eventValue.restDay ? this.unAssignFollowingRestDay() : this.unAssignTodayFollowingDay() } }
                                    />
                                </div>
                                <div>
                                    <Button
                                        className="button"
                                        label="All days"
                                        type="neutral"
                                        onClick={ () => { eventValue.restDay ? this.unAssignRestDayAllDay() : this.unAssignAllDay() } }
                                    />
                                </div>
                            </div>
                        </ModalWrapper>
                    }
                    buttons={ [] }
                />
                {eventValue.restDay === true ? (
                        eventValue.original ? (
                            <div>
                                <div className="sl-c-popover">
                                    <div className="sl-c-popover__header">
                                        <H6 className="sl-c-popover__title">Rest Day Info</H6>
                                        <div className="sl-c-actions">
                                            <CloseButton onClick={ this.props.closeEventPopOver }>
                                                <Icon name="remove" className="icon" />
                                            </CloseButton>
                                        </div>
                                    </div>
                                </div>

                                <div className="flexidetail">
                                    <H4>Rest days:</H4>
                                    <p>{ moment( eventValue.date ).format( 'dddd' )}</p>
                                    <H4>Repeats:</H4>
                                    <p>Weekly</p>
                                    <H4>Effective start date:</H4>
                                    <p>{ moment( eventValue.original.start_date ).format( 'MMMM D, YYYY' )}</p>
                                    <H4>Shift effective end date:</H4>
                                    <p>{ moment( eventValue.original.end_date ).format( 'MMMM D, YYYY' )}</p>
                                </div>

                                <div className="btnFlexi d-flex justify-content-center">
                                    <button onClick={ () => this.handleModal() } >Unassign</button>
                                    <button className="btnEdit ml-1" onClick={ () => browserHistory.push( `/time/shifts/edit-rest-day/${eventValue.id}`, true ) } >Edit</button>
                                </div>
                            </div>
                        ) : (
                            <div>
                                <div className="sl-c-popover">
                                    <div className="sl-c-popover__header">
                                        <H6 className="sl-c-popover__title">Rest Day Info</H6>

                                        <div className="sl-c-actions">
                                            <CloseButton onClick={ this.props.closeEventPopOver }>
                                                <Icon name="remove" className="icon" />
                                            </CloseButton>
                                        </div>
                                    </div>
                                </div>

                                <div className="flexidetail">
                                    <H4>Rest days:</H4>
                                    <p>{ moment( eventValue.date ).format( 'dddd' )}</p>
                                    <H4>Repeats:</H4>
                                    <p>Weekly</p>
                                    <H4>Effective start date:</H4>
                                    <p>N/A</p>
                                    <H4>Effective end date:</H4>
                                    <p>N/A</p>
                                </div>

                                <div className="btnFlexi d-flex justify-content-center">
                                    <button className="btnEdit ml-1" onClick={ () => this.navigateToDefaultSchedulePage() }>
                                        Edit
                                    </button>
                                </div>
                            </div>
                        )
                    )
                    :
                    (
                        eventValue.name === 'Default Schedule' ? (
                            <div>
                                <div className="sl-c-popover">
                                    <div className="sl-c-popover__header">
                                        <H6 className="sl-c-popover__title">Shift Info</H6>

                                        <div className="sl-c-actions">
                                            <CloseButton onClick={ this.props.closeEventPopOver }>
                                                <Icon name="remove" className="icon" />
                                            </CloseButton>
                                        </div>
                                    </div>
                                </div>

                                <div className="flexidetail">
                                    <H4>Shift start and end time:</H4>
                                    <p>{ eventValue.startTime } to { eventValue.endTime }</p>
                                    <H4>Shift effective start date:</H4>
                                    <p>N/A</p>
                                    <H4>Shift effective end date:</H4>
                                    <p>N/A</p>
                                    <H4>Schedule name:</H4>
                                    <p>{ eventValue.name }</p>
                                    <H4>Schedule type:</H4>
                                    <p>Fixed</p>
                                    <H4>Tags:</H4>
                                    <p>None</p>
                                </div>

                                <div className="btnFlexi d-flex justify-content-center">
                                    <button className="btnEdit ml-1" onClick={ () => this.navigateToDefaultSchedulePage() } >Edit</button>
                                </div>
                            </div>
                        ) : (
                            <div>
                                <div className="sl-c-popover">
                                    <div className="sl-c-popover__header">
                                        <H6 className="sl-c-popover__title">Shift Info</H6>

                                        <div className="sl-c-actions">
                                            <CloseButton onClick={ this.props.closeEventPopOver }>
                                                <Icon name="remove" className="icon" />
                                            </CloseButton>
                                        </div>
                                    </div>
                                </div>

                                <div className="flexidetail">
                                    <H4>Shift start and end time:</H4>
                                    <p>{ eventValue.startTime } to { eventValue.endTime }</p>
                                    <H4>Shift effective start date:</H4>
                                    <p>{moment( eventValue.start ).format( 'MMMM D, YYYY' )}</p>
                                    <H4>Shift effective end date:</H4>
                                    <p>{moment( eventValue.end ).format( 'MMMM D, YYYY' )}</p>
                                    <H4>Schedule name:</H4>
                                    <p>{ eventValue.name }</p>
                                    <H4>Schedule Type:</H4>
                                    <p className="flexi_type">{ eventValue.schedule.type === 'flexi' ? 'Flexibile' : eventValue.schedule.type }</p>
                                    <H4>Tags:</H4>
                                    <p>{ eventValue.schedule.tags.length ? eventValue.schedule.tags.map( ( i ) => i.name ).join( ', ' ) : 'None' }</p>
                                </div>
                                <div className="btnFlexi d-flex justify-content-center">
                                    <button onClick={ () => this.handleModal() } >Unassign</button>
                                    <button className="btnEdit ml-1" onClick={ () => this.navigateToEditShiftPage( eventValue.id ) }>Edit</button>
                                </div>
                            </div>
                        )
                    )
                }
            </EventViewWrapper>
        );
    }
}

export default CalendarEventPopOver;
