import styled from 'styled-components';

export const EventViewWrapper = styled.div`
    .sl-c-popover--left {
        &:before {
            right: -8px;
            left: auto;
            border-width: 7.5px 0 7.5px 8px;
            border-color: transparent transparent transparent #fff;
        }
    }

    .sl-c-popover__header {
        display: flex;
        align-items: center;
        justify-content: space-between;
        margin-bottom: 1rem;
        padding-bottom: 1rem;
        border-bottom: 1px solid #ccc;

        .sl-c-actions {
            position: relative;
            svg {
                color: #ccc;
                height: 12px;
                width: 12px;
            }
            button:focus {
                outline: none;
            }
        }
    }

    .sl-c-popover__title {
        font-family: 'sourcesanspro-bold', sans-serif;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;

        &.sl-c-popover__title {
            margin-bottom: 0;
        }
    }

    .sl-c-popover__body {
        +.sl-c-popover__footer {
            margin-top: 1rem;
        }
    }

    .sl-c-popover__footer {
        display: flex;
        justify-content: flex-end;
    }

    .flexidetail .flexi_type {
        text-transform: capitalize;
    }
`;

export const ModalWrapper = styled.div`
    .message{
        margin-bottom: 20px;
    }
    .button{
        width: 100%;
        margin-bottom: 4px;
        font-size: 16px;
    }
    .chat_mainmodal{
        height: 300px;
        max-height: 300px;
        overflow-y: auto;
        .message{
            p{
                font-size: 14px;
                text-align: end;
                color: #9d9d9d;
                margin-right: 15px;
            }
            :last-child{
                border: 1px solid #e8e8e8;
                width: fit-content;
                margin-left: auto;
                padding: 2px;
                color:#000;
            }
            .chatIcon{
                position:relative;
                &:after,&:before{
                    position: absolute;
                    width: 0;
                    height: 0;
                    border-style: solid;
                    content: "";
                }
                &:after{
                    right: -12px;
                    top: -1px;
                    left: auto;
                    z-index: 1;
                    border-width: 0 0 7px 12px;
                    border-color: transparent transparent transparent #f0f4f6;
                }
                &:before{
                    right: -10px;
                    top: 0px;
                    left: auto;
                    z-index: 2;
                    border-width: 0 0 5px 10px;
                    border-color: transparent transparent transparent #fff;
                }
            }
        }
    }
    .chat_modalInput{
        position: relative !important;
        input{
            border: 1px solid #545353 !important;
            border-radius: 50px !important;
            outline: none !important;
            margin-top: 20px;
        }
        :focus-visible{
            border: 1px solid #00a5e5 !important;
        }
        .sendicon{
            position: absolute !important;
            top: 50% !important;
            right: 5% !important;
            transform: translate(0%, -18%) !important;
            border:none;
            width:15px;
            color: green;
        }
    }
`;
