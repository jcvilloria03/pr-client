import styled, { css } from 'styled-components';

const getScheduleHeight = ( shiftCount ) => {
    if ( shiftCount >= 4 ) {
        return 12;
    }

    if ( shiftCount === 3 ) {
        return 14;
    }

    if ( shiftCount === 2 ) {
        return 16;
    }

    return 32;
};

export const assignPopOverWrapperStyles = css`
    position: absolute;
    padding: 1rem;
    filter: drop-shadow(0 0 .1rem #474747);
    background-color: #ffffff;
    border-radius: 0.5rem;
    width: 25rem;
    z-index: 20;

    &.left {
        &:before {
            right: -4px;
            left: auto;
            border-width: 7.5px 0 7.5px 8px;
            border-color: transparent transparent transparent #ffffff;
        }
    }

    &:before {
        position: absolute;
        left: -4px;
        top: 50%;
        transform: translateY(-7px);
        display: block;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 7.5px 8px 7.5px 0;
        border-color: transparent #fff transparent transparent;
        content: '';
    }

    .sl-c-popover {
        h6 {
            color: #474747;
            font-size: 14px;
            font-weight: bold;
        }
    }

    .flexidetail {
        h4 {
            font-size: 14px;
            font-weight: bold;
            margin-bottom: 0;
        }

        p {
            font-size: 13px;
            margin-bottom: 20px;
            color: #474747;
        }
    }

    .btnFlexi{
        display: flex;
        align-items: center;
        justify-content: flex-end;
    
        button {
            background-color: #ec7575;
            border-radius: 24px;
            color: #fff;
            font-weight: 600;
            padding: 8px 20px;
            font-size: 12px;
            cursor: pointer;

            &:focus{
                outline: 0;
            }
        }

        .btnEdit {
            background-color: #84d24b;
        }
    }
`;

export const Wrapper = styled.div`
    .fc-unthemed .fc-today{
        background-color: #5e7d8d1a;
    }

    .sl-c-btn--primary.sl-c-btn--ghost {
        color: #474747;
        border-color: #83d24b;
        background-color: #fff;
    }

    .pop-over-wrapper {
        ${assignPopOverWrapperStyles}
    }
            
`;

export const CalendarWrapper = styled.div`
    display: flex;
    border: 1px solid #ccc;
    margin-bottom: 4rem;

    .fc-view {
        z-index: 0;
    }

    .fc-agendaDay-view {
        .fc-time-grid-event.fc-v-event.fc-event {
            margin-right: 5px;
            margin-top: 1px;
            padding: .25rem;
            border: none;
        }
    }

    .fc-agendaWeek-view {
        .fc-time-grid-event.fc-v-event.fc-event {
            margin-right: 2px;
            margin-top: 1px;
            padding: .25rem;
            border: none;
        }
    }

    .fc-toolbar {
        border: 1px solid #ddd;
        margin-bottom: 0;
        padding: 15px;
        border-bottom: 0;

        .fc-left {
                align-items: center;
                display: flex;
            h2{
                font-size: 21px;
                color: #474747;
                font-weight:700;
            }
        }

        .fc-state-default{
            text-shadow: none;
            background-color: transparent;
            background-image: none;
            border-color: #ccc;
            box-shadow: none;
            height: 30px;
            font-size:14px;
            .fc-icon{
                &::after{
                    font-size: 26px;
                }
            }
            &:focus{
                outline:0;
            }
        }

        .fc-state-active{
            color: #fff;
            background-color: #00a5e5;
        }
    }

    .fc-view-container{
        table{
            thead{
                .fc-day-header{
                    background-color: #5e7d8d1a;
                }
                .fc-axis{
                    background-image: url(data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyMiAyMiI+PGcgaWQ9Im5hdmlnYXRpb24iPjxnIGlkPSJzY2hlZHVsZSI+PHBhdGggaWQ9IkNvbWJpbmVkLVNoYXBlIiBkPSJNMS40MSwxMmExMSwxMSwwLDEsMSwxMSwxMUExMSwxMSwwLDAsMSwxLjQxLDEyWk0xMyw2LjVIMTEuMzFWMTNMMTcsMTYuNGwuODgtMS40TDEzLDEyLjFaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMS40MSAtMSkiLz48L2c+PC9nPjwvc3ZnPg==);
                    background-repeat: no-repeat;
                    background-position: 50%;
                    background-size: .9rem .9rem;
                }
            }
            tbody{
                td.fc-day.fc-past {
                    background-color: #EEEEEE;
                }
                .fc-slats{
                    .fc-time{
                        span{
                            font-size:14px;
                        }
                    }
                }
            }
        }
    }
`;

export const CalendarEmployeesListHeading = styled.div`
    color: #fff;
    font-weight: 700;
    background-color: #00a5e5;
    height: 50px;
    padding: 12px 16px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
    display: flex;
    align-self: center;
    gap: 8px;

    svg {
        width: 16px;
        height: 16px;
        fill: currentColor;
    }
`;

export const CalendarEmployeesListWrapper = styled.div`
`;

export const CalendarEmployeesListItem = styled.li`
    height: 60px;
    padding: 0 1rem;
    display: flex;
    align-items: center;
    overflow: hidden;
    font-size: 14px;
    text-overflow: ellipsis;
    white-space: nowrap;
    border-right: 1px solid #e6e6e6;

    &:nth-child(even) {
        background-color: #5e7d8d1a;
    }
`;

export const CalendarEmployeesList = styled.ul`
    margin-bottom: 0;
    list-style-type: none;
    padding-left: 0;
    min-width: 247px;
`;

export const CalendarSchedule = styled.div`
    flex-grow: 1;
    position: relative;
    overflow-x: auto;
    overflow-y: hidden;

    .sl-c-assign-btn {
        z-index: 10;
        top: 0;
        border-radius: 3px;
    }
`;

export const CalendarScheduleColumnItemText = styled.span`
    font-size: 13px;
    padding: 0 8px;
`;

const dailyCalendarScheduleColumnItemStyles = css`
    ::before, ::after {
        content: "";
        position: absolute;
        border-right: 1px solid #e6e6e6;
        height: 100%;
        z-index: 2;
    }

    ::before {
        top: 50px;
        border-right: 1px solid #e6e6e6;
        transform: translateX(30px);
    }

    ::after {
        top: 0;
        border-right: 1px solid #e6e6e6;
        transform: translateX(60px);
    }
`;

const weeklyCalendarScheduleColumnItemStyles = css`
    border-right: 1px solid #e6e6e6;

    :last-child {
        border-right: none;
    }
`;

export const CalendarScheduleColumnItem = styled.li`
    pointer-events: none;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    height: 100%;
`;

export const CalendarScheduleColumn = styled.ul`
    display: grid;
    align-items: center;
    margin: 0;
    padding: 0;
    width: fit-content;
    min-width: 100%;
    height: 50px;
    background-color: rgba(240,244,246,.5);
    grid-template-columns: ${( props ) => ( props.weekly ? 'repeat(7, minmax(0, 1fr))' : 'repeat(24, 60px)' )};

    ${CalendarScheduleColumnItem} {
        ${( props ) => ( props.weekly
            ? weeklyCalendarScheduleColumnItemStyles
            : dailyCalendarScheduleColumnItemStyles )}
    }

`;

export const CalendarEmployeeSchedule = styled.ul`
    position: relative;
    width: 100%;
    z-index: 2;
    padding: 0;
    margin: 0;
`;

export const DailyCalendarEmployeeScheduleItemWrapper = styled.div`
    height: 60px;
    padding: 0;
    width: fit-content;
    min-width: 100%;
    align-items: center;
    justify-content: space-between;
    display: grid;
    grid-template-columns: repeat(24, 60px);
    cursor: pointer;
    position: relative;
`;

export const AssignButton = styled.button`
    background-color: #fff;
    color: #474747;
    cursor: pointer;
    border: 1px solid #9fdc74;
    border-radius: 4px;
    position: absolute;
    top: 0;
    bottom: 0;
    z-index: 1;
    margin: auto;
    width: 90%;
    height: 80%;
    box-shadow: 1px 1px 6px #56565645;
    font-size: 14px;
    font-weight: 500;
    transition: all .2s ease-in-out;

    ${( props ) => props.xPosition && `transform: translateX(${props.xPosition}px);`}

    & {
        :hover, :active, :focus {
            background-color: #f6feef;
            box-shadow: 1px 1px 10px #56565645;
            text-decoration: none;
        }
    }
`;

export const DailyCalendarEmployeeScheduleItem = styled.li`
    position: relative;

    &:nth-child(even) {
        ${DailyCalendarEmployeeScheduleItemWrapper} {
            background-color: #5e7d8d1a;
        }
    }

    ${AssignButton} {
        z-index: 5;
        width: 100px;
    }
`;

export const WeeklyCalendarEmployeeScheduleItem = styled.li`
    height: 60px;
    padding: 0;
    width: fit-content;
    min-width: 100%;
    align-items: center;
    justify-content: space-between;
    display: grid;
    grid-template-columns: repeat(7, minmax(0, 1fr));
    cursor: pointer;
    position: relative;

    &:nth-child(even) {
        background-color: #5e7d8d1a;
    }
`;

export const WeeklyCalendarEmployeeScheduleItemWrapper = styled.div`
    max-height: 60px;
    height: 100%;
    border-right: 1px solid #e6e6e6;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 12px 6px;
    display: flex;
    width: 100%;
    position: relative;

    :last-child {
        border-right: none;
    }

    ${( props ) => props.overflowAuto && `
        overflow: auto;
        justify-content: flex-start;
    `}
`;

export const WeeklyEmployeeScheduleItem = styled.div`
    color: ${( props ) => props.color};
    background-color: ${( props ) => props.backgroundColor};
    background-image: ${( props ) => props.backgroundImage};
    height: ${( props ) => `${getScheduleHeight( props.shiftCount )}px`};
    width: 100%;
    padding: 1px 5px;
    border-radius: 0.25rem;
    border: 1px solid #cccccc;
    font-size: .8rem;
    display: flex;
    align-items: center;
`;

export const EmployeeScheduleItemText = styled.p`
    font-size: 11px;
    text-overflow: ellipsis;
    white-space: nowrap;
    margin: 0;
    overflow: hidden;
`;

export const DailyEmployeeScheduleItem = styled.div`
    z-index: 3;
    padding: 1px 5px;
    border-radius: 0.25rem;
    border: 1px solid #cccccc;
    margin: 0;
    height: 32px;
    font-size: .8rem;
    display: flex;
    align-items: center;
    background-color: ${( props ) => props.backgroundColor};
    background-image: ${( props ) => props.backgroundImage};
    color: ${( props ) => props.color};
    grid-column: ${({ columnStart, columnSpan }) => `${columnStart} / span ${columnSpan}`};
    grid-row: 1;

    ${EmployeeScheduleItemText} {
        :last-child {
            margin-left: 4px;
        }
    }
`;

export const CloseButton = styled.button`
    cursor: pointer;
`;

