/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-unused-vars */
/* eslint-disable no-confusing-arrow */

import React from 'react';
import Popper from 'popper.js';
import moment from 'moment';
import {
    get,
    groupBy,
    values,
    each,
    filter,
    indexOf,
    first,
    last,
    cloneDeep,
    clone
} from 'lodash';

import { makeRange, getIsoWeekday } from 'utils/datesHelper';

import Icon from 'components/Icon';

import AssignPopOver from './AssignPopOver';
import CalendarEventPopOver from './CalendarEventPopOver';

import { changeColorLuminance, isColorLuminant } from '../color';

import { DATE_FORMATS, DAYS_OF_WEEK } from './constants';

import {
    CalendarWrapper,
    CalendarSchedule,
    WeeklyEmployeeScheduleItem,
    Wrapper,
    CalendarEmployeesList,
    CalendarScheduleColumn,
    CalendarEmployeeSchedule,
    CalendarScheduleColumnItem,
    CalendarScheduleColumnItemText,
    CalendarEmployeesListWrapper,
    CalendarEmployeesListHeading,
    CalendarEmployeesListItem,
    DailyEmployeeScheduleItem,
    EmployeeScheduleItemText,
    WeeklyCalendarEmployeeScheduleItemWrapper,
    AssignButton,
    DailyCalendarEmployeeScheduleItem,
    WeeklyCalendarEmployeeScheduleItem,
    DailyCalendarEmployeeScheduleItemWrapper
} from './styles';

import { CALENDAR_FREQUENCY } from '../constants';

const scheduleStylesCache = {};

/**
 *
 * Calendar
 *
 */
class Calendar extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        employeeShifts: React.PropTypes.array,
        viewName: React.PropTypes.string,
        defaultSchedule: React.PropTypes.array,
        date: React.PropTypes.string,
        list: React.PropTypes.array,
        addShift: React.PropTypes.func,
        addRestDay: React.PropTypes.func,
        unAssignShift: React.PropTypes.func,
        unAssignRestDay: React.PropTypes.func,
        checkInUse: React.PropTypes.bool,
        fetchShiftsData: React.PropTypes.func
    };

    constructor( props ) {
        super( props );

        this.state = {
            showEvent: false,
            timeoutId: null,
            popover: null,
            events: [],
            currentMonth: moment().month(),
            currentInterval: {
                start: moment().subtract( 1, 'month' ).startOf( 'month' ),
                end: moment().add( 1, 'month' ).endOf( 'month' )
            },
            quickAddParams: {},
            eventsWidth: null,
            assignPopper: null,
            tableOrder: 'ascending',
            flipped: false,
            shiftAssignmentData: {
                employeeId: null,
                date: null
            }
        };

        this.closeSchedulePopover = this.closeSchedulePopover.bind( this );
        this.closeAssignPopover = this.closeAssignPopover.bind( this );
        this.removeAssignButton = this.removeAssignButton.bind( this );
        this.handleClickAssign = this.handleClickAssign.bind( this );

        this.employeeScheduleRef = null;
        this.assignPopperRef = null;
        this.schedulePopperRef = null;
        this.popoverRef = null;
    }

    componentDidUpdate( prevProps ) {
        if ( this.props.viewName !== prevProps.viewName ||
            this.props.date !== prevProps.date
        ) {
            this.removeAssignButton();
            this.closeAssignPopover();
            this.closeSchedulePopover();
        }
    }

    getCorrectPosition( initialPosition ) {
        if ( initialPosition < 5 ) {
            return 5;
        }

        if ( initialPosition > 1335 ) {
            return 1335;
        }

        return initialPosition;
    }

    getDefaultScheduleEvent( defaultSchedule, employee, dayBefore = false ) {
        const isRestDay = defaultSchedule && defaultSchedule.day_type === 'rest_day';
        const companyId = get( defaultSchedule, 'company_id' );
        const date = get( defaultSchedule, 'date' );
        const dayOfWeek = get( defaultSchedule, 'day_of_week' );
        const workStart = get( defaultSchedule, 'work_start' );
        const workEnd = get( defaultSchedule, 'work_end' );
        const styles = this.eventStyles( isRestDay ? '#F6F6F6' : '#02688f' );

        return {
            id: 0,
            defaultSchedule,
            company_id: companyId,
            name: isRestDay ? 'Default Schedule - Rest Day' : 'Default Schedule',
            date,
            restDay: isRestDay,
            employee: null,
            employee_id: employee && employee.id,
            start: date,
            end: date,
            color: styles.textColor,
            day: DAYS_OF_WEEK[ dayOfWeek ],
            start_time: moment( workStart, DATE_FORMATS.CLOCK ).format( DATE_FORMATS.HOURS_AND_MINUTES ),
            end_time: moment( workEnd, DATE_FORMATS.CLOCK ).format( DATE_FORMATS.HOURS_AND_MINUTES ),
            schedule_id: 0,
            schedule: { color: '#02688f', id: 0 },
            isDayBefore: dayBefore
        };
    }

    getDayEventObject( event, styles, employee ) {
        if ( event.defaultSchedule ) {
            let startTime = event.start_time;
            const restDay = event.restDay;
            const style = this.eventStyles( event.restDay ? '#F6F6F6' : '#02688f' );

            if ( event.isdayBefore ) {
                startTime = '00:00';
            }

            const defaultSchedule = {
                name: event.name,
                startTime,
                endTime: event.end_time,
                restDay,
                day: event.day,
                schedule: { color: '#02688f' },
                backgroundImage: restDay ? style.backgroundImage : null,
                color: style.textColor,
                defaultSchedule: true,
                date: event.date,
                isDayBefore: event.isDayBefore,
                isDefaultRestDay: !!event.restDay,
                employee_id: employee && employee.id,
                employee_name: this.getEmployeeFullName( employee )
            };

            return defaultSchedule;
        }

        return {
            name: event.name,
            startTime: get( event, 'schedule.start_time', event.start_time ),
            endTime: get( event, 'schedule.end_time', event.end_time ),
            restDay: event.rest_day,
            backgroundImage: styles.backgroundImage,
            color: styles.textColor,
            schedule: event.schedule,
            start: event.start,
            end: event.end,
            id: event.id,
            date: this.props.date,
            original: event.original,
            employee_id: employee && employee.id,
            employee_name: this.getEmployeeFullName( employee ),
            dates: event.dates
        };
    }

    getEmployeeDisplayName( employee ) {
        const {
            employee_id: employeeId,
            first_name: firstName,
            middle_name: middleName,
            last_name: lastName
        } = employee;

        return `${employeeId} - ${firstName} ${middleName ? ` ${middleName} ` : ' '} ${lastName}`;
    }

    getEmployeeFullName( employee ) {
        const {
            first_name: firstName,
            middle_name: middleName,
            last_name: lastName
        } = employee;

        return `${firstName}${middleName ? ` ${middleName} ` : ' '}${lastName}`;
    }

    getSortedDataByEmployees( data ) {
        const { tableOrder } = this.state;

        if ( tableOrder === 'ascending' ) {
            return [...data]
                .sort( ( a, b ) => {
                    const employeeDisplayNameA = this.getEmployeeDisplayName( a );
                    const employeeDisplayNameB = this.getEmployeeDisplayName( b );

                    return ( employeeDisplayNameA > employeeDisplayNameB ) ? 1 : -1;
                });
        }

        return [...data]
            .sort( ( a, b ) => {
                const employeeDisplayNameA = this.getEmployeeDisplayName( a );
                const employeeDisplayNameB = this.getEmployeeDisplayName( b );

                return ( employeeDisplayNameA < employeeDisplayNameB ) ? 1 : -1;
            });
    }

    getWeekEventObject( event, styles, employee ) {
        if ( event.defaultSchedule ) {
            const style = this.eventStyles( event.restDay ? '#F6F6F6' : '#02688f' );

            return {
                name: event.name,
                date: event.date,
                backgroundImage: event.restDay ? style.backgroundImage : null,
                restDay: event.restDay,
                color: style.textColor,
                day: event.day,
                startTime: event.start_time,
                endTime: event.end_time,
                defaultSchedule: true,
                schedule: { color: '#02688f' },
                start: event.start,
                end: event.end,
                id: event.id,
                employee_id: employee && employee.id,
                employee_name: this.getEmployeeFullName( employee ),
                dates: event.dates
            };
        }

        return {
            name: event.name,
            date: event.date,
            backgroundImage: styles.backgroundImage,
            restDay: event.rest_day,
            color: styles.textColor,
            schedule: event.schedule,
            start: event.start,
            end: event.end,
            id: event.id,
            startTime: event.start_time,
            endTime: event.end_time,
            original: event.original,
            employee_id: employee && employee.id,
            employee_name: this.getEmployeeFullName( employee ),
            dates: event.dates
        };
    }

    getWeekViewInterval() {
        const startOfWeek = moment( this.props.date ).startOf( CALENDAR_FREQUENCY.WEEK );
        const interval = [];

        while ( interval.length < 7 ) {
            interval.push( startOfWeek.clone().format( 'YYYY-MM-DD' ) );
            startOfWeek.add( 1, CALENDAR_FREQUENCY.DAY );
        }

        return interval;
    }

    getFormattedShifts( shifts ) {
        const formattedShifts = ( shifts || []).map( ( shift ) => {
            const shiftVal = shift;
            shiftVal.color = shift.schedule.color;
            shiftVal.name = shift.schedule.name;
            shiftVal.start_time = shift.start_time || shift.schedule.start_time;
            shiftVal.end_time = shift.end_time || shift.schedule.end_time;

            return shiftVal;
        });

        return formattedShifts;
    }

    getEventEnd( restDay ) {
        // eslint-disable-next-line no-useless-concat
        return `${restDay.start_date} ` + '23:59:59';
    }

    getIntervalEnd( restDay, intervalEnd ) {
        if ( moment( restDay.end_date ).isBefore( moment( intervalEnd ) ) ) {
            return restDay.end_date;
        }

        return intervalEnd;
    }

    getWeeklyOccurrenceDates( restDay, recurrence, intervalStart ) {
        const groupedDates = values( groupBy( recurrence.all(), ( date ) => date.format( 'YYYY' ) + date.week() ) );

        let allDates = [];

        // Get only every nth week
        groupedDates.forEach( ( dates, index ) => {
            if ( ( index % restDay.repeat.repeat_every ) === 0 ) {
                allDates = allDates.concat( dates );
            }
        });

        return this.getRequestedNumberOfOccurrencesAndAfterIntervalStart( allDates, restDay, intervalStart );
    }

    getRequestedNumberOfOccurrencesAndAfterIntervalStart( allDates, restDay, intervalStart ) {
        // Get only requested number of occurrences
        let shift = allDates;
        if ( restDay.repeat.end_after ) {
            shift = allDates.splice( 0, restDay.repeat.end_after );
        }

        // Get only dates after inside interval
        return shift.filter( ( date ) => date.isSameOrAfter( intervalStart ) )
          .map( ( date ) => date.format( 'YYYY-MM-DD' ) );
    }

    getFormattedRestDays( restDays ) {
        return restDays.map( ( restDay ) => {
            const day = { ...restDay };
            day.name = 'Rest Day';
            day.color = '#F6F6F6';
            day.start_time = moment( this.props.date ).startOf( CALENDAR_FREQUENCY.DAY ).format( 'HH:mm' );
            day.end_time = moment( this.props.date ).endOf( CALENDAR_FREQUENCY.DAY ).format( 'HH:mm' );
            day.rest_day = true;
            day.date = day.start_date;
            day.start = day.start_date;
            day.end = day.end_date;

            return day;
        });
    }

    getDefaultScheduleDayOverlapEvent( defaultScheduleForDayBefore, defaultScheduleForDay, employee ) {
        const defaultDayBefore = this.getDefaultScheduleEvent( defaultScheduleForDayBefore, employee, true );
        const defaultDay = this.getDefaultScheduleEvent( defaultScheduleForDay, employee );

        if ( defaultDayBefore.isDayBefore && defaultDay.isDefaultRestDay ) {
            const newStartTime = defaultDayBefore.end_time;
            const newEndTime = '23:59';

            defaultDay.changedTimesForRestDays = `${newStartTime} - ${newEndTime}`;
        }

        return [ defaultDayBefore, defaultDay ];
    }

    getWeeklyEmployeeEvents() {
        if ( this.props.viewName !== CALENDAR_FREQUENCY.WEEK ) {
            return [];
        }

        let mappedWeeklyEmployeeShifts = this.getSortedDataByEmployees(
            this.getMappedWeeklyEmployeeShifts()
        );

        mappedWeeklyEmployeeShifts = mappedWeeklyEmployeeShifts.map(
            ( employeeShift ) => ( employeeShift.events || []).map( ( event ) => {
                const styles = this.eventStyles( event.color );

                return this.getWeekEventObject( event, styles, employeeShift );
            })
        );

        return mappedWeeklyEmployeeShifts;
    }

    getDailyEmployeeEvents() {
        if ( this.props.viewName !== CALENDAR_FREQUENCY.DAY ) {
            return [];
        }

        let mappedDailyEmployeeShifts = this.getSortedDataByEmployees(
            this.getMappedDailyEmployeeShifts()
        );

        mappedDailyEmployeeShifts = mappedDailyEmployeeShifts.map( ( employeeShift ) => ( employeeShift.dayEvents || []).map( ( event ) => {
            const styles = this.eventStyles( event.color );

            return this.getDayEventObject( event, styles, employeeShift );
        }) );

        return mappedDailyEmployeeShifts;
    }

    getMappedWeeklyEmployeeShifts() {
        const {
            date,
            defaultSchedule,
            employeeShifts
        } = this.props;

        const weekViewInterval = this.getWeekViewInterval();

        const mappedWeeklyEmployeeShifts = [...employeeShifts].map( ( employeeShift ) => {
            const updatedEmployeeShift = { ...employeeShift, events: []};

            let updatedEmployeeShiftEvents = [];

            weekViewInterval.forEach( ( weekViewIntervalDate ) => {
                const dateShifts = updatedEmployeeShift.shifts.filter(
                    ( shift ) => indexOf( shift.dates, weekViewIntervalDate ) >= 0
                );

                dateShifts.forEach( ( shift ) => {
                    const updatedShift = { ...shift };
                    updatedShift.date = weekViewIntervalDate;

                    updatedEmployeeShiftEvents.push( updatedShift );
                });
            });

            updatedEmployeeShiftEvents = this.getFormattedShifts( updatedEmployeeShiftEvents );

            const restDays = this.processRestDays( updatedEmployeeShift.rest_days, {
                start: first( weekViewInterval ),
                end: last( weekViewInterval )
            });

            updatedEmployeeShiftEvents = updatedEmployeeShiftEvents.concat( this.getFormattedRestDays( restDays ) );

            const startOfWeek = moment( date ).startOf( CALENDAR_FREQUENCY.WEEK );
            const endOfWeek = moment( date ).endOf( CALENDAR_FREQUENCY.WEEK );
            const weekDates = makeRange( startOfWeek, endOfWeek, DATE_FORMATS.DEFAULT );
            const defaultSchedules = [];

            weekDates.forEach( ( weekDate ) => {
                const eventsOnSpecificDay = filter( updatedEmployeeShiftEvents, [ 'date', weekDate ]);

                if ( !eventsOnSpecificDay.length ) {
                    const currentDefaultScheduleForDay = defaultSchedule &&
                        defaultSchedule.find( ( schedule ) => (
                            schedule.day_of_week === getIsoWeekday( weekDate )
                        ) );

                    if ( currentDefaultScheduleForDay ) {
                        /**
                         * NOTE: not causing an issue for now, but directly mutating the
                         * `defaultSchedule` prop and assigning it with field `date`
                         * makes the displaying of default schedule for day view work.
                         *
                         * Leave as is for now.
                         */
                        currentDefaultScheduleForDay.date = weekDate;
                    }

                    defaultSchedules.push(
                        this.getDefaultScheduleEvent( currentDefaultScheduleForDay, updatedEmployeeShift )
                    );
                }
            });

            updatedEmployeeShiftEvents.push( ...defaultSchedules );
            updatedEmployeeShift.events = updatedEmployeeShiftEvents;

            return updatedEmployeeShift;
        });

        return mappedWeeklyEmployeeShifts;
    }

    getMappedDailyEmployeeShifts() {
        const {
            date,
            defaultSchedule,
            employeeShifts
        } = this.props;

        const interval = {
            start: date,
            end: date
        };

        const mappedDailyEmployeeShifts = [...employeeShifts].map( ( employeeShift ) => {
            const updatedEmployeeShift = { ...employeeShift, dayEvents: []};
            let updatedEmployeeShiftDayEvents = [];

            const shifts = this.getFormattedShifts(
                this.getProcessedDayShifts( updatedEmployeeShift.shifts, date )
            );

            const restDays = this.processRestDays( updatedEmployeeShift.rest_days, interval );
            updatedEmployeeShiftDayEvents = this.getFormattedRestDays( restDays ).concat( shifts );

            if ( !updatedEmployeeShiftDayEvents.length ) {
                const currentDefaultScheduleForDay = defaultSchedule.find( ( schedule ) => (
                    schedule.day_of_week === getIsoWeekday( date )
                ) );

                const dayBefore = defaultSchedule.find( ( schedule ) => (
                    schedule.day_of_week === getIsoWeekday( moment( date ).subtract( 1, CALENDAR_FREQUENCY.DAY ).format( 'YYYY-MM-DD' ) )
                ) );

                if ( moment( dayBefore.work_end, DATE_FORMATS.CLOCK ).isBefore( moment( dayBefore.work_start, DATE_FORMATS.CLOCK ) ) ) {
                    updatedEmployeeShiftDayEvents = this.getDefaultScheduleDayOverlapEvent( dayBefore, currentDefaultScheduleForDay, updatedEmployeeShift );
                } else {
                    // Handle situation when default rest day should render in whole day
                    updatedEmployeeShiftDayEvents = [this.getDefaultScheduleEvent( currentDefaultScheduleForDay, updatedEmployeeShift )];
                }
            }

            updatedEmployeeShift.dayEvents = updatedEmployeeShiftDayEvents;

            return updatedEmployeeShift;
        });

        return mappedDailyEmployeeShifts;
    }

    getCalendarDates() {
        const {
            viewName
        } = this.props;

        // Return hours of day
        if ( viewName === CALENDAR_FREQUENCY.DAY ) {
            return Array( 24 ).fill( 0 )
                .map( ( _, index ) => ({ text: `${index}:00` }) );
        }

        // Return days of week
        const prevDate = moment( this.props.date ).startOf( CALENDAR_FREQUENCY.WEEK );
        const nextDate = moment( this.props.date ).endOf( CALENDAR_FREQUENCY.WEEK );

        let start = new Date( prevDate );
        const end = new Date( nextDate );

        const dateArr = [];

        while ( start < end ) {
            dateArr.push({
                text: moment( start ).format( 'ddd, DD' ),
                textValue: moment( start ).format( 'YYYY-MM-DD' ),
                isActive: moment( start ).isSame( moment(), CALENDAR_FREQUENCY.DAY )
            });
            const newDate = start.setDate( start.getDate() + 1 );
            start = new Date( newDate );
        }

        return dateArr;
    }

    getSortedEmployees() {
        const { tableOrder } = this.state;

        const mappedWeeklyEmployeeShifts = this.getMappedWeeklyEmployeeShifts();

        return this.getSortedDataByEmployees( mappedWeeklyEmployeeShifts );
    }

    getProcessedRestDay( restDay, interval ) {
        const rest = restDay;
        rest.original = cloneDeep( restDay );

        if ( !rest.repeat ) {
            if ( !this.isRestDayStartDateInsideInterval( rest, interval ) ) {
                return [];
            }

            return [this.processSingleEvent( rest )];
        }

        // No need to process further restDay if it's not before the end of interval.
        if ( moment( restDay.start_date ).isAfter( interval.end ) ) {
            return [];
        }

        const intervalEnd = this.getIntervalEnd( restDay, interval.end );

        return this.processRepeated( restDay, interval.start, intervalEnd );
    }

    getProcessedDayShifts( shifts, date ) {
        const dayBefore = moment( date ).subtract( 1, CALENDAR_FREQUENCY.DAY ).format( 'YYYY-MM-DD' );

        const overnightShiftsStartingDayBefore = ( shifts || []).filter( ( shift ) =>
                indexOf( shift.dates, dayBefore ) >= 0 &&
                    moment( shift.start_time, 'HH:mm' ) > moment( shift.end_time, 'HH:mm' )
            ).map( ( shift ) => ({
                ...shift,
                start_time: '00:00'
            }) );

        return ( shifts || []).filter( ( shift ) => indexOf( shift.dates, date ) >= 0 )
          .concat( overnightShiftsStartingDayBefore );
    }

    mapDatesToEvents( dates, restDay ) {
        return dates.map( ( date ) => {
            const event = clone( restDay );

            event.start_date = date;

            return this.processSingleEvent( event );
        });
    }

    processRepeated( restDay, intervalStart, intervalEnd ) {
        // Define recurrence
        const recurrence = moment( restDay.start_date ).recur( intervalEnd )
          .every( restDay.repeat.rest_days ).daysOfWeek();

        const dates = this.getWeeklyOccurrenceDates( restDay, recurrence, intervalStart );

        return this.mapDatesToEvents( dates, restDay );
    }

    processRestDays( restDays, interval ) {
        let events = [];

        ( restDays || []).forEach( ( restDay ) => {
            events = events.concat( this.getProcessedRestDay( restDay, interval ) );
        });

        return events;
    }

    isRestDayStartDateInsideInterval( restDay, interval ) {
        const startDate = moment( restDay.start_date );

        return startDate.isBetween( interval.start, interval.end, CALENDAR_FREQUENCY.DAY ) ||
          startDate.isSame( interval.start, CALENDAR_FREQUENCY.DAY ) ||
          startDate.isSame( interval.end, CALENDAR_FREQUENCY.DAY );
    }

    processSingleEvent( event ) {
        const processedEvent = event;
        processedEvent.start = processedEvent.start_date;
        processedEvent.end = this.getEventEnd( processedEvent );

        return processedEvent;
    }

    eventStyles( color ) {
        if ( !get( scheduleStylesCache, color ) ) {
            const secondary = changeColorLuminance( color, 60 );

            scheduleStylesCache[ color ] = {
                textColor: isColorLuminant( color ) ? 'black' : 'white',
                backgroundImage: 'repeating-linear-gradient(45deg, transparent, transparent 3px, ' +
              'rgba(0, 0, 0, 0.07) 3px, rgba(0, 0, 0, 0.07) 4px), ' +
              `linear-gradient(to bottom, ${color}, ${secondary})`
            };
        }

        return scheduleStylesCache[ color ];
    }

    handleEventClick( val ) {
        this.closeAssignPopover();
        this.removeAssignButton();

        this.setState({
            popover: {
                val,
                flipped: true
            },
            flipped: true,
            shiftAssignmentData: {
                employeeId: null,
                date: null
            }
        });
    }

    createPopper( element, ref, callback ) {
        return new Popper( element, ref, {
            placement: 'left',
            modifiers: {
                flip: { enabled: true },
                preventOverflow: {
                    enabled: true,
                    priority: [ 'left', 'right' ],
                    padding: 20
                }
            },
            onCreate: callback
        });
    }

    closeSchedulePopover() {
        this.setState({
            popover: null,
            flipped: false
        });
    }

    removeAssignButton() {
        this.setState({
            shiftAssignmentData: {
                employeeId: null,
                date: null
            }
        });
    }

    closeAssignPopover() {
        this.setState({
            assignPopper: null
        });
    }

    sortEmployees() {
        this.setState({
            tableOrder: this.state.tableOrder === 'ascending'
                ? 'descending'
                : 'ascending'
        });
    }

    handleClickDailyScheduleItem( event, employeeSchedules, employeeIndex ) {
        const rect = event.target.getBoundingClientRect();
        const xPosition = this.getCorrectPosition( Math.round( event.clientX - rect.left ) - 45 );

        const date = get(
            employeeSchedules.find( ( employeeSchedule ) => employeeSchedule.date ),
            'date'
        );
        const employeeId = get(
            employeeSchedules.find( ( employeeSchedule ) => employeeSchedule.employee_id ),
            'employee_id'
        );

        this.handleShowShiftAssignmentButton( date, employeeId, xPosition );
    }

    handleShowShiftAssignmentButton( date, employeeId, xPosition ) {
        this.closeSchedulePopover();
        this.closeAssignPopover();

        const {
            date: currentDate,
            employeeId: currentEmployeeId,
            xPosition: currentPosition
        } = this.state.shiftAssignmentData;

        let shiftAssignmentData = {
            date,
            employeeId,
            xPosition
        };

        if ( currentDate &&
            currentEmployeeId &&
            currentDate === date &&
            currentEmployeeId === employeeId
        ) {
            if ( !xPosition || ( xPosition && xPosition === currentPosition ) ) {
                shiftAssignmentData = {
                    date: null,
                    employeeId: null
                };
            }
        }

        this.setState({ shiftAssignmentData });
    }

    handleClickAssign = ( event, employeeId, date ) => {
        event.stopPropagation();

        this.closeSchedulePopover();
        this.closeAssignPopover();

        const eventTarget = event.target;
        const assignButtonData = {
            employeeId,
            clickedDate: date,
            target: eventTarget
        };

        this.setState(
            {
                assignPopper: {
                    val: assignButtonData,
                    flipped: false
                }
            },
            () => {
                this.createPopper(
                    eventTarget,
                    this.assignPopperRef,
                    ( value ) => {
                        this.setState({
                            assignPopper: {
                                val: assignButtonData,
                                flipped: value.flipped
                            }
                        });
                    }
                );
            }
        );
    }

    render() {
        const {
            flipped,
            popover,
            assignPopper,
            shiftAssignmentData
        } = this.state;

        const employees = this.getSortedEmployees() || [];

        const isViewWeekly = this.props.viewName === CALENDAR_FREQUENCY.WEEK;
        const displayWeeklyCalendar = isViewWeekly;
        const weeklyEmployeeEvents = this.getWeeklyEmployeeEvents();

        const isViewDaily = this.props.viewName === CALENDAR_FREQUENCY.DAY;
        const displayDailyCalendar = isViewDaily;
        const dailyEmployeeEvents = this.getDailyEmployeeEvents();

        const calendarDates = this.getCalendarDates();

        const weeklySchedulesContent = weeklyEmployeeEvents.map(
            ( baseWeeklySchedules, employeeIndex ) => (
                <WeeklyCalendarEmployeeScheduleItem
                    key={ `employee-item-${employeeIndex}` }
                    weekly={ isViewWeekly }
                    id={ employeeIndex }
                >
                    { calendarDates.map( ( calendarDate, calendarDateIndex ) => {
                        const weeklySchedules = baseWeeklySchedules || [];
                        const employeeId = get(
                            weeklySchedules.find( ( weeklySchedule ) => get( weeklySchedule, 'employee_id' ) ),
                            'employee_id'
                        );
                        const displayAssignButton = get( shiftAssignmentData, 'date' ) === calendarDate.textValue &&
                            get( shiftAssignmentData, 'employeeId' ) === employeeId;

                        const filteredWeeklySchedule = weeklySchedules.filter(
                            ( weeklySchedule ) => weeklySchedule.date === calendarDate.textValue
                        );

                        return (
                            <WeeklyCalendarEmployeeScheduleItemWrapper
                                key={ `calendar-schedule-wrapper-${calendarDateIndex}` }
                                onClick={ ( event ) => {
                                    event.stopPropagation();

                                    this.handleShowShiftAssignmentButton(
                                        calendarDate.textValue,
                                        employeeId,
                                    );
                                } }
                                overflowAuto={ filteredWeeklySchedule.length >= 5 }
                            >
                                { displayAssignButton && (
                                    <AssignButton
                                        onClick={ ( event ) => {
                                            this.handleClickAssign( event, employeeId, calendarDate.textValue );
                                        } }
                                    >
                                        + Assign
                                    </AssignButton>
                                ) }

                                { filteredWeeklySchedule.map( ( weeklySchedule, calendarTimeIndex ) => (
                                    <WeeklyEmployeeScheduleItem
                                        key={ `calendar-schedule-item-${calendarTimeIndex}` }
                                        shiftCount={ filteredWeeklySchedule.length }
                                        backgroundColor={
                                            weeklySchedule.schedule
                                                ? weeklySchedule.schedule.color
                                                : '#ffffff'
                                        }
                                        backgroundImage={ weeklySchedule.backgroundImage }
                                        color={ weeklySchedule.color }
                                        onClick={ ( event ) => {
                                            event.stopPropagation();

                                            this.handleEventClick( weeklySchedule, event );

                                            this.createPopper(
                                                event.target,
                                                this.popoverRef,
                                                ( value ) => {
                                                    this.setState({
                                                        popover: {
                                                            ...this.state.popover,
                                                            val: weeklySchedule,
                                                            flipped: value.flipped
                                                        }
                                                    });
                                                }
                                            );
                                        } }
                                    >
                                        <EmployeeScheduleItemText>
                                            { weeklySchedule.name }
                                        </EmployeeScheduleItemText>
                                    </WeeklyEmployeeScheduleItem>
                                ) ) }
                            </WeeklyCalendarEmployeeScheduleItemWrapper>
                        );
                    }) }
                </WeeklyCalendarEmployeeScheduleItem>
            )
        );

        const dailySchedulesContent = dailyEmployeeEvents.map(
            ( employeeSchedules, employeeIndex ) => {
                const date = get(
                    employeeSchedules.find( ( employeeSchedule ) => employeeSchedule.date ),
                    'date'
                );
                const employeeId = get(
                    employeeSchedules.find( ( employeeSchedule ) => employeeSchedule.employee_id ),
                    'employee_id'
                );

                const displayAssignButton = get( shiftAssignmentData, 'employeeId' ) === employeeId &&
                    get( shiftAssignmentData, 'xPosition' );

                return (
                    <DailyCalendarEmployeeScheduleItem
                        key={ `employee-item-${employeeIndex}` }
                        id={ `employee-item-${employeeIndex}` }
                    >
                        { displayAssignButton && (
                            <AssignButton
                                onClick={ ( event ) => {
                                    this.handleClickAssign( event, employeeId, date );
                                } }
                                xPosition={ shiftAssignmentData.xPosition }
                            >
                                + Assign
                            </AssignButton>
                        ) }

                        <DailyCalendarEmployeeScheduleItemWrapper
                            key={ `employee-item-wrapper-${employeeIndex}` }
                            id={ `employee-item-wrapper-${employeeIndex}` }
                            onClick={ ( event ) => this.handleClickDailyScheduleItem( event, employeeSchedules, employeeIndex ) }
                        >
                            { ( employeeSchedules || []).map( ( employeeScheduleData, calendarTimeIndex ) => {
                                const scheduleTime = employeeScheduleData.name !== 'Rest Day'
                                    ? ` (${employeeScheduleData.startTime} - ${employeeScheduleData.endTime})`
                                    : '';
                                const scheduleDescription = `${employeeScheduleData.name}${scheduleTime}`;

                                const startDate = moment( `${employeeScheduleData.start || employeeScheduleData.date} ${employeeScheduleData.startTime}` );
                                const endDate = moment( `${employeeScheduleData.end || employeeScheduleData.date} ${employeeScheduleData.endTime}` );

                                const isShiftEndsNextDay = endDate.isBefore( startDate );
                                const isShiftStartWithinCurrentDate = startDate.isSame( moment( employeeScheduleData.date ), 'day' );

                                let columnStart = startDate.format( 'H' );
                                columnStart = isShiftEndsNextDay && !isShiftStartWithinCurrentDate
                                    ? 1
                                    : ( parseInt( columnStart, 10 ) || 0 ) + 1;

                                let columnSpan = endDate.format( 'H' );
                                columnSpan = isShiftEndsNextDay && isShiftStartWithinCurrentDate
                                    ? 25
                                    : ( parseInt( columnSpan, 10 ) || 0 ) + 1;
                                columnSpan -= columnStart;

                                const marginLeft = parseInt( startDate.format( 'm' ) || '0', 10 ) - 1;
                                const marginRight = parseInt( endDate.format( 'm' ) || '0', 10 ) + 1;

                                return (
                                    <DailyEmployeeScheduleItem
                                        key={ `calendar-time-${calendarTimeIndex}` }
                                        title={ scheduleDescription }
                                        columnStart={ columnStart }
                                        columnSpan={ columnSpan }
                                        backgroundColor={ employeeScheduleData.schedule ? employeeScheduleData.schedule.color : '#ffffff' }
                                        backgroundImage={ employeeScheduleData.backgroundImage ? employeeScheduleData.backgroundImage : null }
                                        color={ employeeScheduleData && employeeScheduleData.color }
                                        onClick={ ( event ) => {
                                            event.stopPropagation();

                                            this.handleEventClick( employeeScheduleData, event );

                                            this.createPopper(
                                                event.target,
                                                this.popoverRef,
                                                ( value ) => {
                                                    this.setState({
                                                        popover: {
                                                            ...this.state.popover,
                                                            val: employeeScheduleData,
                                                            flipped: value.flipped
                                                        }
                                                    });
                                                }
                                            );
                                        } }
                                        style={ {
                                            marginLeft: `${marginLeft}px`,
                                            marginRight: `-${marginRight}px`
                                        } }
                                    >
                                        <EmployeeScheduleItemText>
                                            { scheduleDescription }
                                        </EmployeeScheduleItemText>
                                    </DailyEmployeeScheduleItem>
                                );
                            }) }
                        </DailyCalendarEmployeeScheduleItemWrapper>
                    </DailyCalendarEmployeeScheduleItem>
                );
            }
        );

        return (
            <Wrapper>
                <CalendarWrapper>
                    <CalendarEmployeesListWrapper>
                        <CalendarEmployeesListHeading onClick={ () => { this.sortEmployees(); } }>
                            Employees <Icon name="sort" />
                        </CalendarEmployeesListHeading>

                        <CalendarEmployeesList className="sl-c-employee-list">
                            { employees.map( ( employee ) => {
                                const employeeId = get( employee, 'employee_id' );

                                return (
                                    <CalendarEmployeesListItem key={ employeeId }>
                                        { this.getEmployeeDisplayName( employee ) }
                                    </CalendarEmployeesListItem>
                                );
                            }) }
                        </CalendarEmployeesList>
                    </CalendarEmployeesListWrapper>

                    <CalendarSchedule className="calendar-schedule">
                        <CalendarScheduleColumn
                            weekly={ !isViewDaily }
                            className="calender-schedule-time"
                        >
                            { calendarDates.map( ( time, calendarHeaderIndex ) => {
                                const formattedDateTime = isViewDaily
                                    ? moment( `2022-01-01 ${time.text}` ).format( 'h A' )
                                    : time.text;

                                return (
                                    <CalendarScheduleColumnItem
                                        key={ `calendar-header-${calendarHeaderIndex}` }
                                    >
                                        <CalendarScheduleColumnItemText>
                                            { formattedDateTime }
                                        </CalendarScheduleColumnItemText>
                                    </CalendarScheduleColumnItem>
                                );
                            })}
                        </CalendarScheduleColumn>

                        <CalendarEmployeeSchedule ref={ ( ref ) => { this.employeeScheduleRef = ref; } }>
                            { displayWeeklyCalendar ? weeklySchedulesContent : null}

                            { displayDailyCalendar ? dailySchedulesContent : null }
                        </CalendarEmployeeSchedule>
                    </CalendarSchedule>
                </CalendarWrapper>

                <div style={ { display: flipped ? 'block' : 'none' } }>
                    <div
                        ref={ ( ref ) => { this.popoverRef = ref; } }
                        // LIMITATION: popper.js can't seem to work around style component's returned ref object
                        className={ `pop-over-wrapper${!get( popover, 'flipped' ) ? ' left' : ''}` }
                    >
                        { popover !== null && (
                            <CalendarEventPopOver
                                singleEvent={ popover }
                                closeEventPopOver={ this.closeSchedulePopover }
                                DeleteButtonClickHanld={ this.eventDeleted }
                                removeSchedule={ this.props.checkInUse }
                                unAssignShift={ this.props.unAssignShift }
                                unAssignRestDay={ this.props.unAssignRestDay }
                                fetchShiftsData={ this.props.fetchShiftsData }
                            />
                        ) }
                    </div>
                </div>

                { assignPopper !== null && (
                    <div
                        ref={ ( ref ) => { this.assignPopperRef = ref; } }
                        // LIMITATION: popper.js can't seem to work around style component's returned ref object
                        className={ `pop-over-wrapper${!get( assignPopper, 'flipped' ) ? ' left' : ''}` }
                    >
                        <AssignPopOver
                            singleEvent={ assignPopper }
                            closeEventPopOver={ this.closeAssignPopover }
                            DeleteButtonClickHanld={ this.eventDeleted }
                            removeSchedule={ this.props.checkInUse }
                            scheduleList={ this.props.list }
                            addShift={ this.props.addShift }
                            addRestDay={ this.props.addRestDay }
                            fetchShiftsData={ this.props.fetchShiftsData }
                            removeAssignButton={ this.removeAssignButton }
                        />
                    </div>
                ) }

            </Wrapper>
        );
    }
}

export default Calendar;
