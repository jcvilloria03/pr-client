import styled from 'styled-components';

export const EventViewWrapper = styled.div`
    .sl-c-popover--left {
        &:before {
            right: -8px;
            left: auto;
            border-width: 7.5px 0 7.5px 8px;
            border-color: transparent transparent transparent #fff;
        }
    }

    .sl-c-popover__header {
        display: flex;
        align-items: center;
        justify-content: space-between;
        margin-bottom: 1rem;
        padding-bottom: 1rem;
        border-bottom: 1px solid #ccc;

        .sl-c-actions {
            position: relative;
            svg {
                color: #ccc;
                height: 12px;
                width: 12px;
            }

            button:focus {
                outline: none;
            }
        }
    }

    .sl-c-popover__title {
        font-family: 'sourcesanspro-bold', sans-serif;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;

        &.sl-c-popover__title {
            margin-bottom: 0;
        }
    }

    .sl-c-popover__body {
        +.sl-c-popover__footer {
            margin-top: 1rem;
        }
    }

    .sl-c-popover__footer {
        display: flex;
        justify-content: flex-end;
    }

    .header_action{
        position: relative;

        .btn_grpAction{
            display: inline-flex;
            justify-content: flex-start;
            align-items: stretch;
            width:100% !important;

            button{
                padding: 0.25rem 1rem;
                font-size: .9rem;
                border-color: #ccc;
                background-color: transparent;
                margin: 0;
                width:50%;
                border-radius: 0;

                &:focus{
                    outline:none;
                }

                &:first-child{
                    border-right: none;
                    border-top-left-radius: 0.25rem;
                    border-bottom-left-radius: 0.25rem;
                }

                &:last-child{
                    border-left: none;
                    border-top-right-radius: 0.25rem;
                    border-bottom-right-radius: 0.25rem;
                }
            }

            .active{
                color: #fff;
                background-color: #00a5e5;
            }
        }
    }

    .schdule_tooltip {
        display: flex;
        align-items: center;
        font-size: 14px;
        margin-bottom:5px;

        button{
            width: 20px;
            height: 20px;
            padding: 0;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
        }

        a{
            padding: 0;
            border-bottom: none;
            padding-left: 6px;
        }

        .bg_tooltip {
            position: relative;
            display: inline-block;
            width:20px;
            height:20px;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
            margin-left: 6px;
            text-align: center;

            label{
                font-size: 15px;
                font-weight: bold;
            }
        }

        .bg_tooltip .tooltiptext {
            visibility: hidden;
            width: 242px;
            background-color: #fff;
            color: #03a9f4;
            border: 2px solid #03a9f4;
            text-align: left;
            border-radius: 6px;
            padding: 5px 16px;
            position: absolute;
            left: 26px;
            top: -50%;
            transform: translateY(-20%);
            z-index: 1;
        }

        .bg_tooltip .tooltiptextStart{
            transform: translateY(-26%);
        }

        .bg_tooltip .tooltiptextEnd{
            transform: translateY(-20%);
            width: 262px;
        }

        .bg_tooltip .tooltiptextHours{
            transform: translateY(-17%);
            width: 255px;
        }

        .bg_tooltip:hover .tooltiptext {
            visibility: visible;
        }

        .bg_tooltip .tooltiptext::after {
            content: " ";
            position: absolute;
            top: 50%;
            right: 99%;
            margin-top: -5px;
            border: solid #03a9f4;
            border-width: 0 2px 2px 0;
            display: inline-block;
            padding: 4px;
            transform: rotate(135deg);
            background-color: #ffff;
        }
    }

    .btnFlexi{
        width:100%;

        .btnEdit{
            width:100%;
            margin-left:0 !important;
        }
    }

    .flexidetail>div>div>span{
        display:none;
    }

    .flexidetail>div>div>.DayPickerInput{
        width:100%;

        input{
            width:100%;
            border:1px solid #474747;
        }
    }

    .switch input:checked + .slider:before {
        transform:translateX(28px) !important;
    }

    .flexidetail{
        .input_main{
            margin-bottom: 15px;

            .schdule_tooltip{
                margin: 0 !important;
            }

            .DayPickerInput{
                width:100%;
                input{
                    width:100%;
                }
            }
        }

        .add_switch{
            .switch_label{
                margin-bottom:0 !important;
                margin-left:10px;
            }
        }
        .repeat_day{
            margin-bottom: 15px;

            .input_from{
                display:flex;
                width:95%;
                flex-wrap:wrap;
            }
        }

        .schdule_tooltip{
            margin-bottom:7px !important;
        }

        .input-main-margin{
            margin-top: 1rem;
        }

        .input_main:nth-child(2){
            margin-top: 1rem;
        }

        .input_main>div>span{
            display:none;
        }

        .repeat_input{
            margin-bottom:20px;
            display:flex;
            align-items:center;
            justify-content:end;

            .schedule_timeSect{
                .editing{
                    input{
                        width:80%;
                        margin-left:20px;
                    }
                }

                p{
                    margin-left:-10px;
                }
            }
        }

        .summary{
            margin-bottom: 20px;

            .summary_header {
                display: flex;

                .schdule_tooltip{
                    span{
                        white-space: nowrap;
                        text-overflow: ellipsis;
                        overflow: hidden;
                        width: 176px;
                    }
                }
            }

            .summary-sub{
                background: #E5F6FC;
                padding: 10px;
                border-radius: 7px;
                border: 1px solid rgba(0, 165, 229, 0.1);

                .summary-1{
                    display:flex;
                    margin-bottom:20px;
                    width:100%;
                    div{
                        width:50%;
                    }
                    p{
                        margin:0;
                        font-weight:700;
                        font-size: 14px;
                    }
                }

                .summary-2{
                    margin-top:20px;

                    p{
                        margin:0;
                        font-weight:700;
                        font-size: 14px;
                    }
                }
            }
        }
    } 

    .add_switch{
        margin-top: 16px;
        margin-bottom: 14px;
    }

    .add_checkswitch{
        display: flex;
        align-items: center;
    }

    .add_checkswitch label{
        margin-bottom: 0px;
        padding-right: 10px;
    }

    .add_checkswitch .switch_label{
        margin-left: 15px;
            font-size: 14px;
    }

    .add_switch .switch {
        position: relative;
        display: inline-block;
        width: 35px;
        height: 8px;
    }

    .switch input { 
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 17px;
        width: 17px;
        left: 0px;
        top: -5px;
        background-color: #474747;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background: #cccccc;
    }

    input:checked + .slider:before{
        background: #83D24B;
        left: -10px;
        top: -5px;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .Input_from{
        display: flex;
        margin-bottom: 14px;
    }

    .input_check {
        display: flex;
        align-items: center;
        margin-right: 15px;
    }

    .repeat_titls .input_repeat .Select .Select-control{
        width: 72px !important;
        margin-right: 5px;
    }

    .form-group{
        margin-bottom:0;

        select{
            border: 1px solid #95989a;
            height: 46px;
            width: 100%;
            &:focus-visible{
            outline:none;
            }
        }
    }

    .input_repeat {
        display: flex;
        align-items: center;
    }

    .input_repeat .Select .Select-control {
        width: 10%;
    }

    .repeat_ends label{
        margin:15px 0px 5px;
    }

    .repeat_intervals .radio-group > div:nth-child(2){
        margin-top:9px !important;
        margin-bottom:0px !important;
    }

    .repeat_intervals{
        .radio-group{
            #After{
                transform: translateY(0px);
            }
        }
    }

    .radio-group{
        >div{
            font-size: 14px;
            padding: 0 !important;

            >div{
                display: flex;
                align-items: center;
            }
        }
    }

    .repeat_input{
        margin-top:-35px;

        .schedule_timeSect{
            display:flex;
            align-items: center;
            p{
                margin-bottom:0;
                margin-left:15px;
            }
        }
    }

    .schedule_timeSect {
        z-index: 1;
    }

    .flatpickr-input {
        height: 46px;
        width: 368px;
        border: 1px solid #474747;
        padding: 5px 12px 0;
    }
`;
