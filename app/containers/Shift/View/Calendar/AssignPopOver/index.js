/* eslint-disable no-unused-expressions */
/* eslint-disable no-sequences */
/* eslint-disable import/first */
import React from 'react';
import _ from 'lodash';
import { H6 } from 'components/Typography';
import { EventViewWrapper } from './styles';
import Button from 'components/Button';
import Select from 'components/Select';
import CustomCheckbox from 'components/CustomCheckbox';
import A from 'components/A';
import Input from 'components/Input';
import RadioGroup from 'components/RadioGroup';
import Radio from 'components/Radio';
import moment from 'moment';
import { formatDate } from 'utils/functions';
import Icon from 'components/Icon';
import Flatpickr from 'react-flatpickr';
import 'flatpickr/dist/themes/material_green.css';
import {
    DATE_FORMATS,
    REPEAT_WEEKLY_VALUE,
    REPEAT_BY_MONTH,
    NEVER_OPTION,
    REPEAT_DAYS,
    AFTER_OPTION,
    END_AFTER_DEFAULT_VALUE
} from 'utils/constants';
import { company } from 'utils/CompanyService';
import { CloseButton } from '../styles';

/**
 *
 * CalendarEventPopOver Component
 *
 */
class AssignPopOver extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        singleEvent: React.PropTypes.object,
        closeEventPopOver: React.PropTypes.func,
        removeAssignButton: React.PropTypes.func,
        scheduleList: React.PropTypes.array,
        addShift: React.PropTypes.func,
        addRestDay: React.PropTypes.func,
        fetchShiftsData: React.PropTypes.func
    };

    constructor( props ) {
        super( props );

        this.state = {
            defaultOption: 'shift',
            date: moment( new Date() ).format( 'YYYY-MM-DD' ),
            addData: {
                schedule_id: '',
                start: this.props.singleEvent.val.clickedDate,
                end: this.props.singleEvent.val.clickedDate
            },
            selectData: [],
            selectedDay: null,
            form: this.getDefaultFormValues(),
            neverOption: NEVER_OPTION,
            afterOption: AFTER_OPTION,
            repeatEveryOptions: _.range( 30 ).map( ( e, i ) => ({ label: i + 1, value: i + 1 }) ),
            start: '',
            restdaySummary: '',
            startDateOptions: {
                minDate: new Date( null ),
                maxDate: null
            },
            endDateOptions: {
                minDate: new Date( null )
            }
        };
    }

    getDefaultFormValues() {
        return {
            startDate: '',
            schedule_repeat: false,
            repeat: this.getDefaultRepeatValues()
        };
    }

    getDefaultRepeatValues() {
        const selectedDayname = moment( new Date( this.props.singleEvent.val.clickedDate ) ).format( 'ddd' );

        return {
            typeObject: REPEAT_WEEKLY_VALUE,
            type: REPEAT_WEEKLY_VALUE.value,
            repeat_every: 1,
            endsOption: NEVER_OPTION,
            end_on: null,
            end_after: null,
            end_never: true,
            repeat_on: REPEAT_DAYS.map( ( day ) => ( day.label === selectedDayname ? { ...day, selected: true } : day ) ),
            repeat_by: REPEAT_BY_MONTH
        };
    }

    getSchedule = () => {
        if ( Object.keys( this.props.scheduleList ).length === 0 ) {
            return [];
        }
        return this.props.scheduleList.map( ( ele ) => (
            {
                ele,
                value: ele.id,
                label: ele.name
            }) );
    }

    endRepeatOptions( value ) {
        if ( value === this.state.neverOption ) {
            this.state.form.repeat.end_after = null;
            this.state.form.repeat.end_on = null;
            this.state.form.repeat.end_never = true;
        } else if ( value === this.state.afterOption ) {
            this.state.form.repeat.end_never = false;
            this.state.form.repeat.end_on = null;
            this.state.form.repeat.end_after = END_AFTER_DEFAULT_VALUE;
        } else {
            this.state.form.repeat.end_never = false;
            this.state.form.repeat.end_after = null;
        }
    }

    assignShift = () => {
        const shift = {
            employee_id: this.props.singleEvent.val.employeeId,
            start: moment( this.state.addData.start ).format( 'YYYY-MM-DD' ),
            end: moment( this.state.addData.end ).format( 'YYYY-MM-DD' ),
            startDate: this.state.addData.start,
            endDate: this.state.addData.end,
            schedule: this.state.selectData.ele,
            schedule_id: this.state.selectData.ele.id
        };

        this.props.addShift( shift, () => {
            this.props.fetchShiftsData();
        });

        this.props.closeEventPopOver();
        this.props.removeAssignButton();
    }

    restDays = () => {
        let repeat = null;

        if ( this.state.form.schedule_repeat ) {
            repeat = {
                rest_days: this.state.form.repeat.repeat_on.filter( ( r ) => r.selected ).map( ( r ) => r.value ),
                repeat_every: this.state.form.repeat.repeat_every,
                end_never: ( this.state.form.repeat.endsOption === 'never' ),
                end_after: this.state.form.repeat.end_after
            };
        }

        const data = {
            company_id: company.getLastActiveCompanyId(),
            employee_id: this.props.singleEvent.val.employeeId,
            startDate: this.state.start === '' ? moment( this.props.singleEvent.val.clickedDate ).format( 'MMMM D, YYYY' ) : this.state.start,
            start_date: this.state.start === '' ? this.props.singleEvent.val.clickedDate : moment( this.state.start ).format( 'YYYY-MM-DD' ),
            end_date: null,
            repeat
        };

        this.props.addRestDay( data, () => {
            this.props.fetchShiftsData();
        });

        this.props.closeEventPopOver();
        this.props.removeAssignButton();
    }

    render() {
        const {
            addData,
            selectData,
            form,
            neverOption,
            afterOption,
            repeatEveryOptions,
            startDateOptions,
            endDateOptions
        } = this.state;
        const {
            singleEvent
        } = this.props;

        return (
            <EventViewWrapper>
                <div>
                    <div className="sl-c-popover">
                        <div className="sl-c-popover__header">
                            <H6 className="sl-c-popover__title">Assign Shift/Rest Day</H6>

                            <div className="sl-c-actions">
                                <CloseButton onClick={ this.props.closeEventPopOver }>
                                    <Icon name="remove" className="icon" />
                                </CloseButton>
                            </div>
                        </div>
                    </div>
                    <div className="header_action">
                        <div className="btn_grpAction">
                            <Button
                                id="button-filter"
                                label="Shift"
                                type="grey"
                                onClick={ () => this.setState({ defaultOption: 'shift' }) }
                                className={ this.state.defaultOption === 'shift' ? 'active' : '' }
                            />
                            <Button
                                id="button-filter"
                                label="Rest Day"
                                type="grey"
                                onClick={ () => this.setState({ defaultOption: 'rest', selectData: '' }) }
                                className={ this.state.defaultOption === 'rest' ? 'active' : '' }
                            />
                        </div>
                    </div>
                    {this.state.defaultOption === 'shift' ? (
                        <div className="flexidetail pt-1">
                            <div className="input_main">
                                <div className="schdule_tooltip">* Schedule
                                    <div className="bg_tooltip">
                                        <div>?</div>
                                        <span className="tooltiptext">Covers working hours, break periods, and other planned activities.</span>
                                    </div>
                                </div>
                                <Select
                                    id="scheduled"
                                    required
                                    placeholder="Choose an schedule"
                                    data={ this.getSchedule() }
                                    ref={ ( ref ) => {
                                        this.scheduled = ref;
                                    } }
                                    onChange={ ( value ) => {
                                        this.setState({ selectData: value, startDateOptions: { minDate: value.ele.start_date, maxDate: null }, endDateOptions: { minDate: value }, addData: { ...addData, schedule_id: value.value }});
                                    } }
                                />
                            </div>
                            {this.state.selectData.ele && this.state.selectData.ele.id ? (
                                <div className="summary">
                                    <div className="summary_header">
                                        <div>
                                            <strong className="schdule_tooltip summary_tooltip"><span>{ selectData.label } summary</span>
                                                <div className="bg_tooltip">
                                                    <div>?</div>
                                                    <span className="tooltiptext">Schdule A Summary</span>
                                                </div>
                                            </strong>
                                        </div>
                                        <div>
                                            <A
                                                href
                                                onClick={ ( e ) => {
                                                    e.preventDefault();
                                                    window.location.href = `/time/schedules/edit/${this.state.selectData.ele.id}/`;
                                                } }
                                            >
                                                View schedule details
                                            </A>
                                        </div>
                                    </div>
                                    <div className="summary-sub">
                                        <div className="summary-1">
                                            <div>
                                                <p>Schedule type:</p>{ selectData.ele && selectData.ele.type === 'fixed' ? 'Fixed' : 'Flexibile' }
                                            </div>
                                            <div>
                                                <p>Start and end time:</p>{ selectData.ele && selectData.ele.start_time || selectData.ele && selectData.ele.end_time === true ? `${selectData.ele && selectData.ele.start_time} to ${selectData.ele && selectData.ele.end_time}` : '' }
                                            </div>
                                        </div>
                                        <div className="summary-2">
                                            <p>Schedule effective start date:</p>{ selectData.ele && selectData.ele.start_date ? moment( selectData.ele && selectData.ele.start_date ).format( 'MMMM D, YYYY' ) : ''}
                                        </div>
                                        <div className="summary-2">
                                            <p>Schedule effective end date:</p>{ selectData.ele && selectData.ele.end_date === undefined ? 'Never ends' : selectData.ele && selectData.ele.end_date }
                                        </div>
                                    </div>
                                </div>
                            ) : ''}
                            <div className="input_main ">
                                <div className="schdule_tooltip">* Shift effective start date
                                    <div className="bg_tooltip">
                                        <div>?</div>
                                        <span className="tooltiptext">Satrt date refer to the date when the employee should start following the selected schedule.</span>
                                    </div>
                                </div>
                                <Flatpickr
                                    className="date_piker"
                                    name="start_date"
                                    ref={ ( ref ) => { this.start_date = ref; } }
                                    options={ startDateOptions }
                                    onChange={ ([eve]) => this.setState({ endDateOptions: { minDate: eve }, addData: { ...addData, start: eve }}) }
                                    selectedDay={ formatDate( singleEvent.val.clickedDate, DATE_FORMATS.API ) }
                                    value={ singleEvent.val.clickedDate }
                                />
                            </div>
                            <div className="input_main input-main-margin">
                                <div className="schdule_tooltip">* Shift effective end date
                                    <div className="bg_tooltip">
                                        <div>?</div>
                                        <span className="tooltiptext">The employee should follow the selected schedule until the end date indicated here.</span>
                                    </div>
                                </div>
                                <Flatpickr
                                    name="end_date"
                                    ref={ ( ref ) => { this.end_date = ref; } }
                                    options={ endDateOptions }
                                    onChange={ ([eve]) => this.setState({ startDateOptions: { ...startDateOptions, maxDate: eve }, addData: { ...addData, end: eve }}) }
                                    selectedDay={ formatDate( singleEvent.val.clickedDate, DATE_FORMATS.API ) }
                                    value={ singleEvent.val.clickedDate }
                                />
                            </div>

                        </div>
                    ) : (
                        <div className="flexidetail pt-1">
                            <div>
                                <div className="schdule_tooltip">* Rest day effective start date
                                    <div className="bg_tooltip">
                                        <div>?</div>
                                        <span className="tooltiptext">Satrt date refer to the date when the employee should start following the selected schedule.</span>
                                    </div>
                                </div>
                                <Flatpickr
                                    name="start_date"
                                    selectedDay={ formatDate( singleEvent.val.clickedDate, DATE_FORMATS.API ) }
                                    value={ singleEvent.val.clickedDate }
                                    ref={ ( ref ) => { this.start_date = ref; } }
                                    onChange={ ([val]) => this.setState({ start: val }) }
                                />
                            </div>
                            <div className="add_switch add_checkswitch">
                                <div className="switch">
                                    <input
                                        type="checkbox"
                                        id="repeat_checkbox"
                                        name="restday_repeat"
                                        checked={ form.schedule_repeat }
                                        ref={ ( ref ) => { this.restday_repeat = ref; } }
                                        onChange={ () => {
                                            this.setState({ form: { ...form, schedule_repeat: !form.schedule_repeat }});
                                        } }
                                    />
                                    <label htmlFor="repeat_checkbox" className="slider round"></label>
                                </div>
                                <div className="switch_label schdule_tooltip">Rest day repeat
                                        <div className="bg_tooltip" id="rest_day">
                                            <div>?</div>
                                            <span className="tooltiptext">Which days of the week will the rest day/s fall on? How often and until when will the employee have the selected rest day/s?</span>
                                        </div>
                                </div>
                            </div>
                            { form.schedule_repeat === true ? (
                                <div className="repeat_day">
                                    <div>
                                        <div>Choose rest days</div>
                                        <div className="input_from">
                                            {form.repeat.repeat_on.map( ( val ) => (
                                                <div className="input_check">
                                                    <CustomCheckbox
                                                        label={ val.label }
                                                        checkValue={ val.selected }
                                                        onChange={ () => {
                                                            const selectedDays = form.repeat.repeat_on.map( ( data ) => ( data.value === val.value ? ({ ...data, selected: !data.selected }) : data ) );
                                                            this.setState({
                                                                form: {
                                                                    ...form,
                                                                    repeat: {
                                                                        ...form.repeat,
                                                                        repeat_on: selectedDays
                                                                    }
                                                                }
                                                            });
                                                        } }
                                                    />
                                                </div>
                                        ) )
                                        }
                                        </div>
                                    </div>
                                    <div className="repeat_titls">
                                        <div>Repeat every</div>
                                        <div className="form-group input_repeat">
                                            <Select
                                                id="allowance_type"
                                                data={ repeatEveryOptions }
                                                name="repeat_every"
                                                value={ form.repeat.repeat_every }
                                                onChange={ ( value ) => {
                                                    this.setState({ form: { ...form, repeat: { ...form.repeat, repeat_every: value.value }}});
                                                } }
                                            /> weeks
                                        </div>
                                    </div>
                                    <div className="repeat_ends">
                                        <div>Ends</div>
                                    </div>
                                    <div className="repeat_intervals">
                                        <RadioGroup
                                            id="radioAmendmentReturn"
                                            name="repeat-ends"
                                            value={ form.repeat.endsOption }
                                            vertical
                                            className="radio-group"
                                            onChange={ ( value ) => {
                                                this.setState({
                                                    form:
                                                    {
                                                        ...form,
                                                        repeat: {
                                                            ...form.repeat,
                                                            endsOption: this.endRepeatOptions( value ) || value
                                                        }
                                                    }
                                                });
                                            } }
                                        >
                                            <Radio value={ neverOption }>Never</Radio>
                                            <Radio value={ afterOption } className="after after_radio">After</Radio>
                                            <div className="repeat_input">
                                                <div className="schedule_timeSect">
                                                    <Input
                                                        id="membership_program1"
                                                        className="editing"
                                                        value={ form.repeat.end_after || '' }
                                                        disabled={ form.repeat.endsOption === 'never' }
                                                        onChange={ ( value ) => {
                                                            this.setState({
                                                                form:
                                                                {
                                                                    ...form,
                                                                    repeat: {
                                                                        ...form.repeat,
                                                                        end_after: value
                                                                    }
                                                                }
                                                            });
                                                        } }
                                                    />
                                                    <p>occurrences</p>
                                                </div>
                                            </div>
                                        </RadioGroup>
                                    </div>
                                    {( form.repeat.repeat_on.filter( ( r ) => r.selected ).length > 0 ) &&
                                        <b>
                                            Summary:
                                            {form.repeat.repeat_every === 1
                                            ?
                                                ' Weekly on '
                                            :
                                                ` Every ${form.repeat.repeat_every} weeks on `
                                            }

                                            {( form.repeat.repeat_on.filter( ( r ) => r.selected ).length === 7 )
                                            ?
                                                'all days'
                                            :
                                                `${this.state.form.repeat.repeat_on.filter( ( r ) => r.selected ).map( ( r ) => ( r.value.charAt( 0 ).toUpperCase() + r.value.slice( 1 ) ) ).join( ', ' )}`
                                            }

                                            {this.state.form.repeat.endsOption === afterOption &&
                                                `, ${form.repeat.end_after} times`
                                            }
                                        </b>
                                    }
                                </div>
                            ) : '' }
                        </div>
                    )}

                    <div className="btnFlexi d-flex justify-content-center">
                        <button
                            className="btnEdit ml-1"
                            onClick={
                                this.state.defaultOption === 'shift'
                                    ? () => this.assignShift()
                                    : () => this.restDays()
                            }
                        >
                            Assign
                        </button>
                    </div>
                </div>
            </EventViewWrapper>
        );
    }
}

export default AssignPopOver;
