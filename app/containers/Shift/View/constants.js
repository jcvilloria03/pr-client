/*
 *
 * Shift constants
 *
 */

export const DEFAULT_ACTION = 'app/Shift/View/DEFAULT_ACTION';
export const SET_LOADING = 'app/Shift/View/SET_LOADING';
export const SET_MINI_LOADING = 'app/Shift/View/SET_MINI_LOADING';
export const SET_NOTIFICATION = 'app/Shift/View/SET_NOTIFICATION';
export const GET_NOTIFICATION = 'app/Shift/View/GET_NOTIFICATION';
export const GET_DEFAULT_SCHEDULES = 'app/Shift/View/GET_DEFAULT_SCHEDULES';
export const SET_DEFAULT_SCHEDULE = 'app/Shift/View/SET_DEFAULT_SCHEDULE';
export const GET_SHIFTS = 'app/Shift/View/GET_SHIFTS';
export const SET_SHIFTS = 'app/Shift/View/SET_SHIFTS';
export const SET_FILTER_DATA = 'app/Shift/View/SET_FILTER_DATA';
export const GENERATE_CSV = 'app/Shift/View/GENERATE_CSV';
export const SET_SCHEDULE = 'app/Shift/View/SET_SCHEDULE';
export const GET_SCHEDULE = 'app/Shift/View/GET_SCHEDULE';
export const SET_PAGINATION = 'app/Shift/View/SET_PAGINATION';
export const OVERLAP_SHIFT = 'app/Shift/View/OVERLAP_SHIFT';
export const ADD_SHIFT = 'app/Shift/View/ADD_SHIFT';
export const ADD_REST_DAY = 'app/Shift/View/ADD_REST_DAY';
export const SET_UNASSIGNED_SHIFT = 'app/Shift/View/SET_UNASSIGNED_SHIFT';
export const GET_UNASSIGNED_SHIFT = 'app/Shift/View/GET_UNASSIGNED_SHIFT';
export const UNASSIGN_REST_DAY = 'app/Shift/View/UNASSIGN_REST_DAY';

export const CALENDAR_FREQUENCY = { DAY: 'day', WEEK: 'week' };
