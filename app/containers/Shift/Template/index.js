/* eslint-disable react/no-unescaped-entities */
/* eslint-disable jsx-a11y/href-no-hash */
/* eslint-disable import/first */
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import makeSelectShiftTemplete from './selectors';
import { FieldErrors } from './constants';

import { browserHistory } from 'utils/BrowserHistory';

import { H3, H6, H4, P, H5 } from 'components/Typography';
import A from 'components/A';
import Table from 'components/Table';

import {
    NavWrapper,
    PageWrapper
} from './styles';
/**
 *
 * ShiftTemplete
 *
 */
export class ShiftTemplete extends React.Component { // eslint-disable-line react/prefer-stateless-function
    /**
     *
     * ShiftTemplete render method
     *
     */
    render() {
        const tableColumns = [
            {
                header: 'Column name',
                accessor: 'name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.name}
                    </div>
                )
            },
            {
                header: 'Description',
                accessor: 'description',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.description}
                    </div>
                )
            },
            {
                header: 'Required field?',
                accessor: 'required_field',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.required_field}
                    </div>
                )
            },
            {
                header: 'Sample values',
                accessor: 'sample_values',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.sample_values}
                    </div>
                )
            }
        ];

        const tableFiledErrorColumns = [
            {
                header: 'Row Number',
                accessor: 'row_number',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.row_number}
                    </div>
                )
            },
            {
                header: 'Error Type',
                accessor: 'error_type',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.error_type}
                    </div>
                )
            }
        ];

        return (
            <div>
                <Helmet
                    title="Shift Templete"
                    meta={ [
                        { name: 'description', content: 'Description of Shift Templete' }
                    ] }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/shifts/assign/batch', true );
                            } }
                        >
                            &#8592; Back to Shifts
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="heading">
                                <H3 className="main_title">Batch Assign Shift</H3>
                                <div className="template-1">
                                    <div className="template-main-heading">
                                        <div className="template-heading">
                                            <H3>1</H3>
                                        </div>
                                        <span> Download the upload template</span>
                                    </div>
                                    <div className="main-template-1 row">
                                        <div className="col-xs-6 template-col-1">
                                            <div className="personal">
                                                <div className="row steps">
                                                    <div className="step col-sm-6 ">
                                                        <div className="template">
                                                            <H6>Step 1:</H6>
                                                            <p className="margin-p">Download and fill out the batch upload template.</p>
                                                            <p className="font-p"><A target="_blank" style={ { color: '#00A5E5' } } >Click here to view the upload guide.</A></p>
                                                            <A href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/employee_personal_info_template.csv" download>Download Template</A>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-6 template-col-2">
                                            <p>You can download the Batch Assign Shifts Template by clicking the `Download template` button.</p>
                                            <p className="mt-4">After clicking this button, you will get a CSV file which requires information needed to upload deductions. Open the file and edit it as necessary outside the system.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="template-2">
                                    <div className="template-main-heading">
                                        <div className="template-heading">
                                            <H3>2</H3>
                                        </div>
                                        <span>Fill out the CSV template</span>
                                    </div>
                                    <p>To help you fill-out the CSV template, the Batch Assign Shifts Upload Guide is provided.</p>
                                    <div>
                                        <Table
                                            columns={ tableColumns }
                                            ref={ ( ref ) => { this.guidTemplete = ref; } }
                                        />
                                    </div>
                                </div>
                                <div className="template-3">
                                    <div className="template-main-heading">
                                        <div className="template-heading">
                                            <H3>3</H3>
                                        </div>
                                        <span> Upload the accomplished CSV file</span>
                                    </div>
                                    <div className="main-template-1 row">
                                        <div className="col-xs-6 template-col-1">
                                            <div className="personal">
                                                <div className="row steps">
                                                    <div className="step col-sm-6 ">
                                                        <div className="template">
                                                            <H6>Step 2:</H6>
                                                            <p>Download and fill-out the Employee Personal Information Template.</p>
                                                            <A href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/employee_personal_info_template.csv" download>Choose File</A>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-6 template-col-2">
                                            <P>After completely filling out the spreadsheet, save it as a CSV file on your personal drive. You are now ready to upload this into the system.</P>
                                            <P className="mt-4">On Step 2 Upload Template section, choose the file you wish to upload.</P>
                                        </div>
                                    </div>
                                </div>
                                <div className="template-4">
                                    <div className="main-template-1 row">
                                        <div className="col-xs-6 template-col-1">
                                            <div className="personal">
                                                <div className="row steps">
                                                    <div className="step col-sm-6 ">
                                                        <div className="template">
                                                            <H6>Step 2:</H6>
                                                            <p><strong>Shifts_2017.csv<a href="#">Replace</a></strong></p>
                                                            <A href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/employee_personal_info_template.csv" download>Choose File</A>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-6 template-col-2">
                                            <P>Once the accomplished CSV file is chosen, click the &apos;Upload&apos; button.</P>
                                        </div>
                                    </div>
                                </div>
                                <div className="template-4">
                                    <div className="main-template-1 row">
                                        <div className="col-xs-6 template-col-1">
                                            <div className="personal">
                                                <div className="row steps">
                                                    <div className="step col-sm-6 ">
                                                        <div className="template">
                                                            <H6>Step 2:</H6>
                                                            <p><strong>Shifts_2017.csv<a href="#">Replace</a></strong></p>
                                                            <div className="progressBar">
                                                                <div className="innerProgress">
                                                                </div>
                                                            </div>
                                                            <H4>Uploading and validating</H4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-6 template-col-2">
                                            <P>The system will validate the file against the acceptable values. You will see the status of the uplaod and validation process.</P>
                                        </div>
                                    </div>
                                </div>
                                <P style={ { marginTop: '5px' } }>The system will present you the errors on your CSV file, if any. You will have to edit your file to conform to the format presented in the Batch Assign Shifts Upload Guide to avoid such errors. You will also have the ability to copy the errors to your clipboard, if demand necessary.</P>
                                <H5 style={ { color: 'black' } }>Data field errors</H5>
                                <div className="table-2">
                                    <Table
                                        columns={ tableFiledErrorColumns }
                                        data={ FieldErrors }
                                        ref={ ( ref ) => { this.fielderrorData = ref; } }
                                    />
                                </div>
                                <div className="template-4">
                                    <div className="main-template-1 row">
                                        <div className="col-xs-6 template-col-1">
                                            <div className="personal">
                                                <div className="row steps">
                                                    <div className="step col-sm-6 ">
                                                        <div className="template">
                                                            <H6>Step 2:</H6>
                                                            <p><strong>Shifts_2017.csv<a href="#">Replace</a></strong></p>
                                                            <div className="progressBar">
                                                                <div className="innerProgress">
                                                                </div>
                                                            </div>
                                                            <H4>Uploading and validating</H4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-6 template-col-2">
                                            <P>Once you are done correcting the errors, upload your updated CSV file.</P>
                                        </div>
                                    </div>
                                </div>
                                <div className="template-1">
                                    <div className="template-main-heading">
                                        <div className="template-heading">
                                            <H3>4</H3>
                                        </div>
                                        <span>Review the uploaded data</span>
                                    </div>
                                    <div className="main-template-1 row">
                                        <P className="main-template-1-p">All the contents of your CSV file will be presented once the validation is done. Once done, click "Submit."</P>
                                        <div className="col-xs-6 template-col-1">
                                            <div className="personal">
                                                <div className="row steps">
                                                    <div className="step col-sm-6 ">
                                                        <div className="template">
                                                            <H6>Step 1:</H6>
                                                            <p className="margin-p">A batch upload template is available for you to download and fill out.</p>
                                                            <p><A target="_blank" style={ { color: '#00A5E5' } } >Click here to view the upload guide.</A></p>
                                                            <A href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/employee_personal_info_template.csv" download>Download Template</A>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-6 template-col-1">
                                            <div className="personal">
                                                <div className="row steps">
                                                    <div className="step col-sm-6 ">
                                                        <div className="template">
                                                            <H6>Step 2:</H6>
                                                            <P className="transform-p"><strong style={ { color: 'black' } }>Shifts_2017.csv<a href="#">Replace</a></strong></P>
                                                            <A href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/employee_personal_info_template.csv" download>Upload</A>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                    <div className="submit-section">
                        <Container>
                            <div className="buttons">
                                <button className="button1">Cancel</button>
                                <button className="button2">Submit</button>
                            </div>
                        </Container>
                    </div>
                </PageWrapper>
            </div>
        );
    }
}

ShiftTemplete.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
    ShiftTemplete: makeSelectShiftTemplete()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return {
        dispatch
    };
}

export default connect( mapStateToProps, mapDispatchToProps )( ShiftTemplete );
