import { createSelector } from 'reselect';

/**
 * Direct selector to the shiftTemplete state domain
 */
const selectShiftTempleteDomain = () => ( state ) => state.get( 'shiftTemplete' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by ShiftTemplete
 */

const makeSelectShiftTemplete = () => createSelector(
  selectShiftTempleteDomain(),
  ( substate ) => substate.toJS()
);

export default makeSelectShiftTemplete;
export {
  selectShiftTempleteDomain
};
