import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;
`;
export const PageWrapper = styled.div`
    p{
        font-size:15px !important;
        letter-spacing: .1px;
    }
    .row{
        padding-bottom: 0 !impotant;
    }
    .template-col-1 {
        padding-right: 0;
        margin: -4px 0;
    }
    strong{
        a{
            margin-left:3px;
            font-weight:400;
        }
    }
    .template, .upload {
        border-radius: 0.5rem !important;
    }
    padding-top: 120px;
    .content {
        margin-top: 34px;
        .entriTitle{
            text-align:right;
        }
         
        .heading {
            flex-direction: column;
            margin: 0 auto 50px auto;
            .main_title {
                font-weight: 600;
                text-align: center;
                margin-bottom: 63px;
            }
            .template-1{
                margin-top:28px;
                .template-main-heading{
                    display: flex;
                    align-items: center;
                    color: black;
                    .template-heading{
                        background: white;
                        border: 1px solid black;
                        width: max-content;
                        width: 35px;
                        height: 35px;
                        border-radius: 50%;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        h3{
                            margin-top: 16px;
                            font-weight: 600;
                            font-size:16px;
                        }
                    }
                    span{
                        margin-left: 15px;
                        font-weight: 600;
                        font-size:18px;
                    }
                }
                .main-template-1{
                    margin-top:3rem;

                    .main-template-1-p{
                        margin-bottom: 4px !important;
                        margin-left:20px;
                    }
                    .template-col-1{
                        padding-right: 0;
                        .personal, .payroll {
                            & > h4 {
                                text-align: left;
                            }
                            & > button {
                                padding: 12px 40px;
                                margin-top: 2rem;
                            }
                            .steps {
                                display: flex;
                                align-items: stretch;
                                flex-wrap: wrap;
                            }
                            .step {
                                min-width: 400px;
                                margin: 10px auto;
                                width:100%
                            }
                            .template, .upload {
                                text-align: center;
                                padding: 2rem;
                                border: 2px dashed #000;
                                border-radius: 12px;
                                width: 100%;
                                height: 100%;
                                .gntQha{
                                    color: #00A5E5;
                                    transform: translateY(10px);
                                }
                                .margin-p{
                                    margin-bottom:0 !important;
                                    font-size:15px;
                                }
                                .font-p{
                                    font-size:15px;
                                }
                                .fwujWf{
                                    font-size: 26px;
                                    font-weight: 600;
                                    color: black;
                                }
                                & > a {
                                    background-color: #FFF;
                                    color: #333;
                                    border: 1px solid #4ABA4A;
                                    padding: 8px 16px;
                                    border-radius: 30px;
                                    display: inline;
                                    vertical-align: middle;
                                    margin: 0 auto;
                                    max-width: 200px;
                                    font-size:15px;

                                    &:hover, &:focus, &:active:focus {
                                        outline: none;
                                        background-color: #fff;
                                        color: #4ABA4A;
                                    }
                                }
                            }
                            .upload > div:first-of-type > div > div {
                                margin: 0 auto;
                            }
                        }
                    }
                    .template-col-2{
                        margin-top:10px;
                        p{
                            margin-bottom: 6px !important;
                            font-size: 15px;
                        }
                    }
                }       
            }
            .template-2{
                margin-top: 30px;
                .template-main-heading{
                    display: flex;
                    align-items: center;
                    color:black;
                    .template-heading{
                        background: white;
                        border: 1px solid black;
                        width: max-content;
                        width: 35px;
                        height: 35px;
                        border-radius: 50%;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        h3{
                            margin-top: 16px;
                            font-weight: 600;
                            font-size:16px;
                        }
                    }
                    span{
                        margin-left: 15px;
                        font-weight: 600;
                        font-size:18px;
                    }
                }
                p{
                    margin-top: 3rem;
                }
            }
            .template-3{
                margin-top:20px;
                .template-main-heading{
                    display: flex;
                    align-items: center;
                    color:black;
                    font-size:18px;
                    .template-heading{
                        background: white;
                        border: 1px solid black;
                        width: max-content;
                        width: 35px;
                        height: 35px;
                        border-radius: 50%;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        h3{
                            margin-top: 16px;
                            font-weight: 600;
                            font-size:16px;
                        }
                    }
                    span{
                        margin-left: 15px;
                        font-weight: 600;
                    }
                }
                .main-template-1{
                    margin-top:3rem;
                    .template-col-1{
                        padding-right:0 !important;
                        .personal, .payroll {
                            & > h4 {
                                text-align: left;
                            }
                            & > button {
                                padding: 12px 40px;
                                margin-top: 2rem;
                            }
                            .steps {
                                display: flex;
                                align-items: stretch;
                                flex-wrap: wrap;
                            }
                            .step {
                                min-width: 400px;
                                margin: 10px auto;
                                width:100%
                            }
                            .template, .upload {
                                text-align: center;
                                padding: 2rem;
                                border: 2px dashed #000;
                                border-radius: 12px;
                                width: 100%;
                                height: 100%;
                                .gntQha{
                                    color: #00A5E5;
                                }
                                .fwujWf{
                                    color:black;
                                    font-size:1.5714286rem;
                                    font-weight:600;
                                }
                                & > a {
                                    background-color: #FFF;
                                    color: #333;
                                    border: 1px solid #4ABA4A;
                                    padding: 8px 16px;
                                    border-radius: 30px;
                                    display: inline;
                                    vertical-align: middle;
                                    margin: 0 auto;
                                    max-width: 200px;
                                    font-size:15px;

                                    &:hover, &:focus, &:active:focus {
                                        outline: none;
                                        background-color: #fff;
                                        color: #4ABA4A;
                                    }
                                }
                            }
                            .upload > div:first-of-type > div > div {
                                margin: 0 auto;
                            }
                        }
                    }
                    .template-col-2{
                        margin-top:10px;
                        p{
                            margin-bottom:20px !important;
                        }
                    }
                }       
            }
            .template-4{
                .template-main-heading{
                    display: flex;
                    align-items: center;
                    .template-heading{
                        background: white;
                        border: 1px solid black;
                        width: max-content;
                        width: 35px;
                        height: 35px;
                        border-radius: 50%;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        h3{
                            margin-top: 16px;
                            font-weight: 600;
                            font-size:16px;
                        }
                    }
                    span{
                        margin-left: 15px;
                        font-weight: 600;
                    }
                }
                .main-template-1{
                    margin-bottom: 0 !important;
                    .template-col-1{
                        .personal, .payroll {
                            & > h4 {
                                text-align: left;
                            }
                            & > button {
                                padding: 12px 40px;
                                margin-top: 2rem;
                            }
                            .steps {
                                display: flex;
                                align-items: stretch;
                                flex-wrap: wrap;
                            }
                            .step {
                                min-width: 400px;
                                margin: 10px auto;
                                width:100%
                            }
                            .template, .upload {
                                text-align: center;
                                padding: 2rem;
                                border: 2px dashed #000;
                                border-radius: 12px;
                                width: 100%;
                                height: 100%;
                                h4{
                                    color:#83D24B;
                                    font-size: 15px;
                                }
                                .progressBar{
                                    position: relative;
                                    display: block;
                                    height: 1rem;
                                    overflow: hidden;
                                    border: 1px solid #F0F4F6;
                                    border-radius: 1rem;
                                    width: 18%;
                                    margin: auto;
                                    margin-bottom:5px;

                                    .innerProgress{
                                        position: absolute;
                                        top: 0;
                                        left: 0;
                                        bottom: 0;
                                        margin: 2px;
                                        border-radius: 1rem;
                                        -webkit-transition: width .2s ease-in-out;
                                        transition: width .2s ease-in-out;
                                        background-color: #83D24B;
                                        width:75%;
                                    }
                                }
                                .gntQha{
                                    color: #00A5E5;
                                }
                                .fwujWf{
                                    color:black;
                                    font-size:1.85714286rem;
                                    font-weight:600;
                                }
                                & > a {
                                    background-color: #FFF;
                                    color: #333;
                                    border: 1px solid #4ABA4A;
                                    padding: 8px 16px;
                                    border-radius: 30px;
                                    display: inline;
                                    vertical-align: middle;
                                    margin: 0 auto;
                                    max-width: 200px;
                                    font-size:15px;

                                    &:hover, &:focus, &:active:focus {
                                        outline: none;
                                        background-color: #fff;
                                        color: #4ABA4A;
                                    }
                                }
                            }
                            .upload > div:first-of-type > div > div {
                                margin: 0 auto;
                            }
                        }
                    }
                    .template-col-2{
                        margin-top:10px;
                        p{
                            margin-bottom:20px !important;
                        }
                    }
                }       
            }
            .iHqZzh{
                font-size:20px
            }
            .table-2{
                margin-bottom:-20px;
                .hvYuOH{
                    margin-bottom:40px;
                    .ReactTable{
                        .rt-thead{
                            background-color:#EB7575;
                        }
                    }
                }
            }
        }
    }
    .submit-section{
        background:#F0F4F6;
        .container{
            padding: 0 0 ;
            .buttons{
                display:flex;
                padding: 13px 0;
                justify-content: flex-end;
                .button1{
                    display: inline-block;
                    padding: .7rem 2rem;
                    text-align: center;
                    font-size: 1rem;
                    line-height: 1.6;
                    text-decoration: none;
                    vertical-align: middle;
                    border-radius: 2rem;
                    transition: all .2s ease-in-out;
                    color: #474747;
                    border: 1px solid #83D24B;
                    background-color: #ffffff;
                    margin: 0 3px;
                }
                .button2{
                    color: #ffffff;
                    background-color: #83D24B;
                    isplay: inline-block;
                    padding: .7rem 2rem;
                    text-align: center;
                    font-size: 1rem;
                    line-height: 1.6;
                    text-decoration: none;
                    vertical-align: middle;
                    border-radius: 2rem;
                    transition: all .2s ease-in-out;
                }
            }
        }
    }
`;

export const FootWrapper = styled.div`
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        margin-top: 20px;
        position: fixed;
        bottom: 0;
        left: 0;
        right: 0;
        button {
            min-width: 120px;
        }
`;
