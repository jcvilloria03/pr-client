import {
    DEFAULT_ACTION
} from './constants';

/**
 *
 * ShiftTemplete actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
