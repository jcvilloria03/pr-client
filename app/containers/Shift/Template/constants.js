/*
 *
 * ShiftTemplete constants
 *
 */

export const DEFAULT_ACTION = 'app/ShiftTemplete/DEFAULT_ACTION';

export const FieldErrors = [
    {
        row_number: 'Row 10',
        error_type: 'Error message goes here'
    },
    {
        row_number: 'Row 12',
        error_type: 'Error message goes here'
    },
    {
        row_number: 'Row 14',
        error_type: 'Error message goes here'
    },
    {
        row_number: 'Row 20',
        error_type: 'Error message goes here'
    }
];
