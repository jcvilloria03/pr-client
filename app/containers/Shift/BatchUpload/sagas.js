/* eslint-disable require-jsdoc, import/first */
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { Fetch } from '../../../utils/request';

import {
    UPLOAD_PERSONAL_INFO,
    BATCH_UPLOAD_STATUS,
    RESET_PERSONAL_INFO,
    SET_JOB_ID,
    PERSONAL_INFO_STATUS,
    PERSONAL_INFO_ERRORS,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    LOADING,
    BATCH_UPLOAD_ASSIGN_SHIFT
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';
import { browserHistory } from '../../../utils/BrowserHistory';

import { company } from 'utils/CompanyService';

/**
 * Uploads the personal info CSV of employees to add and starts the validation process
 */
export function* uploadPersonalInfo({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const data = new FormData();
        data.append( 'company_id', companyId );
        data.append( 'file', payload.file );
        const upload = yield call( Fetch, `/company/${companyId}/batch_assign_shifts/validate`, {
            method: 'POST',
            data
        });
        if ( upload.id ) {
            yield put({
                type: SET_JOB_ID,
                payload: upload.id
            });
            yield put({
                type: PERSONAL_INFO_STATUS,
                payload: 'validation_queued'
            });
        }

        yield call( checkValidation, { payload: { step: 'personal', company_id: companyId, job_id: upload.id }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();
        const check = yield call( Fetch, `/company/${companyId}/batch_assign_shifts/status?job_id=${payload.job_id}`, {
            method: 'GET'
        });
        if ( check.status === 'validation_failed' ) {
            yield put({
                type: PERSONAL_INFO_STATUS,
                payload: check.status
            });
            yield put({
                type: PERSONAL_INFO_ERRORS,
                payload: check.errors
            });
        } else if ( check.status === 'validated' ) {
            yield put({
                type: PERSONAL_INFO_STATUS,
                payload: check.status
            });
            yield put({
                type: PERSONAL_INFO_ERRORS,
                payload: {}
            });
        } else {
            yield put({
                type: PERSONAL_INFO_STATUS,
                payload: check.status
            });
                // wait for 2 secs and check the status again
            yield call( delay, 2000 );
            yield call( checkValidation, { payload: { step: payload.step, company_id: companyId, job_id: payload.job_id }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

export function* createBatchUploadAssignShifts({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, `/company/${companyId}/batch_assign_shifts/save`, {
            method: 'POST',
            data: { job_id: payload.job_id }
        });
        yield call( notifyUser, {
            title: 'Success',
            message: 'Successfully saved',
            show: true,
            type: 'success'
        });

        yield call( browserHistory.push, '/time/shifts/calendar', true );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * resets the personal info store
 */
export function* resetPersonalInfo() {
    yield put({
        type: RESET_PERSONAL_INFO
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Individual exports for testing
 */
export function* watchForPersonalInfoUpload() {
    const watcher = yield takeLatest( UPLOAD_PERSONAL_INFO, uploadPersonalInfo );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForEmployeeUploadStatus() {
    const watcher = yield takeLatest( BATCH_UPLOAD_STATUS, checkValidation );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForCreateBatchUploadAssignShifts() {
    const watcher = yield takeLatest( BATCH_UPLOAD_ASSIGN_SHIFT, createBatchUploadAssignShifts );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForPersonalInfoUpload,
    watchForEmployeeUploadStatus,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForCreateBatchUploadAssignShifts
];
