/* eslint-disable no-lone-blocks */
/* eslint-disable react/prop-types, import/first, react/no-unused-prop-types */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import Icon from 'components/Icon';
import SnackBar from '../../../components/SnackBar';
import A from '../../../components/A';
import Toggle from '../../../components/Toggle';
import Table from '../../../components/Table';
import Clipboard from '../../../components/Clipboard';
import { H1, H4, H6 } from '../../../components/Typography';
import FileInput from '../../../components/FileInput';
import Button from '../../../components/Button';
import Loader from 'components/Loader';
import { browserHistory } from '../../../utils/BrowserHistory';
import * as batchUploadActions from './actions';
import {
    PageWrapper,
    StyledLoader,
    NavWrapper,
    FootWrapper
} from './styles';
import {
    makeSelectLoading,
    makeSelectEmployees,
    makeSelectNotification,
    makeSelectProductsState
} from './selectors';
import { company } from '../../../utils/CompanyService';

/**
 * EmployeeBatchUpload Container
 */
export class EmployeeBatchUpload extends React.Component {
    static propTypes = {
        uploadPersonalInfo: React.PropTypes.func,
        employees: React.PropTypes.shape({
            data: React.PropTypes.arrayOf(
                React.PropTypes.object
            ),
            job_id: React.PropTypes.string,
            personal_info: React.PropTypes.shape({
                status: React.PropTypes.string,
                errors: React.PropTypes.object
            }).isRequired
        }).isRequired,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool,
        addEmployeeAssignShifts: React.PropTypes.func
    }

    /**
     * component's constructor
     */
    constructor( props ) {
        super( props );
        const companyId = company.getLastActiveCompanyId();

        this.state = {

            form: {
                company_id: companyId,
                job_id: '',
                personal_info: null,
                time_attendance_info: null,
                payroll_info: null
            },
            changeButtonText: false,
            personal_info_status: null,
            personal_info_errors: {},
            personal_info_clipboard: '',
            personal_info_submit: false
        };
        this.getPersonalInfoSection = this.getPersonalInfoSection.bind( this );
    }

    /**
     * this displays all existing locations onload
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.employees.personal_info.status !== this.props.employees.personal_info.status && this.setState({
            personal_info_status: nextProps.employees.personal_info.status
        }, () => {
            this.personalInfoNext.setState({ disabled: nextProps.employees.personal_info.status !== 'validated' });
            this.validatePersonalInfo.setState({ disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.employees.personal_info.status ) });
        });
        nextProps.employees.personal_info.errors !== this.props.employees.personal_info.errors && this.setState({
            personal_info_errors: nextProps.employees.personal_info.errors
        }, () => {
            let clipboard = '';
            const rows = Object.keys( nextProps.employees.personal_info.errors );
            if ( rows.length ) {
                rows.forEach( ( row ) => {
                    nextProps.employees.personal_info.errors[ row ].forEach( ( error ) => {
                        clipboard += `Row #${Number( row ) + 1}, ${error}\n`;
                    });
                });
            }

            this.setState({ personal_info_clipboard: clipboard });
        });

        nextProps.employees.job_id !== this.props.employees.job_id && this.setState({
            form: Object.assign( this.state.form, { job_id: nextProps.employees.job_id })
        });
    }

    /**
     * this renders the personal infomation upload section of the add employees
     */
    getPersonalInfoSection() {
        let errorDisplay = [];

        const errorList = this.state.personal_info_errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'INDEX',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'ERROR',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            const keys = Object.keys( errorList );

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row #{ Number( key ) + 1 }</H4>,
                    error: <ul>{errorList[ key ].map( ( error, index ) => <li key={ index } style={ { margin: '0' } } >{error}</li> )}</ul>
                });
            });

            errorDisplay = (
                <div className="errors">
                    <H4>ERROR OCCURED:</H4>
                    <div style={ { textAlign: 'right' } }><Clipboard value={ this.state.personal_info_clipboard } /></div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                    />
                </div>
            );
        }
        return (
            <div
                className="personal"
            >
                <div className="row steps">
                    <div className="step col-sm-6">
                        <div className="template">
                            <H6>Step 1:</H6>
                            <p>A batch upload template is available for you to download and fill out.</p>
                            <p><A
                                style={ { color: '#0275d8' } }
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/time/shifts/assign/batch/guide', true );
                                } }
                            > You may click here to view the upload guide.</A></p>
                            <A href="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assign-shifts-template.csv" download>Download Template</A>
                        </div>
                    </div>
                    <div className="step col-sm-6">
                        <div className="upload">
                            <H6>Step 2:</H6>
                            <p>After completely filling out the template, choose and upload it here.</p>
                            <div style={ { display: this.state.personal_info_status === 'validating' || this.state.personal_info_status === 'validation_queued' ? 'none' : 'block' } } >
                                <FileInput
                                    accept=".csv"
                                    onDrop={ ( files ) => {
                                        const { acceptedFiles } = files;
                                        this.personalInfoNext.setState({ disabled: true });
                                        this.setState({
                                            form: Object.assign( this.state.form, {
                                                personal_info: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null
                                            }),
                                            personal_info_errors: {},
                                            personal_info_status: null
                                        }, () => {
                                            this.validatePersonalInfo.setState({ disabled: acceptedFiles.length <= 0 });
                                        });
                                    } }
                                    ref={ ( ref ) => { this.personalInput = ref; } }
                                />
                            </div>
                            <div style={ { display: this.state.personal_info_status === 'validated' ? 'block' : 'none' } }>
                                <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                            </div>
                            <div style={ { display: this.state.personal_info_status === 'validating' || this.state.personal_info_status === 'validation_queued' || this.state.personal_info_status === 'validated' ? 'none' : 'block' } } >
                                <Button
                                    label={
                                        this.state.personal_info_submit ? (
                                            <StyledLoader className="animation">
                                                UPLOADING <div className="anim3"></div>
                                            </StyledLoader>
                                        ) : (
                                            'UPLOAD'
                                        )
                                    }
                                    size="large"
                                    disabled={ this.state.form.personal_info ? this.state.form.personal_info.length <= 0 : true }
                                    type="action"
                                    ref={ ( ref ) => { this.validatePersonalInfo = ref; } }
                                    onClick={ () => {
                                        this.setState({
                                            personal_info_errors: {},
                                            personal_info_status: null,
                                            personal_info_submit: true
                                        }, () => {
                                            this.validatePersonalInfo.setState({ disabled: true }, () => {
                                                this.props.uploadPersonalInfo( this.state.form.personal_info, this.state.form.company_id );
                                                setTimeout( () => {
                                                    this.setState({ personal_info_submit: false });
                                                }, 5000 );
                                            });
                                        });
                                    } }
                                />
                            </div>
                            <div style={ { display: this.state.personal_info_status === 'validating' || this.state.personal_info_status === 'validation_queued' ? 'block' : 'none' } } >
                                <StyledLoader className="animation">
                                    <H4 style={ { margin: '0' } }>{ this.state.personal_info_status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                </StyledLoader>
                            </div>
                        </div>
                    </div>
                </div>
                { errorDisplay }
            </div>
        );
    }

    /**
     * gets the batch upload icon
     */
    batchUploadIcon() {
        return (
            <Icon name="batchUpload" />
        );
    }

    /**
     * gets the manual entry icon
     */
    manualEntryIcon() {
        return (
            <Icon name="manualEntry" />
        );
    }

    batchAssignShiftData() {
        const data = {
            company_id: company.getLastActiveCompanyId(),
            job_id: this.props.employees.job_id
        };
        this.props.addEmployeeAssignShifts( data );
    }
    /**
     * renders component to DOM
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Batch Upload"
                    meta={ [
                        { name: 'description', content: 'Shifts Batch Upload' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/shifts/calendar', true );
                            } }
                        >
                            &#8592; Back to Shifts
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container>
                        <div className="heading">
                            <H1>Assign Shift</H1>
                        </div>
                        <div className="toggle">
                            <Toggle
                                options={ [
                                    { title: 'Batch Upload', subtext: 'Upload multiple entries at once using template.', value: 'batch', icon: this.batchUploadIcon() },
                                    { title: 'Manual Entry', subtext: 'Create entries one by one.', value: 'single', icon: this.manualEntryIcon() }
                                ] }
                                defaultSelected="batch"
                                onChange={ ( value ) => { if ( value === 'single' ) { browserHistory.push( '/time/shifts/assign/manual', true ); } } }
                                ref={ ( ref ) => { this.toggle = ref; } }
                            />
                        </div>
                        <div className="employees-form">
                            <div>
                                { this.getPersonalInfoSection() }
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <FootWrapper>
                    <div className="action_button">
                        <Button
                            label={ <span>{ !this.props.loading ? 'Submit' : <Loader /> }</span> }
                            size="large"
                            type="action"
                            ref={ ( ref ) => { this.personalInfoNext = ref; } }
                            onClick={ () => this.batchAssignShiftData() }
                        />
                    </div>
                </FootWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    employees: makeSelectEmployees(),
    products: makeSelectProductsState(),
    loading: makeSelectLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        batchUploadActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EmployeeBatchUpload );
