import { fromJS } from 'immutable';

import {
    PERSONAL_INFO_STATUS,
    PERSONAL_INFO_ERRORS,
    SAVING_STATUS,
    NOTIFICATION_SAGA,
    LOADING,
    SET_JOB_ID,
    BATCH_UPLOAD_ASSIGN_SHIFT
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    createBatchUploadAssignShifts: '',
    employees: {
        data: [],
        job_id: null,
        personal_info: {
            status: '',
            errors: {}
        },
        saving: {
            status: '',
            errors: {}
        }
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Batch Upload reducer
 *
 */
function batchUploadReducer( state = initialState, action ) {
    switch ( action.type ) {
        case PERSONAL_INFO_STATUS:
            return state.setIn([ 'employees', 'personal_info', 'status' ], action.payload );
        case PERSONAL_INFO_ERRORS:
            return state.setIn([ 'employees', 'personal_info', 'errors' ], action.payload );
        case SAVING_STATUS:
            return state.setIn([ 'employees', 'saving', 'status' ], action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_JOB_ID:
            return state.setIn([ 'employees', 'job_id' ], action.payload );
        case BATCH_UPLOAD_ASSIGN_SHIFT:
            return state.set( 'createBatchUploadAssignShifts', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default batchUploadReducer;

