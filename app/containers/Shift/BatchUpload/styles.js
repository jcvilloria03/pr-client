import styled, { keyframes } from 'styled-components';

const anim3 = keyframes`
    to {
        transform: rotate(45deg) translate(3px, 3px);
    }
`;

export const PageWrapper = styled.div`
    min-height: 100vh;
    background: #fff;

    .nav {
        padding: 10px 20px;
        background: #f0f4f6;
        margin-bottom: 50px;
    }

    .step .template {
        justify-content: space-between;

        div > div:last-of-type {
            margin: 0 auto;
        }

        p {
            margin-bottom: 1rem;
        }
    }

    .personal, .payroll {
        margin: 1rem 0 0 0;

        & > button {
            margin: 2rem auto 0;
        }
    }

    .template > div > * {
        margin: 6px 0;
    }

    .loader {
        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .heading {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 150px auto 30px auto;

        h1 {
            font-size: 1.5rem;
            font-weight: 700;
        }

        p {
            text-align: center;
            max-width: 800px;
        }
    }

    .selected .fill {
        fill: #fff;
    }

    .selected .stroke {
        stroke: #fff;
    }

    .fill {
        fill: rgb(0,165,226);
    }

    .stroke {
        stroke: rgb(0,165,226);
    }

    .employees-form {

        > div {

            button .animation {
                color: #FFF;

                .anim3:before, .anim3:after {
                    border-color: #fff;
                }
            }
            .personal, .payroll {
                margin: 3rem 0;

                & > h4 {
                    text-align: left;
                }

                & > button {
                    padding: 12px 40px;
                    margin-top: 2rem;
                }

                .steps {
                    display: flex;
                    align-items: stretch;
                    flex-wrap: wrap;
                }

                .step {
                    min-width: 400px;
                    margin: 10px auto;
                }

                .template, .upload {
                    text-align: center;
                    padding: 2rem;
                    border: 2px dashed #ccc;
                    border-radius: 12px;
                    width: 100%;
                    height: 100%;

                    & > a {
                        background-color: #FFF;
                        color: #333;
                        border: 1px solid #4ABA4A;
                        padding: 8px 12px;
                        border-radius: 30px;
                        display: inline;
                        vertical-align: middle;
                        margin: 0 auto;
                        max-width: 200px;

                        &:hover, &:focus, &:active:focus {
                            outline: none;
                            background-color: #fff;
                            color: #4ABA4A;
                        }
                    }
                }

                .upload > div:first-of-type > div > div {
                    margin: 0 auto;
                }

                .errors {
                    .rt-thead .rt-tr {
                        background: #F21108;
                        color: #fff;
                    }
                    .rt-tbody .rt-tr {
                        background: rgba(242, 17, 8, .1)
                    }
                    .ReactTable.-striped .rt-tr.-odd {
                        background: rgba(242, 17, 8, .2)
                    }

                    .ReactTable .rt-th,
                    .ReactTable .rt-td {
                        flex: 1 0 0px;
                        white-space: initial;
                        text-overflow: ellipsis;
                        padding: 7px 20px;
                        overflow: hidden;
                        transition: 0.3s ease;
                        transition-property: width, min-width, padding, opacity;
                    }

                    .react-bs-container-body tr {
                        background: rgba(249, 210, 210, 0.8);

                        &:hover {
                            background: rgba(249, 210, 210, 1);
                        }
                    }
                }
            }

            .preview {
                .react-bs-table-container {
                    overflow: auto;
                }

                .previewActions {
                    button {
                        min-width: 140px;
                        padding: 16px 10px;

                        &:first-of-type {
                            margin-right: 20px;
                        }
                    }
                }
            }

            .action_button {
                margin: 1rem 0;
                text-align: center;

                button {
                    min-width: 200px;
                }
            }
        }
    }
`;

export const StyledLoader = styled.div`
    font-size: 16px;
    font-weight: 600;
    color: #00a5e5;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;

    .anim3 {
        padding-left: 10px;
        &:before {
            display: block;
            width: 12px;
            height: 12px;
            margin-top: -1px;
            border: 2px solid #00a5e5;
            content: "";
            animation: ${anim3} .5s ease infinite alternate;
            border-radius: 100% 100% 0 100%;
            transform: rotate( 45deg );
        }

        &:after {
            display: block;
            width: 12px;
            height: 3px;
            margin-top: 8px;
            background-color: rgba(0, 0, 0, 0.2);
            content: "";
            border-radius: 100%;
        }
    }
`;

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;
`;
export const FootWrapper = styled.div`
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        margin-top: 20px;
        position: fixed;
        bottom: 0;
        left: 0;
        right: 0;
        button {
            min-width: 120px;
        }
`;
