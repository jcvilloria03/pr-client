import { createSelector } from 'reselect';

/**
 * Direct selector to the setup state domain
 */
const selectSetupDomain = () => ( state ) => state.get( 'assignShifts' );

const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */
const makeSelectEmployees = () => createSelector(
    selectSetupDomain(),
    ( substate ) => substate.get( 'employees' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectSetupDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectSetupDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectBatchUploadAssignShift = () => createSelector(
    selectSetupDomain(),
    ( substate ) => substate.get( 'createBatchUploadAssignShifts' ).toJS()
);

const makeSelectProductsState = () => createSelector(
    selectAppDomain(),
    ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

export {
    selectSetupDomain,
    makeSelectEmployees,
    makeSelectNotification,
    makeSelectProductsState,
    makeSelectLoading,
    makeSelectBatchUploadAssignShift
};

