import { createSelector } from 'reselect';

/**
 * Direct selector to the editRestday state domain
 */
const selectEditRestdayDomain = () => ( state ) => state.get( 'editRestday' );

/**
 * Other specific selectors
 */

const makeSelectLoading = () => createSelector(
  selectEditRestdayDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectRestDetails = () => createSelector(
  selectEditRestdayDomain(),
( substate ) => substate && substate.get( 'restDay' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectEditRestdayDomain(),
  ( substate ) => substate && substate.get( 'notification' ).toJS()
);

const makeSelectErrors = () => createSelector(
  selectEditRestdayDomain(),
  ( substate ) => {
      let error;
      try {
          error = substate.get( 'errors' ).toJS();
      } catch ( err ) {
          error = substate.get( 'errors' );
      }

      return error;
  }
);
export {
  makeSelectLoading,
  makeSelectRestDetails,
  makeSelectNotification,
  makeSelectErrors
};
