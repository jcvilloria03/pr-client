/* eslint-disable require-jsdoc */
import {
    DEFAULT_ACTION,
    GET_RESTDAY,
    GET_UPDATE
} from './constants';

/**
 *
 * EditRestday actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function getRestDetails( payload ) {
    return {
        type: GET_RESTDAY,
        payload
    };
}

export function updateForm( payload ) {
    return {
        type: GET_UPDATE,
        payload
    };
}
