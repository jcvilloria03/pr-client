import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_LOADING,
    SET_RESTDAY,
    SET_ERRORS,
    SET_NOTIFICATION
} from './constants';

const initialState = fromJS({
    loading: true,
    restDay: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * EditRestday reducer
 *
 */
function editRestdayReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_RESTDAY:
            return state.set( 'restDay', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default editRestdayReducer;
