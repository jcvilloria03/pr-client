/* eslint-disable require-jsdoc */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import get from 'lodash/get';

import {
    SET_LOADING,
    GET_RESTDAY,
    SET_RESTDAY,
    SET_UPDATE,
    GET_UPDATE,
    SET_ERRORS,
    SET_NOTIFICATION,
    GET_NOTIFICATION
} from './constants';
import { Fetch } from '../../../utils/request';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* getRestDetails({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const { id } = payload;
        const response = yield call( Fetch, `/rest_day/${id}`, { method: 'GET' });
        yield put({
            type: SET_RESTDAY,
            payload: response
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* updateForm({ payload }) {
    try {
        yield [
            put({ type: SET_UPDATE, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];
        yield call( Fetch, `/rest_day/${payload.id}`, { method: 'PUT', data: payload });
        yield put({
            type: SET_UPDATE,
            payload: false
        });
        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Record Updated Successfully',
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: SET_UPDATE,
            payload: false
        });
    }
}

export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

export function* watchForGetScheduler() {
    const watcher = yield takeEvery( GET_RESTDAY, getRestDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForUpdateForm() {
    const watcher = yield takeEvery( GET_UPDATE, updateForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchNotify() {
    const watcher = yield takeEvery( GET_NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetScheduler,
    watchForUpdateForm
];
