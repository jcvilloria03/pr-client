import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import _ from 'lodash';
import { bindActionCreators } from 'redux';

import { formatDate } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';
import {
    DATE_FORMATS,
    REPEAT_WEEKLY_VALUE,
    REPEAT_BY_MONTH,
    NEVER_OPTION,
    REPEAT_DAYS,
    AFTER_OPTION,
    ON_OPTION,
    END_AFTER_DEFAULT_VALUE,
    REPEAT_EVERY_LABELS
} from 'utils/constants';

import A from 'components/A';
import { H2, H3 } from 'components/Typography';
import Button from 'components/Button';
import Input from 'components/Input';
import DatePicker from 'components/DatePicker';
import SalSelect from 'components/Select';
import CustomCheckbox from 'components/CustomCheckbox';
import RadioGroup from 'components/RadioGroup';
import Radio from 'components/Radio';
import SnackBar from 'components/SnackBar';
import SalConfirm from 'components/SalConfirm';

import * as viewScheduleActions from './actions';
import {
    makeSelectLoading,
    makeSelectRestDetails,
    makeSelectNotification
} from './selectors';

import {
    LoadingStyles,
    NavWrapper,
    PageWrapper,
    ConfirmBodyWrapperStyle
} from './styles';

import { company } from '../../../utils/CompanyService';
/**
 *
 * EditRestday
 *
 */
export class EditRestday extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        loading: React.PropTypes.bool,
        params: React.PropTypes.object,
        getRestDetails: React.PropTypes.func,
        restDetails: React.PropTypes.object,
        updateForm: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            isNameAvailable: true,
            form: this.getDefaultFormValues(),
            isLoading: false,
            tags: [],
            options: [],
            repeatEveryOptions: _.range( 30 ).map( ( e, i ) => ({ label: i + 1, value: i + 1 }) ),
            neverOption: NEVER_OPTION,
            afterOption: AFTER_OPTION,
            onOption: ON_OPTION,
            repeatEveryLabels: REPEAT_EVERY_LABELS,
            formValue: this.formValue()
        };
    }

    componentWillMount() {
        const value = {
            id: this.props.params.id
        };
        this.props.getRestDetails( value );
    }

    getDefaultFormValues() {
        return {
            startDate: this.props.restDetails.start_date,
            schedule_repeat: true,
            repeat: this.getDefaultRepeatValues()
        };
    }

    getDefaultRepeatValues() {
        return {
            typeObject: REPEAT_WEEKLY_VALUE,
            type: REPEAT_WEEKLY_VALUE.value,
            repeat_every: 10,
            endsOption: NEVER_OPTION,
            end_on: null,
            end_after: null,
            end_never: true,
            repeat_on: REPEAT_DAYS,
            repeat_by: REPEAT_BY_MONTH
        };
    }

    // eslint-disable-next-line consistent-return
    formValue() {
        if ( !Object.keys( this.props.restDetails ).length > 0 ) {
            return {
                company_id: company.getLastActiveCompanyId(),
                id: this.props.restDetails && this.props.restDetails.id,
                rest_day_id: this.props.restDetails && this.props.restDetails.id,
                start_date: this.props.restDetails && this.props.restDetails.start_date,
                end_date: this.props.restDetails && this.props.restDetails.end_date,
                employee_id: this.props.restDetails && this.props.restDetails.employee_id,
                employee: this.props.restDetails && this.props.restDetails.employee,
                rest_day_repeat: true,
                repeat: this.getDefaultRepeatValues()
            };
        }
    }

    endRepeatOptions( value ) {
        if ( value === this.state.neverOption ) {
            this.state.form.repeat.end_after = null;
            this.state.form.repeat.end_on = null;
            this.state.form.repeat.end_never = true;
        } else if ( value === this.state.afterOption ) {
            this.state.form.repeat.end_never = false;
            this.state.form.repeat.end_on = null;
            this.state.form.repeat.end_after = END_AFTER_DEFAULT_VALUE;
        } else {
            this.state.form.repeat.end_never = false;
            this.state.form.repeat.end_after = null;
        }
    }

    saveEditing() {
        this.props.updateForm( this.formValue() );
    }

    /**
     *
     * EditRestday render method
     *
     */
    render() {
        const {
            form,
            repeatEveryOptions,
            neverOption,
            afterOption,
            repeatEveryLabels
        } = this.state;
        return (
            <div>
                <Helmet
                    title="Edit Restday"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Restday' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification && this.props.notification.message }
                    title={ this.props.notification && this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification && this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification && this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ () => browserHistory.push( '/time/shifts/calendar', true ) }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                            Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Discard Changes"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Discard"
                    cancelText="Stay on this page"
                    visible={ this.state.showModal }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/shifts/calendar', true );
                            } }
                        >
                            Back to Shifts
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    {this.props.loading ? (
                        <div className="loader">
                            <LoadingStyles>
                                <H2>Please wait...</H2>
                                <br />
                                <H3>Employee Data is Loading...</H3>
                            </LoadingStyles>
                        </div>
                    ) : (
                        <div>
                            <Container>
                                <div className="content">
                                    <div className="heading">
                                        <H2>Edit Employee Rest Days</H2>
                                    </div>
                                </div>
                                <div className="employee_rest">
                                    <div>Schedule name</div>
                                    <Input
                                        name="name"
                                        className="employee_rest_input"
                                        id="membership_program"
                                        value={ this.props.restDetails.employee !== null ? this.props.restDetails.employee.full_name : '' }
                                        ref={ ( ref ) => { this.name = ref; } }
                                        disabled
                                    />
                                </div>
                                <div className="date_input">
                                    <p className="schdule_tooltip">* Shift effective end date
                                    <div className="bg_tooltip">
                                        <div>?</div>
                                        <span className="tooltiptext">When will the wmployee start having the selected rest day/s?</span>
                                    </div>
                                    </p>
                                    <DatePicker
                                        name="start_date"
                                        dayFormat={ DATE_FORMATS.MMDDYYYY }
                                        selectedDay={ formatDate( form.startDate, DATE_FORMATS.API ) }
                                        value={ form.startDate }
                                        ref={ ( ref ) => { this.start_date = ref; } }
                                    />
                                </div>
                                { form.schedule_repeat === false ? (
                                    <div className="date_input">
                                        <p className="schdule_tooltip">* Shift effective end date
                                            <div className="bg_tooltip">
                                                <div>?</div>
                                                <span className="tooltiptext">When will the wmployee start having the selected rest day/s?</span>
                                            </div>
                                        </p>
                                        <DatePicker
                                            name="end_date"
                                            dayFormat={ DATE_FORMATS.MMDDYYYY }
                                            ref={ ( ref ) => { this.end_date = ref; } }
                                        />
                                    </div>
                                ) : ''}
                                <div className="add_switch add_checkswitch">
                                    <div className="switch">
                                        <input
                                            type="checkbox"
                                            id="repeat_checkbox"
                                            name="restday_repeat"
                                            checked={ form.schedule_repeat }
                                            ref={ ( ref ) => { this.restday_repeat = ref; } }
                                            onChange={ () => {
                                                this.setState({ form: { ...form, schedule_repeat: !form.schedule_repeat }});
                                            } }
                                        />
                                        <span className="slider round"></span>
                                    </div>
                                    <p className="switch_label schdule_tooltip">Rest Day repeat
                                        <div className="bg_tooltip">
                                            <div>?</div>
                                            <span className="tooltiptext">Determine how often this schedule will be used.</span>
                                        </div>
                                    </p>
                                </div>
                                { form.schedule_repeat === true ? (
                                    <div>
                                        <div>
                                            <div>Choose rest days</div>
                                            <div className="Input_from">
                                                {form.repeat.repeat_on.map( ( val ) => (
                                                    <div className="input_check">
                                                        <CustomCheckbox
                                                            label={ val.label }
                                                            checkValue={ val.selected }
                                                            onChange={ () => {
                                                                const selectedDays = form.repeat.repeat_on.map( ( data ) => ( data.value === val.value ? ({ ...data, selected: !data.selected }) : data ) );
                                                                this.setState({
                                                                    form: {
                                                                        ...form,
                                                                        repeat: {
                                                                            ...form.repeat,
                                                                            repeat_on: selectedDays
                                                                        }
                                                                    }
                                                                });
                                                            } }
                                                        />
                                                    </div>
                                                ) )
                                                }
                                            </div>
                                        </div>
                                        <div className="repeat_titls">
                                            <div>Repeat every</div>
                                            <div className="form-group input_repeat">
                                                <SalSelect
                                                    id="allowance_type"
                                                    data={ repeatEveryOptions }
                                                    name="repeat_every"
                                                    value={ this.props.restDetails.repeat ? this.props.restDetails.repeat.repeat_every : 1 }
                                                    onChange={ ( value ) => {
                                                        this.setState({ form: { ...form, repeat: { ...form.repeat, repeat_every: value.value }}});
                                                    } }
                                                />
                                                {repeatEveryLabels[ form.repeat.typeObject.label ]}
                                            </div>
                                        </div>
                                        <div className="repeat_ends">
                                            <div>Ends</div>
                                        </div>
                                        <div className="repeat_intervals">
                                            <RadioGroup
                                                id="radioAmendmentReturn"
                                                name="repeat-ends"
                                                value={ form.repeat.endsOption }
                                                vertical
                                                className="radio-group"
                                                onChange={ ( value ) => {
                                                    this.setState({
                                                        form:
                                                        {
                                                            ...form,
                                                            repeat: {
                                                                ...form.repeat,
                                                                endsOption: this.endRepeatOptions( value ) || value
                                                            }
                                                        }
                                                    });
                                                } }
                                            >
                                                <Radio value={ neverOption }>Never</Radio>
                                                <Radio value={ afterOption } className="after after_radio">After</Radio>
                                                <div className="repeat_input">
                                                    {form.repeat.endsOption === 'never' ? (
                                                        <div className="schedule_timeSect">
                                                            <Input
                                                                id="membership_program1"
                                                                className="editing"
                                                                value={ form.repeat.end_after || '' }
                                                                disabled
                                                            />
                                                            <p>occurrences</p>
                                                        </div>
                                                    ) : (
                                                        <div className="schedule_timeSect">
                                                            <Input
                                                                id="membership_program1"
                                                                className="editing"
                                                                value={ form.repeat.end_after || '' }
                                                            />
                                                            <p>occurrences</p>
                                                        </div>
                                                    )}
                                                </div>
                                            </RadioGroup>
                                        </div>
                                    </div>
                            ) : '' }
                            </Container>
                            <div className="from_footer">
                                <Container>
                                    <div className="footer_button">
                                        <Button
                                            label={ <span>cancel</span> }
                                            type="grey"
                                            onClick={ () => {
                                                this.setState({ showModal: false }, () => {
                                                    this.setState({ showModal: true });
                                                });
                                            } }
                                        />
                                        <Button
                                            label="Update"
                                            type="action"
                                            size="large"
                                            onClick={ () => this.saveEditing() }
                                        />
                                    </div>
                                </Container>
                            </div>
                        </div>
                    )}
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    restDetails: makeSelectRestDetails(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        viewScheduleActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EditRestday );
