/*
 *
 * EditRestday constants
 *
 */

export const DEFAULT_ACTION = 'app/EditRestday/DEFAULT_ACTION';
export const SET_LOADING = 'app/Restday/SET_LOADING';
export const SET_RESTDAY = 'app/Restday/SET_RESTDAY';
export const GET_RESTDAY = 'app/Restday/GET_RESTDAY';
export const SET_UPDATE = 'app/Restday/SET_UPDATE';
export const GET_UPDATE = 'app/Restday/GET_UPDATE';
export const SET_NOTIFICATION = 'app/Restday/SET_NOTIFICATION';
export const GET_NOTIFICATION = 'app/Restday/GET_NOTIFICATION';
export const SET_ERRORS = 'app/Restday/SET_ERRORS';
