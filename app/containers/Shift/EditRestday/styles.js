import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 200px;
    margin-top: 100px;
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    justify-content: center;
    min-height: 100%;
`;

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;
`;

export const PageWrapper = styled.div`
    .content {
        margin-top: 150px;
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 90px auto;

            h2 {
                font-weight: 600;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }
    }   
    .employee_rest{
        .employee_rest_input{
            width:483px;
        }
        label{
            font-size:14px;
        }
    }
    .repeat_input{
        .schedule_timeSect{
            display:flex;
            align-items-center;
            p{
                margin-bottom:0;
                margin-left:15px;
            }
        }
    }
    .add_switch{
        .switch{
            .slider{
                &:before{
                    transform: translateX(29px);
                    }                        
                }
            }
        }
        .switch_label{
            margin-top:unset !important;
        }
    }
    .repeat_intervals .radio-group > div:nth-child(2){
            margin-top:9px !important;
            margin-bottom:0px !important;
    }
    .repeat_titls .input_repeat .Select .Select-control{
        width: 72px !important;
    }
    .date_input{
        .DayPickerInput{
            input{
                width:483px;
            }
        }
        span{
            margin:0;
        }
        p{
            display:none;
        }
        .schdule_tooltip{
            display:block !important;
        }
    }
    .schdule_tooltip{
        margin-top: 16px;
        display: flex;
        align-items: center;
        font-size: 14px;
        margin-bottom:5px;
        button{
            width: 20px;
            height: 20px;
            padding: 0;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
        }
        a{
            padding: 0;
            border-bottom: none;
            padding-left: 6px;
        }
        .bg_tooltip {
            position: relative;
            display: inline-block;
            width:20px;
            height:20px;
            border-radius: 50%;
            background-color: #F0F4F6;
            color: #ADADAD;
            margin-left: 6px;
            text-align: center;
            label{
                font-size: 15px;
                font-weight: bold;
            }
        }

        .bg_tooltip .tooltiptext {
            visibility: hidden;
            width: 242px;
            background-color: #fff;
            color: #03a9f4;
            border: 2px solid #03a9f4;
            text-align: left;
            border-radius: 6px;
            padding: 5px 16px;
            position: absolute;
            left: 26px;
            top: -50%;
            transform: translateY(-20%);
            z-index: 1;
        }

        .bg_tooltip .tooltiptextStart{
            transform: translateY(-26%);
        }

        .bg_tooltip .tooltiptextEnd{
            transform: translateY(-20%);
            width: 262px;
        }
        .bg_tooltip .tooltiptextHours{
            transform: translateY(-17%);
            width: 255px;
        }
        .bg_tooltip:hover .tooltiptext {
            visibility: visible;
        }
        .bg_tooltip .tooltiptext::after {
            content: " ";
            position: absolute;
            top: 50%;
            right: 99%;
            margin-top: -5px;
            border: solid #03a9f4;
            border-width: 0 2px 2px 0;
            display: inline-block;
            padding: 4px;
            transform: rotate(135deg);
            background-color: #ffff;
        }
    }
    .add_switch{
        margin-top: 16px;
        margin-bottom: 14px;
    }
    .add_checkswitch{
        display: flex;
        align-items: center;
    }
    .add_checkswitch label{
        margin-bottom: 0px;
        padding-right: 10px;
    }
    .add_checkswitch .switch_label{
        margin-left: 15px;
            font-size: 14px;
    }
    .add_switch .switch {
        position: relative;
        display: inline-block;
        width: 35px;
        height: 8px;
    }
    .switch input { 
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }
    .slider:before {
        position: absolute;
        content: "";
        height: 17px;
        width: 17px;
        left: 0px;
        top: -5px;
        background-color: #474747;
        -webkit-transition: .4s;
        transition: .4s;
    }
    input:checked + .slider {
        background: #cccccc;
    }
    input:checked + .slider:before{
        background: #83D24B;
        left: -10px;
        top: -5px;
    }
    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }
    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .Input_from{
        display: flex;
        margin-bottom: 14px;
    }
    .input_check {
        display: flex;
        align-items: center;
        margin-right: 15px;
    }
    .input_check input{
        margin-right: 10px;
        width: 20px;
        height: 17px;
    }
    .input_check label{
        margin-bottom: 0px;
    }

    .repeat_ends label{
        margin:15px 0px 5px;
    }
    .repeat_intervals{
        margin-top: 15px;
    }
    .repeat_intervals #After{
        float:left;
    }
    .repeat_intervals .repeat_input{
        margin-bottom: 10px;
        margin-left: 75px;
    }

    .radio-group{
        >div{
            font-size: 14px;
            padding: 0 !important;
            >div{
                display: flex;
                align-items: center;
            }
        }
    }
    .repeat_input{
        margin-left:20px;
    }

    .repeat_titls label{
        margin-bottom: 0.25rem;
    }

    .form-group{
        margin-bottom:0;
        select{
            border: 1px solid #95989a;
            height: 46px;
            width: 100%;
            &:focus-visible{
            outline:none;
            }
        }
    }

    .input_repeat {
        display: flex;
        align-items: center;
    }
    .input_repeat .Select .Select-control {
        width: 10%;
    }

    .from_footer{
        margin: 14px 0px 0px;
        padding: 14px 0px;
        background: #F0F4F6;
    }
    .from_footer .footer_button{
        width: 100%;
        text-align: end;
    }
    .from_footer .footer_button button{
        padding: 14px 28px;
        border-radius: 2rem;
        &:focus{
            outline:none;
        }
    }
    .from_footer .footer_button .btn_cancel{
        color: #474747;
        border: 1px solid #83D24B;
        background-color: #ffffff;
        margin-right: 5px;
    }
`;
