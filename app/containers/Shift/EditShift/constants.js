/*
 *
 * EditShift constants
 *
 */

export const DEFAULT_ACTION = 'app/Shift/EditShift/DEFAULT_ACTION';
export const SET_LOADING = 'app/Shift/EditShift/SET_LOADING';
export const SET_SHIFT = 'app/Shift/EditShift/SET_SHIFT';
export const GET_SHIFT = 'app/Shift/EditShift/GET_SHIFT';
export const SET_UPDATE = 'app/Shift/EditShift/SET_UPDATE';
export const GET_UPDATE = 'app/Shift/EditShift/GET_UPDATE';
export const SET_NOTIFICATION = 'app/Shift/EditShift/SET_NOTIFICATION';
export const GET_NOTIFICATION = 'app/Shift/EditShift/GET_NOTIFICATION';
