import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'utils/BrowserHistory';
import moment from 'moment';
import { formatDate } from 'utils/functions';
import {
    DATE_FORMATS
} from 'utils/constants';

import A from 'components/A';
import { H3 } from 'components/Typography';
import Button from 'components/Button';
import DatePicker from 'components/DatePicker';
import Input from 'components/Input';
import SalConfirm from 'components/SalConfirm';
import SnackBar from 'components/SnackBar';
import SalSelect from '../../../components/Select';

import * as editShiftActions from './actions';
import {
    makeSelectShiftDetails,
    makeSelectLoading,
    makeSelectNotification
} from './selectors';
import {
    NavWrapper,
    PageWrapper,
    FootWrapper,
    ConfirmBodyWrapperStyle
} from './styles';

/**
 *
 * EditShift
 *
 */
export class EditShift extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        params: React.PropTypes.object,
        getRestDetails: React.PropTypes.func,
        shiftDetails: React.PropTypes.object,
        updateForm: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );

        this.state = {
            schedule: this.props.shiftDetails.schedule && this.props.shiftDetails.schedule.name,
            start: '',
            end: ''
        };
    }

    componentDidMount() {
        const value = {
            id: this.props.params.id
        };
        this.props.getRestDetails( value );
    }

    update() {
        const data = {
            employeeName: `${this.props.shiftDetails.employee.full_name} - ${this.props.shiftDetails.employee.employee_id}`,
            end: this.state.end === '' ? this.props.shiftDetails.end : moment( this.state.end ).format( 'YYYY-MM-DD' ),
            endDate: this.state.end === '' ? moment( this.props.shiftDetails.end ).format( 'MMMM D, YYYY' ) : this.state.end,
            id: this.props.shiftDetails.id,
            schedule: this.props.shiftDetails.schedule,
            start: this.state.start === '' ? this.props.shiftDetails.start : moment( this.state.start ).format( 'YYYY-MM-DD' ),
            startDate: this.state.start === '' ? moment( this.props.shiftDetails.start ).format( 'MMMM D, YYYY' ) : this.state.start
        };
        this.props.updateForm( data );
    }

    /**
     *
     * EditShift render method
     *
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Edit Shift"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Shift' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification && this.props.notification.message }
                    title={ this.props.notification && this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification && this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification && this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ () => browserHistory.push( '/time/shifts/calendar', true ) }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                            Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Discard Changes"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Discard"
                    cancelText="Stay on this page"
                    visible={ this.state.showModal }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/shifts/calendar', true );
                            } }
                        >
                            &#8592; Back to Shifts
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="heading">
                                <H3>Edit Employee Shift</H3>
                            </div>
                        </div>
                        <div className="schedule">
                            <div>
                                <div className="row">
                                    <div className="col-xs-4">
                                        <p className="schdule_tooltip">Employee name</p>
                                        <div className="datepicker-main">
                                            <Input
                                                name="name"
                                                id="membership_program"
                                                required
                                                value={ `${this.props.shiftDetails.employee && this.props.shiftDetails.employee.full_name} - ${this.props.shiftDetails.employee && this.props.shiftDetails.employee.employee_id}` }
                                                ref={ ( ref ) => { this[ '2' ] = ref; } }
                                                disabled
                                            />
                                        </div>
                                    </div>
                                    <div className="col-xs-4">
                                        <p className="schdule_tooltip">* Schedule
                                            <div className="bg_tooltip">
                                                <div>?</div>
                                                <span className="tooltiptext">Covers working hours,
                                                    break periods, and other planned activities.</span>
                                            </div>
                                        </p>
                                        <div className="datepicker-main">
                                            <SalSelect
                                                id="name"
                                                lable="name"
                                                required
                                                ref={ ( ref ) => { this.name = ref; } }
                                                placeholder={ this.props.shiftDetails && this.props.shiftDetails.schedule && this.props.shiftDetails.schedule.name }
                                                disabled
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="summary">
                                <div className="summary_header">
                                    <div className="table_mainsummry">
                                        <div className="summary_header">
                                            <div>
                                                <p className="schdule_tooltip summary_tooltip">Schedule a summary
                                                    <div className="bg_tooltip">
                                                        <div>?</div>
                                                        <span className="tooltiptext">Schdule A Summary</span>
                                                    </div>
                                                </p>
                                            </div>
                                            <div>
                                                <A
                                                    href
                                                    onClick={ ( e ) => {
                                                        e.preventDefault();
                                                        browserHistory.push( `/time/schedules/edit/${this.props.shiftDetails.schedule.id}`, true );
                                                    } }
                                                >
                                                View schedule details
                                            </A>
                                            </div>
                                        </div>
                                        <div className="all_summary">
                                            <div className="summary_data">
                                                <div>Schedule type:</div>
                                                <span style={ { 'text-transform': 'capitalize' } }>{ this.props.shiftDetails.schedule && this.props.shiftDetails.schedule.type }</span>
                                            </div>
                                            <div className="summary_data">
                                                <div>Start and end time:</div>
                                                <span>{ this.props.shiftDetails.schedule && this.props.shiftDetails.schedule.start_time || this.props.shiftDetails.schedule && this.props.shiftDetails.schedule.end_time === true ? `${this.props.shiftDetails.schedule && this.props.shiftDetails.schedule.start_time} to ${this.props.shiftDetails.schedule && this.props.shiftDetails.schedule.end_time}` : '' }</span>
                                            </div>
                                            <div className="summary_data">
                                                <div>Schedule effective start date:</div>
                                                <span>{ this.props.shiftDetails.schedule && this.props.shiftDetails.schedule.start_date ? moment( this.props.shiftDetails.schedule && this.props.shiftDetails.schedule.start_date ).format( 'MMMM D, YYYY' ) : ''}</span>
                                            </div>
                                            <div className="summary_data">
                                                <div>Schedule effective end date:</div>
                                                <span>{ this.props.shiftDetails.schedule && this.props.shiftDetails.schedule.end_date === undefined ? 'Never ends' : this.props.shiftDetails.schedule && this.props.shiftDetails.schedule.end_date}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="shift-details">
                            <p style={ { 'font-weight': 'bold' } }>Shift details</p>
                            <div className="row">
                                <div className="col-xs-4">
                                    <p className="schdule_tooltip">* Shift effective start date
                                        <div className="bg_tooltip">
                                            <div>?</div>
                                            <span className="tooltiptext">Start date refers to the date when the employee should start following the selected schedule.</span>
                                        </div>
                                    </p>
                                    <div className="datepicker-main">
                                        <DatePicker
                                            dayFormat="MMMM D, YYYY"
                                            selectedDay={ formatDate( this.props.shiftDetails.start, DATE_FORMATS.API ) }
                                            value={ this.props.shiftDetails.start }
                                            onChange={ ( val ) => this.setState({ start: val }) }
                                        />
                                    </div>
                                </div>
                                <div className="col-xs-4">
                                    <p className="schdule_tooltip">* Shift effective end date
                                        <div className="bg_tooltip">
                                            <div>?</div>
                                            <span className="tooltiptext">The employee should follow the selected schedule until the end date indicated here..</span>
                                        </div>
                                    </p>
                                    <div className="datepicker-main">
                                        <DatePicker
                                            dayFormat="MMMM D, YYYY"
                                            selectedDay={ formatDate( this.props.shiftDetails.end, DATE_FORMATS.API ) }
                                            value={ this.props.shiftDetails.end }
                                            disabledDays={ { before: new Date( this.state.start || this.props.shiftDetails.start ) } }
                                            onChange={ ( val ) => this.setState({ end: val }) }
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <FootWrapper>
                    <Button
                        className="button"
                        label={ <span>Cancel</span> }
                        type="grey"
                        size="large"
                        onClick={ () => {
                            this.setState({ showModal: false }, () => {
                                this.setState({ showModal: true });
                            });
                        } }
                    />
                    <Button
                        label={ <span>Update</span> }
                        type="action"
                        size="large"
                        onClick={ () => this.update() }
                    />
                </FootWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    shiftDetails: makeSelectShiftDetails(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        editShiftActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EditShift );
