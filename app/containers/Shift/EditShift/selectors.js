import { createSelector } from 'reselect';

/**
 * Direct selector to the editShift state domain
 */
const selectEditShiftDomain = () => ( state ) => state.get( 'editShift' );

/**
 * Other specific selectors
 */

const makeSelectShiftDetails = () => createSelector(
  selectEditShiftDomain(),
( substate ) => substate && substate.get( 'shift' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectEditShiftDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectEditShiftDomain(),
  ( substate ) => substate && substate.get( 'notification' ).toJS()
);
export {
  makeSelectShiftDetails,
  makeSelectLoading,
  makeSelectNotification
};
