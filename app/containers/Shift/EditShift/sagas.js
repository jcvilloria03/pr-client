/* eslint-disable require-jsdoc */
import { take, call, put, cancel, delay } from 'redux-saga/effects';
import { takeEvery } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import {
    SET_LOADING,
    SET_SHIFT,
    GET_SHIFT,
    SET_UPDATE,
    GET_UPDATE,
    SET_NOTIFICATION,
    GET_NOTIFICATION
} from './constants';
import { Fetch } from '../../../utils/request';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* getShiftDetails({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const { id } = payload;
        const response = yield call( Fetch, `/shift/${id}`, { method: 'GET' });
        yield put({
            type: SET_SHIFT,
            payload: response
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* updateForm({ payload }) {
    try {
        yield [
            put({ type: SET_UPDATE, payload: true })
        ];
        yield call( Fetch, `/shift/${payload.id}`, { method: 'PUT', data: payload });
        yield put({
            type: SET_UPDATE,
            payload: false
        });
        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Record Updated Successfully',
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
        yield put({
            type: SET_UPDATE,
            payload: false
        });
    }
}

export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

export function* watchForGetShift() {
    const watcher = yield takeEvery( GET_SHIFT, getShiftDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForUpdateForm() {
    const watcher = yield takeEvery( GET_UPDATE, updateForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchNotify() {
    const watcher = yield takeEvery( GET_NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetShift,
    watchForUpdateForm
];
