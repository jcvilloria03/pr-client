/* eslint-disable require-jsdoc */
import {
    DEFAULT_ACTION,
    GET_SHIFT,
    GET_UPDATE
} from './constants';

/**
 *
 * EditShift actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

export function getRestDetails( payload ) {
    return {
        type: GET_SHIFT,
        payload
    };
}

export function updateForm( payload ) {
    return {
        type: GET_UPDATE,
        payload
    };
}
