import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_LOADING,
    SET_SHIFT,
    SET_NOTIFICATION
} from './constants';

const initialState = fromJS({
    loading: true,
    shift: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * EditShift reducer
 *
 */
function editShiftReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_SHIFT:
            return state.set( 'shift', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default editShiftReducer;
