import styled, { keyframes } from 'styled-components';

const anim3 = keyframes`
    to {
        transform: rotate(45deg) translate(3px, 3px);
    }
`;

const AlignRight = styled.div`
    text-align: right;
`;

const StyledLoader = styled.div`
    font-size: 16px;
    font-weight: 600;
    color: #00a5e5;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;

    .anim3 {
        padding-left: 10px;
        &:before {
            display: block;
            width: 12px;
            height: 12px;
            margin-top: -1px;
            border: 2px solid #00a5e5;
            content: "";
            animation: ${anim3} .5s ease infinite alternate;
            border-radius: 100% 100% 0 100%;
            transform: rotate( 45deg );
        }

        &:after {
            display: block;
            width: 12px;
            height: 3px;
            margin-top: 8px;
            background-color: rgba(0, 0, 0, 0.2);
            content: "";
            border-radius: 100%;
        }
    } 
`;

const PageWrapper = styled.div`
    .foot {
        text-align: right;
        padding: 10px 0;
        background: #f0f4f6;
    }

    .template {
        justify-content: flex-start !important;

        & > a {
            background-color: #FFF;
            color: #333;
            border: 1px solid #4ABA4A;
            padding: 6px 12px;
            border-radius: 30px;
            display: inline;
            vertical-align: middle;
            margin: auto auto 0 auto;
            max-width: 200px;

            &:hover, &:focus, &:active:focus {
                outline: none;
                background-color: #fff;
                color: #4ABA4A;
            }
        }
    }

`;

export { PageWrapper, AlignRight, StyledLoader };
