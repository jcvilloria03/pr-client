import {
    INITIALIZE,
    SUBMIT_FORM,
    UPLOAD_LEAVE_CREDITS,
    SAVE_LEAVE_CREDITS,
    FETCH_LEAVE_ENTITLEMENT,
    NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../../App/constants';

/**
 * Initialize data
 */
export function initializeData() {
    return {
        type: INITIALIZE
    };
}

/**
 * Fetch leave entitlement for employee and leave type
 * @param {Object} data
 * @returns {Object}
 */
export function fetchLeaveEntitlement( data ) {
    return {
        type: FETCH_LEAVE_ENTITLEMENT,
        payload: data
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Submit a new leave credit
 */
export function submitLeaveCredit( payload, prepopulatedEmployeeId = null ) {
    return {
        type: SUBMIT_FORM,
        payload,
        prepopulatedEmployeeId
    };
}

/**
 * Upload leave credit
 */
export function uploadLeaveCredits( payload ) {
    return {
        type: UPLOAD_LEAVE_CREDITS,
        payload
    };
}

/**
 * Save leave credits
 */
export function saveLeaveCredits( payload ) {
    return {
        type: SAVE_LEAVE_CREDITS,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
