import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectAddLeaveCreditDomain = () => ( state ) => state.get( 'addLeaveCredits' );

const makeSelectLeaveCredit = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'leaveCredit' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectGenerating = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'generating' )
);

const makeSelectFormOptions = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectLeaveEntitlement = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'leaveEntitlement' ).toJS()
);

const makeSelectsubmitForm = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'submit' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectLeaveCreditsPreview = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'leaveCreditsPreview' ).toJS()
);

const makeSelectSaving = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectAddLeaveCreditDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectLeaveCredit,
    makeSelectsubmitForm,
    makeSelectSubmitted,
    makeSelectLeaveEntitlement,
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectLeaveCreditsPreview,
    makeSelectSaving,
    makeSelectGenerating,
    makeSelectErrors,
    makeSelectNotification
};
