import styled from 'styled-components';

export const MainWrapper = styled.div`
    padding-top: 76px;
    padding-bottom: 95px;
    background: #fff;

    .tableAction button {
        width: 130px;
    }

    .title {
        display: flex;
        align-items: center;
        margin-bottom: 20px;

        h5 {
            margin: 0;
            margin-right: 20px;
            font-weight: 600;
            font-size: 22px;
        }

        .search-wrapper {
            flex-grow: 1;

            .search {
                width: 300px;
                border: 1px solid #333;
                border-radius: 30px;

                input {
                    border: none;
                }
            }

            p {
                display: none;
            }

            .input-group,
            .form-control {
                background-color: transparent;
            }

            .input-group-addon {
                background-color: transparent;
                border: none;
            }

            .isvg {
                display: inline-block;
                width: 1rem;
            }
        }
    }

    .nav {
        padding: 10px 20px;
        background: #f0f4f6;
        margin-bottom: 50px;
    }

    .loader {
        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .money {
        input {
            text-align: right;
        }
    }

    .stepper {
        padding: 20px 10vw;
    }

    .radiogroup {
        padding: 0;
        & > span {
            padding: 0 15px;
            color: #5b5b5b;
            font-size: 14px;
            margin-bottom: 4px;
            margin-right: 1rem;
            font-weight: 400;
        }
    }

    .heading {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 0 auto 30px auto;

        h3 {
            font-weight: 600;
        }

        p {
            text-align: center;
            max-width: 800px;
        }
    }

    .hide {
        display: none;
    }

    .selected .fill {
      fill: #fff;
    }

    .selected .stroke {
      stroke: #fff;
    }

    .fill {
      fill: rgb(0,165,226);
    }

    .stroke {
      stroke: rgb(0,165,226);
    }

    .fake-label {
        color: #5b5b5b;
        font-size: 14px;
        font-weight: 400;
        margin-bottom: 4px;
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

export const PageWrapper = styled.div`
    .foot {
        text-align: right;
        padding: 10px 0;
        background: #f0f4f6;
    }
`;
