import React from 'react';
import isEmpty from 'lodash/isEmpty';
import capitalize from 'lodash/capitalize';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import get from 'lodash/get';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { stripNonDigit } from 'utils/functions';
import SnackBar from 'components/SnackBar';
import SalConfirm from 'components/SalConfirm';
import Loader from 'components/Loader';
import Input from 'components/Input';
import SalSelect from 'components/Select';
import Button from 'components/Button';

import {
    PageWrapper,
    MainWrapper,
    ConfirmBodyWrapperStyle,
    Footer
} from './styles';

import { UNLIMITED } from '../constants';
import {
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectLoading,
    makeSelectLeaveEntitlement
} from '../selectors';

import * as createLeaveCreditActions from '../actions';

const inputTypes = {
    input: ['units_remaining'],
    select: [ 'employee', 'leave_type' ]
};

/**
 * ManualEntry Container
 */
export class ManualEntry extends React.Component {
    static propTypes = {
        initializeData: React.PropTypes.func,
        fetchLeaveEntitlement: React.PropTypes.func,
        submitLeaveCredit: React.PropTypes.func,
        formOptions: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        errors: React.PropTypes.object,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        employeeId: React.PropTypes.string
    }

    static defaultProps = {
        loading: true,
        errors: {}
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            leaveTypes: this.props.formOptions.leaveTypes,
            employee: props.employeeId ? { id: props.employeeId } : {},
            leaveType: {},
            leaveEntitlement: {},
            units_remaining: '',
            errors: {},
            showModal: false
        };

        this.searchInput = null;
    }

    /**
     * Page Authorization check
     */
    componentWillMount() {
        isAuthorized(['create.leave_credit'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        this.props.initializeData();
    }

    /**
     * Display API errors in fields
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });

        this.setInitialStateFromProps( this.props );
        this.setState({ leaveEntitlement: nextProps.leaveEntitlement });
    }

    /**
     * Gets filter props values.
     * @param {Array} options
     * @returns {Array}
     */
    getDataOptions( options ) {
        if ( !options || !options.length ) {
            return [];
        }

        return this.props.formOptions[ options ].map( ( option ) => (
            this.formatDataForMultiselect( option )
        ) );
    }

    /**
     * Gets remaining units string.
     */
    getRemainingUnitsLabel = () => (
        this.hasEntitledLeave()
            ? `${capitalize( this.state.leaveEntitlement.leave_credit_unit )} Remaining`
            : this.state.unit
                ? `${capitalize( this.state.unit )} Remaining`
                : 'Units Remaining'
    )

    /**
     * Sets initial state from parsed props.
     * @param {Object} props
     */
    setInitialStateFromProps = ( props ) => {
        this.setState({ leaveTypes: props.leaveTypes });
        this.setState({ employees: props.employees });
    }

    /**
     * Display errors from API
     */
    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.input.includes( key ) || inputTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, label: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    /**
     * Formats data for selection.
     * @param {Object} data
     * @returns {Object}
     */
    formatDataForMultiselect = ( data ) => ({
        value: data,
        label: data.label || `${data.first_name} ${data.middle_name} ${data.last_name} ${data.employee_id}`,
        disabled: false
    })

    /**
     * Validates are all fields filled.
     * @returns {Boolean}
     */
    validateForm() {
        return isEmpty( this.state.employee ) ||
            ( this.state.leaveType.leave_credit_required && !this.state.units_remaining ) ||
            isEmpty( this.state.leaveType ) ||
            !( this.hasEntitledLeave() ? this.state.leaveEntitlement.leave_credit_unit.toLowerCase() : this.state.unit );
    }

    /**
     * Submits leave credit
     */
    submitLeaveCredit = () => {
        const {
            leaveType: {
                value: leaveTypeId,
                leave_credit_required: leaveCreditRequired
            },
            employee,
            units_remaining: unitsRemaining,
            leaveEntitlement,
            unit
        } = this.state;

        if ( !this.validateForm() ) {
            this.props.submitLeaveCredit({
                leave_type_id: leaveTypeId,
                employee_id: this.props.employeeId || employee.id,
                remaining: leaveCreditRequired ? unitsRemaining : null,
                unit: this.hasEntitledLeave() ? leaveEntitlement.leave_credit_unit.toLowerCase() : unit
            }, this.props.employeeId );
        }
    }

    /**
     * Handles showing cancel modal.
     */
    handleCancelModal = () => {
        this.setState({ showModal: false }, () => {
            this.setState({ showModal: true });
        });
    }

    /**
     * Handles leave entitlement fetching.
     */
    handleFetchLeaveEntitlement() {
        if ( isEmpty( this.state.leaveType ) || isEmpty( this.state.employee ) ) {
            return;
        }

        this.props.fetchLeaveEntitlement({
            leave_type_id: this.state.leaveType.value,
            employee_id: this.state.employee.id
        });
    }

    /**
     * Checks for entitled leave.
     * @returns {Boolean}
     */
    hasEntitledLeave = () => (
        !!( this.state.leaveEntitlement && Object.keys( this.state.leaveEntitlement ).length && this.state.leaveEntitlement.leave_credit_unit )
    )

    /**
     * Formats units remaining label according to entitled leave
     * @returns {String}
     */
    formatUnitsRemainingLabel = () => {
        const unit = this.hasEntitledLeave()
            ? this.state.leaveEntitlement.leave_credit_unit.toLowerCase()
            : this.state.unit
                ? this.state.unit
                : 'units';

        return unit === 'units' ? '' : `Computed in ${unit.toUpperCase()}`;
    }

    /**
     * Route to go back to on cancel or after successful form submission.
     * @return {String}
     */
    goBack = () => {
        this.props.employeeId
            ? browserHistory.push( `/employee/${this.props.employeeId}`, true )
            : browserHistory.push( '/time/leaves', true );
    }

    /**
     * Updates remaining units.
     *
     * @param {String} value
     * @returns void
     */
    updateRemainingUnits = ( value ) => {
        this.units_remaining.setState({ value: stripNonDigit( value ) });

        this.setState({ units_remaining: stripNonDigit( value ) }, () => {
            if ( this.hasEntitledLeave ) {
                return;
            }

            this.handleFetchLeaveEntitlement();
        });
    }

    /**
     * Component Render Method
     */
    render() {
        return (
            <PageWrapper>
                <Helmet
                    title="Add Leave Credit"
                    meta={ [
                        { name: 'description', content: 'Create a new Leave Credit' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <MainWrapper>
                    <SalConfirm
                        onConfirm={ () => this.goBack() }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showModal }
                    />

                    <div className={ `loader ${this.props.loading ? '' : 'hide'}` }>
                        <Loader />
                    </div>

                    <div className={ this.props.loading ? 'hide' : '' }>
                        <Container>
                            { !this.props.employeeId && (
                                <div className="row">
                                    <div className="col-xs-4">
                                        <SalSelect
                                            id="employee"
                                            label="Employee name"
                                            required
                                            placeholder="Type in employee name or id"
                                            data={ this.getDataOptions( 'employees' ) }
                                            ref={ ( ref ) => { this.employee = ref; } }
                                            onChange={ ({ value }) => {
                                                this.setState({ employee: value }, () => {
                                                    this.handleFetchLeaveEntitlement();
                                                });
                                            } }
                                        />
                                    </div>
                                    <div className="col-xs-4 text-align-center">
                                        <div className="fake-label">Employee ID</div>
                                        <p style={ { marginTop: '14px' } }>{ this.state.employee && this.state.employee.employee_id || '00-00-000' }</p>
                                    </div>
                                </div>
                            ) }
                            <div className="row sl-u-gap-bottom--lg">
                                <div className="col-xs-4">
                                    <SalSelect
                                        id="leave_type"
                                        label="Leave type"
                                        required
                                        data={ this.getDataOptions( 'leaveTypes' ) }
                                        placeholder="Select Leave Type"
                                        ref={ ( ref ) => { this.leave_type = ref; } }
                                        onChange={ ({ value }) => {
                                            this.setState({ leaveType: value }, () => {
                                                this.handleFetchLeaveEntitlement();
                                                if ( !this.state.leaveType.leave_credit_required ) {
                                                    this.units_remaining.setState({
                                                        value: '',
                                                        error: false,
                                                        errorMessage: ' '
                                                    });
                                                }
                                            });
                                        } }
                                    />
                                </div>
                                <div className="col-xs-4">
                                    <Input
                                        id="units_remaining"
                                        label={ this.getRemainingUnitsLabel() }
                                        type="text"
                                        placeholder={ get( this.state, 'leaveType.leave_credit_required', true ) ? 'Type in desired number of leaves' : UNLIMITED }
                                        required={ this.state.leaveType.leave_credit_required }
                                        key="units_remaining"
                                        ref={ ( ref ) => { this.units_remaining = ref; } }
                                        onChange={ this.updateRemainingUnits }
                                        disabled={ !this.state.leaveType.leave_credit_required }
                                    />
                                    { this.state.leaveType.leave_credit_required && (
                                        <span>{ this.formatUnitsRemainingLabel() }</span>
                                    ) }
                                </div>
                                <div className="col-xs-4">
                                    <SalSelect
                                        id="unit"
                                        label="Unit"
                                        required
                                        data={ this.props.formOptions.units }
                                        disabled={ this.hasEntitledLeave() }
                                        value={ this.hasEntitledLeave() ? this.state.leaveEntitlement.leave_credit_unit.toLowerCase() : this.state.unit }
                                        ref={ ( ref ) => { this.unit = ref; } }
                                        onChange={ ({ value }) => {
                                            this.setState({ unit: value });
                                        } }
                                    />
                                </div>
                            </div>
                        </Container>
                    </div>
                </MainWrapper>
                <Footer>
                    <Container>
                        <Button
                            label="Cancel"
                            type="neutral"
                            size="large"
                            onClick={ this.handleCancelModal }
                        />
                        <Button
                            label={ this.props.submitted ? <Loader /> : 'Add' }
                            type="action"
                            size="large"
                            disabled={ this.props.loading || this.validateForm() }
                            onClick={ this.submitLeaveCredit }
                            ref={ ( ref ) => { this.submitButton = ref; } }
                        />
                    </Container>
                </Footer>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    formOptions: makeSelectFormOptions(),
    leaveEntitlement: makeSelectLeaveEntitlement(),
    submitted: makeSelectSubmitted(),
    errors: makeSelectErrors(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createLeaveCreditActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ManualEntry );
