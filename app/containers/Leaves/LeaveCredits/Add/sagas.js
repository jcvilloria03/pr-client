import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { company } from '../../../../utils/CompanyService';
import { Fetch } from '../../../../utils/request';

import {
    INITIALIZE,
    LOADING,
    SUBMIT_FORM,
    SUBMITTED,
    UPLOAD_LEAVE_CREDITS,
    SET_ERRORS,
    SET_FORM_OPTIONS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    SET_SAVING_STATUS,
    SET_SAVING_ERRORS,
    SAVE_LEAVE_CREDITS,
    SET_LEAVE_ENTITLEMENT,
    GET_LEAVE_CREDITS_PREVIEW,
    SET_LEAVE_CREDITS_PREVIEW_STATUS,
    SET_LEAVE_CREDITS_PREVIEW_DATA,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    FETCH_LEAVE_ENTITLEMENT
} from './constants';

import { REINITIALIZE_PAGE } from '../../../App/constants';
import { resetStore } from '../../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formOptions = {};

        const leaveTypes = yield call( Fetch, `/company/${companyId}/leave_types`, { method: 'GET' });
        const employees = yield call( Fetch, `/company/${companyId}/employees`, { method: 'GET' });

        formOptions.leaveTypes = leaveTypes.data.map( ( value ) => ({
            label: value.name,
            value: value.id,
            companyId: value.company_id,
            leave_credit_required: value.leave_credit_required
        }) );
        formOptions.employees = employees.data;
        formOptions.units = [
            {
                label: 'Hours',
                value: 'hours'
            },
            {
                label: 'Days',
                value: 'days'
            }
        ];

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload, prepopulatedEmployeeId = null }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        const data = {
            company_id: company.getLastActiveCompanyId(),
            employee_id: payload.employee_id,
            leave_type_id: payload.leave_type_id,
            unit: payload.unit,
            value: payload.remaining
        };
        yield call( Fetch, '/leave_credit', { method: 'POST', data });
        yield call( delay, 500 );

        if ( prepopulatedEmployeeId ) {
            yield call( browserHistory.push, `/employee/${prepopulatedEmployeeId}`, true );
        } else {
            yield call( browserHistory.push, '/time/leaves', true );
        }
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Fetches leave entitlement for parsed employee_id and leave_type_id.
 */
export function* fetchLeaveEntitlement({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const employeeId = payload.employee_id;
        const leaveTypeId = payload.leave_type_id;

        const leaveEntitlement = yield call( Fetch, `/employee/${employeeId}/leave_entitlement?leave_type_id=${leaveTypeId}`, {
            method: 'GET'
        });

        yield put({
            type: SET_LEAVE_ENTITLEMENT,
            payload: leaveEntitlement
        });
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }

        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Uploads the CSV of leaveCredits to add and starts the validation process
 */
export function* uploadLeaveCredits({ payload }) {
    try {
        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: {}
        });

        const data = new FormData();

        data.append( 'company_id', company.getLastActiveCompanyId() );
        data.append( 'file', payload.file );

        const upload = yield call( Fetch, '/leave_credit/upload', {
            method: 'POST',
            data
        });

        if ( upload.id ) {
            yield put({
                type: SET_BATCH_UPLOAD_JOB_ID,
                payload: upload.id
            });

            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: 'validation_queued'
            });

            yield call( checkValidation, { payload: { jobId: upload.id, step: 'validation' }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const check = yield call(
            Fetch,
            `/leave_credit/upload/status?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}&step=${payload.step}`,
            { method: 'GET' }
        );

        if ( payload.step === 'validation' ) {
            if ( check.status === 'validation_failed' ) {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_LEAVE_CREDITS_PREVIEW_DATA,
                    payload: []
                });

                yield put({
                    type: SET_BATCH_UPLOAD_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'validated' ) {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_BATCH_UPLOAD_ERRORS,
                    payload: {}
                });

                yield call( getLeaveCreditsPreview, { payload: { jobId: payload.jobId }});
            } else {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield call( delay, 2000 );
                yield call( checkValidation, { payload: { jobId: payload.jobId, step: 'validation' }});
            }
        } else if ( payload.step === 'save' ) {
            if ( check.status === 'save_failed' ) {
                yield put({
                    type: SET_SAVING_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_SAVING_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'saved' ) {
                yield put({
                    type: SET_SAVING_STATUS,
                    payload: check.status
                });

                browserHistory.push( '/time/leaves', true );
            } else {
                yield call( delay, 2000 );
                yield call( checkValidation, { payload });
            }
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Fetches preview for uploaded leave credits
 */
export function* getLeaveCreditsPreview({ payload }) {
    try {
        yield put({
            type: SET_LEAVE_CREDITS_PREVIEW_DATA,
            payload: []
        });

        yield put({
            type: SET_LEAVE_CREDITS_PREVIEW_STATUS,
            payload: 'busy'
        });

        const preview = yield call(
            Fetch,
            `/leave_credit/upload/preview?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}`,
            { method: 'GET' }
        );

        yield put({
            type: SET_LEAVE_CREDITS_PREVIEW_DATA,
            payload: preview
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LEAVE_CREDITS_PREVIEW_STATUS,
            payload: 'ready'
        });
    }
}

/**
 * Saves validated leaveCredits
 */
export function* saveLeaveCredits({ payload }) {
    try {
        const upload = yield call( Fetch, '/leave_credit/upload/save', {
            method: 'POST',
            data: {
                company_id: company.getLastActiveCompanyId(),
                job_id: payload.jobId
            }
        });

        if ( upload.id ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: 'save_queued'
            });

            yield call( checkValidation, { payload: {
                step: 'save',
                jobId: payload.jobId
            }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPLOAD_LEAVE_CREDITS
 */
export function* watchForUploadLeaveCredits() {
    const watcher = yield takeEvery( UPLOAD_LEAVE_CREDITS, uploadLeaveCredits );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_LEAVE_CREDITS_PREVIEW
 */
export function* watchForGetLeaveCreditsPreview() {
    const watcher = yield takeEvery( GET_LEAVE_CREDITS_PREVIEW, getLeaveCreditsPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for FETCH_LEAVE_ENTITLEMENT
 */
export function* watchForFetchLeaveEntitlement() {
    const watcher = yield takeEvery( FETCH_LEAVE_ENTITLEMENT, fetchLeaveEntitlement );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SAVE_LEAVE_CREDITS
 */
export function* watchForSaveLeaveCredits() {
    const watcher = yield takeEvery( SAVE_LEAVE_CREDITS, saveLeaveCredits );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForGetLeaveCreditsPreview,
    watchForUploadLeaveCredits,
    watchForInitializeData,
    watchForFetchLeaveEntitlement,
    watchForReinitializePage,
    watchForSaveLeaveCredits,
    watchNotify
];
