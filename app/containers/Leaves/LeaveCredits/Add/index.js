import React, { Component } from 'react';
import get from 'lodash/get';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../../components/A';
import Icon from '../../../../components/Icon';
import Toggle from '../../../../components/Toggle';
import SnackBar from '../../../../components/SnackBar';
import SubHeader from '../../../../containers/SubHeader';

import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../../utils/constants';
import { subscriptionService } from '../../../../utils/SubscriptionService';
import { browserHistory } from '../../../../utils/BrowserHistory';

import BatchUpload from './BatchUpload';
import ManualEntry from './ManualEntry';

import { makeSelectNotification } from './selectors';

import {
    PageWrapper,
    HeadingWrapper,
    FormWrapper,
    NavWrapper
} from './styles';

const TOGGLE_OPTIONS = {
    BATCH: 'BATCH',
    MANUAL: 'MANUAL'
};

/**
 * Add LeaveCredits Component
 */
class Add extends Component {
    static propTypes = {
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            selectedToggleOption: TOGGLE_OPTIONS.MANUAL,
            prepopulatedEmployeeId: get( this.props, 'location.query.employee_id' )
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    /**
     * Handle component receiving new props.
     * @param {Object} props
     */
    componentWillReceiveProps( props ) {
        const prepopulatedEmployeeId = get( props, 'location.query.employee_id' );

        if ( prepopulatedEmployeeId === this.state.prepopulatedEmployeeId ) {
            this.setState({ prepopulatedEmployeeId });
        }
    }

    /**
     * Returns the list of toggle options
     */
    getToggleOptions = () => ([
        {
            title: 'Batch Upload',
            subtext: 'Upload multiple entries at once using the template.',
            value: TOGGLE_OPTIONS.BATCH,
            icon: <Icon name="batchUpload" />
        },
        {
            title: 'Manual Entry',
            subtext: 'Create entries one by one.',
            value: TOGGLE_OPTIONS.MANUAL,
            icon: <Icon name="manualEntry" />
        }
    ]);

    /**
     * Component Render Method
     */
    render() {
        const { selectedToggleOption } = this.state;

        return (
            <PageWrapper>
                <Helmet
                    title="Add Leave credits"
                    meta={ [
                        { name: 'description', content: 'Create a new Leave Credit' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/time/leaves', true );
                            } }
                        >
                            &#8592; Back to { this.props.previousRoute.name ? this.props.previousRoute.name : 'Leaves' }
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <HeadingWrapper>
                        <h3>Add Leave Credits</h3>
                        <p>
                            Add how much leave time an employee is entitled to through this page.
                            Leave credits can only be added for leave types that require leave credits.
                        </p>
                    </HeadingWrapper>
                    { !this.state.prepopulatedEmployeeId && (
                        <Toggle
                            options={ this.getToggleOptions() }
                            defaultSelected={ TOGGLE_OPTIONS.MANUAL }
                            onChange={ ( value ) => {
                                this.setState({
                                    selectedToggleOption: value
                                });
                            } }
                        />
                    )}
                </Container>
                <FormWrapper>
                    { selectedToggleOption === TOGGLE_OPTIONS.BATCH && !this.state.prepopulatedEmployeeId
                        ? <BatchUpload />
                        : <ManualEntry employeeId={ this.state.prepopulatedEmployeeId } />
                    }
                </FormWrapper>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        {},
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Add );
