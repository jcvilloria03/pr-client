import { fromJS } from 'immutable';

import {
    SET_LOADING,
    SET_UPDATING,
    SET_LEAVE_CREDIT_PREVIEW,
    SET_SUBMITTED,
    SET_API_ERRORS,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../../App/constants';

const initialState = fromJS({
    loading: true,
    updating: false,
    submitted: false,
    apiErrors: {
        editLeaveCredit: false
    },
    leaveCreditPreview: {
        employee: {},
        leave_type: {}
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 * Leave credit edit reducer
 */
const leaveCreditEditReduce = ( state = initialState, action ) => {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_UPDATING:
            return state.set( 'updating', action.payload );
        case SET_SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_LEAVE_CREDIT_PREVIEW:
            return state.set( 'leaveCreditPreview', fromJS( action.payload ) );
        case SET_API_ERRORS:
            return state.set( 'apiErrors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
};

export default leaveCreditEditReduce;
