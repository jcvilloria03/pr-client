import {
    INITIALIZE,
    UPDATE_LEAVE_CREDIT,
    NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../../App/constants';

/**
 * Initialize data
 */
export function initializeData( id ) {
    return {
        type: INITIALIZE,
        payload: {
            id
        }
    };
}

/**
 * Update leave credit
 */
export function updateLeaveCredit( payload ) {
    return {
        type: UPDATE_LEAVE_CREDIT,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
