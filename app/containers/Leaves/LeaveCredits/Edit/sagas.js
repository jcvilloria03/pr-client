import { take, call, put, cancel } from 'redux-saga/effects';
import { takeLatest, takeEvery, delay } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import { Fetch } from '../../../../utils/request';
import { browserHistory } from '../../../../utils/BrowserHistory';
import { formatFeedbackMessage } from '../../../../utils/functions';
import { RECORD_UPDATED_MESSAGE } from '../../../../utils/constants';

import {
    INITIALIZE,
    SET_LOADING,
    SET_UPDATING,
    SET_API_ERRORS,
    SET_LEAVE_CREDIT_PREVIEW,
    UPDATE_LEAVE_CREDIT,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';

import { REINITIALIZE_PAGE } from '../../../App/constants';
import { resetStore } from '../../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const initialPreview = yield call( Fetch, `/leave_credit/${payload.id}` );

        yield put({
            type: SET_LEAVE_CREDIT_PREVIEW,
            payload: initialPreview
        });
    } catch ( error ) {
        yield call( notifyUser, error );
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Update leaveCredit
 */
export function* updateLeaveCredit({ payload }) {
    try {
        yield put({
            type: SET_UPDATING,
            payload: true
        });

        yield put({
            type: SET_API_ERRORS,
            payload: {
                editLeaveCredit: false
            }
        });

        const leaveCreditId = payload.id;
        const data = {
            company_id: payload.company_id,
            value: payload.value
        };

        yield call( Fetch, `/leave_credit/${leaveCreditId}`, { method: 'PUT', data });
        yield call( showSuccessMessage );
        yield call( resetStore );
        yield call( browserHistory.push, '/time/leaves', true );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });

        yield put({
            type: SET_API_ERRORS,
            payload: {
                updateLeaveCredit: true
            }
        });
        yield put({
            type: SET_UPDATING,
            payload: false
        });
    } finally {
        yield put({
            type: SET_UPDATING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    yield call( resetStore );
    yield call( initializeData );
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Watcher for INITIALIZE
 */
export function* watcherForInitializeData() {
    const watcher = yield takeLatest( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for update leaveCredit
 */
export function* watchForUpdateLeaveCredit() {
    const watcher = yield takeLatest( UPDATE_LEAVE_CREDIT, updateLeaveCredit );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for reinitialize page
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export default [
    watcherForInitializeData,
    watchForUpdateLeaveCredit,
    watchForNotifyUser,
    watchForReinitializePage
];
