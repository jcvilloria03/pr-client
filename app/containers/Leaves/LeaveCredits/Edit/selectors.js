import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectEditLeaveCreditDomain = () => ( state ) => state.get( 'editLeaveCredit' );

const makeSelectLoading = () => createSelector(
    selectEditLeaveCreditDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectUpdating = () => createSelector(
    selectEditLeaveCreditDomain(),
    ( substate ) => substate.get( 'updating' )
);

const makeSelectSubmitted = () => createSelector(
    selectEditLeaveCreditDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectApiErrors = () => createSelector(
    selectEditLeaveCreditDomain(),
    ( substate ) => substate.get( 'apiErrors' ).toJS()
);

const makeSelectLeaveCreditPreview = () => createSelector(
    selectEditLeaveCreditDomain(),
    ( substate ) => substate.get( 'leaveCreditPreview' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectEditLeaveCreditDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectUpdating,
    makeSelectSubmitted,
    makeSelectApiErrors,
    makeSelectLeaveCreditPreview,
    makeSelectNotification
};
