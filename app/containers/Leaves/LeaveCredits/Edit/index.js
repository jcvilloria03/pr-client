import React, { Component } from 'react';
import capitalize from 'lodash/capitalize';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import Input from '../../../../components/Input';
import Loader from '../../../../components/Loader';
import Button from '../../../../components/Button';
import SnackBar from '../../../../components/SnackBar';
import { H2, H3, P } from '../../../../components/Typography';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { isAuthorized } from '../../../../utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../../utils/constants';
import { subscriptionService } from '../../../../utils/SubscriptionService';
import { formatDate } from '../../../../utils/functions';

import SubHeader from '../../../../containers/SubHeader';

import {
    makeSelectLoading,
    makeSelectUpdating,
    makeSelectSubmitted,
    makeSelectApiErrors,
    makeSelectNotification,
    makeSelectLeaveCreditPreview
} from './selectors';

import * as actions from './actions';

import {
    PageWrapper,
    HeadingWrapper,
    LoaderWrapper,
    Footer
} from './styles';

/**
 * Edit LeaveCredit Component
 */
class Edit extends Component {
    static propTypes = {
        params: React.PropTypes.object,
        loading: React.PropTypes.bool,
        updating: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        apiErrors: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        leaveCreditPreview: React.PropTypes.object,
        updateLeaveCredit: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            remaining_units: this.props.leaveCreditPreview.value,
            leaveCreditPreview: {},
            permission: {
                edit: false
            },
            isEditView: true
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.leave_credit',
            'edit.leave_credit'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.leave_credit' ];

            if ( authorized ) {
                this.setState({ permission: {
                    edit: authorization[ 'edit.leave_credit' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        const { editLeaveCredit: editLeaveCreditApiErrors } = this.props.apiErrors;

        if ( !editLeaveCreditApiErrors ) {
            this.props.initializeData( this.props.params.id );
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.updating ) {
            return;
        }

        this.getInitialStateFromProps( nextProps );
    }

    /**
     * Extract initial state from props
     */
    getInitialStateFromProps = ({ leaveCreditPreview }) => {
        const locationState = browserHistory.getCurrentLocation().state;
        this.setState({
            leaveCreditPreview,
            isEditView: locationState && locationState.editView
        });
    }

    /**
     * Gets formated datetime.
     * @param {String}
     * @returns {String}
     */
    getformatedDateTime = ( dateTime ) => formatDate( dateTime, DATE_FORMATS.DISPLAY );

    /**
     * Gets remaining units label
     * @returns {String}
     */
    getRemainingUnitsLabel = () => `${capitalize( this.state.leaveCreditPreview.unit )} Remaining`;

    /**
     * Gets leave credit unit.
     * @returns {String}
     */
    getLeaveCreditUnit = () => (
        this.state.leaveCreditPreview && this.state.leaveCreditPreview.unit
            ? this.state.leaveCreditPreview.unit.toUpperCase()
            : ''
    )

    /**
     * Updates leave credit.
     */
    updateLeaveCredit = () => {
        if ( this.props.leaveCreditPreview.value === this.state.remaining_units || !this.state.remaining_units ) {
            return;
        }

        const data = {
            id: this.state.leaveCreditPreview.id,
            company_id: this.state.leaveCreditPreview.company_id,
            value: this.state.remaining_units
        };

        this.props.updateLeaveCredit( data );
    }

    /**
     * Component render method
     */
    render() {
        const { loading, leaveCreditPreview } = this.props;
        const { isEditView } = this.state;
        return (
            <div>
                <PageWrapper>
                    <Helmet
                        title="Edit Leave Credit"
                        meta={ [
                            { name: 'description', content: 'Edit a Leave Credit' }
                        ] }
                    />
                    <SnackBar
                        message={ this.props.notification.message }
                        title={ this.props.notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ this.props.notification.show }
                        delay={ 5000 }
                        type={ this.props.notification.type }
                    />
                    <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                    <Container>
                        <LoaderWrapper style={ { display: loading ? '' : 'none' } }>
                            <H2>Loading</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoaderWrapper>
                        <div style={ { display: !loading ? '' : 'none' } }>
                            <HeadingWrapper>
                                <h3>Edit Leave Credit</h3>
                                <P>You may edit and update employee&#39;s leave credits through this page.</P>
                            </HeadingWrapper>
                            <div>
                                <div className="row sl-u-gap-bottom">
                                    <div className="col-xs-3">
                                        <p>Leave type</p>
                                        { leaveCreditPreview.leave_type.name }
                                    </div>
                                    { isEditView ? (
                                        <div className="col-xs-3">
                                            <Input
                                                id="units_remaining"
                                                label={ this.getRemainingUnitsLabel() }
                                                type="number"
                                                value={ leaveCreditPreview.value }
                                                onChange={ ( value ) => this.setState({ remaining_units: value }) }
                                                required
                                            />
                                            <span>Computed in { this.getLeaveCreditUnit() }</span>
                                        </div>
                                    ) : (
                                        <div className="col-xs-3">
                                            <p>{this.getRemainingUnitsLabel()}</p>
                                            <span>{leaveCreditPreview.value}</span>
                                        </div>
                                    )}
                                    <div className="col-xs-3">
                                        <p>Created By</p>
                                        { leaveCreditPreview.created_by }
                                    </div>
                                    <div className="col-xs-3">
                                        <p>Created on</p>
                                        { this.getformatedDateTime( leaveCreditPreview.created_on ) }
                                    </div>
                                </div>
                                <div className="row sl-u-gap-bottom--lg">
                                    <div className="col-xs-3">
                                        <p>Employee Name</p>
                                        { leaveCreditPreview.employee.full_name }
                                    </div>
                                    <div className="col-xs-3">
                                        <p>Employee ID</p>
                                        { leaveCreditPreview.employee.employee_id }
                                    </div>
                                    <div className="col-xs-3">
                                        <p>Updated by</p>
                                        { leaveCreditPreview.updated_by }
                                    </div>
                                    <div className="col-xs-3">
                                        <p>Updated On</p>
                                        { this.getformatedDateTime( leaveCreditPreview.updated_on ) }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <Footer>
                    <Button
                        label="Cancel"
                        type="neutral"
                        size="large"
                        onClick={ () => {
                            this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/time/leaves', true );
                        } }
                    />
                    { isEditView ? (
                        <Button
                            className={ this.state.permission.edit ? '' : 'hide' }
                            label={ this.props.submitted ? <Loader /> : 'Update' }
                            type="action"
                            size="large"
                            onClick={ this.updateLeaveCredit }
                            disabled={ this.props.updating || this.props.submitted }
                        />
                    ) : (
                        <Button
                            className={ this.state.permission.edit ? '' : 'hide' }
                            label="Edit"
                            type="action"
                            size="large"
                            onClick={ () => this.setState({ isEditView: true }) }
                        />
                    ) }
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    updating: makeSelectUpdating(),
    submitted: makeSelectSubmitted(),
    apiErrors: makeSelectApiErrors(),
    notification: makeSelectNotification(),
    leaveCreditPreview: makeSelectLeaveCreditPreview()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Edit );
