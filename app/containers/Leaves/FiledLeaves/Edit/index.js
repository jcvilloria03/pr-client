import React, { Component } from 'react';
import Helmet from 'react-helmet';

import find from 'lodash/find';
import each from 'lodash/each';
import map from 'lodash/map';
import groupBy from 'lodash/groupBy';
import filter from 'lodash/filter';
import get from 'lodash/get';
import debounce from 'lodash/debounce';

import moment from 'moment';
import { extendMoment } from 'moment-range';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../../components/A';
import Button from '../../../../components/Button';
import SnackBar from '../../../../components/SnackBar';
import SubHeader from '../../../../containers/SubHeader';
import Loader from '../../../../components/Loader';
import SalSelect from '../../../../components/Select';
import Icon from '../../../../components/Icon';
import Input from '../../../../components/Input';
import DatePicker from '../../../../components/DatePicker';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { subscriptionService } from '../../../../utils/SubscriptionService';
import { restDayService } from '../../../../utils/RestDayService';
import { isAuthorized } from '../../../../utils/Authorization';
import {
    stripTime,
    formatDate,
    isSameOrAfter,
    getISOWeekday
} from '../../../../utils/functions';

import {
    EMPLOYEE_SUBHEADER_ITEMS,
    DATE_FORMATS,
    FILED_LEAVE_UNITS_SINGULAR,
    DAY_TYPES
} from '../../../../utils/constants';

import * as actions from './actions';

import {
    makeSelectFiledLeave,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectNotification,
    makeSelectLoadingShifts,
    makeSelectTotalLeaveUnits,
    makeSelectLoadingTotalLeaveUnits,
    makeSelectEmployeeRestDays
} from './selectors';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    Footer,
    ValueWrapper
} from './styles';

const inputTypes = {
    select: [ 'employee', 'status', 'leave_type_id', 'schedule_id' ],
    datePicker: [ 'start_date', 'end_date' ]
};

const REQUIRED_KEYS_FOR_EMPLOYEE_SHIFTS = [ 'start_date', 'end_date', 'employee_id' ];

/**
 * Edit Filed Leave
 */
class Details extends Component {
    static propTypes = {
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        filedLeave: React.PropTypes.object,
        formOptions: React.PropTypes.object,
        loading: React.PropTypes.bool,
        params: React.PropTypes.object,
        submitted: React.PropTypes.bool,
        errors: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        submitFiledLeave: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        products: React.PropTypes.array,
        getEmployeeShifts: React.PropTypes.func,
        loadingShifts: React.PropTypes.bool,
        totalLeaveUnits: React.PropTypes.string,
        loadingTotalLeaveUnits: React.PropTypes.bool,
        getTotalLeaveUnits: React.PropTypes.func,
        employeeRestDays: React.PropTypes.array
    };

    constructor( props ) {
        super( props );

        this.state = {
            filedLeave: {
                employee_id: '',
                leave_type_id: '',
                status: '',
                start_date: '',
                end_date: '',
                unit: '',
                leaves: []
            },
            employeeLeaves: {},
            errors: {},
            isProfile: get( this.props, 'location.query.is_profile' )
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['edit.leave_request'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        const { id: filedLeaveId } = this.props.params;
        this.props.initializeData( filedLeaveId, this.prepareShiftData );
    }

    componentWillReceiveProps( nextProps ) {
        if ( Object.keys( nextProps.filedLeave ).length !== Object.keys( this.props.filedLeave ).length ) {
            this.getInitialStateFromProps( nextProps );
        }

        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });

        nextProps.submitted !== this.props.submitted && this.submitButton.setState({ disabled: nextProps.submitted });

        const isProfile = get( nextProps, 'location.query.is_profile' );

        if ( isProfile === this.state.isProfile ) {
            this.setState({ isProfile });
        }
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    getInitialStateFromProps({ filedLeave }) {
        this.setState({
            filedLeave: {
                id: filedLeave.id,
                employee_id: filedLeave.employee_id,
                leave_type_id: filedLeave.leave_type_id,
                status: filedLeave.status,
                start_date: filedLeave.start_date,
                end_date: filedLeave.end_date,
                leaves: filedLeave.leaves,
                unit: filedLeave.unit,
                employee: filedLeave.employee_detail
            }
        });
    }

    setInitialTimeFields = () => {
        Object.keys( this.state.employeeLeaves ).forEach( ( date, index ) => {
            this.state.employeeLeaves[ date ].forEach( ( leave, leaveIndex ) => {
                if ( leave.start_time ) {
                    this[ `start_time_${index}_${leaveIndex}` ].state.value = leave.start_time;
                }
                if ( leave.end_time ) {
                    this[ `end_time_${index}_${leaveIndex}` ].state.value = leave.end_time;
                }
            });
        });
    }

    /**
     * Get total leave units
     */
    getTotalLeaveUnits = debounce( () => {
        const { unit, employee_id: employeeId } = this.state.filedLeave;

        const leaves = this.prepareFiledLeavePayload();

        this.validateTimeInputs();

        const payload = {
            leaves,
            employee_id: employeeId,
            unit
        };
        this.props.getTotalLeaveUnits( payload );
    }, 1000 )

    /**
     * Formats time string to moment object
     * @param {...String} dates
     *
     * @returns {Array}
     */
    getFormattedTime = ( ...dates ) => (
        dates.map( ( date ) => moment( date, DATE_FORMATS.TIME ) )
    )

    /**
     * Get range of dates that are qualified for applying for a filed leave
     *
     * @returns {Array}
     */
    getShiftRange = () => {
        const { start_date: startDate, end_date: endDate, employee } = this.state.filedLeave;

        const range = extendMoment( moment ).range( startDate, endDate );

        return Array.from( range.by( 'days' ) )
            .map( ( day ) => day.format( DATE_FORMATS.API ) )
            .filter( ( date ) => moment( date ).isAfter( employee.date_hired ) && moment( date ).isBefore( employee.date_ended ) );
    }

    /**
     * Sets error message on desired input
     * @param {String} field
     * @param {String} errorMessage
     */
    setInputErrorMessage = ( field, errorMessage ) => {
        this[ field ].setState({
            error: true,
            errorMessage
        });
    }

    /**
     * Resets error message on desired input
     * @param {String} field
     */
    resetInputErrorMessage = ( field ) => {
        this[ field ].setState({
            error: false,
            errorMessage: '&nbsp'
        });
    }

    resetTimeFields = () => {
        Object.keys( this.state.employeeLeaves ).forEach( ( date, index ) => {
            this.state.employeeLeaves[ date ].forEach( ( leave, leaveIndex ) => {
                if ( leave.start_time ) {
                    this[ `start_time_${index}_${leaveIndex}` ].state.value = '';
                }
                if ( leave.end_time ) {
                    this[ `end_time_${index}_${leaveIndex}` ].state.value = '';
                }
            });
        });
    }

    /**
     * Fetch employee shifts in case we have enough information
     * @param {String} field
     */
    fetchEmployeeShifts = ( field ) => {
        const { start_date: startDate, end_date: endDate, employee_id: employeeId } = this.state.filedLeave;
        if ( REQUIRED_KEYS_FOR_EMPLOYEE_SHIFTS.includes( field ) && ( startDate && endDate && employeeId ) ) {
            const valid = this.validateEndDate();
            valid && this.props.getEmployeeShifts( startDate, endDate, employeeId, this.prepareShiftData );
        }
    }

    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.input.includes( key ) || inputTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, label: this.state.errors[ key ][ 0 ] });
                } else if ( inputTypes.datePicker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    /**
     * Remaps shift data into an object which has dates as keys
     * @param {Array} employeeShifts
     */
    prepareShiftData = ( employeeShifts, initial = false ) => {
        const { leaves: filedLeaves } = this.state.filedLeave;
        let leaves = [];

        !initial && this.resetTimeFields();

        each( employeeShifts, ( shift ) => {
            const preparedData = map( shift.dates, ( date ) => {
                const { id, name, start_time, end_time } = shift.schedule;
                const leave = find( filedLeaves, [ 'date', date ]);
                return this.prepareSingleShift(
                    id,
                    name,
                    date,
                    leave && initial ? leave.start_time : null,
                    leave && initial ? leave.end_time : null,
                    start_time,
                    end_time
                );
            });

            leaves = [ ...leaves, ...preparedData ];
        });

        leaves = this.groupLeavesByDate( leaves );
        leaves = this.includeDefaultSchedule( leaves );

        this.setState({ employeeLeaves: { ...this.sortLeavesByDate( leaves ) }}, () => {
            initial && this.setInitialTimeFields();
        });
    }

    updateFiledLeave( key, value ) {
        if ( typeof value === 'undefined' ) {
            return;
        }

        const dataClone = Object.assign({}, this.state.filedLeave );
        dataClone[ key ] = value !== '' && value !== null ? value : '';

        this.setState({ filedLeave: Object.assign( this.state.filedLeave, dataClone ) }, () => {
            this.fetchEmployeeShifts( key );
        });
    }

    checkStartTime() {
        let valid = false;
        const { value } = this.start_time.state;
        if ( value ) {
            const regex = new RegExp( /\d{2}[:]\d{2}/ );
            valid = regex.test( this.start_time.state.value );
            !valid && this.start_time.setState({
                error: !valid,
                errorMessage: 'Invalid Start Time'
            });
        }
        return valid;
    }

    checkEndTime() {
        let valid = false;
        const { value } = this.end_time.state;
        if ( value ) {
            const regex = new RegExp( /\d{2}[:]\d{2}/ );
            valid = regex.test( this.end_time.state.value );
            !valid && this.end_time.setState({
                error: !valid,
                errorMessage: 'Invalid End Time'
            });
        }
        return valid;
    }

    /**
     * Validate time
     * @param {String} value
     *
     * @returns {Boolean}
     */
    validateTime( value ) {
        let valid = false;
        if ( value ) {
            const regex = new RegExp( /\d{2}[:]\d{2}/ );
            valid = regex.test( value );
        }
        return valid;
    }

    /**
     * Validates times according to schedule
     * @param {Object} leave
     * @param {Number} index
     * @param {Number} leaveIndex
     *
     * @returns {Boolean}
     */
    validateLeaveTimes( leave, index, leaveIndex ) {
        let [
            startTime, // eslint-disable-line
            endTime, // eslint-disable-line
            scheduleStartTime,
            scheduleEndTime
        ] = this.getFormattedTime( leave.start_time, leave.end_time, leave.schedule_start_time, leave.schedule_end_time );

        if ( scheduleStartTime.isAfter( scheduleEndTime ) ) {
            scheduleEndTime = moment( scheduleEndTime ).add( 1, 'day' );
            scheduleStartTime = moment( scheduleStartTime ).subtract( 1, 'day' );
        }

        if ( startTime.isSameOrAfter( moment( endTime ).add( 1, 'day' ) ) ) {
            this.setInputErrorMessage( `start_time_${index}_${leaveIndex}`, 'Start time must be before end time' );
            this.setInputErrorMessage( `end_time_${index}_${leaveIndex}`, 'End time must be after start time' );

            return false;
        }
        if ( startTime.isBefore( scheduleStartTime ) ) {
            this.setInputErrorMessage( `start_time_${index}_${leaveIndex}`, 'Start time must be same or after schedule start time' );

            return false;
        }

        if ( startTime.isSameOrAfter( scheduleEndTime ) ) {
            this.setInputErrorMessage( `start_time_${index}_${leaveIndex}`, 'Start time must be before schedule end time' );

            return false;
        }

        if ( endTime.isAfter( scheduleEndTime ) ) {
            this.setInputErrorMessage( `end_time_${index}_${leaveIndex}`, 'End time must be same or before schedule end time' );

            return false;
        }

        if ( endTime.isSameOrBefore( scheduleStartTime ) ) {
            this.setInputErrorMessage( `end_time_${index}_${leaveIndex}`, 'End time must be after schedule start time' );
            return false;
        }

        return true;
    }

    validateForm() {
        let valid = true;

        if ( !this.status._checkRequire( this.status.state.value ) ) {
            valid = false;
        }

        if ( !this.leave_type_id._checkRequire( this.leave_type_id.state.value ) ) {
            valid = false;
        }

        if ( this.start_date.checkRequired() ) {
            valid = false;
        }

        if ( this.end_date.checkRequired() ) {
            valid = false;
        }

        valid = this.validateEndDate( valid );

        valid = this.validateTimeInputs( valid );

        return valid;
    }

    /**
     * Prepare a single shift object
     * @param {Number} scheduleId
     * @param {String} scheduleName
     * @param {String} date
     * @param {String} startTime
     * @param {String} endTime
     * @param {String} scheduleStartTime
     * @param {String} scheduleEndTime
     *
     * @returns {Object}
     */
    prepareSingleShift = ( scheduleId, scheduleName, date, startTime, endTime, scheduleStartTime, scheduleEndTime ) => ({
        schedule_id: scheduleId,
        schedule_name: scheduleName,
        date,
        start_time: startTime,
        end_time: endTime,
        schedule_start_time: scheduleStartTime,
        schedule_end_time: scheduleEndTime
    })

    /**
     * Groups leaves by date
     * @param {Array} leaves
     *
     * @returns {Object}
     */
    groupLeavesByDate = ( leaves ) => {
        const { start_date: startDate, end_date: endDate } = this.state.filedLeave;

        return groupBy( filter( leaves, ( leave ) => (
            moment( leave.date ).isSameOrAfter( startDate ) && moment( leave.date ).isSameOrBefore( endDate )
        ) ), 'date' );
    }

    /**
     * Sorts leaves by date
     * @param {Object}
     *
     * @returns {Object}
     */
    sortLeavesByDate = ( leaves ) => (
        Object.keys( leaves )
            .sort()
            .reduce( ( accumulator, current ) => ( accumulator[ current ]  = leaves[ current ], accumulator ), {}) // eslint-disable-line
    )

    /**
     * Adds default schedules to leaves
     * @param {Object} employeeLeaves
     *
     * @returns {Object}
     */
    includeDefaultSchedule = ( employeeLeaves ) => {
        const { defaultSchedule } = this.props.formOptions;
        let leaves = { ...employeeLeaves };
        const restDays = this.prepareRestDays();

        each( this.getShiftRange(), ( date ) => {
            if ( !Object.keys( leaves ).includes( date ) && !Object.keys( restDays ).includes( date ) ) {
                const weekDay = getISOWeekday( date );
                const defaultScheduleForDay = find( defaultSchedule, [ 'day_of_week', weekDay ]);
                const [ scheduleStartTime, scheduleEndTime ] = [
                    formatDate( defaultScheduleForDay.work_start, DATE_FORMATS.TIME ),
                    formatDate( defaultScheduleForDay.work_end, DATE_FORMATS.TIME )
                ];

                if ( defaultScheduleForDay.day_type !== DAY_TYPES.REST_DAY ) {
                    const leave = find( this.state.filedLeave.leaves, [ 'date', date ]);

                    leaves = {
                        ...leaves,
                        [ date ]: [this.prepareSingleShift(
                            null,
                            'Default Schedule',
                            date,
                            leave ? leave.start_time : null,
                            leave ? leave.end_time : null,
                            scheduleStartTime,
                            scheduleEndTime )
                        ]
                    };
                }
            }
        });

        return leaves;
    }

    validateEndDate = ( formValid = true ) => {
        let valid = formValid;
        if ( this.state.filedLeave.start_date
            && this.state.filedLeave.end_date
            && !isSameOrAfter( this.state.filedLeave.end_date, this.state.filedLeave.start_date, [DATE_FORMATS.API])
        ) {
            const message = 'The end date must be a date after start date.';
            this.end_date.setState({ error: true, message });

            valid = false;
        }

        return valid;
    }

    validateTimeInputs = ( formValid = true ) => {
        let valid = formValid;
        Object.keys( this.state.employeeLeaves ).forEach( ( date, index ) => {
            this.state.employeeLeaves[ date ].forEach( ( leave, leaveIndex ) => {
                const { start_time: startTime, end_time: endTime } = leave;
                if ( startTime && !endTime ) {
                    this.setInputErrorMessage( `end_time_${index}_${leaveIndex}`, 'End time must be present if start time is present' );
                    valid = false;
                } else if ( !startTime && endTime ) {
                    this.setInputErrorMessage( `start_time_${index}_${leaveIndex}`, 'Start time must be present if end time is present' );
                    valid = false;
                } else if ( startTime && endTime ) {
                    const startTimeValid = this.validateTime( startTime );
                    const endTimeValid = this.validateTime( endTime );

                    if ( startTimeValid && endTimeValid ) {
                        if ( !this.validateLeaveTimes( leave, index, leaveIndex ) ) {
                            valid = false;
                        } else {
                            this.resetInputErrorMessage( `start_time_${index}_${leaveIndex}` );
                            this.resetInputErrorMessage( `end_time_${index}_${leaveIndex}` );
                        }
                    } else {
                        !startTimeValid && this.setInputErrorMessage( `start_time_${index}_${leaveIndex}`, 'Date Invalid' );
                        !endTimeValid && this.setInputErrorMessage( `end_time_${index}_${leaveIndex}`, 'Date Invalid' );
                        valid = false;
                    }
                }
            });
        });

        return valid;
    }

    /**
     * Remap filed leaves payload to match the needs of API
     *
     * @returns {Array}
     */
    prepareFiledLeavePayload = () => {
        const leaves = [];
        Object.keys( this.state.employeeLeaves ).forEach( ( key ) => {
            this.state.employeeLeaves[ key ].forEach( ( leave ) => {
                const { schedule_id: scheduleId, date, start_time: startTime, end_time: endTime } = leave;
                if ( startTime && endTime ) {
                    leaves.push({
                        schedule_id: scheduleId,
                        date,
                        start_time: startTime,
                        end_time: endTime
                    });
                }
            });
        });

        return leaves;
    }

    submitFiledLeave = () => {
        if ( this.validateForm() ) {
            const payload = {
                ...this.state.filedLeave,
                leaves: this.prepareFiledLeavePayload()
            };
            if ( this.state.isProfile ) {
                payload.isProfile = true;
            }

            this.props.submitFiledLeave( payload );
        }
    }

    handleLeaveTypeChange = ( value, unit ) => {
        this.setState({
            filedLeave: {
                ...this.state.filedLeave,
                leave_type_id: parseInt( value, 10 ),
                unit
            }
        }, () => {
            this.getTotalLeaveUnits();
        });
    }

    prepareRestDays = () => {
        const { start_date: startDate, end_Date: endDate } = this.state.filedLeave;
        restDayService.setDates( startDate, endDate );

        return restDayService.handleRestDaysDateRange( this.props.employeeRestDays );
    }

    /**
     * Find leave type by its id
     * @param {String|Number} id
     *
     * @returns {Object}
     */
    findLeaveType = ( id ) => (
        find( this.props.formOptions.leaveTypes, ( leaveType ) => leaveType.id === parseInt( id, 10 ) )
    )

    /**
     * Determine if the status of this filed Leave is either Approving or Declining
     */
    isProcessingStatus() {
        return [ 'Approving', 'Declining' ].includes( this.state.filedLeave.status );
    }

    /**
     * Determine are there shifts selected or is there at least one filled out shift
     */
    areShiftsSelected = () => {
        if ( !Object.keys( this.state.employeeLeaves ).length ) {
            return false;
        }

        const leaves = this.prepareFiledLeavePayload();

        return leaves && !!leaves.length;
    }

    /**
     * Renders shift section
     */
    renderShiftSection = () => {
        const { end_date: endDate, start_date: startDate, employee_id: employeeId } = this.state.filedLeave;

        return (
            <div className="row">
                <div className="col-xs-12" style={ { minHeight: '200px' } }>
                    <strong>Select Shift</strong>
                    { !endDate || !startDate ? (
                        <div style={ { textAlign: 'center', paddingTop: '20px' } }>No selected date range</div>
                    ) : !employeeId ? (
                        <div style={ { textAlign: 'center', paddingTop: '20px' } }>No selected employee</div>
                    ) : this.renderLoadingOrShifts() }
                </div>
            </div>
        );
    }

    renderLoadingOrShifts = () => (
        <div>
            <div className={ !this.props.loadingShifts ? 'hide' : '' } style={ { textAlign: 'center', paddingTop: '20px' } }>Loading shifts...</div>
            { this.renderShiftList() }
        </div>
    )

    /**
     * Renders shift list
     */
    renderShiftList = () => {
        const { employeeLeaves: leaves } = this.state;

        return Object.keys( leaves ).length ? Object.keys( leaves ).map( ( date, index ) => (
            <div key={ index }>
                <div className={ this.props.loadingShifts ? 'hide' : '' } style={ { marginTop: '30px' } }>
                    <div className="row">
                        <div className="col-xs-4">
                            <ValueWrapper>
                                <strong>
                                    { formatDate( date, DATE_FORMATS.DISPLAY ) } Shift
                                </strong>
                            </ValueWrapper>
                        </div>
                    </div>
                    {
                        leaves[ date ].map( ( leave, leaveIndex ) => (
                            <div className="row" key={ `${index}_${leaveIndex}` }>
                                <div className="col-xs-4">
                                    <ValueWrapper>
                                        <ValueWrapper>
                                            <div>Shift</div>
                                            <ValueWrapper>
                                                <div style={ { marginTop: '10px' } }>
                                                    { leave.schedule_name }
                                                </div>
                                            </ValueWrapper>
                                        </ValueWrapper>
                                    </ValueWrapper>
                                </div>
                                <div className="col-xs-4">
                                    <Input
                                        id={ `start-time-${index}-${leaveIndex}` }
                                        label="Start Time"
                                        placeholder="Choose desired time"
                                        ref={ ( ref ) => { this[ `start_time_${index}_${leaveIndex}` ] = ref; } }
                                        addon={ {
                                            content: <Icon name="clock" />,
                                            placement: 'right'
                                        } }
                                        onChange={ ( value ) => {
                                            const formatted = stripTime( value );

                                            this[ `start_time_${index}_${leaveIndex}` ].setState({ value: formatted }, () => {
                                                const leavesClone = { ...this.state.employeeLeaves };
                                                leavesClone[ date ][ leaveIndex ].start_time = formatted;

                                                this.setState({
                                                    employeeLeaves: leavesClone
                                                }, () => {
                                                    this.getTotalLeaveUnits();
                                                });
                                            });
                                        } }
                                    />
                                </div>
                                <div className="col-xs-4">
                                    <Input
                                        id={ `end-time-${index}-${leaveIndex}` }
                                        label="End Time"
                                        placeholder="Choose desired time"
                                        ref={ ( ref ) => { this[ `end_time_${index}_${leaveIndex}` ] = ref; } }
                                        addon={ {
                                            content: <Icon name="clock" />,
                                            placement: 'right'
                                        } }
                                        onChange={ ( value ) => {
                                            const formatted = stripTime( value );

                                            this[ `end_time_${index}_${leaveIndex}` ].setState({ value: formatted }, () => {
                                                const leavesClone = { ...this.state.employeeLeaves };
                                                leavesClone[ date ][ leaveIndex ].end_time = formatted;

                                                this.setState({
                                                    employeeLeaves: leavesClone
                                                }, () => {
                                                    this.getTotalLeaveUnits();
                                                });
                                            });
                                        } }
                                    />
                                </div>
                            </div>
                        ) )
                    }
                </div>
            </div>
        ) ) : (
            <div className={ this.props.loadingShifts ? 'hide' : '' } style={ { textAlign: 'center', paddingTop: '20px' } }>
                No shifts for selected employee and date range
            </div>
        );
    }

    /**
     * Renders summary
     */
    renderSummary = () => {
        const {
            leave_type_id: leaveTypeId, status, start_date: startDate, end_date: endDate
        } = this.state.filedLeave;

        const leaveType = this.findLeaveType( leaveTypeId );
        return (
            <div className="row" style={ { marginTop: '20px' } }>
                <div className="col-xs-12">
                    <p style={ { fontWeight: 'bold' } }>Summary</p>
                    <div className="summary-panel">
                        <div className="summary-panel__column">
                            <span>Leave Type:</span>
                            <ValueWrapper>{ leaveType && leaveType.label ? leaveType.label : 'N / A' }</ValueWrapper>
                        </div>
                        <div className="summary-panel__column">
                            <span>Status:</span>
                            <ValueWrapper>{ status || 'N / A' }</ValueWrapper>
                        </div>
                        <div className="summary-panel__column">
                            <span>Duration:</span>
                            <ValueWrapper>{ startDate && endDate
                                ? `${formatDate( startDate, DATE_FORMATS.DISPLAY )} - ${formatDate( endDate, DATE_FORMATS.DISPLAY )}`
                                : 'N / A' }
                            </ValueWrapper>
                        </div>
                        <div className="summary-panel__column">
                            <span>Number of { !this.state.filedLeave.unit ? 'units' : this.state.filedLeave.unit } used:</span>
                            {
                                !this.props.loadingTotalLeaveUnits ? (
                                    <ValueWrapper>
                                        { this.props.totalLeaveUnits } { parseFloat( this.props.totalLeaveUnits ) === 1 ? FILED_LEAVE_UNITS_SINGULAR[ this.state.filedLeave.unit ] : this.state.filedLeave.unit }
                                    </ValueWrapper>
                                ) : (
                                    <ValueWrapper>
                                        <i className="fa fa-circle-o-notch fa-spin fa-fw" aria-hidden="true" />
                                    </ValueWrapper>
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * Renders form
     */
    renderForm = () => (
        <div className={ this.props.loading ? 'hide' : '' }>
            <Container>
                <div className="row">
                    <div className="col-xs-12">
                        <p style={ { fontSize: '20px' } }>
                            <span><strong>{ this.props.filedLeave.employee_detail && this.props.filedLeave.employee_detail.full_name }</strong></span>
                            <span> ID: { this.props.filedLeave.employee_detail && this.props.filedLeave.employee_detail.employee_id }</span>
                        </p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        {
                            this.isProcessingStatus() ? (
                                <p>Status:<br />
                                Approving / Declining</p>
                            ) : (
                                <SalSelect
                                    id="status"
                                    label="Status"
                                    required
                                    key="status"
                                    data={ [ 'Approved', 'Declined', 'Pending', 'Cancelled' ].map( ( value ) => ({ label: value, value }) ) }
                                    value={ this.state.filedLeave.status }
                                    placeholder="Choose an option"
                                    ref={ ( ref ) => { this.status = ref; } }
                                    onChange={ ({ value }) => { this.updateFiledLeave( 'status', value ); } }
                                />
                            )
                        }
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-4">
                        <SalSelect
                            id="leave-type"
                            label="Leave Type"
                            required
                            key="leave-type"
                            data={ this.props.formOptions.leaveTypes }
                            value={ this.state.filedLeave.leave_type_id ? `${this.state.filedLeave.leave_type_id} ${this.state.filedLeave.unit}` : '' }
                            placeholder="Choose an option"
                            ref={ ( ref ) => { this.leave_type_id = ref; } }
                            onChange={ ( v ) => {
                                const data = v.value.split( ' ' );
                                this.handleLeaveTypeChange( data[ 0 ], data[ 1 ]);
                            } }
                        />
                    </div>
                    <div className="col-xs-4 date">
                        <DatePicker
                            label="Start Date"
                            placeholder="Select desired date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            selectedDay={ this.state.filedLeave.start_date }
                            required
                            ref={ ( ref ) => { this.start_date = ref; } }
                            onChange={ ( value ) => {
                                const selectedDay = formatDate( value, DATE_FORMATS.API );
                                this.updateFiledLeave( 'start_date', selectedDay );
                            } }
                        />
                    </div>
                    <div className="col-xs-4 date">
                        <DatePicker
                            label="End Date"
                            placeholder="Select desired date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            selectedDay={ this.state.filedLeave.end_date }
                            required
                            ref={ ( ref ) => { this.end_date = ref; } }
                            onChange={ ( value ) => {
                                const selectedDay = formatDate( value, DATE_FORMATS.API );
                                this.updateFiledLeave( 'end_date', selectedDay );
                            } }
                        />
                    </div>
                </div>
                { this.renderShiftSection() }
                { this.renderSummary() }
            </Container>
        </div>
    )

    render() {
        return (
            <PageWrapper>
                <Helmet
                    title="Edit Filed Leave"
                    meta={ [
                        { name: 'description', content: 'Add Filed Leaves' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                this.state.isProfile ?
                                browserHistory.push( `/employee/${this.state.filedLeave.employee_id}`, true ) :
                                browserHistory.push( '/time/leaves', true );
                            } }
                        >
                            &#8592; { this.state.isProfile ? 'Back to Profile' : 'Back to Filed Leaves' }
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <HeadingWrapper>
                        <h3>Edit Filed Leave</h3>
                    </HeadingWrapper>

                    <div className={ `loader ${this.props.loading ? '' : 'hide'}` }>
                        <Loader />
                    </div>
                    { !this.props.loading ? this.renderForm() : null }
                </Container>
                <Footer>
                    <Button
                        label="Cancel"
                        type="neutral"
                        size="large"
                        onClick={ () => {
                            this.state.isProfile ?
                                browserHistory.push( `/employee/${this.state.filedLeave.employee_id}`, true ) :
                                browserHistory.push( '/time/leaves', true );
                        } }
                    />
                    <Button
                        label={ this.props.submitted ? <Loader /> : 'Submit' }
                        type="action"
                        size="large"
                        disabled={ this.props.submitted || !this.areShiftsSelected() || this.isProcessingStatus() }
                        onClick={ this.submitFiledLeave }
                        ref={ ( ref ) => { this.submitButton = ref; } }
                    />
                </Footer>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    filedLeave: makeSelectFiledLeave(),
    formOptions: makeSelectFormOptions(),
    loading: makeSelectLoading(),
    submitted: makeSelectSubmitted(),
    loadingShifts: makeSelectLoadingShifts(),
    totalLeaveUnits: makeSelectTotalLeaveUnits(),
    loadingTotalLeaveUnits: makeSelectLoadingTotalLeaveUnits(),
    employeeRestDays: makeSelectEmployeeRestDays()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Details );
