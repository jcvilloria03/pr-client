import {
    INITIALIZE,
    SUBMIT_FORM,
    GET_EMPLOYEE_SHIFTS,
    GET_TOTAL_LEAVE_UNITS
} from './constants';

import { RESET_STORE } from '../../../App/constants';

/**
 * Initialize data
 */
export function initializeData( filedLeaveId, callback ) {
    return {
        type: INITIALIZE,
        payload: {
            filedLeaveId,
            callback
        }
    };
}

/**
 * Get total leave units
 * @param {Object} payload
 */
export function getTotalLeaveUnits( payload ) {
    return {
        type: GET_TOTAL_LEAVE_UNITS,
        payload
    };
}

/**
 * Get employee shifts
 * @param {String} startDate
 * @param {String} endDate
 * @param {String|Number} employeeId
 */
export function getEmployeeShifts( startDate, endDate, employeeId, callback ) {
    return {
        type: GET_EMPLOYEE_SHIFTS,
        payload: {
            startDate,
            endDate,
            employeeId,
            callback
        }
    };
}

/**
 * Submit a new filed leave
 */
export function submitFiledLeave( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
