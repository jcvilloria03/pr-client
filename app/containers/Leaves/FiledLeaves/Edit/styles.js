import styled from 'styled-components';

const PageWrapper = styled.div`
    .selected .fill {
        fill: #fff;
    }

    .selected .stroke {
        stroke: #fff;
    }

    .fill {
        fill: rgb(0,165,226);
    }

    .stroke {
        stroke: rgb(0,165,226);
    }

    .date {
        & > div > span {
            display: block;
        }
        .DayPickerInput {
            display: block;

            input {
                width: 100%;
            }
        }
    }

    .loader {
        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .hide {
        display: none;
    }

    .details {
        margin-top: 50px;
    }

    .details-row {
        margin-bottom: 50px;
    }

    .summary-panel {
        font-size: 14px;
        display: flex;
        padding: 25px;
        background-color: rgba(229, 246, 252, 1);
        border-radius: 5px;
        margin-bottom: 70px;

        &__column {
            flex-grow: 1;
            flex-basis: 0;
            padding: 10px;

            &:not(:last-child) {
                border-right: solid 2px rgb(216, 216, 216);
                margin-right: 40px
            }

            p {
                margin: 0;
            }
        }
    }

    .input-group-addon {
        background-color: #fff !important;
        border-radius: 0;
    }

    .input-group > .form-control {
        border-right: 0;
        z-index: 0;
    }
`;

const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;

    h3 {
        font-weight: 600;
    }

    p {
        text-align: center;
        max-width: 800px;
    }
`;

const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

const ValueWrapper = styled.div`
    padding-left: 12.5px;
`;

export {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    Footer,
    ValueWrapper
};
