import { fromJS } from 'immutable';
import {
    LOADING,
    SET_SUBMITTED,
    SET_ERRORS,
    SET_FILED_LEAVE,
    SET_FORM_OPTIONS,
    NOTIFICATION_SAGA,
    SET_LOADING_SHIFTS,
    SET_LOADING_TOTAL_LEAVE_UNITS,
    SET_TOTAL_LEAVE_UNITS,
    SET_EMPLOYEE_REST_DAYS
} from './constants';

import { RESET_STORE } from '../../../App/constants';

const initialState = fromJS({
    loading: true,
    filedLeave: {
        employee: {
            full_name: ''
        }
    },
    formOptions: {},
    submitted: false,
    loadingShifts: false,
    loadingTotalLeaveUnits: false,
    totalLeaveUnits: '0',
    errors: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    employeeRestDays: []
});

/**
 *
 * Manual entry leaveCredits reducer
 *
 */
function manualEntryLeaveCredits( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_FILED_LEAVE:
            return state.set( 'filedLeave', fromJS( action.payload ) );
        case SET_SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_LOADING_SHIFTS:
            return state.set( 'loadingShifts', action.payload );
        case SET_EMPLOYEE_REST_DAYS:
            return state.set( 'employeeRestDays', fromJS( action.payload ) );
        case SET_LOADING_TOTAL_LEAVE_UNITS:
            return state.set( 'loadingTotalLeaveUnits', action.payload );
        case SET_TOTAL_LEAVE_UNITS:
            return state.set( 'totalLeaveUnits', action.payload );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default manualEntryLeaveCredits;
