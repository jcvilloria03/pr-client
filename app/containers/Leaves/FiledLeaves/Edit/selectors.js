import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectFiledLeaveEditDomain = () => ( state ) => state.get( 'filedLeaveEdit' );

const makeSelectFiledLeave = () => createSelector(
    selectFiledLeaveEditDomain(),
    ( substate ) => substate.get( 'filedLeave' ).toJS()
);

const makeSelectFormOptions = () => createSelector(
    selectFiledLeaveEditDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectLoadingShifts = () => createSelector(
    selectFiledLeaveEditDomain(),
    ( substate ) => substate.get( 'loadingShifts' )
);

const makeSelectTotalLeaveUnits = () => createSelector(
    selectFiledLeaveEditDomain(),
    ( substate ) => substate.get( 'totalLeaveUnits' )
);

const makeSelectLoadingTotalLeaveUnits = () => createSelector(
    selectFiledLeaveEditDomain(),
    ( substate ) => substate.get( 'loadingTotalLeaveUnits' )
);

const makeSelectEmployeeRestDays = () => createSelector(
    selectFiledLeaveEditDomain(),
    ( substate ) => substate.get( 'employeeRestDays' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectFiledLeaveEditDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectSubmitted = () => createSelector(
    selectFiledLeaveEditDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
    selectFiledLeaveEditDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectFiledLeave,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectNotification,
    makeSelectLoadingShifts,
    makeSelectTotalLeaveUnits,
    makeSelectLoadingTotalLeaveUnits,
    makeSelectEmployeeRestDays
};
