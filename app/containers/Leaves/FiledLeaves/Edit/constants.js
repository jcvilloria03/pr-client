export const INITIALIZE = 'app/Leaves/FiledLeaves/Edit/INITIALIZE';
export const SET_FILED_LEAVE = 'app/Leaves/FiledLeaves/Edit/SET_FILED_LEAVE';
export const SET_FORM_OPTIONS = 'app/Leaves/FiledLeaves/Edit/SET_FORM_OTIONS';
export const SET_SUBMITTED = 'app/Leaves/FiledLeaves/Edit/SET_SUBMITTED';
export const SET_ERRORS = 'app/Leaves/FiledLeaves/Edit/SET_ERRORS';
export const SUBMIT_FORM = 'app/Leaves/FiledLeaves/Edit/SUBMIT_FORM';
export const NOTIFICATION_SAGA = 'app/Leaves/FiledLeaves/Edit/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Leaves/FiledLeaves/Edit/NOTIFICATION';
export const LOADING = 'app/Leaves/FiledLeaves/Edit/LOADING';

export const GET_EMPLOYEE_SHIFTS = 'app/Leaves/FiledLeaves/Edit/GET_EMPLOYEE_SHIFTS';
export const SET_LOADING_SHIFTS = 'app/Leaves/FiledLeaves/Edit/SET_LOADING_SHIFTS';

export const SET_LOADING_TOTAL_LEAVE_UNITS = 'app/Leaves/FiledLeaves/Edit/SET_LOADING_TOTAL_LEAVE_UNITS';
export const SET_TOTAL_LEAVE_UNITS = 'app/Leaves/FiledLeaves/Edit/SET_TOTAL_LEAVE_UNITS';
export const GET_TOTAL_LEAVE_UNITS = 'app/Leaves/FiledLeaves/Edit/GET_TOTAL_LEAVE_UNITS';

export const SET_EMPLOYEE_REST_DAYS = 'app/Leaves/FiledLeaves/Add/SET_EMPLOYEE_REST_DAYS';

export const LEAVE_UNITS = {
    DAYS: 'days',
    HOURS: 'hours'
};
