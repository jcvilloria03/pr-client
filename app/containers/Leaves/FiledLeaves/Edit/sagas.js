import some from 'lodash/some';
import decimal from 'js-big-decimal';
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { company } from '../../../../utils/CompanyService';
import { Fetch } from '../../../../utils/request';

import {
    INITIALIZE,
    SET_FILED_LEAVE,
    SET_FORM_OPTIONS,
    SET_SUBMITTED,
    SET_ERRORS,
    SUBMIT_FORM,
    LOADING,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    LEAVE_UNITS,
    SET_LOADING_SHIFTS,
    GET_EMPLOYEE_SHIFTS,
    SET_LOADING_TOTAL_LEAVE_UNITS,
    SET_TOTAL_LEAVE_UNITS,
    GET_TOTAL_LEAVE_UNITS,
    SET_EMPLOYEE_REST_DAYS
} from './constants';

import { REINITIALIZE_PAGE } from '../../../App/constants';
import { resetStore } from '../../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        const { filedLeaveId, callback } = payload;
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formOptions = {};

        const filedLeave = yield call( Fetch, `/leave_request/${filedLeaveId}`, { method: 'GET' });
        const schedules = yield call( Fetch, `/company/${companyId}/schedules`, { method: 'GET' });

        formOptions.leaveTypes = [];
        formOptions.schedules = schedules.data.map( ( value ) => ({ label: value.name, value: value.id }) );

        yield call( getEmployeeLeaveTypes, { payload: { formOptions, employeeId: filedLeave.employee_id }});
        yield call( getDefaultSchedule, { payload: { formOptions }});

        yield put({
            type: SET_FILED_LEAVE,
            payload: filedLeave
        });

        yield put({
            type: SET_TOTAL_LEAVE_UNITS,
            payload: filedLeave.total_value.toString()
        });
        yield call( getEmployeeShifts, { payload: { startDate: filedLeave.start_date, endDate: filedLeave.end_date, employeeId: filedLeave.employee_id, callback, initial: true }});
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Calculate total leave units
 */
export function* getTotalLeaveUnits({ payload }) {
    try {
        yield put({ type: SET_LOADING_TOTAL_LEAVE_UNITS, payload: true });

        const companyId = company.getLastActiveCompanyId();

        const response = yield call(
            Fetch,
            '/leave_request/admin/calculate_leaves_total_value',
            { method: 'POST', data: { ...payload, company_id: companyId }}
        );

        yield put({ type: SET_TOTAL_LEAVE_UNITS, payload: decimal.round( response.total_value, 2 ) });
    } catch ( error ) {
        yield put({ type: SET_TOTAL_LEAVE_UNITS, payload: '0' });
    } finally {
        yield put({ type: SET_LOADING_TOTAL_LEAVE_UNITS, payload: false });
    }
}

/**
 * Get employee leave types
 */
export function* getEmployeeLeaveTypes({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const { formOptions, employeeId } = payload;

        const leaveTypes = yield call( Fetch, `/company/${companyId}/leave_types`, { method: 'GET' });
        const leaveEntitlements = yield call( Fetch, `/company/${companyId}/leave_entitlements`, { method: 'GET' });

        leaveTypes.data.forEach( ( value ) => {
            const hasDaysUnit = some( value.leave_credits, { unit: LEAVE_UNITS.DAYS, employee_id: employeeId });
            const hasHoursUnit = some( value.leave_credits, { unit: LEAVE_UNITS.HOURS, employee_id: employeeId });

            hasHoursUnit && formOptions.leaveTypes.push( prepareLeaveType( value.name, value.id, LEAVE_UNITS.HOURS ) );
            hasDaysUnit && formOptions.leaveTypes.push( prepareLeaveType( value.name, value.id, LEAVE_UNITS.DAYS ) );

            if ( !hasDaysUnit && !hasHoursUnit ) {
                const entitlement = leaveEntitlements.data.find( ( leaveEntitlement ) => {
                    const hasLeaveEntitlementForLeaveType = some( leaveEntitlement.leave_types, { id: value.id });
                    const hasLeaveEntitlementForEmployee = some( leaveEntitlement.affected_employees, { id: employeeId });

                    return hasLeaveEntitlementForEmployee && hasLeaveEntitlementForLeaveType;
                });
                entitlement && formOptions.leaveTypes.push( prepareLeaveType( value.name, value.id, entitlement.leave_credit_unit ) );
            }
        });

        yield put({
            type: SET_FORM_OPTIONS,
            payload: {
                ...formOptions,
                leaveTypes: formOptions.leaveTypes
            }
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Prepare leave type for select dropdown
 * @param {String} name
 * @param {Number} id
 * @param {String} unit
 */
function prepareLeaveType( name, id, unit ) {
    return {
        label: `${name} - ${unit}`,
        value: `${id} ${unit}`,
        id
    };
}

/**
 * Get Employee Shifts
 */
export function* getEmployeeShifts({ payload }) {
    try {
        yield put({ type: SET_LOADING_SHIFTS, payload: true });
        const { startDate, endDate, employeeId } = payload;

        const companyId = company.getLastActiveCompanyId();
        const response = yield call(
            Fetch,
            `/company/${companyId}/employee/${employeeId}/shifts?start_date=${startDate}&end_date=${endDate}`,
            { method: 'GET' }
        );

        const restDays = yield call( Fetch, `/employee/${employeeId}/rest_days?company_id=${companyId}`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE_REST_DAYS,
            payload: restDays.data
        });

        payload.callback( response.data, payload.initial );
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({ type: SET_LOADING_SHIFTS, payload: false });
    }
}

/**
 * Get default company schedule
 */
export function* getDefaultSchedule({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const defaultSchedule = yield call( Fetch, `/default_schedule?company_id=${companyId}`, { method: 'GET' });

        yield put({
            type: SET_FORM_OPTIONS,
            payload: {
                ...payload.formOptions,
                defaultSchedule: defaultSchedule.data
            }
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SET_SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];
        const data = { ...payload, company_id: company.getLastActiveCompanyId() };
        yield call( Fetch, `/leave_request/${payload.id}/admin`, { method: 'PUT', data });
        yield call( delay, 500 );

        if ( payload.isProfile ) {
            yield call( browserHistory.push, `/employee/${payload.employee_id}`, true );
        } else {
            yield call( browserHistory.push, '/time/leaves', true );
        }
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SET_SUBMITTED,
            payload: false
        });
    }
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_EMPLOYEE_SHIFTS
 */
export function* watchForGetEmployeeShifts() {
    const watcher = yield takeEvery( GET_EMPLOYEE_SHIFTS, getEmployeeShifts );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_TOTAL_LEAVE_UNITS
 */
export function* watchForGetTotalLeaveUnits() {
    const watcher = yield takeEvery( GET_TOTAL_LEAVE_UNITS, getTotalLeaveUnits );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForInitializeData,
    watchForReinitializePage,
    watchNotify,
    watchForGetEmployeeShifts,
    watchForGetTotalLeaveUnits
];
