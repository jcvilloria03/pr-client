import {
    INITIALIZE
} from './constants';

import { RESET_STORE } from '../../../App/constants';

/**
 * Initialize data
 */
export function initializeData( filedLeaveId, callback ) {
    return {
        type: INITIALIZE,
        payload: {
            filedLeaveId,
            callback
        }
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
