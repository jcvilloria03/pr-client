export const INITIALIZE = 'app/Leaves/FiledLeaves/Details/INITIALIZE';
export const SET_FILED_LEAVE = 'app/Leaves/FiledLeaves/Details/SET_FILED_LEAVE';
export const NOTIFICATION_SAGA = 'app/Leaves/FiledLeaves/Details/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Leaves/FiledLeaves/Details/NOTIFICATION';
export const LOADING = 'app/Leaves/FiledLeaves/Details/LOADING';
