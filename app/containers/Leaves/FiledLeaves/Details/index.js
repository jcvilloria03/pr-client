import React, { Component } from 'react';
import each from 'lodash/each';
import groupBy from 'lodash/groupBy';

import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../../components/A';
import Button from '../../../../components/Button';
import SnackBar from '../../../../components/SnackBar';
import SubHeader from '../../../../containers/SubHeader';
import Loader from '../../../../components/Loader';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { isAuthorized } from '../../../../utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../../utils/constants';
import { subscriptionService } from '../../../../utils/SubscriptionService';
import { formatDate } from '../../../../utils/functions';

import * as actions from './actions';

import {
    makeSelectFiledLeave,
    makeSelectLoading,
    makeSelectNotification
} from './selectors';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    Footer,
    ValueWrapper
} from './styles';

/**
 * Filed Leave Details
 */
class Details extends Component {
    static propTypes = {
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        filedLeave: React.PropTypes.object,
        loading: React.PropTypes.bool,
        params: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        products: React.PropTypes.array
    };

    constructor( props ) {
        super( props );
        this.state = {
            employeeLeaves: {}
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['view.leave_request'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        const { id: filedLeaveId } = this.props.params;
        this.props.initializeData( filedLeaveId, this.prepareShiftData );
    }

    /**
     * Remaps shift data into an object which has dates as keys
     * @param {Array} employeeShifts
     */
    prepareShiftData = ( employeeShifts ) => {
        let leaves = [];

        each( employeeShifts, ( shift ) => {
            const preparedData = [{
                schedule_name: shift.schedule ? shift.schedule.name : 'Default Schedule',
                date: shift.date,
                start_time: shift.start_time,
                end_time: shift.end_time
            }];

            leaves = [ ...leaves, ...preparedData ];
        });

        leaves = groupBy( leaves, 'date' );

        leaves = Object.keys( leaves )
            .sort()
            .reduce( ( accumulator, current ) => ( accumulator[ current ]  = leaves[ current ], accumulator ), {}); // eslint-disable-line

        this.setState({ employeeLeaves: { ...leaves }});
    }

    /**
     * Renders shift list
     */
    renderShiftList = () => {
        const { employeeLeaves: leaves } = this.state;

        return Object.keys( leaves ).map( ( date, index ) => (
            <div key={ index }>
                <div>
                    <div className="row" style={ { paddingTop: '20px' } }>
                        <div className="col-xs-4" style={ { marginBottom: '10px' } }>
                            <strong>
                                { formatDate( date, DATE_FORMATS.DISPLAY ) } Shift
                            </strong>
                        </div>
                    </div>
                    {
                        leaves[ date ].map( ( leave, leaveIndex ) => (
                            <div className="row" key={ `${index}_${leaveIndex}` } style={ { paddingTop: '20px' } }>
                                <div className="col-xs-3">
                                    <ValueWrapper>
                                        <div><strong>Shift Name</strong></div>
                                        <ValueWrapper>
                                            <div style={ { marginTop: '10px' } }>
                                                { leave.schedule_name ? leave.schedule_name : 'Default Schedule' }
                                            </div>
                                        </ValueWrapper>
                                    </ValueWrapper>
                                </div>
                                <div className="col-xs-3">
                                    <ValueWrapper>
                                        <div><strong>Leave Start Time</strong></div>
                                        <ValueWrapper>
                                            <div style={ { marginTop: '10px' } }>
                                                { leave.start_time }
                                            </div>
                                        </ValueWrapper>
                                    </ValueWrapper>
                                </div>
                                <div className="col-xs-3">
                                    <ValueWrapper>
                                        <div><strong>Leave End Time</strong></div>
                                        <ValueWrapper>
                                            <div style={ { marginTop: '10px' } }>
                                                { leave.end_time }
                                            </div>
                                        </ValueWrapper>
                                    </ValueWrapper>
                                </div>
                            </div>
                        ) )
                    }
                </div>
            </div>
        ) );
    }

    /**
     * Component Render Method
     */
    render() {
        return (
            <PageWrapper>
                <Helmet
                    title="Filed Leave Details"
                    meta={ [
                        { name: 'description', content: 'Add Filed Leaves' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/leaves', true );
                            } }
                        >
                            &#8592; Back to Filed Leaves
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <HeadingWrapper>
                        <h3>Filed Leave Details</h3>
                    </HeadingWrapper>

                    <div className={ `loader ${this.props.loading ? '' : 'hide'}` }>
                        <Loader />
                    </div>

                    { !this.props.loading && (
                        <div className="details">
                            <div className="row details-row">
                                <div className="col-xs-3">
                                    <p><strong>Employee Name</strong></p>
                                    <ValueWrapper>
                                        { this.props.filedLeave.employee.full_name }
                                    </ValueWrapper>
                                </div>
                                <div className="col-xs-3">
                                    <p><strong>Employee ID</strong></p>
                                    <ValueWrapper>
                                        { this.props.filedLeave.employee.employee_id }
                                    </ValueWrapper>
                                </div>
                                <div className="col-xs-3">
                                    <p><strong>Status</strong></p>
                                    <ValueWrapper>
                                        { this.props.filedLeave.status }
                                    </ValueWrapper>
                                </div>
                            </div>

                            <div className="row details-row">
                                <div className="col-xs-3">
                                    <p><strong>Leave Type</strong></p>
                                    <ValueWrapper>
                                        { this.props.filedLeave.leave_type.name }
                                    </ValueWrapper>
                                </div>
                                <div className="col-xs-3">
                                    <p><strong>Number of Units</strong></p>
                                    <ValueWrapper>
                                        { this.props.filedLeave.total_value }
                                        <div style={ { color: '#474747', marginTop: '5px' } }>Computed in { this.props.filedLeave.unit.toUpperCase() }</div>
                                    </ValueWrapper>
                                </div>
                                <div className="col-xs-3">
                                    <p><strong>Start Date</strong></p>
                                    <ValueWrapper>
                                        { formatDate( this.props.filedLeave.start_date, DATE_FORMATS.DISPLAY ) }
                                    </ValueWrapper>
                                </div>
                                <div className="col-xs-3">
                                    <p><strong>End Date</strong></p>
                                    <ValueWrapper>
                                        { formatDate( this.props.filedLeave.end_date, DATE_FORMATS.DISPLAY ) }
                                    </ValueWrapper>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-3">
                                    <p><strong>Shift Details</strong></p>
                                </div>
                            </div>
                            <div>
                                <ValueWrapper>{ this.renderShiftList() }</ValueWrapper>
                            </div>
                        </div>
                    )}
                </Container>
                <Footer>
                    <Button
                        label="Close"
                        type="neutral"
                        size="large"
                        onClick={ () => {
                            browserHistory.push( '/time/leaves', true );
                        } }
                    />
                    <Button
                        label="Edit"
                        type="action"
                        size="large"
                        onClick={ () => {
                            browserHistory.push( `/time/filed-leaves/${this.props.params.id}/edit` );
                        } }
                    />
                </Footer>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    filedLeave: makeSelectFiledLeave(),
    loading: makeSelectLoading()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( Details );
