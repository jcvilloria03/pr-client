import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectFiledLeaveDetailsDomain = () => ( state ) => state.get( 'filedLeaveDetails' );

const makeSelectFiledLeave = () => createSelector(
    selectFiledLeaveDetailsDomain(),
    ( substate ) => substate.get( 'filedLeave' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectFiledLeaveDetailsDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectFiledLeaveDetailsDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectFiledLeave,
    makeSelectLoading,
    makeSelectNotification
};
