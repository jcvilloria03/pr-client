import { fromJS } from 'immutable';
import {
    LOADING,
    SET_FILED_LEAVE,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../../App/constants';

const initialState = fromJS({
    loading: true,
    filedLeave: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Manual entry leaveCredits reducer
 *
 */
function manualEntryLeaveCredits( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_FILED_LEAVE:
            return state.set( 'filedLeave', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default manualEntryLeaveCredits;
