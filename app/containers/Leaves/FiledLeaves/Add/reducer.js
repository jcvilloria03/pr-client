import { fromJS } from 'immutable';
import {
    LOADING,
    SET_FORM_OPTIONS,
    SET_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    RESET_FILED_LEAVES_PREVIEW,
    SET_FILED_LEAVES_PREVIEW_STATUS,
    SET_FILED_LEAVES_PREVIEW_DATA,
    SET_LOADING_SHIFTS,
    SET_SAVING_STATUS,
    SET_SAVING_ERRORS,
    SUBMITTED,
    NOTIFICATION_SAGA,
    SET_LOADING_TOTAL_LEAVE_UNITS,
    SET_TOTAL_LEAVE_UNITS,
    SET_EMPLOYEE_REST_DAYS
} from './constants';

import { RESET_STORE } from '../../../App/constants';

const initialState = fromJS({
    loading: true,
    errors: {},
    submitted: false,
    batchUploadJobId: null,
    batchUploadStatus: '',
    batchUploadErrors: {},
    filedLeavesPreview: {
        status: 'ready',
        data: []
    },
    loadingShifts: false,
    loadingTotalLeaveUnits: false,
    totalLeaveUnits: '0',
    saving: {
        status: '',
        errors: {}
    },
    formOptions: {
        leaveTypes: [],
        employees: []
    },
    employeeRestDays: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Manual entry leaveCredits reducer
 *
 */
function manualEntryLeaveCredits( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_BATCH_UPLOAD_JOB_ID:
            return state.set( 'batchUploadJobId', action.payload );
        case SET_BATCH_UPLOAD_STATUS:
            return state.set( 'batchUploadStatus', action.payload );
        case SET_BATCH_UPLOAD_ERRORS:
            return state.set( 'batchUploadErrors', action.payload );
        case SET_FILED_LEAVES_PREVIEW_STATUS:
            return state.setIn([ 'filedLeavesPreview', 'status' ], action.payload );
        case SET_FILED_LEAVES_PREVIEW_DATA:
            return state.setIn([ 'filedLeavesPreview', 'data' ], fromJS( action.payload ) );
        case SET_EMPLOYEE_REST_DAYS:
            return state.set( 'employeeRestDays', fromJS( action.payload ) );
        case SET_LOADING_SHIFTS:
            return state.set( 'loadingShifts', action.payload );
        case SET_LOADING_TOTAL_LEAVE_UNITS:
            return state.set( 'loadingTotalLeaveUnits', action.payload );
        case SET_TOTAL_LEAVE_UNITS:
            return state.set( 'totalLeaveUnits', action.payload );
        case SET_SAVING_STATUS:
            return state.setIn([ 'saving', 'status' ], action.payload );
        case SET_SAVING_ERRORS:
            return state.setIn([ 'saving', 'errors' ], action.payload );
        case RESET_FILED_LEAVES_PREVIEW:
            return state.set( 'preview', fromJS({
                status: 'ready',
                data: []
            }) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default manualEntryLeaveCredits;
