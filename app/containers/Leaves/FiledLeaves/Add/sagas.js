import flatMap from 'lodash/flatMap';
import decimal from 'js-big-decimal';
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { company } from '../../../../utils/CompanyService';
import { Fetch } from '../../../../utils/request';

import {
    INITIALIZE,
    LOADING,
    SUBMIT_FORM,
    SUBMITTED,
    UPLOAD_FILED_LEAVES,
    SET_ERRORS,
    SET_FORM_OPTIONS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    SET_SAVING_STATUS,
    SET_SAVING_ERRORS,
    SAVE_FILED_LEAVES,
    GET_FILED_LEAVES_PREVIEW,
    SET_FILED_LEAVES_PREVIEW_STATUS,
    SET_FILED_LEAVES_PREVIEW_DATA,
    GET_EMPLOYEE_SHIFTS,
    SET_LOADING_SHIFTS,
    SET_LOADING_TOTAL_LEAVE_UNITS,
    SET_TOTAL_LEAVE_UNITS,
    GET_EMPLOYEE_LEAVE_TYPES,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    GET_TOTAL_LEAVE_UNITS,
    SET_EMPLOYEE_REST_DAYS
} from './constants';

import { REINITIALIZE_PAGE } from '../../../App/constants';
import { resetStore } from '../../../App/sagas';

/**
 * Initialize data
 * @param {Integer} [payload] - Employee ID if coming from employee profile page
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formOptions = {};

        const [ employees, schedules, leaveTypes ] = yield [
            call( Fetch, `/company/${companyId}/employees`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/schedules`, { method: 'GET' }),
            payload && call( Fetch, `/company/${companyId}/employee/${payload}/leave_types`, { method: 'GET' })
        ];

        formOptions.leaveTypes = leaveTypes ?
            flatMap( leaveTypes.data, ( leaveType ) =>
                mapSelectableLeaveTypes( leaveType.selectable_leave_types )
            ) : [];

        formOptions.employees = employees.data.map( ( value ) => ({
            label: `${value.employee_id} - ${value.first_name} ${value.last_name}`,
            value: value.id,
            fullName: `${value.first_name} ${value.last_name}`,
            id: `${value.employee_id}`,
            dateHired: value.date_hired,
            dateEnded: value.date_ended,
            department_id: value.department_id,
            position_id: value.position_id,
            location_id: value.position_id
        }) );
        formOptions.schedules = schedules.data.map( ( value ) => ({ label: value.name, value: value.id }) );
        yield call( getDefaultSchedule, { payload: { formOptions }});
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Calculate total leave units
 */
export function* getTotalLeaveUnits({ payload }) {
    try {
        yield put({ type: SET_LOADING_TOTAL_LEAVE_UNITS, payload: true });

        const companyId = company.getLastActiveCompanyId();

        const response = yield call(
            Fetch,
            '/leave_request/admin/calculate_leaves_total_value',
            { method: 'POST', data: { ...payload, company_id: companyId }}
        );

        yield put({ type: SET_TOTAL_LEAVE_UNITS, payload: decimal.round( response.total_value, 2 ) });
    } catch ( error ) {
        yield put({ type: SET_TOTAL_LEAVE_UNITS, payload: '0' });
    } finally {
        yield put({ type: SET_LOADING_TOTAL_LEAVE_UNITS, payload: false });
    }
}

/**
 * Get default company schedule
 */
export function* getDefaultSchedule({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const defaultSchedule = yield call( Fetch, `/default_schedule?company_id=${companyId}`, { method: 'GET' });

        yield put({
            type: SET_FORM_OPTIONS,
            payload: {
                ...payload.formOptions,
                defaultSchedule: defaultSchedule.data
            }
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Get employee leave types
 */
export function* getEmployeeLeaveTypes({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const { formOptions, employeeId } = payload;

        formOptions.leaveTypes = [];

        const leaveTypes = yield call( Fetch, `/company/${companyId}/employee/${employeeId}/leave_types`, { method: 'GET' });

        formOptions.leaveTypes = flatMap( leaveTypes.data, ( leaveType ) =>
            mapSelectableLeaveTypes( leaveType.selectable_leave_types )
        );

        yield put({
            type: SET_FORM_OPTIONS,
            payload: {
                ...formOptions,
                leaveTypes: formOptions.leaveTypes
            }
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Maps selectable leave types for select dropdown.
 *
 * @param {Array} selectableLeaveTypes
 * @return {Array}
 */
function mapSelectableLeaveTypes( selectableLeaveTypes ) {
    return flatMap( selectableLeaveTypes, ( selectableLeaveType ) =>
        [{
            label: selectableLeaveType.text,
            value: selectableLeaveType.value.replace( / - /gi, ' ' ),
            id: selectableLeaveType.id
        }]
    );
}

/**
 * Get Employee Shifts
 */
export function* getEmployeeShifts({ payload }) {
    try {
        yield put({ type: SET_LOADING_SHIFTS, payload: true });
        const { startDate, endDate, employeeId } = payload;

        const companyId = company.getLastActiveCompanyId();
        const response = yield call(
            Fetch,
            `/company/${companyId}/employee/${employeeId}/shifts?start_date=${startDate}&end_date=${endDate}`,
            { method: 'GET' }
        );

        const restDays = yield call( Fetch, `/employee/${employeeId}/rest_days?company_id=${companyId}`, { method: 'GET' });

        yield put({
            type: SET_EMPLOYEE_REST_DAYS,
            payload: restDays.data
        });

        payload.callback( response.data );
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({ type: SET_LOADING_SHIFTS, payload: false });
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];
        const data = { ...payload, company_id: company.getLastActiveCompanyId() };
        yield call( Fetch, '/leave_request/admin', { method: 'POST', data });
        yield call( delay, 500 );

        if ( payload.isEmployeeIdPrefiled ) {
            yield call( browserHistory.push, `/employee/${payload.employee_id}`, true );
        } else {
            yield call( browserHistory.push, '/time/leaves', true );
        }
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Uploads the CSV of filed leaves to add and starts the validation process
 */
export function* uploadFiledLeaves({ payload }) {
    try {
        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: {}
        });

        const data = new FormData();

        data.append( 'company_id', company.getLastActiveCompanyId() );
        data.append( 'file', payload.file );

        const upload = yield call( Fetch, '/leave_request/upload', {
            method: 'POST',
            data
        });

        if ( upload.id ) {
            yield put({
                type: SET_BATCH_UPLOAD_JOB_ID,
                payload: upload.id
            });

            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: 'validation_queued'
            });

            yield call( checkValidation, { payload: { jobId: upload.id, step: 'validation' }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const check = yield call(
            Fetch,
            `/leave_request/upload/status?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}&step=${payload.step}`,
            { method: 'GET' }
        );

        if ( payload.step === 'validation' ) {
            if ( check.status === 'validation_failed' ) {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_FILED_LEAVES_PREVIEW_DATA,
                    payload: []
                });

                yield put({
                    type: SET_BATCH_UPLOAD_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'validated' ) {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_BATCH_UPLOAD_ERRORS,
                    payload: {}
                });

                yield call( getFiledLeavesPreview, { payload: { jobId: payload.jobId }});
            } else {
                yield put({
                    type: SET_BATCH_UPLOAD_STATUS,
                    payload: check.status
                });

                yield call( delay, 2000 );
                yield call( checkValidation, { payload: { jobId: payload.jobId, step: 'validation' }});
            }
        } else if ( payload.step === 'save' ) {
            if ( check.status === 'save_failed' ) {
                yield put({
                    type: SET_SAVING_STATUS,
                    payload: check.status
                });

                yield put({
                    type: SET_SAVING_ERRORS,
                    payload: check.errors
                });
            } else if ( check.status === 'saved' ) {
                yield put({
                    type: SET_SAVING_STATUS,
                    payload: check.status
                });

                browserHistory.push( '/time/leaves', true );
            } else {
                yield call( delay, 2000 );
                yield call( checkValidation, { payload });
            }
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Fetches preview for uploaded filed leaves
 */
export function* getFiledLeavesPreview({ payload }) {
    try {
        yield put({
            type: SET_FILED_LEAVES_PREVIEW_DATA,
            payload: []
        });

        yield put({
            type: SET_FILED_LEAVES_PREVIEW_STATUS,
            payload: 'busy'
        });

        const preview = yield call(
            Fetch,
            `/leave_request/upload/preview?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}`,
            { method: 'GET' }
        );

        yield put({
            type: SET_FILED_LEAVES_PREVIEW_DATA,
            payload: preview
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_FILED_LEAVES_PREVIEW_STATUS,
            payload: 'ready'
        });
    }
}

/**
 * Saves validated filed leaves
 */
export function* saveFiledLeaves({ payload }) {
    try {
        const upload = yield call( Fetch, '/leave_request/upload/save', {
            method: 'POST',
            data: {
                company_id: company.getLastActiveCompanyId(),
                job_id: payload.jobId
            }
        });

        if ( upload.id ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: 'save_queued'
            });

            yield call( checkValidation, { payload: {
                step: 'save',
                jobId: payload.jobId
            }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_EMPLOYEE_SHIFTS
 */
export function* watchForGetEmployeeShifts() {
    const watcher = yield takeEvery( GET_EMPLOYEE_SHIFTS, getEmployeeShifts );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_TOTAL_LEAVE_UNITS
 */
export function* watchForGetTotalLeaveUnits() {
    const watcher = yield takeEvery( GET_TOTAL_LEAVE_UNITS, getTotalLeaveUnits );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_EMPLOYEE_LEAVE_TYPES
 */
export function* watchForGetEmployeeLeaveTypes() {
    const watcher = yield takeEvery( GET_EMPLOYEE_LEAVE_TYPES, getEmployeeLeaveTypes );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPLOAD_FILED_LEAVES
 */
export function* watchForUploadFiledLeaves() {
    const watcher = yield takeEvery( UPLOAD_FILED_LEAVES, uploadFiledLeaves );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_FILED_LEAVES_PREVIEW
 */
export function* watchForGetFiledLeavesPreview() {
    const watcher = yield takeEvery( GET_FILED_LEAVES_PREVIEW, getFiledLeavesPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SAVE_FILED_LEAVES
 */
export function* watchForSaveFiledLeaves() {
    const watcher = yield takeEvery( SAVE_FILED_LEAVES, saveFiledLeaves );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForGetFiledLeavesPreview,
    watchForUploadFiledLeaves,
    watchForInitializeData,
    watchForReinitializePage,
    watchForSaveFiledLeaves,
    watchForGetEmployeeShifts,
    watchForGetEmployeeLeaveTypes,
    watchForGetTotalLeaveUnits,
    watchNotify
];
