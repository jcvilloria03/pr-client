import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectAddFiledLeaveDomain = () => ( state ) => state.get( 'addFiledLeaves' );

const makeSelectFiledLeave = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'filedLeave' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectGenerating = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'generating' )
);

const makeSelectFormOptions = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectEmployeeRestDays = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'employeeRestDays' ).toJS()
);

const makeSelectLoadingShifts = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'loadingShifts' )
);

const makeSelectSubmitForm = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'submit' ).toJS()
);

const makeSelectTotalLeaveUnits = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'totalLeaveUnits' )
);

const makeSelectLoadingTotalLeaveUnits = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'loadingTotalLeaveUnits' )
);

const makeSelectSubmitted = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectFiledLeavesPreview = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'filedLeavesPreview' ).toJS()
);

const makeSelectSaving = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectAddFiledLeaveDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectFiledLeave,
    makeSelectSubmitForm,
    makeSelectSubmitted,
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectFiledLeavesPreview,
    makeSelectSaving,
    makeSelectGenerating,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectLoadingShifts,
    makeSelectTotalLeaveUnits,
    makeSelectLoadingTotalLeaveUnits,
    makeSelectEmployeeRestDays
};
