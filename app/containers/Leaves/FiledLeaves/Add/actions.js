import {
    INITIALIZE,
    SUBMIT_FORM,
    UPLOAD_FILED_LEAVES,
    SAVE_FILED_LEAVES,
    GET_EMPLOYEE_SHIFTS,
    GET_EMPLOYEE_LEAVE_TYPES,
    NOTIFICATION,
    GET_TOTAL_LEAVE_UNITS
} from './constants';

import { RESET_STORE } from '../../../App/constants';

/**
 * Initialize data
 * @param {Integer} [id] - Employee ID if coming from employee profile page
 */
export function initializeData( id ) {
    return {
        type: INITIALIZE,
        payload: id
    };
}

/**
 * Get employee shifts
 * @param {String} startDate
 * @param {String} endDate
 * @param {String|Number} employeeId
 */
export function getEmployeeShifts( startDate, endDate, employeeId, callback ) {
    return {
        type: GET_EMPLOYEE_SHIFTS,
        payload: {
            startDate,
            endDate,
            employeeId,
            callback
        }
    };
}

/**
 * Get employee leave types
 * @param {String|Number} employeeId
 */
export function getEmployeeLeaveTypes( employeeId, formOptions ) {
    return {
        type: GET_EMPLOYEE_LEAVE_TYPES,
        payload: {
            employeeId,
            formOptions
        }
    };
}

/**
 * Get total leave units
 * @param {Object} payload
 */
export function getTotalLeaveUnits( payload ) {
    return {
        type: GET_TOTAL_LEAVE_UNITS,
        payload
    };
}

/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Submit a new filed leave
 */
export function submitFiledLeave( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}

/**
 * Upload filed leaves
 */
export function uploadFiledLeaves( payload ) {
    return {
        type: UPLOAD_FILED_LEAVES,
        payload
    };
}

/**
 * Save uploaded filed leaves
 */
export function saveFiledLeaves( payload ) {
    return {
        type: SAVE_FILED_LEAVES,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
