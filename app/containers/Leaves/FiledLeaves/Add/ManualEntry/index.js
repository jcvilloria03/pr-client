import React from 'react';

import find from 'lodash/find';
import each from 'lodash/each';
import map from 'lodash/map';
import groupBy from 'lodash/groupBy';
import filter from 'lodash/filter';
import debounce from 'lodash/debounce';

import moment from 'moment';
import { extendMoment } from 'moment-range';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import SnackBar from '../../../../../components/SnackBar';
import SalConfirm from '../../../../../components/SalConfirm';
import Loader from '../../../../../components/Loader';
import Button from '../../../../../components/Button';
import MultiSelect from '../../../../../components/MultiSelect';
import SalSelect from '../../../../../components/Select';
import Icon from '../../../../../components/Icon';
import Input from '../../../../../components/Input';
import DatePicker from '../../../../../components/DatePicker';

import { browserHistory } from '../../../../../utils/BrowserHistory';
import { isAuthorized } from '../../../../../utils/Authorization';
import { restDayService } from '../../../../../utils/RestDayService';
import {
    stripTime,
    formatDate,
    isSameOrAfter,
    getISOWeekday
} from '../../../../../utils/functions';

import {
    DATE_FORMATS,
    FILED_LEAVE_UNITS_SINGULAR,
    DAY_TYPES
} from '../../../../../utils/constants';

import {
    MainWrapper,
    ConfirmBodyWrapperStyle,
    Footer,
    ValueWrapper
} from './styles';

import {
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectLoading,
    makeSelectLoadingShifts,
    makeSelectTotalLeaveUnits,
    makeSelectLoadingTotalLeaveUnits,
    makeSelectEmployeeRestDays
} from '../selectors';

import * as createFiledLeaveActions from '../actions';

const inputTypes = {
    select: [ 'employee', 'status', 'leave_type_id', 'schedule_id' ],
    datePicker: [ 'start_date', 'end_date' ]
};

const REQUIRED_KEYS_FOR_EMPLOYEE_SHIFTS = [ 'start_date', 'end_date', 'employee_id' ];

/**
 * ManualEntry Container
 */
export class ManualEntry extends React.Component {
    static propTypes = {
        initializeData: React.PropTypes.func,
        formOptions: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitFiledLeave: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        errors: React.PropTypes.object,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        employeeId: React.PropTypes.string,
        getEmployeeShifts: React.PropTypes.func,
        loadingShifts: React.PropTypes.bool,
        getEmployeeLeaveTypes: React.PropTypes.func,
        totalLeaveUnits: React.PropTypes.string,
        loadingTotalLeaveUnits: React.PropTypes.bool,
        getTotalLeaveUnits: React.PropTypes.func,
        employeeRestDays: React.PropTypes.array
    }

    static defaultProps = {
        loading: true,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            filedLeave: {
                employee_id: '',
                leave_type_id: '',
                status: '',
                start_date: '',
                end_date: '',
                unit: ''
            },
            employeeLeaves: {},
            showModal: false,
            errors: {}
        };

        this.employee = null;
    }

    componentWillMount() {
        isAuthorized(['create.leave_request'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        this.props.initializeData( this.props.employeeId );
    }

    componentDidMount() {
        if ( this.props.employeeId ) {
            this.updateFiledLeave( 'employee_id', parseInt( this.props.employeeId, 10 ) );
        }
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });

        if ( nextProps.formOptions.employees.length && this.props.employeeId ) {
            this.employee = this.getEmployee();
        }
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    getEmployee() {
        if ( this.props.formOptions.employees.length ) {
            const foundEmployee =
                find( this.props.formOptions.employees, ( employee ) => employee.value === parseInt( this.props.employeeId, 10 ) );
            return foundEmployee;
        }

        return '';
    }

    getEmployeeById = () => {
        if ( this.props.formOptions.employees.length && this.state.filedLeave.employee_id ) {
            return find( this.props.formOptions.employees, ( employee ) => employee.value === parseInt( this.state.filedLeave.employee_id, 10 ) );
        }

        return '';
    }

    /**
     * Get total leave units
     */
    getTotalLeaveUnits = debounce( () => {
        const { unit, employee_id: employeeId } = this.state.filedLeave;

        const leaves = this.prepareFiledLeavePayload();

        this.validateTimeInputs();

        const payload = {
            leaves,
            employee_id: employeeId,
            unit
        };
        this.props.getTotalLeaveUnits( payload );
    }, 1000 )

    /**
     * Formats time string to moment object
     * @param {...String} dates
     *
     * @returns {Array}
     */
    getFormattedTime = ( ...dates ) => (
        dates.map( ( date ) => moment( date, DATE_FORMATS.TIME ) )
    )

    /**
     * Get range of dates that are qualified for applying for a filed leave
     *
     * @returns {Array}
     */
    getShiftRange = () => {
        const { start_date: startDate, end_date: endDate } = this.state.filedLeave;
        const { dateHired, dateEnded } = this.getEmployeeById();

        const range = extendMoment( moment ).range( startDate, endDate );

        return Array.from( range.by( 'days' ) )
            .map( ( day ) => formatDate( day, DATE_FORMATS.API ) )
            .filter( ( date ) => (
                dateEnded ?
                    moment( date ).isAfter( dateHired ) && moment( date ).isBefore( dateEnded ) :
                    moment( date ).isAfter( dateHired ) && moment( date )
            ) );
    }

    /**
     * Sets error message on desired input
     * @param {String} field
     * @param {String} errorMessage
     */
    setInputErrorMessage = ( field, errorMessage ) => {
        this[ field ].setState({
            error: true,
            errorMessage
        });
    }

    /**
     * Resets error message on desired input
     * @param {String} field
     */
    resetInputErrorMessage = ( field ) => {
        this[ field ].setState({
            error: false,
            errorMessage: '&nbsp'
        });
    }

    /**
     * Display api errors underneath appropriate fields
     */
    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.datePicker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    updateFiledLeave( key, value ) {
        if ( typeof value === 'undefined' ) {
            return;
        }

        const dataClone = Object.assign({}, this.state.filedLeave );
        dataClone[ key ] = value !== '' && value !== null ? value : '';

        this.setState({ filedLeave: Object.assign( this.state.filedLeave, dataClone ) }, () => {
            this.fetchEmployeeShifts( key );
        });
    }

    /**
     * Fetch employee shifts in case we have enough information
     * @param {String} field
     */
    fetchEmployeeShifts = ( field ) => {
        const { start_date: startDate, end_date: endDate, employee_id: employeeId } = this.state.filedLeave;
        if ( REQUIRED_KEYS_FOR_EMPLOYEE_SHIFTS.includes( field ) && ( startDate && endDate && employeeId ) ) {
            const valid = this.validateEndDate();
            valid && this.props.getEmployeeShifts( startDate, endDate, employeeId, this.prepareShiftData );
        }
    }

    /**
     * Checks if EmployeeId is in props and field is employee_id
     */
    checkEmployeeIdPropAndField( field ) {
        return this.props.employeeId && field === 'employee_id';
    }

    validateForm() {
        let valid = true;

        if ( !this.props.employeeId && !this.employee_id._checkRequire( this.employee_id.state.value ) ) {
            valid = false;
        }

        if ( !this.status._checkRequire( this.status.state.value ) ) {
            valid = false;
        }

        if ( !this.leave_type_id._checkRequire( this.leave_type_id.state.value ) ) {
            valid = false;
        }

        if ( this.start_date.checkRequired() ) {
            valid = false;
        }

        if ( this.end_date.checkRequired() ) {
            valid = false;
        }

        valid = this.validateEndDate( valid );

        valid = this.validateTimeInputs( valid );

        return valid;
    }

    validateEndDate = ( formValid = true ) => {
        let valid = formValid;
        if ( this.state.filedLeave.start_date
            && this.state.filedLeave.end_date
            && !isSameOrAfter( this.state.filedLeave.end_date, this.state.filedLeave.start_date, [DATE_FORMATS.API])
        ) {
            const message = 'The end date must be a date after start date.';
            this.end_date.setState({ error: true, message });

            valid = false;
        }

        return valid;
    }

    validateTimeInputs = ( formValid = true ) => {
        let valid = formValid;
        Object.keys( this.state.employeeLeaves ).forEach( ( date, index ) => {
            this.state.employeeLeaves[ date ].forEach( ( leave, leaveIndex ) => {
                const { start_time: startTime, end_time: endTime } = leave;
                if ( startTime && !endTime ) {
                    this.setInputErrorMessage( `end_time_${index}_${leaveIndex}`, 'End time must be present if start time is present' );
                    valid = false;
                } else if ( !startTime && endTime ) {
                    this.setInputErrorMessage( `start_time_${index}_${leaveIndex}`, 'Start time must be present if end time is present' );
                    valid = false;
                } else if ( startTime && endTime ) {
                    const startTimeValid = this.validateTime( startTime );
                    const endTimeValid = this.validateTime( endTime );

                    if ( startTimeValid && endTimeValid ) {
                        if ( !this.validateLeaveTimes( leave, index, leaveIndex ) ) {
                            valid = false;
                        } else {
                            this.resetInputErrorMessage( `start_time_${index}_${leaveIndex}` );
                            this.resetInputErrorMessage( `end_time_${index}_${leaveIndex}` );
                        }
                    } else {
                        !startTimeValid && this.setInputErrorMessage( `start_time_${index}_${leaveIndex}`, 'Date Invalid' );
                        !endTimeValid && this.setInputErrorMessage( `end_time_${index}_${leaveIndex}`, 'Date Invalid' );
                        valid = false;
                    }
                }
            });
        });

        return valid;
    }

    /**
     * Remap filed leaves payload to match the needs of API
     *
     * @returns {Array}
     */
    prepareFiledLeavePayload = () => {
        const leaves = [];
        Object.keys( this.state.employeeLeaves ).forEach( ( key ) => {
            this.state.employeeLeaves[ key ].forEach( ( leave ) => {
                const { schedule_id: scheduleId, date, start_time: startTime, end_time: endTime } = leave;
                if ( startTime && endTime ) {
                    leaves.push({
                        schedule_id: scheduleId,
                        date,
                        start_time: startTime,
                        end_time: endTime
                    });
                }
            });
        });

        return leaves;
    }

    submitFiledLeave = () => {
        if ( this.validateForm() ) {
            const payload = {
                ...this.state.filedLeave,
                leaves: this.prepareFiledLeavePayload()
            };

            if ( this.props.employeeId ) {
                payload.isEmployeeIdPrefiled = true;
            }

            this.props.submitFiledLeave( payload );
        }
    }

    /**
     * Validate time
     * @param {String} value
     *
     * @returns {Boolean}
     */
    validateTime( value ) {
        let valid = false;
        if ( value ) {
            const regex = new RegExp( /\d{2}[:]\d{2}/ );
            valid = regex.test( value );
        }
        return valid;
    }

    /**
     * Validates times according to schedule
     * @param {Object} leave
     * @param {Number} index
     * @param {Number} leaveIndex
     *
     * @returns {Boolean}
     */
    validateLeaveTimes( leave, index, leaveIndex ) {
        let [
            startTime, // eslint-disable-line
            endTime, // eslint-disable-line
            scheduleStartTime,
            scheduleEndTime
        ] = this.getFormattedTime( leave.start_time, leave.end_time, leave.schedule_start_time, leave.schedule_end_time );

        if ( scheduleStartTime.isAfter( scheduleEndTime ) ) {
            scheduleEndTime = moment( scheduleEndTime ).add( 1, 'day' );
            scheduleStartTime = moment( scheduleStartTime ).subtract( 1, 'day' );
        }

        if ( startTime.isSameOrAfter( moment( endTime ).add( 1, 'day' ) ) ) {
            this.setInputErrorMessage( `start_time_${index}_${leaveIndex}`, 'Start time must be before end time' );
            this.setInputErrorMessage( `end_time_${index}_${leaveIndex}`, 'End time must be after start time' );

            return false;
        }

        if ( startTime.isBefore( scheduleStartTime ) ) {
            this.setInputErrorMessage( `start_time_${index}_${leaveIndex}`, 'Start time must be same or after schedule start time' );

            return false;
        }

        if ( startTime.isSameOrAfter( scheduleEndTime ) ) {
            this.setInputErrorMessage( `start_time_${index}_${leaveIndex}`, 'Start time must be before schedule end time' );

            return false;
        }

        if ( endTime.isAfter( scheduleEndTime ) ) {
            this.setInputErrorMessage( `end_time_${index}_${leaveIndex}`, 'End time must be same or before schedule end time' );

            return false;
        }

        if ( endTime.isSameOrBefore( scheduleStartTime ) ) {
            this.setInputErrorMessage( `end_time_${index}_${leaveIndex}`, 'End time must be after schedule start time' );
            return false;
        }

        return true;
    }

    /**
     * Prepare a single shift object
     * @param {Number} scheduleId
     * @param {String} scheduleName
     * @param {String} date
     * @param {String} startTime
     * @param {String} endTime
     * @param {String} scheduleStartTime
     * @param {String} scheduleEndTime
     *
     * @returns {Object}
     */
    prepareSingleShift = ( scheduleId, scheduleName, date, startTime, endTime, scheduleStartTime, scheduleEndTime ) => ({
        schedule_id: scheduleId,
        schedule_name: scheduleName,
        date,
        start_time: startTime,
        end_time: endTime,
        schedule_start_time: scheduleStartTime,
        schedule_end_time: scheduleEndTime
    })

    /**
     * Groups leaves by date
     * @param {Array} leaves
     *
     * @returns {Object}
     */
    groupLeavesByDate = ( leaves ) => {
        const { start_date: startDate, end_date: endDate } = this.state.filedLeave;

        return groupBy( filter( leaves, ( leave ) => (
            moment( leave.date ).isSameOrAfter( startDate ) && moment( leave.date ).isSameOrBefore( endDate )
        ) ), 'date' );
    }

    /**
     * Sorts leaves by date
     * @param {Object}
     *
     * @returns {Object}
     */
    sortLeavesByDate = ( leaves ) => (
        Object.keys( leaves )
            .sort()
            .reduce( ( accumulator, current ) => ( accumulator[ current ]  = leaves[ current ], accumulator ), {}) // eslint-disable-line
    )

    /**
     * Adds default schedules to leaves
     * @param {Object} employeeLeaves
     *
     * @returns {Object}
     */
    includeDefaultSchedule = ( employeeLeaves ) => {
        const { defaultSchedule } = this.props.formOptions;
        let leaves = { ...employeeLeaves };
        const restDays = this.prepareRestDays();

        each( this.getShiftRange(), ( date ) => {
            if ( !Object.keys( leaves ).includes( date ) && !Object.keys( restDays ).includes( date ) ) {
                const weekDay = getISOWeekday( date );
                const defaultScheduleForDay = find( defaultSchedule, [ 'day_of_week', weekDay ]);
                const [ scheduleStartTime, scheduleEndTime ] = [
                    formatDate( defaultScheduleForDay.work_start, DATE_FORMATS.TIME ),
                    formatDate( defaultScheduleForDay.work_end, DATE_FORMATS.TIME )
                ];

                if ( defaultScheduleForDay.day_type !== DAY_TYPES.REST_DAY ) {
                    leaves = {
                        ...leaves,
                        [ date ]: [this.prepareSingleShift( null, 'Default Schedule', date, null, null, scheduleStartTime, scheduleEndTime )]
                    };
                }
            }
        });

        return leaves;
    }

    handleLeaveTypeChange = ( value, unit ) => {
        this.setState({
            filedLeave: {
                ...this.state.filedLeave,
                leave_type_id: parseInt( value, 10 ),
                unit
            }
        }, () => {
            this.getTotalLeaveUnits();
        });
    }

    /**
     * Try to fetch employee shifts after setting employee_id
     *
     * @param {String|Number} id
     */
    handleEmployeeChange = ( id ) => {
        this.setState({
            filedLeave: {
                ...this.state.filedLeave,
                employee_id: id
            }
        }, () => {
            this.fetchEmployeeShifts( 'employee_id' );
            this.props.getEmployeeLeaveTypes( id, this.props.formOptions );
            const employee = this.getEmployeeById();
            let errorMsg = '';
            const errorEntitlements = [];
            if ( employee && employee.department_id == null ) {
                errorEntitlements.push( 'department' );
            }
            if ( employee && employee.position_id == null ) {
                errorEntitlements.push( 'position' );
            }
            if ( employee && employee.location_id == null ) {
                errorEntitlements.push( 'location' );
            }
            if ( errorEntitlements.length > 0 ) {
                errorEntitlements.forEach( ( entitlement, index ) => {
                    const totalLength = errorEntitlements.length - 1;
                    if ( index < ( errorEntitlements.length - 1 ) ) {
                        const currentIndex = index + 1;
                        const secondToTheLast = ( currentIndex - totalLength ) === 0;
                        errorMsg = errorMsg + entitlement + ( secondToTheLast ? '' : ', ' );
                    } else {
                        errorMsg = errorMsg + ( errorEntitlements.length > 1 ? ' and ' : '' ) + entitlement;
                    }
                });
            }
            if ( errorMsg ) {
                this.setInputErrorMessage( 'employee_id', `Please add ${errorMsg} to this employee.` );
            }
        });
    }

    prepareRestDays = () => {
        const { start_date: startDate, end_Date: endDate } = this.state.filedLeave;
        restDayService.setDates( startDate, endDate );

        return restDayService.handleRestDaysDateRange( this.props.employeeRestDays );
    }

    /**
     * Remaps shift data into an object which has dates as keys
     * @param {Array} employeeShifts
     */
    prepareShiftData = ( employeeShifts ) => {
        let leaves = [];
        each( employeeShifts, ( shift ) => {
            const preparedData = map( shift.dates, ( date ) => {
                const { id, name, start_time, end_time } = shift.schedule;
                return this.prepareSingleShift( id, name, date, null, null, start_time, end_time );
            });

            leaves = [ ...leaves, ...preparedData ];
        });

        leaves = this.groupLeavesByDate( leaves );
        leaves = this.includeDefaultSchedule( leaves );

        this.setState({ employeeLeaves: { ...this.sortLeavesByDate( leaves ) }});
    }

    /**
     * Find leave type by its id
     * @param {String|Number} id
     *
     * @returns {Object}
     */
    findLeaveType = ( id ) => (
        find( this.props.formOptions.leaveTypes, ( leaveType ) => leaveType.id === parseInt( id, 10 ) )
    )

    /**
     * Determine are there shifts selected or is there at least one filled out shift
     */
    areShiftsSelected = () => {
        if ( !Object.keys( this.state.employeeLeaves ).length ) {
            return false;
        }

        const leaves = this.prepareFiledLeavePayload();

        return leaves && !!leaves.length;
    }

    /**
     * Renders shift section
     */
    renderShiftSection = () => {
        const { end_date: endDate, start_date: startDate, employee_id: employeeId } = this.state.filedLeave;

        return (
            <div className="row">
                <div className="col-xs-12" style={ { minHeight: '200px' } }>
                    <strong>Select Shift</strong>
                    { !endDate || !startDate ? (
                        <div style={ { textAlign: 'center', paddingTop: '20px' } }>No selected date range</div>
                    ) : !employeeId ? (
                        <div style={ { textAlign: 'center', paddingTop: '20px' } }>No selected employee</div>
                    ) : this.props.loadingShifts ? (
                        <div style={ { textAlign: 'center', paddingTop: '20px' } }>Loading shifts...</div>
                    ) : this.renderShiftList()}
                </div>
            </div>
        );
    }

    /**
     * Renders shift list
     */
    renderShiftList = () => {
        const { employeeLeaves: leaves } = this.state;

        return Object.keys( leaves ).length ? Object.keys( leaves ).map( ( date, index ) => (
            <div style={ { marginTop: '30px' } } key={ index }>
                <div className="row">
                    <div className="col-xs-4">
                        <ValueWrapper>
                            <strong>
                                { formatDate( date, DATE_FORMATS.DISPLAY ) } Shift
                            </strong>
                        </ValueWrapper>
                    </div>
                </div>
                {
                    leaves[ date ].map( ( leave, leaveIndex ) => (
                        <div className="row" key={ `${index}_${leaveIndex}` }>
                            <div className="col-xs-4">
                                <ValueWrapper>
                                    <ValueWrapper>
                                        <div>Shift</div>
                                        <ValueWrapper>
                                            <div style={ { marginTop: '10px' } }>
                                                { leave.schedule_name }
                                            </div>
                                        </ValueWrapper>
                                    </ValueWrapper>
                                </ValueWrapper>
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id={ `start-time-${index}-${leaveIndex}` }
                                    label="Start Time"
                                    placeholder="Choose desired time"
                                    ref={ ( ref ) => { this[ `start_time_${index}_${leaveIndex}` ] = ref; } }
                                    addon={ {
                                        content: <Icon name="clock" />,
                                        placement: 'right'
                                    } }
                                    onChange={ ( value ) => {
                                        const formatted = stripTime( value );

                                        this[ `start_time_${index}_${leaveIndex}` ].setState({ value: formatted }, () => {
                                            const leavesClone = { ...this.state.employeeLeaves };
                                            leavesClone[ date ][ leaveIndex ].start_time = formatted;

                                            this.setState({
                                                employeeLeaves: leavesClone
                                            }, () => {
                                                this.getTotalLeaveUnits();
                                            });
                                        });
                                    } }
                                />
                            </div>
                            <div className="col-xs-4">
                                <Input
                                    id={ `end-time-${index}-${leaveIndex}` }
                                    label="End Time"
                                    placeholder="Choose desired time"
                                    ref={ ( ref ) => { this[ `end_time_${index}_${leaveIndex}` ] = ref; } }
                                    addon={ {
                                        content: <Icon name="clock" />,
                                        placement: 'right'
                                    } }
                                    onChange={ ( value ) => {
                                        const formatted = stripTime( value );

                                        this[ `end_time_${index}_${leaveIndex}` ].setState({ value: formatted }, () => {
                                            const leavesClone = { ...this.state.employeeLeaves };
                                            leavesClone[ date ][ leaveIndex ].end_time = formatted;

                                            this.setState({
                                                employeeLeaves: leavesClone
                                            }, () => {
                                                this.getTotalLeaveUnits();
                                            });
                                        });
                                    } }
                                />
                            </div>
                        </div>
                    ) )
                }
            </div>
        ) ) : (
            <div style={ { textAlign: 'center', paddingTop: '20px' } }>No shifts for selected employee and date range</div>
        );
    }

    /**
     * Renders summary
     */
    renderSummary = () => {
        const {
            leave_type_id: leaveTypeId, status, start_date: startDate, end_date: endDate
        } = this.state.filedLeave;

        const leaveType = this.findLeaveType( leaveTypeId );
        return (
            <div className="row" style={ { marginTop: '20px' } }>
                <div className="col-xs-12">
                    <p style={ { fontWeight: 'bold' } }>Summary</p>
                    <div className="summary-panel">
                        <div className="summary-panel__column">
                            <span>Leave Type:</span>
                            <ValueWrapper>{ leaveType && leaveType.label ? leaveType.label : 'N / A' }</ValueWrapper>
                        </div>
                        <div className="summary-panel__column">
                            <span>Status:</span>
                            <ValueWrapper>{ status || 'N / A' }</ValueWrapper>
                        </div>
                        <div className="summary-panel__column">
                            <span>Duration:</span>
                            <ValueWrapper>{ startDate && endDate
                                ? `${formatDate( startDate, DATE_FORMATS.DISPLAY )} - ${formatDate( endDate, DATE_FORMATS.DISPLAY )}`
                                : 'N / A' }
                            </ValueWrapper>
                        </div>
                        <div className="summary-panel__column">
                            <span>Number of { !this.state.filedLeave.unit ? 'units' : this.state.filedLeave.unit } used:</span>
                            {
                                !this.props.loadingTotalLeaveUnits ? (
                                    <ValueWrapper>
                                        { this.props.totalLeaveUnits } { parseFloat( this.props.totalLeaveUnits ) === 1 ? FILED_LEAVE_UNITS_SINGULAR[ this.state.filedLeave.unit ] : this.state.filedLeave.unit }
                                    </ValueWrapper>
                                ) : (
                                    <ValueWrapper>
                                        <i className="fa fa-circle-o-notch fa-spin fa-fw" aria-hidden="true" />
                                    </ValueWrapper>
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * Renders form
     */
    renderForm = () => (
        <div className={ this.props.loading ? 'hide' : '' }>
            <Container>
                <div className="row">
                    <div className="col-xs-4">
                        {
                            this.props.employeeId ? (
                                <div>
                                    <p>Employee Name</p>
                                    <p>{ this.employee && this.employee.fullName }</p>
                                </div>
                            ) : (
                                <MultiSelect
                                    id="employee-id"
                                    label={ <span>Employee Name or ID</span> }
                                    placeholder="Type in name or id"
                                    required
                                    data={ this.props.formOptions.employees }
                                    value={ this.state.filedLeave.employee }
                                    onChange={ ({ value }) => {
                                        this.handleEmployeeChange( value );
                                    } }
                                    ref={ ( ref ) => { this.employee_id = ref; } }
                                    multi={ false }
                                />
                            )
                        }
                    </div>
                    <div className="col-xs-4">
                        {
                            this.props.employeeId && <div>
                                <p>Employee ID</p>
                                <p>{ this.employee && this.employee.id }</p>
                            </div>
                        }
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        <SalSelect
                            id="status"
                            label="Status"
                            required
                            key="status"
                            data={ [ 'Approved', 'Declined', 'Pending', 'Cancelled' ].map( ( value ) => ({ label: value, value }) ) }
                            value={ this.state.filedLeave.status }
                            placeholder="Choose an option"
                            ref={ ( ref ) => { this.status = ref; } }
                            onChange={ ({ value }) => { this.updateFiledLeave( 'status', value ); } }
                        />
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-4">
                        <SalSelect
                            id="leave-type"
                            label="Leave Type"
                            required
                            key="leave-type"
                            data={ this.props.formOptions.leaveTypes }
                            value={ this.state.filedLeave.leave_type_id ? `${this.state.filedLeave.leave_type_id} ${this.state.filedLeave.unit}` : '' }
                            placeholder="Choose an option"
                            ref={ ( ref ) => { this.leave_type_id = ref; } }
                            onChange={ ( v ) => {
                                const data = v.value.split( ' ' );
                                this.handleLeaveTypeChange( data[ 0 ], data[ 1 ]);
                            } }
                        />
                    </div>
                    <div className="col-xs-4 date">
                        <DatePicker
                            label="Start Date"
                            placeholder="Select desired date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            selectedDay={ this.state.filedLeave.start_date }
                            required
                            ref={ ( ref ) => { this.start_date = ref; } }
                            onChange={ ( value ) => {
                                const selectedDay = formatDate( value, DATE_FORMATS.API );
                                this.updateFiledLeave( 'start_date', selectedDay );
                            } }
                        />
                    </div>
                    <div className="col-xs-4 date">
                        <DatePicker
                            label="End Date"
                            placeholder="Select desired date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            selectedDay={ this.state.filedLeave.end_date }
                            required
                            ref={ ( ref ) => { this.end_date = ref; } }
                            onChange={ ( value ) => {
                                const selectedDay = formatDate( value, DATE_FORMATS.API );
                                this.updateFiledLeave( 'end_date', selectedDay );
                            } }
                        />
                    </div>
                </div>
                { this.renderShiftSection() }
                { this.renderSummary() }
            </Container>
        </div>
    )

    render() {
        return (
            <div>
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <MainWrapper>
                    <SalConfirm
                        onConfirm={ () => {
                            this.props.employeeId ?
                            browserHistory.push( `/employee/${this.props.employeeId}`, true ) :
                            browserHistory.push( '/time/leaves', true );
                        } }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showModal }
                    />

                    <div className={ `loader ${this.props.loading ? '' : 'hide'}` }>
                        <Loader />
                    </div>
                    { this.renderForm() }
                </MainWrapper>
                <Footer>
                    <Button
                        label="Cancel"
                        type="neutral"
                        size="large"
                        onClick={ () => {
                            this.setState({ showModal: true });
                        } }
                    />
                    <Button
                        label={ this.props.submitted ? <Loader /> : 'Submit' }
                        disabled={ !this.areShiftsSelected() }
                        type="action"
                        size="large"
                        onClick={ this.submitFiledLeave }
                        ref={ ( ref ) => { this.submitButton = ref; } }
                    />
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    formOptions: makeSelectFormOptions(),
    submitted: makeSelectSubmitted(),
    errors: makeSelectErrors(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    loadingShifts: makeSelectLoadingShifts(),
    totalLeaveUnits: makeSelectTotalLeaveUnits(),
    loadingTotalLeaveUnits: makeSelectLoadingTotalLeaveUnits(),
    employeeRestDays: makeSelectEmployeeRestDays()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createFiledLeaveActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ManualEntry );
