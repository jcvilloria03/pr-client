import styled from 'styled-components';

const MainWrapper = styled.div`
    padding-top: 76px;
    background: #fff;
    margin-bottom: 80px;

    .nav {
        padding: 10px 20px;
        background: #f0f4f6;
        margin-bottom: 50px;
    }

    .loader {
        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .radiogroup {
        padding: 0;
        & > span {
            padding: 0 15px;
            color: #5b5b5b;
            font-size: 14px;
            margin-bottom: 4px;
            margin-right: 1rem;
            font-weight: 400;
        }
    }

    .date {
        & > div > span {
            display: block;
        }
        .DayPickerInput {
            display: block;

            input {
                width: 100%;
            }
        }
    }

    .heading {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 0 auto 30px auto;

        h3 {
            font-weight: 600;
        }

        p {
            text-align: center;
            max-width: 800px;
        }
    }

    .hide {
        display: none;
    }

    .input-group-addon {
        background-color: #fff !important;
        border-radius: 0;
    }

    .input-group > .form-control {
        border-right: 0;
        z-index: 0;
    }
`;

const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;

const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

const ValueWrapper = styled.div`
    padding-left: 12.5px;
`;

export {
    MainWrapper,
    Footer,
    ConfirmBodyWrapperStyle,
    ValueWrapper
};
