export const INITIALIZE = 'app/Leaves/FiledLeaves/Add/INITIALIZE';
export const LOADING = 'app/Leaves/FiledLeaves/Add/LOADING';
export const GENERATING = 'app/Leaves/FiledLeaves/Add/GENERATING';
export const SET_ERRORS = 'app/Leaves/FiledLeaves/Add/SET_ERRORS';
export const SET_FORM_OPTIONS = 'app/Leaves/FiledLeaves/Add/SET_FORM_OPTIONS';
export const SUBMIT_FORM = 'app/Leaves/FiledLeaves/Add/SUBMIT_FORM';
export const UPLOAD_FILED_LEAVES = 'app/Leaves/FiledLeaves/Add/UPLOAD_FILED_LEAVES';
export const GET_FILED_LEAVES_PREVIEW = 'app/leaves/FiledLeaves/Add/GET_FILED_LEAVES_PREVIEW';
export const SET_SAVING_STATUS = 'app/Leaves/FiledLeaves/Add/SET_SAVING_STATUS';
export const SET_SAVING_ERRORS = 'app/Leaves/FiledLeaves/Add/SET_SAVING_ERRORS';
export const SAVE_FILED_LEAVES = 'app/Leaves/FiledLeaves/Add/SAVE_FILED_LEAVES';
export const RESET_FILED_LEAVES_PREVIEW = 'app/Leaves/FiledLeaves/Add/RESET_FILED_LEAVES_PREVIEW';
export const SET_FILED_LEAVES_PREVIEW_STATUS = 'app/Leaves/FiledLeaves/Add/SET_FILED_LEAVES_PREVIEW_STATUS';
export const SET_FILED_LEAVES_PREVIEW_DATA = 'app/Leaves/FiledLeaves/Add/SET_FILED_LEAVES_PREVIEW_DATA';
export const SUBMITTED = 'app/Leaves/FiledLeaves/Add/SUBMITTED';
export const NOTIFICATION_SAGA = 'app/Leaves/FiledLeaves/Add/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Leaves/FiledLeaves/Add/NOTIFICATION';
export const SET_BATCH_UPLOAD_JOB_ID = 'app/Leaves/FiledLeaves/Add/SET_BATCH_UPLOAD_JOB_ID';
export const SET_BATCH_UPLOAD_STATUS = 'app/Leaves/FiledLeaves/Add/SET_BATCH_UPLOAD_STATUS';
export const SET_BATCH_UPLOAD_ERRORS = 'app/Leaves/FiledLeaves/Add/SET_BATCH_UPLOAD_ERRORS';
export const GET_EMPLOYEE_SHIFTS = 'app/Leaves/FiledLeaves/Add/GET_EMPLOYEE_SHIFTS';
export const SET_LOADING_SHIFTS = 'app/Leaves/FiledLeaves/Add/SET_LOADING_SHIFTS';
export const GET_EMPLOYEE_LEAVE_TYPES = 'app/Leaves/FiledLeaves/Add/GET_EMPLOYEE_LEAVE_TYPES';

export const SET_LOADING_TOTAL_LEAVE_UNITS = 'app/Leaves/FiledLeaves/Add/SET_LOADING_TOTAL_LEAVE_UNITS';
export const SET_TOTAL_LEAVE_UNITS = 'app/Leaves/FiledLeaves/Add/SET_TOTAL_LEAVE_UNITS';
export const GET_TOTAL_LEAVE_UNITS = 'app/Leaves/FiledLeaves/Add/GET_TOTAL_LEAVE_UNITS';

export const SET_EMPLOYEE_REST_DAYS = 'app/Leaves/FiledLeaves/Add/SET_EMPLOYEE_REST_DAYS';

export const LEAVE_UNITS = {
    DAYS: 'days',
    HOURS: 'hours'
};
