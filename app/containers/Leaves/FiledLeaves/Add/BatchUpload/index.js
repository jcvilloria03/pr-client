import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../../../components/A';
import Table from '../../../../../components/Table';
import Button from '../../../../../components/Button';
import Clipboard from '../../../../../components/Clipboard';
import FileInput from '../../../../../components/FileInput';
import { H4, P } from '../../../../../components/Typography';

import { browserHistory } from '../../../../../utils/BrowserHistory';
import { formatPaginationLabel, formatCurrency, formatDate } from '../../../../../utils/functions';
import { DATE_FORMATS, FILED_LEAVE_UNITS_SINGULAR } from '../../../../../utils/constants';

import { BASE_PATH_NAME } from '../../../../../constants';

import * as actions from '../actions';

import {
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectFiledLeavesPreview,
    makeSelectSaving
} from '../selectors';

import { Footer, StyledLoader, PageWrapper } from './styles';

/**
 * BatchUpload Component
 */
class BatchUpload extends React.Component {
    static propTypes = {
        batchUploadJobId: React.PropTypes.string,
        uploadFiledLeaves: React.PropTypes.func,
        batchUploadStatus: React.PropTypes.string,
        batchUploadErrors: React.PropTypes.oneOfType([
            React.PropTypes.object,
            React.PropTypes.array
        ]),
        filedLeavesPreview: React.PropTypes.shape({
            status: React.PropTypes.string,
            data: React.PropTypes.array
        }),
        saving: React.PropTypes.shape({
            status: React.PropTypes.string,
            errors: React.PropTypes.object
        }),
        saveFiledLeaves: React.PropTypes.func
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            files: [],
            status: null,
            errors: {},
            clipboard: '',
            submit: false,
            filedLeavesTableLabel: 'Showing 0-0 of 0 entries',
            errorsTableLabel: 'Showing 0-0 of 0 entries',
            filedLeavesPreview: {
                data: [],
                status: 'ready'
            },
            savingStatus: '',
            savingErrors: {}
        };
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.batchUploadStatus !== this.props.batchUploadStatus && this.setState({
            status: nextProps.batchUploadStatus
        }, () => {
            this.validateButton.setState({
                disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.batchUploadStatus )
            });
        });

        nextProps.batchUploadErrors !== this.props.batchUploadErrors && this.setState({
            errors: nextProps.batchUploadErrors
        }, () => {
            let clipboard = '';
            const rows = Object.keys( nextProps.batchUploadErrors );

            if ( rows.length > 0 ) {
                rows.forEach( ( row ) => {
                    Object.values( nextProps.batchUploadErrors[ row ]).forEach( ( value ) => {
                        clipboard += `Row ${row}, ${value}\n`;
                    });
                });
            }

            this.handleErrorsTableChanges();
            this.setState({ clipboard });
        });

        nextProps.filedLeavesPreview.data !== this.props.filedLeavesPreview.data && this.setState({
            filedLeavesPreview: { ...this.state.filedLeavesPreview, data: nextProps.filedLeavesPreview.data }
        }, () => {
            this.handleFiledLeavesTableChanges();
        });

        nextProps.filedLeavesPreview.status !== this.props.filedLeavesPreview.status && this.setState({
            filedLeavesPreview: { ...this.state.filedLeavesPreview, status: nextProps.filedLeavesPreview.status }
        });

        nextProps.saving.status !== this.props.saving.status && this.setState({
            savingStatus: nextProps.saving.status
        }, () => {
            this.submitButton.setState({ disabled: nextProps.saving.status !== 'save_failed' });
        });
    }

    formatToCurrency = ( value ) => (
        value ? `P ${formatCurrency( value )}` : 'P 0.00'
    );

    handleErrorsTableChanges = () => {
        if ( !this.errorsTable ) {
            return;
        }

        this.setState({
            errorsTableLabel: formatPaginationLabel( this.errorsTable.tableComponent.state )
        });
    };

    handleFiledLeavesTableChanges = () => {
        if ( !this.filedLeavesTable ) {
            return;
        }

        this.setState({
            filedLeavesTableLabel: formatPaginationLabel( this.filedLeavesTable.tableComponent.state )
        });
    };

    renderFiledLeavesSection = () => {
        const tableColumns = [
            {
                header: 'Employee ID',
                accessor: 'employee_id',
                minWidth: 200,
                render: ({ row }) => <div>{row.employee_id}</div>
            },
            {
                header: 'Leave Type',
                accessor: 'leave_type_name',
                minWidth: 200,
                render: ({ row }) => <div>{row.leave_type_name}</div>
            },
            {
                header: 'Shift Name',
                accessor: 'schedule_name',
                minWidth: 200,
                render: ({ row }) => <div>{row.schedule_name ? row.schedule_name : 'Default Schedule' }</div>
            },
            {
                header: 'Leave Date From',
                accessor: 'start_date',
                minWidth: 200,
                render: ({ row }) => <div>{row.start_date}</div>
            },
            {
                header: 'Leave Date To',
                accessor: 'end_date',
                minWidth: 200,
                render: ({ row }) => <div>{row.end_date}</div>
            },
            {
                header: 'Leave Time Start',
                accessor: 'start_time',
                minWidth: 200,
                render: ({ row }) => <div>{row.start_time}</div>
            },
            {
                header: 'Leave Time End',
                accessor: 'end_time',
                minWidth: 200,
                render: ({ row }) => <div>{row.end_time}</div>
            },
            {
                header: 'Number of Units',
                accessor: 'total_units',
                minWidth: 200,
                render: ({ row }) => <div>{ row.total_value } { parseFloat( row.total_value ) === 1 ? FILED_LEAVE_UNITS_SINGULAR[ row.unit ] : row.unit }</div>

            },
            {
                header: 'State',
                accessor: 'status',
                minWidth: 200,
                render: ({ row }) => <div>{row.status}</div>
            }
        ];

        const dataForDisplay = this.state.filedLeavesPreview.data.map( ( data ) => ({
            ...data,
            start_date: formatDate( data.start_date, DATE_FORMATS.DISPLAY ),
            end_date: formatDate( data.end_date, DATE_FORMATS.DISPLAY )
        }) );

        return this.state.filedLeavesPreview.data.length === 0
            ? null
            : (
                <div>
                    <H4>Filed Leaves List:</H4>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.filedLeavesTableLabel }
                        </span>
                    </div>
                    <Table
                        columns={ tableColumns }
                        data={ dataForDisplay }
                        pagination
                        ref={ ( ref ) => { this.filedLeavesTable = ref; } }
                        onDataChange={ this.handleFiledLeavesTableChanges }
                    />
                </div>
            );
    };

    renderErrorsSection = () => {
        const errorDisplay = [];
        const errorList = this.state.errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'Row Number',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'Error Type',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            const keys = Object.keys( errorList );

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row {key}</H4>,
                    error: <ul>{Object.values( errorList[ key ]).map( ( value ) => <li key={ value } style={ { margin: '0' } } >{value}</li> )}</ul>
                });
            });

            return (
                <div className="errors">
                    <H4>Data field errors</H4>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.errorsTableLabel }
                        </span>
                        <Clipboard value={ this.state.clipboard } />
                    </div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                        ref={ ( ref ) => { this.errorsTable = ref; } }
                        onDataChange={ this.handleErrorsTableChanges }
                    />
                </div>
            );
        }

        return null;
    }

    render() {
        return (
            <PageWrapper>
                <Container>
                    <div>
                        <div className="steps">
                            <div className="step">
                                <div className="template">
                                    <H4>Step 1:</H4>
                                    <P>Download and fill-out the Employee Payroll Information Template.</P>
                                    <P><A target="_blank" href={ `${BASE_PATH_NAME}/guides/leaves/filed-leaves/batch-upload` }>Click here to view the upload guide.</A></P>
                                    <A className="sl-c-btn--wide" href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/guides/leave_requests_template.csv" download>Download Template</A>
                                </div>
                            </div>
                            <div className="step">
                                <div className="upload">
                                    <H4>Step 2:</H4>
                                    <P>After completely filling out the template, choose and upload it here.</P>
                                    <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'none' : 'block' } } >
                                        <FileInput
                                            accept=".csv"
                                            onDrop={ ( files ) => {
                                                const { acceptedFiles } = files;

                                                this.setState({
                                                    files: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null,
                                                    errors: {},
                                                    status: null
                                                }, () => {
                                                    this.validateButton.setState({ disabled: acceptedFiles.length <= 0 });
                                                });
                                            } }
                                            ref={ ( ref ) => { this.fileInput = ref; } }
                                        />
                                    </div>
                                    <div style={ { display: this.state.status === 'validated' ? 'block' : 'none' } }>
                                        <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                                    </div>
                                    <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'validated' ? 'none' : 'block' } } >
                                        <Button
                                            label={
                                                this.state.submit ? (
                                                    <StyledLoader className="animation">
                                                        Uploading <div className="anim3"></div>
                                                    </StyledLoader>
                                                ) : (
                                                    'Upload'
                                                )
                                            }
                                            disabled={ this.state.files ? this.state.files.length <= 0 : true }
                                            type="neutral"
                                            ref={ ( ref ) => { this.validateButton = ref; } }
                                            onClick={ () => {
                                                this.setState({
                                                    errors: {},
                                                    status: null,
                                                    submit: true
                                                }, () => {
                                                    this.validateButton.setState({ disabled: true }, () => {
                                                        this.props.uploadFiledLeaves({ file: this.state.files });

                                                        setTimeout( () => {
                                                            this.validateButton.setState({ disabled: false });
                                                            this.setState({ submit: false });
                                                        }, 5000 );
                                                    });
                                                });
                                            } }
                                        />
                                    </div>
                                    <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'block' : 'none' } } >
                                        <StyledLoader className="animation">
                                            <H4 style={ { margin: '0' } }>{ this.state.status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                        </StyledLoader>
                                    </div>
                                </div>
                            </div>
                        </div>
                        { this.renderFiledLeavesSection() }
                        { this.renderErrorsSection() }
                    </div>
                </Container>
                <Footer>
                    <Button
                        label="Cancel"
                        type="neutral"
                        size="large"
                        onClick={ () => {
                            browserHistory.push( '/time/leaves', true );
                        } }
                    />
                    <Button
                        ref={ ( ref ) => { this.submitButton = ref; } }
                        type="action"
                        disabled={ this.state.status !== 'validated' }
                        label={
                            this.state.savingStatus === 'save_queued' || this.state.savingStatus === 'saving' ?
                                (
                                    <StyledLoader className="animation">
                                        <span>Saving</span> <div className="anim3"></div>
                                    </StyledLoader>
                                ) : 'Submit'
                        }
                        size="large"
                        onClick={ () => {
                            this.submitButton.setState({ disabled: true });
                            this.setState({ savingStatus: 'save_queued' });
                            this.props.saveFiledLeaves({ jobId: this.props.batchUploadJobId });
                        } }
                    />
                </Footer>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    batchUploadJobId: makeSelectBatchUploadJobId(),
    batchUploadStatus: makeSelectBatchUploadStatus(),
    batchUploadErrors: makeSelectBatchUploadErrors(),
    filedLeavesPreview: makeSelectFiledLeavesPreview(),
    saving: makeSelectSaving()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( BatchUpload );
