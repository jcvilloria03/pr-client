import styled, { keyframes } from 'styled-components';

const anim3 = keyframes`
    to {
        transform: rotate(45deg) translate(3px, 3px);
    }
`;

const StyledLoader = styled.div`
    font-size: 16px;
    font-weight: 600;
    color: #00a5e5;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;

    span {
        color: white;
    }

    .anim3 {
        padding-left: 10px;
        &:before {
            display: block;
            width: 12px;
            height: 12px;
            margin-top: -1px;
            border: 2px solid #00a5e5;
            content: "";
            animation: ${anim3} .5s ease infinite alternate;
            border-radius: 100% 100% 0 100%;
            transform: rotate( 45deg );
        }

        &:after {
            display: block;
            width: 12px;
            height: 3px;
            margin-top: 8px;
            background-color: rgba(0, 0, 0, 0.2);
            content: "";
            border-radius: 100%;
        }
    }
`;

const PageWrapper = styled.div`
    margin-bottom: 80px;

    .template, .upload {
        text-align: center;
        padding: 2rem;
        border: 2px dashed #ccc;
        border-radius: 12px;
        width: 100%;
        height: 100%;

        & > a {
            background-color: #FFF;
            color: #333;
            border: 1px solid #4ABA4A;
            padding: 8px 12px;
            border-radius: 30px;
            display: inline;
            vertical-align: middle;
            margin: 0 auto;
            max-width: 200px;

            &:hover, &:focus, &:active:focus {
                outline: none;
                background-color: #fff;
                color: #4ABA4A;
            }
        }
    }
`;

const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

export { Footer, StyledLoader, PageWrapper };
