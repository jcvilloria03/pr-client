import React from 'react';
import debounce from 'lodash/debounce';
import orderBy from 'lodash/orderBy';
import first from 'lodash/first';
import isEmpty from 'lodash/isEmpty';
import uniqBy from 'lodash/uniqBy';

import { H2, H3, H5 } from '../../../../components/Typography';
import Button from '../../../../components/Button';
import SalDropdown from '../../../../components/SalDropdown';
import AsyncTable from '../../../../components/AsyncTable';
import Search from '../../../../components/Search';
import Icon from '../../../../components/Icon';
import SalConfirm from '../../../../components/SalConfirm';
import Filter from './filter';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { filterHelper } from '../../../../utils/filterHelper';
import { DATE_FORMATS, FILED_LEAVE_UNITS_SINGULAR } from '../../../../utils/constants';
import {
    formatPaginationLabel,
    formatDate
} from '../../../../utils/functions';

import {
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from '../styles';

/**
 *
 * FiledLeave Component
 *
 */
class FiledLeave extends React.Component {
    static propTypes = {
        data: React.PropTypes.array,
        fetchData: React.PropTypes.func,
        exportFiledLeaves: React.PropTypes.func,
        deleteSelectedData: React.PropTypes.func,
        initialLoading: React.PropTypes.bool,
        loading: React.PropTypes.bool,
        permission: React.PropTypes.object,
        filterData: React.PropTypes.shape({
            leaveTypes: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        getDownloadUrl: React.PropTypes.func,
        downloadUrl: React.PropTypes.string,
        filedLeaveJobId: React.PropTypes.string
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            selectedValues: [],
            deleteModalLabel: '',
            dropdownItems: [
                {
                    label: 'Delete',
                    children: <div>Delete</div>,
                    onClick: this.handleDeleteModal
                },
                {
                    label: 'Download',
                    children: <div>Download</div>,
                    onClick: this.exportFiledLeaves
                }
            ],
            pagination: {
                page: 0,
                pageSize: 10,
                totalPages: 0,
                dataLength: 0
            },
            sorting: {
                by: '',
                order: ''
            },
            filter: {},
            downloadUrl: '',
            filedLeaveJobId: ''
        };
    }

    /**
     * Handle new props
     */
    componentWillReceiveProps( newProps ) {
        if ( !isEmpty( newProps.pagination ) ) {
            this.setPagination( newProps.pagination );
        }
        if ( !isEmpty( newProps.filedLeaveJobId ) ) {
            if ( !isEmpty( newProps.downloadUrl ) && newProps.downloadUrl !== this.props.downloadUrl ) {
                this.downloadFile( newProps.downloadUrl );
            } else {
                this.getDownloadUrl( newProps.filedLeaveJobId );
            }
        }
    }

    /**
     * Get employee leave credits.
     * @param {Object} data
     */
    getFiledLeaves() {
        this.props.fetchData({
            term: this.state.term,
            page: this.state.pagination.page,
            per_page: this.state.pagination.pageSize,
            sort_by: this.state.sorting.by,
            sort_order: this.state.sorting.order,
            ...this.state.filter
        });
    }

    /**
     * Sets leave credits Pagination
     */
    setPagination = ( filedLeavesPagination ) => {
        if ( !this.table || !this.table.tableComponent ) {
            return;
        }

        const pagination = {
            page: filedLeavesPagination.current_page,
            pageSize: filedLeavesPagination.per_page,
            totalPages: filedLeavesPagination.total_pages,
            dataLength: filedLeavesPagination.total
        };

        this.setState({
            pagination,
            label: formatPaginationLabel({
                page: filedLeavesPagination.current_page - 1,
                pageSize: filedLeavesPagination.per_page,
                dataLength: filedLeavesPagination.total
            })
        });
    }

    /**
     * Sets pagination label.
     * @param {Object} paginationParams
     */
    setPaginationLabel = ( paginationParams ) => {
        this.setState({
            label: formatPaginationLabel( paginationParams )
        });
    }

    /**
     * Gets ids of selected values from table.
     * Sets valueIds state.
     * @returns {Array}
     */
    setSelectedValues = () => {
        const selectedValues = [];

        this.table.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedValues.push( this.table.props.data[ index ]);
            }
        });

        this.setState({ selectedValues });

        return this.state.selectedValues.map( ( selectedValue ) => selectedValue.id );
    }

    /**
     * Gets selected values ids.
     * @returns {Array}
     */
    getSelectedValuesIds = () => this.state.selectedValues.map( ( selectedValue ) => selectedValue.id )

    /**
     * Get table columns information.
     * @return {Array}
     */
    getTableColumns() {
        return [
            {
                header: 'Employee Name',
                accessor: 'employee_name',
                minWidth: 200,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.employee.full_name}
                    </div>
                )
            },
            {
                header: 'Leave Type',
                accessor: 'leave_type_name',
                minWidth: 150,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.leave_type ? row.leave_type.name : ''}
                    </div>
                )
            },
            {
                header: 'Start Date',
                accessor: 'start_date',
                minWidth: 200,
                sortable: true,
                render: ({ row }) => (
                    <div>{formatDate( row.start_date, DATE_FORMATS.DISPLAY )}</div>
                )
            },
            {
                header: 'End Date',
                accessor: 'end_date',
                minWidth: 200,
                sortable: true,
                render: ({ row }) => (
                    <div>{formatDate( row.end_date, DATE_FORMATS.DISPLAY )}</div>
                )
            },
            {
                header: 'Number of Hours',
                accessor: 'total_value',
                minWidth: 150,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.total_value } { row.total_value === 1 ? FILED_LEAVE_UNITS_SINGULAR[ row.unit ] : row.unit}
                    </div>
                )
            },
            {
                header: 'Shift Name',
                accessor: 'shift_name',
                minWidth: 150,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {this.extractShiftNames( row )}
                    </div>
                )
            },
            {
                header: 'Status',
                accessor: 'status',
                minWidth: 150,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.status}
                    </div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.props.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => browserHistory.pushWithParam( `/time/filed-leaves/${row.id}/edit`, true, { editView: true }) }
                    />
                )
            }
        ];
    }

    /**
     * Sets deleteModalLabel
     */
    setDeleteModalLabel = () => {
        this.setState({
            deleteModalLabel: this.getDeleteLabelText()
        });
    }

    /**
     * Gets singular or plural noun.
     * @returns {String}
     */
    getDeleteLabelText() {
        return `You are about to delete ${this.state.selectedValues.length}
            filed leave${this.state.selectedValues.length > 1 ? 's' : ''}.
            Do you want to proceed?`;
    }

    /**
     * Get download url
     */
    getDownloadUrl = ( filedLeaveJobId ) => {
        this.props.getDownloadUrl({ filedLeaveJobId });
    }

    /**
     * Downloads file to user
     */
    downloadFile = ( downloadUrl = this.props.downloadUrl ) => {
        if ( downloadUrl ) {
            const linkElement = document.getElementById( 'downloadLink' );
            linkElement.href = downloadUrl;
            linkElement.click();
            linkElement.href = '';
        } else {
            this.getDownloadUrl( this.props.filedLeaveJobId );
        }
    }

    /**
     * Extract shift names from filed leave row.
     * @param {Object} row
     * @return {String}
     */
    extractShiftNames( row ) {
        const leaves = uniqBy( row.leaves, ( leave ) => ( leave.schedule ? leave.schedule.name : 'Default Schedule' ) );
        return leaves.reduce( ( shiftNames, leave ) => `${shiftNames}${leave.schedule ? leave.schedule.name : 'Default Schedule'}, `, '' ).slice( 0, -2 );
    }

    /**
     * Toggles filters.
     */
    toggleFilter = () => {
        if ( this.props.loading || this.filterDataIsNotFetched() ) {
            return;
        }

        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    /**
     * Resets filters data.
     */
    resetFilters = () => {
        this.setState({
            showFilter: false,
            hasFiltersApplied: false,
            filterData: {}
        });
    }

    /**
     * Check is table mounted.
     * @returns {Boolean}
     */
    tableIsNotMounted() {
        return !this.table ||
            !this.table.tableComponent ||
            !this.table.tableComponent.state;
    }

    /**
     * Checks is filterData fetched.
     * @returns {Boolean}
     */
    filterDataIsNotFetched() {
        return !this.props.filterData ||
            isEmpty( this.props.filterData.departments ) ||
            !this.props.filterData.departments.length;
    }

    /**
     * Handles showing delete modal.
     */
    handleDeleteModal = () => {
        this.setSelectedValues();
        this.setDeleteModalLabel();

        if ( !this.state.selectedValues || !this.state.selectedValues.length ) {
            return;
        }

        this.setState({ showModal: false }, () => {
            this.setState({ showModal: true });
        });
    }

    /**
     * Checks is any table row selected.
     * @returns {Boolean}
     */
    tableHasSelectedRows() {
        if ( this.tableIsNotMounted() ) {
            return false;
        }

        return !!this.table.state.selected.filter( ( item ) => item ).length;
    }

    /**
     * Send a request to delete
     */
    deleteSelectedData = () => {
        const valueIds = this.getSelectedValuesIds();

        if ( !valueIds || !valueIds.length ) {
            return;
        }
        this.props.deleteSelectedData( valueIds, this.state.filter );
    }

    /**
     * Routes to single filed leave details page.
     * @param {Object} rowInfo
     * @param {Object} column
     * @returns {Object}
     */
    goToDetails = ( state, rowInfo, column ) => ({
        onDoubleClick: () => {
            if ( column.id !== 'select' ) {
                browserHistory.pushWithParam( `/time/filed-leaves/${rowInfo.row.id}`, false, { editView: false });
            }
        }
    })

    applyFilters = ( filters ) => {
        const filter = filterHelper.mapLeavesFilterDataIds( filters );
        const page = 1;
        const pageSize = 10;
        this.setState({
            hasFiltersApplied: !!filters.length,
            filter,
            pagination: {
                ...this.state.pagination,
                pageSize,
                page
            }
        }, () => {
            this.getFiledLeaves();
        });

        this.toggleFilter();
    }

    /**
     * Send a request to export filed leave
     */
    exportFiledLeaves = () => {
        this.setSelectedValues();

        const sortingOptions = this.table.state.sorting;

        const sorted = orderBy(
            this.state.selectedValues,
            sortingOptions.map( ( o ) => o.id ),
            sortingOptions.map( ( o ) => ( o.desc ? 'desc' : 'asc' ) )
        );

        if ( !sorted.length ) {
            return;
        }

        const payload = {
            ...this.state.filter,
            term: this.state.term
        };

        if ( this.table.headerSelect.checked ) {
            payload.download_all = true;
        } else {
            payload.leave_requests_ids = sorted.map( ( item ) => item.id );
        }

        // dispatch an action to send a request to API
        this.setState({
            downloadUrl: '',
            filedLeaveJobId: ''
        }, () => {
            this.props.exportFiledLeaves( payload );
        });
    };

    /**
     * handles filters and search inputs.
     */
    handleSearch = ( term ) => {
        if ( term !== this.state.term ) {
            this.setState({ term }, () => {
                this.getFiledLeaves();
            });
        }
    }

    /**
     * Prevent Delete option when status of a selected filed Leave is Approving/Declining
     */
    filterDropdownItems( dropDownItems ) {
        let showDelete = true;
        this.state.selectedValues.map( ( selectedValue ) => {
            if ( showDelete ) {
                showDelete = !([ 'Approving', 'Declining' ].includes( selectedValue.status ) );
            }

            return null;
        });

        if ( showDelete === false ) {
            return dropDownItems.filter( ( item ) => item.label !== 'Delete' );
        }

        return dropDownItems;
    }

    /**
     * Handle changes to table data.
     * @param {Object} tableProps
     */
    handleDataChange = ( tableProps ) => {
        if ( !tableProps.sorting || !tableProps.sorting.length ) return;

        const sortCondition = first( tableProps.sorting );

        this.setState({
            sorting: {
                by: sortCondition.id,
                order: sortCondition.desc ? 'desc' : 'asc'
            }
        }, () => {
            this.getFiledLeaves();
        });
    }

    /**
     * Handle fetching table data.
     * @param {Oject} data
     */
    handlePageChange = ( page ) => {
        this.setState({
            pagination: {
                ...this.state.pagination,
                page
            }
        }, () => {
            this.getFiledLeaves();
        });
    }

    /**
     * Handles page size change and calls fetchData on change.
     * @param {Number} pageSize
     */
    handlePageSizeChange = ( pageSize ) => {
        if ( !pageSize ) {
            return;
        }
        const page = 1;
        this.setState({
            pagination: {
                ...this.state.pagination,
                pageSize,
                page
            }
        }, () => {
            this.getFiledLeaves();
        });
    }

    render() {
        return (
            <div className="content">
                <SalConfirm
                    onConfirm={ this.deleteSelectedData }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                { this.state.deleteModalLabel }
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Leave Credits"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <div className="heading">
                    <H3>Filed Leave</H3>
                    <div style={ { textAlign: 'center' } } >
                        <div className="main">
                            <Button
                                className={ this.props.permission.create ? '' : 'hide' }
                                label="Add Filed Leave"
                                size="large"
                                type="action"
                                onClick={ () => browserHistory.push( '/time/filed-leaves/add', true ) }
                            />
                        </div>
                    </div>
                </div>
                <div className="loader" style={ { display: this.props.initialLoading ? '' : 'none' } }>
                    <LoadingStyles>
                        <H2>Loading Filed Leave.</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoadingStyles>
                </div>
                <div className="content" style={ { display: this.props.initialLoading ? 'none' : '' } }>
                    <div className="title">
                        <H5>Filed Leave List</H5>
                        <div className="search-wrapper">
                            <Search
                                ref={ ( ref ) => { this.search = ref; } }
                                handleSearch={ debounce( this.handleSearch, 1000 ) }
                            />
                        </div>
                        <span>
                            <div>
                                { this.state.label }
                                &nbsp;
                                <Button
                                    label={ <span><Icon name="filter" className="filter-icon" /> Filter</span> }
                                    type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                    onClick={ this.toggleFilter }
                                />
                            </div>
                        </span>
                        {this.tableHasSelectedRows() &&
                            <span className="sl-u-gap-left--sm">
                                <SalDropdown
                                    dropdownItems={ this.filterDropdownItems( this.state.dropdownItems ) }
                                />
                            </span>
                        }
                    </div>
                    <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                        <Filter
                            filterData={ this.props.filterData }
                            onCancel={ this.resetFilters }
                            onApply={ this.applyFilters }
                        />
                    </div>

                    <AsyncTable
                        data={ this.props.data }
                        columns={ this.getTableColumns() }
                        onDataChange={ this.handleDataChange }
                        onPageChange={ this.handlePageChange }
                        ref={ ( ref ) => { this.table = ref; } }
                        onRowClick={ this.goToDetails }
                        pagination
                        loading={ this.props.loading }
                        pages={ this.state.pagination.totalPages }
                        onPageSizeChange={ this.handlePageSizeChange }
                        pageSize={ this.state.pagination.pageSize }
                        tablePage={ this.state.pagination.page - 1 }
                        selectable
                        manual
                        defaultSorted={ [{ id: 'employee_name', desc: false }] }
                        onSelectionChange={ () => {
                            this.setSelectedValues();
                        } }
                    />
                </div>
            </div>
        );
    }
}

export default FiledLeave;
