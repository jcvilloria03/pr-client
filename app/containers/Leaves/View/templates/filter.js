import React from 'react';
import MultiSelect from '../../../../components/MultiSelect';
import Button from '../../../../components/Button';

import { FilterWrapper } from './styles';

import { FILTER_TYPES } from './constants';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {

    static propTypes = {
        filterData: React.PropTypes.shape({
            leaveTypes: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    onApply = () => {
        const filters = {
            leaveTypes: [],
            locations: [],
            departments: []
        };

        this.leaveTypes.state.value && this.leaveTypes.state.value.forEach( ( leaveType ) => {
            filters.leaveTypes.push( Object.assign( leaveType, { type: FILTER_TYPES.LEAVE_TYPE }) );
        });
        this.locations.state.value && this.locations.state.value.forEach( ( location ) => {
            filters.locations.push( Object.assign( location, { type: FILTER_TYPES.LOCATION }) );
        });
        this.departments.state.value && this.departments.state.value.forEach( ( department ) => {
            filters.departments.push( Object.assign( department, { type: FILTER_TYPES.DEPARTMENT }) );
        });

        this.props.onApply( filters );
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    getFilterPropsValues( filterValues ) {
        if ( !this.props.filterData[ filterValues ]) {
            return [];
        }

        return this.props.filterData[ filterValues ].map( ( filterValue ) => (
            this.formatDataForMultiselect( filterValue )
        ) );
    }

    resetFilters = () => {
        this.leaveTypes.setState({ value: null });
        this.locations.setState({ value: null });
        this.departments.setState({ value: null });
    }

    formatDataForMultiselect = ( data ) => (
        {
            value: data.id,
            label: data.name || `${data.first_name} ${data.last_name} ${data.employee_id}`,
            disabled: false
        }
    )

    render() {
        const filterParamsNames = {
            leaveTypes: 'leaveTypes',
            departments: 'departments',
            locations: 'locations'

        };
        return (
            <FilterWrapper>
                <div className="row">
                    <div className="col-xs-3">
                        <MultiSelect
                            id="leaveTypes"
                            label={
                                <span>Leave Types</span>
                            }
                            ref={ ( ref ) => { this.leaveTypes = ref; } }
                            data={ this.getFilterPropsValues( filterParamsNames.leaveTypes ) }
                            placeholder="All leave types"
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            id="locations"
                            label={
                                <span>Locations</span>
                            }
                            ref={ ( ref ) => { this.locations = ref; } }
                            data={ this.getFilterPropsValues( filterParamsNames.locations ) }
                            placeholder="All locations"
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            id="departments"
                            label={
                                <span>Departments</span>
                            }
                            ref={ ( ref ) => { this.departments = ref; } }
                            data={ this.getFilterPropsValues( filterParamsNames.departments ) }
                            placeholder="All departments"
                        />
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
