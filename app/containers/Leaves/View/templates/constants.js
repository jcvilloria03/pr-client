export const FILTER_TYPES = {
    LEAVE_TYPE: 'leaveType',
    LOCATION: 'location',
    DEPARTMENT: 'department'
};

export const UNLIMITED = 'Unlimited';
