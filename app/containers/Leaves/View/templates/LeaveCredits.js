import React from 'react';
import debounce from 'lodash/debounce';
import orderBy from 'lodash/orderBy';
import first from 'lodash/first';
import isEmpty from 'lodash/isEmpty';

import { H2, H3, H5, P } from '../../../../components/Typography';
import Button from '../../../../components/Button';
import SalDropdown from '../../../../components/SalDropdown';
import AsyncTable from '../../../../components/AsyncTable';
import Search from '../../../../components/Search';
import Icon from '../../../../components/Icon';
import SalConfirm from '../../../../components/SalConfirm';
import Filter from './filter';
import { UNLIMITED } from './constants';
import A from '../../../../components/A';
import { browserHistory } from '../../../../utils/BrowserHistory';
import { filterHelper } from '../../../../utils/filterHelper';
import { DATE_FORMATS, FILED_LEAVE_UNITS_SINGULAR } from '../../../../utils/constants';
import {
    formatPaginationLabel,
    formatDate
} from '../../../../utils/functions';

import {
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from '../styles';

/**
 *
 * FiledLeave Component
 *
 */
class LeaveCredits extends React.Component {
    static propTypes = {
        data: React.PropTypes.array,
        fetchData: React.PropTypes.func,
        exportLeaveCredits: React.PropTypes.func,
        deleteSelectedData: React.PropTypes.func,
        initialLoading: React.PropTypes.bool,
        loading: React.PropTypes.bool,
        permission: React.PropTypes.object,
        filterData: React.PropTypes.shape({
            leaveTypes: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        getDownloadUrl: React.PropTypes.func,
        downloadUrl: React.PropTypes.string,
        jobId: React.PropTypes.string
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            selectedValues: [],
            deleteModalLabel: '',
            dropdownItems: [
                {
                    label: 'Delete',
                    children: <div>Delete</div>,
                    onClick: this.handleDeleteModal
                },
                {
                    label: 'Download',
                    children: <div>Download</div>,
                    onClick: this.exportLeaveCredits
                }
            ],
            pagination: {
                page: 0,
                pageSize: 10,
                totalPages: 0,
                dataLength: 0
            },
            sorting: {
                by: 'employee_name',
                order: 'asc'
            },
            filterData: {},
            downloadUrl: '',
            jobId: ''
        };
    }

    /**
     * Handle new props
     */
    componentWillReceiveProps( newProps ) {
        if ( !isEmpty( newProps.pagination ) ) {
            this.setLeaveCreditsPagination( newProps.pagination );
        }
        if ( !isEmpty( newProps.jobId ) ) {
            if ( !isEmpty( newProps.downloadUrl ) && newProps.downloadUrl !== this.props.downloadUrl ) {
                this.downloadFile( newProps.downloadUrl );
            } else {
                this.getDownloadUrl( newProps.jobId );
            }
        }
    }

    /**
     * Get employee leave credits.
     * @param {Object} data
     */
    getLeaveCredits() {
        this.props.fetchData({
            term: this.state.term,
            page: this.state.pagination.page,
            per_page: this.state.pagination.pageSize,
            sort_by: this.state.sorting.by,
            sort_order: this.state.sorting.order,
            ...this.state.filterData
        });
    }

    /**
     * Sets leave credits Pagination
     */
    setLeaveCreditsPagination = ( leaveCreditsPagination ) => {
        if ( !this.table || !this.table.tableComponent ) {
            return;
        }

        const pagination = {
            page: leaveCreditsPagination.current_page,
            pageSize: leaveCreditsPagination.per_page,
            totalPages: leaveCreditsPagination.total_pages,
            dataLength: leaveCreditsPagination.total
        };

        this.setState({
            pagination,
            label: formatPaginationLabel({
                page: leaveCreditsPagination.current_page,
                pageSize: leaveCreditsPagination.per_page,
                dataLength: leaveCreditsPagination.total
            })
        });
    }

    /**
     * Gets ids of selected values from table.
     * Sets valueIds state.
     * @returns {Array}
     */
    setSelectedValues = () => {
        const selectedValues = [];

        this.table.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedValues.push( this.table.props.data[ index ]);
            }
        });

        this.setState({ selectedValues });

        return this.state.selectedValues.map( ( selectedValue ) => selectedValue.id );
    }

    /**
     * Gets selected values ids.
     * @returns {Array}
     */
    getSelectedValuesIds = () => this.state.selectedValues.map( ( selectedValue ) => selectedValue.id )

    /**
     * Get table columns information.
     * @return {Array}
     */
    getTableColumns() {
        return [
            {
                header: 'Employee ID',
                accessor: 'employee_id',
                minWidth: 150,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { row.employee.employee_id }
                    </div>
                )

            },
            {
                header: 'Employee Name',
                accessor: 'employee_name',
                minWidth: 200,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { row.employee.full_name }
                    </div>
                )
            },
            {
                header: 'Leave Type',
                accessor: 'leave_type_name',
                minWidth: 150,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { row.leave_type.name }
                    </div>
                )
            },
            {
                header: 'Remaining units',
                accessor: 'remaining',
                minWidth: 260,
                sortable: true,
                render: ({ row }) => (
                    <div>
                        { row.leave_type.leave_credit_required ? `${row.value} ${parseFloat( row.value ) === 1 ? FILED_LEAVE_UNITS_SINGULAR[ row.unit ] : row.unit}` : UNLIMITED }
                    </div>
                )
            },
            {
                header: 'Updated By',
                accessor: 'updated_by',
                minWidth: 200,
                sortable: false,
                render: ({ row }) => (
                    <div>
                        { row.updated_by }
                    </div>
                )
            },
            {
                header: 'Updated On',
                accessor: 'updated_at',
                minWidth: 200,
                sortable: false,
                render: ({ row }) => (
                    <div>{formatDate( row.updated_on, DATE_FORMATS.DISPLAY )}</div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        className={ this.props.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => row.leave_type.leave_credit_required && browserHistory.pushWithParam( `/time/leaves/leave-credits/${row.id}/edit`, true, { editView: true }) }
                        disabled={ !row.leave_type.leave_credit_required }
                    />
                )
            }
        ];
    }

    /**
     * Sets deleteModalLabel
     */
    setDeleteModalLabel = () => {
        this.setState({
            deleteModalLabel: this.getDeleteLabelText()
        });
    }

    /**
     * Gets singular or plural noun.
     * @returns {String}
     */
    getDeleteLabelText() {
        return `You are about to delete ${this.state.selectedValues.length}
            leave credit${this.state.selectedValues.length > 1 ? 's' : ''}.
            Do you want to proceed?`;
    }

    /**
     * Get download url
     */
    getDownloadUrl = ( jobId ) => {
        this.props.getDownloadUrl({ jobId });
    };

    /**
     * handles filters and search inputs.
     */
    handleSearch = ( term ) => {
        if ( term !== this.state.term ) {
            const page = 1;
            this.setState({
                term,
                pagination: {
                    ...this.state.pagination,
                    page
                }
            }, () => {
                this.getLeaveCredits();
            });
        }
    }

    /**
     * Handles page size change and calls getLeaveCredits on change.
     * @param {Number} pageSize
     */
    handlePageSizeChange = ( pageSize ) => {
        if ( !pageSize ) {
            return;
        }
        const page = 1;
        this.setState({
            pagination: {
                ...this.state.pagination,
                page,
                pageSize
            }
        }, () => {
            this.getLeaveCredits();
        });
    }

    /**
     * Handle fetching table data.
     * @param {Oject} data
     */
    handlePageChange = ( page ) => {
        this.setState({
            pagination: {
                ...this.state.pagination,
                page
            }
        }, () => {
            this.getLeaveCredits();
        });
    }

    /**
     * Downloads file to user
     */
    downloadFile = ( downloadUrl = this.props.downloadUrl ) => {
        if ( downloadUrl ) {
            const linkElement = document.getElementById( 'downloadLink' );
            linkElement.href = downloadUrl;
            linkElement.click();
            linkElement.href = '';
        } else {
            this.getDownloadUrl( this.props.jobId );
        }
    }

    /**
     * Handle changes to table data.
     * @param {Object} tableProps
     */
    handleDataChange = ( tableProps ) => {
        if ( !tableProps.sorting || !tableProps.sorting.length ) return;

        const sortCondition = first( tableProps.sorting );

        this.setState({
            sorting: {
                by: sortCondition.id,
                order: sortCondition.desc ? 'desc' : 'asc'
            }
        }, () => {
            this.getLeaveCredits();
        });
    }

    /**
     * Send a request to export filed leave
     */
    exportLeaveCredits = () => {
        this.setSelectedValues();

        const sortingOptions = this.table.state.sorting;

        const sorted = orderBy(
            this.state.selectedValues,
            sortingOptions.map( ( o ) => o.id ),
            sortingOptions.map( ( o ) => ( o.desc ? 'desc' : 'asc' ) )
        );

        if ( !sorted.length ) {
            return;
        }

        const payload = {
            ...this.state.filter,
            term: this.state.term
        };

        if ( this.table.headerSelect.checked ) {
            payload.download_all = true;
        } else {
            payload.leave_credits_ids = sorted.map( ( item ) => item.id );
        }

        // dispatch an action to send a request to API
        this.setState({
            downloadUrl: '',
            jobId: ''
        }, () => {
            this.props.exportLeaveCredits( payload );
        });
    };

    applyFilters = ( filters ) => {
        const filterData = filterHelper.mapLeavesFilterDataIds( filters );
        const page = 1;
        const pageSize = 10;
        this.setState({
            hasFiltersApplied: !!filters.length,
            filterData,
            pagination: {
                ...this.state.pagination,
                pageSize,
                page
            }
        }, () => {
            this.getLeaveCredits();
        });

        this.toggleFilter();
    }

    /**
     * Toggles filters.
     */
    toggleFilter = () => {
        if ( this.props.loading || this.filterDataIsNotFetched() ) {
            return;
        }

        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    /**
     * Resets filters data.
     */
    resetFilters = () => {
        this.setState({
            showFilter: false,
            hasFiltersApplied: false,
            filterData: {}
        });
    }

    /**
     * Navigates to Ta client add leave type page.
     */
    navigateToAddLeaveType = () => {
        window.location.href = '/company-settings/leave-settings/leave-type/add';
    }

    /**
     * Check is table mounted.
     * @returns {Boolean}
     */
    tableIsNotMounted() {
        return !this.table ||
            !this.table.tableComponent ||
            !this.table.tableComponent.state;
    }

    /**
     * Checks is filterData fetched.
     * @returns {Boolean}
     */
    filterDataIsNotFetched() {
        return !this.props.filterData ||
            isEmpty( this.props.filterData.departments ) ||
            !this.props.filterData.departments.length;
    }

    /**
     * Handles showing delete modal.
     */
    handleDeleteModal = () => {
        this.setSelectedValues();
        this.setDeleteModalLabel();

        if ( !this.state.selectedValues || !this.state.selectedValues.length ) {
            return;
        }

        this.setState({ showModal: false }, () => {
            this.setState({ showModal: true });
        });
    }

    /**
     * Checks is any table row selected.
     * @returns {Boolean}
     */
    tableHasSelectedRows() {
        if ( this.tableIsNotMounted() ) {
            return false;
        }

        return !!this.table.state.selected.filter( ( item ) => item ).length;
    }

    /**
     * send a request to delete
     */
    deleteSelectedData = () => {
        const valueIds = this.getSelectedValuesIds();

        if ( !valueIds || !valueIds.length ) {
            return;
        }

        this.props.deleteSelectedData( valueIds, this.state.filterData );
    }

    /**
     * Routes to single leaveCredit details page.
     * @param {Object} rowInfo
     * @param {Object} column
     * @returns {Object}
     */
    goToLeaveCreditDetails = ( state, rowInfo, column ) => ({
        onDoubleClick: () => {
            if ( column.id !== 'select' && rowInfo.row.leave_type.leave_credit_required ) {
                browserHistory.pushWithParam( `/time/leaves/leave-credits/${rowInfo.row.id}/edit`, true, { editView: false });
            }
        }
    })

    render() {
        return (
            <div className="content">
                <SalConfirm
                    onConfirm={ this.deleteSelectedData }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                { this.state.deleteModalLabel }
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Leave Credits"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <div className="heading">
                    <H3>Leave Credits</H3>
                    <P>You may manage and view employees&#39; leave credits, leave types and filed leaves through this page.</P>
                    <div style={ { textAlign: 'center' } } >
                        <div className="main">
                            <Button
                                className={ this.props.permission.create ? '' : 'hide' }
                                label="Add Leave Credit"
                                size="large"
                                type="action"
                                onClick={ () => browserHistory.push( '/time/leave-credits/add', true ) }
                            />
                            <Button
                                className={ this.props.permission.create ? '' : 'hide' }
                                label="Add Leave Type"
                                size="large"
                                type="neutral"
                                onClick={ this.navigateToAddLeaveType }
                            />
                        </div>
                    </div>
                </div>
                <div className="loader" style={ { display: this.props.initialLoading ? '' : 'none' } }>
                    <LoadingStyles>
                        <H2>Loading Leave Credits.</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoadingStyles>
                </div>
                <div className="content" style={ { display: this.props.initialLoading ? 'none' : '' } }>
                    <div className="title">
                        <H5>Leave Credits List</H5>
                        <div className="search-wrapper">
                            <Search
                                ref={ ( ref ) => { this.search = ref; } }
                                handleSearch={ debounce( this.handleSearch, 1000 ) }
                            />
                        </div>
                        <span>
                            <div>
                                { this.state.label }
                                &nbsp;
                                <Button
                                    label={ <span><Icon name="filter" className="filter-icon" /> Filter</span> }
                                    type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                    onClick={ this.toggleFilter }
                                />
                            </div>
                        </span>
                        {this.tableHasSelectedRows() &&
                            <span className="sl-u-gap-left--sm">
                                <SalDropdown
                                    dropdownItems={ this.state.dropdownItems }
                                />
                            </span>
                        }
                    </div>
                    <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                        <Filter
                            filterData={ this.props.filterData }
                            onCancel={ this.resetFilters }
                            onApply={ this.applyFilters }
                        />
                    </div>

                    <AsyncTable
                        data={ this.props.data }
                        columns={ this.getTableColumns() }
                        onDataChange={ this.handleDataChange }
                        onPageChange={ this.handlePageChange }
                        ref={ ( ref ) => { this.table = ref; } }
                        onRowClick={ this.goToLeaveCreditDetails }
                        pagination
                        loading={ this.props.loading }
                        pages={ this.state.pagination.totalPages }
                        onPageSizeChange={ this.handlePageSizeChange }
                        pageSize={ this.state.pagination.pageSize }
                        tablePage={ this.state.pagination.page - 1 }
                        manual
                        selectable
                        defaultSorted={ [{ id: 'employee_name', desc: false }] }
                        onSelectionChange={ () => {
                            this.setSelectedValues();
                        } }
                    />
                    <A download id="downloadLink"></A>
                </div>
            </div>
        );
    }
}

export default LeaveCredits;
