import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectViewLeavesDomain = () => ( state ) => state.get( 'leaves' );

const makeSelectInitialLoading = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'initialLoading' )
);

const makeSelectLoadingLeaveCredits = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'loadingLeaveCredits' )
);

const makeSelectLoadingFiledLeave = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'loadingFiledLeave' )
);

const makeSelectDownloading = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'downloading' )
);

const makeSelectLeaveCredits = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'leaveCredits' ).toJS()
);

const makeSelectFilterData = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'filterData' ).toJS()
);

const makeSelectLeaveCreditsPagination = () => createSelector(
    selectViewLeavesDomain(),
  ( substate ) => substate.get( 'leaveCreditsPagination' ).toJS()
);

const makeSelectFiledLeave = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'filedLeave' ).toJS()
);

const makeSelectFiledLeavePagination = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'filedLeavePagination' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectDownloadUrl = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'downloadUrl' )
);

const makeSelectJobId = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'jobId' )
);

const makeSelectFiledLeaveJobId = () => createSelector(
    selectViewLeavesDomain(),
    ( substate ) => substate.get( 'filedLeaveJobId' )
);

export {
    makeSelectInitialLoading,
    makeSelectLoadingLeaveCredits,
    makeSelectLoadingFiledLeave,
    makeSelectDownloading,
    makeSelectLeaveCredits,
    makeSelectFilterData,
    makeSelectLeaveCreditsPagination,
    makeSelectFiledLeave,
    makeSelectFiledLeavePagination,
    makeSelectNotification,
    makeSelectDownloadUrl,
    makeSelectJobId,
    makeSelectFiledLeaveJobId
};
