import { isEmpty } from 'lodash';
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import {
    SET_INITIAL_LOADING,
    SET_LOADING_LEAVE_CREDITS,
    SET_LOADING_FILED_LEAVE,
    GET_LEAVES_DATA,
    GET_LEAVE_CREDITS,
    GET_FILED_LEAVE,
    SET_LEAVE_CREDITS,
    SET_LEAVE_CREDITS_PAGINATION,
    SET_FILED_LEAVE,
    SET_FILED_LEAVE_PAGINATION,
    SET_FILTER_DATA,
    DELETE_LEAVE_CREDITS,
    DELETE_FILED_LEAVE,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    EXPORT_LEAVE_CREDITS,
    EXPORT_FILED_LEAVE,
    SET_DOWNLOADING,
    GET_PAGINATED_LEAVE_CREDITS_DATA,
    SET_DOWNLOAD_URL,
    SET_JOB_ID,
    GET_DOWNLOAD_URL,
    SET_FILED_LEAVE_JOB_ID,
    GET_FILED_LEAVE_DOWNLOAD_URL
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

/**
 * Gets inital Leave Data.
 */
export function* getLeavesData() {
    try {
        yield put({
            type: SET_INITIAL_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();

        const filterData = {};

        const [
            leaveTypes,
            locations,
            departments
        ] = yield [
            call( Fetch, `/company/${companyId}/leave_types`, { method: 'GET' }),
            call( Fetch, `/philippine/company/${companyId}/locations`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/departments`, { method: 'GET' })
        ];

        filterData.leaveTypes = leaveTypes.data;
        filterData.locations = locations.data;
        filterData.departments = departments.data;

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });

        yield call( getPaginatedLeaveCredits, {
            payload: {
                page: 1,
                perPage: 10,
                showLoading: true,
                filters: [],
                orderBy: 'employee_name',
                orderDir: 'asc'
            }
        });

        yield call( getPaginatedLeaveRequests, {
            payload: {
                page: 1,
                perPage: 10,
                showLoading: true,
                filters: [],
                orderBy: 'employee_name',
                orderDir: 'asc'
            }
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_INITIAL_LOADING,
            payload: false
        });
    }
}

/**
 * Gets paginated leave credits data
 */
export function* getPaginatedLeaveCredits({ payload }) {
    try {
        const { page, perPage, showLoading, orderBy, orderDir } = payload;
        yield put({
            type: SET_LOADING_LEAVE_CREDITS,
            payload: showLoading
        });

        const companyId = company.getLastActiveCompanyId();

        const [
            leaveCredits
        ] = yield [
            call( Fetch, `/company/${companyId}/leave_credits?page=${page}&per_page=${perPage}&order_by=${orderBy}&order_dir=${orderDir}`, { method: 'POST', data: { mode: 'simple', filter: payload.filters }})
        ];

        yield put({
            type: SET_LEAVE_CREDITS,
            payload: leaveCredits.data
        });

        yield put({
            type: SET_LEAVE_CREDITS_PAGINATION,
            payload: leaveCredits.meta.pagination
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING_LEAVE_CREDITS,
            payload: false
        });
    }
}

/**
 * Gets paginated leave requests data
 */
export function* getPaginatedLeaveRequests({ payload }) {
    try {
        const { page, perPage, showLoading, filters, orderBy, orderDir } = payload;
        yield put({
            type: SET_LOADING_FILED_LEAVE,
            payload: showLoading
        });

        const companyId = company.getLastActiveCompanyId();

        const [
            filedLeaves
        ] = yield [
            call( Fetch, `/company/${companyId}/leave_requests?page=${page}&per_page=${perPage}&order_by=${orderBy}&order_dir=${orderDir}`, { method: 'POST', data: { mode: 'simple', filter: filters }})
        ];

        yield put({
            type: SET_FILED_LEAVE,
            payload: filedLeaves.data
        });

        yield put({
            type: SET_FILED_LEAVE_PAGINATION,
            payload: filedLeaves.meta.pagination
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING_FILED_LEAVE,
            payload: false
        });
    }
}

/**
 * Get leave credits for table
 * @param {Object} payload
 */
export function* getLeaveCredits({ payload }) {
    try {
        yield put({
            type: SET_LOADING_LEAVE_CREDITS,
            payload: true
        });

        const page = payload && payload.page || 1;
        const perPage = payload && payload.per_page || 10;
        const term = payload && payload.term || '';
        const sortBy = payload && payload.sort_by || null;
        const sortOrder = payload && payload.sort_order || null;
        const locationIds = payload.locations_ids || [];
        const departmentIds = payload.departments_ids || [];
        const leaveTypesIds = payload.leave_types_ids || [];

        yield call( getPaginatedLeaveCredits, {
            payload: {
                page,
                perPage,
                showLoading: true,
                filters: {
                    keyword: term,
                    departments: departmentIds,
                    locations: locationIds,
                    leave_types: leaveTypesIds
                },
                orderBy: sortBy,
                orderDir: sortOrder
            }
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING_LEAVE_CREDITS,
            payload: false
        });
    }
}

/**
 * Batch delete leaveCredits
 */
export function* deleteLeaveCredits({ payload }) {
    try {
        yield call( Fetch, '/leave_credit/bulk_delete', {
            method: 'DELETE',
            data: {
                leave_credits_ids: payload.IDs,
                company_id: company.getLastActiveCompanyId()
            }
        });
        yield call( getLeaveCredits, { payload: { ...payload.filterData }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Batch export leaveCredits
 */
export function* exportLeaveCredits({ payload }) {
    try {
        yield put({
            type: SET_DOWNLOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();

        const response = yield call( Fetch, `/company/${companyId}/leave_credits/download`, {
            method: 'POST',
            data: {
                ...payload,
                company_id: company.getLastActiveCompanyId()
            }
        });

        yield put({
            type: SET_JOB_ID,
            payload: response.data.job_id
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DOWNLOADING,
            payload: false
        });
    }
}

/**
 * Batch export leaveCredits
 */
export function* getDownloadUrl({ payload }) {
    try {
        yield put({
            type: SET_LOADING_LEAVE_CREDITS,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        let downloadUrl = '';
        // Poll to fetch download url
        while ( isEmpty( downloadUrl ) ) {
            const response = yield call( Fetch, `/company/${companyId}/leave_credits/download/status`, {
                method: 'POST',
                data: {
                    job_id: payload.jobId
                }
            });
            downloadUrl = response.download_url;
            if ( !isEmpty( downloadUrl ) ) {
                yield put({
                    type: SET_DOWNLOAD_URL,
                    payload: response.download_url
                });
                yield put({
                    type: SET_JOB_ID,
                    payload: ''
                });
            }
            yield call( delay, 1500 );
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING_LEAVE_CREDITS,
            payload: false
        });
    }
}

/**
 * Get filed leave for table
 * @param {Object} payload
 */
export function* getFiledLeave({ payload }) {
    try {
        yield put({
            type: SET_LOADING_FILED_LEAVE,
            payload: true
        });

        const page = payload && payload.page || 1;
        const perPage = payload && payload.per_page || 10;
        const term = payload && payload.term || '';

        yield call( getPaginatedLeaveRequests, {
            payload: {
                page,
                perPage,
                showLoading: true,
                filters: {
                    keyword: term,
                    departments: payload.departments_ids,
                    locations: payload.locations_ids,
                    leave_types: payload.leave_types_ids
                },
                orderBy: payload.sort_by ? payload.sort_by : '',
                orderDir: payload.sort_order ? payload.sort_order : ''
            }
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING_FILED_LEAVE,
            payload: false
        });
    }
}

/**
 * Batch export filed leave
 */
export function* exportFiledLeave({ payload }) {
    try {
        yield put({
            type: SET_DOWNLOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();

        const response = yield call( Fetch, `/company/${companyId}/leave_requests/download`, {
            method: 'POST',
            data: {
                ...payload,
                company_id: company.getLastActiveCompanyId()
            }
        });

        yield put({
            type: SET_FILED_LEAVE_JOB_ID,
            payload: response.data.job_id
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DOWNLOADING,
            payload: false
        });
    }
}

/**
 * Batch export FiledLeave
 */
export function* getFiledLeaveDownloadUrl({ payload }) {
    try {
        yield put({
            type: SET_LOADING_FILED_LEAVE,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        let downloadUrl = '';
        // Poll to fetch download url
        while ( isEmpty( downloadUrl ) ) {
            const response = yield call( Fetch, `/company/${companyId}/leave_requests/download/status`, {
                method: 'POST',
                data: {
                    job_id: payload.filedLeaveJobId
                }
            });
            downloadUrl = response.download_url;
            if ( !isEmpty( downloadUrl ) ) {
                yield put({
                    type: SET_DOWNLOAD_URL,
                    payload: response.download_url
                });
                yield put({
                    type: SET_FILED_LEAVE_JOB_ID,
                    payload: ''
                });
            }
            yield call( delay, 1500 );
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING_FILED_LEAVE,
            payload: false
        });
    }
}

/**
 * Batch delete filedLeave
 */
export function* deleteFiledLeave({ payload }) {
    try {
        yield call( Fetch, '/leave_request/bulk_delete', {
            method: 'DELETE',
            data: {
                leave_requests_ids: payload.IDs,
                company_id: company.getLastActiveCompanyId()
            }
        });
        yield call( getFiledLeave, { payload: { ...payload.filterData }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}
/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getLeavesData );
}

/**
 * Watcher for GET leaves
 */
export function* watchForGetLeavesData() {
    const watcher = yield takeEvery( GET_LEAVES_DATA, getLeavesData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET leaveCredits Data
 */
export function* watchForGetLeaveCredits() {
    const watcher = yield takeEvery( GET_LEAVE_CREDITS, getLeaveCredits );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET filedLeave Data
 */
export function* watchForGetFiledLeave() {
    const watcher = yield takeEvery( GET_FILED_LEAVE, getFiledLeave );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE leaveCredits
 */
export function* watchForDeleteLeaveCredits() {
    const watcher = yield takeEvery( DELETE_LEAVE_CREDITS, deleteLeaveCredits );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for EXPORT leaveCredits
 */
export function* watchForExportLeaveCredits() {
    const watcher = yield takeEvery( EXPORT_LEAVE_CREDITS, exportLeaveCredits );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE filedLeave
 */
export function* watchForDeleteFiledLeave() {
    const watcher = yield takeEvery( DELETE_FILED_LEAVE, deleteFiledLeave );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for EXPORT filedLeave
 */
export function* watchForExportFiledLeave() {
    const watcher = yield takeEvery( EXPORT_FILED_LEAVE, exportFiledLeave );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for Paginated leaves data
 */
export function* watchForPaginatedLeaveCreditsData() {
    const watcher = yield takeEvery( GET_PAGINATED_LEAVE_CREDITS_DATA, getPaginatedLeaveCredits );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for get download url
 */
export function* watchForGetDownloadUrl() {
    const watcher = yield takeEvery( GET_DOWNLOAD_URL, getDownloadUrl );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for get download url
 */
export function* watchForGetFileLeaveDownloadUrl() {
    const watcher = yield takeEvery( GET_FILED_LEAVE_DOWNLOAD_URL, getFiledLeaveDownloadUrl );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
// All sagas to be loaded
export default [
    watchForGetLeavesData,
    watchForGetLeaveCredits,
    watchForDeleteLeaveCredits,
    watchForExportLeaveCredits,
    watchForGetFiledLeave,
    watchForDeleteFiledLeave,
    watchForExportFiledLeave,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForPaginatedLeaveCreditsData,
    watchForGetDownloadUrl,
    watchForGetFileLeaveDownloadUrl
];
