import {
    GET_LEAVES_DATA,
    GET_LEAVE_CREDITS,
    GET_FILED_LEAVE,
    DELETE_LEAVE_CREDITS,
    DELETE_FILED_LEAVE,
    NOTIFICATION,
    EXPORT_LEAVE_CREDITS,
    EXPORT_FILED_LEAVE,
    GET_PAGINATED_LEAVE_CREDITS_DATA,
    GET_PAGINATED_LEAVE_REQUESTS_DATA,
    GET_DOWNLOAD_URL,
    GET_FILED_LEAVE_DOWNLOAD_URL
} from './constants';

/**
 * Fetch list of leaveCredits.
 * @param {Object} data
 * @returns {Object}
 */
export function getLeavesData( data ) {
    return {
        type: GET_LEAVES_DATA,
        payload: data
    };
}
/**
 * Get Download Url.
 * @param {Object} data
 * @returns {Object}
 */
export function getDownloadUrl( data ) {
    return {
        type: GET_DOWNLOAD_URL,
        payload: data
    };
}
/**
 * Get Filed Leave Download Url.
 * @param {Object} data
 * @returns {Object}
 */
export function getFiledLeaveDownloadUrl( data ) {
    return {
        type: GET_FILED_LEAVE_DOWNLOAD_URL,
        payload: data
    };
}

/**
 * Fetch paginated list of leaveCredits.
 * @param {Object} data
 * @returns {Object}
 */
export function getPaginatedLeaveCreditsgetPaginatedLeaveCredits( payload ) {
    return {
        type: GET_PAGINATED_LEAVE_CREDITS_DATA,
        payload
    };
}

/**
 * Fetch paginated list of leaveRequests.
 * @param {Object} data
 * @returns {Object}
 */
export function getPaginatedLeaveRequests( payload ) {
    return {
        type: GET_PAGINATED_LEAVE_REQUESTS_DATA,
        payload
    };
}
/**
 * Fetch list of leaveCredits data.
 * @param {Object} data
 * @returns {Object}
 */
export function getLeaveCredits( data ) {
    return {
        type: GET_LEAVE_CREDITS,
        payload: data
    };
}

/**
 * Fetch list of filedLeave data.
 * @param {Object} data
 * @returns {Object}
 */
export function getFiledLeave( data ) {
    return {
        type: GET_FILED_LEAVE,
        payload: data
    };
}

/**
 * Delete leaveCredits
 * @param {Array} IDs
 * @returns {Object}
 */
export function deleteLeaveCredits( IDs, filterData ) {
    return {
        type: DELETE_LEAVE_CREDITS,
        payload: {
            IDs,
            filterData
        }
    };
}

/**
 * Delete filedLeave
 * @param {Array} IDs
 * @returns {Object}
 */
export function deleteFiledLeave( IDs, filterData ) {
    return {
        type: DELETE_FILED_LEAVE,
        payload: {
            IDs,
            filterData
        }
    };
}

/**
 * Export leaveCredits
 * @param {Array} IDs
 * @returns {Object}
 */
export function exportLeaveCredits( IDs ) {
    return {
        type: EXPORT_LEAVE_CREDITS,
        payload: IDs
    };
}

/**
 * Export filedLeave
 * @param {Array} IDs
 * @returns {Object}
 */
export function exportFiledLeave( IDs ) {
    return {
        type: EXPORT_FILED_LEAVE,
        payload: IDs
    };
}

/**
 * Display a notification in page
 * @param {Boolean} show
 * @param {String} title
 * @param {String} message
 * @param {String} type
 * @returns {Object}
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
