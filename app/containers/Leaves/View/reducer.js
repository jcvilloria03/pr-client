import { fromJS } from 'immutable';
import {
    SET_INITIAL_LOADING,
    SET_LOADING_LEAVE_CREDITS,
    SET_LOADING_FILED_LEAVE,
    SET_LEAVE_CREDITS,
    SET_LEAVE_CREDITS_PAGINATION,
    SET_FILED_LEAVE,
    SET_FILED_LEAVE_PAGINATION,
    SET_FILTER_DATA,
    NOTIFICATION_SAGA,
    SET_DOWNLOADING,
    SET_JOB_ID,
    SET_DOWNLOAD_URL,
    SET_FILED_LEAVE_JOB_ID
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    initialLoading: false,
    loadingLeaveCredits: false,
    loadingFiledLeave: false,
    downloading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    leaveCredits: [],
    leaveCreditsPagination: {},
    filedLeave: [],
    filedLeavePagination: {},
    filterData: {},
    downloadUrl: '',
    jobId: ''
});

/**
 * LeaveCredits view reducer
 */
function leaveCreditsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_DOWNLOADING:
            return state.set( 'downloading', action.payload );
        case SET_INITIAL_LOADING:
            return state.set( 'initialLoading', action.payload );
        case SET_LOADING_LEAVE_CREDITS:
            return state.set( 'loadingLeaveCredits', action.payload );
        case SET_LOADING_FILED_LEAVE:
            return state.set( 'loadingFiledLeave', action.payload );
        case SET_LEAVE_CREDITS:
            return state.set( 'leaveCredits', fromJS( action.payload ) );
        case SET_LEAVE_CREDITS_PAGINATION:
            return state.set( 'leaveCreditsPagination', fromJS( action.payload ) );
        case SET_FILED_LEAVE:
            return state.set( 'filedLeave', fromJS( action.payload ) );
        case SET_FILED_LEAVE_PAGINATION:
            return state.set( 'filedLeavePagination', fromJS( action.payload ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_DOWNLOAD_URL:
            return state.set( 'downloadUrl', fromJS( action.payload ) );
        case SET_JOB_ID:
            return state.set( 'jobId', fromJS( action.payload ) );
        case SET_FILED_LEAVE_JOB_ID:
            return state.set( 'filedLeaveJobId', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default leaveCreditsReducer;
