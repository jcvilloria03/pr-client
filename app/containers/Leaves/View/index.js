import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import LeaveCredits from './templates/LeaveCredits';
import FiledLeave from './templates/FiledLeave';
import SubHeader from '../../SubHeader';
import SnackBar from '../../../components/SnackBar';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import {
    makeSelectLoadingLeaveCredits,
    makeSelectLoadingFiledLeave,
    makeSelectLeaveCredits,
    makeSelectLeaveCreditsPagination,
    makeSelectFiledLeave,
    makeSelectFiledLeavePagination,
    makeSelectFilterData,
    makeSelectNotification,
    makeSelectDownloading,
    makeSelectInitialLoading,
    makeSelectDownloadUrl,
    makeSelectJobId,
    makeSelectFiledLeaveJobId
} from './selectors';
import * as leaveCreditsActions from './actions';

import { PageWrapper } from './styles';

/**
 * View Leave Component
 */
export class View extends React.Component {
    static propTypes = {
        deleteFiledLeave: React.PropTypes.func,
        deleteLeaveCredits: React.PropTypes.func,
        exportFiledLeave: React.PropTypes.func,
        exportLeaveCredits: React.PropTypes.func,
        filedLeave: React.PropTypes.array,
        filedLeavePagination: React.PropTypes.object,
        getFiledLeave: React.PropTypes.func,
        getLeaveCredits: React.PropTypes.func,
        getLeavesData: React.PropTypes.func,
        leaveCredits: React.PropTypes.array,
        leaveCreditsPagination: React.PropTypes.object,
        filterData: React.PropTypes.shape({
            leaveTypes: React.PropTypes.array,
            positions: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array,
            employees: React.PropTypes.array
        }),
        initialLoading: React.PropTypes.bool,
        loadingLeaveCredits: React.PropTypes.bool,
        loadingFiledLeave: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array,
        getPaginatedLeaveCredits: React.PropTypes.func,
        downloadUrl: React.PropTypes.string,
        jobId: React.PropTypes.string,
        getDownloadUrl: React.PropTypes.func,
        getFiledLeaveDownloadUrl: React.PropTypes.func,
        filedLeaveJobId: React.PropTypes.string
    };

    static defaultProps = {
        loading: true,
        initialLoading: true,
        downloading: false,
        errors: {},
        page: 1
    };
    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            showDelete: false,
            deleteModalLabel: '',
            term: '',
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            showModal: false,
            moreActionsVisible: false,
            showFilter: false,
            hasFiltersApplied: false,
            currentPage: this.props.leaveCreditsPagination.current_page,
            perPage: this.props.leaveCreditsPagination.per_page,
            selectedValues: [],
            keysMap: {
                employee_id: 'employee_id',
                updated_on: 'updated_on',
                updated_by: 'updated_by',
                remaining_units: 'value'
            }
        };
    }

    /**
     * Page authorization check
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.leave_credit',
            'create.leave_credit',
            'delete.leave_credit',
            'edit.leave_credit'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.leave_credit' ];

            if ( authorized ) {
                this.setState({ permission: {
                    view: authorization[ 'view.leave_credit' ],
                    create: authorization[ 'create.leave_credit' ],
                    delete: authorization[ 'delete.leave_credit' ],
                    edit: authorization[ 'edit.leave_credit' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    /**
     * Fetch leaveCredits list
     * Fetch filtersData
     */
    componentDidMount() {
        this.props.getLeavesData();
    }

    render() {
        return (
            <div>
                <Helmet
                    title="View Leaves"
                    meta={ [
                        { name: 'description', content: 'View Leaves' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <Container>
                        <LeaveCredits
                            data={ this.props.leaveCredits }
                            fetchData={ this.props.getLeaveCredits }
                            exportLeaveCredits={ this.props.exportLeaveCredits }
                            deleteSelectedData={ this.props.deleteLeaveCredits }
                            pagination={ this.props.leaveCreditsPagination }
                            getPaginatedLeaveCredits={ this.props.getPaginatedLeaveCredits }
                            initialLoading={ this.props.initialLoading }
                            loading={ this.props.loadingLeaveCredits }
                            filterData={ this.props.filterData }
                            permission={ this.state.permission }
                            getDownloadUrl={ this.props.getDownloadUrl }
                            downloadUrl={ this.props.downloadUrl }
                            jobId={ this.props.jobId }
                        />
                        <FiledLeave
                            data={ this.props.filedLeave }
                            fetchData={ this.props.getFiledLeave }
                            exportFiledLeaves={ this.props.exportFiledLeave }
                            deleteSelectedData={ this.props.deleteFiledLeave }
                            pagination={ this.props.filedLeavePagination }
                            initialLoading={ this.props.initialLoading }
                            loading={ this.props.loadingFiledLeave }
                            filterData={ this.props.filterData }
                            permission={ this.state.permission }
                            getDownloadUrl={ this.props.getFiledLeaveDownloadUrl }
                            downloadUrl={ this.props.downloadUrl }
                            filedLeaveJobId={ this.props.filedLeaveJobId }
                        />
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    initialLoading: makeSelectInitialLoading(),
    loadingLeaveCredits: makeSelectLoadingLeaveCredits(),
    loadingFiledLeave: makeSelectLoadingFiledLeave(),
    downloading: makeSelectDownloading(),
    leaveCredits: makeSelectLeaveCredits(),
    leaveCreditsPagination: makeSelectLeaveCreditsPagination(),
    filedLeave: makeSelectFiledLeave(),
    filedLeavePagination: makeSelectFiledLeavePagination(),
    filterData: makeSelectFilterData(),
    notification: makeSelectNotification(),
    downloadUrl: makeSelectDownloadUrl(),
    jobId: makeSelectJobId(),
    filedLeaveJobId: makeSelectFiledLeaveJobId()

});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        leaveCreditsActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
