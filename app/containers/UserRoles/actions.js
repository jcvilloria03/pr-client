/* eslint-disable require-jsdoc */
import {
    GET_USER_ROLES,
    DELETE_USER_ROLE
} from './constants';

export function getUserRoles() {
    return {
        type: GET_USER_ROLES
    };
}

export function deleteUserRole( payload ) {
    return {
        type: DELETE_USER_ROLE,
        payload
    };
}
