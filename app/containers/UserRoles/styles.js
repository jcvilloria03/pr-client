/* eslint-disable no-confusing-arrow */
import styled from 'styled-components';
import { Container } from 'reactstrap';

export const StyledContainer = styled( Container )`
    padding: 80px 0 20px 20px;
    height: 100%;

    .ReactTable .rt-td {
        padding: 14px 20px;
    }

    .btn-add-role {
        margin-top: 56px;
    }

    .user-search {
        margin-top: 21px;
        margin-bottom: 14px;

        h5 {
            font-size: 18px;
            font-weight: 700;
            line-height: 28.8px;
            margin-right: 28px;
        }

        .search {
            width: 200px;

            input, .input-group {
                height: 42px;
            }
        }
    }

    .btn-delete {
        background-color: transparent;
        border: none;

        svg{
            width: 14px;
            height: 14px;
            margin-top:-2px;
        }
    }
`;

export const ActionContainer = styled.div`
    display: flex;
    justify-content: flex-end;
`;

export const HeaderTitle = styled.h1`
    text-align: center;
    margin-bottom: 0;
    font-weight: 700;
    font-size: 36px;
    line-height: 57.6px;
`;

export const SubTitle = styled.p`
    font-size: 14px;
    line-height: 22.4px;
    text-align: center;
    margin: 0;
`;

export const SectionTitle = styled.p`
    font-size: 16px;
    text-align: left;
    margin-top: 36px;
    font-weight: 600;
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const Center = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
`;

export const Padder = styled.div`
    padding-left: ${( props ) => ( props.horizontal ? props.horizontal : 0 )}px;
    padding-right: ${( props ) => ( props.horizontal ? props.horizontal : 0 )}px;
    padding-top: ${( props ) => ( props.vertical ? props.vertical : 0 )}px;
    padding-bottom: ${( props ) => ( props.vertical ? props.vertical : 0 )}px;
`;

export const FlexContainer = styled.div`
    display: flex;
    justify-content: ${( props ) => props.justifyContent};
    align-items: ${( props ) => props.alignItems};
    gap: ${( props ) => props.gap ? props.gap : 0}px;
`;
