import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectUserRolessDomain = () => ( state ) => state.get( 'UserRoles' );

const makeSelectLoading = () => createSelector(
  selectUserRolessDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectUserRolessDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectUserRoles = () => createSelector(
  selectUserRolessDomain(),
  ( substate ) => substate.get( 'userRoles' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectNotification,
  makeSelectUserRoles
};
