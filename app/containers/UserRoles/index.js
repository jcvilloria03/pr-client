import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { cloneDeep, isEqual, startCase } from 'lodash';

import SnackBar from 'components/SnackBar';
import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';
import { H2, H3, H5, P } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Search from 'components/Search';
import Table from 'components/Table';
import Icon from 'components/Icon';
import Modal from 'components/Modal';

import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { formatPaginationLabel } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';

import { makeSelectIsExpired } from 'containers/App/selectors';

import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectUserRoles
} from './selectors';
import * as usersActions from './actions';

import {
    Center,
    HeaderTitle,
    StyledContainer,
    SubTitle,
    FlexContainer,
    LoadingStyles
} from './styles';

/**
 *
 * Users
 *
 */
export class UserRoles extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        isExpired: React.PropTypes.bool,
        getUserRoles: React.PropTypes.func,
        userRoles: React.PropTypes.object,
        deleteUserRole: React.PropTypes.func
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            tableLabel: 'Showing 0-0 of 0 entries',
            showFilter: false,
            filter: {
                role: null,
                status: 'all'
            },
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            },
            searchTerm: '',
            deleteRoleId: null
        };
    }

    componentDidMount() {
        this.props.getUserRoles();
    }

    componentWillReceiveProps( nextProps ) {
        const listChanged = !isEqual( nextProps.userRoles.roles, this.props.userRoles.roles );

        this.setState( ( prevState ) => ({
            tableLabel: formatPaginationLabel( this.userRolesTable.tableComponent.state ),
            pagination: {
                total: nextProps.userRoles.roles.length || 0,
                per_page: 10,
                current_page: listChanged ? 1 : prevState.pagination.current_page,
                last_page: Math.ceil( nextProps.userRoles.roles.length / 10 ) || 0,
                to: 0
            }
        }) );
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.userRolesTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.userRolesTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    onClickDelete( roleId ) {
        this.setState({ deleteRoleId: roleId });
        this.DeleteModal.toggle();
    }

    handleDelete() {
        const roleId = cloneDeep( this.state.deleteRoleId );
        this.setState({ deleteRoleId: null });
        this.DeleteModal.toggle();
        this.props.deleteUserRole( roleId );
    }

    handleTableChanges = ( tableProps = this.userRolesTable.tableComponent.state ) => {
        this.setState({
            tableLabel: formatPaginationLabel( tableProps )
        });
    }

    formatCompaniesDisplay( ids ) {
        const { essentialData } = this.props.userRoles;
        const count = ids.length;
        const { company } = essentialData;
        const selectedCompany = count ? company.find( ( entry ) => entry.id === ids[ 0 ]) : company[ 0 ];

        return count > 1
            ? `${selectedCompany.name} and +${count - 1} more`
            : company.length > 1
                ? `${selectedCompany.name} and +${company.length - 1} more`
                : `${selectedCompany.name}`;
    }

    formatModulesDisplay( data ) {
        const modules = Object.keys( data );

        return modules.filter( ( module ) => module !== 'root' ).map( startCase ).join( ', ' );
    }

    handleSearch = ( term = '' ) => {
        this.setState({ searchTerm: term });
    }

    /**
     *
     * Users render method
     *
     */
    render() {
        const { isExpired, loading, userRoles } = this.props;
        const { roles } = cloneDeep( userRoles );
        const { pagination, tableLabel, searchTerm } = this.state;

        let displayData = roles;

        if ( searchTerm ) {
            const regex = new RegExp( searchTerm, 'i' );
            displayData = displayData.filter( ( row ) => ( regex.test( row.name ) ) );
        }

        const tableColumns = [
            {
                header: 'Name',
                accessor: 'name',
                width: 250
            },
            {
                header: 'Companies',
                accessor: 'company_ids',
                width: 200,
                render: ({ row }) => this.formatCompaniesDisplay( row.company_ids )
            },
            {
                header: 'Permissions',
                accessor: 'policy',
                sortable: false,
                render: ({ row }) => this.formatModulesDisplay( row.policy.resource )
            },
            {
                header: ' ',
                accessor: 'actions',
                sortable: false,
                width: 140,
                render: ({ row }) => (
                    row.is_system_defined ? null :
                    <div>
                        <Button
                            className="btn-edit"
                            label={ <span>Edit</span> }
                            type="grey"
                            size="small"
                            onClick={ () => browserHistory.push( `/control-panel/roles/${row.id}/role-edit`, true ) }
                        />
                        <Button
                            className="btn-delete"
                            label={ <div><Icon name="trash" className="icon" /></div> }
                            type="grey"
                            size="small"
                            onClick={ () => this.onClickDelete( row.id ) }
                        />
                    </div>
                )
            }
        ];

        return (
            <div>
                <Helmet
                    title="User Roles"
                    meta={ [
                        { name: 'description', content: 'List of User Roles' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        isExpired,
                        salpayViewPermission: true,
                        isSubscribedToPayroll: true,
                        accountViewPermission: true
                    }) }
                />

                <Modal
                    title="Confirm Your Action"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.DeleteModal = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'No',
                            onClick: () => {
                                this.setState({ deleteRoleId: null });
                                this.DeleteModal.toggle();
                            }
                        },
                        {
                            type: 'darkRed',
                            label: 'Yes',
                            onClick: () => this.handleDelete()
                        }
                    ] }
                />

                <div className="loader" style={ { display: loading ? '' : 'none' } }>
                    <LoadingStyles>
                        <H2>Loading User Roles.</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoadingStyles>
                </div>
                <div className="content" style={ { display: loading ? 'none' : '' } }>
                    <StyledContainer>
                        <HeaderTitle>User Roles</HeaderTitle>
                        <SubTitle>
                            Create, view and delete roles through this page. Note that roles define what kind of access you want to give an administrator or user.
                        </SubTitle>
                        <div className="btn-add-role">
                            <Center>
                                <Button
                                    label="Create New Role"
                                    type="action"
                                    onClick={ () => browserHistory.push( '/control-panel/roles/role-create', true ) }
                                />
                            </Center>
                        </div>
                        <form onSubmit={ this.handleSearch }>
                            <div className="user-search">
                                <FlexContainer
                                    justifyContent="space-between"
                                    alignItems="center"
                                >
                                    <FlexContainer
                                        justifyContent="start"
                                        alignItems="center"
                                        gap="8"
                                    >
                                        <H5 noBottomMargin className="user-list">Roles List</H5>
                                        <Search handleSearch={ ( value ) => this.handleSearch( value.toLowerCase() ) } />
                                    </FlexContainer>
                                    <P noBottomMargin>{this.state.tableLabel}</P>
                                </FlexContainer>
                            </div>
                        </form>

                        <Table
                            data={ displayData }
                            columns={ tableColumns }
                            loading={ loading }
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => { this.userRolesTable = ref; } }
                            external
                        />

                        <div>
                            <FooterTablePaginationV2
                                page={ pagination.current_page }
                                pageSize={ pagination.per_page }
                                pagination={ pagination }
                                onPageChange={ this.onPageChange }
                                onPageSizeChange={ this.onPageSizeChange }
                                paginationLabel={ tableLabel }
                                fluid
                            />
                        </div>
                    </StyledContainer>
                </div>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    isExpired: makeSelectIsExpired(),
    userRoles: makeSelectUserRoles()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        usersActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( UserRoles );
