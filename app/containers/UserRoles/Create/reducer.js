import { fromJS } from 'immutable';

import {
    DEFAULT_ACTION,
    SET_LOADING,
    SET_ESSENTIAL_DATA,
    SET_COMPANIES,
    SET_NOTIFICATION
} from './constants';

const initialState = fromJS({
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: ''
    },
    essentialData: {},
    companies: []
});

/**
 *
 * Users reducer
 *
 */
function userRolesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_ESSENTIAL_DATA:
            return state.set( 'essentialData', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_COMPANIES:
            return state.set( 'companies', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default userRolesReducer;
