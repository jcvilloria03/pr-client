/* eslint-disable no-confusing-arrow */
import styled from 'styled-components';
import { Container } from 'reactstrap';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;

    .icon-arrow {
        width: 14px;
        font-size: 14px;
        display: inline-block;
        margin-right: 4px;

        > i {
            align-self: center;
        }
    }

    .back-text {
        font-size: 14px;
    }
`;

export const StyledContainer = styled( Container )`
    padding-top: 80px;
    height: 100%;
    width: 60vw;

    .role-details-title,
    .permissions-title {
        margin-top: 28px;
        font-size: 26px;
        font-weight: 700;
        line-height: 41.6px;
    }
`;

export const HeaderTitle = styled.h1`
    text-align: center;
    margin-top: 60px;
    margin-bottom: 0;
    font-weight: 700;
    font-size: 36px;
    line-height: 57.6px;
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding-top: 200px;
`;

export const Card = styled.div`
    padding: 14px;
    box-shadow: 0 0 26px -6px hsl(0deg 0% 54% / 50%);
    border-radius: 15px;

    .input-role-name-container {
        padding: 0 7px 14px;

        input {
            font-size: 14px;
        }
    }

    .input-error {
        input {
            border-color:#eb7575 !important;
        }
        label {
            color:#eb7575 !important;
        }
        span {
            color:#eb7575;
        }
        p {
            color:#eb7575;
            fontSize: 14px;
            margin-bottom: 0;
        }
        input:focus~label {
            color:#eb7575 !important;
        }
    }
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 20vw;
    background: #f0f4f6;
    margin-top: 20px;
    height: 72px;

    button {
        min-width: 120px;
    }

    .cancel-button {
        &:hover {
            color: #474747;
        }
    }
`;
