/*
 *
 * UserRoles constants
 *
 */
export const nameSpace = 'app/createRoles';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;

export const SET_LOADING = `${nameSpace}/SET_LOADING`;

export const SET_NOTIFICATION = `${nameSpace}/SET_NOTIFICATION`;
export const GET_NOTIFICATION = `${nameSpace}/GET_NOTIFICATION`;

export const GET_ESSENTIAL_DATA = `${nameSpace}/GET_ESSENTIAL_DATA`;
export const SET_ESSENTIAL_DATA = `${nameSpace}/SET_ESSENTIAL_DATA`;

export const GET_COMPANIES = `${nameSpace}/GET_COMPANIES`;
export const SET_COMPANIES = `${nameSpace}/SET_COMPANIES`;

export const SET_CREATE_USER_ROLE = `${nameSpace}/SET_CREATE_USER_ROLE`;
