/* eslint-disable require-jsdoc */
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';

import { browserHistory } from 'utils/BrowserHistory';
import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';

import {
    GET_ESSENTIAL_DATA,
    SET_ESSENTIAL_DATA,
    SET_NOTIFICATION,
    GET_NOTIFICATION,
    SET_LOADING,
    SET_COMPANIES,
    SET_CREATE_USER_ROLE
} from './constants';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* getEssentialData() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const accountId = auth.getUser().account_id;
        const { company, ...essentialData } = yield call( Fetch, `/accounts/${accountId}/essential_data`, { method: 'GET' });

        yield put({
            type: SET_ESSENTIAL_DATA,
            payload: { ...essentialData }
        });
        yield put({
            type: SET_COMPANIES,
            payload: company
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* createUserRole({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const accountId = auth.getUser().account_id;

        yield call( Fetch, `/accounts/${accountId}/roles`, {
            method: 'POST',
            data: payload
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully Added',
            show: true,
            type: 'success'
        });
        yield call( browserHistory.push( '/control-panel/roles/user-roles', true ) );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* notifyUser( payload ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

export function* watchGetEssentialData() {
    const watcher = yield takeEvery( GET_ESSENTIAL_DATA, getEssentialData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchCreateUserRole() {
    const watcher = yield takeEvery( SET_CREATE_USER_ROLE, createUserRole );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchNotify() {
    const watcher = yield takeEvery( GET_NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchGetEssentialData,
    watchCreateUserRole,
    watchNotify
];
