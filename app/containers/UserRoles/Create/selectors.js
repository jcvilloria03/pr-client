import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectUserRolesCreateDomain = () => ( state ) => state.get( 'createRoles' );

const makeSelectLoading = () => createSelector(
  selectUserRolesCreateDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectUserRolesCreateDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectEssentialData = () => createSelector(
  selectUserRolesCreateDomain(),
  ( substate ) => substate.get( 'essentialData' ).toJS()
);

const makeSelectCompanies = () => createSelector(
  selectUserRolesCreateDomain(),
  ( substate ) => substate.get( 'companies' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectNotification,
  makeSelectEssentialData,
  makeSelectCompanies
};
