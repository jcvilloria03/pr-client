/* eslint-disable require-jsdoc */
import {
    GET_ESSENTIAL_DATA,
    SET_CREATE_USER_ROLE
} from './constants';

export function getEssentialData() {
    return {
        type: GET_ESSENTIAL_DATA
    };
}

export function createUserRole( payload ) {
    return {
        type: SET_CREATE_USER_ROLE,
        payload
    };
}
