import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import { each, forIn, cloneDeep, isEmpty } from 'lodash';

import SnackBar from 'components/SnackBar';
import A from 'components/A';
import { H2, H3, P } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Input from 'components/Input';
import Icon from 'components/Icon';
import Modal from 'components/Modal';

import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';

import { makeSelectIsExpired } from 'containers/App/selectors';

import Permission from '../components/Permission';

import { initalizeAccordion, PERMISSION_MODULES, OTHER_SCOPE_GROUPS } from '../components/constants';

import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectCompanies,
    makeSelectEssentialData
} from './selectors';
import * as usersActions from './actions';

import {
    HeaderTitle,
    StyledContainer,
    LoadingStyles,
    NavWrapper,
    Card,
    Footer
} from './styles';

/**
 *
 * Users
 *
 */
export class CreateUserRoles extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        isExpired: React.PropTypes.bool,
        getEssentialData: React.PropTypes.func,
        createUserRole: React.PropTypes.func,
        companies: React.PropTypes.array,
        essentialData: React.PropTypes.object
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            roleName: '',
            isNameValid: true,
            validateName: 'name',
            validateExistName: false,
            accordions: initalizeAccordion()
        };
    }

    componentDidMount() {
        this.props.getEssentialData();
    }

    handleAccordionOpenClose( accordionKey, isExpanded ) {
        const accordions = cloneDeep( this.state.accordions );
        let newAccordions = {};

        forIn( accordions, ( accordion, key ) => {
            let expanded = false;

            if ( key === accordionKey ) {
                expanded = isExpanded;
            }

            newAccordions = { ...newAccordions, [ key ]: { ...accordion, isExpanded: expanded }};
        });

        this.setState({ accordions: newAccordions });
    }

    handleAccordionChanges( accordionKey, hasChanges ) {
        const accordions = cloneDeep( this.state.accordions );
        let newAccordions = {};

        forIn( accordions, ( accordion, key ) => {
            let changes = false;

            if ( key === accordionKey ) {
                changes = hasChanges;
            }

            newAccordions = { ...newAccordions, [ key ]: { ...accordion, hasChanges: changes }};
        });

        this.setState({ accordions: newAccordions });
    }

    handleSaveAccordion( accordionKey, accordionData ) {
        const accordions = cloneDeep( this.state.accordions );
        let newAccordions = {};

        forIn( accordions, ( accordion, key ) => {
            let newAccordionData = accordion;

            if ( key === accordionKey ) {
                newAccordionData = accordionData;
            }

            newAccordions = { ...newAccordions, [ key ]: { ...newAccordionData }};
        });

        this.setState({ accordions: newAccordions });
    }

    handleClickCreate() {
        if ( this.validateForm() ) {
            if ( this.validateMainPage() ) {
                this.preparePayload();
            }
        } else {
            this.InvalidFormModal.toggle();
        }
    }

    validateForm() {
        const { roleName, accordions } = cloneDeep( this.state );

        let valid = true;

        const emptyAccordions = {};

        let isNameValid = true;
        if ( roleName.trim() === '' ) {
            isNameValid = false;
        } else {
            isNameValid = true;
        }
        valid = isNameValid;

        each( accordions, ( accordion, index ) => {
            emptyAccordions[ index ] = !accordion.resources.actions.read;
        });

        if ( Object.values( emptyAccordions ).every( Boolean ) ) {
            valid = false;
        }

        this.setState({ isNameValid });

        return valid;
    }

    validateMainPage() {
        const { accordions } = cloneDeep( this.state );

        let valid = true;

        if ( !accordions.main_page.resources.actions.read ) {
            valid = false;
            this.MainPageEmptyModal.toggle();
        }

        return valid;
    }

    preparePayload() {
        const accordions = cloneDeep( this.state.accordions );

        const modules = this.preparePayloadModules();
        const isEmployee = ( accordions.ess.companies.length || accordions.ess.resources.actions.read ) && ( Object.values( accordions.ess.resources.actions ) ).includes( true );
        let companyIds = [];

        each( accordions, ( accordion ) => {
            const companies = this.prepareCompanyIds( accordion.companies, accordion.is_all_company );

            if ( ![ 'control_panel', 'main_page', 'ess' ].includes( accordion.id ) ) {
                each( companies, ( ids ) => {
                    !companyIds.includes( ids ) && companyIds.push( ids );
                });
            }
        });

        if ( companyIds.includes( '__ALL__' ) ) {
            companyIds = [];
        }

        const payload = {
            name: this.state.roleName,
            is_employee: Boolean( isEmployee ),
            permissions: {
                ...modules
            }
        };

        Object.assign( payload, {
            company_ids: companyIds,
            is_all_companies: isEmpty( companyIds )
        });

        this.props.createUserRole( payload );
    }

    preparePayloadModules() {
        const accordions = cloneDeep( this.state.accordions );

        const modules = {};

        each( PERMISSION_MODULES, ( permission, index ) => {
            const scopes = {};
            const actions = [];

            if ( !isEmpty( accordions[ index ].companies ) && ( Object.values( accordions[ index ].resources.actions ) ).includes( true ) ) {
                if ( !isEmpty( accordions[ index ].resources.scope ) ) {
                    each( accordions[ index ].resources.scope, ( scopeItem ) => {
                        const groupName = ( scopeItem.group ).toUpperCase();

                        if ( !Object.keys( scopes ).includes( groupName ) ) {
                            scopes[ groupName ] = [];
                        }

                        if ( OTHER_SCOPE_GROUPS.includes( scopeItem.value ) ) {
                            scopes[ groupName ].push( '__ALL__' );
                        } else {
                            scopes[ groupName ].push( scopeItem.value );
                        }
                    });
                }

                const companies = this.prepareCompanyIds( accordions[ index ].companies, accordions[ index ].is_all_company );

                if ( accordions[ index ].companies.length > 0 ) {
                    Object.assign( scopes, { COMPANY: companies });
                }

                each( accordions[ index ].resources.actions, ( actionItem, key ) => {
                    actionItem && actions.push( key.toUpperCase() );
                });

                each( permission, ( moduleItem ) => {
                    modules[ moduleItem ] = {
                        scopes: { ...scopes },
                        actions
                    };
                });
            }
        });

        return modules;
    }

    prepareCompanyIds( data, allCompanies ) {
        const companies = [];

        allCompanies
          ? companies.push( '__ALL__' )
          : each( data, ( company ) => {
              companies.push( company );
          });

        return companies;
    }

    /**
     *
     * Users render method
     *
     */
    render() {
        const {
            isExpired,
            notification,
            loading,
            companies,
            essentialData
        } = this.props;

        const { accordions, isNameValid } = this.state;

        return (
            <div>
                <Helmet
                    title="User Roles"
                    meta={ [
                        { name: 'description', content: 'List of User Roles' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        isExpired,
                        salpayViewPermission: true,
                        isSubscribedToPayroll: true,
                        accountViewPermission: true
                    }) }
                />

                <Modal
                    title="Create Role"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.InvalidFormModal = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Unable to Create Role. Please review permissions and try again.</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'action',
                            label: 'Okay',
                            onClick: () => this.InvalidFormModal.toggle()
                        }
                    ] }
                />

                <Modal
                    title="Create Role"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.MainPageEmptyModal = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>At least `READ` permission for main page is required when permission is enabled for company settings, employees, time and attendance or payroll. The system will append this for your convenience.</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'Cancel',
                            onClick: () => this.MainPageEmptyModal.toggle()
                        },
                        {
                            type: 'action',
                            label: 'Confirm',
                            onClick: () => {
                                this.MainPageEmptyModal.toggle();
                                this.preparePayload();
                            }
                        }
                    ] }
                />

                <Modal
                    title="Discard Changes"
                    className="modal-md modal-commission-type"
                    body={
                        <div>
                            <p> Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.discardModal.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/control-panel/roles/user-roles', true );
                                this.discardModal.toggle();
                            }
                        }
                    ] }
                    center
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                />

                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/control-panel/roles/user-roles', true );
                            } }
                        >
                            <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to User Roles</span>
                        </A>
                    </Container>
                </NavWrapper>
                <div className="loader" style={ { display: loading ? '' : 'none' } }>
                    <LoadingStyles>
                        <H2>Loading User Roles.</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoadingStyles>
                </div>
                <div className="content" style={ { display: loading ? 'none' : '' } }>
                    <StyledContainer>
                        <HeaderTitle>Add Roles</HeaderTitle>

                        <div>
                            <H3 noBottomMargin className="role-details-title">Role Details</H3>

                            <Card>
                                <div className={ isNameValid ? 'input-role-name-container' : 'input-role-name-container input-error' }>
                                    <Input
                                        label="* Role Name"
                                        id="role-name"
                                        name="role-name"
                                        onChange={ ( val ) => {
                                            this.setState({ roleName: val, isNameValid: true });
                                        } }
                                        placeholder="Type your desired role name"
                                    />
                                    {!isNameValid && <P noBottomMargin>Role Name is required</P>}
                                </div>
                            </Card>
                        </div>

                        <div>
                            <H3 noBottomMargin className="permissions-title">Permissions</H3>

                            <Card>
                                {
                                    Object.keys( accordions ).map( ( accordionKey ) => {
                                        const accordion = accordions[ accordionKey ];

                                        return (
                                            <Permission
                                                key={ accordion.id }
                                                companyList={ companies }
                                                essentialData={ essentialData }
                                                accordion={ accordion }
                                                onClickEdit={ () => this.handleAccordionOpenClose( accordionKey, true ) }
                                                onClickClose={ () => this.handleAccordionOpenClose( accordionKey, false ) }
                                                onChange={ ( changes ) => this.handleAccordionChanges( accordionKey, changes ) }
                                                onSave={ ( accordionData ) => this.handleSaveAccordion( accordionKey, accordionData ) }
                                            />
                                        );
                                    })
                                }
                            </Card>
                        </div>
                    </StyledContainer>

                    <Footer>
                        <div className="submit">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                className="cancel-button"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                label="Create"
                                type="action"
                                size="large"
                                onClick={ () => this.handleClickCreate() }
                            />
                        </div>
                    </Footer>
                </div>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    companies: makeSelectCompanies(),
    essentialData: makeSelectEssentialData(),
    isExpired: makeSelectIsExpired()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        usersActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( CreateUserRoles );
