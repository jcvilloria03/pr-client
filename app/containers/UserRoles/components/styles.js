/* eslint-disable no-confusing-arrow */
import styled from 'styled-components';

export const CollapsibleContainer = styled.div`
    font-weight: 600;
    border-radius: 15px;
    padding: 21px;
    margin: 10px 0;
    background-color: #f6f6f6;

    h4 {
        color: #00a5e5;
        font-size: 20px;
        font-weight: 600;
        line-height: 32px;
    }

    .control-buttons {
        button {
            padding-top: 4px;
            padding-bottom: 4px;
        }
    }
`;

export const CollapsibleBody = styled.div`
    color: #474747;
    margin-top: 1rem;
    padding: 1rem;
    border-radius: 15px;
    background-color: #fff;

    .actions-container {
        p {
            font-weight: 600;
        }
    }

    .actions-error {
        color: #eb7575;
    }
`;

export const FlexContainer = styled.div`
    display: flex;
    justify-content: ${( props ) => props.justifyContent};
    align-items: ${( props ) => props.alignItems};
    gap: ${( props ) => props.gap ? props.gap : 0}px;

    &.actions-switch {
        flex-wrap: wrap;
    }
`;

export const MultiSelectWrapper = styled.div`
    .Select .Select-control {
        height: 162px;
    }
`;

export const CompanySelectWrapper = styled.div`
    .Select .Select-control {
        min-height: 50px;
    }
`;

export const SwitchAllWrapper = styled.div`
    width: fit-content;
`;
