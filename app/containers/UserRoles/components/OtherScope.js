import React, { Component } from 'react';
import { Col } from 'reactstrap';
import { forEach, cloneDeep } from 'lodash';

import MultiSelect from 'components/MultiSelect';
import { P } from 'components/Typography';

import SimpleSwitch from './SimpleSwitch';

import {
    FlexContainer,
    MultiSelectWrapper
} from './styles';

const scopeAllSwitch = {
    department: 'All Department',
    position: 'All Position',
    team: 'All Team',
    location: 'All Location',
    payroll_group: 'All Payroll Group'
};

/**
 * Other scope
*/
export class OtherScope extends Component {
    static propTypes = {
        accordionId: React.PropTypes.string,
        essentialData: React.PropTypes.object,
        scope: React.PropTypes.array,
        scopeSwitchGroup: React.PropTypes.object,
        onChange: React.PropTypes.func
    }

    static defaultProps = {
        scopeSwitchGroup: {
            department: false,
            location: false,
            payroll_group: false,
            position: false,
            team: false
        },
        essentialData: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            options: [],
            values: this.props.scope || [],
            scopeSwitchGroup: this.props.scopeSwitchGroup || {
                department: false,
                location: false,
                payroll_group: false,
                position: false,
                team: false
            }
        };
    }

    clearScope = () => {
        this.setState({
            values: [],
            scopeSwitchGroup: {
                department: false,
                location: false,
                payroll_group: false,
                position: false,
                team: false
            }
        });
    }

    handleSelectedScopes( selected ) {
        const { scopeSwitchGroup } = cloneDeep( this.state );

        const values = selected.map( ( s ) => s.value );

        const selectedAll = values.filter( ( v ) => isNaN( v ) );

        forEach( scopeSwitchGroup, ( val, key ) => {
            if ( selectedAll.includes( key ) ) {
                scopeSwitchGroup[ key ] = true;
            } else {
                scopeSwitchGroup[ key ] = false;
            }
        });

        this.setState(
            {
                values: selected,
                scopeSwitchGroup
            },
            () => this.props.onChange()
        );
    }

    handleSwitchAction( name, checked ) {
        const { scopeSwitchGroup } = cloneDeep( this.state );
        let { values } = cloneDeep( this.state );

        scopeSwitchGroup[ name ] = checked;

        if ( checked ) {
            values.push({
                label: scopeAllSwitch[ name ],
                value: name,
                group: name
            });
        } else {
            values = values.filter( ( v ) => v.value !== name );
        }

        this.setState( ( prevState ) => ({
            ...prevState,
            values,
            scopeSwitchGroup
        }),
        () => this.props.onChange()
        );
    }

    selectOptions() {
        const { accordionId } = this.props;

        const essentialData = cloneDeep( this.props.essentialData );
        const { scopeSwitchGroup } = cloneDeep( this.state );

        const options = [];
        const selectedAllGroup = [];

        forEach( scopeSwitchGroup, ( val, key ) => {
            if ( val ) {
                selectedAllGroup.push( key );
            }
        });

        if ( accordionId === 'payroll' ) {
            forEach( essentialData, ( scope, key ) => {
                if ( key !== 'payroll_group' ) {
                    delete essentialData[ key ];
                }
            });
        }

        forEach( essentialData, ( scope, key ) => {
            options.push({
                label: scopeAllSwitch[ key ],
                value: key,
                group: key
            });

            scope.forEach( ( data ) => {
                if ( !selectedAllGroup.includes( key ) ) {
                    options.push({
                        label: `${data.company_name}: ${data.name}`,
                        value: data.id,
                        group: key
                    });
                }
            });
        });

        return options;
    }

    render() {
        const { accordionId } = this.props;
        const { scopeSwitchGroup, values } = cloneDeep( this.state );

        const {
            department,
            position,
            team,
            location,
            payroll_group: payrollGroup
        } = scopeSwitchGroup;

        const selectedValues = values.map( ( v ) => v.value );

        const options = this.selectOptions();

        return (
            <Col xs={ 9 }>
                <P noBottomMargin><strong>Other Scopes</strong></P>

                <MultiSelectWrapper className="other-scope-multi-select">
                    <MultiSelect
                        id="other-scope"
                        placeholder="Enter Department, Location, Position, Teams or Payroll Groups"
                        value={ selectedValues }
                        data={ options }
                        onChange={ ( value ) => this.handleSelectedScopes( value ) }
                        disabled={ [ 'control_panel', 'company_settings', 'ess' ].includes( accordionId ) }
                    />
                </MultiSelectWrapper>

                <FlexContainer justifyContent="space-between" alignItems="center" gap={ 4 } className="actions-switch">
                    <SimpleSwitch
                        checked={ department }
                        name="department"
                        label="All Department"
                        onClick={ ( name, isChecked ) => {
                            this.handleSwitchAction( name, !isChecked );
                        } }
                        disabled={ [ 'control_panel', 'company_settings', 'ess', 'payroll' ].includes( accordionId ) }
                    />

                    <SimpleSwitch
                        checked={ position }
                        name="position"
                        label="All Position"
                        onClick={ ( name, isChecked ) => {
                            this.handleSwitchAction( name, !isChecked );
                        } }
                        disabled={ [ 'control_panel', 'company_settings', 'ess', 'payroll' ].includes( accordionId ) }
                    />

                    <SimpleSwitch
                        checked={ team }
                        name="team"
                        label="All Team"
                        onClick={ ( name, isChecked ) => {
                            this.handleSwitchAction( name, !isChecked );
                        } }
                        disabled={ [ 'control_panel', 'company_settings', 'ess', 'payroll' ].includes( accordionId ) }
                    />

                    <SimpleSwitch
                        checked={ location }
                        name="location"
                        label="All Location"
                        onClick={ ( name, isChecked ) => {
                            this.handleSwitchAction( name, !isChecked );
                        } }
                        disabled={ [ 'control_panel', 'company_settings', 'ess', 'payroll' ].includes( accordionId ) }
                    />

                    <SimpleSwitch
                        checked={ payrollGroup }
                        name="payroll_group"
                        label="All Payroll Group"
                        onClick={ ( name, isChecked ) => {
                            this.handleSwitchAction( name, !isChecked );
                        } }
                        disabled={ [ 'control_panel', 'company_settings', 'ess' ].includes( accordionId ) }
                    />
                </FlexContainer>
            </Col>
        );
    }
}

export default OtherScope;
