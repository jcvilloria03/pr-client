/* eslint-disable no-confusing-arrow */
/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';

const InputWrapper = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    gap: 14px;
    margin: 14px 0;
    font-size: 14px;

    &:hover {
        cursor:  ${( props ) => ( props.disabled ? 'not-allowed' : 'pointer' )};
    }

    &.disabled {
        color: #a7a7a7;
    }
`;

const Input = styled.input`
    position: absolute;
    left: -9999px;
    top: -9999px;

    &:checked + span {
        &:before {
            left: 12px;
            background-color: #83d24b;
        }
    }
`;

const Slider = styled.span`
    display: flex;
    cursor:  ${( props ) => ( props.disabled ? 'not-allowed' : 'pointer' )};
    width: 28px;
    height: 10px;
    border-radius: 80px;
    background-color: #bfbfbf;
    position: relative;
    transition: background-color 0.2s;

    &:before {
        content: "";
        position: absolute;
        top: -4px;
        left: 0px;
        width: 18px;
        height: 18px;
        border-radius: 18px;
        transition: 0.2s;
        background-color: ${( props ) =>
            props.disabled ? '#fafafa' : '#474747'};
        cursor:  ${( props ) => ( props.disabled ? 'not-allowed' : 'pointer' )};   
        box-shadow: 0 2px 4px 0 rgba(0, 35, 11, 0.2);
    }
`;

const SimpleSwitch = ({ label, name, checked, onClick, disabled }) => (
    <InputWrapper
        onClick={ () => {
            if ( disabled ) return;
            onClick( name, checked );
        } }
        disabled={ disabled }
        className={ disabled ? 'disabled' : '' }
    >
        <Input
            type="checkbox"
            name={ name }
            id={ name }
            checked={ checked }
            onClick={ () => {
                if ( disabled ) return;
                onClick( name, checked );
            } }
            disabled={ disabled }
        />
        <Slider
            onClick={ () => {
                if ( disabled ) return;
                onClick( name, checked );
            } }
            disabled={ disabled }
        />
        <span>{label}</span>
    </InputWrapper>
  );

export default SimpleSwitch;
