import React, { Component } from 'react';
import { Col } from 'reactstrap';

import { P } from 'components/Typography';

import SimpleSwitch from './SimpleSwitch';

/**
 * ActionScope
 */
export class ActionScope extends Component {
    static propTypes = {
        accordionId: React.PropTypes.string,
        actions: React.PropTypes.object,
        allActions: React.PropTypes.bool,
        onSwitchAction: React.PropTypes.func,
        onSwitchActionAll: React.PropTypes.func,
        showError: React.PropTypes.bool
    }

    static defaultProps = {
        actions: {
            create: false,
            read: false,
            update: false,
            delete: false
        },
        allActions: false,
        showError: false
    };

    constructor( props ) {
        super( props );

        this.state = { };
    }

    render() {
        const { actions, allActions, accordionId, showError } = this.props;

        return (
            <Col xs={ 3 } className={ showError ? 'actions-error actions-container' : 'actions-container' }>
                <P noBottomMargin>Actions*</P>

                <div>
                    <SimpleSwitch
                        checked={ actions.create }
                        name="create"
                        label="Create"
                        onClick={ ( name, isChecked ) => {
                            this.props.onSwitchAction( name, !isChecked );
                        } }
                    />
                </div>
                <div>
                    <SimpleSwitch
                        checked={ actions.read }
                        name="read"
                        label="Read"
                        onClick={ ( name, isChecked ) => {
                            this.props.onSwitchAction( name, !isChecked );
                        } }
                    />
                </div>
                <div>
                    <SimpleSwitch
                        checked={ actions.update }
                        name="update"
                        label="Update"
                        onClick={ ( name, isChecked ) => {
                            this.props.onSwitchAction( name, !isChecked );
                        } }
                    />
                </div>
                <div>
                    <SimpleSwitch
                        checked={ actions.delete }
                        name="delete"
                        label="Delete"
                        onClick={ ( name, isChecked ) => {
                            this.props.onSwitchAction( name, !isChecked );
                        } }
                        disabled={ [ 'ess', 'main_page' ].includes( accordionId ) }
                    />
                </div>
                <hr />
                <div>
                    <SimpleSwitch
                        checked={ allActions }
                        name="all_actions"
                        label="Authorize all actions"
                        onClick={ ( name, isChecked ) => {
                            this.props.onSwitchActionAll( !isChecked );
                        } }
                    />
                </div>

                {showError ? <P noBottomMargin>Atleast 1 Action is required</P> : null}
            </Col>
        );
    }
}

export default ActionScope;
