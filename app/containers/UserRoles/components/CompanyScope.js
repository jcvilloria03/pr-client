import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';

import MultiSelect from 'components/MultiSelect';
import { P } from 'components/Typography';

import SimpleSwitch from './SimpleSwitch';

import {
    SwitchAllWrapper
} from './styles';

const allCompanies = [
    {
        value: '__ALL__',
        label: 'All Companies'
    }
];

/**
 * Company scope
*/
export class CompanyScope extends Component {
    static propTypes = {
        companyList: React.PropTypes.array,
        selectedCompanies: React.PropTypes.array,
        disabled: React.PropTypes.bool,
        onSelectCompany: React.PropTypes.func,
        onSelectAll: React.PropTypes.func,
        selectedAll: React.PropTypes.bool
    }

    constructor( props ) {
        super( props );

        this.state = {
            options: [],
            values: this.props.selectedCompanies || []
        };
    }

    componentWillReceiveProps() {
        const options = this.generateOptions( this.props.companyList );
        this.setState({ options });
    }

    handleAllCompanyToggle = ( name, value ) => {
        this.setState(
            {
                values: []
            },
            () => this.props.onSelectAll( !value )
        );
    }

    generateOptions = ( scopes ) => scopes.map( ( v ) => ({ label: `${v.name}`, value: v.id }) )

    handleChange = ( selected ) => {
        const selectedValues = selected.map( ( s ) => s.value );
        this.setState(
            {
                values: selectedValues
            },
            () => this.props.onSelectCompany( selectedValues )
        );
    }

    clearScope() {
        this.setState({
            values: []
        });
    }

    render() {
        const { options, values } = this.state;
        const { disabled, selectedAll } = this.props;

        return (
            <Row>
                <Col>
                    <P noBottomMargin><strong>* Company Scopes</strong></P>

                    { selectedAll
                        ? (
                            <MultiSelect
                                id="company"
                                data={ allCompanies }
                                value={ '__ALL__' }
                                disabled
                            />
                        ) : (
                            <MultiSelect
                                id="company"
                                data={ options }
                                value={ values }
                                placeholder="Select Companies"
                                onChange={ this.handleChange }
                                disabled={ disabled }
                            />
                        )
                    }
                </Col>
                <Col>
                    <SwitchAllWrapper>
                        <SimpleSwitch
                            checked={ selectedAll }
                            name="is_all_company"
                            label="All Companies"
                            onClick={ this.handleAllCompanyToggle }
                        />
                    </SwitchAllWrapper>
                </Col>
            </Row>
        );
    }
}

export default CompanyScope;
