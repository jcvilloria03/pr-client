import { forEach } from 'lodash';

export const ACCORDIONS = [
    {
        id: 'control_panel',
        name: 'Control Panel',
        isExpanded: false
    },
    {
        id: 'company_settings',
        name: 'Company Settings',
        isExpanded: false
    },
    {
        id: 'employees',
        name: 'Employees',
        isExpanded: false
    },
    {
        id: 'time_and_attendance',
        name: 'Time & Attendance',
        isExpanded: false
    },
    {
        id: 'payroll',
        name: 'Payroll',
        isExpanded: false
    },
    {
        id: 'main_page',
        name: 'Main Page',
        isExpanded: false
    },
    {
        id: 'ess',
        name: 'Employee Self Service (ESS)',
        isExpanded: false
    }
];

export const OTHER_SCOPE_GROUPS = [ 'department', 'position', 'team', 'location', 'payroll_group' ];

export const disabledScope = ( accordion ) => [ 'control_panel', 'company_settings', 'ess' ].includes( accordion );

// modules used for Cusrom RBAC
export const PERMISSION_MODULES = {
    control_panel: [
        'control_panel.subscriptions',
        'control_panel.subscriptions.subscriptions_tab',
        'control_panel.subscriptions.licenses_tab',
        'control_panel.subscriptions.licenses_tab.subscription_plan',
        'control_panel.subscriptions.licenses_tab.subscription_license',
        'control_panel.subscriptions.invoices_tab',
        'control_panel.subscriptions.invoices_tab.billing_information',
        'control_panel.subscriptions.invoices_tab.invoice_history',
        'control_panel.subscriptions.receipts_tab',
        'control_panel.companies',
        'control_panel.companies.company_information',
        'control_panel.companies.government_issued_id_number',
        'control_panel.companies.contact_information',
        'control_panel.device_management',
        'control_panel.salpay_integration',
        'control_panel.users',
        'control_panel.permissions',
        'control_panel.roles',
        'control_panel.audit_trail'
    ],
    company_settings: [
        'company_settings.company_structure',
        'company_settings.company_structure.company_details',
        'company_settings.company_structure.company_details.company_information',
        'company_settings.company_structure.company_details.government_issued_id_number',
        'company_settings.company_structure.company_details.contact_information',
        'company_settings.company_structure.locations',
        'company_settings.company_structure.locations.location',
        'company_settings.company_structure.locations.ip_address',
        'company_settings.company_structure.organizational_chart',
        'company_settings.company_structure.organizational_chart.department',
        'company_settings.company_structure.organizational_chart.position',
        'company_settings.company_structure.cost_centers',
        'company_settings.company_structure.ranks',
        'company_settings.company_structure.teams',
        'company_settings.company_structure.employment_types',
        'company_settings.company_structure.projects',
        'company_settings.day_hour_rates',
        'company_settings.max_clock_outs',
        'company_settings.company_payroll',
        'company_settings.company_payroll.payroll_groups',
        'company_settings.company_payroll.bonus_types',
        'company_settings.company_payroll.commission_types',
        'company_settings.company_payroll.allowance_types',
        'company_settings.company_payroll.deduction_types',
        'company_settings.company_payroll.loan_type_settings',
        'company_settings.company_payroll.disbursements',
        'company_settings.leave_settings',
        'company_settings.leave_settings.leave_types',
        'company_settings.leave_settings.leave_entitlements',
        'company_settings.schedule_settings',
        'company_settings.schedule_settings.default_schedule',
        'company_settings.schedule_settings.tardiness_rules',
        'company_settings.schedule_settings.night_shift',
        'company_settings.schedule_settings.holidays',
        'company_settings.workflow_automation'
    ],
    employees: [
        'employees.people',
        'employees.people.termination_information',
        'employees.people.basic_information',
        'employees.people.employment_information',
        'employees.people.payroll_information',
        'employees.people.approval_groups',
        'employees.people.annual_earnings',
        'employees.people.payment_methods',
        'employees.people.basic_pay_adjustments',
        'employees.people.leave_credits',
        'employees.people.filed_leaves',
        'employees.people.allowances',
        'employees.people.bonuses',
        'employees.people.commissions',
        'employees.people.loans',
        'employees.people.deductions',
        'employees.people.adjustments',
        'employees.loans',
        'employees.deductions',
        'employees.adjustments',
        'employees.allowances',
        'employees.leaves',
        'employees.leaves.leave_credits',
        'employees.leaves.filed_leave',
        'employees.annual_earnings',
        'employees.workflows',
        'employees.government_forms'
    ],
    time_and_attendance: [
        'time_and_attendance.schedules',
        'time_and_attendance.shifts',
        'time_and_attendance.shifts.assign_shift',
        'time_and_attendance.shifts.assign_rest_day',
        'time_and_attendance.attendance_computation',
        'time_and_attendance.attendance_computation.attendance',
        'time_and_attendance.attendance_computation.attendance.time_records',
        'time_and_attendance.attendance_computation.attendance.computed_attendance',
        'time_and_attendance.attendance_computation.attendance.leaves',
        'time_and_attendance.approvals_list',
        'time_and_attendance.approvals_list.request_details'
    ],
    payroll: [
        'payroll.payroll_summary',
        'payroll.payroll_summary.generate_regular_payroll',
        'payroll.payroll_summary.generate_special_payroll_final_pay_run',
        'payroll.payslips',
        'payroll.government_forms',
        'payroll.bonuses',
        'payroll.commissions'
    ],
    main_page: [
        'main_page.dashboard',
        'main_page.announcements',
        'main_page.profile_information',
        'main_page.profile_information.change_password'
    ],
    ess: [
        'ess.dashboard',
        'ess.approvals',
        'ess.requests',
        'ess.requests.leaves',
        'ess.requests.time_dispute',
        'ess.requests.overtime',
        'ess.requests.undertime',
        'ess.requests.shift_change',
        'ess.announcements',
        'ess.payslips',
        'ess.calendar',
        'ess.teams',
        'ess.profile_information'
    ]
};

export const initalizeAccordion = () => {
    const data = {};

    forEach( ACCORDIONS, ( accordion ) => {
        data[ accordion.id ] = {
            ...accordion,
            companies: [],
            is_all_company: false,
            resources: {
                all_actions: false,
                actions: {
                    create: false,
                    read: false,
                    update: false,
                    delete: false
                },
                scope: [
                    /* { value: 'department', label: 'All department' } */
                ],
                scope_switch_group: {
                    department: false,
                    position: false,
                    team: false,
                    location: false,
                    payroll_group: false
                }
            },
            hasChanges: false,
            saved: false
        };
    });
    return data;
};
