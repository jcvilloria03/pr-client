import React, { Component } from 'react';

import { Collapse, Row } from 'reactstrap';
import { cloneDeep, forIn } from 'lodash';

import Button from 'components/Button';
import Modal from 'components/Modal';
import { H4 } from 'components/Typography';
import Icon from 'components/Icon';

import CompanyScope from './CompanyScope';
import OtherScope from './OtherScope';
import ActionScope from './ActionsScope';

import {
    CollapsibleContainer,
    CollapsibleBody,
    FlexContainer
} from './styles';

/**
 * Permission
 */
export class Permission extends Component {
    static propTypes = {
        accordion: React.PropTypes.object,
        onClickEdit: React.PropTypes.func,
        onClickClose: React.PropTypes.func,
        companyList: React.PropTypes.array,
        essentialData: React.PropTypes.object,
        onChange: React.PropTypes.func,
        onSave: React.PropTypes.func
    }

    static defaultProps = {
        name: ''
    };

    constructor( props ) {
        super( props );

        this.state = {
            actions: {
                create: false,
                read: false,
                update: false,
                delete: false
            },
            allActions: false,
            accordionData: this.props.accordion,
            showActionsError: false
        };

        this.companyScopeRef = null;
        this.otherScopeRef = null;

        this.discardModal = null;
        this.clearModal = null;
    }

    componentWillReceiveProps( nextProps ) {
        this.setState( ( prevState ) => ({
            accordionData: {
                ...prevState.accordionData,
                isExpanded: nextProps.accordion.isExpanded
            }
        }) );
    }

    onClickClear() {
        this.clearModal.toggle();
    }

    onClickSave() {
        const companyScopeState = this.companyScopeRef.state;
        const otherScopeState = this.otherScopeRef.state;
        const currentAccordion = cloneDeep( this.state.accordionData );

        if ( !currentAccordion.resources.actions.read ) {
            this.setState({ showActionsError: true });
            return;
        }

        this.setState({ showActionsError: false });

        currentAccordion.hasChanges = true;
        currentAccordion.isExpanded = false;
        currentAccordion.saved = true;

        if ( !currentAccordion.is_all_company ) {
            currentAccordion.companies = companyScopeState.values;
        }

        currentAccordion.resources.scope = otherScopeState.values;
        currentAccordion.resources.scope_switch_group = { ...otherScopeState.scopeSwitchGroup };

        this.props.onSave( currentAccordion );
    }

    handleClearChanges() {
        const currentAccordion = cloneDeep( this.state.accordionData );

        currentAccordion.is_all_company = false;
        currentAccordion.resources.actions = {
            create: false,
            read: false,
            update: false,
            delete: false
        };
        currentAccordion.resources.all_actions = false;
        currentAccordion.resources.scope = [];
        currentAccordion.resources.scope_switch_group = {
            department: false,
            location: false,
            payroll_group: false,
            position: false,
            team: false
        };

        currentAccordion.hasChanges = false;
        currentAccordion.isExpanded = false;
        currentAccordion.saved = false;

        this.companyScopeRef.clearScope();
        this.otherScopeRef.clearScope();

        this.clearModal.toggle();

        this.setState({
            accordionData: currentAccordion
        }, () => this.props.onSave( currentAccordion ) );
    }

    handleSelectCompany( values ) {
        const currentAccordion = cloneDeep( this.state.accordionData );

        if ( values.length ) {
            currentAccordion.resources.actions.read = true;
        }

        this.setState( ( prevState ) => ({
            accordionData: {
                ...prevState.accordionData,
                ...currentAccordion
            }
        }), () => this.checkForChanges() );
    }

    handleSelectAll( value ) {
        const currentAccordion = cloneDeep( this.state.accordionData );

        const companies = value ? ['__ALL__'] : [];

        if ( value ) {
            currentAccordion.resources.actions.read = true;
        } else {
            currentAccordion.resources.actions = {
                create: false,
                read: false,
                update: false,
                delete: false
            };
            currentAccordion.resources.all_actions = false;
        }

        this.setState( ( prevState ) => ({
            accordionData: {
                ...prevState.accordionData,
                ...currentAccordion,
                companies,
                is_all_company: value
            }
        }), () => this.checkForChanges() );
    }

    validateActions() {
        const currentAccordion = cloneDeep( this.state.accordionData );

        return currentAccordion.resources.actions.read;
    }

    handleSwitchAction( name, value ) {
        const currentAccordion = cloneDeep( this.state.accordionData );

        if ( name === 'read' && !value ) {
            this.discardModal.toggle();
            return;
        }

        currentAccordion.resources.actions[ name ] = value;

        if ( value ) {
            currentAccordion.resources.actions.read = value;

            if ( this.companyScopeRef.state.values.length === 0 ) {
                currentAccordion.is_all_company = value;
                currentAccordion.companies = ['__ALL__'];
            }
        }

        let actionsCount = 0;

        forIn( currentAccordion.resources.actions, ( val ) => {
            if ( val ) {
                actionsCount += 1;
            }
        });

        if ( actionsCount === 4 ) {
            currentAccordion.resources.all_actions = true;
        } else {
            currentAccordion.resources.all_actions = false;
        }

        this.setState({
            accordionData: currentAccordion
        }, () => this.checkForChanges() );
    }

    handleSwitchActionAll( value ) {
        const currentAccordion = cloneDeep( this.state.accordionData );

        if ( value ) {
            if ( this.companyScopeRef.state.values.length === 0 ) {
                currentAccordion.is_all_company = true;
                currentAccordion.companies = ['__ALL__'];
            }

            currentAccordion.resources.actions = {
                create: true,
                read: true,
                update: true,
                delete: ![ 'ess', 'main_page' ].includes( currentAccordion.id )
            };
            currentAccordion.resources.all_actions = true;

            this.setState({
                accordionData: currentAccordion
            }, () => this.checkForChanges() );
        } else {
            this.discardModal.toggle();
        }
    }

    handleDiscardChanges() {
        const currentAccordion = cloneDeep( this.state.accordionData );

        currentAccordion.is_all_company = false;
        currentAccordion.resources.actions = {
            create: false,
            read: false,
            update: false,
            delete: false
        };
        currentAccordion.resources.all_actions = false;

        this.setState({
            accordionData: currentAccordion
        }, () => this.checkForChanges() );

        this.discardModal.toggle();
    }

    checkForChanges() {
        const otherScopeState = this.otherScopeRef.state;
        const { accordionData } = this.state;

        let hasChange = false;

        if ( accordionData.resources.actions.read ) {
            hasChange = true;
        }

        if ( otherScopeState.values.length ) {
            hasChange = true;
        }

        this.props.onChange( hasChange );
    }

    render() {
        const { accordion, companyList, essentialData } = this.props;
        const { name, isExpanded, id, hasChanges, saved } = accordion;

        const { accordionData, showActionsError } = this.state;

        return (
            <CollapsibleContainer>
                <Modal
                    title="Discard Changes"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.discardModal = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Disabling all actions will clear the current scope. Are you sure?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'Cancel',
                            onClick: () => this.discardModal.toggle()
                        },
                        {
                            type: 'darkRed',
                            label: 'Confirm',
                            onClick: () => this.handleDiscardChanges()
                        }
                    ] }
                />
                <Modal
                    title="Clear Changes"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.clearModal = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Click confirm to clear all permissions and close permission tab.</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'Cancel',
                            onClick: () => this.clearModal.toggle()
                        },
                        {
                            type: 'darkRed',
                            label: 'Confirm',
                            onClick: () => this.handleClearChanges()
                        }
                    ] }
                />
                <FlexContainer justifyContent="space-between" alignItems="center">

                    <H4 noBottomMargin>
                        {name}
                        {!isExpanded && saved ? (
                            <span style={ { display: 'inline-block', marginLeft: 8, width: 18 } }>
                                <Icon name="tickCircle" style={ { color: '#83d24b' } } />
                            </span>
                        ) : null}
                    </H4>

                    <div className="control-buttons">
                        {isExpanded
                            ? hasChanges
                            ? (
                                <div>
                                    <Button
                                        type="neutral"
                                        label="Clear"
                                        onClick={ () => this.onClickClear() }
                                    />

                                    <Button
                                        type="action"
                                        label="Save"
                                        onClick={ () => this.onClickSave() }
                                    />
                                </div>
                            )
                            : <Button
                                type="neutral"
                                label="Close"
                                onClick={ () => this.props.onClickClose() }
                            />
                            : <Button
                                type="primary"
                                label="Edit"
                                onClick={ () => this.props.onClickEdit() }
                            />
                        }
                    </div>
                </FlexContainer>

                <Collapse isOpen={ isExpanded }>
                    <CollapsibleBody>
                        <CompanyScope
                            companyList={ companyList }
                            selectedCompanies={ accordionData.companies }
                            disabled={ ['control_panel'].includes( accordionData.id ) }
                            onSelectCompany={ ( val ) => this.handleSelectCompany( val ) }
                            onSelectAll={ ( val ) => this.handleSelectAll( val ) }
                            selectedAll={ accordionData.is_all_company }
                            ref={ ( ref ) => { this.companyScopeRef = ref; } }
                        />

                        <Row>
                            <OtherScope
                                accordionId={ id }
                                essentialData={ essentialData }
                                scope={ accordionData.resources.scope }
                                scopeSwitchGroup={ accordionData.resources.scope_switch_group }
                                ref={ ( ref ) => { this.otherScopeRef = ref; } }
                                onChange={ () => this.checkForChanges() }
                            />

                            <ActionScope
                                accordionId={ id }
                                actions={ accordionData.resources.actions }
                                allActions={ accordionData.resources.all_actions }
                                onSwitchAction={ ( action, value ) => this.handleSwitchAction( action, value ) }
                                onSwitchActionAll={ ( val ) => this.handleSwitchActionAll( val ) }
                                showError={ showActionsError }
                            />
                        </Row>
                    </CollapsibleBody>
                </Collapse>
            </CollapsibleContainer>
        );
    }
}

export default Permission;
