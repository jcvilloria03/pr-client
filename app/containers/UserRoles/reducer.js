import { fromJS } from 'immutable';

import {
    DEFAULT_ACTION,
    SET_NOTIFICATION,
    SET_LOADING,
    SET_USER_ROLES
} from './constants';

const initialState = fromJS({
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    userRoles: {
        roles: [],
        essentialData: {}
    }
});

/**
 *
 * Users reducer
 *
 */
function userRolesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_USER_ROLES:
            return state.set( 'userRoles', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default userRolesReducer;
