/*
 *
 * UserRoles constants
 *
 */
export const nameSpace = 'app/UserRoles';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;

export const SET_LOADING = `${nameSpace}/SET_LOADING`;

export const SET_NOTIFICATION = `${nameSpace}/SET_NOTIFICATION`;
export const GET_NOTIFICATION = `${nameSpace}/GET_NOTIFICATION`;

export const GET_USER_ROLES = `${nameSpace}/GET_USER_ROLES`;
export const SET_USER_ROLES = `${nameSpace}/SET_USER_ROLES`;

export const DELETE_USER_ROLE = `${nameSpace}/DELETE_USER_ROLE`;
