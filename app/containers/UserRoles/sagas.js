/* eslint-disable require-jsdoc */
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';

import {
    GET_USER_ROLES,
    SET_USER_ROLES,
    SET_NOTIFICATION,
    GET_NOTIFICATION,
    SET_LOADING,
    DELETE_USER_ROLE
} from './constants';

import { Fetch } from '../../utils/request';

import { auth } from '../../utils/AuthService';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* getUserRoles() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const accountId = auth.getUser().account_id;
        const [ roles, essentialData ] = yield [
            call( Fetch, `/accounts/${accountId}/roles`, { method: 'GET' }),
            call( Fetch, `/accounts/${accountId}/essential_data`, { method: 'GET' })
        ];

        yield put({
            type: SET_USER_ROLES,
            payload: {
                roles: roles.data,
                essentialData
            }
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* deleteUserRole({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        yield put({
            type: SET_USER_ROLES,
            payload: {
                roles: [],
                essentialData: {}
            }
        });

        const accountId = auth.getUser().account_id;

        yield call( Fetch, `/accounts/${accountId}/roles/${payload}`, {
            method: 'DELETE'
        });

        yield call( getUserRoles );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully deleted!',
            show: true,
            type: 'success'
        });

        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 2000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

export function* watchGetUserRoles() {
    const watcher = yield takeEvery( GET_USER_ROLES, getUserRoles );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchDeleteUserRole() {
    const watcher = yield takeEvery( DELETE_USER_ROLE, deleteUserRole );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchNotify() {
    const watcher = yield takeEvery( GET_NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchNotify,
    watchGetUserRoles,
    watchDeleteUserRole
];
