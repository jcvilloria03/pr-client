/* eslint-disable require-jsdoc */
import {
    SET_UPDATE_USER_ROLE,
    GET_USER_ROLE
} from './constants';

export function getUserRole( payload ) {
    return {
        type: GET_USER_ROLE,
        payload
    };
}

export function updateUserRole( payload ) {
    return {
        type: SET_UPDATE_USER_ROLE,
        payload
    };
}
