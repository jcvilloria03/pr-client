/* eslint-disable require-jsdoc */
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';

import { browserHistory } from 'utils/BrowserHistory';
import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';

import {
    SET_ESSENTIAL_DATA,
    SET_NOTIFICATION,
    GET_NOTIFICATION,
    SET_LOADING,
    SET_COMPANIES,
    SET_UPDATE_USER_ROLE,
    GET_USER_ROLE,
    SET_USER_ROLE_DATA
} from './constants';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* getGetUserRole({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const accountId = auth.getUser().account_id;
        const [ roleData, essentialData ] = yield [
            call( Fetch, `/accounts/${accountId}/roles/${payload}`, { method: 'GET' }),
            call( Fetch, `/accounts/${accountId}/essential_data`, { method: 'GET' })
        ];

        yield put({
            type: SET_USER_ROLE_DATA,
            payload: roleData.data
        });

        const { company, ...others } = essentialData;
        yield put({
            type: SET_ESSENTIAL_DATA,
            payload: { ...others }
        });
        yield put({
            type: SET_COMPANIES,
            payload: company
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* updateUserRole({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const accountId = auth.getUser().account_id;

        yield call( Fetch, `/accounts/${accountId}/roles/${payload.roleId}`, {
            method: 'PUT',
            data: payload.payload
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Role has been successfully Updated',
            show: true,
            type: 'success'
        });
        yield call( browserHistory.push( '/control-panel/roles/user-roles', true ) );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

export function* notifyUser( payload ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

export function* watchUpdateUserRole() {
    const watcher = yield takeEvery( SET_UPDATE_USER_ROLE, updateUserRole );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchGetUserRole() {
    const watcher = yield takeEvery( GET_USER_ROLE, getGetUserRole );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchNotify() {
    const watcher = yield takeEvery( GET_NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchUpdateUserRole,
    watchGetUserRole,
    watchNotify
];
