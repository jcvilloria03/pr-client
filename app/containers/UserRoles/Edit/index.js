import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import { each, forIn, cloneDeep, isEmpty, values } from 'lodash';

import SnackBar from 'components/SnackBar';
import A from 'components/A';
import { H2, H3, P } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Input from 'components/Input';
import Icon from 'components/Icon';
import Modal from 'components/Modal';

import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';

import { makeSelectIsExpired } from 'containers/App/selectors';

import Permission from '../components/Permission';

import { initalizeAccordion, PERMISSION_MODULES, OTHER_SCOPE_GROUPS } from '../components/constants';

import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectUserRoleData,
    makeSelectCompanies,
    makeSelectEssentialData
} from './selectors';
import * as usersActions from './actions';

import {
    HeaderTitle,
    StyledContainer,
    LoadingStyles,
    NavWrapper,
    Card,
    Footer
} from './styles';

/**
 *
 * Users
 *
 */
export class EditUserRoles extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        params: React.PropTypes.object,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        isExpired: React.PropTypes.bool,
        getUserRole: React.PropTypes.func,
        updateUserRole: React.PropTypes.func,
        userRoleData: React.PropTypes.object,
        companies: React.PropTypes.array,
        essentialData: React.PropTypes.object
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            roleName: '',
            isNameValid: true,
            validateName: 'name',
            validateExistName: false,
            accordions: initalizeAccordion(),
            loadingAccordionData: false
        };
    }

    componentDidMount() {
        this.props.getUserRole( this.props.params.roleId );
    }

    componentWillReceiveProps() {
        if ( !isEmpty( this.props.userRoleData ) && !isEmpty( this.props.essentialData ) && !isEmpty( this.props.companies ) ) {
            this.setState({ loadingAccordionData: true });
            this.initalizeRoleData();
        }
    }

    initalizeRoleData() {
        const { name, policy } = cloneDeep( this.props.userRoleData );
        const essentialData = cloneDeep( this.props.essentialData );
        const companies = cloneDeep( this.props.companies );
        const accordions = cloneDeep( this.state.accordions );

        const modules = {};

        if ( Object.keys( policy.resource ).includes( 'root' ) ) {
            delete policy.resource.root;
        }

        each( policy.resource, ( permissionModules, index ) => {
            const permissionModule = values( permissionModules )[ 0 ];
            const resources = {
                companies: [],
                all_companies: false,
                actions: {
                    create: false,
                    read: false,
                    update: false,
                    delete: false
                },
                scopes: [],
                scope_switch_group: {
                    department: false,
                    position: false,
                    team: false,
                    location: false,
                    payroll_group: false
                }
            };

            each( permissionModule.actions, ( item ) => {
                resources.actions[ item.toLowerCase() ] = true;
            });

            each( permissionModule.scopes, ( item, i ) => {
                each( item, ( scope ) => {
                    if ( scope === '__ALL__' ) {
                        if ( i === 'COMPANY' ) {
                            resources.all_companies = true;
                            resources.companies.push( '__ALL__' );
                        } else {
                            const key = i.toLowerCase();

                            resources.scope_switch_group[ key ] = true;
                            resources.scopes.push({
                                group: key,
                                value: key
                            });
                        }
                    } else if ( i === 'COMPANY' ) {
                        const company = companies.find( ( d ) => d.id === scope );
                        if ( company ) {
                            resources.companies.push( company.id );
                        }
                    } else {
                        const essential = essentialData[ i.toLowerCase() ].find( ( d ) => d.id === scope );
                        if ( essential ) {
                            resources.scopes.push({ group: i.toLowerCase(), value: essential.id });
                        }
                    }
                });
            });

            modules[ index ] = {
                companies: resources.companies,
                is_all_company: resources.all_companies,
                resources: {
                    all_actions: Object.values( resources.actions ).every( Boolean ),
                    actions: resources.actions,
                    scope: resources.scopes,
                    scope_switch_group: resources.scope_switch_group
                },
                hasChanges: true,
                saved: true
            };
        });

        each( modules, ( accordion, index ) => {
            Object.assign( accordions[ index ], accordion );
        });

        this.setState({ roleName: name, loadingAccordionData: false, accordions });
    }

    handleAccordionOpenClose( accordionKey, isExpanded ) {
        const accordions = cloneDeep( this.state.accordions );
        let newAccordions = {};

        forIn( accordions, ( accordion, key ) => {
            let expanded = false;

            if ( key === accordionKey ) {
                expanded = isExpanded;
            }

            newAccordions = { ...newAccordions, [ key ]: { ...accordion, isExpanded: expanded }};
        });

        this.setState({ accordions: newAccordions });
    }

    handleAccordionChanges( accordionKey, hasChanges ) {
        const accordions = cloneDeep( this.state.accordions );
        let newAccordions = {};

        forIn( accordions, ( accordion, key ) => {
            let changes = false;

            if ( key === accordionKey ) {
                changes = hasChanges;
            }

            newAccordions = { ...newAccordions, [ key ]: { ...accordion, hasChanges: changes }};
        });

        this.setState({ accordions: newAccordions });
    }

    handleSaveAccordion( accordionKey, accordionData ) {
        const accordions = cloneDeep( this.state.accordions );
        let newAccordions = {};

        forIn( accordions, ( accordion, key ) => {
            let newAccordionData = accordion;

            if ( key === accordionKey ) {
                newAccordionData = accordionData;
            }

            newAccordions = { ...newAccordions, [ key ]: { ...newAccordionData }};
        });

        this.setState({ accordions: newAccordions });
    }

    handleClickUpdate() {
        if ( this.validateForm() ) {
            if ( this.validateMainPage() ) {
                this.preparePayload();
            }
        } else {
            this.InvalidFormModal.toggle();
        }
    }

    validateForm() {
        const { roleName, accordions } = cloneDeep( this.state );

        let valid = true;

        const emptyAccordions = {};

        let isNameValid = true;
        if ( roleName.trim() === '' ) {
            isNameValid = false;
        } else {
            isNameValid = true;
        }
        valid = isNameValid;

        each( accordions, ( accordion, index ) => {
            emptyAccordions[ index ] = !accordion.resources.actions.read;
        });

        if ( Object.values( emptyAccordions ).every( Boolean ) ) {
            valid = false;
        }

        this.setState({ isNameValid });

        return valid;
    }

    validateMainPage() {
        const { accordions } = cloneDeep( this.state );

        let valid = true;

        if ( !accordions.main_page.resources.actions.read ) {
            valid = false;
            this.MainPageEmptyModal.toggle();
        }

        return valid;
    }

    preparePayload() {
        const accordions = cloneDeep( this.state.accordions );

        const modules = this.preparePayloadModules();
        const isEmployee = ( accordions.ess.companies.length || accordions.ess.resources.actions.read ) && ( Object.values( accordions.ess.resources.actions ) ).includes( true );
        let companyIds = [];

        each( accordions, ( accordion ) => {
            const companies = this.prepareCompanyIds( accordion.companies, accordion.is_all_company );

            if ( ![ 'control_panel', 'main_page', 'ess' ].includes( accordion.id ) ) {
                each( companies, ( ids ) => {
                    !companyIds.includes( ids ) && companyIds.push( ids );
                });
            }
        });

        if ( companyIds.includes( '__ALL__' ) ) {
            companyIds = [];
        }

        const payload = {
            name: this.state.roleName,
            is_employee: Boolean( isEmployee ),
            permissions: {
                ...modules
            }
        };

        Object.assign( payload, {
            company_ids: companyIds,
            is_all_companies: isEmpty( companyIds )
        });

        this.props.updateUserRole({ payload, roleId: this.props.params.roleId });
    }

    preparePayloadModules() {
        const accordions = cloneDeep( this.state.accordions );

        const modules = {};

        each( PERMISSION_MODULES, ( permission, index ) => {
            const scopes = {};
            const actions = [];

            if ( !isEmpty( accordions[ index ].companies ) && ( Object.values( accordions[ index ].resources.actions ) ).includes( true ) ) {
                if ( !isEmpty( accordions[ index ].resources.scope ) ) {
                    each( accordions[ index ].resources.scope, ( scopeItem ) => {
                        const groupName = ( scopeItem.group ).toUpperCase();

                        if ( !Object.keys( scopes ).includes( groupName ) ) {
                            scopes[ groupName ] = [];
                        }

                        if ( OTHER_SCOPE_GROUPS.includes( scopeItem.value ) ) {
                            scopes[ groupName ].push( '__ALL__' );
                        } else {
                            scopes[ groupName ].push( scopeItem.value );
                        }
                    });
                }

                const companies = this.prepareCompanyIds( accordions[ index ].companies, accordions[ index ].is_all_company );

                if ( accordions[ index ].companies.length > 0 ) {
                    Object.assign( scopes, { COMPANY: companies });
                }

                each( accordions[ index ].resources.actions, ( actionItem, key ) => {
                    actionItem && actions.push( key.toUpperCase() );
                });

                each( permission, ( moduleItem ) => {
                    modules[ moduleItem ] = {
                        scopes: { ...scopes },
                        actions
                    };
                });
            }
        });

        return modules;
    }

    prepareCompanyIds( data, allCompanies ) {
        const companies = [];

        allCompanies
          ? companies.push( '__ALL__' )
          : each( data, ( company ) => {
              companies.push( company );
          });

        return companies;
    }

    /**
     *
     * Users render method
     *
     */
    render() {
        const {
            isExpired,
            notification,
            loading,
            companies,
            essentialData
        } = this.props;

        const { accordions, isNameValid, roleName, loadingAccordionData } = this.state;

        return (
            <div>
                <Helmet
                    title="User Roles"
                    meta={ [
                        { name: 'description', content: 'List of User Roles' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        isExpired,
                        salpayViewPermission: true,
                        isSubscribedToPayroll: true,
                        accountViewPermission: true
                    }) }
                />

                <Modal
                    title="Create Role"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.InvalidFormModal = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Unable to Create Role. Please review permissions and try again.</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'action',
                            label: 'Okay',
                            onClick: () => this.InvalidFormModal.toggle()
                        }
                    ] }
                />

                <Modal
                    title="Create Role"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.MainPageEmptyModal = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>At least `READ` permission for main page is required when permission is enabled for company settings, employees, time and attendance or payroll. The system will append this for your convenience.</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'Cancel',
                            onClick: () => this.MainPageEmptyModal.toggle()
                        },
                        {
                            type: 'action',
                            label: 'Confirm',
                            onClick: () => {
                                this.MainPageEmptyModal.toggle();
                                this.preparePayload();
                            }
                        }
                    ] }
                />

                <Modal
                    title="Discard Changes"
                    className="modal-md modal-commission-type"
                    body={
                        <div>
                            <p> Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.discardModal.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/control-panel/roles/user-roles', true );
                                this.discardModal.toggle();
                            }
                        }
                    ] }
                    center
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                />

                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/control-panel/roles/user-roles', true );
                            } }
                        >
                            <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to User Roles</span>
                        </A>
                    </Container>
                </NavWrapper>
                {( loading || loadingAccordionData )
                    ? (
                        <div className="loader">
                            <LoadingStyles>
                                <H2>Loading User Roles.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                    ) : (
                        <div className="content">
                            <StyledContainer>
                                <HeaderTitle>Edit Roles</HeaderTitle>

                                <div>
                                    <H3 noBottomMargin className="role-details-title">Role Details</H3>

                                    <Card>
                                        <div className={ isNameValid ? 'input-role-name-container' : 'input-role-name-container input-error' }>
                                            <Input
                                                label="* Role Name"
                                                id="role-name"
                                                name="role-name"
                                                value={ roleName }
                                                onChange={ ( val ) => {
                                                    this.setState({ roleName: val, isNameValid: true });
                                                } }
                                                placeholder="Type your desired role name"
                                            />
                                            {!isNameValid && <P noBottomMargin>Role Name is required</P>}
                                        </div>
                                    </Card>
                                </div>

                                <div>
                                    <H3 noBottomMargin className="permissions-title">Permissions</H3>

                                    <Card>
                                        {
                                            Object.keys( accordions ).map( ( accordionKey ) => {
                                                const accordion = accordions[ accordionKey ];

                                                return (
                                                    <Permission
                                                        key={ accordion.id }
                                                        companyList={ companies }
                                                        essentialData={ essentialData }
                                                        accordion={ accordion }
                                                        onClickEdit={ () => this.handleAccordionOpenClose( accordionKey, true ) }
                                                        onClickClose={ () => this.handleAccordionOpenClose( accordionKey, false ) }
                                                        onChange={ ( changes ) => this.handleAccordionChanges( accordionKey, changes ) }
                                                        onSave={ ( accordionData ) => this.handleSaveAccordion( accordionKey, accordionData ) }
                                                    />
                                                );
                                            })
                                        }
                                    </Card>
                                </div>
                            </StyledContainer>

                            <Footer>
                                <div className="submit">
                                    <Button
                                        label="Cancel"
                                        type="action"
                                        size="large"
                                        className="cancel-button"
                                        alt
                                        onClick={ () => this.discardModal.toggle() }
                                    />
                                    <Button
                                        label="Update"
                                        type="action"
                                        size="large"
                                        onClick={ () => this.handleClickUpdate() }
                                    />
                                </div>
                            </Footer>
                        </div>
                    )
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    companies: makeSelectCompanies(),
    essentialData: makeSelectEssentialData(),
    userRoleData: makeSelectUserRoleData(),
    isExpired: makeSelectIsExpired()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        usersActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EditUserRoles );
