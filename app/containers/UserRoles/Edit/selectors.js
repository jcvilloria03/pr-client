import { createSelector } from 'reselect';

/**
 * Direct selector to the users state domain
 */
const selectUserRolesEditDomain = () => ( state ) => state.get( 'editRoles' );

const makeSelectLoading = () => createSelector(
  selectUserRolesEditDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectUserRolesEditDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectEssentialData = () => createSelector(
  selectUserRolesEditDomain(),
  ( substate ) => substate.get( 'essentialData' ).toJS()
);

const makeSelectCompanies = () => createSelector(
  selectUserRolesEditDomain(),
  ( substate ) => substate.get( 'companies' ).toJS()
);

const makeSelectUserRoleData = () => createSelector(
    selectUserRolesEditDomain(),
    ( substate ) => substate.get( 'userRoleData' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectNotification,
  makeSelectEssentialData,
  makeSelectCompanies,
  makeSelectUserRoleData
};
