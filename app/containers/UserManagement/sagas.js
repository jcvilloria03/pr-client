import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { call, cancel, put, take } from 'redux-saga/effects';
import { Fetch } from '../../utils/request';
import {
    FETCH_PAGINATED_USER_LIST,
    GET_USERS_STATS,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_LOADING,
    SET_PAGINATED_USER_LIST,
    SET_TABLE_LOADING,
    SET_USERS_STATS,
    SET_USER_STATUS,
    UPDATE_USER_STATUS,
    SET_PAGINATION
} from './constants';
/**
 * Handle paginated request for list of users
 */
export function* fetchPaginatedUsersHandler({ payload }) {
    try {
        yield put({
            type: SET_TABLE_LOADING,
            payload: true
        });
        const { data, total, per_page, current_page, last_page, to } = yield call(
            Fetch, `/account/users?${payload}`, {
                method: 'GET'
            },
            payload
        );
        yield put({
            type: SET_PAGINATED_USER_LIST,
            payload: { data, total }
        });
        yield put({
            type: SET_PAGINATION,
            payload: { total, per_page: Number( per_page ), current_page, last_page, to }
        });
// current_page: 1
// from: 1
// last_page: 7
// per_page: "10"
// to: 10
// total: 65
        yield put({
            type: SET_TABLE_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield put(
            yield call( notifyUser, {
                title: 'Error',
                message: error.response.data.message || 'Something went wrong',
                show: 'true',
                type: 'error'
            })
        );
        yield delay( 5500 );
    } finally {
        yield put({
            type: SET_TABLE_LOADING,
            payload: false
        });
    }
}

/**
 * handle fetching of user stats
 */
export function* getUserStatsHandler() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const profile = JSON.parse( localStorage.getItem( 'user' ) );
        const stats = yield call( Fetch, `/subscriptions/${profile.subscription.id}/stats`, { method: 'GET' });
        yield put({
            type: SET_USERS_STATS,
            payload: stats.data
        });
    } catch ( error ) {
        yield put(
            yield call( notifyUser, {
                title: 'Error',
                message: error.response.data.message || 'Something went wrong',
                show: 'true',
                type: 'error'
            })
        );
        yield delay( 5500 );
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}
/**
 * handle updating user statis
 */
export function* updateUserStatusHandler({ payload: { id, status }}) {
    try {
        yield put({
            type: SET_TABLE_LOADING,
            payload: true
        });
        yield call( Fetch, `/user/${id}/set_status`, { method: 'PATCH', data: { status }});

        yield put({
            type: SET_USER_STATUS,
            payload: { id, status }
        });
        yield put({
            type: SET_TABLE_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield put(
            yield call( notifyUser, {
                title: 'Error',
                message: error.response.data.message || 'Something went wrong',
                show: 'true',
                type: 'error'
            })
        );
        yield delay( 5500 );
    } finally {
        yield put({
            type: SET_TABLE_LOADING,
            payload: false
        });
    }
}

/**
 * Individual exports for testing
 */
export function* fetchPaginatedUsersWatcher() {
    const watcher = yield takeLatest(
        FETCH_PAGINATED_USER_LIST,
        fetchPaginatedUsersHandler
    );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * get user stats watcher
 */
export function* getUserStatsWatcher() {
    const watcher = yield takeLatest(
        GET_USERS_STATS,
        getUserStatsHandler
    );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * update user status
 */
export function* udpateUserStatusWatcher() {
    const watcher = yield takeLatest(
        UPDATE_USER_STATUS,
        updateUserStatusHandler
    );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watch For Notification
 */
export function* watchForNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [ fetchPaginatedUsersWatcher, getUserStatsWatcher, udpateUserStatusWatcher, watchForNotification ];
