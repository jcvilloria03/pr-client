/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectIsExpired } from 'containers/App/selectors';

import Button from 'components/Button';
import SalDropdown from 'components/SalDropdown';
import Search from 'components/Search';
import Table from 'components/Table';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';
import { H5, H3, H2, P } from 'components/Typography';

import { formatPaginationLabel } from 'utils/functions';
import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';

import {
    Center,
    HeaderTitle,
    StyledContainer,
    SubTitle,
    FlexContainer,
    LoadingStyles
} from './styles';
import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectProductsState,
    makeSelectTableLoading,
    makeSelectUserManagement,
    makeSelectPagination
} from './selectors';
import * as getUsersStatsActions from './actions';
import UserStats from './templates/UserStats';

/**
 *
 * UserManagement
 *
 */
export class UserManagement extends React.Component {
    static propTypes = {
        fetchPaginatedUserList: React.PropTypes.func,
        getUsersStats: React.PropTypes.func,
        updateUserStatus: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool,
        tableLoading: React.PropTypes.bool,
        isExpired: React.PropTypes.bool
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            search: '',
            page: 1,
            pageSize: 10,
            tableLabel: ''
        };
    }

    componentDidMount() {
        this.props.getUsersStats();
    }

    componentWillReceiveProps() {
        const { current_page, per_page, total } = this.props.pagination;
        this.setState({ tableLabel: formatPaginationLabel({ page: Number( current_page ) - 1, pageSize: per_page, dataLength: total }) });
    }

    getUserList( _params ) {
        const params = new URLSearchParams( _params );
        this.props.fetchPaginatedUserList( params && params.toString() );
    }

    handleAddNewUser = () => {
        browserHistory.push( '/control-panel/users/create', true );
    };

    handleStatusClick = ( id, status ) => {
        this.props.updateUserStatus( id, status );
    };

    handleTableChanges = ({ page, pageSize }) => {
        this.getUserList({
            page: page + 1,
            per_page: pageSize,
            ...( this.state.search && { keyword: this.state.search })
        });
    };

    handleSearch = ( e ) => {
        e.preventDefault();

        this.getUserList({
            page: 1,
            per_page: 10,
            ...( this.state.search && { keyword: this.state.search })
        });
    };

    handleTableParamsChanges = ( name, value ) => {
        if ( this.props.tableLoading ) {
            return;
        }

        this.setState(
            ( prev ) => ({ ...prev, [ name ]: value }),
            () => {
                this.getUserList({
                    page: this.state.page,
                    per_page: this.state.pageSize,
                    ...( this.state.search && { keyword: this.state.search })
                });
            }
        );
    };

    /**
     *
     *
     * UserManagement render method
     *
     */
    render() {
        const statusList = [
            {
                label: 'Active',
                value: 'active'
            },
            {
                label: 'Semi Active',
                value: 'semi-active'
            },
            {
                label: 'Inactive',
                value: 'inactive'
            }
        ];

        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Name',
                minWidth: 120,
                sortable: true,
                render: ({ row }) => (
                    <span>{row.last_name}, {row.first_name}</span>
                )
            },
            {
                header: 'Email',
                accessor: 'email',
                minWidth: 250,
                sortable: false
            },

            {
                header: 'User Role',
                accessor: 'crbac_role.name',
                minWidth: 250,
                sortable: false
            },
            {
                header: 'Status',
                accessor: 'status',
                width: 170,
                sortable: false,
                render: ({ row }) => {
                    const dropdownItems = statusList.map( ( status ) => ({
                        children: status.label,
                        onClick: () => this.handleStatusClick( row.id, status.value )
                    }) );

                    const statusItem = statusList.find( ( status ) => status.value === row.active );

                    let label = '';
                    if ( statusItem ) {
                        label = statusItem.label;
                    }

                    return (
                        <div className="status-dropdown">
                            <SalDropdown
                                new
                                backgroundColor="#f0f4f6"
                                textColor="#474747"
                                label={ label }
                                dropdownItems={ dropdownItems }
                            />
                        </div>
                    );
                }
            },
            {
                header: ' ',
                accessor: 'actions',
                width: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        label={ <span>View</span> }
                        type="grey"
                        size="small"
                        onClick={ () => {
                            browserHistory.push( `/control-panel/users/${row.id}`, true );
                        } }
                    />
                )
            }
        ];

        const {
            notification,
            loading,
            tableLoading,
            isExpired,
            UserManagement,
            pagination
        } = this.props;

        return (
            <div>
                <Helmet
                    title="User Management"
                    meta={ [
                        { name: 'description', content: 'Description of User Management' }
                    ] }
                />
                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        isExpired,
                        salpayViewPermission: true,
                        isSubscribedToPayroll: true,
                        accountViewPermission: true
                    }) }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <div className="loader" style={ { display: loading ? '' : 'none' } }>
                    <LoadingStyles>
                        <H2>Loading User Management.</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoadingStyles>
                </div>
                <div className="content" style={ { display: loading ? 'none' : '' } }>
                    <StyledContainer>
                        <HeaderTitle>User Management</HeaderTitle>
                        <SubTitle>
                            Manage your users and administrators manually or by batch
                            through this page.
                        </SubTitle>
                        <div className="btn-add-user">
                            <Center>
                                <Button
                                    label="Add New User"
                                    type="action"
                                    onClick={ () => this.handleAddNewUser() }
                                />
                            </Center>
                        </div>
                        <div className="user-stats">
                            <UserStats stats={ UserManagement.userStats } />
                        </div>
                        <form onSubmit={ this.handleSearch }>
                            <div className="user-search">
                                <FlexContainer
                                    justifyContent="space-between"
                                    alignItems="center"
                                >
                                    <FlexContainer
                                        justifyContent="start"
                                        alignItems="center"
                                        gap="8"
                                    >
                                        <H5 noBottomMargin className="user-list">User List</H5>
                                        <Search
                                            handleSearch={ ( val ) => {
                                                this.setState({ search: val });
                                            } }
                                        />
                                    </FlexContainer>
                                    <P noBottomMargin>{this.state.tableLabel}</P>
                                </FlexContainer>
                            </div>
                        </form>
                        <Table
                            data={ UserManagement.list }
                            columns={ tableColumns }
                            pages={ UserManagement.total }
                            pageSize={ pagination.per_page }
                            page={ 0 }
                            onDataChange={ this.handleTableChanges }
                            loading={ tableLoading }
                            external
                            className="userlist-table"
                        />
                        <div>
                            <FooterTablePaginationV2
                                page={ pagination.current_page || 1 }
                                pageSize={ pagination.per_page || 10 }
                                pagination={ pagination }
                                onPageChange={ ( val ) => this.handleTableParamsChanges( 'page', val ) }
                                onPageSizeChange={ ( val ) => this.handleTableParamsChanges( 'pageSize', val ) }
                                paginationLabel={ this.state.tableLabel }
                                fluid
                            />
                        </div>
                    </StyledContainer>
                </div>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    UserManagement: makeSelectUserManagement(),
    products: makeSelectProductsState(),
    isExpired: makeSelectIsExpired(),
    loading: makeSelectLoading(),
    tableLoading: makeSelectTableLoading(),
    notification: makeSelectNotification(),
    pagination: makeSelectPagination()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        getUsersStatsActions,
        dispatch
    );
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( UserManagement );
