import { createSelector } from 'reselect';

/**
 * Direct selector to the userManagement state domain
 */
const selectUserManagementDomain = () => ( state ) => state.get( 'userManagement' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by UserManagement
 */

const makeSelectUserManagement = () =>
    createSelector( selectUserManagementDomain(), ( substate ) => substate.toJS() );

const makeSelectProductsState = () =>
    createSelector( selectAppDomain(), ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    });

const makeSelectLoading = () => createSelector(
        selectUserManagementDomain(),
      ( substate ) => substate.get( 'loading' )
    );

const makeSelectTableLoading = () => createSelector(
        selectUserManagementDomain(),
      ( substate ) => substate.get( 'tableLoading' )
    );

const makeSelectNotification = () => createSelector(
    selectUserManagementDomain(),
        ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectPagination = () => createSelector(
    selectUserManagementDomain(),
        ( substate ) => substate.get( 'pagination' ).toJS()
);

export default makeSelectUserManagement;
export {
    selectUserManagementDomain,
    makeSelectProductsState,
    makeSelectUserManagement,
    makeSelectLoading,
    makeSelectTableLoading,
    makeSelectNotification,
    makeSelectPagination
};
