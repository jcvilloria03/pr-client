import {
    FETCH_ROLES,
    FETCH_SUBSCRIPTIONS,
    GET_USER_DETAILS_BY_ID,
    NOTIFICATION,
    REST_DETAILS,
    UPDATE_USER
} from './constants';

/**
 *
 * EditUser actions
 *
 */
/**
 * @param {Number} id - user id
 */
export function getUserDetailsById( id ) {
    return {
        type: GET_USER_DETAILS_BY_ID,
        payload: id
    };
}

/**
 * reset user details state
 */
export function resetDetails() {
    return {
        type: REST_DETAILS
    };
}

/**
 * get user roles
 *@param {number} accountId - account id
 */
export function fetchRolesOption( accountId ) {
    return {
        type: FETCH_ROLES,
        payload: accountId
    };
}

/**
 * fetch subscriptions
 *
 */
export function fetchSubscriptions() {
    return {
        type: FETCH_SUBSCRIPTIONS
    };
}

/**
 * update User
 *@param {Number} id - user id
 *@param {Object} user - user object
 */
export function updateUser( id, user ) {
    return {
        type: UPDATE_USER,
        payload: { id, user }
    };
}

/**
 * create user
 *@param {Object} notify
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

