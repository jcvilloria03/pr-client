/* eslint-disable no-shadow */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
import { makeSelectIsExpired } from 'containers/App/selectors';
import React from 'react';
import { connect } from 'react-redux';
import { Col, Row } from 'reactstrap';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import Button from 'components/Button';
import { H3, H2, P } from 'components/Typography';
import Modal from 'components/Modal';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';

import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';

import userPayloadValidationSchema from '../utils/validation';
import AssignSubscription from '../templates/AssignSubscription';
import LinkedEmployee from '../templates/LinkedEmployee';
import UserForm from '../templates/UserForm';
import {
    makeSelectEditUser,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectProductsState,
    makeSelectUserInfo,
    makeSelectUserManagement
} from './selectors';
import * as mainActions from '../actions';
import * as addUserActions from './actions';

import {
    Card,
    FormContainer,
    StyledContainer,
    Footer,
    LoadingStyles
} from '../styles';

const initialFormState = {
    role_id: 0,
    account_id: 0,
    first_name: '',
    middle_name: '',
    last_name: '',
    email: '',
    assigned_product_seats_ids: [],
    license_unit_id: 0,
    subscription_product_id: 0
};

/**
 *
 * EditUser
 *
 */
export class EditUser extends React.Component {
    static propTypes = {
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool,
        fetchRolesOption: React.PropTypes.func,
        getUserDetailsById: React.PropTypes.func,
        fetchSubscriptions: React.PropTypes.func,
        updateUser: React.PropTypes.func,
        isExpired: React.PropTypes.bool,
        userInfo: React.PropTypes.object
    }

    static defaultProps = {
        loading: false
    };

    constructor( props ) {
        super( props );

        this.state = {
            error: {},
            formBody: initialFormState,
            loading: false
        };
        this.cancelModal = null;
    }

    componentDidMount() {
        this.props.resetDetails();
        this.props.getUserDetailsById( this.props.router.params.userId );
        this.props.fetchSubscriptions();
        this.props.getUsersStats();
        const { account_id } = this.props.userInfo;
        if ( account_id ) {
            this.handleInputChange( 'account_id', account_id );
            this.props.fetchRolesOption( account_id );
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.userInfo.account_id !== this.props.userInfo.account_id ) {
            this.handleInputChange( 'account_id', nextProps.userInfo.account_id );
            this.props.fetchRolesOption( nextProps.userInfo.account_id );
        }

        if ( nextProps.EditUser.userDetails && nextProps.EditUser.availableProductSeats.length > 0 ) {
            const assigned_product_seats_ids =
                nextProps.EditUser.userDetails.assigned_licenses_id.map(
                    ( item ) => item.id
                );

            const subscription_product_id =
                nextProps.EditUser.availableProductSeats.find(
                    ( item ) => item.id === assigned_product_seats_ids[ 0 ]
                ).subscription_product_id;

            this.setState( ( prev ) => ({
                ...prev,
                formBody: {
                    ...prev.formBody,
                    role_id: nextProps.EditUser.userDetails.role_id,
                    first_name: nextProps.EditUser.userDetails.first_name,
                    middle_name: nextProps.EditUser.userDetails.middle_name,
                    last_name: nextProps.EditUser.userDetails.last_name,
                    email: nextProps.EditUser.userDetails.email,
                    license_unit_id: nextProps.EditUser.userDetails.license_unit_id,
                    assigned_product_seats_ids,
                    subscription_product_id
                }
            }) );
        }
    }

    handleInputChange = ( name, val ) => {
        this.setState( ( prev ) => ({
            ...prev,
            formBody: {
                ...prev.formBody,
                [ name ]: val
            }
        }) );
    };

    handleSubscriptionChange = (
        assigned_product_seats_ids,
        subscription_product_id
    ) => {
        this.setState( ( prev ) => ({
            ...prev,
            formBody: {
                ...prev.formBody,
                assigned_product_seats_ids,
                subscription_product_id
            }
        }) );
    };

    handleSave = () => {
        userPayloadValidationSchema( this.state.formBody )
            .then( ( validatedForm ) => {
                this.props.updateUser( this.props.EditUser.userDetails.id, validatedForm );
            })
            .catch( ( err ) => {
                const _error = {};
                err.inner.forEach( ( e ) => {
                    _error[ e.path ] = e.message;
                });

                this.setState({ error: _error });
            });
    };

    handleCancel = () => {
        this.cancelModal.toggle();
    };
    handleCancelModal = () => {
        this.cancelModal.close();
    };
    handleDiscard = () => {
        this.setState({ formBody: initialFormState });
        this.props.router.push( '/control-panel/users/management' );
    };
    /**
     *
     * EditUser render method
     *
     */
    render() {
        const { notification, loading, UserManagement, userInfo, isExpired, EditUser } = this.props;
        const { error, formBody } = this.state;
        const { assigned_product_seats_ids, subscription_product_id } = this.state.formBody;

        return (
            <div>
                <Helmet
                    title="Edit User"
                    meta={ [
                        { name: 'description', content: 'Description of Edit User' }
                    ] }
                />
                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        isExpired,
                        salpayViewPermission: true,
                        isSubscribedToPayroll: true,
                        accountViewPermission: true
                    }) }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Modal
                    ref={ ( ref ) => {
                        this.cancelModal = ref;
                    } }
                    title="Discard changes"
                    className="modal-md modal-commission-type"
                    showClose={ false }
                    body={
                        <p>
                            Clicking Discard will undo all changes you made on
                            this page. Are you sure you want to proceed?
                        </p>
                    }
                    buttons={ [
                        {
                            label: 'Stay on this page',
                            type: 'grey',
                            onClick: this.handleCancelModal
                        },
                        {
                            label: 'Discard',
                            type: 'darkRed',
                            onClick: this.handleDiscard
                        }
                    ] }
                    center
                />
                <StyledContainer>
                    <div className="loader" style={ { display: loading ? '' : 'none' } }>
                        <LoadingStyles>
                            <H2>Loading Edit User Details</H2>
                            <br />
                            <H3>Please wait...</H3>
                        </LoadingStyles>
                    </div>
                    <div className="content" style={ { display: loading ? 'none' : '' } }>
                        <FormContainer>
                            <H3 noBottomMargin className="edit-user-title">Edit User</H3>

                            <P noBottomMargin className="edit-user-details">User Details</P>

                            <Card>
                                <UserForm
                                    handleInputChange={ this.handleInputChange }
                                    rolesOptions={ EditUser.rolesOptions }
                                    error={ error }
                                    role_id={ formBody.role_id }
                                    last_name={ formBody.last_name }
                                    first_name={ formBody.first_name }
                                    middle_name={ formBody.middle_name }
                                    email={ formBody.email }
                                />

                                <Row>
                                    <Col xs={ 6 }>
                                        <AssignSubscription
                                            mode="edit"
                                            subscriptions={ EditUser.availableProductSeats }
                                            value={ {
                                                assigned_product_seats_ids,
                                                subscription_product_id
                                            } }
                                            onChange={ this.handleSubscriptionChange }
                                            user={ userInfo }
                                            subscriptionStats={ UserManagement.userStats }
                                        />
                                    </Col>
                                </Row>
                            </Card>

                            <P noBottomMargin className="user-linked-id">Linked Employee ID</P>

                            <Card className="user-details-card-linked-id">
                                <LinkedEmployee employee={ EditUser.userDetails.employee } />
                            </Card>
                        </FormContainer>
                    </div>
                </StyledContainer>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.handleCancel() }
                            />
                            <Button
                                label={ loading ? <Loader /> : 'Update' }
                                type="action"
                                onClick={ () => this.handleSave() }
                                disabled={ loading }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    EditUser: makeSelectEditUser(),
    products: makeSelectProductsState(),
    isExpired: makeSelectIsExpired(),
    userInfo: makeSelectUserInfo(),
    UserManagement: makeSelectUserManagement(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { ...mainActions, ...addUserActions },
        dispatch
    );
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( EditUser );
