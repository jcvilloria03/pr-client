import { createSelector } from 'reselect';

/**
 * Direct selector to the editUser state domain
 */
const selectEditUserDomain = () => ( state ) => state.get( 'editUser' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );
const selectUserManagement = () => ( state ) => state.get( 'userManagement' );
/**
 * Other specific selectors
 */

/**
 * Default selector used by EditUser
 */

const makeSelectEditUser = () => createSelector(
    selectEditUserDomain(), ( substate ) => substate.toJS()
);

const makeSelectUserManagement = () => createSelector(
    selectUserManagement(), ( substate ) => substate.toJS()
);

const makeSelectUserInfo = () => createSelector(
    selectAppDomain(), ( substate ) =>
        substate.get( 'userInformation' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectEditUserDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectEditUserDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectProductsState = () => createSelector(
    selectAppDomain(), ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

export default makeSelectEditUser;
export {
    selectEditUserDomain,
    makeSelectEditUser,
    makeSelectUserInfo,
    makeSelectProductsState,
    makeSelectUserManagement,
    makeSelectNotification,
    makeSelectLoading
};
