import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { call, cancel, put, take } from 'redux-saga/effects';

import { Fetch } from '../../../utils/request';
import { browserHistory } from '../../../utils/BrowserHistory';

import {
    FETCH_ROLES,
    FETCH_SUBSCRIPTIONS,
    GET_USER_DETAILS_BY_ID,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    UPDATE_USER,
    SET_LOADING,
    SET_ROLES,
    SET_SUBSCRIPTIONS,
    SET_USER_DETAILS
} from './constants';

/**
 * fetch roles handler
 */
export function* fetchRolesHandler({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const rolesList = yield call(
            Fetch, `/accounts/${payload}/roles`, { method: 'GET' },
            payload
        );
        const rolesOptions = rolesList.data.map( ( role ) => ({
            label: role.name,
            value: role.id
        }) );
        yield put({
            type: SET_ROLES,
            payload: rolesOptions
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
    }
}

/**
 * fetch subscription handler
 */
export function* fetchSubscriptionsHandler() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const availableProductSeats = yield call(
            Fetch, '/available_product_seats', { method: 'GET' }
        );
        yield put({
            type: SET_SUBSCRIPTIONS,
            payload: availableProductSeats.data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
    }
}

/**
 * handle fetching user details
 */
export function* getUserDetailsByIdHandler({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const result = yield call(
            Fetch, `/user/${payload}`, { method: 'GET' },
            payload
        );
        yield put({
            type: SET_USER_DETAILS,
            payload: result
        });
    } catch ( error ) {
        yield put(
            yield call( notifyUser, {
                title: 'Error',
                message: error.response.data.message || 'Something went wrong',
                show: 'true',
                type: 'error'
            })
        );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * handle updating user details
 */
export function* updateUserHandler({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        yield call(
            Fetch, `/user/${payload.id}`, { method: 'PATCH', data: payload.user },
            payload.id,
            payload.user
        );
        yield call( notifyUser, {
            title: 'Success',
            message: 'User successfully updated.',
            show: 'true',
            type: 'success'
        });
        yield call( browserHistory.push( '/control-panel/users/management', true ) );
    } catch ( error ) {
        yield put(
            yield call( notifyUser, {
                title: 'Error',
                message: error.response.data.message || 'Something went wrong',
                show: 'true',
                type: 'error'
            })
        );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * watch fetching role
 */
export function* fetchRolesWatcher() {
    const watcher = yield takeLatest(
        FETCH_ROLES,
        fetchRolesHandler
    );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watch fetching subscriptions
 */
export function* fetchSubscriptionsWatcher() {
    const watcher = yield takeLatest(
        FETCH_SUBSCRIPTIONS,
        fetchSubscriptionsHandler
    );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watch fetching user by id
 */
export function* getUserDetailsByIdWatcher() {
    const watcher = yield takeLatest(
        GET_USER_DETAILS_BY_ID,
        getUserDetailsByIdHandler
    );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watch updating user
 */
export function* updateUserWatcher() {
    const watcher = yield takeLatest(
        UPDATE_USER,
        updateUserHandler
    );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watch For Notification
 */
export function* watchForNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    fetchRolesWatcher,
    fetchSubscriptionsWatcher,
    getUserDetailsByIdWatcher,
    updateUserWatcher,
    watchForNotification
];
