/*
 *
 * EditUser constants
 *
 */
export const nameSpace = 'app/UserManagement/Edit';

export const FETCH_ROLES = `${nameSpace}/FETCH_ROLES`;
export const SET_ROLES = `${nameSpace}/SET_ROLES`;
export const GET_USER_DETAILS_BY_ID = `${nameSpace}/GET_USER_DETAILS_BY_ID`;
export const SET_USER_DETAILS = `${nameSpace}/SET_USER_DETAILS`;
export const REST_DETAILS = `${nameSpace}/REST_DETAILS`;
export const FETCH_SUBSCRIPTIONS = `${nameSpace}/FETCH_SUBSCRIPTIONS`;
export const SET_SUBSCRIPTIONS = `${nameSpace}/SET_SUBSCRIPTIONS`;
export const UPDATE_USER = `${nameSpace}/UPDATE_USER`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
export const SET_LOADING = `${nameSpace}/SET_LOADING`;
