/*
 *
 * ViewUser constants
 *
 */

export const nameSpace = 'app/UserManagement/View';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const GET_USER_DETAILS_BY_ID = `${nameSpace}/GET_USER_DETAILS_BY_ID`;
export const SET_USER_DETAILS = `${nameSpace}/SET_USER_DETAILS`;
export const REST_DETAILS = `${nameSpace}/REST_DETAILS`;
export const RESEND_EMAIL_VERIFICATION = `${nameSpace}/RESEND_EMAIL_VERIFICATION`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
export const SET_LOADING = `${nameSpace}/SET_LOADING`;
