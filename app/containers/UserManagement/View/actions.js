
import {
    GET_USER_DETAILS_BY_ID,
    NOTIFICATION,
    RESEND_EMAIL_VERIFICATION,
    REST_DETAILS
} from './constants';

/**
 *
 * ViewUser actions
 *
 */

/**
 * @param {Number} id - user id
 */
export function getUserDetailsById( id ) {
    return {
        type: GET_USER_DETAILS_BY_ID,
        payload: id
    };
}

/**
 * reset user details state
 */
export function resetDetails() {
    return {
        type: REST_DETAILS
    };
}

/**
 * @param {Number} id - user's id
 */
export function resendEmailVerification( id ) {
    return {
        type: RESEND_EMAIL_VERIFICATION,
        payload: { id }
    };
}

/**
 * create user
 *@param {Object} notify
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
