import { fromJS } from 'immutable';
import {
    NOTIFICATION_SAGA,
    REST_DETAILS,
    SET_USER_DETAILS,
    SET_LOADING
} from './constants';

const defaultUserDetailState = {
    id: 0,
    account_id: 0,
    email: '',
    role_id: null,
    user_type: '',
    first_name: '',
    middle_name: '',
    last_name: '',
    full_name: '',
    last_active_company_id: null,
    active: '',
    created_at: {
        date: new Date(),
        timezone_type: 0,
        timezone: 'Asia/Manila'
    },
    with_registered_face: false,
    role: [],
    employee: null,
    account: {
        id: 0,
        name: '',
        email: '',
        user_id: '',
        subscriptions: { id: 0, name: '' },
        owner: {
            id: 0,
            account_id: 0,
            email: '',
            role_id: 0,
            user_type: '',
            first_name: '',
            middle_name: null,
            last_name: '',
            full_name: '',
            last_active_company_id: 0,
            active: '',
            created_at: {
                date: new Date(),
                timezone_type: 0,
                timezone: 'Asia/Manila'
            },
            with_registered_face: false,
            role: {
                id: 0,
                owner_account_id: 0,
                name: '',
                level: '',
                is_employee: false,
                is_system_defined: true,
                company_ids: [],
                policy: null
            },
            employee: null,
            product_seats: []
        },
        companies: []
    },
    product_seats: [],
    companies: [],
    salpay_integration: true,
    license_unit_id: 0,
    salpay_settings: [],
    authz_role: null,
    email_verified: false,
    assigned_licenses_id: [{ id: 2, name: 'payroll' }],
    admin_of: [],
    employee_of: []
};

const initialState = fromJS({
    userDetails: defaultUserDetailState,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    },
    loading: true
});

/**
 *
 * ViewUser reducer
 *
 */
function viewUserReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_USER_DETAILS:
            return state.set( 'userDetails', action.payload );
        case REST_DETAILS:
            return state.set( 'userDetails', defaultUserDetailState );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        default:
            return state;
    }
}

export default viewUserReducer;
