import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { call, cancel, put, take } from 'redux-saga/effects';
import { Fetch } from '../../../utils/request';

import { GET_USER_DETAILS_BY_ID, NOTIFICATION, NOTIFICATION_SAGA, RESEND_EMAIL_VERIFICATION, SET_LOADING, SET_USER_DETAILS } from './constants';

/**
 * handle fetching user details
 */
export function* getUserDetailsByIdHandler({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const result = yield call( Fetch, `/user/${payload}`, { method: 'GET' }, payload );
        yield put({
            type: SET_USER_DETAILS,
            payload: result
        });
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield put(
            yield call( notifyUser, {
                title: 'Error',
                message: error.response.data.message || 'Something went wrong',
                show: 'true',
                type: 'error'
            })
        );
        yield delay( 5500 );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * handle resend email verfication
 */
export function* resendEmailVerificationHandler({ payload: { id }}) {
    try {
        yield call( Fetch, `/user/${id}/resend_verification`, { method: 'POST' }, id );
        yield put(
            yield call( notifyUser, {
                title: 'Verification Email Sent!',
                message: 'You have successfully resent the email verification',
                show: 'true',
                type: 'success'
            })
        );
        yield delay( 5500 );
    } catch ( error ) {
        yield put(
            yield call( notifyUser, {
                title: 'Error',
                message: error.response.data.message || 'Something went wrong',
                show: 'true',
                type: 'error'
            })
        );
        yield delay( 5500 );
    }
}

/**
 * Individual exports for testing
 */
export function* getUserDetailsByIdWatcher() {
    const watcher = yield takeLatest(
        GET_USER_DETAILS_BY_ID,
        getUserDetailsByIdHandler
    );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * resend email verification watcher
 */
export function* resendEmailVerificationWatcher() {
    const watcher = yield takeLatest(
        RESEND_EMAIL_VERIFICATION,
        resendEmailVerificationHandler
    );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watch For Notification
 */
export function* watchForNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    getUserDetailsByIdWatcher,
    resendEmailVerificationWatcher
];
