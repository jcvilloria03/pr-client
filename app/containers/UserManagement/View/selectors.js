import { createSelector } from 'reselect';

/**
 * Direct selector to the viewUser state domain
 */
const selectViewUserDomain = () => ( state ) => state.get( 'viewUser' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by ViewUser
 */

const makeSelectViewUser = () => createSelector(
    selectViewUserDomain(),
    ( substate ) => substate.toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewUserDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
  );

const makeSelectLoading = () => createSelector(
    selectViewUserDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectProductsState = () => createSelector(
    selectAppDomain(), ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

export default makeSelectViewUser;
export {
    selectViewUserDomain,
    makeSelectProductsState,
    makeSelectViewUser,
    makeSelectNotification,
    makeSelectLoading
};
