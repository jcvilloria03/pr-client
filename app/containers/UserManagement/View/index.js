/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import { makeSelectIsExpired } from 'containers/App/selectors';
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Col, Row, Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { H3, H2, P } from 'components/Typography';
import A from 'components/A';
import Button from 'components/Button';
import Icon from 'components/Icon';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';

import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';

import LinkedEmployee from '../templates/LinkedEmployee';
import {
    Card,
    DetailsContainer,
    Label,
    LoadingStyles,
    ResendInviteBtn,
    StyledContainer,
    NavWrapper,
    Footer
} from '../styles';
import * as getUserDetailsAction from './actions';
import {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectProductsState,
    makeSelectViewUser
} from './selectors';

import { PRODUCT_SEATS_LABELS } from '../../UserProfile/constants';

const subscriptionMap = PRODUCT_SEATS_LABELS;

/**
 *
 * ViewUser
 *
 */
export class ViewUser extends React.Component {
    static propTypes = {
        getUserDetailsById: React.PropTypes.func,
        resetDetails: React.PropTypes.func,
        resendEmailVerification: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool,
        isExpired: React.PropTypes.bool
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {};
    }

    componentDidMount() {
        this.props.getUserDetailsById( this.props.router.params.userId );
    }

    componentWillReceiveProps( nextProps ) {
        if (
            nextProps.router.params.userId !== this.props.router.params.userId
        ) {
            this.props.getUserDetailsById( nextProps.router.params.userId );
        }
    }

    componentWillUnmount() {
        this.props.resetDetails();
    }

    handleResendEmailVerficationClick = () => {
        this.props.resendEmailVerification( this.props.ViewUser.userDetails.id );
    };

    /**
     *
     * ViewUser render method
     *
     */
    render() {
        const { notification, loading, isExpired, router, ViewUser } = this.props;

        const showResendBtn = ViewUser.userDetails.active === 'active' && !ViewUser.userDetails.email_verified;

        return (
            <div>
                <Helmet
                    title="User Details"
                    meta={ [
                        { name: 'description', content: 'Description of User Details' }
                    ] }
                />
                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        isExpired,
                        salpayViewPermission: true,
                        isSubscribedToPayroll: true,
                        accountViewPermission: true
                    }) }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/control-panel/users/management', true );
                            } }
                        >
                            <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to User Management</span>
                        </A>
                    </Container>
                </NavWrapper>
                <div className="loader" style={ { display: loading ? '' : 'none' } }>
                    <LoadingStyles>
                        <H2>Loading User Details.</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoadingStyles>
                </div>
                <div className="content" style={ { display: loading ? 'none' : '' } }>
                    <StyledContainer>
                        <H3 noBottomMargin className="userdetail-title">User Details</H3>

                        <DetailsContainer>
                            <div className="details-main">
                                <H2 noBottomMargin className="details-full-name">{ViewUser.userDetails.full_name}</H2>

                                <P className="user-details">User Details</P>

                                <Card className="user-details-card">
                                    <Row className="user-details-row-role">
                                        {ViewUser.userDetails.authz_role && (
                                            <Col xs={ 6 }>
                                                <Label>User Role:</Label>
                                                <P noBottomMargin>{ViewUser.userDetails.authz_role.name}</P>
                                            </Col>
                                        )}
                                        <Col xs={ 6 }>
                                            <Label>Email</Label>
                                            <P noBottomMargin>{ViewUser.userDetails.email}</P>
                                            {showResendBtn && (
                                                <ResendInviteBtn onClick={ () => this.handleResendEmailVerficationClick() }>
                                                    <span>
                                                        <Icon name="envelope" />
                                                    </span>
                                                    Resend Invitation
                                                </ResendInviteBtn>
                                            )}
                                        </Col>
                                    </Row>
                                    <Row className="user-details-row-license">
                                        <Col xs={ 6 }>
                                            <Label>Assigned License:</Label>
                                            <P noBottomMargin>{subscriptionMap[ ViewUser.userDetails.assigned_licenses_id[ 0 ].name ]}</P>
                                        </Col>
                                    </Row>
                                </Card>

                                <P noBottomMargin className="user-linked-id">Linked Employee ID</P>

                                <Card className="user-details-card-linked-id">
                                    <LinkedEmployee employee={ ViewUser.userDetails.employee } />
                                </Card>
                            </div>
                        </DetailsContainer>
                    </StyledContainer>
                    <Footer>
                        <div className="submit">
                            <div className="col-xs-12">
                                <Button
                                    label="Cancel"
                                    type="action"
                                    size="large"
                                    alt
                                    onClick={ () => {
                                        router.push(
                                            '/control-panel/users/management'
                                    );
                                    } }
                                />
                                <Button
                                    label="Edit"
                                    type="action"
                                    onClick={ () => {
                                        router.push( `/control-panel/users/${router.params.userId}/edit` );
                                    } }
                                />
                            </div>
                        </div>
                    </Footer>
                </div>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    ViewUser: makeSelectViewUser(),
    products: makeSelectProductsState(),
    isExpired: makeSelectIsExpired(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        getUserDetailsAction,
        dispatch
    );
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( ViewUser );
