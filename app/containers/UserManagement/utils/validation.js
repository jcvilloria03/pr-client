import * as yup from 'yup';

/** validation schema
 *  role_id: 0,
    account_id: 0,
    first_name: '',
    middle_name: '',
    last_name: '',
    email: '',
    assigned_product_seats_ids: [],
    license_unit_id: 0,
    subscription_product_id: 0
 */
export default function userPayloadValidationSchema( form ) {
    const schema = yup.object().shape({
        role_id: yup.number().min( 1, 'User Role is a required field.' ),
        first_name: yup.string().required( 'First Name is a required field.' ),
        last_name: yup.string().required( 'Last Name is a required field.' ),
        email: yup.string().email( 'Must be a valid email.' ).required( 'Email is a required field.' )
    });
    return schema.validate( form, { abortEarly: false });
}
