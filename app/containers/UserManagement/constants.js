/*
 *
 * UserManagement constants
 *
 */
export const nameSpace = 'app/UserManagement';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;

export const FETCH_PAGINATED_USER_LIST = `${nameSpace}/FETCH_PAGINATED_USER_LIST`;
export const SET_PAGINATED_USER_LIST = `${nameSpace}/SET_PAGINATED_USER_LIST`;

export const GET_USERS_STATS = `${nameSpace}/GET_USERS_STATS`;
export const SET_USERS_STATS = `${nameSpace}/SET_USERS_STATS`;

export const UPDATE_USER_STATUS = `${nameSpace}/UPDATE_USER_STATUS`;
export const SET_USER_STATUS = `${nameSpace}/SET_USER_STATUS`;

export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;

export const SET_LOADING = `${nameSpace}/SET_LOADING`;
export const SET_TABLE_LOADING = `${nameSpace}/SET_TABLE_LOADING`;

export const SET_PAGINATION = `${nameSpace}/SET_PAGINATION`;
