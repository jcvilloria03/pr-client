/* eslint-disable no-confusing-arrow */
import styled, { keyframes } from 'styled-components';
import { Container } from 'reactstrap';

const rotate360 = keyframes`
    from {
        transform: rotate(0deg);
    }
    to {
        transform: rotate(360deg);
    }
`;

export const Spinner = styled.div`
    animation: ${rotate360} 1s linear infinite;
    transform: translateZ(0);

    border-top: 2px solid #B5B5B5;
    border-right: 2px solid #B5B5B5;
    border-bottom: 2px solid #B5B5B5;
    border-left: 4px solid black;
    background: transparent;
    width: 5rem;
    height: 5rem;
    border-radius: 50%;
`;

export const StyledContainer = styled( Container )`
    padding: 80px 0 20px 20px;
    height: 100%;

    .userdetail-title {
        text-align: center;
        padding-top: 64px;
        font-size: 26px;
        font-weight: 700;
        line-height: 41.6px;
    }

    .userlist-table {
        .rt-table {
            overflow: inherit;

            .rt-td {
                overflow:inherit !important;
            }
        }
    }

    .active-select {
        width: 100%;

        .Select {
            .Select-control {
                background-color: rgb(240, 244, 246);
                color: #474747;
                border-color: rgb(173, 173, 173);
                height: 27px !important;
                line-height: 27px;
                border-radius: 15px !important;

                .Select-value {
                    line-height: 22px !important;

                    .Select-value-label {
                        line-height: 30px;
                    }
                }

                .Select-input {
                    height: 30px !important;

                    input {
                        background-color: rgb(240, 244, 246);
                        padding: 0;
                    }
                }
            }
        }

        .Select.is-focused {
            .Select-control {
                box-shadow:none !important;
            }
        }
    }

    .status-dropdown {
        .dropdown {
            .sl-c-dropdown-border-solid {
                border-color: #adadad;
                border-radius: 28px;
                padding: 4px 12px;
            }

            .dropdown-item {
                font-size: 14px;
                padding: 8px 16px;
            }

            .dropdown-menu {
                margin-top: 2px;
                padding: 4px 0;
                min-width: 105px;
            }
        }
    }

    .btn-add-user {
        margin-top: 56px;
    }

    .user-stats {
        margin: 21px 0;
    }

    .user-search {
        margin-bottom: 14px;

        h5 {
            font-size: 18px;
            font-weight: 700;
            line-height: 28.8px;
            margin-right: 28px;
        }

        .search {
            width: 200px;

            input, .input-group {
                height: 42px;
            }
        }
    }
`;
export const ActionContainer = styled.div`
    display: flex;
    justify-content: flex-end;
`;

export const HeaderTitle = styled.h1`
    text-align: center;
    margin-bottom: 0;
    font-weight: 700;
    font-size: 36px;
    line-height: 57.6px;
`;

export const SubTitle = styled.p`
    font-size: 14px;
    line-height: 22.4px;
    text-align: center;
    margin: 0;
`;

export const SectionTitle = styled.p`
    font-size: 16px;
    text-align: left;
    margin-top: 36px;
    font-weight: 600;
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const ComponentWrapper = styled.div`
    display: ${( props ) => ( props.status === 'pending' ? 'none' : 'block' )};
`;

export const Center = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
`;

export const Padder = styled.div`
    padding-left: ${( props ) => ( props.horizontal ? props.horizontal : 0 )}px;
    padding-right: ${( props ) => ( props.horizontal ? props.horizontal : 0 )}px;
    padding-top: ${( props ) => ( props.vertical ? props.vertical : 0 )}px;
    padding-bottom: ${( props ) => ( props.vertical ? props.vertical : 0 )}px;
`;

export const DetailsContainer = styled.div`
    max-width: 60vw;
    margin-right: auto;
    margin-left: auto;
    margin-top: 42px;

    .details-main {
        padding-bottom: 60px;

        .details-full-name {
            font-size: 32px;
            font-weight: 700;
            line-height: 51.2px;
        }

        .user-details,
        .user-linked-id {
            font-size: 14px;
            font-weight: 700;
            line-height: 22.4px;
        }

        .user-details {
            margin-bottom: 4px;
            margin-left: 14px;
        }

        .user-linked-id {
            margin-bottom: 4px;
            margin-top: 14px;
            margin-left: 14px;
        }

        h3 {
            font-weight:600;
            font-size: 24px;
            margin:15px 0 20px 0;
        }
    }
`;

export const Card = styled.div`
    padding: 14px;
    box-shadow: 0 0 26px -6px hsl(0deg 0% 54% / 50%);
    border-radius: 15px;
    margin-bottom: 2rem;

    &.user-details-card {
        margin-bottom: 42px;

        p {
            padding: 0 7px;
        }
    }

    &.user-details-card-linked-id {
        .row {
            margin-bottom: 0;
        }

        h6 {
            font-size: 16px;
            font-weight: 600;
            line-height: 25.6px;
        }
    }

    .user-details-row-role {
        margin-top: 14px;
        margin-bottom: 28px;
    }

    .user-details-row-role,
    .user-details-row-license {
        padding: 0 7px;
    }
`;

export const Label = styled.label`
    font-size: 14px;
    line-height: 1.6;
    font-weight: bold;
`;

export const ResendInviteBtn = styled.button`
    cursor: pointer;
    padding: 0.25rem 1rem;
    font-size: .9rem;
    border-radius: 0.25rem;
    display: inline-block;
    color: #474747;
    text-align: center;
    line-height: 1.6;
    text-decoration: none;
    vertical-align: middle;
    background-color: #f0f4f6;
    border: 1px solid #adadad;;
    transition: all .2s ease-in-out;
    margin-top: 7px;

    &:hover {
        background-color: #fff;
    }

    span {
        width: 16px;
        display: inline-block;
        margin-right: 4px;
    }
`;

export const FlexContainer = styled.div`
    display: flex;
    justify-content: ${( props ) => props.justifyContent};
    align-items: ${( props ) => props.alignItems};
    gap: ${( props ) => props.gap ? props.gap : 0}px;
`;

export const FormContainer = styled.div`
    max-width: 60vw;
    margin-right: auto;
    margin-left: auto;
    padding-bottom: 60px;

    .edit-user-title {
        text-align: center;
        font-size: 26px;
        font-weight: 700;
        line-height: 41.6px;
        padding-top: 28px;
    }

    .edit-user-details {
        font-weight: 700;
        margin-top: 56px;
        margin-left: 14px;
        margin-bottom: 4px;
    }
`;

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;

    .icon-arrow {
        width: 14px;
        font-size: 14px;
        display: inline-block;
        margin-right: 4px;

        > i {
            align-self: center;
        }
    }

    .back-text {
        font-size: 14px;
    }
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 18vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        padding: 14px 28px;
        border-radius: 25px;
    }
`;
