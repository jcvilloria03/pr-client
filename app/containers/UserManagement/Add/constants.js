/*
 *
 * CreateUser constants
 *
 */
export const nameSpace = 'app/UserManagement/Add';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const FETCH_ROLES = `${nameSpace}/FETCH_ROLES`;
export const SET_ROLES = `${nameSpace}/SET_ROLES`;
export const FETCH_SUBSCRIPTIONS = `${nameSpace}/FETCH_SUBSCRIPTIONS`;
export const SET_SUBSCRIPTIONS = `${nameSpace}/SET_SUBSCRIPTIONS`;
export const CREATE_USER = `${nameSpace}/CREATE_USER`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
export const SET_LOADING = `${nameSpace}/SET_LOADING`;
