import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { call, cancel, put, take } from 'redux-saga/effects';

import { Fetch } from '../../../utils/request';
import { browserHistory } from '../../../utils/BrowserHistory';

import {
    CREATE_USER,
    FETCH_ROLES,
    FETCH_SUBSCRIPTIONS,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_LOADING,
    SET_ROLES,
    SET_SUBSCRIPTIONS
} from './constants';

/**
 * fetch roles
 */
export function* fetchRolesHandler({ payload }) {
    try {
        const rolesList = yield call(
            Fetch, `/accounts/${payload}/roles`, { method: 'GET' },
            payload
        );
        const rolesOptions = rolesList.data.map( ( role ) => ({
            label: role.name,
            value: role.id
        }) );
        yield put({
            type: SET_ROLES,
            payload: rolesOptions
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
        yield delay( 2000 );
    }
}

/**
 * fetch subscriptions
 */
export function* fetchSubscriptionsHandler() {
    try {
        const availableProductSeats = yield call(
            Fetch, '/available_product_seats', { method: 'GET' }
        );
        yield put({
            type: SET_SUBSCRIPTIONS,
            payload: availableProductSeats.data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
        yield delay( 2000 );
    }
}

/**
 * post user
 */
export function* createUserHandler({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        yield call( Fetch, '/user', { method: 'POST', data: payload });
        yield call( notifyUser, {
            title: 'Success',
            message: 'User successfully created.',
            show: 'true',
            type: 'success'
        });
        yield delay( 2000 );
        yield call( browserHistory.push( '/control-panel/users/management', true ) );
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || error.response.data.email || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
        yield delay( 2000 );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * fetch roles watcher
 */
export function* fetchRolesWatcher() {
    const watcher = yield takeLatest(
        FETCH_ROLES,
        fetchRolesHandler
    );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * fetch subscriptions watcher
 */
export function* fetchSubscriptionsWatcher() {
    const watcher = yield takeLatest(
        FETCH_SUBSCRIPTIONS,
        fetchSubscriptionsHandler
    );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * post user watcher
 */
export function* createUserWatcher() {
    const watcher = yield takeLatest(
        CREATE_USER,
        createUserHandler
    );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watch For Notification
 */
export function* watchForNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    fetchRolesWatcher,
    fetchSubscriptionsWatcher,
    createUserWatcher,
    watchForNotification
];
