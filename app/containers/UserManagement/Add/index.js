/* eslint-disable no-shadow */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
import { makeSelectIsExpired } from 'containers/App/selectors';
import React from 'react';
import { connect } from 'react-redux';
import { Col, Row } from 'reactstrap';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import Button from 'components/Button';
import Modal from 'components/Modal';
import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import Loader from 'components/Loader';
import { P } from 'components/Typography';

import { getControlPanelSidebarLinks } from 'utils/sidebarHelper';

import userPayloadValidationSchema from '../utils/validation';
import {
    makeSelectCreateUser,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectProductsState,
    makeSelectUserInfo,
    makeSelectUserManagement
} from './selectors';
import * as mainActions from '../actions';
import * as addUserActions from './actions';

import AssignSubscription from '../templates/AssignSubscription';
import UserForm from '../templates/UserForm';

import {
    Card,
    FormContainer, HeaderTitle,
    StyledContainer,
    SubTitle,
    Footer
} from '../styles';

const initialFormState = {
    role_id: 0,
    account_id: 0,
    first_name: '',
    middle_name: '',
    last_name: '',
    email: '',
    assigned_product_seats_ids: [],
    license_unit_id: 0,
    subscription_product_id: 0
};
/**
 *
 * CreateUser
 *
 */
export class CreateUser extends React.Component {
    static propTypes = {
        fetchRolesOption: React.PropTypes.func,
        fetchSubscriptions: React.PropTypes.func,
        willCreateUser: React.PropTypes.func,
        CreateUser: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool,
        isExpired: React.PropTypes.bool,
        userInfo: React.PropTypes.object
    }

    constructor( props ) {
        super( props );

        this.state = {
            error: {},
            formBody: initialFormState
        };
        this.cancelModal = null;
    }

    componentDidMount() {
        const { account_id } = this.props.userInfo;
        if ( account_id ) {
            this.handleInputChange( 'account_id', account_id );
            this.props.fetchRolesOption( account_id );
        }

        this.props.fetchSubscriptions();
        this.props.getUsersStats();
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.userInfo.account_id !== this.props.userInfo.account_id ) {
            this.handleInputChange( 'account_id', nextProps.userInfo.account_id );
            this.props.fetchRolesOption( nextProps.userInfo.account_id );
        }
    }

    handleInputChange = ( name, val ) => {
        this.setState( ( prev ) => ({
            ...prev,
            formBody: {
                ...prev.formBody,
                [ name ]: val
            }
        }) );
    };

    handleSave = () => {
        userPayloadValidationSchema( this.state.formBody )
            .then( ( validatedForm ) => {
                this.props.willCreateUser( validatedForm );
            })
            .catch( ( err ) => {
                const _error = {};
                err.inner.forEach( ( e ) => {
                    _error[ e.path ] = e.message;
                });

                this.setState({ error: _error });
            });
    };

    handleSubscriptionChange = (
        assigned_product_seats_ids,
        subscription_product_id
    ) => {
        this.setState( ( prev ) => ({
            ...prev,
            formBody: {
                ...prev.formBody,
                assigned_product_seats_ids,
                subscription_product_id
            }
        }) );
    };

    handleCancel = () => {
        this.cancelModal.toggle();
    };
    handleCancelModal = () => {
        this.cancelModal.close();
    };
    handleDiscard = () => {
        this.setState({ formBody: initialFormState });
        this.props.router.push( '/control-panel/users/management' );
    };

    /**
     *
     * CreateUser render method
     *
     */
    render() {
        const { notification, loading, isExpired, UserManagement, userInfo, CreateUser } = this.props;
        const { error, formBody } = this.state;
        return (
            <div>
                <Helmet
                    title="Add User"
                    meta={ [
                        { name: 'description', content: 'Description of Add User' }
                    ] }
                />
                <Sidebar
                    items={ getControlPanelSidebarLinks({
                        isExpired,
                        salpayViewPermission: true,
                        isSubscribedToPayroll: true,
                        accountViewPermission: true
                    }) }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Modal
                    ref={ ( ref ) => {
                        this.cancelModal = ref;
                    } }
                    title="Discard changes"
                    body={
                        <p>
                            Clicking Discard will undo all changes you made on
                            this page. Are you sure you want to proceed?
                        </p>
                    }
                    buttons={ [
                        {
                            label: 'Stay on this page',
                            type: 'grey',
                            onClick: this.handleCancelModal
                        },
                        {
                            label: 'Discard',
                            type: 'darkRed',
                            onClick: this.handleDiscard
                        }
                    ] }
                    className="modal-md modal-commission-type"
                    showClose={ false }
                    center
                />
                <StyledContainer>
                    <FormContainer>
                        <HeaderTitle>Add User</HeaderTitle>
                        <SubTitle>Add users one at a time through this page.</SubTitle>

                        <P noBottomMargin className="edit-user-details">User Details</P>

                        <Card>
                            <UserForm
                                handleInputChange={ this.handleInputChange }
                                rolesOptions={ CreateUser.rolesOptions }
                                error={ error }
                                role_id={ formBody.role_id }
                                last_name={ formBody.last_name }
                                first_name={ formBody.first_name }
                                middle_name={ formBody.middle_name }
                                email={ formBody.email }
                            />
                            <Row>
                                <Col xs={ 6 }>
                                    <AssignSubscription
                                        mode="create"
                                        subscriptions={ CreateUser.availableProductSeats }
                                        value={ {
                                            assigned_product_seats_ids: formBody.assigned_product_seats_ids,
                                            subscription_product_id: formBody.subscription_product_id
                                        } }
                                        onChange={ this.handleSubscriptionChange }
                                        user={ userInfo }
                                        subscriptionStats={ UserManagement.userStats }
                                    />
                                </Col>
                            </Row>
                        </Card>
                    </FormContainer>
                </StyledContainer>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.handleCancel() }
                            />
                            <Button
                                label={ loading ? <Loader /> : 'Submit' }
                                type="action"
                                onClick={ () => this.handleSave() }
                                ref={ ( ref ) => { this.submitButton = ref; } }
                                disabled={ loading }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    CreateUser: makeSelectCreateUser(),
    products: makeSelectProductsState(),
    isExpired: makeSelectIsExpired(),
    userInfo: makeSelectUserInfo(),
    UserManagement: makeSelectUserManagement(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators({ ...mainActions, ...addUserActions }, dispatch );
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( CreateUser );
