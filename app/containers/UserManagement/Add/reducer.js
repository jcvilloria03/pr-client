import { fromJS } from 'immutable';
import {
    SET_LOADING,
    NOTIFICATION_SAGA,
    SET_ROLES,
    SET_SUBSCRIPTIONS
} from './constants';

const initialState = fromJS({
    rolesOptions: [],
    availableProductSeats: [],
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    }
});

/**
 *
 * CreateUser reducer
 *
 */
function createUserReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_ROLES:
            return state.set( 'rolesOptions', action.payload );
        case SET_SUBSCRIPTIONS:
            return state.set( 'availableProductSeats', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        default:
            return state;
    }
}

export default createUserReducer;
