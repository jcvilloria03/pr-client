import { createSelector } from 'reselect';

/**
 * Direct selector to the createUser state domain
 */
const selectCreateUserDomain = () => ( state ) => state.get( 'createUser' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );
const selectUserManagement = () => ( state ) => state.get( 'userManagement' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by CreateUser
 */

const makeSelectCreateUser = () => createSelector(
    selectCreateUserDomain(),
    ( substate ) => substate.toJS()
);

const makeSelectUserManagement = () => createSelector(
    selectUserManagement(),
    ( substate ) => substate.toJS()
);

const makeSelectUserInfo = () => createSelector(
    selectAppDomain(),
    ( substate ) => substate.get( 'userInformation' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectCreateUserDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
    selectCreateUserDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
  );

const makeSelectProductsState = () => createSelector(
    selectAppDomain(), ( substate ) => {
        if ( !substate.get( 'products' ) ) {
            return substate.get( 'products' );
        }
        return substate.get( 'products' ).toJS();
    }
);

export default makeSelectCreateUser;
export {
    selectCreateUserDomain,
    makeSelectCreateUser,
    makeSelectProductsState,
    makeSelectUserInfo,
    makeSelectUserManagement,
    makeSelectLoading,
    makeSelectNotification
};
