import {
    CREATE_USER,
    FETCH_ROLES,
    FETCH_SUBSCRIPTIONS,
    NOTIFICATION
} from './constants';

/**
 * get user roles
 *@param {number} accountId - account id
 */
export function fetchRolesOption( accountId ) {
    return {
        type: FETCH_ROLES,
        payload: accountId
    };
}
/**
 * fetch subscriptions
 */
export function fetchSubscriptions() {
    return {
        type: FETCH_SUBSCRIPTIONS
    };
}

/**
 * create user
 *@param {Object} user - user details
 */
export function willCreateUser( user ) {
    return {
        type: CREATE_USER,
        payload: user
    };
}

/**
 * create user
 *@param {Object} notify
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
