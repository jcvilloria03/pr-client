import {
    FETCH_PAGINATED_USER_LIST,
    GET_USERS_STATS,
    NOTIFICATION,
    UPDATE_USER_STATUS
} from './constants';

/**
 *
 * UserManagement actions
 *
 */

/* async call actions */
/**
 * @param {String} queryString - query params
 */
export function fetchPaginatedUserList( queryString ) {
    return {
        type: FETCH_PAGINATED_USER_LIST,
        payload: queryString
    };
}

/**
 * get user stats
 */
export function getUsersStats() {
    return {
        type: GET_USERS_STATS
    };
}

/**
 * update user status
 * @param {number} id - user id
 * @param {string} status - user stutus active | semi-active | inactive
 */
export function updateUserStatus( id, status ) {
    return {
        type: UPDATE_USER_STATUS,
        payload: { id, status }
    };
}

/**
 * create user
 *@param {Object} notify
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

