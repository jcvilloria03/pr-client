import { fromJS } from 'immutable';
import {
    NOTIFICATION_SAGA,
    SET_LOADING,
    SET_PAGINATED_USER_LIST,
    SET_TABLE_LOADING,
    SET_USERS_STATS,
    SET_USER_STATUS,
    SET_PAGINATION
} from './constants';

const initialState = fromJS({
    list: [],
    userStats: [],
    total: 0,
    loading: true,
    tableLoading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    },
    pagination: {
        total: 1,
        per_page: 10,
        current_page: 1,
        last_page: 1,
        to: 1
    }
});

/**
 *
 * UserManagement reducer
 *
 */
function userManagementReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_PAGINATED_USER_LIST:
            return state
                .set( 'list', action.payload.data )
                .set( 'total', action.payload.total );
        case SET_USERS_STATS:
            return state
                .set( 'userStats', action.payload );
        case SET_USER_STATUS: {
            const updatedList = state.get( 'list' ).map( ( item ) => {
                if ( item.id === action.payload.id ) {
                    return { ...item, active: action.payload.status };
                }
                return item;
            });
            return state
                .set( 'list', updatedList );
        }
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_TABLE_LOADING:
            return state.set( 'tableLoading', action.payload );

        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );

        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );

        default:
            return state;
    }
}

export default userManagementReducer;
