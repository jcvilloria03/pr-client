/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import Icon from '../../../components/Icon';
const BackNavigation = ({ title, onClick }) => (
    <div
        style={ {
            backgroundColor: '#f0f4f6',
            paddingTop: 100,
            paddingLeft: 100,
            height: 145,
            position: 'sticky',
            top: 0,
            left: 0,
            zIndex: 9
        } }
    >
        <span
            style={ {
                color: '#00a5e5',
                cursor: 'pointer',
                display: 'flex',
                alignItems: 'center',
                gap: 8
            } }
            onClick={ () => onClick() }
        >
            <div style={ { width: 24, marginTop: -2 } }>
                <Icon name="arrow" className="icon" />
            </div>
            {title}
        </span>
    </div>
);

BackNavigation.propTypes = {
    title: React.PropTypes.string,
    onClick: React.PropTypes.func
};

export default BackNavigation;
