/* eslint-disable react/prop-types */
import React from 'react';
import { Col, Row } from 'reactstrap';
import styled from 'styled-components';

const Wrapper = styled.div`
    border: 1px solid #ccc;
    padding: 1rem;

    .license-details {
        text-align: left;
    }

    .total,
    .used,
    .available {
        margin-left: 14px;
    }

    p {
        font-size: 14px;
        line-height: 22.4px;
    }

    .row {
        margin-bottom: 14px;
    }
`;

const StatusHeader = styled.p`
    font-weight: 700;
    text-align: center;
    margin: 0;
`;

const StatusData = styled.p`
    text-align: center;
    margin: 0;
`;

/**
 * User stats component
 */
const UserStats = ({ stats }) => (
    <Wrapper>
        <Row>
            <Col xs={ 3 }>
                <StatusHeader className="license-details">License details</StatusHeader>
            </Col>
            {stats.products && stats.products.map( ( stat, index ) => (
                <Col xs={ 3 } key={ index }>
                    <StatusHeader>{stat.product_name}</StatusHeader>
                </Col>
            ) )}
        </Row>
        <Row>
            <Col xs={ 3 }>
                <StatusData className="license-details total">Total:</StatusData>
            </Col>
            {stats.products && stats.products.map( ( stat, index ) => (
                <Col xs={ 3 } key={ index }>
                    <StatusData>
                        {stat.licenses_used + stat.licenses_available}
                    </StatusData>
                </Col>
            ) )}
        </Row>
        <Row>
            <Col xs={ 3 }>
                <StatusData className="license-details used">Used:</StatusData>
            </Col>
            {stats.products && stats.products.map( ( stat, index ) => (
                <Col xs={ 3 } key={ index }>
                    <StatusData>{stat.licenses_used}</StatusData>
                </Col>
            ) )}
        </Row>
        <Row>
            <Col xs={ 3 }>
                <StatusData className="license-details available">Available:</StatusData>
            </Col>
            {stats.products && stats.products.map( ( stat, index ) => (
                <Col xs={ 3 } key={ index }>
                    <StatusData>{stat.licenses_available}</StatusData>
                </Col>
            ) )}
        </Row>
    </Wrapper>
);

export default UserStats;
