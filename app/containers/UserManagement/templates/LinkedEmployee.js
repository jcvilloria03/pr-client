/* eslint-disable react/prop-types */
import React from 'react';
import { Col, Row } from 'reactstrap';
import { Center, Label } from '../styles';

const LinkedEmployee = ({ employee }) => {
    if ( employee ) {
        return (
            <Row>
                <Col xs={ 6 }>
                    <Label>Employee ID:</Label>
                    <p>{employee.employee_id}</p>
                </Col>
                <Col xs={ 6 }>
                    <Label>Company Name:</Label>
                    <p>{employee.company_name}</p>
                </Col>
            </Row>
        );
    }

    return (
        <Row>
            <Col xs={ 12 }>
                <Center>
                    <h6>
                        This user profile is not linked to an employee profile. If you would like to link this to an employee profile,
                        <br />
                        you may do so at Employee User Page
                    </h6>
                </Center>
            </Col>
        </Row>
    );
};

export default LinkedEmployee;
