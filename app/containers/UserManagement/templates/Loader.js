/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React from 'react';
import { Modal } from 'reactstrap';
import styled from 'styled-components';
import { Spinner } from '../styles';

const LoaderModal = styled( Modal )`
    position: absolute;
    top: 25%;
    left: 50%;
    transform: translateX(-50%);
    & .modal-content {
        background-color: transparent;
        color: white;
        font-size: 32px;
        border: none;
    }
`;

const Loader = ({ status }) => (
    <div>
        <LoaderModal isOpen={ status } toggle={ () => {} } backdrop>
            <Spinner />
        </LoaderModal>
    </div>
);

export default Loader;
