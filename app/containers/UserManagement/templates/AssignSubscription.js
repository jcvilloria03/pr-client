/* eslint-disable no-confusing-arrow */
/* eslint-disable require-jsdoc */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import styled from 'styled-components';
import { H6, P } from 'components/Typography';

import { PRODUCT_SEATS, PRODUCT_SEATS_IDS, PRODUCT_SEATS_LABELS } from '../../UserProfile/constants';

const InputWrapper = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    gap: 16px;
    margin: 16px 0;
    &:hover {
        cursor:  ${( props ) => ( props.disabled ? 'not-allowed' : 'pointer' )};
    }
`;
const Input = styled.input`
    position: absolute;
    left: -9999px;
    top: -9999px;

    &:checked + span {
        &:before {
            left: 20px;
            background-color: #83d24b;
        }
    }
`;

const Slider = styled.span`
    display: flex;
    cursor:  ${( props ) => ( props.disabled ? 'not-allowed' : 'pointer' )};
    width: 40px;
    height: 10px;
    border-radius: 80px;
    background-color: #bfbfbf;
    position: relative;
    transition: background-color 0.2s;

    &:before {
        content: "";
        position: absolute;
        top: -5px;
        left: -2px;
        width: 21px;
        height: 21px;
        border-radius: 21px;
        transition: 0.2s;
        background-color: ${( props ) => props.disabled ? '#fafafa' : '#474747'};
        cursor:  ${( props ) => ( props.disabled ? 'not-allowed' : 'pointer' )};
        box-shadow: 0 2px 4px 0 rgba(0, 35, 11, 0.2);
    }
`;

class AssignSubscription extends Component {
    constructor( props ) {
        super( props );

        this.state = {
            assigned_product_seats_ids: [],
            subscription_product_id: 0,

            currentSubscription: '',

            productsLicensesAvailable: {}
        };
    }

    componentWillReceiveProps( nextProps ) {
        if (
            nextProps.value.assigned_product_seats_ids !==
            this.props.value.assigned_product_seats_ids
        ) {
            this.setState({
                assigned_product_seats_ids:
                    nextProps.value.assigned_product_seats_ids
            });
        }

        if (
            nextProps.value.subscription_product_id !==
            this.props.value.subscription_product_id
        ) {
            const currentSubObj = nextProps.subscriptions.find(
                ( item ) =>
                    item.subscription_product_id ===
                    nextProps.value.subscription_product_id
            );
            this.setState({
                subscription_product_id:
                    nextProps.value.subscription_product_id,
                currentSubscription: ( currentSubObj ) ? currentSubObj.name : PRODUCT_SEATS.HRIS
            });
        }

        if (
            this.props.mode === 'edit' &&
            nextProps.user.product_seats &&
            nextProps.subscriptionStats.length > 0
        ) {
            nextProps.subscriptionStats.forEach( ( product ) => {
                let licensesAvailable = product.licenses_available;

                if (
                    product.product_code ===
                    nextProps.user.product_seats[ 0 ].name
                ) {
                    // Add 1 for allowance in user's own license
                    licensesAvailable += 1;
                }

                this.setState( ( prev ) => ({
                    ...prev,
                    productsLicensesAvailable: {
                        ...prev.productsLicensesAvailable,
                        [ product.product_code ]: licensesAvailable
                    }
                }) );
            });
        }

        if (
            nextProps.subscriptionStats.length > 0 &&
            this.props.mode === 'create'
        ) {
            nextProps.subscriptionStats.forEach( ( product ) => {
                this.setState( ( prev ) => ({
                    ...prev,
                    productsLicensesAvailable: {
                        ...prev.productsLicensesAvailable,
                        [ product.product_code ]: product.licenses_available
                    }
                }) );
            });
        }
    }

    handleChange = ( subs ) => {
        const chosenSubscription = ( subs.name === this.state.currentSubscription ) ? PRODUCT_SEATS.HRIS : subs.name;
        this.setState(
            {
                currentSubscription: chosenSubscription,
                assigned_product_seats_ids: ( chosenSubscription === PRODUCT_SEATS.HRIS )
                        ? [PRODUCT_SEATS_IDS.HRIS]
                        : [subs.id],
                subscription_product_id: ( chosenSubscription === PRODUCT_SEATS.HRIS )
                        ? 0 /* default */
                        : subs.subscription_product_id
            },
            () => {
                this.props.onChange(
                    this.state.assigned_product_seats_ids,
                    this.state.subscription_product_id
                );
            }
        );
    };

    render() {
        const { subscriptions } = this.props;
        const subscriptionMap = PRODUCT_SEATS_LABELS;
        return (
            <div style={ { padding: '0 8px', marginTop: '14px' } }>
                <H6 noBottomMargin>Assign Subscription License</H6>

                {subscriptions.length > 0 && subscriptions.map( ( subs ) => {
                    if ( subs.name === PRODUCT_SEATS.HRIS ) return null;

                    const disabled = this.state.productsLicensesAvailable[ subs.name ] <= 0;

                    return (
                        <InputWrapper
                            key={ subs.id }
                            onClick={ () => {
                                if ( disabled ) return;
                                this.handleChange( subs );
                            } }
                            disabled={ disabled }
                        >
                            <Input
                                type="radio"
                                name={ subs.name }
                                value={ subs.name }
                                checked={
                                    subs.name === this.state.currentSubscription
                                }
                                onChange={ () => this.handleChange( subs ) }
                                disabled={ disabled }
                            />
                            <Slider
                                onClick={ () => {
                                    if ( disabled ) return;
                                    this.handleChange( subs );
                                } }
                                disabled={ disabled }
                            />
                            <P noBottomMargin>{subscriptionMap[ subs.name ]}</P>
                        </InputWrapper>
                    );
                })}
            </div>
        );
    }
}

export default AssignSubscription;
