/* eslint-disable react/prop-types */
/* eslint-disable camelcase */
import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';

import Input from '../../../components/Input';
import Select from '../../../components/Select';
import { Padder } from '../styles';

/**
 * User form component
 */
export class UserForm extends Component {
    constructor( props ) {
        super( props );

        this.state = {};
    }

    render() {
        const {
            handleInputChange,
            rolesOptions,
            error,
            role_id,
            last_name,
            first_name,
            middle_name,
            email
        } = this.props;

        return (
            <Padder horizontal={ 8 }>
                <Row>
                    <Col xs={ 8 }>
                        <Select
                            label="User Role"
                            id="role_id"
                            name="role_id"
                            value={ role_id || 0 }
                            onChange={ ({ value }) => {
                                handleInputChange( 'role_id', value );
                            } }
                            data={ rolesOptions }
                            required
                            clientError={ error.role_id }
                            placeholder="Select a user role"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={ 4 }>
                        <Input
                            label="Last Name"
                            id="last_name"
                            name="last_name"
                            value={ last_name || '' }
                            onChange={ ( val ) => {
                                handleInputChange( 'last_name', val );
                            } }
                            required
                            clientError={ error.last_name }
                            placeholder="Enter user's last name"
                        />
                    </Col>
                    <Col xs={ 4 }>
                        <Input
                            label="First Name"
                            id="first_name"
                            name="first_name"
                            value={ first_name || '' }
                            onChange={ ( val ) => {
                                handleInputChange( 'first_name', val );
                            } }
                            required
                            clientError={ error.first_name }
                            placeholder="Enter user's first name"
                        />
                    </Col>
                    <Col xs={ 4 }>
                        <Input
                            label="Middle Name"
                            id="middle_name"
                            name="middle_name"
                            value={ middle_name || '' }
                            onChange={ ( val ) => {
                                handleInputChange( 'middle_name', val );
                            } }
                            placeholder="Enter user's middle name"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={ 4 }>
                        <Input
                            label="Email"
                            id="email"
                            name="email"
                            value={ email || '' }
                            onChange={ ( val ) => {
                                handleInputChange( 'email', val );
                            } }
                            required
                            clientError={ error.email }
                            placeholder="Enter user's email address"
                        />
                    </Col>
                </Row>
            </Padder>
        );
    }
}

export default UserForm;
