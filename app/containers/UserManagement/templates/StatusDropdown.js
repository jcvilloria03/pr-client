/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import Input from 'reactstrap/lib/Input';

/**
 * Status dropdown
 */
class StatusDropdown extends Component {
    constructor( props ) {
        super( props );

        this.state = {
            currentStatus: this.props.currentStatus
        };
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.currentStatus !== this.props.currentStatus ) {
            this.setState({ currentStatus: nextProps.currentStatus });
        }
    }

    handleChange = ( e ) => {
        this.setState({ currentStatus: e.target.value });
        this.props.onClick( e.target.value );
    };

    render() {
        const statusMap = {
            active: 'Active',
            'semi-active': 'Semi-active',
            inactive: 'Inactive'
        };

        return (
            <div style={ { width: '100%', cursor: 'pointer' } }>
                <Input
                    type="select"
                    style={ { height: 30 } }
                    onChange={ this.handleChange }
                    value={ this.state.currentStatus }
                >
                    {this.props.menuItems.map( ( item ) => (
                        <option
                            style={ { cursor: 'pointer' } }
                            key={ item }
                            value={ item }
                            selected={ this.state.currentStatus === item }
                        >
                            {statusMap[ item ]}
                        </option>
                    ) )}
                </Input>
            </div>
        );
    }
}

export default StatusDropdown;
