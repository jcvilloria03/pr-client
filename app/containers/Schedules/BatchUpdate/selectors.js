/* eslint-disable no-unused-vars */
import { createSelector } from 'reselect';

/**
 * Direct selector to the batchEditSchedule state domain
 */
const selectAddScheduleDomain = () => (state) => state.get('batchEditSchedule');

const makeSelectSubmitted = () => createSelector(
  selectAddScheduleDomain(),
  (substate) => substate.get('submitted'),
);

const makeSelectIsNameAvailables = () => createSelector(
  selectAddScheduleDomain(),
  (substate) => substate.get('isNameAvailable'),
);

const makeSelectNotification = () => createSelector(
  selectAddScheduleDomain(),
  (substate) => substate.get('notification').toJS(),
);

const makeSelectErrors = () => createSelector(
  selectAddScheduleDomain(),
  (substate) => {
      let error;
      try {
          error = substate.get('errors').toJS();
      } catch (err) {
          error = substate.get('errors');
      }

      return error;
  },
);

const makeSelectSchedules = () => createSelector(
  selectAddScheduleDomain(),
  (substate) => substate.get('schedules').toJS(),
);

const makeSelectLoading = () => createSelector(
  selectAddScheduleDomain(),
  (substate) => substate.get('loading'),
);

const makeSelectUploadStatus = () => createSelector(
  selectAddScheduleDomain(),
  (substate) => substate.get('uploadStatus').toJS(),
);

/**
 * Default selector used by AddSchedule
 */
export {
  makeSelectSubmitted,
  makeSelectIsNameAvailables,
  makeSelectNotification,
  makeSelectSchedules,
  makeSelectLoading,
  makeSelectErrors,
  makeSelectUploadStatus,
};
