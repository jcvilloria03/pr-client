const namespace = 'app/Schedules/Add';

export const SUBMIT_FORM = `${namespace}/SUBMIT_FORM`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const IS_NAME_AVAILABLE = `${namespace}/IS_NAME_AVAILABLE`;

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_SUBMITTED = `${namespace}/SET_SUBMITTED`;
export const SET_IS_NAME_AVAILABLE = `${namespace}/SET_IS_NAME_AVAILABLE`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_ERRORS = `${namespace}/SET_ERRORS`;

export const UPLOAD_SCHEDULES = `${namespace}/UPLOAD_SCHEDULES`;
export const SAVE_SCHEDULE = `${namespace}/SAVE_SCHEDULE`;

export const GET_SCHEDULES = `${namespace}/GET_SCHEDULES`;
export const SET_SCHEDULES = `${namespace}/SET_SCHEDULES`;

export const SET_UPLOAD_STATUS = `${namespace}/SET_UPLOAD_STATUS`;

export const breckData = [
    { value: 'fixed', label: 'Fixed break' },
    { value: 'flexi', label: 'Flexi break' },
    { value: 'floating', label: 'Floating break' },
];

export const ENTITLED_EMPLOYEES = [
    { value: 'location', label: 'Location' },
    { value: 'department', label: 'Department' },
    { value: 'position', label: 'Position' },
];

export const TOGGLE_OPTIONS = {
    BATCH: 'BATCH',
    MANUAL: 'MANUAL',
};

export const DEFAULT_HEADERS = {
    'X-Authz-Entities': 'time_and_attendance.schedules'
};
