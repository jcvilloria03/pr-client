import { fromJS } from 'immutable';
import {
    SET_ERRORS,
    SET_LOADING,
    SET_SUBMITTED,
    SET_SCHEDULES,
    SET_NOTIFICATION,
    SET_IS_NAME_AVAILABLE,
    SET_UPLOAD_STATUS,
} from './constants';

const initialState = fromJS({
    errors: {},
    schedules: [],
    loading: true,
    submitted: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error',
    },
    isNameAvailable: false,
    uploadStatus: {
        success: false,
        jobId: null,
        companyId: null,
        error: false,
    },
});

/**
 *
 * AddSchedule reducer
 *
 */
function addScheduleReducer(state = initialState, action) {
    switch (action.type) {
        case SET_SUBMITTED:
            return state.set('submitted', action.payload);
        case SET_LOADING:
            return state.set('loading', action.payload);
        case SET_IS_NAME_AVAILABLE:
            return state.set('isNameAvailable', action.payload);
        case SET_ERRORS:
            return state.set('errors', fromJS(action.payload));
        case SET_SCHEDULES:
            return state.set('schedules', fromJS(action.payload));
        case SET_NOTIFICATION:
            return state.set('notification', fromJS(action.payload));
        case SET_UPLOAD_STATUS:
            return state.set('uploadStatus', fromJS(action.payload));
        default:
            return state;
    }
}

export default addScheduleReducer;
