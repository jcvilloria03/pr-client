import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';

import A from 'components/A';
import { H2, H3 } from 'components/Typography';
import SnackBar from 'components/SnackBar';
import Button from 'components/Button';

import {
    makeSelectSchedules,
    makeSelectLoading,
    makeSelectTags,
    makeSelectNotification,
    makeSelectSchedInUse
} from './selectors';
import * as viewScheduleActions from './actions';

import { MainWrapper, LoadingStyles, StyledFooter } from './styles';

import EditScheduleForm from '../Forms';
import { browserHistory } from '../../../utils/BrowserHistory';

export class Edit extends React.Component {
    // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        tags: React.PropTypes.array,
        loading: React.PropTypes.bool,
        params: React.PropTypes.object,
        updateSchedule: React.PropTypes.func,
        getScheduleDetails: React.PropTypes.func,
        schedule: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string,
        }),
        checkInUse: React.PropTypes.func,
        schedInUse: React.PropTypes.bool,
    };

    constructor(props) {
        super(props);

        this.state = {
            footerButtons: {
                submit: {
                    onClick: () => {},
                    label: 'Submit',
                    disabled: true,
                    type: 'action',
                    size: 'large',
                    className: '',
                    visible: false,
                },
                cancel: {
                    onClick: () => {},
                    label: 'Cancel',
                    disabled: true,
                    type: 'action',
                    size: 'large',
                    className: '',
                    visible: false,
                },
            },
        };
    }

    componentWillMount() {
        const value = {
            id: this.props.params.id,
        };

        this.props.getScheduleDetails(value);
        this.props.checkInUse([value.id]);
    }

    handleFooterButtonsUpdate = (options) => {
        this.setState({
            footerButtons: options,
        });
    };

    render() {
        const { tags, loading, schedule, updateSchedule, notification, schedInUse } =
            this.props;

        const { footerButtons } = this.state;

        return (
            <div>
                <Helmet
                    title="Edit Schedule"
                    meta={ [
                        { name: 'description', content: 'Description of Edit' },
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    show={ notification.show }
                    type={ notification.type }
                />

                <MainWrapper>
                    <div className="nav">
                        <Container>
                            <A
                                className="back-btn"
                                href
                                onClick={ (e) => {
                                    e.preventDefault();
                                    browserHistory.push(
                                        '/time/schedules',
                                        true,
                                    );
                                } }
                            >
                                <span>&#8592;</span> Back to Schedules
                            </A>
                        </Container>
                    </div>
                    {loading ? (
                        <div className="loader">
                            <LoadingStyles>
                                <H2>Loading Schedule</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                    ) : (
                        <div>
                            <Container>
                                <div className="heading">
                                    <h3>Edit Schedule</h3>
                                    <p>
                                        Update this schedule’s name, validity
                                        period, duration, and recurrence.
                                    </p>
                                </div>
                            </Container>
                            <EditScheduleForm
                                submitForm={ updateSchedule }
                                schedule={ schedule }
                                tags={ tags }
                                handleFooterButtonsUpdate={
                                    this.handleFooterButtonsUpdate
                                }
                                footerButtons={ footerButtons }
                                schedInUse={ schedInUse }
                            />
                        </div>
                    )}
                </MainWrapper>

                { !loading && (footerButtons.cancel.visible ||
                    footerButtons.submit.visible) && (
                        <StyledFooter className="footer">
                            <div className="container">
                                {footerButtons.cancel.visible && (
                                    <Button { ...footerButtons.cancel } />
                                )}
                                {footerButtons.submit.visible && (
                                    <Button { ...footerButtons.submit } />
                                )}
                            </div>
                        </StyledFooter>
                    )}
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    schedule: makeSelectSchedules(),
    loading: makeSelectLoading(),
    tags: makeSelectTags(),
    notification: makeSelectNotification(),
    schedInUse: makeSelectSchedInUse(),
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps(dispatch) {
    return bindActionCreators(viewScheduleActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
