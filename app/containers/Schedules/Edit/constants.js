/*
 *
 * Edit constants
 *
 */

const namespace = 'app/Schedules/Add';

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const GET_NOTIFICATION = `${namespace}/GET_NOTIFICATION`;
export const SET_ERRORS = `${namespace}/SET_ERRORS`;

export const SET_TAGS = `${namespace}/SET_TAGS`;
export const SET_UPDATE = `${namespace}/SET_UPDATE`;
export const SET_SCHEDULE = `${namespace}/SET_SCHEDULE`;
export const GET_SCHEDULE = `${namespace}/GET_SCHEDULE`;

export const UPDATE_SCHEDULE = `${namespace}/UPDATE_SCHEDULE`;

export const GET_SCHED_CHECK_IN_USE = `${namespace}/GET_SCHED_CHECK_IN_USE`;
export const SET_SCHED_CHECK_IN_USE = `${namespace}/SET_SCHED_CHECK_IN_USE`;

export const DEFAULT_HEADERS = {
    'X-Authz-Entities': 'time_and_attendance.schedules'
};
