import { fromJS } from 'immutable';
import {
    SET_TAGS,
    SET_LOADING,
    SET_SCHEDULE,
    SET_ERRORS,
    SET_NOTIFICATION,
    SET_SCHED_CHECK_IN_USE
} from './constants';

const initialState = fromJS({
    loading: true,
    schedule: {},
    tags: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    schedInUse: false
});

/**
 *
 * Edit reducer
 *
 */
function editReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_SCHEDULE:
            return state.set( 'schedule', fromJS( action.payload ) );
        case SET_TAGS:
            return state.set( 'tags', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_SCHED_CHECK_IN_USE:
            return state.set( 'schedInUse', action.payload );
        default:
            return state;
    }
}

export default editReducer;
