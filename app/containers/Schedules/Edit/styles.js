import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 200px;
`;

export const MainWrapper = styled.div`
    min-height: 100vh;
    background: #fff;

    .nav {
        padding: 10px 20px;
        background: #f0f4f6;
        margin-bottom: 50px;
        margin-top: 75px;
    }

    .loader {
        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .back-btn {
        font-size: 14px;
        vertical-align: middle;

        span {
            position: relative;
            top: -1px;
            display: inline-block;
        }
    }

    .stepper {
        padding: 20px 0;
    }

    .radiogroup {
        padding: 0;
        & > span {
            padding: 0 15px;
            color: #5b5b5b;
            font-size: 14px;
            margin-bottom: 4px;
            margin-right: 1rem;
            font-weight: 400;
        }
    }

    .basic, .employment, .payroll {
        padding: 20px 0;

        h4 {
            font-weight: 600;
        }
    }

    .employment .DayPickerInput-Overlay {
        bottom: 47px;
    }

    .date {
        & > div > span {
            display: block;
        }
        .DayPickerInput {
            display: block;

            input {
                width: 100%;
            }
        }
    }

    .heading {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 0 auto 30px auto;

        h3 {
            font-weight: 600;
        }

        p {
            text-align: center;
            max-width: 800px;
        }
    }

    .foot {
        text-align: right;
        padding: 10px 0;
        background: #f0f4f6;

        button {
            min-width: 120px;
        }

        .cancel {
            margin-right: 10px;
        }
    }

    .hide {
        display: none;
    }

    .selected .fill {
      fill: #fff;
    }

    .selected .stroke {
      stroke: #fff;
    }

    .fill {
      fill: rgb(0,165,226);
    }

    .stroke {
      stroke: rgb(0,165,226);
    }

    .btn.btn-default {
        display: none;
    }

    .switch {
        & > div, .switch-label {
            font-size: 14px;
            margin-bottom: 20px;
        }

        span {
            margin-left: 5px;
        }

        .rc-switch-checked.rc-switch-disabled::after {
            background-color: #87b865;
        }
    }

    .sub-switch {
        padding-left: 40px;
    }

    .form {
        margin-top: 50px;
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        text-align: left;
    }
`;

export const StyledFooter = styled.div`
    background: #f0f4f6;
    margin-top: 24px;
    padding: 12px 0;

    .container {  
        display: flex;
        justify-content: flex-end;
    }
`;
