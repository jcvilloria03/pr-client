/* eslint-disable require-jsdoc */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import {
    GET_SCHEDULE,
    SET_SCHEDULE,
    SET_TAGS,
    SET_ERRORS,
    SET_NOTIFICATION,
    GET_NOTIFICATION,
    UPDATE_SCHEDULE,
    DEFAULT_HEADERS,
    GET_SCHED_CHECK_IN_USE,
    SET_SCHED_CHECK_IN_USE
} from './constants';

import {
    setLoading
} from './actions';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

export function* getScheduleDetails({ payload }) {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put( setLoading( true ) );

        const { id } = payload;
        const [
            schedules,
            tags
        ] = yield [
            call( Fetch, `/schedule/${id}`, { method: 'GET', headers: DEFAULT_HEADERS }),
            call( Fetch, `/company/${companyId}/tags`, { method: 'GET', headers: DEFAULT_HEADERS })
        ];

        yield put({
            type: SET_TAGS,
            payload: tags.data
        });

        yield put({
            type: SET_SCHEDULE,
            payload: schedules
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

export function* updateSchedule({ payload }) {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put( setLoading( true ) );

        const response = yield call( Fetch, `/company/${companyId}/schedule/is_name_available`, { method: 'POST', data: payload, headers: DEFAULT_HEADERS });

        if ( response.available ) {
            yield call( Fetch, `/schedule/${payload.schedule_id}`, { method: 'PUT',
                data: {
                    company_id: companyId,
                    ...payload
                },
                headers: DEFAULT_HEADERS
            });

            yield call( notifyUser, {
                show: true,
                title: 'Success',
                message: 'Record Updated successfully',
                type: 'success'
            });

            yield call( browserHistory.push, '/time/schedules', true );
        } else {
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: 'Schedule name already exists',
                type: 'error'
            });
            yield put( setLoading( false ) );
        }
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

/**
 * Check if schedule is in use
 */
export function* checkInUse({ payload }) {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put( setLoading( true ) );

        const check = yield call( Fetch, '/schedule/check_in_use', {
            method: 'POST',
            data: {
                company_id: companyId,
                schedules_ids: payload
            },
            headers: DEFAULT_HEADERS
        });

        if ( check.in_use === 0 ) {
            yield put({
                type: SET_SCHED_CHECK_IN_USE,
                payload: false
            });
        } else {
            yield put({
                type: SET_SCHED_CHECK_IN_USE,
                payload: true
            });
        }
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

export function* watchForGetScheduleDetails() {
    const watcher = yield takeEvery( GET_SCHEDULE, getScheduleDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForUpdateSchedules() {
    const watcher = yield takeEvery( UPDATE_SCHEDULE, updateSchedule );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchNotify() {
    const watcher = yield takeEvery( GET_NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export function* watchForCheckInUse() {
    const watcher = yield takeEvery( GET_SCHED_CHECK_IN_USE, checkInUse );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetScheduleDetails,
    watchForUpdateSchedules,
    watchNotify,
    watchForCheckInUse
];
