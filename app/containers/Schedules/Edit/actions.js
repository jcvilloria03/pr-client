/* eslint-disable require-jsdoc */
import {
    SET_LOADING,
    GET_SCHEDULE,
    UPDATE_SCHEDULE,
    GET_SCHED_CHECK_IN_USE
} from './constants';

export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

export function getScheduleDetails( payload ) {
    return {
        type: GET_SCHEDULE,
        payload
    };
}

export function updateSchedule( payload ) {
    return {
        type: UPDATE_SCHEDULE,
        payload
    };
}

export function checkInUse( payload ) {
    return {
        type: GET_SCHED_CHECK_IN_USE,
        payload
    };
}
