import { createSelector } from 'reselect';

/**
 * Direct selector to the edit state domain
 */
const selectEditDomain = () => ( state ) => state.get( 'editSchedule' );

const makeSelectLoading = () => createSelector(
  selectEditDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectSchedules = () => createSelector(
  selectEditDomain(),
( substate ) => substate && substate.get( 'schedule' ).toJS()
);

const makeSelectTags = () => createSelector(
  selectEditDomain(),
( substate ) => substate && substate.get( 'tags' ).toJS()
);

const makeSelectNotification = () => createSelector(
  selectEditDomain(),
  ( substate ) => substate && substate.get( 'notification' ).toJS()
);

const makeSelectErrors = () => createSelector(
  selectEditDomain(),
  ( substate ) => {
      let error;
      try {
          error = substate.get( 'errors' ).toJS();
      } catch ( err ) {
          error = substate.get( 'errors' );
      }

      return error;
  }
);

const makeSelectSchedInUse = () => createSelector(
  selectEditDomain(),
  ( substate ) => substate.get( 'schedInUse' )
);

/**
 * Other specific selectors
 */

/**
 * Default selector used by Edit
 */
export {
  makeSelectTags,
  makeSelectLoading,
  makeSelectSchedules,
  makeSelectNotification,
  makeSelectErrors,
  makeSelectSchedInUse
};
