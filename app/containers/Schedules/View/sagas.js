/* eslint-disable camelcase */
/* eslint-disable no-inner-declarations */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import {
    setLoading,
    setSchedules,
    setFilterData,
    setNotification
} from './actions';

import {
    INITIALIZE,
    EXPORT_CSV,
    NOTIFICATION,
    GET_CHECK_IN_USE,
    DEFAULT_HEADERS,
    ACTION_LOADING
} from './constants';

/**
 * initialize page
 */
export function* initializePage() {
    try {
        yield put( setLoading( true ) );
        const companyId = company.getLastActiveCompanyId();

        const [
            schedules,
            locations,
            positions,
            departments
        ] = yield [
            call( Fetch, `/company/${companyId}/schedules?type=schedules`, { method: 'GET', headers: DEFAULT_HEADERS }),
            call( Fetch, `/company/${companyId}/time_attendance_locations`, { method: 'GET', headers: DEFAULT_HEADERS }),
            call( Fetch, `/company/${companyId}/departments`, { method: 'GET', headers: DEFAULT_HEADERS }),
            call( Fetch, `/company/${companyId}/positions`, { method: 'GET', headers: DEFAULT_HEADERS })
        ];

        const filterData = {
            locations: locations.data,
            departments: departments.data,
            positions: positions.data
        };

        yield [
            put( setSchedules( schedules.data ) ),
            put( setFilterData( filterData ) )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Check if schedule is in use
 */
export function* checkInUse({ payload }) {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put({ type: ACTION_LOADING, payload: true });

        const check = yield call( Fetch, '/schedule/check_in_use', {
            method: 'POST',
            data: {
                company_id: companyId,
                schedules_ids: payload
            },
            headers: DEFAULT_HEADERS
        });

        if ( check.in_use === 0 ) {
            yield call( Fetch, '/schedule/bulk_delete', {
                method: 'DELETE',
                data: {
                    company_id: companyId,
                    schedules_ids: payload
                },
                headers: DEFAULT_HEADERS
            });

            yield put({ type: ACTION_LOADING, payload: false });
            yield call( notifyUser, {
                title: 'Success',
                message: 'Schedule deleted successfully',
                show: true,
                type: 'success'
            });
            yield call( initializePage );
        } else if ( check.in_use === 1 && check.message ) {
            yield put({ type: ACTION_LOADING, payload: false });
            yield call( notifyUser, {
                title: 'Schedule is in use and cannot be deleted.',
                message: check.message,
                show: true,
                type: 'warning'
            });
        } else {
            yield put({ type: ACTION_LOADING, payload: false });
            yield call( notifyUser, {
                title: 'Warning',
                message: 'Schedule is in use and cannot be deleted',
                show: true,
                type: 'warning'
            });
        }
    } catch ( error ) {
        yield put({ type: ACTION_LOADING, payload: false });
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Export Employee Data
 * @param {array} data
 */
export function* exportCSV({ payload }) {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put( setLoading( true ) );
        const res = yield call( Fetch, `/company/${companyId}/schedules/generate_csv`, {
            method: 'POST',
            data: {
                schedules_ids: payload
            },
            headers: DEFAULT_HEADERS
        });
        const link = document.createElement( 'a' );

        link.href = res.file_name;
        link.setAttribute( 'download', '' );
        document.body.appendChild( link );

        link.click();

        document.body.removeChild( link );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializePage );
}

/**
 * Watcher for GET_CHECK_IN_USE
 */
export function* watchForCheckInUse() {
    const watcher = yield takeEvery( GET_CHECK_IN_USE, checkInUse );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForExport() {
    const watcher = yield takeLatest( EXPORT_CSV, exportCSV );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForInitializePage() {
    const watcher = yield takeLatest( INITIALIZE, initializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForExport,
    watchForCheckInUse,
    watchForNotifyUser,
    watchForInitializePage,
    watchForReinitializePage
];
