import {
    EXPORT_CSV,
    INITIALIZE,
    GET_CHECK_IN_USE,
    SET_IN_USE,
    SET_LOADING,
    SET_SCHEDULES,
    SET_FILTER_DATA,
    SET_NOTIFICATION,
    RESET_STORE
} from './constants';

/**
 * initialize page
 */
export function initializePage() {
    return {
        type: INITIALIZE
    };
}

/**
 * Check if schedule is in use
 */
export function checkInUse( payload ) {
    return {
        type: GET_CHECK_IN_USE,
        payload
    };
}

/**
 * Set schedule in use
 * @param {Boolean} payload - In use status
 */
export function setInUse( payload ) {
    return {
        type: SET_IN_USE,
        payload
    };
}

/**
 * export data12
 */
export function exportCsv( payload ) {
    return {
        type: EXPORT_CSV,
        payload
    };
}

/**
 * Sets page loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object}
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets notification
 * @param {Object} payload - Notification config
 * @returns {Object}
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Sets filter data
 * @param {Object} payload - Filter data
 * @returns {Object}
 */
export function setFilterData( payload ) {
    return {
        type: SET_FILTER_DATA,
        payload
    };
}

/**
 * Sets schedules
 * @param {Object} payload - Schedules
 * @returns {Object}
 */
export function setSchedules( payload ) {
    return {
        type: SET_SCHEDULES,
        payload
    };
}

/**
 *
 * Reset to default values
 *
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
