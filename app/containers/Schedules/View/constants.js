/*
 *
 * Schedules constants
 *
 */
const namespace = 'app/containers/Schedules';

export const LOADING = `${namespace}/LOADING`;
export const INITIALIZE = `${namespace}/INITIALIZE`;
export const EXPORT_CSV = `${namespace}/EXPORT_CSV`;

export const RESET_STORE = `${namespace}/RESET_STORE`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const ACTION_LOADING = `${namespace}/ACTION_LOADING`;

export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;

export const SET_SCHEDULES = `${namespace}/SET_SCHEDULES`;
export const SET_FILTER_DATA = `${namespace}/SET_FILTER_DATA`;

export const GET_SCHEDULES = `${namespace}/GET_SCHEDULES`;
export const GET_FILTER_DATA = `${namespace}/GET_FILTER_DATA`;

export const SET_IN_USE = `${namespace}/Calendar/SET_IN_USE`;
export const GET_CHECK_IN_USE = `${namespace}/Calendar/GET_CHECK_IN_USE`;

export const FILTER_TYPES = {
    LOCATION: 'location',
    POSITION: 'position',
    DEPARTMENT: 'department',
    SCHEDULED_TYPE: 'scheduled_type'
};

export const scheduledTypes = [
            { value: 'fixed', label: 'Fixed' },
            { value: 'flexi', label: 'Flexi' }
];

export const DEFAULT_HEADERS = {
    'X-Authz-Entities': 'time_and_attendance.schedules'
};
