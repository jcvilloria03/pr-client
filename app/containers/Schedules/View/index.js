/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import moment from 'moment';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import styled, { css } from 'styled-components';

import Icon from 'components/Icon';
import Table from 'components/Table';
import Button from 'components/Button';
import SnackBar from 'components/SnackBar';
import SubHeader from 'containers/SubHeader';
import SalConfirm from 'components/SalConfirm';
import { H2, H3 } from 'components/Typography';
import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';
import Loader from 'components/Loader';

import {
    isAnyRowSelected,
    getIdsOfSelectedRows
} from 'components/Table/helpers';

import { browserHistory } from 'utils/BrowserHistory';
import { TIME_AND_ATTENDANCE_SUBHEADER_ITEMS } from 'utils/constants';
import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';

import Calendar from './micro-components/calendar';
import Filter from './micro-components/filter';

import * as schedulerAction from './actions';
import {
    makeSelectLoading,
    makeSelectSchedules,
    makeSelectFilterData,
    makeSelectNotification,
    makeSelectActionLoading
} from './selectors';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';

const ModifiedSnackBar = styled.div`
    p {
        white-space: pre-wrap;
        column-count: auto;
        column-width: 20rem;
    }
`;

const alignedButton = css`
    display: flex;
    align-items: center;
    justify-content: center;

    span {
        display: flex;
    }`;

const StyledH3 = styled.h3`
    &&&& {
        font-size: 2.3rem;
        font-weight: 700;
    }
`;

const Description = styled.p`
    font-size: 14px;
    margin-bottom: 60px;
`;

const HeadingButton = styled( Button )`
    padding: 9px 22px 7px;
    font-family: "sourcesanspro-regular", sans-serif;
`;

const ControlWrapper = styled.div`
    display: flex;
    justify-content: space-between;`;

const LabelWrapper = styled.div`
    display: flex;
    align-items: center;
    flex: 1;
    font-weight: 700;
    font-size: 18px;
`;

const GridWrapper = styled.div`
    display: flex;
    align-items: center;
    flex: 1;
    justify-content: flex-end;
`;

const FilterButton = styled.button`
    ${alignedButton}
`;

const StyledButton = styled.button`
    &&&&&& {
        ${alignedButton}
        padding-top: 12px;
        padding-bottom: 12px;
    }
`;

const ExportButton = styled( Button )`
    border-radius: 2rem !important;
`;

/**
 *
 * Schedules
 *
 */
class Schedules extends React.PureComponent {// eslint-disable-next-line react/no-unused-prop-types
    static propTypes = {
        loading: React.PropTypes.bool,
        exportCsv: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        schedules: React.PropTypes.array,
        filterData: React.PropTypes.object,
        initializePage: React.PropTypes.func,
        checkInUse: React.PropTypes.func,
        actionLoading: React.PropTypes.bool
    }

    constructor( props ) {
        super( props );

        this.state = {
            showList: false,
            showFilter: false,
            selectedIds: [],
            eventData: [],
            filter: {},
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            },
            showModal: false
        };

        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );

        this.scheduleTable = null;
    }

    componentDidMount() {
        this.props.initializePage();
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.schedules ) {
            this.setState({
                eventData: nextProps.schedules.map( ( data ) => (
                    {
                        ...data,
                        title: data.name,
                        start: `${data.start_date} ${data.start_time}`,
                        end: moment( `${data.start_date} ${data.end_time}` ).format( 'YYYY-MM-DD HH:mm' )
                    }) ),
                pagination: {
                    total: Object.keys( nextProps.schedules ).length,
                    per_page: 10,
                    current_page: 1,
                    last_page: Math.ceil( Object.keys( nextProps.schedules ).length / 10 ),
                    to: 0
                }
            });
        }
    }

    /**
      * Navigate to add schedules page
      */
    // eslint-disable-next-line react/sort-comp
    navigateToAddSchedulesPage() {
        browserHistory.push( '/time/schedules/add', true );
    }

    handleTableChanges = ( tableProps = this.scheduleTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: Object.keys( this.props.schedules ).length });
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.scheduleTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.scheduleTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    export = () => {
        const schedulesIds = [];

        if ( !this.state.showList ) {
            this.state.eventData.forEach( ( data ) => { schedulesIds.push( data.id ); });
        } else {
            const ids = getIdsOfSelectedRows( this.scheduleTable );

            schedulesIds.push( ...ids );
        }

        this.props.exportCsv( schedulesIds );
    }

    remove = () => {
        let schedulesIds = [];

        if ( !this.state.showList ) {
            schedulesIds = this.state.selectedIds;
        } else {
            schedulesIds = getIdsOfSelectedRows( this.scheduleTable );
        }

        this.props.checkInUse( schedulesIds );
    }

    applyFilters = ( filters ) => {
        if ( Object.values( filters ).filter( Boolean ).length === 0 ) {
            this.resetFilters();
        } else {
            let newEventData = this.props.schedules;

            if ( filters.scheduled_type ) {
                const filteredType = [];

                filters.scheduled_type.forEach( ( type ) => {
                    const data = newEventData.filter( ( item ) => item.type === type.value );

                    filteredType.push( ...data );
                });

                newEventData = filteredType;
            }

            if ( filters.start_date && !filters.end_date ) {
                newEventData = newEventData.filter( ( data ) => filters.start_date === data.start_date );
            }

            if ( !filters.start_date && filters.end_date ) {
                newEventData = newEventData.filter( ( data ) => filters.end_date === data.end_date );
            }

            if ( filters.start_date && filters.end_date ) {
                newEventData = newEventData.filter( ( data ) => moment( data.start ).isBetween( filters.start_date, filters.end_date ) );
            }

            this.setState({
                filter: filters,
                eventData: newEventData.map( ( data ) => ({
                    ...data,
                    title: data.name,
                    start: `${data.start_date} ${data.start_time}`,
                    end: moment( `${data.start_date} ${data.end_time}` ).format( 'YYYY-MM-DD HH:mm' )
                }) )
            });
        }
    }

    resetFilters = () => {
        this.setState({
            eventData: this.props.schedules.map( ( data ) => (
                {
                    ...data,
                    title: data.name,
                    start: `${data.start_date} ${data.start_time}`,
                    end: moment( `${data.start_date} ${data.end_time}` ).format( 'YYYY-MM-DD HH:mm' )
                }) ),
            filter: {},
            showFilter: false
        });
    }

    showModal = ( id ) => {
        this.setState({ showModal: false }, () => {
            this.setState({
                showModal: true,
                selectedIds: id
            });
        });
    }

    render() {
        const {
            loading,
            schedules,
            filterData,
            notification
        } = this.props;

        const {
            label,
            filter,
            showList,
            eventData,
            showFilter,
            pagination,
            deleteLabel
        } = this.state;

        const tableColumns = [
            {
                id: 'name',
                header: 'Schedule Name ',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.name}
                    </div>
                    )
            },
            {
                id: 'type',
                header: 'Schedule Type',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.type.charAt( 0 ).toUpperCase() + row.type.slice( 1 ) }
                    </div>
                    )
            },
            {
                id: 'start_time',
                header: 'Start Time',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.start_time}
                    </div>
                    )
            },
            {
                id: 'break_start_time',
                header: 'Break Start Time',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.breaks && row.breaks.map( ( val ) => val.start ).join( ',' ) }
                    </div>
                    )
            },
            {
                id: 'break_end_time',
                header: 'Break End Time',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.breaks && row.breaks.map( ( val ) => val.end ).join( ',' )}
                    </div>
                    )
            },
            {
                id: 'end_time',
                header: 'End Time',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.end_time}
                    </div>
                    )
            },
            {
                id: 'entitled_employees',
                header: 'Entitled Employees',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.affected_employees && row.affected_employees.map( ( val ) => val.name )}
                    </div>
                    )
            },
            {
                header: ' ',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    <Button
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => browserHistory.push( `/time/schedules/edit/${row.id}`, true ) }
                    />
                    )
            }
        ];
        return (
            <div>
                <Helmet
                    title={ showList ? 'Schedules - Table View' : 'Schedules - Calendar View' }
                    meta={ [
                        { name: 'description', content: 'Description of Schedules' }
                    ] }
                />
                <ModifiedSnackBar>
                    <SnackBar
                        message={ notification.message }
                        title={ notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ notification.show }
                        delay={ 5000 }
                        type={ notification.type }
                    />
                </ModifiedSnackBar>
                <SubHeader items={ TIME_AND_ATTENDANCE_SUBHEADER_ITEMS } />
                <SalConfirm
                    onConfirm={ () => this.remove() }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Are you sure you want to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Schedule"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <PageWrapper>
                    {
                        loading ?
                        (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading Schedules</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        ) : (
                            <Container>
                                <div className="content">
                                    <div className="heading">
                                        <StyledH3>Schedules</StyledH3>
                                        <Description>
                                        View and update your company’s working hours, breaks, and other planned activities in calendar or table format. You can add or edit schedules one by one or by batch. You can also download schedules and use them for reports.
                                        </Description>
                                        <div className="button-wrapper">
                                            <div className="addScheduleBtn">
                                                <HeadingButton label="Add Schedules" size="large" type="action" onClick={ this.navigateToAddSchedulesPage } />
                                            </div>
                                            <div className="main">
                                                <HeadingButton label="Batch Update" size="large" type="action" onClick={ () => browserHistory.push( '/time/schedules/batch-update', true ) } />
                                            </div>
                                        </div>
                                    </div>
                                    <ControlWrapper className="calender-wrapper">
                                        <LabelWrapper>
                                                Schedules
                                            </LabelWrapper>
                                        <GridWrapper className="button-wrapper">
                                            { isAnyRowSelected( this.scheduleTable ) && <span>{deleteLabel}</span> }
                                            <FilterButton className="btnFilter" onClick={ () => this.setState({ showFilter: !showFilter }) }>
                                                <Icon name="filter" />
                                                <span>Filter</span>
                                            </FilterButton>

                                            {( !showList || isAnyRowSelected( this.scheduleTable ) ) &&
                                                <ExportButton label="Export" className="btnExport" onClick={ () => this.export() } />
                                            }

                                            {isAnyRowSelected( this.scheduleTable )
                                            && showList
                                            && <Button
                                                alt
                                                className="btnDelete"
                                                label={ this.props.actionLoading ? <Loader /> : 'Delete' }
                                                type="danger"
                                                disabled={ this.props.actionLoading }
                                                onClick={ () => this.showModal() }
                                            />
                                            }

                                            <StyledButton
                                                className={ !showList ? 'btnCalender active' : 'btnCalender' }
                                                onClick={ () => { this.setState({ showList: false, showFilter: false }); } }
                                            >
                                                <Icon name="calendar" />
                                            </StyledButton>
                                            <StyledButton
                                                className={ showList ? 'btnList btnCalender active' : 'btnList btnCalender' }
                                                onClick={ () => { this.setState({ showList: true, showFilter: false }); } }
                                            >
                                                <Icon name="list" />
                                            </StyledButton>
                                        </GridWrapper>
                                    </ControlWrapper>
                                    { showFilter &&
                                    <Filter
                                        filterData={ filterData }
                                        onCancel={ () => { this.resetFilters(); } }
                                        onApply={ ( filters ) => { this.applyFilters( filters ); } }
                                    />
                                    }
                                    {showList
                                    ?
                                        <Table

                                            data={ schedules }
                                            columns={ tableColumns }
                                            page={ pagination.current_page - 1 }
                                            pageSize={ pagination.per_page }
                                            pages={ pagination.total }
                                            onDataChange={ this.handleTableChanges }
                                            ref={ ( ref ) => { this.scheduleTable = ref; } }
                                            onSelectionChange={ ({ selected }) => {
                                                const selectionLength = selected.filter( ( row ) => row ).length;

                                                this.setState({
                                                    deleteLabel: formatDeleteLabel( selectionLength )
                                                });
                                            } }
                                            selectable
                                            external
                                        />

                                    : <Calendar
                                        filter={ filter }
                                        schedules={ eventData }
                                        deleteModal={ ( data ) => this.showModal( data ) }
                                    />
                                }
                                </div>
                            </Container>
                        )}
                </PageWrapper>
                <Container
                    className="loader schedule-table"
                    style={ { display: showList ? 'block' : 'none', background: '#fff' } }
                >
                    <FooterTablePaginationV2
                        page={ pagination.current_page }
                        pageSize={ pagination.per_page }
                        pagination={ pagination }
                        onPageChange={ this.onPageChange }
                        onPageSizeChange={ this.onPageSizeChange }
                        paginationLabel={ label }
                        fluid
                    />
                </Container>
            </div>

        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    schedules: makeSelectSchedules(),
    filterData: makeSelectFilterData(),
    notification: makeSelectNotification(),
    actionLoading: makeSelectActionLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        schedulerAction,
        dispatch,
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Schedules );
