import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 200px;
    height:50vh;
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    justify-content: center;
    min-height: 100%;
    // text-align: center;
`;

export const PageWrapper = styled.div`
   
    @media(max-width:499px){
        overflow: auto;
        height: 100vh;
        padding-top: 53px;
    }
    .content {
        margin-top: 40px;
        .ReactTable{
            .selected{
                background-color: rgba(131, 210, 75, 0.15);
            }

            .rt-tbody .rt-td {
                padding: 21px 20px;
                font-size: 14px;
            }
        }

        .schedule-table {
            margin-bottom: 5rem;
        }

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 20px auto;

            h3 {
                font-weight: 600;
            }

            .button-wrapper{
                display: flex;
                align-items: center;
                flex-wrap: wrap;
            }
        }
        .calender-wrapper{
            display: flex;
            align-items: center;
            justify-content: flex-end;
            flex-wrap: wrap;
            margin-bottom: .5rem;

            h3{
                margin-bottom:0;
                font-size: 18px;
                color: #474747;
                font-weight: 700;
            }
            .button-wrapper{
                display: flex;
                align-items: center;
                flex-wrap: wrap;

                button{
                    border-radius: 2px;
                    margin-left: 0.3rem;
                    margin-right: 0;
                    padding: 0.5rem 1.5rem;
                    cursor: pointer;
                    &:focus{
                        outline:none;
                    }
                }

                .addScheduleBtn {
                    button {
                        margin-left: 0;
                    }
                }
                
                .btnFilter{
                    min-width: 93px;
                    display: flex;
                    border: 1px solid #adadad;
                    border-radius: 0.25rem;
                    font-size: 15px;
                    padding: 8px 16px 6px;
                    span{
                        padding-left: 3px;
                    }
                    svg{
                        position: relative;
                        top: -2px;
                        width: 14px;
                        height: 14px;
                    }
                }
                .btnExport{
                    border-color: #83d24b;
                    background-color: #fff;
                    color: #474747;
                    font-size: 15px;
                    padding: 8px 21px 6px;
                }
                .btnDelete{
                    border-color: #EB7575;
                    color: #ffffff;
                    background-color: #EB7575;
                    font-size: 15px;
                }
                .btnCalender{
                    width: 58px;
                    background-color:transparent;
                    border: 1px solid #f6f6f6;
                    border-radius: 0.25rem 0px 0px 0.25rem;
                    svg{
                        width: 14px;
                        height: 14px;
                        color: #474747;
                    }
                    &:hover{
                        background-color: #19bfff;
                        svg{
                            color: #fff;
                        }
                    }
                    &:last-child{
                        margin: 0;
                    }
                }
                .btnList{
                    border-radius: 0 0.25rem 0.25rem 0 !important;
                }
                .active{
                    background-color: #19bfff !important;
                    svg{
                        color: #fff;
                    }
                }
            }
        }
        .tableAction button {
            width: 130px;
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 20px;

            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 600;
                font-size: 20px;
            }
        }
    }
    .search-wrapper {
        flex-grow: 1;

        .search {
            width: 300px;
            border: 1px solid #333;
            border-radius: 30px;

            input {
                border: none;
            }
        }

        p {
            display: none;
        }

        .input-group,
        .form-control {
            background-color: transparent;
        }

        .input-group-addon {
            background-color: transparent;
            border: none;
        }

        .isvg {
            display: inline-block;
            width: 1rem;
        }
    }

    .leave-types {
        clear:both;
    }

    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }

    .editing {
        input{
            height: 25px;
        }

        p {
            display:none;
        }
    }
`;

export const FilterWrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    margin-bottom: 42px;
    border: 1px solid #ccc;
    border-radius: .5rem;

    &:before,
    &:after {
        position: absolute;
        right: 15.5rem;
        display: block;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 0 10px 10px 10px;
        content: '';
    }

    &:before {
        top: -10px;
        border-color: transparent transparent #ccc transparent;
    }
    
    &:after {
        top: -9px;
        z-index: 2;
        border-color: transparent transparent #fff transparent;
    }
    .filterMain{
        padding: 14px 14px 28px 14px;
    }
    .sl-c-filter-actions {
        display: flex;
        justify-content: space-between;
        border-top: 1px solid lightgrey;
        padding-top: 20px;
        align-items: center;
        padding: 6px 14px;

        .sl-c-filter-reset > .btn {
            margin-left: 0;
            color: #00A5E5;
            cursor: pointer;
            border: none;
        }

        .sl-c-filter-buttons > .btn {
            padding: .5rem 1.5rem;
        }
    }
    .date-picker {
        .DayPickerInput {
            width: 100%;
        }
        span {
            display: block;
        }
        input {
            width: 100%;
            padding-top: 0px !important;
            border: 1px solid #474747;
            font-size: 14px;
        }
    }
    .filterRow{
        padding: 0px 7px;
        .colSect{
            padding: 0px 7px;
            .Select-control{
                border: 1px solid #474747;
                color: #adadad;
                .Select-value-label{
                    color: #adadad;
                }
            }
        }
    }
`;
