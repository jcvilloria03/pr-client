import { createSelector } from 'reselect';

/**
 * Direct selector to the schedules state domain
 */
const selectSchedulesDomain = () => ( state ) => state.get( 'schedules' );

const makeSelectSchedules = () => createSelector(
  selectSchedulesDomain(),
  ( substate ) => substate.get( 'schedules' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectSchedulesDomain(),
  ( schedules ) => schedules.get( 'loading' )
);

const makeSelectActionLoading = () => createSelector(
  selectSchedulesDomain(),
  ( substate ) => substate.get( 'actionLoading' )
);

const makeSelectNotification = () => createSelector(
  selectSchedulesDomain(),
  ( schedules ) => schedules && schedules.get( 'notification' ).toJS()
);

const makeSelectFilterData = () => createSelector(
  selectSchedulesDomain(),
  ( schedules ) => schedules && schedules.get( 'filterData' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectSchedules,
  makeSelectFilterData,
  makeSelectNotification,
  makeSelectActionLoading
};
