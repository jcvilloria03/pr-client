import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_SCHEDULES,
    SET_FILTER_DATA,
    SET_NOTIFICATION,
    ACTION_LOADING
} from './constants';

const initialState = fromJS({
    loading: false,
    actionLoading: false,
    schedules: [],
    filterData: {
        locations: [],
        positions: [],
        departments: []
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Schedules reducer
 *
 */
function schedulesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case ACTION_LOADING:
            return state.set( 'actionLoading', action.payload );
        case SET_SCHEDULES:
            return state.set( 'schedules', fromJS( action.payload ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default schedulesReducer;
