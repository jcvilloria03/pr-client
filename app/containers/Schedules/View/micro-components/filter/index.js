/* eslint-disable camelcase */
import React from 'react';

import Button from 'components/Button';
import MultiSelect from 'components/MultiSelect';
import DatePicker from 'components/DatePicker';

import { formatDate } from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import { scheduledTypes } from '../../constants';
import { FilterWrapper } from '../../styles';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {
    static propTypes = {
        filterData: React.PropTypes.shape({
            positions: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    constructor( props ) {
        super( props );
        this.state = {
            endDate: null,
            startDate: null
        };

        this.onApply = this.onApply.bind( this );
    }

    onApply = () => {
        const filters = {
            scheduled_type: this.scheduled_type && this.scheduled_type.state.value,
            department: this.department && this.department.state.value,
            location: this.location && this.location.state.value,
            position: this.position && this.position.state.value
        };

        const scheduled_type = filters.scheduled_type || null;
        const department = filters.department || null;
        const location = filters.location || null;
        const position = filters.position || null;

        this.props.onApply({
            scheduled_type,
            department,
            location,
            position,
            start_date: this.state.startDate,
            end_date: this.state.endDate
        });
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    getFilterPropsValues( filterValues ) {
        const { filterData } = this.props;

        if ( !filterData[ filterValues ]) {
            return [];
        }
        return filterData[ filterValues ].map( ( filterValue ) => ({
            value: filterValue.name,
            label: filterValue.name,
            disabled: false
        }) );
    }

    resetFilters = () => {
        this.setState({
            startDate: null,
            endDate: null
        });

        this.location.setState({ value: null });
        this.department.setState({ value: null });
        this.position.setState({ value: null });
        this.scheduled_type.setState({ value: null });
    }

    render() {
        return (
            <FilterWrapper>
                <div className="filterMain">
                    <div className="row filterRow" key="1">
                        <div className="col-xs-3 colSect date-picker">
                            <DatePicker
                                label="Start date"
                                dayFormat={ DATE_FORMATS.API }
                                ref={ ( ref ) => { this.start_date = ref; } }
                                selectedDay={ this.state.startDate }
                                onChange={ ( value ) => {
                                    const selectedDay = formatDate( value, DATE_FORMATS.API );

                                    if ( selectedDay !== this.state.startDate ) {
                                        this.setState({
                                            startDate: formatDate( value, DATE_FORMATS.API )
                                        });
                                    }
                                } }
                                hasIcon
                                placeholder="Choose a date"
                            />
                        </div>
                        <div className="col-xs-3 colSect date-picker">
                            <DatePicker
                                label="End date"
                                dayFormat={ DATE_FORMATS.API }
                                ref={ ( ref ) => { this.end_date = ref; } }
                                selectedDay={ this.state.endDate }
                                onChange={ ( value ) => {
                                    const selectedDay = formatDate( value, DATE_FORMATS.API );

                                    if ( selectedDay !== this.state.endDate ) {
                                        this.setState({
                                            endDate: formatDate( value, DATE_FORMATS.API )
                                        });
                                    }
                                } }
                                hasIcon
                                placeholder="Choose a date"
                            />
                        </div>
                    </div>
                    <div className="row filterRow">
                        <div className="col-xs-3 colSect">
                            <MultiSelect
                                id="scheduled_type"
                                label="Schedule Type"
                                placeholder="Select types"
                                data={ scheduledTypes }
                                ref={ ( ref ) => { this.scheduled_type = ref; } }
                            />
                        </div>
                        <div className="col-xs-3 colSect">
                            <MultiSelect
                                id="location"
                                label="Locations"
                                data={ this.getFilterPropsValues( 'locations' ) }
                                placeholder="Select locations"
                                ref={ ( ref ) => { this.location = ref; } }
                            />
                        </div>
                        <div className="col-xs-3 colSect">
                            <MultiSelect
                                id="department"
                                label="Departments"
                                data={ this.getFilterPropsValues( 'departments' ) }
                                placeholder="Select departments"
                                ref={ ( ref ) => { this.department = ref; } }
                            />
                        </div>
                        <div className="col-xs-3 colSect">
                            <MultiSelect
                                id="position"
                                label="Positions"
                                placeholder="Select positions"
                                data={ this.getFilterPropsValues( 'positions' ) }
                                ref={ ( ref ) => { this.position = ref; } }
                            />
                        </div>
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
