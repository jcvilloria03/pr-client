import styled from 'styled-components';

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    justify-content: center;
    min-height: 100%;
    // text-align: center;
`;

export const EventViewWrapper = styled.div`
.hide {
    display: none;
}

.sl-c-popover--left {
    &:before {
        right: -8px;
        left: auto;
        border-width: 7.5px 0 7.5px 8px;
        border-color: transparent transparent transparent #fff;
    }
}

.sl-c-popover__header {
    display: flex;
    align-items: flex-end;
    margin-bottom: 1rem;
    padding-bottom: 1rem;
    border-bottom: 1px solid #ccc;

    .sl-c-popover__title {
        display: flex;
        flex: 1;
        font-family: 'sourcesanspro-bold', sans-serif;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        line-height: 1rem;
    
        &.sl-c-popover__title {
            margin-bottom: 0;
        }
    }

    .sl-c-actions {
        display: flex;
        flex: 0 0 30px;
        position: relative;
        line-height: 1rem;

        > button {
            display: flex;
            flex: 1;
        }

        .icon {
            width: 12px;
            display: inline-block;
            color: lightgrey;
            cursor: pointer;

            > i {
                align-self: center;
            }

            > svg {
                width: 12px;
                position: relative;
                top: -1px;
            }
        }
    }
}

.sl-c-popover__body {
    +.sl-c-popover__footer {
        margin-top: 1rem;
    }
}

.sl-c-popover__footer {
    display: flex;
    justify-content: flex-end;
}

`;

export const Wrapper = styled.div`
#calendarList{
    margin-bottom:2rem;
    .fc-view {
        z-index: 0;
    }

    .fc-agendaDay-view {
        .fc-time-grid-event.fc-v-event.fc-event {
            margin-right: 5px;
            margin-top: 1px;
            padding: .25rem;
            border: none;
        }
    }

    .fc-agendaWeek-view {
        .fc-time-grid-event.fc-v-event.fc-event {
            margin-right: 2px;
            margin-top: 1px;
            padding: .25rem;
            border: none;
        }
    }

    .fc-toolbar{
        border: 1px solid #ddd;
        margin-bottom: 0;
        padding: 15px;
        border-bottom: 0;
        .fc-left{
                align-items: center;
                display: flex;
            h2{
                font-size: 21px;
                color: #474747;
                font-weight:700;
            }
        }
        .fc-state-default{
            display: flex;
            align-items: center;
            justify-content: center;
            text-shadow: none;
            background-color: transparent;
            background-image: none;
            border-color: #ccc;
            box-shadow: none;
            height: 30px;
            font-size:14px;
            .fc-icon{
                &::after{
                    font-size: 26px;
                }
            }
            &:focus{
                outline:0;
            }
        }
        .fc-state-active{
            color: #fff;
            background-color: #00a5e5;
        }
    }
    .fc-view-container{
        table{
            thead{
                .fc-day-header{
                    background-color: #f0f4f6;
                }
                .fc-axis{
                    background-image: url(data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyMiAyMiI+PGcgaWQ9Im5hdmlnYXRpb24iPjxnIGlkPSJzY2hlZHVsZSI+PHBhdGggaWQ9IkNvbWJpbmVkLVNoYXBlIiBkPSJNMS40MSwxMmExMSwxMSwwLDEsMSwxMSwxMUExMSwxMSwwLDAsMSwxLjQxLDEyWk0xMyw2LjVIMTEuMzFWMTNMMTcsMTYuNGwuODgtMS40TDEzLDEyLjFaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMS40MSAtMSkiLz48L2c+PC9nPjwvc3ZnPg==);
                    background-repeat: no-repeat;
                    background-position: 50%;
                    background-size: .9rem .9rem;
                }
            }
            tbody{
                td.fc-day.fc-past {
                    background-color: #EEEEEE;
                }
                .fc-slats{
                    .fc-time{
                        span{
                            font-size:14px;
                        }
                    }
                }
            }
        }
    }
    }
    .fc-unthemed .fc-today{
    background-color: #f0f4f6;
    }
    .event-popover-main{
        position: absolute;
    padding: 1rem;
    filter: drop-shadow(0 0 .1rem #474747);
    background-color: #ffffff;
    border-radius: 0.5rem;
    width: 25rem;
    z-index: 20;
        &:before {
        position: absolute;
        left: -8px;
        top: 50%;
        transform: translateY(-50%);
        display: block;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 7.5px 8px 7.5px 0;
        border-color: transparent #fff transparent transparent;
        content: '';
        }
        .sl-c-popover{
                h6{
                color: #474747;
                font-size: 16px;
                font-weight: bold;
            }
        }
        .flexidetail{
                h4{
                    margin-bottom: 0;
                    font-size: 14px;
                    font-weight: bold;
                }
                p{
                    font-size: 13px;
                    margin-bottom: 20px;
                    color: #474747;
                }
        }
        .btnFlexi{
                display: flex;
                align-items: center;
                justify-content: flex-end;

                button{
                    background-color: #ec7575;
                    border-radius: 24px;
                    color: #fff;
                    font-weight: 600;
                    padding: 8px 20px;
                    font-size: 12px;
                    cursor:pointer;
                    border-color: transparent;
                    transition: all 0.3s ease-in-out;

                    &:focus{
                        outline:0;
                    }

                    &:hover {
                        opacity: 0.8;
                    }
                }
                .btnEdit{
                    background-color: #84d24b;
                }
        }
    }
    .event_popover_left{
    &:before{
            right: -8px;
            left:auto;
            border-width: 7.5px 0 7.5px 8px;
            border-color: transparent transparent transparent #ffffff;
    }
    }
`;
