/* eslint-disable radix */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
import moment from 'moment';
import 'moment-recur';
import _ from 'lodash';
import $ from 'jquery';

const scheduleStylesCache = {};
const END_NEVER_VALUE = 730;
const SCHEDULE_TYPES = {
    flexi: 'Flexi Schedule',
    fixed: 'Fixed Schedule'
};

  /**
   * Render event to calendar
   * @param {Object} calendarEvent calendar event object
   * @param {jQuery Object} element jquery element of event
   * @param {Object} view selected view in calendar
   */
export const renderEvent = ( calendarEvent, element, view ) => {
    if ( view.name === 'agendaDay' ) {
        renderDayViewEvent( calendarEvent, element );
    } else if ( view.name === 'agendaWeek' ) {
        renderWeekViewEvent( calendarEvent, element );
    } else {
        renderMonthViewEvent( calendarEvent, element );
    }
};

 /**
   * Process schedules and return repeated events for calendar.
   * @param {Array} schedules
   * @return {Array}
   */
export const processSchedules = ( schedules, interval ) => {
    let events = [];

    _.each( schedules, ( schedule ) => {
        events = events.concat( processSchedule( schedule, interval ) );
    });

    return events;
};

/**
   * Get all occurrences.
   * @param {Object} schedule
   * @param {Object} recurrence
   * @return {Array}
   */
export const getAllOccurrences = ( schedule, recurrence ) => {
    if ( schedule.repeat.end_on ) {
        return getAllOccurrencesWhenEndIsAvailable( schedule, recurrence );
    }

    return getAllOccurrencesWhenEndIsNotSet( schedule, recurrence );
};

/**
   * Get only requested number of occurrences.
   * @param {Array} allDates
   * @param {Object} schedule
   * @return {Array}
   */
const applyLimitToNumberOfOccurrences = ( allOccurrences, schedule ) => {
    if ( !schedule.repeat.end_after && !schedule.repeat.end_never ) {
        return allOccurrences;
    }

    if ( schedule.repeat.end_never ) {
        return allOccurrences.splice( 0, END_NEVER_VALUE );
    }

    return allOccurrences.splice( 0, schedule.repeat.end_after );
};

/**
   * Create add schedule button
   * @param {Object} target clicked target (where to add button)
   * @param {momentjs} date clicked date, where to append button
   * @param {String} viewName name of selected calendar view
   * @returns {jQuery Object} Add schedule button element
   */
export const getAddScheduleButton = ( target, date, viewName ) => {
    const dayElement = $( target );
    const addScheduleButton = $( '<button class="sl-c-add-schedule-btn sl-c-btn sl-c-btn--primary sl-c-btn--ghost js-add-schedule-button">+ Add schedule</button>' );
    const heightCorrection = viewName === 'month' ? 1 : 2;
    const height = dayElement[ 0 ].clientHeight * heightCorrection;
    const dataDate = date.clone().format( 'YYYY-MM-DD' );

    const columnElement = $( `.fc-day.fc-widget-content[data-date="${dataDate}"]` );

    // top 671 umesto 450
    addScheduleButton.css({
        position: 'absolute',
        top: `${dayElement[ 0 ].offsetTop}px`,
        width: `${columnElement.width()}px`,
        height: `${height}px`,
        cursor: 'pointer',
        'z-index': '100'
    });

    if ( viewName === 'month' ) {
        addScheduleButton.css({
            left: `${columnElement.offset().left}px`,
            top: `${dayElement.offset().top}px`
        });
    }

    return addScheduleButton;
};

  /**
   * Navigate calendar view to date
   * @param {String} date to navigate
   */
export const goToDate = ( date ) => {
    $( '#calendar' ).fullCalendar( 'gotoDate', date );
};

/**
   * Remove given event from calendar
   * @param {Object} event to remove from calendar
   */
export const removeEvent = ( event ) => {
    $( '#calendar' ).fullCalendar( 'removeEvents', [event.id]);
};

  /**
   * Render event for day view to jquery element in calendar
   * @param {Object} calendarEvent calendar event object
   * @param {jQuery Object} element jquery element of event
   */
const renderDayViewEvent = ( calendarEvent, element ) => {
    const dayViewEventHtml = getDayEventHtml( calendarEvent );
    element.html( dayViewEventHtml );

    if ( calendarEvent.color ) {
        addEventStyles( calendarEvent.color, element );
    }
};

  /**
   * Render event for week view to jquery element in calendar
   * @param {Object} calendarEvent calendar event object
   * @param {jQuery Object} element jquery element of event
   */
const renderWeekViewEvent = ( calendarEvent, element ) => {
    const weekViewEventHtml = getWeekEventHtml( calendarEvent );
    element.html( weekViewEventHtml );

    if ( calendarEvent.color ) {
        addEventStyles( calendarEvent.color, element );
    }
};

  /**
   * Render event for month view to jquery element in calendar
   * @param {Object} calendarEvent calendar event object
   * @param {jQuery Object} element jquery element of event
   */
const renderMonthViewEvent = ( calendarEvent, element ) => {
    const monthViewEventHtml = getMonthEventHtml( calendarEvent );
    element.html( monthViewEventHtml );
};

  /**
   * Create event html for day view
   * @param {Object} calendarEvent calendar event object
   * @returns {String} html string
   */
const getDayEventHtml = ( calendarEvent ) => {
    const time = `${getFormattedEventTime( calendarEvent.start_time )
      } to ${getFormattedEventTime( calendarEvent.end_time )}`;

    return `
      <div class="calendar-event">
        <span class="event-title" title="${calendarEvent.name}" style="display: block;">
          ${calendarEvent.name}
        </span>
        <span class="event-type" title="${calendarEvent.titleType}" style="display: block;">
          ${calendarEvent.titleType}
        </span>
        <span class="event-time" title="${time}" style="display: block;">
          ${time}
        </span>
      </div>
    `;
};

  /**
   * Create html for event in week view
   * @param {Object} calendarEvent calendar event object
   * @returns {String} html string
   */
const getWeekEventHtml = ( calendarEvent ) => {
    const date = moment( calendarEvent.start_date ).format( 'ddd D' );
    const time = `${getFormattedEventTime( calendarEvent.start_time )
    } to ${getFormattedEventTime( calendarEvent.end_time )}`;

    return `
      <div class="calendar-event">
        <span class="event-title" title="${calendarEvent.name}" style="display: block;">
          ${calendarEvent.name}
        </span>
        <span class="event-date" title="${date}" style="display: block;">
          ${date}
        </span>
        <span class="event-time" title="${time}" style="display: block;">
          ${time}
        </span>
      </div>
    `;
};

  /**
   * Create html for event in month view
   * @param {Object} calendarEvent calendar event object
   * @returns {String} html string
   */
const getMonthEventHtml = ( calendarEvent ) => `
      <div class="block-content calendar-event calendar-event-element">
        <span class="event-title">
          <span class="sl-u-bullet sl-u-gap-right--sm" style="background-color: ${calendarEvent.color}"></span>
          ${calendarEvent.name}
        </span>
      </div>
    `;

  /**
   * Format time for event representation in calendar
   * @param {String} time time in format HH:mm:ss
   * @returns {String} formattedTime in HH:mm format
   */
const getFormattedEventTime = ( time ) => moment( time, 'HH:mm:ss' ).format( 'HH:mm' );

  /**
   * Process one schedule and return repeated events.
   * @param {Object} schedule
   * @return {Array}
   */
const processSchedule = ( schedule, interval ) => {
    if ( !schedule.repeat ) {
        const event = _.clone( schedule );
        event.schedule = schedule;

        return [processSingleEvent( event )];
    }

    // Wrap at "end on" date if it's the lower limit.
    const intervalEnd = getIntervalEnd( schedule.repeat, interval );

    // No need to process further schedule if it's not before the end of interval.
    if ( moment( schedule.start_date ).isAfter( intervalEnd ) ) {
        return [];
    }

    return processRepeated( schedule, interval.start, intervalEnd );
};

  /**
   * Process repeating schedules depending on the type.
   * @param {Object} schedule
   * @param {Moment} intervalStart
   * @param {Moment} intervalEnd
   * @return {Array}
   */
const processRepeated = ( schedule, intervalStart, intervalEnd ) => {
    switch ( schedule.repeat.type ) {
        case 'daily':
            return processDailyEvents( schedule, intervalStart, intervalEnd );
        case 'every_weekday':
            return processEveryWeekdayEvents( schedule, intervalStart, intervalEnd );
        case 'every_monday_wednesday_friday':
            return processEveryMondayWednesdayFridayEvents( schedule, intervalStart, intervalEnd );
        case 'every_tuesday_thursday':
            return processEveryTuesdayThursdayEvents( schedule, intervalStart, intervalEnd );
        case 'weekly':
            return processWeeklyEvents( schedule, intervalStart, intervalEnd );
        case 'monthly':
            return processMonthlyEvents( schedule, intervalStart, intervalEnd );
        case 'yearly':
            return processYearlyEvents( schedule, intervalStart, intervalEnd );

        default:
    }

    return [];
};

  /**
   * Get lower limit from interval end repeat "end on", unless it never ends.
   * @param {Object} scheduleRepeat
   * @param {Object} interval
   * @return {Moment}
   */
const getIntervalEnd = ( scheduleRepeat, interval ) => {
    const endOn = moment( scheduleRepeat.end_on );
    const validEndOn = endOn.isValid() && endOn.isBefore( interval.end );

    if ( scheduleRepeat.end_never || !validEndOn ) {
        return interval.end;
    }

    return endOn;
};

  /**
   * Process single event to set start, end and title.
   * @param {Object} schedule
   * @return {Object}
   */
const processSingleEvent = ( schedule ) => {
    schedule.start = `${schedule.start_date} ${schedule.start_time}`;
    schedule.end = getEventEnd( schedule );
    schedule.titleType = SCHEDULE_TYPES[ schedule.type ];

    return schedule;
};

  /**
   * Calculate event end.
   * If the end time is before start time, that means we need date for tomorrow.
   * @param {Object} schedule
   * @return {String}
   */
const getEventEnd = ( schedule ) => {
    const start = moment( `${schedule.start_date} ${schedule.start_time}`, 'YYYY-MM-DD HH:mm' );
    let end = moment( `${schedule.start_date} ${schedule.end_time}`, 'YYYY-MM-DD HH:mm' );

    if ( end.isSameOrBefore( start ) ) {
        end = end.add( 1, 'd' );
    }

    return end.format( 'YYYY-MM-DD HH:mm' );
};

  /**
   * Limit occurrences and map dates to single event.
   * @param {Array} dates
   * @param {Object} schedule
   * @return {Array}
   */
const mapDatesToEvents = ( dates, schedule ) => dates.map( ( date ) => {
    const event = _.clone( schedule );

    event.schedule = schedule;
    event.start_date = date;

    return processSingleEvent( event );
});

  /**
   * Get dates of occurrences having in mind "end after" value.
   * @param {Object} schedule
   * @param {Object} recurrence
   * @param {Moment} intervalStart
   * @return {Array}
   */
const getOccurrenceDates = ( schedule, recurrence, intervalStart ) => {
    if ( !schedule.repeat.end_after ) {
        return recurrence.fromDate( intervalStart ).all( 'YYYY-MM-DD' );
    }

    // Get only those occurrences that are actually within interval.
    return _.intersection(
      getRequiredNumberOfOccurrences( schedule, recurrence ),
      recurrence.fromDate( intervalStart ).all( 'YYYY-MM-DD' )
    );
};

  /**
   * Get all dates of occurrences.
   * @param {Object} schedule
   * @param {Object} recurrence
   * @return {Array} dates
   */
const getRequiredNumberOfOccurrences = ( schedule, recurrence ) => {
    const dates = [];
    let takeNext = schedule.repeat.end_never
      ? END_NEVER_VALUE
      : schedule.repeat.end_after;

    // If start date is included in the occurrences array,
    // take only the rest of them
    if ( recurrence.matches( schedule.start_date ) ) {
        dates.push( schedule.start_date );
        takeNext--;
    }

    return dates.concat( recurrence.next( takeNext, 'YYYY-MM-DD' ) );
};

  /**
   * Get all occurrences when end date is available.
   * @param {Object} schedule
   * @param {Object} recurrence
   * @return {Array}
   */
const getAllOccurrencesWhenEndIsAvailable = ( schedule, recurrence ) => {
    recurrence.endDate( schedule.repeat.end_on );

    return recurrence.all();
};

  /**
   * Get required number of occurrences if we don't have end set on recurrence.
   * NOTE: This is not very performant and is currently used to get
   * last date for only one schedule so it's kinda fine.
   *
   * @param {Object} schedule
   * @param {Object} recurrence
   * @return {[type]}
   */
const getAllOccurrencesWhenEndIsNotSet = ( schedule, recurrence ) => {
    const numberOfOccurrencesToTake = schedule.repeat.end_never
      ? END_NEVER_VALUE
      : schedule.repeat.end_after;

    let takeNext = numberOfOccurrencesToTake * schedule.repeat.repeat_every;

    const allDates = [];
    if ( recurrence.matches( schedule.start_date ) ) {
        allDates.push( moment( schedule.start_date ) );
        takeNext--;
    }

    return allDates.concat( recurrence.next( takeNext ) );
};

  /**
   * Get event occurrences for interval.
   * @param {Object} schedule
   * @param {Object} recurrence
   * @param {Moment} intervalStart
   * @return {Array}
   */
const getRecurringEvents = ( schedule, recurrence, intervalStart ) => {
    // Get all occurrences within interval.
    const dates = getOccurrenceDates( schedule, recurrence, intervalStart );

    // Make event for every occurrence.
    return mapDatesToEvents( dates, schedule );
};

  /**
   * Process schedule with "daily" repeat type.
   * @param {Object} schedule
   * @param {Moment} intervalStart
   * @param {Moment} intervalEnd
   * @return {Array}
   */
const processDailyEvents = ( schedule, intervalStart, intervalEnd ) => {
    // Define recurrence
    const recurrence = moment( schedule.start_date ).recur( intervalEnd )
      .every( schedule.repeat.repeat_every, 'days' );

    // Get all occurrences within interval.
    return getRecurringEvents( schedule, recurrence, intervalStart );
};

  /**
   * Process schedule with "every_weekday" repeat type.
   * @param {Object} schedule
   * @param {Moment} intervalStart
   * @param {Moment} intervalEnd
   * @return {Array}
   */
const processEveryWeekdayEvents = ( schedule, intervalStart, intervalEnd ) => {
    // Define recurrence
    const recurrence = moment( schedule.start_date ).recur( intervalEnd )
      .every([ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday' ]).daysOfWeek();

    // Get all occurrences within interval.
    return getRecurringEvents( schedule, recurrence, intervalStart );
};

  /**
   * Process schedule with "every_monday_wednesday_friday" repeat type.
   * @param {Object} schedule
   * @param {Moment} intervalStart
   * @param {Moment} intervalEnd
   * @return {Array}
   */
const processEveryMondayWednesdayFridayEvents = ( schedule, intervalStart, intervalEnd ) => {
    // Define recurrence
    const recurrence = moment( schedule.start_date ).recur( intervalEnd )
      .every([ 'Monday', 'Wednesday', 'Friday' ]).daysOfWeek();

    // Get all occurrences within interval.
    return getRecurringEvents( schedule, recurrence, intervalStart );
};

  /**
   * Process schedule with "every_tuesday_thursday" repeat type.
   * @param {Object} schedule
   * @param {Moment} intervalStart
   * @param {Moment} intervalEnd
   * @return {Array}
   */
const processEveryTuesdayThursdayEvents = ( schedule, intervalStart, intervalEnd ) => {
    // Define recurrence
    const recurrence = moment( schedule.start_date ).recur( intervalEnd )
      .every([ 'Tuesday', 'Thursday' ]).daysOfWeek();

    // Get all occurrences within interval.
    return getRecurringEvents( schedule, recurrence, intervalStart );
};

  /**
   * Process schedule with "yearly" repeat type.
   * @param {Object} schedule
   * @param {Moment} intervalStart
   * @param {Moment} intervalEnd
   * @return {Array}
   */
const processYearlyEvents = ( schedule, intervalStart, intervalEnd ) => {
    // Define recurrence
    const recurrence = moment( schedule.start_date ).recur( intervalEnd )
      .every( schedule.repeat.repeat_every, 'years' );

    // Get all occurrences within interval.
    return getRecurringEvents( schedule, recurrence, intervalStart );
};

  /**
   * Process schedule with "monthly" repeat type.
   * @param {Object} schedule
   * @param {Moment} intervalStart
   * @param {Moment} intervalEnd
   * @return {Array}
   */
const processMonthlyEvents = ( schedule, intervalStart, intervalEnd ) => {
    const startDate = moment( schedule.start_date );

    // Define recurrence
    const recurrence = startDate.recur( intervalEnd );

    if ( schedule.repeat.repeat_by === 'week' ) {
      // Get for all weeks, we'll later take the proper ones
        recurrence.every( startDate.format( 'e' ) ).daysOfWeek();
    } else {
        recurrence.every( startDate.date() ).daysOfMonth();
    }

    // Get all occurrences within interval.
    const dates = getMonthlyOccurrenceDates( schedule, recurrence, intervalStart );

    // Make event for every occurrence.
    return mapDatesToEvents( dates, schedule );
};

  /**
   * Get dates of monthly occurrences having in mind "end after" value.
   * @param {Object} schedule
   * @param {Object} recurrence
   * @param {Moment} intervalStart
   * @return {Array}
   */
const getMonthlyOccurrenceDates = ( schedule, recurrence, intervalStart ) => {
    const allOccurrences = getAllMonthlyOccurrences( schedule, recurrence.all() );

    return getRequestedNumberOfOccurrenceDatesAndAfterIntervalStart( allOccurrences, schedule, intervalStart );
};

  /**
   * Get all monthly occurrence dates regardless of interval or limit to number of occurrences.
   * @param {Object} schedule
   * @param {Object} recurrence
   * @return {Array}
   */
const getAllMonthlyOccurrences = ( schedule, allOccurrences ) => {
    const startDate = moment( schedule.start_date );
    const all = schedule.repeat.repeat_by === 'week'
      ? getValidWeekDayInMonths( allOccurrences, startDate.monthWeekByDay() )
      : allOccurrences;

    // Get only every nth item
    return all.filter( ( item, index ) => ( index % schedule.repeat.repeat_every ) === 0 );
};

  /**
   * Get valid weekday for each month.
   * @param {Array} all
   * @param {Number} weekNumberByDay
   * @return {Array}
   */
const getValidWeekDayInMonths = ( all, weekNumberByDay ) => {
    const groups = _.groupBy( all, ( date ) => date.format( 'YYYYMM' ) );
    const dates = [];
    let addedForTheMonth = false;

    Object.keys( groups ).forEach( ( group ) => {
        addedForTheMonth = false;

        groups[ group ].forEach( ( date ) => {
            // We only want to add one date for a month
            if ( addedForTheMonth ) {
                return;
            }

            // We need 0-based value for comparison
            const weekDayNumberInMonth = getWeekDayNumberInMonth( date ) - 1;
            const lastValidWeekOfMonth = weekDayNumberInMonth < weekNumberByDay && date.monthWeekByDay() === weekDayNumberInMonth;
            const exactWeek = date.monthWeekByDay() === weekNumberByDay;

            // If month has 4 Fridays and we need 5th Friday, we'll use the 4th one.
            // Otherwise use exact occurrence of weekday.
            if ( lastValidWeekOfMonth || exactWeek ) {
                dates.push( date );

                // Mark that we added a value for this month
                addedForTheMonth = true;
            }
        });
    });
    return dates;
};

  /**
   * Get number of times given weekday is within month.
   * @param {Moment} momentObj
   * @return {Number}
   */
const getWeekDayNumberInMonth = ( momentObj ) => {
    const weekday = momentObj.day();
    const date = momentObj.clone().startOf( 'month' ).startOf( 'day' );
    const days = [];

    for ( let i = 0; i < momentObj.daysInMonth(); i++ ) {
        if ( weekday === date.day() ) {
            days.push( moment( date ) );
        }

        date.add( 1, 'day' );
    }

    return days.length;
};

  /**
   * Process schedule with "weekly" repeat type.
   * @param {Object} schedule
   * @param {Moment} intervalStart
   * @param {Moment} intervalEnd
   * @return {Array}
   */
const processWeeklyEvents = ( schedule, intervalStart, intervalEnd ) => {
    // Define recurrence
    const recurrence = moment( schedule.start_date ).recur( intervalEnd )
      .every( schedule.repeat.repeat_on ).daysOfWeek();

    const dates = getWeeklyOccurrenceDates( schedule, recurrence, intervalStart );

    // Make event for every occurrence.
    return mapDatesToEvents( dates, schedule );
};

  /**
   * Get dates of monthly occurrences having in mind "end after" value.
   * @param {Object} schedule
   * @param {Object} recurrence
   * @param {Moment} intervalStart
   * @return {Array}
   */
const getWeeklyOccurrenceDates = ( schedule, recurrence, intervalStart ) => {
    const allOccurrences = getAllWeeklyOccurrences( schedule, recurrence.all() );

    return getRequestedNumberOfOccurrenceDatesAndAfterIntervalStart( allOccurrences, schedule, intervalStart );
};

  /**
   * Get all weekly occurrences.
   * @param {Object} schedule
   * @param {Array} allOccurrences
   * @return {Array}
   */
const getAllWeeklyOccurrences = ( schedule, allOccurrences ) => {
    const groupedDates = _.values( _.groupBy( allOccurrences, ( date ) => date.format( 'YYYY' ) + date.week() ) );

    let occurrences = [];

    // Get only every nth week
    groupedDates.forEach( ( dates, index ) => {
        if ( ( index % schedule.repeat.repeat_every ) === 0 ) {
            occurrences = occurrences.concat( dates );
        }
    });

    return occurrences;
};

  /**
   * Get requested number of occurrences and after interval start.
   * @param {Array} allDates
   * @param {Object} schedule
   * @param {Moment} intervalStart
   * @return {Array}
   */
const getRequestedNumberOfOccurrenceDatesAndAfterIntervalStart = ( allOccurrences, schedule, intervalStart ) =>
    // Get only dates after inside interval
     applyLimitToNumberOfOccurrences( allOccurrences, schedule )
      .filter( ( date ) => date.isSameOrAfter( intervalStart ) )
      .map( ( date ) => date.format( 'YYYY-MM-DD' ) );

  /**
   * Apply event styles to the element.
   * @param {String} color
   * @param {Object} element
   */
const addEventStyles = ( color, element ) => {
    const styles = eventStyles( color );
    element.css( 'background-image', styles.backgroundImage );
    element.css( 'color', styles.textColor );
};

  /**
   * Generate specific styles for events.
   * Cache them for performance.
   * @param {String} color
   * @return {Object}
   */
const eventStyles = ( color ) => {
    if ( !scheduleStylesCache.hasOwnProperty( color ) ) {
        const secondary = changeColorLuminance( color, 60 );

        scheduleStylesCache[ color ] = {
            textColor: isColorLuminant( color ) ? 'black' : 'white',
            backgroundImage: 'repeating-linear-gradient(45deg, transparent, transparent 3px, ' +
          'rgba(0, 0, 0, 0.07) 3px, rgba(0, 0, 0, 0.07) 4px), ' +
          `linear-gradient(to bottom, ${color}, ${secondary})`
        };
    }

    return scheduleStylesCache[ color ];
};
export const changeColorLuminance = ( color, lum ) => {
    const hex = rgb2hex( color ).replace( '#', '' );

    lum = lum || 0;

  // Convert to decimal and change luminosity
    let rgb = '#';
    let c;
    for ( let i = 0; i < 3; i++ ) {
        c = parseInt( hex.substr( i * 2, 2 ), 16 );
        c = Math.round( Math.min( Math.max( 0, c + lum ), 255 ) ).toString( 16 );
        rgb += ( `00${c}` ).substr( c.length );
    }

    return rgb;
};

const rgb2hex = ( color ) => {
    if ( color.indexOf( 'rgb' ) === -1 ) {
        let hex = String( color ).replace( /[^0-9a-f]/gi, '' );

        if ( hex.length < 6 ) {
            hex = hex[ 0 ] + hex[ 0 ] + hex[ 1 ] + hex[ 1 ] + hex[ 2 ] + hex[ 2 ];
        }

        return `#${hex}`;
    }

    const rgb = color.match( /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/ );

    return `#${hex( rgb[ 1 ])}${hex( rgb[ 2 ])}${hex( rgb[ 3 ])}`;
};

const hex = ( x ) => ( `0${parseInt( x ).toString( 16 )}` ).slice( -2 );

const colorLuminance = ( color ) => {
    const hexCode = rgb2hex( color ).replace( '#', '' );

    const colorRgb = {
        red: parseInt( hexCode.substr( 0, 2 ), 16 ),
        green: parseInt( hexCode.substr( 2, 2 ), 16 ),
        blue: parseInt( hexCode.substr( 4, 2 ), 16 )
    };

    Object.keys( colorRgb ).forEach( ( name ) => {
        let value = colorRgb[ name ] / 255;

        if ( value < 0.03928 ) {
            value /= 12.92;
        } else {
            value = ( ( value + 0.055 ) / 1.055 ) ** 2;
        }

        colorRgb[ name ] = value;
    });

    return colorRgb.red * 0.2126 + colorRgb.green * 0.7152 + colorRgb.blue * 0.0722;
};

export const isColorLuminant = ( color ) => colorLuminance( color ) > 0.55;
