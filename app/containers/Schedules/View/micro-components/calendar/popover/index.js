import React from 'react';
import moment from 'moment';

import Icon from 'components/Icon';
import Button from 'components/Button';
import { H6, H4 } from 'components/Typography';
import { browserHistory } from 'utils/BrowserHistory';

import { EventViewWrapper } from '../styles';
import styled from 'styled-components';
/**
 *
 * CalendarEventPopOver Component
 *
 */

const StyledData = styled.p`
    text-transform: capitalize;
`;

class CalendarEventPopOver extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        singleEvent: React.PropTypes.object,
        closeEventPopOver: React.PropTypes.func,
        removeSchedule: React.PropTypes.func
    };

    componentDidMount() {
        setTimeout( () => {
            // eslint-disable-next-line no-undef
            $( '.calendar-event' ).parents( '.fc-bg' ).addClass( 'fc-time-grid-event fc-v-event fc-event fc-start fc-end' );
        }, 100 );
    }

    remove = () => {
        this.props.removeSchedule([this.props.singleEvent.event.id]);
    }

    render() {
        const eventValue = this.props.singleEvent.event;
        return (
            <div>
                <EventViewWrapper>
                    <div className="sl-c-popover">
                        <div className="sl-c-popover__header">
                            <H6 className="sl-c-popover__title">{ eventValue.name }</H6>

                            <div className="sl-c-actions">
                                <button onClick={ () => this.props.closeEventPopOver() }>
                                    <Icon name="remove" className="icon" />
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="flexidetail pt-1">
                        <H4>Schedule type:</H4>
                        <StyledData>{eventValue.type} Schedule</StyledData>
                        <H4>Start date:</H4>
                        <p>{ moment( eventValue.start_date ).format( 'dddd, MMMM Do, YYYY' )}</p>
                        <H4>Start and end time:</H4>
                        <p>{eventValue.start_time} to {eventValue.end_time}</p>
                        <H4>Entitled employees</H4>
                        <p>{eventValue.affected_employees.length ? eventValue.affected_employees.map( ( i ) => i.name ).join( ', ' ) : 'None'}</p>
                        <H4>Tags:</H4>
                        <p>{eventValue.tags.length ? eventValue.tags.map( ( i ) => i.name ).join( ', ' ) : 'None' }</p>
                    </div>
                    <div className="btnFlexi d-flex justify-content-center">
                        <Button
                            label={ <span>Delete</span> }
                            type="danger"
                            onClick={ () => this.remove() }
                        />
                        <button className="btnEdit ml-1" onClick={ () => browserHistory.push( `/time/schedules/edit/${eventValue.id}`, true ) }>Edit</button>
                    </div>
                </EventViewWrapper>
            </div>
        );
    }
}

export default CalendarEventPopOver;
