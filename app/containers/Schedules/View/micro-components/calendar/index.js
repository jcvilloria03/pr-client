/* eslint-disable react/sort-comp */
/* eslint-disable require-jsdoc */
/* eslint-disable react/no-string-refs */
/* eslint-disable no-useless-return */
import React from 'react';
import 'fullcalendar';
import $ from 'jquery';
import _ from 'lodash';
import moment from 'moment';
import Popper from 'popper.js';

import { browserHistory } from 'utils/BrowserHistory';

import {
    renderEvent,
    processSchedules,
    getAddScheduleButton
} from './utils';

import CalendarEventPopOver from './popover';
import { Wrapper } from './styles';

export default class Calendar extends React.Component {
    static propTypes = {
        filter: React.PropTypes.object,
        schedules: React.PropTypes.array,
        deleteModal: React.PropTypes.func
    };
    /**
    * constructor()
    */
    constructor( props ) {
        super( props );

        this.state = {
            timeoutId: null,
            popover: null,
            events: [],
            currentMonth: moment().month(),
            currentInterval: {
                start: moment().subtract( 1, 'month' ).startOf( 'month' ),
                end: moment().add( 1, 'month' ).endOf( 'month' )
            }
        };
    }

    componentDidMount() {
        this.showCalendar();
    }

    componentDidUpdate(prevProps) {
        if ( !_.isEqual(prevProps.schedules, this.props.schedules) ) {
            this.reloadEvents( this.props.schedules, this.props.filter );
        }
    }

    /**
       * Initialize the calendar.
       */
    showCalendar() {
        this.prepareEvents();

        $( document ).ready( () => {
            $( this.calendar ).fullCalendar({
                header: {
                    left: 'prev,next title',
                    center: '',
                    right: 'agendaDay,agendaWeek,month'
                },
                nowIndicator: true,
                defaultView: 'agendaDay',
                allDaySlot: false,
                slotLabelFormat: 'H:mm',
                slotEventOverlap: false,
                eventOverlap: false,
                buttonText: {
                    agendaDay: 'Day',
                    agendaWeek: 'Week',
                    month: 'Month'
                },
                views: {
                    agendaDay: {
                        columnFormat: 'dddd D',
                        titleFormat: 'dddd MMMM D, YYYY'
                    },
                    agendaWeek: {
                        columnFormat: 'ddd D',
                        titleFormat: 'MMMM D, YYYY',
                        eventBorderColor: 'darkgray'
                    },
                    month: {
                        columnFormat: 'dddd',
                        titleFormat: 'MMMM YYYY',
                        eventLimit: 5
                    }
                },
                events: this.state.events,

                eventRender: ( calendarEvent, element, view ) => {
                    renderEvent( calendarEvent, element, view );
                },

                viewRender: ( view ) => {
                    this.handleViewRenderToChangeInterval( view );

                    this.removeAddScheduleButton();
                },

                dayClick: ( date, jsEvent, view ) => {
                    if ( jsEvent.target.className === 'js-add-schedule-button' ) {
                        browserHistory.push( '/time/schedules/add', true );
                        return;
                    }

                    this.removeAddScheduleButton();

                    const target = this.getAddScheduleButtonTarget( jsEvent, date, view.name );
                    this.appendAddScheduleButton( target, date, view.name );
                },

                eventClick: ( event, jsEvent ) => {
                    const element = $( jsEvent.target );

                    this.removeAddScheduleButton();
                    this.showPopover( event );

                    this.createNewPopover( element.closest( '.fc-event' ), this.eventPopover, ( data ) => {
                        this.setState( ( prevState ) => (
                            { popover: {
                                ...prevState.popover,
                                flipped: data.flipped
                            }}
                        ) );
                    });
                }
            });
        });
    }

    prepareEvents( schedules = this.props.schedules, filter = this.props.filter ) {
        const { currentInterval } = this.state;

        const hasFilter = Object.values( filter ).length;

        const interval = {
            start: hasFilter && filter.start_date || currentInterval.start,
            end: hasFilter && filter.end_date || currentInterval.end
        };

        this.setState({ events: processSchedules( schedules, interval ) });
    }

    /**
     * Remove add schedule button from calendar
     */
    removeAddScheduleButton() {
        $( '.js-add-schedule-button' ).remove();
    }

    showPopover( event ) {
        this.setState({ popover: {
            event,
            flipped: false
        }});
    }

    closePopover() {
        if ( this.eventPopover ) {
            this.setState({ popover: null });
        }
    }

    async reloadEvents( schedules, filter ) {
        await this.prepareEvents( schedules, filter );
        await $( this.calendar ).fullCalendar( 'removeEvents' );
        await $( this.calendar ).fullCalendar( 'addEventSource', this.state.events );
        await $( this.calendar ).fullCalendar( 'rerenderEvents' );
    }

    /**
       * Handle view render to change interval of prepared events.
       * @param {Object} view
       */
    handleViewRenderToChangeInterval( view ) {
        const { currentMonth } = this.state;
        if ( currentMonth !== view.intervalStart.month() ) {
            this.setState({
                currentMonth: view.intervalStart.month(),
                currentInterval: {
                    start: view.intervalStart,
                    end: view.intervalEnd
                }
            });
        }
        this.reloadEvents();
    }

    /**
     * returns target for addSchedule button based on view
     * @param {Object} jsEvent js click event
     * @param {momentjs} date clicked date, where to append button
     * @param {String} viewName name of selected calendar view
     * @return {object} target for adding button
     */
    getAddScheduleButtonTarget( jsEvent, date, viewName ) {
        if ( viewName === 'month' ) {
            const dataDate = date.clone().format( 'YYYY-MM-DD' );
            const target = $( `.fc-day.fc-widget-content[data-date="${dataDate}"]` );

            return target;
        }

        return jsEvent.target;
    }

    /**
       * Append add schedule button to body
       * @param {Object} target clicked target (where to add button)
       * @param {momentjs} date clicked date, where to append button
       * @param {String} viewName name of selected calendar view
       */
    appendAddScheduleButton( target, date, viewName ) {
        const { timeoutId } = this.state;

        if ( timeoutId ) {
            clearTimeout( timeoutId );
        }
        const addScheduleButton = getAddScheduleButton( target, date, viewName );

        addScheduleButton.click( () => {
            this.removeAddScheduleButton();
            browserHistory.push( '/time/schedules/add', true );

            return;
        });
        this.closePopover();

        if ( viewName === 'agendaDay' ) {
            addScheduleButton.appendTo( $( '.fc-helper-container' ) );
        } else if ( viewName === 'agendaWeek' ) {
            const weekViewRows = $( '.fc-helper-container' );
            addScheduleButton.appendTo( weekViewRows[ date.weekday() ]);
        } else {
            addScheduleButton.appendTo( $( 'body' ) );
        }

        setTimeout( () => {
            this.removeAddScheduleButton();
        }, 5000 );
    }

    createNewPopover( reference, popper, callback ) {
        return new Popper( reference, popper, {
            placement: 'left',
            modifiers: {
                flip: { enabled: true },
                preventOverflow: {
                    enabled: true,
                    priority: [ 'left', 'right' ],
                    padding: 20
                }
            },
            onCreate: callback
        });
    }

    render() {
        return (
            <Wrapper>
                <div id="calendarList" className="sl-c-calendar" ref={ ( ref ) => { this.calendar = ref; } }></div>
                {this.state.popover !== null &&
                    <div ref={ ( ref ) => { this.eventPopover = ref; } } className={ this.state.popover.flipped ? 'event-popover-main' : 'event-popover-main event_popover_left' }>
                        <CalendarEventPopOver
                            singleEvent={ this.state.popover }
                            closeEventPopOver={ () => this.closePopover() }
                            DeleteButtonClickHanld={ this.eventDeleted }
                            removeSchedule={ ( id ) => this.props.deleteModal( id ) }
                        />
                    </div>
                }
            </Wrapper>
        );
    }
}
