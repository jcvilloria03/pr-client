/* eslint-disable no-inner-declarations */
import { LOCATION_CHANGE } from 'react-router-redux';
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay } from 'redux-saga';
import get from 'lodash/get';
import axios from 'axios';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import {
    SET_ERRORS,
    SET_SUBMITTED,
    GET_SCHEDULES,
    SET_NOTIFICATION,
    SET_IS_NAME_AVAILABLE,
    SUBMIT_FORM,
    NOTIFICATION,
    CREATE_SCHEDULES,
    UPLOAD_SCHEDULES,
    IS_NAME_AVAILABLE,
    SET_UPLOAD_STATUS,
    SAVE_SCHEDULE,
    SET_TAGS,
    GET_TAGS,
    DEFAULT_HEADERS
} from './constants';

import { setSchedules, setLoading } from './actions';

/**
 * resets the setup store to initial values
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SET_SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {} }),
        ];
        yield call(Fetch, '/schedule', { method: 'POST', data: payload, headers: DEFAULT_HEADERS });
        yield put({
            type: SET_SUBMITTED,
            payload: false,
        });
        yield call(notifyUser, {
            show: true,
            title: 'Success',
            message: 'Record Added successfully',
            type: 'success',
        });
        yield call(browserHistory.push, '/time/schedules', true);
    } catch (error) {
        yield call(notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response
                ? error.response.data.message
                : error.statusText,
            type: 'error',
        });
        yield put({
            type: SET_SUBMITTED,
            payload: false,
        });
    }
}

/**
 * gets schedules data
 */
export function* getSchedules() {
    const companyId = company.getLastActiveCompanyId();

    try {
        const schedules = yield call(
            Fetch,
            `/company/${companyId}/schedules?type=schedules`,
            { method: 'GET', headers: DEFAULT_HEADERS },
        );

        yield put(setSchedules(schedules.data));
    } catch (error) {
        yield call(notifyError, error);
    } finally {
        yield put(setLoading(false));
    }
}

/**
 * create schedules
 */
export function* createSchedules({ payload }) {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put(setLoading(true));
        yield put({
            type: SET_IS_NAME_AVAILABLE,
            payload: true,
        });

        const response = yield call(
            Fetch,
            `/company/${companyId}/schedule/is_name_available`,
            { method: 'POST', data: payload, headers: DEFAULT_HEADERS },
        );

        if (response.available) {
            yield call(Fetch, '/schedule', {
                method: 'POST',
                data: {
                    company_id: companyId,
                    ...payload,
                },
                headers: DEFAULT_HEADERS
            });

            yield call(notifyUser, {
                show: true,
                title: 'Success',
                message: 'Record Added successfully',
                type: 'success',
            });

            yield put(setLoading(false));

            yield put({
                type: SET_IS_NAME_AVAILABLE,
                payload: true,
            });
            yield call(browserHistory.push, '/time/schedules', true);
        } else {
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: 'Schedule name already exists',
                type: 'error'
            });
            yield put(setLoading(false));

            yield put({
                type: SET_IS_NAME_AVAILABLE,
                payload: false,
            });
        }
    } catch (error) {
        yield call(notifyError, error);
    } finally {
        yield put(setLoading(false));
    }
}

/**
 * resets the setup store to initial values
 */
export function* isNameAvaliableForm({ payload }) {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield [
            put({ type: SET_IS_NAME_AVAILABLE, payload: true }),
            put({ type: SET_ERRORS, payload: {} }),
        ];

        yield call(Fetch, `/company/${companyId}/schedule/is_name_available`, {
            method: 'POST',
            data: payload,
            headers: DEFAULT_HEADERS
        });

        yield put({
            type: SET_IS_NAME_AVAILABLE,
            payload: false,
        });
    } catch (error) {
        yield call(notifyError, error);
    } finally {
        yield put({
            type: SET_IS_NAME_AVAILABLE,
            payload: false,
        });
    }
}

/**
 * upload schedules
 * @param {Object} payload
 * @param {Object} payload.file
 */
export function* uploadSchedules({ payload }) {
    const companyId = company.getLastActiveCompanyId();
    let jobID = null;

    axios.defaults.headers.common['X-Authz-Company-Id'] = companyId;

    try {
        yield put({
            type: SET_ERRORS,
            payload: {},
        });

        const formData = new FormData();
        formData.append('file', payload.file);
        formData.append('company_id', companyId);
        formData.append('type', 'create');

        /**
         * upload schedule
         */
        function* upload() {
            const response = yield call(Fetch, '/schedule/upload', {
                method: 'POST',
                data: formData,
                headers: DEFAULT_HEADERS
            });

            if (response) {
                jobID = response.id;
                yield call(fetchJobStatus, response.id);
            }
        }

        /**
         * Get the calculation status
         */
        function* fetchJobStatus(jobId) {
            const response = yield call(
                Fetch,
                `/schedule/upload/status?company_id=${companyId}&job_id=${jobId}&step=validation`,
                { method: 'GET', headers: DEFAULT_HEADERS },
            );

            if (response.status === 'validated') {
                const preview = yield call(
                    Fetch,
                    `/schedule/upload/preview?company_id=${companyId}&job_id=${jobId}`,
                    { method: 'GET', headers: DEFAULT_HEADERS },
                );

                yield put(setSchedules(preview));

                yield put({
                    type: SET_UPLOAD_STATUS,
                    payload: {
                        success: true,
                        jobId,
                        companyId,
                        error: false,
                    },
                });
            } else if (response.status === 'validation_failed') {
                yield put({
                    type: SET_UPLOAD_STATUS,
                    payload: {
                        success: false,
                        jobId,
                        companyId,
                        error: true,
                    },
                });

                yield put({
                    type: SET_UPLOAD_STATUS,
                    payload: {
                        success: false,
                        jobId,
                        companyId,
                        error: true,
                    },
                });

                yield put({
                    type: SET_ERRORS,
                    payload: response.errors,
                });
            } else {
                yield call(delay, 1000);
                yield call(fetchJobStatus, jobId);
            }

            return true;
        }

        if (!jobID) yield call(upload);
    } catch (error) {
        yield call(notifyError, error);
    } finally {
        yield put({
            type: SET_SUBMITTED,
            payload: false,
        });
    }
}

export function* saveSchedule({ payload }) {
    const { companyId, jobId } = payload;
    yield call(
        Fetch,
        '/schedule/upload/save',
        { method: 'POST', data: { company_id: companyId, job_id: jobId }, headers: DEFAULT_HEADERS },
    );
}

/**
 * changes store with errors from API
 * @param errors
 */
export function* setErrors(errors) {
    yield put({
        type: SET_ERRORS,
        payload: errors,
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError(error) {
    const payload = {
        show: true,
        title: get(error, 'response.statusText', 'Error'),
        message: get(error, 'response.data.message', error.statusText),
        type: 'error',
    };

    yield call(notifyUser, payload);
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser(payload) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error',
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification,
    });

    yield put({
        type: SET_NOTIFICATION,
        payload,
    });

    yield call(delay, 5000);

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification,
    });
}

/**
 * Individual exports for testing
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery(SUBMIT_FORM, submitForm);

    yield take(LOCATION_CHANGE);
    yield cancel(watcher);
}

/**
 * WATCHER FOR GET_SCHEDULES
 */
export function* watchForSchedules() {
    const watcher = yield takeEvery(GET_SCHEDULES, getSchedules);

    yield take(LOCATION_CHANGE);
    yield cancel(watcher);
}

/**
 * Individual exports for isName available testing
 */
export function* watchForIsNameAvailableForm() {
    const watcher = yield takeEvery(IS_NAME_AVAILABLE, isNameAvaliableForm);

    yield take(LOCATION_CHANGE);
    yield cancel(watcher);
}

/**
 * Watcher for UPLOAD_SCHEDULES
 */
export function* watchForUploadSchedules() {
    const watcher = yield takeEvery(UPLOAD_SCHEDULES, uploadSchedules);

    yield take(LOCATION_CHANGE);
    yield cancel(watcher);
}

/**
 * watcher for CREATE_SCHEDULES
 */
export function* watchForCreateSchedules() {
    const watcher = yield takeEvery(CREATE_SCHEDULES, createSchedules);

    yield take(LOCATION_CHANGE);
    yield cancel(watcher);
}

export function* watchForSaveSchedule() {
    const watcher = yield takeEvery(SAVE_SCHEDULE, saveSchedule);

    yield take(LOCATION_CHANGE);
    yield cancel(watcher);
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery(NOTIFICATION, notifyUser);

    yield take(LOCATION_CHANGE);
    yield cancel(watcher);
}

export function* getTags() {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put(setLoading(true));
        const [
            tags,
        ] = yield [
            call(Fetch, `/company/${companyId}/tags`, { method: 'GET', headers: DEFAULT_HEADERS }),
        ];

        yield put({
            type: SET_TAGS,
            payload: tags.data,
        });
    } catch (error) {
        yield call(notifyError, error);
    } finally {
        yield put(setLoading(false));
    }
}

export function* watchForGetTags() {
    const watcher = yield takeEvery(GET_TAGS, getTags);

    yield take(LOCATION_CHANGE);
    yield cancel(watcher);
}

// All sagas to be loaded
export default [
    watchNotify,
    watchForSchedules,
    watchForSubmitForm,
    watchForUploadSchedules,
    watchForCreateSchedules,
    watchForIsNameAvailableForm,
    watchForSaveSchedule,
    watchForGetTags,
];
