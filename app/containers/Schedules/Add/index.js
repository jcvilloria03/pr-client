import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from 'components/A';
import Icon from 'components/Icon';
import Toggle from 'components/Toggle';
import SnackBar from 'components/SnackBar';
import Button from 'components/Button';
import { H2, H3 } from 'components/Typography';

import { browserHistory } from 'utils/BrowserHistory';

import * as schedulerAddActions from './actions';
import { makeSelectLoading, makeSelectNotification, makeSelectTags } from './selectors';

import {
    MainWrapper,
    NavWrapper,
    StyledContainer,
    StyledH3,
    LoadingStyles,
    Description,
    StyledFooter,
} from './styles';

import { TOGGLE_OPTIONS } from './constants';

import BatchAdd from './BatchAdd';
import ManualEntry from './ManualEntry';

export class AddSchedule extends React.Component {
    static propTypes = {
        loading: React.PropTypes.bool,
        getSchedules: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string,
        }),
        tags: React.PropTypes.array,
        getTags: React.PropTypes.func,
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedToggleOption: TOGGLE_OPTIONS.BATCH,
            footerButtons: {
                submit: {
                    onClick: () => {},
                    label: 'Submit',
                    disabled: true,
                    type: 'action',
                    size: 'large',
                    visible: false,
                },
                cancel: {
                    onClick: () => {},
                    label: 'Cancel',
                    disabled: true,
                    type: 'action',
                    size: 'large',
                    className: '',
                    visible: false,
                },
            },
        };
    }

    componentDidMount() {
        this.props.getSchedules();
        this.props.getTags();
    }

    getToggleOptions = () => [
        {
            title: 'Batch Upload',
            subtext: 'Upload multiple schedules at once using the template.',
            value: TOGGLE_OPTIONS.BATCH,
            icon: <Icon name="batchUpload" />,
        },
        {
            title: 'Manual Entry',
            subtext: 'Create one schedule at a time.',
            value: TOGGLE_OPTIONS.MANUAL,
            icon: <Icon name="manualEntry" />,
        },
    ];

    handleFooterButtonsUpdate = (options) => {
        this.setState({
            footerButtons: options,
        });
    };

    render() {
        const { selectedToggleOption, footerButtons } = this.state;
        const { notification, loading, tags } = this.props;

        return (
            <div>
                <Helmet
                    title={
                        selectedToggleOption === TOGGLE_OPTIONS.BATCH
                            ? 'Add Schedules'
                            : 'Add Schedule'
                    }
                    meta={ [
                        {
                            name: 'description',
                            content: 'Description of Add Schedule',
                        },
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    show={ notification.show }
                    type={ notification.type }
                />
                <div className="loader" style={ { display: `${loading ? 'block' : 'none'}` } }>
                    <LoadingStyles>
                        <H2>Loading Add Attendance</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoadingStyles>
                </div>
                <MainWrapper style={ { display: `${loading ? 'none' : 'block'}` } }>
                    <NavWrapper>
                        <Container>
                            <A
                                className="back-btn"
                                href
                                onClick={ (e) => {
                                    e.preventDefault();
                                    browserHistory.push(
                                        '/time/schedules',
                                        true,
                                    );
                                } }
                            >
                                <span>&#8592;</span> Back to Schedules
                            </A>
                        </Container>
                    </NavWrapper>
                    <StyledContainer>
                        <div>
                            <StyledH3>
                                {selectedToggleOption ===
                                TOGGLE_OPTIONS.BATCH
                                    ? 'Add Schedules'
                                    : 'Add Schedule'}
                            </StyledH3>
                            <Description>
                                Create schedules and set up their validity
                                period, duration, and recurrence. You can
                                add schedules in batches or set them up one
                                by one.
                            </Description>
                        </div>
                        <Toggle
                            options={ this.getToggleOptions() }
                            defaultSelected={ TOGGLE_OPTIONS.BATCH }
                            onChange={ (value) => {
                                this.setState({
                                    selectedToggleOption: value,
                                });
                            } }
                        />
                        {selectedToggleOption === TOGGLE_OPTIONS.BATCH ? (
                            <BatchAdd
                                handleFooterButtonsUpdate={
                                    this.handleFooterButtonsUpdate
                                }
                            />
                        ) : (
                            <ManualEntry
                                handleFooterButtonsUpdate={
                                    this.handleFooterButtonsUpdate
                                }
                                tags={ tags }
                            />
                        )}
                    </StyledContainer>
                </MainWrapper>

                { !loading && (footerButtons.cancel.visible ||
                    footerButtons.submit.visible) && (
                        <StyledFooter className="footer">
                            <div className="container">
                                {footerButtons.cancel.visible && (
                                    <Button { ...footerButtons.cancel } className="footer-cancel-button" />
                                )}
                                {footerButtons.submit.visible && (
                                    <Button { ...footerButtons.submit } />
                                )}
                            </div>
                        </StyledFooter>
                    )}
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    tags: makeSelectTags(),
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps(dispatch) {
    return bindActionCreators(schedulerAddActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AddSchedule);
