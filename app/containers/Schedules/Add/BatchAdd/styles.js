import styled, { keyframes } from 'styled-components';

const anim3 = keyframes`
    to {
        transform: rotate(45deg) translate(3px, 3px);
    }
`;

const StyledLoader = styled.div`
    font-size: 16px;
    font-weight: 600;
    color: #00a5e5;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;

    span {
        color: white;
    }

    .anim3 {
        padding-left: 10px;
        &:before {
            display: block;
            width: 12px;
            height: 12px;
            margin-top: -1px;
            border: 2px solid #00a5e5;
            content: "";
            animation: ${anim3} 0.5s ease infinite alternate;
            border-radius: 100% 100% 0 100%;
            transform: rotate(45deg);
        }

        &:after {
            display: block;
            width: 12px;
            height: 3px;
            margin-top: 8px;
            background-color: rgba(0, 0, 0, 0.2);
            content: "";
            border-radius: 100%;
        }
    }
`;

const PageWrapper = styled.div`
    padding-top: 20px;

    .template,
    .upload {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        text-align: center;
        padding: 2rem;
        border: 1px dashed #474747;
        border-radius: 12px;
        width: 100%;
        height: 100%;

        > div {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;

            > h4 {
                font-weight: 600;
            }

            > p {
                margin: 0;
            }

            > a {
                background-color: #fff;
                color: #333;
                border: 1px solid #4aba4a;
                padding: 8px 18px;
                border-radius: 30px;
                display: inline;
                vertical-align: middle;
                margin: 0 auto;
                max-width: 200px;
                font-size: 14px;

                &:hover,
                &:focus,
                &:active:focus {
                    outline: none;
                    background-color: #fff;
                    color: #4aba4a;
                }
            }
        }
    }

    .template {
        div:nth-child(2) {
            margin-bottom: 20px;
        }
    }

    .upload {
        div:nth-child(2) p {
            margin-bottom: 20px;
        }

        div.uploaded {
            &:nth-child(2) p {
                margin-bottom: 0;
            }
        }

        button {
            &:hover {
                color: #4aba4a;
            }
        }
    }

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;

        button {
            min-width: 120px;
        }
    }

    .uploaded {
        position: relative;
        top: -5px;
        border: none !important;
        width: auto !important;

        > p {
            display: none;
        }

        .label {
            border: none;
            padding: 0;

            .content {
                width: 100%;
                font-size: 14px;
                text-align: left;
                font-weight: 900;
            }
        }

        .clicker {
            border: none;
            background: transparent !important;

            .content {
                font-size: 14px;
                font-weight: 400;
                color: #00a5e5;
            }
        }
    }

    .selecting {
        height: auto !important;
        width: auto !important;
        border: none !important;

        .label {
            display: none;
        }

        .clicker {
            border: 1px solid #4aba4a;
            font-size: 14px;
            color: #333;
            background: transparent !important;
            font-weight: normal;
            border-radius: 30px;
            text-transform: capitalize;
            padding: 8px 24px;
            transition: all 0.3s ease;

            &:hover {
                color: #4aba4a;
            }
        }
    }

    .footer {
        padding: 15px 0px;
        background: #f0f4f6;

        .container {
            padding: 0 10px;
            display: flex;
            align-items: center;
            justify-content: flex-end;
        }
    }
`;

const FormWrapper = styled.div`
    > div {
        & > h4 {
            text-align: left;
        }

        & > button {
            padding: 12px 40px;
            margin-top: 2rem;
        }

        .steps {
            display: flex;
            align-items: stretch;
            justify-content: space-between;
            margin: 1rem 0;

            > .step {
                width: 49%;
            }
        }

        .errors {
            margin-bottom: 5rem;
            .rt-thead .rt-tr {
                background: #eb7575;
                color: #fff;
            }
            .ReactTable .rt-th,
            .ReactTable .rt-td {
                flex: 1 0 0px;
                white-space: initial;
                text-overflow: ellipsis;
                padding: 7px 20px;
                overflow: hidden;
                transition: 0.3s ease;
                transition-property: width, min-width, padding, opacity;
            }

            .react-bs-container-body tr {
                background: rgba(249, 210, 210, 0.8);

                &:hover {
                    background: rgba(249, 210, 210, 1);
                }
            }
        }
    }

    .ReactTable .rt-tbody .rt-td {
        padding: 21px 20px;
        font-size: 14px;
    }
`;

export { StyledLoader, PageWrapper, FormWrapper };
