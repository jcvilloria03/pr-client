/* eslint-disable no-plusplus */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import A from 'components/A';
import Table from 'components/Table';
import Button from 'components/Button';
import FileInput from 'components/FileInput';
import { H4, P } from 'components/Typography';
import { Spinner } from 'components/Spinner';
import Modal from 'components/Modal/index';
import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';
import { formatPaginationLabel } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';

import * as actions from '../actions';

import {
    makeSelectSchedules,
    makeSelectErrors,
    makeSelectUploadStatus,
} from '../selectors';

import { StyledLoader, PageWrapper, FormWrapper } from './styles';

/**
 * Annual Earnings Batch Upload Component
 */

class BatchAddSchedules extends React.Component {
    static propTypes = {
        schedules: React.PropTypes.array,
        getSchedules: React.PropTypes.func,
        uploadSchedules: React.PropTypes.func,
        errors: React.PropTypes.object,
        handleFooterButtonsUpdate: React.PropTypes.func,
        uploadStatus: React.PropTypes.object,
        saveSchedule: React.PropTypes.func,
    };

    constructor(props) {
        super(props);

        this.state = {
            file: null,
            status: null,
            errors: {},
            hasErrors: false,
            clipboard: '',
            submit: false,
            label: 'Showing 0-0 of 0 entries',
            errorsTableLabel: 'Showing 0-0 of 0 entries',
            savingStatus: '',
            savingErrors: {},
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0,
            },
            modal: {
                isOpen: false,
                title: 'Confirm Your Action',
                body: 'Do you wish to replace the existing records with this file?',
                confirmButtonLabel: 'Yes',
                confirmButtonType: 'action',
                cancelButtonLabel: 'No',
                cancelButtonType: 'grey',
                onConfirm: () => {
                    browserHistory.push('/time/schedules', true);
                },
                onCancel: () => {},
            },
            showCancelButton: false,
            isLoading: false,
        };

        this.onPageChange = this.onPageChange.bind(this);
        this.onPageSizeChange = this.onPageSizeChange.bind(this);
    }

    componentDidMount() {
        this.props.getSchedules();
        this.onFooterButtonsUpdate({
            cancel: {
                onClick: () => {},
                label: 'Cancel',
                disabled: true,
                type: 'action',
                size: 'large',
                className: '',
                visible: false,
            },
            submit: {
                onClick: () => {},
                label: 'Submit',
                type: 'action',
                size: 'large',
                className: '',
                disabled: true,
                visible: true,
            },
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.schedules) {
            this.setState(
                {
                    pagination: {
                        total: Object.keys(nextProps.schedules).length,
                        per_page: 10,
                        current_page: 1,
                        last_page: Math.ceil(
                            Object.keys(nextProps.schedules).length / 10,
                        ),
                        to: 0,
                    },
                },
                () => {
                    this.handleTableChanges();
                },
            );
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.errors !== prevProps.errors && Object.keys(this.props.errors).length > 0) {
            this.setState({
                submit: false,
                hasErrors: true,
                errors: this.props.errors,
            });
        }
        if (prevState.modal.isOpen !== this.state.modal.isOpen) {
            this.modal.toggle();
        }

        if (prevProps.schedules !== this.props.schedules) {
            this.setState({
                isLoading: false,
            });
        }

        if (prevProps.uploadStatus !== this.props.uploadStatus && this.props.uploadStatus.success) {
            this.onFooterButtonsUpdate({
                cancel: {
                    onClick: () => {
                        this.setState({
                            showCancelButton: true,
                            modal: {
                                isOpen: true,
                                title: 'Confirm Your Action',
                                body: 'Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?',
                                confirmButtonLabel: 'Discard',
                                confirmButtonType: 'darkRed',
                                cancelButtonLabel: 'Stay on this page',
                                cancelButtonType: 'grey',
                                onConfirm: () => {
                                    this.setState({
                                        modal: {
                                            ...this.state.modal,
                                            isOpen: false,
                                        },
                                    }, () => {
                                        browserHistory.push(
                                            '/time/schedules',
                                            true,
                                        );
                                    });
                                },
                                onCancel: () => {
                                    this.setState({
                                        modal: {
                                            ...this.state.modal,
                                            isOpen: false,
                                        },
                                    });
                                },
                            },
                        });
                    },
                    label: 'Cancel',
                    disabled: false,
                    type: 'grey',
                    size: 'large',
                    className: '',
                    visible: true,
                },
                submit: {
                    onClick: async () => {
                        const { jobId, companyId } = this.props.uploadStatus;
                        if (jobId && companyId) {
                            await this.handleSubmitSchedule(jobId, companyId);
                        }
                    },
                    label: 'Submit',
                    type: 'action',
                    size: 'large',
                    className: '',
                    disabled: this.props.uploadStatus.error,
                    visible: true,
                },
            });
        }

        if (prevProps.schedules !== this.props.schedules && this.state.showRecordSuccessModal) {
            if (this.props.uploadStatus.success) {
                this.setState({
                    modal: {
                        isOpen: true,
                        title: 'Success',
                        body: 'Successfully imported template file.',
                        confirmButtonLabel:
                            'OK',
                        confirmButtonType:
                            'action',
                        onConfirm: () => {
                            this.setState({
                                modal: {
                                    ...this
                                        .state
                                        .modal,
                                    isOpen: false,

                                },
                                showRecordSuccessModal: false,
                            });
                        },
                    },
                    showCancelButton: false,
                    submit: false,
                    hasErrors: false,
                    errors: {},
                });
            }

            if (prevProps.uploadStatus.error !== this.props.uploadStatus.error && this.props.uploadStatus.error) {
                this.setState({
                    modal: {
                        isOpen: true,
                        title: 'Error',
                        body: <span>
                            Please reupload a template file with valid data. You may refer to the template guidelines <a href="/guides/schedules/batch-upload">here</a>.
                        </span>,
                        confirmButtonLabel:
                            'OK',
                        confirmButtonType:
                            'darkRed',
                        onConfirm: () => {
                            this.setState({
                                modal: {
                                    ...this
                                        .state
                                        .modal,
                                    isOpen: false,

                                },
                                showRecordSuccessModal: false,
                            });
                        },
                    },
                    showCancelButton: false,
                });

                this.onFooterButtonsUpdate({
                    cancel: {
                        onClick: () => {
                            this.setState({
                                showCancelButton: true,
                                modal: {
                                    isOpen: true,
                                    title: 'Confirm Your Action',
                                    body: 'Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?',
                                    confirmButtonLabel: 'Discard',
                                    confirmButtonType: 'danger',
                                    cancelButtonLabel: 'Stay on this page',
                                    cancelButtonType: 'grey',
                                    onConfirm: () => {
                                        this.setState({
                                            modal: {
                                                ...this.state.modal,
                                                isOpen: false,
                                            },
                                        }, () => {
                                            browserHistory.push(
                                                '/time/schedules',
                                                true,
                                            );
                                        });
                                    },
                                    onCancel: () => {
                                        this.setState({
                                            modal: {
                                                ...this.state.modal,
                                                isOpen: false,
                                            },
                                        });
                                    },
                                },
                            });
                        },
                        label: 'Cancel',
                        disabled: false,
                        type: 'grey',
                        size: 'large',
                        className: '',
                        visible: true,
                    },
                    submit: {
                        onClick: async () => {
                            const { jobId, companyId } = this.props.uploadStatus;
                            if (jobId && companyId) {
                                await this.props.saveSchedule({ jobId, companyId });
                                await this.props.getSchedules();
                            }
                        },
                        label: 'Submit',
                        type: 'action',
                        size: 'large',
                        className: '',
                        disabled: true,
                        visible: true,
                    },
                });
            }
        }
    }

    onFooterButtonsUpdate = (update) => {
        this.props.handleFooterButtonsUpdate(update);
    };

    onPageSizeChange = (pageSize) => {
        this.setState(
            (prevState) => ({
                pagination: {
                    ...prevState.pagination,
                    per_page: pageSize,
                    current_page: 1,
                    last_page: Math.ceil(prevState.pagination.total / pageSize),
                },
            }),
            () => {
                Object.assign(
                    this[this.state.hasErrors ? 'errorsTable' : 'scheduleTable']
                        .tableComponent.state,
                    {
                        page: 0,
                        pageSize,
                    },
                );
                this.handleTableChanges();
            },
        );
    };

    onPageChange = (page) => {
        this.setState(
            (prevState) => ({
                page,
                pagination: {
                    ...prevState.pagination,
                    current_page: page,
                },
            }),
            () => {
                Object.assign(
                    this[this.state.hasErrors ? 'errorsTable' : 'scheduleTable']
                        .tableComponent.state,
                    {
                        page: page - 1,
                    },
                );
                this.handleTableChanges();
            },
        );
    };

    handleTableChanges = (
        tableProps = this[
            this.state.hasErrors ? 'errorsTable' : 'scheduleTable'
        ].tableComponent.state,
    ) => {
        Object.assign(tableProps, {
            dataLength: Object.keys(this.state.errors).length,
        });
        this.setState({
            label: formatPaginationLabel(tableProps),
        });
    };

    handleUploadFile = () => {
        const uploadFile = () => {
            this.setState({
                errors: {},
                status: null,
                submit: true,
                showRecordSuccessModal: true,
                modal: {
                    ...this
                        .state
                        .modal,
                    isOpen: false,
                },
            });
            if (this.validateButton) {
                this.validateButton.setState({ disabled: true });
            }

            const file = this.state.file;
            this.props.uploadSchedules({ file });
        };

        if (this.props.uploadStatus.success) {
            this.setState({
                showCancelButton: true,
                modal: {
                    isOpen: true,
                    title: 'Confirm Your Action',
                    body: 'Do you wish to replace the existing records with this file?',
                    confirmButtonLabel:
                        'Yes',
                    confirmButtonType:
                        'action',
                    cancelButtonLabel:
                        'No',
                    cancelButtonType:
                        'grey',
                    onConfirm: () => {
                        uploadFile();
                    },
                    onCancel: () => {
                        this.setState({
                            modal: {
                                ...this
                                    .state
                                    .modal,
                                isOpen: false,
                                showRecordSuccessModal: true,
                            },
                        });
                    },
                },
            });
        } else {
            uploadFile();
        }
    }

    handleSubmitSchedule = async (jobId, companyId) => {
        this.setState({
            isLoading: true,
            file: null,
            status: null,
            submit: false,
        });

        await this.props.saveSchedule({
            jobId,
            companyId,
        });
        await this.props.getSchedules();
    };

    renderErrorsSection = () => {
        const errorDisplay = [];
        const errorList = this.state.errors;

        if (Object.keys(errorList).length) {
            const columns = [
                {
                    header: 'Row Number',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false,
                },
                {
                    header: 'Error Type',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false,
                },
            ];

            Object.keys(errorList).forEach((key) => {
                errorDisplay.push({
                    row: (
                        <H4
                            style={ {
                                margin: '0',
                                marginLeft: '1rem',
                                textAlign: 'center',
                            } }
                        >
                            Row {key}
                        </H4>
                    ),
                    error: (
                        <ul>
                            {Object.values(errorList[key]).map((value) => (
                                <li key={ value } style={ { margin: '0' } }>
                                    {value}
                                </li>
                            ))}
                        </ul>
                    ),
                });
            });

            return (
                <div className="errors">
                    <H4>Data field errors</H4>
                    <p>
                        There seems to be an error in the file you uploaded.
                        Review the list below, correct the errors and upload the
                        file again.
                    </p>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        ref={ (ref) => {
                            this.errorsTable = ref;
                        } }
                        onDataChange={ this.handleTableChanges }
                        page={ this.state.pagination.current_page - 1 }
                        pageSize={ this.state.pagination.per_page }
                        pages={ this.state.pagination.total }
                        external
                    />
                </div>
            );
        }

        return null;
    };

    renderTableSection = () => {
        const { pagination } = this.state;

        const { schedules } = this.props;

        const tableColumns = [
            {
                id: 'name',
                header: 'Schedule Name ',
                sortable: true,
                render: ({ row }) => <div>{row.name}</div>,
            },
            {
                id: 'type',
                header: 'Schedule Type',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.type.charAt(0).toUpperCase() + row.type.slice(1)}
                    </div>
                ),
            },
            {
                id: 'start_time',
                header: 'Start Time',
                sortable: true,
                render: ({ row }) => <div>{row.start_time}</div>,
            },
            {
                id: 'break_start_time',
                header: 'Break Start Time',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.breaks &&
                            row.breaks.map((val) => val.start).join(',')}
                    </div>
                ),
            },
            {
                id: 'break_end_time',
                header: 'Break End Time',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.breaks &&
                            row.breaks.map((val) => val.end).join(',')}
                    </div>
                ),
            },
            {
                id: 'end_time',
                header: 'End Time',
                sortable: true,
                render: ({ row }) => <div>{row.end_time}</div>,
            },
            {
                id: 'entitled_employees',
                header: 'Entitled Employees',
                sortable: true,
                render: ({ row }) => (
                    <div>
                        {row.affected_employees &&
                            row.affected_employees.map((val) => val.name)}
                    </div>
                ),
            },
        ];

        return (
            <Table
                data={ schedules }
                columns={ tableColumns }
                page={ pagination.current_page - 1 }
                pageSize={ pagination.per_page }
                pages={ pagination.total }
                onDataChange={ this.handleTableChanges }
                ref={ (ref) => {
                    this.scheduleTable = ref;
                } }
                external
            />
        );
    };

    render() {
        const { modal, showCancelButton, isLoading } = this.state;

        return (
            <PageWrapper>
                { isLoading && <Spinner /> }

                <FormWrapper>
                    <Container className="sl-u-gap-bottom--xlg">
                        <div className="steps">
                            <div className="step">
                                <div className="template">
                                    <div>
                                        <H4>Step 1:</H4>
                                    </div>
                                    <div>
                                        <P>
                                            A batch upload template is available
                                            for you to download and fill out.
                                        </P>
                                        <P>
                                            <A
                                                target="_blank"
                                                href="/guides/schedules/batch-upload"
                                            >
                                                You may click here to view the
                                                upload guide.
                                            </A>
                                        </P>
                                    </div>
                                    <div>
                                        <A
                                            className="sl-c-btn--wide"
                                            href="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/add-schedules-template-2.csv"
                                            download
                                        >
                                            Download Template
                                        </A>
                                    </div>
                                </div>
                            </div>
                            <div className="step">
                                <div className="upload">
                                    <div>
                                        <H4>Step 2:</H4>
                                    </div>
                                    <div
                                        className={
                                            this.state.file && 'uploaded'
                                        }
                                    >
                                        <P>
                                            After completely filling out the
                                            template, choose and upload it here.
                                        </P>
                                        <div
                                            style={ {
                                                display:
                                                    this.state.status ===
                                                        'validating' ||
                                                    this.state.status ===
                                                        'validation_queued' ||
                                                    this.state.savingStatus ===
                                                        'success'
                                                        ? 'none'
                                                        : 'block',
                                            } }
                                        >
                                            <FileInput
                                                label={
                                                    this.state.file
                                                        ? 'Replace'
                                                        : 'Choose file'
                                                }
                                                className={
                                                    this.state.file
                                                        ? 'uploaded'
                                                        : 'selecting'
                                                }
                                                accept=".csv"
                                                onDrop={ (file) => {
                                                    const { acceptedFiles } =
                                                        file;
                                                    this.setState(
                                                        {
                                                            file:
                                                                acceptedFiles.length >
                                                                0
                                                                    ? acceptedFiles[0]
                                                                    : null,
                                                            errors: {},
                                                            status: null,
                                                        },
                                                        () => {
                                                            this
                                                                .validateButton &&
                                                                this.validateButton.setState(
                                                                    {
                                                                        disabled:
                                                                            acceptedFiles.length <=
                                                                            0,
                                                                    },
                                                                );
                                                        },
                                                    );
                                                } }
                                                ref={ (ref) => {
                                                    this.fileInput = ref;
                                                } }
                                            />
                                        </div>
                                    </div>
                                    <div>
                                        {this.state.file && (
                                            <Button
                                                label={
                                                    this.state.submit ? (
                                                        <StyledLoader className="animation">
                                                            Uploading{' '}
                                                            <div className="anim3"></div>
                                                        </StyledLoader>
                                                    ) : (
                                                        'Upload'
                                                    )
                                                }
                                                type="neutral"
                                                ref={ (ref) => {
                                                    this.validateButton = ref;
                                                } }
                                                onClick={ () => this.handleUploadFile() }
                                            />
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.renderErrorsSection()}
                        {this.renderTableSection()}
                        <FooterTablePaginationV2
                            page={ this.state.pagination.current_page }
                            pageSize={ this.state.pagination.per_page }
                            pagination={ this.state.pagination }
                            onPageChange={ this.onPageChange }
                            onPageSizeChange={ this.onPageSizeChange }
                            paginationLabel={ this.state.label }
                            fluid
                        />
                    </Container>
                </FormWrapper>
                <Modal
                    title={ modal.title }
                    body={ modal.body }
                    buttons={ [
                        ...(showCancelButton ? [{
                            type: modal.cancelButtonType || 'grey',
                            label: modal.cancelButtonLabel || 'No',
                            onClick: () => {
                                modal.onCancel();
                            },
                        }] : []),
                        {
                            type: modal.confirmButtonType || 'action',
                            label: modal.confirmButtonLabel || 'Yes',
                            onClick: () => {
                                modal.onConfirm();
                            },
                        },
                    ] }
                    showClose={ false }
                    ref={ (ref) => {
                        this.modal = ref;
                    } }
                />
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    schedules: makeSelectSchedules(),
    errors: makeSelectErrors(),
    uploadStatus: makeSelectUploadStatus(),
});

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(BatchAddSchedules);
