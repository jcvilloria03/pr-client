import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';

import * as addSchedulesActions from '../actions';

import { MainWrapper } from './styles';

import ScheduleForm from '../../Forms';

import { makeSelectIsNameAvailables } from '../selectors';

class ManualEntry extends React.Component {
    static propTypes = {
        createSchedules: React.PropTypes.func,
        handleFooterButtonsUpdate: React.PropTypes.func,
        footerButtons: React.PropTypes.object,
        tags: React.PropTypes.array,
        isNameAvailable: React.PropTypes.bool,
    };

    render() {
        const { handleFooterButtonsUpdate, footerButtons, createSchedules, tags, isNameAvailable } = this.props;
        return (
            <div>
                <MainWrapper>
                    <ScheduleForm
                        submitForm={ createSchedules }
                        handleFooterButtonsUpdate={ handleFooterButtonsUpdate }
                        footerButtons={ footerButtons }
                        tags={ tags }
                        isNameAvailable={ isNameAvailable }
                    />
                </MainWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    isNameAvailable: makeSelectIsNameAvailables(),
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators(addSchedulesActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ManualEntry);
