import styled from 'styled-components';

export const LoadingStyles = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 200px;
`;

export const MainWrapper = styled.div`
    min-height: 100vh;
    background: #fff;
    margin-top: 2rem;

`;
