/* eslint-disable require-jsdoc */
import {
    SUBMIT_FORM,
    SET_LOADING,
    GET_SCHEDULES,
    SET_SCHEDULES,
    CREATE_SCHEDULES,
    UPLOAD_SCHEDULES,
    IS_NAME_AVAILABLE,
    SAVE_SCHEDULE,
    GET_TAGS,
} from './constants';

/**
 * Submit a new adjustment
 * @param {object} payload
 */
export function submitForm(payload) {
    return {
        type: SUBMIT_FORM,
        payload,
    };
}

/**
 * Submit a isNameAvailable adjustment
 * @param {object} payload
 */
export function isNameAvailableForm(payload) {
    return {
        type: IS_NAME_AVAILABLE,
        payload,
    };
}

/**
 * Upload schedules
 */
export function uploadSchedules(payload) {
    return {
        type: UPLOAD_SCHEDULES,
        payload,
    };
}

/**
 * Sets schedules
 * @param {Object} payload - Schedules
 * @returns {Object}
 */
export function setSchedules(payload) {
    return {
        type: SET_SCHEDULES,
        payload,
    };
}

/**
 * Create schedules
 */
export function createSchedules(payload) {
    return {
        type: CREATE_SCHEDULES,
        payload,
    };
}

/**
 * Gets schedules
 * @param {Object} payload - Schedules
 * @returns {Object}
 */
export function getSchedules(payload) {
    return {
        type: GET_SCHEDULES,
        payload,
    };
}

/**
 * Sets loading
 * @param {Object} payload - Loading
 * @returns {Object}
 */
export function setLoading(payload) {
    return {
        type: SET_LOADING,
        payload,
    };
}

export function saveSchedule(payload) {
    return {
        type: SAVE_SCHEDULE,
        payload,
    };
}

export function getTags() {
    return {
        type: GET_TAGS,
    };
}
