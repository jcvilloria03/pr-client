import { uniqueId } from 'lodash';

import { REPEAT_DAYS } from 'utils/constants';

import { DEFAULT_TIME_OBJECTS, TIME_OBJECT_KEYS } from './constants';
import { validation } from './validation';

export const updatedTimeObject = (id, form, data, actionType, key, reference = {}) => {
    let update = [...form[key]];

    switch (actionType) {
        case 'edit': {
            update = update
                .map((item) => {
                    if (item.id === id) {
                        return {
                            ...item,
                            ...data,
                            editing: true,
                        };
                    }

                    return item;
                })
                .filter((item) => {
                    if (item.type === 'floating' && item.break_hours) {
                        return true;
                    }
                    return Boolean(item.start && item.end);
                });
            break;
        }
        case 'save': {
            const errors = validation(form, key, data);

            update = update.map((item) => {
                if (item.id === id) {
                    if (data.type === 'floating') {
                        return {
                            ...item,
                            ...data,
                            start: null,
                            end: null,
                            errors,
                        };
                    }

                    return {
                        ...item,
                        ...data,
                        errors,
                    };
                }

                return item;
            });

            if (!Object.keys(errors).length) {
                update = [
                    ...update.filter((item) => {
                        if (item.type === 'floating' && item.break_hours) {
                            return true;
                        }
                        return Boolean(item.start && item.end);
                    }),
                    {
                        ...DEFAULT_TIME_OBJECTS[key],
                        id: uniqueId(`${key}_`),
                    },
                ].map((item) => {
                    if (item.id === id) {
                        return {
                            ...item,
                            editing: false,
                        };
                    }

                    return item;
                });
            }
            break;
        }
        case 'cancel': {
            const errors = validation(form, key, data);
            let allowNew = true;
            update = [
                ...update.map((item) => {
                    if (item.id === id) {
                        if (Object.keys(errors).length) {
                            const scheduleKeyMap = {
                                [TIME_OBJECT_KEYS.BREAKTIME]: 'breaks',
                                [TIME_OBJECT_KEYS.OVERTIME]: 'scheduled_overtime',
                                [TIME_OBJECT_KEYS.CORETIME]: 'core_times',
                            };

                            let refObject = {};

                            if (Object.keys(reference).length) {
                                if (key === TIME_OBJECT_KEYS.OVERTIME) {
                                    const formRef = form[key].findIndex((refItem) => refItem.id === id);
                                    refObject = reference[scheduleKeyMap[key]][formRef];
                                } else {
                                    refObject = reference[scheduleKeyMap[key]].find((refItem) => refItem.id === id);
                                }
                            }

                            if (Object.keys(refObject).length) {
                                return {
                                    ...item,
                                    ...refObject,
                                    errors: {},
                                    editing: false,
                                };
                            }

                            allowNew = false;
                            return {
                                ...DEFAULT_TIME_OBJECTS[key],
                                errors: {},
                                editing: true,
                            };
                        }

                        return {
                            ...item,
                            errors: {},
                            editing: false,
                        };
                    }

                    return item;
                }),
            ];

            if (allowNew) {
                update = [ ...update, { ...DEFAULT_TIME_OBJECTS[key], id: uniqueId(`${key}_`) }];
            }

            break;
        }
        default:
            break;
    }

    return update;
};

export const newTimeObject = (id, form, data, key) => {
    let update = [...form[key]];

    const errors = validation(form, key, data);

    update = update.map((item) => {
        if (item.id === id) {
            return {
                ...item,
                ...data,
                errors,
            };
        }

        return item;
    });

    if (!Object.keys(errors).length) {
        update = [
            ...update,
            {
                ...DEFAULT_TIME_OBJECTS[key],
                id: uniqueId(`${key}_`),
            },
        ].map((item) => {
            if (item.id === id) {
                return {
                    ...item,
                    editing: false,
                    saved: true,
                };
            }

            return item;
        });
    }

    return update;
};

export const removedTimeObject = (id, form, key) => {
    let update = [...form[key]];
    update = update.filter((item) => item.id !== id);

    return update;
};

export const mapRepeatDays = (days) => {
    if (days && days.length) {
        const update = REPEAT_DAYS.map((day) => {
            if (days.includes(day.value)) {
                return {
                    ...day,
                    selected: true,
                };
            }

            return {
                ...day,
                selected: false,
            };
        });
        return update;
    }

    return REPEAT_DAYS;
};
