/* eslint-disable */
import React from "react";
import { Container } from "reactstrap";

import _, { uniqueId, isEqual } from "lodash";
import moment from "moment";
import Flatpickr from "react-flatpickr";
import Creatable from "react-select/lib/Creatable";

import Icon from "components/Icon";
import Radio from "components/Radio";
import Input from "components/Input";
import Loader from "components/Loader";
import Switch from "components/Switch2";
import DatePicker from "components/DatePicker";
import RadioGroup from "components/RadioGroup";
import MultiSelect from "components/MultiSelect";
import TimeVisualizer from "components/TimeVisualizer";
import Modal from "components/Modal/index";
import {
    REPEAT_DAYS,
    DATE_FORMATS,
    NEVER_OPTION,
    REPEAT_BY_MONTH,
    REPEAT_WEEKLY_VALUE,
    TIME_METHODS_ALLOWED_VALUES,
    SELECT_ALL_EMPLOYEES_OPTIONS,
    TIME_FORMATS,
} from "utils/constants";
import { Fetch } from "utils/request";
import { company } from "utils/CompanyService";
import { browserHistory } from "utils/BrowserHistory";
import {
    formatDate,
    calculateTimeDifference,
    calculateTotalTime,
} from "utils/functions";

import { ScheduleFormWrapper } from "./styles";
import { validation } from "./validation";

import TimeScheduleForm from "./micro-components/time-methods";
import BreakScheduleForm from "./micro-components/breaks";
import RepeatScheduleForm from "./micro-components/repeat";
import OvertimeScheduleForm from "./micro-components/overtime";
import CoreTimeScheduleForm from "./micro-components/core-time";
import { TIME_OBJECT_KEYS, DEFAULT_TIME_OBJECTS, DEFAULT_HEADERS } from "./constants";
import {
    updatedTimeObject,
    newTimeObject,
    removedTimeObject,
    mapRepeatDays,
} from "./helpers";

export default class ScheduleForm extends React.Component {
    static propTypes = {
        schedule: React.PropTypes.object,
        paramsValue: React.PropTypes.object,
        submitForm: React.PropTypes.func,
        handleFooterButtonsUpdate: React.PropTypes.func,
        tags: React.PropTypes.array,
        schedInUse: React.PropTypes.bool,
        isNameAvailable: React.PropTypes.bool,
    };

    constructor(props) {
        super(props);

        this.state = {
            isNameAvailable: true,
            form: {
                schedule_id: null,
                type: "fixed",
                name: "",
                start_date: null,
                start_time: null,
                end_time: null,
                total_hours: null,
                schedule_repeat: false,
                repeat: this.getDefaultRepeatValues(),
                on_holidays: false,
                on_rest_day: false,
                timeMethodsAllowed: true,
                auto_assign: false,
                affected_employees: [],
                tags: [],
                allowed_time_methods: TIME_METHODS_ALLOWED_VALUES,
                [TIME_OBJECT_KEYS.BREAKTIME]: [
                    {
                        ...DEFAULT_TIME_OBJECTS[TIME_OBJECT_KEYS.BREAKTIME],
                        id: uniqueId(`${TIME_OBJECT_KEYS.BREAKTIME}_`)
                    }
                ],
                [TIME_OBJECT_KEYS.OVERTIME]: [
                    {
                        ...DEFAULT_TIME_OBJECTS[TIME_OBJECT_KEYS.OVERTIME],
                        id: uniqueId(`${TIME_OBJECT_KEYS.OVERTIME}_`)
                    }
                ],
                [TIME_OBJECT_KEYS.CORETIME]: [
                    {
                        ...DEFAULT_TIME_OBJECTS[TIME_OBJECT_KEYS.CORETIME],
                        id: uniqueId(`${TIME_OBJECT_KEYS.CORETIME}_`)
                    }
                ],
            },
            isLoading: false,
            options: [],
            errors: {},
            buttonTextSubmit: false,
            isButtonDisabled: false,
            modal: {
                isOpen: false,
                title: "Confirm Your Action",
                body: "Do you wish to remove this record?",
                confirmButtonLabel: "Yes",
                confirmButtonType: "action",
                cancelButtonLabel: "No",
                cancelButtonType: "grey",
                onConfirm: () => {},
                onCancel: () => {},
            },
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.totalScheduleHours();

        if (this.props.paramsValue) {
            this.form = _.merge(this.form, this.props.paramsValue);
        }

        if (this.props.schedule) {
            this.loadExistingSchedule(this.props.schedule);
        }

        this.onFooterButtonsUpdate({
            cancel: {
                className: "cancel",
                label: "Cancel",
                type: "neutral",
                size: "large",
                onClick: () => {
                    this.handleConfirmAction({
                        title: "Discard Changes",
                        body: "Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?",
                        confirmButtonLabel: "Discard",
                        confirmButtonType: "danger",
                        cancelButtonLabel: "Stay on this page",
                        cancelButtonType: "grey",
                        onConfirm: () => {
                            this.handleCloseModal();
                            browserHistory.push("/time/schedules", true);
                        },
                        onCancel: () => this.handleCloseModal(),
                    });
                },
                visible: true,
            },
            submit: {
                label: this.state.buttonTextSubmit ? (
                    <Loader />
                ) : this.props.schedule && this.props.schedule.id ? (
                    "Update"
                ) : (
                    "Submit"
                ),
                type: "action",
                size: "large",
                onClick: () => this.handleSubmit(),
                disabled:
                    !!this.state.buttonTextSubmit ||
                    this.state.isButtonDisabled,
                visible: true,
            },
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (!isEqual(prevProps.schedule, this.props.schedule)) {
            this.loadExistingSchedule(this.props.schedule);
        }

        if (prevState.modal.isOpen !== this.state.modal.isOpen) {
            this.modal.toggle();
        }

        if (prevProps.isNameAvailable !== this.props.isNameAvailable) {
            this.setState({
                isNameAvailable: this.props.isNameAvailable
            })
        }

        if (
            prevState.buttonTextSubmit !== this.state.buttonTextSubmit ||
            prevState.isButtonDisabled !== this.state.isButtonDisabled ||
            !isEqual(prevProps.schedule, this.props.schedule)
        ) {
            this.onFooterButtonsUpdate({
                cancel: {
                    className: "cancel",
                    label: "Cancel",
                    type: "neutral",
                    size: "large",
                    onClick: () => {
                        this.handleConfirmAction({
                            title: "Discard Changes",
                            body: "Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?",
                            confirmButtonLabel: "Discard",
                            confirmButtonType: "danger",
                            cancelButtonLabel: "Stay on this page",
                            cancelButtonType: "grey",
                            onConfirm: () => {
                                this.handleCloseModal();
                                browserHistory.push("/time/schedules", true);
                            },
                            onCancel: () => this.handleCloseModal(),
                        });
                    },
                    visible: true,
                },
                submit: {
                    label: this.state.buttonTextSubmit ? (
                        <Loader />
                    ) : this.props.schedule && this.props.schedule.id ? (
                        "Update"
                    ) : (
                        "Submit"
                    ),
                    type: "action",
                    size: "large",
                    onClick: () => this.handleSubmit(),
                    disabled:
                        !!this.state.buttonTextSubmit ||
                        this.state.isButtonDisabled,
                    visible: true,
                },
            });
        }
    }

    onFooterButtonsUpdate = (update) => {
        this.props.handleFooterButtonsUpdate(update);
    };

    getDefaultRepeatValues() {
        return {
            typeObject: REPEAT_WEEKLY_VALUE,
            type: REPEAT_WEEKLY_VALUE.value,
            repeat_every: 1,
            endsOption: NEVER_OPTION,
            end_on: null,
            end_after: null,
            end_never: true,
            repeat_on: REPEAT_DAYS,
            repeat_by: REPEAT_BY_MONTH,
        };
    }

    handleConfirmAction(action) {
        const { modal } = this.state;
        this.setState({
            modal: {
                ...modal,
                ...action,
                isOpen: true,
            },
        });
    }

    handleCloseModal() {
        const { modal } = this.state;
        this.setState({
            modal: {
                ...modal,
                isOpen: false,
            },
        });
    }

    handleAddTimeObject(id, data, key) {
        const { form } = this.state;
        const update = newTimeObject(id, form, data, key);
        this.setState({
            form: {
                ...form,
                [key]: [...update],
            },
        });
    }

    handleUpdateTimeObject = (id, data, actionType, key, reference = {}) => {
        const { form } = this.state;
        const { schedule } = this.props;

        const resetRef = Object.keys(reference).length ? reference : schedule;

        const update = updatedTimeObject(id, form, data, actionType, key, resetRef);
        this.setState({
            form: {
                ...form,
                [key]: [...update],
            },
        });
    };

    handleRemoveTimeObject = (id, key) => {
        const { form, modal } = this.state;
        const update = removedTimeObject(id, form, key);

        this.setState({
            form: {
                ...form,
                [key]: [...update],
            },
        });

        if (modal.isOpen) {
            this.handleCloseModal();
        }

        this.handleSubmit({ [key]: [...update] });
    };

    handleRepeatChange = (repeat) => {
        const { form } = this.state;
        const newObject = form.repeat;

        Object.assign(newObject, repeat);

        this.setState({
            form: {
                ...form,
                repeat: newObject,
            },
        });
    };

    handleTimeMethodChange = (index, selected) => {
        const { form } = this.state;
        form.allowed_time_methods[index].selected = !selected;
        this.setState({ form });
    };

    loadExistingSchedule(schedule) {
        const form = {
            schedule_id: schedule.id,
            type: schedule.type,
            name: schedule.name,
            start_date: schedule.start_date,
            start_time: schedule.start_time,
            end_time: schedule.end_time,
            total_hours: schedule.total_hours,
            [TIME_OBJECT_KEYS.BREAKTIME]: [
                ...(schedule.breaks
                    ? schedule.breaks.map((item) => {
                        let update = {};
                        Object.keys(item).forEach((key) => {
                            if (item.type === 'floating' && key !== "start" && key !== "end") {
                                update = {
                                    ...update,
                                    [key]: item[key],
                                };
                            } else {
                                update = {
                                    ...update,
                                    [key]: item[key],
                                };
                            }
                        });

                        return {
                            ...update,
                            id:
                              item.id ||
                              uniqueId(`${TIME_OBJECT_KEYS.BREAKTIME}_`),
                            saved: true,
                            errors: {},
                        };
                    })
                    : []),
                {
                    ...DEFAULT_TIME_OBJECTS[TIME_OBJECT_KEYS.BREAKTIME],
                    id: uniqueId(`${TIME_OBJECT_KEYS.BREAKTIME}_`),
                },
            ],
            [TIME_OBJECT_KEYS.OVERTIME]: [
                ...(schedule.scheduled_overtime
                    ? schedule.scheduled_overtime.map((item) => ({
                        ...item,
                        id:
                              item.id ||
                              uniqueId(`${TIME_OBJECT_KEYS.OVERTIME}_`),
                        saved: true,
                        errors: {},
                    }))
                    : []),
                {
                    ...DEFAULT_TIME_OBJECTS[TIME_OBJECT_KEYS.OVERTIME],
                    id: uniqueId(`${TIME_OBJECT_KEYS.OVERTIME}_`),
                },
            ],
            [TIME_OBJECT_KEYS.CORETIME]: [
                ...(schedule.core_times
                    ? schedule.core_times.map((item) => ({
                        ...item,
                        id:
                              item.id ||
                              uniqueId(`${TIME_OBJECT_KEYS.CORETIME}_`),
                        saved: true,
                        errors: {},
                    }))
                    : []),
                {
                    ...DEFAULT_TIME_OBJECTS[TIME_OBJECT_KEYS.CORETIME],
                    id: uniqueId(`${TIME_OBJECT_KEYS.CORETIME}_`),
                },
            ],
            schedule_repeat: !!schedule.repeat,
            repeat: {
                ...(schedule.repeat ? schedule.repeat : this.getDefaultRepeatValues()),
                repeat_on: schedule.repeat ? mapRepeatDays(schedule.repeat.repeat_on) : REPEAT_DAYS,
            },
            on_holidays: schedule.on_holidays,
            on_rest_day: schedule.on_rest_day,
            timeMethodsAllowed: schedule.allowed_time_methods && schedule.allowed_time_methods.length > 0,
            allowed_time_methods: this.state.form.allowed_time_methods.map(
                (method) => {
                    if (schedule.allowed_time_methods.includes(method.name)) {
                        return {
                            ...method,
                            selected: true,
                        };
                    }

                    return {
                        ...method,
                        selected: false,
                    };
                },
            ),
            auto_assign: schedule.auto_assign,
            affected_employees: [
                ...(schedule.affected_employees
                    ? schedule.affected_employees.map((employee) => ({
                        id: employee.id,
                        value: employee.id,
                        label: employee.name,
                        type: employee.type,
                    }))
                    : []),
            ],
            tags: schedule.tags.map((tag) => ({
                label: tag.name,
                value: tag.id,
            })),
        };

        this.setState({ form: { ...form } });
    }

    totalScheduleHours() {
        const { form } = this.state;
        const { paramsValue } = this.props;

        if (form.start_time && form.end_time) {
            form.total_hours = calculateTimeDifference(
                form.start_time,
                form.end_time,
            );
        }

        if (paramsValue && paramsValue.start_time && paramsValue.end_time) {
            form.total_hours = calculateTimeDifference(
                paramsValue.start_time,
                paramsValue.end_time,
            );
        }

        return null;
    }

    formatDate(date) {
        return moment(date, DATE_FORMATS.SCHEDULES_DISPLAY).format(
            DATE_FORMATS.DEFAULT,
        );
    }

    preparePayload(data) {
        const scheduleForm = { ...data };
        const excluded = [ "uid", "saved", "editing", "errors" ];

        let core_times = [];
        let breaks = [];
        let scheduled_overtime = [];
        if (data.type === "fixed") {
            core_times = [];
        } else {
            breaks = [];
            scheduled_overtime = [];
        }

        if (data[TIME_OBJECT_KEYS.CORETIME]) {
            core_times = data[TIME_OBJECT_KEYS.CORETIME]
                .map((item) => {
                    let update = {};
                    Object.keys(item).forEach((key) => {
                        if (!excluded.includes(key)) {
                            update = {
                                ...update,
                                [key]: item[key],
                            };
                        }
                    });

                    return update;
                })
                .filter((item) => item.start !== null && item.end !== null);
        }

        if (data[TIME_OBJECT_KEYS.BREAKTIME]) {
            breaks = data[TIME_OBJECT_KEYS.BREAKTIME]
                .map((item) => {
                    let update = {};
                    Object.keys(item).forEach((key) => {
                        if (!excluded.includes(key)) {
                            update = {
                                ...update,
                                [key]: item[key],
                            };
                        }

                        if (item.type === 'floating') {
                            update = {
                                ...update,
                                start: null,
                                end: null,
                            };
                        }
                    });

                    return update;
                })
                .filter((item) => item.break_hours !== null);
        }

        if (data[TIME_OBJECT_KEYS.OVERTIME]) {
            scheduled_overtime = data[TIME_OBJECT_KEYS.OVERTIME]
                .map((item) => {
                    let update = {};
                    Object.keys(item).forEach((key) => {
                        if (!excluded.includes(key)) {
                            update = {
                                ...update,
                                [key]: item[key],
                            };
                        }
                    });

                    return update;
                })
                .filter((item) => item.overtime_hours !== null);
        }

        let allowed_time_methods = [];
        if (!data.timeMethodsAllowed) {
            allowed_time_methods = [];
        } else {
            allowed_time_methods = data.allowed_time_methods
                .filter((item) => item.selected === true)
                .map((item) => item.name);
        }

        if (!scheduleForm.total_hours && data.start_time && data.end_time) {
            scheduleForm.total_hours = calculateTimeDifference(
                data.start_time,
                data.end_time,
            );
        }

        if (!data.schedule_id) {
            delete scheduleForm.schedule_id;
        }

        let repeat = { ...scheduleForm.repeat };
        if (data.schedule_repeat) {
            repeat.repeat_on = data.repeat.repeat_on
                .filter((item) => item.selected)
                .map((item) => item.value);
        } else {
            repeat = null;
        }

        let affected_employees = [];
        if (data.affected_employees.length) {
            affected_employees = data.affected_employees.map(
                (employee) => ({
                    id: employee.id,
                    type: employee.type,
                    name: employee.label,
                    uid: `${employee.type}${employee.id}`,
                }),
            );
        }

        let tags = [];
        if (data.tags.length) {
            tags = data.tags.map((tag) => tag.label);
        }

        let payload = {
            core_times,
            breaks,
            scheduled_overtime,
            allowed_time_methods,
            repeat,
            affected_employees,
            tags,
        };
        Object.keys(scheduleForm).forEach((key) => {
            if (!Object.values(TIME_OBJECT_KEYS).includes(key)) {
                payload = {
                    [key]: scheduleForm[key],
                    ...payload,
                };
            }
        });

        return payload;
    }

    handleSubmit(data = {}) {
        const { submitForm } = this.props;
        const { form } = this.state;

        const payload = this.preparePayload({
            ...form,
            ...data,
        });

        const errors = validation(payload, "schedule");

        if (Object.keys(errors).length > 0) {
            this.setState({ errors });
        } else {
            submitForm(payload);
        }
    }

    loadEmployeeList = (keyword, callback) => {
        const companyId = company.getLastActiveCompanyId();
        Fetch(
            `/company/${companyId}/affected_employees/search?term=${keyword}&limit=10`,
            { method: "GET", headers: DEFAULT_HEADERS },
        )
            .then((result) => {
                const list = result.map((option) => ({
                    id: option.id,
                    value: option.id,
                    label: option.name,
                    type: option.type,
                }));

                callback(null, {
                    options: [ ...SELECT_ALL_EMPLOYEES_OPTIONS, ...list ],
                });
            })
            .catch((error) => callback(error, null));
    };
    tagPlaceholder() {
        if (this.state.form.tags.length) {
            return "";
        }

        return "Tag this schedule with previously created tags or you can also create new tags";
    }

    totalBreakHours() {
        const breakHours = this.state.form[TIME_OBJECT_KEYS.BREAKTIME]
            .map((item) => item.break_hours)
            .filter((item) => item);
        return calculateTotalTime(breakHours);
    }

    totalOvertimeHours() {
        const overtimeHours = this.state.form[TIME_OBJECT_KEYS.OVERTIME]
            .map((item) => item.overtime_hours)
            .filter((item) => item);
        return calculateTotalTime(overtimeHours);
    }

    totalRegularHours = () => {
        const { form } = this.state;
        const timeDiff = calculateTimeDifference(
            form.start_time,
            form.end_time,
        );

        const breakHours = this.totalBreakHours();
        const totalBreakHours = moment(breakHours).format(TIME_FORMATS.DEFAULT).split(':');

        const total = moment(timeDiff, TIME_FORMATS.DEFAULT).subtract({
            hours: totalBreakHours[0],
            minutes: totalBreakHours[1],
        });

        return total.format(TIME_FORMATS.DEFAULT);
    }

    render() {
        const { form, errors, modal, isNameAvailable } = this.state;
        const paramsData = this.props.paramsValue;
        let defaultOptions = [];

        // workaround to display active tags in search dropdown
        if (this.props.tags) {
            defaultOptions = this.props.tags
            .map((tag) => {
                let item = {
                    label: tag.name,
                    value: tag.name,
                };

                if (form.tags.find((formTag) => item.label === formTag.label)) {
                    item = {
                        ...item,
                        disabled: true,
                    };
                }

                return item;
            });
        }

        return (
            <div>
                <ScheduleFormWrapper>
                    <Container className="schdule_wrap">
                        <div className="schedule_type mb-1">
                            <p className="schdule_tooltip">
                                * Schedule type
                                <span className="bg_tooltip">
                                    <span>?</span>
                                    <span className="tooltiptext">
                                        Determine whether this schedule is fixed
                                        or flexi.
                                        <br />
                                        A. Fixed - A schedule with definite
                                        start and end time
                                        <br />
                                        B. Flexi - A schedule with no definite
                                        start and end time but with indicated
                                        core hours.
                                    </span>
                                </span>
                            </p>
                            <div className="schedule_radio">
                                <RadioGroup
                                    id="radioAmendmentReturn"
                                    name="type"
                                    value={ form.type }
                                    horizontal
                                    className="radio-group"
                                    onChange={ (value) => {
                                        this.setState({
                                            form: { ...form, type: value },
                                        });
                                    } }
                                    ref={ (ref) => {
                                        this.schedule_type = ref;
                                    } }
                                >
                                    <Radio value="fixed" key="fixed">
                                        Fixed Schedule
                                    </Radio>
                                    <Radio value="flexi" key="flexi">
                                        Flexi Schedule
                                    </Radio>
                                </RadioGroup>
                            </div>
                        </div>
                        <div className="row">
                            <div
                                className={
                                    this.state.isNameSubmitNull ||
                                    !isNameAvailable ||
                                    errors.name
                                        ? "col-md-4 schedule_main has-error"
                                        : "col-md-4 schedule_main"
                                }
                            >
                                <div className="break_title">
                                    * Schedule name
                                </div>
                                <Input
                                    name="name"
                                    id="schedule_name"
                                    value={ form.name }
                                    ref={ (ref) => {
                                        this.schedule_name = ref;
                                    } }
                                    className={
                                        this.state.isNameNull ||
                                        this.state.isNameSubmitNull ||
                                        errors.name
                                            ? "input_pay"
                                            : ""
                                    }
                                    onChange={ (value) => {
                                        this.setState({
                                            isNameAvailable: true,
                                            isNameSubmitNull: value === "",
                                            form: { ...form, name: value },
                                        });
                                    } }
                                    onBlur={ (value) => {
                                        this.setState({
                                            isNameNull: value === "",
                                        });
                                    } }
                                />
                                {this.state.isNameSubmitNull || errors.name ? (
                                    <p className="error">
                                        {errors.name
                                            ? errors.name
                                            : "This field is required"}{" "}
                                    </p>
                                ) : (
                                    ""
                                )}
                                {!isNameAvailable ? (
                                    <p className="error">
                                        Record name already exists
                                    </p>
                                ) : (
                                    ""
                                )}
                            </div>
                            <div
                                className={
                                    this.state.isDateSubmitNull ||
                                    errors.start_date
                                        ? "col-md-4 schedule_main has-error"
                                        : "col-md-4 schedule_main"
                                }
                            >
                                <div className="break_title">
                                    * Schedule validity start date
                                </div>
                                <div className="schedule_timeSect validity_timesect">
                                    <DatePicker
                                        name="start_date"
                                        dayFormat={ DATE_FORMATS.MMDDYYYY }
                                        selectedDay={
                                            formatDate(
                                                paramsData &&
                                                    paramsData.start_date,
                                                DATE_FORMATS.API,
                                            ) ||
                                            formatDate(
                                                form.start_date,
                                                DATE_FORMATS.API,
                                            ) ||
                                            null
                                        }
                                        value={
                                            (paramsData &&
                                                paramsData.start_date) ||
                                            form.start_date ||
                                            null
                                        }
                                        ref={ (ref) => {
                                            this.start_date = ref;
                                        } }
                                        onChange={ (value) => {
                                            this.setState({
                                                isDateSubmitNull:
                                                    value === null,
                                                form: {
                                                    ...form,
                                                    start_date:
                                                        moment(value).format(
                                                            "YYYY-MM-DD",
                                                        ),
                                                },
                                            });
                                        } }
                                        onBlur={ (value) => {
                                            this.setState({
                                                isDateNull: value === null,
                                            });
                                        } }
                                    />
                                    <Icon name="calendar" />
                                    {this.state.isDateSubmitNull ||
                                    errors.start_date ? (
                                        <p className="error">
                                            {errors.start_date ||
                                                "Please choose a date"}
                                        </p>
                                    ) : (
                                        ""
                                    )}
                                </div>
                            </div>
                        </div>
                        <div
                            className="row"
                            style={ { display: "flex", flexWrap: "wrap" } }
                        >
                            <div
                                className={
                                    this.state.isStartTimeSubmitNull ||
                                    errors.start_time
                                        ? "col-md-4 schedule_main input_pay has-error"
                                        : "col-md-4 schedule_main"
                                }
                            >
                                <p className="schdule_tooltip">
                                    * Start time
                                    <span className="bg_tooltip">
                                        <span>?</span>
                                        <span className="tooltiptext tooltiptextStart">
                                            The exact time this schedule starts.
                                            Use the 24-hour hour/military time
                                            format.
                                        </span>
                                    </span>
                                </p>
                                <div className="schedule_timeSect timeIcon">
                                    <Flatpickr
                                        placeholder="Enter the start time in military format"
                                        data-enable-time
                                        name="start_time"
                                        value={ form.start_time }
                                        options={ {
                                            noCalendar: true,
                                            enableTime: true,
                                            time_24hr: true,
                                            allowInput: true,
                                        } }
                                        ref={ (ref) => {
                                            this.start_time = ref;
                                        } }
                                        onClose={ (value) => {
                                            this.setState({
                                                isStartTimeSubmitNull:
                                                    value === null,
                                                form: {
                                                    ...form,
                                                    start_time: moment(
                                                        new Date(value),
                                                    ).format("HH:mm"),
                                                },
                                            });
                                        } }
                                    />
                                    <Icon name="time2" />
                                    {this.state.isStartTimeSubmitNull ||
                                    errors.start_time ? (
                                        <p className="error">
                                            {errors.start_time ||
                                                "Please choose a time"}
                                        </p>
                                    ) : (
                                        ""
                                    )}
                                </div>
                            </div>
                            <div
                                className={
                                    this.state.isEndTimeSubmitNull ||
                                    errors.end_time
                                        ? "col-md-4 schedule_main input_pay has-error"
                                        : "col-md-4 schedule_main"
                                }
                            >
                                <p className="schdule_tooltip">
                                    * End time
                                    <span className="bg_tooltip">
                                        <span>?</span>
                                        <span className="tooltiptext tooltiptextEnd">
                                            The exact time this schedule ends.
                                            Use the 24-hour hour/military time
                                            format.
                                        </span>
                                    </span>
                                </p>
                                <div className="schedule_timeSect timeIcon">
                                    <Flatpickr
                                        placeholder="Enter the end time in military format"
                                        data-enable-time
                                        name="end_time"
                                        value={
                                            (paramsData &&
                                                paramsData.end_time) ||
                                            form.end_time ||
                                            null
                                        }
                                        options={ {
                                            noCalendar: true,
                                            enableTime: true,
                                            time_24hr: true,
                                            allowInput: true,
                                        } }
                                        ref={ (ref) => {
                                            this.end_time = ref;
                                        } }
                                        onClose={ (value) => {
                                            this.setState({
                                                isEndTimeSubmitNull:
                                                    value === null,
                                                form: {
                                                    ...form,
                                                    end_time: moment(
                                                        new Date(value),
                                                    ).format("HH:mm"),
                                                },
                                            });
                                        } }
                                    />
                                    <Icon name="time2" />
                                    {(this.state.isEndTimeSubmitNull ||
                                        errors.end_time) && (
                                        <p className="error">
                                            {errors.end_time ||
                                                "Please choose a time"}
                                        </p>
                                    )}
                                </div>
                            </div>
                            {form.type === "fixed" && (
                                <div className="col-md-4 schedule_main">
                                    <TimeVisualizer
                                        start={ form.start_time }
                                        end={ form.end_time }
                                    />
                                </div>
                            )}
                            <div className="col-md-4 schedule_main">
                                <p className="schdule_tooltip">
                                    * Total schedule hours
                                    <span className="bg_tooltip">
                                        <span>?</span>
                                        <span className="tooltiptext tooltiptextHours">
                                            Number of hours from this
                                            schedule&apos;s start time to its
                                            end time.
                                        </span>
                                    </span>
                                </p>
                                <div className="schedule_timeSect timeIcon">
                                    <Flatpickr
                                        data-enable-time
                                        name="total_hours"
                                        value={
                                            form.start_time &&
                                            form.end_time &&
                                            form.type === "fixed"
                                                ? calculateTimeDifference(
                                                      form.start_time,
                                                      form.end_time,
                                                  )
                                                : ( form.total_hours || "" )
                                        }
                                        options={ {
                                            noCalendar: true,
                                            enableTime: true,
                                            time_24hr: true,
                                        } }
                                        disabled={ form.type === "fixed" }
                                        ref={ (ref) => {
                                            this.total_hours = ref;
                                        } }
                                        onChange={ (value) => {
                                            this.setState({
                                                form: {
                                                    ...form,
                                                    total_hours: moment(
                                                        new Date(value),
                                                    ).format("HH:mm"),
                                                },
                                            });
                                        } }
                                    />
                                    {form.type === "flexi" && (
                                        <Icon name="time2" />
                                    )}
                                </div>
                            </div>
                        </div>
                        <hr />
                        {form.type === "fixed" && (
                            <div>
                                <BreakScheduleForm
                                    form={ form }
                                    addBreakSchedule={ (id, data) =>
                                        this.handleAddTimeObject(
                                            id,
                                            data,
                                            TIME_OBJECT_KEYS.BREAKTIME,
                                        )
                                    }
                                    editBreakSchedule={ (id, data, actionType, reference) => {
                                        this.handleUpdateTimeObject(
                                                id,
                                                data,
                                                actionType,
                                                TIME_OBJECT_KEYS.BREAKTIME,
                                                reference,
                                            );
                                    } }
                                    removeBreakSchedule={ (id) =>
                                        this.handleConfirmAction({
                                            onConfirm: () =>
                                                this.handleRemoveTimeObject(
                                                    id,
                                                    TIME_OBJECT_KEYS.BREAKTIME,
                                                ),
                                            onCancel: () =>
                                                this.handleCloseModal(),
                                        })
                                    }
                                    break_time={
                                        form[TIME_OBJECT_KEYS.BREAKTIME]
                                    }
                                />
                                <hr />
                                <OvertimeScheduleForm
                                    form={ form }
                                    over_time={ form[TIME_OBJECT_KEYS.OVERTIME] }
                                    addOvertimeSchedule={ (id, data) =>
                                        this.handleAddTimeObject(
                                            id,
                                            data,
                                            TIME_OBJECT_KEYS.OVERTIME,
                                        )
                                    }
                                    editOvertimeSchedule={ (
                                        id,
                                        data,
                                        actionType,
                                        reference = {},
                                    ) =>
                                        this.handleUpdateTimeObject(
                                            id,
                                            data,
                                            actionType,
                                            TIME_OBJECT_KEYS.OVERTIME,
                                            reference,
                                        )
                                    }
                                    removeOvertimeSchedule={ (id) =>
                                        this.handleConfirmAction({
                                            onConfirm: () =>
                                                this.handleRemoveTimeObject(
                                                    id,
                                                    TIME_OBJECT_KEYS.OVERTIME,
                                                ),
                                            onCancel: () =>
                                                this.handleCloseModal(),
                                        })
                                    }
                                />
                            </div>
                        )}
                        {form.type === "flexi" && (
                            <CoreTimeScheduleForm
                                form={ form }
                                core_time={ form[TIME_OBJECT_KEYS.CORETIME] }
                                addCoreTimeSchedule={ (id, data) =>
                                    this.handleAddTimeObject(
                                        id,
                                        data,
                                        TIME_OBJECT_KEYS.CORETIME,
                                    )
                                }
                                removeCoreTimeSchedule={ (id) =>
                                    this.handleConfirmAction({
                                        onConfirm: () =>
                                            this.handleRemoveTimeObject(
                                                id,
                                                TIME_OBJECT_KEYS.CORETIME,
                                            ),
                                        onCancel: () => this.handleCloseModal(),
                                    })
                                }
                                editCoreTimeSchedule={ (id, data, actionType, reference) =>
                                    this.handleUpdateTimeObject(
                                        id,
                                        data,
                                        actionType,
                                        TIME_OBJECT_KEYS.CORETIME,
                                        reference,
                                    )
                                }
                            />
                        )}
                        <hr />
                        {form.type === "fixed" && (
                            <div className="row">
                                <div className="schedule_rows">
                                    <div className="col-md-4 schedule_main">
                                        <p className="schdule_tooltip">
                                            * Total Expected Work Hours
                                            <span className="bg_tooltip">
                                                <span>?</span>
                                                <span className="tooltiptext tooltiptextHours">
                                                    This refers to the total
                                                    number of hours excluding
                                                    break hours and scheduled
                                                    overtime hours.
                                                </span>
                                            </span>
                                        </p>
                                        <div className="schedule_timeSect">
                                            <Input
                                                id="total_regular_hours"
                                                value={
                                                    form.start_time &&
                                                    form.end_time
                                                        ? this.totalRegularHours()
                                                        : ""
                                                }
                                                className="editing"
                                                disabled
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="schedule_rows">
                                    <div className="col-md-4 schedule_main">
                                        <p className="schdule_tooltip">
                                            * Total Break Hours
                                            <span className="bg_tooltip">
                                                <span>?</span>
                                                <span className="tooltiptext tooltiptextHours">
                                                    This refers to the total
                                                    number of break hours from
                                                    start to end.
                                                </span>
                                            </span>
                                        </p>
                                        <div className="schedule_timeSect">
                                            <Input
                                                id="total_break_hours"
                                                className="editing"
                                                value={ moment(
                                                    this.totalBreakHours(),
                                                ).format("HH:mm") }
                                                disabled
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="schedule_rows">
                                    <div className="col-md-4 schedule_main">
                                        <p className="schdule_tooltip">
                                            * Total Scheduled Overtime Hours
                                            <span className="bg_tooltip">
                                                <span>?</span>
                                                <span className="tooltiptext tooltiptextHours">
                                                    This refers to the total
                                                    number of overtime hours
                                                    from start to end.
                                                </span>
                                            </span>
                                        </p>
                                        <div className="schedule_timeSect">
                                            <Input
                                                id="total_overtime_hours"
                                                className="editing"
                                                value={ moment(
                                                    this.totalOvertimeHours(),
                                                ).format("HH:mm") }
                                                disabled
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                        <div>
                            <div className="add_switch add_checkswitch swtich-padding">
                                <Switch
                                    id="schedule_repeat_checkbox"
                                    name="schedule_repeat"
                                    disabled={ this.props.schedInUse }
                                    checked={ form.schedule_repeat }
                                    ref={ (ref) => {
                                        this.schedule_repeat = ref;
                                    } }
                                    onChange={ () => {
                                        this.setState({
                                            form: {
                                                ...form,
                                                schedule_repeat:
                                                    !form.schedule_repeat,
                                            },
                                        });
                                    } }
                                />
                                <p className="switch_label schdule_tooltip">
                                    Schedule repeat
                                    <span className="bg_tooltip">
                                        <span>?</span>
                                        <span className="tooltiptext tooltiptextHours">
                                            Determine how often this schedule
                                            will be used.
                                        </span>
                                    </span>
                                </p>
                            </div>
                            {form.schedule_repeat && (
                                <RepeatScheduleForm
                                    repeat={ form.repeat }
                                    startDate={ form.start_date }
                                    handleRepeatChange={ (value) =>
                                        this.handleRepeatChange(value)
                                    }
                                    schedInUse={ this.props.schedInUse }
                                />
                            )}
                            <div className="add_switch add_checkswitch swtich-padding">
                                <Switch
                                    name="on_holidays"
                                    id="on_holidays_checkbox"
                                    ref={ (ref) => {
                                        this.holiday_checkbox = ref;
                                    } }
                                    checked={ form.on_holidays }
                                    onChange={ () => {
                                        this.setState({
                                            form: {
                                                ...form,
                                                on_holidays: !form.on_holidays,
                                            },
                                        });
                                    } }
                                />
                                <div className="switch_label">
                                    Schedule on Holidays
                                </div>
                            </div>
                            <div className="add_switch add_checkswitch swtich-padding">
                                <Switch
                                    name="on_rest_day"
                                    id="on_rest_day_checkbox"
                                    checked={ form.on_rest_day }
                                    ref={ (ref) => {
                                        this.rest_day_checkbox = ref;
                                    } }
                                    onChange={ () => {
                                        this.setState({
                                            form: {
                                                ...form,
                                                on_rest_day: !form.on_rest_day,
                                            },
                                        });
                                    } }
                                />
                                <div className="switch_label">
                                    Schedule on Rest Days
                                </div>
                            </div>
                            <div className="add_switch add_checkswitch swtich-padding">
                                <Switch
                                    name="timeMethodsAllowed"
                                    id="time_method_allowed_checkbox"
                                    checked={ form.timeMethodsAllowed }
                                    ref={ (ref) => {
                                        this.time_method_checkbox = ref;
                                    } }
                                    onChange={ () => {
                                        this.setState({
                                            form: {
                                                ...form,
                                                timeMethodsAllowed:
                                                    !form.timeMethodsAllowed,
                                            },
                                        });
                                    } }
                                />
                                <div className="switch_label">
                                    Time Methods Allowed
                                </div>
                            </div>
                            {form.timeMethodsAllowed && (
                                <TimeScheduleForm
                                    allowedTimeMethods={
                                        form.allowed_time_methods
                                    }
                                    handleTimeMethodChange={ (i, value) =>
                                        this.handleTimeMethodChange(i, value)
                                    }
                                />
                            )}
                            <div className="add_switch add_checkswitch swtich-padding">
                                <Switch
                                    name="auto_assign"
                                    id="auto_assign_checkbox"
                                    checked={ form.auto_assign }
                                    ref={ (ref) => {
                                        this.auto_assign_checkbox = ref;
                                    } }
                                    onChange={ () => {
                                        this.setState({
                                            form: {
                                                ...form,
                                                auto_assign: !form.auto_assign,
                                            },
                                        });
                                    } }
                                />
                                <p className="switch_label schdule_tooltip">
                                    Auto-assign as shift
                                    <span className="bg_tooltip">
                                        <span>?</span>
                                        <span className="tooltiptext tooltiptextHours">
                                            Automatically assign this schedule
                                            as shift to entitled employees.
                                        </span>
                                    </span>
                                </p>
                            </div>
                        </div>
                        <div className="input_employees">
                            <p>Entitled Employees</p>
                            <MultiSelect
                                id="leader"
                                async
                                loadOptions={ this.loadEmployeeList }
                                ref={ (ref) => {
                                    this.employees = ref;
                                } }
                                value={ form.affected_employees }
                                placeholder="Enter location, department name, position, or employee name"
                                onChange={ (value) => {
                                    this.setState({
                                        form: {
                                            ...form,
                                            affected_employees: value,
                                        },
                                    });
                                } }
                                required
                                hasSearchIcon
                            />
                        </div>
                        <div className="tag_input">
                            <p className="schdule_tooltip">
                                Tags
                                <span className="bg_tooltip">
                                    <span>?</span>
                                    <span className="tooltiptext tooltiptextHours">
                                        Keywords that will help users find this
                                        schedule.
                                    </span>
                                </span>
                            </p>
                            <Creatable
                                id="recipients"
                                placeholder={
                                    "Tag this schedule with previously created tags or you can also create new tags"
                                }
                                options={ defaultOptions }
                                name="tags"
                                value={ form.tags }
                                onChange={ (value) => {
                                    this.setState({
                                        form: {
                                            ...form,
                                            tags: value,
                                        },
                                    });
                                } }
                                isClearable={ false }
                                removeSelected={ false }
                                multi
                            />
                        </div>
                    </Container>
                </ScheduleFormWrapper>
                <Modal
                    title={ modal.title }
                    body={ modal.body }
                    buttons={ [
                        {
                            type: modal.cancelButtonType || "grey",
                            label: modal.cancelButtonLabel || "No",
                            onClick: () => {
                                modal.onCancel();
                            },
                        },
                        {
                            type: modal.confirmButtonType || "action",
                            label: modal.confirmButtonLabel || "Yes",
                            onClick: () => {
                                modal.onConfirm();
                            },
                        },
                    ] }
                    showClose={ false }
                    ref={ (ref) => {
                        this.modal = ref;
                    } }
                />
            </div>
        );
    }
}
