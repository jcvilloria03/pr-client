import React from 'react';
import Flatpickr from 'react-flatpickr';
import moment from 'moment';

import { calculateTimeDifference } from 'utils/functions';
import Icon from 'components/Icon';
import { TIME_OBJECT_KEYS } from '../constants';
import { validation } from '../validation';

export default class OvertimeScheduleForm extends React.Component {
    static propTypes = {
        form: React.PropTypes.object,
        addOvertimeSchedule: React.PropTypes.func,
        editOvertimeSchedule: React.PropTypes.func,
        removeOvertimeSchedule: React.PropTypes.func,
        over_time: React.PropTypes.array,
    };

    constructor(props) {
        super(props);

        this.state = {
            isButtonDisabled: false,
            [TIME_OBJECT_KEYS.OVERTIME]: this.props.over_time,
            previous: [],
        };
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps[TIME_OBJECT_KEYS.OVERTIME] !==
            this.props[TIME_OBJECT_KEYS.OVERTIME]
        ) {
            this.setState({
                [TIME_OBJECT_KEYS.OVERTIME]: [
                    ...this.props[TIME_OBJECT_KEYS.OVERTIME],
                ],
            });
        }
    }

    onCalculateOvertime(id, event, element) {
        this.setState({
            [TIME_OBJECT_KEYS.OVERTIME]: this.state[
                TIME_OBJECT_KEYS.OVERTIME
            ].map((item) => {
                if (item.id === id) {
                    let update = {
                        ...item,
                    };

                    if (element.name === 'start') {
                        const startTime = moment(new Date(event)).format(
                            'HH:mm',
                        );
                        const endTime = item.end;
                        update = {
                            ...update,
                            start: startTime,
                            overtime_hours:
                                startTime && endTime
                                    ? calculateTimeDifference(
                                          startTime,
                                          endTime,
                                      )
                                    : null,
                        };
                    } else if (element.name === 'end') {
                        const startTime = item.start;
                        const endTime = moment(new Date(event)).format('HH:mm');

                        update = {
                            ...update,
                            end: moment(new Date(event)).format('HH:mm'),
                            overtime_hours:
                                startTime && endTime
                                    ? calculateTimeDifference(
                                          startTime,
                                          endTime,
                                      )
                                    : null,
                        };
                    }
                    return update;
                }
                return item;
            }),
        });
    }

    handleValueChange(id, value, element) {
        let update = [...this.state[TIME_OBJECT_KEYS.OVERTIME]];

        update = update.map((item) => {
            if (item.id === id) {
                return {
                    ...item,
                    [element.name]: value,
                    errors: {
                        ...item.errors,
                        [element.name]: null,
                    },
                };
            }
            return item;
        });

        this.setState({ [TIME_OBJECT_KEYS.OVERTIME]: [...update] });
    }

    handleUpdateSchedule(id, data, actionType) {
        if (actionType === 'save' || actionType === 'add') {
            let update = [...this.state[TIME_OBJECT_KEYS.OVERTIME]];
            const errors = validation({ ...this.props.form, [TIME_OBJECT_KEYS.OVERTIME]: update }, TIME_OBJECT_KEYS.OVERTIME, data);

            if (!Object.keys(errors).length) {
                update = update.map((item) => {
                    if (item.id === id) {
                        return {
                            ...item,
                            saved: true,
                        };
                    }
                    return item;
                });
                this.setState({
                    previous: [
                        ...this.state.previous,
                        update,
                    ],
                });
            }
        }

        if (actionType === 'add') {
            this.props.addOvertimeSchedule(
                data.id,
                data,
            );
        } else {
            this.props.editOvertimeSchedule(id, data, actionType);
        }
    }

    handleResetValues(id) {
        let update = [...this.state[TIME_OBJECT_KEYS.OVERTIME]];

        update = update.map((item) => {
            if (item.id === id) {
                return {
                    ...this.props[TIME_OBJECT_KEYS.OVERTIME].find(
                        (data) => data.id === id,
                    ),
                };
            }
            return item;
        });

        const previous = [...this.state.previous];
        if (previous.length > 0) {
            const reference = { scheduled_overtime: previous.pop() };
            this.props.editOvertimeSchedule(id, update, 'cancel', reference);
        } else {
            this.props.editOvertimeSchedule(id, update, 'cancel');
        }
    }

    render() {
        const { isButtonDisabled } = this.state;
        const overtime = this.state[TIME_OBJECT_KEYS.OVERTIME];

        return (
            <div className="brecks_title">
                <div className="break_title">Scheduled Overtime</div>
                <div className="row">
                    <div className="col-md-10 schedule_main">
                        <div className="col-md-2 name schedule_main"></div>
                        <div className="col-md-4 schedule_main">
                            <div className="break_type">Start Time</div>
                        </div>
                        <div className="col-md-4 schedule_main">
                            <div className="break_type">End Time</div>
                        </div>
                    </div>
                    <div className="col-md-2 schedule_main"></div>
                </div>
                <div className="row">
                    {overtime.map((data, i) => (
                        <div key={ `${data.id}_${i}` }>
                            <div className="col-md-10 schedule_main">
                                <div className="col-md-2 name schedule_main">
                                    Overtime {i + 1}
                                </div>
                                <div className="col-md-4 data schedule_main">
                                    <div
                                        className={
                                            data.errors.start
                                                ? 'schedule_timeSect errorTimeIcon'
                                                : 'schedule_timeSect timeIcon'
                                        }
                                    >
                                        {!data.editing ||
                                        (data.saved && !data.editing) ? (
                                            <div className="data">
                                                {data.start}
                                            </div>
                                        ) : (
                                            <div>
                                                <Flatpickr
                                                    data-enable-time
                                                    name="start"
                                                    placeholder="Choose time"
                                                    value={ data.start }
                                                    options={ {
                                                        noCalendar: true,
                                                        enableTime: true,
                                                        time_24hr: true,
                                                    } }
                                                    onChange={ (
                                                        event,
                                                        _,
                                                        { element },
                                                    ) => {
                                                        this.handleValueChange(
                                                            data.id,
                                                            _,
                                                            element,
                                                        );
                                                        this.onCalculateOvertime(
                                                            data.id,
                                                            event,
                                                            element,
                                                        );
                                                    } }
                                                />
                                                <Icon name="time2" />
                                            </div>
                                        )}
                                    </div>
                                    {data.errors.start && (
                                        <p style={ { color: '#eb7575' } }>
                                            {data.errors.start}
                                        </p>
                                    )}
                                </div>
                                <div className="col-md-4 data schedule_main">
                                    <div
                                        className={
                                            data.errors.end
                                                ? 'schedule_timeSect errorTimeIcon'
                                                : 'schedule_timeSect timeIcon'
                                        }
                                    >
                                        {!data.editing ||
                                        (data.saved && !data.editing) ? (
                                            <div className="data">
                                                {data.end}
                                            </div>
                                        ) : (
                                            <div>
                                                <Flatpickr
                                                    data-enable-time
                                                    name="end"
                                                    placeholder="Choose time"
                                                    value={ data.end }
                                                    options={ {
                                                        noCalendar: true,
                                                        enableTime: true,
                                                        time_24hr: true,
                                                    } }
                                                    onChange={ (
                                                        event,
                                                        _,
                                                        { element },
                                                    ) => {
                                                        this.handleValueChange(
                                                            data.id,
                                                            _,
                                                            element,
                                                        );
                                                        this.onCalculateOvertime(
                                                            data.id,
                                                            event,
                                                            element,
                                                        );
                                                    } }
                                                />
                                                <Icon name="time2" />
                                            </div>
                                        )}
                                    </div>
                                    {data.errors.end && (
                                        <p style={ { color: '#eb7575' } }>
                                            {data.errors.end}
                                        </p>
                                    )}
                                </div>
                            </div>
                            {data.start && data.end && data.saved ? (
                                <div>
                                    <div className="col-md-2 schedule_main btnEditSect">
                                        <div className="break_button">
                                            <button
                                                className={
                                                    !data.editing
                                                        ? 'sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost'
                                                        : 'sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost btnSave'
                                                }
                                                onClick={ () =>
                                                    this.handleUpdateSchedule(
                                                        data.id,
                                                        data,
                                                        !data.editing
                                                            ? 'edit'
                                                            : 'save',
                                                    )
                                                }
                                            >
                                                {!data.editing && (
                                                    <Icon name="pencil" />
                                                )}
                                                <span>
                                                    {!data.editing
                                                        ? 'Edit'
                                                        : 'Save'}
                                                </span>
                                            </button>
                                        </div>
                                        <div className="break_button">
                                            <button
                                                className="sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost btnClose"
                                                onClick={ () => {
                                                    !data.editing
                                                        ? this.props.removeOvertimeSchedule(
                                                              data.id,
                                                          )
                                                        : this.handleResetValues(
                                                              data.id,
                                                          );
                                                } }
                                            >
                                                {!data.editing && (
                                                    <Icon name="trash" />
                                                )}
                                                <span>
                                                    {!data.editing
                                                        ? 'Delete'
                                                        : 'Cancel'}
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                <div className="col-md-2 schedule_main">
                                    <div className="break_button">
                                        <button
                                            className="sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost"
                                            onClick={ () =>
                                                this.handleUpdateSchedule(data.id, data, 'add')
                                            }
                                            disabled={ isButtonDisabled }
                                        >
                                            <Icon name="plus" />
                                            <span>Add</span>
                                        </button>
                                    </div>
                                </div>
                            )}
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}
