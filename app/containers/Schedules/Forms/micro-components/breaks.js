/* eslint-disable camelcase */
import React from 'react';
import Flatpickr from 'react-flatpickr';
import moment from 'moment';
import { isEqual } from 'lodash';

import MultiSelect from 'components/MultiSelect';
import Switch from 'components/Switch2';
import Input from 'components/Input';
import Icon from 'components/Icon';

import { calculateTimeDifference } from 'utils/functions';
import { TIME_OBJECT_KEYS } from '../constants';
import { validation } from '../validation';

export class BreakScheduleForm extends React.Component {
    static propTypes = {
        form: React.PropTypes.object,
        addBreakSchedule: React.PropTypes.func,
        editBreakSchedule: React.PropTypes.func,
        removeBreakSchedule: React.PropTypes.func,
        break_time: React.PropTypes.array,
    };

    constructor(props) {
        super(props);

        this.state = {
            list_type: [
                { value: 'fixed', label: 'Fixed break' },
                { value: 'flexi', label: 'Flexi break' },
                { value: 'floating', label: 'Floating break' },
            ],
            [TIME_OBJECT_KEYS.BREAKTIME]: this.props.break_time,
            previous: [],
            isButtonDisabled: false,
        };
    }

    componentDidUpdate(prevProps) {
        if ( !isEqual( prevProps[TIME_OBJECT_KEYS.BREAKTIME], this.props[TIME_OBJECT_KEYS.BREAKTIME] ) ) {
            this.setState({
                [TIME_OBJECT_KEYS.BREAKTIME]: [
                    ...this.props[TIME_OBJECT_KEYS.BREAKTIME],
                ],
            });
        }
    }

    onCalculateBreak = (id, event, element) => {
        const { break_time } = this.state;

        this.setState({
            [TIME_OBJECT_KEYS.BREAKTIME]: break_time.map((item) => {
                if (item.id === id) {
                    let update = { ...item };

                    if (element === 'type') {
                        update = {
                            ...item,
                            type: event.value,
                        };

                        if (event.value === 'fixed') {
                            update = {
                                ...update,
                                break_hours: '',
                                start: '',
                                end: '',
                            };
                        }
                    } else if (element.name === 'start') {
                        update = {
                            ...item,
                            start: moment(new Date(event)).format('HH:mm'),
                        };
                    } else if (element.name === 'end') {
                        update = {
                            ...item,
                            end: moment(new Date(event)).format('HH:mm'),
                        };
                    } else if (element.name === 'break_hours') {
                        update = {
                            ...item,
                            break_hours: moment(new Date(event)).format(
                                'HH:mm',
                            ),
                        };
                    } else if (element === 'is_paid') {
                        update = {
                            ...item,
                            is_paid: event,
                        };
                    }

                    return update;
                }
                return item;
            }),
        });
    };

    totalScheduleHours(data) {
        return data.start && data.end
            ? calculateTimeDifference(data.start, data.end)
            : null;
    }

    handleValueChange(id, value, element) {
        let update = [...this.state[TIME_OBJECT_KEYS.BREAKTIME]];

        update = update.map((item) => {
            if (item.id === id && element.name) {
                return {
                    ...item,
                    [element.name]: value,
                    break_hours: this.totalScheduleHours({
                        ...item,
                        [element.name]: value,
                    }),
                    errors: {
                        ...item.errors,
                        [element.name]: null,
                    },
                };
            }
            return item;
        });

        this.setState({ [TIME_OBJECT_KEYS.BREAKTIME]: update });
    }

    handleUpdateSchedule(id, data, actionType) {
        if (actionType === 'save' || actionType === 'add') {
            let update = [...this.state[TIME_OBJECT_KEYS.BREAKTIME]];
            const errors = validation({ ...this.props.form, [TIME_OBJECT_KEYS.BREAKTIME]: update }, TIME_OBJECT_KEYS.BREAKTIME, data);

            if (!Object.keys(errors).length) {
                update = update.map((item) => {
                    if (item.id === id) {
                        return {
                            ...item,
                            saved: true,
                        };
                    }
                    return item;
                });
                this.setState({
                    previous: [
                        ...this.state.previous,
                        update,
                    ],
                });
            }
        }

        if (actionType === 'add') {
            this.props.addBreakSchedule(
                data.id,
                data,
            );
        } else {
            this.props.editBreakSchedule(id, data, actionType);
        }
    }

    handleResetValues(id) {
        let update = [...this.state[TIME_OBJECT_KEYS.BREAKTIME]];

        update = update.map((item) => {
            if (item.id === id) {
                return {
                    ...this.props[TIME_OBJECT_KEYS.BREAKTIME].find(
                        (data) => data.id === id,
                    ),
                };
            }
            return item;
        });

        const previous = [...this.state.previous];
        if (previous.length > 0) {
            const reference = { breaks: previous.pop() };
            this.props.editBreakSchedule(id, update, 'cancel', reference);
        } else {
            this.props.editBreakSchedule(id, update, 'cancel');
        }
    }

    render() {
        const { isButtonDisabled, list_type } = this.state;
        const breaks = this.state[TIME_OBJECT_KEYS.BREAKTIME];
        return (
            <div className="brecks_title">
                <div className="break_title">Breaks</div>
                <div className="row">
                    <div className="col-md-10 schedule_main">
                        <div className="col-md-1 schedule_main"></div>
                        <div className="col-md-2 schedule_main">
                            <div className="break_type">Break type</div>
                        </div>
                        <div className="col-md-3 schedule_main">
                            <div className="break_type">Break start time</div>
                        </div>
                        <div className="col-md-3 schedule_main">
                            <div className="break_type">Break end time</div>
                        </div>
                        <div className="col-md-2 schedule_main">
                            <div className="break_type">Break hours</div>
                        </div>
                        <div className="col-md-1 schedule_main">
                            <div className="break_type">Paid Break</div>
                        </div>
                    </div>
                    <div className="col-md-2 schedule_main"></div>
                </div>
                <div className="row">
                    {breaks.map((data, i) => (
                        <div key={ `${data.id}_${i}` }>
                            <div className="col-md-10 data schedule_main">
                                <div className="col-md-1 name">
                                    Break {i + 1}
                                </div>
                                <div className="col-md-2 data schedule_main breakType">
                                    <div
                                        className={
                                            data.errors.type
                                                ? 'form-group error-select-field errorTimeIcon'
                                                : 'form-group'
                                        }
                                    >
                                        {!data.editing ||
                                        (data.saved && !data.editing) ? (
                                            <p>{data.type}</p>
                                        ) : (
                                            <MultiSelect
                                                id="break_type"
                                                value={ data.type }
                                                data={ list_type }
                                                multi={ false }
                                                ref={ (ref) => {
                                                    this.employees = ref;
                                                } }
                                                onChange={ (event) => {
                                                    this.onCalculateBreak(
                                                        data.id,
                                                        event,
                                                        'type',
                                                    );
                                                } }
                                                placeholder=""
                                            />
                                        )}
                                    </div>
                                    {data.errors.type && (
                                        <p style={ { color: '#eb7575' } }>
                                            {data.errors.type}
                                        </p>
                                    )}
                                </div>
                                {data.type !== 'floating' ? (
                                    <div>
                                        <div className="col-md-3 schedule_main">
                                            <div
                                                className={
                                                    data.errors.start
                                                        ? 'schedule_timeSect errorTimeIcon'
                                                        : 'schedule_timeSect timeIcon'
                                                }
                                            >
                                                {!data.editing ||
                                                (data.saved &&
                                                    !data.editing) ? (
                                                        <p>{data.start}</p>
                                                ) : (
                                                    <div>
                                                        <Flatpickr
                                                            data-enable-time
                                                            name="start"
                                                            placeholder="Choose time"
                                                            value={ data.start }
                                                            options={ {
                                                                noCalendar: true,
                                                                enableTime: true,
                                                                time_24hr: true,
                                                            } }
                                                            onChange={ (
                                                                event,
                                                                _,
                                                                { element },
                                                            ) => {
                                                                this.handleValueChange(
                                                                    data.id,
                                                                    _,
                                                                    element,
                                                                );
                                                                this.onCalculateBreak(
                                                                    data.id,
                                                                    event,
                                                                    element,
                                                                );
                                                            } }
                                                        />
                                                        <Icon name="time2" />
                                                    </div>
                                                )}
                                            </div>
                                            {data.errors.start && (
                                                <p
                                                    style={ {
                                                        color: '#eb7575',
                                                    } }
                                                >
                                                    {data.errors.start}
                                                </p>
                                            )}
                                        </div>
                                        <div className="col-md-3 schedule_main">
                                            <div
                                                className={
                                                    data.errors.end
                                                        ? 'schedule_timeSect errorTimeIcon'
                                                        : 'schedule_timeSect timeIcon'
                                                }
                                            >
                                                {!data.editing ||
                                                (data.saved &&
                                                    !data.editing) ? (
                                                        <p>{data.end}</p>
                                                ) : (
                                                    <div>
                                                        <Flatpickr
                                                            data-enable-time
                                                            name="end"
                                                            placeholder="Choose time"
                                                            value={ data.end }
                                                            options={ {
                                                                noCalendar: true,
                                                                enableTime: true,
                                                                time_24hr: true,
                                                            } }
                                                            onChange={ (
                                                                event,
                                                                _,
                                                                { element },
                                                            ) => {
                                                                this.handleValueChange(
                                                                    data.id,
                                                                    _,
                                                                    element,
                                                                );
                                                                this.onCalculateBreak(
                                                                    data.id,
                                                                    event,
                                                                    element,
                                                                );
                                                            } }
                                                        />
                                                        <Icon name="time2" />
                                                    </div>
                                                )}
                                            </div>
                                            {data.errors.end && (
                                                <p
                                                    style={ {
                                                        color: '#eb7575',
                                                    } }
                                                >
                                                    {data.errors.end}
                                                </p>
                                            )}
                                        </div>
                                    </div>
                                ) : (
                                    <div>
                                        <div className="col-md-3 schedule_main">
                                            <div className="schedule_timeSect">
                                                {!data.editing ||
                                                (data.saved &&
                                                    !data.editing) ? (
                                                        <p>{null}</p>
                                                ) : (
                                                    <Flatpickr
                                                        className="start_Time"
                                                        data-enable-time
                                                        options={ {
                                                            noCalendar: true,
                                                            enableTime: true,
                                                            time_24hr: true,
                                                        } }
                                                        disabled
                                                    />
                                                )}
                                            </div>
                                        </div>
                                        <div className="col-md-3 schedule_main">
                                            <div className="schedule_timeSect">
                                                {!data.editing ||
                                                (data.saved &&
                                                    !data.editing) ? (
                                                        <p>{null}</p>
                                                ) : (
                                                    <Flatpickr
                                                        data-enable-time
                                                        className="start_Time"
                                                        options={ {
                                                            noCalendar: true,
                                                            enableTime: true,
                                                            time_24hr: true,
                                                        } }
                                                        disabled
                                                    />
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                )}
                                {data.type !== 'fixed' ? (
                                    <div className="col-md-2 schedule_main">
                                        <div
                                            className={
                                                data.errors.break_hours
                                                    ? 'schedule_timeSect errorTimeIcon'
                                                    : 'schedule_timeSect'
                                            }
                                        >
                                            {!data.editing ||
                                            (data.saved && !data.editing) ? (
                                                <p>{data.break_hours}</p>
                                            ) : (
                                                <div>
                                                    <Flatpickr
                                                        data-enable-time
                                                        name="break_hours"
                                                        placeholder="Choose time"
                                                        value={ data.break_hours }
                                                        options={ {
                                                            noCalendar: true,
                                                            enableTime: true,
                                                            time_24hr: true,
                                                        } }
                                                        onChange={ (
                                                            event,
                                                            _,
                                                            { element },
                                                        ) => {
                                                            this.onCalculateBreak(
                                                                data.id,
                                                                event,
                                                                element,
                                                            );
                                                        } }
                                                    />
                                                </div>
                                            )}
                                        </div>
                                        {data.errors.break_hours && (
                                            <p style={ { color: '#eb7575' } }>
                                                {data.errors.break_hours}
                                            </p>
                                        )}
                                    </div>
                                ) : (
                                    <div className="col-md-2 schedule_main">
                                        <div className="schedule_timeSect">
                                            {!data.editing ||
                                            (data.saved && !data.editing) ? (
                                                <p>{data.break_hours}</p>
                                            ) : (
                                                <Input
                                                    id="membership_program"
                                                    className="editing"
                                                    name="break_hours"
                                                    value={ data.break_hours }
                                                    disabled
                                                />
                                            )}
                                        </div>
                                        {data.errors.break_hours && (
                                            <p style={ { color: '#eb7575' } }>
                                                {data.errors.break_hours}
                                            </p>
                                        )}
                                    </div>
                                )}
                                <div className="col-md-1 schedule_main">
                                    <div className="switch_toggle">
                                        {!data.editing ? (
                                            <p>
                                                {data.is_paid === true
                                                    ? 'Yes'
                                                    : 'No'}
                                            </p>
                                        ) : (
                                            <Switch
                                                id="repeat_checkbox"
                                                name="is_paid"
                                                checked={ data.is_paid }
                                                onChange={ () => {
                                                    this.onCalculateBreak(
                                                        data.id,
                                                        !data.is_paid,
                                                        'is_paid',
                                                    );
                                                } }
                                            />
                                        )}
                                    </div>
                                </div>
                            </div>
                            {(data.type === 'floating' &&
                                data.saved &&
                                data.break_hours) ||
                            data.saved ? (
                                <div>
                                    <div className="col-md-2 schedule_main btnEditSect">
                                        <div className="break_button">
                                            <button
                                                className={
                                                    !data.editing
                                                        ? 'sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost'
                                                        : 'sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost btnSave'
                                                }
                                                onClick={ () => {
                                                    this.handleUpdateSchedule(data.id,
                                                        data,
                                                        !data.editing
                                                            ? 'edit'
                                                            : 'save');
                                                } }
                                            >
                                                {!data.editing && (
                                                    <Icon name="pencil" />
                                                )}
                                                <span>
                                                    {!data.editing
                                                        ? 'Edit'
                                                        : 'Save'}
                                                </span>
                                            </button>
                                        </div>
                                        <div className="break_button">
                                            <button
                                                className="sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost btnClose"
                                                onClick={ () => {
                                                    !data.editing
                                                        ? this.props.removeBreakSchedule(
                                                              data.id,
                                                          )
                                                        : this.handleResetValues(
                                                              data.id,
                                                          );
                                                } }
                                            >
                                                {!data.editing && (
                                                    <Icon name="trash" />
                                                )}
                                                <span>
                                                    {!data.editing
                                                        ? 'Delete'
                                                        : 'Cancel'}
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                <div className="col-md-2 schedule_main">
                                    <div className="break_button">
                                        <button
                                            className="sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost"
                                            onClick={ () =>
                                                this.handleUpdateSchedule(data.id,
                                                    data,
                                                    'add')

                                            }
                                            disabled={ isButtonDisabled }
                                        >
                                            <Icon name="plus" />
                                            <span>Add</span>
                                        </button>
                                    </div>
                                </div>
                            )}
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

export default BreakScheduleForm;
