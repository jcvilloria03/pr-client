/* eslint-disable camelcase */
/* eslint-disable no-confusing-arrow */
import React from 'react';
import moment from 'moment';
import _ from 'lodash';

import Radio from 'components/Radio';
import Input from 'components/Input';
import SalSelect from 'components/Select';
import DatePicker from 'components/DatePicker';
import RadioGroup from 'components/RadioGroup';
import CustomCheckbox from 'components/CustomCheckbox';

import { formatDate } from 'utils/functions';

import {
    DATE_FORMATS,
    REPEAT_BY_WEEK,
    REPEAT_BY_MONTH,
    NEVER_OPTION,
    REPEAT_VALUES,
    REPEAT_EVERY_LABELS,
    AFTER_OPTION,
    ON_OPTION,
    END_AFTER_DEFAULT_VALUE,
    REPEAT_MONTHLY_VALUE,
    REPEAT_WEEKLY_VALUE,
} from 'utils/constants';
import { REPEAT_EVERY_VALUES, WEEKDAYS } from '../../../../utils/constants';

/**
 *
 * RepeatSchedule
 *
 */
export class RepeatScheduleForm extends React.Component {
    // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        repeat: React.PropTypes.object,
        startDate: React.PropTypes.string,
        handleRepeatChange: React.PropTypes.func,
        schedInUse: React.PropTypes.bool,
    };
    /**
     * component constructor
     */
    constructor(props) {
        super(props);

        this.state = {
            typeOptions: REPEAT_VALUES,
            repeatEveryOptions: _.range(30).map((e, i) => ({
                label: String(i + 1),
                value: i + 1,
            })),
            repeatEveryLabels: REPEAT_EVERY_LABELS,
            neverOption: NEVER_OPTION,
            afterOption: AFTER_OPTION,
            onOption: ON_OPTION,
            repeatByWeek: REPEAT_BY_WEEK,
            repeatByMonth: REPEAT_BY_MONTH,
            custom: true,
        };
    }

    /**
     * Type Object value
     */
    typeObjectValue(newValue) {
        this.props.repeat.type = newValue.value;
    }

    endRepeatOptions(value) {
        this.props.handleRepeatChange({
            end_never: value === 'never',
            end_on: value === 'on' ? this.minEndOnDate() : null,
            end_after: value === 'after' ? END_AFTER_DEFAULT_VALUE : null,
        });
    }

    minEndOnDate() {
        if (this.props.startDate) {
            return moment(this.props.startDate, DATE_FORMATS.SCHEDULES_DISPLAY)
                .add(1, 'days')
                .format(DATE_FORMATS.SCHEDULES_DISPLAY);
        }
        return moment().format(DATE_FORMATS.SCHEDULES_DISPLAY);
    }

    getRepeatSummary = (data) => {
        const { repeat_every, type, repeat_on, end_after, end_on } = data;
        let message = 'Summary: ';

        if (repeat_every > 1) {
            message = `${message} Every ${repeat_every} ${REPEAT_EVERY_LABELS[type]}`;
        } else if (repeat_every === 1) {
            const transformedType = REPEAT_EVERY_VALUES.find(
                (label) => label.toLowerCase() === type,
            );

            if (transformedType) {
                if (transformedType === 'Yearly') {
                    message = `${message} Annually`;
                } else {
                    message = `${message} ${transformedType}`;
                }
            } else {
                const definedType = REPEAT_VALUES.find((value) => value.value === type);
                message = `${message} ${definedType.summary || definedType.label}`;
            }
        }

        if (repeat_on.length && (type === 'daily' || type === 'weekly' || type === 'monthly')) {
            const repeatOn = repeat_on
                .filter((day) => day.selected)
                .map((day) => day.label);

            if (JSON.stringify(repeatOn) === JSON.stringify(WEEKDAYS)) {
                message = `${message} on weekdays`;
            } else if (repeatOn.length) {
                message = `${message} on ${repeatOn.join(', ')}`;
            }
        }

        if (end_after) {
            message = `${message}, ${end_after} times`;
        }

        if (end_on) {
            message = `${message}, until ${moment(end_on).format('MMMM DD, YYYY')}`;
        }

        return message;
    };

    render() {
        const {
            typeOptions,
            repeatEveryOptions,
            repeatByMonth,
            repeatByWeek,
            repeatEveryLabels,
        } = this.state;

        const { repeat, startDate } = this.props;

        let endsOption = '';
        if (repeat.end_never) {
            endsOption = NEVER_OPTION;
        } else if (repeat.end_after) {
            endsOption = AFTER_OPTION;
        } else if (repeat.end_on) {
            endsOption = ON_OPTION;
        }

        const isRepeatEveryShown = ![
            'every_weekday',
            'every_monday_wednesday_friday',
            'every_tuesday_thursday',
        ].includes(repeat.type);

        return (
            <div className="switch_report">
                <div>
                    <div>Repeats</div>
                    <div className="form-group">
                        <SalSelect
                            id="type_options"
                            placeholder="Select desired allowance type"
                            data={ typeOptions }
                            name="typeOptions"
                            value={ repeat && repeat.type }
                            disabled={ this.props.schedInUse }
                            onChange={ (value) => {
                                this.props.handleRepeatChange({
                                    typeObject:
                                        this.typeObjectValue(value) || value,
                                });
                            } }
                            searchable={ false }
                        />
                    </div>
                </div>
                {isRepeatEveryShown && (
                    <div className="repeat_titls">
                        <div>Repeat every</div>
                        <div className="form-group input_repeat">
                            <SalSelect
                                id="repeat_every"
                                data={ repeatEveryOptions }
                                name="repeat_every"
                                disabled={ this.props.schedInUse }
                                value={ repeat && repeat.repeat_every }
                                onChange={ ({ value }) => {
                                    this.props.handleRepeatChange({
                                        repeat_every: value,
                                    });
                                } }
                                searchable={ false }
                            />
                            {repeatEveryLabels[repeat.type] || ''}
                        </div>
                    </div>
                )}
                {repeat && repeat.type === REPEAT_MONTHLY_VALUE.value && (
                    <div>
                        <div>Repeat by</div>
                        <RadioGroup
                            id="radioAmendmentReturn"
                            name="repeat_by"
                            value={ repeat && repeat.repeat_by }
                            className="radio-group"
                            onChange={ (value) => {
                                this.props.handleRepeatChange({
                                    repeat_by: value,
                                });
                            } }
                            disabled={ this.props.schedInUse }
                        >
                            <Radio value={ repeatByMonth }>
                                Day of the month
                            </Radio>
                            <Radio value={ repeatByWeek }>Day of the week</Radio>
                        </RadioGroup>
                    </div>
                )}
                {repeat && repeat.type === REPEAT_WEEKLY_VALUE.value && (
                    <div>
                        <div>Repeat on</div>
                        <div className="Input_from">
                            {repeat.repeat_on.map((val, i) => (
                                <div className="input_check" key={ i }>
                                    <CustomCheckbox
                                        label={ val.label }
                                        checkValue={ val.selected }
                                        disabled={ this.props.schedInUse }
                                        onChange={ () => {
                                            const selectedDays =
                                                repeat.repeat_on.map((data) =>
                                                    data.value === val.value
                                                        ? {
                                                            ...data,
                                                            selected:
                                                                  !data.selected,
                                                        }
                                                        : data,
                                                );
                                            this.props.handleRepeatChange({
                                                repeat_on: selectedDays,
                                            });
                                        } }
                                    />
                                </div>
                            ))}
                        </div>
                    </div>
                )}
                {startDate && (
                    <div className="start-on">
                        Starts on: {moment(startDate).format('MM/DD/YYYY')}
                    </div>
                )}
                <div className="repeat_ends" style={ { marginTop: '1rem' } }>
                    <div>Ends</div>
                </div>
                <div className="repeat_intervals">
                    <RadioGroup
                        id="radioAmendmentReturn"
                        name="repeat-ends"
                        value={ endsOption }
                        className="radio-group"
                        onChange={ (value) => {
                            this.endRepeatOptions(value);
                        } }
                    >
                        <Radio
                            value={ 'never' }
                            disabled={ this.props.schedInUse }
                        >
                            Never
                        </Radio>
                        <Radio
                            value={ 'after' }
                            className="after"
                            disabled={ this.props.schedInUse }
                        >
                            After
                        </Radio>

                        <Radio
                            value={ 'on' }
                            disabled={ this.props.schedInUse }
                        >
                            On
                        </Radio>
                    </RadioGroup>

                    <div className="repeat_input">
                        <div className="schedule_timeSect filler"></div>
                        <div className="schedule_timeSect">
                            <Input
                                id="membership_program1"
                                className="editing"
                                value={ repeat.end_after || '' }
                                disabled={
                                    endsOption !== 'after' || this.props.schedInUse
                                }
                                onChange={ (value) => {
                                    this.props.handleRepeatChange({
                                        end_after: value,
                                    });
                                } }
                            />
                            <span>&nbsp;&nbsp;occurrences</span>
                        </div>
                        <div className="schedule_timeSect">
                            <DatePicker
                                label=""
                                name="end_on"
                                dayFormat={ DATE_FORMATS.MMDDYYYY }
                                value={ repeat.end_on || '' }
                                selectedDay={ repeat.end_on || '' }
                                disabled={ endsOption !== 'on' || this.props.schedInUse }
                                onChange={ (value) => {
                                    this.props.handleRepeatChange({
                                        end_on: formatDate(
                                            value,
                                            DATE_FORMATS.API,
                                        ),
                                    });
                                } }
                                hasIcon
                                disablePreviousDates
                            />
                        </div>
                    </div>
                </div>
                {repeat && repeat.repeat_every > 0 && (
                    <div className="repeat_summary">
                        <p>{this.getRepeatSummary(repeat)}</p>
                    </div>
                )}
            </div>
        );
    }
}

export default RepeatScheduleForm;
