
import React from 'react';
import CustomCheckbox from 'components/CustomCheckbox';

/**
 *
 * Time Schedule Form
 *
 */
export class TimeScheduleForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        allowedTimeMethods: React.PropTypes.object,
        handleTimeMethodChange: React.PropTypes.func
    }

    render() {
        const { allowedTimeMethods } = this.props;

        return (
            <div className="add_switch add_checkswitch">
                {allowedTimeMethods.map( ( data, i ) => (
                    <CustomCheckbox
                        key={ i }
                        label={ data.name }
                        checkValue={ data.selected }
                        onChange={ () => {
                            this.props.handleTimeMethodChange( i, data.selected );
                        } }
                    />
                ) )}
            </div>
        );
    }
}

export default TimeScheduleForm;
