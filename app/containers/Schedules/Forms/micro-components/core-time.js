import React from 'react';
import Flatpickr from 'react-flatpickr';
import moment from 'moment';
import Icon from 'components/Icon';
import styled from 'styled-components';
import { TIME_OBJECT_KEYS } from '../constants';
import { validation } from '../validation';

const StyledIcon = styled(Icon)`
    &&&&& {
        top: 32px;
    }
`;

/**
 *
 * Time Schedule Form
 *
 */
export class CoreTimeScheduleForm extends React.Component {
    // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        form: React.PropTypes.object,
        addCoreTimeSchedule: React.PropTypes.func,
        removeCoreTimeSchedule: React.PropTypes.func,
        editCoreTimeSchedule: React.PropTypes.func,
        core_time: React.PropTypes.array,
    };

    /**
     * component constructor
     */
    constructor(props) {
        super(props);

        this.state = {
            error: '',
            [TIME_OBJECT_KEYS.CORETIME]: this.props.core_time,
            previous: [],
        };
    }

    componentDidMount() {
        this.setState({
            [TIME_OBJECT_KEYS.CORETIME]: [
                ...this.props[TIME_OBJECT_KEYS.CORETIME],
            ],
        });
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps[TIME_OBJECT_KEYS.CORETIME] !==
            this.props[TIME_OBJECT_KEYS.CORETIME]
        ) {
            this.setState({
                [TIME_OBJECT_KEYS.CORETIME]: [
                    ...this.props[TIME_OBJECT_KEYS.CORETIME],
                ],
            });
        }
    }

    onCalculateCoreTime = (id, event, element) => {
        this.setState({
            [TIME_OBJECT_KEYS.CORETIME]: this.state[
                TIME_OBJECT_KEYS.CORETIME
            ].map((item) => {
                if (item.id === id) {
                    let update = { ...item };
                    if (element.name === 'start') {
                        update = {
                            ...item,
                            start: moment(new Date(event)).format('HH:mm'),
                        };
                    } else if (element.name === 'end') {
                        update = {
                            ...item,
                            end: moment(new Date(event)).format('HH:mm'),
                        };
                    }

                    return update;
                }
                return item;
            }),
        });
    };

    handleValueChange(id, value, element) {
        let update = [...this.state[TIME_OBJECT_KEYS.CORETIME]];

        update = update.map((item) => {
            if (item.id === id) {
                return {
                    ...item,
                    [element.name]: value,
                    errors: {
                        ...item.errors,
                        [element.name]: null,
                    },
                };
            }
            return item;
        });

        this.setState({ [TIME_OBJECT_KEYS.CORETIME]: [...update] });
    }

    handleUpdateSchedule(id, data, actionType) {
        if (actionType === 'save' || actionType === 'add') {
            let update = [...this.state[TIME_OBJECT_KEYS.CORETIME]];
            const errors = validation({ ...this.props.form, [TIME_OBJECT_KEYS.CORETIME]: update }, TIME_OBJECT_KEYS.CORETIME, data);

            if (!Object.keys(errors).length) {
                update = update.map((item) => {
                    if (item.id === id) {
                        return {
                            ...item,
                            saved: true,
                        };
                    }
                    return item;
                });
                this.setState({
                    previous: [
                        ...this.state.previous,
                        update,
                    ],
                });
            }
        }

        if (actionType === 'add') {
            this.props.addCoreTimeSchedule(
                data.id,
                data,
            );
        } else {
            this.props.editCoreTimeSchedule(id, data, actionType);
        }
    }

    handleResetValues(id) {
        let update = [...this.state[TIME_OBJECT_KEYS.CORETIME]];

        update = update.map((item) => {
            if (item.id === id) {
                return {
                    ...this.props[TIME_OBJECT_KEYS.CORETIME].find(
                        (data) => data.id === id,
                    ),
                };
            }
            return item;
        });

        const previous = [...this.state.previous];
        if (previous.length > 0) {
            const reference = { core_times: previous.pop() };
            this.props.editCoreTimeSchedule(id, update, 'cancel', reference);
        } else {
            this.props.editCoreTimeSchedule(id, update, 'cancel');
        }
    }

    render() {
        const core = this.state[TIME_OBJECT_KEYS.CORETIME];

        return (
            <div className="brecks_title">
                <p className="schdule_tooltip">
                    * Core times
                    <span className="bg_tooltip">
                        <span>?</span>
                        <span className="tooltiptext tooltiptextEnd">
                            Time when employees on a flexi schedule should
                            report to works.
                        </span>
                    </span>
                </p>
                <div className="row">
                    {core.map((data, i) => (
                        <div key={ i }>
                            <div
                                className={ `col-md-10 core_time schedule_main ${
                                    !data.editing && 'saved'
                                }` }
                            >
                                <div className="name schedule_main core_time">
                                    Core time {i + 1}
                                </div>
                                <div className="data schedule_main core_time">
                                    <div
                                        className={
                                            data.errors.start
                                                ? 'schedule_timeSect errorTimeIcon'
                                                : 'schedule_timeSect timeIcon'
                                        }
                                    >
                                        <div>Core start time</div>
                                        {!data.editing ||
                                        (data.saved && !data.editing) ? (
                                            <div className="core-data">{data.start}</div>
                                        ) : (
                                            <div>
                                                <Flatpickr
                                                    data-enable-time
                                                    name="start"
                                                    placeholder="Choose time"
                                                    value={ data.start }
                                                    options={ {
                                                        noCalendar: true,
                                                        enableTime: true,
                                                        time_24hr: true,
                                                    } }
                                                    onChange={ (
                                                        event,
                                                        _,
                                                        { element },
                                                    ) => {
                                                        this.handleValueChange(
                                                            data.id,
                                                            _,
                                                            element,
                                                        );
                                                        this.onCalculateCoreTime(
                                                            i,
                                                            event,
                                                            element,
                                                        );
                                                    } }
                                                />
                                                <StyledIcon name="time2" />
                                            </div>
                                        )}
                                    </div>
                                    {data.errors.start && (
                                        <p style={ { color: '#eb7575' } }>
                                            {data.errors.start}
                                        </p>
                                    )}
                                </div>
                                <div className="data schedule_main core_time">
                                    <div
                                        className={
                                            data.errors.start
                                                ? 'schedule_timeSect errorTimeIcon'
                                                : 'schedule_timeSect timeIcon'
                                        }
                                    >
                                        <div>Core end time</div>
                                        {!data.editing ||
                                        (data.saved && !data.editing) ? (
                                            <div className="core-data">{data.end}</div>
                                        ) : (
                                            <div>
                                                <Flatpickr
                                                    data-enable-time
                                                    name="end"
                                                    placeholder="Choose time"
                                                    value={ data.end }
                                                    options={ {
                                                        noCalendar: true,
                                                        enableTime: true,
                                                        time_24hr: true,
                                                    } }
                                                    onChange={ (
                                                        event,
                                                        _,
                                                        { element },
                                                    ) => {
                                                        this.handleValueChange(
                                                            data.id,
                                                            _,
                                                            element,
                                                        );
                                                        this.onCalculateCoreTime(
                                                            i,
                                                            event,
                                                            element,
                                                        );
                                                    } }
                                                />
                                                <StyledIcon name="time2" />
                                            </div>
                                        )}
                                    </div>
                                    {data.errors.end && (
                                        <p style={ { color: '#eb7575' } }>
                                            {data.errors.end}
                                        </p>
                                    )}
                                </div>
                            </div>
                            {data.start && data.end && data.saved ? (
                                <div
                                    className={ `col-md-2 schedule_main core_time btnEditSect ${
                                        !data.editing && 'saved'
                                    }` }
                                >
                                    <div className="break_button">
                                        <button
                                            className={
                                                !data.editing
                                                    ? 'sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost'
                                                    : 'sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost btnSave'
                                            }
                                            onClick={ () => {
                                                this.handleUpdateSchedule(
                                                    data.id,
                                                    data,
                                                    !data.editing
                                                        ? 'edit'
                                                        : 'save',
                                                );
                                            } }
                                        >
                                            {!data.editing && (
                                                <Icon name="pencil" />
                                            )}
                                            <span>
                                                {!data.editing
                                                    ? 'Edit'
                                                    : 'Save'}
                                            </span>
                                        </button>
                                    </div>
                                    <div className="break_button">
                                        <button
                                            className="sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost btnClose"
                                            onClick={ () => {
                                                !data.editing
                                                    ? this.props.removeCoreTimeSchedule(
                                                          data.id,
                                                      )
                                                    : this.handleResetValues(
                                                          data.id,
                                                      );
                                            } }
                                        >
                                            {!data.editing && (
                                                <Icon name="trash" />
                                            )}
                                            <span>
                                                {!data.editing
                                                    ? 'Delete'
                                                    : 'Cancel'}
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            ) : (
                                <div className="col-md-2 schedule_main schedule_main_edit">
                                    <div className="break_button">
                                        <button
                                            className="sl-c-btn--primary sl-c-btn--block sl-c-btn--ghost"
                                            onClick={ () =>
                                                this.handleUpdateSchedule(
                                                    data.id,
                                                    data,
                                                    'add',
                                                )
                                            }
                                        >
                                            <Icon name="plus" />
                                            <span>Add</span>
                                        </button>
                                    </div>
                                </div>
                            )}
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

export default CoreTimeScheduleForm;
