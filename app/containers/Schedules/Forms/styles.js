import styled from 'styled-components';

export const ScheduleFormWrapper = styled.div`
    .form-control {
        color: #000 !important;
    }
    select {
        color: #000 !important;
    }
    .schdule_wrap {
        margin-bottom: 5rem;
        .row {
            margin: 0;
        }
    }
    .schedule_type {
        label {
            font-size: 14px;
        }
        .schedule_radio {
            .radio-group {
                > div {
                    font-size: 14px;
                    padding: 0 !important;
                    > div {
                        display: flex;
                        align-items: center;
                    }
                }
            }
        }
    }
    .validity_timesect {
        span {
            > * {
                background-repeat: no-repeat;
                background-size: 14px;
            }
        }

        > span.isvg {
            position: absolute;
            right: 25px;
            top: 46px;
            width: 14px;
            transform: translateY(-50%);
        }

        .label {
            display: none;
        }
    }
    .schedule_name {
        h2,
        label,
        p {
            font-size: 14px;
            margin-bottom: 3px;
        }
        .DayPickerInput,
        .DayPickerInput input {
            width: 100%;
        }
    }
    .after_radio {
        display: flex;
        align-items: center;
        #After,
        #On {
            margin-bottom: 0 !important;
        }
        .repeat_input {
            margin-left: 20px;
        }
    }
    .on_radio {
        margin-top: 1rem;
        .repeat_input {
            margin-left: 32px !important;
        }
    }
    .schdule_tooltip {
        display: flex;
        align-items: center;
        font-size: 14px;
        margin-bottom: 5px;
        button {
            width: 20px;
            height: 20px;
            padding: 0;
            border-radius: 50%;
            background-color: #f0f4f6;
            color: #adadad;
        }
        a {
            padding: 0;
            border-bottom: none;
            padding-left: 6px;
        }
        .bg_tooltip {
            position: relative;
            display: inline-block;
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: #f0f4f6;
            color: #adadad;
            margin-left: 6px;
            text-align: center;
            margin-bottom: 5px;
            label {
                font-size: 15px;
                font-weight: bold;
            }
        }

        .bg_tooltip .tooltiptext {
            visibility: hidden;
            width: 242px;
            background-color: #fff;
            color: #03a9f4;
            border: 2px solid #03a9f4;
            text-align: left;
            border-radius: 6px;
            font-size: 12px;
            font-weight: 600;
            padding: 5px 16px;
            position: absolute;
            left: 26px;
            top: -50%;
            transform: translateY(-38%);
            z-index: 1;
        }
        .bg_tooltip .tooltiptextStart {
            transform: translateY(-26%);
        }

        .bg_tooltip .tooltiptextEnd {
            transform: translateY(-20%);
            width: 262px;
        }
        .bg_tooltip .tooltiptextHours {
            transform: translateY(-17%);
            width: 255px;
        }
        .bg_tooltip:hover .tooltiptext {
            visibility: visible;
        }
        .bg_tooltip .tooltiptext::after {
            content: " ";
            position: absolute;
            top: 50%;
            right: 99%;
            margin-top: -5px;
            border: solid #03a9f4;
            border-width: 0 2px 2px 0;
            display: inline-block;
            padding: 4px;
            transform: rotate(135deg);
            background-color: #ffff;
        }
    }
    .breakType {
        flex-wrap: wrap;

        .form-group {
            width: 100%;
            margin-bottom: 0;

            p {
                margin-bottom: 0;
                font-size: 14px;
                text-transform: capitalize;
            }

            select {
                border: 1px solid #95989a;
                height: 46px;
                width: 100%;
                &:focus-visible {
                    outline: none;
                }
            }
        }
    }
    .schedule_main_edit {
        margin-top: 20px;
    }

    .data {
        font-size: 14px;
    }

    .break_title {
        font-size: 14px;
        margin-bottom: 5px;
    }

    .schedule_main {
        padding: 0px 10px 14px 0px;

        .name {
            max-width: 100px;
        }

        .name, .data {
            font-size: 14px;
            display: flex;
            align-items: flex-start;
            min-height: 46px;
            padding-bottom: 0;
            flex-direction: column;
            justify-content: center;
            padding-left: 0;

            > div, p {
                width: 100%;
            }

            .switch_toggle {
                margin-top: 10px;
                margin-left: 20px;
            }
        }

        .switch_toggle {
            margin-top: 12px;
        }

        .schedule_timeSect {
            flex: 1;

            p {
                margin-bottom: 0;
                margin-top: 12px;
                font-size: 14px;
            }

            input {
                width: 100%;
                height: 46px;
                border: 1px solid #95989a;
                padding: 0 0.75rem;
                color: #000;
                &:focus {
                    outline: none;
                }
                font-size: 14px;

                &:disabled {
                    border-color: #c7c7c7;
                    background: #f8f8f8;
                    color: #999;
        
                    & + .input-group-addon {
                        border-color: #c7c7c7;
                    }
        
                    &~label {
                        color: #9da0a1;
                    }
                }
            }
        }
        .Select {
            .Select-placeholder, 
            .Select-input, 
            .Select-value-label {
                font-size: 14px;
            }
            .Select-arrow-zone {
                padding-top: 0;
            }
        }
        .break_button {
            .sl-c-btn--primary {
                display: flex;
                min-height: 38px;
                align-items: center;
                justify-content: center;
                cursor: pointer;
                span {
                    svg {
                        margin-right: 5px;
                    }
                }
            }
        }
        button {
            span {
                svg {
                    width: 12px;
                    margin-top: -5px;
                    display: inline-block;
                }
            }
        }
        .errorTimeIconcore {
            position: relative;
            span {
                position: absolute;
                top: 33px;
                right: 12px;
                color: #eb7575;
            }
            div {
                input {
                    border: 1px solid #eb7575 !important;
                }
            }
        }
        .timeIcon {
            position: relative;
            span {
                position: absolute;
                top: 10px;
                right: 12px;
            }

            svg {
                width: 18px;
            }
        }
        .coreTimeIcon {
            position: relative;
            min-height: 68px;
            display: flex;
            align-items: flex-start;
            justify-content: space-between;
            flex-direction: column;

            span.isvg {
                position: absolute;
                top: 32px;
                right: 12px;

                svg {
                    width: 18px;
                }
            }

            > div {
                width: 100%;
            }
        }
        .errorTimeIcon {
            position: relative;
            span {
                color: #eb7575;
            }
            span.isvg {
                position: absolute;
                top: 10px;
                right: 12px;

                svg {
                    width: 18px;
                }
            }
            div {
                input {
                    border: 1px solid #eb7575 !important;
                }
            }
            select {
                border: 1px solid #eb7575 !important;
            }

            .Select-control, .Select-input {
                border-color: #eb7575;

                input {
                    border-color: transparent !important;
                }
            }
        }
        .start_Time {
            background: #f8f8f8;
            cursor: not-allowed;
        }
        input {
            font-size: 14px;
        }

        &.core_time {
            display: flex;
            min-height: 88px;

            > div {
                flex: 1;

                > div {
                    flex: unset;
                    justify-content: flex-start;
                }
            }

            &.name {
                max-width: none;
            }

            &.data {
                justify-content: flex-start;
            }

            .core-data {
                margin-top: 25px;
            }
        }

        &.saved {
            .name {
                min-height: 112px;
            }

            .data {
                > div {
                    justify-content: space-between;
                }
            }

            &.btnEditSect.core_time {
                min-height: 125px;
            }
        }

        &.has-error {
            .break_title, .schdule_tooltip {
                color: #eb7575;
            }

            .input_pay, .DayPickerInput, .timeIcon {
                input {
                    border-color: #eb7575;
                }

                svg {
                    color: #eb7575;
                }
            }

            svg {
                color: #eb7575;
            }

            .error {
                color: #eb7575;
                font-size: 14px;
                margin-top: 5px;
            }
        }


    }
    
    .btnEditSect {
        display: flex;
        margin-bottom: 30px;
        .break_button {
            width: 50%;
        }
        .btnSave {
            color: #ffffff;
            background-color: #83d24b;
        }
        .btnClose {
            border-color: #eb7575;
        }

        &.core_time {
            min-height: 102px;
            align-items: center;
        }
    }
    .schedule_rows {
        margin-bottom: 0px;
    }
    .brecks_title .break_button button {
        width: 100%;
        padding: 7px;
        border-radius: 2rem;
        &:focus {
            outline: none;
        }
    }
    .add_switch_toggle label {
        margin-top: 16px;
    }

    .swtich-padding {
        margin: 1rem 0;
    }

    .add_checkswitch {
        display: flex;
        align-items: center;
    }
    .add_checkswitch label {
        margin-bottom: 0px;
        padding-right: 10px;
    }
    .add_checkswitch .switch_label {
        margin: 0;
        margin-left: 15px;
        font-size: 14px;
    }
    .add_switch .switch {
        position: relative;
        display: inline-block;
        width: 35px;
        height: 8px;
    }
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }
    .schedule_timeSect .DayPickerInput {
        width: 100%;
    }
    label,
    .break_type {
        margin-bottom: 0;
        font-size: 14px;
    }
    .DayPickerInput input {
        width: 100%;
    }
    .schedule_main p {
        margin-bottom: 0;
    }
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: 0.4s;
        transition: 0.4s;
    }
    .slider:before {
        position: absolute;
        content: "";
        height: 17px;
        width: 17px;
        left: 0px;
        top: -5px;
        background-color: #474747;
        -webkit-transition: 0.4s;
        transition: 0.4s;
    }
    input:checked + .slider {
        background: #cccccc;
    }
    input:checked + .slider:before {
        background: #83d24b;
        left: -10px;
        top: -5px;
    }
    input:focus + .slider {
        box-shadow: 0 0 1px #2196f3;
    }
    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .Input_from {
        margin-top: 14px;
        display: flex;
    }
    .input_check {
        display: flex;
        align-items: center;
        margin-right: 15px;
    }
    .input_check input {
        margin-right: 10px;
        width: 20px;
        height: 17px;
    }
    .input_check label {
        margin-bottom: 0px;
    }
    .input_employees {
        margin-top: 18px;

        p {
            margin-bottom: 5px;
            font-size: 14px;
        }
    }

    .tag_input {
        margin-top: 18px;

        .Select-multi-value-wrapper {
            .Select-placeholder {
                font-size: 14px;
            }
        }

        .Select-clear {
            display: none;
        }
    }

    .from_footer {
        margin: 14px 0px 0px;
        padding: 14px 0px;
        background: #f0f4f6;
    }
    .from_footer .footer_button {
        width: 100%;
        text-align: end;
    }
    .from_footer .footer_button button {
        padding: 14px 28px;
        border-radius: 2rem;
        cursor: pointer;

        &:focus {
            outline: none;
        }
    }
    .from_footer .footer_button .cancel {
        color: #474747;
        border: 1px solid #83d24b;
        background-color: #ffffff;
        margin-right: 5px;
    }
    .switch_report {
        margin-top: 1rem;
        font-size: 14px;
    }
    .input_repeat {
        display: flex;
        align-items: center;
    }
    .input_repeat .Select .Select-control {
        width: 5%;
    }
    .repeat_intervals {
        margin-top: 15px;
        display: flex;
        align-items: flex-end;
        flex-direction: row;

        > div {
            display: flex;
            flex-direction: column;
        }

        .radio-group {
            > div {
                height: 50px;
                display: flex;
                align-items: center;
                margin-bottom: 5px !important;
                color: #4e4e4e !important;
            }
        }

        .repeat_input {
            align-items: flex-start;

            .schedule_timeSect {
                height: 50px;
                display: flex;
                flex-direction: row;
                align-items: center;
                margin-bottom: 5px;

                &:first-child {
                    align-items: center;
                    flex-direction: row;
                }

                &:last-child {
                    div {
                        margin-bottom: 5px;

                        .label,
                        .error-message {
                            display: none;
                        }
                    }
                }
            }
        }
    }
    .repeat_intervals #After {
        float: left;
    }
    .repeat_intervals .repeat_input {
        margin-left: 75px;
    }
    .repeat_intervals #On {
        float: left;
    }
    .repeat_from {
        margin-top: 15px;
        display: flex;
        align-items: center;
    }
    .repeat_input {
        display: flex;
        align-items: center;
    }
    .repeat_input label {
        margin-left: 20px;
    }
    .repeat_titls label {
        margin-bottom: 0.25rem;
        font-size: 14px;
    }
    .repeat_ends label {
        margin: 15px 0px 5px;
    }
    .input_checkout {
        width: 70px;
    }
    .repeat_summary {
        p {
            font-weight: 600;
            font-size: 14px;
        }
    }
    .start-on {
        margin: 20px 0;
    }

    .Select {
        order: 2;

        &.Select--multi {
            .Select-menu-outer{
                .is-disabled {
                    font-weight: 600;
                }
            }

            .Select-control {
                .Select-clear-zone {
                    display: none;
                }

                .Select-multi-value-wrapper {
                    display: inline-flex;
                    align-items: center;
                    flex-wrap: wrap;

                    .Select-value {
                        display: flex;
                        flex-direction: row-reverse;
                        margin-top: 3px;
                        margin-bottom: 3px;
                        padding: 5px;
                        border-radius: 20px;
                        border: 1px solid #cccccc;
                        color: #474747;
                        background-color: #ffffff;
                        font-size: 14px;
                    }

                    .Select-value-icon {
                        border: none;
                        background-color: #f0f4f6;
                        padding: 0 7px;
                        font-weight: 700;
                        border-radius: 50%;

                        &:hover {
                            color: #FFFFFF;
                        }
                    }
                }

            }
        }

        .Select-control {
            border-color: #95989a;
            height: 46px;

            .Select-input {
                height: 44px;

                & > input {
                    line-height: 28px;
                    font-size: 1rem;
                    padding: 10px 0;
                }
            }

            .Select-value, 
            .Select-placeholder {
                vertical-align: baseline;
                font-size: 14px;
            }

            .Select-value {
                display: flex;
                align-items: center;
            }

            .Select-placeholder {
                line-height: 44px;
            }

            .Select-arrow-zone {
                padding-right: 12px;
                padding-top: 4px;
            }
        }

        .Select-menu-outer {
            margin-top: 2px;
            max-height: none;
            border: none;
            box-shadow: 0px 0px 15px -4px rgba(128,128,128,1);

            .Select-menu {
                .Select-option {
                    padding: 12px 16px;
                }
            }
        }

        &.is-open, &.is-focused, &.is-pseudo-focused {
            .Select-control {
                border-color: #0096d0;
                box-shadow: none;
                border-bottom-right-radius: 4px;
                border-bottom-left-radius: 4px;
            }
            &~label {
                color: #149ed3;
            }
        }

        &.is-disabled {
            .Select-control {
                cursor: not-allowed;
                background: #f8f8f8;
            }
            .Select-value-label {
                color: #d3d3d3 !important;
            }
            &~label {
                color: #9da0a1;
            }
        }

        &.is-error {
            .Select-control {
                border-color: #f21108;
            }

            &~label {
                color: #f2130a;
            }
        }
    }
`;
