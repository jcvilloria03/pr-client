import {
    calculateTotalTime,
    calculateTimeDifference,
    isTimeWithinTimeInterval,
    isTimeOutsideTimeInterval,
    isTimeIntervalsHasNoOverLap,
    isTimeOutsideOvertimeIntervals,
    isValidSubintervalWithinTimeInterval,
} from 'utils/dateTimeHelper';

import map from 'lodash/map';
import filter from 'lodash/filter';
import moment from 'moment';

import { TIME_OBJECT_KEYS, ERROR_MESSAGES } from './constants';

export const validation = (form, type, data) => {
    const errors = {};

    switch (type) {
        case TIME_OBJECT_KEYS.BREAKTIME: {
            if (!data.start && data.type !== 'floating') {
                errors.start = errors.start || ERROR_MESSAGES.TIME_REQUIRED;
            }

            if (!data.end && data.type !== 'floating') {
                errors.end = errors.end || ERROR_MESSAGES.TIME_REQUIRED;
            }

            if (!data.break_hours) {
                errors.end = errors.end || ERROR_MESSAGES.TIME_REQUIRED;
            }

            if (!data.type) {
                errors.type = errors.type || ERROR_MESSAGES.VALID_INPUT_REQUIRED;
            }

            if (data.start) {
                errors.start =
                    errors.start ||
                    (!withinSchedule(data.start, form) &&
                        ERROR_MESSAGES.WITHIN_SCHEDULE_TIME);
            }

            if (data.end) {
                errors.end =
                    errors.end ||
                    (!withinSchedule(data.end, form) &&
                        ERROR_MESSAGES.WITHIN_SCHEDULE_TIME);
            }

            if (data.start && data.end) {
                errors.start =
                    errors.start ||
                    (!validBreakTime(form, data) &&
                        ERROR_MESSAGES.BREAK_START_NO_OVERLAP);

                errors.end =
                    errors.end ||
                    (!validBreakTime(form, data) &&
                        ERROR_MESSAGES.BREAK_END_NO_OVERLAP);

                errors.break_hours =
                    errors.break_hours ||
                    (!validTotalBreaksHours(data.break_hours, form) &&
                        ERROR_MESSAGES.BREAK_EXCEEDED);

                errors.start =
                    errors.start ||
                    (!noOverlap(form[TIME_OBJECT_KEYS.BREAKTIME], data, 'start') &&
                        ERROR_MESSAGES.BREAK_INTERVAL_NO_OVERLAP);

                errors.end =
                    errors.end ||
                    (!noOverlap(form[TIME_OBJECT_KEYS.BREAKTIME], data, 'end') &&
                        ERROR_MESSAGES.BREAK_INTERVAL_NO_OVERLAP);
            }

            if (form.type === 'flexi' || form.type === 'floating') {
                if (!data.break_hours) {
                    errors.break_hours =
                        errors.break_hours || ERROR_MESSAGES.TIME_REQUIRED;
                }
            }

            if (data.break_hours) {
                errors.break_hours =
                    errors.break_hours ||
                    (!validTotalBreaksHours(data.break_hours, form) &&
                        ERROR_MESSAGES.BREAK_EXCEEDED);
            }

            break;
        }

        case TIME_OBJECT_KEYS.OVERTIME: {
            const refForm = {
                ...form,
                [TIME_OBJECT_KEYS.OVERTIME]: form[TIME_OBJECT_KEYS.OVERTIME].filter(
                    (overtime) => overtime.id !== data.id,
                ),
            };

            if (!data.start) {
                errors.start = ERROR_MESSAGES.TIME_REQUIRED;
            }

            if (!data.end) {
                errors.end = ERROR_MESSAGES.TIME_REQUIRED;
            }

            if (data.start) {
                if (!refForm.start_time) {
                    errors.start = ERROR_MESSAGES.OVERTIME_START_NO_OVERLAP;
                }

                errors.start =
                    errors.start ||
                    (!outsideSchedule(data.start, form, 'start') &&
                        ERROR_MESSAGES.OVERTIME_START_SCHEDULE_NO_OVERLAP);
                errors.start =
                    errors.start ||
                    (!outsideOvertimeIntervals(data.start, refForm) &&
                        ERROR_MESSAGES.OVERTIME_START_NO_OVERLAP);
            }

            if (data.end) {
                if (!refForm.end_time) {
                    errors.end = ERROR_MESSAGES.OVERTIME_END_NO_OVERLAP;
                }

                errors.end =
                    errors.end ||
                    (!outsideSchedule(data.end, form, 'end') &&
                        ERROR_MESSAGES.OVERTIME_END_SCHEDULE_NO_OVERLAP);
                errors.end =
                    errors.end ||
                    (!outsideOvertimeIntervals(data.end, refForm) &&
                        ERROR_MESSAGES.OVERTIME_END_NO_OVERLAP);
            }

            if (data.start && data.end) {
                errors.start =
                    errors.start ||
                    (!validOvertime(form, data) &&
                        ERROR_MESSAGES.OVERTIME_START_NO_OVERLAP);

                errors.end =
                    errors.end ||
                    (!validOvertime(form, data) &&
                        ERROR_MESSAGES.OVERTIME_END_NO_OVERLAP);
            }

            break;
        }

        case TIME_OBJECT_KEYS.CORETIME: {
            const refForm = {
                ...form,
                [TIME_OBJECT_KEYS.CORETIME]: form[TIME_OBJECT_KEYS.CORETIME].filter(
                    (core) => core.id !== data.id,
                ),
            };

            if (form.type === 'flexi' || data.type === 'floating') {
                if (!data.start) {
                    errors.start = ERROR_MESSAGES.TIME_REQUIRED;
                }

                if (!data.end) {
                    errors.end = ERROR_MESSAGES.TIME_REQUIRED;
                }

                if (data.start) {
                    errors.start =
                        errors.start ||
                        (!outsideOvertimeIntervals(data.start, refForm) &&
                            ERROR_MESSAGES.CORE_START_NO_OVERLAP);
                }

                if (data.end) {
                    errors.end =
                        errors.end ||
                        (!outsideOvertimeIntervals(data.end, refForm) &&
                            ERROR_MESSAGES.CORE_END_NO_OVERLAP);
                }

                if (data.start && data.end) {
                    errors.start =
                        errors.start ||
                        (!validBreakTime(refForm, data) &&
                            ERROR_MESSAGES.CORE_START_NO_OVERLAP);

                    errors.end =
                        errors.end ||
                        (!validBreakTime(refForm, data) &&
                            ERROR_MESSAGES.CORE_END_NO_OVERLAP);

                    errors.start =
                        errors.start ||
                        (!noOverlap(refForm[TIME_OBJECT_KEYS.CORETIME], data, 'start') &&
                            ERROR_MESSAGES.CORE_OUTSIDE_INTERVAL);

                    errors.end =
                        errors.end ||
                        (!noOverlap(refForm[TIME_OBJECT_KEYS.CORETIME], data, 'end') &&
                            ERROR_MESSAGES.CORE_OUTSIDE_INTERVAL);
                }

                if (data.start && !refForm.start_time) {
                    errors.start =
                        errors.start || ERROR_MESSAGES.WITHIN_SCHEDULE_TIME;
                }

                if (data.end && !refForm.end_time) {
                    errors.end =
                        errors.end || ERROR_MESSAGES.WITHIN_SCHEDULE_TIME;
                }

                if (data.start) {
                    errors.start =
                        errors.start ||
                        (!withinSchedule(data.start, refForm) &&
                            ERROR_MESSAGES.WITHIN_SCHEDULE_TIME);
                }

                if (data.end) {
                    errors.end =
                        errors.end ||
                        (!withinSchedule(data.end, refForm) &&
                            ERROR_MESSAGES.WITHIN_SCHEDULE_TIME);
                }
            }

            break;
        }

        case 'schedule': {
            if (!form.name) {
                errors.name = ERROR_MESSAGES.FIELD_REQUIRED;
            }

            if (!form.start_time) {
                errors.start_time = ERROR_MESSAGES.TIME_REQUIRED;
            }

            if (!form.end_time) {
                errors.end_time = ERROR_MESSAGES.TIME_REQUIRED;
            }

            if (!form.start_date) {
                errors.start_date = ERROR_MESSAGES.DATE_REQUIRED;
            }

            break;
        }

        default:
            break;
    }

    let errorMessages = {};

    Object.keys(errors).forEach((key) => {
        if (errors[key]) {
            errorMessages = {
                ...errorMessages,
                [key]: errors[key],
            };
        }
    });

    return errorMessages;
};

function validBreakTime(form, data) {
    const breakInterval = {
        start: data.start,
        end: data.end,
    };

    const scheduleInterval = {
        start: form.start_time,
        end: form.end_time,
    };

    return isValidSubintervalWithinTimeInterval(
        breakInterval,
        scheduleInterval,
    );
}

function withinSchedule(value, form) {
    if (!value) return true;
    if (!form.start_time || !form.end_time) return false;

    const scheduleInterval = {
        start: form.start_time,
        end: form.end_time,
    };

    return isTimeWithinTimeInterval(value, scheduleInterval);
}

/**
 * Validation to check if break hours have no overtime
 */
function validTotalBreaksHours(value, form) {
    if (!value) return true;
    if (form[TIME_OBJECT_KEYS.BREAKTIME].length > 1) return true;

    const breakHours = map(form[TIME_OBJECT_KEYS.BREAKTIME], 'break_hours');

    const totalScheduleHours = moment(
        calculateTimeDifference(form.start_time, form.end_time),
        'HH:mm',
    );

    if (
        totalScheduleHours >
        calculateTotalTime(filter(breakHours, { saved: true }))
    ) {
        return true;
    }
    return false;
}

function validOvertime(form, data) {
    const overtimeInterval = {
        start: data.start,
        end: data.end,
    };

    const scheduleInterval = {
        start: form.start_time,
        end: form.end_time,
    };

    return isTimeIntervalsHasNoOverLap(overtimeInterval, scheduleInterval);
}

function outsideSchedule(value, form) {
    if (!value) return true;
    if (!form.start_time || !form.end_time) return true;

    const scheduleInterval = {
        start: form.start_time,
        end: form.end_time,
    };

    return isTimeOutsideTimeInterval(value, scheduleInterval);
}

function outsideOvertimeIntervals(value, form) {
    if (!value) return true;

    const intervals = form[TIME_OBJECT_KEYS.OVERTIME].filter((item) => item.saved).map((item) => ({
        start: item.start,
        end: item.end,
    }));

    return isTimeOutsideOvertimeIntervals(
        value,
        intervals,
    );
}

function noOverlap(dataGroup, data, key) {
    let hasOverlap = false;
    dataGroup.forEach((item) => {
        if (item.id !== data.id && isTimeWithinTimeInterval(data[key], { start: item.start, end: item.end })) {
            hasOverlap = true;
        }
    });
    return !hasOverlap;
}
