export const TIME_OBJECT_KEYS = {
    OVERTIME: 'over_time',
    CORETIME: 'core_time',
    BREAKTIME: 'break_time',
};

export const DEFAULT_TIME_OBJECTS = {
    [TIME_OBJECT_KEYS.OVERTIME]: {
        end: null,
        start: null,
        editing: true,
        overtime_hours: null,
        saved: false,
        errors: {},
    },
    [TIME_OBJECT_KEYS.CORETIME]: {
        saved: false,
        editing: true,
        start: null,
        end: null,
        errors: {},
    },
    [TIME_OBJECT_KEYS.BREAKTIME]: {
        break_hours: null,
        end: null,
        is_paid: false,
        start: null,
        type: '',
        editing: true,
        saved: false,
        errors: {},
    },
};

export const ERROR_MESSAGES = {
    FIELD_REQUIRED: 'This field is required',
    TIME_REQUIRED: 'Please choose a time',
    DATE_REQUIRED: 'Please choose a date',
    VALID_DATE_REQUIRED: 'Please choose a valid date',
    VALID_INPUT_REQUIRED: 'Please input valid data',
    WITHIN_SCHEDULE_TIME:
        'Please input data within the schedule start and end times',
    BREAK_START_NO_OVERLAP:
        'Please input a valid start time that does not overlap with schedule and other break time settings',
    BREAK_END_NO_OVERLAP:
        'Please input a valid start time that does not overlap with schedule and other break time settings',
    OVERTIME_START_NO_OVERLAP:
        'Please input a valid start time that does not overlap with schedule and other overtime settings',
    OVERTIME_END_NO_OVERLAP:
        'Please input a valid end time that does not overlap with schedule and other overtime settings',
    CORE_START_NO_OVERLAP:
        'Please input a valid start time that does not overlap with schedule and other core time settings',
    CORE_END_NO_OVERLAP:
        'Please input a valid end time that does not overlap with schedule and other core time settings',
    BREAK_EXCEEDED:
        'The total number of break times are exceeded. Please input a valid data',
    CORE_OUTSIDE_INTERVAL: 'Please input data outside core times intervals',
    OVERTIME_START_SCHEDULE_NO_OVERLAP: 'Start time overlaps with schedule time setting',
    OVERTIME_END_SCHEDULE_NO_OVERLAP: 'End time overlaps with schedule time setting',
    BREAK_INTERVAL_NO_OVERLAP: 'Please input data outside break intervals',
};

export const DEFAULT_HEADERS = {
    'X-Authz-Entities': 'time_and_attendance.schedules'
};
