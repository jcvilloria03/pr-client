import styled, { keyframes } from 'styled-components';

const bounce = keyframes`
    0%, 20%, 50%, 80%, 100% {
        transform: translate3d(0, 0, 0);
    }
    40% {
        transform: translate3d(0, -30px, 0);
    }
    60% {
        transform: translate3d(0, -15px, 0);
    }
`;

export const Wrapper = styled.div`
    padding: 90px 0;
    display: flex;
    justify-content: center;
    height: 100vh;
`;

export const ContentWrapper = styled.div`
    display: flex;
    flex-direction: column
    justify-content: center;

    .svg-wrapper {
        align-self: center;

        > svg {
            width: 160px;
            height: auto;
        }

        &.animate > svg {
            animation: ${bounce} 2s infinite;
        }
    }

    .description {
        padding-top: 40px;
        text-align: center;

        a:not([href]):not([tabindex]) {
            color: #00A5E5;
        }
        h3 {
            font-weight: 600;
        }
        #downloadLink {
            display: none;
        }
    }
`;
