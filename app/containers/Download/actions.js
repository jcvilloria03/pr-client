import {
    GET_DOWNLOAD_URL
} from './constants';

/**
 * Fetch the temporary download url from server
 */
export function getDownloadUrl( module, id ) {
    return {
        type: GET_DOWNLOAD_URL,
        payload: {
            module,
            id
        }
    };
}
