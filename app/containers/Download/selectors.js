import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectViewDomain = () => ( state ) => state.get( 'download' );

const makeSelectInitiatingDownload = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'initiating' )
);

const makeSelectDownloadUrl = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'download_url' )
);

const makeSelectNotification = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    selectViewDomain,
    makeSelectInitiatingDownload,
    makeSelectDownloadUrl,
    makeSelectNotification
};
