/*
 *
 * Download constants
 *
 */

export const GET_DOWNLOAD_URL = 'app/Download/GET_DOWNLOAD_URL';
export const SET_DOWNLOAD_URL = 'app/Download/SET_DOWNLOAD_URL';
export const INITIATING_DOWNLOAD = 'app/Download/INITIATING_DOWNLOAD';

export const NOTIFICATION_SAGA = 'app/Download/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Download/NOTIFICATION';
