import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import axios from 'axios';
import has from 'lodash/has';

import { Fetch } from '../../utils/request';

import {
    INITIATING_DOWNLOAD,
    GET_DOWNLOAD_URL,
    SET_DOWNLOAD_URL,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../App/constants';
import { resetStore } from '../App/sagas';

/**
 * Fetch the temporary download link from server
 */
export function* getDownloadUrl({ payload }) {
    try {
        yield put({
            type: INITIATING_DOWNLOAD,
            payload: true
        });

        const { module, id } = payload;

        if ( module === 'payslips' ) {
            yield call( getPayslipDownloadLink, id );
        } else if ( module === 'masterfile' ) {
            yield call( getMasterfileDownloadLink, id );
        } else if (module === 'payroll_registers') {
            yield call(getZippedPayrollRegisterDownloadLink, id);
        } else {
            yield call( downloadFile, module, id );
        }

        yield put({
            type: SET_DOWNLOAD_URL,
            payload: ''
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: INITIATING_DOWNLOAD,
            payload: false
        });
    }
}

/**
 * Download file by type and ID
 * @param {String} type - Type of resource
 * @param {String} id - ID of the resource
 */
export function* downloadFile( type, id ) {
    try {
        const response = yield call( axios, `/download/${type}/${id}`, { method: 'GET' });

        const contentType = response.headers[ 'content-type' ];
        const mime = contentType.split( ';' )[ 0 ].split( '/' )[ 1 ];
        const extension = `.${mime}`;

        let fileName = `${type}${extension}`;
        if ( has( response, 'headers.content-disposition' ) ) {
            fileName = response.headers[ 'content-disposition' ].substring( 21 );
        }

        const link = document.createElement( 'a' );
        link.href = window.URL.createObjectURL( new Blob([response.data], { type: contentType }) );
        link.setAttribute( 'download', fileName );
        document.body.appendChild( link );
        link.click();
        document.body.removeChild( link );
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Gets the payslip download link for
 */
export function* getPayslipDownloadLink( id ) {
    try {
        const uriResponse = yield call( Fetch, `/payslip/zipped/${id}/url`, { method: 'GET' });

        yield put({
            type: SET_DOWNLOAD_URL,
            payload: uriResponse.uri
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Gets the masterfile download link for
 */
export function* getMasterfileDownloadLink( id ) {
    try {
        const uriResponse = yield call( Fetch, `/download/masterfile/${id}`, { method: 'GET' });
        yield put({
            type: SET_DOWNLOAD_URL,
            payload: uriResponse.data.uri
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Gets the zipped payroll register download link
 */
export function* getZippedPayrollRegisterDownloadLink( id ) {
    try {
        const uriResponse = yield call( Fetch, `/download/payroll_registers/${id}`, { method: 'GET' });
        yield put({
            type: SET_DOWNLOAD_URL,
            payload: uriResponse
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );
}

/**
 * Watcher for GET_DOWNLOAD_URL
 */
export function* watchForGetDownloadUrl() {
    const watcher = yield takeEvery( GET_DOWNLOAD_URL, getDownloadUrl );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetDownloadUrl,
    watchForNotifyUser,
    watchForReinitializePage
];
