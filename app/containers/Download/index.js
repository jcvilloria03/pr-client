import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import has from 'lodash/has';

import { browserHistory } from '../../utils/BrowserHistory';

import SnackBar from '../../components/SnackBar';
import A from '../../components/A';
import { H3 } from '../../components/Typography';

import * as viewActions from './actions';
import {
    makeSelectInitiatingDownload,
    makeSelectDownloadUrl,
    makeSelectNotification
} from './selectors';

import { Wrapper, ContentWrapper } from './styles';

/**
 * Download component
 */
export class Download extends React.Component {
    static propTypes = {
        getDownloadUrl: React.PropTypes.func,
        initiating: React.PropTypes.bool,
        downloadUrl: React.PropTypes.string,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        location: React.PropTypes.object
    }

    constructor( props ) {
        super( props );

        this.query = null;
    }

    /**
     * Perform actions after rendering the component
     */
    componentDidMount() {
        const downloadParams = this.getDownloadParams();
        this.query = downloadParams || this.props.location.query;

        if ( has( this.query, 'type' ) && has( this.query, 'uuid' ) ) {
            this.downloadFile();
        } else {
            browserHistory.replace( '/unauthorized' );
        }
    }

    /**
     * Perform actions when component props changes
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.downloadUrl.length
            && nextProps.downloadUrl !== this.props.downloadUrl
                && this.downloadFile( nextProps.downloadUrl );
    }

    /**
     * Get download params, if any
     * @returns {Object|null}
     */
    getDownloadParams() {
        const downloadParams = localStorage.getItem( 'download_params' );
        try {
            localStorage.removeItem( 'download_params' );
            return JSON.parse( downloadParams );
        } catch ( error ) {
            return null;
        }
    }

    /**
     * Downloads file to user
     */
    downloadFile = ( downloadUrl = this.props.downloadUrl ) => {
        const { type, uuid } = this.query;
        if ( downloadUrl  && typeof(downloadUrl) === 'string' ) {
            const linkElement = document.getElementById( 'downloadLink' );
            linkElement.href = downloadUrl;
            linkElement.click();
            linkElement.href = '';
        } else {
            this.props.getDownloadUrl( type, uuid );
        }
    }

    /**
     * Renders Download compoent to DOM
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Download Page"
                    meta={ [
                        { name: 'description', content: 'Description of Download Page' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Wrapper>
                    <ContentWrapper>
                        <div className={ `svg-wrapper${this.props.initiating ? ' animate' : ''}` }>
                            <svg xmlns="http://www.w3.org/2000/svg" width="195" height="195" viewBox="0 0 195 195">
                                <g fill="none">
                                    <circle cx="97.5" cy="97.5" r="97.5" fill="#3DA8E3" />
                                    <path d="M99 139.9L99 140 96 140 96 139.9 95.9 140 69 113.1 71.1 111 96 135.9 96 55 99 55 99 135.9 123.9 111 126 113.1 99.1 140 99 139.9Z" fill="#FFF" />
                                </g>
                            </svg>
                        </div>
                        <div className="description">
                            <H3>File will start downloading shortly</H3>
                            <p>
                                You may&nbsp;
                                <A onClick={ this.downloadFile } >
                                    retry downloading
                                </A>
                                <A download id="downloadLink"></A>
                                &nbsp;if it doesn&apos;t start automatically.
                            </p>
                        </div>
                    </ContentWrapper>
                </Wrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    initiating: makeSelectInitiatingDownload(),
    downloadUrl: makeSelectDownloadUrl(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        viewActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Download );
