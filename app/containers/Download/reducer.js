import { fromJS } from 'immutable';
import {
    INITIATING_DOWNLOAD,
    SET_DOWNLOAD_URL,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../App/constants';

const initialState = fromJS({
    initiating: false,
    download_url: '',
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Download Reducer
 *
 */
function downloadReducer( state = initialState, action ) {
    switch ( action.type ) {
        case INITIATING_DOWNLOAD:
            return state.set( 'initiating', action.payload );
        case SET_DOWNLOAD_URL:
            return state.set( 'download_url', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default downloadReducer;
