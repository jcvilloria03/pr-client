import {
    DEFAULT_ACTION, EDIT_LEAVEENTITLEMENTS, GET_LEAVEENTITLEMENTS, GET_LEAVETYPE, NOTIFICATION
} from './constants';

/**
 *
 * EditLeaveEntitlements actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Get LeaveEntitlements action
 *
 */
export function getLeaveEntitlement( payload ) {
    return {
        type: GET_LEAVEENTITLEMENTS,
        payload
    };
}

/**
 *
 * Get Leave type action
 *
 */
export function getLeaveType() {
    return {
        type: GET_LEAVETYPE
    };
}

/**
 *
 * Edit LeaveEntitlements action
 *
 */
export function editLeaveEntitlements( payload ) {
    return {
        type: EDIT_LEAVEENTITLEMENTS,
        payload
    };
}

/**
 *
 * Notification action
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
