import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';
import { EDIT_LEAVEENTITLEMENTS, GET_LEAVEENTITLEMENTS, GET_LEAVETYPE, NOTIFICATION, NOTIFICATION_SAGA, SET_EDIT_LEAVEENTITLEMENTS, SET_LEAVEENTITLEMENTS, SET_LEAVETYPE, SET_LOADING } from './constants';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';
import { browserHistory } from '../../../utils/BrowserHistory';

import { REINITIALIZE_PAGE } from '../../App/constants';

/**
 * GET Leave Entitlements
 */
export function* getLeaveEntitlements({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const res = yield call( Fetch, `/leave_entitlement/${payload}`, { method: 'GET' });

        yield put({
            type: SET_LEAVEENTITLEMENTS,
            payload: res
        });
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * GET Leave Types
 */
export function* getLeaveType() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/leave_types/search?credit_required=true`, { method: 'GET' });

        yield put({
            type: SET_LEAVETYPE,
            payload: res.data || []
        });
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * EDIT Leave Entitlements
 */
export function* editLeaveEntitlements({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const res = yield call( Fetch, `/leave_entitlement/${payload.id}`, { method: 'PUT', data: payload });

        yield put({
            type: SET_EDIT_LEAVEENTITLEMENTS,
            payload: res
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully saved!',
            show: 'true',
            type: 'success'
        });
        yield call( browserHistory.push( '/company-settings/leave-settings/leave-entitlement', true ) );

        yield put({
            type: SET_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize Page
 */
export function* reinitializePage() {
    yield call( getLeaveEntitlements );
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Watch for get LeaveEntitlements
 */
export function* watchForGetLeaveEntitlements() {
    const watcher = yield takeEvery( GET_LEAVEENTITLEMENTS, getLeaveEntitlements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for get LeaveEntitlements
 */
export function* watchForGetLeaveTypes() {
    const watcher = yield takeEvery( GET_LEAVETYPE, getLeaveType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For ReinitializePage
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for LeaveEntitlements
 */
export function* watchForLeaveEntitlements() {
    const watcher = yield takeEvery( EDIT_LEAVEENTITLEMENTS, editLeaveEntitlements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Notification
 */
export function* watchFroNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetLeaveEntitlements,
    watchForReinitializePage,
    watchForGetLeaveTypes,
    watchForLeaveEntitlements,
    watchFroNotification
];
