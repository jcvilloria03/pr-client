import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, NOTIFICATION_SAGA, SET_EDIT_LEAVEENTITLEMENTS, SET_LEAVEENTITLEMENTS, SET_LEAVETYPE, SET_LOADING
} from './constants';

const initialState = fromJS({
    getLeaveEntitlements: {},
    getLeaveType: [],
    editLeaveEntitlements: '',
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    }
});

/**
 *
 * EditLeaveEntitlements reducer
 *
 */
function editLeaveEntitlementsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LEAVEENTITLEMENTS:
            return state.set( 'getLeaveEntitlements', fromJS( action.payload ) );
        case SET_LEAVETYPE:
            return state.set( 'getLeaveType', fromJS( action.payload ) );
        case SET_EDIT_LEAVEENTITLEMENTS:
            return state.set( 'editLeaveEntitlements', action.payload );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default editLeaveEntitlementsReducer;
