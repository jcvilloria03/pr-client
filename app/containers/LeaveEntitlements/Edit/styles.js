import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;

    .icon-arrow {
        width: 14px;
        font-size: 14px;
        display: inline-block;
        margin-right: 4px;

        > i {
            align-self: center;
        }
    }

    .back-text {
        font-size: 14px;
    }
`;

export const PageWrapper = styled.div`
    padding-top: 120px;
    .content {
        margin-top: 40px;
        padding-bottom: 91px;
        .entriTitle{
            text-align:right;
        }
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 600;
            }
            h1 {
                text-align: center;
                font-size: 36px;
                font-weight: 700;
                line-height: 57.6px;
            }
            p{
                margin-bottom:0.25rem;
            }
        }
        input:focus~label,.Select.is-focused~label{
            color:#5b5b5b !important;
        }
        .submit {
            & > div {
                text-align: right;
                margin-top: 50px;

                & > button {
                    min-width: 160px;
                    margin-bottom: 50px;
                }
            }
        }
        .input_pay_error{
            input{
                border-color:#eb7575 !important;
            }
            label{
                color:#eb7575 !important;
            }
            input:focus~label,.Select.is-focused~label{
                color:#eb7575 !important;
            }
            span{
                color:#eb7575;
            }
            p{
                color:#eb7575;
                font-size: 14px;
            }
            .Select-control{
                border-color: #eb7575;
                box-shadow: unset;
            }
        }
        .entitled-employee-select {
            .Select-control{
                border-color: #95989a;
            }
        }
        .accrue-input {
            margin-top: 3px;
        }
        .update_conatin{
            margin-bottom: 1rem;
        }
        .select_section{
            margin-bottom: 1rem;
            
        }
        .title_leavetypes{
            margin-bottom: 2px;
            font-size: 14px;
        }
        .leave_typedeta{
            width: 50%;
            display: flex;
            flex-flow: wrap;
            .leave_typesdata{
                width: 50%;
                p{
                    margin-bottom: 1px;
                    .rc-checkbox-inner{
                        width: 14px;
                        height: 14px;
                        border-radius: 0px;
                        border: 1px solid #474747;
                    }
                    .label{
                        font-size: 14px;
                    }
                }
            }
        }
        .Accrual_title{
            padding: 0px 15px;
            color: #474747;
            font-weight: 600;
            font-size: 14px;
        }
        .select_date{
            position: relative;
            margin-top: 4px;

            .DayPickerInput{
                width: 100%;
                input{
                    width: 100%;
                }
            }
            .calender_icon{
                position: absolute;
                right: 12px;
                top: 32px;
                .icon{
                    display:block;
                    svg{
                        width:18px;
                    }
                }
            }
            .cal_icon{
                right:27px;
                top:32px;
            }
        }
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;
    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;
        > i {
            align-self: center;
        }
    }
    .message {
        display: flex;
        align-self: center;
    }
`;
