/*
 *
 * EditLeaveEntitlements constants
 *
 */
export const nameSpace = 'app/LeaveEntitlements/Edit';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const GET_LEAVEENTITLEMENTS = `${nameSpace}/GET_LEAVEENTITLEMENTS`;
export const SET_LEAVEENTITLEMENTS = `${nameSpace}/SET_LEAVEENTITLEMENTS`;
export const GET_LEAVETYPE = `${nameSpace}/GET_LEAVETYPE`;
export const SET_LEAVETYPE = `${nameSpace}/SET_LEAVETYPE`;
export const EDIT_LEAVEENTITLEMENTS = `${nameSpace}/EDIT_LEAVEENTITLEMENTS`;
export const SET_EDIT_LEAVEENTITLEMENTS = `${nameSpace}/SET_EDIT_LEAVEENTITLEMENTS`;
export const SET_LOADING = `${nameSpace}/SET_LOADING`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
