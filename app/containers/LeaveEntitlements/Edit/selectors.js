import { createSelector } from 'reselect';

/**
 * Direct selector to the editLeaveEntitlements state domain
 */
const selectEditLeaveEntitlementsDomain = () => ( state ) => state.get( 'editLeaveEntitlements' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by EditLeaveEntitlements
 */

const makeSelectEditLeaveEntitlements = () => createSelector(
  selectEditLeaveEntitlementsDomain(),
  ( substate ) => substate.toJS()
);

const makeSelecGetLeaveEntitlements = () => createSelector(
  selectEditLeaveEntitlementsDomain(),
  ( substate ) => substate.get( 'getLeaveEntitlements' ).toJS()
);

const makeSelecGetLeaveType = () => createSelector(
  selectEditLeaveEntitlementsDomain(),
  ( substate ) => substate.get( 'getLeaveType' ).toJS()
);

const makeSelecEditLeaveEntitlements = () => createSelector(
  selectEditLeaveEntitlementsDomain(),
  ( substate ) => substate.get( 'editLeaveEntitlements' )
);

const makeSelectLoading = () => createSelector(
  selectEditLeaveEntitlementsDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectEditLeaveEntitlementsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export default makeSelectEditLeaveEntitlements;
export {
  selectEditLeaveEntitlementsDomain,
  makeSelecGetLeaveEntitlements,
  makeSelecGetLeaveType,
  makeSelecEditLeaveEntitlements,
  makeSelectLoading,
  makeSelectNotification
};
