/* eslint-disable import/first */
/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';
import { clone, intersection } from 'lodash';

import A from 'components/A';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import { H3, H2, H1, P } from 'components/Typography';
import Input from 'components/Input';
import DayPickerInput from 'components/DatePicker';
import SalCheckbox from 'components/SalCheckbox';
import Select from 'components/Select';
import SnackBar from 'components/SnackBar';
import MultiSelect from 'components/MultiSelect';
import Loader from 'components/Loader';
import Icon from 'components/Icon';
import Modal from 'components/Modal';

import { UNITDATA, ACCRUEPERIODDATA, DATA1, RUNDATA, TYPEDATA, TERMINATIONDATA, DATE_FORMATS, PRODUCTS } from 'utils/constants';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';

import * as getAllActions from './actions';

import makeSelectEditLeaveEntitlements, {
    makeSelecEditLeaveEntitlements,
    makeSelecGetLeaveEntitlements,
    makeSelecGetLeaveType,
    makeSelectLoading,
    makeSelectNotification
} from './selectors';

import {
    PageWrapper,
    NavWrapper,
    Footer,
    LoadingStyles
} from './styles';

/**
 *
 * EditLeaveEntitlements
 *
 */
export class EditLeaveEntitlements extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
        getLeaveEntitlementList: React.PropTypes.object,
        getLeaveEntitlement: React.PropTypes.func,
        params: React.PropTypes.object,
        getLeaveType: React.PropTypes.func,
        getLeaveTypeList: React.PropTypes.array,
        editLeaveEntitlements: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            formData: {},
            employeeList: '',
            validAccrualPeriod: 'accrual_period',
            validAccrueEvery: 'accrue_every',
            validAccrueLeaveCredits: 'accrue_leave_credits',
            validAffectedEmployees: 'affected_employees',
            validLeaveConversionRun: 'leave_conversion_run',
            validLeaveConversionType: 'leave_conversion_type',
            validLeaveCreditUnit: 'leave_credit_unit',
            validLeaveTypeIds: 'leave_type_ids',
            validName: 'name',
            validTerminationLeaveConversionType: 'termination_leave_conversion_type',
            nameExist: 'name',
            validNameExist: '',
            showModel: false,
            payableLeaveTypesIds: []
        };
    }

    componentDidMount() {
        this.props.getLeaveEntitlement( this.props.params.id );
        this.props.getLeaveType();
    }

    componentWillReceiveProps( nextProps ) {
        const payableLeaveTypesIds = nextProps.getLeaveTypeList.filter( ( leaveType ) => leaveType.payable ).map( ( leaveType ) => leaveType.id );

        this.setState({
            formData: {
                ...nextProps.getLeaveEntitlementList,
                affected_employees: nextProps.getLeaveEntitlementList.affected_employees &&
                    nextProps.getLeaveEntitlementList.affected_employees.map( ( value ) => ({
                        value: value.id,
                        label: value.name,
                        type: value.type,
                        uid: `${value.type}${value.id}`
                    }) ),
                leave_type_ids: nextProps.getLeaveEntitlementList.leave_types && nextProps.getLeaveEntitlementList.leave_types.map( ( val ) => val.id ),
                accrue_date: nextProps.getLeaveEntitlementList.accrue_date === '0000-00-00' ? null : moment( new Date( nextProps.getLeaveEntitlementList.accrue_date ) ).format( 'YYYY-MM-DD' ),
                leave_conversion_run_date: nextProps.getLeaveEntitlementList.leave_conversion_run_date === '0000-00-00' ? null : moment( new Date( nextProps.getLeaveEntitlementList.leave_conversion_run_date ) ).format( 'YYYY-MM-DD' )
            },
            payableLeaveTypesIds
        });
    }

    handleSubmit = async () => {
        const companyId = company.getLastActiveCompanyId();
        if ( this.validateFields() ) {
            const formData = {
                ...this.state.formData,
                leave_entitlement_id: this.state.formData.id,
                leave_type_ids: this.state.formData.leave_type_ids,
                affected_employees: this.state.formData.affected_employees.map( ( value ) => ({
                    id: value.value,
                    name: value.label,
                    type: value.type
                }) )
            };
            const res = await Fetch( `/company/${companyId}/leave_entitlement/is_name_available`, { method: 'POST', data: formData });
            this.setState({ validNameExist: res.available });
            if ( res.available ) {
                this.props.editLeaveEntitlements( formData );
            } else {
                this.setState({
                    nameExist: ''
                });
            }
        }
    }

    AssignedEmployees = ( keyword, callback ) => {
        const companyId = company.getLastActiveCompanyId();
        Fetch( `/company/${companyId}/affected_employees/search?term=${keyword}&limit=10&without_users=yes`, { method: 'GET' })
        .then( ( result ) => {
            const list = result.map( ( data ) => ({
                value: data.id,
                label: data.name,
                type: data.type,
                uid: `${data.type}${data.id}`
            }) );
            callback( null, { options: list });
        })
        .catch( ( error ) => callback( error, null ) );
    }

    validateFields = () => {
        let valid = true;
        if ( this.state.formData.name === '' ) {
            this.setState({
                validName: ''
            });
            valid = false;
        }

        if ( this.state.formData.affected_employees && this.state.formData.affected_employees.map( ( value ) => value ).length <= 0 ) {
            this.setState({
                validAffectedEmployees: ''
            });
            valid = false;
        }
        if ( this.state.formData.leave_type_ids && this.state.formData.leave_type_ids.map( ( value ) => value ).length <= 0 ) {
            this.setState({
                validLeaveTypeIds: ''
            });
            valid = false;
        }
        if ( this.state.formData.accrue_leave_credits === '' ) {
            this.setState({
                validAccrueLeaveCredits: ''
            });
            valid = false;
        }
        if ( this.state.formData.leave_credit_unit === '' ) {
            this.setState({
                validLeaveCreditUnit: ''
            });
            valid = false;
        }
        if ( this.state.formData.accrue_every === '' ) {
            this.setState({
                validAccrueEvery: ''
            });
            valid = false;
        }
        if ( this.state.formData.accrual_period === '' ) {
            this.setState({
                validAccrualPeriod: ''
            });
            valid = false;
        }
        if ( this.state.formData.leave_conversion_run === '' ) {
            this.setState({
                validLeaveConversionRun: ''
            });
            valid = false;
        }
        if ( this.state.formData.leave_conversion_type === '' ) {
            this.setState({
                validLeaveConversionType: ''
            });
            valid = false;
        }
        if ( this.state.formData.termination_leave_conversion_type === '' ) {
            this.setState({
                validTerminationLeaveConversionType: ''
            });
            valid = false;
        }
        return valid;
    }

    hasPayrollProduct() {
        const products = auth.getProducts();

        if ( products ) {
            return ( products.includes( PRODUCTS.PAYROLL ) || products.includes( PRODUCTS.BOTH ) );
        }

        return false;
    }

    /**
     *
     * EditLeaveEntitlements render method
     *
     */
    render() {
        const date = new Date();

        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const { notification, loading } = this.props;
        const { payableLeaveTypesIds, formData, validName, validTerminationLeaveConversionType, nameExist, validLeaveCreditUnit, validLeaveConversionRun, validLeaveConversionType, validAccrualPeriod, validAccrueEvery, validAccrueLeaveCredits } = this.state;
        const { leave_type_ids } = formData;

        const LEAVE_CONVERSION_TYPE_DATA = clone( TYPEDATA );
        const LEAVE_TERMINATION_DATA = clone( TERMINATIONDATA );

        if ( this.hasPayrollProduct() && intersection( leave_type_ids, payableLeaveTypesIds ).length > 0 ) {
            const forPayroll = {
                value: 'For Payroll',
                label: 'For Payroll'
            };

            LEAVE_CONVERSION_TYPE_DATA.unshift( forPayroll );
            LEAVE_TERMINATION_DATA.unshift( forPayroll );
        }

        return (
            <div>
                <div>
                    <Helmet
                        title="Edit Leave Entitlement"
                        meta={ [
                        { name: 'description', content: 'Description of Edit Leave Entitlement' }
                        ] }
                    />
                    <Sidebar items={ sidebarLinks } />
                    <SnackBar
                        message={ notification.message }
                        title={ notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ notification.show }
                        delay={ 5000 }
                        type={ notification.type }
                    />
                    <Modal
                        title="Discard Changes"
                        body={
                            <div>
                                <p> Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p>
                            </div>
                        }
                        buttons={ [
                            {
                                id: 'buttonCancel',
                                type: 'grey',
                                label: 'Stay on this page',
                                onClick: () => this.confirmDiscardModal.toggle()
                            },
                            {
                                id: 'buttonProceed',
                                type: 'darkRed',
                                label: 'Yes',
                                onClick: () => browserHistory.push( '/company-settings/leave-settings/leave-entitlement', true )
                            }
                        ] }
                        showClose={ false }
                        ref={ ( ref ) => { this.confirmDiscardModal = ref; } }
                        className="modal-md modal-commission-type"
                        center
                    />
                    <NavWrapper>
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/leave-settings/leave-entitlement', true );
                                } }
                            >
                                <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Leave Entitlements</span>
                            </A>
                        </Container>
                    </NavWrapper>
                    <PageWrapper>
                        <Container className="editLeave">
                            <div className="content">
                                <div className="loader" style={ { display: loading ? '' : 'none' } }>
                                    <LoadingStyles>
                                        <H2>Loading Edit Leave Entitlements</H2>
                                        <br />
                                        <H3>Please wait...</H3>
                                    </LoadingStyles>
                                </div>
                                <div className="content" style={ { display: loading ? 'none' : '' } }>
                                    <div className="heading">
                                        <H1 noBottomMargin>Edit Leave Entitlement</H1>
                                        <P noBottomMargin>View and update leave entitlements. You can add, edit, or delete leave entitlements.</P>
                                    </div>

                                    <div className="w-100">
                                        <div className="row">
                                            <div className={ validName === '' ? 'col-md-4 input_pay_error' : nameExist === '' ? 'col-md-4 input_pay_error' : 'col-md-4' }>
                                                <div className="update_conatin">
                                                    <Input
                                                        id="name"
                                                        label="* Leave entitlement name"
                                                        value={ formData.name }
                                                        onChange={ ( value ) => this.setState({ validName: value, formData: { ...formData, name: value }}) }
                                                    />
                                                    {validName === '' ? <p>This field is required</p> : nameExist === '' ? <p>Record name already exists</p> : ''}
                                                </div>
                                            </div>
                                            <div className={ this.state.formData.affected_employees && this.state.formData.affected_employees.map( ( value ) => value ).length <= 0 ? 'col-md-12 input_pay_error' : 'col-md-12 entitled-employee-select' }>
                                                <div className="select_section">
                                                    <MultiSelect
                                                        id="Employee"
                                                        async
                                                        loadOptions={ this.AssignedEmployees }
                                                        label="* Entitled Employee/s"
                                                        value={ formData.affected_employees }
                                                        onChange={ ( value ) => {
                                                            this.setState({
                                                                validAffectedEmployees: value,
                                                                formData:
                                                                { ...this.state.formData,
                                                                    affected_employees: value }
                                                            });
                                                        } }
                                                        ref={ ( ref ) => { this.employee = ref; } }
                                                        placeholder="Enter location, department name, position, employee name"
                                                    />
                                                </div>
                                                {this.state.formData.affected_employees && this.state.formData.affected_employees.map( ( value ) => value ).length <= 0
                                                    ? <p>This field is required</p>
                                                    : ''
                                                }
                                            </div>
                                            <div className={ this.state.formData.leave_type_ids && this.state.formData.leave_type_ids.map( ( value ) => value ).length <= 0 ? 'col-md-12 input_pay_error' : 'col-md-12' }>
                                                <p className="title_leavetypes">* Leave types</p>
                                                <div className="leave_typedeta">
                                                    {this.props.getLeaveTypeList.map( ( value ) =>
                                                        <div className="leave_typesdata" key={ value.id }>
                                                            <p>
                                                                <SalCheckbox
                                                                    id={ `${value.id}` }
                                                                    name={ value.name }
                                                                    checked={ formData.leave_type_ids && formData.leave_type_ids.map( ( val ) => val ).includes( value.id ) }
                                                                    onChange={ () => {
                                                                        formData.leave_type_ids.includes( value.id )
                                                                            ? this.setState({ formData: {
                                                                                ...formData,
                                                                                leave_type_ids: formData.leave_type_ids.filter( ( val ) => val !== value.id )
                                                                            }})
                                                                            : this.setState({ formData: {
                                                                                ...formData,
                                                                                leave_type_ids: [ ...formData.leave_type_ids, value.id ]
                                                                            }});
                                                                    } }
                                                                >{value.name}</SalCheckbox>
                                                            </p>
                                                        </div>
                                                    )}
                                                </div>
                                                {this.state.formData.leave_type_ids && this.state.formData.leave_type_ids.map( ( value ) => value ).length <= 0 ? <p>Please select at least one leave type</p> : null}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <p className="Accrual_title">Accrual Setup</p>
                                            <div className="col-md-12">
                                                <div className={ validAccrueLeaveCredits === '' ? 'col-md-4  input_pay_error p-0 accrue-input' : 'col-md-4 p-0 accrue-input' }>
                                                    <Input
                                                        id="name"
                                                        label="* Number of leave credits to accrue "
                                                        type="number"
                                                        value={ formData.accrue_leave_credits }
                                                        min={ 0 }
                                                        onChange={ ( value ) => this.setState({ validAccrueLeaveCredits: value, formData: { ...formData, accrue_leave_credits: value }}) }
                                                    />
                                                    {validAccrueLeaveCredits === '' ? <p>Please input valid data</p> : null}
                                                </div>
                                                <div className={ validLeaveCreditUnit === '' ? 'col-md-4 input_pay_error' : 'col-md-4' }>
                                                    <Select
                                                        id="basis"
                                                        label="* Unit"
                                                        data={ UNITDATA }
                                                        value={ formData.leave_credit_unit }
                                                        onChange={ ( value ) => this.setState({ validLeaveCreditUnit: value, formData: { ...formData, leave_credit_unit: value.value }}) }
                                                    />
                                                    {validLeaveCreditUnit === '' ? <p>Please input valid data</p> : null}
                                                </div>
                                            </div>
                                            <div className="col-md-12 mt-1">
                                                <div className={ formData.accrual_period !== 'Once-Off' && validAccrueEvery === '' ? 'col-md-4 input_pay_error p-0 accrue-input' : 'col-md-4 p-0 accrue-input' }>
                                                    <Input
                                                        id="name"
                                                        label="* Accrue every"
                                                        type="number"
                                                        value={ formData.accrue_every }
                                                        disabled={ formData.accrual_period === 'Once-Off' }
                                                        min={ 0 }
                                                        onChange={ ( value ) => this.setState({ validAccrueEvery: value, formData: { ...formData, accrue_every: value }}) }
                                                    />
                                                    {formData.accrual_period !== 'Once-Off' && validAccrueEvery === '' ? <p>Please input valid data</p> : null}
                                                </div>
                                                <div className={ validAccrualPeriod === '' ? 'col-md-4 input_pay_error' : 'col-md-4' }>
                                                    <Select
                                                        id="basis"
                                                        label="* Accrue period"
                                                        data={ ACCRUEPERIODDATA }
                                                        value={ formData.accrual_period }
                                                        onChange={ ( value ) => {
                                                            this.setState({ validAccrualPeriod: value,
                                                                formData: {
                                                                    ...formData,
                                                                    accrual_period: value.value,
                                                                    accrue_every: value.value === 'Once-Off' ? '1' : formData.accrue_every,
                                                                    accrue_date: value.value === 'Once-Off' ? formData.accrue_date === null ? date : formData.accrue_date : formData.accrue_date
                                                                }});
                                                        } }
                                                    />
                                                    {validAccrualPeriod === '' ? <p>Please input valid data</p> : null}
                                                </div>
                                                {formData.accrual_period === 'Once-Off'
                                                    ? <div className="col-md-4 p-0">
                                                        <div className="select_date">
                                                            <DayPickerInput
                                                                dayFormat={ DATE_FORMATS.DISPLAY }
                                                                selectedDay={ formData.accrue_date }
                                                                onChange={ ( value ) => this.setState({ formData: {
                                                                    ...formData,
                                                                    accrueDate: value,
                                                                    accrue_date: moment( new Date( value ) ).format( 'YYYY-MM-DD' )
                                                                }}) }
                                                            />
                                                            <div className="calender_icon">
                                                                <Icon name="calendar" className="icon" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    : null
                                                }
                                            </div>
                                            <div className="col-md-12 mt-1">
                                                <div className="col-md-4 p-0 accrue-input">
                                                    <Input
                                                        id="name"
                                                        label="Start accruing after"
                                                        type="number"
                                                        value={ formData.start_accruing_after }
                                                        disabled={ formData.accrual_period === 'Once-Off' }
                                                        min={ 0 }
                                                        onChange={ ( value ) => this.setState({ formData: { ...formData, start_accruing_after: parseFloat( value ) }}) }
                                                    />
                                                </div>
                                                <div className="col-md-4">
                                                    <Select
                                                        id="basis"
                                                        label="."
                                                        data={ DATA1 }
                                                        value={ formData.after_accrual_period }
                                                        disabled={ formData.accrual_period === 'Once-Off' }
                                                        onChange={ ( value ) => this.setState({ formData: { ...formData, after_accrual_period: value.value }}) }
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <p className="Accrual_title">Conversion Setup</p>
                                            <div className="col-md-12">
                                                <div className={ validLeaveConversionRun === '' ? 'col-md-4 input_pay_error p-0' : 'col-md-4 p-0' }>
                                                    <Select
                                                        id="basis"
                                                        label="* Leave conversion run"
                                                        data={ RUNDATA }
                                                        value={ formData.leave_conversion_run }
                                                        onChange={ ( value ) => this.setState({ validLeaveConversionRun: value,
                                                            formData: {
                                                                ...formData,
                                                                leave_conversion_run: value.value,
                                                                leave_conversion_run_date: value.value === 'Specific date' ? formData.leave_conversion_run_date === null ? date : formData.leave_conversion_run_date : formData.leave_conversion_run_date
                                                            }}) }
                                                    />
                                                    {validLeaveConversionRun === '' ? <p>Please input valid data</p> : null}
                                                </div>
                                                {formData.leave_conversion_run === 'Specific date'
                                                    ? <div className="col-md-4 select_date">
                                                        <DayPickerInput
                                                            dayFormat={ DATE_FORMATS.DISPLAY }
                                                            selectedDay={ formData.leave_conversion_run_date }
                                                            onChange={ ( value ) => this.setState({ formData: {
                                                                ...formData,
                                                                leaveConversionRunDate: value,
                                                                leave_conversion_run_date: moment( new Date( value ) ).format( 'YYYY-MM-DD' )
                                                            }}) }
                                                        />
                                                        <div className="calender_icon cal_icon">
                                                            <Icon name="calendar" className="icon" />
                                                        </div>
                                                    </div>
                                                    : null
                                                }
                                            </div>
                                            <div className="col-md-12 mt-1">
                                                <div className={ validLeaveConversionType === '' ? 'col-md-4 input_pay_error p-0' : 'col-md-4 p-0' }>
                                                    <Select
                                                        id="basis"
                                                        label="* Leave conversion type"
                                                        data={ LEAVE_CONVERSION_TYPE_DATA }
                                                        value={ formData.leave_conversion_type }
                                                        onChange={ ( value ) => this.setState({ validLeaveConversionType: value, formData: { ...formData, leave_conversion_type: value.value }}) }
                                                    />
                                                    {validLeaveConversionType === '' ? <p>Please input valid data</p> : null}
                                                </div>
                                            </div>
                                            <div className="col-md-12 mt-1">
                                                <div className={ validTerminationLeaveConversionType === '' ? 'col-md-4 input_pay_error p-0' : 'col-md-4 p-0' }>
                                                    <Select
                                                        id="basis"
                                                        label="* Termination leave conversion type"
                                                        data={ LEAVE_TERMINATION_DATA }
                                                        value={ formData.termination_leave_conversion_type }
                                                        onChange={ ( value ) => this.setState({ validTerminationLeaveConversionType: value, formData: { ...formData, termination_leave_conversion_type: value.value }}) }
                                                    />
                                                    {validTerminationLeaveConversionType === '' ? <p>Please input valid data</p> : null}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Container>
                    </PageWrapper>
                </div>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.confirmDiscardModal.toggle() }
                            />
                            <Button
                                label={ loading ? <Loader /> : 'Update' }
                                type="action"
                                size="large"
                                disabled={ loading }
                                onClick={ () => this.handleSubmit() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    EditLeaveEntitlements: makeSelectEditLeaveEntitlements(),
    getLeaveEntitlementList: makeSelecGetLeaveEntitlements(),
    getLeaveTypeList: makeSelecGetLeaveType(),
    editLeaveEntitlementList: makeSelecEditLeaveEntitlements(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        getAllActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( EditLeaveEntitlements );
