import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { GET_ADD_LEAVE, GET_DATA, LOADING, NOTIFICATION, NOTIFICATION_SAGA, SET_ADD_LEAVE, SET_DATA } from './constants';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';
import { browserHistory } from '../../../utils/BrowserHistory';

import { REINITIALIZE_PAGE } from '../../App/constants';

/**
 * get Leave data
 */
export function* getdata() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/leave_types/search?credit_required=true`, { method: 'GET' });
        yield put({
            type: SET_DATA,
            payload: res.data || []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}
/**
 * get Leave data
 */
export function* addLeaveEntitilement({ payload }) {
    try {
        yield put({
            type: SET_ADD_LEAVE,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const data = {
            company_id: companyId,
            ...payload
        };
        yield call( Fetch, '/leave_entitlement', { method: 'POST', data });
        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully added!',
            show: true,
            type: 'success'
        });
        browserHistory.push( '/company-settings/leave-settings/leave-entitlement', true );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_ADD_LEAVE,
            payload: false
        });
    }
}
 /**
 * notifyUser
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}
/**
* reinitialize
*/
export function* reinitializePage() {
    yield call( getdata );
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
* watchForReinitializePage
*/
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * watchForNotifyUser
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for GET_TEAMS
 *
 */
export function* watchForGetData() {
    const watcher = yield takeEvery( GET_DATA, getdata );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for add data
 *
 */
export function* watchforaddLeaveEntitilement() {
    const watcher = yield takeEvery( GET_ADD_LEAVE, addLeaveEntitilement );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetData,
    watchForReinitializePage,
    watchforaddLeaveEntitilement,
    watchForNotifyUser
];
