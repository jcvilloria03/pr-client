import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, LOADING, NOTIFICATION_SAGA, SET_ADD_LEAVE, SET_DATA
} from './constants';

const initialState = fromJS({
    data: [],
    addData: {},
    loading: true,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Add reducer
 *
 */
function addReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_DATA:
            return state.set( 'data', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_ADD_LEAVE:
            return state.set( 'addData', action.payload );
        default:
            return state;
    }
}

export default addReducer;
