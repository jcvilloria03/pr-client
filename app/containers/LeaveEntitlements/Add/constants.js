/*
 *
 * Add constants
 *
 */

export const DEFAULT_ACTION = 'app/LeaveEntitlements/Add/DEFAULT_ACTION';
export const GET_DATA = 'app/LeaveEntitlements/Add/GET_DATA';
export const SET_DATA = 'app/LeaveEntitlements/Add/SET_DATA';
export const GET_ADD_LEAVE = 'app/LeaveEntitlements/Add/GET_ADD_LEAVE';
export const SET_ADD_LEAVE = 'app/LeaveEntitlements/Add/SET_ADD_LEAVE';
export const LOADING = 'app/LeaveEntitlements/Add/SET_ADD_LEAVE';
export const NOTIFICATION = 'app/LeaveEntitlements/Add/NOTIFICATION ';
export const NOTIFICATION_SAGA = 'app/LeaveEntitlements/Add/NOTIFICATION_SAGA ';
