import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;

    .icon-arrow {
        width: 14px;
        font-size: 14px;
        display: inline-block;
        margin-right: 4px;

        > i {
            align-self: center;
        }
    }

    .back-text {
        font-size: 14px;
    }
`;

export const PageWrapper = styled.div`
    padding-top: 120px;
    .content {
        margin-top: 40px;
        padding-bottom: 91px;
        .entriTitle{
            text-align:right;
        }
        .leaveField{
            margin-bottom: 3px;
            font-size: 14px;
            .rc-checkbox-inner{
                width: 14px;
                height: 14px;
                border-radius: 0px;
                border: 1px solid #474747;
            }
        }
        input:focus~label,.Select.is-focused~label{
            color:#5b5b5b !important;
        }
        .heading {
            h3{
                text-align: center;
                font-size: 24px;
                font-weight: bold;
            }
            h6{
                text-align: center;
                font-size: 16px
            }
            h4 {
                font-weight: bold;
                font-size: 14px;
            }

            h1 {
                text-align: center;
                font-size: 36px;
                font-weight: 700;
                line-height: 57.6px;
            }

            p{
                text-align: center;
                margin-bottom:0.25rem;
            }
        }
        .accrue-every {
            margin-top: 3px;
        }
        .DayPickerInput{
            width: 100%;

            input {
                width: 100%;
            }
        }
        .calender{
            position:relative;

            .date-period {
                margin-top: 4px;
            }

            .icon {
                position:absolute;
                right: 16px;
                top: 32px;
                display:block;

                svg {
                    width:18px;
                }
            }
        }
        .date_calender{
            position:relative;

            .date-period {
                margin-top: 4px;
            }

            .icon {
                position:absolute;
                right: 28px;
                top: 32px;
                display:block;

                svg {
                    width:18px;
                }
            }
        }
        .cpMTyx input{
            border-color: #000;
        }
        .RsTJf .Select .Select-control{
            border-color: #000;
        }
        .giQTap .Select .Select-control{
            border-color: #000;

        }
        .comm-detail{
            .col-md-2{
                padding: 0 0.5rem 1rem;
            }
            h5{
                font-size: 16px;
                color: #474747;
                font-weight: 700;
                line-height:25px;
            }
            input{
                padding: 7px;
                color:#474747;
                border-color: #000;
                font-weight: 400;
                &::placeholder{
                    color:#cbc7c7 !important;
                    font-size:14px;
                    font-weight:400;
                }
            }
             .Select .Select-control{
                border-color: red;
                font-size:14px;
                width: 488px !important;
            }
            .taxable-input input{
                border-color:#adadad;
            }
            .switch-input{
                margin-top:10px;
            }
        
        }
        .input_pay{
            input{   
                border-color:#eb7575 !important;
            }
            input:focus~label{
                color:#eb7575 !important;
            }
            label{
                color:#eb7575 !important;
            }
            span{
                color:#eb7575;
            }
            p{
                color:#eb7575;
            }
            .Select-control{
                border-color: #eb7575 !important;
                box-shadow: unset;
            }
        }
        .entitled-employee-select {
            .Select-control{
                border-color: #95989a;
            }
        }
        .select_error{
            label{
                color:#eb7575 !important;
            }
            .Select-control{
                border-color: #eb7575 !important;
                box-shadow: unset;
            }
           .Select.is-focused~label{
                color:#eb7575 !important;
            }
        }
  
        .addType{
            height: 38px;
            border-radius: 50px;
            width: 226px;
            font-size:15px;
            svg{
                width:13px;
                margin-top:-2px;
                margin-right:3px;
            }
        }
        .deletebtn{
            height: 38px;
            border-radius: 50px;
            width: 226px;
            font-size:15px;
            border: 1px solid #eb7575;
            background-color:unset !important;
            color:#474747;
            svg{
                width:13px;
                margin-top:-2px;
                margin-right:3px;
            }
        }

        .submit {
            & > div {
                text-align: right;
                margin-top: 50px;

                & > button {
                    min-width: 160px;
                    margin-bottom: 50px;
                }
            }
        }
    }

    .setup_heading{
        font-width: bold;
        padding-top: 10px;
        color: #000;
    }
    .unit_list{
        marginTop: 50px;
    }
    
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;
    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;
        > i {
            align-self: center;
        }
    }
    .message {
        display: flex;
        align-self: center;
    }
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;
