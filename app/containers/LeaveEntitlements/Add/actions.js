import {
    DEFAULT_ACTION,
    GET_ADD_LEAVE,
    GET_DATA,
    NOTIFICATION
} from './constants';
/**
 *
 * Add actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * Leave get data
 */
export function getLeaveAction() {
    return {
        type: GET_DATA
    };
}
/**
 * add leave
 */
export function addLeaveEntitilement( payload ) {
    return {
        type: GET_ADD_LEAVE,
        payload
    };
}
/**
 * Display a notification to a user
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

