/* eslint-disable import/first */
/* eslint-disable react/prop-types */
/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';
import { clone, intersection } from 'lodash';

import A from 'components/A';
import SnackBar from 'components/SnackBar';
import { H3, H4, H2, H1, P } from 'components/Typography';
import Input from 'components/Input';
import DayPickerInput from 'components/DatePicker';
import MultiSelect from 'components/MultiSelect';
import SalCheckbox from 'components/SalCheckbox';
import Select from 'components/Select';
import Sidebar from 'components/Sidebar';
import Loader from 'components/Loader';
import Button from 'components/Button';
import Icon from 'components/Icon';
import Modal from 'components/Modal';

import {
    UNITDATA,
    ACCRUEPERIODDATA,
    DATA1,
    RUNDATA,
    TYPEDATA,
    TERMINATIONDATA,
    DATE_FORMATS,
    PRODUCTS
} from 'utils/constants';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { auth } from 'utils/AuthService';

import * as LeaveActions from './actions';
import makeSelectAdd, {
    makeSelectaddData,
    makeSelectdataList,
    makeSelectLoading,
    makeSelectNotification
} from './selectors';

import {
    PageWrapper,
    NavWrapper,
    LoadingStyles,
    Footer
} from './styles';

/**
 *
 * Add
 *
 */
export class Add extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        dataList: React.PropTypes.array,
        getLeaveAction: React.PropTypes.func,
        addData: React.PropTypes.object,
        addLeaveEntitilement: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    static defaultProps = {
        loading: true
    };
    /**
     *
     * Add render method
     *
     */

    constructor( props ) {
        super( props );
        const date = new Date();
        this.state = {
            formData: {
                accrual_period: '',
                accrueDate: '',
                accrue_date: date,
                accrue_every: '',
                accrue_leave_credits: '',
                after_accrual_period: '',
                affected_employees: null,
                leaveConversionRunDate: '',
                leave_conversion_run: '',
                leave_conversion_run_date: date,
                leave_conversion_type: '',
                leave_credit_unit: '',
                leave_type_ids: [],
                name: '',
                start_accruing_after: '',
                termination_leave_conversion_type: ''
            },
            validAccrualPeriod: 'accrual_period',
            validAccrueEvery: 'accrue_every',
            validAccrueLeaveCredits: 'accrue_leave_credits',
            validAffectedEmployees: 'affected_employees',
            validLeaveConversionRun: 'leave_conversion_run',
            validLeaveConversionType: 'leave_conversion_type',
            validLeaveCreditUnit: 'leave_credit_unit',
            validName: 'name',
            validLeaveTypeIds: 'LeaveTypeIds',
            validTerminationLeaveConversionType: 'termination_leave_conversion_type',
            exitName: false,
            showModel: false,
            payableLeaveTypesIds: []
        };
    }

    componentDidMount() {
        this.props.getLeaveAction();
    }

    componentWillReceiveProps( nextProps ) {
        const payableLeaveTypesIds = nextProps.dataList.filter( ( leaveType ) => leaveType.payable ).map( ( leaveType ) => leaveType.id );
        this.setState({ payableLeaveTypesIds });
    }

    AssignedEmployees = ( keyword, callback ) => {
        const companyId = company.getLastActiveCompanyId();
        Fetch( `/company/${companyId}/affected_employees/search?term=${keyword}&limit=10&without_users=yes`, { method: 'GET' })
        .then( ( result ) => {
            const list = result.map( ( data ) => ({
                value: data.id,
                label: data.name,
                type: data.type,
                uid: `${data.type}${data.id}`
            }) );
            callback( null, { options: list });
        })
        .catch( ( error ) => callback( error, null ) );
    }

    handleSubmit = async () => {
        if ( this.validateFields() ) {
            const data1 = {
                accrual_period: this.state.formData.accrual_period,
                accrueDate: this.state.formData.accrueDate,
                accrue_date: this.state.formData.accrual_period === 'Once-Off' ? moment( new Date( this.state.formData.accrue_date ) ).format( 'YYYY-MM-DD' ) : null,
                accrue_every: this.state.formData.accrue_every,
                accrue_leave_credits: this.state.formData.accrue_leave_credits,
                after_accrual_period: this.state.formData.after_accrual_period,
                affected_employees: this.state.formData.affected_employees.map( ( val ) => ({
                    id: val.value,
                    name: val.label,
                    type: val.type,
                    uid: val.uid
                }) ),
                leaveConversionRunDate: this.state.formData.leaveConversionRunDate,
                leave_conversion_run: this.state.formData.leave_conversion_run,
                leave_conversion_run_date: this.state.formData.leave_conversion_run === 'Specific date' ? moment( new Date( this.state.formData.leave_conversion_run_date ) ).format( 'YYYY-MM-DD' ) : null,
                leave_conversion_type: this.state.formData.leave_conversion_type,
                leave_credit_unit: this.state.formData.leave_credit_unit,
                leave_type_ids: this.state.formData.leave_type_ids,
                name: this.state.formData.name,
                start_accruing_after: this.state.formData.start_accruing_after === '' ? 0 : this.state.formData.start_accruing_after,
                termination_leave_conversion_type: this.state.formData.termination_leave_conversion_type

            };
            const companyId = company.getLastActiveCompanyId();
            const data = await Fetch( `/company/${companyId}/leave_entitlement/is_name_available`, { method: 'POST', data: this.state.formData });
            if ( data.available === true ) {
                this.props.addLeaveEntitilement( data1 );
            } else {
                this.setState({
                    exitName: true
                });
            }
        }
    }

    validateFields = () => {
        let valid = true;
        if ( this.state.formData.name === '' ) {
            this.setState({
                validName: ''
            });
            valid = false;
        }
        if ( this.state.formData.affected_employees === null || this.state.formData.affected_employees.length === 0 ) {
            this.setState( ( prevState ) => ({
                validAffectedEmployees: '',
                formData: { ...prevState.formData, affected_employees: []}
            }) );
            valid = false;
        }
        if ( this.state.formData.leave_type_ids.map( ( value ) => value ).length <= 0 ) {
            this.setState({
                validLeaveTypeIds: ''
            });
            valid = false;
        }
        if ( this.state.formData.accrue_leave_credits === '' ) {
            this.setState({
                validAccrueLeaveCredits: ''
            });
            valid = false;
        }
        if ( this.state.formData.leave_credit_unit === '' ) {
            this.setState({
                validLeaveCreditUnit: ''
            });
            valid = false;
        }
        if ( this.state.formData.accrue_every === '' ) {
            this.setState({
                validAccrueEvery: ''
            });
            valid = false;
        }
        if ( this.state.formData.accrual_period === '' ) {
            this.setState({
                validAccrualPeriod: ''
            });
            valid = false;
        }
        if ( this.state.formData.leave_conversion_run === '' ) {
            this.setState({
                validLeaveConversionRun: ''
            });
            valid = false;
        }
        if ( this.state.formData.leave_conversion_type === '' ) {
            this.setState({
                validLeaveConversionType: ''
            });
            valid = false;
        }
        if ( this.state.formData.termination_leave_conversion_type === '' ) {
            this.setState({
                validTerminationLeaveConversionType: ''
            });
            valid = false;
        }
        return valid;
    }

    hasPayrollProduct() {
        const products = auth.getProducts();

        if ( products ) {
            return ( products.includes( PRODUCTS.PAYROLL ) || products.includes( PRODUCTS.BOTH ) );
        }

        return false;
    }

    render() {
        const date = new Date();
        const { loading } = this.props;
        const { payableLeaveTypesIds, formData } = this.state;
        const { leave_type_ids } = formData;

        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const LEAVE_CONVERSION_TYPE_DATA = clone( TYPEDATA );
        const LEAVE_TERMINATION_DATA = clone( TERMINATIONDATA );

        if ( this.hasPayrollProduct() && intersection( leave_type_ids, payableLeaveTypesIds ).length > 0 ) {
            const forPayroll = {
                value: 'For Payroll',
                label: 'For Payroll'
            };

            LEAVE_CONVERSION_TYPE_DATA.unshift( forPayroll );
            LEAVE_TERMINATION_DATA.unshift( forPayroll );
        }

        return (
            <div>
                <Helmet
                    title="Add Leave Entitlement"
                    meta={ [
                        { name: 'description', content: 'Description of Add Leave Entitlement' }
                    ] }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Discard Changes"
                    body={
                        <div>
                            <p> Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonCancel',
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.confirmDiscardModal.toggle()
                        },
                        {
                            id: 'buttonProceed',
                            type: 'darkRed',
                            label: 'Yes',
                            onClick: () => browserHistory.push( '/company-settings/leave-settings/leave-entitlement', true )
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.confirmDiscardModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/company-settings/leave-settings/leave-entitlement', true );
                            } }
                        >
                            <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Leave Entitlements</span>
                        </A>
                    </Container>
                </NavWrapper>
                <PageWrapper>
                    <Container style={ { paddingBottom: '95px' } } className="addLeave" >
                        <div className="loader" style={ { display: loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Add Leave Entitlement.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: loading ? 'none' : '' } }>
                            <div className="heading">
                                <H1 noBottomMargin>Add Leave Entitlement</H1>
                                <P noBottomMargin>View and update leave entitlements. You can add, edit, or delete leave entitlements.</P>
                            </div>

                            <div style={ { marginTop: '55px' } }>
                                <div className="col-md-12">
                                    <div className={ this.state.validName === '' ? 'col-md-4 p-0 input_pay' : 'col-md-4 p-0' }>
                                        <Input
                                            id="name"
                                            name="name"
                                            label="* Leave entitlement name"
                                            ref={ ( ref ) => { this.names = ref; } }
                                            onChange={ ( val ) => {
                                                this.setState({
                                                    validName: val,
                                                    exitName: false,
                                                    formData: { ...this.state.formData, name: val }
                                                });
                                            } }
                                        />
                                        {this.state.validName === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>This field is required</p> : ''}
                                        {this.state.exitName ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Record name already exists</p> : '' }
                                    </div>
                                </div>

                                <div className="col-md-12">
                                    <div className={ this.state.formData.affected_employees && this.state.formData.affected_employees.map( ( value ) => value ).length <= 0 ? 'col-md-12 p-0 select_error mt-1' : 'col-md-12 p-0 mt-1 entitled-employee-select' }>
                                        <MultiSelect
                                            id="assignedemployees"
                                            async
                                            loadOptions={ this.AssignedEmployees }
                                            label="* Entitled Employee/s"
                                            ref={ ( ref ) => { this.assignedemployees = ref; } }
                                            placeholder="Enter location, department name,position,employee name"
                                            onChange={ ( val ) => {
                                                this.setState({
                                                    formData: { ...this.state.formData, affected_employees: val }
                                                });
                                            } }
                                        />
                                    </div>
                                    {this.state.formData.affected_employees && this.state.formData.affected_employees.map( ( value ) => value ).length <= 0 ? <p style={ { color: '#eb7575', fontSize: '14px' } }>This field is required</p> : ''}

                                    <div className={ this.state.validLeaveTypeIds === '' ? 'col-md-6 input_pay' : 'col-md-6' } style={ { marginTop: '15px', padding: '0' } }>
                                        <p>* Leave types</p>
                                        <div className="row" >
                                            {this.props.dataList.map( ( value ) => (
                                                <div className="col-md-6" key={ value.id }>
                                                    <p className="leaveField">
                                                        <SalCheckbox
                                                            onChange={ () => {
                                                                this.state.formData.leave_type_ids.includes( value.id )
                                                                    ? this.setState({
                                                                        formData: {
                                                                            ...this.state.formData,
                                                                            leave_type_ids: this.state.formData.leave_type_ids.filter( ( val ) => val !== value.id )
                                                                        },
                                                                        validLeaveTypeIds: this.state.formData.leave_type_ids
                                                                    })
                                                                    : this.setState({
                                                                        formData: {
                                                                            ...this.state.formData,
                                                                            leave_type_ids: [ ...this.state.formData.leave_type_ids, value.id ]
                                                                        },
                                                                        validLeaveTypeIds: this.state.formData.leave_type_ids
                                                                    });
                                                            } }
                                                            checked={ this.state.formData.leave_type_ids && this.state.formData.leave_type_ids.map( ( val ) => val ).includes( value.id ) }
                                                        >{
                                                            value.name}
                                                        </SalCheckbox>
                                                    </p>
                                                </div>
                                            ) )}
                                        </div>
                                        {this.state.validLeaveTypeIds === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>This field is required</p> : ''}
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <div className="setup">
                                        <div className={ this.state.validAccrueLeaveCredits === '' ? 'col-md-4 input_pay p-0' : 'col-md-4 p-0' }>
                                            <div className="heading">
                                                <H4>Accrual Setup</H4>
                                            </div>
                                            <div>
                                                <Input
                                                    id="name"
                                                    name="name"
                                                    type="number"
                                                    label="* Number of leave credits to accrue"
                                                    ref={ ( ref ) => { this.names = ref; } }
                                                    onChange={ ( val ) => {
                                                        this.setState({
                                                            validAccrueLeaveCredits: val,
                                                            formData: { ...this.state.formData, accrue_leave_credits: val }
                                                        });
                                                    } }
                                                    min={ 0 }
                                                />
                                            </div>
                                            {this.state.validAccrueLeaveCredits === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please Input valid Data</p> : ''}
                                        </div>
                                        <div className={ this.state.validLeaveCreditUnit === '' ? 'col-md-4 input_pay' : 'col-md-4' }>
                                            <p style={ { paddingTop: '39px', margin: '0px', fontSize: '14px' } }>* Unit</p>
                                            <Select
                                                data={ UNITDATA }
                                                id="leader"
                                                ref={ ( ref ) => { this.leader = ref; } }
                                                onChange={ ( val ) => {
                                                    this.setState({
                                                        validLeaveCreditUnit: val,
                                                        formData: { ...this.state.formData, leave_credit_unit: val.value }
                                                    });
                                                } }
                                            />
                                            {this.state.validLeaveCreditUnit === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please Input valid Data</p> : ''}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 mt-1">
                                    <div className={ this.state.formData.accrual_period !== 'Once-Off' && this.state.validAccrueEvery === '' ? 'col-md-4 input_pay p-0 accrue-every' : 'col-md-4 p-0 accrue-every' }>
                                        <Input
                                            label="* Accrue every"
                                            id="name"
                                            name="name"
                                            type="number"
                                            ref={ ( ref ) => { this.names = ref; } }
                                            onChange={ ( val ) => {
                                                this.setState({
                                                    validAccrueEvery: val,
                                                    formData: { ...this.state.formData, accrue_every: val }
                                                });
                                            } }
                                            min={ 0 }
                                            disabled={ this.state.formData.accrual_period === 'Once-Off' }
                                        />
                                        {this.state.formData.accrual_period !== 'Once-Off' && this.state.validAccrueEvery === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please Input valid Data</p> : ''}
                                    </div>
                                    <div className={ this.state.validAccrualPeriod === '' ? 'col-md-4 input_pay' : 'col-md-4' }>
                                        <div>
                                            <Select
                                                label="*  Accrue period"
                                                data={ ACCRUEPERIODDATA }
                                                id="leader"
                                                ref={ ( ref ) => { this.leader = ref; } }
                                                onChange={ ( val ) => {
                                                    this.setState({
                                                        validAccrualPeriod: val,
                                                        formData: { ...this.state.formData,
                                                            accrual_period: val.value,
                                                            accrue_every: val.value === 'Once-Off' ? '1' : this.state.formData.accrue_every
                                                        }
                                                    });
                                                } }
                                            />
                                        </div>
                                        {this.state.validAccrualPeriod === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please Input valid Data</p> : ''}
                                    </div>
                                    {this.state.formData.accrual_period === 'Once-Off'
                                        ? <div className="calender">
                                            <div className="col-md-4 p-0 date-period">
                                                <DayPickerInput
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    selectedDay={ this.state.formData.accrue_date }
                                                    disabledDays={ { before: date } }
                                                    onChange={ ( value ) => this.setState({ formData: {
                                                        ...this.state.formData,
                                                        accrueDate: value,
                                                        accrue_date: moment( new Date( value ) ).format( 'YYYY-MM-DD' )
                                                    }}) }
                                                />
                                                <div>
                                                    <Icon name="calendar" className="icon" />
                                                </div>
                                            </div>
                                        </div>
                                        : null
                                    }
                                </div>
                                <div className="col-md-12 mt-1">
                                    <div className="col-md-4 p-0">
                                        <div>
                                            <Input
                                                label="Start accruing after"
                                                id="name"
                                                name="name"
                                                type="number"
                                                ref={ ( ref ) => { this.names = ref; } }
                                                onChange={ ( val ) => {
                                                    this.setState({
                                                        formData: { ...this.state.formData, start_accruing_after: val }
                                                    });
                                                } }
                                                min={ 0 }
                                                disabled={ this.state.formData.accrual_period === 'Once-Off' }
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div style={ { marginTop: '25px' } }>
                                            <Select
                                                data={ DATA1 }
                                                id="leader"
                                                ref={ ( ref ) => { this.leader = ref; } }
                                                onChange={ ( val ) => {
                                                    this.setState({
                                                        formData: { ...this.state.formData, after_accrual_period: val.value }
                                                    });
                                                } }
                                                disabled={ this.state.formData.accrual_period === 'Once-Off' }
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="heading col-md-12 mt-1"><H4 className="mb-0">Conversion Setup</H4></div>
                                <div className="col-md-12 mt-1">
                                    <div className={ this.state.validLeaveConversionRun === '' ? 'col-md-4 input_pay p-0' : 'col-md-4 p-0' }>
                                        <Select
                                            label="* Leave conversion run"
                                            data={ RUNDATA }
                                            id="leader"
                                            ref={ ( ref ) => { this.leader = ref; } }
                                            onChange={ ( val ) => {
                                                this.setState({
                                                    validLeaveConversionRun: val,
                                                    formData: { ...this.state.formData, leave_conversion_run: val.value }
                                                });
                                            } }
                                        />
                                        {this.state.validLeaveConversionRun === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please Input valid Data</p> : ''}
                                    </div>
                                    {this.state.formData.leave_conversion_run === 'Specific date'
                                        ? <div className="date_calender">
                                            <div className="col-md-4 date-period">
                                                <DayPickerInput
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    selectedDay={ this.state.formData.leave_conversion_run_date }
                                                    disabledDays={ { before: date } }
                                                    onChange={ ( value ) => this.setState({ formData: {
                                                        ...this.state.formData,
                                                        leaveConversionRunDate: value,
                                                        leave_conversion_run_date: moment( new Date( value ) ).format( 'YYYY-MM-DD' )
                                                    }}) }
                                                />
                                                <div>
                                                    <Icon name="calendar" className="icon" />
                                                </div>
                                            </div>
                                        </div>
                                        : null
                                    }
                                </div>
                                <div className="col-md-12 mt-1">
                                    <div className={ this.state.validLeaveConversionType === '' ? 'col-md-4 input_pay p-0' : 'col-md-4 p-0' }>
                                        <Select
                                            label="* Leave conversion type"
                                            data={ LEAVE_CONVERSION_TYPE_DATA }
                                            id="leader"
                                            ref={ ( ref ) => { this.leader = ref; } }
                                            onChange={ ( val ) => {
                                                this.setState({
                                                    validLeaveConversionType: val,
                                                    formData: { ...this.state.formData, leave_conversion_type: val.value }
                                                });
                                            } }
                                        />
                                        {this.state.validLeaveConversionType === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please Input valid Data</p> : ''}
                                    </div>
                                </div>
                                <div className="col-md-12 mt-1">
                                    <div className={ this.state.validTerminationLeaveConversionType === '' ? 'col-md-4 input_pay p-0' : 'col-md-4 p-0' }>
                                        <Select
                                            label="* Termination leave conversion type"
                                            data={ LEAVE_TERMINATION_DATA }
                                            id="leader"
                                            ref={ ( ref ) => { this.leader = ref; } }
                                            onChange={ ( val ) => {
                                                this.setState({
                                                    validTerminationLeaveConversionType: val,
                                                    formData: { ...this.state.formData, termination_leave_conversion_type: val.value }
                                                });
                                            } }
                                        />
                                        {this.state.validTerminationLeaveConversionType === '' ? <p style={ { color: '#eb7575', fontSize: '14px' } }>Please Input valid Data</p> : ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
                <Footer>
                    <div className="pull-right">
                        <Button
                            label="Cancel"
                            type="action"
                            size="large"
                            alt
                            onClick={ () => this.confirmDiscardModal.toggle() }
                        />
                        <Button
                            id="submitButton"
                            label={ loading ? <Loader /> : 'Submit' }
                            type="action"
                            size="large"
                            onClick={ () => this.handleSubmit() }
                            disabled={ loading }
                        />
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    Add: makeSelectAdd(),
    dataList: makeSelectdataList(),
    addData: makeSelectaddData(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        LeaveActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Add );
