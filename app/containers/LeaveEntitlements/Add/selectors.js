import { createSelector } from 'reselect';

/**
 * Direct selector to the add state domain
 */
const selectAddLeaveEntitlementsDomain = () => ( state ) => state.get( 'addLeaveEntitlements' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by Add
 */

const makeSelectAdd = () => createSelector(
  selectAddLeaveEntitlementsDomain(),
  ( substate ) => substate.toJS()
);
const makeSelectdataList = () => createSelector(
  selectAddLeaveEntitlementsDomain(),
  ( substate ) => substate.get( 'data' ).toJS()
);
const makeSelectaddData = () => createSelector(
  selectAddLeaveEntitlementsDomain(),
  ( substate ) => substate.get( 'addData' )
);
const makeSelectLoading = () => createSelector(
  selectAddLeaveEntitlementsDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectNotification = () => createSelector(
  selectAddLeaveEntitlementsDomain(),
( substate ) => substate.get( 'notification' ).toJS()
);

export default makeSelectAdd;
export {
  selectAddLeaveEntitlementsDomain,
  makeSelectdataList,
  makeSelectaddData,
  makeSelectLoading,
  makeSelectNotification
};
