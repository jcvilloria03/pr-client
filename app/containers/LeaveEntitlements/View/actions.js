import {
    DEFAULT_ACTION, DELETE_LEAVEENTITLEMENTS, GET_LEAVEENTITLEMENTS, NOTIFICATION
} from './constants';

/**
 *
 * LeaveEntitlements actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Get LeaveEntitlements action
 *
 */
export function getLeaveEntitlement() {
    return {
        type: GET_LEAVEENTITLEMENTS
    };
}

/**
 *
 * Delete LeaveEntitlements action
 *
 */
export function deleteLeaveEntitlements( payload ) {
    return {
        type: DELETE_LEAVEENTITLEMENTS,
        payload
    };
}

/**
 *
 * Notification action
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
