import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';
import { DELETE_LEAVEENTITLEMENTS, GET_LEAVEENTITLEMENTS, NOTIFICATION, NOTIFICATION_SAGA, SET_LEAVEENTITLEMENTS, SET_LOADING } from './constants';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

import { REINITIALIZE_PAGE } from '../../App/constants';

/**
 * GET Leave Entitlements
 */
export function* getLeaveEntitlements() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/leave_entitlements`, { method: 'GET' });

        yield put({
            type: SET_LEAVEENTITLEMENTS,
            payload: res.data || []
        });
        yield put({
            type: SET_LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize Page
 */
export function* reinitializePage() {
    yield call( getLeaveEntitlements );
}

/**
 * Delete Leave Entitlements
 */
export function* deleteLeaveEntitlements({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const formData = new FormData();
        formData.append( '_method', 'DELETE' );
        formData.append( 'company_id', companyId );
        for ( const i of payload ) {
            formData.append( 'leave_entitlements_ids[]', i );
        }

        yield call( Fetch, '/leave_entitlement/bulk_delete', {
            method: 'POST',
            data: formData
        });

        yield call( getLeaveEntitlements );

        yield call( notifyUser, {
            title: 'Deleted',
            message: 'Record successfully deleted!',
            show: 'true',
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: 'true',
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Watch for get LeaveEntitlements
 */
export function* watchForGetLeaveEntitlements() {
    const watcher = yield takeEvery( GET_LEAVEENTITLEMENTS, getLeaveEntitlements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For ReinitializePage
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For Delete LeaveEntitlements
 */
export function* watchForDeleteLeaveEntitlements() {
    const watcher = yield takeEvery( DELETE_LEAVEENTITLEMENTS, deleteLeaveEntitlements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Notification
 */
export function* watchFroNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetLeaveEntitlements,
    watchForReinitializePage,
    watchFroNotification,
    watchForDeleteLeaveEntitlements
];
