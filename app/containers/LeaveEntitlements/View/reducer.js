import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, NOTIFICATION_SAGA, SET_DELETE_LEAVEENTITLEMENTS, SET_LEAVEENTITLEMENTS, SET_LOADING
} from './constants';

const initialState = fromJS({
    getLeaveEntitlements: [],
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    }
});

/**
 *
 * LeaveEntitlements reducer
 *
 */
function leaveEntitlementsReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LEAVEENTITLEMENTS:
            return state.set( 'getLeaveEntitlements', fromJS( action.payload ) );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_DELETE_LEAVEENTITLEMENTS:
            return state.set( action.payload );
        default:
            return state;
    }
}

export default leaveEntitlementsReducer;
