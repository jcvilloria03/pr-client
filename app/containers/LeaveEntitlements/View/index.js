import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { isEqual } from 'lodash';

import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';
import Sidebar from 'components/Sidebar';
import SalDropdown from 'components/SalDropdown';
import SnackBar from 'components/SnackBar';
import Button from 'components/Button';
import Table from 'components/Table';
import Modal from 'components/Modal';
import { getIdsOfSelectedRows } from 'components/Table/helpers';
import { H3, H5, H2, H1, P } from 'components/Typography';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { browserHistory } from 'utils/BrowserHistory';

import * as getAllActions from './actions';
import makeSelectLeaveEntitlements, {
    makeSelecGetLeaveEntitlements,
    makeSelectLoading,
    makeSelectNotification
} from './selectors';

import {
    PageWrapper,
    LoadingStyles
} from './styles';

/**
 *
 * LeaveEntitlements
 *
 */
export class LeaveEntitlements extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        getLeaveEntitlementList: React.PropTypes.array,
        getLeaveEntitlement: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        deleteLeaveEntitlements: React.PropTypes.func
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            show: false,
            showModel: false,
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            }
        };
    }

    componentDidMount() {
        this.props.getLeaveEntitlement();
    }

    componentWillReceiveProps( nextProps ) {
        const listChanged = !isEqual( nextProps.getLeaveEntitlementList, this.props.getLeaveEntitlementList );

        this.setState( ( prevState ) => ({
            displayedData: nextProps.getLeaveEntitlementList,
            label: formatPaginationLabel( this.LeaveEntitlementsTable.tableComponent.state ),
            pagination: {
                total: nextProps.getLeaveEntitlementList.length,
                per_page: 10,
                current_page: listChanged ? 1 : prevState.pagination.current_page,
                last_page: Math.ceil( nextProps.getLeaveEntitlementList.length / 10 ),
                to: 0
            }
        }) );
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.LeaveEntitlementsTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.LeaveEntitlementsTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    handleTableChanges = ( tableProps = this.LeaveEntitlementsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    leaveEntitlementsDelete = () => {
        const payload = getIdsOfSelectedRows( this.LeaveEntitlementsTable );
        this.props.deleteLeaveEntitlements( payload );
        // setTimeout( () => {
        //     this.props.getLeaveEntitlement();
        // }, 2000 );
        this.confirmDeleteModal.toggle();
        // this.setState({ show: false, showModel: false });
    }

    employeeData = ( row ) => {
        if ( ( !row || row === '' ) && ( row.length > 0 ) ) {
            return '';
        }
        if ( row.length === 1 ) {
            return row[ 0 ];
        }
        return row.length > 2 ? `${row[ 0 ]} and ${row.length - 1} more` : `${row[ 0 ]} and ${row[ 1 ]}`;
    }

    leaveTypeData = ( row ) => {
        if ( ( !row || row === '' ) && ( row.length > 0 ) ) {
            return '';
        }
        if ( row.length === 1 ) {
            return row[ 0 ];
        }
        return row.length > 2 ? `${row[ 0 ]} and ${row.length - 1} more` : `${row[ 0 ]} and ${row[ 1 ]}`;
    }

    /**
     *
     * LeaveEntitlements render method
     *
     */
    render() {
        const tableColumn = [
            {
                id: 'name',
                header: 'Leave Entitlement Name',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    <div>{row.name}</div>
                )
            },
            {
                id: 'entitled',
                header: 'Entitled Employees',
                sortable: false,
                minWidth: 200,
                render: ({ row }) => (
                    <div>{this.employeeData( row.affected_employees.map( ( value ) => value.name ) )}</div>
                )
            },
            {
                id: 'types',
                header: 'Leave Types',
                sortable: false,
                minWidth: 120,
                render: ({ row }) => (
                    <div>{this.leaveTypeData( row.leave_types.map( ( value ) => value.name ) )}</div>
                )
            },
            {
                id: 'credits',
                header: () => <div className="credits-header">Number of Leave Credits to Accrue</div>,
                sortable: false,
                width: 280,
                render: ({ row }) => (
                    <div className="credits-cell">{row.accrue_leave_credits}</div>
                )
            },
            {
                id: 'unit',
                header: 'Leave Credit Unit',
                sortable: false,
                minWidth: 150,
                render: ({ row }) => (
                    <div>{row.leave_credit_unit}</div>
                )
            },
            {
                id: 'accrue',
                header: () => <div className="accrue-header">Accrue Every</div>,
                sortable: false,
                width: 150,
                render: ({ row }) => (
                    <div className="accrue-cell">{row.accrue_every}</div>
                )
            },
            {
                id: 'period',
                header: 'Accrual Period',
                sortable: false,
                minWidth: 120,
                render: ({ row }) => (
                    <div>{row.accrual_period}</div>
                )
            },
            {
                id: 'name',
                header: '',
                sortable: false,
                minWidth: 90,
                style: { justifyContent: 'right' },
                render: ({ row }) => (
                    <Button
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        onClick={ () => browserHistory.push( `/company-settings/leave-settings/leave-entitlement/${row.id}/edit`, true ) }
                    />
                )
            }
        ];

        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const { notification } = this.props;
        const { label, show, deleteLabel } = this.state;

        return (
            <div>
                <Helmet
                    title="Leave Entitlements"
                    meta={ [
                        { name: 'description', content: 'Description of Leave Entitlements' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Modal
                    title="Confirm Your Action"
                    body={
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonCancel',
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.confirmDeleteModal.toggle()
                        },
                        {
                            id: 'buttonProceed',
                            type: 'darkRed',
                            label: 'Yes',
                            onClick: () => this.leaveEntitlementsDelete()
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.confirmDeleteModal = ref; } }
                    className="modal-md modal-commission-type"
                    center
                />
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Loading Leave Entitlements</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                            <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                                <div className="heading">
                                    <H1 noBottomMargin>Leave Entitlements</H1>
                                    <P noBottomMargin>View and update leave etitlements. You can add, edit, or delete leave entitlements.</P>
                                </div>

                                <div className="title">
                                    <H5>Leave Entitlements List</H5>
                                    <span className="title-Content">
                                        <P noBottomMargin className="mb-0 mr-1">{show ? deleteLabel : label}</P>
                                        <div>
                                            {show ? (
                                                <SalDropdown
                                                    dropdownItems={ [
                                                        {
                                                            label: 'Delete',
                                                            children: <div>Delete</div>,
                                                            onClick: () => this.confirmDeleteModal.toggle()
                                                        }
                                                    ] }
                                                />
                                            ) : (
                                                <Button
                                                    id="button-filter"
                                                    label="Add Leave Entitlement"
                                                    type="action"
                                                    onClick={ () => browserHistory.push( '/company-settings/leave-settings/leave-entitlement/add', true ) }
                                                />
                                            )}
                                        </div>
                                    </span>
                                </div>

                                <Table
                                    data={ this.props.getLeaveEntitlementList }
                                    columns={ tableColumn }
                                    onDataChange={ this.handleTableChanges }
                                    onSelectionChange={ ({ selected }) => {
                                        const selectionLength = selected.filter( ( row ) => row ).length;
                                        this.setState({
                                            show: selectionLength > 0,
                                            deleteLabel: formatDeleteLabel( selectionLength )
                                        });
                                    } }
                                    ref={ ( ref ) => { this.LeaveEntitlementsTable = ref; } }
                                    selectable
                                />

                                <div>
                                    <FooterTablePaginationV2
                                        page={ this.state.pagination.current_page }
                                        pageSize={ this.state.pagination.per_page }
                                        pagination={ this.state.pagination }
                                        onPageChange={ this.onPageChange }
                                        onPageSizeChange={ this.onPageSizeChange }
                                        paginationLabel={ this.state.label }
                                        fluid
                                    />
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    LeaveEntitlements: makeSelectLeaveEntitlements(),
    getLeaveEntitlementList: makeSelecGetLeaveEntitlements(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        getAllActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( LeaveEntitlements );
