/*
 *
 * LeaveEntitlements constants
 *
 */
export const nameSpace = 'app/LeaveEntitlements/View';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const GET_LEAVEENTITLEMENTS = `${nameSpace}/GET_LEAVEENTITLEMENTS`;
export const SET_LEAVEENTITLEMENTS = `${nameSpace}/SET_LEAVEENTITLEMENTS`;
export const SET_LOADING = `${nameSpace}/SET_LOADING`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
export const DELETE_LEAVEENTITLEMENTS = `${nameSpace}/DELETE_LEAVEENTITLEMENTS`;
export const SET_DELETE_LEAVEENTITLEMENTS = `${nameSpace}/SET_DELETE_LEAVEENTITLEMENTS`;
