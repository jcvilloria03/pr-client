import { createSelector } from 'reselect';

/**
 * Direct selector to the leaveEntitlements state domain
 */
const selectLeaveEntitlementsDomain = () => ( state ) => state.get( 'leaveEntitlements' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by LeaveEntitlements
 */

const makeSelectLeaveEntitlements = () => createSelector(
  selectLeaveEntitlementsDomain(),
  ( substate ) => substate.toJS()
);

const makeSelecGetLeaveEntitlements = () => createSelector(
  selectLeaveEntitlementsDomain(),
  ( substate ) => substate.get( 'getLeaveEntitlements' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectLeaveEntitlementsDomain(),
( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectLeaveEntitlementsDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export default makeSelectLeaveEntitlements;
export {
  selectLeaveEntitlementsDomain,
  makeSelecGetLeaveEntitlements,
  makeSelectLoading,
  makeSelectNotification
};
