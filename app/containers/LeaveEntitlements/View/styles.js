import styled from 'styled-components';

export const PageWrapper = styled.div`
    .content {
        margin-top: 110px;
        .ReactTable{
            .rt-tbody{
                .selected{
                    background-color: rgba(131, 210, 75, 0.15);
                    &:hover{
                        background-color: rgba(131, 210, 75, 0.15) !important;
                    }
                }
            }
        }
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h1 {
                font-size: 36px;
                font-weight: 700;
                line-height: 57.6px;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .main {
            .btn {
                min-width: 140px;
            }
        }

        .tableAction button {
            width: 130px;
        }
        .title {
            display: flex;
            align-items: center;
            margin-bottom: 12px;
            height: 41px;
            justify-content: space-between;

            .title-Content{
                display: flex;
                align-items: center;

                .dropdown {
                    span {
                        border-radius: 2px;
                    }
                }

                span{
                    font-size: 14px;
                }

                .dropdown,
                .dropdown-item {
                    font-size: 14px;
                }

                .dropdown-menu {
                    min-width: 122px;
                }
            }

            h5 {
                margin: 0;
                margin-right: 20px;
                font-size: 18px;
                font-weight: 600;
                line-height: 28.8px;
            }

            .search-wrapper {
                flex-grow: 1;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 30px;

                    input {
                        border: none;
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }
        }
    }

    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }

    .no-license-error {
        color: #e13232;
    }

    .credits-header,
    .credits-cell,
    .accrue-header,
    .accrue-cell {
        text-align: right;
    }
    .credits-cell {
        width: 206px;
    }
    .accrue-cell {
        width: 78px;
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;
    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;
        > i {
            align-self: center;
        }
    }
    .message {
        display: flex;
        align-self: center;
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;
