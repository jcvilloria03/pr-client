/* eslint-disable no-restricted-syntax */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';

import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import Table from 'components/Table';
import SalDropdown from 'components/SalDropdown';
import { H2, H3, H5 } from 'components/Typography';
import Modal from 'components/Modal';
import Button from 'components/Button';
import Loader from 'components/Loader';
import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';
import { getIdsOfSelectedRows, isAnyRowSelected } from 'components/Table/helpers';

import * as leaveTypesAction from './actions';
import { makeSelectDeleteLoading, makeSelectLeaveData, makeSelectLoading, makeSelectNotification } from './selectors';

import { LoadingStyles, PageWrapper } from './styles';

/**
 *
 * LeaveTypes
 *
 */
export class LeaveTypes extends React.Component { // eslint-disable-line react/prefer-stateless-function

    static propTypes={
        getLeaveData: React.PropTypes.func,
        LeaveData: React.PropTypes.array,
        Loading: React.PropTypes.bool,
        DeleteLoading: React.PropTypes.bool,
        leaveDelete: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    constructor( props ) {
        super( props );
        this.state = {
            data: [],
            label: 'Showing 0-0 of 0 entries',
            selection_Label: '',
            totalSelected: '',
            selectPage: '',
            flag: '',
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            }
        };
        this.confirmSaveModal = null;
    }

    componentDidMount() {
        this.props.getLeaveData();
    }

    componentWillReceiveProps( nextProps ) {
        this.setState({
            data: nextProps.LeaveData,
            pagination: {
                total: nextProps.LeaveData.length,
                per_page: 10,
                current_page: 1,
                last_page: Math.ceil( nextProps.LeaveData.length / 10 ),
                to: 0
            }
        }, () => this.handleTableChanges() );
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.leaveTypesTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.leaveTypesTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    getDropdownItems() {
        return [
            {
                children: <div>Delete</div>,
                onClick: this.handleDeleteLeaveRecord
            }
        ];
    }

    checkBoxchanged=() => {
        this.leaveTypesTable.state.selected = this.leaveTypesTable.state.selected.map( ( value ) => {
            this.setState({ flag: false });
            return ( value || !value );
        });
        const selectionLength = this.leaveTypesTable.state.selected.length;
        this.setState({ selectPage: false, selection_Label: formatDeleteLabel( selectionLength ) });
    }

    handleDeleteLeaveRecord=() => {
        this.confirmSaveModal.toggle();
    }

    handlePageSelectOne=( pageObj ) => {
        let isTrue;
        for ( const key in pageObj ) {
            if ( ( this.leaveTypesTable.tableComponent.state.page === +key ) && ( pageObj[ key ] === true ) ) {
                isTrue = pageObj[ key ];
            }
        }
        return isTrue;
    }

    handleModalDeleteClick=() => {
        const ids = getIdsOfSelectedRows( this.leaveTypesTable );
        const companyId = company.getLastActiveCompanyId();
        const payload = ({ company_id: companyId, ids });
        this.props.leaveDelete( payload );
        this.confirmSaveModal.toggle();
    }

    /**
    * Handles table changes
    */
    handleTableChanges = ( tableProps = this.leaveTypesTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
        if ( this.leaveTypesTable ) {
            this.setState({ selectPage: this.handlePageSelectOne( this.leaveTypesTable.state.pageSelected ) || false });
        } else {
            this.setState({ selectPage: false });
        }
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        const { LeaveData, Loading, DeleteLoading } = this.props;
        const { selectPage, flag, totalSelected, data } = this.state;
        const Selected = isAnyRowSelected( this.leaveTypesTable );

        const tableColumns = [
            {
                id: 'name',
                header: 'Leave Type',
                sortable: false,
                accessor: 'name',
                minWidth: 130,
                style: { whiteSpace: 'break-spaces' }
            },
            {
                id: 'abbreviation',
                header: 'Abbreviation',
                accessor: 'abbreviation',
                sortable: false,
                minWidth: 100
            },
            {
                id: 'leave_credit_required',
                header: 'Required Leave Credit',
                sortable: false,
                accessor: 'leave_credit_required',
                minWidth: 90
            },
            {
                id: 'payable',
                header: 'Payable',
                sortable: false,
                accessor: 'payable',
                minWidth: 80
            },
            {
                id: 'documents',
                header: 'Required Documents',
                sortable: false,
                accessor: 'documents',
                minWidth: 90
            },
            {
                header: ' ',
                minWidth: 80,
                sortable: false,
                style: { justifyContent: 'end' },
                render: ({ row }) => (
                    <Button
                        label={ <span>Edit </span> }
                        type="grey"
                        size="small"
                        to={ [ `/company-settings/leave-settings/leave-types/${row.id}/edit`, true ] }
                    />
                )
            }
        ];

        return (
            <div>
                <Helmet
                    title="Leave Types"
                    meta={ [
                        { name: 'description', content: 'Description of Leave Types' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Sidebar
                    items={ sidebarLinks }
                />
                <Modal
                    title="Confirm Your Action"
                    className="modal-md modal-commission-type"
                    body={
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                            }
                    buttons={ [
                        {
                            id: 'buttonCancel',
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.confirmSaveModal.toggle()
                        },
                        {
                            id: 'buttonProceed',
                            type: 'darkRed',
                            label: DeleteLoading ? <Loader /> : 'Yes',
                            onClick: () => this.handleModalDeleteClick()
                        }
                    ] }
                    center
                    showClose={ false }
                    ref={ ( ref ) => { this.confirmSaveModal = ref; } }
                />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: Loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Leave Types.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: Loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Leave Types</H3>
                            </div>
                            <div className="title">
                                <H5>Leave Types List</H5>
                                {Selected ?
                                    <div className="title-Content">
                                        <p>{this.state.selection_Label}</p>
                                        <SalDropdown dropdownItems={ this.getDropdownItems() } />
                                    </div> :
                                    <div className="title-Content">
                                        <p>{this.state.label}</p>
                                        <Button
                                            label="Add Leave Type"
                                            type="action"
                                            size="default"
                                            onClick={ () => browserHistory.push( '/company-settings/leave-settings/leave-types/add', true ) }
                                        />
                                    </div>
                                }
                            </div>

                            {selectPage === true && flag === 'false' ?
                                <div className="selectPage">
                                    <p>{`All ${totalSelected} entries on this page are selected.`}<a onClick={ () => this.checkBoxchanged() }> {`Select all ${LeaveData.length} entries from all pages`}</a></p>
                                </div>
                                : ''
                            }
                            <Table
                                data={ data }
                                columns={ tableColumns }
                                loading={ DeleteLoading }
                                onDataChange={ this.handleTableChanges }
                                onSelectionChange={ ({ selected }) => {
                                    const selectedLength = selected.filter( ( row ) => row ).length;
                                    const isSelected = LeaveData.length === selectedLength ? 'true' : 'false';
                                    this.setState({
                                        totalSelected: selectedLength,
                                        selectPage: this.handlePageSelectOne( this.leaveTypesTable.state.pageSelected ),
                                        flag: isSelected,
                                        selection_Label: formatDeleteLabel( selectedLength )
                                    });
                                } }
                                ref={ ( ref ) => { this.leaveTypesTable = ref; } }
                                page={ this.state.pagination.current_page - 1 }
                                pageSize={ this.state.pagination.per_page }
                                pages={ this.state.pagination.total }
                                selectable
                                external
                            />

                            <div className="footer-table-pagination">
                                <FooterTablePaginationV2
                                    page={ this.state.pagination.current_page }
                                    pageSize={ this.state.pagination.per_page }
                                    pagination={ this.state.pagination }
                                    onPageChange={ this.onPageChange }
                                    onPageSizeChange={ this.onPageSizeChange }
                                    paginationLabel={ this.state.label }
                                    fluid
                                />
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    LeaveData: makeSelectLeaveData(),
    Loading: makeSelectLoading(),
    DeleteLoading: makeSelectDeleteLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        leaveTypesAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( LeaveTypes );
