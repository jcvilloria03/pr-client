import {
    DEFAULT_ACTION,
    DELETE_LEAVE,
    GET_LEAVE
} from './constants';

/**
 *
 * Leave-types actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * leaveType
 * @param {object} payload
 */
export function getLeaveData() {
    return {
        type: GET_LEAVE
    };
}

/**
 * leaveType Delete
 * @param {object} payload
 */
export function leaveDelete( payload ) {
    return {
        type: DELETE_LEAVE,
        payload
    };
}
