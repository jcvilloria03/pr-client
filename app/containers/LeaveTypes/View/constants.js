/*
 *
 * LeaveTypes constants
 *
 */
const nameSpace = 'app/LeaveTypes/View';
export const DEFAULT_ACTION = 'app/LeaveTypes/DEFAULT_ACTION';
export const LOADING = `${nameSpace}/LOADING`;
export const DELETE_LOADING = `${nameSpace}/DELETE_LOADING`;

export const GET_LEAVE = `${nameSpace}/GET_LEAVE`;
export const SET_LEAVE = `${nameSpace}/SET_LEAVE`;

export const DELETE_LEAVE = `${nameSpace}/DELETE_LEAVE`;

export const SET_NOTIFICATION = `${nameSpace}/SET_NOTIFICATION`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;

export const REINITIALIZE_PAGE = `${nameSpace}/REINITIALIZE_PAGE`;
