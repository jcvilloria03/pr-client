import { createSelector } from 'reselect';

/**
 * Direct selector to the leaveTypes state domain
 */
const selectLeaveTypesDomain = () => ( state ) => state.get( 'leaveTypes' );

/**
 * Other specific selectors
 */
const makeSelectLeaveData = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'leave' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectDeleteLoading = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'deleteLoading' )
);

const makeSelectNotification = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

/**
 * Default selector used by LeaveTypes
 */
const makeSelectLeaveTypes = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.toJS()
);

export {
  selectLeaveTypesDomain,
  makeSelectLoading,
  makeSelectDeleteLoading,
  makeSelectLeaveData,
  makeSelectNotification,
  makeSelectLeaveTypes
};
