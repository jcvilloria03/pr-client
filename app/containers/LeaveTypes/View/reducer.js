import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    DELETE_LOADING,
    LOADING,
    SET_LEAVE,
    SET_NOTIFICATION
} from './constants';

const initialState = fromJS({
    loading: false,
    deleteLoading: false,
    leave: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * LeaveTypes reducer
 *
 */
function leaveTypesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LEAVE:
            return state.set( 'leave', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        case DELETE_LOADING:
            return state.set( 'deleteLoading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default leaveTypesReducer;
