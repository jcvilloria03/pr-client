import { delay, takeEvery, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import { take, cancel, put, call } from 'redux-saga/effects';
import { get } from 'lodash';

import { Fetch } from 'utils/request';
import { company } from '../../../utils/CompanyService';

import { DELETE_LEAVE, DELETE_LOADING, GET_LEAVE, LOADING, NOTIFICATION, REINITIALIZE_PAGE, SET_LEAVE, SET_NOTIFICATION } from './constants';
import { formatFeedbackMessage } from '../../../utils/functions';
import { RECORD_DELETE_MESSAGE } from '../../../utils/constants';
import { resetStore } from '../../App/sagas';

/**
 * Get Leave Data Fetch
 */
export function* getLeaveData() {
    try {
        yield put({ type: LOADING, payload: true });
        const companyId = company.getLastActiveCompanyId();

        const res = yield call( Fetch, `/company/${companyId}/leave_types`, { method: 'GET' });
        let { data } = res;
        data = data.map( ( oneData ) => {
            const newData = {
                ...oneData,
                leave_credit_required: oneData.leave_credit_required ? 'Yes' : 'No',
                payable: oneData.payable ? 'Yes' : 'No'
            };
            return newData;
        });
        yield put({ type: SET_LEAVE, payload: data });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
    } finally {
        yield put({ type: LOADING, payload: false });
    }
}

/**
 * Get Leave Data Fetch
 */
export function* leaveDelete({ payload }) {
    try {
        yield put({ type: DELETE_LOADING, payload: true });
        const { company_id, ids: leave_types_ids } = payload;
        const isUse = yield call( Fetch, '/leave_type/check_in_use', { method: 'POST', data: { company_id, leave_types_ids }});

        if ( isUse && isUse.in_use === 0 ) {
            const formData = new FormData();

            formData.append( '_method', 'DELETE' );
            formData.append( 'company_id', company_id );
            leave_types_ids.forEach( ( id ) => {
                formData.append( 'leave_type_ids[]', id );
            });

            yield call( Fetch, '/leave_type/bulk_delete', {
                method: 'POST',
                data: formData
            });

            yield call( showSuccessMessage );
            yield call( getLeaveData );
        }
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
        yield call( getLeaveData );
    } finally {
        yield put({ type: DELETE_LOADING, payload: false });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getLeaveData );
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( true, 'success', RECORD_DELETE_MESSAGE )
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetLeave() {
    const watcher = yield takeEvery( GET_LEAVE, getLeaveData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForDeleteLeave() {
    const watcher = yield takeEvery( DELETE_LEAVE, leaveDelete );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForReinitializePage,
    watchForGetLeave,
    watchForDeleteLeave
];
