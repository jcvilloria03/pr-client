import styled from 'styled-components';

export const PageWrapper = styled.div`
    height:100%;
    .container{
        height:100%;
        .loader{
            height:100vh;
        }
    }
    .content {
        margin-top: 110px;
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-weight: 700;
                margin-bottom: 1rem;
                font-size: 36px;
            }

            p {
                text-align: center;
                max-width: 800px;
                font-size:14px;
                margin-bottom: 0px;
            }
        }
        .selectPage{
            p{
                text-align: center;
                background-color: #e5f6fc;
                margin: 0;
                padding: 12px 10px;
                font-size: 14px;
                font-weight: 600;
                margin-top: 1.5rem;
                a{
                    color: #00a5e5;
                    padding: 0;
                }
            }
        }
        .title {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
            justify-content: space-between;
            .title-Content{
                display: flex;
                align-items: center;
                gap:15px;
                p,.dropdown span,span{
                    font-size: 14px;
                    margin-bottom:0px;
                }

                .dropdown,
                .dropdown-item {
                    font-size: 14px;
                }

                .dropdown-menu {
                    min-width: 122px;
                }
            }
            h5 {
                margin: 0;
                margin-right: 20px;
                font-weight: 700;
                font-size: 18px;
            }

        }

        .footer-table-pagination {
            padding-bottom: 32px;
        }
    }
    .ReactTable{
        .rt-table{
            font-size: 14px;
        }
        .rt-tbody{
            .rt-tr-group:nth-child(even){
                background-color:#fafbfc;
            }
            .rt-tr{
                border-top:0 !important;
                .rt-th{
                    height:63px;
                }
                .rt-td{
                    height:63px;
                }
            }
            .selected{
                background-color: rgba(131, 210, 75, 0.15);
                &:hover{
                    background-color: rgba(131, 210, 75, 0.15) !important;
                }
            }
        }
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    height: 100%;
    justify-content: center;
    padding: 140px 0;
`;
