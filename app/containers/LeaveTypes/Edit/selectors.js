import { createSelector } from 'reselect';

/**
 * Direct selector to the leaveTypes state domain
 */
const selectLeaveTypesDomain = () => ( state ) => state.get( 'leaveTypesEdit' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by LeaveTypes
 */

const makeSelectLeaveTypes = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.toJS()
);
const makeSelectgetEditLeaveTypes = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'isEdit' ).toJS()
);
const makeSelecteditLeaveType = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'editLeavetype' )
);
const makeSelectLoading = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'loading' )
);
const makeSelectSubmitLoading = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'submitLoading' )
);
const makeSelectNotification = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);
export default makeSelectLeaveTypes;
export {
  selectLeaveTypesDomain,
  makeSelectgetEditLeaveTypes,
  makeSelecteditLeaveType,
  makeSelectLoading,
  makeSelectSubmitLoading,
  makeSelectNotification
};
