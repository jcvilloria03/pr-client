/*
 *
 * LeaveTypes constants
 *
 */
const namespace = 'app/LeaveTypes/Edit';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;

export const GET_EDIT = `${namespace}/GET_EDIT`;
export const SET_EDIT = `${namespace}/SET_EDIT`;

export const GET_EDIT_LEAVETYPES = `${namespace}/GET_EDIT_LEAVETYPES`;
export const SET_EDIT_LEAVETYPES = `${namespace}/SET_EDIT_LEAVETYPES`;

export const LOADING = `${namespace}/LOADING`;
export const SUBMIT_LOADING = `${namespace}/SUBMIT_LOADING`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
