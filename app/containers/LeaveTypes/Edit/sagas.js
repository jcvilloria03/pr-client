import { get } from 'lodash';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';
import { cancel, put, call, take } from 'redux-saga/effects';
import { Fetch } from 'utils/request';
import { REINITIALIZE_PAGE } from '../../App/constants';
import { GET_EDIT, GET_EDIT_LEAVETYPES, LOADING, NOTIFICATION, NOTIFICATION_SAGA, SET_EDIT } from './constants';
import { browserHistory } from '../../../utils/BrowserHistory';
import { RECORD_UPDATED_MESSAGE } from '../../../utils/constants';
import { formatFeedbackMessage } from '../../../utils/functions';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}
/**
 * get leave type
 * @param payload
 */
export function* getEditLeaveTypes({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const getEditData = yield call( Fetch, `/leave_type/${payload}`, { method: 'GET' });
        yield put({ type: SET_EDIT, payload: getEditData });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * edit leave type
 * @param payload
 */
export function* editLeaveTypes({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const data = {
            leave_type_id: payload.id,
            ...payload
        };
        yield call( Fetch, `/leave_type/${payload.id}`, { method: 'PUT', data });
        yield call( showSuccessMessage );
        browserHistory.push( '/company-settings/leave-settings/leave-types', true );
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message || `Request failed with statuscode ${error.response.status}` : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}
/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}
 /**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( true, 'success', RECORD_UPDATED_MESSAGE )
    });

    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}
/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });
}
/**
* reinitialize
*/
export function* reinitializePage() {
    yield call( getEditLeaveTypes );
}
/**
* watchForNotifyUser
*/
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
* watchForReinitializePage
*/
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Individual exports for testing
 */
export function* watchForGetEditLeaveType() {
    const watcher = yield takeEvery( GET_EDIT, getEditLeaveTypes );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for edit leave type
 */
export function* watchForeditLeaveTypes() {
    const watcher = yield takeEvery( GET_EDIT_LEAVETYPES, editLeaveTypes );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetEditLeaveType,
    watchForeditLeaveTypes,
    watchForNotifyUser
];
