/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import Sidebar from 'components/Sidebar';
import A from 'components/A';
import Input from 'components/Input';
import RadioGroup from 'components/RadioGroup';
import Radio from 'components/Radio';
import SnackBar from 'components/SnackBar';
import Modal from 'components/Modal';
import Button from 'components/Button';
import Icon from 'components/Icon';
import { Spinner } from 'components/Spinner';

import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';
import { Fetch } from 'utils/request';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';

import makeSelectLeaveTypes, {
    makeSelecteditLeaveType,
    makeSelectgetEditLeaveTypes,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSubmitLoading
} from './selectors';
import * as leaveTypeEditAction from './actions';
import { Footer, PageWrapper, Header, ModalBody } from './styles';

/**
 *
 * LeaveTypes
 *
 */
export class LeaveTypes extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        params: React.PropTypes.object,
        getEditLeaveTypes: React.PropTypes.func,
        isEdit: React.PropTypes.object,
        editLeaveTypes: React.PropTypes.func,
        loading: React.PropTypes.bool,
        submitLoading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }
    static defaultProps = {
        loading: false,
        submitLoading: false
    };
    constructor( props ) {
        super( props );

        this.state = {
            formData: {},
            validateName: '',
            validateexistName: false,
            validateexistNameLoading: false
        };
        this.discardModal = null;
    }
    componentDidMount() {
        this.props.getEditLeaveTypes( this.props.params.id );
    }
    componentWillReceiveProps( nextProps ) {
        this.setState({
            formData: nextProps.isEdit,
            validateName: nextProps.isEdit.name
        });
    }
    submitForm= async () => {
        this.setState({ validateexistNameLoading: true });
        const companyId = company.getLastActiveCompanyId();
        const data = {
            leave_type_id: this.props.params.id,
            ...this.state.formData
        };
        const response = await Fetch( `/company/${companyId}/leave_type/is_name_available`, { method: 'POST', data });
        this.setState({ validateexistNameLoading: false });
        if ( response.available && response.available ) {
            this.props.editLeaveTypes( this.state.formData );
        } else {
            this.setState({ validateexistName: true });
        }
    }
    /**
     *
     * LeaveTypes render method
     *
     */
    render() {
        const { formData, validateName, validateexistName, validateexistNameLoading } = this.state;
        const { loading } = this.props;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        return (
            <div style={ { height: '100vh', overflow: 'auto' } }>
                { ( loading || validateexistNameLoading ) && <Spinner /> }

                <Sidebar items={ sidebarLinks } />
                <Helmet
                    title="Edit Leave Type"
                    meta={ [
                        { name: 'description', content: 'Description of Edit Leave Type' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <Modal
                    title="Discard Changes"
                    className="modal-md modal-commission-type"
                    body={
                        <ModalBody>
                           Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?
                        </ModalBody>
                    }
                    buttons={ [
                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.discardModal.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/company-settings/leave-settings/leave-types', true );
                                this.discardModal.toggle();
                            }
                        }
                    ] }
                    center
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                />
                <Header>
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/leave-settings/leave-types', true );
                                } }
                            >
                                <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Leave Types</span>
                            </A>
                        </Container>
                    </div>
                </Header>
                <PageWrapper>
                    <div className="content" style={ { display: loading ? 'none' : '' } }>
                        <div className="contentData">
                            <h1>Edit Leave Type</h1>
                        </div>
                        <div className="MainRow">
                            <div className="Maincolumn">
                                <div className={ validateName === '' || validateexistName ? 'col-xs-4 input_pay' : 'col-xs-4' }>
                                    <Input
                                        id="leavetype_name"
                                        label="* Leave type name"
                                        type="text"
                                        value={ formData.name }
                                        onChange={ ( value ) => this.setState({
                                            formData: { ...formData, name: value },
                                            validateName: value,
                                            validateexistName: false
                                        }) }
                                    />
                                    {validateName === '' ? <p>Please input valid data</p> : ''}
                                    {validateexistName ? <p>Record name already exists</p> : ''}
                                </div>
                                <div className="col-xs-4">
                                    <Input
                                        id="abbreviation"
                                        label=" Abbreviation"
                                        type="text"
                                        value={ formData.abbreviation }
                                        onChange={ ( value ) => this.setState({ formData: { ...formData, abbreviation: value }}) }
                                    />
                                </div>
                                <div className="col-xs-4">
                                    <Input
                                        id="requiredDocuments"
                                        label=" Required documents"
                                        type="text"
                                        value={ formData.documents }
                                        onChange={ ( value ) => this.setState({ formData: { ...formData, documents: value }}) }
                                    />
                                </div>
                            </div>
                            <div className="Maincolumn">
                                <div className="col-xs-4">
                                    <span>*Requires leave credit?</span>
                                    <RadioGroup
                                        horizontal
                                        value={ `${formData.leave_credit_required}` }
                                        onChange={ ( value ) => this.setState({ formData: { ...formData, leave_credit_required: value }}) }
                                        className="mainRadio"
                                    >
                                        <Radio
                                            value="true"
                                        >Yes</Radio>
                                        <Radio
                                            value="false"
                                        >No</Radio>
                                    </RadioGroup>
                                </div>
                                <div className="col-xs-4">
                                    <span>*Payable?</span>
                                    <RadioGroup
                                        horizontal
                                        value={ `${formData.payable}` }
                                        onChange={ ( value ) => this.setState({ formData: { ...formData, payable: value }}) }
                                        className="mainRadio"
                                    >
                                        <Radio
                                            value="true"
                                        >Yes</Radio>
                                        <Radio
                                            value="false"
                                        >No</Radio>
                                    </RadioGroup>
                                </div>
                            </div>
                        </div>
                    </div>
                </PageWrapper>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                label="Update"
                                type="action"
                                size="large"
                                onClick={ () => this.submitForm() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    LeaveTypes: makeSelectLeaveTypes(),
    isEdit: makeSelectgetEditLeaveTypes(),
    editLeavetype: makeSelecteditLeaveType(),
    loading: makeSelectLoading(),
    submitLoading: makeSelectSubmitLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        leaveTypeEditAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( LeaveTypes );
