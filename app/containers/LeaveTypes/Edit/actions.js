import {
    DEFAULT_ACTION, GET_EDIT, GET_EDIT_LEAVETYPES
} from './constants';

/**
 *
 * LeaveTypes actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
/**
 * get LeaveTypes action
 * @param {object} payload
 */
export function getEditLeaveTypes( payload ) {
    return {
        type: GET_EDIT,
        payload
    };
}

/**
 *
 * Edit leave type type action
 *
 */
export function editLeaveTypes( payload ) {
    return {
        type: GET_EDIT_LEAVETYPES,
        payload
    };
}

