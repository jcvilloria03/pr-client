import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, LOADING, NOTIFICATION_SAGA, SET_EDIT, SET_EDIT_LEAVETYPES, SUBMIT_LOADING
} from './constants';

const initialState = fromJS({
    isEdit: {},
    editLeavetype: '',
    loading: false,
    submitLoading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * LeaveTypes reducer
 *
 */
function leaveTypesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_EDIT:
            return state.set( 'isEdit', fromJS( action.payload ) );
        case SET_EDIT_LEAVETYPES:
            return state.set( 'editLeavetype', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case SUBMIT_LOADING:
            return state.set( 'submitLoading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default leaveTypesReducer;
