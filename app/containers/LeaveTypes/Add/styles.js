import styled from 'styled-components';

export const Header = styled.div`
    .nav{
        padding:10px 10px;
        background:#f0f4f6;
        margin-bottom:10px;
        position: fixed;
        top: 76px;
        right: 0;
        left: 0;
        width: 100%;
        z-index:9;
    }

    .icon-arrow {
        width: 14px;
        font-size: 14px;
        display: inline-block;
        margin-right: 4px;

        > i {
            align-self: center;
        }
    }

    .back-text {
        font-size: 14px;
    }
`;
export const PageWrapper = styled.div`
    padding-left: 91px;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 120px;
    .content{
        width: 80vw;
        margin-right: auto;
        margin-left: auto;
        max-width: 100%;
        padding-top: 2rem;
        .contentData{
            text-align: center;
            color: #474747;
            margin-bottom: 64px;
            h1{
                font-weight: bold;
                font-size: 36px;
                line-height: 1.6;
                margin: 0;
            }
            p{
                font-size: 14px;
            }
        }
        
        .MainRow{
            .Maincolumn{
                padding-bottom: 14px;
                display: flex;
                .col-xs-4{
                    padding: 0 0.5rem 1rem;
                }
                .input_pay{
                    input{
                        border-color:#eb7575 !important;
                    }
                    label{
                        color:#eb7575 !important;
                    }
                    span{
                        color:#eb7575;
                    }
                    p{
                        color:#eb7575;
                        fontSize: 14px;
                        margin-bottom: 0;
                    }
                    input:focus~label,{
                        color:#eb7575 !important;
                    }
                }
                input{
                    border-color:#000 !important;
                    color:#000 !important;
                    height: 42px;
                    font-size: 14px;
                }
                input:focus~label,{
                    color:#5b5b5b !important;
                }
                span{
                    color: #474747;
                    font-size: 14px;
                    margin-bottom: 4px;
                }
                .mainradio{
                    font-size: 14px;
                    margin-bottom: 0.25rem;
                    span{
                        margin-left: 6px;
                    }
                    div{
                        padding: 0 !important;
                        margin-right: 0.3rem;
                        div{
                            align-items: center;
                        }
                    }
                    #Yes{
                        margin-right: 1rem !important;
                    }
                    #No{
                        padding: 0 !important;
                        div{
                            align-items: center;
                        }
                    }
                }
                
            }
        }
    }
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

export const ModalBody = styled.div`
    font-size:14px;
    margin-bottom:20px;
`;
