import {
    DEFAULT_ACTION, GET_LEAVETYPE_ADD, NOTIFICATION
} from './constants';

/**
 *
 * LeaveTypes actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * payroll Request Type
 * @param {object} payload
 */
export function getaddLeaveType( payload ) {
    return {
        type: GET_LEAVETYPE_ADD,
        payload
    };
}

/**
 *
 * Notification action
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
