
import { cancel, call, take, put } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery } from 'redux-saga';

import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
import { company } from 'utils/CompanyService';

import {
    GET_LEAVETYPE_ADD,
    LOADING,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Submit form
 * @param payload
 */
export function* getaddLeaveType({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const data = {
            company_id: companyId,
            ...payload
        };

        yield call( Fetch, '/leave_type', {
            method: 'POST',
            data
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully Added',
            show: true,
            type: 'success'
        });
        yield call( browserHistory.push( '/company-settings/leave-settings/leave-types', true ) );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}
/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}
/**
* watchForNotifyUser
*/
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * getsubmitForm
 */
export function* watchForGetSubmitForm() {
    const watcher = yield takeEvery( GET_LEAVETYPE_ADD, getaddLeaveType );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetSubmitForm,
    watchForNotifyUser
];
