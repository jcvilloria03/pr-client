/*
 *
 * LeaveTypes constants
 *
 */
const namespace = 'app/LeaveTypes/Add';
export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;

export const SET_LEAVETYPE_ADD = `${namespace}/SET_LEAVETYPE_ADD`;
export const GET_LEAVETYPE_ADD = `${namespace}/GET_LEAVETYPE_ADD`;

export const LOADING = `${namespace}/LOADING`;

export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
