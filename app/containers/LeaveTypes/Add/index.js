/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';

import SnackBar from 'components/SnackBar';
import Button from 'components/Button';
import Icon from 'components/Icon';
import Sidebar from 'components/Sidebar';
import A from 'components/A';
import Input from 'components/Input';
import RadioGroup from 'components/RadioGroup';
import Radio from 'components/Radio';
import Modal from 'components/Modal/index';
import { Spinner } from 'components/Spinner';

import { company } from 'utils/CompanyService';
import { Fetch } from 'utils/request';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';

import makeSelectLeaveTypes, { makeSelectaddLeaveTypes, makeSelectLoading, makeSelectNotification } from './selectors';
import * as leaveTypeAction from './actions';
import { Footer, PageWrapper, Header, ModalBody } from './styles';

/**
 *
 * LeaveTypes
 *
 */
export class LeaveTypes extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        getLeaveType: React.PropTypes.func,
        AddLeaveType: React.PropTypes.object,
        getaddLeaveType: React.PropTypes.func,
        loading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    static defaultProps = {
        loading: false
    };

    constructor( props ) {
        super( props );

        this.state = {
            leaveTypeForm: {
                leaveTypeName: '',
                abbreviation: '',
                reqDoc: '',
                reqLeaveCredit: null,
                payable: null
            },
            validateName: 'name',
            validateRadioBtn: 'RadioBtn',
            validateRadioBtn2: 'RadioBtn2',
            validateexistName: false,
            validateexistNameLoading: false
        };
        this.discardModal = null;
    }

    submitForm = async () => {
        const leaveTypeList = {
            name: this.state.leaveTypeForm.leaveTypeName,
            abbreviation: this.state.leaveTypeForm.abbreviation,
            documents: this.state.leaveTypeForm.reqDoc,
            leave_credit_required: this.state.leaveTypeForm.reqLeaveCredit,
            payable: this.state.leaveTypeForm.payable
        };
        const valid = this.validateFields();

        const companyId = company.getLastActiveCompanyId();

        if ( valid ) {
            try {
                this.setState({ validateexistNameLoading: true });
                const response = await Fetch( `/company/${companyId}/leave_type/is_name_available`, { method: 'POST', data: leaveTypeList });
                this.setState({ validateexistNameLoading: false });

                if ( response.available && response.available ) {
                    if ( this.validateFields() ) {
                        this.props.getaddLeaveType( leaveTypeList );
                    }
                } else {
                    this.setState({ validateexistName: true });
                }
            } catch ( _ ) {
                this.setState({ validateexistNameLoading: false });
            }
        }
    }

    validateFields = () => {
        let valid = true;
        if ( this.state.leaveTypeForm.leaveTypeName === '' ) {
            this.setState({
                validateName: ''
            });
            valid = false;
        }
        if ( this.state.leaveTypeForm.reqLeaveCredit === null ) {
            this.setState({
                validateRadioBtn: ''
            });
            valid = false;
        }

        if ( this.state.leaveTypeForm.payable === null ) {
            this.setState({
                validateRadioBtn2: ''
            });
            valid = false;
        }
        return valid;
    }

    /**
     *
     * LeaveTypes render method
     *
     */
    render() {
        const { loading } = this.props;
        const { leaveTypeName, abbreviation, reqDoc, reqLeaveCredit, payable } = this.state.leaveTypeForm;
        const { leaveTypeForm, validateName, validateexistName, validateRadioBtn, validateRadioBtn2, validateexistNameLoading } = this.state;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });
        return (
            <div>
                { ( loading || validateexistNameLoading ) && <Spinner /> }
                <Helmet
                    title="Add Leave Type"
                    meta={ [
                        { name: 'description', content: 'Description of Add Leave Type' }
                    ] }
                />
                <Header>
                    <SnackBar
                        message={ this.props.notification.message }
                        title={ this.props.notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ this.props.notification.show }
                        delay={ 5000 }
                        type={ this.props.notification.type }
                    />
                    <div className="nav">
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/leave-settings/leave-types', true );
                                } }
                            >
                                <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Leave Types</span>
                            </A>
                        </Container>
                    </div>
                </Header>
                <Modal
                    title="Discard Changes"
                    className="modal-md modal-commission-type"
                    body={
                        <ModalBody>
                          Clicking Discard will undo all changes you made on this page. Are you sure you want to proceed?
                        </ModalBody>
                    }
                    buttons={ [

                        {
                            id: 'buttonProceed',
                            type: 'grey',
                            label: 'Stay on this page',
                            onClick: () => {
                                this.discardModal.toggle();
                            }
                        },
                        {
                            id: 'buttonCancel',
                            type: 'darkRed',
                            label: 'Discard',
                            onClick: () => {
                                browserHistory.push( '/company-settings/leave-settings/leave-types', true );
                                this.discardModal.toggle();
                            }
                        }
                    ] }
                    center
                    showClose={ false }
                    ref={ ( ref ) => { this.discardModal = ref; } }
                />
                <PageWrapper>
                    <Sidebar
                        items={ sidebarLinks }
                    />
                    <div className="content">
                        <div className="contentData">
                            <h1>Add Leave Type</h1>
                        </div>
                        <div className="MainRow">
                            <div className="Maincolumn">
                                <div className={ validateName === '' || validateexistName ? 'col-xs-4 input_pay' : 'col-xs-4' }>
                                    <Input
                                        id="leavetype_name"
                                        label="* Leave type name"
                                        type="text"
                                        value={ leaveTypeName }
                                        ref={ ( ref ) => { this.name = ref; } }
                                        onChange={ ( value ) => { this.setState({ validateName: value, validateexistName: false, leaveTypeForm: { ...leaveTypeForm, leaveTypeName: value }}); } }
                                    />
                                    {validateName === '' ? <p>Please input valid data</p> : ''}
                                    {validateexistName ? <p>Record name already exists</p> : ''}
                                </div>

                                <div className="col-xs-4">
                                    <Input
                                        id="abbreviation"
                                        label=" Abbreviation"
                                        type="text"
                                        value={ abbreviation }
                                        onChange={ ( value ) => { this.setState({ leaveTypeForm: { ...leaveTypeForm, abbreviation: value }}); } }
                                    />
                                </div>
                                <div className="col-xs-4">
                                    <Input
                                        id="requiredDocuments"
                                        label=" Required documents"
                                        type="text"
                                        value={ reqDoc }
                                        onChange={ ( value ) => { this.setState({ leaveTypeForm: { ...leaveTypeForm, reqDoc: value }}); } }
                                    />
                                </div>
                            </div>
                            <div className="Maincolumn">
                                <div className={ validateRadioBtn === '' ? 'col-xs-4 input_pay' : 'col-xs-4' }>
                                    <span>*Requires leave credit?</span>
                                    <RadioGroup
                                        horizontal
                                        value={ reqLeaveCredit }
                                        className="mainradio"
                                        onChange={ ( value ) => { this.setState({ validateRadioBtn: value, leaveTypeForm: { ...leaveTypeForm, reqLeaveCredit: value }}); } }
                                    >
                                        <Radio
                                            value="true"
                                        ><span>Yes</span></Radio>
                                        <Radio
                                            value="false"
                                        ><span>No</span></Radio>
                                    </RadioGroup>
                                    {validateRadioBtn === '' ? <p>Please input valid data</p> : ''}
                                </div>
                                <div className={ validateRadioBtn2 === '' ? 'col-xs-4 input_pay' : 'col-xs-4' }>
                                    <span>*Payable?</span>
                                    <RadioGroup
                                        horizontal
                                        value={ payable }
                                        className="mainradio"
                                        onChange={ ( value ) => { this.setState({ validateRadioBtn2: value, leaveTypeForm: { ...leaveTypeForm, payable: value }}); } }
                                    >
                                        <Radio
                                            value="true"
                                        ><span>Yes</span></Radio>
                                        <Radio
                                            value="false"
                                        ><span>No</span></Radio>
                                    </RadioGroup>

                                    {validateRadioBtn2 === '' ? <p>Please input valid data</p> : ''}
                                </div>
                            </div>
                        </div>
                    </div>
                </PageWrapper>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => this.discardModal.toggle() }
                            />
                            <Button
                                label="Submit"
                                type="action"
                                size="large"
                                onClick={ () => this.submitForm() }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    LeaveTypes: makeSelectLeaveTypes(),
    AddLeaveType: makeSelectaddLeaveTypes(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        leaveTypeAction,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( LeaveTypes );
