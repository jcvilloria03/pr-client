import { createSelector } from 'reselect';

/**
 * Direct selector to the leaveTypes state domain
 */
const selectLeaveTypesDomain = () => ( state ) => state.get( 'leaveTypesAdd' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by LeaveTypes
 */

const makeSelectLeaveTypes = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.toJS()
);

const makeSelectaddLeaveTypes = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'AddLeaveType' )
);
const makeSelectLoading = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectNotification = () => createSelector(
  selectLeaveTypesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);
export default makeSelectLeaveTypes;
export {
  selectLeaveTypesDomain,
  makeSelectaddLeaveTypes,
  makeSelectNotification,
  makeSelectLoading
};
