import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION, LOADING, NOTIFICATION_SAGA, SET_LEAVETYPE_ADD
} from './constants';

const initialState = fromJS({
    AddLeaveType: {},
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * LeaveTypes reducer
 *
 */
function leaveTypesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LEAVETYPE_ADD:
            return state.set( 'AddLeaveType', action.payload );
        case LOADING:
            return state.set( 'loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default leaveTypesReducer;
