import { fromJS } from 'immutable';
import {
    LOADING_PAYROLL_HISTORY_PAGE,
    UPDATE_STORE_COMPANY,
    UPDATE_STORE_PAYROLLS,
    SET_STATISTICS_DATA,
    NOTIFICATION_SAGA,
    SET_FILTER_DATA,
    SET_DOWNLOAD_URL,
    SET_PAYROLL_STATUS,
    SET_LOADING_TABLE,
    SET_PAGINATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: {
        show: false,
        message: ''
    },
    loadingTable: false,
    company: {
        id: null
    },
    pagination: {
        from: 1,
        to: 1,
        total: 1,
        current_page: 0,
        last_page: 1,
        per_page: 10
    },
    payrolls: [],
    statistics: {},
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    payrollStatus: [],
    filterData: {
        payrollGroups: [],
        statuses: []
    },
    downloadUrl: null
});

/**
 * modifies passed payroll data to match client structure
 * @param payrollList = list of payrolls returned by API
 * @returns {array} = formatted list of payrolls
 */
function preparePayrollData( payrollList ) {
    return payrollList.map( ({
        payroll_group_id,
        payroll_group_name,
        start_date,
        end_date,
        payroll_date,
        total_employees,
        ...rest
    }) => ({
        payrollGroupId: payroll_group_id,
        payrollGroup: payroll_group_name,
        start_date,
        end_date,
        period: JSON.stringify({
            start: start_date,
            end: end_date
        }),
        payrollDate: payroll_date,
        employees: total_employees,
        ...rest
    }) );
}

/**
 *
 * Payroll History reducer
 *
 */
function payrollHistoryReducer( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING_PAYROLL_HISTORY_PAGE:
            return state.set( 'loading', action.payload );
        case UPDATE_STORE_COMPANY:
            return state.setIn([ 'company', 'id' ], action.payload );
        case UPDATE_STORE_PAYROLLS:
            return state.set( 'payrolls', fromJS( preparePayrollData( action.payload ) ) );
        case SET_PAYROLL_STATUS:
            return state.set( 'payrollStatus', fromJS( action.payload ) );
        case SET_LOADING_TABLE:
            return state.set( 'loadingTable', action.payload );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_STATISTICS_DATA:
            return state.set( 'statistics', fromJS( action.payload ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case SET_DOWNLOAD_URL:
            return state.set( 'downloadUrl', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default payrollHistoryReducer;
