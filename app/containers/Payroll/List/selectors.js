import { createSelector } from 'reselect';

/**
 * Direct selector to the history state domain
 */
const selectPayrollHistoryDomain = () => ( state ) => state.get( 'payrollHistory' );

/**
 * Specific selectors
 */
const makeSelectCompany = () => createSelector(
  selectPayrollHistoryDomain(),
  ( payroll ) => payroll.get( 'company' ).toJS()
);

const makeSelectPayrollHistory = () => createSelector(
  selectPayrollHistoryDomain(),
  ( payroll ) => payroll.get( 'payrolls' ).toJS()
);

const makeSelectStatistics = () => createSelector(
    selectPayrollHistoryDomain(),
    ( payroll ) => payroll.get( 'statistics' ).toJS()
);

const makeSelectPayrollStatus = () => createSelector(
    selectPayrollHistoryDomain(),
    ( payroll ) => payroll.get( 'payrollStatus' ).toJS()
);

const makeSelectFilterData = () => createSelector(
    selectPayrollHistoryDomain(),
    ( payroll ) => payroll.get( 'filterData' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectPayrollHistoryDomain(),
    ( payroll ) => payroll.get( 'notification' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectPayrollHistoryDomain(),
    ( payroll ) => payroll.get( 'loading' )
);

const makeSelectLoadingTable = () => createSelector(
    selectPayrollHistoryDomain(),
    ( payroll ) => payroll.get( 'loadingTable' )
);

const makeSelectPagination = () => createSelector(
    selectPayrollHistoryDomain(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectDownloadUrl = () => createSelector(
    selectPayrollHistoryDomain(),
    ( payroll ) => payroll.get( 'downloadUrl' )
);

export {
  makeSelectPagination,
  selectPayrollHistoryDomain,
  makeSelectPayrollStatus,
  makeSelectCompany,
  makeSelectPayrollHistory,
  makeSelectStatistics,
  makeSelectNotification,
  makeSelectFilterData,
  makeSelectLoading,
  makeSelectLoadingTable,
  makeSelectDownloadUrl
};
