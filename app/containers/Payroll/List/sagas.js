/* eslint-disable camelcase */
/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable no-return-assign */
/* eslint-disable no-inner-declarations */
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import get from 'lodash/get';
import uniq from 'lodash/uniq';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    LOADING_PAYROLL_HISTORY_PAGE,
    UPDATE_STORE_COMPANY,
    LOAD_INITIAL_DATA,
    DELETE_PAYROLL,
    CLOSE_PAYROLL,
    UPDATE_STORE_PAYROLLS,
    SET_FILTER_DATA,
    SET_STATISTICS_DATA,
    NOTIFICATION,
    NOTIFICATION_FROM_ACTION,
    NOTIFICATION_SAGA,
    DOWNLOAD_PAYROLL_REGISTERS,
    REGENERATE_PAYROLL,
    GET_PAYROLL_STATUS,
    RUN_PAYROLL,
    OPEN_PAYROLL,
    GET_PAYROLLS
} from './constants';

import {
    setDownloadUrl,
    setPayrollStatus,
    setLoadingTable,
    setPagination
} from './actions';

/**
 * Load initial data for payroll history page
 *
 */
export function* loadInitialData() {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put({
            type: LOADING_PAYROLL_HISTORY_PAGE,
            payload: {
                show: true,
                message: 'Loading payroll history. Please wait...'
            }
        });

        yield put({
            type: UPDATE_STORE_COMPANY,
            payload: companyId
        });

        const [ payrollStat, payrollGroup ] = yield [
            call( Fetch, `/company/${companyId}/payroll_data_counts`, { method: 'GET' }),
            call( Fetch, `/philippine/company/${companyId}/payroll_groups`, { method: 'GET' })
        ];

        yield put({
            type: SET_STATISTICS_DATA,
            payload: payrollStat[ 0 ]
        });

        yield put({
            type: SET_FILTER_DATA,
            payload: {
                payrollGroups: payrollGroup.data,
                statuses: [
                    'OPEN', 'DRAFT', 'CLOSED', 'DISBURSED'
                ]
            }
        });

        yield call( getPayrolls, { payload: {
            companyId,
            page: 1,
            per_page: 10,
            search_term: '',
            filter: [],
            sorting: {
                order_by: '',
                order_dir: ''
            }
        }});
    } catch ( e ) {
        yield put({
            type: NOTIFICATION,
            payload: {
                message: e.message,
                type: 'error'
            }
        });
    } finally {
        yield put({
            type: LOADING_PAYROLL_HISTORY_PAGE,
            payload: {
                show: false,
                message: ''
            }
        });
    }
}

/**
 * Run Payroll
 */
export function* runPayroll({ payload }) {
    const { payrollId, start_date, end_date, attendance_start_date, attendance_end_date, search_term, annualize } = payload;
    try {
        yield put( setLoadingTable( true ) );

        const data = {
            payroll_group_id: payrollId,
            start_date,
            end_date,
            attendance_start_date,
            attendance_end_date,
            search_term,
            annualize
        };
        let jobID = null;

        /**
         * Generate Payroll
         */
        function* generate() {
            const response = yield call( Fetch, '/payroll/regular_payroll_job', {
                method: 'POST',
                data
            });
            if ( response ) {
                jobID = response.jobId;
                yield call( fetchJobStatus, response.jobId );
            } else {
                yield call( generate, data );
            }
        }

        /**
         * Get the calculation status
         */
        function* fetchJobStatus( jobId ) {
            const response = yield call( Fetch, `/payroll/regular_payroll_job/${jobId}`, { method: 'GET' });

            if ( response.result.status === 'CREATED' ) {
                localStorage.setItem( 'draft_payroll_jobs', JSON.stringify({
                    type: 'regular', id: response.result.payrollId
                }) );

                yield call( browserHistory.push, '/payroll/generate' );
            } else if ( response.result.status === 'FAILED' ) {
                yield call( notifyUser, {
                    show: true,
                    title: 'Failed',
                    message: response.result.error,
                    type: 'error',
                    delay: 5000
                });
            } else {
                yield call( delay, 5000 );
                yield call( fetchJobStatus, jobId );
            }

            return true;
        }

        if ( !jobID ) yield call( generate, data );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * Open Payroll
 */
export function* openPayroll({ payload }) {
    const { payrollId, ...payrollQueries } = payload;

    try {
        yield put( setLoadingTable( true ) );
        yield call( Fetch, `/payroll/${payrollId}/open`, { method: 'POST' });

        yield [
            call( notifyUser, {
                title: 'Success',
                message: 'Payroll successfully opened',
                show: true,
                type: 'success'
            })
        ];

        yield call( getPayrolls, { payload: { ...payrollQueries }});
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * Gets payrolls history by company id
 * @param {Integer} id - Company ID
 */
export function* getPayrolls({ payload }) {
    const companyId = company.getLastActiveCompanyId();
    const { page, per_page, search_term, filter, sorting } = payload;
    const { order_by, order_dir } = sorting;

    const filters = {};

    if ( Object.keys( filter ).length > 0 ) {
        Object.keys( filter ).map( ( item ) => filters[ `filter[${item}]` ] = filter[ item ]);
    }

    try {
        yield put( setLoadingTable( true ) );

        const response = yield call( Fetch, `/company/${payload.companyId || companyId}/payrolls`, { method: 'Get',
            params: {
                page,
                per_page,
                keyword: search_term,
                order_by,
                order_dir,
                ...filters
            }});

        const { data, ...pagination } = response;

        yield put({
            type: UPDATE_STORE_PAYROLLS,
            payload: data
        });

        const calculating = data.filter( ( item ) => item.status === 'CALCULATING' );

        if ( calculating.length > 0 ) {
            yield put( setLoadingTable( false ) );
            yield put({
                type: LOADING_PAYROLL_HISTORY_PAGE,
                payload: {
                    show: false,
                    message: ''
                }
            });

            const calculatingIds = calculating.map( ( item ) => item.id );

            yield call( getPayrollStatus, {
                payload: {
                    payroll_ids: calculatingIds
                }
            });
        }
        yield put( setPagination( pagination ) );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * Get the payroll deletion status
 */
export function* fetchDeleteJobStatus( jobId ) {
    const response = yield call( Fetch, `/payrolls/delete_payroll_job/${jobId}`, { method: 'GET' });

    if ( response.result.status === 'PENDING' ) {
        yield call( delay, 2000 );
        yield call( fetchDeleteJobStatus, jobId );
    } else if ( response.result.status === 'FAILED' ) {
        return [ false, '' ];
    }

    return [ true, '' ];
}

/**
 * get payroll status
 *
 */
export function* getPayrollStatus({ payload }) {
    const { payroll_ids, type = '' } = payload;
    const companyId = company.getLastActiveCompanyId();

    try {
        let payloadIds = [...payroll_ids];
        let finishedIds = [];
        let failedCounter = 0;

        /**
         * get payroll status
         */
        function* status( ids ) {
            if ( ids.length ) {
                const { data } = yield call( Fetch, '/payrolls/jobs/status', { method: 'GET',
                    params: {
                        payroll_ids: uniq( ids )
                    }});

                payloadIds = [];
                finishedIds = [];
                let hasFinished = false;

                data.forEach( ( item ) => {
                    if ( item.data.attributes.status === 'FINISHED' ) {
                        finishedIds.push( item.data.attributes.payload.payrollId );
                        hasFinished = true;
                    } else if ( item.data.attributes.status === 'PENDING' ) {
                        payloadIds.push( item.data.attributes.payload.payrollId );
                    } else if ( item.data.attributes.status === 'FAILED' ) {
                        payloadIds.push( item.data.attributes.payload.payrollId );
                        failedCounter += 1;
                    }
                });

                if ( hasFinished && type !== 'regenerate' ) {
                    const pendingPayrollIds = JSON.parse( localStorage.getItem( 'pending_payroll' ) ) || [];

                    finishedIds.forEach( ( id ) => {
                        const finishedId = pendingPayrollIds.indexOf( id );
                        pendingPayrollIds.splice( finishedId, 1 );
                    });

                    localStorage.setItem( 'pending_payroll', JSON.stringify( pendingPayrollIds ) );

                    const payrollStat = yield call( Fetch, `/company/${companyId}/payroll_data_counts`, { method: 'GET' });

                    yield put({
                        type: SET_STATISTICS_DATA,
                        payload: payrollStat[ 0 ]
                    });

                    yield call( notifyUser, {
                        title: 'Success',
                        message: 'Payroll successfully created',
                        show: true,
                        type: 'success'
                    });
                }

                yield put( setPayrollStatus( data ) );

                if ( failedCounter <= 5 ) {
                    yield call( delay, 7000 );
                    yield call( status, payloadIds.filter( Boolean ) );
                }
            } else {
                yield call( delay, 5000 );
                yield put( setPayrollStatus([]) );

                yield call( getPayrolls, { payload: {
                    companyId,
                    page: 1,
                    per_page: 10,
                    search_term: '',
                    filter: [],
                    sorting: {
                        order_by: '',
                        order_dir: ''
                    }
                }});
            }
        }

        yield call( status, payloadIds );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Triggers calculation of payroll
 */
export function* calculatePayroll({ payload }) {
    yield [
        call( notifyUser, {
            show: false,
            title: '',
            message: '',
            type: 'default'
        })
    ];

    try {
        yield call( Fetch, `/payroll/${payload}/calculate`, { method: 'POST' });
    } catch ( error ) {
        window.timerComponent.handleResume();
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * regenerate payroll
 */
export function* regeneratePayroll({ payload }) {
    const { payrollId } = payload;

    try {
        yield put( setLoadingTable( true ) );

        yield call( Fetch, `/payroll/${payrollId}/recalculate`, { method: 'POST',
            data: {
                saveResultToParent: true
            }});

        yield put( setLoadingTable( false ) );
        yield call( getPayrollStatus, { payload: { payroll_ids: [payrollId], type: 'regenerate' }});

        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Payroll Recalculating',
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * Deletes payroll by payroll id
 * @param {Integer} payload.company_id - Company ID
 * @param {Integer[]} payload.ids - Array of payroll IDs
 */
export function* deletePayroll({ payload }) {
    try {
        if ( payload.ids.length > 0 ) {
            yield put( setLoadingTable( true ) );

            const response = yield call( Fetch, '/payrolls/delete_payroll_job', {
                method: 'DELETE',
                data: {
                    id: payload.ids
                }
            });
            const [ status, msg ] = yield call( fetchDeleteJobStatus, response.jobId );
            yield call( getPayrolls, {
                payload: {
                    companyId: payload.company_id,
                    page: payload.page,
                    per_page: payload.per_page,
                    search_term: payload.search_term,
                    filter: payload.filter,
                    sorting: payload.sorting
                }
            });
            if ( status ) {
                yield call( notifyUser, {
                    show: true,
                    title: 'Delete payroll success!',
                    message: `Your selected ${payload.ids.length > 1 ? 'payrolls were' : 'payroll was'} deleted successfully!`,
                    type: 'success'
                });

                const payrollStat = yield call( Fetch, `/company/${payload.company_id}/payroll_data_counts`, { method: 'GET' });

                yield put({
                    type: SET_STATISTICS_DATA,
                    payload: payrollStat[ 0 ]
                });
            } else {
                yield call( notifyUser, {
                    show: true,
                    title: 'Delete payroll Fails!',
                    message: msg || 'Some error occured while deleting payroll!',
                    type: 'error'
                });
            }
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * Close a payroll by payroll id
 * @param {Integer} payload.payrollId - Payroll ID
 * @param {String} payload.companyId - Company ID
 * @param {String} payload.payrollGroup - Payroll Group name
 */
export function* closePayroll({ payload }) {
    try {
        if ( payload.payrollId > 0 ) {
            yield put({
                type: LOADING_PAYROLL_HISTORY_PAGE,
                payload: {
                    show: true,
                    message: `Closing payroll '${payload.payrollGroup}' ...`
                }
            });

            yield call( Fetch, `/payroll/${payload.payrollId}/close`, { method: 'POST' });
            yield call( notifyUser, {
                show: true,
                title: 'Close payroll success!',
                message: `You have CLOSED the payroll '${payload.payrollGroup}'`,
                type: 'success'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING_PAYROLL_HISTORY_PAGE,
            payload: {
                show: false,
                message: ''
            }
        });
    }
}

/**
 *
 * Sends request to download multiple payroll registers
 *
 * @param {Number[]} payload - Payroll IDs
 * @returns {Object} action
 */
export function* downloadPayrollRegisters({ payload }) {
    try {
        yield put( setLoadingTable( true ) );
        if ( payload.length > 0 ) {
            const data = new FormData();
            payload.forEach( ( payrollId ) => {
                data.append( 'id[]', payrollId );
            });

            const response = yield call( Fetch, '/payroll_register/multiple', {
                method: 'POST',
                data
            });

            yield call( notifyUser, {
                show: true,
                title: 'Accepted',
                message: get( response, 'data.message', response.statusText ),
                type: 'success'
            });
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: get( error, 'response.statusText', 'Error' ),
            message: get( error, 'response.data.message', error.statusText ),
            type: 'error'
        });
    } finally {
        yield put( setLoadingTable( false ) );
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( loadInitialData );
}

/**
 * Display a notification to user
 */
export function* notifyFromClient({ payload }) {
    yield call( notifyUser, payload );
}

/**
 * watch GET_PAYROLLS
 */
export function* watchGetPayrolls() {
    const watcher = yield takeEvery( GET_PAYROLLS, getPayrolls );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for LOAD_INITIAL_DATA
 *
 */
export function* watchForLoadInitialData() {
    const watcher = yield takeEvery( LOAD_INITIAL_DATA, loadInitialData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * watcher for  GET_PAYROLL_STATUS
 */
export function* watchForGetPayrollStatus() {
    const watcher = yield takeEvery( GET_PAYROLL_STATUS, getPayrollStatus );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE_PAYROLL
 *
 */
export function* watchForDeletePayroll() {
    const watcher = yield takeEvery( DELETE_PAYROLL, deletePayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for CLOSE_PAYROLL
 *
 */
export function* watchForClosePayroll() {
    const watcher = yield takeEvery( CLOSE_PAYROLL, closePayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REGENERATE_PAYROLL action
 */
export function* watchForRegeneratePayroll() {
    const watcher = yield takeLatest( REGENERATE_PAYROLL, regeneratePayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for RUN_PAYROLL action
 */
export function* watchForRunPayroll() {
    const watcher = yield takeLatest( RUN_PAYROLL, runPayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for OPEN_PAYROLL action
 */
export function* watchForOpenPayroll() {
    const watcher = yield takeLatest( OPEN_PAYROLL, openPayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DOWNLOAD_PAYROLL_REGISTERS
 *
 */
export function* watchForDownloadPayrollRegisters() {
    const watcher = yield takeEvery( DOWNLOAD_PAYROLL_REGISTERS, downloadPayrollRegisters );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 *
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 *
 */
export function* watchForNotify() {
    const watcher = yield takeEvery( NOTIFICATION_FROM_ACTION, notifyFromClient );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchGetPayrolls,
    watchForRunPayroll,
    watchForOpenPayroll,
    watchForLoadInitialData,
    watchForDeletePayroll,
    watchForGetPayrollStatus,
    watchForClosePayroll,
    watchForDownloadPayrollRegisters,
    watchForNotifyUser,
    watchForNotify,
    watchForReinitializePage,
    watchForRegeneratePayroll
];
