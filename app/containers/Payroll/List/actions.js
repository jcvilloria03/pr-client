/* eslint-disable camelcase */
import {
    LOAD_INITIAL_DATA,
    DELETE_PAYROLL,
    CLOSE_PAYROLL,
    NOTIFICATION_FROM_ACTION,
    DOWNLOAD_PAYROLL_REGISTERS,
    SET_DOWNLOAD_URL,
    REGENERATE_PAYROLL,
    GET_PAYROLL_STATUS,
    SET_PAYROLL_STATUS,
    SET_LOADING_TABLE,
    RUN_PAYROLL,
    OPEN_PAYROLL,
    GET_PAYROLLS,
    SET_PAGINATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 *
 * Fetch intiial data
 *
 */
export function loadInitialData() {
    return {
        type: LOAD_INITIAL_DATA
    };
}

/**
 * set pagination
 */
export function setPagination( payload ) {
    return {
        type: SET_PAGINATION,
        payload
    };
}

/**
 * get payrolls
 */
export function getPayrolls( payload ) {
    return {
        type: GET_PAYROLLS,
        payload
    };
}

/**
* Run payroll
*/
export function runPayroll( payload ) {
    return {
        type: RUN_PAYROLL,
        payload
    };
}

/**
 * Open payroll
 */
export function openPayroll( payload ) {
    return {
        type: OPEN_PAYROLL,
        payload
    };
}

/**
 * Table Loading
 */
export function setLoadingTable( payload ) {
    return {
        type: SET_LOADING_TABLE,
        payload
    };
}

/**
 * get payroll status
 */
export function getPayrollStatus( payload ) {
    return {
        type: GET_PAYROLL_STATUS,
        payload
    };
}

/**
 * set payroll status
 */
export function setPayrollStatus( payload ) {
    return {
        type: SET_PAYROLL_STATUS,
        payload
    };
}

/**
 *
 * Deletes payroll by payroll id
 * @param {Integer} companyId - Company ID
 * @param {Integer[]} ids - Array of payroll IDs
 * @returns {Object} action
 */
export function deletePayroll( companyId, search_term, pagination, filter, sorting, ids ) {
    return {
        type: DELETE_PAYROLL,
        payload: {
            company_id: companyId,
            page: pagination.current_page,
            per_page: pagination.per_page,
            search_term,
            filter,
            sorting,
            ids
        }
    };
}

/**
 *
 * Closes a payroll by payroll id
 *
 */
export function closePayroll( companyId, payrollId, payrollGroup ) {
    return {
        type: CLOSE_PAYROLL,
        payload: {
            companyId,
            payrollId,
            payrollGroup
        }
    };
}

/**
 * Regenerate payroll
 * @param {Integer} payload - Payroll ID
 * @returns {Object} action
 */
export function regeneratePayroll( payload ) {
    return {
        type: REGENERATE_PAYROLL,
        payload
    };
}

/**
 *
 * Downloads multiple payroll registers
 *
 * @param {Number[]} ids - Payroll IDs
 * @returns {Object} action
 */
export function downloadPayrollRegisters( ids ) {
    return {
        type: DOWNLOAD_PAYROLL_REGISTERS,
        payload: ids
    };
}

/**
 * Sets download URL
 * @param {String} payload - Zipped payroll registers download URL
 */
export function setDownloadUrl( payload ) {
    return {
        type: SET_DOWNLOAD_URL,
        payload
    };
}

/**
 * display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION_FROM_ACTION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Resets the payrolls
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
