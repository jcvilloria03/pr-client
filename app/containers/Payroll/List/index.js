/* eslint-disable react/sort-comp */
/* eslint-disable no-param-reassign */
import React from 'react';
import capitalize from 'lodash/capitalize';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment';

import isEqual from 'lodash/isEqual';
import upperCase from 'lodash/upperCase';
import startCase from 'lodash/startCase';

import { Icon as Iconify } from '@iconify/react';
import settingsHorizontal from '@iconify/icons-akar-icons/settings-horizontal';

import A from 'components/A';
import Icon from 'components/Icon';
import Input from 'components/Input';
import Table from 'components/Table';
import Button from 'components/Button';
import SnackBar from 'components/SnackBar';
import SalConfirm from 'components/SalConfirm';
import SalDropdown from 'components/SalDropdown';
import FooterTablePagination from 'components/FooterTablePagination';

import { H2, H3 } from 'components/Typography';
import { isAnyRowSelected, getIdsOfSelectedRows } from 'components/Table/helpers';

import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { PAYROLL_SUBHEADER_ITEMS, DATE_FORMATS } from 'utils/constants';

import {
    formatDate,
    formatCurrency,
    formatPaginationLabel,
    formatDeleteLabel
} from 'utils/functions';
import { subscriptionService } from 'utils/SubscriptionService';

import SubHeader from '../../SubHeader';

import Filter from './templates/filter';
import ProgressBar from './templates/progress-bar';
import StatisticsCard from './templates/statisticscard';

import {
    PageWrapper,
    LoadingStyles,
    StatWrapper
} from './styles';

import {
    makeSelectCompany,
    makeSelectPagination,
    makeSelectPayrollHistory,
    makeSelectStatistics,
    makeSelectNotification,
    makeSelectFilterData,
    makeSelectLoading,
    makeSelectDownloadUrl,
    makeSelectPayrollStatus,
    makeSelectLoadingTable
} from './selectors';

import * as actions from './actions';

/**
 *
 * History
 *
 */
class History extends React.Component {
    static propTypes = {
        loadInitialData: React.PropTypes.func,
        runPayroll: React.PropTypes.func,
        openPayroll: React.PropTypes.func,
        deletePayroll: React.PropTypes.func,
        closePayroll: React.PropTypes.func,
        regeneratePayroll: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        filterData: React.PropTypes.shape({
            payrollGroups: React.PropTypes.array,
            statuses: React.PropTypes.array
        }),
        company: React.PropTypes.shape({
            id: React.PropTypes.oneOfType([
                React.PropTypes.number,
                React.PropTypes.object
            ])
        }).isRequired,
        payrolls: React.PropTypes.arrayOf(
            React.PropTypes.object
        ).isRequired,
        statistics: React.PropTypes.object.isRequired,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.shape({
            show: React.PropTypes.bool,
            message: React.PropTypes.string
        }),
        loadingTable: React.PropTypes.bool,
        products: React.PropTypes.array,
        payrollStatus: React.PropTypes.any,
        getPayrolls: React.PropTypes.func,
        downloadPayrollRegisters: React.PropTypes.func,
        downloadUrl: React.PropTypes.string,
        pagination: React.PropTypes.object
    }

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            delete_label: '',
            displayedData: [],
            showFilter: false,
            hasFiltersApplied: false,
            dropdownItems: [
                {
                    label: 'Delete',
                    children: <div>Delete</div>,
                    onClick: this.deletePayroll
                },
                {
                    label: 'Download Payroll Register',
                    children: <div>Download Payroll Register</div>,
                    onClick: this.downloadPayrollRegisters
                }
            ],
            statistics: [
                {
                    subLabel: 'Number of',
                    label: 'Employees',
                    percentage: '1%',
                    value: 0
                },
                {
                    subLabel: 'Gross',
                    label: 'Payroll',
                    percentage: '1%',
                    value: 0
                },
                {
                    subLabel: 'Number of',
                    label: 'Payruns',
                    percentage: '1%',
                    value: 0
                },
                {
                    subLabel: 'Next Payrun Scheduled',
                    label: 'Button',
                    percentage: 0,
                    value: 0,
                    action: () => this.runPayroll()
                }
            ],
            payrollDataCounts: {},
            nextPayrun: {},
            payrollsCalculating: [],
            notification: {
                title: '',
                message: '',
                show: false,
                type: 'error'
            },
            filter: {},
            searchQuery: '',
            sort_by: {
                order_by: '',
                order_dir: ''
            },
            percentage: 0,
            permission: {
                view: false,
                create: false,
                delete: false,
                close: false
            },
            warning: {
                show: false,
                message: null,
                onConfirm: () => {}
            },
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            },
            calculatingJobs: {}
        };

        this.onSortingChange = this.onSortingChange.bind( this );
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.payroll',
            'create.payroll',
            'delete.payroll',
            'close.payroll'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.payroll' ];

            if ( authorized ) {
                this.setState({ permission: Object.assign( this.state.permission, {
                    view: authorization[ 'view.payroll' ],
                    create: authorization[ 'create.payroll' ],
                    delete: authorization[ 'delete.payroll' ],
                    close: authorization[ 'close.payroll' ]
                }) });
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });

        localStorage.removeItem( 'draft_payroll_jobs' );
    }

    componentDidMount() {
        this.props.loadInitialData();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.notification !== this.props.notification && this.setState({ notification: nextProps.notification });

        if ( !isEqual( nextProps.pagination, this.props.pagination ) ) {
            this.setState({
                pagination: nextProps.pagination,
                label: formatPaginationLabel({
                    page: nextProps.pagination.current_page - 1,
                    pageSize: nextProps.pagination.per_page,
                    dataLength: nextProps.pagination.total
                })
            });
        }

        if ( nextProps.payrolls !== this.props.payrolls ) {
            this.setState({
                displayedData: nextProps.payrolls
            }, () => {
                if ( this.payrollHistoryTable ) {
                    const tableProps = this.payrollHistoryTable.tableComponent.state;

                    Object.assign( tableProps, { selectedRows: []});
                }
            });
        }

        nextProps.downloadUrl !== this.props.downloadUrl
            && nextProps.downloadUrl
            && this.downloadFile( nextProps.downloadUrl );

        if ( nextProps.statistics ) {
            this.handleStatisticsData( nextProps.statistics );
            this.setState({ payrollDataCounts: nextProps.statistics });
        }
    }

    componentWillUnmount() {
        this.props.resetStore();

        localStorage.removeItem( 'pending_payroll' );
    }

    getTableColumns = () => (
        [
            {
                id: 'id',
                show: false
            },
            {
                id: 'payroll_group_name',
                header: 'Payroll Group',
                sortable: true,
                minWidth: 180,
                accessor: ( row ) => (
                    row.type === 'regular'
                        ? row.payrollGroup
                        : `${startCase( row.type )} Pay Run`
                )
            },
            {
                id: 'start_date',
                header: 'Period',
                sortable: true,
                minWidth: 140,
                accessor: ( row ) => {
                    if ( row.type !== 'regular' ) {
                        return 'N/A';
                    }

                    if ( row.period ) {
                        const period = JSON.parse( row.period || { });

                        return `${formatDate( period.start, DATE_FORMATS.DISPLAY )} to ${formatDate( period.end, DATE_FORMATS.DISPLAY )}`;
                    }

                    return '';
                }
            },
            {
                id: 'payroll_date',
                header: 'Payroll Date',
                sortable: true,
                minWidth: 140,
                accessor: ( row ) => (
                    formatDate( row.payrollDate, DATE_FORMATS.DISPLAY )
                )
            },
            {
                id: 'total_employees',
                header: 'No. of Employees',
                sortable: true,
                minWidth: 80,
                accessor: ( row ) => (
                    row.employees
                )
            },
            {
                id: 'gross',
                header: 'Gross Amount',
                sortable: true,
                minWidth: 120,
                accessor: ( row ) => (
                    `Php ${formatCurrency( row.gross )}`
                )
            },
            {
                id: 'net',
                header: 'Net Amount',
                sortable: true,
                minWidth: 120,
                accessor: ( row ) => (
                    `Php ${formatCurrency( row.net )}`
                )
            },
            {
                id: 'status',
                header: 'Status',
                sortable: true,
                minWidth: 100,
                accessor: ( row ) => (
                    capitalize( row.status.toLowerCase() )
                ),
                render: ( row ) => this.getStatusRowComponent( row.value )
            },
            {
                id: 'actions',
                header: 'Action',
                minWidth: 200,
                sortable: false,
                style: { justifyContent: 'flex-start' },
                render: ({ row }) => this.state.permission.view && (
                    this.getRegenerateRowComponent( row )
                )
            }
        ]
    )

    getStatusRowComponent = ( value ) => {
        const style = {
            borderRadius: '5px',
            padding: '0 15px',
            marginBottom: 0
        };
        switch ( value ) {
            case 'Calculating':
                return ( <p style={ { color: '#f1c600', backgroundColor: '#faf9c3', ...style } }>{ value }</p> );

            case 'Recalculating':
                return ( <p style={ { color: '#f1c600', backgroundColor: '#faf9c3', ...style } }>{ value }</p> );

            case 'Closed':
                return ( <p style={ { color: 'green', backgroundColor: '#D4EFE6', ...style } }>{ value }</p> );

            case 'Closing':
                return ( <p style={ { color: 'green', backgroundColor: '#D4EFE6', ...style } }>{ value }</p> );

            case 'Draft':
                return ( <p style={ { color: 'orange', backgroundColor: '#FDECE1', ...style } }>{ value }</p> );

            case 'Open':
                return ( <p style={ { color: '#00A5E5', backgroundColor: '#C5EBF8', ...style } }>{ value }</p> );

            case 'Reopening':
                return ( <p style={ { color: '#00A5E5', backgroundColor: '#C5EBF8', ...style } }>{ value }</p> );

            case 'Disbursed':
                return ( <p style={ { color: '#474747', backgroundColor: '#ECECEC', ...style } }>{ value }</p> );

            case 'Disbursing':
                return ( <p style={ { color: '#474747', backgroundColor: '#ECECEC', ...style } }>{ value }</p> );

            default:
                return ( value );
        }
    }

    getRegenerateRowComponent( row ) {
        const { payrollStatus } = this.props;

        const payrollItem = payrollStatus.find( ( item ) => item.data.attributes.payload.payrollId === row.id );

        const canRegenerate = row.status.toLowerCase() !== 'draft'
            && row.status.toLowerCase() !== 'disbursed'
            && row.status.toLowerCase() !== 'disbursing';

        const regenerateActionComponent = canRegenerate
            ? (
                <Button
                    label={ <span>Regenerate</span> }
                    alt
                    type="action"
                    size="small"
                    onClick={ () => this.props.regeneratePayroll({ payrollId: row.id }) }
                />
            ) : null;

        // TODO: workaround for now: hide the "Reopen" button for closed special payrolls
        const reOpenActionComponent = row.type !== 'special' ? (
            <Button
                label={ <span>Reopen</span> }
                alt
                style={ { padding: '3.5px 21px' } }
                type="action"
                size="small"
                onClick={ () => {
                    const { openPayroll, pagination } = this.props;
                    const { filter, searchQuery, sort_by } = this.state;

                    openPayroll({
                        page: pagination.current_page,
                        per_page: pagination.per_page,
                        search_term: searchQuery,
                        filter,
                        sorting: sort_by,
                        payrollId: row.id
                    });
                } }
            />
        ) : null;

        const payrollActionComponent = row.status.toLowerCase() === 'closed'
            ? reOpenActionComponent
            : regenerateActionComponent;

        return (
            <div className="button-actions">
                <Button
                    alt
                    label={ <span>View</span> }
                    type="action"
                    size="small"
                    disabled={ row.status.toLowerCase() === 'calculating' }
                    onClick={ () => {
                        if ( row.status.toLowerCase() === 'draft' ) {
                            localStorage.setItem( 'draft_payroll_jobs', JSON.stringify({
                                type: `${row.type}`, id: row.type === 'regular' ? row.id : null
                            }) );

                            browserHistory.push( '/payroll/generate' );
                        } else {
                            browserHistory.push( `/payroll/${row.id}` );
                        }
                    } }
                />

                { payrollItem && row.status.toLowerCase() !== 'disbursed'
                    ? <ProgressBar completed={ Math.floor( payrollItem.data.attributes.percentage ) } bgcolor={ '#83d24b' } />
                    : payrollActionComponent
                }
            </div>
        );
    }

    onSortingChange = async ( column, additive ) => {
        let newSorting = JSON.parse( JSON.stringify( this.payrollHistoryTable.state.sorting, ( key, value ) => {
            if ( typeof value === 'function' ) {
                return value.toString();
            }
            return value;
        }) );

        // check if already sorted by same column
        const existingIndex = newSorting.findIndex( ( d ) => d.id === column.id );

        let isDesc = false;
        if ( existingIndex > -1 ) {
            const existing = newSorting[ existingIndex ];
            if ( existing.desc ) {
                if ( additive ) {
                    newSorting.splice( existingIndex, 1 );
                } else {
                    existing.desc = false;
                    newSorting = [existing];
                }
            } else {
                existing.desc = true;
                isDesc = true;
                if ( !additive ) {
                    newSorting = [existing];
                }
            }
        } else if ( additive ) {
            newSorting.push({
                id: column.id,
                desc: false
            });
        } else {
            newSorting = [{
                id: column.id,
                desc: false
            }];
        }

        const orderDir = isDesc ? 'desc' : 'asc';
        const { getPayrolls, pagination } = this.props;
        const { filter, searchQuery } = this.state;

        await getPayrolls({
            page: pagination.current_page,
            per_page: pagination.per_page,
            search_term: searchQuery || '',
            filter,
            sorting: {
                order_by: column.id,
                order_dir: orderDir
            }
        });

        this.setState({
            sort_by: {
                order_by: column.id,
                order_dir: orderDir
            }
        });

        this.payrollHistoryTable.setState({
            sorting: newSorting
        });
        this.payrollHistoryTable.tableComponent.setState({
            sorting: newSorting
        });
    }

    handleTableChanges = ( tableProps = this.payrollHistoryTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });

        this.setState({
            label: formatPaginationLabel( tableProps ),
            delete_label: formatDeleteLabel()
        });
    }

    handlePagination( page, pageSize ) {
        const { getPayrolls } = this.props;
        const { filter, sort_by } = this.state;
        const searchQuery = this.searchInput.state.value.toLowerCase();

        this.setState({ searchQuery });

        getPayrolls({
            page,
            per_page: pageSize,
            search_term: searchQuery || '',
            filter,
            sorting: sort_by
        });
    }

    handleStatisticsData = ( data ) => {
        const { statistics } = this.state;

        statistics[ 0 ].value = data.cur_total_employees;
        statistics[ 0 ].percentage = data.cur_total_employees - data.prev_total_employees;

        statistics[ 1 ].value = data.cur_total_gross;
        statistics[ 1 ].percentage = parseFloat( data.cur_total_gross ) && parseFloat( data.prev_total_gross )
            ? ( data.cur_total_gross / data.prev_total_gross ) * 100
            : 0;

        statistics[ 2 ].value = data.cur_pay_runs;
        statistics[ 2 ].percentage = data.cur_pay_runs - data.prev_pay_runs;

        this.setState({ statistics });
    }

    deletePayroll = () => {
        const selected = getIdsOfSelectedRows( this.payrollHistoryTable );

        this.setState({
            warning: {
                show: false,
                message: '',
                onConfirm: () => {}
            }
        }, () => this.setState( ( prevState ) => {
            if ( !prevState.permission.delete ) {
                return {
                    warning: {
                        show: true,
                        message: 'You are not authorized to make this action.',
                        onConfirm: () => {},
                        confirmText: 'Close',
                        showCancel: false
                    }
                };
            }

            return {
                warning: {
                    show: true,
                    message: `Warning: You are about to delete ${selected.length > 1 ? 'these' : 'this'} payroll ${selected.length > 1 ? 'records' : 'record'}. You can't undo this action. Are you sure you want to proceed?`,
                    onConfirm: () => this.props.deletePayroll(
                        this.props.company.id,
                        this.state.searchQuery,
                        this.props.pagination,
                        this.state.filter,
                        this.state.sort_by,
                        selected
                    )
                }
            };
        }) );
    }

    downloadPayrollRegisters = () => {
        const selected = getIdsOfSelectedRows( this.payrollHistoryTable );
        this.props.downloadPayrollRegisters( selected );
    }

    closePayroll = ( payrollId, payrollGroup ) => {
        this.setState({
            warning: {
                show: false,
                message: '',
                onConfirm: () => {}
            }
        }, () => this.setState({
            warning: {
                show: true,
                message: `Are you sure you want to CLOSE payroll '${payrollGroup}' ?`,
                onConfirm: () => this.props.closePayroll( this.props.company.id, payrollId, payrollGroup )
            }
        }) );
    }

    runPayroll = () => {
        const { statistics: { next_pay_run }, runPayroll } = this.props;

        const payload = {
            payrollId: next_pay_run.id,
            start_date: next_pay_run.pay_dates.period.start,
            end_date: next_pay_run.pay_dates.period.cutoff,
            attendance_start_date: next_pay_run.pay_dates.attendance_period.start,
            attendance_end_date: next_pay_run.pay_dates.attendance_period.cutoff,
            search_term: '',
            annualize: false
        };

        runPayroll( payload );
    }

    applyFilters = ( filterData ) => {
        const { getPayrolls } = this.props;
        const { pagination, searchQuery, sort_by } = this.state;

        const startDateFilter = filterData.find( ( f ) => f.type === 'start_date' );
        const endDateFilter = filterData.find( ( f ) => f.type === 'end_date' );

        const payrollGroupFilters = filterData.filter( ( f ) => f.type === 'payroll_group' );
        const payrollGroupsIds = payrollGroupFilters.map( ( p ) => p.value );

        const statusFilters = filterData.filter( ( f ) => f.type === 'status' );
        const statuses = statusFilters.map( ( p ) => upperCase( p.value ) );

        const filters = {};

        if ( startDateFilter ) {
            filters.start_date = startDateFilter.value;
        }

        if ( endDateFilter ) {
            filters.end_date = endDateFilter.value;
        }

        if ( payrollGroupsIds.length ) {
            filters.payroll_group_ids = payrollGroupsIds;
        }

        if ( statuses.length ) {
            filters.status = statuses;
        }

        this.setState({
            hasFiltersApplied: filters.length !== 0,
            filter: filters
        });

        const payload = {
            page: 1,
            per_page: pagination.per_page,
            search_term: searchQuery || '',
            filter: filters,
            sorting: sort_by
        };

        getPayrolls( payload );
    }

    checkIfPayrollDateMatchesFilters = ( startDateFilter, endDateFilter, payrollDate ) => {
        if ( startDateFilter && endDateFilter ) {
            return (
                moment( payrollDate ).isSameOrAfter( startDateFilter.value ) &&
                moment( payrollDate ).isSameOrBefore( endDateFilter.value )
            );
        }

        if ( startDateFilter ) {
            return moment( payrollDate ).isSameOrAfter( startDateFilter.value );
        }

        if ( endDateFilter ) {
            return moment( payrollDate ).isSameOrBefore( endDateFilter.value );
        }

        return true;
    }

    resetFilters = () => {
        this.setState({
            showFilter: false,
            displayedData: this.props.payrolls,
            hasFiltersApplied: false
        }, () => {
            this.handlePagination( 1, 10 );
        });
    }

    toggleFilter = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    downloadFile( url ) {
        const linkElement = document.getElementById( 'downloadLink' );
        linkElement.href = url;
        linkElement.click();
        linkElement.href = '';
    }

    render() {
        const {
            onConfirm,
            message,
            show,
            ...additionalModalProps
        } = this.state.warning;

        return (
            <div>
                <Helmet
                    title="History"
                    meta={ [
                        { name: 'description', content: 'Description of History' }
                    ] }
                />
                <SnackBar
                    message={ this.state.notification.message }
                    title={ this.state.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.state.notification.show }
                    delay={ 5000 }
                    type={ this.state.notification.type }
                />
                <SalConfirm
                    onConfirm={ onConfirm }
                    body={
                        <div style={ { display: 'flex', padding: '0 20px' } }>
                            <div style={ { alignSelf: 'center' } } >
                                { message }
                            </div>
                        </div>
                    }
                    title="Warning!"
                    visible={ show }
                    { ...additionalModalProps }
                />
                <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                <A download id="downloadLink" />
                <PageWrapper>
                    <Container>
                        { this.props.loading.show ? (
                            <div className="loader">
                                <LoadingStyles>
                                    <H2>Loading Payroll Summary.</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                        ) : (
                            <div className="content">
                                <div className="heading">
                                    <H3>Payroll Summary</H3>

                                    <StatWrapper>
                                        { this.state.statistics.map( ( item, i ) => (
                                            <StatisticsCard
                                                key={ i }
                                                data={ item }
                                                payrollDataCounts={ this.state.payrollDataCounts }
                                            />
                                              ) )
                                        }
                                    </StatWrapper>
                                </div>
                                <div className="title">
                                    <div className="search-wrapper">
                                        <Input
                                            className="search"
                                            placeholder="Search Payroll"
                                            id="search"
                                            ref={ ( ref ) => { this.searchInput = ref; } }
                                            onKeyPress={ ( e ) => {
                                                e.charCode === 13 && this.handlePagination( 1, 10 );
                                            } }
                                            addon={ {
                                                content: <Icon name="search" className="icon" />,
                                                placement: 'left'
                                            } }
                                        />
                                        <button
                                            className="button-filter"
                                            onClick={ () => this.toggleFilter() }
                                        >
                                            <Iconify icon={ settingsHorizontal } height="1.4em" color="white" />
                                        </button>
                                    </div>
                                    <div className="actions-wrapper">
                                        { isAnyRowSelected( this.payrollHistoryTable ) && (
                                        <span className="more-action">
                                            <span>{this.state.delete_label}</span>
                                            <SalDropdown
                                                new
                                                label="More Option"
                                                dropdownItems={ this.state.dropdownItems }
                                                backgroundColor="#00A4E4"
                                            />
                                        </span>
                                        ) }
                                        <span>
                                            <div>
                                                <Button
                                                    id="button-generate"
                                                    label="Generate Payroll"
                                                    type="action"
                                                    onClick={ () => browserHistory.push( '/payroll/generate' ) }
                                                />
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                                    <Filter
                                        filterData={ this.props.filterData }
                                        onCancel={ () => { this.resetFilters(); } }
                                        onApply={ ( values ) => { this.applyFilters( values ); } }
                                    />
                                </div>
                                <Table
                                    className="payroll-history-table"
                                    ref={ ( ref ) => { this.payrollHistoryTable = ref; } }
                                    columns={ this.getTableColumns() }
                                    data={ this.props.payrolls }
                                    loading={ this.props.loadingTable }
                                    pages={ this.props.pagination.total }
                                    pageSize={ this.props.pagination.per_page }
                                    onDataChange={ this.handleTableChanges }
                                    onSelectionChange={ ({ selected }) => {
                                        const selectionLength = selected.filter( ( row ) => row ).length;

                                        this.setState({ delete_label: formatDeleteLabel( selectionLength ) });
                                    } }
                                    onSortingChange={ this.onSortingChange }
                                    selectable
                                    manual
                                />
                            </div>
                        ) }
                    </Container>
                </PageWrapper>
                {
                  !this.props.loading.show && (
                  <div
                      className="loader"
                      style={ { position: 'fixed', bottom: '0', background: '#fff', width: '100%' } }
                  >
                      <FooterTablePagination
                          page={ this.props.pagination.current_page }
                          pageSize={ this.props.pagination.per_page }
                          pagination={ this.props.pagination }
                          onPageChange={ ( data ) => this.handlePagination( data, this.state.pagination.per_page ) }
                          onPageSizeChange={ ( data ) => this.handlePagination( 1, data ) }
                          paginationLabel={ this.state.label }
                      />
                  </div>
                  )
                }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    company: makeSelectCompany(),
    pagination: makeSelectPagination(),
    payrolls: makeSelectPayrollHistory(),
    payrollStatus: makeSelectPayrollStatus(),
    statistics: makeSelectStatistics(),
    notification: makeSelectNotification(),
    filterData: makeSelectFilterData(),
    loading: makeSelectLoading(),
    loadingTable: makeSelectLoadingTable(),
    downloadUrl: makeSelectDownloadUrl()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators( actions, dispatch )
);

export default connect( mapStateToProps, mapDispatchToProps )( History );
