import styled from 'styled-components';

export const PageWrapper = styled.div`
    .container {
        @media (max-width: 1440px) {
            width: 86vw
        }


        .content {
            margin-top: 40px;
            margin-bottom: 80px;
    
            button {
              border-radius: 5px;
            }

            .payroll-history-table{
                .ReactTable.-highlight .rt-tr.-even {
                    background: white
                }
            }
    
            .heading {
                display: flex;
                align-items: center;
                flex-direction: column;
                margin: 0 auto 1rem auto;
    
                h3 {
                    font-weight: 600;
                }
    
                p {
                    text-align: center;
                    max-width: 800px;
                }
            }
    
            .tableAction button {
                width: 130px;
            }
        }
    
        .title {
          display: flex;
          align-items: center;
          flex-direction: row;
          margin-bottom: 1rem;
    
          .button-filter {
            background-color: #1D2E5B;
            padding: 7px;
            line-height: 1;
            border-radius: 5px;
            margin-left: 5px;

            &:focus {
                outline: 0;
            }
          }
    
          .search-wrapper {
              display: flex;
              align-items: center;
              flex-direction: row;
    
              .search {
                width: 300px;
                border: 2px solid #cccccc;
                border-radius: 5px;
                margin-right: 3px;
    
                .input-group {
                    height: 35px;
    
                    input {
                        height: 35px;
                        border: none;
                    }
    
                    .icon {
                        font-size: 18px;
                        color: #83d24b;
                        display: flex;
                        width: 18px;
    
                        svg {
                            height: 18px;
                            width: 18px;
                        }
    
                        > i {
                            align-self: center;
                        }
                    }
                }
              }
      
              p {
                  display: none;
              }
      
              .input-group,
              .form-control {
                  background-color: transparent;
              }
      
              .input-group-addon {
                  background-color: transparent;
                  border: none;
              }
      
              .isvg {
                  display: inline-block;
                  width: 1rem;
              }
          }
      
          .actions-wrapper {
            display: flex;
            flex-grow: 1;
            align-items: center;
            justify-content: flex-end;
    
            .more-action {
                display: flex;
                align-items: center;
                flex-direction: row;
    
                span {
                    margin-right: 10px;
                }
    
                .dropdown {
                    span {
                        border-radius: 5px;
                    }
    
                    .sl-c-dropdown-border-solid {
                        padding: 6px 1rem;
                    }
                }
            }
          }
        }
    
        .leave-types {
            clear:both;
        }
    
        a {
            padding: 4px 10px;
            color: #00A5E5;
            text-decoration: none !important;
        }
    
        .button-actions {
            display: flex;
            align-items: center;
            justify-content: flex-start;
            flex-direction: row;
    
            button {
                color: #83d24b;

                &:hover {
                    background-color: #83d24b;
                    color: white
                }
            }
    
            button:first-of-type {
                margin-right: 5px;
            }
        }
    
        .hide {
            display: none;
        }
    
        .bullet-red {
            color: #eb7575;
        }
    
        .bullet-green {
            color: #9fdc74;
        }
    
        .filter-icon > svg {
            height: 10px;
        }
    
        .editing {
            input{
                height: 25px;
            }
    
            p {
                display:none;
            }
        }
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
    z-index: 10;
`;

export const MessageWrapperStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding-top: 90px;
`;

export const StatWrapper = styled.div`
    width: 100%;
    display: grid;
    grid-template-columns: repeat(4, minmax(0, 1fr));
    gap: 1rem;
`;
