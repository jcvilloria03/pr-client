/*
 * Generic constants
 */
const namespace = 'app/Payroll/History';
export const LOAD_INITIAL_DATA = `${namespace}/LOAD_INITIAL_DATA`;
export const LOADING_PAYROLL_HISTORY_PAGE = `${namespace}/LOADING_PAYROLL_HISTORY_PAGE`;
export const SET_LOADING_TABLE = `${namespace}/SET_LOADING_TABLE`;
export const SET_PAGINATION = `${namespace}/SET_PAGINATION`;

/*
 * Payroll History constants
 */
export const GET_PAYROLLS = `${namespace}/GET_PAYROLL_HISTORY`;
export const UPDATE_STORE_PAYROLLS = `${namespace}/UPDATE_STORE_PAYROLLS`;
export const SET_STATISTICS_DATA = `${namespace}/SET_STATISTICS_DATA`;
export const DELETE_PAYROLL = `${namespace}/DELETE_PAYROLL`;
export const CLOSE_PAYROLL = `${namespace}/CLOSE_PAYROLL`;
export const DOWNLOAD_PAYROLL_REGISTERS = `${namespace}/DOWNLOAD_PAYROLL_REGISTERS`;
export const SET_DOWNLOAD_URL = `${namespace}/SET_DOWNLOAD_URL`;
export const CHECK_CALCULATE_STATUS = `${namespace}/CHECK_CALCULATE_STATUS`;
export const REGENERATE_PAYROLL = `${namespace}/REGENERATE_PAYROLL`;

export const RUN_PAYROLL = `${namespace}/RUN_PAYROLL`;
export const OPEN_PAYROLL = `${namespace}/OPEN_PAYROLL`;

export const GET_PAYROLL_STATUS = `${namespace}/GET_PAYROLL_STATUS`;
export const SET_PAYROLL_STATUS = `${namespace}/SET_PAYROLL_STATUS`;
/*
 * Company constants
 */
export const UPDATE_STORE_COMPANY = `${namespace}/UPDATE_STORE_COMPANY`;

/*
 * Notification constants
 */
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_FROM_ACTION = `${namespace}/NOTIFICATION_FROM_ACTION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;

export const SET_FILTER_DATA = `${namespace}/SET_FILTER_DATA`;

export const PENDING = 'PENDING';

export const FAILED = 'FAILED';

export const FINISHED = 'FINISHED';
