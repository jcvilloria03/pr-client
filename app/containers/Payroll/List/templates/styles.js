import styled from 'styled-components';

import { H3 as BaseH3 } from 'components/Typography';

export const H3 = styled( BaseH3 )`
    margin: 0;
    text-align: center;
    width: 100%;
    font-size: 38px;

    &&&&& {
        font-weight: 700;
    }
`;

export const CardPayrollHeading = styled( H3 )`
    font-size: 26px;

    &&&&& {
        font-weight: 600;
    }
`;

export const FilterWrapper = styled.div`
    display: flex;
    flex-direction: column;
    border: 1px solid #ccc;
    padding: 30px 15px;
    padding-bottom: 10px;
    margin-bottom: 25px;
    border-radius: .5rem;

    .sl-c-filter-actions {
        display: flex;
        justify-content: space-between;
        border-top: 1px solid lightgrey;
        padding-top: 20px;
        align-items: center;

        .sl-c-filter-reset > .btn {
            margin-left: 0;
            color: #00A5E5;
            cursor: pointer;
            border: none;
        }

        .sl-c-filter-buttons > .btn {
            width: 100px;
        }
    }

    .date-picker {
        .DayPickerInput {
            width: 100%;
        }

        span {
            display: block;
        }

        input {
            width: 100%;
            padding-top: 0px !important;
        }
    }
`;

export const Wrapper = styled.div`
    min-height: 164px;
    background-color: #fff;
    border-radius: 4px;
    border: 1px solid #cccccc;
    padding: 1rem;
`;

export const CardContent = styled.div`
`;

export const CardFooter = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

    p {
        margin: 0 2px;
    }

    p:last-child {
        margin-left: 5px;
    }

    span {
        margin-right: 10px;
        margin-top:-4px
    }

    .icon {
        margin-top:-4px;
        margin-right: 5px;

        svg {
            height: 17px;
            width: 17px;
        }
    }

    .possitive {
        color: #3FB78F;
    }

    .negative {
        color: #EF8184;
    }
`;

export const CardWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    height: 100%;
`;

export const CardPayrollWrapper = styled( CardWrapper )`
    justify-content: center;

`;

export const CardTitleContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    width: 100%;
`;

export const CardDate = styled.p`
    margin: 0;
    font-size: 14px;
`;

export const CardTitle = styled.p`
    &&&&& {
        display: flex;
        line-height: 1.2;
        flex-direction: column;
        justify-content: flex-start;
        font-size: 14px;
        text-align: left;
        margin: 0;

        span {
            font-weight: bold;
        }
    }
`;

export const CardSubtitle = styled.p`
    margin: 0;
`;
