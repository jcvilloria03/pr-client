export const FILTER_TYPES = {
    START_DATE: 'start_date',
    END_DATE: 'end_date',
    PAYROLL_GROUP: 'payroll_group',
    STATUS: 'status'
};
