import React, { PropTypes } from 'react';
import moment from 'moment';

import Button from 'components/Button';
import Icon from 'components/Icon';

import { formatCurrency } from 'utils/functions';

import {
    CardContent,
    CardDate,
    CardFooter,
    CardPayrollHeading,
    CardSubtitle,
    CardTitle,
    CardTitleContainer,
    CardWrapper,
    H3,
    Wrapper
} from './styles';

/**
 *
 * Statistics Card
 *
 */
function StatisticsCard( props ) {
    const { data, payrollDataCounts } = props;
    const isPositive = data.percentage >= 0;

    return (
        <Wrapper>
            { Object.keys( payrollDataCounts ).length && data.label === 'Button'
                ? (
                    <CardWrapper className="run-payroll">
                        <CardTitle>{ data.subLabel } </CardTitle>

                        <div>
                            <CardPayrollHeading>
                                {moment( payrollDataCounts.pay_run_scheduled ).format( 'MMM DD, YYYY' )}
                            </CardPayrollHeading>

                            <CardSubtitle>{ payrollDataCounts.next_pay_run.name }</CardSubtitle>
                        </div>

                        <Button label="Run Pay" size="default" type="action" onClick={ data.action } />
                    </CardWrapper>
                ) : (
                    <CardWrapper>
                        <CardTitleContainer>
                            <CardTitle className="label">
                                { data.subLabel } <span>{ data.label }</span>
                            </CardTitle>

                            <CardDate>{moment().format( 'MMM' )} 1 - {moment().daysInMonth()}</CardDate>
                        </CardTitleContainer>

                        <CardContent>
                            { data.label === 'Payroll'
                                ? <H3>{formatCurrency( data.value )}</H3>
                                : <H3>{data.value}</H3>
                            }
                        </CardContent>

                        <CardFooter>
                            { isPositive
                                ? <Icon className="icon" name="greenArrow" />
                                : <Icon className="icon" name="redArrow" />
                            }

                            <CardSubtitle className={ isPositive ? 'possitive' : 'negative' }>
                                {data.label === 'Payroll'
                                    ? `${parseFloat( data.percentage ).toFixed( 2 )} %`
                                    : data.percentage
                                }
                            </CardSubtitle>

                            <CardSubtitle>Last month vs Current</CardSubtitle>
                        </CardFooter>
                    </CardWrapper>
                )
            }
        </Wrapper>
    );
}

StatisticsCard.propTypes = {
    data: PropTypes.object,
    payrollDataCounts: PropTypes.object
};

StatisticsCard.defaultdata = {
    value: 0,
    total: 0
};

export default StatisticsCard;

