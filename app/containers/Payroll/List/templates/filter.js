import React from 'react';
import capitalize from 'lodash/capitalize';

import Button from '../../../../components/Button';
import DatePicker from '../../../../components/DatePicker';
import MultiSelect from '../../../../components/MultiSelect';

import { FilterWrapper } from './styles';

import { FILTER_TYPES } from './constants';
import { formatDate } from '../../../../utils/functions';
import { DATE_FORMATS } from '../../../../utils/constants';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {

    static propTypes = {
        filterData: React.PropTypes.shape({
            payrollGroups: React.PropTypes.array,
            statuses: React.PropTypes.array
        }),
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    constructor( props ) {
        super( props );

        this.state = {
            startDate: null,
            endDate: null
        };
    }

    onApply = () => {
        const filters = [];

        if ( this.state.startDate !== null ) {
            filters.push({ value: this.state.startDate, type: FILTER_TYPES.START_DATE });
        }

        if ( this.state.endDate !== null ) {
            filters.push({ value: this.state.endDate, type: FILTER_TYPES.END_DATE });
        }

        this.payrollGroups.state.value && this.payrollGroups.state.value.forEach( ( payrollGroup ) => {
            filters.push({ ...payrollGroup, type: FILTER_TYPES.PAYROLL_GROUP });
        });

        this.statuses.state.value && this.statuses.state.value.forEach( ( status ) => {
            filters.push({ ...status, type: FILTER_TYPES.STATUS });
        });

        this.props.onApply( filters );
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    getPayrollGroups = () => {
        if ( !this.props.filterData.payrollGroups ) {
            return [];
        }

        const payrollGroups = this.props.filterData.payrollGroups.map( ( payrollGroup ) => ({
            value: payrollGroup.id,
            label: payrollGroup.name,
            disabled: false
        }) );

        return [
            {
                value: 'special',
                label: 'Special Pay Run',
                disabled: false
            },
            {
                value: 'final',
                label: 'Final Pay Run',
                disabled: false
            },
            ...payrollGroups
        ];
    }

    getStatuses = () => {
        if ( !this.props.filterData.statuses ) {
            return [];
        }

        const statuses = this.props.filterData.statuses.map( ( status ) => ({
            value: status,
            label: capitalize( status.toLowerCase() ),
            disabled: false
        }) );

        return statuses;
    }

    resetFilters = () => {
        this.setState({
            startDate: null,
            endDate: null
        });

        this.payrollGroups.setState({ value: null }, () => {
            this.onApply();
        });

        this.statuses.setState({ value: null }, () => {
            this.onApply();
        });
    }

    render() {
        return (
            <FilterWrapper>
                <div className="row">
                    <div className="col-xs-3 date-picker">
                        <DatePicker
                            label="Start Date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            ref={ ( ref ) => { this.start_date = ref; } }
                            selectedDay={ this.state.startDate }
                            onChange={ ( value ) => {
                                const selectedDay = formatDate( value, DATE_FORMATS.API );

                                if ( selectedDay !== this.state.startDate ) {
                                    this.setState({
                                        startDate: formatDate( value, DATE_FORMATS.API )
                                    });
                                }
                            } }
                        />
                    </div>
                    <div className="col-xs-3 date-picker">
                        <DatePicker
                            label="End Date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            ref={ ( ref ) => { this.end_date = ref; } }
                            selectedDay={ this.state.endDate }
                            onChange={ ( value ) => {
                                const selectedDay = formatDate( value, DATE_FORMATS.API );

                                if ( selectedDay !== this.state.endDate ) {
                                    this.setState({
                                        endDate: formatDate( value, DATE_FORMATS.API )
                                    });
                                }
                            } }
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            id="payroll_groups"
                            label={
                                <span>Payroll Groups</span>
                            }
                            ref={ ( ref ) => { this.payrollGroups = ref; } }
                            data={ this.getPayrollGroups() }
                            placeholder="All payroll groups"
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            id="statuses"
                            label={
                                <span>Statuses</span>
                            }
                            ref={ ( ref ) => { this.statuses = ref; } }
                            data={ this.getStatuses() }
                            placeholder="All statuses"
                        />
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                        <Button
                            label="Apply"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
