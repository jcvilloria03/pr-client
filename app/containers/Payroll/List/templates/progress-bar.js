/* eslint-disable react/prop-types */
import React from 'react';

const ProgressBar = ( props ) => {
    const { bgcolor, completed } = props;

    const wrapper = {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        width: '120px',
        marginLeft: '2px'
    };
    const containerStyles = {
        height: 20,
        width: '70px',
        backgroundColor: '#e0e0de',
        borderRadius: 10
    };

    const fillerStyles = {
        height: '100%',
        width: `${completed}%`,
        backgroundColor: bgcolor,
        textAlign: 'right',
        transition: 'width 2s ease',
        borderRadius: 10
    };

    const labelStyles = {
        padding: 5,
        width: '50px',
        color: 'black'
    };

    return (
        <div style={ wrapper }>
            <div style={ containerStyles }>
                <div style={ fillerStyles }>
                    <span style={ labelStyles }></span>
                </div>
            </div>
            <span style={ labelStyles }>{`${completed}%`}</span>
        </div>
    );
};

export default ProgressBar;
