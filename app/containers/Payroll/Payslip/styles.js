import styled from 'styled-components';

export const ContentWrapper = styled.div`
    padding-top: 70px;
    display: flex;
    flex-direction: column;

    .heading {
        display: flex;
        align-items: center;
        flex-direction: column;

        h3 {
            font-weight: 600;
        }

        p {
            text-align: center;
            max-width: 800px;
        }
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;
    justify-content: center;
    text-align: center;

    .message {
        display: flex;
        align-self: center;

        > strong {
            font-size: 17px;
        }
    }
`;
