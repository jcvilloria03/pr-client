import { fromJS } from 'immutable';
import {
    SET_PAYSLIP_DATA,
    SET_PAYSLIP_LOADING,
    SET_PAYSLIP_PAGINATION,
    SET_PAYROLL_GROUP_DATA,
    SET_PAYROLL_GROUP_LOADING,
    SET_COMPANY_DATA,
    SET_PAYSLIP_DOWNLOAD_LINK,
    SET_BULK_DOWNLOADING,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    payslips: {
        data: [],
        loading: false,
        pagination: {
            from: 1,
            to: 1,
            total: 1,
            current_page: 1,
            last_page: 1
        }
    },
    payroll_groups: {
        data: [],
        loading: false
    },
    company_id: null,
    payslip_download: {
        id: 0,
        uri: '',
        download: false,
        bulk: false,
        email: ''
    },
    bulk_downloading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 * modifies passed payroll group data to match client structure
 * @param payrollGroupList = list of payroll group returned by API
 * @returns {array} = formatted list of payroll groups
 */
function preparePayrollGroupData( payrollGroupList ) {
    let formattedData = [];
    if ( payrollGroupList ) {
        formattedData = Object.keys( payrollGroupList ).map( ( id ) => {
            const name = payrollGroupList[ id ];
            return {
                id,
                name
            };
        });
    }
    return fromJS( formattedData );
}

/**
 *
 * Payslips reducer
 *
 */
function payslipReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_PAYSLIP_DATA:
            return state.setIn([ 'payslips', 'data' ], fromJS( action.payload ) );
        case SET_PAYSLIP_LOADING:
            return state.setIn([ 'payslips', 'loading' ], action.payload );
        case SET_PAYSLIP_PAGINATION:
            return state.setIn([ 'payslips', 'pagination' ], action.payload );
        case SET_PAYROLL_GROUP_DATA:
            return state.setIn([ 'payroll_groups', 'data' ], preparePayrollGroupData( action.payload ) );
        case SET_PAYROLL_GROUP_LOADING:
            return state.setIn([ 'payroll_groups', 'loading' ], action.payload );
        case SET_COMPANY_DATA:
            return state.set( 'company_id', action.payload );
        case SET_PAYSLIP_DOWNLOAD_LINK:
            return state.set( 'payslip_download', fromJS( action.payload ) );
        case SET_BULK_DOWNLOADING:
            return state.set( 'bulk_downloading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default payslipReducer;
