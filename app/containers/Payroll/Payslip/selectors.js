import { createSelector } from 'reselect';

/**
 * Direct selector to the payslip state domain
 */
const selectPayslipDomain = () => ( state ) => state.get( 'payslip' );

/**
 * Other specific selectors
 */
const makeSelectPayslip = () => createSelector(
    selectPayslipDomain(),
    ( substate ) => substate.get( 'payslips' ).toJS()
);

const makeSelectPayrollGroup = () => createSelector(
    selectPayslipDomain(),
    ( substate ) => substate.get( 'payroll_groups' ).toJS()
);

const makeSelectCompanyId = () => createSelector(
    selectPayslipDomain(),
    ( substate ) => substate.get( 'company_id' )
);

const makeSelectPayslipDownload = () => createSelector(
    selectPayslipDomain(),
    ( substate ) => substate.get( 'payslip_download' ).toJS()
);

const makeSelectBulkDownloading = () => createSelector(
    selectPayslipDomain(),
    ( substate ) => substate.get( 'bulk_downloading' )
);

const makeSelectNotification = () => createSelector(
    selectPayslipDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectPayslip,
    makeSelectPayrollGroup,
    makeSelectCompanyId,
    makeSelectPayslipDownload,
    makeSelectBulkDownloading,
    makeSelectNotification
};
