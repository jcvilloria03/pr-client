import styled, { keyframes } from 'styled-components';

export const PayslipViewWrapper = styled.div`
    display: flex;
    flex-direction: column;

    button {
        text-transform: capitalize;
    }

    #downloadLink {
        display: none;
     }
`;

export const ActionsWrapper = styled.div`
    background-color: #fff;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 40px 10px;
    border: 1px solid #E5E5E5;
    border-bottom: none;

    > div {
        display: flex;
        flex-direction: row;

        > div {
            display: inline-flex;

            label, .to {
                margin: 10px 15px 0;
                vertical-align: top;
            }

            .select-control,
            .date-picker {
                width: 160px;
                margin-bottom: 0;
                height: 40px;

                .Select-control {
                    height: 40px;

                    .Select-input {
                        height: 40px;
                    }
                }

                input {
                    width: 160px;
                    height: 40px;
                    padding-top: 2px !important;
                    padding-bottom: 2px !important;
                }
            }

            .date-picker > div > span {
                display: none;
            }
        }

        button {
            margin: 0 10px;
            min-width: 120px;
            height: 40px;
            border-radius: 40px;
            padding: 6px;
            font-size: 1em;
        }

        &.right {
            justify-content: flex-end;

            p {
                margin: 10px 15px 0;
                vertical-align: top;
            }
        }
    }

    @media (min-width: 1260px) and (max-width: 1449px) {
        div.left {
            flex-wrap: wrap;
            width: 60%;

            > div:first-child {
                width: 100%;
                margin-bottom: 10px;
            }
        }
    }

    @media (max-width: 1260px) {
        flex-wrap: wrap;
        padding: 40px 10px 20px;

        > div {
            flex-grow: 1;
            margin-bottom: 20px;
            width: 50%;

            &.left {
                flex-wrap: wrap;

                > div {
                    margin-bottom: 10px;
                }
            }
        }
    }
`;

export const PageSelectionWrapper = styled.div`
    display: flex;
    justify-content: center;
    padding: 12px 0;
    background-color: #c4e9f8;
`;

export const TableViewWrapper = styled.div`
    &.no-data .rt-table {
        min-height: 440px;
    }

    .rt-table {
         .rt-resizable-header-content {
            text-align: left;
        }

        .rt-td {
            padding: 12px 20px;

            &:first-child {
                justify-content: start !important;
            }
        }

        .status {
            text-transform: capitalize;

            .open {
                color: #4ABA4A;
            }
            .closed {
                color: #F21108;
            }
        }

        .rowActions {
            button {
                font-size: 14px;
                padding: 4px 12px;

                &:first-child {
                    margin-right: 5px;
                }
            }
        }
    }
`;

export const MessageWrapperStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 120px 20px 90px;
`;

export const InlineLoadingStyles = styled.div`
    display: inline-flex;
    flex-direction: row;
    margin-right: 20px !important;

    i {
        align-self: center;
    }
    span {
        margin-top: 10px;
        vertical-align: top;
    }
`;

const anim3 = keyframes`
    to {
        transform: rotate(45deg) translate(3px, 3px);
    }
`;

export const StyledLoader = styled.div`
    display: inline-flex;
    padding: 0 12px;

    .anim3 {
        padding-left: 10px;
        &:before {
            display: block;
            width: 12px;
            height: 12px;
            margin-top: -1px;
            border: 2px solid #fff;
            content: "";
            animation: ${anim3} .5s ease infinite alternate;
            border-radius: 100% 100% 0 100%;
            transform: rotate( 45deg );
        }

        &:after {
            display: block;
            width: 12px;
            height: 3px;
            margin-top: 8px;
            background-color: #fff;
            content: "";
            border-radius: 100%;
        }
    }
`;

