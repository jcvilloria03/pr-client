/* eslint-disable react/sort-comp */
import React from 'react';
import moment from 'moment';
import startCase from 'lodash/startCase';

import Table from 'components/Table';
import A from 'components/A';
import Button from 'components/Button';
import SalSelect from 'components/Select';
import DatePicker from 'components/DatePicker';
import { H3 } from 'components/Typography';

import {
    PayslipViewWrapper,
    TableViewWrapper,
    ActionsWrapper,
    PageSelectionWrapper,
    MessageWrapperStyles,
    InlineLoadingStyles,
    StyledLoader
} from './styles';

/**
 * PayslipView component
 */
export class PayslipView extends React.PureComponent {
    static propTypes = {
        getPayslipData: React.PropTypes.func,
        getPayslipDownloadLink: React.PropTypes.func,
        getPayslipBulkDownloadLink: React.PropTypes.func,
        sendPayslip: React.PropTypes.func,
        payslips: React.PropTypes.shape({
            data: React.PropTypes.arrayOf(
                React.PropTypes.object
            ),
            loading: React.PropTypes.bool,
            pagination: React.PropTypes.shape({
                last_page: React.PropTypes.number
            })
        }).isRequired,
        companyId: React.PropTypes.oneOfType([
            React.PropTypes.number,
            React.PropTypes.object
        ]),
        payrollGroups: React.PropTypes.shape({
            data: React.PropTypes.arrayOf(
                React.PropTypes.object
            ),
            loading: React.PropTypes.bool
        }).isRequired,
        payslipDownload: React.PropTypes.shape({
            id: React.PropTypes.number,
            uri: React.PropTypes.string,
            download: React.PropTypes.bool,
            bulk: React.PropTypes.bool
        }).isRequired,
        bulkDownloading: React.PropTypes.bool,
        defaultFilter: React.PropTypes.shape({
            ids: React.PropTypes.string,
            startDate: React.PropTypes.string,
            endDate: React.PropTypes.string
        }).isRequired
    }

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            bulkDownloading: props.bulkDownloading,
            showFilters: false,
            selection: {
                count: 0,
                allInPage: false,
                pageSize: 10
            },
            selected_payroll_group: null,
            selected_payslip_status: '',
            tablePayslipsPage: 0,
            tablePayslipsPageSize: 10,
            filters: {
                employee_name: ''
            },
            filterTypingTimeout: 0
        };

        this.applyFilter = this.applyFilter.bind( this );
        this.updateSelection = this.updateSelection.bind( this );
        this.updateSelectionHeader = this.updateSelectionHeader.bind( this );
        this.selectAllPayslips = this.selectAllPayslips.bind( this );
        this.clearSelection = this.clearSelection.bind( this );
        this.validateDateFilter = this.validateDateFilter.bind( this );
        this.initiateBulkDownload = this.initiateBulkDownload.bind( this );
        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
        this.onFilteringChange = this.onFilteringChange.bind( this );
    }

    /**
     * Perform any action before component receive new props
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.payslipDownload.uri !== this.props.payslipDownload.uri
            && this.downloadPayslip( nextProps.payslipDownload );

        nextProps.payrollGroups !== this.props.payrollGroups
            && this.buttonApply.setState({
                disabled: !nextProps.payrollGroups.data.length || nextProps.payslips.loading || nextProps.bulkDownloading
            });

        nextProps.bulkDownloading !== this.props.bulkDownloading
            && this.enableBulkDownloadControls( nextProps.bulkDownloading );
    }

    /**
     * On table page change handler
     */
    onPageChange( page ) {
        this.setState({ tablePayslipsPage: page });
        this.tablePayslips.tableComponent.setState({ page });
        this.applyFilter(
            page,
            this.state.tablePayslipsPageSize
        );
    }

    /**
     * On table page size change handler
     */
    onPageSizeChange( pageSize ) {
        this.setState({ tablePayslipsPageSize: pageSize });
        this.tablePayslips.tableComponent.setState({ pageSize });
        this.applyFilter(
            this.state.tablePayslipsPage,
            pageSize
        );
    }

    /**
     * Returns payroll groups for filter
     */
    getPayrollGroups() {
        const payrollGroups = this.props.payrollGroups.data.map( ( payrollGroup ) => ({
            value: payrollGroup.id,
            label: payrollGroup.name
        }) );

        payrollGroups.unshift({ value: '0', label: 'All' });
        return payrollGroups;
    }

    /**
     * Returns Payslip status types for filter
     */
    getPayslipStatusFilters() {
        const payslipFilters = [
            { value: 'unread', label: 'Unread' },
            { value: 'read', label: 'Read' }
        ];
        payslipFilters.unshift({ value: '', label: 'All' });
        return payslipFilters;
    }

    /**
     * Gets the default selected payroll group filter
     */
    getDefaultPayrollGroup() {
        const payrollGroup = this.props.payrollGroups.data.find( ( group ) => group.id === this.props.defaultFilter.ids );
        return payrollGroup
            ? { value: payrollGroup.id, label: payrollGroup.name }
            : { value: '0', label: 'All' };
    }

    /**
     * Get table columns for payslip table
     */
    getColumns() {
        return [
            {
                header: <span>Employee ID</span>,
                accessor: 'company_employee_id',
                sortable: false,
                width: 200,
                hideFilter: true
            },
            {
                header: '',
                accessor: 'payroll_id',
                show: false
            },
            {
                header: <span>
                    Employee Name
                    <btn
                        style={ { padding: '0 20px' } }
                        onClick={ () => { this.setState({ showFilters: !this.state.showFilters }); } }
                    >
                        <i className="fa fa-search"></i>
                    </btn>
                </span>,
                accessor: 'employee_name',
                sortable: false,
                minWidth: 300,
                filterRender: ({ onFilterChange }) => (
                    <input
                        type="text"
                        style={ {
                            width: '100%'
                        } }
                        value={ this.state.filters.employee_name || '' }
                        onChange={ ( event ) => onFilterChange( event.target.value ) }
                    />
                  )
            },
            {
                header: <span>Payroll Group</span>,
                accessor: 'payroll_group_name',
                sortable: false,
                width: 250,
                hideFilter: true
            },
            {
                header: <span>Payroll Type</span>,
                accessor: 'payroll_type',
                sortable: false,
                width: 200,
                hideFilter: true
            },
            {
                header: 'Payslip Status',
                accessor: 'status',
                sortable: false,
                minWidth: 150,
                hideFilter: true,
                render: ({ value }) => startCase( value )
            },
            {
                header: <span>Payroll Status</span>,
                accessor: 'payroll_status',
                sortable: false,
                width: 200,
                hideFilter: true,
                render: ( row ) => (
                    <span className="status">
                        <span className={ row.row.payroll_status === 'OPEN' ? 'open' : 'closed' }>●</span>
                        &nbsp;{ row.row.payroll_status.toLowerCase() }
                    </span>
                )
            },
            {
                header: 'Payroll Date',
                accessor: 'payroll_date',
                sortable: false,
                width: 200,
                hideFilter: true,
                render: ( row ) => (
                    moment( row.value ).format( 'MMM DD, YYYY' )
                )
            },
            {
                accessor: 'actions',
                sortable: false,
                width: 240,
                className: 'rowActions',
                hideFilter: true,
                render: ( row ) => (
                    <span>
                        <Button
                            id={ `buttonViewPayslip${row.row.payslip_id}` }
                            ref={ ( ref ) => { this[ `buttonViewPayslip${row.row.payslip_id}` ] = ref; } }
                            type="neutral"
                            size="small"
                            onClick={ () => {
                                this[ `buttonViewPayslip${row.row.payslip_id}` ].setState({ disabled: true });
                                this.props.getPayslipDownloadLink( row.row.payslip_id, false );
                            } }
                            label="View"
                        />
                        <Button
                            id={ `buttonDownloadPayslip${row.row.payslip_id}` }
                            ref={ ( ref ) => { this[ `buttonDownloadPayslip${row.row.payslip_id}` ] = ref; } }
                            type="neutral"
                            size="small"
                            onClick={ () => {
                                this[ `buttonDownloadPayslip${row.row.payslip_id}` ].setState({ disabled: true });
                                this.props.getPayslipDownloadLink( row.row.payslip_id, true );
                            } }
                            label="Download"
                        />
                        <Button
                            id={ `buttonSendPayslip${row.row.payslip_id}` }
                            ref={ ( ref ) => { this[ `buttonSendPayslip${row.row.payslip_id}` ] = ref; } }
                            type="neutral"
                            size="small"
                            onClick={ () => {
                                this[ `buttonSendPayslip${row.row.payslip_id}` ].setState({ disabled: true });
                                this.props.sendPayslip( row.row.payroll_id, row.row.employee_id );
                            } }
                            label="Send"
                        />
                    </span>
                )
            }
        ];
    }

    /**
     * Updates selection header strip when user selects a different page
     */
    updateSelectionHeader() {
        if ( this.tablePayslips ) {
            const pageIndex = this.tablePayslips.tableComponent.state.page;
            this.setState({
                selection: Object.assign({}, this.state.selection, { allInPage: this.tablePayslips.state.pageSelected[ pageIndex ] })
            });
        }
    }

    /**
     * Updates selection label texts when user selects/deselect from payslip table
     */
    updateSelection() {
        if ( this.tablePayslips ) {
            const pageIndex = this.tablePayslips.tableComponent.state.page;
            const selection = Object.assign({}, this.state.selection, {
                count: this.tablePayslips.state.selected.filter( ( item ) => item ).length,
                allInPage: this.tablePayslips.state.pageSelected[ pageIndex ],
                pageSize: this.tablePayslips.tableComponent.pageSize
            });
            this.setState({
                selection
            }, () => {
                this.buttonBulkDownload.setState({ disabled: !this.state.selection.count });
            });
        }
    }

    /**
     * Selects all payslips in table of all pages
     */
    selectAllPayslips( event ) {
        event.preventDefault();
        this.tablePayslips
            && this.tablePayslips.setState({
                selected: this.tablePayslips.state.selected.map( () => true )
            }, () => {
                const selection = Object.assign({}, this.state.selection, {
                    count: this.props.payslips.data.length,
                    allInPage: false
                });
                this.setState({
                    selection
                });
            });
    }

    /**
     * Clears selection from payslip table
     */
    clearSelection( event ) {
        event && event.preventDefault();
        this.tablePayslips
            && this.tablePayslips.setState({
                selected: this.tablePayslips.state.selected.map( () => false ),
                pageSelected: {
                    0: false
                }
            }, () => {
                this.tablePayslips.headerSelect.checked = false;
                const selection = Object.assign({}, this.state.selection, {
                    count: 0,
                    allInPage: false
                });
                this.setState({
                    selection
                }, () => this.buttonBulkDownload.setState({ disabled: !this.state.selection.count }) );
            });
    }

    /**
     * Validates date filter values
     */
    validateDateFilter() {
        const startDate = this.pickerStartDate.state.selectedDay;
        const endDate = this.pickerEndDate.state.selectedDay;

        this.buttonApply.setState({
            disabled: moment( endDate ) < moment( startDate ) || this.props.payslips.loading || this.state.bulkDownloading
        });
    }

    /**
     * Apply payslip table filter based of selected payroll group and date range
     */
    applyFilter( page = null, pageSize = null ) {
        try {
            this.buttonApply.setState({ disabled: true });

            const id = Number( this.state.selected_payroll_group || this.selectPayrollGroup.state.value.value );
            const includeSpecial = id === 0;

            let startDate = this.pickerStartDate.state.selectedDay;
            let endDate = this.pickerEndDate.state.selectedDay;

            const ids = parseInt( id, 10 ) || this.props.payrollGroups.data.map( ( payrollGroup ) => payrollGroup.id ).join();
            startDate = moment( startDate ).format( 'YYYY-MM-DD' );
            endDate = moment( endDate ).format( 'YYYY-MM-DD' );

            this.props.getPayslipData(
                this.props.companyId,
                ids,
                startDate,
                endDate,
                includeSpecial,
                page + 1 || this.state.tablePayslipsPage + 1,
                pageSize || this.state.tablePayslipsPageSize,
                this.state.filters,
                this.state.selected_payslip_status
            );
        } catch ( error ) {
            this.buttonApply.setState({ disabled: false });
        }
    }

    /**
     * Initiates bulk download.
     */
    initiateBulkDownload() {
        if ( this.tablePayslips ) {
            const ids = Object.keys( this.tablePayslips.state.selected )
                .filter( ( id ) => this.tablePayslips.state.selected[ id ])
                .map( ( id ) => this.tablePayslips.props.data[ id ].payslip_id ).join();
            this.props.getPayslipBulkDownloadLink( ids );
        }
    }

    /**
     * View or download payslip from server
     */
    downloadPayslip( payslip ) {
        if ( payslip.uri.length ) {
            if ( payslip.bulk && payslip.email.length ) return;

            const linkElement = document.getElementById( 'downloadLink' );
            linkElement.removeAttribute( 'download' );
            if ( payslip.download ) {
                linkElement.target = '';
                linkElement.download = '';
            } else {
                linkElement.target = '_blank';
            }
            linkElement.href = payslip.uri;
            linkElement.click();
            linkElement.href = '';
        }

        // Re-enable elements once download is triggered
        if ( payslip.id ) {
            if ( payslip.download ) {
                this[ `buttonDownloadPayslip${payslip.id}` ].setState({ disabled: false });
            } else {
                this[ `buttonViewPayslip${payslip.id}` ].setState({ disabled: false });
            }
        }
    }

    /**
     * Enable/disable controls for bulk download
     */
    enableBulkDownloadControls( bulkDownloading ) {
        this.setState({ bulkDownloading });

        bulkDownloading && this.buttonBulkDownload.setState({ disabled: true });

        // Re-enable filters
        this.buttonApply.setState({ disabled: bulkDownloading });

        // Re-enable checkboxes
        if ( this.tablePayslips ) {
            this.tablePayslips.setState({ disabledSelection: bulkDownloading });
            this.tablePayslips.headerSelect.disabled = bulkDownloading;
        }

        // Reset selection in table
        !bulkDownloading && this.clearSelection();
    }

    /**
     * this renders a loading screen
     */
    loadingState() {
        return (
            <MessageWrapperStyles>
                <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw" />
                <br />
                <H3>Loading payslip data. Please wait...</H3>
            </MessageWrapperStyles>
        );
    }

    /**
     * renders payroll groups and periods filter
     */
    renderPayrollGroupFilter() {
        return (
            this.props.payrollGroups.loading
                ? (
                    <InlineLoadingStyles>
                        <i className="fa fa-circle-o-notch fa-spin fa-2x fa-fw" />
                        <span>Loading payroll group filter...</span>
                    </InlineLoadingStyles>
                )
                : (
                    <div>
                        <label htmlFor="payrollGroup">Payroll Group</label>
                        <div className="select-control">
                            <SalSelect
                                ref={ ( ref ) => { this.selectPayrollGroup = ref; } }
                                id="selectPayrollGroup"
                                value={ this.state.selected_payroll_group || this.getDefaultPayrollGroup() }
                                data={ this.getPayrollGroups() }
                                autoSize
                                placeholder="Payroll Group Name"
                                disabled={ this.state.bulkDownloading }
                                onChange={ ({ value }) => this.setState({ selected_payroll_group: value }) }
                            />
                        </div>
                    </div>
                )
        );
    }

    /**
     * renders payslip status filter
     */
    renderPayslipStatusFilter() {
        return (
            <div>
                <label htmlFor="payslipStatusfilter">Payslip Status</label>
                <div className="select-control">
                    <SalSelect
                        id="payslipStatusfilter"
                        value={ this.state.selected_payslip_status }
                        data={ this.getPayslipStatusFilters() }
                        autoSize
                        placeholder="Payslip status"
                        disabled={ this.state.bulkDownloading }
                        onChange={ ({ value }) => this.setState({ selected_payslip_status: value }) }
                    />
                </div>
            </div>
        );
    }

    /**
     * renders action header to DOM
     */
    renderActionHeader() {
        return (
            <ActionsWrapper>
                <div className="left">
                    { this.renderPayrollGroupFilter() }
                    { this.renderPayslipStatusFilter() }
                    <div className="date-range">
                        <label htmlFor="payrollGroup">Payroll Period</label>
                        <div className="date-picker">
                            <DatePicker
                                ref={ ( ref ) => { this.pickerStartDate = ref; } }
                                label=""
                                autoSize
                                dayFormat="MMM DD, YYYY"
                                selectedDay={ this.props.defaultFilter.startDate }
                                onChange={ this.validateDateFilter }
                                disabled={ this.state.bulkDownloading }
                            />
                        </div>
                        <span className="to">to</span>
                        <div className="date-picker">
                            <DatePicker
                                ref={ ( ref ) => { this.pickerEndDate = ref; } }
                                label=""
                                autoSize
                                dayFormat="MMM DD, YYYY"
                                selectedDay={ this.props.defaultFilter.endDate }
                                onChange={ this.validateDateFilter }
                                disabled={ this.state.bulkDownloading }
                            />
                        </div>
                    </div>
                    <Button
                        label="Apply"
                        size="large"
                        alt
                        onClick={ this.applyFilter }
                        disabled={ false }
                        ref={ ( ref ) => { this.buttonApply = ref; } }
                    />
                </div>
                <div className="right">
                    {
                        !this.props.payslips.loading ? (
                            <p>
                                <span style={ { fontWeight: 'bold', fontSize: '17px' } }>{ this.state.selection.count }</span>
                                &nbsp;of { this.props.payslips.data.length } payslips selected
                            </p>
                        ) : false
                    }
                    <Button
                        ref={ ( ref ) => { this.buttonBulkDownload = ref; } }
                        size="large"
                        label={
                            this.state.bulkDownloading ? (
                                <StyledLoader>
                                    Downloading... <div className="anim3"></div>
                                </StyledLoader>
                            ) : 'Download'
                        }
                        disabled={ !this.state.selection.count || this.state.bulkDownloading }
                        onClick={ this.initiateBulkDownload }
                    />
                </div>
            </ActionsWrapper>
        );
    }
    onFilteringChange( column, value ) {
        if ( this.filterTypingTimeout !== 0 ) {
            clearTimeout( this.filterTypingTimeout );
        }

        this.filterTypingTimeout = setTimeout( () => {
            this.applyFilter(
                0,
                this.state.tablePayslipsPageSize,
            );
        }, 500 );
        this.setState({ filters: { ...this.state.filters, [ column.id ]: value }});
    }

    /**
     * Renders page selection content
     */
    renderPageSelectionHeader() {
        if ( !this.props.payslips.loading ) {
            if ( this.state.selection.count && this.state.selection.count === this.props.payslips.data.length ) {
                return (
                    <PageSelectionWrapper>
                        <span>
                            All <strong>{ this.props.payslips.data.length }</strong> payslips are selected.&nbsp;
                            <A href onClick={ ( event ) => { !this.state.bulkDownloading && this.clearSelection( event ); } }>Clear selection</A>
                        </span>
                    </PageSelectionWrapper>
                );
            } else if ( this.state.selection.allInPage ) {
                return (
                    <PageSelectionWrapper>
                        <span>
                            All { this.state.selection.pageSize } payslips on this page are selected.&nbsp;
                            <A href onClick={ ( event ) => { !this.state.bulkDownloading && this.selectAllPayslips( event ); } }>
                                Select all&nbsp;
                                <strong>{ this.props.payslips.data.length }</strong>
                                &nbsp;payslips from all pages
                            </A>
                        </span>
                    </PageSelectionWrapper>
                );
            }
        }
        return false;
    }

    /**
     * Renders PayslipView to DOM
     */
    render() {
        return (
            <PayslipViewWrapper>
                { this.renderActionHeader() }
                { this.renderPageSelectionHeader() }
                {
                    (
                        <TableViewWrapper
                            className={ !this.props.payslips.data.length ? 'no-data' : '' }
                        >
                            <Table
                                ref={ ( ref ) => { this.tablePayslips = ref; } }
                                data={ this.props.payslips.data }
                                columns={ this.getColumns() }
                                pagination={ this.props.payslips.data.length > 10 || this.props.payslips.pagination.last_page > 1 }
                                noDataText="There are no payslips available to display"
                                onSelectionChange={ this.updateSelection }
                                onDataChange={ this.updateSelectionHeader }
                                defaultSorted={ [
                                    { id: 'payroll_date', desc: true },
                                    { id: 'payroll_group_name' },
                                    { id: 'payroll_type' },
                                    { id: 'employee_name' }
                                ] }
                                showFilters={ this.state.showFilters }
                                selectable
                                onPageChange={ this.onPageChange }
                                onFilteringChange={ this.onFilteringChange }
                                onPageSizeChange={ this.onPageSizeChange }
                                page={ this.state.tablePayslipsPage }
                                pageSize={ this.state.tablePayslipsPageSize }
                                sizeOptions={ [ 10, 20, 50 ] }
                                pages={ this.props.payslips.pagination.last_page || 1 }
                                manual
                            />
                        </TableViewWrapper>
                    )
                }
                <A id="downloadLink"></A>
            </PayslipViewWrapper>
        );
    }
}

export default PayslipView;
