/* eslint-disable camelcase */
import {
    INITIAL_DATA,
    GET_PAYSLIP_DATA,
    GET_PAYSLIP_DOWNLOAD_LINK,
    GET_PAYSLIP_BULK_DOWNLOAD_LINK,
    SEND_INDIVIDUAL_PAYSLIP
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data for payslip page
 */
export function initializeData( payload ) {
    return {
        type: INITIAL_DATA,
        payload
    };
}

/**
 * Fetch payslip data from server
 */
export function getPayslipData( companyId, ids, startDate, endDate, includeSpecial = false, page = 1, perPage = 10, filters = {}, payslip_status ) {
    return {
        type: GET_PAYSLIP_DATA,
        payload: {
            companyId,
            ids,
            startDate,
            endDate,
            includeSpecial,
            page,
            perPage,
            filters,
            payslip_status
        }
    };
}

/**
 * Gets the payslip download link for either view or download
 */
export function getPayslipDownloadLink( id, download ) {
    return {
        type: GET_PAYSLIP_DOWNLOAD_LINK,
        payload: {
            id,
            download
        }
    };
}

/**
 * Gets the payslip bulk download link
 */
export function getPayslipBulkDownloadLink( ids ) {
    return {
        type: GET_PAYSLIP_BULK_DOWNLOAD_LINK,
        payload: ids
    };
}

/**
 * Send payslip
 */
export function sendPayslip( payrollId, employeeId ) {
    return {
        type: SEND_INDIVIDUAL_PAYSLIP,
        payload: { payrollId, employeeId }
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
