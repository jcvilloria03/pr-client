/*
 *
 * Payslip constants
 *
 */

export const INITIAL_DATA = 'app/Payslip/INITIAL_DATA';

export const GET_PAYSLIP_DATA = 'app/Payslip/GET_PAYSLIP_DATA';
export const SET_PAYSLIP_DATA = 'app/Payslip/SET_PAYSLIP_DATA';
export const SET_PAYSLIP_LOADING = 'app/Payslip/SET_PAYSLIP_DATA_LOADING';
export const SET_PAYSLIP_PAGINATION = 'app/Payslip/SET_PAYSLIP_PAGINATION';

export const GET_PAYSLIP_DOWNLOAD_LINK = 'app/Payslip/GET_PAYSLIP_DOWNLOAD_LINK';
export const GET_PAYSLIP_BULK_DOWNLOAD_LINK = 'app/Payslip/GET_PAYSLIP_BULK_DOWNLOAD_LINK';
export const SET_PAYSLIP_DOWNLOAD_LINK = 'app/Payslip/SET_PAYSLIP_DOWNLOAD_LINK';
export const SEND_INDIVIDUAL_PAYSLIP = 'app/Payslip/SEND_INDIVIDUAL_PAYSLIP';

export const SET_BULK_DOWNLOADING = 'app/Payslip/SET_BULK_DOWNLOADING';

export const SET_PAYROLL_GROUP_DATA = 'app/Payslip/SET_PAYROLL_GROUP_DATA';
export const SET_PAYROLL_GROUP_LOADING = 'app/Payslip/SET_PAYROLL_GROUP_DATA_LOADING';

export const SET_COMPANY_DATA = 'app/Payslip/SET_COMPANY_DATA';

export const NOTIFICATION_SAGA = 'app/Payslip/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Payslip/NOTIFICATION';
