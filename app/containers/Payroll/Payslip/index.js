import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';
import { Container } from 'reactstrap';

import SubHeader from '../../SubHeader';
import SnackBar from '../../../components/SnackBar';
import { H3 } from '../../../components/Typography';
import SalConfirm from '../../../components/SalConfirm';
import PayslipView from './templates/payslips';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { PAYROLL_SUBHEADER_ITEMS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import * as viewActions from './actions';
import {
    makeSelectPayslip,
    makeSelectPayrollGroup,
    makeSelectCompanyId,
    makeSelectPayslipDownload,
    makeSelectBulkDownloading,
    makeSelectNotification
} from './selectors';

import {
    ContentWrapper,
    ConfirmBodyWrapperStyle
} from './styles';

/**
 * Payslip component
 */
export class Payslip extends React.Component {
    static propTypes = {
        initializeData: React.PropTypes.func,
        getPayslipData: React.PropTypes.func,
        getPayslipDownloadLink: React.PropTypes.func,
        getPayslipBulkDownloadLink: React.PropTypes.func,
        sendPayslip: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        payslips: React.PropTypes.shape({
            data: React.PropTypes.arrayOf(
                React.PropTypes.object
            ),
            loading: React.PropTypes.bool
        }).isRequired,
        payrollGroups: React.PropTypes.shape({
            data: React.PropTypes.arrayOf(
                React.PropTypes.object
            ),
            loading: React.PropTypes.bool
        }).isRequired,
        companyId: React.PropTypes.oneOfType([
            React.PropTypes.number,
            React.PropTypes.object
        ]),
        payslipDownload: React.PropTypes.shape({
            id: React.PropTypes.number,
            uri: React.PropTypes.string,
            download: React.PropTypes.bool,
            bulk: React.PropTypes.bool,
            email: React.PropTypes.string
        }).isRequired,
        bulkDownloading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    }

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            bulkDownload: {
                email: '',
                showConfirm: false,
                count: 0
            },
            defaultFilter: {
                ids: '',
                startDate: moment().subtract( 30, 'days' ).format( 'YYYY-MM-DD' ),
                endDate: moment().format( 'YYYY-MM-DD' )
            }
        };

        this.showBulkDownloadConfirm = this.showBulkDownloadConfirm.bind( this );
    }

    /**
     * Initializes initial data before rendering to DOM
     */
    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        this.initializeDefaultFilter();
    }

    /**
     * Initializes payslip view page with initial data
     */
    componentDidMount() {
        isAuthorized(['view.payslip'], ( authorized ) => { !authorized && browserHistory.replace( '/unauthorized' ); });

        this.props.initializeData( Object.assign({}, this.state.defaultFilter ) );
    }

    /**
     * Perform any action before component receive new props
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.payslipDownload.email !== this.props.payslipDownload.email && this.showBulkDownloadConfirm( nextProps.payslipDownload );
    }

    /**
     * Called before component unmounts.
     */
    componentWillUnmount() {
        this.props.resetStore();
    }

    /**
     * Initializes default filter
     */
    initializeDefaultFilter() {
        const { query } = browserHistory.getCurrentLocation();
        if ( query ) {
            const { ids, startDate, endDate } = this.state.defaultFilter;

            this.setState({
                defaultFilter: Object.assign({}, this.state.defaultFilter, {
                    ids: query.id || ids,
                    payrollId: query.payroll_id || null,
                    startDate: query.date || startDate,
                    endDate: query.date || endDate
                })
            });
        }
    }

    /**
     * Displays email bulk download if selected payslips is more than 50
     */
    showBulkDownloadConfirm( payslipDownload ) {
        payslipDownload.bulk
            && payslipDownload.email
            && this.setState({ bulkDownload: { showConfirm: false }},
                () => this.setState({ bulkDownload: {
                    email: payslipDownload.email,
                    showConfirm: true,
                    count: this.payslipView.state.selection.count
                }})
            );
    }

    /**
     * Renders Payslip component to DOM
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Payslips"
                    meta={ [
                        { name: 'description', content: 'Description of Payslips' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    title="Payslip download notification"
                    onConfirm={ () => { } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div>
                                A download link will be sent to { this.state.bulkDownload.email }
                                <br />once the <strong>{ this.state.bulkDownload.count } payslips</strong> are available.
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    visible={ this.state.bulkDownload.showConfirm }
                    confirmText="Okay"
                    showCancel={ false }
                />
                <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                <ContentWrapper>
                    <Container>
                        <div className="heading">
                            <H3>Payslips</H3>
                            <p>
                                View employee payslips through this page.
                                You can filter your search via payroll group
                                and payroll period. Payroll period refers to
                                the pay date of the generated payroll.
                            </p>
                        </div>
                        <PayslipView
                            ref={ ( ref ) => { this.payslipView = ref; } }
                            companyId={ this.props.companyId }
                            defaultFilter={ this.state.defaultFilter }
                            payslips={ this.props.payslips }
                            payrollGroups={ this.props.payrollGroups }
                            payslipDownload={ this.props.payslipDownload }
                            sendPayslip={ this.props.sendPayslip }
                            bulkDownloading={ this.props.bulkDownloading }
                            getPayslipData={ this.props.getPayslipData }
                            getPayslipDownloadLink={ this.props.getPayslipDownloadLink }
                            getPayslipBulkDownloadLink={ this.props.getPayslipBulkDownloadLink }
                        />
                    </Container>
                </ContentWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    payslips: makeSelectPayslip(),
    payrollGroups: makeSelectPayrollGroup(),
    companyId: makeSelectCompanyId(),
    payslipDownload: makeSelectPayslipDownload(),
    bulkDownloading: makeSelectBulkDownloading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        viewActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Payslip );
