/* eslint-disable camelcase */
import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import {
    INITIAL_DATA,
    GET_PAYSLIP_DATA,
    SET_PAYSLIP_DATA,
    SET_PAYSLIP_LOADING,
    SET_PAYSLIP_PAGINATION,
    SET_PAYROLL_GROUP_DATA,
    SET_PAYROLL_GROUP_LOADING,
    SET_COMPANY_DATA,
    GET_PAYSLIP_DOWNLOAD_LINK,
    GET_PAYSLIP_BULK_DOWNLOAD_LINK,
    SET_PAYSLIP_DOWNLOAD_LINK,
    SET_BULK_DOWNLOADING,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    SEND_INDIVIDUAL_PAYSLIP
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data for payslip page
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: SET_PAYSLIP_LOADING,
            payload: true
        });
        yield put({
            type: SET_PAYROLL_GROUP_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        yield put({
            type: SET_COMPANY_DATA,
            payload: companyId
        });

        if ( companyId ) {
            const payrollGroups = yield call( getPayrollGroupData, companyId );

            let { ids, includeSpecial } = payload;
            const { payrollId, startDate, endDate } = payload;
            if ( typeof ( includeSpecial ) === 'undefined' || includeSpecial === null ) {
                includeSpecial = ( ids === 'null' || !ids );
            }
            ids = ( Array.isArray( ids ) ) ? ids : Object.keys( payrollGroups ).join();

            const page = 1;
            const perPage = 10;
            const filters = {
                employee_name: ''
            };
            const payslip_status = '';
            yield call( getPayslipData, { payload: Object.assign( payload, { companyId, payrollId, ids, startDate, endDate, includeSpecial, page, perPage, filters, payslip_status }) });
        }
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_PAYROLL_GROUP_LOADING,
            payload: false
        });
        yield put({
            type: SET_PAYSLIP_LOADING,
            payload: false
        });
    }
}

/**
 * Get payslip data
 */
export function* getPayslipData({ payload }) {
    try {
        yield put({
            type: SET_PAYSLIP_LOADING,
            payload: true
        });
        const { companyId, payrollId, ids, startDate, endDate, includeSpecial, page, perPage, filters, payslip_status } = payload;
        window.timerComponent.handlePause();
        const response = yield call(
            Fetch,
            `/company/${companyId}/payslips?payroll_id=${payrollId}&payroll_group_ids=${ids}&date_from=${startDate}&date_to=${endDate}&include_special=${Number( includeSpecial )}&page=${page}&per_page=${perPage}&employee_name=${filters.employee_name}&payslip_status=${payslip_status}`,
            { method: 'GET' }
        );
        window.timerComponent.handleResume();

        const paginationData = {
            from: response.from || 1,
            to: response.to || 1,
            total: response.total || 1,
            current_page: response.current_page || 1,
            last_page: response.last_page || 1
        };

        yield put({
            type: SET_PAYSLIP_DATA,
            payload: response.data || []
        });
        yield put({
            type: SET_PAYSLIP_PAGINATION,
            payload: paginationData
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_PAYSLIP_LOADING,
            payload: false
        });
    }
}

/**
 * Get payroll group data
 */
export function* getPayrollGroupData( companyId ) {
    let payrollGroups = {};

    try {
        window.timerComponent.handlePause();
        const response = yield call( Fetch, `/company/${companyId}/payslips/authorized_payroll_groups`, { method: 'GET' });
        window.timerComponent.handleResume();
        payrollGroups = response || {};

        yield put({
            type: SET_PAYROLL_GROUP_DATA,
            payload: payrollGroups
        });
    } catch ( error ) {
        window.timerComponent.handleResume();
        yield call( notifyUser, error );
    }

    return payrollGroups;
}

/**
 * Gets the payslip download link for either view or download
 */
export function* getPayslipDownloadLink({ payload }) {
    try {
        const { id, download } = payload;
        window.timerComponent.handlePause();
        const response = yield call( Fetch, `/payslip/${id}?download=${download}`, { method: 'GET' });
        window.timerComponent.handleResume();
        if ( response ) {
            yield put({
                type: SET_PAYSLIP_DOWNLOAD_LINK,
                payload: {
                    id,
                    uri: response.uri,
                    download
                }
            });
        }
    } catch ( error ) {
        window.timerComponent.handleResume();
        yield call( notifyUser, error );
    }
}

/**
 * Gets the payslip bulk download link for
 */
export function* getPayslipBulkDownloadLink({ payload }) {
    try {
        yield put({
            type: SET_BULK_DOWNLOADING,
            payload: true
        });

        let email = '';
        let uri = '';
        window.timerComponent.handlePause();
        const response = yield call( Fetch, '/payslip/download_multiple', {
            method: 'POST',
            data: {
                ids: payload
            }
        });
        window.timerComponent.handleResume();
        if ( response.is_email ) {
            email = response.email_address;
        } else {
            const uriResponse = yield call( Fetch, `/payslip/zipped/${response.id}/url`, { method: 'GET' });
            uri = uriResponse.uri;
        }

        yield put({
            type: SET_PAYSLIP_DOWNLOAD_LINK,
            payload: {
                uri,
                bulk: true,
                email,
                download: true
            }
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_BULK_DOWNLOADING,
            payload: false
        });
        yield put({
            type: SET_PAYSLIP_DOWNLOAD_LINK,
            payload: {
                uri: '',
                bulk: true,
                email: '',
                download: true
            }
        });
    }
}

/**
 * Send payslip
 */
export function* sendPayslip({ payload }) {
    const { payrollId, employeeId } = payload;
    const formData = new FormData();
    formData.append( 'employeeId', employeeId );
    try {
        window.timerComponent.handlePause();
        yield call( Fetch, `/payroll/${payrollId}/send_single_payslip`, {
            method: 'POST',
            data: formData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
        window.timerComponent.handleResume();
        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                title: 'Success',
                message: 'Payslip sent!',
                show: true,
                type: 'success'
            }
        });

        yield call( delay, 2000 );
        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                title: ' ',
                message: ' ',
                show: false,
                type: 'success'
            }
        });
    } catch ( error ) {
        window.timerComponent.handleResume();
        yield call( notifyUser, error );
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watcher for INITIAL_DATA
 *
 */
export function* watchForInitialData() {
    const watcher = yield takeEvery( INITIAL_DATA, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_PAYSLIP_DATA
 *
 */
export function* watchForGetPayslipData() {
    const watcher = yield takeEvery( GET_PAYSLIP_DATA, getPayslipData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_PAYSLIP_DOWNLOAD_LINK
 *
 */
export function* watchForGetPayslipDownloadLink() {
    const watcher = yield takeEvery( GET_PAYSLIP_DOWNLOAD_LINK, getPayslipDownloadLink );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_PAYSLIP_BULK_DOWNLOAD_LINK
 *
 */
export function* watchForGetPayslipBulkDownloadLink() {
    const watcher = yield takeEvery( GET_PAYSLIP_BULK_DOWNLOAD_LINK, getPayslipBulkDownloadLink );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Send payslip
 */
export function* watchForSendPayslip() {
    const watcher = yield takeLatest( SEND_INDIVIDUAL_PAYSLIP, sendPayslip );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
// All sagas to be loaded
export default [
    watchForGetPayslipData,
    watchForInitialData,
    watchForGetPayslipDownloadLink,
    watchForGetPayslipBulkDownloadLink,
    watchForNotifyUser,
    watchForReinitializePage,
    watchForSendPayslip
];
