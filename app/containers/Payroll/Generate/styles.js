import styled, { keyframes } from 'styled-components';
import Container from 'reactstrap/lib/Container';

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding-top: 90px;
`;

export const PageWrapper = styled.div`
    .selected .fill {
        fill: #fff;
    }

    .selected .stroke {
        stroke: #fff;
    }

    .fill {
        fill: #00a5e2;
    }

    .stroke {
        stroke: #00a5e2;
    }
`;

export const OuterWrapper = styled.div`
    padding: 5vh 0;
`;

export const MainWrapper = styled( Container )`
    display: flex;
    color: #444;
    justify-content: center;
    align-items: center;
    flex-direction: column;

    .payrollHeader {
        width: 900px;
        text-align: center;

        .stepper {
            margin-top: 0;
            margin-bottom: 1rem;
        }
    }
`;

export const StyledGeneratePayroll = styled.section`
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: #fff;
    width: 100%;
    padding: 20px 30px;

    .header {
        text-align: center;

        h3 {
            margin: 0;
        }

        .instruction {
            text-align: center;
            margin: 2rem 0;
            width: 700px;
        }
    }

    .content {
        width: 700px;

        .form {
            width: 100%;
            height: 400px;
            border: 1px solid #ececec;
            border-radius: 5px;
            padding-top: 0;

            .selection-tabs {
                display: flex;
                flex-direction: row;
                justify-content: space-around;
                align-items: center;

                .tab {
                    cursor: pointer;
                    background-color: #ececec;
                    border-top: 3px solid #ececec;
                    color: #474747;
                    padding: 1rem 0;
                    width: 100%;
                    text-align: center;
                }

                .active {
                    color: #00a5e5;
                    border-top: 3px solid #00a5e5;
                    background-color: #fff;
                }
            }
        }

        .selection-form {
            padding: 1rem;
            height: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;

            .message {
                height: 100%;
                display: flex;
                flex-direction: column;
                align-items: center;
                text-align: center;
                justify-content: center;
            }

            .regular-selection {
                width: 450px;

                .control-group {
                    label,
                    span {
                        font-size: 18px;
                        margin-top: 5px;
                        margin-right: 20px;
                        vertical-align: top;
                    }

                    .select-control {
                        width: 100% !important;
                        margin: 1.5rem 0;

                        .Select-loading {
                            margin-top: 13px;
                        }

                        .Select-arrow-zone {
                            padding-top: 8px;
                        }
                    }
                }

                .annualize {
                    display: flex;
                    justify-content: flex-start;
                    align-items: center;

                    span:first-of-type {
                        margin: 3.5px 5px 0 0;
                    }
                }
            }

            .info {
                border: 1px solid #ececec;
                margin: 1rem 0;
                padding: 0.5rem 0;

                p {
                    margin: 0.5rem;
                }
            }
        }

        .table-loading {
            margin-top: 2.5rem;
        }
    }

    .footer {
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        margin-top: 2rem;

        button {
            margin: 0 .5rem;
            padding: 6px 21px;
            border-radius: 5px;
            font-size: 14px;
            line-height: 1.5;
            display: inline;
            vertical-align: middle;
            cursor: pointer;

            &:focus {
                outline: none;
                box-shadow: none;
            }
        }


        .cancel-btn {
            &:hover {
                color: #fff;
                background-color: #EF8184;
            }
        }
    }
`;

export const PayrollTableWrapper = styled.div`
    width: 100%;

    .title {
        font-weight: bold;
    }

    .header {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        height: 45px;

        .regenerate {
            height: 40px;
            width: 40px;

            &:focus,
            &:active:focus {
                outline: none;
                background-color: #fff;
                border-radius: 50%;
                color: #4aba4a;
            }
        }
    }

    .content {
        background-color: #fff;
        padding: 1rem;
        padding-bottom: 3rem;

        .payroll-info {
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            border: 1px solid #ececec;
            width: 700px;
            margin: 1rem auto;
            padding: 1rem;

            hr {
                border: unset;
                width: 100%;
                border-top: 1px solid #ececec;
            }

            .info {
                display: flex;
                flex-direction: row;
                justify-content: flex-start;
                align-items: center;
                text-align: left;

                p {
                    margin-bottom: 0;
                }

                .title {
                    text-transform: capitalize;
                    font-weight: unset;
                }

                p:first-of-type {
                    width: 150px;
                }

                p:last-of-type {
                    color: #00a5e5;
                }
            }
        }

        .generate-table {
            .rt-table {
                .rt-thead {
                    .rt-tr {
                        .rt-resizable-header-content {
                            font-weight: 400;
                        }
                    }
                }
            }
        }
    }

    .header {
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: flex-end;
        flex-direction: row;
        margin-bottom: 10px;

        button {
            border-radius: 5px;
        }

        .search-wrapper {
            flex-grow: 1;
            display: flex;
            align-items: center;
            flex-direction: row;
            justify-content: flex-start;

            .search {
                width: 300px;
                border: 2px solid #cccccc;
                border-radius: 5px;
                margin-right: 3px;

                .input-group {
                    height: 35px;

                    input {
                        height: 35px;
                        border: none;
                    }

                    .icon {
                        font-size: 18px;
                        color: #83d24b;
                        display: flex;
                        width: 18px;

                        svg {
                            height: 18px;
                            width: 18px;
                        }

                        > i {
                            align-self: center;
                        }
                    }
                }
            }

            p {
                display: none;
            }

            .input-group,
            .form-control {
                background-color: transparent;
            }

            .input-group-addon {
                background-color: transparent;
                border: none;
            }

            .isvg {
                display: inline-block;
                width: 1rem;
            }

            .filter-icon > svg {
                height: 10px;
            }
        }

        .dropdown-wrapper {
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: flex-end;

            .icon-button {
                font-size: 20px;
                font-weight: bold;
                color: white;
                display: flex;
                width: 20px;
                margin-right: 0.5rem;

                > i {
                    align-self: center;
                }
            }

            .btn {
                margin: 0 .3rem;

                > span {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    flex-direction: row;
                }
            }

            .sl-c-text-align-right {
                font-size: 14px;
            }

            :last-child {
                margin-left: 1px;
            }

            .dropdown {
                margin: 0 .3rem;

                .dropdown-toggle {
                    padding: 7px 21px;
                    border-radius: 5px;
                }

                .icon {
                    font-size: 20px;
                    font-weight: bold;
                    color: white;
                    display: flex;
                    width: 20px;
                    margin-right: 0.5rem;

                    > i {
                        align-self: center;
                    }
                }

                .dropdown-menu {
                    text-align: left;
                    font-size: 14px;
    
                    .dropdown-item {
                        display: unset;
                        padding: 0.5rem 1.5rem 0.5rem 1rem;
                        > div {
                            display: flex;
                            flex-direction: row;
                            align-items: center;
                            justify-content: flex-start;
    
                            .icon {
                                font-size: 15px;
                                color: #00a4e4;
                                display: flex;
                                width: 15px;
                                margin-right: 0.8rem;
                            }
                        }
                    }
                }
            }

            .error-button-wrapper {
                display: flex;
                flex-direction: row;
                align-items: flex-start;
                margin-right: -1rem;

                .error-count {
                    margin-left: -10px;
                    margin-top: -5px;
                    background-color: #FF0000;
                    color: white;
                    line-height: 1.2;
                    height: 18px;
                    width: 18px;
                    text-align: center;
                    border-radius: 50%;
                    font-size: 13px;
                }

                .error-button {
                    background-color: #ffc9c4;
                    padding: 9px;
                    margin: .3rem;
    
                    .icon {
                        font-size: 20px;
                        font-weight: bold;
                        color: #FF0000;
                        display: flex;
                        width: 20px;
    
                        > i {
                            align-self: center;
                        }
                    }
                }
            }
        }
    }
`;

export const anim3 = keyframes`
    to {
        transform: rotate(45deg) translate(3px, 3px);
    }
`;

export const StyledLoader = styled.div`
    font-size: 16px;
    font-weight: 600;
    color: #00a5e5;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;

    span {
        color: white;
    }

    .anim3 {
        padding-left: 10px;

        &:before {
            display: block;
            width: 12px;
            height: 12px;
            margin-top: -1px;
            border: 2px solid #00a5e5;
            content: '';
            animation: ${anim3} 0.5s ease infinite alternate;
            border-radius: 100% 100% 0 100%;
            transform: rotate(45deg);
        }

        &:after {
            display: block;
            width: 12px;
            height: 3px;
            margin-top: 8px;
            background-color: rgba(0, 0, 0, 0.2);
            content: '';
            border-radius: 100%;
        }
    }
`;

export const UploadPageWrapper = styled.div`
    padding-top: 75px;

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;

        button {
            min-width: 120px;
        }
    }
`;

export const FormWrapper = styled.div`
    > div {
        & > h4 {
            text-align: left;
        }

        & > button {
            padding: 12px 40px;
            margin-top: 2rem;
        }

        .steps {
            display: flex;
            align-items: stretch;
            justify-content: space-between;
            margin: 1rem 0;

            > .step {
                width: 49%;
            }
        }

        .upload > div:first-of-type > div > div {
            margin: 0 auto;
        }

        .errors {
            margin-bottom: 5rem;

            .rt-thead .rt-tr {
                background: #f21108;
                color: #fff;
            }

            .rt-tbody .rt-tr {
                background: rgba(242, 17, 8, 0.1);
            }

            .ReactTable.-striped .rt-tr.-odd {
                background: rgba(242, 17, 8, 0.2);
            }

            .ReactTable .rt-th,
            .ReactTable .rt-td {
                flex: 1 0 0px;
                white-space: initial;
                text-overflow: ellipsis;
                padding: 7px 20px;
                overflow: hidden;
                transition: 0.3s ease;
                transition-property: width, min-width, padding, opacity;
            }

            .react-bs-container-body tr {
                background: rgba(249, 210, 210, 0.8);

                &:hover {
                    background: rgba(249, 210, 210, 1);
                }
            }
        }
    }
`;

export const NavWrapper = styled.div`
    padding: 15px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

export const HeadingWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;

    h3 {
        font-weight: 600;
    }

    p {
        text-align: center;
        max-width: 800px;
    }
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;

export const EmployeeName = styled.span`
    display: flex;
    gap: 0.5rem;
    align-items: center;

    .link {
        text-decoration: underline !important;
    }
`;

export const EmployeePhoto = styled.img`
    width: 1.5rem;
    height: 1.5rem;
    object-fit: cover;
    object-position: center;
    border: 1px solid #bbb;
    border-radius: 50%;
    background-color: #ccc;
`;

export const AmountWrapper = styled.p`
    background-color: ${( props ) => ( props.isPossitive ? 'rgba(63, 183, 143, .2)' : 'rgba( 239, 129, 132, 0.2 )' )};
    color: ${( props ) => ( props.isPossitive ? '#3FB78F' : '#f21108' )};
    padding: 5px 1rem;
    border-radius: 5px;
    margin-bottom: 0;
`;

export const ModalBodyWrapper = styled.div`
    .header {
        display: flex;
        justify-content: space-between;
        align-items: center;
        flex-direction: row;
        margin-bottom: 1rem;

        p {
            margin-bottom: 0;
        }

        a {
            margin-top: 0;
        }
    }

    .errors {
        p:first-of-type {
            float: right;
        }

        .rt-thead .rt-tr {
            background: #F21108;
            color: #fff;
        }

        .rt-thead .rt-th:last-child {
            padding-left: 30px;
        }

        .ReactTable.-highlight .rt-tr.-even {
            background: white
        }
    }
`;
