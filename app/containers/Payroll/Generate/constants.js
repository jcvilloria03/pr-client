/* eslint-disable no-prototype-builtins */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/prop-types */
import React from 'react';
import moment from 'moment';

import { formatCurrency, formatDate } from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

import {
    EmployeeName,
    EmployeePhoto,
    AmountWrapper
} from './styles';

/*
 *
 * Payroll Create constants
 *
 */

const namespace = 'app/Containers/Payroll/Generate';

export const INITIALIZE = `${namespace}/INITIALIZE`;
export const RESET_STORE = `${namespace}/RESET_STORE`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;

export const RUN_PAYROLL = `${namespace}/RUN_PAYROLL`;
export const CREATE_REGULAR_PAYROLL = `${namespace}/CREATE_REGULAR_PAYROLL`;
export const UPLOAD_ATTENDANCE = `${namespace}/UPLOAD_ATTENDANCE`;
export const SET_UPLOAD_STATUS = `${namespace}/SET_UPLOAD_STATUS`;
export const SET_UPLOAD_ERROR = `${namespace}/SET_UPLOAD_ERROR`;

export const GET_PAYROLL_GROUPS = `${namespace}/GET_PAYROLL_GROUPS`;
export const GET_PAYROLL_GROUP_PERIODS = `${namespace}/GET_PAYROLL_GROUP_PERIODS`;
export const GET_REGULAR_PAY_RUN = `${namespace}/GET_REGULAR_PAY_RUN`;
export const GET_SPECIAL_PAY_RUN = `${namespace}/GET_SPECIAL_PAY_RUN`;
export const GET_FINAL_PAY_RUN = `${namespace}/GET_FINAL_PAY_RUN`;
export const GET_PAYROLL_VALIDATION = `${namespace}/GET_PAYROLL_VALIDATION`;

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_TABLE_LOADING = `${namespace}/SET_TABLE_LOADING`;
export const SET_PAGINATION = `${namespace}/SET_PAGINATION`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const SET_PAYROLL_GROUPS = `${namespace}/SET_PAYROLL_GROUPS`;
export const SET_PAYROLL_GROUP_PERIODS = `${namespace}/SET_PAYROLL_GROUP_PERIODS`;
export const SET_PAYROLL_ID = `${namespace}/SET_PAYROLL_ID`;
export const SET_REGULAR_PAY_RUN = `${namespace}/SET_REGULAR_PAY_RUN`;
export const SET_SPECIAL_PAY_RUN = `${namespace}/SET_SPECIAL_PAY_RUN`;
export const SET_FINAL_PAY_RUN = `${namespace}/SET_FINAL_PAY_RUN`;
export const SET_PAYROLL_VALIDATION_ERRORS = `${namespace}/SET_PAYROLL_VALIDATION`;
export const SET_ERRORS_CSV = `${namespace}/SET_ERRORS_CSV`;

export const FORCE_FETCH_ATTENDANCE_STATUS = `${namespace}/FORCE_FETCH_ATTENDANCE_STATUS`;

export const START_OF_MONTH = moment().startOf( 'month' ).format( DATE_FORMATS.API );
export const END_OF_MONTH = moment().endOf( 'month' ).format( DATE_FORMATS.API );

export const PROFILE_PICTURE = 'https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/placeholder.png';

export const ATTENDANCE_TEMPLATE_LINK = 'https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/time_and_attendance_payroll_template.csv';

export const DROPDOWN_OPTIONS = [
    {
        label: 'Adjustments',
        icon: 'cog',
        link: '/adjustments'

    },
    {
        label: 'Allowances',
        icon: 'money',
        link: '/allowances'
    },
    {
        label: 'Bonuses',
        icon: 'moneyBag',
        link: '/bonuses'
    },
    {
        label: 'Commissions',
        icon: 'percentage',
        link: '/commissions'
    },
    {
        label: 'Deductions',
        icon: 'minusDollar',
        link: '/deductions'
    },
    {
        label: 'Loans',
        icon: 'handMoneyBag',
        link: '/loans'
    }
];

export const REGULAR_PAY_RUN_COLUMNS = [
    {
        id: 'id',
        header: 'id',
        accessor: 'id',
        minWidth: 150,
        show: false
    },
    {
        id: 'employee_name',
        header: 'Employees',
        sortable: true,
        accessor: ( row ) => {
            const firstName = get( row, 'first_name', '' );
            const lastName = get( row, 'last_name', '' );
            const picture = get( row, 'picture' );

            return (
                <EmployeeName>
                    <EmployeePhoto src={ picture || PROFILE_PICTURE } />

                    {`${firstName} ${lastName}`.trim()}
                </EmployeeName>
            );
        },
        minWidth: 150

    },
    {
        id: 'position',
        header: 'Position',
        sortable: true,
        accessor: ( row ) => row.position_name,
        minWidth: 100

    },
    {
        id: 'basic_pay',
        header: 'Basic Pay',
        sortable: true,
        accessor: ( row ) => ( <span>{formatCurrency( row.base_pay )} Php/Month</span> ),
        minWidth: 150

    },
    {
        id: 'attendance',
        header: 'Attendance',
        sortable: true,
        accessor: ( row ) => !isEmpty( row.attendances ) ? `${row.attendances.total_scheduled_hours.split( ' ', 1 )[ 0 ]} Hrs` : '0 Hrs',
        minWidth: 110
    },
    {
        id: 'additional_income',
        header: 'Additional Income',
        sortable: true,
        accessor: ( row ) => ( <span>{formatCurrency( row.incomes )} Php</span> ),
        minWidth: 110
    },
    {
        id: 'total_deductions',
        header: 'Total Deductions',
        sortable: true,
        accessor: ( row ) => {
            const isPossitive = row.deductions > 0;

            return (
                row.deductions !== 0
                ? <AmountWrapper isPossitive={ isPossitive }>
                    {isPossitive ? '+' : '-'} {formatCurrency( row.deductions )} Php
                </AmountWrapper>
                : <span>{formatCurrency( row.deductions )} Php</span>
            );
        },
        minWidth: 110
    }
];

export const SPECIAL_PAY_RUN_COLUMNS = [
    {
        id: 'id',
        header: 'id',
        accessor: 'id',
        minWidth: 150,
        show: false
    },
    {
        id: 'employee_name',
        header: 'Employees',
        sortable: true,
        accessor: ( row ) => {
            const firstName = get( row, 'first_name', '' );
            const lastName = get( row, 'last_name', '' );
            const picture = get( row, 'picture' );

            return (
                <EmployeeName>
                    <EmployeePhoto src={ picture || PROFILE_PICTURE } />

                    {`${firstName} ${lastName}`.trim()}
                </EmployeeName>
            );
        },
        minWidth: 150

    },
    {
        id: 'position',
        header: 'Position',
        sortable: true,
        accessor: ( row ) => row.position ? row.position.name : '',
        minWidth: 100

    },
    {
        id: 'special_pay_item',
        header: 'Special Pay Item',
        sortable: true,
        accessor: ( row ) => row.special_pay_item,
        minWidth: 150

    },
    {
        id: 'gross_amount',
        header: 'Gross Amount',
        sortable: false,
        accessor: ( row ) => ( <span>{formatCurrency( row.gross_amount )} Php</span> ),
        minWidth: 110
    },
    {
        id: 'release_date',
        header: 'Release Date',
        sortable: true,
        accessor: ( row ) => formatDate( row.release_detail.date, DATE_FORMATS.DISPLAY ),
        minWidth: 135
    }];

export const FINAL_PAY_RUN_COLUMNS = [
    {
        id: 'id',
        header: 'id',
        accessor: 'id',
        minWidth: 150,
        show: false
    },
    {
        id: 'employee_name',
        header: 'Employees',
        sortable: true,
        accessor: ( row ) => {
            const picture = get( row.attributes, 'picture' );

            return (
                <EmployeeName>
                    <EmployeePhoto src={ picture || PROFILE_PICTURE } />

                    {row.attributes.employee_name.trim()}
                </EmployeeName>
            );
        },
        minWidth: 150
    },
    {
        id: 'position',
        header: 'Position',
        sortable: true,
        accessor: ( row ) => row.attributes.position ? row.attributes.position.name : '',
        minWidth: 100

    },
    {
        id: 'department',
        header: 'Department',
        sortable: true,
        accessor: ( row ) => row.attributes.department ? row.attributes.department.name : '',
        minWidth: 100
    },
    {
        id: 'last_period_covered',
        header: 'Last Period Covered',
        sortable: true,
        accessor: ( row ) => formatDate( row.attributes.last_date, DATE_FORMATS.DISPLAY ),
        minWidth: 135
    },
    {
        id: 'release_date',
        header: 'Release Date',
        sortable: true,
        accessor: ( row ) => formatDate( row.attributes.release_date, DATE_FORMATS.DISPLAY ),
        minWidth: 135
    }
];

