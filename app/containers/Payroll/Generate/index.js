/* eslint-disable no-prototype-builtins */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';

import Modal from 'components/Modal';
import Stepper from 'components/Stepper';
import SnackBar from 'components/SnackBar';
import { H2, H3 } from 'components/Typography';

import { browserHistory } from 'utils/BrowserHistory';
import { PAYROLL_SUBHEADER_ITEMS } from 'utils/constants';
import { subscriptionService } from 'utils/SubscriptionService';

import UploadAttendance from './UploadAttendance';

import * as generateActions from './actions';
import {
    OuterWrapper,
    MainWrapper,
    LoadingStyles
} from './styles';
import {
    makeSelectLoading,
    makeSelectErrorsCsv,
    makeSelectPayrollId,
    makeSelectPagination,
    makeSelectFinalPayRun,
    makeSelectUploadStatus,
    makeSelectUploadErrors,
    makeSelectTableLoading,
    makeSelectNotification,
    makeSelectRegularPayRun,
    makeSelectSpecialPayRun,
    makeSelectPayrollGroupList,
    makeSelectValidationErrors,
    makeSelectPayrollGroupPeriods
} from './selectors';

import PayrollSelection from './templates/payrollSelection';
import ErrorTableModal from './templates/errorTableModal';
import PayrollTable from './templates/payrollTable';

import SubHeader from '../../SubHeader';

/**
 *
 * Generate
 *
 */
export class Generate extends React.PureComponent {
    static propTypes = {
        loading: React.PropTypes.bool,
        tableLoading: React.PropTypes.bool,
        resetStore: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        payrollID: React.PropTypes.number,
        products: React.PropTypes.array,
        payrollGroupList: React.PropTypes.array,
        payrollGroupPeriods: React.PropTypes.object,
        specialPayRun: React.PropTypes.array,
        finalPayRun: React.PropTypes.array,
        regularPayRun: React.PropTypes.object,
        runPayroll: React.PropTypes.func,
        getPayrollGroups: React.PropTypes.func,
        getPayrollGroupPeriods: React.PropTypes.func,
        forceFetchAttendanceStatus: React.PropTypes.func,
        getRegularPayRun: React.PropTypes.func,
        getSpecialPayRun: React.PropTypes.func,
        getFinalPayRun: React.PropTypes.func,
        validationErrors: React.PropTypes.array,
        createRegularPayroll: React.PropTypes.func,
        uploadAttendance: React.PropTypes.func,
        uploadStatus: React.PropTypes.string,
        uploadErrors: React.PropTypes.any,
        errorsCsv: React.PropTypes.string,
        setLoading: React.PropTypes.func
    }

    constructor( props ) {
        super( props );
        this.state = {
            step: 0,
            steps: [
                { label: 'Type of Payroll' },
                { label: 'Finalize Payroll Items' }
            ],
            type: 'regular',
            tableData: [],
            tablePagination: {
                page: 1,
                page_size: 10,
                total: 0
            },
            notification: {
                title: '',
                message: '',
                show: false,
                type: 'error'
            },
            selectedRegularPayroll: {}
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        let draftPayroll = localStorage.getItem( 'draft_payroll_jobs' );
        draftPayroll = draftPayroll ? JSON.parse( draftPayroll ) : {};

        if ( !isEmpty( draftPayroll ) ) {
            this.props.setLoading( true );

            if ( draftPayroll.type === 'regular' ) {
                this.props.getRegularPayRun({
                    payrollId: draftPayroll.id,
                    page: 1,
                    per_page: 10,
                    search_term: '',
                    get_validation: true
                });
            } else {
                this.handleTypeSelection({ active_tab: draftPayroll.type });
            }
        } else {
            this.props.getPayrollGroups();
        }
    }

    componentDidMount() {
        const searchParams = new URLSearchParams( document.location.search );
        const payrollId = searchParams.get( 'payrollId' );
        const jobId = searchParams.get( 'jobId' );

        if ( payrollId && jobId ) {
            this.props.forceFetchAttendanceStatus({ payrollId: Number( payrollId ), jobId });
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.regularPayRun, this.props.regularPayRun ) ) {
            this.setState({
                step: 1,
                type: 'regular',
                selectedRegularPayroll: nextProps.regularPayRun,
                tableData: nextProps.regularPayRun.employees,
                tablePagination: {
                    total: nextProps.pagination.total,
                    page: nextProps.pagination.current_page,
                    page_size: nextProps.pagination.per_page
                }
            });
        }

        if ( !isEqual( nextProps.specialPayRun, this.props.specialPayRun ) ) {
            this.setState({
                step: 1,
                type: 'special',
                tableData: nextProps.specialPayRun.map( ( item ) => ({
                    id: `${item.employee_id}_${item.release_detail.id}`,
                    ...item
                }) ),
                tablePagination: {
                    total: nextProps.pagination.total,
                    page: nextProps.pagination.page,
                    page_size: nextProps.pagination.perPage
                }
            });
        }

        if ( !isEqual( nextProps.finalPayRun, this.props.finalPayRun ) ) {
            this.setState({
                step: 1,
                type: 'final',
                tableData: nextProps.finalPayRun,
                tablePagination: {
                    total: nextProps.pagination.total,
                    page: nextProps.pagination.current_page,
                    page_size: nextProps.pagination.per_page
                }
            });
        }

        !isEqual( nextProps.notification, this.props.notification ) && this.setState({
            notification: nextProps.notification
        });
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    handleTypeSelection( data ) {
        switch ( data.active_tab ) {
            case 'special':
                this.setState({ type: data.active_tab });
                this.props.getSpecialPayRun({
                    page: 1,
                    per_page: 10,
                    search_term: ''
                });
                break;
            case 'final':
                this.setState({ type: data.active_tab });
                this.props.getFinalPayRun({
                    page: 1,
                    per_page: 10,
                    search_term: ''
                });
                break;

            default:
                this.setState({ type: data.active_tab });

                data.hasOwnProperty( 'payroll_group' ) &&
                this.props.createRegularPayroll({
                    payrollId: data.payroll_group.value,
                    start_date: data.payroll_period.value.start,
                    end_date: data.payroll_period.value.cutoff,
                    attendance_start_date: data.attendance_period.value.start,
                    attendance_end_date: data.attendance_period.value.cutoff,
                    search_term: '',
                    annualize: data.annualize
                });
                break;
        }
    }

    toggleModal() {
        this.errorModal && this.errorModal.toggle();
    }

    handleStepperChange( step ) {
        this.setState({ step });
    }

    /**
     *
     * Generate render method
     *
     */
    render() {
        const {
            payrollGroupPeriods,
            getPayrollGroupPeriods,
            getRegularPayRun,
            getSpecialPayRun,
            validationErrors,
            payrollGroupList,
            uploadAttendance,
            getFinalPayRun,
            tableLoading,
            uploadErrors,
            uploadStatus,
            runPayroll,
            errorsCsv,
            payrollID,
            loading
        } = this.props;

        const {
            selectedRegularPayroll,
            tablePagination,
            notification,
            tableData,
            type,
            steps,
            step
        } = this.state;

        let selection = '';
        let view = '';
        let upload = '';

        switch ( this.state.step ) {
            case 0:
                selection = (
                    <PayrollSelection
                        data={ payrollGroupList }
                        tableLoading={ tableLoading }
                        payrollGroupPeriods={ payrollGroupPeriods }
                        getPayrollGroupPeriods={ getPayrollGroupPeriods }
                        handleTypeSelection={ ( value ) => this.handleTypeSelection( value ) }
                    />
                );
                view = '';
                upload = '';
                break;
            case 1:
                view = (
                    <PayrollTable
                        handleStepperChange={ ( value ) => this.handleStepperChange( value ) }
                        toggleModal={ () => this.toggleModal() }
                        selectedRegularPayroll={ selectedRegularPayroll }
                        getRegularPayRun={ getRegularPayRun }
                        getSpecialPayRun={ getSpecialPayRun }
                        getFinalPayRun={ getFinalPayRun }
                        validationErrors={ validationErrors }
                        tableData={ tableData }
                        generateType={ type }
                        pagination={ tablePagination }
                        loading={ tableLoading }
                        runPayroll={ runPayroll }
                        payrollId={ payrollID }
                        step={ step }
                    />
                );
                selection = '';
                upload = '';
                break;
            case 2:
                upload = (
                    <UploadAttendance
                        handleStepperChange={ ( value ) => this.handleStepperChange( value ) }
                        uploadAttendance={ uploadAttendance }
                        uploadErrors={ uploadErrors }
                        uploadStatus={ uploadStatus }
                        payrollId={ payrollID }
                    />
                );
                selection = '';
                view = '';
                break;

            default:
                selection = (
                    <div className="loader">
                        <Container>
                            <LoadingStyles>
                                <H2>Loading Payroll Generate</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </Container>
                    </div>
                );
                view = '';
                upload = '';
        }

        return (
            <div>
                { step !== 2 && (
                <OuterWrapper>
                    <Helmet
                        title="Generate Payroll"
                        meta={ [
                          { name: 'Payroll', content: 'Generate Payroll' }
                        ] }
                    />
                    <SnackBar
                        message={ notification.message }
                        title={ notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ notification.show }
                        delay={ 5000 }
                        type={ notification.type }
                    />
                    <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                    { loading
                        ? (
                            <div className="loader">
                                <Container>
                                    <LoadingStyles>
                                        <H2>Loading Payroll Generate</H2>
                                        <br />
                                        <H3>Please wait...</H3>
                                    </LoadingStyles>
                                </Container>
                            </div>
                        ) : (
                            <div>
                                <MainWrapper>
                                    < div className="payrollHeader" >
                                        <H2>Payroll Generate</H2>
                                        <Stepper
                                            newStyle
                                            steps={ steps }
                                            activeStep={ step }
                                            size={ 35 }
                                            circleFontSize={ 14 }
                                            titleFontSize={ 14 }
                                            defaultColor="#818a91"
                                            defaultTitleColor="#818a91"
                                            completeColor="#83D24B"
                                            completeTitleColor="#83D24B"
                                            activeColor="#83D24B"
                                            activeTitleColor="#83D24B"
                                            useNumbers
                                        />
                                    </div>
                                    { selection }
                                </MainWrapper>
                                { view }
                            </div>
                        )
                    }
                    <Modal
                        ref={ ( ref ) => { this.errorModal = ref; } }
                        noFooter
                        size="lg"
                        title="Employee issues"
                        backdrop={ 'static' }
                        body={
                            <ErrorTableModal
                                data={ validationErrors }
                                csv={ errorsCsv }
                            />
                        }
                        buttons={ [] }
                    />
                </OuterWrapper>
                )}
                { upload }
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    errorsCsv: makeSelectErrorsCsv(),
    payrollID: makeSelectPayrollId(),
    pagination: makeSelectPagination(),
    finalPayRun: makeSelectFinalPayRun(),
    uploadStatus: makeSelectUploadStatus(),
    uploadErrors: makeSelectUploadErrors(),
    tableLoading: makeSelectTableLoading(),
    notification: makeSelectNotification(),
    regularPayRun: makeSelectRegularPayRun(),
    specialPayRun: makeSelectSpecialPayRun(),
    validationErrors: makeSelectValidationErrors(),
    payrollGroupList: makeSelectPayrollGroupList(),
    payrollGroupPeriods: makeSelectPayrollGroupPeriods()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        generateActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Generate );
