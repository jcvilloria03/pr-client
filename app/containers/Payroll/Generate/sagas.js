/* eslint-disable consistent-return */
/* eslint-disable camelcase */
/* eslint-disable no-inner-declarations */

import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import get from 'lodash/get';

import moment from 'moment';

import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';
import { DATE_FORMATS } from 'utils/constants';
import { company } from 'utils/CompanyService';
import { browserHistory } from 'utils/BrowserHistory';
import {
    processValidationErrors,
    processGetAttendanceErrors
} from 'utils/uploadValidationErrorsHelper';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import {
    setLoading,
    setTableLoading,
    setPagination,
    setNotification,
    setPayrollGroups,
    setRegularPayRun,
    setSpecialPayRun,
    setFinalPayRun,
    setPayrollID,
    setUploadStatus,
    setUploadErrors,
    setErrorsCsv,
    setPayrollGroupPeriods,
    setPayrollValidationErrors
} from './actions';

import {
    NOTIFICATION,
    RUN_PAYROLL,
    UPLOAD_ATTENDANCE,
    GET_PAYROLL_GROUPS,
    GET_REGULAR_PAY_RUN,
    GET_SPECIAL_PAY_RUN,
    GET_FINAL_PAY_RUN,
    GET_PAYROLL_GROUP_PERIODS,
    CREATE_REGULAR_PAYROLL,
    START_OF_MONTH,
    END_OF_MONTH,
    GET_PAYROLL_VALIDATION,
    FORCE_FETCH_ATTENDANCE_STATUS
} from './constants';

/**
 * Get Payroll Groups
 */
export function* getPayrollGroups() {
    try {
        yield put( setLoading( true ) );

        const companyId = company.getLastActiveCompanyId();
        const { data } = yield call( Fetch, `/company/${companyId}/payroll_group_periods`, { method: 'GET' });

        yield put( setPayrollGroups( data ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Get Payroll Group Periods
 */
export function* getPayrollGroupPeriods({ payload }) {
    const { payrollGroupId } = payload;

    try {
        const { data } = yield call( Fetch, `/payroll_group/${payrollGroupId}/periods`, { method: 'GET' });

        yield put( setPayrollGroupPeriods( data.pay_date_periods ) );
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Upload Attendance
 */
export function* uploadAttendance({ payload }) {
    const { payrollId, file } = payload;

    try {
        yield put( setLoading( true ) );
        let jobID = null;

        const data = new FormData();
        data.append( 'payroll_id', payrollId );
        data.append( 'file', file );

        /**
         * upoload attendance
         */
        function* upload() {
            const response = yield call( Fetch, '/payroll/upload/attendance', {
                method: 'POST',
                data
            });

            if ( response ) {
                jobID = response.data.id;
                yield call( fetchJobStatus, response.data.id );
            } else {
                yield call( upload, data );
            }
        }

        /**
         * Get the calculation status
         */
        function* fetchJobStatus( jobId ) {
            const params = {
                payroll_id: payrollId,
                job_id: jobId,
                step: 'validation',
                include: 'errors'
            };

            const response = yield call( Fetch, '/payroll/upload/attendance/status', { method: 'GET', params });

            const {
                included,
                data: { attributes, relationships }
            } = response;

            const {
                status,
                payload: { name }
            } = attributes;

            const { errors } = relationships;

            let uploadStatus;
            switch ( status ) {
                case 'FINISHED':
                    if ( errors.data ) {
                        uploadStatus = 'validation_failed';
                    } else {
                        uploadStatus = 'validated';
                    }
                    break;
                case 'FAILED':
                    uploadStatus = 'validation_failed';
                    break;
                default:
                    if ( errors.data ) {
                        uploadStatus = 'validation_failed';
                    } else {
                        uploadStatus = 'validating';
                    }
            }

            yield put( setUploadStatus( uploadStatus ) );

            if ( status === 'PENDING' && !errors.data ) {
                yield call( fetchJobStatus, jobId );
            } else {
                const errorDescription = get( included, '[0].attributes.description' );
                const noActiveEmployees = get( errorDescription, 'name' ) === 'task-errors'
                && new RegExp( 'No active employees found', 'i' ).test( get( errorDescription, 'errors[0]' ) );

                let errorsData;

                if ( errors.data === null ) {
                    errorsData = {};
                } else if ( noActiveEmployees ) {
                    errorsData = {};
                    yield call( notifyUser, {
                        show: true,
                        title: 'Error',
                        message: 'There are no active employees found on the selected payroll cut-off period.',
                        type: 'error'
                    });
                } else if ( name === 'validate-attendance-service-data' ) {
                    errorsData = processGetAttendanceErrors( included );
                } else {
                    errorsData = processValidationErrors( included[ 0 ].attributes.description.errors );
                }

                yield put( setUploadErrors( errorsData ) );
            }
        }

        if ( !jobID ) yield call( upload, data );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Create Regular Payroll
 */
export function* createPayroll({ payload }) {
    const { payrollId, start_date, end_date, attendance_start_date, attendance_end_date, search_term, annualize } = payload;

    try {
        yield put( setLoading( true ) );

        const data = {
            payroll_group_id: payrollId,
            start_date,
            end_date,
            attendance_start_date,
            attendance_end_date,
            search_term,
            annualize
        };
        let jobID = null;

        /**
         * Generate Payroll
         */
        function* generate() {
            const response = yield call( Fetch, '/payroll/regular_payroll_job', {
                method: 'POST',
                data
            });

            if ( response ) {
                jobID = response.jobId;
                yield call( fetchJobStatus, response.jobId );
            } else {
                yield call( generate, data );
            }
        }

        /**
         * Get the calculation status
         */
        function* fetchJobStatus( jobId ) {
            const response = yield call( Fetch, `/payroll/regular_payroll_job/${jobId}`, { method: 'GET' });

            if ( response.result.status === 'CREATED' ) {
                yield call( fetchAttendanceStatus, {
                    payrollId: response.result.payrollId,
                    jobId: response.result.attendanceJobId
                });
            } else if ( response.result.status === 'FAILED' ) {
                yield call( notifyUser, {
                    show: true,
                    title: 'Error',
                    message: response.result.error,
                    type: 'error'
                });
            } else {
                yield call( delay, 5000 );
                yield call( fetchJobStatus, jobId );
            }

            return true;
        }

        /**
         *  Fetch Attendance Status
         */
        function* fetchAttendanceStatus( payloadData ) {
            const params = {
                payroll_id: payloadData.payrollId,
                job_id: payloadData.jobId,
                include: 'errors'
            };

            const response = yield call( Fetch, '/payroll/upload/attendance/status', { method: 'GET', params });

            const {
                included,
                data: { attributes }
            } = response;

            const { status } = attributes;

            switch ( status ) {
                case 'FINISHED':
                    localStorage.setItem( 'draft_payroll_jobs', JSON.stringify({
                        type: 'regular', id: payloadData.payrollId
                    }) );
                    yield put( setPayrollID( payloadData.payrollId ) );
                    yield call( getRegularPayRun, {
                        payload: {
                            page: 1,
                            per_page: 10,
                            payrollId: payloadData.payrollId,
                            search_term: '',
                            get_validation: true
                        }
                    });
                    break;

                case 'FAILED': {
                    const errorDescription = get( included, '[0].attributes.description' );
                    const noActiveEmployees = get( errorDescription, 'name' ) === 'task-errors'
                        && new RegExp( 'No active employees found', 'i' ).test( get( errorDescription, 'errors[0]' ) );

                    if ( noActiveEmployees ) {
                        yield call( notifyUser, {
                            show: true,
                            title: 'Error',
                            message: 'There are no active employees found on the selected payroll cut-off period.',
                            type: 'error'
                        });
                    } else {
                        yield call( notifyUser, {
                            show: true,
                            title: 'Error',
                            message: errorDescription.errors[ 0 ],
                            type: 'error'
                        });
                    }
                }
                    break;

                case 'PENDING':
                    yield call( delay, 5000 );
                    yield call( fetchAttendanceStatus, payloadData );

                    break;
                default:
            }

            return true;
        }

        if ( !jobID ) yield call( generate, data );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 *  Fetch Attendance Status
 */
export function* forceFetchAttendanceStatus({ payload }) {
    const { payrollId, jobId } = payload;

    /**
     *  Fetch Attendance Status
    */
    function* fetchAttendanceStatus( payloadData ) {
        const params = {
            payroll_id: payloadData.payrollId,
            job_id: payloadData.jobId,
            include: 'errors'
        };

        const response = yield call( Fetch, '/payroll/upload/attendance/status', { method: 'GET', params });

        const {
            included,
            data: { attributes }
        } = response;

        const { status } = attributes;

        switch ( status ) {
            case 'FINISHED':
                localStorage.setItem( 'draft_payroll_jobs', JSON.stringify({
                    type: 'regular', id: payloadData.payrollId
                }) );
                yield put( setPayrollID( payloadData.payrollId ) );
                yield call( getRegularPayRun, {
                    payload: {
                        page: 1,
                        per_page: 10,
                        payrollId: payloadData.payrollId,
                        search_term: '',
                        get_validation: true
                    }
                });
                break;

            case 'FAILED': {
                const errorDescription = get( included, '[0].attributes.description' );
                const noActiveEmployees = get( errorDescription, 'name' ) === 'task-errors'
                    && new RegExp( 'No active employees found', 'i' ).test( get( errorDescription, 'errors[0]' ) );

                if ( noActiveEmployees ) {
                    yield call( notifyUser, {
                        show: true,
                        title: 'Error',
                        message: 'There are no active employees found on the selected payroll cut-off period.',
                        type: 'error'
                    });
                } else {
                    yield call( notifyUser, {
                        show: true,
                        title: 'Error',
                        message: errorDescription.errors[ 0 ],
                        type: 'error'
                    });
                }
            }
                break;

            case 'PENDING':
                yield call( delay, 5000 );
                yield call( fetchAttendanceStatus, payloadData );

                break;
            default:
        }

        return true;
    }

    yield call( fetchAttendanceStatus, {
        payrollId,
        jobId
    });
}

/**
 * Get Regular Pay Run
 */
export function* getRegularPayRun({ payload }) {
    const { search_term, payrollId, page, per_page, get_validation = false } = payload;

    try {
        yield put( setTableLoading( true ) );
        yield put( setPayrollID( parseFloat( payrollId ) ) );

        const params = {
            with_employees: true,
            search_term,
            page,
            per_page
        };

        const response = yield call( Fetch, `/payroll/${payrollId}`, {
            method: 'GET',
            params
        });

        if ( get_validation ) {
            yield call( getPayrollValidation, {
                payload: {
                    start_date: response.start_date,
                    end_date: response.end_date,
                    payroll_id: payrollId
                }
            });
        }

        yield put( setPagination( response.pagination ) );
        yield put( setRegularPayRun( response ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setTableLoading( false ) );
    }
}

/**
 * Get Special Pay Run
 */
export function* getSpecialPayRun({ payload }) {
    const { page, per_page, search_term } = payload;

    try {
        yield put( setTableLoading( true ) );

        const companyId = company.getLastActiveCompanyId();
        const params = {
            release_date_from: START_OF_MONTH,
            release_date_to: END_OF_MONTH,
            page,
            per_page,
            keyword: search_term
        };

        const response = yield call( Fetch, `/company/${companyId}/payroll/special/available_items`,
            {
                method: 'GET',
                params
            });

        const { data, pagination } = response;

        if ( search_term === '' && data.length === 0 ) {
            localStorage.removeItem( 'draft_payroll_jobs' );

            yield call( notifyUser, {
                show: true,
                title: 'Special Pay Run',
                message: 'No Data Found',
                type: 'warning'
            });
        } else {
            localStorage.setItem( 'draft_payroll_jobs', JSON.stringify({
                type: 'special', id: null
            }) );
        }

        yield put( setPagination( pagination ) );
        yield put( setSpecialPayRun( data ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
        yield put( setTableLoading( false ) );
    }
}

/**
 * Get Final Pay Run
 */
export function* getFinalPayRun({ payload }) {
    const { page, per_page, search_term } = payload;

    try {
        yield put( setTableLoading( true ) );

        const companyId = company.getLastActiveCompanyId();
        const params = {
            release_date_from: moment().subtract( 6, 'months' ).startOf( 'month' ).format( DATE_FORMATS.API ),
            release_date_to: moment().add( 1, 'months' ).endOf( 'month' ).format( DATE_FORMATS.API ),
            page,
            per_page,
            keyword: search_term
        };

        const response = yield call( Fetch, `/company/${companyId}/employees/final_pay`,
            {
                method: 'GET',
                params
            });

        const { data, pagination } = response;

        if ( search_term === '' && data.length === 0 ) {
            localStorage.removeItem( 'draft_payroll_jobs' );

            yield call( notifyUser, {
                show: true,
                title: 'Final Pay Run',
                message: 'No Data Found',
                type: 'warning'
            });
        } else {
            localStorage.setItem( 'draft_payroll_jobs', JSON.stringify({
                type: 'final', id: null
            }) );
        }

        yield put( setPagination( pagination ) );
        yield put( setFinalPayRun( data ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
        yield put( setTableLoading( false ) );
    }
}

/**
 * Get Payroll Employees Validation
*/
export function* getPayrollValidation({ payload }) {
    const companyId = company.getLastActiveCompanyId();
    const { start_date, end_date, employeeIds = [], payroll_id = null } = payload;
    let hasError = false;

    try {
        const data = new FormData();

        data.append( 'start_date', start_date );
        data.append( 'end_date', end_date );
        data.append( 'id[]', employeeIds );
        data.append( 'company_id', companyId );

        if ( payroll_id ) {
            data.append( 'payroll_id', payroll_id );
        }

        let jobID = null;

        /**
         * Validate Payroll
         */
        function* validate() {
            const response = yield call( Fetch, '/employee/employees_for_payroll_validation', {
                method: 'POST',
                data
            });

            if ( response ) {
                jobID = response.jobId;
                yield call( fetchJobStatus, response.jobId );
            }

            return true;
        }

        /**
         * fetch job status
         */
        function* fetchJobStatus( jobId ) {
            const response = yield call( Fetch, `/employee/employees_for_payroll_validation/${jobId}`, { method: 'GET' });

            if ( response.result.status === 'validation_error' ) {
                hasError = true;
                yield put( setErrorsCsv( response.result.download_csv ) );
                yield put( setPayrollValidationErrors( JSON.parse( response.result.errors ) ) );
            } else if ( response.result.status === 'validating' ) {
                yield call( delay, 5000 );
                yield call( fetchJobStatus, jobId );
            }

            return true;
        }

        if ( !jobID ) yield call( validate, data );
        return hasError;
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Run Payroll
 */
export function* runPayroll({ payload }) {
    const { type, employee_ids, regularPayrollId = null } = payload;

    try {
        yield put( setTableLoading( true ) );

        const data = {
            company_id: company.getLastActiveCompanyId(),
            account_id: auth.getUser().account_id
        };

        /**
         * fetch job status
         */
        function* fetchJobStatus( jobId ) {
            const response = yield call( Fetch, `/payroll/special_payroll_job/${jobId}`, { method: 'GET' });

            if ( response.result.status === 'CREATED' ) {
                localStorage.setItem( 'pending_payroll', JSON.stringify([response.result.payrollId]) );

                yield call( calculate, response.result.payrollId );
                yield call( browserHistory.push, '/payroll' );
            } else if ( response.result.status === 'FAILED' ) {
                const errorPayload = {
                    show: true,
                    title: 'Error',
                    message: response.result.error,
                    type: 'error'
                };

                yield call( notifyUser, errorPayload );
            } else {
                yield call( delay, 5000 );
                yield call( fetchJobStatus, jobId );
            }

            return true;
        }

        /**
         * calculate special and final payroll
         */
        function* calculate( payrollID ) {
            const response = yield call( Fetch, `/payroll/${payrollID}/calculate`, { method: 'POST' });

            if ( response ) {
                yield call( browserHistory.push, '/payroll' );
            }

            return true;
        }

        switch ( type ) {
            case 'special': {
                Object.assign( data, {
                    included_items: employee_ids
                });

                const hasError = yield call( getPayrollValidation, {
                    payload: {
                        start_date: START_OF_MONTH,
                        end_date: END_OF_MONTH,
                        employeeIds: employee_ids.map( ( item ) => item.employee_id )
                    }
                });

                if ( !hasError ) {
                    const response = yield call( Fetch, '/payroll/special_payroll_job', {
                        method: 'POST',
                        data
                    });

                    yield call( fetchJobStatus, response.jobId );
                }
            }
                break;

            case 'final': {
                Object.assign( data, {
                    final_pay_ids: employee_ids
                });

                const hasError = yield call( getPayrollValidation, {
                    payload: {
                        start_date: START_OF_MONTH,
                        end_date: END_OF_MONTH,
                        employeeIds: employee_ids
                    }
                });

                if ( !hasError ) {
                    const response = yield call( Fetch, '/payroll/final', {
                        method: 'POST',
                        data: { data }
                    });

                    localStorage.setItem( 'pending_payroll', JSON.stringify([response.id]) );
                    yield call( calculate, response.id );
                }
            }
                break;

            default:
                yield call( Fetch, `/payroll/${regularPayrollId}/calculate`, { method: 'POST' });

                localStorage.setItem( 'pending_payroll', JSON.stringify([regularPayrollId]) );
                yield call( browserHistory.push, '/payroll' );
                break;
        }
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setTableLoading( false ) );
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getPayrollGroups );
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * watch for UPLOAD_ATTENDANCE
 */
export function* watchUploadAttendance() {
    const watcher = yield takeLatest( UPLOAD_ATTENDANCE, uploadAttendance );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for RUN_PAYROLL
 */
export function* watchRunPayroll() {
    const watcher = yield takeLatest( RUN_PAYROLL, runPayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_PAYROLL_GROUPS
 */
export function* watchForGetPayrollGroups() {
    const watcher = yield takeLatest( GET_PAYROLL_GROUPS, getPayrollGroups );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_PAYROLL_GROUP_PERIODS
 */
export function* watchForGetPayrollGroupPeriods() {
    const watcher = yield takeLatest( GET_PAYROLL_GROUP_PERIODS, getPayrollGroupPeriods );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for CREATE_REGULAR_PAYROLL
 */
export function* watchForGetCreateRegularPayRun() {
    const watcher = yield takeLatest( CREATE_REGULAR_PAYROLL, createPayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for FETCH_ATTENDANCE_STATUS
 */
export function* watchForForceFetchAttendanceStatus() {
    const watcher = yield takeLatest( FORCE_FETCH_ATTENDANCE_STATUS, forceFetchAttendanceStatus );
    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_REGULAR_PAY_RUN
 */
export function* watchForGetRegularPayRun() {
    const watcher = yield takeLatest( GET_REGULAR_PAY_RUN, getRegularPayRun );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_SPECIAL_PAY_RUN
 */
export function* watchForGetSpecialPayRun() {
    const watcher = yield takeLatest( GET_SPECIAL_PAY_RUN, getSpecialPayRun );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_FINAL_PAY_RUN
 */
export function* watchForGetFinalPayRun() {
    const watcher = yield takeLatest( GET_FINAL_PAY_RUN, getFinalPayRun );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_PAYROLL_VALIDATION
 */
export function* watchForGetPayrollValidation() {
    const watcher = yield takeLatest( GET_PAYROLL_VALIDATION, getPayrollValidation );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchRunPayroll,
    watchForNotifyUser,
    watchUploadAttendance,
    watchForGetFinalPayRun,
    watchForGetPayrollGroups,
    watchForGetRegularPayRun,
    watchForForceFetchAttendanceStatus,
    watchForGetSpecialPayRun,
    watchForReinitializePage,
    watchForGetPayrollValidation,
    watchForGetPayrollGroupPeriods,
    watchForGetCreateRegularPayRun
];
