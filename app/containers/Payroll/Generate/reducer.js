import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_TABLE_LOADING,
    SET_PAGINATION,
    SET_NOTIFICATION,
    SET_PAYROLL_GROUPS,
    SET_REGULAR_PAY_RUN,
    SET_SPECIAL_PAY_RUN,
    SET_FINAL_PAY_RUN,
    SET_PAYROLL_GROUP_PERIODS,
    SET_PAYROLL_ID,
    SET_UPLOAD_STATUS,
    SET_UPLOAD_ERROR,
    SET_PAYROLL_VALIDATION_ERRORS,
    SET_ERRORS_CSV,
    RESET_STORE
} from './constants';

const initialState = fromJS({
    errors: {},
    UploadJobId: null,
    UploadStatus: '',
    UploadErrors: {},
    validationErrors: [],
    errorsCsv: '',
    saving: {
        status: '',
        errors: {}
    },
    loading: false,
    tableLoading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    pagination: {
        from: 1,
        to: 1,
        total: 1,
        current_page: 0,
        last_page: 1,
        per_page: 10
    },
    payrollID: null,
    payrollGroupList: [],
    payrollGroupPeriods: {},
    regularPayRun: {},
    specialPayRun: [],
    finalPayRun: []
});

/**
 * modifies passed user data to match client structure
 * @param userList = list of users returned by API
 * @returns {array} = formatted list of users
 */
function preparePayrollGroupData( payload ) {
    let formattedData = [];
    if ( payload.length > 0 ) {
        formattedData = payload.map( ( payrollGroup ) => {
            const { id, name, pay_date_periods } = payrollGroup;
            return {
                id,
                name,
                pay_date_periods
            };
        });
    }
    return formattedData;
}

/**
 *
 * Generate reducer
 *
 */
function generateReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_TABLE_LOADING:
            return state.set( 'tableLoading', action.payload );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_PAYROLL_VALIDATION_ERRORS:
            return state.set( 'validationErrors', fromJS( action.payload ) );
        case SET_ERRORS_CSV:
            return state.set( 'errorsCsv', action.payload );
        case SET_PAYROLL_GROUPS:
            return state.set( 'payrollGroupList', fromJS( preparePayrollGroupData( action.payload ) ) );
        case SET_PAYROLL_GROUP_PERIODS:
            return state.set( 'payrollGroupPeriods', fromJS( action.payload ) );
        case SET_PAYROLL_ID:
            return state.set( 'payrollID', action.payload );
        case SET_REGULAR_PAY_RUN:
            return state.set( 'regularPayRun', fromJS( action.payload ) );
        case SET_SPECIAL_PAY_RUN:
            return state.set( 'specialPayRun', fromJS( action.payload ) );
        case SET_FINAL_PAY_RUN:
            return state.set( 'finalPayRun', fromJS( action.payload ) );
        case SET_UPLOAD_STATUS:
            return state.set( 'UploadStatus', fromJS( action.payload ) );
        case SET_UPLOAD_ERROR:
            return state.set( 'UploadErrors', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default generateReducer;
