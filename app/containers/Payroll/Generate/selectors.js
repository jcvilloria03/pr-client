import { createSelector } from 'reselect';

/**
 * Direct selector to the generate state domain
 */
const selectGenerateDomain = () => ( state ) => state.get( 'generate' );

/**
 * Default selector used by Generate
 */

const makeSelectLoading = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectTableLoading = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'tableLoading' )
);

const makeSelectNotification = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectPagination = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectPayrollGroupList = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'payrollGroupList' ).toJS()
);

const makeSelectPayrollGroupPeriods = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'payrollGroupPeriods' ).toJS()
);

const makeSelectPayrollId = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'payrollID' )
);

const makeSelectUploadStatus = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'UploadStatus' )
);

const makeSelectUploadErrors = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'UploadErrors' ).toJS()
);

const makeSelectRegularPayRun = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'regularPayRun' ).toJS()
);

const makeSelectSpecialPayRun = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'specialPayRun' ).toJS()
);

const makeSelectFinalPayRun = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'finalPayRun' ).toJS()
);

const makeSelectValidationErrors = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'validationErrors' ).toJS()
);

const makeSelectErrorsCsv = () => createSelector(
    selectGenerateDomain(),
  ( substate ) => substate.get( 'errorsCsv' )
);

export {
  makeSelectLoading,
  makeSelectPayrollId,
  makeSelectPagination,
  makeSelectValidationErrors,
  makeSelectTableLoading,
  makeSelectNotification,
  makeSelectPayrollGroupList,
  makeSelectPayrollGroupPeriods,
  makeSelectRegularPayRun,
  makeSelectSpecialPayRun,
  makeSelectFinalPayRun,
  makeSelectUploadStatus,
  makeSelectUploadErrors,
  makeSelectErrorsCsv
};
