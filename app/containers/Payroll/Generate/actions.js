import {
    INITIALIZE,
    RESET_STORE,
    RUN_PAYROLL,
    UPLOAD_ATTENDANCE,
    SET_UPLOAD_STATUS,
    SET_UPLOAD_ERROR,
    CREATE_REGULAR_PAYROLL,
    GET_PAYROLL_GROUPS,
    GET_REGULAR_PAY_RUN,
    GET_PAYROLL_GROUP_PERIODS,
    GET_SPECIAL_PAY_RUN,
    GET_FINAL_PAY_RUN,
    GET_PAYROLL_VALIDATION,
    SET_LOADING,
    SET_TABLE_LOADING,
    SET_NOTIFICATION,
    SET_PAYROLL_GROUPS,
    SET_PAYROLL_GROUP_PERIODS,
    SET_REGULAR_PAY_RUN,
    SET_SPECIAL_PAY_RUN,
    SET_FINAL_PAY_RUN,
    SET_PAGINATION,
    SET_PAYROLL_ID,
    SET_PAYROLL_VALIDATION_ERRORS,
    SET_ERRORS_CSV,
    FORCE_FETCH_ATTENDANCE_STATUS
} from './constants';

/**
 * initialize page
 */
export function initializePage() {
    return {
        type: INITIALIZE
    };
}

/**
 * run payroll
 */
export function runPayroll( payload ) {
    return {
        type: RUN_PAYROLL,
        payload
    };
}

/**
 * create regular payroll
 */
export function createRegularPayroll( payload ) {
    return {
        type: CREATE_REGULAR_PAYROLL,
        payload
    };
}

/**
 * Fetch attendance status
 */
export function forceFetchAttendanceStatus( payload ) {
    return {
        type: FORCE_FETCH_ATTENDANCE_STATUS,
        payload
    };
}

/**
 * upload attendance
 */
export function uploadAttendance( payload ) {
    return {
        type: UPLOAD_ATTENDANCE,
        payload
    };
}

/**
 * updates the status of uploads
 */
export function setUploadStatus( payload ) {
    return {
        type: SET_UPLOAD_STATUS,
        payload
    };
}

/**
 * Sets upload errors
 */
export function setUploadErrors( payload ) {
    return {
        type: SET_UPLOAD_ERROR,
        payload
    };
}

/**
 * Sets errors csv
 */
export function setErrorsCsv( payload ) {
    return {
        type: SET_ERRORS_CSV,
        payload
    };
}

/**
 * Validates payroll employees
 */
export function getPayrollValidation( payload ) {
    return {
        type: GET_PAYROLL_VALIDATION,
        payload
    };
}

/**
 * Gets the existing payroll groups with payroll periods given company id
 */
export function getPayrollGroups() {
    return {
        type: GET_PAYROLL_GROUPS
    };
}

/**
 * Gets the  payroll periods given payroll group id
 */
export function getPayrollGroupPeriods( payload ) {
    return {
        type: GET_PAYROLL_GROUP_PERIODS,
        payload
    };
}

/**
 * Gets Regular Pay Run
 */
export function getRegularPayRun( payload ) {
    return {
        type: GET_REGULAR_PAY_RUN,
        payload
    };
}

/**
 * Gets Special Pay Run
 */
export function getSpecialPayRun( payload ) {
    return {
        type: GET_SPECIAL_PAY_RUN,
        payload
    };
}

/**
 * Gets Final Pay Run
 */
export function getFinalPayRun( payload ) {
    return {
        type: GET_FINAL_PAY_RUN,
        payload
    };
}

/**
 * Set table pagination
 */
export function setPagination( payload ) {
    return {
        type: SET_PAGINATION,
        payload
    };
}

/**
 * Set Regular Pay Run
 */
export function setRegularPayRun( payload ) {
    return {
        type: SET_REGULAR_PAY_RUN,
        payload
    };
}

/**
 * Sets the Special Pay Run
 * @param {object} data
 */
export function setSpecialPayRun( payload ) {
    return {
        type: SET_SPECIAL_PAY_RUN,
        payload
    };
}

/**
 * Sets the Final Pay Run
 * @param {object} data
 */
export function setFinalPayRun( payload ) {
    return {
        type: SET_FINAL_PAY_RUN,
        payload
    };
}

/**
 * set payroll ID
 */
export function setPayrollID( payload ) {
    return {
        type: SET_PAYROLL_ID,
        payload
    };
}

/**
 * Sets the existing payroll groups with payroll periods given company id
 */
export function setPayrollGroups( payload ) {
    return {
        type: SET_PAYROLL_GROUPS,
        payload
    };
}

/**
 * Sets the  payroll periods given payroll group id
 */
export function setPayrollGroupPeriods( payload ) {
    return {
        type: SET_PAYROLL_GROUP_PERIODS,
        payload
    };
}

/**
 * Sets the payroll validation
 */
export function setPayrollValidationErrors( payload ) {
    return {
        type: SET_PAYROLL_VALIDATION_ERRORS,
        payload
    };
}

/**
 * Sets loading status\
 * @param {Boolean} payload - Loading status
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets Table loading status
 * @param {Boolean} payload - Loading status
 */
export function setTableLoading( payload ) {
    return {
        type: SET_TABLE_LOADING,
        payload
    };
}

/**
 * Sets notification
 * @param {Object} payload - Notification config
 * @returns {Object}
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}

