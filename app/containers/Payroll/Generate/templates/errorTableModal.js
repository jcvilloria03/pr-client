/* eslint-disable array-callback-return */
/* eslint-disable react/sort-comp */
import React, { PureComponent } from 'react';

import A from 'components/A';
import Table from 'components/Table';
import FooterTablePagination from 'components/FooterTablePagination';

import { formatPaginationLabel } from 'utils/functions';

import { PROFILE_PICTURE } from '../constants';

import {
    ModalBodyWrapper,
    EmployeeName,
    EmployeePhoto
} from '../styles';

import { DownloadTemplateLink } from '../UploadAttendance/styles';
/**
 * Error Table Modal
 */
class ErrorTableModal extends PureComponent {
    static propTypes = {
        data: React.PropTypes.array,
        csv: React.PropTypes.string
    }

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0 to 0 of 0 entries',
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            }
        };

        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
    }

    componentWillMount() {
        this.setState({
            pagination: {
                total: this.props.data.length,
                per_page: 10,
                current_page: 1,
                last_page: Math.ceil( this.props.data.length / 10 ),
                to: 0
            }
        });
    }

    handleTableChanges = ( tableProps = this.errorsTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.data.length });

        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.errorsTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.errorsTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    renderErrorsSection = () => {
        let errorDisplay = [];
        const errorList = this.props.data;

        const columns = [
            {
                header: 'Employee Name',
                accessor: 'row',
                minWidth: 200,
                sortable: false
            },
            {
                header: 'Error',
                accessor: 'error',
                minWidth: 450,
                sortable: false
            }
        ];

        errorDisplay = errorList.map( ( item ) => ({
            row:
                <EmployeeName>
                    <EmployeePhoto src={ item.employee.photo || PROFILE_PICTURE } />
                    <A href={ `/employee/${item.employee.id}` } target="_blank" className="link" >
                        { item.employee.full_name }
                    </A>
                </EmployeeName>,
            error: (
                <ul>
                    { Object.values( item.errors )
                            .map( ( value ) => (
                                <li key={ value } style={ { margin: '0' } } >{value}</li>
                            ) )
                        }
                </ul>
                    )
        }) );

        return (
            <div className="errors">
                <Table
                    columns={ columns }
                    data={ errorDisplay }
                    ref={ ( ref ) => { this.errorsTable = ref; } }
                    onDataChange={ this.handleTableChanges }
                    page={ this.state.pagination.current_page - 1 }
                    pageSize={ this.state.pagination.per_page }
                    pages={ this.state.pagination.total }
                    external
                />
            </div>
        );
    }

    render() {
        const { csv } = this.props;
        const { label, pagination } = this.state;

        return (
            <div>
                <ModalBodyWrapper>
                    <div className="header">
                        <p>Download the employee issues</p>
                        <DownloadTemplateLink
                            href={ csv }
                            download
                        >
                            Download
                        </DownloadTemplateLink>
                    </div>
                    { this.renderErrorsSection() }

                </ModalBodyWrapper>
                <FooterTablePagination
                    fluid
                    bgColor="#fff"
                    page={ pagination.current_page }
                    pageSize={ pagination.per_page }
                    pagination={ pagination }
                    onPageChange={ this.onPageChange }
                    onPageSizeChange={ this.onPageSizeChange }
                    paginationLabel={ label }
                />
            </div>
        );
    }
}

export default ErrorTableModal;
