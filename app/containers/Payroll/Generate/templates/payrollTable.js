/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable react/sort-comp */
/* eslint-disable no-confusing-arrow */
/* eslint-disable camelcase */
import React from 'react';
import Container from 'reactstrap/lib/Container';
import moment from 'moment';

import Icon from 'components/Icon';
import Table from 'components/Table';
import Input from 'components/Input';
import Button from 'components/Button';
import SalDropdown from 'components/SalDropdown';
import FooterTablePagination from 'components/FooterTablePagination';

import {
    isAnyRowSelected,
    getIdsOfSelectedRows
} from 'components/Table/helpers';

import { DATE_FORMATS } from 'utils/constants';
import { browserHistory } from 'utils/BrowserHistory';
import { formatDate, formatPaginationLabel } from 'utils/functions';

import { PayrollTableWrapper } from '../styles';
import {
    REGULAR_PAY_RUN_COLUMNS,
    SPECIAL_PAY_RUN_COLUMNS,
    FINAL_PAY_RUN_COLUMNS,
    DROPDOWN_OPTIONS
} from '../constants';

/**
 * Payroll Table
 */
class PayrollTable extends React.PureComponent {
    static propTypes = {
        selectedRegularPayroll: React.PropTypes.object,
        handleStepperChange: React.PropTypes.func,
        validationErrors: React.PropTypes.array,
        getRegularPayRun: React.PropTypes.func,
        getSpecialPayRun: React.PropTypes.func,
        getFinalPayRun: React.PropTypes.func,
        generateType: React.PropTypes.string,
        toggleModal: React.PropTypes.func,
        payrollId: React.PropTypes.number,
        tableData: React.PropTypes.array,
        runPayroll: React.PropTypes.func,
        loading: React.PropTypes.bool,
        pagination: React.PropTypes.shape({
            page: React.PropTypes.number,
            total: React.PropTypes.number,
            page_size: React.PropTypes.number
        })
    }

    static defaultProps = {
        payrollAttendance: []
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            delete_label: '',
            sort_by: { date: 'ASC', employee_name: 'ASC' },
            dropdownItems: DROPDOWN_OPTIONS.map( ( item ) => ({
                label: item.label,
                children: <div><Icon name={ item.icon } className="icon" />{item.label}</div>,
                onClick: () => browserHistory.push( item.link )
            }) ),
            saving: false,
            hasSelection: false
        };

        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.handleSearch = this.handleSearch.bind( this );
    }

    componentWillReceiveProps( nextProps ) {
        const { tableData } = nextProps;

        if ( tableData ) {
            this.setState({
                pagination: nextProps.pagination,
                label: formatPaginationLabel({
                    page: nextProps.pagination.page - 1,
                    pageSize: nextProps.pagination.page_size,
                    dataLength: nextProps.pagination.total
                })
            });
        }
    }

    handlePagination( page, pageSize, searchTerm = '' ) {
        const { generateType, payrollId } = this.props;

        switch ( generateType ) {
            case 'special':
                this.props.getSpecialPayRun({
                    page,
                    per_page: pageSize,
                    search_term: searchTerm
                });
                break;

            case 'final':
                this.props.getFinalPayRun({
                    page,
                    per_page: pageSize,
                    search_term: searchTerm
                });
                break;

            default:
                this.props.getRegularPayRun({
                    payrollId,
                    page,
                    per_page: pageSize,
                    search_term: searchTerm
                });
                break;
        }
    }

    handleTableChanges = ( tableProps = this.generateTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });

        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    handleSearch = () => {
        const searchQuery = this.searchInput.state.value.toLowerCase();

        this.handlePagination( 1, 10, searchQuery );
    };

    handlePayRun = () => {
        const { generateType, payrollId, tableData } = this.props;
        const selected = getIdsOfSelectedRows( this.generateTable );

        switch ( generateType ) {
            case 'special': {
                const selectedEmployees = selected.map( ( id ) => {
                    const employeeId = parseFloat( id.split( '_' )[ 0 ]);
                    const releaseId = parseFloat( id.split( '_' )[ 1 ]);

                    const employee = tableData.find( ( item ) => item.employee_id === employeeId && item.release_detail.id === releaseId );

                    return {
                        employee_id: employee.employee_id,
                        employee_uid: employee.employee_uid,
                        release_id: employee.release_detail.id
                    };
                });

                this.props.runPayroll({
                    type: 'special',
                    employee_ids: selectedEmployees
                });
            }
                break;

            case 'final':
                this.props.runPayroll({
                    type: 'final',
                    employee_ids: selected
                });
                break;

            default:
                this.props.runPayroll({
                    type: 'regular',
                    regularPayrollId: payrollId
                });
                break;
        }
    }

    render() {
        const {
          label
        } = this.state;

        const {
            loading,
            tableData,
            pagination,
            toggleModal,
            generateType,
            validationErrors,
            handleStepperChange,
            selectedRegularPayroll
        } = this.props;

        const tableColumns = generateType === 'special'
                ? SPECIAL_PAY_RUN_COLUMNS
                : generateType === 'final'
                    ? FINAL_PAY_RUN_COLUMNS
                    : REGULAR_PAY_RUN_COLUMNS;

        const runPayValidation = generateType !== 'regular'
            ? loading || !isAnyRowSelected( this.generateTable ) || validationErrors.length > 0
            : loading || validationErrors.length > 0;

        return (
            <PayrollTableWrapper>
                <Container>
                    <div className="content">
                        <div className="payroll-info">
                            <div className="info">
                                <p>Payroll: </p>
                                <p className="title">{generateType} Pay Run</p>
                            </div>
                            { generateType === 'regular'
                                ? <div>
                                    <hr></hr>
                                    <div className="info">
                                        <p>Payroll Group:</p>
                                        <p>{selectedRegularPayroll.payroll_group_name}</p>
                                    </div>
                                    <hr></hr>
                                    <div className="info">
                                        <p>Payday:</p>
                                        <p>{ moment( selectedRegularPayroll.payroll_date ).format( DATE_FORMATS.DISPLAY )}</p>
                                    </div>
                                    <hr></hr>
                                    <div className="info">
                                        <p>Payroll Period:</p>
                                        <p>{moment( selectedRegularPayroll.start_date ).format( DATE_FORMATS.DISPLAY )} to {moment( selectedRegularPayroll.end_date ).format( DATE_FORMATS.DISPLAY )}</p>
                                    </div>
                                    <hr></hr>
                                    <div className="info">
                                        <p>Attendance Period:</p>
                                        <p>{moment( selectedRegularPayroll.attendance_start_date ).format( DATE_FORMATS.DISPLAY )} to {moment( selectedRegularPayroll.attendance_end_date ).format( DATE_FORMATS.DISPLAY )}</p>
                                    </div>
                                </div>
                                : <div>
                                    <hr></hr>
                                    <div className="info">
                                        <p>Pay Run Date:</p>
                                        <p>{formatDate( moment(), DATE_FORMATS.DISPLAY )}</p>
                                    </div>
                                </div>
                            }
                        </div>
                        <div className="header">
                            <div className="search-wrapper">
                                <Input
                                    className="search"
                                    id="search"
                                    ref={ ( ref ) => { this.searchInput = ref; } }
                                    onKeyPress={ ( e ) => {
                                        e.charCode === 13 && this.handleSearch();
                                    } }
                                    placeholder="Search Payroll"
                                    addon={ {
                                        content: <Icon name="search" className="icon" />,
                                        placement: 'left'
                                    } }
                                />
                            </div>
                            <span className="dropdown-wrapper">
                                { generateType !== 'special' &&
                                <Button
                                    label={ <span> <Icon name="plusCircleOutline" className="icon-button" />Add Attendance</span> }
                                    type="primary"
                                    onClick={ () => handleStepperChange( 2 ) }
                                />}
                                { generateType !== 'final' &&
                                <SalDropdown
                                    new
                                    icon="plusCircleOutline"
                                    backgroundColor="#00A4E4"
                                    label="Add Other Income"
                                    dropdownItems={ this.state.dropdownItems }
                                /> }
                                <Button
                                    label="Run Payroll"
                                    type="action"
                                    disabled={ runPayValidation }
                                    onClick={ () => this.handlePayRun() }
                                />
                                {
                                    validationErrors.length > 0 && <div className="error-button-wrapper">
                                        <button
                                            className="error-button"
                                            disabled={ loading }
                                            onClick={ () => {
                                                toggleModal();
                                            } }
                                        >
                                            <Icon name="warning" className="icon" />
                                        </button>
                                        <span className="error-count">{validationErrors.length}</span>
                                    </div>
                                }

                            </span>
                        </div>
                        <Table
                            ref={ ( ref ) => { this.generateTable = ref; } }
                            className="generate-table"
                            data={ tableData }
                            columns={ tableColumns }
                            loading={ loading }
                            selectable={ generateType !== 'regular' }
                            noDataText="No Data Found"
                            onDataChange={ this.handleTableChanges }
                            onSelectionChange={ ({ selected }) => {
                                const selectionLength = selected.filter( ( row ) => row ).length;

                                this.setState({ hasSelection: selectionLength > 0 });
                            } }
                            pages={ pagination.total }
                            pageSize={ pagination.page_size }
                            external
                        />
                    </div>
                </Container>
                <div
                    className="loader"
                    style={ { display: 'block', position: 'fixed', bottom: '0', background: '#fff' } }
                >
                    <FooterTablePagination
                        page={ pagination.page }
                        pageSize={ pagination.page_size }
                        pagination={ pagination }
                        onPageChange={ ( data ) => this.handlePagination( data, pagination.page_size ) }
                        onPageSizeChange={ ( data ) => this.handlePagination( 1, data ) }
                        paginationLabel={ label }
                        sizeOptions={ [ 10, 20, 50, 100, 250, 500, 750, 1000 ] }
                    />
                </div>
            </PayrollTableWrapper>
        );
    }
}

export default PayrollTable;
