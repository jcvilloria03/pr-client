/* eslint-disable no-prototype-builtins */
/* eslint-disable react/sort-comp */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import moment from 'moment';

import Switch from 'components/Switch';
import Loader from 'components/Loader';
import Button from 'components/Button';
import Select from 'components/Select';
import ToolTip from 'components/ToolTip';
import { H3 } from 'components/Typography';

import { formatDate } from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';
import { browserHistory } from 'utils/BrowserHistory';

import { StyledGeneratePayroll } from '../styles';

/**
 * Generate Payroll
 */
class PayrollSelection extends React.PureComponent {
    static propTypes = {
        handleTypeSelection: React.PropTypes.func,
        payrollGroupPeriods: React.PropTypes.object,
        getPayrollGroupPeriods: React.PropTypes.func,
        tableLoading: React.PropTypes.bool,
        data: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                id: React.PropTypes.number,
                name: React.PropTypes.string,
                periods: React.PropTypes.arrayOf(
                    React.PropTypes.shape({
                        start: React.PropTypes.string,
                        cutoff: React.PropTypes.string
                    })
                ),
                pay_date_periods: React.PropTypes.object
            })
        )
    }
    /**
     * component constructor
     */
    constructor( props ) {
        super( props );
        this.state = {
            payroll_group: { label: '', value: '' },
            payroll_period: { label: '', value: '' },
            attendance_period: { label: '', value: '' },
            active_tab: 'regular',
            isLoading: false,
            groups: [],
            periods: [],
            data: [],
            loading: false,
            annualize: false
        };
    }

    componentWillMount() {
        this.prepareData();
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.payrollGroupPeriods ) {
            const periods = Object.keys( nextProps.payrollGroupPeriods ).map( ( date ) => ({
                value: date,
                label: formatDate( date, DATE_FORMATS.DISPLAY )
            }) );
            periods.sort( ( prevDate, nextDate ) => moment( nextDate.value, 'YYYY-MM-DD' ) - moment( prevDate.value, 'YYYY-MM-DD' ) ).reverse();

            this.setState({
                periods,
                isLoading: false
            });
        }
    }

    /**
     * prapares data for the component
     */
    prepareData = () => {
        const groups = this.props.data.map( ( group ) => ({
            label: group.name,
            value: group.id
        }) );

        this.setState({ data: this.props.data, groups });
    }

    /**
     * handles payroll group selection event
     */
    onPayrollGroupSelect = ( value ) => {
        if ( !value ) {
            this.setState({
                payroll_group: { label: '', value: '' },
                payroll_period: { label: '', value: '' }
            });
            this.payrollPeriodInput.setState({ value: null });

            return;
        }

        const groupId = this.payrollGroupInput.state.value.value;

        this.setState({
            payroll_group: this.payrollGroupInput.state.value,
            payroll_period: { label: '', value: '' },
            attendance_period: { label: '', value: '' },
            isLoading: true
        });

        this.props.getPayrollGroupPeriods({ payrollGroupId: groupId });
        this.payrollPeriodInput.setState({ value: null });
    }

    /**
     * handles changes on period select field
     */
    onPeriodSelect = ({ value }) => {
        const payroll = this.props.payrollGroupPeriods[ value ].payroll_period;
        const attendance = this.props.payrollGroupPeriods[ value ];
        const hasAttendanceData = attendance.hasOwnProperty( 'attendance_period' );

        this.setState({
            payroll_period: {
                label: `${moment( payroll.start ).format( DATE_FORMATS.DISPLAY )} to ${moment( payroll.cutoff ).format( DATE_FORMATS.DISPLAY )}`,
                value: payroll
            },
            attendance_period: hasAttendanceData
              ? {
                  label: `${moment( attendance.attendance_period.start ).format( DATE_FORMATS.DISPLAY )} to ${moment( attendance.attendance_period.cutoff ).format( DATE_FORMATS.DISPLAY )}`,
                  value: attendance.attendance_period
              } : {
                  label: `${moment( payroll.start ).format( DATE_FORMATS.DISPLAY )} to ${moment( payroll.cutoff ).format( DATE_FORMATS.DISPLAY )}`,
                  value: payroll
              }
        });
    }

    /**
     * component render method
     */
    render() {
        return (
            <StyledGeneratePayroll>
                <div className="header">
                    <H3>Select type of Payroll</H3>
                    <div className="instruction">
                        <p>
                            {'Generating payroll is easy; simply select the Payroll Group Name & Payroll Period then hit Generate. Note that you may only generate payroll for Payroll Group(s) you created. For payouts like 13th Month, Final Pay, bonuses, & other non-regular payouts, you\'re going to need to use Special Payroll instead.'}
                        </p>
                    </div>
                </div>
                <div className="content">
                    <div className="form">
                        <div className="selection-tabs">
                            <div
                                className={ this.state.active_tab === 'regular'
                                ? 'active tab'
                                : 'tab' }
                                onClick={ () => this.setState({ active_tab: 'regular' }) }
                            >Regular</div>
                            <div
                                className={
                                this.state.active_tab === 'special'
                                    ? 'active tab'
                                    : 'tab'
                                }
                                onClick={ () => this.setState({ active_tab: 'special' }) }
                            >Special Pay Run</div>
                            <div
                                className={
                                this.state.active_tab === 'final'
                                    ? 'active tab'
                                    : 'tab'
                                }
                                onClick={ () => this.setState({ active_tab: 'final' }) }
                            >Final Pay Run</div>
                        </div>
                        <div className="selection-form">
                            {this.state.active_tab === 'regular'
                            ? (
                                <div className="regular-selection">
                                    <div className="control-group">
                                        <div className="select-control">
                                            <Select
                                                ref={ ( ref ) => { this.payrollGroupInput = ref; } }
                                                id="payrollGroup"
                                                autosize
                                                data={ this.state.groups }
                                                placeholder="Select Payroll Group"
                                                onChange={ this.onPayrollGroupSelect }
                                            >
                                            </Select>
                                        </div>
                                    </div>
                                    <div className="control-group">
                                        <div className="select-control">
                                            <Select
                                                autosize
                                                id="payrollPeriod"
                                                placeholder="Select Pay Date"
                                                data={ this.state.periods }
                                                onChange={ this.onPeriodSelect }
                                                disabled={ !this.state.payroll_group.value && this.state.periods.length === 0 }
                                                isLoading={ this.state.isLoading }
                                                loadingPlaceholder="Fetching pay date information"
                                                ref={ ( ref ) => { this.payrollPeriodInput = ref; } }
                                            >
                                            </Select>
                                        </div>
                                    </div>
                                    <div className="info">
                                        <p>Payroll Period: {this.state.payroll_period.label !== ''
                                            ? this.state.payroll_period.label
                                            : ' N/A'
                                          }
                                        </p>
                                        <p>Attendance Period: { this.state.attendance_period.label !== ''
                                            ? this.state.attendance_period.label
                                            : ' N/A'
                                          }
                                        </p>
                                    </div>
                                    <div className="control-group annualize">
                                        <Switch
                                            id="switch-annualize"
                                            checked={ this.state.annualize }
                                            onChange={ ( value ) => this.setState({ annualize: value }) }
                                        />
                                        <span> Annualize Payroll?&nbsp;
                                            <ToolTip
                                                id="tooltipFiledLeaves"
                                                target={ <i className="fa fa-question-circle" /> }
                                                placement="top"
                                            >
                                                Enabling the annualize switch means the system will compute
                                                the withholding tax of the employee using annualize tax
                                                table based on the employee&apos;s year-to-date amounts.
                                            </ToolTip>
                                        </span>
                                    </div>
                                </div>
                            )
                            : this.state.active_tab === 'special'
                              ? (
                                  <div className="message">
                                      <p>{'Click Next to select employees\' other income which will be a part of the special pay run.'}</p>
                                  </div> )
                              : (
                                  <div className="message">
                                      <p>{'Click Next to select employees\' who will be included in final pay processing.'}</p>
                                  </div> )
                          }
                        </div>
                    </div>
                    {
                        this.props.tableLoading
                            ? <div className="table-loading"><Loader /></div>
                            : (
                                <div className="footer">
                                    <button
                                        className="btn cancel-btn"
                                        onClick={ () => browserHistory.push( '/payroll' ) }
                                    >
                                    Cancel
                                    </button>
                                    <Button
                                        label={ 'Next' }
                                        type="action"
                                        onClick={ () => this.props.handleTypeSelection( this.state ) }
                                    />
                                </div>
                            )
                    }
                </div>
            </StyledGeneratePayroll>
        );
    }
}

export default PayrollSelection;
