/* eslint-disable no-plusplus */
import React from 'react';
import { Container } from 'reactstrap';
import isEqual from 'lodash/isEqual';
import get from 'lodash/get';
import CloudUpload from '@iconify/icons-akar-icons/cloud-upload';
import DocumentText16Filled from '@iconify/icons-fluent/document-text-16-filled';

import { formatPaginationLabel } from 'utils/functions';

import A from 'components/A';
import Table from 'components/Table';
import { H3, H4, H5, P } from 'components/Typography';
import FooterTablePagination from 'components/FooterTablePagination';

import {
    UploadStep,
    UploadStepSubContainer,
    UploadStepHeading,
    FileErrorText,
    FileDetailsContainer,
    IconsContainer,
    CloudUploadIcon,
    DocumentText16FilledIcon,
    InputFile,
    DownloadTemplateLink,
    UploadButton,
    BackLink,
    UploadClickText
} from './styles';
import {
    StyledLoader,
    UploadPageWrapper,
    FormWrapper,
    NavWrapper,
    HeadingWrapper
} from '../styles';

import { ATTENDANCE_TEMPLATE_LINK } from '../constants';

/**
 * Annual Earnings Batch Upload Component
 */
class UploadAttendance extends React.Component {
    static propTypes = {
        handleStepperChange: React.PropTypes.func,
        uploadAttendance: React.PropTypes.func,
        payrollId: React.PropTypes.number,
        uploadStatus: React.PropTypes.string,
        uploadErrors: React.PropTypes.oneOfType([
            React.PropTypes.object,
            React.PropTypes.array
        ])
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            files: [],
            errors: {},
            submit: false,
            fileInvalid: false,
            label: 'Showing 0-0 of 0 entries',
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            }
        };

        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
        this.handleOnChangeFile = this.handleOnChangeFile.bind( this );
        this.handleOnClickUpload = this.handleOnClickUpload.bind( this );
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.uploadErrors, this.props.uploadErrors ) ) {
            this.setState({
                pagination: {
                    total: Object.keys( nextProps.uploadErrors ).length,
                    per_page: 10,
                    current_page: 1,
                    last_page: Math.ceil( Object.keys( nextProps.uploadErrors ).length / 10 ),
                    to: 0
                }
            }, () => this.handleTableChanges() );
        }
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.errorsTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.errorsTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    getFileName() {
        return get( this.state.files, 'name' ) || '';
    }

    handleTableChanges = ( tableProps = this.errorsTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: Object.keys( this.state.errors ).length });
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    processData( allText ) {
        const allTextLines = allText.split( /\r\n|\n/ );
        const headers = allTextLines[ 0 ].split( ',' );
        const IDList = [];
        const dateList = [];

      // Get CSV data
        for ( let i = 1; i < allTextLines.length; i++ ) {
            const data = allTextLines[ i ].split( ',' );
            if ( data.length === headers.length ) {
                for ( let j = 0; j < headers.length; j++ ) {
                    switch ( headers[ j ]) {
                        case 'Employee Id':
                            IDList.push( data[ j ]);
                            break;
                        case 'Timesheet Date':
                            dateList.push( data[ j ].replace(
                              /(\d\d)\/(\d\d)\/(\d{4})/,
                              '$3-$1-$2'
                            ) );
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        this.props.uploadAttendance({
            file: this.state.files,
            payrollId: this.props.payrollId
        });
    }

    handleOnChangeFile( event ) {
        const file = get( event.target, 'files.0' ) || null;
        const fileType = get( file, 'type' ) || '';

        if ( file && !fileType.includes( 'csv' ) ) {
            this.setState({ fileInvalid: true });

            return;
        }

        this.setState({
            files: file,
            fileInvalid: false,
            errors: {},
            status: null
        }, () => {
            this.validateButton && this.validateButton.setState({ disabled: !file });
        });
    }

    handleOnClickUpload() {
        const { files } = this.state;

        this.setState({
            errors: {},
            status: null,
            submit: true
        }, () => {
            this.validateButton && this.validateButton.setState({ disabled: true }, () => {
                const reader = new FileReader();
                reader.onload = () => {
                    this.processData( reader.result );
                };
                reader.readAsText( files );

                setTimeout( () => {
                    this.validateButton.setState({ disabled: false });
                    this.setState({ submit: false });
                }, 5000 );
            });
        });
    }

    renderErrorsSection = () => {
        const errorDisplay = [];
        const errorList = this.props.uploadErrors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'Row Number',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'Error Type',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            Object.keys( errorList ).forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', marginLeft: '1rem', textAlign: 'center' } }>Row {key}</H4>,
                    error: (
                        <ul>
                            { Object.values( errorList[ key ])
                                .map( ( value ) => (
                                    <li key={ value } style={ { margin: '0' } } >{value}</li>
                                ) )
                            }
                        </ul>
                    )
                });
            });

            return (
                <div className="errors">
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        ref={ ( ref ) => { this.errorsTable = ref; } }
                        onDataChange={ this.handleTableChanges }
                        page={ this.state.pagination.current_page - 1 }
                        pageSize={ this.state.pagination.per_page }
                        pages={ this.state.pagination.total }
                        external
                    />
                </div>
            );
        }

        return null;
    }

    render() {
        const {
            files,
            submit,
            fileInvalid
        } = this.state;

        const uploadStatusValidating = this.props.uploadStatus === 'validating';
        const uploadStatusValidated = this.props.uploadStatus === 'validated';

        const uploadButtonDisabled = submit || fileInvalid
            ? true
            : files
                ? files.length <= 0
                : true;

        return (
            <UploadPageWrapper>
                <NavWrapper>
                    <Container>
                        <BackLink
                            onClick={ () => {
                                this.props.handleStepperChange( 1 );
                            } }
                        >
                                &#8592; Back to Payroll Generate
                        </BackLink>
                    </Container>
                </NavWrapper>

                <HeadingWrapper>
                    <h3>Add Attendance</h3>

                    <p>To make it easier, you may upload employees&apos; attendance by batch. A template will be made available for you through this page.</p>
                </HeadingWrapper>

                <FormWrapper>
                    <Container className="sl-u-gap-bottom--xlg">
                        <div className="steps">
                            <div className="step">
                                <UploadStep>
                                    <UploadStepHeading>Step 01:</UploadStepHeading>

                                    <UploadStepSubContainer>
                                        <H3>Time and Attendance CSV Template</H3>

                                        <P>Click <A href="/payroll/guides/payroll/generate/time-and-attendance" target="_blank">Here</A> for the Time and Attendance upload guide.</P>
                                        <P>Download the CSV file template.</P>

                                        <DownloadTemplateLink
                                            href={ ATTENDANCE_TEMPLATE_LINK }
                                            download
                                        >
                                            Download
                                        </DownloadTemplateLink>
                                    </UploadStepSubContainer>
                                </UploadStep>
                            </div>

                            <div className="step">
                                <UploadStep>
                                    <InputFile
                                        type="file"
                                        accept=".csv"
                                        onChange={ ( event ) => this.handleOnChangeFile( event ) }
                                        innerRef={ ( ref ) => { this.fileInput = ref; } }
                                        hide={ uploadStatusValidating || submit }
                                    />

                                    <UploadStepHeading>Step 02:</UploadStepHeading>

                                    <UploadStepSubContainer>
                                        <H3>Upload and attach files</H3>

                                        <FileDetailsContainer>
                                            <IconsContainer>
                                                <CloudUploadIcon icon={ CloudUpload } />

                                                <DocumentText16FilledIcon icon={ DocumentText16Filled } />
                                            </IconsContainer>

                                            { fileInvalid
                                                ? (
                                                    <FileErrorText>Invalid file</FileErrorText>
                                                ) : (
                                                    <P>{this.getFileName()}</P>
                                                )
                                            }
                                        </FileDetailsContainer>

                                        <div>
                                            <P>
                                                <UploadClickText
                                                    onClick={ () => this.fileInput.click() }
                                                >
                                                    Click to upload
                                                </UploadClickText> or drag and drop files.
                                            </P>

                                            <P>Maximum file size is 50MB.</P>
                                        </div>

                                        <div style={ { display: uploadStatusValidated ? 'block' : 'none' } }>
                                            <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                                        </div>

                                        <div
                                            style={ {
                                                display: uploadStatusValidating || uploadStatusValidated
                                                    ? 'none'
                                                    : 'block'
                                            } }
                                        >
                                            <UploadButton
                                                size="large2"
                                                type="action"
                                                label={ submit ? 'Uploading...' : 'Upload' }
                                                disabled={ uploadButtonDisabled }
                                                ref={ ( ref ) => { this.validateButton = ref; } }
                                                onClick={ () => this.handleOnClickUpload() }
                                            />
                                        </div>

                                        <div style={ { display: uploadStatusValidating ? 'block' : 'none' } } >
                                            <StyledLoader className="animation">
                                                <H5 style={ { margin: '0' } }>{ uploadStatusValidating && 'VALIDATING' }</H5>
                                                <div className="anim3"></div>
                                            </StyledLoader>
                                        </div>

                                        <div style={ { display: uploadStatusValidated ? 'block' : 'none' } } >
                                            <StyledLoader className="animation">
                                                <H5 style={ { margin: '0' } }>Uploaded successfully</H5>
                                            </StyledLoader>
                                        </div>
                                    </UploadStepSubContainer>
                                </UploadStep>
                            </div>
                        </div>

                        { this.renderErrorsSection() }
                    </Container>
                </FormWrapper>

                <div
                    className="loader"
                    style={ { display: Object.keys( this.props.uploadErrors ).length ? 'block' : 'none', position: 'fixed', bottom: '0', background: '#fff', zIndex: 1 } }
                >
                    <FooterTablePagination
                        page={ this.state.pagination.current_page }
                        pageSize={ this.state.pagination.per_page }
                        pagination={ this.state.pagination }
                        onPageChange={ this.onPageChange }
                        onPageSizeChange={ this.onPageSizeChange }
                        paginationLabel={ this.state.label }
                    />
                </div>
            </UploadPageWrapper>
        );
    }
}

export default UploadAttendance;
