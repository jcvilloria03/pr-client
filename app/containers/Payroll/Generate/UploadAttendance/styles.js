import styled from 'styled-components';
import { Icon } from '@iconify/react';

import A from 'components/A';
import Button from 'components/Button';
import { H3, H4, P } from 'components/Typography';

export const UploadStep = styled.div`
    text-align: center;
    padding: 1.5rem;
    border: 2px dashed #00a5e5;
    border-radius: 4px;
    width: 100%;
    height: 100%;
    display: flex;
    gap: 28px;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    position: relative;

    ${H4} {
        color: #00a5e5;
        font-weight: 600;
        margin-bottom: 0;
    }

    ${H3} {
        font-weight: 600;
        margin-bottom: 0;
    }

    ${P} {
        font-size: 16px;
        margin-bottom: 0;
    }


`;

export const UploadStepSubContainer = styled.div`
    margin: auto;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    gap: 12px;

    ${P} {
        line-height: 1.2;
    }
`;

export const UploadStepHeading = styled( H3 )`
    color: #00a5e5;
`;

export const FileErrorText = styled( P )`
    color: #f21108;
`;

export const FileDetailsContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;

    ${P} {
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
        width: 100%;
        max-width: 400px;
    }

    ${P}:not(${FileErrorText}) {
        color: gray;
    }
`;

export const IconsContainer = styled.div`
    position: relative;
    width: fit-content;
    margin: auto;
`;

export const CloudUploadIcon = styled( Icon )`
    font-size: 20px;
    color: #fff;
    background-color: #00a5e5;
    border-radius: 50%;
    position: absolute;
    bottom: 0;
    border: 1px solid #fff;
    z-index: 1;
    padding: 4px;
`;

export const DocumentText16FilledIcon = styled( Icon )`
    font-size: 40px;
    color: #bbbbbb;
`;

export const InputFile = styled.input`
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: 0;
    opacity: 0;
    display: ${( props ) => ( props.hide ? 'none' : 'block' )};
`;

export const DownloadTemplateLink = styled.a`
    background-color: #83d24b;
    color: #fff;
    padding: 0.5rem 1rem;
    border-radius: 4px;
    font-size: 14px;
    line-height: 1.5;
    margin-top: 16px;
    border: 1px solid #83d24b;
    display: inline-block;

    :hover, :active, :focus {
        text-decoration: none;
        color: #fff
    }
`;

export const UploadButton = styled( Button )`
    margin-top: 8px;
    z-index: 1;
    position: relative;
    pointer-events: auto;
    cursor: pointer;
    color: #fff;
`;

export const BackLink = styled( A )`
    &&& {
        color: #00a5e5;
    }
`;

export const UploadClickText = styled.span`
    cursor: pointer;
    font-weight: 600;
    text-decoration: underline;
    position: relative;
`;
