import { fromJS } from 'immutable';
import { formatFullName } from 'utils/functions';
import {
    SET_PAYROLL_DATA,
    SET_LOADING,
    SET_DELETE,
    SET_UPDATE,
    SET_CLOSE,
    SET_OPEN,
    SET_PAYSLIP,
    NOTIFICATION_SAGA,
    SET_PAYROLL_DETAILS_TABLE_LOADING,
    SET_PAYROLL_EMPLOYEES,
    SET_DISBURSEMENT_SUMMARY,
    SET_GENERATE_PAYSLIP_LOADING,
    SET_LOADING_DISBURSMENT_METHOD_DETAILS,
    SET_DISBURSMENT_METHOD_DETAILS_TOTAL_EMPLOYEES,
    SET_DISBURSMENT_METHOD_DETAILS_EMPLOYEES,
    CLEAR_DISBURSMENT_METHOD_DETAILS_EMPLOYEES,
    SET_HAS_PAYROLL_REGISTER,
    SET_SALPAY_DISBURSEMENT_DETAILS_LOADING,
    SET_SALPAY_DISBURSEMENT_DETAILS,
    SET_DISBURSING_SALARIES,
    SET_DISBURSEMENT_TRANSFER_STATUS,
    SET_UPDATING_POSTING_DATE,
    SET_HAS_FAILED_DISBURSEMENT_TRANSFER,
    SET_MODAL_BUTTON_LOADING,
    SET_OTP_MODAL_BUTTON_LOADING,
    SET_OTP_MODAL_SHOW,
    SET_OTP_DETAILS,
    SET_OTP_ERROR_DETAILS,
    SET_OTP_ERROR,
    SET_OTP_REMAINING_DURATION,
    SET_OTP_TRIES_LEFT,
    SET_OTP_EXPIRED,
    SET_OTP_MOBILE_NUMBER
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    data: {},
    has_payslip: false,
    generate_payslip_loading: false,
    loading: {
        title: null,
        message: '',
        show: false
    },
    deleting: false,
    updating: false,
    closing: false,
    opening: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    payroll_details_table_loading: false,
    disbursement_summary: [],
    disbursement_method_details_table_loading: false,
    disbursement_method_details: {
        total: 0,
        employees: {}
    },
    has_payroll_register: false,
    salpay_disbursement_details_loading: false,
    salpay_disbursement_details: {
        balance: null,
        employees: [],
        total_fee_amount: null,
        total_net_pay_amount: null,
        company_salpay_account: null
    },
    disbursing_salaries: false,
    disbursement_transfer: {
        amount: 0,
        fee: 0,
        percent_completed: 0,
        total_transfers: 0,
        transfers_failed: 0,
        transfers_completed: 0
    },
    updating_posting_date: false,
    has_failed_disbursement_transfer: false,
    modal_button_loading: false,
    otp_details: {
        mobile_number: '',
        object_id: null,
        reference_number: '',
        salpay_user_id: null,
        type: null,
        expires_in: 300,
        expiration_date: '',
        resend_duration: 0
    },
    otp_error_details: {
        error: null,
        tries_left: 0,
        expired: false,
        remaining: 300
    },
    otp_modal_show: false,
    otp_modal_button_loading: false
});

/**
 * adds employee full name and total contributions to employee data
 * @param {Array} employees
 * @returns {Array}
 */
function prepareEmployeeData( employees ) {
    const formattedEmployees = Array.from( employees );
    formattedEmployees.forEach( ( employee, index ) => {
        formattedEmployees[ index ] = {
            ...formattedEmployees[ index ],
            total_contribution: Math.abs( employee.total_mandatory_employee ) + Math.abs( employee.total_mandatory_employer ),
            employeeName: `${employee.first_name || ''} ${employee.middle_name || ''}${employee.middle_name ? ' ' : ''}${employee.last_name || ''}`
        };
    });
    return formattedEmployees;
}

/**
 * Formats data to be read by Disbursement Summary table
 * @param {Array} disbursements
 * @returns {Array}
 */
function prepareDisbursementSummaryData( disbursements ) {
    return disbursements.map( ( disbursement ) => ({
        id: disbursement.name,
        disbursement_method: disbursement.name,
        employees: disbursement.attributes.count,
        net_pay: disbursement.attributes.amount,
        supported: disbursement.attributes.bankFile.supported
    }) );
}

/**
 * Formats data to be read by Disbursement Details table
 * @param {Array} employees
 * @returns {Array}
 */
function prepareDisbursementDetailsEmployeesData( employees ) {
    return employees.map( ( employee ) => ({
        id: employee.id,
        employee_name: employee.attributes.employeeName,
        account_number: employee.attributes.accountNumber,
        net_pay: employee.attributes.netPay
    }) );
}

/**
 * Formats data to be read by the SALPay Disburse Salaries table
 * @param {Object} data
 * @returns {Object}
 */
function prepareSalpayDisburseDetailsData( data ) {
    const { employees = [], ...rest } = data;

    return {
        ...rest,
        employees: employees.map( ( employee ) => ({
            id: employee.id,
            full_name: formatFullName({
                first_name: employee.first_name,
                middle_name: employee.middle_name,
                last_name: employee.last_name
            }),
            account_number: employee.salpay_account_number,
            net_pay: employee.net_pay,
            status: employee.status || 'Unpaid'
        }) ),
        total_amount: rest.total_net_pay_amount + rest.total_fee_amount
    };
}

/**
 * Flattens Disbursement Transfer Status
 * @param {Object} data - Transfer details
 */
function formatDisbursementTransferStatus( data ) {
    const {
        processing_status: processingStatus,
        ...restDisbursementStatus
    } = data;

    return {
        ...restDisbursementStatus,
        ...processingStatus
    };
}

/**
 *
 * View reducer
 *
 */
function viewReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_PAYROLL_DATA:
            return state.set( 'data', fromJS( action.payload ) );
        case SET_DELETE:
            return state.set( 'deleting', action.payload );
        case SET_UPDATE:
            return state.set( 'updating', action.payload );
        case SET_CLOSE:
            return state.set( 'closing', action.payload );
        case SET_OPEN:
            return state.set( 'opening', action.payload );
        case SET_PAYSLIP:
            return state.set( 'has_payslip', action.payload );
        case SET_GENERATE_PAYSLIP_LOADING:
            return state.set( 'generate_payslip_loading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_PAYROLL_DETAILS_TABLE_LOADING:
            return state.set( 'payroll_details_table_loading', action.payload );
        case SET_PAYROLL_EMPLOYEES:
            return state.setIn([ 'data', 'employees', action.payload.page ], fromJS( prepareEmployeeData( action.payload.employees ) ) );
        case SET_DISBURSEMENT_SUMMARY:
            return state.set( 'disbursement_summary', fromJS( prepareDisbursementSummaryData( action.payload ) ) );
        case SET_LOADING_DISBURSMENT_METHOD_DETAILS:
            return state.set( 'disbursement_method_details_table_loading', action.payload );
        case SET_DISBURSMENT_METHOD_DETAILS_TOTAL_EMPLOYEES:
            return state.setIn([ 'disbursement_method_details', 'total' ], action.payload );
        case SET_DISBURSMENT_METHOD_DETAILS_EMPLOYEES:
            return state.setIn(
                [ 'disbursement_method_details', 'employees', action.payload.page ],
                fromJS( prepareDisbursementDetailsEmployeesData( action.payload.employees ) )
            );
        case CLEAR_DISBURSMENT_METHOD_DETAILS_EMPLOYEES:
            return state.setIn([ 'disbursement_method_details', 'employees' ], fromJS({}) );
        case SET_HAS_PAYROLL_REGISTER:
            return state.set( 'has_payroll_register', action.payload );
        case SET_SALPAY_DISBURSEMENT_DETAILS_LOADING:
            return state.set( 'salpay_disbursement_details_loading', action.payload );
        case SET_SALPAY_DISBURSEMENT_DETAILS:
            return state.set( 'salpay_disbursement_details', fromJS( prepareSalpayDisburseDetailsData( action.payload ) ) );
        case SET_DISBURSING_SALARIES:
            return state.set( 'disbursing_salaries', action.payload );
        case SET_DISBURSEMENT_TRANSFER_STATUS:
            return state.set( 'disbursement_transfer', fromJS( formatDisbursementTransferStatus( action.payload ) ) );
        case SET_UPDATING_POSTING_DATE:
            return state.set( 'updating_posting_date', action.payload );
        case SET_HAS_FAILED_DISBURSEMENT_TRANSFER:
            return state.set( 'has_failed_disbursement_transfer', action.payload );
        case SET_OTP_MODAL_BUTTON_LOADING:
            return state.set( 'otp_modal_button_loading', action.payload );
        case SET_MODAL_BUTTON_LOADING:
            return state.set( 'modal_button_loading', action.payload );
        case SET_OTP_MODAL_SHOW:
            return state.set( 'otp_modal_show', action.payload );
        case SET_OTP_DETAILS:
            return state.set( 'otp_details', fromJS( action.payload ) );
        case SET_OTP_ERROR_DETAILS:
            return state.set( 'otp_error_details', fromJS( action.payload ) );
        case SET_OTP_ERROR:
            return state.setIn([ 'otp_error_details', 'error' ], action.payload );
        case SET_OTP_TRIES_LEFT:
            return state.setIn([ 'otp_error_details', 'tries_left' ], action.payload );
        case SET_OTP_EXPIRED:
            return state.setIn([ 'otp_error_details', 'expired' ], action.payload );
        case SET_OTP_REMAINING_DURATION:
            return state.setIn([ 'otp_error_details', 'remaining' ], action.payload );
        case SET_OTP_MOBILE_NUMBER:
            return state.setIn([ 'otp_details', 'mobile_number' ], action.payload );

        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default viewReducer;
