/* eslint-disable react/no-unescaped-entities */
/* eslint-disable camelcase */
import React from 'react';
import moment from 'moment';
import A from '../../../../components/A';

import { OtpWrapper, InputMask } from './styles';

import OtpBlocked from '../OtpBlocked';

/**
* Otp
*/
class Otp extends React.PureComponent {
    static propTypes = {
        error: React.PropTypes.string,
        triesLeft: React.PropTypes.number,
        mobile: React.PropTypes.string,
        referenceNumber: React.PropTypes.string,
        onRef: React.PropTypes.func,
        resendOtp: React.PropTypes.func,
        onChange: React.PropTypes.func,
        blocked: React.PropTypes.bool,
        remainingDuration: React.PropTypes.number,
        actionLocked: React.PropTypes.bool,
        expired: React.PropTypes.bool,
        resendDuration: React.PropTypes.number
    }

    static defaultProps = {
        mobile: '',
        error: '',
        triesLeft: 0,
        remainingDuration: 0,
        expired: false
    }

    constructor( props ) {
        super( props );

        this.state = {
            code: '',
            countdown: null,
            timer: null,
            resend_countdown: props.resendDuration,
            resend_timer: 0,
            error_not_found: false
        };

        this.resetTimer = this.resetTimer.bind( this );

        this.updateResendCountdown = this.updateResendCountdown.bind( this );
    }

    componentWillMount() {
        this.setState({
            resend_timer: setInterval( this.updateResendCountdown, 1000 )
        });
    }

    componentDidMount() {
        this.props.onRef( this );
        this.input && this.input.focus();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.resendDuration !== this.props.resendDuration
            && this.resetResendTimer( nextProps.resendDuration );

        if ( nextProps.referenceNumber !== this.props.referenceNumber ) {
            this.setState({
                resend_countdown: nextProps.resendDuration,
                resend_timer: setInterval( this.updateResendCountdown, 1000 )
            });
        }

        if ( nextProps.error === 'Record not found.' ) {
            this.setState({
                error_not_found: true,
                resend_countdown: 30
            });
        } else if ( nextProps.error ) {
            nextProps.error !== this.props.error
                && this.resetTimer( false ) && this.setState({ error_not_found: false });
        }

        if ( nextProps.error == null ) {
            this.setState({
                error_not_found: false,
                error: ''
            });
        }
    }

    componentWillUnmount() {
        this.state.timer && clearInterval( this.state.timer );
        this.state.resend_timer && clearInterval( this.state.resend_timer );
        this.props.onRef( null );
    }

    resetTimer( resendCode = true ) {
        clearInterval( this.state.timer );
        this.setState({
            code: '',
            countdown: this.props.blocked ? 0 : 60,
            timer: this.props.blocked ? null : setInterval( this.checkResend, 1000 )
        }, () => {
            if ( resendCode ) {
                this.props.resendOtp();
            }
        });
    }

    resetResendTimer( duration ) {
        clearInterval( this.state.resend_timer );
        this.setState({
            code: '',
            resend_countdown: duration,
            resend_timer: null
        });
    }

    updateResendCountdown() {
        const { resend_countdown } = this.state;
        if ( resend_countdown > 0 ) {
            this.setState({ resend_countdown: resend_countdown - 1 });
        } else {
            clearInterval( this.state.resend_timer );
            this.setState({ resend_timer: null });
        }
    }

    checkResend() {
        if ( this.state.countdown ) {
            const { countdown } = this.state;
            if ( countdown > 0 ) {
                this.setState({ countdown: countdown - 1 });
            } else {
                clearInterval( this.state.timer );
                this.setState({ timer: null });
            }
        }
    }

    renderChars() {
        const { code } = this.state;
        const inputs = [];
        for ( let i = 0; i < 4; i += 1 ) {
            inputs.push(
                <div
                    key={ i }
                    className={ `char${code.length === i ? ' active' : ''}` }
                >
                    { code[ i ] || '' }
                </div>
            );
        }

        return (
            <label htmlFor="code" className="code" >
                { inputs }
            </label>
        );
    }

    /**
    * OTP render method.
    */
    render() {
        const {
            error,
            mobile,
            blocked,
            remainingDuration,
            triesLeft,
            actionLocked,
            expired
        } = this.props;

        if ( blocked ) {
            return (
                <OtpBlocked
                    mobile={ mobile }
                    remainingDuration={ remainingDuration }
                    actionLocked={ actionLocked }
                />
            );
        }

        const resend_countdown = moment.duration( this.state.resend_countdown, 'seconds' );
        const remainingResendTime = `${resend_countdown.minutes()}:${resend_countdown.seconds().toString().padStart( 2, '0' )}`;
        const showResend = !( this.state.resend_countdown > 0 );

        return (
            <OtpWrapper className="otp">
                <p className="message"> Enter the OTP that was sent to <b> { this.props.mobile }</b></p>
                <p className="message"><b> Reference No.: { this.props.referenceNumber }</b></p>
                <InputMask expired={ blocked }>
                    { this.renderChars() }
                    <input
                        id="code"
                        name="code"
                        type="number"
                        pattern="[0-9]*"
                        onChange={ ( event ) => {
                            let value = event.target.value;
                            value = value.replace( /\D/g, '' );
                            if ( value.length <= 4 ) {
                                this.setState({
                                    code: value
                                }, () => this.props.onChange( this.state.code ) );
                            }
                        } }
                        maxLength="4"
                        max={ 9999 }
                        value={ this.state.code }
                        ref={ ( ref ) => { this.input = ref; } }
                        autoComplete="off"
                    />
                    <div className="error">
                        { error && !this.state.error_not_found && expired ? ( <span> { error } </span> ) : '' }

                        { error && !this.state.error_not_found && triesLeft > 0 && triesLeft < 3 ? (
                            <span>
                                You have entered an invalid OTP. Please input the correct OTP code for the disbursement to proceed
                                <br /> <br />
                                Number of attempts left: { triesLeft } out of 3
                            </span>
                        ) : '' }

                    </div>
                    <div className="error">
                        { error && this.state.error_not_found ? (
                            <span>
                                OTP Record Not Found.
                            </span>
                        ) : '' }

                    </div>
                </InputMask>

                <br />
                <p className="message"> Didn't receive your One-Time Password (OTP)?&nbsp;
                    { !showResend
                        ? <span><del style={ { color: '#808080' } }>Resend</del> { this.state.error_not_found ? '' : remainingResendTime}</span>
                        : <A onClick={ this.props.resendOtp } >
                            <b><u>Resend</u></b>
                        </A>
                    }
                </p>
            </OtpWrapper>
        );
    }
}

export default Otp;
