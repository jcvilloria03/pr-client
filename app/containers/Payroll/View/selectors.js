import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectViewDomain = () => ( state ) => state.get( 'view' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by View
 */

const makeSelectData = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'data' ).toJS()
);

const makeSelectHasPayslip = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'has_payslip' )
);

const makeSelectGeneratePayslipLoading = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'generate_payslip_loading' )
);

const makeSelectLoading = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectDeleting = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'deleting' )
);

const makeSelectUpdating = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'updating' )
);

const makeSelectClosing = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'closing' )
);

const makeSelectOpening = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'opening' )
);

const makeSelectNotification = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectPayrollDetailsTableLoading = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'payroll_details_table_loading' )
);

const makeSelectDisbursementSummary = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'disbursement_summary' ).toJS()
);

const makeSelectDisbursementDetailsTableLoading = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'disbursement_method_details_table_loading' )
);

const makeSelectDisbursementMethodDetails = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'disbursement_method_details' ).toJS()
);

const makeSelectHasPayrollRegister = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'has_payroll_register' )
);

const makeSelectSalpayDisbursementDetailsLoading = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'salpay_disbursement_details_loading' )
);

const makeSelectSalpayDisbursementDetails = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'salpay_disbursement_details' ).toJS()
);

const makeSelectDisbursingSalaries = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'disbursing_salaries' )
);

const makeSelectDisbursementTransferStatus = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'disbursement_transfer' ).toJS()
);

const makeSelectUpdatingPostingDate = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'updating_posting_date' )
);

const makeSelectHasFailedDisbursementTransfer = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'has_failed_disbursement_transfer' )
);

const makeSelectModalButtonLoading = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'modal_button_loading' )
);

const makeSelectOtpDetails = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'otp_details' ).toJS()
);

const makeSelectOtpErrorDetails = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'otp_error_details' ).toJS()
);

const makeSelectOtpModalShow = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'otp_modal_show' )
);

const makeSelectOtpModalButtonLoading = () => createSelector(
    selectViewDomain(),
    ( substate ) => substate.get( 'otp_modal_button_loading' )
);

export {
    selectViewDomain,
    makeSelectData,
    makeSelectHasPayslip,
    makeSelectGeneratePayslipLoading,
    makeSelectLoading,
    makeSelectDeleting,
    makeSelectUpdating,
    makeSelectClosing,
    makeSelectOpening,
    makeSelectNotification,
    makeSelectPayrollDetailsTableLoading,
    makeSelectDisbursementSummary,
    makeSelectDisbursementDetailsTableLoading,
    makeSelectDisbursementMethodDetails,
    makeSelectHasPayrollRegister,
    makeSelectSalpayDisbursementDetailsLoading,
    makeSelectSalpayDisbursementDetails,
    makeSelectDisbursingSalaries,
    makeSelectDisbursementTransferStatus,
    makeSelectUpdatingPostingDate,
    makeSelectHasFailedDisbursementTransfer,
    makeSelectModalButtonLoading,
    makeSelectOtpDetails,
    makeSelectOtpErrorDetails,
    makeSelectOtpModalShow,
    makeSelectOtpModalButtonLoading
};
