import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';

import { H3 } from 'components/Typography';
import Input from 'components/Input';
import Button from 'components/Button';

import { PAYROLL_STATUSES, PAYROLL_TYPES } from '../constants';

import { SummaryStyleWrapper } from './styles';

/**
 * Payroll View Summary
 */
class PayrollViewSummary extends React.PureComponent {
    static propTypes = {
        generatePayslip: React.PropTypes.func,
        generatePayslipLoading: React.PropTypes.bool,
        sendPayslips: React.PropTypes.func,
        data: React.PropTypes.object,
        hasPayslip: React.PropTypes.bool,
        edit: React.PropTypes.bool,
        actionState: React.PropTypes.shape({
            deleting: React.PropTypes.bool,
            closing: React.PropTypes.bool
        }),
        viewPayslipAuth: React.PropTypes.bool,
        generatePayslipAuth: React.PropTypes.bool,
        sendPayslipAuth: React.PropTypes.bool,
        regeneratePayroll: React.PropTypes.func,
        downloadPayrollRegister: React.PropTypes.func,
        hasPayrollRegister: React.PropTypes.bool
    }

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            data: this.props.data
        };

        this.editModeContent = this.editModeContent.bind( this );
        this.viewModeContent = this.viewModeContent.bind( this );
    }

    /**
     * Updates the component when new props are received.
     */
    componentWillReceiveProps( nextProps ) {
        if ( nextProps.edit !== this.props.edit || nextProps.actionState !== this.props.actionState ) {
            this.buttonGeneratePayslip
            && this.buttonGeneratePayslip.setState({
                disabled: nextProps.edit || nextProps.actionState.deleting || nextProps.actionState.closing
            });
        }
    }

    /**
     * Format the date by MMMM DD, YYYY
     * @param date = date to format
     * @returns {string} formmatted date
     */
    formatDate( date ) {
        if ( moment( date ).isValid() ) {
            return moment( date ).format( 'MMMM DD, YYYY' );
        }
        return 'Invalid Date';
    }

    /**
     *  converts a number to currency format.
     * @param value = value to format
     * @returns {string} value in currency format
     */
    formatNumber( value, withCurrency = true ) {
        const convertedValue = Number( value );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            formattedValue = `${Number( convertedValue ).toFixed( 2 ).replace( /\d(?=(\d{3})+\.)/g, '$&,' )}`;
            if ( withCurrency ) formattedValue = `PHP ${formattedValue}`;
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    /**
     * updates payrll summary values in state
     * @param property = employee property to change.
     * @param value = new value.
     */
    updateSummaryValues( property, value ) {
        const stateCopy = Object.assign({}, this.state );
        stateCopy.data[ property ] = value;
        this.setState( stateCopy );
    }

    /**
     * Reset values to props
     */
    resetData() {
        this.setState({ data: this.props.data });
    }

    /**
    * renders buttons for payslip
    */
    buttonPayslipsContent() {
        const { OPEN, CLOSED, DISBURSED } = PAYROLL_STATUSES;

        return (
            <div className="column" style={ { display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' } }>
                { this.props.regeneratePayroll && this.props.data.status === OPEN && (
                    <Button
                        id="regenerate-payroll-button"
                        key="0"
                        size="large"
                        type="action"
                        label="Regenerate Payroll"
                        alt
                        className="button"
                        onClick={ () => { this.props.regeneratePayroll( this.props.data.id ); } }
                    />
                ) }
                { this.props.sendPayslipAuth && ( this.props.data.status === CLOSED || this.props.data.status === DISBURSED ) && (
                    <Button
                        id="send-payslips-button"
                        key="0"
                        size="large"
                        type="action"
                        label="Send Payslips"
                        className="button"
                        disabled={ !this.props.hasPayslip }
                        onClick={ () => { this.props.sendPayslips( this.props.data.id ); } }
                    />
                ) }
                {
                    this.props.generatePayslipAuth
                        ? <Button
                            id="generate-payslip-button"
                            ref={ ( ref ) => { this.buttonGeneratePayslip = ref; } }
                            size="large"
                            type="action"
                            label={ (
                                <span>
                                    { this.props.hasPayslip
                                        ? 'Regenerate Payslips'
                                        : 'Generate Payslips'
                                    }
                                    { this.props.generatePayslipLoading
                                        ? <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                                        : null
                                    }
                                </span>
                            ) }
                            alt
                            onClick={ this.props.generatePayslip }
                            disabled={ this.props.edit || this.props.generatePayslipLoading }
                        />
                        : false
                }
                {
                    this.props.viewPayslipAuth
                        ? ([
                            <Button
                                id="view-payslip-button"
                                key="0"
                                size="large"
                                type={ this.props.hasPayslip ? 'action' : 'grey' }
                                label="View Payslips"
                                alt={ this.props.hasPayslip }
                                disabled={ !this.props.hasPayslip }
                                onClick={ () => { document.getElementById( 'linkViewPayslip' ).click(); } }
                            />,
                            <Link
                                key="1"
                                id="linkViewPayslip"
                                to={ `/payroll/payslip?id=${this.props.data.payroll_group_id}&payroll_id=${this.props.data.id}&date=${this.props.data.payroll_date}` }
                                target="_blank"
                                style={ { display: 'none' } }
                            >
                                VIEW PAYSLIPS
                            </Link>
                        ])
                        : false
                }
                { this.props.downloadPayrollRegister && (
                    <Button
                        id="payroll-register-button"
                        key="payroll-register-button"
                        size="large"
                        type={ this.props.hasPayrollRegister ? 'action' : 'grey' }
                        label="Payroll Register"
                        alt={ this.props.hasPayrollRegister }
                        disabled={ !this.props.hasPayrollRegister }
                        onClick={ () => { this.props.downloadPayrollRegister( this.props.data ); } }
                    />
                ) }
            </div>
        );
    }

    /**
     * Gets content of summary in edit mode
     */
    editModeContent() {
        return (
            <div className="info edit">
                <div className="column">
                    <div>
                        <label htmlFor="inputGross" className="label">Gross Pay:</label>
                        <Input
                            id="inputGross"
                            onChange={ ( value ) => { this.updateSummaryValues( 'gross', value ); } }
                            value={ this.state.data.gross }
                            type="number"
                        />
                    </div>
                    <div>
                        <label htmlFor="inputNet" className="label">Net Pay:</label>
                        <Input
                            id="inputNet"
                            onChange={ ( value ) => { this.updateSummaryValues( 'net', value ); } }
                            value={ this.state.data.net }
                            type="number"
                        />
                    </div>
                </div>
                <div className="column">
                    <div>
                        <label htmlFor="inputTotalDeduction" className="label">Total Deductions:</label>
                        <Input
                            id="inputTotalDeduction"
                            onChange={ ( value ) => { this.updateSummaryValues( 'total_deductions', value ); } }
                            value={ this.state.data.total_deductions }
                            type="number"
                        />
                    </div>
                    <div>
                        <label htmlFor="inputTotalContributions" className="label">Total Contributions:</label>
                        <Input
                            id="inputTotalContributions"
                            onChange={ ( value ) => { this.updateSummaryValues( 'total_contributions', value ); } }
                            value={ this.state.data.total_contributions }
                            type="number"
                        />
                    </div>
                </div>
                <div className="column">
                    <div>
                        <label htmlFor="inputTotalEmployees" className="label">No. of Employees:</label>
                        <div className="value">{ this.state.data.total_employees }</div>
                    </div>
                    <div>
                        <label htmlFor="inputTotalStatus" className="label">Status:</label>
                        <div className={ `value status ${this.props.data.status.toLowerCase()}` }>{ this.props.data.status }</div>
                    </div>
                </div>
                { this.buttonPayslipsContent() }
            </div>
        );
    }

    /**
     * Gets content of summary in view mode
     */
    viewModeContent() {
        return (
            <div className="info">
                <div className="column">
                    <div>
                        <span className="label">Gross Pay:</span>
                        <div className="value">{ this.formatNumber( this.state.data.gross )}</div>
                    </div>
                    <div>
                        <span className="label">Net Pay:</span>
                        <div className="value">{ this.formatNumber( this.state.data.net )}</div>
                    </div>
                </div>
                <div className="column">
                    <div>
                        <span className="label">Total Deductions:</span>
                        <div className="value">{ this.formatNumber( this.state.data.total_deductions )}</div>
                    </div>
                    <div>
                        <span className="label">Total Contributions:</span>
                        <div className="value">{ this.formatNumber( this.state.data.total_contributions )}</div>
                    </div>
                </div>
                <div className="column">
                    <div>
                        <span className="label">No. of Employees:</span>
                        <div className="value">{ this.state.data.total_employees }</div>
                    </div>
                    <div>
                        <span className="label">Status:</span>
                        <div className={ `value status ${this.props.data.status.toLowerCase()}` }>{ this.props.data.status }</div>
                    </div>
                </div>
                { this.buttonPayslipsContent() }
            </div>
        );
    }

    renderPayrollDate() {
        const {
            type,
            payroll_group_name: payrollGroup,
            start_date: startDate,
            end_date: endDate,
            payroll_date: payrollDate
        } = this.props.data;

        return type === PAYROLL_TYPES.REGULAR
            ? `${payrollGroup} : ${this.formatDate( startDate )} - ${this.formatDate( endDate )}`
            : this.formatDate( payrollDate );
    }

    /**
     * Renders the component to DOM
     */
    render() {
        return (
            <SummaryStyleWrapper>
                <div className="title">
                    <div className={ `status ${this.props.data.status.toLowerCase()}` }>{ this.props.data.status }</div>
                    <H3>
                        { this.renderPayrollDate() }
                    </H3>
                </div>
                {
                    this.props.edit ?
                        this.editModeContent()
                        : this.viewModeContent()
                }
            </SummaryStyleWrapper>
        );
    }
}

export default PayrollViewSummary;
