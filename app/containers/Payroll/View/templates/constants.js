export const TIME_ABBREVIATION_FULL_TEXT = {
    REGULAR: 'Regular',
    UNDERTIME: 'Undertime',
    TARDY: 'Tardiness',
    OVERBREAK: 'Overbreak',
    PAID_LEAVE: 'Paid Leave',
    ABSENT: 'Absent',
    UNPAID_LEAVE: 'Unpaid Leave',
    UH: 'Unworked Holiday',
    SH: 'Special Holiday',
    RH: 'Regular Holiday',
    '2RH': 'Double Regular Holiday',
    NT: 'Night',
    SHNT: 'Special Holiday Night',
    RHNT: 'Regular Holiday Night',
    '2RHNT': 'Double Regular Holiday Night',
    RHSH: 'Regular and Special Holiday',
    RHSHNT: 'Regular and Special Holiday Night',
    RHSHOT: 'Regular and Special Holiday Overtime',
    RHSHNTOT: 'Regular and Special Holiday Night Overtime',
    OT: 'Overtime',
    SHOT: 'Special Holiday Overtime',
    RHOT: 'Regular Holiday Overtime',
    '2RHOT': 'Double Regular Holiday Overtime',
    NTOT: 'Night Overtime',
    SHNTOT: 'Special Holiday Night Overtime',
    RHNTOT: 'Regular Holiday Night Overtime',
    '2RHNTOT': 'Double Regular Holiday Night Overtime',
    RD: 'Rest Day',
    RDSH: 'Rest Day Special Holiday',
    RDRH: 'Rest Day Regular Holiday',
    RD2RH: 'Rest Day Double Regular Holiday',
    RDNT: 'Rest Day Night',
    RDSHNT: 'Rest Day Special Holiday Night',
    RDRHNT: 'Rest Day Regular Holiday Night',
    RDRHSH: 'Rest Day Regular and Special Holiday',
    RDRHSHNT: 'Rest Day Regular and Special Holiday Night',
    RDRHSHOT: 'Rest Day Regular and Special Holiday Overtime',
    RDRHSHNTOT: 'Rest Day Regular and Special Holiday Night Overtime',
    RD2RHNT: 'Rest Day Double Regular Holiday Night',
    RDOT: 'Rest Day Overtime',
    RDSHOT: 'Rest Day Special Holiday Overtime',
    RDRHOT: 'Rest Day Regular Holiday Overtime',
    RD2RHOT: 'Rest Day Double Regular Holiday Overtime',
    RDNTOT: 'Rest Day Night Overtime',
    RDSHNTOT: 'Rest Day Special Holiday Night Overtime',
    RDRHNTOT: 'Rest Day Regular Holiday Night Overtime',
    RD2RHNTOT: 'Rest Day Double Regular Holiday Night Overtime'
};

export const ARRANGED_ATTENDANCE_TYPES = [
    'ABSENT',
    'UNPAID_LEAVE',
    'UNDERTIME',
    'TARDY',
    'OVERBREAK',
    'NT',
    'OT',
    'NT+OT',
    'RD',
    'RD+NT',
    'RD+OT',
    'RD+NT+OT',
    'RH',
    'RH+NT',
    'RH+OT',
    'RH+NT+OT',
    'SH',
    'SH+NT',
    'SH+OT',
    'SH+NT+OT',
    'RH+SH',
    'RH+SH+NT',
    'RH+SH+OT',
    'RH+SH+NT+OT',
    '2RH',
    '2RH+NT',
    '2RH+OT',
    '2RH+NT+OT',
    'RD+RH',
    'RD+RH+NT',
    'RD+RH+OT',
    'RD+RH+NT+OT',
    'RD+SH',
    'RD+SH+NT',
    'RD+SH+OT',
    'RD+SH+NT+OT',
    'RD+RH+SH',
    'RD+RH+SH+NT',
    'RD+RH+SH+OT',
    'RD+RH+SH+NT+OT',
    'RD+2RH',
    'RD+2RH+NT',
    'RD+2RH+OT',
    'RD+2RH+NT+OT'
];

export const NEGATIVE_VALUES = [
    'TARDY',
    'OVERBREAK',
    'UNDERTIME',
    'ABSENT',
    'UNPAID_LEAVE'
];

export const NEGATIVE_STYLE = 'negative value';
