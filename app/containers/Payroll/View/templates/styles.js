import styled from 'styled-components';

export const SummaryStyleWrapper = styled.div`
    margin-bottom: 50px;

    .title {
        background-color: #00A5E5;
        box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
        color: #fff;
        padding: 12px 18px;
        display: flex;
        align-content: center;

        .status {
            padding: 8px 10px;
            border-radius: 4px;
            margin-right: 20px;
            font-weight: 600;
            display: inline-flex;
            align-self: center;
        }
        .status.open, .status.disbursed {
            background-color: #4ABA4A;
        }
        .status.closed {
            background-color: #F21108;
        }
        h3 {
            display: flex;
            margin: 0 10px 0 0;
            display: flex;
            align-self: center;
            font-weight: 500;
        }
    }
    .info {
        display: flex;
        flex-direction: row;
        height: 193px;
        background-color: #f8f8f8;
        padding: 20px 15px;

        .column {
            display: inline-flex;
            flex-direction: column;
            width: 24%;
            padding-right: 40px;
            justify-content: space-around;

            > div {
                display: inline-flex;
                flex-direction: row;

                .label {
                    flex-grow: 1;
                }
                .value {
                    font-weight: bold;
                    text-align: right;
                }
            }

            &:last-child {
                flex-direction: row;
                width: 28%;
                padding-right: 20px;
                justify-content: center;

                button {
                    line-height: 9px;
                    width: 100%;

                    .fa-circle-o-notch {
                        margin-left: 5px;
                    }
                }
            }
        }

        .status.open, .status.disbursed { color: #4ABA4A; }
        .status.closed { color: #F21108; }

        &.edit {
            label + div {
                max-width: 55%;
                flex-grow: 2;
                text-align: right;
            }
            input {
                height: 36px;
                text-align: right;
            }
            input + p {
                display: none;
            }
            .column {
                padding: 0 20px;
            }
        }
    }

    .rt-tr .rt-td:last-of-type {
        padding-left: 0;
    }

    .disbursement-actions {
        border-left: 1px solid #a3a3a3;
        padding-left: 20px;
    }

    .negative {
        color: red;
    }
`;

export const Wrapper = styled.div`
    .rt-resizable-header a {
        border-bottom: none !important;
        i {
            color: #444;
            font-size: 16px;
            margin-right: 4px;
        }
    }
    .rt-tr-group .rt-td {
        div > input[type=number] {
            height: 36px;
            text-align: right;
            padding: 0 7px;
            width: 140px;
        }
        & > div > p {
            display: none;
        }
    }

    .rt-thead.-headerGroups {
        border-bottom: none !important;

        & .rt-th {
            padding: 12px 7px;
        }
    }

    .details {
        padding: 20px;
        background: #f8f8f8;

        hr {
            margin: 0;
        }

        h4 {
            margin-bottom: 0;
            font-weight: 700;
        }

        .category {
            display: inline-block;
            margin-bottom: 10px;

            .entry {
                display: inline-flex;
                flex-direction: column;

                .header {
                    font-weight: 600;
                    margin-top: 10px;

                    a {
                        color: inherit !important;
                        border: none;
                    }
                }

                .line {
                    display: inline-flex;
                    min-width: 320px;
                    justify-content: space-between;
                    margin: 10px;
                    align-items: center;

                    p {
                        display: none;
                    }
                    .label {
                        font-weight: 400;
                        background: rgba(0, 0, 0, 0.05);
                        border-radius: 4px 0 0 4px;
                        padding: 9px 8px;
                        flex-grow: 1;
                        height: 40px;
                        align-self: stretch;
                    }

                    .value {
                        font-weight: 400;
                        background: rgba(0, 0, 0, 0.05);
                        border-radius: 0 4px 4px 0;
                        padding: 8px;
                        align-self: stretch;
                        position: relative;

                        & > div {
                            width: 140px;
                        }
                    }

                    .truncate {
                        max-width: 173px;
                        overflow: hidden;
                        white-space: nowrap;
                        text-overflow: ellipsis;
                    }

                    .stretch {
                        flex-grow: 1;
                        text-align: right;
                    }
                }
            }

            .has-subsection {
                display: block;
            }
        }
    }

    .uppercase {
        text-transform: uppercase;
    }

    .negative {
        color: red;
    }
`;

export const ModalBody = styled.div`
    min-height: 500px;

    .total-amount {
        margin-left: 25px;
    }

    .account-balance {
        padding-left: 20px;
    }

    .total-amount, .account-balance {
        font-weight: 600;
    }

    section {
        display: flex;
        flex-direction: row;
        justify-content: center;
        width: 100%;

        & > div {
            width: 50%;
        }
    }

    .amount-breakdown {
        display: flex;
        flex-direction: row;
        margin-bottom: 10px;

        & > div {
            border-top: 1px solid #c7c7c7;
            padding-top: 5px;

            h6 {
                &:first-child {
                    font-weight: 400;
                }

                &:last-child {
                    margin-bottom: 5px;
                }
            }
        }

        .net-pay {
            border-right: 1px solid #c7c7c7;
        }

        .fees, .net-pay {
            padding-right: 20px;
            padding-left: 20px;
        }
    }

    .progress-wrapper {
        position: relative;

        .disbursed {
            margin-left: 5px;
            margin-right: 30px;
            color: #93c531;
        }

        .failed {
            margin-left: 5px;
            color: #f21108;
        }

        progress[value] {
            appearance: none;
            background-color: #E6F5DB;

            &::-webkit-progress-bar {
                background-color: #E6F5DB;
                border-radius: 2px;
            }

            &::-moz-progress-bar {
                background-color: #83d24b;
            }

            &::-webkit-progress-value {
                background-color: #83d24b;
            }

            &::after {
                content: attr(value) ' %';
                position: absolute;
                top: -25%;
                left: calc( 50% - 12px );
            }
        }
    }

    .failed-disburse {
        padding: 20px 0;
        font-size: 14px;
    }
`;

export const ProgressFallback = styled.div`
    background-color: #E6F5DB;
    border-radius: 2px;
    width: 100%;
    position: relative;
    display: block;

    div {
        background-color: #83d24b;
        border-radius: 2px;
        display: block;
        width: ${( props ) => props.progress}%;

        span {
            opacity: 0;
        }
    }
`;

export const OtpModalBody = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const SpinnerWrapper = styled.span`
    padding: 0 20px;
`;
