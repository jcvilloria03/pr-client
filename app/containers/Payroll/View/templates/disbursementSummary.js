/* eslint-disable react/sort-comp */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';

import Table from 'components/Table';
import AsyncTable from 'components/AsyncTable';
import Button from 'components/Button';
import Modal from 'components/Modal';
import OtpModal from 'components/OtpModal';
import { H4, H6 } from 'components/Typography';
import { formatCurrency } from 'utils/functions';

import { salpayAdminService } from 'utils/SalpayAdminService';
import { makeSelectUserInformationState } from 'containers/App/selectors';

import {
    SummaryStyleWrapper,
    ModalBody,
    OtpModalBody,
    ProgressFallback
} from './styles';

import { PAYROLL_STATUSES, FILE_TYPE } from '../constants';
import {
    makeSelectDisbursementSummary,
    makeSelectDisbursementDetailsTableLoading,
    makeSelectDisbursementMethodDetails,
    makeSelectSalpayDisbursementDetailsLoading,
    makeSelectSalpayDisbursementDetails,
    makeSelectDisbursingSalaries,
    makeSelectDisbursementTransferStatus,
    makeSelectHasFailedDisbursementTransfer,
    makeSelectOtpDetails,
    makeSelectOtpErrorDetails,
    makeSelectModalButtonLoading,
    makeSelectOtpModalButtonLoading,
    makeSelectOtpModalShow
} from '../selectors';

import {
    clearDisbursementMethodDetailsEmployees,
    downloadBankAdvise,
    downloadBankFile,
    setModalButtonLoading,
    setOtpModalButtonLoading,
    setOtpModalShow,
    setOtpErrorDetails
} from '../actions';

import Otp from '../Otp';

/**
 * Disbursement Summary
 */
class DisbursementSummary extends React.PureComponent {
    static propTypes = {
        payrollId: React.PropTypes.number,
        companyId: React.PropTypes.number,
        disbursementSummary: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                id: React.PropTypes.string,
                disbursement_method: React.PropTypes.string,
                employees: React.PropTypes.number,
                net_pay: React.PropTypes.number,
                supported: React.PropTypes.bool
            })
        ),
        getDisbursementMethodDetails: React.PropTypes.func,
        clearDisbursementMethodDetailsEmployees: React.PropTypes.func,
        setOtpErrorDetails: React.PropTypes.node,
        setOtpModalButtonLoading: React.PropTypes.func,
        setOtpModalShow: React.PropTypes.func,
        loadingDisbursementMethodDetails: React.PropTypes.bool,
        disbursementMethodDetails: React.PropTypes.object,
        payrollStatus: React.PropTypes.string,
        getSalpayDisbursementDetails: React.PropTypes.func,
        salpayDisbursementDetailsLoading: React.PropTypes.bool,
        salpayDisbursementDetails: React.PropTypes.shape({
            balance: React.PropTypes.number,
            employees: React.PropTypes.array,
            total_fee_amount: React.PropTypes.number,
            total_net_pay_amount: React.PropTypes.number,
            company_salpay_account: React.PropTypes.string,
            total_amount: React.PropTypes.number
        }),
        requestDisburseSalaries: React.PropTypes.func,
        requestValidateDisbursement: React.PropTypes.func,
        disbursingSalaries: React.PropTypes.bool,
        disburseTransferStatus: React.PropTypes.object,
        getDisburseSalariesStatus: React.PropTypes.func,
        hasFailedDisbursementTransfer: React.PropTypes.bool,
        downloadBankAdvise: React.PropTypes.func,
        downloadBankFile: React.PropTypes.func,
        modalButtonLoading: React.PropTypes.bool,
        otpModalButtonLoading: React.PropTypes.bool,
        otpModalShow: React.PropTypes.bool,
        otpDetails: React.PropTypes.object,
        otpErrorDetails: React.PropTypes.object,
        requestResendOtp: React.PropTypes.func,
        userInformation: React.PropTypes.object
    }

    constructor( props ) {
        super( props );

        this.state = {
            selected_disbursement_method_total_amount: null,
            disbursement_method_filter: '',
            is_bank_selected: null,
            disbursement_method_detail_page: 1,
            disbursement_method_per_page: 5,
            otp_code: '',
            resend_duration: 0
        };
    }

    componentDidMount() {
        const isDisbursingOrHasFailed = this.props.payrollStatus === PAYROLL_STATUSES.DISBURSING || this.props.hasFailedDisbursementTransfer;

        if ( isDisbursingOrHasFailed ) {
            this.disburseSalaries.toggle();
        }

        if ( isDisbursingOrHasFailed || this.props.payrollStatus === PAYROLL_STATUSES.DISBURSED ) {
            this.props.getDisburseSalariesStatus();
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.otpModalShow !== nextProps.otpModalShow ) {
            this.validateDisbursementSalaries.toggle();
        }

        this.setState({ resend_duration: nextProps.otpDetails.resend_duration });
    }

    getTableColumns( isSalpayAdmin ) {
        return [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Disbursement Method',
                accessor: 'disbursement_method',
                minWidth: 400,
                sortable: false
            },
            {
                header: 'Employees',
                accessor: 'employees',
                minWidth: 120,
                sortable: false,
                render: ({ row }) => ( row.employees || '-' )
            },
            {
                header: 'Net Pay',
                accessor: 'net_pay',
                minWidth: 210,
                sortable: false,
                render: ({ row }) => this.formatCurrency( row.net_pay )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 250,
                sortable: false,
                render: ({ row }) => {
                    const isBank = ![ 'cash', 'cheque' ].includes( row.disbursement_method.toLowerCase() );
                    const isSalpay = row.disbursement_method.toLowerCase() === 'salpay';

                    return (
                        <div className="disbursement-actions">
                            <Button
                                id="disbursement-details-button"
                                label="Details"
                                type="neutral"
                                onClick={ () => {
                                    const filter = `attributes.${isBank ? 'bankName' : 'type'}|${row.disbursement_method}`;

                                    this.disbursementMethodDetails.toggle();

                                    this.setState({
                                        disbursement_method_filter: filter,
                                        disbursement_method_detail_page: 1,
                                        is_bank_selected: isBank,
                                        selected_disbursement_method_total_amount: row.net_pay
                                    });
                                    this.props.getDisbursementMethodDetails({
                                        filter,
                                        page: 1,
                                        per_page: this.state.disbursement_method_per_page
                                    });
                                } }
                            />
                            { isBank && [ PAYROLL_STATUSES.CLOSED, PAYROLL_STATUSES.DISBURSED ].includes( this.props.payrollStatus ) && row.supported && [
                                <Button
                                    id="download-bank-advise"
                                    key="download-bank-advise"
                                    label="Bank Advise"
                                    type="neutral"
                                    onClick={ () => {
                                        this.props.downloadBankAdvise({
                                            bankName: row.disbursement_method,
                                            payrollId: this.props.payrollId,
                                            fileType: FILE_TYPE.BANK_ADVISE
                                        });
                                    } }
                                />,
                                <Button
                                    id="download-bank-file"
                                    key="download-bank-file"
                                    label="Bank File"
                                    type="neutral"
                                    onClick={ () => {
                                        this.props.downloadBankFile({
                                            bankName: row.disbursement_method,
                                            payrollId: this.props.payrollId,
                                            fileType: FILE_TYPE.BANK_FILE
                                        });
                                    } }
                                />
                            ] }
                            { isSalpay && [ PAYROLL_STATUSES.CLOSED, PAYROLL_STATUSES.DISBURSED ].includes( this.props.payrollStatus ) && (
                                <Button
                                    id="salpay-disburse-button"
                                    label="Disburse"
                                    type="neutral"
                                    disabled={ !isSalpayAdmin }
                                    onClick={ () => {
                                        if ( !this.props.salpayDisbursementDetails.balance ) {
                                            this.props.getSalpayDisbursementDetails();
                                        }
                                        this.disburseSalaries.toggle();
                                    } }
                                />
                            ) }
                        </div>
                    );
                }
            }
        ];
    }

    getDisbursementDetailColumnConfig() {
        return [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Employee',
                accessor: 'employee_name',
                minWidth: 400,
                sortable: false
            },
            {
                header: 'Account Number',
                accessor: 'account_number',
                minWidth: 160,
                show: this.state.is_bank_selected,
                sortable: false
            },
            {
                header: 'Net Pay',
                accessor: 'net_pay',
                minWidth: 160,
                sortable: false,
                render: ({ row }) => ( `Php ${formatCurrency( row.net_pay )}` )
            }
        ];
    }

    getSalpayDisburseColumnConfig() {
        return [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Employee',
                accessor: 'full_name',
                minWidth: 250,
                sortable: false
            },
            {
                header: 'Account Number',
                accessor: 'account_number',
                minWidth: 220,
                sortable: false
            },
            {
                header: 'Net Pay',
                accessor: 'net_pay',
                minWidth: 160,
                sortable: false,
                render: ({ row }) => `Php ${formatCurrency( row.net_pay )}`
            },
            {
                header: 'Status',
                accessor: 'status',
                sortable: false,
                show: false
            }
        ];
    }

    handleOtpInputChange = ( code ) => {
        this.setState({ otp_code: code });
    }

    /**
     * Formats currency for display
     * @param {Number} amount
     * @returns {String}
     */
    formatCurrency( amount ) {
        return amount ? `Php ${formatCurrency( amount )}` : '-';
    }

    isOtpExpired() {
        if ( this.props.otpDetails && this.props.otpDetails.expiration_date ) {
            const expiryDate = moment( this.props.otpDetails.expiration_date );
            const now = moment();

            return now.isAfter( expiryDate );
        }

        return false;
    }

    renderDisbursementDetailsBody() {
        return (
            <ModalBody>
                <H4>TOTAL AMOUNT:</H4>
                <H4 className="total-amount">{ `Php ${formatCurrency( this.state.selected_disbursement_method_total_amount )}` }</H4>
                <AsyncTable
                    columns={ this.getDisbursementDetailColumnConfig() }
                    data={ this.props.disbursementMethodDetails.employees[ `${this.state.disbursement_method_detail_page}` ] || [] }
                    pagination
                    pages={ Math.ceil( this.props.disbursementMethodDetails.total / this.state.disbursement_method_per_page ) }
                    showPageSizeOptions={ false }
                    loading={ this.props.loadingDisbursementMethodDetails }
                    onPageChange={ ( page ) =>
                        this.setState({ disbursement_method_detail_page: page }, () =>
                            this.props.getDisbursementMethodDetails({
                                filter: this.state.disbursement_method_filter,
                                per_page: this.state.disbursement_method_per_page,
                                page
                            })
                        )
                    }
                />
            </ModalBody>
        );
    }

    renderDisburseSalariesBody() {
        const disburseProgress = this.props.disburseTransferStatus.percent_completed === 100 ? (
            <div className="progress-wrapper">
                DISBURSED: <span className="disbursed">{ this.props.disburseTransferStatus.transfers_completed }</span>
                FAILED: <span className="failed">{ this.props.disburseTransferStatus.transfers_failed }</span>
            </div>
        ) : (
            <div className="progress-wrapper">
                <progress className="progress" max="100" value={ Math.trunc( this.props.disburseTransferStatus.percent_completed || 0 ) }>
                    <ProgressFallback progress={ Math.trunc( this.props.disburseTransferStatus.percent_completed || 0 ) }>
                        <div>
                            <span>{ Math.trunc( this.props.disburseTransferStatus.percent_completed || 0 ) }</span>
                        </div>
                    </ProgressFallback>
                </progress>
            </div>
        );

        const showDisburseProgress = this.props.disbursingSalaries
            || this.props.payrollStatus === PAYROLL_STATUSES.DISBURSED
            || ( this.props.payrollStatus === PAYROLL_STATUSES.CLOSED && this.props.hasFailedDisbursementTransfer );

        return (
            <ModalBody>
                <section>

                    <div>
                        <H4>TOTAL AMOUNT:</H4>
                        <H4 className="total-amount">{ this.formatCurrency( this.props.salpayDisbursementDetails.total_amount ) }</H4>
                        <div className="amount-breakdown">
                            <div className="net-pay">
                                <H6>NET PAY:</H6>
                                <H6>{ this.formatCurrency( this.props.salpayDisbursementDetails.total_net_pay_amount ) }</H6>
                            </div>
                            <div className="fees">
                                <H6>FEES:</H6>
                                <H6>{ this.formatCurrency( this.props.salpayDisbursementDetails.total_fee_amount ) }</H6>
                            </div>
                        </div>
                    </div>

                    <div>
                        <H4>SALPay ACCOUNT BALANCE:</H4>
                        <H4 className="account-balance">{ this.formatCurrency( this.props.salpayDisbursementDetails.balance ) }</H4>
                        { showDisburseProgress && disburseProgress }
                        { this.props.hasFailedDisbursementTransfer && (
                            <div className="failed-disburse">
                                {
                                    this.props.disburseTransferStatus.safe_for_retry
                                        ? 'You may now retry to disburse all failed disbursements.'
                                        : 'The system has detected a failed disbursement. You may retry once the \'Retry\' button is enabled.'
                                }
                            </div>
                        ) }
                    </div>

                </section>
                <Table
                    columns={ this.getSalpayDisburseColumnConfig() }
                    data={ this.props.salpayDisbursementDetails.employees }
                    pagination
                    pages={ Math.ceil( this.props.salpayDisbursementDetails.employees.length / this.state.disbursement_method_per_page ) }
                    pageSize={ this.state.disbursement_method_per_page }
                    showPageSizeOptions={ false }
                    loading={ this.props.salpayDisbursementDetailsLoading }
                />
            </ModalBody>
        );
    }

    renderOtpModalBody() {
        return (
            <OtpModalBody>
                <Otp
                    mobile={ this.props.otpDetails.mobile_number }
                    salpayUserId={ this.props.otpDetails.salpay_user_id }
                    objectId={ this.props.otpDetails.object_id }
                    referenceNumber={ this.props.otpDetails.reference_number }
                    type={ this.props.otpDetails.type }
                    expiryDate={ this.props.otpDetails.expiration_date }
                    resendDuration={ this.isOtpExpired() ? 0 : this.state.resend_duration }
                    error={ this.props.otpErrorDetails.error }
                    triesLeft={ this.props.otpErrorDetails.tries_left }
                    remainingDuration={ this.props.otpErrorDetails.remaining }
                    blocked={ this.props.otpErrorDetails.tries_left === 0 }
                    onChange={ this.handleOtpInputChange }
                    actionLocked={ this.props.otpDetails.object_id === null }
                    resendOtp={ this.requestResendOtp }
                    expired={ this.props.otpErrorDetails.expired }
                    onRef={ ( ref ) => { this.otpRef = ref; } }
                />
            </OtpModalBody>
        );
    }

    closeOtpModal() {
        this.props.setOtpModalShow( false );
        this.props.setOtpModalButtonLoading( false );
        this.props.setOtpErrorDetails({
            error: null,
            tries_left: 0,
            expired: false,
            remaining: 0
        });
    }

    isSalpayAdmin() {
        if ( this.props.companyId && this.props.userInformation ) {
            return salpayAdminService.isSalpayAdmin(
                this.props.companyId,
                this.props.userInformation.salpay_settings
            );
        }
        return false;
    }

    requestResendOtp = () => {
        this.props.requestResendOtp({
            salpay_user_id: this.props.otpDetails.salpay_user_id,
            object_id: this.props.otpDetails.object_id,
            type: this.props.otpDetails.type,
            reference_number: this.props.otpDetails.reference_number
        });
    }

    render() {
        const isDisbursingOrHasFailed = this.props.disbursingSalaries || this.props.hasFailedDisbursementTransfer;
        const disableDisburseButton = ( this.props.salpayDisbursementDetails.balance < this.props.salpayDisbursementDetails.total_amount )
            || this.props.disbursingSalaries
            || this.props.payrollStatus === PAYROLL_STATUSES.DISBURSED
            || ( this.props.hasFailedDisbursementTransfer && !this.props.disburseTransferStatus.safe_for_retry )
            || this.props.modalButtonLoading;

        const overrideRetry = this.props.payrollStatus !== PAYROLL_STATUSES.DISBURSED // not disbursed
            && this.props.hasFailedDisbursementTransfer // has failed
            && this.props.disburseTransferStatus.safe_for_retry; // safe to retry

        const disbursementId = this.props.hasFailedDisbursementTransfer
            ? this.props.disburseTransferStatus.disbursement_id
            : 0;

        const isSalpayAdmin = this.isSalpayAdmin();
        const otpBlocked = this.props.otpErrorDetails.tries_left === 0 || this.props.otpModalButtonLoading;

        return (
            <SummaryStyleWrapper>
                <Modal
                    title="Disbursement Details"
                    size="lg"
                    body={ this.renderDisbursementDetailsBody() }
                    buttons={ [] }
                    onClose={ () => {
                        this.setState({ disbursement_method_filter: '', is_bank_selected: null });
                        this.props.clearDisbursementMethodDetailsEmployees();
                    } }
                    ref={ ( ref ) => { this.disbursementMethodDetails = ref; } }
                />

                <OtpModal
                    title="Authorize Salary Disbursement"
                    showClose={ false }
                    keyboard={ this.state.otp_code.length === 0 }
                    backdrop={ this.state.otp_code.length === 0 || 'static' }
                    buttons={ [
                        {
                            type: 'neutral',
                            label: 'Close',
                            onClick: () => { this.closeOtpModal(); }
                        },
                        {
                            type: 'action',
                            label: 'Confirm',
                            disabled: this.state.otp_code.length !== 4 || otpBlocked,
                            onClick: () => this.props.requestValidateDisbursement({
                                salpay_user_id: this.props.otpDetails.salpay_user_id,
                                object_id: this.props.otpDetails.object_id,
                                type: this.props.otpDetails.type,
                                password: this.state.otp_code
                            })
                        }
                    ] }
                    body={ this.renderOtpModalBody() }
                    ref={ ( ref ) => { this.validateDisbursementSalaries = ref; } }
                />

                <Modal
                    title="Disburse Salaries"
                    size="lg"
                    body={ this.renderDisburseSalariesBody() }
                    showClose={ !isDisbursingOrHasFailed }
                    keyboard={ !isDisbursingOrHasFailed }
                    backdrop={ !isDisbursingOrHasFailed || 'static' }
                    buttons={ [
                        {
                            type: 'neutral',
                            label: 'Close',
                            onClick: () => {
                                this.disburseSalaries.toggle();
                            },
                            disabled: isDisbursingOrHasFailed
                        },
                        {
                            type: 'action',
                            label: this.props.hasFailedDisbursementTransfer
                                ? 'Retry'
                                : 'Disburse Salaries',
                            onClick: () => this.props.requestDisburseSalaries( disbursementId ),
                            disabled: isSalpayAdmin ? ( overrideRetry ? false : disableDisburseButton ) : true
                        }
                    ] }
                    ref={ ( ref ) => { this.disburseSalaries = ref; } }
                />
                <Table
                    id="disbursement-summary-table"
                    data={ this.props.disbursementSummary }
                    columns={ this.getTableColumns( isSalpayAdmin ) }
                    ref={ ( ref ) => { this.disbursementSummaryTable = ref; } }
                />
            </SummaryStyleWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    disbursementSummary: makeSelectDisbursementSummary(),
    loadingDisbursementMethodDetails: makeSelectDisbursementDetailsTableLoading(),
    disbursementMethodDetails: makeSelectDisbursementMethodDetails(),
    salpayDisbursementDetailsLoading: makeSelectSalpayDisbursementDetailsLoading(),
    salpayDisbursementDetails: makeSelectSalpayDisbursementDetails(),
    disbursingSalaries: makeSelectDisbursingSalaries(),
    disburseTransferStatus: makeSelectDisbursementTransferStatus(),
    hasFailedDisbursementTransfer: makeSelectHasFailedDisbursementTransfer(),
    userInformation: makeSelectUserInformationState(),
    modalButtonLoading: makeSelectModalButtonLoading(),
    otpDetails: makeSelectOtpDetails(),
    otpErrorDetails: makeSelectOtpErrorDetails(),
    otpModalShow: makeSelectOtpModalShow(),
    otpModalButtonLoading: makeSelectOtpModalButtonLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        { clearDisbursementMethodDetailsEmployees,
            downloadBankAdvise,
            downloadBankFile,
            setModalButtonLoading,
            setOtpModalButtonLoading,
            setOtpModalShow,
            setOtpErrorDetails },
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( DisbursementSummary );
