import React from 'react';
import get from 'lodash/get';
import has from 'lodash/has';
import { H4, P, SUB } from 'components/Typography';
import AsyncTable from 'components/AsyncTable';
import ToolTip from 'components/ToolTip';
import Input from 'components/Input';
import { formatNumber } from 'utils/functions';

import { PAYROLL_TYPES } from '../constants';

import { Wrapper } from './styles';
import { TIME_ABBREVIATION_FULL_TEXT, ARRANGED_ATTENDANCE_TYPES, NEGATIVE_VALUES, NEGATIVE_STYLE } from './constants';

/**
 * Import section
 */
class PayrollDetail extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        edit: React.PropTypes.bool,
        employees: React.PropTypes.object,
        totalEmployees: React.PropTypes.number,
        loading: React.PropTypes.bool,
        onPayrollDetailsPageChange: React.PropTypes.func,
        payrollType: React.PropTypes.string,
        isAnnualizePayroll: React.PropTypes.bool
    }

    static defaultProps = {
        totalEmployees: 0
    }

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );
        this.state = {
            showFilters: false,
            employees: {},
            page: 1
        };

        this.formatCurrency = this.formatCurrency.bind( this );
        this.updateEmployeeValues = this.updateEmployeeValues.bind( this );
    }

    /**
     * sets data on mount
     */
    componentWillMount() {
        this.setState({ employees: this.props.employees });
    }

    /**
     * updates values when new data is received
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.employees !== this.props.employees
            && ( !this.props.edit || !has( this.props.employees, this.state.page ) )
            && this.setState( ( prevState ) => ({
                employees: {
                    ...prevState.employees,
                    [ prevState.page ]: get( nextProps.employees, prevState.page )
                }
            }) );
    }

    /**
     * onPageChange handler
     * @param {Integer} page - Page number
     */
    onPageChange = ( page ) => (
        this.setState({ page }, () => {
            this.props.onPayrollDetailsPageChange( page );
        })
    )

    /**
     * Generates columns config for table
     * @returns {Array}
     */
    getColumnsConfig() {
        const showAsAdjustedText = this.props.payrollType === PAYROLL_TYPES.FINAL || this.props.isAnnualizePayroll;

        const columns = [
            {
                header: ' ',
                columns: [
                    {
                        header: <span>EMPLOYEE ID</span>,
                        accessor: 'company_employee_id',
                        sortable: true,
                        width: 139,
                        style: { justifyContent: 'center' },
                        filterMethod: ( filter, row ) => (
                            `${row.company_employee_id}`.toLowerCase().indexOf( filter.value.toLowerCase() ) >= 0
                        )
                    },
                    {
                        header: <span>EMPLOYEE NAME</span>,
                        accessor: 'employeeName',
                        sortable: true,
                        width: 280,
                        filterMethod: ( filter, row ) => (
                            row.employeeName.toLowerCase().indexOf( filter.value.toLowerCase() ) >= 0
                        )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="gross_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                    Employee&#39;s total income before taxes and deductions.
                                </ToolTip>
                                <span>GROSS INCOME</span>
                            </span>
                        ),
                        accessor: 'gross',
                        sortable: true,
                        width: 172,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `gross${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'gross', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="taxable_income_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                    Employee&#39;s income after non-taxable income has been deducted.
                                </ToolTip>
                                <span>TAXABLE INCOME</span>
                            </span>
                        ),
                        accessor: 'taxable_income',
                        sortable: true,
                        width: 190,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `taxable_income${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'taxable_income', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="withholding_tax_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                    Employee&#39;s withholding tax computed based on taxable income. This should be a negative value.
                                </ToolTip>
                                WITHHOLDING TAX { showAsAdjustedText ? 'AS ADJUSTED' : '' } {this.props.edit ? <span style={ { color: '#F21108' } }>*</span> : ''}
                            </span>
                        ),
                        accessor: 'tax_amount_due',
                        sortable: true,
                        width: showAsAdjustedText ? 310 : 208,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `withholding_tax${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'withholding_tax', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    }
                ]
            },
            {
                header: ' ',
                columns: [
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="total_deduction_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                    Sum of all deductibles inclusive of withholding tax and employee contributions. This should be a negative value.
                                </ToolTip>
                                DEDUCTIONS {this.props.edit ? <span style={ { color: '#F21108' } }>*</span> : ''}
                            </span>
                        ),
                        accessor: 'total_deduction',
                        sortable: true,
                        width: 170,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `total_deduction${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'total_deduction', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="net_pay_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                    This is your take-home pay after all necessary taxes and deductions.
                                </ToolTip>
                                <span>NET PAY</span>
                            </span>
                        ),
                        accessor: 'net_pay',
                        sortable: true,
                        width: 160,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `net_pay${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'net_pay', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    }
                ]
            }
        ];

        if ( this.props.payrollType !== PAYROLL_TYPES.SPECIAL ) {
            const contributionsColumns = {
                header: '',
                columns: [
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="sss_employee_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                Employee&#39;s mandatory SSS contribution and should be a negative value.
                            </ToolTip>
                            SSS EMPLOYEE {this.props.edit ? <span style={ { color: '#F21108' } }>*</span> : ''}
                            </span>
                    ),
                        accessor: 'sss_employee',
                        sortable: true,
                        width: 179,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `sss_employee${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'sss_employee', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="sss_employee_provident_fund_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                    Employee&#39;s mandatory SSS Provident Fund contribution and should be a negative value.
                            </ToolTip>
                            SSS PROVIDENT EMPLOYEE { this.props.edit ? <span style={ { color: '#F21108' } }>*</span> : '' }
                            </span>
                        ),
                        accessor: 'sss_employee_provident_fund',
                        sortable: true,
                        width: 260,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `sss_employee_provident_fund${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'sss_employee_provident_fund', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="sss_employer_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                Employer&#39;s SSS contribution.
                            </ToolTip>
                                <span>SSS EMPLOYER</span>
                            </span>
                    ),
                        accessor: 'sss_employer',
                        sortable: true,
                        width: 180,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `sss_employer${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'sss_employer', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="sss_employer_provident_fund_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                    Employer&#39;s SSS Provident Fund contribution.
                            </ToolTip>
                                <span>SSS PROVIDENT EMPLOYER</span>
                            </span>
                        ),
                        accessor: 'sss_employer_provident_fund',
                        sortable: true,
                        width: 260,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `sss_employer_provident_fund${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'sss_employer_provident_fund', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="sss_ec_employer_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                Employee&#39;s Compensation contribution paid by employers on behalf of the employees.
                            </ToolTip>
                                <span>SSS EC EMPLOYER</span>
                            </span>
                    ),
                        accessor: 'sss_ec_employer',
                        sortable: true,
                        width: 200,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `sss_ec_employer${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'sss_ec_employer', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="hdmf_employee_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                Employee&#39;s mandatory HDMF (Pag-ibig Fund) contribution and should be a negative value.
                            </ToolTip>
                            HDMF EMPLOYEE {this.props.edit ? <span style={ { color: '#F21108' } }>*</span> : ''}
                            </span>
                    ),
                        accessor: 'hdmf_employee',
                        sortable: true,
                        width: 200,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `hdmf_employee${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'hdmf_employee', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="hdmf_employer_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                Employer&#39;s HDMF (Pag-ibig Fund) contribution.
                            </ToolTip>
                                <span>HDMF EMPLOYER</span>
                            </span>
                    ),
                        accessor: 'hdmf_employer',
                        sortable: true,
                        width: 190,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `hdmf_employer${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'hdmf_employer', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="philhealth_employee_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                Employee&#39;s mandatory PhilHealth contribution and should be a negative value.
                            </ToolTip>
                            PHILHEALTH EMPLOYEE {this.props.edit ? <span style={ { color: '#F21108' } }>*</span> : ''}
                            </span>
                    ),
                        accessor: 'philhealth_employee',
                        sortable: true,
                        width: 250,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `philhealth_employee${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'philhealth_employee', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="philhealth_employer_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                Employer&#39;s PhilHealth contribution.
                            </ToolTip>
                                <span>PHILHEALTH EMPLOYER</span>
                            </span>
                    ),
                        accessor: 'philhealth_employer',
                        sortable: true,
                        width: 240,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `philhealth_employer${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'philhealth_employer', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="total_mandatory_employee_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                Sum of all employees&#39; mandatory government contributions. This should be a negative value.
                            </ToolTip>
                            TOTAL EMPLOYEE {this.props.edit ? <span style={ { color: '#F21108' } }>*</span> : ''}
                            </span>
                    ),
                        accessor: 'total_mandatory_employee',
                        sortable: true,
                        width: 200,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `total_mandatory_employee${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'total_mandatory_employee', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    },
                    {
                        header: () => (
                            <span>
                                <ToolTip
                                    id="total_mandatory_employer_tip"
                                    target={ <i className="fa fa-info-circle" /> }
                                    placement="left"
                                >
                                Sum of all employer&#39;s mandatory government contributions.
                            </ToolTip>
                                <span>TOTAL EMPLOYER</span>
                            </span>
                    ),
                        accessor: 'total_mandatory_employer',
                        sortable: true,
                        width: 190,
                        style: { justifyContent: this.props.edit ? 'center' : 'flex-end' },
                        render: ( row ) => ( this.props.edit ? <Input
                            id={ `total_mandatory_employer${row.row.id}` }
                            value={ Number( row.value ).toFixed( 2 ) }
                            type="number"
                            onChange={ ( value ) => { this.updateEmployeeValues( value, 'total_mandatory_employer', row.index ); } }
                        /> : this.formatCurrency( row.value ) )
                    }
                ]
            };

            columns.splice( 1, 0, contributionsColumns );
        }

        return columns;
    }

    /**
     * Get CSS class for value
    */
    getValueClass( attendanceType ) {
        return ( NEGATIVE_VALUES.includes( attendanceType ) ? NEGATIVE_STYLE : 'value' );
    }

    /**
     * Reset data
     */
    resetData() {
        this.setState({ employees: this.props.employees });
    }

    /**
     * updates employee values in state
     * @param value = new value.
     * @param property = employee property to change.
     * @param index = index of employee to update.
     */
    updateEmployeeValues( value, property, index ) {
        this.setState( ( prevState ) => {
            const employeesByPage = Array.from( prevState.employees[ `${prevState.page}` ]);
            employeesByPage.splice(
                index,
                1,
                {
                    ...prevState.employees[ `${prevState.page}` ][ index ],
                    [ property ]: value
                }
            );

            const employees = Object.assign({}, prevState.employees, { [ `${prevState.page}` ]: employeesByPage });
            return { employees };
        });
    }

     /**
     *  converts a number to currency format.
     * @param value = value to format
     * @returns {string} value in currency format
     */
    formatCurrency( value, negative = false ) {
        const convertedValue = Number( value );
        let formattedValue;
        if ( !isNaN( convertedValue ) ) {
            const currency = Number( convertedValue ).toFixed( 2 ).replace( /\d(?=(\d{3})+\.)/g, '$&,' );
            if ( negative ) {
                formattedValue = `PHP (${currency})`;
            } else {
                formattedValue = `PHP ${currency}`;
            }
        } else {
            formattedValue = 'Invalid Amount';
        }
        return convertedValue < 0 ? <span style={ { color: '#F21108' } }>{`(${formattedValue})`}</span> : formattedValue;
    }

    /**
     * component render method
     */
    render() {
        const NotAvailable = <P style={ { fontWeight: '300', color: '#666', margin: '0', fontStyle: 'italic' } } >Not Available</P>;

        return (
            <Wrapper>
                <AsyncTable
                    data={ get( this.state.employees, this.state.page, []) }
                    columns={ this.getColumnsConfig() }
                    showFilters={ this.state.showFilters }
                    pagination={ this.props.totalEmployees > 10 }
                    pages={ Math.ceil( this.props.totalEmployees / 10 ) }
                    showPageSizeOptions={ false }
                    loading={ this.props.loading }
                    onPageChange={ this.onPageChange }
                    SubComponent={ ( row ) => {
                        const attendanceKeys = [...Object.keys( row.row.items.attendance || {})];
                        const allowanceKeys = [...Object.keys( row.row.items.other_incomes.ALLOWANCE || {})];
                        const bonusKeys = [...Object.keys( row.row.items.other_incomes.BONUS || {})];
                        const commissionKeys = [...Object.keys( row.row.items.other_incomes.COMMISSION || {})];
                        const loanAmortizationKeys = [...Object.keys( row.row.items.loan_amortizations || {})];
                        const adjustmentKeys = [...Object.keys( row.row.items.adjustments || {})];
                        const deminimisKeys = [...Object.keys( row.row.items.deminimis || {})];
                        const leaveConversion = get( row.row.items, 'leave_conversion', {});

                        const hasAttendance = attendanceKeys.length > 0;
                        const hasBasicPay = Object.prototype.hasOwnProperty.call( row.row, 'full_pay' ) && Object.prototype.hasOwnProperty.call( row.row, 'total_scheduled_hours' );
                        const hasDeduction = row.row.items.deductions && row.row.items.deductions.length > 0;
                        const hasUnpaidLeave = Object.prototype.hasOwnProperty.call( row.row.items.leaves, 'UNPAID_LEAVE' ) && Object.keys( row.row.items.leaves.UNPAID_LEAVE ).length > 0;
                        const hasAllowance = allowanceKeys.length > 0 && allowanceKeys.some( ( allowanceType ) => row.row.items.other_incomes.ALLOWANCE[ allowanceType ].length > 0 );
                        const hasBonus = bonusKeys.length > 0 && bonusKeys.some( ( bonusType ) => row.row.items.other_incomes.BONUS[ bonusType ].length > 0 );
                        const hasCommission = commissionKeys.length > 0 && commissionKeys.some( ( commissionType ) => row.row.items.other_incomes.COMMISSION[ commissionType ].length > 0 );
                        const hasLoanAmortization = loanAmortizationKeys.length > 0 && loanAmortizationKeys.some( ( loanType ) => row.row.items.loan_amortizations[ loanType ].length > 0 );
                        const hasAdjustment = adjustmentKeys.length > 0 && adjustmentKeys.some( ( adjustmentType ) => row.row.items.adjustments[ adjustmentType ].length > 0 );
                        const hasLoansToCredit = row.row.items.loans_to_credit && row.row.items.loans_to_credit.length > 0;
                        const hasNonTaxableDeminimis = deminimisKeys.length > 0 && row.row.items.deminimis.NON_TAXABLE.length > 0;
                        const hasReduceTaxableDeminimis = deminimisKeys.length > 0 && row.row.items.deminimis.REDUCE_TAXABLE.length > 0;
                        const hasLeaveConversion = Object.prototype.hasOwnProperty.call( row.row.items, 'leave_conversion' ) && Object.keys( leaveConversion ).length > 0;

                        return (
                            <section className="details">
                                <H4>BASIC INFO</H4>
                                <hr />
                                <div className="category">
                                    {
                                        row.row.tax_status && (
                                            <div className="entry">
                                                <div className="line">
                                                    <div className="label">TAX STATUS:</div>
                                                    <div className="value">{ row.row.tax_status || NotAvailable }</div>
                                                </div>
                                            </div>
                                        )
                                    }
                                    {
                                        row.row.tax_type && (
                                            <div className="entry">
                                                <div className="line">
                                                    <div className="label">TAX TYPE:</div>
                                                    <div className="value">{ row.row.tax_type || NotAvailable }</div>
                                                </div>
                                            </div>
                                        )
                                    }
                                    {
                                        row.row.rank_name && (
                                            <div className="entry">
                                                <div className="line">
                                                    <div className="label">RANK:</div>
                                                    <div className="value">{ row.row.rank_name || NotAvailable }</div>
                                                </div>
                                            </div>
                                        )
                                    }
                                    {
                                        row.row.department_name && (
                                            <div className="entry">
                                                <div className="line">
                                                    <div className="label">DEPARTMENT:</div>
                                                    <div className="value">{ row.row.department_name || NotAvailable }</div>
                                                </div>
                                            </div>
                                        )
                                    }
                                    {
                                        row.row.cost_center_name && (
                                            <div className="entry">
                                                <div className="line">
                                                    <div className="label">COST CENTER:</div>
                                                    <div className="value">{ row.row.cost_center_name || NotAvailable }</div>
                                                </div>
                                            </div>
                                        )
                                    }
                                </div>
                                { this.props.payrollType !== PAYROLL_TYPES.SPECIAL && [
                                    <H4 key="compensation-header">COMPENSATION</H4>,
                                    <hr key="compensation-divider" />,
                                    <div key="compensation-content" className="category">
                                        <div className="entry">
                                            <div className="line">
                                                <div className="label">BASE PAY:</div>
                                                <div className="value">{ this.formatCurrency( row.row.base_pay ) || NotAvailable }</div>
                                            </div>
                                        </div>
                                        <div className="entry">
                                            <div className="line">
                                                <div className="label">PAY RATE TYPE:</div>
                                                <div className="value">{ row.row.base_pay_unit || NotAvailable }</div>
                                            </div>
                                        </div>
                                        <div className="entry">
                                            <div className="line">
                                                <div className="label">MONTHLY INCOME:</div>
                                                <div className="value">{ this.formatCurrency( row.row.monthly_income ) || NotAvailable }</div>
                                            </div>
                                        </div>
                                        <div className="entry">
                                            <div className="line">
                                                <div className="label">DAILY RATE:</div>
                                                <div className="value">{ this.formatCurrency( row.row.daily_rate ) || NotAvailable }</div>
                                            </div>
                                        </div>
                                        <div className="entry">
                                            <div className="line">
                                                <div className="label">HOURLY RATE:</div>
                                                <div className="value">{ this.formatCurrency( row.row.hourly_rate ) || NotAvailable }</div>
                                            </div>
                                        </div>
                                    </div>
                                ] }
                                { ( hasAttendance || hasBasicPay ) && this.props.payrollType !== PAYROLL_TYPES.SPECIAL && [
                                    <H4 key="hours-worked-header">HOURS WORKED</H4>,
                                    <hr key="hours-worked-divider" />,
                                    <div key="hours-worked-content" className="category">
                                        { hasBasicPay && (
                                        <div className="entry">
                                            <div className="header">BASIC PAY</div>
                                            <div className="line">
                                                <div className="label">HOURS:</div>
                                                <div className="value">{ formatNumber( row.row.total_scheduled_hours ) }</div>
                                            </div>
                                            <div className="line">
                                                <div className="label">AMOUNT:</div>
                                                <div className="value">{ this.formatCurrency( row.row.full_pay ) }</div>
                                            </div>
                                        </div>
                                            ) }
                                        { hasUnpaidLeave && (
                                            <div className="entry">
                                                <div className="header">UNPAID LEAVE</div>
                                                <div className="line">
                                                    <div className="label">HOURS:</div>
                                                    <div className="value">{ formatNumber( row.row.items.leaves.UNPAID_LEAVE.total_hours ) }</div>
                                                </div>
                                                <div className="line">
                                                    <div className="label">AMOUNT:</div>
                                                    <div className="negative value">{ this.formatCurrency( row.row.items.leaves.UNPAID_LEAVE.total_amount, true ) }</div>
                                                </div>
                                            </div>
                                        ) }
                                        {
                                                hasAttendance && ARRANGED_ATTENDANCE_TYPES.map( ( attendanceType, idx ) => (
                                                    row.row.items.attendance[ attendanceType ] && (
                                                        <div className="entry" key={ `${attendanceType}${idx}` }>
                                                            <div className="header uppercase">
                                                                <ToolTip id={ `${attendanceType.split( '+' ).join( '' )}${row.row.id}` } target={ attendanceType.toUpperCase() } placement="right">{ TIME_ABBREVIATION_FULL_TEXT[ attendanceType.toUpperCase().split( '+' ).join( '' ) ] || 'undefined' }</ToolTip>
                                                            </div>
                                                            <div className="line">
                                                                <div className="label">HOURS:</div>
                                                                <div className="value">{ formatNumber( row.row.items.attendance[ attendanceType ].total_hours ) }</div>
                                                            </div>
                                                            <div className="line">
                                                                <div className="label">AMOUNT:</div>
                                                                <div className={ this.getValueClass( attendanceType ) }>{ this.formatCurrency( row.row.items.attendance[ attendanceType ].total_add_amount || row.row.items.attendance[ attendanceType ].total_amount, NEGATIVE_VALUES.includes( attendanceType ) ) }</div>
                                                            </div>
                                                        </div>
                                                    )
                                                ) )
                                            }
                                    </div>
                                ] }
                                { hasAllowance && <H4>ALLOWANCES</H4> }
                                { hasAllowance && <hr /> }
                                {
                                    hasAllowance && (
                                        <div className="category">
                                            {
                                                allowanceKeys.map( ( allowanceType, idx ) => (
                                                    row.row.items.other_incomes.ALLOWANCE[ allowanceType ].length > 0 && (
                                                        <div className="entry has-subsection" key={ `${allowanceType}${idx}` }>
                                                            <div className="header uppercase">{ allowanceType.split( '_' ).join( '-' ) }</div>
                                                            {
                                                                row.row.items.other_incomes.ALLOWANCE[ allowanceType ].map( ({ type, amount }, lineIndex ) => (
                                                                    <div className="line" key={ `${type}${lineIndex}` }>
                                                                        <div className="label uppercase">{ type }</div>
                                                                        <div className="value">{ this.formatCurrency( amount ) }</div>
                                                                    </div>
                                                                ) )
                                                            }
                                                        </div>
                                                    )
                                                ) )
                                            }
                                        </div>
                                    )
                                }
                                { hasLeaveConversion && [
                                    <H4 key="leave-conversion-title">LEAVE CONVERSION</H4>,
                                    <hr key="leave-conversion-hr" />,
                                    <div className="category" >
                                        {
                                            <div className="entry">
                                                <div className="line">
                                                    <div className="label">QUANTITY:</div>
                                                    <div className="value uppercase">
                                                        { `${formatNumber( leaveConversion.unit === 'hours' ? leaveConversion.total_hours : leaveConversion.total_days )} ${leaveConversion.unit}` }
                                                    </div>
                                                </div>
                                                <div className="line">
                                                    <div className="label">AMOUNT:</div>
                                                    <div className="value">{ this.formatCurrency( leaveConversion.total_amount ) }</div>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                ] }
                                { ( hasNonTaxableDeminimis || hasReduceTaxableDeminimis ) && [
                                    <H4 key="de-minimis-title">DE MINIMIS</H4>,
                                    <hr key="de-minimis-hr" />,
                                    <div className="category" key="de-minimis-entries">
                                        {
                                            hasReduceTaxableDeminimis && (
                                                <div className="entry has-subsection">
                                                    <div className="header">TAX EXEMPT</div>
                                                    {
                                                        row.row.items.deminimis.REDUCE_TAXABLE.map( ({ name, amount }, lineIndex ) => (
                                                            <div className="line" id={ `${name}-${lineIndex}` } key={ `${name}${lineIndex}` }>
                                                                <div className="label uppercase truncate">{ name }</div>
                                                                <div className="value stretch">{ this.formatCurrency( amount ) }</div>
                                                            </div>
                                                        ) )
                                                    }
                                                </div>
                                            )
                                        }
                                        {
                                            hasNonTaxableDeminimis && (
                                                <div className="entry has-subsection">
                                                    <div className="header">NON-TAX EXEMPT</div>
                                                    {
                                                        row.row.items.deminimis.NON_TAXABLE.map( ({ name, amount }, lineIndex ) => (
                                                            <div className="line" id={ `${name}-${lineIndex}` } key={ `${name}${lineIndex}` }>
                                                                <div className="label uppercase truncate">{ name }</div>
                                                                <div className="value stretch">{ this.formatCurrency( amount ) }</div>
                                                            </div>
                                                        ) )
                                                    }
                                                </div>
                                            )
                                        }
                                    </div>
                                ] }
                                { hasBonus && <H4>BONUSES</H4> }
                                { hasBonus && <hr /> }
                                {
                                    hasBonus && (
                                        <div className="category">
                                            {
                                                bonusKeys.map( ( bonusType, idx ) => (
                                                    row.row.items.other_incomes.BONUS[ bonusType ].length > 0 && (
                                                        <div className="entry has-subsection" key={ `${bonusType}${idx}` }>
                                                            <div className="header uppercase">{ bonusType.split( '_' ).join( '-' ) }</div>
                                                            {
                                                                row.row.items.other_incomes.BONUS[ bonusType ].map( ({ type, amount }, lineIndex ) => (
                                                                    <div className="line" key={ `${type}${lineIndex}` }>
                                                                        <div className="label uppercase">{ type }</div>
                                                                        <div className="value">{ this.formatCurrency( amount ) }</div>
                                                                    </div>
                                                                ) )
                                                            }
                                                        </div>
                                                    )
                                                ) )
                                            }
                                        </div>
                                    )
                                }
                                { hasCommission && <H4>COMMISSIONS</H4> }
                                { hasCommission && <hr /> }
                                {
                                    hasCommission && (
                                        <div className="category">
                                            {
                                                commissionKeys.map( ( commissionType, idx ) => (
                                                    row.row.items.other_incomes.COMMISSION[ commissionType ].length > 0 && (
                                                        <div className="entry has-subsection" key={ `${commissionType}${idx}` }>
                                                            <div className="header uppercase">{ commissionType.split( '_' ).join( '-' ) }</div>
                                                            {
                                                                row.row.items.other_incomes.COMMISSION[ commissionType ].map( ({ type, amount }, lineIndex ) => (
                                                                    <div className="line" key={ `${type}${lineIndex}` }>
                                                                        <div className="label uppercase">{ type }</div>
                                                                        <div className="value">{ this.formatCurrency( amount ) }</div>
                                                                    </div>
                                                                ) )
                                                            }
                                                        </div>
                                                    )
                                                ) )
                                            }
                                        </div>
                                    )
                                }
                                { hasDeduction && <H4>DEDUCTIONS</H4> }
                                { hasDeduction && <hr /> }
                                {
                                    hasDeduction && (
                                        <div className="category">
                                            {
                                                row.row.items.deductions.map( ( deduction, idx ) => (
                                                    <div className="entry" key={ `${deduction.type}${idx}` }>
                                                        <div className="line">
                                                            <div className="label uppercase">{ deduction.type }</div>
                                                            <div className="value">{ this.formatCurrency( deduction.amount ) }</div>
                                                        </div>
                                                    </div>
                                                ) )
                                            }
                                        </div>
                                    )
                                }
                                { ( hasLoanAmortization || hasLoansToCredit ) && <H4>LOANS</H4> }
                                { ( hasLoanAmortization || hasLoansToCredit ) && <hr /> }
                                {
                                    ( hasLoanAmortization || hasLoansToCredit ) && (
                                        <div className="category">
                                            {
                                                loanAmortizationKeys.map( ( loanType, idx ) => (
                                                    row.row.items.loan_amortizations[ loanType ].length > 0 && (
                                                        <div className="entry has-subsection" key={ `${loanType}${idx}` }>
                                                            <div className="header uppercase">{ loanType.split( '_' ).join( ' ' ) }</div>
                                                            {
                                                                row.row.items.loan_amortizations[ loanType ].map( ({ type, amount, meta }) => {
                                                                    let id;
                                                                    try {
                                                                        const parsedMeta = JSON.parse( meta );
                                                                        id = parsedMeta.id;
                                                                    } catch ( error ) {
                                                                        id = type;
                                                                    }

                                                                    return (
                                                                        <div className="line" id={ `loan-amortization-${id}` } key={ `${type}-${id}` }>
                                                                            <div className="label uppercase">{ type }</div>
                                                                            <div className="value">{ this.formatCurrency( amount ) }</div>
                                                                        </div>
                                                                    );
                                                                })
                                                            }
                                                        </div>
                                                    )
                                                ) )
                                            }
                                            { hasLoansToCredit && (
                                                <div className="entry has-subsection">
                                                    <div className="header">LOAN CREDIT</div>
                                                        {
                                                            row.row.items.loans_to_credit.map( ({ type, amount, meta }) => {
                                                                let id;
                                                                try {
                                                                    const parsedMeta = JSON.parse( meta );
                                                                    id = parsedMeta.id;
                                                                } catch ( error ) {
                                                                    id = type;
                                                                }

                                                                return (
                                                                    <div className="line" id={ `loans-to-credit-${id}` } key={ `${type}${id}` }>
                                                                        <div className="label uppercase">{ type }</div>
                                                                        <div className="value">{ this.formatCurrency( amount ) }</div>
                                                                    </div>
                                                                );
                                                            })
                                                        }
                                                </div>
                                            ) }
                                        </div>
                                    )
                                }
                                { hasAdjustment && <H4>ADJUSTMENTS</H4> }
                                { hasAdjustment && <hr /> }
                                {
                                    hasAdjustment && (
                                        <div className="category">
                                            {
                                                adjustmentKeys.map( ( adjustmentType ) => (
                                                    row.row.items.adjustments[ adjustmentType ].length > 0 && (
                                                        <div className="entry has-subsection" key={ adjustmentType }>
                                                            <div className="header uppercase">{ adjustmentType.split( '_' ).join( ' ' ) }</div>
                                                            {
                                                                row.row.items.adjustments[ adjustmentType ].map( ({ reason, amount }, lineIndex ) => (
                                                                    <div className="line" key={ `${adjustmentType}${lineIndex}` }>
                                                                        <div className="label uppercase truncate">{ reason }</div>
                                                                        <div className="value stretch">{ this.formatCurrency( amount ) }</div>
                                                                    </div>
                                                                ) )
                                                            }
                                                        </div>
                                                    )
                                                ) )
                                            }
                                        </div>
                                    )
                                }
                            </section>
                        );
                    } }
                />
                {
                    this.props.edit && (
                        <SUB style={ { textAlign: 'left' } }>
                            <br />
                            NOTE:
                            <br /> - Positive values will be converted and saved as negative amounts for columns marked with red asterisks (<span style={ { color: '#F21108', fontWeight: '900' } } >*</span>)
                            <br /> - All fields are <strong>REQUIRED</strong>.
                        </SUB>
                    )
                }
            </Wrapper>
        );
    }
}

export default PayrollDetail;
