import { take, call, put, cancel, select } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { browserHistory } from 'utils/BrowserHistory';
import { Fetch } from 'utils/request';
import get from 'lodash/get';
import has from 'lodash/has';

import {
    INITIAL_DATA,
    SET_PAYROLL_DATA,
    SET_LOADING,
    DELETE_PAYROLL,
    SET_DELETE,
    CLOSE_PAYROLL,
    SET_CLOSE,
    OPEN_PAYROLL,
    SET_OPEN,
    UPDATE_PAYROLL,
    SET_UPDATE,
    GENERATE_PAYSLIP,
    SET_PAYSLIP,
    SEND_PAYSLIP,
    NOTIFICATION_SAGA,
    NOTIFICATION,
    REGENERATE_PAYROLL,
    DOWNLOAD_BANK_ADVISE,
    DOWNLOAD_BANK_FILE,
    DOWNLOAD_PAYROLL_REGISTER,
    SET_PAYROLL_EMPLOYEES,
    SET_PAYROLL_DETAILS_TABLE_PAGE_NUMBER,
    STORED_PAYROLL_JOBS_KEY,
    GET_DISBURSMENT_METHOD_DETAILS,
    PAYROLL_STATUSES,
    GET_SALPAY_DISBURSEMENT_DETAILS,
    REQUEST_DISBURSE_SALARIES_SALPAY,
    GET_DISBURSEMENT_TRANSFER_STATUS,
    UPDATE_SPECIAL_PAY_RUN_POSTING_DATE,
    SET_HAS_FAILED_DISBURSEMENT_TRANSFER,
    REQUEST_VALIDATE_DISBURSEMENT_SALPAY,
    REQUEST_RESEND_OTP
} from './constants';
import {
    setPayrollDetailsTableLoading,
    setDisbursementSummary,
    setGeneratePayslipLoading,
    setLoadingDisbursementMethodDetails,
    setDisbursementMethodDetailsTotalEmployees,
    setDisbursementMethodDetailsEmployees,
    setHasPayrollRegister,
    setSalpayDisbursementDetailsLoading,
    setSalpayDisbursementDetails,
    setDisbursementTransferStatus,
    setDisbursingSalaries,
    setUpdatingPostingDate,
    setModalButtonLoading,
    setOtpModalButtonLoading,
    setOtpModalShow,
    setOtpDetails,
    setOtpRemainingDuration,
    setOtpError,
    setOtpTriesLeft,
    setOtpExpired
} from './actions';
import {
    makeSelectData,
    makeSelectDisbursementMethodDetails
} from './selectors';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';
import { fetchDeleteJobStatus } from '../List/sagas';

/**
 * Fetch initial payroll data from server
 */
export function* initializePayrollData({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: {
                message: 'Loading data. Please wait...',
                show: true
            }
        });

        yield call( getPayslipStatus, payload );
        yield call( getPayrollData, payload );
    } catch ( error ) {
        yield call( notifyError, error );
        yield put({
            type: SET_LOADING,
            payload: {
                title: '',
                message: '',
                show: false
            }
        });
    }
}

/**
 * Individual exports for testing
 */
export function* getPayrollData( id, page = 1 ) {
    try {
        window.timerComponent.handlePause();
        const payrollDataResponse = yield call( Fetch, `/payroll/${id}?with_employees=true&page=${page}&per_page=10`, { method: 'GET' });
        window.timerComponent.handleResume();
        const payrollStatus = payrollDataResponse.status;
        if ( payrollStatus === PAYROLL_STATUSES.REOPENING || payrollStatus === PAYROLL_STATUSES.CLOSING ) {
            const status = payrollStatus === PAYROLL_STATUSES.REOPENING ? 'Opening' : 'Closing';
            yield put({
                type: SET_LOADING,
                payload: {
                    title: `${status} your payroll.`,
                    message: 'Please wait...',
                    show: true
                }
            });
            yield call( delay, 3000 );
            yield call( getPayrollData, id );
            return;
        }

        let payrollJobs;
        switch ( payrollStatus ) {
            case PAYROLL_STATUSES.DRAFT:
                payrollJobs = {
                    id,
                    jobs: payrollDataResponse.jobs,
                    payroll_group_name: payrollDataResponse.payroll_group_name,
                    payroll_group_id: payrollDataResponse.payroll_group_id,
                    start_date: payrollDataResponse.start_date,
                    end_date: payrollDataResponse.end_date,
                    type: payrollDataResponse.type,
                    annualize: payrollDataResponse.annualize,
                    employees: payrollDataResponse.employees
                };

                localStorage.setItem( STORED_PAYROLL_JOBS_KEY, JSON.stringify( payrollJobs ) );
                yield call( browserHistory.push, '/payroll/generate' );

                break;
            case PAYROLL_STATUSES.CALCULATING:
            case PAYROLL_STATUSES.RECALCULATING:
                yield put({
                    type: SET_LOADING,
                    payload: {
                        title: `${payrollDataResponse.status === PAYROLL_STATUSES.RECALCULATING ? 'Re-calculating' : 'Calculating'} your payroll. Please wait...`,
                        message: '',
                        show: true
                    }
                });
                yield call( delay, 2000 );
                yield call( getPayrollData, id );

                break;
            default: {
                const { employees, ...payrollData } = payrollDataResponse;
                const isDisbursingOrDisbursed = [ PAYROLL_STATUSES.DISBURSING, PAYROLL_STATUSES.DISBURSED ].includes( payrollDataResponse.status );

                const [
                    disbursementSummary,
                    payrollGroup,
                    payrollRegister
                ] = yield [
                    call( Fetch, `/payroll/${id}/disbursement_summary`, { method: 'GET' }),
                    payrollData.payroll_group_id && call( Fetch, `/philippine/payroll_group/${payrollData.payroll_group_id}`, { method: 'GET' }),
                    call( Fetch, `/payroll_register/${id}/has_payroll_register`, { method: 'GET' })
                ];

                let salpayDisbursementData;
                let balance;
                let disbursementStatus;
                if ( isDisbursingOrDisbursed || payrollDataResponse.status === PAYROLL_STATUSES.CLOSED ) {
                    try {
                        salpayDisbursementData = yield call( Fetch, `/salpay/companies/${payrollData.company_id}/payrolls/${id}/employees`, { method: 'GET' });
                        balance = yield call( Fetch, `/salpay/companies/${payrollData.company_id}/remaining-balance`, { method: 'GET' });
                        disbursementStatus = yield call( Fetch, `/salpay/companies/${payrollData.company_id}/payrolls/${id}/disbursements`, { method: 'GET' });
                    } catch ( error ) {
                        salpayDisbursementData = null;
                        balance = null;
                        disbursementStatus = null;
                    }
                }

                if ( payrollDataResponse.status === PAYROLL_STATUSES.CLOSED ) {
                    yield call( checkForFailedDisbursementTransfer, {
                        payload: {
                            company_id: payrollData.company_id,
                            payroll_id: id
                        }
                    });
                }

                let hasGapLoanCandidates = 0;
                if ( payrollData.payroll_group_id ) {
                    payrollData.enforceGapLoans = payrollGroup.enforce_gap_loans;
                    if ( payrollData.enforceGapLoans ) {
                        hasGapLoanCandidates = yield call( Fetch, `/payroll/${id}/has_gap_loan_candidates`, { method: 'GET' });
                    }
                }

                payrollData.hasGapLoanCandidates = !!hasGapLoanCandidates; // Backend returns either 0 or 1

                yield [
                    put({
                        type: SET_PAYROLL_DATA,
                        payload: payrollData
                    }),
                    put({
                        type: SET_PAYROLL_EMPLOYEES,
                        payload: {
                            page,
                            employees
                        }
                    }),
                    put( setDisbursementSummary( disbursementSummary.data ) ),
                    put( setHasPayrollRegister( payrollRegister.has_payroll_register ) )
                ];

                if ( salpayDisbursementData && balance && disbursementStatus ) {
                    yield [
                        put( setSalpayDisbursementDetails({
                            balance: parseFloat( balance.remaining_balance ),
                            ...salpayDisbursementData
                        }) ),
                        put( setDisbursementTransferStatus( disbursementStatus ) )
                    ];
                }

                yield put({
                    type: SET_LOADING,
                    payload: {
                        title: '',
                        message: '',
                        show: false
                    }
                });
            }
        }
    } catch ( error ) {
        yield call( notifyError, error );
        yield call( browserHistory.push, '/payroll' );
    }
}

/**
 * Individual exports for testing
 */
export function* deletePayroll({ payload }) {
    try {
        yield put({
            type: SET_LOADING,
            payload: {
                title: 'Deleting payroll. Please wait...',
                message: '',
                show: true
            }
        });
        yield put({
            type: SET_DELETE,
            payload: true
        });

        const response = yield call( Fetch, '/payrolls/delete_payroll_job', {
            method: 'DELETE',
            data: {
                id: [payload]
            }
        });
        yield call( fetchDeleteJobStatus, response.jobId );
        localStorage.setItem( 'deleted_a_payroll', true );
        yield call( delay, 3000 );
        yield call( browserHistory.push, '/payroll' );
        yield put({
            type: SET_DELETE,
            payload: false
        });
    } catch ( error ) {
        yield put({
            type: SET_DELETE,
            payload: false
        });

        yield call( notifyError, error );
    } finally {
        yield put({
            type: SET_LOADING,
            payload: {
                title: '',
                message: '',
                show: false
            }
        });
    }
}

/**
 * Individual exports for testing
 */
export function* closePayroll({ payload }) {
    try {
        yield put({
            type: SET_CLOSE,
            payload: true
        });

        yield call( Fetch, `/payroll/${payload.id}/close`, {
            method: 'POST',
            data: {
                skipEnforceGapLoans: payload.skipEnforceGapLoans
            }
        });
        yield call( getPayrollData, payload.id );

        yield [
            call( notifyUser, {
                title: 'Success',
                message: 'Payroll successfully closed',
                show: true,
                type: 'success'
            }),
            put({
                type: SET_CLOSE,
                payload: false
            })
        ];
    } catch ( error ) {
        yield call( notifyError, error );
        yield put({
            type: SET_CLOSE,
            payload: false
        });
    }
}

/**
 * Individual exports for testing
 */
export function* openPayroll({ payload }) {
    try {
        yield put({
            type: SET_OPEN,
            payload: true
        });

        yield call( Fetch, `/payroll/${payload}/open`, { method: 'POST' });

        yield call( getPayrollData, payload );

        yield [
            call( notifyUser, {
                title: 'Success',
                message: 'Payroll successfully opened',
                show: true,
                type: 'success'
            }),
            put({
                type: SET_OPEN,
                payload: false
            })
        ];
    } catch ( error ) {
        yield call( notifyError, error );
        yield put({
            type: SET_OPEN,
            payload: false
        });
    }
}

/**
 * Individual exports for testing
 */
export function* editPayroll({ payload }) {
    try {
        const { id, data, page } = payload;
        yield put({
            type: SET_UPDATE,
            payload: true
        });

        yield call( Fetch, `/payroll/${id}`, {
            method: 'PATCH',
            data
        });

        yield [
            call( notifyUser, {
                title: 'Success',
                message: 'Payroll successfully updated.',
                show: true,
                type: 'success'
            }),
            put({
                type: SET_UPDATE,
                payload: false
            }),
            call( getPayrollData, id, page )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
        yield put({
            type: SET_UPDATE,
            payload: false
        });
    }
}

/**
 * Checks whether payroll has generated payslips
 */
export function* getPayslipStatus( id ) {
    try {
        const response = yield call( Fetch, `/payroll/${id}/has_payslips`, { method: 'GET' });
        yield put({
            type: SET_PAYSLIP,
            payload: response.has_payslips
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Generates payslip for current payroll
 */
export function* generatePayslip({ payload }) {
    try {
        const { id } = payload;
        yield put( setGeneratePayslipLoading( true ) );

        yield call( Fetch, `/payroll/${id}/payslips/generate`, { method: 'GET' });
        yield call( checkGeneratePayslipStatus, payload );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setGeneratePayslipLoading( false ) );
    }
}

/**
 * Checks payslip generation every 2 second
 * @param {*} id The payroll id
 */
export function* checkGeneratePayslipStatus( payload ) {
    try {
        const { id } = payload;

        yield call( delay, 2000 );
        const response = yield call( Fetch, `/payroll/${id}/job/payslip?include=errors,results`, { method: 'GET' });
        if ( response.data.attributes.status === 'PENDING' ) {
            yield call( checkGeneratePayslipStatus, payload );
        } else if ( response.data.relationships.results.data !== null ) {
            yield call( getPayslipStatus, id );
            yield call( notifyUser, {
                show: true,
                title: 'Success',
                message: 'Payslip generation complete. You may now view your company payslips.',
                type: 'success'
            });
        } else {
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: 'Payslip generation failed. Please try again later.',
                type: 'error'
            });
        }
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: emptyNotification
    });
}

/**
 * sends request to send payslip notification to employees
 * @param payload
 */
export function* sendPayslip({ payload }) {
    try {
        yield call( Fetch, `/payroll/${payload}/send_payslips`, { method: 'POST' });
        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                title: ' ',
                message: ' ',
                show: false,
                type: 'success'
            }
        });
        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                show: true,
                title: 'Success',
                message: 'Email notification sent.',
                type: 'success'
            }
        });
        yield call( delay, 5000 );
        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                title: ' ',
                message: ' ',
                show: false,
                type: 'success'
            }
        });
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Sends request to regenerate payroll
 * @param {Integer} payload - Payroll ID
 */
export function* regeneratePayroll({ payload }) {
    yield put({
        type: SET_LOADING,
        payload: {
            title: 'Re-calculating your payroll. Please wait...',
            message: '',
            show: true
        }
    });

    try {
        yield call( Fetch, `/payroll/${payload}/recalculate`, {
            method: 'POST',
            data: {
                saveResultToParent: true
            }
        });

        yield call( getPayrollData, payload );
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Sends request to download payroll register
 * @param {object} payload - Payroll Object
 */
export function* downloadPayrollRegister({ payload }) {
    const { id } = payload;
    try {
        const response = yield call( Fetch, `/download/payroll_register/${id}`, { method: 'GET' });
        const link = document.createElement( 'a' );
        link.href = response.data.uri;

        link.setAttribute( 'download', '' );
        document.body.appendChild( link );
        link.click();

        document.body.removeChild( link );
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Sends request to download Bank Advise or Bank File
 * @param {String} payload.bankName - Bank Name
 * @param {Integer} payload.payrollId - Payroll Id
 * @param {String} payload.fileType - File Type
 */
export function* downloadBankFileAndBankAdvise({ payload: { bankName, payrollId, fileType }}) {
    try {
        const response = yield call( Fetch, `/payroll/${payrollId}/${fileType}`, {
            method: 'POST',
            data: {
                data: {
                    bank: bankName
                }
            }
        });

        const link = document.createElement( 'a' );
        link.href = response.data.uri;

        link.setAttribute( 'download', '' );
        document.body.appendChild( link );
        link.click();

        document.body.removeChild( link );
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Sends request to fetch payroll data by page
 * @param {Integer} payload.id - Payroll ID
 * @param {Integer} payload.page - Page number
 */
export function* fetchPayrollDataByPage({ payload: { id, page }}) {
    const payrollData = yield select( makeSelectData() );

    if ( !Object.prototype.hasOwnProperty.call( payrollData.employees, `${page}` ) ) {
        try {
            yield put( setPayrollDetailsTableLoading( true ) );
            window.timerComponent.handlePause();
            const { employees } = yield call( Fetch, `/payroll/${id}?with_employees=true&page=${page}&per_page=10`, { method: 'GET' });
            window.timerComponent.handleResume();
            yield [
                put({
                    type: SET_PAYROLL_EMPLOYEES,
                    payload: {
                        page,
                        employees
                    }
                })
            ];
        } catch ( error ) {
            window.timerComponent.handleResume();
            yield call( notifyError, error );
        } finally {
            yield put( setPayrollDetailsTableLoading( false ) );
        }
    }
}

/**
 * Sends request to fetch disbursement method details by page
 * @param {Integer} payload.id - Payroll ID
 * @param {String} payload.filter - filter string
 * @param {Integer} payload.page - Page number
 * @param {Integer} payload.per_page - Number of items per page
 */
export function* fetchDisbursementMethodDetails({ payload: { id, filter, page, per_page: perPage }}) {
    const disbursementDetails = yield select( makeSelectDisbursementMethodDetails() );

    if ( !Object.prototype.hasOwnProperty.call( disbursementDetails.employees, `${page}` ) ) {
        try {
            yield put( setLoadingDisbursementMethodDetails( true ) );
            const response = yield call( Fetch, `/payroll/${id}/employee_disbursement_method?filter=${filter}&page=${page}&perPage=${perPage}` );

            yield put( setDisbursementMethodDetailsTotalEmployees( response.total ) );
            yield put( setDisbursementMethodDetailsEmployees({ page, employees: response.data }) );
        } catch ( error ) {
            yield call( notifyError, error );
        } finally {
            yield put( setLoadingDisbursementMethodDetails( false ) );
        }
    }
}

/**
 * Request to get employees for disbursement through SALPay
 * @param {Integer} payload.company_id - Company ID
 * @param {Integer} payload.payroll_id - Payroll ID
 */
export function* getSalpayDisbursementDetails({ payload: { company_id: companyId, payroll_id: payrollId }}) {
    yield put( setSalpayDisbursementDetailsLoading( true ) );

    try {
        const salpayDisbursementData = yield call( Fetch, `/salpay/companies/${companyId}/payrolls/${payrollId}/employees`, { method: 'GET' });
        const balance = yield call( Fetch, `/salpay/companies/${companyId}/remaining-balance`, { method: 'GET' });

        yield put( setSalpayDisbursementDetails({
            balance: parseFloat( balance.remaining_balance ),
            ...salpayDisbursementData
        }) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setSalpayDisbursementDetailsLoading( false ) );
    }
}

/**
 * Request to disburse salaries through SALPay
 * @param {Integer} payload.company_id - Company ID
 * @param {Integer} payload.payroll_id - Payroll ID
 */
export function* requestDisburseSalaries({ payload }) {
    const {
        company_id: companyId,
        payroll_id: payrollId,
        disbursement_id: disbursementId
    } = payload;

    yield put( setModalButtonLoading( true ) );

    try {
        let response;
        if ( disbursementId ) {
            response = yield call( Fetch, `/salpay/companies/${companyId}/payrolls/${payrollId}/disbursements/${disbursementId}`, { method: 'PUT' });
        } else {
            response = yield call( Fetch, `/salpay/companies/${companyId}/payrolls/${payrollId}/disbursements`, { method: 'POST' });
        }

        yield put( setOtpTriesLeft( 3 ) );

        yield put( setOtpDetails({
            salpay_user_id: response.otp_details.user_id,
            object_id: response.otp_details.object_id,
            reference_number: response.otp_details.reference_number,
            type: response.otp_details.type,
            remaining: response.otp_details.expires_in,
            mobile_number: response.otp_details.mobile_number,
            expiration_date: response.otp_details.expiration_date,
            resend_duration: 30
        }) );

        yield put( setOtpModalShow( true ) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setModalButtonLoading( false ) );
    }
}

/**
 * Request to verify disbursment OTP through SALPay
 * @param {number} payload.company_id - Company ID
 * @param {number} payload.payroll_id - Payroll ID
 * @param {number} salpay_user_id - SALPay User ID
 * @param {number} salpay_object_id - SALPay Object ID
 * @param {number} password - OTP
 * @param {String} type - Type
 */
export function* requestValidateDisbursement({ payload }) {
    const {
        company_id: companyId,
        payroll_id: payrollId,
        salpay_user_id,
        object_id,
        type,
        password
    } = payload;

    yield put( setOtpModalButtonLoading( true ) );

    try {
        const status = yield call( Fetch, `/salpay/companies/${companyId}/payrolls/${payrollId}/otp/disbursement/verify`, {
            method: 'POST',
            data: {
                salpay_user_id,
                salpay_object_id: object_id,
                type,
                password
            }
        });

        yield put( setDisbursingSalaries( true ) );
        yield put( setOtpModalShow( false ) );
        yield put( setOtpModalButtonLoading( false ) );

        yield put( setDisbursementTransferStatus( status ) );
        yield call( getPayrollData, payrollId );

        yield call( delay, 3000 );
        yield call( checkDisburseSalariesStatus, { payload });
    } catch ( error ) {
        if ( !has( error, 'response.data.message' ) ) {
            yield call( notifyError, error );
            yield put( setDisbursingSalaries( false ) );
        }

        let errorData;
        try {
            errorData = JSON.parse( error.response.data.message );
            yield put( setOtpError( get( errorData, 'error.message', 'Error' ) ) );

            if ( get( errorData, 'errors.otp.remaining_duration' ) ) {
                yield [
                    put( setOtpTriesLeft( 0 ) ),
                    put( setOtpRemainingDuration( get( errorData, 'errors.otp.remaining_duration', 0 ) ) )
                ];
            } else if ( get( errorData, 'errors.otp.tries_left' ) ) {
                yield put( setOtpTriesLeft( get( errorData, 'errors.otp.tries_left', 0 ) ) );
            } else if ( get( errorData, 'errors.otp.expired' ) ) {
                yield put( setOtpExpired( get( errorData, 'errors.otp.expired', true ) ) );
            } else {
                yield call( notifyError, error );
                yield put( setDisbursingSalaries( false ) );
            }
        } catch ( _error ) {
            yield call( notifyError, error );
            yield put( setDisbursingSalaries( false ) );
        }
    } finally {
        yield put( setOtpModalButtonLoading( false ) );
    }
}

/**
 * Sends request to resend OTP
 * @param {Integer} payload.company_id - Company ID
 * @param {Integer} payload.payroll_id - Payroll ID
 * @param {Integer} payload.salpay_user_id - SALPay Account ID
 * @param {Integer} payload.salpay_object_id - SALPay Object ID
 * @param {String} payload.reference_number - OTP Reference Number
 * @param {String} payload.type - OTP Transaction Type
 */
export function* requestResendOtp({ payload }) {
    const {
        company_id: companyId,
        payroll_id: payrollId,
        salpay_user_id,
        object_id,
        reference_number,
        type
    } = payload;

    yield put( setOtpModalButtonLoading( true ) );

    try {
        const response = yield call( Fetch, `/salpay/companies/${companyId}/payrolls/${payrollId}/otp/disbursement/resend`, {
            method: 'POST',
            data: {
                salpay_user_id,
                salpay_object_id: object_id,
                reference_number,
                type
            }
        });

        yield put( setOtpDetails({
            salpay_user_id: response.otp_details.user_id,
            object_id: response.otp_details.object_id,
            reference_number: response.otp_details.reference_number,
            type: response.otp_details.type,
            remaining: response.otp_details.expires_in,
            mobile_number: response.otp_details.mobile_number,
            expiration_date: response.otp_details.expiration_date,
            resend_duration: 30
        }) );

        yield put( setOtpError( null ) );
    } catch ( error ) {
        if ( !has( error, 'response.data.message' ) ) {
            yield call( notifyError, error );
            yield put( setDisbursingSalaries( false ) );
        }

        let errorData;
        try {
            errorData = JSON.parse( error.response.data.message );
            yield put( setOtpError( get( errorData, 'error.message', 'Error' ) ) );

            if ( get( errorData, 'error.code' ) === 423 ) {
                yield put( setOtpTriesLeft( 0 ) );
            } else {
                yield call( notifyError, error );
            }
        } catch ( _error ) {
            yield call( notifyError, error );
        }
    } finally {
        yield put( setOtpModalButtonLoading( false ) );
    }
}

/**
 * Check status of disburse salaries through SALPay
 * @param {Integer} payload.company_id - Company ID
 * @param {Integer} payload.payroll_id - Payroll ID
 */
export function* checkDisburseSalariesStatus({ payload }) {
    const {
        company_id: companyId,
        payroll_id: payrollId
    } = payload;

    try {
        const disbursement = yield call( Fetch, `/salpay/companies/${companyId}/payrolls/${payrollId}/disbursements`, { method: 'GET' });

        yield put( setDisbursementTransferStatus( disbursement ) );

        const hasFailedTransfer = disbursement.processing_status.transfers_failed > 0;
        yield put({
            type: SET_HAS_FAILED_DISBURSEMENT_TRANSFER,
            payload: hasFailedTransfer
        });

        if ( hasFailedTransfer ) {
            yield put( setDisbursingSalaries( false ) );

            if ( !disbursement.processing_status.safe_for_retry ) {
                yield call( delay, 10000 );
                yield call( checkDisburseSalariesStatus, { payload });
            }
        } else if ( disbursement.processing_status.percent_completed === 100 ) {
            yield call( getPayrollData, payrollId );
            yield put( setDisbursingSalaries( false ) );
        } else {
            yield put( setDisbursingSalaries( true ) );
            yield call( delay, 3000 );
            yield call( checkDisburseSalariesStatus, { payload });
        }
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Sends request to update payroll posting date
 * @param {Integer} payload.id - Payroll ID
 * @param {String} payload.posting_date - Posting date with format YYYY-MM-DD
 */
export function* updateSpecialPayRunPostingDate({ payload }) {
    const {
        id,
        posting_date: postingDate
    } = payload;

    try {
        yield put( setUpdatingPostingDate( true ) );
        yield call( Fetch, `/payroll/${id}`, {
            method: 'PATCH',
            data: {
                posting_date: postingDate
            }
        });

        yield call( closePayroll, { payload: { id }});
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setUpdatingPostingDate( false ) );
    }
}

/**
 * Checks disbursement status for any failed disbursement transfers
 * @param {number} payload.company_id
 * @param {number} payload.payroll_id
 */
export function* checkForFailedDisbursementTransfer({ payload }) {
    try {
        const disbursement = yield call( Fetch, `/salpay/companies/${payload.company_id}/payrolls/${payload.payroll_id}/disbursements` );

        yield put({
            type: SET_HAS_FAILED_DISBURSEMENT_TRANSFER,
            payload: disbursement.processing_status.transfers_failed > 0
        });
    } catch ( error ) {
        yield put({
            type: SET_HAS_FAILED_DISBURSEMENT_TRANSFER,
            payload: false
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializePayrollData );
}

/**
 * Watcher for INITIAL_DATA
 *
 */
export function* watchForInitialData() {
    const watcher = yield takeEvery( INITIAL_DATA, initializePayrollData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForDeletePayroll() {
    const watcher = yield takeEvery( DELETE_PAYROLL, deletePayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForClosePayroll() {
    const watcher = yield takeEvery( CLOSE_PAYROLL, closePayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForOpenPayroll() {
    const watcher = yield takeEvery( OPEN_PAYROLL, openPayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForEditPayroll() {
    const watcher = yield takeEvery( UPDATE_PAYROLL, editPayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GENERATE_PAYSLIP
 */
export function* watchForGeneratePayslip() {
    const watcher = yield takeEvery( GENERATE_PAYSLIP, generatePayslip );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForSendPayslip() {
    const watcher = yield takeEvery( SEND_PAYSLIP, sendPayslip );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REGENERATE_PAYROLL action
 */
export function* watchForRegeneratePayroll() {
    const watcher = yield takeLatest( REGENERATE_PAYROLL, regeneratePayroll );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DOWNLOAD_PAYROLL_REGISTER action
 */
export function* watchForDownloadPayrollRegister() {
    const watcher = yield takeLatest( DOWNLOAD_PAYROLL_REGISTER, downloadPayrollRegister );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DOWNLOAD_BANK_FILE and DOWNLOAD_BANK_ADVISE action
 */
export function* watchFordownloadBankFileAndBankAdvise() {
    const watcher = yield takeLatest([ DOWNLOAD_BANK_FILE, DOWNLOAD_BANK_ADVISE ], downloadBankFileAndBankAdvise );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for SET_PAYROLL_DETAILS_TABLE_PAGE_NUMBER action
 */
export function* watchForSetPayrollDetailsTablePage() {
    const watcher = yield takeLatest( SET_PAYROLL_DETAILS_TABLE_PAGE_NUMBER, fetchPayrollDataByPage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_DISBURSMENT_METHOD_DETAILS action
 */
export function* watchForGetDisbursementMethodDetails() {
    const watcher = yield takeLatest( GET_DISBURSMENT_METHOD_DETAILS, fetchDisbursementMethodDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_SALPAY_DISBURSEMENT_DETAILS action
 */
export function* watchForGetSalpayDisbursementDetails() {
    const watcher = yield takeLatest( GET_SALPAY_DISBURSEMENT_DETAILS, getSalpayDisbursementDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REQUEST_DISBURSE_SALARIES_SALPAY action
 */
export function* watchForRequestDisburseSalaries() {
    const watcher = yield takeLatest( REQUEST_DISBURSE_SALARIES_SALPAY, requestDisburseSalaries );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_DISBURSEMENT_TRANSFER_STATUS action
 */
export function* watchForGetDisbursementTransferStatus() {
    const watcher = yield takeLatest( GET_DISBURSEMENT_TRANSFER_STATUS, checkDisburseSalariesStatus );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for UPDATE_SPECIAL_PAY_RUN_POSTING_DATE action
 */
export function* watchForUpdateSpecialPayRunPostingDate() {
    const watcher = yield takeLatest( UPDATE_SPECIAL_PAY_RUN_POSTING_DATE, updateSpecialPayRunPostingDate );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REQUEST_RESEND_OTP
 */
export function* watchForRequestResendOtp() {
    const watcher = yield takeEvery( REQUEST_RESEND_OTP, requestResendOtp );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REQUEST_VALIDATE_DISBURSEMENT_SALPAY
 */
export function* watchForRequestValidateDisbursement() {
    const watcher = yield takeEvery( REQUEST_VALIDATE_DISBURSEMENT_SALPAY, requestValidateDisbursement );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitialData,
    watchForDeletePayroll,
    watchForClosePayroll,
    watchForOpenPayroll,
    watchForEditPayroll,
    watchForGeneratePayslip,
    watchForSendPayslip,
    watchForNotifyUser,
    watchForSetPayrollDetailsTablePage,
    watchForGetDisbursementMethodDetails,
    watchForRegeneratePayroll,
    watchForDownloadPayrollRegister,
    watchForGetSalpayDisbursementDetails,
    watchForRequestDisburseSalaries,
    watchForGetDisbursementTransferStatus,
    watchForUpdateSpecialPayRunPostingDate,
    watchForReinitializePage,
    watchFordownloadBankFileAndBankAdvise,
    watchForRequestResendOtp,
    watchForRequestValidateDisbursement
];
