import {
    INITIAL_DATA,
    DELETE_PAYROLL,
    CLOSE_PAYROLL,
    UPDATE_PAYROLL,
    OPEN_PAYROLL,
    GENERATE_PAYSLIP,
    SEND_PAYSLIP,
    REGENERATE_PAYROLL,
    DOWNLOAD_BANK_ADVISE,
    DOWNLOAD_BANK_FILE,
    DOWNLOAD_PAYROLL_REGISTER,
    SET_PAYROLL_DETAILS_TABLE_PAGE_NUMBER,
    SET_PAYROLL_DETAILS_TABLE_LOADING,
    SET_DISBURSEMENT_SUMMARY,
    SET_GENERATE_PAYSLIP_LOADING,
    GET_DISBURSMENT_METHOD_DETAILS,
    SET_LOADING_DISBURSMENT_METHOD_DETAILS,
    SET_DISBURSMENT_METHOD_DETAILS_TOTAL_EMPLOYEES,
    SET_DISBURSMENT_METHOD_DETAILS_EMPLOYEES,
    CLEAR_DISBURSMENT_METHOD_DETAILS_EMPLOYEES,
    SET_HAS_PAYROLL_REGISTER,
    GET_SALPAY_DISBURSEMENT_DETAILS,
    SET_SALPAY_DISBURSEMENT_DETAILS,
    SET_SALPAY_DISBURSEMENT_DETAILS_LOADING,
    REQUEST_DISBURSE_SALARIES_SALPAY,
    SET_DISBURSING_SALARIES,
    SET_DISBURSEMENT_TRANSFER_STATUS,
    GET_DISBURSEMENT_TRANSFER_STATUS,
    UPDATE_SPECIAL_PAY_RUN_POSTING_DATE,
    SET_UPDATING_POSTING_DATE,
    REQUEST_VALIDATE_DISBURSEMENT_SALPAY,
    SET_OTP_MODAL_SHOW,
    SET_OTP_DETAILS,
    SET_OTP_ERROR_DETAILS,
    SET_MODAL_BUTTON_LOADING,
    SET_OTP_MODAL_BUTTON_LOADING,
    REQUEST_RESEND_OTP,
    SET_OTP_ERROR,
    SET_OTP_REMAINING_DURATION,
    SET_OTP_TRIES_LEFT,
    SET_OTP_EXPIRED,
    SET_OTP_MOBILE_NUMBER
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Fetch initial payroll data from server
 */
export function initializePayrollData( id ) {
    return {
        type: INITIAL_DATA,
        payload: id
    };
}

/**
 * deletes payroll with given ID
 */
export function deletePayroll( id ) {
    return {
        type: DELETE_PAYROLL,
        payload: id
    };
}

/**
 * closes payroll with given ID
 */
export function closePayroll( id, skipEnforceGapLoans = false ) {
    return {
        type: CLOSE_PAYROLL,
        payload: {
            id,
            skipEnforceGapLoans
        }
    };
}

/**
 * open payroll with given ID
 */
export function openPayroll( id ) {
    return {
        type: OPEN_PAYROLL,
        payload: id
    };
}

/**
 * updates the data of payroll with given id
 * @param {Integer} id - Payroll ID
 * @param {Object} data - Edited data
 * @param {Integer} page - Payroll details table page
 * @returns {Object} action
 */
export function editPayroll( id, data, page ) {
    return {
        type: UPDATE_PAYROLL,
        payload: {
            id,
            data,
            page
        }
    };
}

/**
 * generates payslip data for an open payroll
 */
export function generatePayslip( payload ) {
    return {
        type: GENERATE_PAYSLIP,
        payload
    };
}

/**
 * Set Generate Payslip loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setGeneratePayslipLoading( payload ) {
    return {
        type: SET_GENERATE_PAYSLIP_LOADING,
        payload
    };
}

/**
 * Send email notifications of generated payslips to employees
 */
export function sendPayslips( payload ) {
    return {
        type: SEND_PAYSLIP,
        payload
    };
}

/**
 * Regenerate payroll
 * @param {Integer} payload - Payroll ID
 * @returns {Object} action
 */
export function regeneratePayroll( payload ) {
    return {
        type: REGENERATE_PAYROLL,
        payload
    };
}

/**
 * Download payroll register
 * @param {object} payload - Payroll Object
 * @returns {Object} action
 */
export function downloadPayrollRegister( payload ) {
    return {
        type: DOWNLOAD_PAYROLL_REGISTER,
        payload
    };
}

/**
 * Download Bank Advise
 * @param {String} payload.bankName - Bank Name
 * @param {Integer} payload.payrollId - Payroll Id
 * @param {String} payload.fileType - File Type
 * @returns {Object} action
 */
export function downloadBankAdvise( payload ) {
    return {
        type: DOWNLOAD_BANK_ADVISE,
        payload
    };
}

/**
 * Download Bank File
 * @param {String} payload.bankName - Bank Name
 * @param {Integer} payload.payrollId - Payroll Id
 * @param {String} payload.fileType - File Type
 * @returns {Object} action
 */
export function downloadBankFile( payload ) {
    return {
        type: DOWNLOAD_BANK_FILE,
        payload
    };
}

/**
 * Set Payroll Details table loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setPayrollDetailsTableLoading( payload ) {
    return {
        type: SET_PAYROLL_DETAILS_TABLE_LOADING,
        payload
    };
}

/**
 * Send request to fetch additional Payroll Details by page
 * @param {Integer} payrollId - Payroll ID
 * @param {Integer} page - Page number
 * @returns {Object} action
 */
export function setPayrollDetailsTablePage( payrollId, page ) {
    return {
        type: SET_PAYROLL_DETAILS_TABLE_PAGE_NUMBER,
        payload: {
            id: payrollId,
            page
        }
    };
}

/**
 * Set Disbursement Summary Details
 * @param {Object} payload
 * @returns {Object} action
 */
export function setDisbursementSummary( payload ) {
    return {
        type: SET_DISBURSEMENT_SUMMARY,
        payload
    };
}

/**
 * Get Disbursement Summary Details
 * @param {Integer} payload.id - Payroll ID
 * @param {String} payload.filter - filter string
 * @param {Integer} payload.page - Page number
 * @param {Integer} payload.per_page - Number of items per page
 * @returns {Object} action
 */
export function getDisbursementMethodDetails({ id, filter, page, per_page }) {
    return {
        type: GET_DISBURSMENT_METHOD_DETAILS,
        payload: {
            id,
            filter,
            page,
            per_page
        }
    };
}

/**
 * Set Disbursement Method Details Total Employees
 * @param {Integer} payload
 * @returns {Object} action
 */
export function setDisbursementMethodDetailsTotalEmployees( payload ) {
    return {
        type: SET_DISBURSMENT_METHOD_DETAILS_TOTAL_EMPLOYEES,
        payload
    };
}

/**
 * Set Disbursement Method Details Employees
 * @param {Object} payload
 * @returns {Object} action
 */
export function setDisbursementMethodDetailsEmployees( payload ) {
    return {
        type: SET_DISBURSMENT_METHOD_DETAILS_EMPLOYEES,
        payload
    };
}

/**
 * Set Loading status of Disbursement Method Details table
 * @param {Boolean} payload
 * @returns {Object} action
 */
export function setLoadingDisbursementMethodDetails( payload ) {
    return {
        type: SET_LOADING_DISBURSMENT_METHOD_DETAILS,
        payload
    };
}

/**
 * Clear Disbursement Method Details Employees
 * @returns {Object} action
 */
export function clearDisbursementMethodDetailsEmployees() {
    return {
        type: CLEAR_DISBURSMENT_METHOD_DETAILS_EMPLOYEES
    };
}

/**
 * Set Payroll Register button status
 * @param {Boolean} payload
 * @returns {Object} action
 */
export function setHasPayrollRegister( payload ) {
    return {
        type: SET_HAS_PAYROLL_REGISTER,
        payload
    };
}

/**
 * Request to get employees and account balance for disbursement through SALPay
 * @param {Integer} payload.company_id - Company ID
 * @param {Integer} payload.payroll_id - Payroll ID
 * @returns {Object} action
 */
export function getSalpayDisbursementDetails( payload ) {
    return {
        type: GET_SALPAY_DISBURSEMENT_DETAILS,
        payload
    };
}

/**
 * Sets employees and account balance for disbursement through SALPay
 * @param {Object} payload - Details object
 * @returns {Object} action
 */
export function setSalpayDisbursementDetails( payload ) {
    return {
        type: SET_SALPAY_DISBURSEMENT_DETAILS,
        payload
    };
}

/**
 * Set SALPay Disbursement Modal loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setSalpayDisbursementDetailsLoading( payload ) {
    return {
        type: SET_SALPAY_DISBURSEMENT_DETAILS_LOADING,
        payload
    };
}

/**
 * Request to disburse salaries through SALPay
 * @param {number} payload.company_id - Company ID
 * @param {number} payload.payroll_id - Payroll ID
 * @param {number} [disbursement_id] - Disbursement ID
 * @returns {Object} action
 */
export function requestDisburseSalaries( payload ) {
    return {
        type: REQUEST_DISBURSE_SALARIES_SALPAY,
        payload
    };
}

/**
 * Set disbursing salaries status
 * @param {Boolean} payload - Disbursing status
 * @returns {Object} action
 */
export function setDisbursingSalaries( payload ) {
    return {
        type: SET_DISBURSING_SALARIES,
        payload
    };
}

/**
 * Set status of payroll disbursement transfer
 * @param {Objcet} payload - Status
 */
export function setDisbursementTransferStatus( payload ) {
    return {
        type: SET_DISBURSEMENT_TRANSFER_STATUS,
        payload
    };
}

/**
 * Fetch status of payroll disbursement transfer
 * @param {Integer} payload.company_id - Company ID
 * @param {Integer} payload.payroll_id - Payroll ID
 */
export function getDisburseSalariesStatus( payload ) {
    return {
        type: GET_DISBURSEMENT_TRANSFER_STATUS,
        payload
    };
}

/**
 * Sends request to update payroll posting date
 * @param {Integer} id - Payroll ID
 * @param {String} postingDate - Posting date with format YYYY-MM-DD
 * @returns {Object}
 */
export function updateSpecialPayRunPostingDate( id, postingDate ) {
    return {
        type: UPDATE_SPECIAL_PAY_RUN_POSTING_DATE,
        payload: {
            id,
            posting_date: postingDate
        }
    };
}

/**
 * Set status of updating posting date
 * @param {Boolean} payload - Updating status
 */
export function setUpdatingPostingDate( payload ) {
    return {
        type: SET_UPDATING_POSTING_DATE,
        payload
    };
}

/**
 * resets the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}

/**
 * Sets modal button loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setModalButtonLoading( payload ) {
    return {
        type: SET_MODAL_BUTTON_LOADING,
        payload
    };
}

/**
 * Sets OTP modal button loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object} action
 */
export function setOtpModalButtonLoading( payload ) {
    return {
        type: SET_OTP_MODAL_BUTTON_LOADING,
        payload
    };
}

/**
 * Sends request to resend OTP
 * @param {number} payload.company_id - Company ID
 * @param {number} payload.payroll_id - Payroll ID
 * @param {number} [salpay_user_id] - SALPay User ID
 * @param {number} [salpay_object_id] - SALPay Object ID
 * @param {String} [reference_number] - OTP Reference Number
 * @param {String} [type] - Type
 * @returns {Object} action
 */
export function requestResendOtp( payload ) {
    return {
        type: REQUEST_RESEND_OTP,
        payload
    };
}

/**
 * Clear OTP details
 * @param {Object} payload - OTP Details
 * @returns {Object} action
 */
export function setOtpModalShow( payload ) {
    return {
        type: SET_OTP_MODAL_SHOW,
        payload
    };
}

/**
 * Sets OTP details
 * @param {Object} payload - OTP Details
 * @returns {Object} action
 */
export function setOtpDetails( payload ) {
    return {
        type: SET_OTP_DETAILS,
        payload
    };
}

/**
 * Sets OTP error details
 * @param {Object} payload - OTP Error Details
 * @returns {Object} action
 */
export function setOtpErrorDetails( payload ) {
    return {
        type: SET_OTP_ERROR_DETAILS,
        payload
    };
}

/**
 * Sets OTP error message
 * @param {String} payload - Error message
 * @returns {Object} action
 */
export function setOtpError( payload ) {
    return {
        type: SET_OTP_ERROR,
        payload
    };
}

/**
 * Sets OTP tries left
 * @param {Integer} payload - Tries left
 * @returns {Object} action
 */
export function setOtpTriesLeft( payload ) {
    return {
        type: SET_OTP_TRIES_LEFT,
        payload
    };
}

/**
 * Sets OTP expired
 * @param {Integer} payload - Expired
 * @returns {Object} action
 */
export function setOtpExpired( payload ) {
    return {
        type: SET_OTP_EXPIRED,
        payload
    };
}

/**
 * Sets OTP max tries remaining duration
 * @param {Integer} payload - Remaining duration
 * @returns {Object} action
 */
export function setOtpRemainingDuration( payload ) {
    return {
        type: SET_OTP_REMAINING_DURATION,
        payload
    };
}

/**
 * Sets mobile number used for the OTP
 * @param {String} payload - Mobile number
 * @returns {Object} action
 */
export function setOtpMobileNumber( payload ) {
    return {
        type: SET_OTP_MOBILE_NUMBER,
        payload
    };
}

/**
 * Request to verify disbursment OTP through SALPay
 * @param {number} payload.company_id - Company ID
 * @param {number} payload.payroll_id - Payroll ID
 * @param {number} salpay_user_id - SALPay User ID
 * @param {number} salpay_object_id - SALPay Object ID
 * @param {number} password - OTP
 * @param {String} type - Type
 * @returns {Object} action
 */
export function requestValidateDisbursement( payload ) {
    return {
        type: REQUEST_VALIDATE_DISBURSEMENT_SALPAY,
        payload
    };
}
