import styled from 'styled-components';

export const BlockedWrapper = styled.div`
    width: 100%;

    .image {
        padding-top: 76px;
        display: flex;
        justify-content: center;
        align-items: flex-end;
        margin-bottom: 30px;

        .isvg {
            margin: 0 auto;
            width: 80px;
        }
    }

    .title {
        margin: 0;
        margin-bottom: 10px;
        font-size: 16px;
        font-weight: 500;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: center;
    }

    .message {
        font-size: 14px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.43;
        padding: 0 20px;
        padding-bottom: 70px;
        letter-spacing: normal;
        text-align: center;
        color: #4e4e4e;

        .mobile {
            font-weight: 500;
        }
    }
`;
