import React from 'react';
import moment from 'moment/moment';

import Icon from 'components/Icon';
import { BlockedWrapper } from './styles';

/**
 * Otp Blocked Component.
 */
export default class OtpBlocked extends React.PureComponent {

    static propTypes = {
        mobile: React.PropTypes.string,
        remainingDuration: React.PropTypes.number,
        actionLocked: React.PropTypes.bool
    }

    static defaultProps = {
        mobile: ''
    }

    constructor( props ) {
        super( props );

        this.state = {
            countdown: props.remainingDuration || 300,
            timer: null
        };

        this.updateCountdown = this.updateCountdown.bind( this );
    }

    componentWillMount() {
        this.setState({
            timer: setInterval( this.updateCountdown, 1000 )
        });
    }

    componentWillUnmount() {
        this.state.timer && clearInterval( this.state.timer );
    }

    updateCountdown() {
        const { countdown } = this.state;
        if ( countdown > 0 ) {
            this.setState({ countdown: countdown - 1 });
        } else {
            clearInterval( this.state.timer );
            this.setState({ timer: null });
        }
    }

    /**
    * Component render method
    */
    render() {
        const countdown = moment.duration( this.state.countdown, 'seconds' );
        const { actionLocked } = this.props;
        const remainingTime = `${countdown.minutes()}:${countdown.seconds().toString().padStart( 2, '0' )}`;

        return (
            <BlockedWrapper>
                <div className="image">
                    <Icon name="lock" />
                </div>
                { actionLocked ? (
                    <p className="message">
                        You have reached the maximum number of OTP attempts.
                        <br /><br />
                        Please try again after { remainingTime }
                    </p>
                ) : [
                    <h3 key="title" className="title">Maximum Attempts Reached</h3>,
                    <p key="message" className="message">
                        Maximum retries reached for <span className="mobile">{ this.props.mobile }</span>.
                        <br />
                        For added security, please try again after { remainingTime }
                    </p>
                ] }
            </BlockedWrapper>
        );
    }
}
