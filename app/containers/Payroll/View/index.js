import React from 'react';
import flattenDeep from 'lodash/flattenDeep';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import SnackBar from 'components/SnackBar';
import { H2, H3, SUB } from 'components/Typography';
import SalConfirm from 'components/SalConfirm';
import Button from 'components/Button';
import Modal from 'components/Modal';
import DatePicker from 'components/DatePicker';
import Loader from 'components/Loader';
import { browserHistory } from 'utils/BrowserHistory';
import { isAuthorized } from 'utils/Authorization';
import { PAYROLL_SUBHEADER_ITEMS, DATE_FORMATS } from 'utils/constants';
import { subscriptionService } from 'utils/SubscriptionService';
import { formatDate } from 'utils/functions';
import { auth } from 'utils/AuthService';

import {
    makeSelectData,
    makeSelectHasPayslip,
    makeSelectGeneratePayslipLoading,
    makeSelectLoading,
    makeSelectDeleting,
    makeSelectUpdating,
    makeSelectClosing,
    makeSelectNotification,
    makeSelectPayrollDetailsTableLoading,
    makeSelectHasPayrollRegister,
    makeSelectUpdatingPostingDate
} from './selectors';

import PayrollDetail from './templates/detail';
import PayrollSummary from './templates/summary';
import DisbursementSummary from './templates/disbursementSummary';

import * as viewActions from './actions';

import {
    OuterWrapper,
    Wrapper,
    MessageWrapperStyles,
    ConfirmBodyWrapperStyle,
    StyledLoader,
    ClosePayRunWrapper
} from './styles';

import SubHeader from '../../SubHeader';

import { PAYROLL_STATUSES, PAYROLL_TYPES } from './constants';

/**
 *
 * View
 *
 */
export class View extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        initializePayrollData: React.PropTypes.func,
        deletePayroll: React.PropTypes.func,
        closePayroll: React.PropTypes.func,
        openPayroll: React.PropTypes.func,
        editPayroll: React.PropTypes.func,
        generatePayslip: React.PropTypes.func,
        generatePayslipLoading: React.PropTypes.bool,
        sendPayslips: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        data: React.PropTypes.object,
        hasPayslip: React.PropTypes.bool,
        loading: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool
        }),
        updating: React.PropTypes.bool,
        deleting: React.PropTypes.bool,
        opening: React.PropTypes.bool,
        closing: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        routeParams: React.PropTypes.object,
        products: React.PropTypes.array,
        regeneratePayroll: React.PropTypes.func,
        downloadPayrollRegister: React.PropTypes.func,
        payrollDetailsTableLoading: React.PropTypes.bool,
        setPayrollDetailsTablePage: React.PropTypes.func,
        getDisbursementMethodDetails: React.PropTypes.func,
        hasPayrollRegister: React.PropTypes.bool,
        getSalpayDisbursementDetails: React.PropTypes.func,
        requestDisburseSalaries: React.PropTypes.func,
        requestValidateDisbursement: React.PropTypes.func,
        requestResendOtp: React.PropTypes.func,
        getDisburseSalariesStatus: React.PropTypes.func,
        updateSpecialPayRunPostingDate: React.PropTypes.func,
        updatingPostingDate: React.PropTypes.bool
    }
    /**
     * component constructor
     */
    constructor( props ) {
        super( props );
        this.state = {
            edit: false,
            data: null,
            showWarning: false,
            showDeleteConfirm: false,
            showCloseConfirm: false,
            showOpenConfirm: false,
            showGenerateConfirm: false,
            showGapLoanConfirm: false,
            permissions: {
                open: false,
                close: false,
                delete: false,
                edit: false,
                disburse: false,
                generatePayslip: false,
                viewPayslip: false,
                sendPayslip: false
            },
            special_pay_run_posting_date: null
        };

        this.diff = this.diff.bind( this );
        this.loadingState = this.loadingState.bind( this );
        this.saveEdits = this.saveEdits.bind( this );
    }

    /**
     * fetches permission for payroll actions
     */
    componentWillMount() {
        auth.fetchSalpaySetting();

        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.payroll',
            'open.payroll',
            'close.payroll',
            'delete.payroll',
            'edit.payroll',
            'disburse.payroll',
            'create.payroll',
            'view.payslip',
            'send_payslip.payroll'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.payroll' ];

            if ( authorized ) {
                this.setState({ permissions: Object.assign( this.state.permissions, {
                    open: authorization[ 'open.payroll' ],
                    close: authorization[ 'close.payroll' ],
                    delete: authorization[ 'delete.payroll' ],
                    edit: authorization[ 'edit.payroll' ],
                    disburse: authorization[ 'disburse.payroll' ],
                    generatePayslip: authorization[ 'create.payroll' ],
                    viewPayslip: authorization[ 'view.payslip' ],
                    sendPayslip: authorization[ 'send_payslip.payroll' ]
                }) });
            } else {
                // redirect to 403
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    /**
     * fetches the payroll information from the API
     */
    componentDidMount() {
        this.props.initializePayrollData( this.props.routeParams.id );
    }

    /**
     * handles changes in props
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.data !== this.props.data && this.setState({
            data: nextProps.data,
            showGapLoanConfirm: nextProps.data.hasGapLoanCandidates && nextProps.data.status === PAYROLL_STATUSES.OPEN
        });
        nextProps.updating === false && nextProps.updating !== this.props.updating && this.setState({ edit: false });

        if ( nextProps.closing !== this.props.closing || nextProps.deleting !== this.props.deleting ) {
            this.closeButton && this.closeButton.setState({ disabled: nextProps.closing || nextProps.deleting });
            this.deleteButton && this.deleteButton.setState({ disabled: nextProps.closing || nextProps.deleting });
            this.editButton && this.editButton.setState({ disabled: nextProps.closing || nextProps.deleting });

            nextProps.closing
                && this.props.updatingPostingDate
                && this.closeModalSpecialPayRun
                && this.closeModalSpecialPayRun.toggle();
        }

        nextProps.updating !== this.props.updating && this.saveButton && this.saveButton.setState({ disabled: nextProps.updating });
        nextProps.opening !== this.props.opening && this.openButton && this.openButton.setState({ disabled: nextProps.opening });
    }

    /**
     * Called before component unmounts.
     */
    componentWillUnmount() {
        // Clears payroll data
        this.props.resetStore();
    }

    /**
     * gets the message to display in generate payroll modal
     */
    getGeneratePayrollConfirmMessage() {
        if ( this.props.hasPayslip ) {
            return (
                <span>
                    Payslips have been generated for this payroll.
                    Continuing this action will overwrite the previously generated payslips.
                    <br /><br />
                    Do you want to proceed?
                </span>
            );
        } else if ( this.props.data.status === 'OPEN' ) {
            return (
                <span>
                    This is an OPEN payroll.
                    <br /><br />
                    You are about to generate payslips for an OPEN payroll.
                    Payslips generated from this payroll may not be final as open payrolls may not be disbursed
                </span>
            );
        }
        return false;
    }

    getDisbursementMethodDetails = ( payload ) => {
        this.props.getDisbursementMethodDetails({
            ...payload,
            id: this.props.routeParams.id
        });
    }

    getSalpayDisbursementDetails = () => {
        this.props.getSalpayDisbursementDetails({
            company_id: this.state.data.company_id,
            payroll_id: Number( this.props.routeParams.id )
        });
    }

    getDisburseSalariesStatus = () => {
        this.props.getDisburseSalariesStatus({
            company_id: this.state.data.company_id,
            payroll_id: Number( this.props.routeParams.id )
        });
    }

    requestDisburseSalaries = ( disbursementId = 0 ) => {
        this.props.requestDisburseSalaries({
            company_id: this.state.data.company_id,
            payroll_id: Number( this.props.routeParams.id ),
            disbursement_id: disbursementId
        });
    }

    requestValidateDisbursement = ( payload ) => {
        this.props.requestValidateDisbursement({
            ...payload,
            company_id: this.state.data.company_id,
            payroll_id: Number( this.props.routeParams.id )
        });
    }

    requestResendOtp = ( payload ) => {
        this.props.requestResendOtp({
            ...payload,
            company_id: this.state.data.company_id,
            payroll_id: Number( this.props.routeParams.id )
        });
    }

    /**
     * this renders a loading screen
     * @returns {XML}
     */
    loadingState() {
        const { title, message } = this.props.loading;

        return (
            <MessageWrapperStyles>
                <div>
                    <i className="fa fa-circle-o-notch fa-spin fa-fw" />
                    <div className="description">
                        {
                            title
                            ? ([
                                <H3 key="0">{ title }</H3>,
                                <p key="1">{ message }</p>
                            ])
                            : (
                                <H3>{ message }</H3>
                            )
                        }
                    </div>
                </div>
            </MessageWrapperStyles>
        );
    }

    /**
     * generates payslip for this payroll
     */
    generatePayslip( id ) {
        if ( this.props.hasPayslip || this.props.data.status === 'OPEN' ) {
            this.setState({ showGenerateConfirm: false },
                () => this.setState({ showGenerateConfirm: true })
            );
        } else {
            this.props.generatePayslip({
                id,
                payrollGroupId: this.props.data.payroll_group_id,
                payrollDate: this.props.data.payroll_date
            });
        }
    }

    /**
     * checks difference between two objects
     * @param obj1
     * @param obj2
     * @param exclude = array of properties to exclude in check
     * @returns {Object} = object properties of obj1 that differs from obj2
     */
    diff( obj1, obj2, exclude = []) {
        const isArray = obj1.constructor === Array;
        const r = isArray ? [] : {};
        const props = Object.keys( obj1 );
        props.forEach( ( prop ) => {
            if ( Object.prototype.hasOwnProperty.call( obj1, prop ) && prop !== '__proto__' ) {
                if ( exclude.indexOf( obj1[ prop ]) === -1 ) {
                    if ( !Object.prototype.hasOwnProperty.call( obj2, prop ) ) {
                        if ( isArray ) {
                            r.push( obj1[ prop ]);
                        } else {
                            r[ prop ] = obj1[ prop ];
                        }
                    } else if ( obj1[ prop ] === Object( obj1[ prop ]) ) {
                        const difference = this.diff( obj1[ prop ], obj2[ prop ]);
                        if ( Object.keys( difference ).length > 0 ) {
                            r[ prop ] = difference;
                            if ( difference && !isArray ) {
                                r.id = obj1.id;
                            }
                        }
                    } else if ( obj1[ prop ] !== obj2[ prop ]) {
                        if ( obj1[ prop ] === undefined ) {
                            r[ prop ] = 'undefined';
                        }
                        if ( obj1[ prop ] === null ) {
                            r[ prop ] = null;
                        } else if ( typeof obj1[ prop ] === 'function' ) {
                            r[ prop ] = 'function';
                        } else if ( typeof obj1[ prop ] === 'object' ) {
                            r[ prop ] = 'object';
                        } else if ( isArray ) {
                            r.push( obj1[ prop ]);
                        } else {
                            r[ prop ] = obj1[ prop ];
                            r.id = obj1.id;
                        }
                    }
                }
            }
        });
        return r;
    }

    /**
     * checks changes and sends request to edit payroll.
     */
    saveEdits() {
        const { page, employees } = this.details.state;
        const newData = Object.assign({}, this.summary.state.data, { employees: flattenDeep( Object.values( employees ) ) });
        const diffValues = this.diff( newData, Object.assign({}, this.props.data, { employees: flattenDeep( Object.values( this.props.data.employees ) ) }) );
        if ( Object.keys( diffValues ).length !== 0 ) {
            delete diffValues.id;
            this.props.editPayroll( this.props.routeParams.id, diffValues, page );
        }
    }

    handlePayrollDetailsPageChange = ( pageIndex ) => {
        this.props.setPayrollDetailsTablePage( this.props.routeParams.id, pageIndex );
    }

    renderCloseModal = () => ( this.state.data && this.state.data.type !== PAYROLL_TYPES.REGULAR
        ? (
            <Modal
                title="Close Pay Run"
                showClose={ false }
                body={ (
                    <ClosePayRunWrapper>
                        <div>
                            <p>You are about to close this Pay Run.</p>
                            <p>Enter a posting date to proceed</p>
                            <DatePicker
                                label="Posting Date"
                                dayFormat={ DATE_FORMATS.DISPLAY }
                                required
                                selectedDay={ this.state.special_pay_run_posting_date }
                                onChange={ ( value ) => this.setState({ special_pay_run_posting_date: formatDate( value, DATE_FORMATS.API ) }) }
                                disabled={ this.props.updatingPostingDate }
                            />
                        </div>
                    </ClosePayRunWrapper>
                ) }
                buttons={ [
                    {
                        type: 'action',
                        label: this.props.updatingPostingDate ? <Loader /> : 'Proceed',
                        onClick: () => this.props.updateSpecialPayRunPostingDate( this.props.routeParams.id, this.state.special_pay_run_posting_date ),
                        disabled: !this.state.special_pay_run_posting_date || this.props.updatingPostingDate
                    },
                    {
                        type: 'neutral',
                        label: 'Dismiss',
                        onClick: () => this.closeModalSpecialPayRun.toggle(),
                        disabled: this.props.updatingPostingDate
                    }
                ] }
                onClose={ () => this.setState({ special_pay_run_posting_date: null }) }
                ref={ ( ref ) => { this.closeModalSpecialPayRun = ref; } }
            />
        )
        : (
            <SalConfirm
                onConfirm={ () => { this.props.closePayroll( this.props.routeParams.id, this.state.data.hasGapLoanCandidates ); } }
                body={
                    <div style={ { display: 'flex', padding: '0 20px' } }>
                        <div style={ { display: 'flex', alignSelf: 'center' } } >
                            Are you sure you want to CLOSE this payroll?
                        </div>
                    </div>
                }
                title="Warning!"
                visible={ this.state.showCloseConfirm }
            />
        ) );

    renderGapLoanModal = () => (
        <SalConfirm
            onConfirm={ () => { browserHistory.push( `/payroll/${this.props.routeParams.id}/transaction` ); } }
            body={
                <ConfirmBodyWrapperStyle>
                    <div style={ { display: 'flex', padding: '0 20px', flexDirection: 'column', alignItems: 'center' } }>
                        <div style={ { display: 'flex', alignSelf: 'center' } } >
                            The Net Take Home Pay of some employees in this payroll group fall below the set Minimum Take Home Pay.
                            You have the option to avail of Gap Loan transactions.
                        </div>
                        <div className="close-div">
                            { !this.props.data.enforceGapLoans ? (
                                <Button
                                    id="button-close-payroll"
                                    className="close-button"
                                    label="Close payroll"
                                    type="danger"
                                    onClick={ () => { this.props.closePayroll( this.props.routeParams.id, this.state.data.hasGapLoanCandidates ); } }
                                /> ) : null
                            }
                        </div>
                    </div>
                </ConfirmBodyWrapperStyle>
            }
            confirmText="Go to Gap Loan Transactions"
            cancelText="Dismiss"
            title="Net pays fall below the set Minimum Net Take Home Pay"
            visible={ this.state.showGapLoanConfirm }
            buttonStyle="info"
            backdrop="static"
            keyboard={ false }
        />
    )

    /**
     *
     * View render method
     *
     */
    render() {
        const payrollID = this.props.routeParams.id;
        const hasPayrollData = this.state.data && Object.keys( this.state.data ).length > 0;
        const { OPEN, DRAFT, CLOSED } = PAYROLL_STATUSES;

        return (
            <OuterWrapper>
                <Helmet
                    title="View Payroll"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ () => { this.setState({ edit: true, showWarning: false }); } }
                    body={
                        <div style={ { display: 'flex', padding: '0 20px' } }>
                            <div style={ { fontSize: '50px', color: 'orange' } }><i className="fa fa-exclamation-circle" /></div>
                            <div style={ { paddingLeft: '20px', display: 'flex', alignSelf: 'center' } } >Editing output here will override the payroll computation and can result in inconsistent values. We recommend instead editing the values of your time and attendance sheet and Employee 201 files.</div>
                        </div>
                    }
                    title="Warning!"
                    visible={ this.state.showWarning }
                />
                <SalConfirm
                    onConfirm={ () => { this.props.deletePayroll( payrollID ); } }
                    body={
                        <div style={ { display: 'flex', padding: '0 20px' } }>
                            <div style={ { fontSize: '50px', color: 'orange' } }><i className="fa fa-exclamation" /></div>
                            <div style={ { paddingLeft: '20px', alignSelf: 'center' } } >
                                Are you sure you want to DELETE this payroll?
                                <br />
                                <SUB>This cannot be undone.</SUB>
                            </div>
                        </div>
                    }
                    title="Warning!"
                    buttonStyle="danger"
                    confirmText="DELETE"
                    visible={ this.state.showDeleteConfirm }
                />
                { this.renderCloseModal() }
                { this.props.data.hasGapLoanCandidates && this.renderGapLoanModal() }
                <SalConfirm
                    onConfirm={ () => { this.props.openPayroll( payrollID ); } }
                    body={
                        <div style={ { display: 'flex', padding: '0 20px' } }>
                            <div style={ { display: 'flex', alignSelf: 'center' } } >
                                Are you sure you want to OPEN this payroll?
                            </div>
                        </div>
                    }
                    title="Warning!"
                    visible={ this.state.showOpenConfirm }
                />
                <SalConfirm
                    onConfirm={ () => this.props.generatePayslip({
                        id: payrollID,
                        payrollGroupId: this.props.data.payroll_group_id,
                        payrollDate: this.props.data.payroll_date
                    }) }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="icon"><i className="fa fa-exclamation-circle" /></div>
                            <div className="message">
                                { this.getGeneratePayrollConfirmMessage() }
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ this.state.showGenerateConfirm }
                    confirmText="Generate Payslips"
                />

                {
                    this.props.loading.show ? this.loadingState() : (
                        <Wrapper>
                            <div className="actions">
                                {
                                    this.state.permissions.delete && hasPayrollData && [ DRAFT, OPEN ].includes( this.state.data.status ) && !this.state.edit ?
                                        <Button
                                            id="button-delete"
                                            label={
                                                this.props.deleting ? (
                                                    <StyledLoader className="animation">
                                                        DELETING... <div className="anim3"></div>
                                                    </StyledLoader>
                                                ) : 'DELETE'
                                            }
                                            alt={ !this.props.deleting }
                                            size="large"
                                            type="danger"
                                            disabled={ this.props.deleting }
                                            onClick={ () => {
                                                this.setState({ showDeleteConfirm: false }, () => {
                                                    this.setState({ showDeleteConfirm: true });
                                                });
                                            } }
                                            ref={ ( ref ) => { this.deleteButton = ref; } }
                                        /> : ''
                                }
                                {
                                    this.state.edit ? (
                                        <div>
                                            <Button
                                                id="button-save"
                                                label={
                                                    this.props.updating ? (
                                                        <StyledLoader className="animation">
                                                            SAVING... <div className="anim3"></div>
                                                        </StyledLoader>
                                                    ) : 'SAVE'
                                                }
                                                size="large"
                                                type="action"
                                                disabled={ this.props.updating }
                                                onClick={ this.saveEdits }
                                                ref={ ( ref ) => { this.saveButton = ref; } }
                                            />
                                            <Button
                                                id="button-cancel"
                                                label="CANCEL"
                                                type="neutral"
                                                size="large"
                                                disabled={ this.props.updating }
                                                onClick={ () => {
                                                    this.summary.resetData();
                                                    this.details.resetData();
                                                    this.setState({ edit: false });
                                                } }
                                                ref={ ( ref ) => { this.cancelButton = ref; } }
                                            />
                                        </div>
                                    )
                                    : ''
                                }
                                {
                                    this.state.permissions.edit && hasPayrollData && this.state.data.status === OPEN && !this.state.edit ?
                                        <Button
                                            id="button-edit"
                                            label="EDIT"
                                            alt
                                            size="large"
                                            onClick={ () => {
                                                this.setState({ showWarning: false }, () => {
                                                    this.setState({ showWarning: true });
                                                });
                                            } }
                                            disabled={ this.props.updating }
                                            ref={ ( ref ) => { this.editButton = ref; } }
                                        /> : ''
                                }
                                {
                                    this.state.permissions.open && hasPayrollData && this.state.data.status === CLOSED && !this.state.edit ?
                                        <Button
                                            id="button-open-payroll"
                                            label={
                                                this.props.opening ? (
                                                    <StyledLoader className="animation">
                                                        OPENING... <div className="anim3"></div>
                                                    </StyledLoader>
                                                ) : 'OPEN PAYROLL'
                                            }
                                            size="large"
                                            type="action"
                                            alt
                                            onClick={ () => {
                                                this.setState({ showOpenConfirm: false }, () => {
                                                    this.setState({ showOpenConfirm: true });
                                                });
                                            } }
                                            disabled={ this.props.opening }
                                            ref={ ( ref ) => { this.openButton = ref; } }
                                        /> : ''
                                }
                                {
                                    this.state.permissions.close && hasPayrollData && this.state.data.status === OPEN && !this.state.edit ?
                                        <Button
                                            id="button-close-payroll"
                                            label={
                                                this.props.closing ? (
                                                    <StyledLoader className="animation">
                                                        CLOSING... <div className="anim3"></div>
                                                    </StyledLoader>
                                                ) : 'CLOSE PAYROLL'
                                            }
                                            size="large"
                                            type="action"
                                            onClick={ () => (
                                                this.state.data.type !== PAYROLL_TYPES.REGULAR
                                                    ? this.closeModalSpecialPayRun.toggle()
                                                    : this.setState({ showCloseConfirm: false }, () => {
                                                        this.setState({ showCloseConfirm: true });
                                                    })
                                                )
                                            }
                                            disabled={ this.props.closing }
                                            ref={ ( ref ) => { this.closeButton = ref; } }
                                        /> : ''
                                }
                            </div>
                            <H2>PAYROLL SUMMARY</H2>
                            {
                                hasPayrollData ? (
                                    <PayrollSummary
                                        ref={ ( ref ) => { this.summary = ref; } }
                                        data={ Object.assign({}, this.state.data ) }
                                        edit={ this.state.edit }
                                        actionState={ { closing: this.props.closing, deleting: this.props.deleting } }
                                        hasPayslip={ this.props.hasPayslip }
                                        generatePayslip={ () => this.generatePayslip( payrollID ) }
                                        generatePayslipLoading={ this.props.generatePayslipLoading }
                                        viewPayslipAuth={ this.state.permissions.viewPayslip }
                                        generatePayslipAuth={ this.state.permissions.generatePayslip }
                                        sendPayslipAuth={ this.state.permissions.sendPayslip }
                                        sendPayslips={ this.props.sendPayslips }
                                        regeneratePayroll={ this.props.regeneratePayroll }
                                        downloadPayrollRegister={ this.props.downloadPayrollRegister }
                                        hasPayrollRegister={ this.props.hasPayrollRegister }
                                    />
                                ) : <H2>INVALID PAYROLL INFORMATION</H2>
                            }
                            <H2>DISBURSEMENT SUMMARY</H2>
                            {
                                hasPayrollData ? (
                                    <DisbursementSummary
                                        payrollId={ Number( this.props.routeParams.id ) }
                                        companyId={ this.state.data.company_id }
                                        payrollStatus={ this.state.data.status }
                                        getDisbursementMethodDetails={ this.getDisbursementMethodDetails }
                                        getSalpayDisbursementDetails={ this.getSalpayDisbursementDetails }
                                        requestDisburseSalaries={ this.requestDisburseSalaries }
                                        requestValidateDisbursement={ this.requestValidateDisbursement }
                                        requestResendOtp={ this.requestResendOtp }
                                        getDisburseSalariesStatus={ this.getDisburseSalariesStatus }
                                    />
                                )
                                : <H2>INVALID PAYROLL INFORMATION</H2>
                            }
                            <H2>PAYROLL DETAILS</H2>
                            {
                                hasPayrollData ?
                                    <PayrollDetail
                                        ref={ ( ref ) => { this.details = ref; } }
                                        edit={ this.state.edit }
                                        employees={ this.state.data.employees }
                                        totalEmployees={ this.state.data.total_employees }
                                        loading={ this.props.payrollDetailsTableLoading }
                                        onPayrollDetailsPageChange={ this.handlePayrollDetailsPageChange }
                                        payrollType={ this.state.data.type }
                                        isAnnualizePayroll={ Boolean( this.state.data.annualize ) }
                                    /> : <H2>INVALID PAYROLL INFORMATION</H2>
                            }
                        </Wrapper>
                    )
                }
            </OuterWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    data: makeSelectData(),
    hasPayslip: makeSelectHasPayslip(),
    generatePayslipLoading: makeSelectGeneratePayslipLoading(),
    loading: makeSelectLoading(),
    deleting: makeSelectDeleting(),
    updating: makeSelectUpdating(),
    closing: makeSelectClosing(),
    notification: makeSelectNotification(),
    payrollDetailsTableLoading: makeSelectPayrollDetailsTableLoading(),
    hasPayrollRegister: makeSelectHasPayrollRegister(),
    updatingPostingDate: makeSelectUpdatingPostingDate()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        viewActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
