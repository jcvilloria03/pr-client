import styled, { keyframes } from 'styled-components';

export const OuterWrapper = styled.div`
    padding-top: 70px;
`;

export const Wrapper = styled.div`
    background-color: #fff;
    display: flex;
    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
    margin: 20px 2vw;
    color: #444;
    padding: 30px 40px;
    flex-direction: column;

    .actions {
        text-align: right;
        button {
            min-width: 145px;

            &:not(:last-of-type) {
                margin-right: 8px;
            }
        }
    }

    & > h2 {
        font-weight: 600;
    }
`;

export const MessageWrapperStyles = styled.div`
    display: flex;
    justify-content: center;
    height: calc(90vh - 90px);

    > div {
        display: flex;
        flex-direction: column;
        justify-content: center;

        i {
            font-size: 7em;
            align-self: center;
        }

        .description {
            padding-top: 40px;
            text-align: center;

            h3 {
                font-weight: 600;
            }
        }
    }
`;

const anim3 = keyframes`
    to {
        transform: rotate(45deg) translate(3px, 3px);
    }
`;

export const StyledLoader = styled.div`
    font-size: 12px;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;

    .anim3 {
        padding-left: 10px;
        &:before {
            display: block;
            width: 12px;
            height: 12px;
            margin-top: -1px;
            border: 2px solid #fff;
            content: "";
            animation: ${anim3} .5s ease infinite alternate;
            border-radius: 100% 100% 0 100%;
            transform: rotate( 45deg );
        }

        &:after {
            display: block;
            width: 12px;
            height: 3px;
            margin-top: 8px;
            background-color: #fff;
            content: "";
            border-radius: 100%;
        }
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .close-div {
        width: 47%;
        margin-top: 10px;
    }

    .close-button {
        width: 95%;
    }

    .message {
        display: flex;
        align-self: center;
    }
`;

export const ClosePayRunWrapper = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;

    & > div {
        width: 250px;

        & > div {
            margin: 30px 0;
        }
    }
`;
