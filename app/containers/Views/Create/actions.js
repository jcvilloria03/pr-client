import {
    INITIALIZE,
    CREATE_VIEW,
    EDIT_VIEW
} from './constants';

/**
 * Fetch form options on page load
 */
export function initialize( callback, id ) {
    return {
        type: INITIALIZE,
        callback,
        id
    };
}

/**
 * Submit new user view
 */
export function createView( data ) {
    return {
        type: CREATE_VIEW,
        payload: data
    };
}

/**
 * Submit patch request to API to update view
 */
export function editView( id, data ) {
    return {
        type: EDIT_VIEW,
        payload: {
            id,
            data
        }
    };
}
