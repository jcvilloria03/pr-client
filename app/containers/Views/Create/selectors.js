import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectCreateViewDomain = () => ( state ) => state.get( 'createView' );

const makeSelectLoading = () => createSelector(
    selectCreateViewDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectFilters = () => createSelector(
    selectCreateViewDomain(),
    ( substate ) => substate.get( 'filters' ).toJS()
);

const makeSelectFilterTypes = () => createSelector(
    selectCreateViewDomain(),
    ( substate ) => substate.get( 'filter_types' ).toJS()
);

const makeSelectSelectedColumn = () => createSelector(
    selectCreateViewDomain(),
    ( substate ) => substate.get( 'selected_columns' ).toJS()
);

const makeSelectColumns = () => createSelector(
    selectCreateViewDomain(),
    ( substate ) => substate.get( 'columns' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectCreateViewDomain(),
    ( substate ) => substate.get( 'errors' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectCreateViewDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectFilters,
    makeSelectFilterTypes,
    makeSelectSelectedColumn,
    makeSelectColumns,
    makeSelectErrors,
    makeSelectLoading,
    makeSelectNotification
};
