import styled from 'styled-components';

export const MainWrapper = styled.div`
    background: #fff;
    min-height: 100vh;

    .form-error {
        color: #f21108;
        font-size: 13px;
        list-style-type: none;
        padding-left: 0;
    }

    .hide {
        display: none !important;
    }

    .back {
        padding: 20px 10vw;
        background: #f0f4f6;

        a, a:hover, a:active, a:visited {
            color: inherit;
            text-decoration: none;
        }

        i {
            color: #00A5E5;
            margin-right: 20px;
        }
    }

    .actions {
        padding: 20px 10vw;
        background: #EFF4F6;
        text-align: right;

        button {
            min-width: 180px;
            padding: 16px;
            margin-left: 10px;
        }
    }
`;

export const Content = styled.div`
    padding: 30px 10vw;
    margin-bottom: 120px;

    & > h2 {
        font-weight: 600;
        text-align: center;
    }

    & > p {
        text-align: center;
    }

    .columns {
        display: flex;

        .list {
            border: 1px solid #95989a;
            padding: 10px;
            display: flex;
            height: 100%;
            flex-direction: column;
            position: relative;

            & > .search {
                &:after {
                    content: '\\f002';
                    position: absolute;
                    top: 10px;
                    right: 10px;
                    font: normal normal normal 20px/1 FontAwesome;
                    display: flex;
                    height: 40px;
                    align-items: center;
                    margin-right: 6px;
                    color: #00A5E5;
                }

                input {
                    width: 100%;
                    height: 40px;
                    margin-bottom: 10px;
                    padding: 0 10px;
                    border-bottom: 2px solid #ccc;

                    &:focus {
                        outline: none;
                        border-color: #00A5E5;
                    }
                }
            }

            ul {
                margin: 0;
                padding: 0;
                list-style-type: none;
                width: 100%;
                min-height: 100px;

                &#selected li a.plus {
                    display: none !important;
                }

                &#available li a.minus {
                    display: none !important;
                }

                li {
                    position: relative;
                    width: 100%;
                    background: #EFF4F6;
                    padding: 10px 20px;
                    margin-bottom: 10px;
                    cursor: -webkit-grab;
                    border-radius: 6px;
                    border: 1px solid transparent;

                    a {
                        display: none;
                        float: right;
                        color: #00A5E5;
                    }

                    &:hover {
                        border-color: #00A5E5;
                        background: #E4F6FC;
                        a {
                            display: block;
                        }
                    }

                    &.sortable-chosen {
                        background: #E4F6FC;
                    }

                    &:before {
                        content: ''
                        position: absolute;
                        width: 2px;
                        height: 30px;
                        left: 10px;
                        top: 7px;
                        border: 0px solid #ccc;
                        border-left-width: 1px;
                    }
                }
            }
        }
    }

    .filters {
        border: 1px solid #95989a;
        padding: 50px 4vw 10px 4vw;

        table {
            width: 100%;

            button {
                min-width: 120px;
            }

            td {
                padding: 6px;
                vertical-align: top;

                .date {
                    .DayPickerInput, input {
                        width: 100%;
                    }
                    & > div > span {
                        display: none;
                    }
                }
            }
        }
    }
`;
