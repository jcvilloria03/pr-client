import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import Sortable from 'sortablejs';

import SnackBar from '../../../components/SnackBar';
import { H2, P, H5, H6 } from '../../../components/Typography';
import Input from '../../../components/Input';
import Button from '../../../components/Button';
import RadioGroup from '../../../components/RadioGroup';
import Radio from '../../../components/Radio';
import SalSelect from '../../../components/Select';
import SubHeader from '../../SubHeader';
import DatePicker from '../../../components/DatePicker';

import { browserHistory } from '../../../utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';

import * as createViewActions from './actions';
import {
    MainWrapper,
    Content
} from './styles';
import {
    makeSelectFilters,
    makeSelectFilterTypes,
    makeSelectColumns,
    makeSelectSelectedColumn,
    makeSelectLoading,
    makeSelectErrors,
    makeSelectNotification
} from './selectors';

/**
 * CreateView Container
 */
export class CreateView extends React.Component {
    static propTypes = {
        initialize: React.PropTypes.func,
        createView: React.PropTypes.func,
        editView: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        selected_columns: React.PropTypes.array,
        columns: React.PropTypes.array,
        filter_types: React.PropTypes.object,
        filters: React.PropTypes.array,
        errors: React.PropTypes.object,
        routeParams: React.PropTypes.object
    }

    static defaultProps = {
        loading: false,
        filter_types: {
            number: [],
            date: [],
            keyword: [],
            boolean: []
        },
        selected_columns: [],
        filters: [],
        columns: []
    };

    constructor( props ) {
        super( props );

        this.state = {
            filter: '',
            filter_type: 'keyword',
            filters: [],
            selected_columns: [],
            sort: [],
            pagination: {},
            mode: 'OR'
        };

        this.availableDrag = null;
        this.selectedDrag = null;
        this.addFilter = this.addFilter.bind( this );
        this.setupDnD = this.setupDnD.bind( this );
        this.onAdd = this.onAdd.bind( this );
        this.onRemove = this.onRemove.bind( this );
        this.onUpdate = this.onUpdate.bind( this );
        this.createView = this.createView.bind( this );
    }

    componentDidMount() {
        this.props.initialize( this.setupDnD, this.props.routeParams.id || null );
    }

    /**
     * handles changes in props
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.errors.name && this.view_name.setState({ error: true, errorMessage: nextProps.errors.name[ 0 ] });
    }

    /**
     * handles addition to selected columns
     * @param event
     */
    onAdd( event ) {
        const columnCopy = [].concat( this.state.selected_columns );
        const itemData = this.props.columns.filter( ( column ) => column.value === event.item.getAttribute( 'data-value' ) );
        columnCopy.splice( event.newIndex, 0, itemData[ 0 ]);

        this.setState({ selected_columns: columnCopy });
    }

    /**
     * handles removal of item in selected columns list
     * @param event
     */
    onRemove( event ) {
        const columnCopy = [].concat( this.state.selected_columns );
        columnCopy.splice( event.oldIndex, 1 );

        this.setState({ selected_columns: columnCopy });
    }

    /**
     * handles sorting within selected columns list
     * @param event
     */
    onUpdate( event ) {
        const columnCopy = [].concat( this.state.selected_columns );
        const itemData = columnCopy.splice( event.oldIndex, 1 );
        columnCopy.splice( event.newIndex, 0, itemData[ 0 ]);

        this.setState({ selected_columns: columnCopy });
    }

    /**
     * initialize drag and drop feature
     */
    setupDnD( viewData ) {
        const requiredColumns = this.props.columns.filter( ( item ) => item.required === true );
        const requiredFields = requiredColumns.map( ( requiredItem ) => requiredItem.value );
        if ( viewData ) {
            // const selected_columns = viewData.columns.map( ( colInData ) => this.props.columns.filter( ( colInList ) => colInList.value === colInData )[ 0 ] );
            this.view_name.setState({ value: viewData.name });
            this.setState({
                mode: viewData.mode,
                filters: viewData.filters,
                sort: viewData.sort,
                pagination: viewData.pagination,
                selected_columns: this.props.selected_columns
            });
        } else {
            this.setState({ selected_columns: requiredColumns });
        }

        if ( this.availableDrag ) {
            this.availableDrag.destroy();
        }

        this.availableDrag = Sortable.create( this.available_columns, {
            group: {
                name: 'available',
                put: ['selected'],
                pull: true
            },
            animation: 150
        });

        if ( this.selectedDrag ) {
            this.selectedDrag.destroy();
        }

        this.selectedDrag = Sortable.create( this.visible_columns, {
            group: {
                name: 'selected',
                put: ['available'],
                pull( to, from, item ) {
                    return !requiredFields.includes( item.getAttribute( 'data-value' ) );
                }
            },
            onAdd: this.onAdd,
            onRemove: this.onRemove,
            onUpdate: this.onUpdate,
            animation: 150
        });
    }

    /**
     * submits a new view
     */
    createView() {
        const error = this.view_name._validate( this.view_name.state.value );

        if ( error ) {
            return;
        }

        // construct view data
        const data = {
            company_id: JSON.parse( localStorage.user ).last_active_company_id,
            name: this.view_name.state.value,
            mode: this.mode.state.value,
            columns: this.state.selected_columns.map( ( column ) => column.value ),
            filters: this.state.filters
        };

        if ( this.props.routeParams.id ) {
            delete data.company_id;
            this.props.editView( this.props.routeParams.id, data );
        } else {
            this.props.createView( data );
        }
    }

    /**
     * verifies and adds new filter to list
     */
    addFilter() {
        // VAlIDATE FIELD VALUES
        let fieldPassed = false;
        let conditionsPassed = false;
        let valueError = true;
        try {
            fieldPassed = this.fieldForm._checkRequire( this.fieldForm.state.value.value );
        } catch ( e ) {
            fieldPassed = this.fieldForm._checkRequire( this.fieldForm.state.value );
        }

        try {
            conditionsPassed = this.conditionsForm._checkRequire( this.conditionsForm.state.value.value );
        } catch ( e ) {
            conditionsPassed = this.conditionsForm._checkRequire( this.conditionsForm.state.value );
        }

        if ([ 'keyword', 'number' ].includes( this.state.filter_type ) ) {
            valueError = this.valueForm._validate( this.valueForm.state.value );
        } else if ( this.state.filter_type === 'date' ) {
            valueError = this.valueDateForm.checkRequired();
        }

        // CHECK FOR ERRORS
        if ( !fieldPassed || !conditionsPassed ) {
            return;
        } else if ( this.state.filter_type === 'date' && valueError ) {
            return;
        } else if ([ 'keyword', 'number' ].includes( this.state.filter_type ) && valueError ) {
            return;
        }

        // CONSTRUCT OBJECT
        const newFilter = {
            field: `${this.fieldForm.state.value.value}`,
            condition: `${this.conditionsForm.state.value.value}`
        };

        if ([ 'keyword', 'number' ].includes( this.state.filter_type ) ) {
            newFilter.value = this.valueForm.state.value;
        } else if ( this.state.filter_type === 'date' ) {
            newFilter.value = this.valueDateForm.state.selectedDay;
        }

        // APPEND NEW FILTER
        this.setState({ filters: this.state.filters.concat([newFilter]) });
        this.fieldForm.setState({ value: '' });
        this.conditionsForm.setState({ value: null });
        this.valueForm.setState({ value: '' });
        this.valueDateForm.setState({ selectedDay: '' });
    }

    /**
     * Component Render Method
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Employees"
                    meta={ [
                        { name: 'description', content: 'Employee List' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <MainWrapper>
                    <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                    <div className="back">
                        <a href="" onClick={ ( e ) => { e.preventDefault(); browserHistory.push( '/employees', true ); } }><i className="fa fa-arrow-left" /> Back to Previous Page</a>
                    </div>
                    <Content>
                        <H2>Add New View</H2>
                        <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce efficitur porta lacinia. Phasellus a enim euismod, feugiat mauris nec, accumsan enim. Proin vel magna sed purus bibendum blandit molestie sed velit. Suspendisse auctor rhoncus accumsan. Vivamus facilisis nunc id purus mollis dapibus.</P>
                        <br />
                        <div className="row">
                            <div className="col-xs-4">
                                <Input id="name" required label="View name" ref={ ( ref ) => { this.view_name = ref; } } />
                            </div>
                        </div>
                        <br />
                        <div className="row">
                            <div className="col-xs-12">
                                <H5>Select columns to display</H5>
                                <P>
                                    1. Choose the columns to show by dragging entries to the right.
                                    <br />
                                    2. Rearrange the order of display of the columns by dragging up or down the entries on the right.
                                </P>
                                <div className="row columns">
                                    <div className="col-xs-4 filtered">
                                        <H6>Available columns</H6>
                                        <div className="list">
                                            <div className="search">
                                                <input
                                                    onChange={ ( event ) => {
                                                        this.setState({ filter: event.target.value });
                                                    } }
                                                />
                                            </div>
                                            <ul
                                                id="available"
                                                ref={ ( ref ) => { this.available_columns = ref; } }
                                            >
                                                {
                                                    this.props.routeParams.id ? this.props.columns.filter( ( item ) => !this.props.selected_columns.map( ( selected ) => selected.value ).includes( item.value ) ).map( ( availableItem, index ) => (
                                                        <li
                                                            key={ index }
                                                            data-value={ availableItem.value }
                                                            className={
                                                                this.state.filter && !availableItem.label.toLowerCase().includes( this.state.filter.toLowerCase() ) ? 'hide' : ''
                                                            }
                                                        >
                                                            { this.props.columns.filter( ( item ) => item.required ).map( ( reqItem ) => reqItem.label ).includes( availableItem.label ) ? '* ' : '' }
                                                            { availableItem.label }
                                                        </li>
                                                    ) ) : this.props.columns.filter( ( item ) => !item.required ).map( ( availableItem, index ) => (
                                                        <li
                                                            key={ index }
                                                            data-value={ availableItem.value }
                                                            className={
                                                                this.state.filter && !availableItem.label.toLowerCase().includes( this.state.filter.toLowerCase() ) ? 'hide' : ''
                                                            }
                                                        >
                                                            { this.props.columns.filter( ( item ) => item.required ).map( ( reqItem ) => reqItem.label ).includes( availableItem.label ) ? '* ' : '' }
                                                            { availableItem.label }
                                                        </li>
                                                    ) )
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-xs-4 visible">
                                        <H6>Selected columns</H6>
                                        <div className="list">
                                            <ul
                                                id="selected"
                                                ref={ ( ref ) => { this.visible_columns = ref; } }
                                            >
                                                {
                                                    this.props.routeParams.id ? this.props.selected_columns.map( ( requiredItem, index ) => (
                                                        <li
                                                            key={ index }
                                                            data-value={ requiredItem.value }
                                                        >
                                                            { this.props.columns.filter( ( item ) => item.required ).map( ( reqItem ) => reqItem.label ).includes( requiredItem.label ) ? '* ' : '' }
                                                            { requiredItem.label }
                                                        </li>
                                                    ) ) : this.props.columns.filter( ( item ) => item.required ).map( ( requiredItem, index ) => (
                                                        <li
                                                            key={ index }
                                                            data-value={ requiredItem.value }
                                                        >
                                                            { this.props.columns.filter( ( item ) => item.required ).map( ( reqItem ) => reqItem.label ).includes( requiredItem.label ) ? '* ' : '' }
                                                            { requiredItem.label }
                                                        </li>
                                                    ) )
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {
                            this.props.errors.columns ? (
                                <ul style={ { textAlign: 'left', marginTop: '40px' } } className="form-error">
                                    {
                                        this.props.errors.columns.map( ( error, index ) => (
                                            <li key={ index }>{ error }</li>
                                        ) )
                                    }
                                </ul>
                            ) : false
                        }
                        <br />
                        <br />
                        <br />
                        <br />
                        <div className="row">
                            <div className="col-xs-12">
                                <H5>Add filters</H5>
                                <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce efficitur porta lacinia. Phasellus a enim euismod, feugiat mauris nec, accumsan enim. Proin vel magna sed purus bibendum blandit molestie sed velit.</P>
                            </div>
                        </div>
                        <div className="filters">
                            <table>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th width="20%">Column name</th>
                                        <th width="20%">Condition</th>
                                        <th width="20%">Value</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.filters && this.state.filters.map( ( item, index ) => (
                                            <tr key={ index }>
                                                <td>{ index + 1 }.</td>
                                                <td>
                                                    { this.props.filters.filter( ( ( filter ) => filter.value === item.field ) )[ 0 ].label }
                                                    {
                                                        this.props.errors.filters && this.props.errors.filters[ index ] && this.props.errors.filters[ index ].field ? (
                                                            <ul className="form-error">
                                                                {
                                                                    this.props.errors.filters[ index ].field.map( ( error, idx ) => (
                                                                        <li key={ idx }>{ error }</li>
                                                                    ) )
                                                                }
                                                            </ul>
                                                        ) : false
                                                    }
                                                </td>
                                                <td>
                                                    { item.condition }
                                                    {
                                                        this.props.errors.filters && this.props.errors.filters[ index ] && this.props.errors.filters[ index ].condition ? (
                                                            <ul className="form-error">
                                                                {
                                                                    this.props.errors.filters[ index ].condition.map( ( error, idx ) => (
                                                                        <li key={ idx }>{ error }</li>
                                                                    ) )
                                                                }
                                                            </ul>
                                                        ) : false
                                                    }
                                                </td>
                                                <td>
                                                    { item.value }
                                                    {
                                                        this.props.errors.filters && this.props.errors.filters[ index ] && this.props.errors.filters[ index ].value ? (
                                                            <ul className="form-error">
                                                                {
                                                                    this.props.errors.filters[ index ].value.map( ( error, idx ) => (
                                                                        <li key={ idx }>{ error }</li>
                                                                    ) )
                                                                }
                                                            </ul>
                                                        ) : false
                                                    }
                                                </td>
                                                <td>
                                                    <Button
                                                        label={ <span><i className="fa fa-trash" /> Delete</span> }
                                                        type="danger"
                                                        alt
                                                        onClick={ () => {
                                                            this.setState({ filters: this.state.filters.filter( ( filter, i ) => index !== i ) });
                                                        } }
                                                    />
                                                </td>
                                            </tr>
                                        ) )
                                    }
                                    <tr>
                                        <td></td>
                                        <td>
                                            <SalSelect
                                                id="fieldName"
                                                data={ this.props.filters }
                                                ref={ ( ref ) => { this.fieldForm = ref; } }
                                                onChange={ ( value ) => {
                                                    this.setState({ filter_type: value.type }, () => {
                                                        this.conditionsForm.setState({ value: null });
                                                        this.valueForm.setState({ value: '' });
                                                        this.valueDateForm.setState({ selectedDay: '' });
                                                    });
                                                } }
                                                required
                                            />
                                        </td>
                                        <td>
                                            <SalSelect
                                                id="fieldName"
                                                data={ this.props.filter_types[ this.state.filter_type ] }
                                                ref={ ( ref ) => { this.conditionsForm = ref; } }
                                                required
                                            />
                                        </td>
                                        <td>
                                            <Input
                                                className={ [ 'keyword', 'number' ].includes( this.state.filter_type ) ? '' : 'hide' }
                                                id="formValue"
                                                ref={ ( ref ) => { this.valueForm = ref; } }
                                                onChange={ ( value ) => {
                                                    if ( value && this.state.filter_type === 'number' ) {
                                                        this.valueForm.setState({ value: this.valueForm.state.value.toString().replace( /[^0-9.-]/g, '' ) });
                                                    }
                                                } }
                                                required
                                            />
                                            <span className={ this.state.filter_type === 'date' ? 'date' : 'date hide' } >
                                                <DatePicker
                                                    label=""
                                                    ref={ ( ref ) => { this.valueDateForm = ref; } }
                                                    dayFormat={ DATE_FORMATS.DISPLAY }
                                                    required
                                                />
                                            </span>
                                        </td>
                                        <td>
                                            <Button
                                                label={ <span><i className="fa fa-plus" /> Add</span> }
                                                type="action"
                                                alt
                                                onClick={ this.addFilter }
                                            />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div className="row">
                                <RadioGroup
                                    horizontal
                                    value={ this.state.mode }
                                    ref={ ( ref ) => { this.mode = ref; } }
                                    onChange={ ( mode ) => { this.setState({ mode }); } }
                                >
                                    <Radio value="OR">Show results that matches ANY filter</Radio>
                                    <Radio value="AND">Show results that matches ALL filters</Radio>
                                </RadioGroup>
                                {
                                    this.props.errors.mode ? (
                                        <ul style={ { textAlign: 'left', marginTop: '-10px', paddingLeft: '18px' } } className="form-error">
                                            {
                                                this.props.errors.mode.map( ( error, index ) => (
                                                    <li key={ index }>{ error }</li>
                                                ) )
                                            }
                                        </ul>
                                    ) : false
                                }
                            </div>
                            <br />
                            <br />
                        </div>
                    </Content>
                    <div className="actions">
                        <Button
                            label="Cancel"
                            type="action"
                            size="large"
                            alt
                            onClick={ () => { browserHistory.goBack(); } }
                        />
                        <Button
                            label="Submit"
                            type="action"
                            size="large"
                            onClick={ this.createView }
                        />
                    </div>
                </MainWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    filters: makeSelectFilters(),
    filter_types: makeSelectFilterTypes(),
    columns: makeSelectColumns(),
    loading: makeSelectLoading(),
    selected_columns: makeSelectSelectedColumn(),
    errors: makeSelectErrors(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createViewActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( CreateView );
