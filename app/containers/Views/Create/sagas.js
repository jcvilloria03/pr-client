import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { browserHistory } from '../../../utils/BrowserHistory';
import { Fetch } from '../../../utils/request';

import {
    INITIALIZE,
    LOADING,
    FILTERS,
    FILTER_TYPES,
    SET_SELECTED_COLUMNS,
    COLUMNS,
    ERRORS,
    CREATE_VIEW,
    EDIT_VIEW,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * fetch form options
 */
export function* getFormData({ callback, id }) {
    yield put({
        type: LOADING,
        payload: true
    });

    const options = yield call( Fetch, '/view/employee/form_options', { method: 'GET' });

    if ( options.data ) {
        yield put({
            type: FILTERS,
            payload: options.data.filters
        });

        yield put({
            type: FILTER_TYPES,
            payload: options.data.filter_types
        });

        yield put({
            type: COLUMNS,
            payload: options.data.columns
        });
    }

    let viewData = null;
    if ( id ) {
        const views = yield call( Fetch, '/view/employee/user', { method: 'GET' });

        viewData = views.data.filter( ( view ) => view.id === Number( id ) )[ 0 ];

        if ( viewData.columns.length ) {
            const selectedColumns = [];
            viewData.columns.forEach( ( column ) => {
                selectedColumns.push( options.data.columns.filter( ( col ) => col.value === column )[ 0 ]);
            });
            yield put({
                type: SET_SELECTED_COLUMNS,
                payload: selectedColumns
            });
        }
    }

    yield call( callback, viewData );

    yield put({
        type: LOADING,
        payload: false
    });
}

/**
 * submit new view
 */
export function* createView({ payload }) {
    yield put({
        type: LOADING,
        payload: true
    });

    try {
        yield call( Fetch, '/view/employee', {
            method: 'POST',
            data: payload
        });

        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                title: ' ',
                message: ' ',
                show: false,
                type: 'success'
            }
        });

        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                show: true,
                title: 'SUCCESS',
                message: `View ${payload.name} successfully created`
            }
        });

        yield call( delay, 5000 );
        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                title: ' ',
                message: ' ',
                show: false
            }
        });

        browserHistory.goBack();
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * submit new view
 */
export function* editView({ payload }) {
    yield put({
        type: LOADING,
        payload: true
    });

    yield put({
        type: ERRORS,
        payload: {}
    });

    try {
        yield call( Fetch, `/view/employee/${payload.id}`, {
            method: 'PATCH',
            data: payload.data
        });

        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                title: ' ',
                message: ' ',
                show: false,
                type: 'success'
            }
        });

        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                show: true,
                title: 'SUCCESS',
                message: `View ${payload.data.name} successfully updated`
            }
        });

        yield call( delay, 5000 );
        yield put({
            type: NOTIFICATION_SAGA,
            payload: {
                title: ' ',
                message: ' ',
                show: false
            }
        });
        browserHistory.goBack();
    } catch ( error ) {
        // Check if error is from form mismatch (406)
        if ( error.response.status === 406 ) {
            // DISPLAY ERROR VIA FORM
            yield put({
                type: ERRORS,
                payload: error.response.data
            });
        } else {
            // DISPLAY ERROR VIA NOTIFICATION
            yield call( notifyUser, {
                show: true,
                title: error.response ? error.response.statusText : 'Error',
                message: error.response ? error.response.data.message : error.statusText,
                type: 'error'
            });
        }
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getFormData );
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchInitialize() {
    const watcher = yield takeEvery( INITIALIZE, getFormData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchCreateView() {
    const watcher = yield takeEvery( CREATE_VIEW, createView );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchEditView() {
    const watcher = yield takeEvery( EDIT_VIEW, editView );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchInitialize,
    watchCreateView,
    watchEditView,
    watchNotify,
    watchForReinitializePage
];
