/*
 *
 * Create Views constants
 *
 */

export const LOADING = 'app/Views/Create/LOADING';
export const INITIALIZE = 'app/Views/Create/INITIALIZE';
export const FILTERS = 'app/Views/Create/FILTERS';
export const FILTER_TYPES = 'app/Views/Create/FILTER_TYPES';
export const COLUMNS = 'app/Views/Create/COLUMNS';
export const CREATE_VIEW = 'app/Views/Create/CREATE_VIEW';
export const EDIT_VIEW = 'app/Views/Create/EDIT_VIEW';
export const ERRORS = 'app/Views/Create/ERRORS';
export const SET_SELECTED_COLUMNS = 'app/Views/Create/SET_SELECTED_COLUMNS';
export const NOTIFICATION = 'app/Views/Create/NOTIFICATION';
export const NOTIFICATION_SAGA = 'app/Views/Create/NOTIFICATION_SAGA';
