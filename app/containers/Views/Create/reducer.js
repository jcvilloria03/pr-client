import { fromJS } from 'immutable';
import {
    LOADING,
    FILTERS,
    FILTER_TYPES,
    COLUMNS,
    ERRORS,
    SET_SELECTED_COLUMNS,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    selected_columns: [],
    filters: [],
    filter_types: {},
    columns: [],
    errors: {}
});

/**
 *
 * Create View Reducer
 *
 */
function createViewReducer( state = initialState, { type, payload }) {
    switch ( type ) {
        case LOADING:
            return state.set( 'loading', payload );
        case FILTERS:
            return state.set( 'filters', fromJS( payload ) );
        case FILTER_TYPES:
            return state.set( 'filter_types', fromJS( payload ) );
        case COLUMNS:
            return state.set( 'columns', fromJS( payload ) );
        case SET_SELECTED_COLUMNS:
            return state.set( 'selected_columns', fromJS( payload ) );
        case ERRORS:
            return state.set( 'errors', fromJS( payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( Object.assign({}, state.get( 'notification' ).toJS(), payload ) ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default createViewReducer;
