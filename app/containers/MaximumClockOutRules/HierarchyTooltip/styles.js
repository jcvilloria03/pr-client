import styled from 'styled-components';

export const HierarchyList = styled.ol`
    margin: 0;
`;

export const HierarchyListItem = styled.li`
    margin: 0;
`;

export const HierarchyToolTipIcon = styled.i`
    &&& {
        color: ${({ white }) => ( white ? 'white' : 'unset' )};
        background-color: ${({ white }) => ( white ? '#ffffff4a' : '#00a6e526' )};
        width: 20px;
        height: 20px;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
`;
