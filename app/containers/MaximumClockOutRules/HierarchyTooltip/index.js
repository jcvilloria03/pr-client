import React from 'react';

import ToolTip from 'components/ToolTip';

import { HierarchyList, HierarchyListItem, HierarchyToolTipIcon } from './styles';

// eslint-disable-next-line require-jsdoc, react/prop-types
export function HierarchyTootlip({ white }) {
    return (
        <ToolTip
            id="tooltipEarnings"
            target={ <HierarchyToolTipIcon white={ white } className="fa fa-question" /> }
            placement="top"
        >
            <p>
                This is the hierarchy of the max clock-out rules which will determine what rule will be used by your employee:
            </p>

            <HierarchyList>
                <HierarchyListItem>Specific Employee</HierarchyListItem>
                <HierarchyListItem>All Employee</HierarchyListItem>
                <HierarchyListItem>Position</HierarchyListItem>
                <HierarchyListItem>All Position</HierarchyListItem>
                <HierarchyListItem>Department</HierarchyListItem>
                <HierarchyListItem>All Department</HierarchyListItem>
                <HierarchyListItem>Location</HierarchyListItem>
                <HierarchyListItem>All Location</HierarchyListItem>
            </HierarchyList>
        </ToolTip>
    );
}
