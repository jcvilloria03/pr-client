import { RESET_STORE } from 'containers/App/constants';

import {
    SET_LOADING,
    SET_NOTIFICATION,
    ADD_MAX_CLOCKOUT_RULES,
    SET_ACTION_LOADING
} from './constants';

/**
 *
 * MaximumClockOutRulesAdd actions
 *
 */

/**
 * Create max clock-out rule
 */
export function addMaxClockOutRules( payload ) {
    return {
        type: ADD_MAX_CLOCKOUT_RULES,
        payload
    };
}

/**
 * Sets page loading status
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets action loading status
 */
export function setActionLoading( payload ) {
    return {
        type: SET_ACTION_LOADING,
        payload
    };
}

/**
 * Sets notification
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
