/* eslint-disable camelcase */
import React from 'react';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { get } from 'lodash';

import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';
import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';

import { H2, H3, H5 } from 'components/Typography';
import SalConfirm from 'components/SalConfirm';
import SnackBar from 'components/SnackBar';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Loader from 'components/Loader';
import Input from 'components/Input';
import A from 'components/A';

import * as maxClockOutActions from './actions';

import {
    makeSelectActionLoading,
    makeSelectLoading,
    makeSelectNotification
} from './selectors';

import ApplicableGroupsOrEmployeesField from '../ApplicableGroupsOrEmployeesField';
import HoursMinutesInput from '../HoursMinutesInput';

import { NAMESPACE_OPTIONS } from '../View/constants';

import {
    Footer,
    ConfirmBodyWrapperStyle,
    FormPageWrapper,
    LoadingStyles,
    NavWrapper
 } from './styles';

/**
 *
 * MaximumClockOutRulesAdd
 *
 */
export class MaximumClockOutRulesAdd extends React.PureComponent {
    static propTypes = {
        loading: React.PropTypes.bool,
        actionLoading: React.PropTypes.bool,
        products: React.PropTypes.array,
        addMaxClockOutRules: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    };

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            show_modal: false,
            profile: null,
            ruleName: '',
            allowedHours: 0
        };

        this.ruleName = null;
        this.employees = null;
        this.allowedHours = null;
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        } else {
            const profile = auth.getUser();

            this.setState({ profile });
        }
    }

    loadEmployeesList = ( keyword, callback ) => {
        Fetch(
            `/company/${this.state.profile.last_active_company_id}/affected_employees/search`,
            {
                params: {
                    term: keyword,
                    limit: 10,
                    without_users: 'yes'
                },
                headers: {
                    'X-Authz-Entities': 'company_settings.max_clock_outs'
                }
            }
        ).then( ( result ) => {
            const list = result.map( ({ id, name, type }) => ({
                id,
                type,
                name,
                uid: `${type}${id}`,
                value: id,
                label: name,
                field: type
            }) );

            callback( null, { options: NAMESPACE_OPTIONS.concat( list ) });
        }).catch(
            ( error ) => callback( error, null )
        );
    }

    handleAddRule = () => {
        const { ruleName, allowedHours, profile } = this.state;
        const formValid = this.validateForm();

        if ( formValid ) {
            const payload = {
                company_id: profile.last_active_company_id,
                name: ruleName,
                hours: allowedHours,
                affected_employees: this.employees.state.value
            };

            this.props.addMaxClockOutRules( payload );
        }
    }

    handleCancel = () => {
        const { ruleName, allowedHours } = this.state;

        const hasChanges = ruleName !== ''
            || allowedHours
            || get( this.employees, 'state.value.length' );

        if ( hasChanges ) {
            this.setState({ show_modal: true });

            return;
        }

        this.goBack();
    }

    /**
     * Validates form
     * @returns {Boolean}
     */
    validateForm() {
        const { ruleName } = this.state;

        let valid = true;

        if ( this.ruleName._validate( ruleName ) ) {
            valid = false;
        }

        if ( !this.employees._checkRequire( this.employees.state.value ) ) {
            valid = false;
        }

        if ( !this.allowedHours.validate() ) {
            valid = false;
        }

        return valid;
    }

    goBack() {
        browserHistory.push( '/company-settings/time-attendance/maximum-clock-out-rules', true );
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: this.props.products && subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA: this.props.products && subscriptionService.isSubscribedToTA( this.props.products )
        });

        const {
          loading,
          actionLoading,
          notification
         } = this.props;

        const {
           show_modal
         } = this.state;

        return (
            <div>
                <Helmet
                    title="Add Maximum Clock-out Rules"
                    meta={ [
                        { name: 'description', content: 'Description of MaximumClockOutRulesAdd' }
                    ] }
                />

                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />

                <SalConfirm
                    onConfirm={ () => this.goBack() }
                    onClose={ () => this.setState({ show_modal: false }) }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                You are about to leave the page with unsaved changes.
                                <br /><br />
                                Do you wish to proceed?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Warning!"
                    visible={ show_modal }
                />

                <Sidebar items={ sidebarLinks } />

                <NavWrapper>
                    <Container>
                        <A
                            href={ '/company-settings/time-attendance/maximum-clock-out-rules' }
                            disabled={ loading }
                        >
                            &#8592; Back to Maximum Clock-out Rules
                        </A>
                    </Container>
                </NavWrapper>

                <FormPageWrapper>
                    <Container>
                        { loading
                            ? (
                                <div className="loader">
                                    <LoadingStyles>
                                        <H2>Loading Maximum Clock Out</H2>
                                        <br />
                                        <H3>Please wait...</H3>
                                    </LoadingStyles>
                                </div>
                            ) : (
                                <div className="content">
                                    <div className="heading">
                                        <H3>Maximum Clock-out Rules</H3>
                                    </div>

                                    <div className="header">
                                        <H5>Add New Rule</H5>
                                    </div>

                                    <div className="body">
                                        <Input
                                            id="ruleName"
                                            className="mb-1"
                                            label="Rule Name"
                                            name="ruleName"
                                            ref={ ( ref ) => { this.ruleName = ref; } }
                                            onChange={ ( data ) => {
                                                this.setState({ ruleName: data });
                                            } }
                                            required
                                        />

                                        {/* <Input
                                            id="allowedHours"
                                            className="mb-1"
                                            label="Number of Hours Allowed"
                                            type="number"
                                            max={ 260 }
                                            minNumber={ 0 }
                                            ref={ ( ref ) => { this.allowedHours = ref; } }
                                            onChange={ ( data ) => {
                                                this.setState({ allowedHours: data });
                                            } }
                                            required
                                        /> */}

                                        <HoursMinutesInput
                                            id="allowedHours"
                                            ref={ ( ref ) => { this.allowedHours = ref; } }
                                            className="mb-1"
                                            label="Number of Hours Allowed"
                                            onChange={ ( value ) => {
                                                this.setState({ allowedHours: value });
                                            } }
                                            required
                                        />

                                        <ApplicableGroupsOrEmployeesField
                                            onRef={ ( ref ) => { this.employees = ref; } }
                                        />
                                    </div>
                                </div>
                            )
                        }
                    </Container>

                    { !loading && (
                        <Footer>
                            <div className="from_footer">
                                <Container>
                                    <div className="footer_button">
                                        <Button
                                            label="Cancel"
                                            type="neutral"
                                            size="large"
                                            onClick={ this.handleCancel }
                                            disabled={ actionLoading || loading }
                                        />

                                        <Button
                                            id="addButton"
                                            type="action"
                                            size="large"
                                            label={ actionLoading ? <Loader /> : 'Add' }
                                            ref={ ( ref ) => { this.addButton = ref; } }
                                            onClick={ this.handleAddRule }
                                            disabled={ actionLoading || loading }
                                        />
                                    </div>
                                </Container>
                            </div>
                        </Footer>
                    ) }
                </FormPageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    actionLoading: makeSelectActionLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        maxClockOutActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( MaximumClockOutRulesAdd );
