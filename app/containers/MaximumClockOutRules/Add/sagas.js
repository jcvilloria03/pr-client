import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import get from 'lodash/get';

import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';

import {
    setActionLoading,
    setNotification
} from './actions';

import {
    NOTIFICATION,
    ADD_MAX_CLOCKOUT_RULES
} from './constants';

/**
 * Get max clock-out rules
 */
export function* addMaxClockOutRules({ payload }) {
    try {
        yield put( setActionLoading( true ) );

        yield call(
            Fetch,
            '/max_clock_out',
            {
                method: 'POST',
                data: payload,
                headers: { 'X-Authz-Entities': 'company_settings.max_clock_outs' }
            }
        );

        yield put( setActionLoading( false ) );

        browserHistory.push( '/company-settings/time-attendance/maximum-clock-out-rules', true );
    } catch ( error ) {
        yield put( setActionLoading( false ) );

        yield call( notifyError, error );
    }
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Watcher for ADD_MAX_CLOCKOUT_RULES
 */
export function* watchForAddMaxClockOutRules() {
    const watcher = yield takeEvery( ADD_MAX_CLOCKOUT_RULES, addMaxClockOutRules );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForAddMaxClockOutRules
];
