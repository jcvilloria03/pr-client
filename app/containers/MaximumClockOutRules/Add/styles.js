import styled from 'styled-components';

export const Footer = styled.div`
    .from_footer {
        margin: 14px 0px 0px;
        padding: 14px 0px;
        background: #F0F4F6;
        margin-top:280px;
        position: fixed;
        bottom: 0;
        right: 0;
        left: 0;
        text-align: end;
    }

    .from_footer .footer_button {
        width: 105%;
        text-align: end;

        button {
            padding: 10px;
        }

        .animation {
            color: #fff;
        }
    }

    .from_footer .footer_button button {
        border-radius: 2rem;
        cursor: pointer;
        line-height: 1;
        height: 45px;
        width: 130px;

        &:focus{
            outline: none;
        }

        &:disabled{
            cursor: not-allowed;
        }
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const PageWrapper = styled.div`
    .hide {
        display: none;
    }

    .loader {
        margin-top: 190px;

        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .content {
        margin-top: 120px;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-size: 26px;
                font-weight: bold;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .main {
            .btn {
                min-width: 140px;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .clock-out-table {
          .empty-table {
            padding: 20px;
          }
        }

        .header {
            width: 100%;
            display: flex;
            justify-content: space-between;
            align-items: flex-end;
            flex-direction: row;
            margin-bottom: 10px;

            .search-wrapper {
                flex-grow: 1;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 30px;

                    input {
                        border: none;
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }

            .filter-icon > svg {
                height: 10px;
            }
        }
    }
`;

export const FormPageWrapper = styled.div`
    padding-bottom: 80px;
    
    .loader {
        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .content {
        padding-bottom: 60px;

        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h3 {
                font-size: 26px;
                font-weight: bold;
            }

            p {
                text-align: center;
                max-width: 800px;
            }
        }

        .main {
            .btn {
                min-width: 140px;
            }
        }

        .tableAction button {
            width: 130px;
        }

        .clock-out-table {
          .empty-table {
            padding: 20px;
          }
        }

        .header {
            width: 100%;
            display: flex;
            justify-content: space-between;
            align-items: flex-end;
            flex-direction: row;
            margin-bottom: 10px;
        }

        .body {
          width: 40vw;
        }
    }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;

export const NavWrapper = styled.div`
    margin-top: 70px;
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;
