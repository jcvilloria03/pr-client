/*
 *
 * MaximumClockOutRulesAdd constants
 *
 */

const namespace = 'app/containers/MaximumClockOutRulesAdd';

export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_ACTION_LOADING = `${namespace}/SET_ACTION_LOADING`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;
export const ADD_MAX_CLOCKOUT_RULES = `${namespace}/ADD_MAX_CLOCKOUT_RULES`;
