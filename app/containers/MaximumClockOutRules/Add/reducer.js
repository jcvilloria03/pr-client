import { fromJS } from 'immutable';

import { RESET_STORE } from 'containers/App/constants';

import {
    SET_ACTION_LOADING,
    SET_LOADING,
    SET_NOTIFICATION
} from './constants';

const initialState = fromJS({
    loading: false,
    actionLoading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * MaximumClockOutRulesAdd reducer
 *
 */
export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_ACTION_LOADING:
            return state.set( 'actionLoading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
};
