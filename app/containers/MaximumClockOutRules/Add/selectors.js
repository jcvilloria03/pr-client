import { createSelector } from 'reselect';

/**
 * Direct selector to the maximumClockOutRulesAdd state domain
 */
const makeSelectAddMaxClockOut = () => (
    ( state ) => ( state.get( 'maximumClockOutRulesAdd' ) )
);

const makeSelectLoading = () => createSelector(
    makeSelectAddMaxClockOut(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectActionLoading = () => createSelector(
    makeSelectAddMaxClockOut(),
  ( substate ) => substate.get( 'actionLoading' )
);

const makeSelectNotification = () => createSelector(
    makeSelectAddMaxClockOut(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectActionLoading,
  makeSelectNotification
};
