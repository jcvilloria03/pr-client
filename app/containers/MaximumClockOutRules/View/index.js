/* eslint-disable no-restricted-syntax */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';
import isEqual from 'lodash/isEqual';
import get from 'lodash/get';

import { auth } from 'utils/AuthService';
import { browserHistory } from 'utils/BrowserHistory';
import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import {
    formatDeleteLabel,
    formatPaginationLabel
} from 'utils/functions';

import SnackBar from 'components/SnackBar';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Modal from 'components/Modal';
import Table from 'components/Table';
import Input from 'components/Input';
import Loader from 'components/Loader';
import { H2, H3 } from 'components/Typography';
import FooterTablePagination from 'components/FooterTablePagination';
import { getIdsOfSelectedRows, isAnyRowSelected } from 'components/Table/helpers';

import { HierarchyTootlip } from '../HierarchyTooltip';

import ApplicableGroupsOrEmployeesField from '../ApplicableGroupsOrEmployeesField';
import HoursMinutesInput from '../HoursMinutesInput';

import {
    getHourAndMinuteValues,
    getMaxHoursDisplayValue
} from '../utils';

import * as maximumClockOutRulesActions from './actions';

import { NAMESPACE_OPTIONS } from './constants';

import {
    makeSelectNotification,
    makeSelectLoading,
    makeSelectTableLoading,
    makeSelectActionLoading,
    makeSelectMaximumClockOutRules
} from './selectors';

import { Content,
    ContentHeading,
    TableHeading,
    FooterTablePaginationLoadingContent,
    LoadingContent,
    LoadingStyles,
    PageWrapper,
    TableActions,
    TableHeadingContainer
} from './styles';

/**
 *
 * MaximumClockOutRules
 *
 */
export class MaximumClockOutRules extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes={
        getMaxClockOutRules: React.PropTypes.func,
        maximumClockOutRules: React.PropTypes.object,
        loading: React.PropTypes.bool,
        tableLoading: React.PropTypes.bool,
        actionLoading: React.PropTypes.bool,
        deleteMaxClockOutRules: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        updateMaxClockOutRules: React.PropTypes.func,
        products: React.PropTypes.array
    }

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            selectionLabel: '',
            selectPage: false,
            pagination: {
                total: 0,
                last_page: 1,
                per_page: 10,
                current_page: 1
            },
            edited: {}
        };

        this.handlePageChange = this.handlePageChange.bind( this );
        this.handlePageSizeChange = this.handlePageSizeChange.bind( this );
        this.handleSetRowToEdit = this.handleSetRowToEdit.bind( this );

        this.nameInputRef = null;
        this.allowedHoursInputRef = null;
        this.affectedEmployeesRef = null;
        this.maximumClockOutRulesTableRef = null;
        this.confirmDeleteModalRef = null;
        this.notificationRef = null;
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        } else {
            const profile = auth.getUser();

            this.setState({ profile });
        }
    }

    componentDidMount() {
        this.props.getMaxClockOutRules({
            page: 1,
            pageSize: 10
        });
    }

    componentDidUpdate( prevProps ) {
        if (
            !isEqual( prevProps.maximumClockOutRules, this.props.maximumClockOutRules )
        ) {
            const maximumClockOutRulesPagination = { ...this.props.maximumClockOutRules };

            delete maximumClockOutRulesPagination.data;

            // eslint-disable-next-line react/no-did-update-set-state
            this.setState({
                pagination: maximumClockOutRulesPagination
            });
        }
    }

    getTableColumns() {
        return [
            {
                id: 'name',
                header: 'Rule Name',
                sortable: false,
                style: { whiteSpace: 'inherit', wordBreak: 'break-word' },
                render: ({ row }) => (
                    this.isCurrentlyEdited( row )
                        ? (
                            <div className="row-input">
                                <Input
                                    id="name"
                                    type="text"
                                    placeholder="Rule Name"
                                    value={ this.state.edited.name }
                                    ref={ ( ref ) => { this.nameInputRef = ref; } }
                                    onChange={ ( value ) => {
                                        this.handleEditRowValue( 'name', value );
                                    } }
                                    required
                                />
                            </div>
                        ) : (
                            <div> { row.name } </div>
                        )
                )
            },
            {
                id: 'affectedEmployeesRef',
                header: (
                    <TableHeadingContainer>
                        <p>Applicable Groups/Employees</p>

                        <HierarchyTootlip white />
                    </TableHeadingContainer>
                ),
                sortable: false,
                width: 400,
                style: { whiteSpace: 'inherit', wordBreak: 'break-word' },
                render: ({ row }) => {
                    const namespaces = ( get( row, 'affected_employees' ) || [])
                        .map( ( namespace ) => namespace.rel_name )
                        .join( ', ' );

                    return this.isCurrentlyEdited( row )
                        ? (
                            <div className="row-input">
                                <ApplicableGroupsOrEmployeesField
                                    hideLabel
                                    value={ this.state.edited.affected_employees }
                                    onRef={ ( ref ) => { this.affectedEmployeesRef = ref; } }
                                />
                            </div>
                        ) : (
                            <div>{ namespaces }</div>
                        );
                }
            },
            {
                id: 'hours',
                header: 'Maximum Hours',
                sortable: false,
                style: { whiteSpace: 'inherit', wordBreak: 'break-word' },
                render: ({ row }) => {
                    const hourAndMinuteValues = getHourAndMinuteValues( row.hours );
                    const displayValue = getMaxHoursDisplayValue(
                        hourAndMinuteValues[ 0 ],
                        hourAndMinuteValues[ 1 ]
                    );

                    return this.isCurrentlyEdited( row )
                        ? (
                            <div className="row-input">
                                <HoursMinutesInput
                                    id="hours"
                                    ref={ ( ref ) => { this.allowedHoursInputRef = ref; } }
                                    onChange={ ( value ) => {
                                        this.handleEditRowValue( 'hours', value );
                                    } }
                                    value={ this.state.edited.hours }
                                    required
                                />
                            </div >
                        ) : (
                            <div> { displayValue } </div>
                        );
                }
            },
            {
                header: '',
                sortable: false,
                width: 200,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) ? (
                        <div>
                            <Button
                                label={ <span>Cancel</span> }
                                size="small"
                                onClick={ this.handleCancelEditing }
                                disabled={ this.props.actionLoading }
                            />
                            <Button
                                label={ this.props.actionLoading ? <Loader /> : 'Save' }
                                type="action"
                                size="small"
                                disabled={ this.props.actionLoading }
                                onClick={ () => this.handleSaveMaxClockOutRule() }
                            />
                        </div>
                    ) : (
                        <div>
                            <Button
                                label={ <span>Edit</span> }
                                type="grey"
                                size="small"
                                onClick={ () => { this.handleSetRowToEdit( row ); } }
                                disabled={ this.props.actionLoading || this.state.totalSelected > 0 }
                            />
                        </div>
                    )
                )
            }
        ];
    }

    getPaginationLabel() {
        const {
            per_page: perPage,
            current_page: currentPage,
            total
        } = this.state.pagination;

        if ( !total ) return '';

        return formatPaginationLabel({
            page: currentPage - 1,
            pageSize: perPage,
            dataLength: total
        });
    }

    handleEditRowValue( property, value ) {
        this.setState({
            edited: {
                ...this.state.edited,
                [ property ]: value
            }
        });
    }

    isCurrentlyEdited( row ) {
        return this.state.edited && this.state.edited.id === row.id;
    }

    handleBulkDelete = () => {
        const { pagination } = this.state;
        const ids = getIdsOfSelectedRows( this.maximumClockOutRulesTableRef );
        const resetPageValue = 1; // Reset to page 1

        this.props.deleteMaxClockOutRules(
            ids,
            () => {
                this.setState({
                    totalSelected: 0,
                    pagination: {
                        ...pagination,
                        current_page: resetPageValue
                    }
                }, () => {
                    this.confirmDeleteModalRef.toggle();

                    this.props.getMaxClockOutRules({
                        page: resetPageValue,
                        pageSize: pagination.per_page
                    });
                });
            }
        );
    }

    handlePageChange( page ) {
        const { pagination } = this.state;

        this.setState({
            pagination: {
                ...pagination,
                current_page: page
            }
        }, () => {
            this.props.getMaxClockOutRules({
                page,
                pageSize: pagination.per_page
            });
        });
    }

    handlePageSizeChange( pageSize ) {
        if ( !pageSize || pageSize === this.state.pagination.per_page ) return;

        const currentPage = 1; // Reset current pages to 1

        const { pagination } = this.state;

        this.setState({
            pagination: {
                ...pagination,
                per_page: pageSize,
                current_page: currentPage
            }
        }, () => {
            this.props.getMaxClockOutRules({
                page: currentPage,
                pageSize
            });
        });
    }

    handleSetRowToEdit( row ) {
        const rowData = { ...row };

        const mappedAffectedEmployees = rowData.affected_employees.map( ({ rel_id: id, rel_name: name, type }) => {
            let matchedGenericNamespace = NAMESPACE_OPTIONS.find( ( genericNamespace ) => (
                genericNamespace.type === type && genericNamespace.name.toLowerCase() === name.toLowerCase()
            ) );

            if ( matchedGenericNamespace ) {
                matchedGenericNamespace = {
                    ...matchedGenericNamespace,
                    id: null
                };
            }

            return matchedGenericNamespace || {
                id,
                type,
                name,
                uid: `${type}${id}`,
                value: id,
                label: name,
                field: type
            };
        });

        this.setState({
            edited: {
                ...rowData,
                affected_employees: mappedAffectedEmployees
            }
        });
    }

    handleSaveMaxClockOutRule() {
        const {
            pagination,
            edited
        } = this.state;

        const updatedAffectedEmployees = ( this.affectedEmployeesRef.state.value || [])
            .map( ({ id, type, name }) => ({
                id,
                name,
                type,
                uid: `${type}${id}`
            }) );

        if ( !edited.name || !edited.hours || updatedAffectedEmployees.length === 0 ) {
            return;
        }

        this.props.updateMaxClockOutRules(
            {
                id: edited.id,
                name: edited.name,
                hours: edited.hours,
                affected_employees: updatedAffectedEmployees
            },
            () => {
                const resetPageValue = 1; // Reset to page 1

                this.setState({
                    edited: null,
                    pagination: {
                        ...pagination,
                        current_page: resetPageValue
                    }
                }, () => {
                    this.props.getMaxClockOutRules({
                        page: resetPageValue,
                        pageSize: pagination.per_page
                    });
                });
            }
        );
    }

    handleCancelEditing = ( callback ) => {
        this.setState({
            edited: null
        }, callback );
    }

    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const {
            loading,
            tableLoading
        } = this.props;

        const maximumClockOutRulesData = get( this.props.maximumClockOutRules, 'data' ) || [];

        const { selectionLabel } = this.state;

        return (
            <div>
                <Helmet
                    title="Maximum Clock-out Rules"
                    meta={ [
                        { name: 'description', content: 'Description of Maximum Clock-out Rules' }
                    ] }
                />

                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notificationRef = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />

                <Sidebar items={ sidebarLinks } />

                <Modal
                    title="Confirm your action"
                    body={
                        <div>
                            <p>Are you sure you want to delete the selected records?</p>
                        </div>
                    }
                    buttons={ [
                        {
                            id: 'buttonCancel',
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.confirmDeleteModalRef.toggle()
                        },
                        {
                            id: 'buttonProceed',
                            type: 'danger',
                            label: 'Yes',
                            onClick: () => this.handleBulkDelete()
                        }
                    ] }
                    showClose={ false }
                    ref={ ( ref ) => { this.confirmDeleteModalRef = ref; } }
                />

                <PageWrapper>
                    <Container>
                        <LoadingContent hide={ !loading }>
                            <LoadingStyles>
                                <H2>Loading Maximum Clock-out Rules</H2>

                                <br />

                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </LoadingContent>

                        <Content hide={ loading }>
                            <ContentHeading>
                                <H3>Maximum Clock-out Rules</H3>
                            </ContentHeading>

                            <TableHeading>
                                <TableActions>
                                    { isAnyRowSelected( this.maximumClockOutRulesTableRef )
                                            ? <p className="mb-0 mr-1">{ selectionLabel }</p>
                                            : (
                                                <Button
                                                    label="Add Rule"
                                                    type="action"
                                                    onClick={ () =>
                                                        browserHistory.push(
                                                            '/company-settings/time-attendance/maximum-clock-out-rules/add',
                                                            true
                                                        )
                                                    }
                                                />
                                            )
                                        }

                                    { isAnyRowSelected( this.maximumClockOutRulesTableRef ) && (
                                        <Button
                                            alt
                                            label={ this.props.actionLoading ? <Loader /> : 'Delete' }
                                            type="danger"
                                            disabled={ this.props.actionLoading }
                                            onClick={ () => { this.confirmDeleteModalRef.toggle(); } }
                                        />
                                    ) }
                                </TableActions>
                            </TableHeading>

                            <Table
                                selectable
                                external
                                data={ maximumClockOutRulesData }
                                columns={ this.getTableColumns() }
                                loading={ tableLoading }
                                ref={ ( ref ) => { this.maximumClockOutRulesTableRef = ref; } }
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;

                                    this.setState({
                                        totalSelected: selectionLength,
                                        selectionLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                                pageSize={ this.state.pagination.per_page }
                            />
                        </Content>
                    </Container>
                </PageWrapper>

                <FooterTablePaginationLoadingContent hide={ loading }>
                    <FooterTablePagination
                        page={ this.state.pagination.current_page }
                        pageSize={ this.state.pagination.per_page }
                        pagination={ this.state.pagination }
                        onPageChange={ this.handlePageChange }
                        onPageSizeChange={ this.handlePageSizeChange }
                        paginationLabel={ this.getPaginationLabel() }
                    />
                </FooterTablePaginationLoadingContent>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    maximumClockOutRules: makeSelectMaximumClockOutRules(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading(),
    tableLoading: makeSelectTableLoading(),
    actionLoading: makeSelectActionLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        maximumClockOutRulesActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( MaximumClockOutRules );
