import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    SET_NOTIFICATION,
    ACTION_LOADING,
    SET_MAX_CLOCK_OUT_RULES,
    TABLE_LOADING
} from './constants';

const initialState = fromJS({
    allowanceTypes: [],
    maximumClockOutRules: {
        total: 0,
        per_page: 10,
        current_page: 1,
        last_page: 1,
        from: 1,
        to: 10
    },
    taxOptions: [],
    loading: true,
    tableLoading: true,
    actionLoading: false,
    pagination: {
        total: 1,
        current_page: 1,
        last_page: 1,
        per_page: 10
    },
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    isNameAvailable: {
        name: '',
        available: true
    }
});

/**
 *
 * MaximumClockOutRules reducer
 *
 */
function maximumClockOutRulesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case LOADING:
            return state.set( 'loading', action.payload );
        case TABLE_LOADING:
            return state.set( 'tableLoading', action.payload );
        case ACTION_LOADING:
            return state.set( 'actionLoading', action.payload );
        case SET_MAX_CLOCK_OUT_RULES:
            return state.set( 'maximumClockOutRules', fromJS( action.payload ) );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default maximumClockOutRulesReducer;

