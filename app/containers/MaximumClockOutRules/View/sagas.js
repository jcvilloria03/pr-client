import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { cancel, put, call, take } from 'redux-saga/effects';
import { get } from 'lodash';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { formatFeedbackMessage } from 'utils/functions';
import { RECORD_DELETE_MESSAGE, RECORD_UPDATED_MESSAGE } from 'utils/constants';

import { resetStore } from '../../App/sagas';
import { REINITIALIZE_PAGE } from '../../App/constants';

import { getMaxClockOutRules as getMaxClockOutRulesAction } from './actions';

import {
    DELETE_MAX_CLOCK_OUT_RULES,
    UPDATE_MAX_CLOCK_OUT_RULES,
    LOADING,
    SET_NOTIFICATION,
    ACTION_LOADING,
    GET_MAX_CLOCK_OUT_RULES,
    SET_MAX_CLOCK_OUT_RULES,
    TABLE_LOADING
} from './constants';

/**
 * Default saga
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Fetch maximum clock-out rules data
 */
export function* getMaxClockOutRules({ payload }) {
    try {
        yield put({
            type: TABLE_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const params = {
            page: payload.page,
            per_page: payload.pageSize,
            mode: 'simple'
        };

        const maxClockOutRulesData = yield call(
            Fetch,
            `/company/${companyId}/max_clock_outs`,
            {
                params,
                headers: { 'X-Authz-Entities': 'company_settings.max_clock_outs' }
            }
        );

        yield put({
            type: SET_MAX_CLOCK_OUT_RULES,
            payload: maxClockOutRulesData
        });
    } catch ( error ) {
        yield call( notifyError, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: TABLE_LOADING,
            payload: false
        });

        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Delete maximum clock-out rules
 */
export function* deleteMaxClockOutRules({ payload, successCallback }) {
    try {
        yield put({ type: ACTION_LOADING, payload: true });

        const companyId = company.getLastActiveCompanyId();

        yield call(
            Fetch, '/max_clock_out/bulk_delete',
            {
                method: 'DELETE',
                data: {
                    company_id: companyId,
                    max_clock_out_ids: payload
                },
                headers: { 'X-Authz-Entities': 'company_settings.max_clock_outs' }
            }
        );

        yield put({ type: ACTION_LOADING, payload: false });

        yield call( successCallback );

        yield call( showSuccessMessage, RECORD_DELETE_MESSAGE );
    } catch ( error ) {
        yield put({ type: ACTION_LOADING, payload: false });

        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.response.statusText,
            type: 'error'
        });
    }
}

/**
 * Update maximum clock-out rules
 */
export function* updateMaxClockOutRules({ payload, successCallback }) {
    try {
        yield put({ type: ACTION_LOADING, payload: true });

        const companyId = company.getLastActiveCompanyId();

        yield call(
            Fetch,
            `/max_clock_out/${payload.id}`,
            {
                method: 'PATCH',
                data: { company_id: companyId, ...payload },
                headers: { 'X-Authz-Entities': 'company_settings.max_clock_outs' }
            }
        );

        yield put({ type: ACTION_LOADING, payload: false });

        yield call( successCallback );

        yield call( showSuccessMessage, RECORD_UPDATED_MESSAGE );
    } catch ( error ) {
        yield put({ type: ACTION_LOADING, payload: false });

        yield call( notifyError, error );
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield put({
        type: LOADING,
        payload: true
    });

    yield put( getMaxClockOutRulesAction({
        page: 1,
        pageSize: 10
    }) );
}

 /**
 * Display a notification to user
 */
export function* showSuccessMessage( message ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( true, 'success', message )
    });

    yield call( delay, 5000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Handles error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.message ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );

    yield put({
        type: SET_NOTIFICATION,
        payload: emptyNotification
    });
}

/**
 * Watch DELETE_MAX_CLOCK_OUT_RULES
 */
export function* watchForDeleteMaxClockOutRules() {
    const watcher = yield takeEvery( DELETE_MAX_CLOCK_OUT_RULES, deleteMaxClockOutRules );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch UPDATE_MAX_CLOCK_OUT_RULES
 */
export function* watchForUpdateMaxClockOutRules() {
    const watcher = yield takeEvery( UPDATE_MAX_CLOCK_OUT_RULES, updateMaxClockOutRules );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch GET_MAX_CLOCK_OUT_RULES
 */
export function* watchForGetMaximumClockOutRules() {
    const watcher = yield takeEvery( GET_MAX_CLOCK_OUT_RULES, getMaxClockOutRules );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForReinitializePage,
    watchForDeleteMaxClockOutRules,
    watchForUpdateMaxClockOutRules,
    watchForGetMaximumClockOutRules
];
