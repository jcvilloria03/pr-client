import { createSelector } from 'reselect';

/**
 * Direct selector to the maximumClockOutRulesView state domain
 */
const selectMaximumClockOutRulesListDomain = () => ( state ) => state.get( 'maximumClockOutRulesView' );

/**
 * Other specific selectors
 */
const makeSelectMaximumClockOutRules = () => createSelector(
  selectMaximumClockOutRulesListDomain(),
  ( substate ) => substate.get( 'maximumClockOutRules' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectMaximumClockOutRulesListDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectTableLoading = () => createSelector(
  selectMaximumClockOutRulesListDomain(),
  ( substate ) => substate.get( 'tableLoading' )
);

const makeSelectActionLoading = () => createSelector(
  selectMaximumClockOutRulesListDomain(),
  ( substate ) => substate.get( 'actionLoading' )
);

const makeSelectNotification = () => createSelector(
  selectMaximumClockOutRulesListDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  selectMaximumClockOutRulesListDomain,
  makeSelectMaximumClockOutRules,
  makeSelectLoading,
  makeSelectTableLoading,
  makeSelectNotification,
  makeSelectActionLoading
};
