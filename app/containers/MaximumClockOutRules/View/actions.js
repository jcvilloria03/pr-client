import {
    GET_MAX_CLOCK_OUT_RULES,
    DELETE_MAX_CLOCK_OUT_RULES,
    UPDATE_MAX_CLOCK_OUT_RULES,
    DEFAULT_ACTION
} from './constants';

/**
 *
 * MaximumClockOutRules actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 * Get maximum clock-out rules
 *
 * @param {object} payload          Request params
 * @param {object} payload.page     Current page
 * @param {object} payload.pageSize Page list size
 */
export function getMaxClockOutRules( payload ) {
    return {
        type: GET_MAX_CLOCK_OUT_RULES,
        payload
    };
}

/**
 * Delete maximum clock-out rules
 */
export function deleteMaxClockOutRules( payload, successCallback ) {
    return {
        type: DELETE_MAX_CLOCK_OUT_RULES,
        payload,
        successCallback
    };
}

/**
 * Update maximum clock-out rule
 */
export function updateMaxClockOutRules( payload, successCallback ) {
    return {
        type: UPDATE_MAX_CLOCK_OUT_RULES,
        payload,
        successCallback
    };
}
