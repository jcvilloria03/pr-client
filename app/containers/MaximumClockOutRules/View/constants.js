/*
 *
 * MaximumClockOutRules constants
 *
 */
const namespace = 'app/containers/MaximumClockOutRules';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;

export const LOADING = `${namespace}/LOADING`;
export const ACTION_LOADING = `${namespace}/ACTION_LOADING`;
export const TABLE_LOADING = `${namespace}/TABLE_LOADING`;

export const SET_NOTIFICATION = `${namespace}/SET_NOTIFICATION`;

export const DELETE_MAX_CLOCK_OUT_RULES = `${namespace}/DELETE_MAX_CLOCK_OUT_RULES`;
export const UPDATE_MAX_CLOCK_OUT_RULES = `${namespace}/UPDATE_MAX_CLOCK_OUT_RULES`;

export const GET_MAX_CLOCK_OUT_RULES = `${namespace}/GET_MAX_CLOCK_OUT_RULES`;
export const SET_MAX_CLOCK_OUT_RULES = `${namespace}/SET_MAX_CLOCK_OUT_RULES`;

export const NAMESPACE_OPTIONS = [
    {
        type: 'department',
        uid: 'all_department',
        value: 'department',
        field: 'department',
        label: 'All departments',
        name: 'All departments',
        disabled: false
    },
    {
        type: 'position',
        uid: 'all_position',
        value: 'position',
        field: 'position',
        label: 'All positions',
        name: 'All positions',
        disabled: false
    },
    {
        type: 'location',
        uid: 'all_location',
        value: 'location',
        field: 'location',
        label: 'All locations',
        name: 'All locations',
        disabled: false
    },
    {
        type: 'employee',
        uid: 'all_employee',
        value: 'employee',
        field: 'employee',
        label: 'All employee',
        name: 'All employee',
        disabled: false
    }
];
