import styled from 'styled-components';

export const PageWrapper = styled.div`
    height:100%;
    overflow-y: auto;

    .hide {
        display: none;
    }

    .ReactTable {
        .rt-table {
            overflow: visible !important;
        }

        .rt-td {
            overflow: visible !important;

            .row-input {
                width: 100%;
            }

            .row-input input.error {
                margin-top: 25px;
            }
        }

        .Select-control{
            .Select-menu-outer{
                z-index: 9 !important;
            }
        }
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    height: 100%;
    justify-content: center;
    padding: 140px 0;
`;

export const LoadingContent = styled.div`
    display: ${({ hide }) => ( hide ? 'none' : 'block' )};
`;

export const Content = styled( LoadingContent )`
    margin-top: 110px;
    padding-bottom: 100px;

    .selectPage{
        p{
            text-align: end;
            background-color: #e5f6fc;
            margin: 0;
            padding: 12px 10px;
            font-size: 14px;
            font-weight: 600;
            a{
                color: #00a5e5;
                padding: 0;
            }
        }
    }

    .main {
        .btn {
            min-width: 140px;
        }
    }

    .tableAction button {
        width: 130px;
    }

    .selected {
        border-left-color: #00a5e5;
        background-color: #e5f6fc;
    }

    &&&& {
        .ReactTable {
            .Select {
                .Select-control {

                    .Select-multi-value-wrapper {
                        
                        .Select-value {
                            line-height: 32px;
                        }
                    }
                }
            }
        }
    }
`;

export const ContentHeading = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 0 auto 50px auto;

    h3 {
        font-weight: 600;
        margin-bottom: 5px;
    }

    p {
        text-align: center;
        max-width: 800px;
        font-size:14px;
        margin-bottom: 0px;
    }
`;

export const TableHeading = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 8px;
    justify-content: flex-end;

    h5 {
        margin: 0;
        margin-right: 20px;
        font-weight: 700;
        font-size: 18px;
    }
`;

export const TableActions = styled.div`
    display: flex;
    align-items: center;

    p,.dropdown span,span{
        font-size: 14px;
    }
`;

export const FooterTablePaginationLoadingContent = styled( LoadingContent )`
    position: fixed;
    bottom: 0;
    background: #fff;
`;

export const TableHeadingContainer = styled.div`
    display: flex;
    align-items: center;
    gap: 8px;

    p {
        margin: 0;
    }
`;
