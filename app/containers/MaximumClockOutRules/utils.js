import get from 'lodash/get';

const getTextValueDisplay = ( value, unit ) => {
    if ( value === 1 ) {
        return `${value} ${unit}`;
    }

    if ( value > 1 ) {
        return `${value} ${unit}s`;
    }

    return '';
};

export const getMaxHoursDisplayValue = ( hours, minutes ) => {
    const parsedHoursValue = parseInt( hours, 10 );
    const parsedMinutesValue = parseInt( minutes, 10 );

    const hoursTextDisplay = getTextValueDisplay( parsedHoursValue, 'hour' );
    const minutesTextDisplay = getTextValueDisplay( parsedMinutesValue, 'minute' );

    const comma = hoursTextDisplay && minutesTextDisplay
            ? ', '
            : '';

    return `${hoursTextDisplay}${comma}${minutesTextDisplay}`;
};

export const getHourAndMinuteValues = ( maxHours ) => {
    const splitValue = ( maxHours || '' ).split( '.' );
    const hoursValue = parseInt( get( splitValue, '0' ) || '0', 10 );
    const minutesValue = parseFloat( `0.${get( splitValue, '1' ) || '0'}` ) * 60;

    return [ hoursValue, minutesValue ];
};
