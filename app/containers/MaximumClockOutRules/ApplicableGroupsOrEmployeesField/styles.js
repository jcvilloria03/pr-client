import styled from 'styled-components';

export const LabelContainer = styled.div`
    display: inline-flex;
    gap: 8px;
    align-items: center;
`;

