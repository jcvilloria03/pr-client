import React from 'react';

import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';

import MultiSelect from 'components/MultiSelect';

import { HierarchyTootlip } from '../HierarchyTooltip';

import { NAMESPACE_OPTIONS } from '../View/constants';

import { LabelContainer } from './styles';

/**
 *
 * ApplicableGroupsOrEmployeesField
 *
 */
export class ApplicableGroupsOrEmployeesField extends React.PureComponent {
    static propTypes = {
        id: React.PropTypes.string,
        onRef: React.PropTypes.func,
        value: React.PropTypes.array,
        hideLabel: React.PropTypes.bool
    }

    loadEmployeesList = ( keyword, callback ) => {
        const profile = auth.getUser();

        Fetch(
            `/company/${profile.last_active_company_id}/affected_employees/search`,
            {
                params: {
                    term: keyword,
                    limit: 10,
                    without_users: 'yes'
                },
                headers: { 'X-Authz-Entities': 'company_settings.max_clock_outs' }
            }
        ).then( ( result ) => {
            const list = result.map( ({ id, name, type }) => ({
                id,
                type,
                name,
                uid: `${type}${id}`,
                value: id,
                label: name,
                field: type
            }) );

            callback( null, { options: NAMESPACE_OPTIONS.concat( list ) });
        }).catch(
            ( error ) => callback( error, null )
        );
    }

    render() {
        const {
            id,
            onRef,
            value,
            hideLabel
        } = this.props;

        const props = {
            id: id || 'employees',
            value,
            async: true,
            required: true,
            loadOptions: this.loadEmployeesList,
            ref: onRef,
            placeholder: 'Enter location, department name, position, employee name'
        };

        if ( !hideLabel ) {
            props.label = (
                <LabelContainer>
                    <span>Group of Employees</span>

                    <HierarchyTootlip />
                </LabelContainer>
            );
        }

        return (
            <MultiSelect { ...props } />
        );
    }
}

export default ApplicableGroupsOrEmployeesField;
