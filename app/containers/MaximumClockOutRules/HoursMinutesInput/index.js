import React from 'react';
import { getHourAndMinuteValues, getMaxHoursDisplayValue } from '../utils';

import {
    Container,
    ErrorMessage,
    InputLabel,
    InputsContainer,
    MainInput,
    MainInputContainer
} from './styles';

const NON_NUMBER_CHARACTER_MAP = {
    SMALL_E: 69,
    PLUS_SIGN: 189,
    MINUS_SIGN: 187,
    DOT: 190
};

/**
 *
 * HoursMinutesInput
 *
 */
export class HoursMinutesInput extends React.PureComponent {
    static propTypes = {
        id: React.PropTypes.string,
        label: React.PropTypes.string,
        value: React.PropTypes.number,
        onChange: React.PropTypes.func,
        required: React.PropTypes.bool,
        className: React.PropTypes.bool
    };

    constructor( props ) {
        super( props );

        this.state = {
            lastActiveInput: '',
            showInputs: false,
            values: {
                hours: '0',
                minutes: '0'
            },
            focus: {
                hours: false,
                minutes: false
            },
            error: false
        };

        this.handleOnFocusMainInput = this.handleOnFocusMainInput.bind( this );
        this.handleClickListener = this.handleClickListener.bind( this );
        this.handleOnKeyDownInput = this.handleOnKeyDownInput.bind( this );

        this.containerRef = null;
        this.hourMinutesInputRef = null;
        this.hoursInputRef = null;
        this.minutesInputRef = null;
    }

    componentDidMount() {
        window.addEventListener( 'click', this.handleClickListener );

        const { value } = this.props;

        const hourAndMinuteValues = getHourAndMinuteValues( value );

        // eslint-disable-next-line react/no-did-mount-set-state
        this.setState({
            values: {
                hours: parseInt( hourAndMinuteValues[ 0 ], 10 ),
                minutes: parseInt( hourAndMinuteValues[ 1 ], 10 )
            }
        });
    }

    componentWillUnmount() {
        window.removeEventListener( 'click', this.handleClickListener );
    }

    getComputedValue() {
        let { values: { hours, minutes }} = this.state;

        hours = parseInt( hours, 10 );
        minutes = parseInt( minutes, 10 ) / 60;

        return hours + minutes;
    }

    validate = () => {
        const { required } = this.props;

        const error = Boolean( required && !this.getComputedValue() );

        this.setState({ error });

        return !error;
    }

    handleClickListener = ( event ) => {
        if ( !this.containerRef.contains( event.target ) ) {
            this.setState({ showInputs: false });
        }
    }

    handleOnFocusMainInput = () => {
        const { lastActiveInput } = this.state;

        if ( document.activeElement === this.hoursInputRef
            || document.activeElement === this.minutesInputRef ) {
            return;
        }

        this.setState({
            showInputs: true
        }, () => {
            if ( lastActiveInput === 'minutes' ) {
                this.minutesInputRef.focus();
            } else {
                this.hoursInputRef.focus();
            }
        });
    }

    handleOnChangeInput = ( key, event ) => {
        const { onChange } = this.props;

        const value = event.target.value;
        const parsedValue = parseInt( value, 10 ) || 0;

        const { values } = this.state;
        let updatedValues = {
            ...values,
            [ key ]: parsedValue.toString()
        };

        if ( parsedValue < 0 ) {
            updatedValues = {
                ...updatedValues,
                [ key ]: parsedValue.toString()
            };
        }

        if ( key === 'minutes' && parsedValue >= 60 ) {
            updatedValues = {
                minutes: '0',
                hours: ( parseInt( updatedValues.hours, 10 ) + 1 ).toString()
            };
        }

        this.setState({ values: updatedValues }, () => {
            this.validate();

            if ( onChange ) onChange( this.getComputedValue() );
        });
    }

    handleOnFocusOrBlurInput = ( key, value ) => {
        const { focus } = this.state;

        this.setState({
            lastActiveInput: key,
            focus: { ...focus, [ key ]: value }
        });
    }

    handleOnKeyDownInput = ( key, event ) => {
        const { keyCode, shiftKey } = event;

        if ( Object.values( NON_NUMBER_CHARACTER_MAP ).includes( keyCode ) ) {
            event.preventDefault();
        }

        if ( key === 'hours' ) return;

        // Hide inputs if user presses tab key
        if ( !shiftKey && keyCode === 9 ) {
            this.setState({ showInputs: false }, this.validate );
        }
    }

    render() {
        const { id, label, required, className } = this.props;

        const { values, showInputs, error } = this.state;
        let { hours: hoursValue, minutes: minutesValue } = values;
        hoursValue = hoursValue || '0';
        minutesValue = minutesValue || '0';

        return (
            <Container
                className={ className }
                innerRef={ ( ref ) => { this.containerRef = ref; } }
                active={ showInputs }
                error={ error }
            >
                <MainInputContainer>
                    { label && (
                        <InputLabel htmlFor={ id }>
                            {required && (
                                <span className="required">* </span>
                            )}

                            { label }
                        </InputLabel>
                    )}

                    <MainInput
                        id={ id }
                        value={ getMaxHoursDisplayValue( hoursValue, minutesValue ) }
                        className={ error ? 'error' : '' }
                        onFocus={ this.handleOnFocusMainInput }
                        readOnly
                    />

                    {error && !showInputs && (
                        <ErrorMessage>This field is required</ErrorMessage>
                    )}
                </MainInputContainer>

                <InputsContainer show={ showInputs }>
                    <input
                        ref={ ( ref ) => { this.hoursInputRef = ref; } }
                        type="number"
                        value={ hoursValue }
                        min={ 0 }
                        onFocus={ () => this.handleOnFocusOrBlurInput( 'hours', true ) }
                        onBlur={ () => {
                            this.handleOnFocusOrBlurInput( 'hours', false );
                            this.validate();
                        } }
                        onChange={ ( event ) => this.handleOnChangeInput( 'hours', event ) }
                        onKeyDown={ ( event ) => this.handleOnKeyDownInput( 'hours', event ) }
                    />

                    <span>:</span>

                    <input
                        ref={ ( ref ) => { this.minutesInputRef = ref; } }
                        type="number"
                        value={ minutesValue }
                        min={ 0 }
                        max={ 60 }
                        onFocus={ () => this.handleOnFocusOrBlurInput( 'minutes', true ) }
                        onBlur={ () => {
                            this.handleOnFocusOrBlurInput( 'minutes', false );
                            this.validate();
                        } }
                        onChange={ ( event ) => this.handleOnChangeInput( 'minutes', event ) }
                        onKeyDown={ ( event ) => this.handleOnKeyDownInput( 'minutes', event ) }
                    />
                </InputsContainer>
            </Container>
        );
    }
}

export default HoursMinutesInput;
