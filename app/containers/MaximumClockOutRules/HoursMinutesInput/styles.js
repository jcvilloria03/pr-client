import styled, { css } from 'styled-components';

export const InputLabel = styled.label`
    color: #5b5b5b;
    font-size: 14px;
    margin-bottom: 4px;
    font-weight: 400;

    &.error {
        margin-bottom: 25px;
    }

    .required {
        color: #f2130a;
    }

    :focus {
        color: red;
    }
`;

export const MainInput = styled.input`
    &&& {
        border: 1px solid #95989a;
        border-radius: 0px;
        line-height: 28px;
        background-color: #fff;
        padding: 0 .75rem;
        height: 46px;
    }

    :focus {
        outline: none;
    }
`;

export const InputsContainer = styled.div`
    display: ${({ show }) => ( show ? 'inline-flex' : 'none' )};
    align-items: center;
    position: absolute;
    margin-top: 4px;
    background-color: #fff;
    z-index: 1;
    box-shadow: 2px 2px 16px #0000003d;

    input {
        width: 80px;
        padding: 10px;
        text-align: center;
            
        :focus {
            outline: none;
        }
    }

    span {
        padding: 2px;
    }
`;

const activeStyles = css`
    ${InputLabel} {
        color: #0096d0;
    }

    &&& {
        ${MainInput}, ${InputsContainer} {
            border: 1px solid #0096d0;
        }
    }
`;

const errorStyles = css`
    ${InputLabel} {
        color: #f2130a;
    }

    &&& {
        ${MainInput}, ${InputsContainer} {
            border: 1px solid #f2130a;
        }
    }
`;

export const MainInputContainer = styled.div`
    display: flex;
    flex-direction: column;
`;

export const ErrorMessage = styled.p`
    color: #f21108;
    font-size: 13px;
    padding-left: 2px;
    margin-bottom: 6px;
`;

export const Container = styled.div`
    position: relative;

    ${({ active }) => active && activeStyles}
    ${({ error }) => error && errorStyles}
`;
