import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import { H4 } from 'components/Typography';
import A from 'components/A';
import { subscriptionService } from 'utils/SubscriptionService';

import { BASE_PATH_NAME } from '../../constants';

import { Wrapper, ContentWrapper } from './styles';

/**
 *
 * Guide
 *
 */
export class Guide extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        products: React.PropTypes.array
    }

    constructor( props ) {
        super( props );
        this.state = {
            links: []
        };
    }

    componentWillMount() {
        this.setState({
            links: [
                {
                    title: 'Subscriptions',
                    url: `${BASE_PATH_NAME}/guides/subscriptions/`,
                    visible: subscriptionService.isSubscribedToAnyProduct( this.props.products )
                },
                {
                    title: 'Employees',
                    url: `${BASE_PATH_NAME}/guides/employees/`,
                    visible: subscriptionService.isSubscribedToAnyProduct( this.props.products )
                }, {
                    title: 'Loans',
                    url: `${BASE_PATH_NAME}/guides/loans/`,
                    visible: subscriptionService.isSubscribedToPayroll( this.props.products )
                }, {
                    title: 'Deductions',
                    url: `${BASE_PATH_NAME}/guides/deductions/`,
                    visible: subscriptionService.isSubscribedToPayroll( this.props.products )
                }, {
                    title: 'Adjustments',
                    url: `${BASE_PATH_NAME}/guides/adjustments/`,
                    visible: subscriptionService.isSubscribedToPayroll( this.props.products )
                }, {
                    title: 'Allowances',
                    url: `${BASE_PATH_NAME}/guides/allowances/`,
                    visible: subscriptionService.isSubscribedToPayroll( this.props.products )
                }, {
                    title: 'Leaves',
                    url: `${BASE_PATH_NAME}/guides/leaves/`,
                    visible: subscriptionService.isSubscribedToTA( this.props.products )
                }, {
                    title: 'Payroll',
                    url: `${BASE_PATH_NAME}/guides/payroll/`,
                    visible: subscriptionService.isSubscribedToPayroll( this.props.products )
                }, {
                    title: 'Bonuses',
                    url: `${BASE_PATH_NAME}/guides/bonuses/`,
                    visible: subscriptionService.isSubscribedToPayroll( this.props.products )
                }, {
                    title: 'Commissions',
                    url: `${BASE_PATH_NAME}/guides/commissions/`,
                    visible: subscriptionService.isSubscribedToPayroll( this.props.products )
                }, {
                    title: 'Annual Earnings',
                    url: `${BASE_PATH_NAME}/guides/annual-earnings/`,
                    visible: subscriptionService.isSubscribedToPayroll( this.props.products )
                }, {
                    title: 'Workflow Entitlements',
                    url: `${BASE_PATH_NAME}/guides/workflow-entitlements/`,
                    visible: subscriptionService.isSubscribedToTA( this.props.products )
                }
            ]
        });
    }

    /**
     *
     * Guide render method
     *
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Salarium"
                    meta={ [
                        { name: 'description', content: 'Description of Guide' }
                    ] }
                />
                <Wrapper>
                    <ContentWrapper>
                        <H4>Guides</H4>
                        <ul>
                            {
                                this.state.links.map( ( link ) => (
                                    link.visible ? (
                                        <li key={ link.title }><A href={ link.url }>{ link.title }</A></li>
                                    ) : null
                                ) )
                            }
                        </ul>
                    </ContentWrapper>
                </Wrapper>
            </div>
        );
    }
}

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return {
        dispatch
    };
}

export default connect( null, mapDispatchToProps )( Guide );
