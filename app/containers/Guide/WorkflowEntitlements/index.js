import React from 'react';
import Helmet from 'react-helmet';
import { Container, Breadcrumb, BreadcrumbItem } from 'reactstrap';

import { H4 } from '../../../components/Typography';
import SubHeader from '../../SubHeader';
import A from '../../../components/A';

import { browserHistory } from '../../../utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import { ContentWrapper, NavWrapper } from './styles';

/**
 *
 * WorkflowEntitlementsGuide
 *
 */
export default class WorkflowEntitlementsGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/workflow-entitlements/add?batch', true );
                            } }
                        >
                            &#8592;Back to Workflows
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <Helmet
                        title="Workflow Entitlements Guide"
                        meta={ [
                            { name: 'description', content: 'Description of Workflow Entitlements Guide' }
                        ] }
                    />
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem active>Workflow Entitlements</BreadcrumbItem>
                        </Breadcrumb>
                        <H4>Workflow Entitlements</H4>
                        <ul>
                            <li><A href="/payroll/guides/workflow-entitlements/batch-upload">Batch Upload</A></li>
                        </ul>
                    </ContentWrapper>
                </Container>
            </div>
        );
    }
}
