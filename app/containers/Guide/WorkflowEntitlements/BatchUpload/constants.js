export const EXAMPLE_COLUMNS_1 = [
    {
        accessor: 'columnName',
        header: 'Column Name',
        sortable: false
    },
    {
        accessor: 'description',
        header: 'Description',
        sortable: false
    },
    {
        accessor: 'requiredField',
        header: 'Required field?',
        sortable: false
    },
    {
        accessor: 'sampleValue',
        header: 'Sample values',
        sortable: false
    }
];

export const EXAMPLE_DATA_1 = [
    {
        columnName: 'Employee ID',
        description: 'ID given to Employee',
        requiredField: 'Yes',
        sampleValue: '11-111111-1'
    },
    {
        columnName: 'Leave',
        description: 'Workflow to be used for Leave requests',
        requiredField: 'Yes',
        sampleValue: 'Leaves Workflow'
    },
    {
        columnName: 'Time Correction',
        description: 'Workflow to be used for Time Correction requests',
        requiredField: 'Yes',
        sampleValue: 'Sample Workflow Name'
    },
    {
        columnName: 'Shift Change',
        description: 'Workflow to be used for Shift Change requests',
        requiredField: 'Yes',
        sampleValue: 'Sample Workflow Name'
    },
    {
        columnName: 'Overtime',
        description: 'Workflow to be used for Overtime requests',
        requiredField: 'Yes',
        sampleValue: 'Sample Workflow Name'
    },
    {
        columnName: 'Undertime',
        description: 'Workflow to be used for Undertime requests',
        requiredField: 'Yes',
        sampleValue: 'Sample Workflow Name'
    },
    {
        columnName: 'Loan',
        description: 'Workflow to be used for Loan requests',
        requiredField: 'Yes',
        sampleValue: 'Sample Workflow Name'
    }
];

export const EXAMPLE_COLUMNS_2 = [
    {
        accessor: 'rowNumber',
        header: 'Row Number',
        sortable: false
    },
    {
        accessor: 'errorType',
        header: 'Error Type',
        sortable: false
    }
];

export const EXAMPLE_DATA_2 = [
    {
        rowNumber: 'Row 10',
        errorType: 'Error message goes here'
    },
    {
        rowNumber: 'Row 12',
        errorType: 'Error message goes here'
    },
    {
        rowNumber: 'Row 14',
        errorType: 'Error message goes here'
    },
    {
        rowNumber: 'Row 20',
        errorType: 'Error message goes here'
    }
];

export const EXAMPLE_COLUMNS_3 = [
    {
        accessor: 'employee_name',
        header: 'Employee Name',
        sortable: true
    },
    {
        accessor: 'employee_id',
        header: 'Employee ID',
        sortable: true
    },
    {
        accessor: 'workflow_name',
        header: 'Workflow',
        sortable: true
    },
    {
        accessor: 'request_type',
        header: 'Request Type',
        sortable: true
    }
];

export const EXAMPLE_DATA_3 = [
    {
        employee_name: 'John Doe',
        employee_id: '11-11111-11',
        workflow_name: 'Leave Workflow',
        request_type: 'Leave'
    },
    {
        employee_name: 'Jane Doe',
        employee_id: '2A-34E',
        workflow_name: 'Loan Workflow',
        request_type: 'Loan'
    },
    {
        employee_name: 'Jack Doe',
        employee_id: 'ABCDE1234',
        workflow_name: 'General Workflow',
        request_type: 'Overtime'
    }
];
