import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';

import { H4 } from '../../../components/Typography';
import A from '../../../components/A';

import { browserHistory } from '../../../utils/BrowserHistory';
import { subscriptionService } from '../../../utils/SubscriptionService';

import { Wrapper, ContentWrapper } from './styles';

/**
 *
 * EmployeeGuide
 *
 */
export class EmployeeGuide extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToAnyProduct( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    /**
     *
     * EmployeeGuide render method
     *
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Employee Guide"
                    meta={ [
                        { name: 'description', content: 'Description of EmployeeGuide' }
                    ] }
                />
                <Wrapper>
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem active>Employees</BreadcrumbItem>
                        </Breadcrumb>
                        <H4>Employees</H4>
                        <ul>
                            <li><A href="/payroll/guides/employees/batch-upload">Batch Upload</A></li>
                        </ul>
                    </ContentWrapper>
                </Wrapper>
            </div>
        );
    }
}

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return {
        dispatch
    };
}

export default connect( null, mapDispatchToProps )( EmployeeGuide );
