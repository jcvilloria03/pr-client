import styled from 'styled-components';

export const Wrapper = styled.div`
    padding: 90px 0;    
`;

export const ContentWrapper = styled.div`
    display: flex;
    flex-direction: column;    
`;

export const StyledTableContent = styled.section`
    > div { padding-top: 60px; }
    .req1 { color: #F21108; display: inline; }
    .req2 { color: #00A5E5; display: inline; }
    table {
        border: 1px solid #eceeef;
        thead {
            background: #00a5e5;
            color: #fff;
        }
    }
`;

export const StyledInstructionContent = styled.section`
    img { max-width: 100%;  }
    @media (min-width: 576px) {
        img { margin-left: 5%; margin-bottom: 20px; }
        img.offset-sm { margin-left: 2%; }
        img.offset-none { margin-left: 0; }
    }
`;

export const GuideWrapper = styled.div`
    .sl-c-page__header {
        text-align: center;
    }

    .sl-c-page__title {
        margin: 0 0 1.5rem 0;
        font-size: 32px;
        font-weight: bold;
        font-family: "sourcesanspro-bold", sans-serif
    }

    .sl-c-section__title {
        margin-top: auto;
        font-family: "sourcesanspro-regular", sans-serif;
        font-size: 18px;

        .sl-c-circle {
            margin-right: .5rem;
            color: #333;
            border-color: #333;
        }
    }

    .sl-c-grid {
        display: grid;
        grid-column-gap: 1rem;
        grid-row-gap: 1rem;

        &.sl-c-grid--twos {
            grid-template-columns: 1fr 1fr;
        }
    }

    .sl-c-progress {
        position: relative;
        display: block;
        height: 1rem;
        overflow: hidden;
        border: 1px solid #f0f4f6;
        border-radius:  1rem;
        width: 50%;
    }

    .sl-c-progress__bar {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        margin: 2px;
        border-radius: 1rem;
        transition: width .2s ease-in-out;
    }

    .sl-c-progress--success {
        .sl-c-progress__bar {
            background-color: #83d24b;
        }
    }

    .sl-c-text--success {
        padding-top: 3px;
        color: #83d24b;
    }

    .sl-c-table-header {
        display: flex;
        justify-content: space-between;
        padding-bottom: 20px;
        align-items: center;
    }

    .sl-c-link {
        color:  #00A5E5;
    }

    .sl-c-modal-buttons {
        width: 100%;
        display: flex;
        flex-direction: column;
        justify-items:  space-around;

        .btn {
            width: 100%;
        }
    }

    .errors {
        .rt-thead .rt-tr {
            background: #F21108;
            color: #fff;
        }
        .rt-tbody .rt-tr {
            background: rgba(242, 17, 8, .1)
        }
        .ReactTable.-striped .rt-tr.-odd {
            background: rgba(242, 17, 8, .2)
        }

        .ReactTable .rt-th,
        .ReactTable .rt-td {
            flex: 1 0 0px;
            white-space: initial;
            text-overflow: ellipsis;
            padding: 7px 20px;
            overflow: hidden;
            transition: 0.3s ease;
            transition-property: width, min-width, padding, opacity;
        }

        .react-bs-container-body tr {
            background: rgba(249, 210, 210, 0.8);

            &:hover {
                background: rgba(249, 210, 210, 1);
            }
        }
    }
`;
