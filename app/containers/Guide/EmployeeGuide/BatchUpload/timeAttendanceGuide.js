import React from 'react';

import { Table } from 'reactstrap';
import { H4 } from '../../../../components/Typography';

import { StyledTableContent } from './styles';
import { TIME_ATTENDANCE_GUIDE } from './constants';

/**
 *
 * Employee Personal Information CSV Template Guide
 *
 */
export default function () {
    return (
        <StyledTableContent>
            <div id={ TIME_ATTENDANCE_GUIDE }>
                <br />
                <H4>Employee Time and Attendance Information CSV Template Guide</H4>
                <br />
                <Table>
                    <thead>
                        <tr>
                            <th>Column Name</th>
                            <th>Description</th>
                            <th>Required?</th>
                            <th>Example</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Employee ID</th>
                            <td>Unique Identifier of an employee</td>
                            <td><span className="req1">yes</span></td>
                            <td>Emp0001</td>
                        </tr>
                        <tr>
                            <th scope="row">Last Name</th>
                            <td>Last Name of the employee</td>
                            <td><span className="req1">yes</span></td>
                            <td>Dela Cruz</td>
                        </tr>
                        <tr>
                            <th scope="row">First Name</th>
                            <td>First Name of the employee</td>
                            <td><span className="req1">yes</span></td>
                            <td>Juan</td>
                        </tr>
                        <tr>
                            <th scope="row">Middle Name</th>
                            <td>Middle Name of the employee</td>
                            <td>no</td>
                            <td>Santos</td>
                        </tr>
                        <tr>
                            <th scope="row">Hours Per Day</th>
                            <td>How many hours per day this employee should be working. Time format of HH:MM </td>
                            <td><span className="req1">yes</span></td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">Status</th>
                            <td>
                            Marital status of the employee. Select either:
                                <ul>
                                    <li>Single</li>
                                    <li>Married</li>
                                    <li>Widowed</li>
                                    <li>Separated</li>
                                </ul>
                            </td>
                            <td><span className="req1">yes</span></td>
                            <td>Single</td>
                        </tr>
                        <tr>
                            <th scope="row">Date Hired</th>
                            <td>When the employee is hired. Date format of MMM-DD-YYYY</td>
                            <td><span className="req1">yes</span></td>
                            <td>Jun-05-2016</td>
                        </tr>
                        <tr>
                            <th scope="row">Termination Date</th>
                            <td>When the employee resigns. Date format of MMM-DD-YYYY</td>
                            <td>no</td>
                            <td>Jan-12-2017</td>
                        </tr>
                        <tr>
                            <th scope="row">Employment Type</th>
                            <td>Value should come from the employment type names previously added</td>
                            <td><span className="req1">yes</span></td>
                            <td>Regular</td>
                        </tr>
                        <tr>
                            <th scope="row">Position</th>
                            <td>Value should come from the position names previously added</td>
                            <td><span className="req1">yes</span></td>
                            <td>Project Manager</td>
                        </tr>
                        <tr>
                            <th scope="row">Department</th>
                            <td>Value should come from the department names previously added</td>
                            <td><span className="req1">yes</span></td>
                            <td>Operations</td>
                        </tr>
                        <tr>
                            <th scope="row">Rank</th>
                            <td>Value should come from the rank names previously added</td>
                            <td>no</td>
                            <td>Managerial</td>
                        </tr>
                        <tr>
                            <th scope="row">Team</th>
                            <td>Value should come from the team names previously added</td>
                            <td>no</td>
                            <td>T&A 3.0</td>
                        </tr>
                        <tr>
                            <th scope="row">Team Role</th>
                            <td>Select either:
                                <ul>
                                    <li>
                                    Leader
                                    </li>
                                    <li>
                                    Member
                                    </li>
                                </ul>
                            </td>
                            <td>no</td>
                            <td>Leader</td>
                        </tr>
                        <tr>
                            <th scope="row">Primary Location</th>
                            <td>Value should come from the location names previously added - Primary basis of time and attendance rules that are location related filter</td>
                            <td><span className="req1">yes</span></td>
                            <td>Buendia-Makati</td>
                        </tr>
                        <tr>
                            <th scope="row">Secondary Location</th>
                            <td>Value should come from the location names previously added</td>
                            <td>no</td>
                            <td>Paseo-Makati</td>
                        </tr>
                        <tr>
                            <th scope="row">Timesheet Required?</th>
                            <td>Input “Yes” as value if the employee is required to submit time records. Else, input “No”.</td>
                            <td><span className="req1">yes</span></td>
                            <td>Yes</td>
                        </tr>
                        <tr>
                            <th scope="row">Entitled to Overtime?</th>
                            <td>Input “Yes” as value if the employee is allowed to Overtime computation. Else, input “No”.</td>
                            <td><span className="req1">yes</span></td>
                            <td>No</td>
                        </tr>
                        <tr>
                            <th scope="row">Entitled Night Differential?</th>
                            <td>Input “Yes” as value if the employee is allowed to Night Differential computation. Else, input “No”.</td>
                            <td><span className="req1">yes</span></td>
                            <td>Yes</td>
                        </tr>
                        <tr>
                            <th scope="row">Entitled Unworked Regular Holiday Pay?</th>
                            <td>Input “Yes” as value if the employee is allowed to Unworked Regular Holiday computation. Else, input “No”.</td>
                            <td><span className="req1">yes</span></td>
                            <td>No</td>
                        </tr>
                        <tr>
                            <th scope="row">Entitled Unworked Special Holiday Pay?</th>
                            <td>Input “Yes” as value if the employee is allowed to Unworked Special Holiday computation. Else, input “No”.</td>
                            <td><span className="req1">yes</span></td>
                            <td>Yes</td>
                        </tr>
                        <tr>
                            <th scope="row">Entitled Holiday Premium Pay?</th>
                            <td>Input “Yes” as value if the employee is allowed to Holiday Premium computation. Else, input “No”.</td>
                            <td><span className="req1">yes</span></td>
                            <td>No</td>
                        </tr>
                        <tr>
                            <th scope="row">Entitled Rest Day Pay?</th>
                            <td>Input “Yes” as value if the employee is allowed to Rest Day computation. Else, input “No”.</td>
                            <td><span className="req1">yes</span></td>
                            <td>Yes</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </StyledTableContent>
    );
}
