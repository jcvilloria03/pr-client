import React from 'react';

import { Table } from 'reactstrap';
import { H4 } from '../../../../components/Typography';

import { StyledTableContent } from './styles';
import { PERSONAL_GUIDE } from './constants';

/**
 *
 * Employee Personal Information CSV Template Guide
 *
 */
export default function () {
    return (
        <StyledTableContent>
            <div id={ PERSONAL_GUIDE }>
                <br />
                <H4>Employee Personal Information CSV Template Guide</H4>
                <br />
                <Table>
                    <thead>
                        <tr>
                            <th>Column Name</th>
                            <th>Description</th>
                            <th>Required?</th>
                            <th>Example</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row"> Employee ID</th>
                            <td>Company ID</td>
                            <td><span className="req1">yes</span></td>
                            <td>2015001</td>
                        </tr>
                        <tr>
                            <th scope="row">Last Name</th>
                            <td>Surname</td>
                            <td><span className="req1">yes</span></td>
                            <td>Doe</td>
                        </tr>
                        <tr>
                            <th scope="row">First Name</th>
                            <td>Given name</td>
                            <td><span className="req1">yes</span></td>
                            <td>John</td>
                        </tr>
                        <tr>
                            <th scope="row">Middle Name</th>
                            <td>Middle Name</td>
                            <td>no</td>
                            <td>Smith</td>
                        </tr>
                        <tr>
                            <th scope="row">Email</th>
                            <td>Primary email address</td>
                            <td><span className="req1">yes</span></td>
                            <td>johndoe@mail.com</td>
                        </tr>
                        <tr>
                            <th scope="row">Telephone Number</th>
                            <td>Landline</td>
                            <td>no</td>
                            <td>3084125</td>
                        </tr>
                        <tr>
                            <th scope="row">Mobile Number</th>
                            <td>(country code) national destination code subscriber number</td>
                            <td>no</td>
                            <td>(63)9171234567</td>
                        </tr>
                        <tr>
                            <th scope="row">Address Line 1</th>
                            <td></td>
                            <td>no</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th scope="row">Address Line 2</th>
                            <td></td>
                            <td>no</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th scope="row">Country</th>
                            <td></td>
                            <td>no</td>
                            <td>Philippines</td>
                        </tr>
                        <tr>
                            <th scope="row">City</th>
                            <td></td>
                            <td>no</td>
                            <td>Manila</td>
                        </tr>
                        <tr>
                            <th scope="row">Zip Code</th>
                            <td></td>
                            <td>no</td>
                            <td>1407</td>
                        </tr>
                        <tr>
                            <th scope="row">Birthdate</th>
                            <td>Date format: MM/DD/YYYY</td>
                            <td>no</td>
                            <td>11/03/1980</td>
                        </tr>
                        <tr>
                            <th scope="row">Gender</th>
                            <td>Male or Female</td>
                            <td>no</td>
                            <td>Male</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </StyledTableContent>
    );
}
