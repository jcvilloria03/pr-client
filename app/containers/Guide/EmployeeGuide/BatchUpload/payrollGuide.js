import React from 'react';

import { Table } from 'reactstrap';
import { H4 } from '../../../../components/Typography';

import { StyledTableContent } from './styles';
import { PAYROLL_GUIDE } from './constants';

/**
 *
 * Employee Payroll Information Upload Guide
 *
 */
export default function () {
    return (
        <StyledTableContent>
            <div id={ PAYROLL_GUIDE }>
                <br />
                <H4><a name="payroll_info">Employee Payroll Information Upload Guide</a></H4>
                <br />
                <Table>
                    <thead>
                        <tr>
                            <th>Column Name</th>
                            <th>Description</th>
                            <th>Required?</th>
                            <th>Example</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row"> Employee ID</th>
                            <td>Company ID</td>
                            <td><span className="req1">yes</span></td>
                            <td>2015001</td>
                        </tr>
                        <tr>
                            <th scope="row">Last Name</th>
                            <td>Surname</td>
                            <td><span className="req1">yes</span></td>
                            <td>Doe</td>
                        </tr>
                        <tr>
                            <th scope="row">First Name</th>
                            <td>Given name</td>
                            <td><span className="req1">yes</span></td>
                            <td>John</td>
                        </tr>
                        <tr>
                            <th scope="row">Middle Name</th>
                            <td>Middle Name</td>
                            <td>
                                <span className="req2">
                                    Required if Middle Name exists<br />
                                    on the Employee Personal Information
                                </span>
                            </td>
                            <td>Smith</td>
                        </tr>
                        <tr>
                            <th scope="row">Tax Type</th>
                            <td>
                                <ul>
                                    <li>Regular</li>
                                    <li>ROHQ</li>
                                    <li>Consultant</li>
                                    <li>Minimum Wage</li>
                                </ul>
                            </td>
                            <td><span className="req1">yes</span></td>
                            <td>Regular</td>
                        </tr>
                        <tr>
                            <th scope="row">Consultant Tax Rate</th>
                            <td>Only applicable to consultants and should be a whole number. (0-100)</td>
                            <td><span className="req2">Required if <strong>Tax Type</strong> is set to <strong>Consultant</strong></span></td>
                            <td>20</td>
                        </tr>
                        <tr>
                            <th scope="row">Tax Status</th>
                            <td>
                                <ul>
                                    <li>S for Single</li>
                                    <li>S1 for Single - 1 Dependent</li>
                                    <li>S2 for Single - 2 Dependents</li>
                                    <li>S3 for Single - 3 Dependents</li>
                                    <li>S4 for Single - 4 Dependents</li>
                                    <li>M for Married</li>
                                    <li>M1 for Married - 1 Dependent</li>
                                    <li>M2 for Married - 2 Dependent</li>
                                    <li>M3 for Married - 3 Dependent</li>
                                    <li>M4 for Married - 4 Dependent</li>
                                    <li>Z for No Exemption</li>
                                </ul>
                            </td>
                            <td><span className="req1">yes</span></td>
                            <td>S1</td>
                        </tr>
                        <tr>
                            <th scope="row">Base Pay</th>
                            <td></td>
                            <td><span className="req1">yes</span></td>
                            <td>100,000.00</td>
                        </tr>
                        <tr>
                            <th scope="row">Base Pay Unit</th>
                            <td>
                                <ul>
                                    <li>Per Day</li>
                                    <li>Per Hour</li>
                                    <li>Per Month</li>
                                    <li>Per Year</li>
                                </ul>
                            </td>
                            <td><span className="req1">yes</span></td>
                            <td>Per Month</td>
                        </tr>
                        <tr>
                            <th scope="row">Date Hired</th>
                            <td>Date format: MM/DD/YYYY</td>
                            <td><span className="req1">yes</span></td>
                            <td>11/03/2011</td>
                        </tr>
                        <tr>
                            <th scope="row">Termination Date</th>
                            <td>Date format: MM/DD/YYYY</td>
                            <td>no</td>
                            <td>11/03/2014</td>
                        </tr>
                        <tr>
                            <th scope="row">Hours per Day</th>
                            <td>Hours rendered by employee per day</td>
                            <td>no</td>
                            <td>8</td>
                        </tr>
                        <tr>
                            <th scope="row">Payroll Group</th>
                            <td>Payroll Group Name</td>
                            <td><span className="req1">yes</span></td>
                            <td>Regular</td>
                        </tr>
                        <tr>
                            <th scope="row">Department</th>
                            <td>Must be an existing department</td>
                            <td><span className="req1">yes</span></td>
                            <td>Engineering</td>
                        </tr>
                        <tr>
                            <th scope="row">Rank</th>
                            <td>Must be an existing rank</td>
                            <td><span className="req1">yes</span></td>
                            <td>Rank and File</td>
                        </tr>
                        <tr>
                            <th scope="row">Cost Center</th>
                            <td>Must be an existing cost center</td>
                            <td><span className="req1">yes</span></td>
                            <td>Research and Development</td>
                        </tr>
                        <tr>
                            <th scope="row">SSS</th>
                            <td>Social Security System Identification Number</td>
                            <td><span className="req1">yes</span></td>
                            <td>04-7451477-2</td>
                        </tr>
                        <tr>
                            <th scope="row">TIN</th>
                            <td>9 or 12-digit Taxpayer Identification Number</td>
                            <td><span className="req1">yes</span></td>
                            <td>123-456-789-012</td>
                        </tr>
                        <tr>
                            <th scope="row">HDMF</th>
                            <td>House Development Mutual Fund Identification Number</td>
                            <td><span className="req1">yes</span></td>
                            <td>4177-3254-4787</td>
                        </tr>
                        <tr>
                            <th scope="row">PhilHealth</th>
                            <td>Philippine Health Insurance Corp</td>
                            <td><span className="req1">yes</span></td>
                            <td>74-560045512-8</td>
                        </tr>
                        <tr>
                            <th scope="row">SSS Basis</th>
                            <td>
                                <ul>
                                    <li>Gross Income</li>
                                    <li>Basic Pay</li>
                                    <li>Fixed</li>
                                </ul>
                            </td>
                            <td><span className="req1">yes</span></td>
                            <td>Basic Pay</td>
                        </tr>
                        <tr>
                            <th scope="row">SSS Amount</th>
                            <td>SSS contribution amount</td>
                            <td><span className="req2">Required if SSS Basis is set to &#39;Fixed&#39;</span></td>
                            <td>50.00</td>
                        </tr>
                        <tr>
                            <th scope="row">PhilHealth Basis</th>
                            <td>
                                <ul>
                                    <li>Gross Income</li>
                                    <li>Basic Pay</li>
                                    <li>Fixed</li>
                                </ul>
                            </td>
                            <td><span className="req1">yes</span></td>
                            <td>Gross Income</td>
                        </tr>
                        <tr>
                            <th scope="row">PhilHealth Amount</th>
                            <td>Philhealth contribution amount</td>
                            <td><span className="req2">Required if PhilHealth Basis is set to &#39;Fixed&#39;</span></td>
                            <td>110.00</td>
                        </tr>
                        <tr>
                            <th scope="row">HDMF Basis</th>
                            <td>
                                <ul>
                                    <li>Gross Income</li>
                                    <li>Basic Pay</li>
                                    <li>Fixed</li>
                                </ul>
                            </td>
                            <td><span className="req1">yes</span></td>
                            <td>Fixed</td>
                        </tr>
                        <tr>
                            <th scope="row">HDMF Amount</th>
                            <td>HDMF contribution amount</td>
                            <td><span className="req2">Required if HDMF Basis is set to &#39;Fixed&#39;</span></td>
                            <td>220.00</td>
                        </tr>
                        <tr>
                            <th scope="row">Work Location</th>
                            <td>Must match one of the existing Company Work Location</td>
                            <td><span className="req1">yes</span></td>
                            <td>Head Office</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </StyledTableContent>
    );
}
