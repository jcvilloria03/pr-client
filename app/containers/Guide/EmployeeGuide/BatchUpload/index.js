import React from 'react';
import Helmet from 'react-helmet';
import { Container, Breadcrumb, BreadcrumbItem } from 'reactstrap';

import Instruction from './instruction';
import PersonalGuide from './personalGuide';
import TimeAttendanceGuide from './timeAttendanceGuide';
import PayrollGuide from './payrollGuide';

import { Wrapper, GuideWrapper } from './styles';
import { PERSONAL_GUIDE, PAYROLL_GUIDE } from './constants';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { subscriptionService } from '../../../../utils/SubscriptionService';

/**
 *
 * EmployeeGuide
 *
 */
class EmployeeBatchUploadGuide extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        products: React.PropTypes.array
    }
    /**
     * Constructor
     */
    constructor() {
        super();
        this.scrollToGuideByHash = this.scrollToGuideByHash.bind( this );

        //  OnLoad event handler to detect hash in url
        this.scrollToGuideByHashHandler = () => {
            this.scrollToGuideByHash( window.location.hash.substring( 1 ) );
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToAnyProduct( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    /**
     * Onload event when component is rendered to DOM
     */
    componentDidMount() {
        //  This scrolls to desired section when loading the page for first time
        window.addEventListener( 'load', this.scrollToGuideByHashHandler );
    }

    /**
     * Unload event when component is removed from DOM
     */
    componentWillUnmount() {
        //  Remove scrollToGuide from DOM load event
        window.removeEventListener( 'load', this.scrollToGuideByHashHandler );
    }

    /**
     * Scroll to section based on hash
     */
    scrollToGuideByHash( guide ) {
        switch ( ( guide || '' ).toLowerCase() ) {
            case PERSONAL_GUIDE:
                document.getElementById( PERSONAL_GUIDE ).scrollIntoView( true );
                break;
            case PAYROLL_GUIDE:
                document.getElementById( PAYROLL_GUIDE ).scrollIntoView( true );
                break;
            default:
                break;
        }
    }

    /**
     *
     * EmployeeGuide render method
     *
     */
    render() {
        return (
            <Container>
                <Helmet
                    title="Employee Guide"
                    meta={ [
                        { name: 'description', content: 'Description of EmployeeGuide' }
                    ] }
                />
                <Wrapper>
                    <Breadcrumb>
                        <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                        <BreadcrumbItem><a href="/payroll/guides/employees">Employees</a></BreadcrumbItem>
                        <BreadcrumbItem active>Batch Upload</BreadcrumbItem>
                    </Breadcrumb>
                    <GuideWrapper>
                        <Instruction scrollToGuideByHash={ this.scrollToGuideByHash } />
                        <PersonalGuide />
                        <TimeAttendanceGuide />
                        <PayrollGuide />
                    </GuideWrapper>
                </Wrapper>
            </Container>
        );
    }
}

export default EmployeeBatchUploadGuide;
