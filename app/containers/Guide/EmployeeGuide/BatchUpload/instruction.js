import React, { PropTypes } from 'react';

import { H4 } from '../../../../components/Typography';
import A from '../../../../components/A';

import { StyledInstructionContent } from './styles';
import { FRONT_END_ASSETS_URL, PERSONAL_GUIDE, PAYROLL_GUIDE } from './constants';

/**
 *
 * Employee Guide Instruction
 *
 */
const Instruction = function Instruction({ scrollToGuideByHash }) {
    return (
        <StyledInstructionContent>
            <div className="sl-c-page__header">
                <H4 className="sl-c-page__title">Employee Batch Upload</H4>
                <p className="sl-c-page__desc">Users can start to upload employee records
                 after creating at least one Payroll Group.</p>
            </div>

            <div className="sl-c-page__body">
                <p>You need to upload the Employee Personal Information first</p>
                <p>
                    On the Personal Information stage, you can download the Personal Information CSV Template.&nbsp;<br />
                    You can do this by clicking either the download link or the download button
                </p>
                <img alt="Download template" src={ `${FRONT_END_ASSETS_URL}/assets/personal_download1.png` } />
                &nbsp;<br />
                <img alt="Download template" src={ `${FRONT_END_ASSETS_URL}/assets/personal_download2.png` } />
                <p>To hep you correctly fill-out the CSV template, the <A href={ `#${PERSONAL_GUIDE}` } onClick={ () => { scrollToGuideByHash( PERSONAL_GUIDE ); } }>Employee Personal Information Upload Guide</A> is provided</p>
                <img alt="Click here for personal template upload guide" src={ `${FRONT_END_ASSETS_URL}/assets/personal_click_here.png` } />
                <p>
                    After completely filling out the spreadsheet, save it as a CSV file on your personal drive.&nbsp;<br />
                    You are now ready to upload this into the system.
                </p>
                <p>On the Upload Template section, choose the file you wish to upload.</p>
                <img alt="Choose file" src={ `${FRONT_END_ASSETS_URL}/assets/personal_choose_file.png` } />
                <p>Click the UPLOAD button</p>
                <img alt="Upload template" src={ `${FRONT_END_ASSETS_URL}/assets/personal_upload.png` } />
                <p>The system will valildate your file against the acceptable values. You will see the status of the upload process</p>
                <img alt="Upload template" src={ `${FRONT_END_ASSETS_URL}/assets/personal_uploading.png` } />
                <p>
                    The system will present you the errors on your CSV file, if any.&nbsp;<br />
                    You will have to edit your file to conform to the formats presented in the <A href={ `#${PERSONAL_GUIDE}` } onClick={ () => { scrollToGuideByHash( PERSONAL_GUIDE ); } }>Employee Personal Information Upload Guide</A> to avoid such errors.
                    <br />You wil also have the ability to copy the errors to your clipboard, if demand necessary.
                </p>
                <img className="offset-sm" alt="" src={ `${FRONT_END_ASSETS_URL}/assets/personal_error.png` } />
                <p>You have to re-upload your file after correcting the errors</p>
                <img alt="Upload validated" src={ `${FRONT_END_ASSETS_URL}/assets/personal_upload_validated.png` } />
                <p>Click the NEXT button to proceed with the Employee Payroll Information upload.</p>
                <img className="offset-sm" alt="" src={ `${FRONT_END_ASSETS_URL}/assets/personal_upload_next.png` } />
                <p>
                    On the Payroll Information stage, you can download the Payroll Information CSV template.&nbsp;<br />
                    You can do this by right clicking either the download link or the download button.
                </p>
                <img alt="Download template" src={ `${FRONT_END_ASSETS_URL}/assets/payroll_download1.png` } />
                &nbsp;<br />
                <img alt="Download template" src={ `${FRONT_END_ASSETS_URL}/assets/payroll_download2.png` } />
                <p>To help you correctly fill-out the CSV template, the <A href={ `#${PAYROLL_GUIDE}` } onClick={ () => { scrollToGuideByHash( PAYROLL_GUIDE ); } }>Employee Payroll Information Upload Guide</A> is provided.</p>
                <img alt="Click here for payroll template upload guide" src={ `${FRONT_END_ASSETS_URL}/assets/payroll_click_here.png` } />
                <p>
                    After completely filling out the spreadsheet, save it as a CSV file on your personal drive.&nbsp;<br />
                    You are now ready to upload this into the system.
                </p>
                <p>On the UPLOAD Template section, choose the file you wish to upload.</p>
                <img alt="Choose file" src={ `${FRONT_END_ASSETS_URL}/assets/payroll_choose_file.png` } />
                <p>Click the UPLOAD button</p>
                <img alt="Payroll upload" src={ `${FRONT_END_ASSETS_URL}/assets/payroll_upload.png` } />
                <p>The system will validate your file against the acceptable values. You will see the status of the upload process.</p>
                <img alt="Payroll upload" src={ `${FRONT_END_ASSETS_URL}/assets/payroll_uploading.png` } />
                <p>
                    The system wil present you the errors on your CSV file, if any.&nbsp;<br />
                    You will have to edit your file to conform to the formats presented in the <A href={ `#${PAYROLL_GUIDE}` } onClick={ () => { scrollToGuideByHash( PAYROLL_GUIDE ); } }>Employee Payroll Information Upload Guide</A> to avoid such errors.
                    <br />You will also have the ability to copy the errors to your clipboard, if deemed necessary.
                </p>
                <img className="offset-sm" alt="" src={ `${FRONT_END_ASSETS_URL}/assets/payroll_error.png` } />
                <p>You have to re-upload your file after correcting the errors.</p>
                <img alt="Payroll upload validated" src={ `${FRONT_END_ASSETS_URL}/assets/payroll_upload_validated.png` } />
                <p>Once both Personal and Payroll Information file have been validated, click NEXT.</p>
                <img className="offset-sm" alt="" src={ `${FRONT_END_ASSETS_URL}/assets/payroll_upload_next.png` } />
                <p>
                    The system will create a preview of the merged files. Note that in the preview stage, all files uploaded are already checked for errors, but are not yet saved.<br />
                    You can still change these files. However, all the previously validated data will be lost when you re-upload files.
                </p>
                <img className="offset-none" alt="Merged files preview" src={ `${FRONT_END_ASSETS_URL}/assets/employee_preview.png` } />
                <p>
                    Upon clicking save, you will have the ability to add more employees and go through the steps again.&nbsp;<br />
                    If you are satisfied with your employee records, click &quot;Finish Setup&quot;.
                </p>
                <img className="offset-none" alt="Setup result" src={ `${FRONT_END_ASSETS_URL}/assets/finish setup.png` } />
            </div>
        </StyledInstructionContent>
    );
};

Instruction.propTypes = {
    scrollToGuideByHash: PropTypes.func.isRequired
};

export default Instruction;
