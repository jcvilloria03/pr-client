
/**
 * Employee Guide Batch Upload page constants
 */

export const FRONT_END_ASSETS_URL = 'https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev';
export const PERSONAL_GUIDE = 'personal-guide';
export const TIME_ATTENDANCE_GUIDE = 'time-attendance-guide';
export const PAYROLL_GUIDE = 'payroll-guide';
