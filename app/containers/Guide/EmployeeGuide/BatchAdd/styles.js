import styled from 'styled-components';

export const StyledTableContent = styled.section`
    height: 100%;
    display: contents;

    .req1 { color: #F21108; display: inline; }
    .req2 { color: #00A5E5; display: inline; }
    table {
        margin-bottom: 0;
        border: 1px solid #eceeef;

        thead {
            background: #00a5e5;
            color: #fff;
        }

        tbody {
            tr > th > .required {
                color: #eb7575;
            }
        }
    }
`;
