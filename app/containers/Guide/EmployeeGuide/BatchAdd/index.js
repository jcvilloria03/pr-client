/* eslint-disable react/prop-types */
/* eslint-disable camelcase */
import React from 'react';

import { Table } from 'reactstrap';
import { subscriptionService } from 'utils/SubscriptionService';

import { StyledTableContent } from './styles';

/**
 *
 * Batch Add Employee Guide
 *
 */
export default function ({ subscription }) {
    const is_subscribed_to_ta_only = subscription && subscriptionService.isSubscribedToTAOnly( subscription );
    const is_subscribed_to_payroll_only = subscription && subscriptionService.isSubscribedToPayrollOnly( subscription );
    const is_subscribed_to_ta_plus_payroll = subscription && subscriptionService.isSubscribedToBothPayrollAndTA( subscription );

    return (
        <StyledTableContent>
            <div id="batch_employee_guide">
                <Table>
                    <thead>
                        <tr>
                            <th>Column Name</th>
                            <th>Description</th>
                            <th>Example</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row"><span className="required">*</span> Employee ID</th>
                            <td>{ "Employee's Company ID Number" }</td>
                            <td>{ 'ID-00001' }</td>
                        </tr>
                        <tr>
                            <th scope="row"><span className="required">*</span> Last Name</th>
                            <td></td>
                            <td>Doe</td>
                        </tr>
                        <tr>
                            <th scope="row"><span className="required">*</span> First name</th>
                            <td></td>
                            <td>John</td>
                        </tr>
                        <tr>
                            <th scope="row">Middle Name</th>
                            <td></td>
                            <td>Smith</td>
                        </tr>
                        <tr>
                            <th scope="row"><span className="required">*</span> Email</th>
                            <td>{ "Employee's Valid Email Address" }</td>
                            <td>{ 'john_doe123@email.com' }</td>
                        </tr>
                        <tr>
                            <th scope="row"><span className="required">*</span> Employee Status</th>
                            <td>Possible Values: Active, Semi-Active, or Inactive NewLine Note: Activated employees will receive a notification email where they can activate their account.</td>
                            <td>Active</td>
                        </tr>
                        <tr>
                            <th scope="row">Telephone Number</th>
                            <td></td>
                            <td>88391378</td>
                        </tr>
                        <tr>
                            <th scope="row">Mobile Number</th>
                            <td></td>
                            <td>639991234567</td>
                        </tr>
                        <tr>
                            <th scope="row">Address Line 1</th>
                            <td></td>
                            <td>U702 & 704 7F Alabang Business Center Tower</td>
                        </tr>
                        <tr>
                            <th scope="row">Address Line 2</th>
                            <td></td>
                            <td>1216 Acacia Ave. Madrigal Business Park</td>
                        </tr>
                        <tr>
                            <th scope="row">Country</th>
                            <td></td>
                            <td>Philippines</td>
                        </tr>
                        <tr>
                            <th scope="row">City</th>
                            <td></td>
                            <td>Muntinlupa</td>
                        </tr>
                        <tr>
                            <th scope="row">ZIP</th>
                            <td></td>
                            <td>1781</td>
                        </tr>
                        <tr>
                            <th scope="row">Birthdate</th>
                            <td>Accepted Format: MM/DD/YYYY or YYYY-MM-DD</td>
                            <td>1988-01-01</td>
                        </tr>
                        <tr>
                            <th scope="row">Gender</th>
                            <td>Possible Values: Male or Female</td>
                            <td>Male</td>
                        </tr>
                        <tr>
                            <th scope="row">Marital Status</th>
                            <td>Possible Values: Single, Married Separated, or Widowed</td>
                            <td>Married</td>
                        </tr>
                        <tr>
                            <th scope="row">Date Hired</th>
                            <td>Accepted Format: MM/DD/YYYY or YYYY-MM-DD</td>
                            <td>2021-12-26</td>
                        </tr>
                        <tr>
                            <th scope="row">Date Resigned</th>
                            <td>Accepted Format: MM/DD/YYYY or YYYY-MM-DD</td>
                            <td>2022-05-25</td>
                        </tr>
                        <tr>
                            <th scope="row">Primary Location</th>
                            <td>To manage the list of <strong>Locations,</strong> click here</td>
                            <td>Main Office</td>
                        </tr>
                        <tr>
                            <th scope="row">Secondary Location</th>
                            <td></td>
                            <td>Branch Office</td>
                        </tr>
                        <tr>
                            <th scope="row">Employment Type</th>
                            <td>To manage the list of <strong>Employment Types,</strong> click here</td>
                            <td>Regular</td>
                        </tr>
                        <tr>
                            <th scope="row">Position</th>
                            <td>To manage the list of <strong>Positions,</strong> click here</td>
                            <td>Sales</td>
                        </tr>
                        <tr>
                            <th scope="row">Department</th>
                            <td>To manage the list of <strong>Departments,</strong> click here</td>
                            <td>Sales</td>
                        </tr>
                        <tr>
                            <th scope="row">Rank</th>
                            <td>To manage the list of <strong>Ranks,</strong> click here</td>
                            <td>Rank and File</td>
                        </tr>
                    </tbody>
                    { ( is_subscribed_to_ta_only || is_subscribed_to_ta_plus_payroll ) && (
                        <tbody>
                            <tr>
                                <th scope="row">Team</th>
                                <td>To manage the list of <strong>Teams,</strong> click here</td>
                                <td>Sales</td>
                            </tr>
                            <tr>
                                <th scope="row">Team Role</th>
                                <td></td>
                                <td>Member</td>
                            </tr>
                            <tr>
                                <th scope="row">Timesheet Required?</th>
                                <td>Possible Values: Yes or No</td>
                                <td>Yes</td>
                            </tr>
                            <tr>
                                <th scope="row">Entitled to Overtime?</th>
                                <td>Possible Values: Yes or No</td>
                                <td>Yes</td>
                            </tr>
                            <tr>
                                <th scope="row">Entitled Night Differential?</th>
                                <td>Possible Values: Yes or No</td>
                                <td>Yes</td>
                            </tr>
                            <tr>
                                <th scope="row">Entitled Unworked Regular Holiday Pay?</th>
                                <td>{"Possible Values: Yes or No NewLine Note: Will be set to 'Yes' if the timesheet column is set to 'No'"}</td>
                                <td>Yes</td>
                            </tr>
                            <tr>
                                <th scope="row">Entitled Unworked Special Holiday Pay?</th>
                                <td>{"Possible Values: Yes or No NewLine Note: Will be set to 'Yes' if the timesheet column is set to 'No'"}</td>
                                <td>Yes</td>
                            </tr>
                            <tr>
                                <th scope="row">Entitled Holiday Premium Pay?</th>
                                <td>Possible Values: Yes or No</td>
                                <td>Yes</td>
                            </tr>
                            <tr>
                                <th scope="row">Entitled Rest Day Pay?</th>
                                <td>Possible Values: Yes or No</td>
                                <td>Yes</td>
                            </tr>
                        </tbody>
                        )}

                    { ( is_subscribed_to_payroll_only || is_subscribed_to_ta_plus_payroll ) && (
                        <tbody>
                            <tr>
                                <th scope="row">Hours Per Day</th>
                                <td>Can be used if Employee Hours Per Day do not fall under the default Hours Per Day</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Tax Type</th>
                                <td>Possible Values: Regular, Minimum Wage, Consultant, or None</td>
                                <td>Regular</td>
                            </tr>
                            <tr>
                                <th scope="row">Consultant Tax Rate</th>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Tax Status</th>
                                <td>Possible Values: Z, S, S1, S2, S3, S4, M, M1, M2, M3, or M4</td>
                                <td>M1</td>
                            </tr>
                            <tr>
                                <th scope="row"><span className="req2">* </span>Base Pay</th>
                                <td></td>
                                <td>30000</td>
                            </tr>
                            <tr>
                                <th scope="row">Base Pay Adjustment Reason</th>
                                <td></td>
                                <td>Salary Raise</td>
                            </tr>
                            <tr>
                                <th scope="row"><span className="req2">* </span>Base Pay Adjustment Effective Date</th>
                                <td></td>
                                <td>2021-12-26</td>
                            </tr>
                            <tr>
                                <th scope="row">Base Pay Adjustment Date</th>
                                <td></td>
                                <td>2021-10-26</td>
                            </tr>
                            <tr>
                                <th scope="row"><span className="req2">* </span>Base Pay Unit</th>
                                <td>Possible Values: Per Hour, Per Day, Per Month, or Pay YearNo</td>
                                <td>Per Month</td>
                            </tr>
                            <tr>
                                <th scope="row">Payroll Group</th>
                                <td>To manage the list of Payroll Groups, click here</td>
                                <td>{'Semi-monthly R&F'}</td>
                            </tr>
                            <tr>
                                <th scope="row">Cost Center</th>
                                <td>To manage the list of Cost Centers, click here</td>
                                <td>Sales</td>
                            </tr>
                            <tr>
                                <th scope="row">Payment Method</th>
                                <td>Possible Values: Cash, Cheque, or Bank</td>
                                <td>Bank</td>
                            </tr>
                            <tr>
                                <th scope="row">Bank Name</th>
                                <td></td>
                                <td>Banco De Oro</td>
                            </tr>
                            <tr>
                                <th scope="row">Bank Type</th>
                                <td></td>
                                <td>Savings</td>
                            </tr>
                            <tr>
                                <th scope="row">Bank Account No.</th>
                                <td></td>
                                <td>001234567890</td>
                            </tr>
                            <tr>
                                <th scope="row">SSS Basis</th>
                                <td>Possible Values: Basic Pay, Gross Income, Fixed Monthly Amount, No Contribution, Gross Taxable, Net Basic Pay</td>
                                <td>Gross Income</td>
                            </tr>
                            <tr>
                                <th scope="row">SSS Amount</th>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">PhilHealth Basis</th>
                                <td>Possible Values: Basic Pay, Gross Income, Fixed Monthly Amount, No Contribution, Gross Taxable, Net Basic Pay</td>
                                <td>Basic Pay</td>
                            </tr>
                            <tr>
                                <th scope="row">PhilHealth Amount</th>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">HDMF Basis</th>
                                <td>Possible Values: Basic Pay, Gross Income, Fixed Monthly Amount, No Contribution, Gross Taxable, Net Basic Pay</td>
                                <td>Gross Income</td>
                            </tr>
                            <tr>
                                <th scope="row">HDMF Amount</th>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">TIN</th>
                                <td></td>
                                <td>999-999-999-999</td>
                            </tr>
                            <tr>
                                <th scope="row">RDO</th>
                                <td>BIR Revenue District Office code</td>
                                <td>53B</td>
                            </tr>
                            <tr>
                                <th scope="row">SSS Number</th>
                                <td></td>
                                <td>99-9999999-9</td>
                            </tr>
                            <tr>
                                <th scope="row">HDMF Number</th>
                                <td></td>
                                <td>9999-9999-9999</td>
                            </tr>
                            <tr>
                                <th scope="row">PhilHealth Number</th>
                                <td></td>
                                <td>99-999999999-9</td>
                            </tr>
                            <tr>
                                <th scope="row">Entitled Deminimis</th>
                                <td>Possible Values: Yes or No</td>
                                <td>Yes</td>
                            </tr>
                        </tbody>
                        )}
                </Table>
            </div>
        </StyledTableContent>
    );
}
