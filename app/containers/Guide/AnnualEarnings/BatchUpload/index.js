import React from 'react';
import Helmet from 'react-helmet';
import { Breadcrumb, BreadcrumbItem, Container } from 'reactstrap';

import A from '../../../../components/A';
import SubHeader from '../../../SubHeader';
import Button from '../../../../components/Button';
import Table from '../../../../components/Table';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../../utils/constants';
import { subscriptionService } from '../../../../utils/SubscriptionService';

import {
    EXAMPLE_COLUMNS_1,
    EXAMPLE_DATA_1,
    EXAMPLE_COLUMNS_2,
    EXAMPLE_DATA_2,
    EXAMPLE_COLUMNS_3,
    EXAMPLE_DATA_3
} from './constants';

import { ContentWrapper, GuideWrapper, NavWrapper } from './styles';

/**
 *
 * EmployeeGuide
 *
 */
export default class AnnualEarningsBatchUploadGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/annual-earnings/add' );
                            } }
                        >
                            &#8592; Back to Annual Earnings
                        </A>
                    </Container>
                </NavWrapper>
                <Helmet
                    title="Annual Earnings Batch Upload Guide"
                    meta={ [
                        { name: 'description', content: 'Description of AnnualEarningsBatchUploadGuide' }
                    ] }
                />
                <ContentWrapper>
                    <Breadcrumb>
                        <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                        <BreadcrumbItem><a href="/payroll/guides/annual-earnings/">Annual Earnings</a></BreadcrumbItem>
                        <BreadcrumbItem active>Batch Upload</BreadcrumbItem>
                    </Breadcrumb>
                    <GuideWrapper>
                        <div className="sl-c-page__header">
                            <h1 className="sl-c-page__title">Add Annual Earnings</h1>
                            <p className="sl-c-page__desc">
                                Follow the instructions below on how to add annual earnings via batch uploading
                            </p>
                        </div>

                        <div className="sl-c-page__body">
                            <div className="sl-c-section">
                                <div className="sl-c-section__header">
                                    <h5 className="sl-c-section__title">
                                        <span className="sl-c-circle">1</span> Download the Annual Earnings upload template
                                    </h5>
                                </div>

                                <div className="sl-c-section__body">
                                    <div className="sl-c-grid sl-c-grid--twos">
                                        <div className="sl-c-upload-area">
                                            <h3>Step 1:</h3>
                                            <p>A batch upload template is available for you to download and fill out.</p>
                                            <p><span className="sl-c-link">You may click here to view the upload guide.</span></p>
                                            <Button
                                                label="Download template"
                                                type="neutral"
                                            />
                                        </div>
                                        <div>
                                            <p>
                                                You can download the Annual Earnings Batch Upload Template by clicking the
                                                &#39;Download template&#39; button.
                                            </p>
                                            <p>After clicking this button, you will get a CSV file which requires information needed to upload Annual Earnings. Open the file and edit it as necessary outside the system.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="sl-c-section">
                                <div className="sl-c-section__header">
                                    <h5 className="sl-c-section__title">
                                        <span className="sl-c-circle">2</span> Fill out the CSV template
                                    </h5>
                                </div>

                                <div className="sl-c-section__body">
                                    <p>To help you correctly fill-out the CSV template, the Annual Earnings Batch
                                        Upload Guide is provided.
                                    </p>
                                    <Table
                                        columns={ EXAMPLE_COLUMNS_1 }
                                        data={ EXAMPLE_DATA_1 }
                                    />
                                </div>
                            </div>

                            <div className="sl-c-section">
                                <div className="sl-c-section__header">
                                    <h5 className="sl-c-section__title">
                                        <span className="sl-c-circle">3</span> Upload the accomplished CSV file
                                    </h5>
                                </div>

                                <div className="sl-c-section__body">
                                    <div className="sl-c-grid sl-c-grid--twos sl-u-gap-bottom--lg">
                                        <div className="sl-c-upload-area">
                                            <h3>Step 2:</h3>
                                            <p>After completely filling out the template, choose and upload it here.</p>
                                            <Button
                                                label="Choose file"
                                                type="neutral"
                                            />
                                        </div>
                                        <div>
                                            <p>After completely filling out the spreadsheet, save it as a CSV file
                                                on your personal drive. You are now ready to upload this into the system.
                                            </p>
                                            <p>On Step 2 Upload Template section, choose the file you wish to upload.
                                            </p>
                                        </div>
                                    </div>

                                    <div className="sl-c-grid sl-c-grid--twos sl-u-gap-bottom--lg">
                                        <div className="sl-c-upload-area">
                                            <h3>Step 2:</h3>
                                            <p><strong>AnnualEarnings_2017.csv</strong> <span className="sl-c-link">Replace</span></p>
                                            <Button
                                                label="Upload"
                                                type="neutral"
                                            />
                                        </div>
                                        <div>
                                            <p>Once the accomplished CSV file is choosen, click the &#39;Upload&#39; button.</p>
                                        </div>
                                    </div>

                                    <div className="sl-c-grid sl-c-grid--twos sl-u-gap-bottom--lg">
                                        <div className="sl-c-upload-area">
                                            <h3>Step 2:</h3>
                                            <p><strong>AnnualEarnings_2017.csv</strong> <span className="sl-c-link">Replace</span></p>
                                            <div className="sl-c-progress sl-c-progress--success">
                                                <div className="sl-c-progress__bar" style={ { width: '30%' } }></div>
                                            </div>
                                            <p className="sl-c-text--success">Uploading and validating</p>
                                        </div>
                                        <div>
                                            <p>The system will validate the file against the acceptable values.
                                                You will see the status upload and validation process.</p>
                                        </div>
                                    </div>

                                    <p>The system will present you the errors on your CSV file, if any.
                                        You will have to edit your file to confirm to the format presented
                                        in the Annual Earnings Batch Upload Guide to avoid such errors.
                                        You will also have the abillity to copy the errors to your clipboard,
                                        if deemed necessary.
                                    </p>

                                    <div className="sl-c-table-header">
                                        <h5 className="sl-c-section__title">Data field errors</h5>
                                        <div>
                                            <span>Showing 4 errors </span>
                                            <Button
                                                label="Copy errors"
                                                type="neutral"
                                            />
                                        </div>
                                    </div>
                                    <p>
                                        There seems to be an error in the file you uploaded. Review the list below, correct the errors and upload the file again.
                                    </p>

                                    <div className="errors">
                                        <Table
                                            columns={ EXAMPLE_COLUMNS_2 }
                                            data={ EXAMPLE_DATA_2 }
                                        />
                                    </div>

                                    <br />

                                    <div className="sl-c-grid sl-c-grid--twos sl-u-gap-bottom--lg">
                                        <div className="sl-c-upload-area">
                                            <h3>Step 2:</h3>
                                            <p><strong>AnnualEarnings_2017.csv</strong> <span className="sl-c-link">Replace</span></p>
                                            <div className="sl-c-progress sl-c-progress--success">
                                                <div className="sl-c-progress__bar" style={ { width: '30%' } }></div>
                                            </div>
                                            <p className="sl-c-text--success">Uploading and validating</p>
                                        </div>
                                        <div>
                                            <p>Once the errors are corrected, re-upload the accomplished file.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="sl-c-section">
                                <div className="sl-c-section__header">
                                    <h5 className="sl-c-section__title">
                                        <span className="sl-c-circle">4</span> Review the uploaded data
                                    </h5>
                                </div>

                                <div className="sl-c-section__body">
                                    <p>When the validation is successful, the data from the CSV will be presented
                                        on the screen. There&#39;s still an option to add more data by clicking the
                                        &#39;Add more&#39; button. Once done, click the &#39;Submit&#39; button.
                                    </p>

                                    <div className="sl-c-grid sl-c-grid--twos sl-u-gap-bottom--lg">
                                        <div className="sl-c-upload-area">
                                            <h3>Step 1:</h3>
                                            <p>A batch upload template is available for you to download and fill out.</p>
                                            <p><span className="sl-c-link">You may click here to view the upload guide.</span></p>
                                            <Button
                                                label="Download template"
                                                type="neutral"
                                            />
                                        </div>
                                        <div className="sl-c-upload-area">
                                            <h3>Step 2:</h3>
                                            <p>After completely filling out the template, choose and upload it here.</p>
                                            <p>&nbsp;</p>
                                            <Button
                                                label="Choose file"
                                                type="neutral"
                                            />
                                        </div>
                                    </div>
                                    <div className="sl-c-table-header">
                                        <h5 className="sl-c-section__title">Employee Annual Earnings List</h5>
                                        <span >Showing 1-10 of 500 entries</span>
                                    </div>
                                    <Table
                                        columns={ EXAMPLE_COLUMNS_3 }
                                        data={ EXAMPLE_DATA_3 }
                                    />
                                </div>
                            </div>
                        </div>
                    </GuideWrapper>
                </ContentWrapper>
            </div>
        );
    }
}
