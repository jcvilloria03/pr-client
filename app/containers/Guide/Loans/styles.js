import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

export const ContentWrapper = styled.div`
    display: flex;
    flex-direction: column;
`;
