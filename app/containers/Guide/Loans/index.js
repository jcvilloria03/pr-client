import React from 'react';
import Helmet from 'react-helmet';
import { Container, Breadcrumb, BreadcrumbItem } from 'reactstrap';

import { H4 } from '../../../components/Typography';
import SubHeader from '../../SubHeader';
import A from '../../../components/A';

import { browserHistory } from '../../../utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import { ContentWrapper, NavWrapper } from './styles';

/**
 *
 * LoansGuide
 *
 */
export default class LoansGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/loans/add' );
                            } }
                        >
                            &#8592;Back to Loans
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <Helmet
                        title="Loans Guide"
                        meta={ [
                            { name: 'description', content: 'Description of LoansGuide' }
                        ] }
                    />
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem active>Loans</BreadcrumbItem>
                        </Breadcrumb>
                        <H4>Loans</H4>
                        <ul>
                            <li><A href="/payroll/guides/loans/batch-upload">Batch Upload</A></li>
                        </ul>
                    </ContentWrapper>
                </Container>
            </div>
        );
    }
}
