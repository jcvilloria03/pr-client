import React from 'react';
import Helmet from 'react-helmet';
import { Container, Breadcrumb, BreadcrumbItem } from 'reactstrap';

import { H2, P } from 'components/Typography';
import Stepper from 'components/Stepper';

import { Wrapper } from 'containers/Guide/styles';
import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';

import { BASE_PATH_NAME } from '../../../../constants';

import ChangePlan from './ChangePlan';
import Add from './Add';
import Deduct from './Deduct';
import ConfirmUpdate from './ConfirmUpdate';
import FinalizeUpdate from './FinalizeUpdate';

import { HeaderWrapper } from './styles';

/**
 *
 * SubscriptionsUpdateGuide
 *
 */
class SubscriptionsUpdateGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    constructor( props ) {
        super( props );

        this.state = {
            active_step: 0
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToAnyProduct( this.props.products ) && !subscriptionService.isExpired() ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    /**
     * Generates steps config
     * @returns {Array}
     */
    getSteps() {
        return [
            'Change Account Subscription Plan',
            'Add Licenses (Optional)',
            'Deduct Licenses (Optional)',
            'Confirm Account Subscription Update',
            'Finalize Account Subscription Update'
        ].map( ( label, index ) => ({
            label,
            onClick: () => this.setState({ active_step: index })
        }) );
    }

    updateStep = ( index ) => {
        this.setState({ active_step: index });
    }

    render() {
        let section;
        switch ( this.state.active_step ) {
            case 0:
                section = <ChangePlan updateStep={ this.updateStep } />;
                break;
            case 1:
                section = <Add updateStep={ this.updateStep } />;
                break;
            case 2:
                section = <Deduct updateStep={ this.updateStep } />;
                break;
            case 3:
                section = <ConfirmUpdate updateStep={ this.updateStep } />;
                break;
            case 4:
                section = <FinalizeUpdate updateStep={ this.updateStep } />;
                break;
            default:
                section = false;
        }

        return (
            <Container>
                <Helmet
                    title="Employee Guide"
                    meta={ [
                        { name: 'description', content: 'Description of SubscriptionsUpdateGuide' }
                    ] }
                />
                <Wrapper>
                    <Breadcrumb tag="nav">
                        <BreadcrumbItem tag="a" href={ `${BASE_PATH_NAME}/guides` }>Guides</BreadcrumbItem>
                        <BreadcrumbItem tag="a" href={ `${BASE_PATH_NAME}/guides/subscriptions` }>Subscriptions</BreadcrumbItem>
                        <BreadcrumbItem active>Subscription Update</BreadcrumbItem>
                    </Breadcrumb>

                    <HeaderWrapper>
                        <H2>Subscription Update Guide</H2>
                        <P>As a user with sufficient permission, you may now manage the account&apos;s subscription plan in Control Panel &gt; Subscriptions &gt; Licenses.</P>
                    </HeaderWrapper>

                    <Stepper
                        steps={ this.getSteps() }
                        activeStep={ this.state.active_step }
                        size={ 45 }
                        circleFontSize={ 18 }
                        titleFontSize={ 16 }
                        completeColor="#818a91"
                        completeTitleColor="#818a91"
                        activeColor="#00a5e5"
                        activeTitleColor="#00a5e5"
                        useIndex
                        useNumbers
                    />

                    { section }

                </Wrapper>
            </Container>
        );
    }
}

export default SubscriptionsUpdateGuide;
