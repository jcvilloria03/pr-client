import styled from 'styled-components';

export const HeaderWrapper = styled.header`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    h2 {
        font-weight: 600;
    }

    p {
        width: 50%;
        font-size: 16px;
        font-weight: 500;
        letter-spacing: normal;
        text-align: center;
        margin-bottom: 0px;
    }
`;

export const RowWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    margin-bottom: 30px;

    & > div {
        width: 50%;
        margin: 5px;
    }

    & > div:first-child {
        border: 2px dashed #ccc;
        border-radius: 12px;

        div:nth-child( 2 ) {
            border-top: 2px dashed #ccc;
        }
    }
`;

export const ImageWrapper = styled.div`
    width: 100%;
    padding: 10px;

    img {
        width: 100%;
        object-fit: contain;
    }
`;

export const NoteWrapper = styled.aside`
    padding: 16px;
    background-color: #eceeef;
    border-radius: 12px;
    font-size: 16px;
    font-weight: 500;
    margin-bottom: 20px;

    p {
        color: #4e4e4e;
    }

    span {
        color: rgba(255, 0, 0, 0.5)
    }
`;

export const TableWrapper = styled.div`
    .underline {
        text-decoration: underline;
    }

    thead {
        background-color: #00a5e5;
        color: #fff;

        th {
            vertical-align: middle;
            text-align: center;
        }
    }

    td {
        vertical-align: middle;

        &[rowspan] {
            border-right: 1px solid #eceeef;
            border-left: 1px solid #eceeef;
        }
    }
`;
