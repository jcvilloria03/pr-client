import React from 'react';

import Button from 'components/Button';

import {
    RowWrapper,
    ImageWrapper,
    NoteWrapper
} from '../styles';
import { FRONT_END_ASSETS_URL } from '../constants';

/**
 *
 * Finalize Update section Component
 *
 */
function FinalizeUpdate( props ) {
    return (
        <div>
            <RowWrapper>
                <div>
                    <ImageWrapper>
                        <img src={ `${FRONT_END_ASSETS_URL}/subscription_update_finalize_update_1.png` } alt="Finalize Account Subscription Update Guide 1" />
                    </ImageWrapper>
                    <ImageWrapper>
                        <img src={ `${FRONT_END_ASSETS_URL}/subscription_update_finalize_update_2.png` } alt="Finalize Account Subscription Update Guide 2" />
                    </ImageWrapper>
                </div>

                <div>
                    <ul>
                        <li>In the Update Subscription Summary pop-up, click the &apos;Submit&apos; button to finalize the change.</li>
                        <li>You may opt out of any changes you&apos;ve made by clicking the &apos;Cancel&apos; button.</li>
                    </ul>

                    <NoteWrapper>
                        <span>Important note:</span>
                        <p>
                            If you are a paid subscriber, if you deducted licenses or downgraded
                            your subscription plan, a draft of the next billing will be displayed
                            after you click &apos;Submit&apos;. You may opt to cancel the changes you&apos;ve made
                            by clicking &apos;Cancel&apos; as long as your current billing period has not lapsed.
                        </p>
                    </NoteWrapper>

                    <Button
                        type="action"
                        alt
                        label="Back"
                        onClick={ () => props.updateStep( 3 ) }
                    />
                </div>
            </RowWrapper>
        </div>
    );
}

FinalizeUpdate.propTypes = {
    updateStep: React.PropTypes.func
};

export default FinalizeUpdate;
