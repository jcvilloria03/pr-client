import React from 'react';

import Button from 'components/Button';

import {
    RowWrapper,
    ImageWrapper,
    NoteWrapper
} from '../styles';
import { FRONT_END_ASSETS_URL } from '../constants';

/**
 *
 * Deduct Licenses section Component
 *
 */
function DeductLicenses( props ) {
    return (
        <div>
            <RowWrapper>
                <div>
                    <ImageWrapper>
                        <img src={ `${FRONT_END_ASSETS_URL}/subscription_update_deduct_licenses.png` } alt="Deduct Licenses Guide" />
                    </ImageWrapper>
                </div>

                <div>
                    <ul>
                        <li><b>This step is optional.</b></li>
                        <li>Click the (-) button to add licenses one at a time or manually update the amount in the text field.</li>
                    </ul>

                    <NoteWrapper>
                        <span>Important note:</span>
                        <p>
                            Changes when deducting licenses on free subscriptions will
                            take change immediately once submitted. For paid subscriptions,
                            changes will take effect on the next billing period.
                        </p>
                    </NoteWrapper>

                    <Button
                        type="action"
                        alt
                        label="Back"
                        onClick={ () => props.updateStep( 1 ) }
                    />
                    <Button
                        type="action"
                        label="Next"
                        onClick={ () => props.updateStep( 3 ) }
                    />
                </div>
            </RowWrapper>
        </div>
    );
}

DeductLicenses.propTypes = {
    updateStep: React.PropTypes.func
};

export default DeductLicenses;
