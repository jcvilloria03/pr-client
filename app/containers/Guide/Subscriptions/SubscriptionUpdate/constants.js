/**
 * Subscriptions Guide page constants
 */

export const FRONT_END_ASSETS_URL = 'https://frontendv3-assets-dev.s3-us-west-2.amazonaws.com/assets';
