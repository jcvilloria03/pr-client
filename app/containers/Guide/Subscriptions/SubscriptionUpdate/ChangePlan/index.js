import React from 'react';

import { Table } from 'reactstrap';

import Button from 'components/Button';

import {
    RowWrapper,
    ImageWrapper,
    TableWrapper,
    NoteWrapper
} from '../styles';
import { FRONT_END_ASSETS_URL } from '../constants';

/**
 *
 * Change Plan section Component
 *
 */
function ChangePlan( props ) {
    return (
        <div>
            <RowWrapper>
                <div>
                    <ImageWrapper>
                        <img src={ `${FRONT_END_ASSETS_URL}/subscription_update_change_plan.png` } alt="Change Account Subscription Plan Guide" />
                    </ImageWrapper>
                </div>

                <div>
                    <ul>
                        <li>Click the drop-down arrow and select the appropriate subscription plan you wish to assign to the account.</li>
                        <li>Select the appropriate subscription plan you wish to assign to the account.</li>
                    </ul>

                    <NoteWrapper>
                        <span>Important note:</span>
                        <p>
                            Changing the account subscription plan may automatically
                            assign or unseat licenses for specific users. For further
                            details, please refer to the table below:
                        </p>
                    </NoteWrapper>

                    <Button
                        type="action"
                        label="Next"
                        onClick={ () => props.updateStep( 1 ) }
                    />
                </div>
            </RowWrapper>
            <TableWrapper>
                <Table>
                    <thead>
                        <tr>
                            <th>Scenario</th>
                            <th>Free Subscriptions</th>
                            <th>Paid Subscriptions</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Upgrade from <span className="underline">Payroll only</span> to <span className="underline">T&amp;A and Payroll</span></td>
                            <td rowSpan={ 6 }>Changes will take effect immediately</td>
                            <td>Changes will take effect immediately, &amp; owner will be billed for it on the current billing cycle.</td>
                            <td>
                                <ul>
                                    <li>User with a role of Account Owner will auto update their licenses from Payroll to T&amp;A and Payroll</li>
                                    <li>All users with assigned Licenses of Payroll will retain their licenses</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Upgrade from <span className="underline">T&amp;A only</span> to <span className="underline">T&amp;A and Payroll</span></td>
                            <td>Changes will take effect immediately, &amp; owner will be billed for it on the current billing cycle.</td>
                            <td>
                                <ul>
                                    <li>User with a role of Account Owner will auto update their licenses from T&amp;A to T&amp;A and Payroll</li>
                                    <li>All users with assigned Licenses of T&amp;A will retain their licenses</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Downgrade from <span className="underline">T&amp;A and Payroll</span> to <span className="underline">Payroll only</span></td>
                            <td>Changes will take effect on the Next billing period</td>
                            <td>
                                <ul>
                                    <li>User with a role of Account Owner will auto update their licenses from T&amp;A and Payroll to Payroll only</li>
                                    <li>All users with assigned Licenses of Payroll only will retain</li>
                                    <li>All users with assigned licenses of T&amp;A only or T&amp;A and Payroll, system will auto unassigned their licenses</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Downgrade from <span className="underline">T&amp;A and Payroll</span> to <span className="underline">T&amp;A only</span></td>
                            <td>Changes will take effect on the Next billing period</td>
                            <td>
                                <ul>
                                    <li>User with a role of Account Owner will auto update their licenses from T&amp;A and Payroll to T&amp;A only</li>
                                    <li>All users with assigned Licenses of T&amp;A only will retain</li>
                                    <li>All users with assigned licenses of Payroll only or T&amp;A and Payroll, system will auto unassigned their licenses</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Switch from <span className="underline">T&amp;A only</span> to <span className="underline">Payroll only</span></td>
                            <td>Changes will take effect on the Next billing period</td>
                            <td>
                                <ul>
                                    <li>User with a role of Account Owner will auto update their licenses from T&amp;A to Payroll only</li>
                                    <li>All users with assigned licenses of T&amp;A, system will auto unassigned their licenses</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Switch from <span className="underline">Payroll only</span> to <span className="underline">T&amp;A only</span></td>
                            <td>Changes will take effect on the Next billing period</td>
                            <td>
                                <ul>
                                    <li>User with a role of Account Owner, system will auto update their licenses from Payroll to T&amp;A only</li>
                                    <li>All users with assigned licenses of Payroll, system will auto unassigned their licenses</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </Table>
            </TableWrapper>
        </div>
    );
}

ChangePlan.propTypes = {
    updateStep: React.PropTypes.func
};

export default ChangePlan;
