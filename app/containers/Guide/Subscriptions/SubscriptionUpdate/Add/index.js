import React from 'react';

import Button from 'components/Button';

import {
    RowWrapper,
    ImageWrapper,
    NoteWrapper
} from '../styles';
import { FRONT_END_ASSETS_URL } from '../constants';

/**
 *
 * Add Licenses section Component
 *
 */
function AddLicenses( props ) {
    return (
        <div>
            <RowWrapper>
                <div>
                    <ImageWrapper>
                        <img src={ `${FRONT_END_ASSETS_URL}/subscription_update_add_licenses.png` } alt="Add Licenses Guide" />
                    </ImageWrapper>
                </div>

                <div>
                    <ul>
                        <li><b>This step is optional.</b></li>
                        <li>Click the (+) button to add licenses one at a time or manually update the amount in the text field.</li>
                    </ul>

                    <NoteWrapper>
                        <span>Important note:</span>
                        <p>
                            Adding licenses will take effect immediately once submitted.
                        </p>
                    </NoteWrapper>

                    <Button
                        type="action"
                        alt
                        label="Back"
                        onClick={ () => props.updateStep( 0 ) }
                    />
                    <Button
                        type="action"
                        label="Next"
                        onClick={ () => props.updateStep( 2 ) }
                    />
                </div>
            </RowWrapper>
        </div>
    );
}

AddLicenses.propTypes = {
    updateStep: React.PropTypes.func
};

export default AddLicenses;
