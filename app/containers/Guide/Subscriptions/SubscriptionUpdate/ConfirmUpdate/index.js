import React from 'react';

import Button from 'components/Button';

import {
    RowWrapper,
    ImageWrapper
} from '../styles';
import { FRONT_END_ASSETS_URL } from '../constants';

/**
 *
 * Confirm Update section Component
 *
 */
function ConfirmUpdate( props ) {
    return (
        <div>
            <RowWrapper>
                <div>
                    <ImageWrapper>
                        <img src={ `${FRONT_END_ASSETS_URL}/subscription_update_confirm_update.png` } alt="Confirm Account Subscription Update Guide" />
                    </ImageWrapper>
                </div>

                <div>
                    <ul>
                        <li><b>This step is optional.</b></li>
                        <li>Click the &apos;Confirm&apos; button after you have selected the correct subscription plan and/or if you have added or deducted licenses.</li>
                    </ul>

                    <Button
                        type="action"
                        alt
                        label="Back"
                        onClick={ () => props.updateStep( 2 ) }
                    />
                    <Button
                        type="action"
                        label="Next"
                        onClick={ () => props.updateStep( 4 ) }
                    />
                </div>
            </RowWrapper>
        </div>
    );
}

ConfirmUpdate.propTypes = {
    updateStep: React.PropTypes.func
};

export default ConfirmUpdate;
