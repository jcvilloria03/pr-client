import React from 'react';
import Helmet from 'react-helmet';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';

import { H4 } from 'components/Typography';
import A from 'components/A';

import { browserHistory } from 'utils/BrowserHistory';
import { subscriptionService } from 'utils/SubscriptionService';

import { BASE_PATH_NAME } from '../../../constants';

import { Wrapper, ContentWrapper } from '../styles';

/**
 *
 * Subscriptions Guide
 *
 */
export class SubscriptionsGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToAnyProduct( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <Helmet
                    title="Subscriptions Guide"
                    meta={ [
                        { name: 'description', content: 'Description of Subscriptions Guide' }
                    ] }
                />
                <Wrapper>
                    <ContentWrapper>
                        <Breadcrumb tag="nav">
                            <BreadcrumbItem tag="a" href={ `${BASE_PATH_NAME}/guides` }>Guides</BreadcrumbItem>
                            <BreadcrumbItem active>Subscriptions</BreadcrumbItem>
                        </Breadcrumb>
                        <H4>Subscriptions</H4>
                        <ul>
                            <li><A href={ `${BASE_PATH_NAME}/guides/subscriptions/update` }>Subscription Update</A></li>
                        </ul>
                    </ContentWrapper>
                </Wrapper>
            </div>
        );
    }
}

export default SubscriptionsGuide;
