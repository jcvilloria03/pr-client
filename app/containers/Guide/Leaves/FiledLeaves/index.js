import React from 'react';
import Helmet from 'react-helmet';
import { Container, Breadcrumb, BreadcrumbItem } from 'reactstrap';

import { H4 } from '../../../../components/Typography';
import SubHeader from '../../../SubHeader';
import A from '../../../../components/A';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../../utils/constants';
import { subscriptionService } from '../../../../utils/SubscriptionService';

import { ContentWrapper, NavWrapper } from './styles';

/**
 *
 * Filed LeavesGuide
 *
 */
export default class FiledLeavesGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/leaves', true );
                            } }
                        >
                            &#8592;Back to Leaves
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <Helmet
                        title="Filed Leaves Guide"
                        meta={ [
                            { name: 'description', content: 'Description of Filed Leaves Guide' }
                        ] }
                    />
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/leaves">Leaves</a></BreadcrumbItem>
                            <BreadcrumbItem active>Filed Leaves</BreadcrumbItem>
                        </Breadcrumb>
                        <H4>Filed Leaves</H4>
                        <ul>
                            <li><A href="/payroll/guides/leaves/filed-leaves/batch-upload">Batch Upload</A></li>
                        </ul>
                    </ContentWrapper>
                </Container>
            </div>
        );
    }
}
