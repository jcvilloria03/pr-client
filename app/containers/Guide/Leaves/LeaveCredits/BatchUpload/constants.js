export const EXAMPLE_COLUMNS_1 = [
    {
        accessor: 'columnName',
        header: 'Column Name',
        sortable: false
    },
    {
        accessor: 'description',
        header: 'Description',
        sortable: false
    },
    {
        accessor: 'requiredField',
        header: 'Required field?',
        sortable: false
    },
    {
        accessor: 'sampleValue',
        header: 'Sample values',
        sortable: false
    }
];

export const EXAMPLE_DATA_1 = [
    {
        columnName: 'Column name goes here',
        description: 'Description goes here',
        requiredField: 'Yes',
        sampleValue: 'Sample value goes here'
    },
    {
        columnName: 'Column name goes here',
        description: 'Description goes here',
        requiredField: 'Yes',
        sampleValue: 'Sample value goes here'
    }
];

export const EXAMPLE_COLUMNS_2 = [
    {
        accessor: 'rowNumber',
        header: 'Row Number',
        sortable: false
    },
    {
        accessor: 'errorType',
        header: 'Error Type',
        sortable: false
    }
];

export const EXAMPLE_DATA_2 = [
    {
        rowNumber: 'Row 10',
        errorType: 'Error message goes here'
    },
    {
        rowNumber: 'Row 12',
        errorType: 'Error message goes here'
    },
    {
        rowNumber: 'Row 14',
        errorType: 'Error message goes here'
    },
    {
        rowNumber: 'Row 20',
        errorType: 'Error message goes here'
    }
];

export const EXAMPLE_COLUMNS_3 = [
    {
        accessor: 'columnName1',
        header: 'Column name',
        sortable: true
    },
    {
        accessor: 'columnName2',
        header: 'Column name',
        sortable: true
    },
    {
        accessor: 'columnName3',
        header: 'Column name',
        sortable: true
    },
    {
        accessor: 'columnName4',
        header: 'Column name',
        sortable: true
    },
    {
        accessor: 'columnName5',
        header: 'Column name',
        sortable: true
    },
    {
        accessor: 'columnName6',
        header: 'Column name',
        sortable: true
    }
];

export const EXAMPLE_DATA_3 = [
    {
        columnName1: 'Data name',
        columnName2: 'Data name',
        columnName3: 'Data name',
        columnName4: 'Data name',
        columnName5: 'Data name',
        columnName6: 'Data name'
    },
    {
        columnName1: 'Data name',
        columnName2: 'Data name',
        columnName3: 'Data name',
        columnName4: 'Data name',
        columnName5: 'Data name',
        columnName6: 'Data name'
    },
    {
        columnName1: 'Data name',
        columnName2: 'Data name',
        columnName3: 'Data name',
        columnName4: 'Data name',
        columnName5: 'Data name',
        columnName6: 'Data name'
    },
    {
        columnName1: 'Data name',
        columnName2: 'Data name',
        columnName3: 'Data name',
        columnName4: 'Data name',
        columnName5: 'Data name',
        columnName6: 'Data name'
    }
];

