import React from 'react';
import Helmet from 'react-helmet';
import { Container, Breadcrumb, BreadcrumbItem } from 'reactstrap';

import { H4 } from '../../../components/Typography';
import SubHeader from '../../SubHeader';
import A from '../../../components/A';

import { browserHistory } from '../../../utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import { ContentWrapper, NavWrapper } from './styles';

/**
 *
 * LeaveCreditsGuide
 *
 */
export default class LeaveCreditsGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/leaves', true );
                            } }
                        >
                            &#8592;Back to Leaves
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <Helmet
                        title="Leave Credits Guide"
                        meta={ [
                            { name: 'description', content: 'Description of Leave Credits Guide' }
                        ] }
                    />
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem active>Leaves</BreadcrumbItem>
                        </Breadcrumb>
                        <H4>Leaves</H4>
                        <ul>
                            <li><A href="/payroll/guides/leaves/leave-credits">Leave Credits</A></li>
                            <li><A href="/payroll/guides/leaves/filed-leaves">Filed Leaves</A></li>
                        </ul>
                    </ContentWrapper>
                </Container>
            </div>
        );
    }
}
