import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
`;

export const ContentWrapper = styled.div`
    display: flex;
    flex-direction: column;

.sl-l-page-content-wrap {
    display: flex;
    flex-direction: column;
    flex-grow: 1;
}

.sl-l-app-content {
  flex-grow: 1;
  padding-top: 2rem;
}

.sl-u-container {
    max-width: 80vw;
    margin-right: auto;
    margin-left: auto;
}

.sl-c-section {
    &:not(:first-child) {
        margin-top: 1.5rem;
    }

    &.sl-is-empty {
        text-align: center;

        .sl-c-icon-circle {
            width: 9rem;
            height: 9rem;
            color: #ffffff;
            background-color: #00A5E5;

            .sl-c-icon {
                width: 4rem;
                height: 4rem;
            }
        }
    }
}

.sl-c-section__header {
    margin-bottom: 4rem;
}

.sl-c-badge {
    width: 2.5rem;
    height: 2.5rem;
}

.sl-u-text--center {
    text-align: center;
}

.sl-l-schedules {
    .sl-c-col--wrapper {
        width: 100%;
    }

    .sl-l-page-content-wrap {
        .sl-l-app-nav {
            + div {
                display: flex;
                flex-direction: column;
                flex-grow: 1;
            }
        }
    }

    .sl-l-app-content {
        .sl-c-btn-group {
            .sl-c-btn {
                width: 50%;
            }
        }
    }

    .sl-c-table-header {
        flex-wrap: wrap;

        .sl-c-btn-group {
            width: auto;

            .sl-c-btn {
                width: auto;
            }
        }

        .sl-c-panel {
            width: 100%;
            margin-top: 1rem;
        }
    }

    .sl-c-panel-dropdown {
        &:before,
        &:after {
            right: 17.5rem;
        }
    }
}



.sl-c-col--wrapper {
  display: flex;
  flex-wrap: wrap;
  margin-right: -0.5rem;
  margin-left: -0.5rem;
  list-style: none;
}

.sl-c-col--1 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 1 / 12);
  width: calc(100% * 1 / 12);
}
.sl-c-col--2 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 2 / 12);
  width: calc(100% * 2 / 12);
}
.sl-c-col--3 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 3 / 12);
  width: calc(100% * 3 / 12);
}
.sl-c-col--4 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 4 / 12);
  width: calc(100% * 4 / 12);
}
.sl-c-col--5 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 5 / 12);
  width: calc(100% * 5 / 12);
}
.sl-c-col--6 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 6 / 12);
  width: calc(100% * 6 / 12);
}
.sl-c-col--7 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 7 / 12);
  width: calc(100% * 7 / 12);
}
.sl-c-col--8 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 8 / 12);
  width: calc(100% * 8 / 12);
}
.sl-c-col--9 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 9 / 12);
  width: calc(100% * 9 / 12);
}
.sl-c-col--10 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 10 / 12);
  width: calc(100% * 10 / 12);
}
.sl-c-col--11 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 11 / 12);
  width: calc(100% * 11 / 12);
}
.sl-c-col--12 {
  padding: 0 0.5rem 0.5rem 0.5rem;
  flex: 0 0 calc(100% * 12 / 12);
  width: calc(100% * 12 / 12);
}
.sl-c-col--align-center {
  align-items: center;
}

.sl-c-progress {
    position: relative;
    display: block;
    height: 1rem;
    overflow: hidden;
    border: 1px solid #cccccc;
    border-radius: 1rem;
}

.sl-c-progress__bar {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    margin: 2px;
    border-radius: 1rem;
    transition: width .2s ease-in-out;
}

.sl-c-progress--success {
    .sl-c-progress__bar {
        background-color: #83D24B;
    }
}

.sl-c-btn {
    display: inline-block;
    padding: 1rem 2rem;
    color: #474747;
    text-align: center;
    font-size: 1rem;
    line-height: 1.6;
    text-decoration: none;
    vertical-align: middle;
    border-radius: 2rem;
    background-color: transparent;
    border: 1px solid transparent;
    transition: all .2s ease-in-out;

    &:hover,
    &:focus,
    &:active,
    &.sl-is-active {
        cursor: pointer;
    }

    &[disabled],
    &.sl-is-disabled {
        cursor: not-allowed;
        pointer-events: none;
    }
}


.sl-c-btn--primary {
    color: #ffffff;
    background-color: #83D24B

    &:hover,
    &:focus,
    &:active,
    &.sl-is-active {
        background-color: lighten(#83D24B, 10%);
    }

    &[disabled],
    &.sl-is-disabled {
        color: #fff;
        background-color: lighten(#83D24B, 15%);
    }

    &.sl-c-btn--ghost {
        color: #474747;
        border-color: #83D24B;
        background-color: #fff;

        &:hover,
        &:focus,
        &:active,
        &.sl-is-active {
            background-color: lighten(#83D24B, 75%);
        }

        &[disabled],
        &.sl-is-disabled {
            color: #ADADAD;
            border-color: #83D24B;

            &:hover,
            &:focus,
            &:active {
                background-color: transparent;
            }
        }
    }
}

.sl-c-table-header,
.sl-c-table-options {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 1rem;
}

.sl-c-table-header {

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        margin-bottom: 0;
        word-break: break-all;
    }

    &.sl-is-flex-left {
        justify-content: flex-start;

        .sl-u-actions {
            flex-grow: 1;
            text-align: right;
        }
    }

    .sl-u-actions {
        .sl-c-dropdown-menu {
            margin-top: .5rem;
            background-color: #fff;
            border: 1px solid #e5e5e5;

            a {
                padding: .5rem 1rem;

                &.sl-is-active,
                &:hover {
                    border-left-color: transparent;
                    background-color: #F0F4F6;
                }
            }

            >.sl-c-btn {
                padding: 1rem-sm 1rem;
                border-radius: 0;

                &:hover {
                    border-left-color: transparent;
                    background-color: #F0F4F6;
                }
            }
        }
    }
}


.sl-c-icon {
    margin-left: .5rem;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 1rem;
    height: 1rem;
    fill: currentColor;
    vertical-align: -.15rem;
}

.sl-c-icon-circle {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 2rem;
    height: 2rem;
    border-radius: 50%;
    background-color: #F0F4F6;
    color: #ADADAD;
}

.sl-c-icon-circled {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 2rem;
    height: 2rem;
    border-radius: 50%;
    border: #ADADAD;
    color: #ADADAD;
}

.s-c-icon-circle--sm {
    width: 1.5rem;
    height: 1.5rem;

    .sl-c-icon {
        width: .75rem;
        height: .75rem;
    }
}

.sl-c-icon-circle--md {
    width: 3rem;
    height: 3rem;

    .sl-c-icon {
        width: 1.5rem;
        height: 1.5rem;
    }
}

.sl-c-icon--xs {
    width: .5rem;
    height: .5rem;
    vertical-align: 0;
}

.sl-c-icon--sm {
    width: .75rem;
    height: .75rem;
}

.sl-c-icon--lg {
    width: 2rem;
    height: 2rem;
}

.sl-c-icon-badge {
    width: 12px;
    background-color: #EB7575;
    height: 12px;
    border-radius: 50%;
    position: relative;
    right: 12px;
    top: 0px;
}





.sl-c-table__sortable {
    cursor: pointer;
}

.sl-u-no-pointer-events {
    cursor: none;
    pointer-events: none;
}


select {
    &.sl-c-form-control {
        height: 3rem;
    }

    &.sl-c-form-control--md {
        height: 2.5rem;
    }
}

.sl-c-form-control--md {
    min-height: 2.5rem;
    padding: .25rem;
}

.sl-c-table-options {
    margin-top: .5rem;
    margin-bottom: 2rem;

    .sl-c-form-control {
        min-width: 5rem !important;
        border: 1px solid;
        margin: 0 0.5rem;
    }

    .sl-c-btn {
        padding: .5rem 1rem;
        margin-left: .5rem;
    }
}

.sl-c-badge {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    padding: 0 .5rem;
    border-radius: 50%;
    border: 1px solid transparent;
}

.sl-c-badge--circled {
    border-color: #474747;
}

.sl-c-badge--info {
    border-color: #00A5E5;
    color: #00A5E5;
}


.sl-c-table {
    width: 100%;
    margin-bottom: 1rem;
    border-collapse: collapse;

    > thead {
        > tr {
            color: #ffffff;

            > th,
            > td {
                height: 4.5rem;
                padding: 1rem;
                white-space: nowrap;
                text-align: left;
                background-color: #00A5E5;

                &.sl-u-text--center {
                    text-align: center;
                }

                &.sl-u-text--right {
                    text-align: right;
                }

                &.sl-is-narrow {
                    width: 1.5rem;
                }

                &.sl-is-wide {
                    min-width: 500px;
                }
            }

            &.sl-has-error {
                > th,
                > td {
                    color: #ffffff;
                    background-color: #EB7575;
                }
            }

            &.sl-c-alert {
                padding-right: 0;

                > th,
                > td {
                    height: auto;
                    text-align: center;
                }
            }

            &.sl-c-alert--info {
                > th,
                > td {
                    color: #474747;
                    background-color: #E5F6FC;
                }
            }

            &.sl-c-alert--danger,
            &.sl-c-alert--warning,
            &.sl-c-alert--error {
                > th,
                > td {
                    color: #ffffff;
                    background-color: #EB7575;
                }
            }

            .sl-c-check,
            .sl-c-radio {
                + .sl-c-control-label {
                    margin-right: 0;
                }
            }
        }
    }

    > tbody {
        > tr {
            border-left: 2px solid transparent;

            &:nth-child(even) {
                background-color: #F0F4F6;
            }

            &.sl-is-selected {
                border-left-color: #00A5E5;
                background-color: transparentize(#83D24B, .85);
            }

            &.sl-has-error {
                color: #ffffff;
                background-color: #EB7575;
            }

            &.sl-c-table-detail {
                > td {
                    padding: 0;
                }
            }

            > td {
                min-width: 7rem;
                height: 4.5rem;
                padding: 1rem;

                &.sl-is-short {
                    min-width: 4rem;
                }

                &.sl-is-wide {
                    min-width: 12rem;
                }

                &.custom-td-for-user-management{
                    vertical-align: initial;
                    padding-top: 1.25rem;
                }
            }
        }

        .sl-c-check {
            + .sl-c-control-label {
                margin-right: 2rem;
            }
        }
    }

    .sl-u-indent-1 {
        td {
            &:first-child {
                .sl-c-check {
                    + .sl-c-control-label {
                        margin-left: 4rem;
                    }
                }
            }
        }
    }

    .sl-u-indent-2 {
        td {
            &:first-child {
                .sl-c-check {
                    + .sl-c-control-label {
                        margin-left: 8rem;
                    }
                }
            }
        }
    }

    .sl-c-check {
        + .sl-c-control-label {
            &:empty {
                margin-bottom: 0;
            }
        }
    }
}
`;

export const Footer = styled.div`
    margin-top: 1rem;
    padding: 1rem 0;
    background-color: #F0F4F6;

    > .sl-u-container {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .sl-c-form-group {
        margin-top: 0;

        &:only-child {
            flex-grow: 1;
        }
    }

    .sl-c-btn {
    display: inline-block;
    padding: 1rem 2rem;
    color: #474747;
    text-align: center;
    font-size: 1rem;
    line-height: 1.6;
    text-decoration: none;
    vertical-align: middle;
    border-radius: 2rem;
    background-color: transparent;
    border: 1px solid transparent;
    transition: all .2s ease-in-out;
    margin-left: .25rem;

    &:hover,
    &:focus,
    &:active,
    &.sl-is-active {
        cursor: pointer;
    }

    &[disabled],
    &.sl-is-disabled {
        cursor: not-allowed;
        pointer-events: none;
    }
}

    .sl-c-btn--primary {
        color: #ffffff;
        background-color: #83D24B

        &:hover,
        &:focus,
        &:active,
        &.sl-is-active {
            background-color: #83D24B
        }

        &[disabled],
        &.sl-is-disabled {
            color: #fff;
            background-color: #83D24B
        }

        &.sl-c-btn--ghost {
            color: #474747;
            border-color: #83D24B;
            background-color: #fff;

            &:hover,
            &:focus,
            &:active,
            &.sl-is-active {
                background-color: #83D24B
            }

            &[disabled],
            &.sl-is-disabled {
                color: #ADADAD;
                border-color: #83D24B;

                &:hover,
                &:focus,
                &:active {
                    background-color: transparent;
                }
            }
        }
    }

`;
