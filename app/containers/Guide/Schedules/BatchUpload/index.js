/* eslint-disable jsx-a11y/href-no-hash */
import React from 'react';
import Helmet from 'react-helmet';
import { Container, Breadcrumb, BreadcrumbItem } from 'reactstrap';

import A from 'components/A';
import Icon from 'components/Icon';

import { browserHistory } from 'utils/BrowserHistory';
import { TIME_AND_ATTENDANCE_SUBHEADER_ITEMS } from 'utils/constants';
import { subscriptionService } from 'utils/SubscriptionService';

import SubHeader from '../../../SubHeader';
import { ContentWrapper, NavWrapper, Footer } from './styles';

/**
 *
 * EarningsGuide
 *
 */
export default class EarningsGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <SubHeader items={ TIME_AND_ATTENDANCE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/time/schedules/add', true );
                            } }
                        >
                            &#8592;Back to Batch Upload
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <Helmet
                        title="Schedules Guide"
                        meta={ [
                            { name: 'description', content: 'Description of Schedules Batch Add Guide' }
                        ] }
                    />
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem active>Schedules</BreadcrumbItem>
                        </Breadcrumb>
                        <main className="sl-l-app-content">
                            <div className="sl-u-container">
                                <div className="sl-c-section">
                                    <div className="sl-c-section__header sl-u-text--center">
                                        <h2 className="sc-l-section__title">
                                    Schedules Batch Add Guide
                                    </h2>
                                        <p>This page guides you in uploading your schedules to the system through the Schedules Batch Upload Template.</p>
                                    </div>
                                </div>

                                <div className="sl-c-section">
                                    <div className="sl-c-section__header">
                                        <h5 className="sc-l-section__title">
                                            <span className="sl-c-badge sl-c-badge--circled sl-u-gap-right--sm">1</span> Download the Schedules Batch Upload Template
                                    </h5>
                                    </div>

                                    <div className="sl-c-col--wrapper">
                                        <div className="sl-c-col--6">
                                            <div className="sl-c-upload-area sl-u-text--center">
                                                <div className="sl-c-upload-area__wrap">
                                                    <h3>Step 1:</h3>
                                                    <p className="sl-u-help-text sl-u-gap-bottom">
                                            A batch upload template is available for you to download and fill out. <br />
                                                        <a href="#" className="sl-u-no-pointer-events">You may click here to view the upload guide.</a>
                                                    </p>

                                                    <div className="sl-u-center">
                                                        <button type="button" className="sl-c-btn sl-c-btn--primary sl-c-btn--ghost sl-c-btn--md sl-u-no-pointer-events">
                                                Download Template
                                            </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="sl-c-col--6">
                                            <p>{'You can download the Schedules Batch Upload Template by clicking "Download template."'}</p>

                                            <p>What you will get is a CSV file containing column headers that indicate which schedule-related information are needed by the system. Provide the needed information by filling out the columns.</p>
                                        </div>
                                    </div>
                                </div>

                                <div className="sl-c-section">
                                    <div className="sl-c-section__header">
                                        <h5 className="sc-l-section__title">
                                            <span className="sl-c-badge sl-c-badge--circled sl-u-gap-right--sm">2</span> Fill out the CSV template
                                    </h5>
                                    </div>

                                    <p>To help you fill out the batch upload template, we listed down the descriptions of the column headers as well as sample entries for each of them.</p>

                                    <table className="sl-c-table sl-u-gap-top">
                                        <thead>
                                            <tr>
                                                <th>Column name</th>
                                                <th>Description</th>
                                                <th>Required field?</th>
                                                <th>Sample values</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>Schedule Validity Start Date</td>
                                                <td>When the schedule is valid to be tagged as a shift to employee/s starting this date</td>
                                                <td>Yes</td>
                                                <td>Apr-02-2018</td>
                                            </tr>
                                            <tr>
                                                <td>Schedule Name</td>
                                                <td>Name or the schedule record</td>
                                                <td>Yes</td>
                                                <td>Opening; Closing</td>
                                            </tr>
                                            <tr>
                                                <td>Start Time</td>
                                                <td>In 24-hour format what time the schedule starts</td>
                                                <td>Yes</td>
                                                <td>06:00; 13:00</td>
                                            </tr>
                                            <tr>
                                                <td>End Time</td>
                                                <td>In 24-hour format what time the schedule ends</td>
                                                <td>Yes</td>
                                                <td>15:00; 22:00</td>
                                            </tr>
                                            <tr>
                                                <td>Schedule Type</td>
                                                <td>Either a Fixed or Flexi type</td>
                                                <td>Yes</td>
                                                <td>Fixed</td>
                                            </tr>
                                            <tr>
                                                <td>Core Start Time</td>
                                                <td>Schedule Type is Flexi. In 24-hour format when the start of a core time frame; Can add more core start time provided that there is a core end time pair</td>
                                                <td>No. Validation: Must be within schedule start time and end time.</td>
                                                <td>14:00</td>
                                            </tr>
                                            <tr>
                                                <td>Core End Time</td>
                                                <td>Schedule Type is Flexi. In 24-hour format when the end of a core time frame; Can add more core end time provided that there is a core start time pair</td>
                                                <td>No. Validation: Must be within schedule start time and end time.</td>
                                                <td>16:00</td>
                                            </tr>
                                            <tr>
                                                <td>Break Type</td>
                                                <td>Schedule Type is Fixed. Flexi Break Type and/ or Fixed Break Type</td>
                                                <td>No. Validation: Only accept values “Flexi” ,“Fixed”, and “Floating”</td>
                                                <td>Flexi; Fixed; Floating</td>
                                            </tr>
                                            <tr>
                                                <td>Fixed Break Start Time</td>
                                                <td>Schedule Type is Fixed. Break Type is Fixed. In 24-hour format when the schedule’s break time starts</td>
                                                <td>No. Validation: Must be within schedule start time and end time.</td>
                                                <td>11:00</td>
                                            </tr>
                                            <tr>
                                                <td>Fixed Break End Time</td>
                                                <td>Schedule Type is Fixed. Break Type is Fixed. In 24-hour format when the schedule’s break time ends</td>
                                                <td>No. Validation: Must be within schedule start time and end time, Must be after Fixed Break Start Time.</td>
                                                <td>14:00</td>
                                            </tr>
                                            <tr>
                                                <td>Flexi Break Start Time</td>
                                                <td>Schedule Type is Fixed. Break Type is Flexi. In 24-hour format when the schedule’s break time starts</td>
                                                <td>No. Validation: Must be within schedule start time and end time.</td>
                                                <td>10:00; 12:00</td>
                                            </tr>
                                            <tr>
                                                <td>Flexi Break End Time</td>
                                                <td>Schedule Type is Fixed. Break Type is Flexi. In 24-hour format when the schedule’s break time ends</td>
                                                <td>No. Validation: Must be within schedule start time and end time, Must be after flexi break start time.</td>
                                                <td>11:00; 13:00</td>
                                            </tr>
                                            <tr>
                                                <td>Flexi Break Hours</td>
                                                <td>Schedule Type is Fixed. Break Type is Flexi. In 24-hour format total number of break hours:minutes to used between break start time up to break end time</td>
                                                <td>No. Cannot be greater than the difference of Flexi Break End Time and Flexi Break Start Time.</td>
                                                <td>1:30</td>
                                            </tr>
                                            <tr>
                                                <td>Location Names</td>
                                                <td>Locations whose employees are entitled to the schedule. Data must be present in active company’s locations table.</td>
                                                <td>No</td>
                                                <td>Makati HQ</td>
                                            </tr>
                                            <tr>
                                                <td>Department Names</td>
                                                <td>Departments whose employees are entitled to the schedule. Data must be present in active company’s departments table.</td>
                                                <td>No</td>
                                                <td>Sales Department</td>
                                            </tr>
                                            <tr>
                                                <td>Position Names</td>
                                                <td>Positions whose employees are entitled to the schedule. Data must be present in active company’s positions table.</td>
                                                <td>No</td>
                                                <td>Manager</td>
                                            </tr>
                                            <tr>
                                                <td>Employee Names</td>
                                                <td>Employees who are entitled to the schedule. Data must be present in active company’s employee table.</td>
                                                <td>No. Validation: Must be in format FirstName LastName - EmployeeID</td>
                                                <td>John Doe - EMP001, All employees</td>
                                            </tr>
                                            <tr>
                                                <td>Time Methods Allowed?</td>
                                                <td>Configures the type of time entry the schedule will consider in computing the attendance</td>
                                                <td>No</td>
                                                <td>Time sheet; Web bundy; Bundy app; Biometric</td>
                                            </tr>
                                            <tr>
                                                <td>Schedule on Holidays?</td>
                                                <td>Allows to use this as a schedule on a holiday</td>
                                                <td>No</td>
                                                <td>Yes; No</td>
                                            </tr>
                                            <tr>
                                                <td>Schedule on Rest Day?</td>
                                                <td>Determines whether schedule can be assigned on rest days.</td>
                                                <td>No</td>
                                                <td>Yes; No</td>
                                            </tr>
                                            <tr>
                                                <td>Auto-assign as shift?</td>
                                                <td>Triggers the schedule record to auto-assign it to entitled employees as shifts</td>
                                                <td>No</td>
                                                <td>Yes; No</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div className="sl-c-section">
                                    <div className="sl-c-section__header">
                                        <h5 className="sc-l-section__title">
                                            <span className="sl-c-badge sl-c-badge--circled sl-u-gap-right--sm">3</span> Upload the accomplished CSV file
                                    </h5>
                                    </div>

                                    <div className="sl-c-col--wrapper sl-gap-bottom">
                                        <div className="sl-c-col--6">
                                            <div className="sl-c-upload-area sl-u-text--center">
                                                <div className="sl-c-upload-area__wrap">
                                                    <h3>Step 2:</h3>
                                                    <p className="sl-u-help-text sl-u-gap-bottom">
                                            After filling out the template, choose and upload it here.
                                        </p>

                                                    <div className="sl-u-center">
                                                        <button type="button" className="sl-c-btn sl-c-btn--primary sl-c-btn--ghost sl-c-btn--md sl-u-no-pointer-events">
                                                Choose file
                                            </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="sl-c-col--6">
                                            <p>After filling out the batch upload template, save it as a CSV file on your computer. You are now ready to upload the file to the system.</p>

                                            <p>{'On Step 2, click "Choose file" to select the CSV file that you filled out.'}</p>
                                        </div>
                                    </div>

                                    <div className="sl-c-col--wrapper sl-gap-bottom">
                                        <div className="sl-c-col--6">
                                            <div className="sl-c-upload-area sl-u-text--center">
                                                <div className="sl-c-upload-area__wrap">
                                                    <h3>Step 2:</h3>
                                                    <p className="sl-u-help-text sl-u-gap-bottom">
                                                        <strong>Schedules_2017.csv</strong> <a href="#" className="sl-u-no-pointer-events">Replace</a>
                                                    </p>

                                                    <div className="sl-u-center">
                                                        <button type="button" className="sl-c-btn sl-c-btn--primary sl-c-btn--ghost sl-c-btn--md sl-u-no-pointer-events">
                                            Upload
                                            </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="sl-c-col--6">
                                            <p>{'After choosing the filled out CSV file, click "Upload."'}</p>
                                        </div>
                                    </div>

                                    <div className="sl-c-col--wrapper sl-gap-bottom">
                                        <div className="sl-c-col--6">
                                            <div className="sl-c-upload-area sl-u-text--center">
                                                <div className="sl-c-upload-area__wrap">
                                                    <h3>Step 2:</h3>
                                                    <p className="sl-u-help-text sl-u-gap-bottom">
                                                        <strong>Schedules_2017.csv</strong> <a href="#" className="sl-u-no-pointer-events">Replace</a>
                                                    </p>

                                                    <div className="sl-c-progress sl-c-progress--success">
                                                        <div className="sl-c-progress__bar" style={ { width: '30%' } }></div>
                                                    </div>

                                                    <p className="sl-u-text--success">Uploading and validating</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="sl-c-col--6">
                                            <p>The system will check the file to see if all the entries that you entered are valid in format. You will see the status of the upload and validation.</p>
                                        </div>
                                    </div>

                                    <p>The system will then present the errors — if there are any — in the CSV file.</p>

                                    <div className="sl-c-table-header sl-u-gap-top--lg">
                                        <h5>Data field errors</h5>

                                        <div className="sl-u-actions">
                                    Showing 1-4 of 4 entries
                                    </div>
                                    </div>

                                    <table className="sl-c-table sl-u-gap-bottom--lg">
                                        <thead>
                                            <tr className="sl-has-error">
                                                <th>
                                        Row Number
                                        </th>
                                                <th>
                                        Error Type
                                        </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>
                                        Row 10
                                        </td>
                                                <td>
                                                    {'Invalid "Time Methods Allowed?". These time methods are available: Bundy App, Time Sheet, Web Bundy, Biometric.'}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                        Row 12
                                        </td>
                                                <td>
                                        Invalid Schedule Validity Start Date format. Date must be in MMM-DD-YYYY format.
                                        </td>
                                            </tr>
                                            <tr>
                                                <td>
                                        Row 14
                                        </td>
                                                <td>
                                        Name Schedule1 has a duplicate in the CSV file you are trying to upload.
                                        </td>
                                            </tr>
                                            <tr>
                                                <td>
                                        Row 20
                                        </td>
                                                <td>
                                        The Total Schedule Hours field is required when type is flexi.
                                        </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div className="sl-c-col--wrapper sl-gap-bottom">
                                        <div className="sl-c-col--6">
                                            <div className="sl-c-upload-area sl-u-text--center">
                                                <div className="sl-c-upload-area__wrap">
                                                    <h3>Step 2:</h3>
                                                    <p className="sl-u-help-text sl-u-gap-bottom">
                                                        <strong>Schedules_2017.csv</strong> <a href="#" className="sl-u-no-pointer-events">Replace</a>
                                                    </p>

                                                    <div className="sl-c-progress sl-c-progress--success">
                                                        <div className="sl-c-progress__bar" style={ { width: '75%' } }></div>
                                                    </div>

                                                    <p className="sl-u-text--success">Uploading and validating</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="sl-c-col--6">
                                            <p>Once you are done correcting the errors, upload your updated CSV file.</p>
                                        </div>
                                    </div>
                                </div>

                                <div className="sl-c-section">
                                    <div className="sl-c-section__header">
                                        <h5 className="sc-l-section__title">
                                            <span className="sl-c-badge sl-c-badge--circled sl-u-gap-right--sm">4</span> Review the uploaded data
                                    </h5>
                                    </div>

                                    <p>{'All the contents of your CSV file will be presented once the validation is done. Once done, click "Submit."'}</p>

                                    <div className="sl-c-col--wrapper sl-gap-bottom sl-u-gap-top">
                                        <div className="sl-c-col--6">
                                            <div className="sl-c-upload-area sl-u-text--center sl-u-big">
                                                <div className="sl-c-upload-area__wrap">
                                                    <h3>Step 1:</h3>
                                                    <p className="sl-u-help-text sl-u-gap-bottom">
                                            A batch upload template is available for you to download and fill out. <br />
                                                        <a href="#" className="sl-u-no-pointer-events">You may click here to view the upload guide.</a>
                                                    </p>

                                                    <div className="sl-u-center">
                                                        <button type="button" className="sl-c-btn sl-c-btn--primary sl-c-btn--ghost sl-c-btn--md sl-u-no-pointer-events">
                                                Download Template
                                            </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="sl-c-col--6">
                                            <div className="sl-c-upload-area sl-u-text--center sl-u-big">
                                                <div className="sl-c-upload-area__wrap">
                                                    <h3>Step 2:</h3>
                                                    <p className="sl-u-help-text sl-u-gap-bottom">
                                            After completely filling out the template, choose and upoload it here.
                                        </p>

                                                    <div className="sl-u-center">
                                                        <button type="button" className="sl-c-btn sl-c-btn--primary sl-c-btn--ghost sl-c-btn--md sl-u-no-pointer-events">
                                            Choose file
                                            </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="sl-c-table-header sl-u-gap-top--lg">
                                        <h4> Uploaded CSV data </h4>
                                        <div className="sl-u-actions">
                                    Showing 1-2 of 2 entries
                                    </div>
                                    </div>

                                    <table className="sl-c-table sl-u-gap-bottom--lg">
                                        <thead>
                                            <tr>
                                                <th>
                                        Schedule Name
                                                <span>
                                                    <Icon name="sortArrows" className="sl-c-icon" />
                                                </span>
                                                </th>
                                                <th>
                                        Schedule Type
                                        <span>
                                            <Icon name="sortArrows" className="sl-c-icon" />
                                        </span>
                                                </th>
                                                <th>
                                        Start Time
                                        <span>
                                            <Icon name="sortArrows" className="sl-c-icon" />
                                        </span>
                                                </th>
                                                <th>
                                        Break Start Time
                                        <span>
                                            <Icon name="sortArrows" className="sl-c-icon" />
                                        </span>
                                                </th>
                                                <th>
                                        Break End Time
                                        <span>
                                            <Icon name="sortArrows" className="sl-c-icon" />
                                        </span>
                                                </th>
                                                <th>
                                        End Time
                                        <span>
                                            <Icon name="sortArrows" className="sl-c-icon" />
                                        </span>
                                                </th>
                                                <th>
                                        Entitled Employees
                                        <span>
                                            <Icon name="sortArrows" className="sl-c-icon" />
                                        </span>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>
                                        Night shift
                                        </td>
                                                <td>
                                        Fixed
                                        </td>
                                                <td>
                                        18:00
                                        </td>
                                                <td>
                                        19:00,21:00
                                        </td>
                                                <td>
                                        20:00,03:00
                                        </td>
                                                <td>
                                        06:00
                                        </td>
                                                <td>
                                        All employees
                                        </td>
                                            </tr>
                                            <tr>
                                                <td>
                                        Opening
                                        </td>
                                                <td>
                                        Fixed
                                        </td>
                                                <td>
                                        06:00
                                        </td>
                                                <td>
                                        10:00
                                        </td>
                                                <td>
                                        12:00
                                        </td>
                                                <td>
                                        18:00
                                        </td>
                                                <td>
                                        Sales Department
                                        </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div className="sl-c-table-options">
                                        <div>
                                    Display
                                    <select className="sl-c-form-control sl-c-form-control--md sl-c-form-control--inline sl-u-no-pointer-events">
                                        <option value="1">10</option>
                                        <option value="1">200</option>
                                    </select>
                                    per page
                                    </div>

                                        <div>
                                    Page
                                    <select className="sl-c-form-control sl-c-form-control--md sl-c-form-control--inline sl-u-no-pointer-events">
                                        <option value="1">1</option>
                                        <option value="1">200</option>
                                    </select>
                                    of 1
                                    <button className="sl-c-btn sl-c-btn--primary sl-c-btn--ghost sl-c-btn--md sl-u-gap-left sl-u-no-pointer-events" disabled>Previous</button>
                                            <button className="sl-c-btn sl-c-btn--primary sl-c-btn--ghost sl-c-btn--md sl-u-no-pointer-events" disabled>Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </main>

                    </ContentWrapper>
                </Container>
                <Footer>
                    <Container>
                        <div className="sl-u-container">
                            <div className="sl-c-form-group sl-u-text--right">
                                <button type="button" className="sl-c-btn sl-c-btn--primary sl-c-btn--ghost sl-u-no-pointer-events" disabled>Cancel</button>
                                <button type="button" className="sl-c-btn sl-c-btn--primary sl-u-no-pointer-events" disabled>Submit</button>
                            </div>
                        </div>
                    </Container>
                </Footer>
            </div>
        );
    }
}
