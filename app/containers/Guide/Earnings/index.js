import React from 'react';
import Helmet from 'react-helmet';
import { Container, Breadcrumb, BreadcrumbItem } from 'reactstrap';

import { H4 } from '../../../components/Typography';
import SubHeader from '../../SubHeader';
import A from '../../../components/A';

import { browserHistory } from '../../../utils/BrowserHistory';
import { EMPLOYEE_SUBHEADER_ITEMS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import { ContentWrapper, NavWrapper } from './styles';

/**
 *
 * EarningsGuide
 *
 */
export default class EarningsGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <SubHeader items={ EMPLOYEE_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                browserHistory.push( '/earnings' );
                            } }
                        >
                            &#8592;Back to Allowances
                        </A>
                    </Container>
                </NavWrapper>
                <Container>
                    <Helmet
                        title="Earnings Guide"
                        meta={ [
                            { name: 'description', content: 'Description of EarmingsGuide' }
                        ] }
                    />
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem active>Earnings</BreadcrumbItem>
                        </Breadcrumb>
                        <H4>Earnings</H4>
                        <ul>
                            <li><A href="/payroll/guides/earnings/batch-update">Batch Update</A></li>
                        </ul>
                    </ContentWrapper>
                </Container>
            </div>
        );
    }
}
