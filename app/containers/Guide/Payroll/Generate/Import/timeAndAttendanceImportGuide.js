import React from 'react';

import { H4 } from '../../../../../components/Typography';
import A from '../../../../../components/A';
import { FRONT_END_ASSETS_URL } from './constants';
import { StyledInstructionContent } from './styles';

/**
 *
 * Import Time and Attendance Guide
 *
 */
export default function TimeAndAttendanceImportGuide() {
    return (
        <StyledInstructionContent>
            <H4>Payroll Time and Attendance Data Upload</H4>
            <p>To start generating your payroll, click on the Payroll tab on the main dashboard.</p>
            <img className="offset-none" alt="Click Payroll tab" src={ `${FRONT_END_ASSETS_URL}/assets/payroll_tab.png` } />
            <p>
                You then need to select a Payroll Group and the Payroll Period of the payroll you wish to generate.&nbsp;<br />
                Only the existing payroll groups can be selected.
            </p>
            <img alt="Generate payroll button" src={ `${FRONT_END_ASSETS_URL}/assets/generate_payroll_empty.png` } />
            &nbsp;<br />
            <img alt="Select payroll group" src={ `${FRONT_END_ASSETS_URL}/assets/generate_payroll_PG.png` } />
            &nbsp;<br />
            <img alt="Select payroll period" src={ `${FRONT_END_ASSETS_URL}/assets/generate_payroll_PP.png` } />
            <p>If you are okay with your selections, click Generate Payroll.</p>
            <img alt="Click generate payroll button" src={ `${FRONT_END_ASSETS_URL}/assets/generate_payroll.png` } />
            <p>
                In generating payroll, Time and Attendance data are required. Other payroll data can only be added and processed after&nbsp;
                Time and Attendance has been imported.
            </p>
            <img className="offset-none" alt="Time and Attendance" src={ `${FRONT_END_ASSETS_URL}/assets/T&A_section.png` } />
            <p>
                On the Time and Attendance section, you can download the Time and Attendance CSV template.&nbsp;<br />
                You can do this by clicking the download link or the download button.
            </p>
            <img alt="Click download link" src={ `${FRONT_END_ASSETS_URL}/assets/T&A_download_link.png` } />
            &nbsp;<br />
            <img alt="Click download button" src={ `${FRONT_END_ASSETS_URL}/assets/T&A_download_button.png` } />
            <p>To help you correctly fill-out the CSV template, the <A href="/payroll/guides/payroll/generate/time-and-attendance">Time and Attendance CSV Template Guide</A> is provided.</p>
            <img alt="Click Time and Attendance link" src={ `${FRONT_END_ASSETS_URL}/assets/T&A_click_here.png` } />
            <p>
                After completely filling out the spreadsheet, save it as a CSV file on your personal drive.&nbsp;<br />
                You are now ready to upload this into the system.
            </p>
            <p>On the Upload Template section, choose the file you wish to upload.</p>
            <img alt="Choose template file" src={ `${FRONT_END_ASSETS_URL}/assets/T&A_choose_file.png` } />
            <p>Click the Upload button.</p>
            <img alt="Click upload button" src={ `${FRONT_END_ASSETS_URL}/assets/T&A_upload_file.png` } />
            <p>
                The system will present you the erros on your CSV file, if any.&nbsp;<br />
                You will have to edit your file to conform to the formats presented in the <A href="/payroll/guides/payroll/generate/time-and-attendance">Time and Attendance CSV Template Guide</A> to avoid such errors&nbsp;<br />
                You will also have the ability to copy the errors to your clipboard, if deemed necessary.
            </p>
            <img className="offset-none" alt="CSV template errors" src={ `${FRONT_END_ASSETS_URL}/assets/T&A_error.png` } />
            <p>You have to re-upload your file after correcting the errors.</p>
            <img alt="CSV template validated" src={ `${FRONT_END_ASSETS_URL}/assets/T&A_validated.png` } />
            <p>
                Once your Time and Attendance data has been validated, you may now proceed to finish and generate our payroll by clicking Finish Import.&nbsp;<br />
                Salarium will now generate your payroll by assessing your employees&#39; Witholding Tax and Government contributions.
            </p>
            <p>
                However, if you wish to add other income sources such as Allowances, Bonus, Commissions,
                Reimbursements, Adjustments and Deductions, you may do this in the same manner as you did in Time and Attendance.
            </p>
            <img className="offset-none" alt="Time and Attendance importing" src={ `${FRONT_END_ASSETS_URL}/assets/T&A_finish.png` } />
        </StyledInstructionContent>
    );
}

