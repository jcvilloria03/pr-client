import styled from 'styled-components';

export const Wrapper = styled.div`
    padding: 90px 0;    
`;

export const ContentWrapper = styled.div`
    background-color: #fff;
    display: flex;
    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
    margin: 20px 2vw;
    color: #444;
    padding: 30px 40px;
    flex-direction: column;
`;

export const StyledInstructionContent = styled.section`
    img { max-width: 100%;  }
    @media (min-width: 576px) {
        img { margin-left: 5%; margin-bottom: 20px; }
        img.offset-sm { margin-left: 2%; }
        img.offset-none { margin-left: 0; }
    }
`;
