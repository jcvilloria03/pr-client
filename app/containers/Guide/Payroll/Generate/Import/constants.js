
/**
 * Import Payroll Guide constants
 */

export const FRONT_END_ASSETS_URL = 'https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev';
export const IMPORT_PAYROLL_DATA = 'import-payroll-data';
