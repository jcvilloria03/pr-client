import React from 'react';

import { Table } from 'reactstrap';
import { H4 } from '../../../../../../components/Typography';

import { StyledTableContent } from './styles';

/**
 * Time and Attendance, Overtime and Holiday Rates Import Guide
 */
export default function TaOvertimeHolidayTableGuide() {
    return (
        <StyledTableContent>
            <div>
                <H4>Time and Attendance, Overtime, and Holiday Rates</H4>
                <br />
                <Table>
                    <thead>
                        <tr>
                            <th>Day Type</th>
                            <th>Holiday Type</th>
                            <th>Time Type</th>
                            <th>Abbreviation</th>
                            <th>Rate</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Regular</td>
                            <td>Non-holiday</td>
                            <td>Regular</td>
                            <td>Regular</td>
                            <td>1.000000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Non-holiday</td>
                            <td>Regular</td>
                            <td>Paid Leave</td>
                            <td>1.000000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Non-holiday</td>
                            <td>Regular</td>
                            <td>Unpaid Leave</td>
                            <td>1.000000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Special Holiday</td>
                            <td>Regular</td>
                            <td>SH</td>
                            <td>1.300000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Regular Holiday</td>
                            <td>Regular</td>
                            <td>SH</td>
                            <td>2.000000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>
                                Regular Holiday,<br />
                                Regular Holiday
                            </td>
                            <td>Regular</td>
                            <td>2RH</td>
                            <td>3.000000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Non-holiday</td>
                            <td>Night</td>
                            <td>NT</td>
                            <td>1.100000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Special Holiday</td>
                            <td>Night</td>
                            <td>SH+NT</td>
                            <td>1.430000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Regular Holiday</td>
                            <td>Night</td>
                            <td>RH+NT</td>
                            <td>2.200000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>
                                Regular Holiday,<br />
                                Regular Holiday
                            </td>
                            <td>Night</td>
                            <td>2RH+NT</td>
                            <td>3.300000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Non-holiday</td>
                            <td>Overtime</td>
                            <td>OT</td>
                            <td>1.250000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Special Holiday</td>
                            <td>Overtime</td>
                            <td>SH+OT</td>
                            <td>1.690000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Regular Holiday</td>
                            <td>Overtime</td>
                            <td>RH+OT</td>
                            <td>2.600000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>
                                Regular Holiday,<br />
                                Regular Holiday
                            </td>
                            <td>Overtime</td>
                            <td>2RH+OT</td>
                            <td>3.900000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Non-holiday</td>
                            <td>Night, Overtime</td>
                            <td>NT+OT</td>
                            <td>1.375000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Special Holiday</td>
                            <td>Night, Overtime</td>
                            <td>SH+NT+OT</td>
                            <td>1.859000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>Regular Holiday</td>
                            <td>Night, Overtime</td>
                            <td>RH+NT+OT</td>
                            <td>2.860000</td>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td>
                                Regular Holiday,<br />
                                Regular Holiday
                            </td>
                            <td>Night, Overtime</td>
                            <td>2RH+NT+OT</td>
                            <td>4.290000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Non-holiday</td>
                            <td>Regular</td>
                            <td>RD</td>
                            <td>1.300000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Special Holiday</td>
                            <td>Regular</td>
                            <td>RD+SH</td>
                            <td>1.500000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Regular Holiday</td>
                            <td>Regular</td>
                            <td>RD+RH</td>
                            <td>2.600000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>
                                Regular Holiday,<br />
                                Regular Holiday
                            </td>
                            <td>Regular</td>
                            <td>RD+2RH</td>
                            <td>3.900000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Non-holiday</td>
                            <td>Night</td>
                            <td>RD+NT</td>
                            <td>1.430000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Special Holiday</td>
                            <td>Night</td>
                            <td>RD+SH+NT</td>
                            <td>1.650000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Regular Holiday</td>
                            <td>Night</td>
                            <td>RD+RH+NT</td>
                            <td>2.860000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>
                                Regular Holiday,<br />
                                Regular Holiday
                            </td>
                            <td>Night</td>
                            <td>RD+2RH+NT</td>
                            <td>4.290000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Non-holiday</td>
                            <td>Overtime</td>
                            <td>RD+OT</td>
                            <td>1.690000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Special Holiday</td>
                            <td>Overtime</td>
                            <td>RD+SH+OT</td>
                            <td>1.950000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Regular Holiday</td>
                            <td>Overtime</td>
                            <td>RD+RH+OT</td>
                            <td>3.380000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>
                                Regular Holiday,<br />
                                Regular Holiday
                            </td>
                            <td>Overtime</td>
                            <td>RD+2RH+OT</td>
                            <td>5.070000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Non-holiday</td>
                            <td>Night, Overtime</td>
                            <td>RD+NT+OT</td>
                            <td>1.859000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Special Holiday</td>
                            <td>Night, Overtime</td>
                            <td>RD+SH+NT+OT</td>
                            <td>2.145000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>Regular Holiday</td>
                            <td>Night, Overtime</td>
                            <td>RD+RH+NT+OT</td>
                            <td>3.718000</td>
                        </tr>
                        <tr>
                            <td>Rest Day</td>
                            <td>
                                Regular Holiday,<br />
                                Regular Holiday
                            </td>
                            <td>Night, Overtime</td>
                            <td>RD+2RH+NT+OT</td>
                            <td>5.577000</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </StyledTableContent>
    );
}
