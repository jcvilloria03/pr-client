import React from 'react';
import Helmet from 'react-helmet';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';

import TaOvertimeHolidayTableGuide from './taOvertimeHolidayGuide';

import { browserHistory } from '../../../../../../utils/BrowserHistory';
import { subscriptionService } from '../../../../../../utils/SubscriptionService';

import { Wrapper, ContentWrapper } from './styles';

/**
 *
 * TaOvertimeHolidayGuide
 *
 */
export default class TaOvertimeHolidayGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToBothPayrollAndTA( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <Helmet
                    title="Time and Attendance, Overtime, and Holiday Rates Import Guide"
                    meta={ [
                        { name: 'description', content: 'Time and Attendance, Overtime, and Holiday Rates Import Guide' }
                    ] }
                />
                <Wrapper>
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll">Payroll</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll/generate">Generate</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll/generate/import">Import</a></BreadcrumbItem>
                            <BreadcrumbItem active>Time and Attendance, Overtime, and Holiday Rates</BreadcrumbItem>
                        </Breadcrumb>
                        <TaOvertimeHolidayTableGuide />
                    </ContentWrapper>
                </Wrapper>
            </div>
        );
    }
}
