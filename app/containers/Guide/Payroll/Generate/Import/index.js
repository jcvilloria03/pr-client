import React from 'react';
import Helmet from 'react-helmet';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';

import { H4 } from '../../../../../components/Typography';
import A from '../../../../../components/A';

import { IMPORT_PAYROLL_DATA } from './constants';
import { Wrapper, ContentWrapper } from './styles';
import TimeAndAttendace from './timeAndAttendanceImportGuide';
import OtherIncome from './otherIncomeImportGuide';

import { browserHistory } from '../../../../../utils/BrowserHistory';
import { subscriptionService } from '../../../../../utils/SubscriptionService';

/**
 *
 * PayrollGenerateGuide
 *
 */
class PayrollGenerateGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    /**
     * Constructor
     */
    constructor() {
        super();
        this.scrollToGuideByHash = this.scrollToGuideByHash.bind( this );

        //  OnLoad event handler to detect hash in url
        this.scrollToGuideByHashHandler = () => {
            this.scrollToGuideByHash( window.location.hash.substring( 1 ) );
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    /**
     * Onload event when component is rendered to DOM
     */
    componentDidMount() {
        //  This scrolls to desired section when loading the page for first time
        window.addEventListener( 'load', this.scrollToGuideByHashHandler );
    }

    /**
     * Unload event when component is removed from DOM
     */
    componentWillUnmount() {
        //  Remove scrollToGuide from DOM load event
        window.removeEventListener( 'load', this.scrollToGuideByHashHandler );
    }

    /**
     * Scroll to section based on hash
     */
    scrollToGuideByHash( guide ) {
        switch ( ( guide || '' ).toLowerCase() ) {
            case IMPORT_PAYROLL_DATA:
                document.getElementById( IMPORT_PAYROLL_DATA ).scrollIntoView( true );
                break;
            default:
                break;
        }
    }

    /**
     * Renders the component to DOM
     */
    render() {
        return (
            <div>
                <Helmet
                    title="Generate Payroll Guides"
                    meta={ [
                        { name: 'description', content: 'Description of PayrollGenerateGuide' }
                    ] }
                />
                <Wrapper>
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll/">Payroll</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll/generate">Generate</a></BreadcrumbItem>
                            <BreadcrumbItem active>Import</BreadcrumbItem>
                        </Breadcrumb>
                        <TimeAndAttendace />
                        <OtherIncome scrollToGuideByHash={ this.scrollToGuideByHash } />
                        <H4 id={ IMPORT_PAYROLL_DATA }>Import Payroll Data</H4>
                        <ul>
                            <li><A target="_blank" href="/payroll/guides/payroll/generate/time-and-attendance">Time and Attendance</A></li>
                            <li><A target="_blank" href="/payroll/guides/payroll/generate/ta-overtime-holiday">Time and Attendance, Overtime and Holiday Rates</A></li>
                            <li><A target="_blank" href="/payroll/guides/payroll/generate/bonuses">Bonuses</A></li>
                            <li><A target="_blank" href="/payroll/guides/payroll/generate/allowances">Allowances</A></li>
                            <li><A target="_blank" href="/payroll/guides/payroll/generate/commissions">Commissions</A></li>
                            <li><A target="_blank" href="/payroll/guides/payroll/generate/deductions">Deductions</A></li>
                        </ul>
                    </ContentWrapper>
                </Wrapper>
            </div>
        );
    }
}

export default PayrollGenerateGuide;
