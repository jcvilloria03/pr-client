import React from 'react';
import Helmet from 'react-helmet';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';

import CommissionTableGuide from './commissionGuide';

import { browserHistory } from '../../../../../../utils/BrowserHistory';
import { subscriptionService } from '../../../../../../utils/SubscriptionService';

import { Wrapper, ContentWrapper } from './styles';

/**
 *
 * CommissionGuide
 *
 */
export default class CommissionGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <Helmet
                    title="Commissions Import Guide"
                    meta={ [
                        { name: 'description', content: 'Commissions Import Guide' }
                    ] }
                />
                <Wrapper>
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll">Payroll</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll/generate">Generate</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll/generate/import">Import</a></BreadcrumbItem>
                            <BreadcrumbItem active>Commissions</BreadcrumbItem>
                        </Breadcrumb>
                        <CommissionTableGuide />
                    </ContentWrapper>
                </Wrapper>
            </div>
        );
    }
}
