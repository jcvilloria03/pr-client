import React from 'react';

import { Table } from 'reactstrap';
import { H4 } from '../../../../../../components/Typography';

import { StyledTableContent } from './styles';

/**
 * Time And Attendance Import Guide
 */
export default function TimeAndAttendanceTableGuide() {
    return (
        <StyledTableContent>
            <div>
                <H4>Time & Attendance CSV Upload Field Guide</H4>
                <br />
                <Table>
                    <thead>
                        <tr>
                            <th>Column Name</th>
                            <th>Description</th>
                            <th>Required?</th>
                            <th>Example</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Employee ID</th>
                            <td>Company ID</td>
                            <td><span className="req1">yes</span></td>
                            <td>2015001</td>
                        </tr>
                        <tr>
                            <th scope="row">Last Name</th>
                            <td>Surname</td>
                            <td><span className="req1">yes</span></td>
                            <td>Doe</td>
                        </tr>
                        <tr>
                            <th scope="row">First Name</th>
                            <td>Given name</td>
                            <td><span className="req1">yes</span></td>
                            <td>John</td>
                        </tr>
                        <tr>
                            <th scope="row">Middle Name</th>
                            <td>Middle Name</td>
                            <td>no</td>
                            <td>Smith</td>
                        </tr>
                        <tr>
                            <th scope="row">Timesheet Date</th>
                            <td>
                                Attendance date<br />
                                Date format:<br />
                                MM/DD/YYYY
                            </td>
                            <td><span className="req1">yes</span></td>
                            <td>03/15/2017</td>
                        </tr>
                        <tr>
                            <th scope="row">Scheduled Hours</th>
                            <td>
                                Employee scheduled working hours<br />
                                Time Format:<br />
                                HH:MM (24 Hour  time)
                            </td>
                            <td><span className="req1">yes</span></td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">Regular</th>
                            <td>
                                Employee Clock-in Time<br />
                                Time Format:<br />
                                HH:MM (military time) or HH:MM:SS
                            </td>
                            <td><span className="req1">yes</span></td>
                            <td>
                                07:55<br />
                                07:55:33
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Paid Leave</th>
                            <td>
                                Employee Clock-out Time<br />
                                Time Format:<br />
                                HH:MM (military time) or HH:MM:SS
                            </td>
                            <td>no</td>
                            <td>
                                03:55<br />
                                03:55:33
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Unpaid Leave</th>
                            <td>
                                Regular Working Hours<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">OT</th>
                            <td>
                                Rendered Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>02:00</td>
                        </tr>
                        <tr>
                            <th scope="row">NT</th>
                            <td>
                                Night<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>05:00</td>
                        </tr>
                        <tr>
                            <th scope="row">NT+OT</th>
                            <td>
                                Night Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>07:00</td>
                        </tr>
                        <tr>
                            <th scope="row">UH</th>
                            <td>
                                Unworked Holiday<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>8:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RH</th>
                            <td>
                                Regular Holiday<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>00:30</td>
                        </tr>
                        <tr>
                            <th scope="row">RH+NT</th>
                            <td>
                                Regular Holiday Night<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RH+OT</th>
                            <td>
                                Regular Holiday Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RH+NT+OT</th>
                            <td>
                                Regular Holiday Night Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">SH</th>
                            <td>
                                Special Holiday<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">SH+NT</th>
                            <td>
                                Special Holiday Night<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">SH+OT</th>
                            <td>
                                Special Holiday Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">SH+NT+OT</th>
                            <td>
                                Special Holiday Night Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">2RH</th>
                            <td>
                                Double Regular Holiday<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">2RH+NT</th>
                            <td>
                                Double Regular Holiday Night<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">2RH+OT</th>
                            <td>
                                Double Regular Holiday Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">2RH+NT+OT</th>
                            <td>
                                Double Regular Holiday Night Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD</th>
                            <td>
                                Rest Day<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+SH</th>
                            <td>
                                Rest Day Special Holiday<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+RH</th>
                            <td>
                                Rest Day Special Holiday<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+2RH</th>
                            <td>
                                Rest Day Double Regular Holiday<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+NT</th>
                            <td>
                                Rest Day Night<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+SH+NT</th>
                            <td>
                                Rest Day Special Holiday Night<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+RH+NT</th>
                            <td>
                                Rest Day Regular Holiday Night<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+2RH+NT</th>
                            <td>
                                Rest Day Double Regular Holiday Night<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+OT</th>
                            <td>
                                Rest Day Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+SH+OT</th>
                            <td>
                                Rest Day Special Holiday Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+RH+OT</th>
                            <td>
                                Rest Day Regular Holiday Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+2RH+OT</th>
                            <td>
                                Rest Day Double Regular Holiday Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+NT+OT</th>
                            <td>
                                Rest Day Night Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+SH+NT+OT</th>
                            <td>
                                Rest Day Special Holiday Night Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+RH+NT+OT</th>
                            <td>
                                Rest Day Regular Holiday Night Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                        <tr>
                            <th scope="row">RD+2RH+NT+OT</th>
                            <td>
                                Rest Day Double Regular Holiday Night Overtime<br />
                                Time Format:<br />
                                HH:MM (military time)
                            </td>
                            <td>no</td>
                            <td>08:00</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </StyledTableContent>
    );
}
