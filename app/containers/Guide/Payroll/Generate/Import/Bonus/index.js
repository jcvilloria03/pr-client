import React from 'react';
import Helmet from 'react-helmet';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';

import BonusTableGuide from './bonusGuide';

import { browserHistory } from '../../../../../../utils/BrowserHistory';
import { subscriptionService } from '../../../../../../utils/SubscriptionService';

import { Wrapper, ContentWrapper } from './styles';

/**
 *
 * BonusGuide
 *
 */
export default class BonusGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <Helmet
                    title="Bonuses Import Guide"
                    meta={ [
                        { name: 'description', content: 'Bonuses Import Guide' }
                    ] }
                />
                <Wrapper>
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll">Payroll</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll/generate">Generate</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll/generate/import">Import</a></BreadcrumbItem>
                            <BreadcrumbItem active>Bonuses</BreadcrumbItem>
                        </Breadcrumb>
                        <BonusTableGuide />
                    </ContentWrapper>
                </Wrapper>
            </div>
        );
    }
}
