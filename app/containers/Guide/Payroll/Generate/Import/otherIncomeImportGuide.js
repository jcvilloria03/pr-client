import React, { PropTypes } from 'react';

import { H4 } from '../../../../../components/Typography';
import A from '../../../../../components/A';
import { FRONT_END_ASSETS_URL, IMPORT_PAYROLL_DATA } from './constants';
import { StyledInstructionContent } from './styles';

/**
 *
 * Import Other Income Guide
 *
 */
const OtherIncome = function OtherIncomeImportGuide({ scrollToGuideByHash }) {
    return (
        <StyledInstructionContent>
            <H4>Other Income Data Upload</H4>
            <p>
                Other Income data such as Allowances, Bonus, Commissions, Deductions, Reimbursements and Adjustments can be included in your payroll generation.<br />
                It is important to note that other income data can only be imported if Time and Attendance has already been validated.
            </p>
            <img className="offset-none" alt="Import Time and Allowance upload" src={ `${FRONT_END_ASSETS_URL}/assets/other_income_t&a.png` } />
            <p>Click on any of the items below, for instance, Allowances.</p>
            <img className="offset-none" alt="Import Allowances upload" src={ `${FRONT_END_ASSETS_URL}/assets/other_incomes.png` } />
            <br />
            <img className="offset-none" alt="Allowances Download upload" src={ `${FRONT_END_ASSETS_URL}/assets/other_income_allowances.png` } />
            <p>
                You can download the Allowances CSV file template.<br />
                You can do this by clicking the download link or the download button.
            </p>
            <img alt="Allowances Download upload" src={ `${FRONT_END_ASSETS_URL}/assets/other_income_allowances_click_download.png` } />
            <br />
            <img alt="Allowances Download upload" src={ `${FRONT_END_ASSETS_URL}/assets/other_income_allowances_download.png` } />
            <p>To help you correctly fill-out the CSV template, a <A href={ `#${IMPORT_PAYROLL_DATA}` } onClick={ () => { scrollToGuideByHash( IMPORT_PAYROLL_DATA ); } }>CSV Template Guide</A> is provided.</p>
            <img alt="Click to download allowance guide" src={ `${FRONT_END_ASSETS_URL}/assets/other_income_allowances_click_here.png` } />
            <p>
                After completely filling out the spreadsheet, save it as a CSV file on your personal drive.<br />
                You are now ready to upload this into the system.
            </p>
            <p>
                On the Upload Template section, choose the file you wish to upload.<br />
                You can also cancel if you wish not to upload additional income data.
            </p>
            <img alt="Upload the Allowances template" src={ `${FRONT_END_ASSETS_URL}/assets/other_income_allowances_upload.png` } />
            <p>Click the Upload button.</p>
            <img alt="Click Upload button" src={ `${FRONT_END_ASSETS_URL}/assets/other_income_allowances_click_upload.png` } />
            <p>
                The system will present you the errors on your CSV file, if any.<br />
                You will have to edit your file to conform to the formats persented in the <A href="#import-payroll-data">Import Payroll Data</A> to avoid such errors.<br />
                You will also have the ability to copy the errors to your clipboard, if deemed necessary.
            </p>
            <img className="offset-none" alt="Allowances upload error" src={ `${FRONT_END_ASSETS_URL}/assets/other_income_allowances_error.png` } />
            <p>You have to re-upload your file after correcting the errors.</p>
            <img alt="Allowances upload error" src={ `${FRONT_END_ASSETS_URL}/assets/other_income_allowances_validated.png` } />
            <p>
                You can process multiple imports at once. Icons are present to indicate validation process, a successful validation and a failed validation.
            </p>
            <img alt="Allowances upload validation statuses" src={ `${FRONT_END_ASSETS_URL}/assets/other_income_allowances_statuses.png` } />
            <p>If all desired input data has been successfully validated. You can now proceed with generating payroll by clicking the Finish Import button.</p>
            <img className="offset-none" alt="Finished Allowances upload" src={ `${FRONT_END_ASSETS_URL}/assets/other_income_finish.png` } />
        </StyledInstructionContent>
    );
};

OtherIncome.propTypes = {
    scrollToGuideByHash: PropTypes.func.isRequired
};

export default OtherIncome;
