import styled from 'styled-components';

export const Wrapper = styled.div`
    padding: 90px 0;    
`;

export const ContentWrapper = styled.div`
    background-color: #fff;
    display: flex;
    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
    margin: 20px 2vw;
    color: #444;
    padding: 30px 40px;
    flex-direction: column;
`;

export const StyledTableContent = styled.section`
    > div { padding-top: 20px; }
    .req1 { color: #F21108; display: inline; }
    .req2 { color: #00A5E5; display: inline; }
    table {
        border: 1px solid #eceeef;
        thead {
            background: #00a5e5;
            color: #fff;
        }
    }
`;
