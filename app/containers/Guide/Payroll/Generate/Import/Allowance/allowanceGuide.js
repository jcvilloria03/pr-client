import React from 'react';

import { Table } from 'reactstrap';
import { H4 } from '../../../../../../components/Typography';

import { StyledTableContent } from './styles';

/**
 * Allowances CSV Upload Field Guide
 */
export default function AllowanceTableGuide() {
    return (
        <StyledTableContent>
            <div>
                <H4>Allowances CSV Upload Field Guide</H4>
                <br />
                <Table>
                    <thead>
                        <tr>
                            <th>Column Name</th>
                            <th>Description</th>
                            <th>Required?</th>
                            <th>Example</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Employee ID</th>
                            <td>Company ID</td>
                            <td><span className="req1">yes</span></td>
                            <td>2015001</td>
                        </tr>
                        <tr>
                            <th scope="row">Last Name</th>
                            <td>Surname</td>
                            <td><span className="req1">yes</span></td>
                            <td>Doe</td>
                        </tr>
                        <tr>
                            <th scope="row">First Name</th>
                            <td>Given name</td>
                            <td><span className="req1">yes</span></td>
                            <td>John</td>
                        </tr>
                        <tr>
                            <th scope="row">Middle Name</th>
                            <td>Middle Name</td>
                            <td>no</td>
                            <td>Smith</td>
                        </tr>
                        <tr>
                            <th scope="row">Allowance Type</th>
                            <td>Type of Allowance</td>
                            <td><span className="req1">yes</span></td>
                            <td>Transportation</td>
                        </tr>
                        <tr>
                            <th scope="row">Amount</th>
                            <td>Amount Of Allowance</td>
                            <td><span className="req1">yes</span></td>
                            <td>5,000.00</td>
                        </tr>
                        <tr>
                            <th scope="row">Taxable?</th>
                            <td>Yes or No</td>
                            <td><span className="req1">yes</span></td>
                            <td>Yes</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </StyledTableContent>
    );
}
