import React from 'react';
import Helmet from 'react-helmet';
import { Container, Breadcrumb, BreadcrumbItem } from 'reactstrap';

import { H4 } from '../../../../components/Typography';
import A from '../../../../components/A';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { subscriptionService } from '../../../../utils/SubscriptionService';

import { Wrapper, ContentWrapper } from './styles';

/**
 *
 * PayrollGenerateGuide
 *
 */
export default class PayrollGenerateGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <Container>
                <Helmet
                    title="Generate Payroll Guides"
                    meta={ [
                        { name: 'description', content: 'Description of PayrollGenerateGuide' }
                    ] }
                />
                <Wrapper>
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem><a href="/payroll/guides/payroll">Payroll</a></BreadcrumbItem>
                            <BreadcrumbItem active>Generate</BreadcrumbItem>
                        </Breadcrumb>
                        <H4>Generate</H4>
                        <ul>
                            <li><A href="/payroll/guides/payroll/generate/import/">Import</A></li>
                        </ul>
                    </ContentWrapper>
                </Wrapper>
            </Container>
        );
    }
}
