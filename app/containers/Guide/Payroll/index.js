import React from 'react';
import Helmet from 'react-helmet';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';

import { H4 } from '../../../components/Typography';
import A from '../../../components/A';

import { browserHistory } from '../../../utils/BrowserHistory';
import { subscriptionService } from '../../../utils/SubscriptionService';

import { Wrapper, ContentWrapper } from './styles';

/**
 *
 * PayrollGuide
 *
 */
export default class PayrollGuide extends React.PureComponent {
    static propTypes = {
        products: React.PropTypes.array
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }
    }

    render() {
        return (
            <div>
                <Helmet
                    title="Payroll Guide"
                    meta={ [
                        { name: 'description', content: 'Description of PayrollGuide' }
                    ] }
                />
                <Wrapper>
                    <ContentWrapper>
                        <Breadcrumb>
                            <BreadcrumbItem><a href="/payroll/guides">Guides</a></BreadcrumbItem>
                            <BreadcrumbItem active>Payroll</BreadcrumbItem>
                        </Breadcrumb>
                        <H4>Payroll</H4>
                        <ul>
                            <li><A href="/payroll/guides/payroll/generate/">Generate</A></li>
                        </ul>
                    </ContentWrapper>
                </Wrapper>
            </div>
        );
    }
}
