import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_ADD_RANK,
    SET_ERRORS,
    NOTIFICATION_SAGA
} from './constants';

const initialState = fromJS({
    errors: {},
    addRankData: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * AddRanks reducer
 *
 */
function addRanksReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_ADD_RANK:
            return state.set( 'addRankData', action.payload );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default addRanksReducer;
