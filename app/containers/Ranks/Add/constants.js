/*
 *
 * AddRanks constants
 *
 */

export const namespace = 'app/containers/ranks/add';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const SET_LOADING = `${namespace}/SET_LOADING`;
export const GET_LOADING = `${namespace}/GET_LOADING`;
export const SET_ERRORS = `${namespace}/SET_ERRORS`;
export const SET_ADD_RANK = `${namespace}/SET_ADD_RANK`;
export const GET_ADD_RANK = `${namespace}/GET_ADD_RANK`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
