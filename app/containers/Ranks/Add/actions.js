import {
    GET_ADD_RANK,
    NOTIFICATION
} from './constants';

/**
 * Add Rank function
 */
export function addRank( payload ) {
    return {
        type: GET_ADD_RANK,
        payload
    };
}
/**
 * Display a notification in page
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

