import { createSelector } from 'reselect';

/**
 * Direct selector to the addRanks state domain
 */
const selectAddRanksDomain = () => ( state ) => state.get( 'addRanks' );

/**
 * Other specific selectors
 */
const makeSelectSubmitted = () => createSelector(
  selectAddRanksDomain(),
  ( substate ) => substate.get( 'addRankData' )
);

const makeSelectNotification = () => createSelector(
  selectAddRanksDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectErrors = () => createSelector(
  selectAddRanksDomain(),
  ( substate ) => {
      let error;
      try {
          error = substate.get( 'errors' ).toJS();
      } catch ( err ) {
          error = substate.get( 'errors' );
      }

      return error;
  }
);
/**
 * Default selector used by AddRanks
 */

const makeSelectAddRanks = () => createSelector(
  selectAddRanksDomain(),
  ( substate ) => substate.toJS()
);

export default makeSelectAddRanks;
export {
  selectAddRanksDomain,
  makeSelectSubmitted,
  makeSelectNotification,
  makeSelectErrors
};
