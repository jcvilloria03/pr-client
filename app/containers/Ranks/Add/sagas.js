import { LOCATION_CHANGE } from 'react-router-redux';
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';

import { Fetch } from 'utils/request';
import { browserHistory } from 'utils/BrowserHistory';
import {
    GET_ADD_RANK,
    SET_ADD_RANK,
    SET_ERRORS,
    NOTIFICATION,
    NOTIFICATION_SAGA
} from './constants';
import { resetStore } from '../../App/sagas';
import { REINITIALIZE_PAGE } from '../../App/constants';

/**
 * isNameAvalibility saga
 */
export function* addRanks({ payload }) {
    try {
        yield [
            put({ type: SET_ADD_RANK, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];
        let response;
        const checkRankName = payload.names.includes( ',' );
        if ( checkRankName === true ) {
            const arrayValue = payload.names.split( ',' );
            for ( const element of arrayValue ) {
                response = yield call( Fetch, `/company/${payload.company_id}/rank/is_name_available`,
                    {
                        method: 'POST', data: { name: element }
                    });

                if ( response.available === true ) {
                    try {
                        yield call( Fetch, '/rank/bulk_create',
                            {
                                method: 'POST', data: { company_id: payload.company_id, names: arrayValue }
                            });
                        yield put({
                            type: SET_ADD_RANK,
                            payload: false
                        });

                        yield call( browserHistory.push, '/company-settings/company-structure/ranks', true );
                    } catch ( error ) {
                        yield call( notifyUser, {
                            show: true,
                            title: error.response ? error.response.statusText : 'Error',
                            message: error.response ? error.response.data.message : 'You have entered a rank name which already exists',
                            type: 'error'
                        });
                        yield put({
                            type: SET_ADD_RANK,
                            payload: false
                        });
                    }
                }
            }
        } else {
            const res = yield call( Fetch, `/company/${payload.company_id}/rank/is_name_available`,
                {
                    method: 'POST', data: { name: payload.names }
                });

            if ( res.available === true ) {
                try {
                    yield call( Fetch, '/rank/bulk_create',
                        {
                            method: 'POST', data: { company_id: payload.company_id, names: [payload.names]}
                        });
                    yield put({
                        type: SET_ADD_RANK,
                        payload: false
                    });

                    yield call( browserHistory.push, '/company-settings/company-structure/ranks', true );
                } catch ( error ) {
                    if ( error.response && error.response.status === 406 ) {
                        yield call( setErrors, error.response.data.data );
                    } else {
                        yield call( notifyUser, error );
                    }
                }
            } else {
                yield call( notifyUser, {
                    show: true,
                    title: 'Error',
                    message: 'You have entered a rank name which already exists',
                    type: 'error'
                });
                yield put({
                    type: SET_ADD_RANK,
                    payload: false
                });
            }
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : 'You have entered a rank name which already exists',
            type: 'error'
        });
        yield put({
            type: SET_ADD_RANK,
            payload: false
        });
    }
}

/**
 * changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.message,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    yield call( resetStore );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    yield takeLatest( GET_ADD_RANK, addRanks );
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForReinitializePage,
    watchNotify,
    watchForSubmitForm
];
