/*
 *
 * Ranks constants
 *
 */

export const namespace = 'app/containers/ranks/view';

export const DEFAULT_ACTION = `${namespace}/DEFAULT_ACTION`;
export const SET_LOADING = `${namespace}/SET_LOADING`;
export const SET_EDITING = `${namespace}/SET_EDITING`;
export const SET_RANK = `${namespace}/SET_RANK`;
export const GET_RANK = `${namespace}/GET_RANK`;
export const SET_RANK_LENGTH = `${namespace}/SET_RANK_LENGTH`;
export const NOTIFICATION = `${namespace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${namespace}/NOTIFICATION_SAGA`;
export const SET_EDIT_RANK = `${namespace}/SET_EDIT_RANK`;
export const GET_EDIT_RANK = `${namespace}/GET_EDIT_RANK`;
export const GET_DELETE_RANK = `${namespace}/GET_DELETE_RANK`;
export const SET_DELETE_RANK = `${namespace}/SET_DELETE_RANK`;
