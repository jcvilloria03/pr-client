/* eslint-disable space-in-parens */
import { createSelector } from 'reselect';

/**
 * Direct selector to the ranks state domain
 */
const selectRanksDomain = () => ( state ) => state.get( 'ranks' );
const selectAppDomain = () => ( state ) => state.get( 'topLevel' );

/**
 * Other specific selectors
 */
const makeSelectLoading = () => createSelector(
  selectRanksDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectRankList = () => createSelector(
  selectRanksDomain(),
  ( schedules ) => schedules.get( 'rankList' ).toJS()
);

const makeSelectRankLength = () => createSelector(
  selectRanksDomain(),
  ( schedules ) => schedules.get( 'length' )
);

const makeSelectIsEditing = () => createSelector(
  selectRanksDomain(),
( substate ) => substate.get( 'isEditing' )
);

const makeSelectNotification = () => createSelector(
  selectRanksDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectProducts = () => createSelector(
  selectAppDomain(),
  ( substate ) => {
      if ( !substate.get( 'products' ) ) {
          return substate.get( 'products' );
      }
      return substate.get( 'products' ).toJS();
  }
);

/**
 * Default selector used by Ranks
 */

const makeSelectRanks = () => createSelector(
  selectRanksDomain(),
  ( substate ) => substate.toJS()
);

export default makeSelectRanks;
export {
  selectRanksDomain,
  makeSelectProducts,
  makeSelectLoading,
  makeSelectIsEditing,
  makeSelectRankList,
  makeSelectRankLength,
  makeSelectNotification
};
