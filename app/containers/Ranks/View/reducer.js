import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    SET_RANK,
    SET_EDITING,
    SET_LOADING,
    NOTIFICATION_SAGA,
    SET_RANK_LENGTH
} from './constants';

const initialState = fromJS({
    loading: true,
    isEditing: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    rankList: [],
    length: 0
});

/**
 *
 * Ranks reducer
 *
 */
function ranksReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_RANK:
            return state.set( 'rankList', fromJS( action.payload ) );
        case SET_EDITING:
            return state.set( 'isEditing', action.payload );
        case SET_RANK_LENGTH:
            return state.set( 'length', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default ranksReducer;
