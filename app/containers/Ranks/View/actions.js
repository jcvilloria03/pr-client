import {
    DEFAULT_ACTION,
    GET_RANK,
    GET_EDIT_RANK,
    GET_DELETE_RANK
} from './constants';

/**
 * Get Rank List
 */
export function rankAction() {
    return {
        type: GET_RANK
    };
}
/**
 * Edit Rank List
 */
export function rankEditAction( payload ) {
    return {
        type: GET_EDIT_RANK,
        payload
    };
}

/**
 * Delete Rank List
 */
export function rankDeleteAction( payload ) {
    return {
        type: GET_DELETE_RANK,
        payload
    };
}
/**
 *
 * Ranks actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}
