/* eslint-disable react/sort-comp */
/* eslint-disable react/prop-types */
/* eslint-disable import/first */
import React from 'react';
import { Container } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import clone from 'lodash/clone';

import { createStructuredSelector } from 'reselect';
import makeSelectRanks, {
    makeSelectProducts,
    makeSelectNotification,
    makeSelectIsEditing,
    makeSelectLoading,
    makeSelectRankList,
    makeSelectRankLength
} from './selectors';

import * as RankActionList from './actions';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { subscriptionService } from 'utils/SubscriptionService';
import { browserHistory } from 'utils/BrowserHistory';
import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';
import { company } from 'utils/CompanyService';

import { H2, H3, H5 } from 'components/Typography';
import Sidebar from 'components/Sidebar';
import Button from 'components/Button';
import Loader from 'components/Loader';
import SalDropdown from 'components/SalDropdown';
import Input from 'components/Input';
import Table from 'components/Table';
import SnackBar from 'components/SnackBar';
import Modal from 'components/Modal';
import { isAnyRowSelected, getIdsOfSelectedRows } from 'components/Table/helpers';

import {
    PageWrapper,
    LoadingStyles
} from './styles';

/**
 *
 * Ranks
 *
 */
export class Ranks extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        loading: React.PropTypes.bool,
        rankList: React.PropTypes.array,
        rankAction: React.PropTypes.func,
        rankEditAction: React.PropTypes.func,
        rankDeleteAction: React.PropTypes.func,
        length: React.PropTypes.number,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    static defaultProps = {
        loading: true
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            delete_label: '',
            dataListView: this.props.rankList,
            selection_label: '',
            notification: {
                title: '',
                message: '',
                show: false,
                type: 'error'
            },
            editableCell: false,
            edited: null
        };
    }
    /**
        *
        * Ranks render method
        *
    */
    componentWillMount() {
        this.props.rankAction();
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.notification !== this.props.notification && this.setState({ notification: nextProps.notification });

        nextProps.rankList !== this.props.rankList
            && this.setState({
                displayedData: nextProps.rankList,
                label: formatPaginationLabel( this.rankFormsTable.tableComponent.state )
            });
    }

    handleTableChanges = () => {
        this.setState({
            selection_label: formatDeleteLabel()
        });
    }

    handleTableChanges = ( tableProps = this.rankFormsTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            delete_label: formatDeleteLabel()
        });
    };

    handlePageSizeChange = ( pageSize ) => (
        this.setState({
            page_size: pageSize,
            page: 1
        }, this.rankAction )
    )

    handlePageChange = ( page ) => (
        this.setState({ page }, this.rankAction )
    )

    getDropdownItems() {
        return [
            {
                children: <div>Delete</div>,
                onClick: () => this.DeleteModel.toggle()
            }
        ];
    }

    isCurrentlyEdited( row ) {
        return this.state.edited && this.state.edited.id === row.id;
    }

    /**
     * Start editing one row.
     * @param {Object} row
     */
    startEditingRow( row ) {
        this.setState({
            edited: clone( row )
        });
    }

    /**
   * Update edited row state.
   * @param {String} property
   * @param {Number} value
   */
    editedRowState( property, value ) {
        this.rankName.setState({ property: value });

        this.setState({
            edited: {
                ...this.state.edited,
                [ property ]: value
            }
        });
    }

    stopEditing = ( callback ) => {
        this.setState({
            edited: null
        }, callback );
        this.setState({ editableCell: false });
    }

    saveEdited() {
        this.setState({ editableCell: false });

        this.props.rankEditAction({
            id: this.state.edited.id,
            data: this.state.edited,
            ranks: this.props.rankList
        });
    }

    hanldeDelete = () => {
        const payload = getIdsOfSelectedRows( this.rankFormsTable );
        this.props.rankDeleteAction({ ids: payload, company_id: company.getLastActiveCompanyId() });
        this.DeleteModel.toggle();
        setTimeout( async () => {
            await this.props.rankAction();
        }, 6000 );
    }

    setPagination() {
        const {
            page,
            page_size: pageSize
        } = this.state;
        const { length } = this.props;

        const pageCeiling = page * pageSize;
        const shownRange = `${length ? ( pageCeiling + 1 ) - pageSize : 0} - ${pageCeiling > length ? length : pageCeiling}`;

        return `Showing ${shownRange} of ${length} entr${length > 1 || length === 0 ? 'ies' : 'y'}`;
    }

    getTableColumns() {
        return [
            {
                id: 'name',
                header: 'Rank Name',
                minWidth: 600,
                sortable: false,
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) && this.state.editableCell === true ? (
                        <div style={ { width: '100%' } }>
                            <Input
                                id="name"
                                type="text"
                                required
                                value={ this.state.edited.name }
                                ref={ ( ref ) => { this.rankName = ref; } }
                                onChange={ ( value ) => {
                                    this.editedRowState( 'name', value );
                                } }
                            />
                        </div>
                    ) : (
                        <div>{row.name}</div>
                    )
                )
            },
            {
                header: ' ',
                sortable: false,
                minWidth: 150,
                style: { justifyContent: 'center' },
                render: ({ row }) => (
                    this.isCurrentlyEdited( row ) && this.state.editableCell === true ? (
                        <div>
                            <Button
                                label={ <span>Cancel</span> }
                                type="grey"
                                size="small"
                                onClick={ this.stopEditing }
                            />

                            <Button
                                label={ <span>Save</span> }
                                type="action"
                                size="small"
                                onClick={ () => this.saveEdited() }
                            />
                        </div>
                    ) : (
                        <Button
                            label={ ( !!this.state.edited && this.state.edited.id === row.id && this.props.isEditing ) ? <Loader /> : 'Edit' }
                            type="grey"
                            size="small"
                            onClick={ () => {
                                this.startEditingRow( row );
                                this.setState({ editableCell: !this.state.editableCell });
                            } }
                        />
                    )
                )
            }
        ];
    }
    render() {
        const { notification, rankList, loading } = this.props;
        const { label } = this.state;
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll:
                this.props.products &&
                subscriptionService.isSubscribedToPayroll( this.props.products ),
            isSubscribedToTA:
                this.props.products &&
                subscriptionService.isSubscribedToTA( this.props.products )
        });
        const selectedItems = isAnyRowSelected( this.rankFormsTable );
        return (
            <div>
                <Helmet
                    title="Ranks"
                    meta={ [
                        { name: 'description', content: 'Description of Ranks' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Modal
                    title="Confirm Your Action"
                    ref={ ( ref ) => {
                        this.DeleteModel = ref;
                    } }
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.DeleteModel.toggle()
                        },
                        {
                            type: 'danger',
                            label: 'Yes',
                            onClick: this.hanldeDelete
                        }
                    ] }
                />

                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Ranks</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Ranks</H3>
                                <p>A rank determines an employee&apos; s place in your company&apos; s hierarchy.</p>
                            </div>

                            <div className="title">
                                <H5>Ranks List</H5>
                                <span className="title-Content">
                                    {selectedItems ? <p className="mb-0 mr-1">{this.state.delete_label}</p> : <p className="mb-0 mr-1">{label}</p>}
                                    <div>
                                        {selectedItems ? (
                                            <SalDropdown dropdownItems={ this.getDropdownItems() } />
                                        ) :
                                            <Button
                                                id="button-filter"
                                                label="Add Ranks"
                                                type="action"
                                                onClick={ () => browserHistory.push( '/company-settings/company-structure/ranks/add', true ) }
                                            />
                                        }
                                    </div>
                                </span>
                            </div>
                            <Table
                                loading={ this.props.rankFormsTable }
                                data={ rankList }
                                columns={ this.getTableColumns() }
                                pagination
                                selectable
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.rankFormsTable = ref; } }
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;
                                    this.setState({ delete_label: formatDeleteLabel( selectionLength ) });
                                } }
                            />
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    Ranks: makeSelectRanks(),
    products: makeSelectProducts(),
    rankList: makeSelectRankList(),
    loading: makeSelectLoading(),
    notification: makeSelectNotification(),
    length: makeSelectRankLength(),
    isEditing: makeSelectIsEditing()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        RankActionList,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Ranks );
