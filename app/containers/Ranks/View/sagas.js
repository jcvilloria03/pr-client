/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import { REINITIALIZE_PAGE } from 'containers/App/constants';
import { resetStore } from 'containers/App/sagas';

import {
    GET_LOADING,
    SET_LOADING,
    SET_EDITING,
    GET_RANK,
    SET_RANK,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SET_RANK_LENGTH,
    GET_EDIT_RANK,
    SET_EDIT_RANK,
    GET_DELETE_RANK
} from './constants';
/**
 * rank Saga functions
 */
export function* rankAction() {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/ranks`, { method: 'Get' });

        yield put({
            type: SET_RANK,
            payload: response && response.data || []
        });
        yield put({
            type: SET_RANK_LENGTH,
            payload: response && response.data.length || []
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * editRank saga
 */
export function* rankEditAction({ payload }) {
    const { id, data, ranks } = payload;
    try {
        yield put({
            type: SET_EDITING,
            payload: true
        });

        const res = yield call( Fetch, `/rank/${id}`,
            {
                method: 'PUT', data
            });

        yield put({
            type: SET_RANK,
            payload: ranks.map( ( item ) => {
                if ( item.id === res.id ) {
                    return { ...item, name: res.name };
                }

                return item;
            })
        });

        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Changed succefully save!',
            type: 'success'
        });
    } catch ( error ) {
        if ( error.response && error.response.status === 406 ) {
            yield call( notifyUser, {
                show: true,
                title: error.response ? error.response.statusText : 'Error',
                message: error.response ? error.response.data.message : error.statusText,
                type: 'error'
            });
        } else {
            yield call( notifyUser, error );
        }
        yield put({
            type: SET_EDIT_RANK,
            payload: false
        });
    } finally {
        yield put({
            type: SET_EDITING,
            payload: false
        });
    }
}

/**
 * Delete Data saga
 */
export function* rankDeleteAction({ payload }) {
    try {
        const res = yield call( Fetch, '/company/rank/is_in_use',
            {
                method: 'POST', data: payload
            });

        if ( res.in_use === 0 ) {
            const formData = new FormData();

            formData.append( '_method', 'DELETE' );
            formData.append( 'company_id', payload.company_id );

            payload.ids.forEach( ( id ) => {
                formData.append( 'rank_ids[]', id );
            });

            yield call( Fetch, '/rank/bulk_delete',
                {
                    method: 'POST', data: formData
                });
        } else {
            yield call( notifyUser, {
                show: true,
                title: 'Error',
                message: 'Rank name is in use',
                type: 'error'
            });
        }
    } catch ( error ) {
        if ( error.response && error.response.status === 406 ) {
            yield call( notifyUser, {
                show: true,
                title: error.response ? error.response.statusText : 'Error',
                message: error.response ? error.response.data.message : error.statusText,
                type: 'error'
            });
        } else {
            yield call( notifyUser, error );
        }
        yield put({
            type: SET_DELETE_RANK,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });
    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: '',
            message: '',
            show: false,
            type: 'general'
        }
    });
}

/**
 * Display a notification to user
 */
export function* notifyFromClient({ payload }) {
    yield call( notifyUser, payload );
}

/**
 * Watcher for NOTIFICATION
 *
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    yield call( resetStore );

    yield call( rankAction );
}

/**
 * Watcher for GET_RANK
 *
 */
export function* watchForRankData() {
    const watcher = yield takeEvery( GET_RANK, rankAction );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EDIT_RANK
 *
*/
export function* watchForEditRankData() {
    const watcher = yield takeEvery( GET_EDIT_RANK, rankEditAction );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EDIT_RANK
 *
*/
export function* watchForDeleteRankData() {
    const watcher = yield takeEvery( GET_DELETE_RANK, rankDeleteAction );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForNotifyUser,
    watchForReinitializePage,
    watchForRankData,
    watchForEditRankData,
    watchForDeleteRankData
];
