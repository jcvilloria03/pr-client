import { createSelector } from 'reselect';

/**
 * Direct selector to the bonuses state domain
 */
const selectViewBonusesDomain = () => ( state ) => state.get( 'bonuses' );

const makeSelectLoading = () => createSelector(
    selectViewBonusesDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectNumberOfDeleteUnavailable = () => createSelector(
    selectViewBonusesDomain(),
    ( substate ) => substate.get( 'numberOfDeleteUnavailable' )
);

const makeSelectDownloading = () => createSelector(
    selectViewBonusesDomain(),
    ( substate ) => substate.get( 'downloading' )
);

const makeSelectBonuses = () => createSelector(
    selectViewBonusesDomain(),
  ( substate ) => substate.get( 'bonuses' ).toJS()
);

const makeSelectFilterData = () => createSelector(
    selectViewBonusesDomain(),
  ( substate ) => substate.get( 'filterData' ).toJS()
);

const makeSelectPagination = () => createSelector(
  selectViewBonusesDomain(),
  ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectViewBonusesDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectDownloading,
  makeSelectNumberOfDeleteUnavailable,
  makeSelectBonuses,
  makeSelectFilterData,
  makeSelectNotification,
  makeSelectPagination
};
