import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_BONUSES,
    SET_PAGINATION,
    SET_FILTER_DATA,
    SET_NUMBER_OF_DELETE_UNAVAILABLE,
    NOTIFICATION_SAGA,
    SET_DOWNLOADING
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: false,
    downloading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    bonuses: [],
    numberOfDeleteUnavailable: null,
    filterData: {},
    pagination: {
        current_page: 0,
        from: 0,
        last_page: 0,
        per_page: 0,
        to: 0,
        total: 0
    }
});

/**
 * Bonuses view reducer
 * @param {object} state
 * @param {object} action
 */
function bonusesReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_DOWNLOADING:
            return state.set( 'downloading', action.payload );
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_NUMBER_OF_DELETE_UNAVAILABLE:
            return state.set( 'numberOfDeleteUnavailable', action.payload );
        case SET_BONUSES:
            return state.set( 'bonuses', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_FILTER_DATA:
            return state.set( 'filterData', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default bonusesReducer;
