import React from 'react';
import orderBy from 'lodash/orderBy';
import isEqual from 'lodash/isEqual';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import {
    makeSelectLoading,
    makeSelectBonuses,
    makeSelectFilterData,
    makeSelectNotification,
    makeSelectDownloading,
    makeSelectNumberOfDeleteUnavailable,
    makeSelectPagination
} from './selectors';
import * as bonusesActions from './actions';

import {
    PageWrapper,
    LoadingStyles,
    ConfirmBodyWrapperStyle
} from './styles';

import SubHeader from '../../SubHeader';
import Loader from '../../../components/Loader';
import SnackBar from '../../../components/SnackBar';
import Table from '../../../components/Table';
import { H2, H3, H5 } from '../../../components/Typography';
import Button from '../../../components/Button';
import Input from '../../../components/Input';
import SalConfirm from '../../../components/SalConfirm';
import Icon from '../../../components/Icon';
import Filter from './templates/filter';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import {
    formatPaginationLabel,
    formatDeleteLabel,
    formatCurrency,
    formatDate
} from '../../../utils/functions';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { PAYROLL_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';

/**
 * View Bonuses Component
 */
export class View extends React.Component {
    static propTypes = {
        getBonuses: React.PropTypes.func,
        getPaginatedBonuses: React.PropTypes.func,
        deleteBonuses: React.PropTypes.func,
        exportBonuses: React.PropTypes.func,
        bonuses: React.PropTypes.array,
        pagination: React.PropTypes.object,
        checkIsDeleteAvailable: React.PropTypes.func,
        numberOfDeleteUnavailable: React.PropTypes.number,
        filterData: React.PropTypes.shape({
            payrollGroups: React.PropTypes.array,
            bonusTypes: React.PropTypes.array,
            positions: React.PropTypes.array,
            locations: React.PropTypes.array,
            departments: React.PropTypes.array
        }),
        loading: React.PropTypes.bool,
        downloading: React.PropTypes.bool,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        products: React.PropTypes.array
    };

    static defaultProps = {
        loading: true,
        downloading: false,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: this.props.bonuses,
            showDelete: false,
            deleteLabel: '',
            permission: {
                view: true,
                create: true,
                delete: true,
                edit: true
            },
            showModal: false,
            showFilter: false,
            hasFiltersApplied: false,
            previousQuery: '',
            filtersApplied: []
        };

        this.searchInput = null;
        this.searchTypingTimeout = 0;
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized([
            'view.bonuses',
            'create.bonuses',
            'delete.bonuses',
            'edit.bonuses'
        ], ( authorization ) => {
            const authorized = authorization[ 'view.bonuses' ];

            if ( authorized ) {
                this.setState({ permission: {
                    view: authorization[ 'view.bonuses' ],
                    create: authorization[ 'create.bonuses' ],
                    delete: authorization[ 'delete.bonuses' ],
                    edit: authorization[ 'edit.bonuses' ]
                }});
            } else {
                browserHistory.replace( '/unauthorized' );
            }
        });
    }

    componentDidMount() {
        this.props.getBonuses();
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.bonuses, this.props.bonuses ) ) {
            this.handleSearch( nextProps.bonuses );
        }

        if ( this.props.numberOfDeleteUnavailable === null && nextProps.numberOfDeleteUnavailable !== null ) {
            if ( nextProps.numberOfDeleteUnavailable === 0 ) {
                this.deleteBonuses();
                return;
            }

            this.setState({
                showIsDeleteAvailableModal: nextProps.numberOfDeleteUnavailable !== 0
            });
        }
    }

    handleTableChanges = ( tableProps = this.bonusesTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });

        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    checkIsDeleteAvailable = () => {
        const bonusIDs = [];
        this.bonusesTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                bonusIDs.push( this.bonusesTable.props.data[ index ].id );
            }
        });

        this.props.checkIsDeleteAvailable( bonusIDs );
    }

    handleSearch = ( bonuses = this.props.bonuses ) => {
        let searchQuery = null;
        const dataToDisplay = bonuses;

        if ( this.searchInput ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        if ( (searchQuery != this.state.previousQuery) && (searchQuery || ( this.searchInput && searchQuery === '' )) ) {
            const currentFilters = this.state.filtersApplied.filter( ( filter ) => filter.type !== 'keyword' );
            const filtersToApply = [
                ...currentFilters,
                {
                    disabled: false,
                    type: 'keyword',
                    value: searchQuery
                }
            ];

            if ( this.searchTypingTimeout !== 0 ) {
                clearTimeout( this.searchTypingTimeout );
            }
            this.searchTypingTimeout = setTimeout( () => {
                if ( searchQuery !== '' || this.state.previousQuery !== '' ) {
                    this.props.getPaginatedBonuses({ page: 1, perPage: this.props.pagination.per_page, showLoading: false }, filtersToApply );
                }

                this.setState({
                    previousQuery: searchQuery,
                    filtersApplied: filtersToApply,
                    displayedData: this.props.bonuses,
                    hasFiltersApplied: true
                }, () => {
                    this.handleTableChanges();
                });
            }, 500 );
        } else {
            this.setState({ displayedData: dataToDisplay }, () => {
                this.handleTableChanges();
            });
        }
    }

    deleteBonuses = () => {
        const bonusIDs = [];
        this.bonusesTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                bonusIDs.push( this.bonusesTable.props.data[ index ].id );
            }
        });
        this.props.deleteBonuses( bonusIDs );
        this.setState({ showDelete: false });
    }

    exportBonuses = () => {
        const selectedBonuses = [];

        const sortingOptions = this.bonusesTable.state.sorting;

        this.bonusesTable.state.selected.forEach( ( rowSelected, index ) => {
            if ( rowSelected ) {
                selectedBonuses.push( this.bonusesTable.props.data[ index ]);
            }
        });

        const sortedBonuses = orderBy(
            selectedBonuses,
            sortingOptions.map( ( o ) => o.id ),
            sortingOptions.map( ( o ) => ( o.desc ? 'desc' : 'asc' ) )
        );

        this.props.exportBonuses( sortedBonuses.map( ( bonus ) => bonus.id ) );
    };

    toggleFilter = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    applyFilters = ( filters ) => {
        this.props.getBonuses( filters );
        this.setState({
            filtersApplied: filters,
            hasFiltersApplied: !!filters.length
        }, () => {
            this.handleTableChanges();
        });
    }

    resetFilters = () => {
        this.setState({
            showFilter: false,
            displayedData: this.props.bonuses,
            hasFiltersApplied: false
        });
    }

    goToBonusDetail = ( state, rowInfo, column ) => ({
        onDoubleClick: () => {
            if ( column.id !== 'select' ) {
                browserHistory.push( `/bonuses/${rowInfo.row.id}/detail` );
            }
        },
        style: {
            cursor: column.id !== 'select' ? 'pointer' : 'default'
        }
    })

    formatRecipients = ( recipients ) => {
        let departmentsNames = [];
        if ( this.props.filterData.departments ) {
            const filterDataDepartmentsIds = this.props.filterData.departments.map( ( d ) => d.id );
            const departments = recipients.departments
                    ? recipients.departments.filter( ( d ) => filterDataDepartmentsIds.includes( d.recipient_id ) )
                    : [];

            departmentsNames = departments.map( ( d ) =>
                    this.props.filterData.departments.find( ( fd ) => fd.id === d.recipient_id ).name
                );
        }

        let payrollGroupsNames = [];
        if ( this.props.filterData.payrollGroups ) {
            const filterDataPayrollGroupsIds = this.props.filterData.payrollGroups.map( ( pg ) => pg.id );
            const payrollGroups = recipients.payroll_groups
                    ? recipients.payroll_groups.filter( ( pg ) => filterDataPayrollGroupsIds.includes( pg.recipient_id ) )
                    : [];

            payrollGroupsNames = payrollGroups.map( ( p ) =>
                    this.props.filterData.payrollGroups.find( ( fpg ) => fpg.id === p.recipient_id ).name
                );
        }

        const employeesNames = recipients.employees ? recipients.employees.map( ( e ) => {
            const firstName = e.first_name ? `${e.first_name} ` : '';
            const middleName = e.middle_name ? `${e.middle_name} ` : '';
            const lastName = e.last_name ? e.last_name : '';

            return `${firstName}${middleName}${lastName}`;
        }) : [];

        return [
            ...employeesNames,
            ...payrollGroupsNames,
            ...departmentsNames
        ].reduce( ( string, name ) => `${name}, ${string}`, '' ).slice( 0, -2 );
    }

    /**
     * Component Render Method
     */
    render() {
        const tableColumns = [
            {
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                header: 'Recipients',
                accessor: 'recipients',
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {row.recipients}
                    </div>
                )
            },
            {
                header: 'Bonus Type',
                accessor: 'type.name',
                minWidth: 260,
                render: ({ row }) => (
                    <div>
                        {row.type ? ( row.type.name === '13TH_MONTH_PAY' ? '13th Month Pay' : row.type.name ) : ''}
                    </div>
                )
            },
            {
                header: 'Bonus per Recipient',
                accessor: 'bonus_per_recipient',
                minWidth: 260,
                render: ({ row }) => (
                    <div>
                        {row.bonus_per_recipient}
                    </div>
                )
            },
            {
                header: 'Total Bonus Amount',
                accessor: 'total_amount',
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {formatCurrency( row.total_amount )}
                    </div>
                )
            },
            {
                header: 'Schedule of Release',
                accessor: 'schedule_of_release',
                minWidth: 200,
                render: ({ row }) => (
                    <div>
                        {formatDate( row.schedule_of_release, DATE_FORMATS.DISPLAY )}
                    </div>
                )
            },
            {
                header: 'Status',
                accessor: 'term',
                minWidth: 150,
                render: ({ row }) => (
                    <div>
                        {row.status ? 'Released' : 'Not Yet Released'}
                    </div>
                )
            },
            {
                header: ' ',
                accessor: 'actions',
                minWidth: 100,
                sortable: false,
                style: { justifyContent: 'center' },
                render: ({ row }) => ( !row.status ? (
                    <Button
                        className={ this.state.permission.edit ? '' : 'hide' }
                        label={ <span>Edit</span> }
                        type="grey"
                        size="small"
                        to={ `/bonuses/${row.id}/edit` }
                    /> ) : null
                )
            }
        ];

        const dataForDisplay = this.state.displayedData ? this.state.displayedData.map( ( data ) => {
            const lastReleaseDetails = data.release_details.find( ( releaseDetails ) => !releaseDetails.status ) || data.release_details[ 0 ];
            return {
                ...data,
                schedule_of_release: lastReleaseDetails.date,
                status: lastReleaseDetails.status,
                recipients: this.formatRecipients( data.recipients ),
                bonus_per_recipient: data.type.basis === 'FIXED' ? `Fixed Bonus P ${formatCurrency( data.amount )}` : `Salary Based ${data.percentage}%`,
                total_amount: data.type.basis === 'FIXED' ? `P ${data.amount}` : `${data.percentage}%`
            };
        }) : [];

        return (
            <div>
                <Helmet
                    title="View Bonuses"
                    meta={ [
                        { name: 'description', content: 'Description of View' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SalConfirm
                    onConfirm={ this.checkIsDeleteAvailable }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                Are you sure you want to delete the record?
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Delete Bonuses"
                    buttonStyle="danger"
                    showCancel
                    confirmText="Delete"
                    cancelText="Cancel"
                    visible={ this.state.showModal }
                />
                <SalConfirm
                    onConfirm={ () => { this.setState({ showIsDeleteAvailableModal: false }); } }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                {`${this.props.numberOfDeleteUnavailable} bonus(es) cannot be deleted.`}
                            </div>
                        </ConfirmBodyWrapperStyle>
                    }
                    title="Cannot Delete Bonuses"
                    showCancel={ false }
                    buttonStyle="info"
                    confirmText="Ok"
                    visible={ this.state.showIsDeleteAvailableModal }
                />
                <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                <PageWrapper>
                    <Container>
                        <div className="loader" style={ { display: this.props.loading ? '' : 'none' } }>
                            <LoadingStyles>
                                <H2>Loading Bonuses.</H2>
                                <br />
                                <H3>Please wait...</H3>
                            </LoadingStyles>
                        </div>
                        <div className="content" style={ { display: this.props.loading ? 'none' : '' } }>
                            <div className="heading">
                                <H3>Bonuses</H3>
                                <p>
                                    You may add create and assign employee bonuses through this page.
                                    Bonuses can be set for both Fixed or Salary Based which either be
                                    scheduled as one-time or periodic.
                                </p>
                                <div style={ { textAlign: 'center' } } >
                                    <div className="main">
                                        <Button
                                            className={ this.state.permission.create ? '' : 'hide' }
                                            label="Add Bonuses"
                                            size="large"
                                            type="action"
                                            to="/bonuses/add"
                                        />
                                        <Button
                                            className={ this.state.permission.create ? '' : 'hide' }
                                            label="Add Bonus Types"
                                            size="large"
                                            type="neutral"
                                            onClick={ () => {
                                                window.location.href = '/company-settings/payroll/bonus-types/add';
                                            } }
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="title">
                                <H5>Bonuses List</H5>
                                <div className="search-wrapper">
                                    <Input
                                        className="search"
                                        id="search"
                                        ref={ ( ref ) => { this.searchInput = ref; } }
                                        onChange={ () => { this.handleSearch(); } }
                                        addon={ {
                                            content: <Icon name="search" />,
                                            placement: 'right'
                                        } }
                                    />
                                </div>
                                <span>
                                    {this.state.showDelete ? (
                                        <div>
                                            { this.state.deleteLabel }
                                            &nbsp;

                                            <Button
                                                className={ this.state.permission.view ? '' : 'hide' }
                                                label={ this.props.downloading ? <Loader /> : <span>Download</span> }
                                                type="neutral"
                                                onClick={ this.exportBonuses }
                                            />
                                            <Button
                                                className={ this.state.permission.delete ? '' : 'hide' }
                                                label={ <span>Delete</span> }
                                                type="danger"
                                                onClick={ () => {
                                                    this.setState({ showModal: false }, () => {
                                                        this.setState({ showModal: true });
                                                    });
                                                } }
                                            />
                                        </div>
                                    ) : (
                                        <div>
                                            { this.state.label }
                                            &nbsp;
                                            <Button
                                                label={ <span><Icon className="filter-icon" name="filter" /> Filter</span> }
                                                type={ this.state.hasFiltersApplied ? 'primary' : 'neutral' }
                                                onClick={ this.toggleFilter }
                                            />
                                        </div>
                                    )}
                                </span>
                            </div>
                            <div style={ { display: this.state.showFilter ? 'block' : 'none' } }>
                                <Filter
                                    filterData={ this.props.filterData }
                                    onCancel={ () => { this.resetFilters(); } }
                                    onApply={ ( values ) => { this.applyFilters( values ); } }
                                />
                            </div>
                            <Table
                                data={ dataForDisplay }
                                columns={ tableColumns }
                                pagination
                                onRowClick={ this.goToBonusDetail }
                                onDataChange={ this.handleTableChanges }
                                ref={ ( ref ) => { this.bonusesTable = ref; } }
                                selectable
                                onSelectionChange={ ({ selected }) => {
                                    const selectionLength = selected.filter( ( row ) => row ).length;

                                    this.setState({
                                        showDelete: selectionLength > 0,
                                        deleteLabel: formatDeleteLabel( selectionLength )
                                    });
                                } }
                                onPageChange={ ( data ) => this.props.getPaginatedBonuses({ page: data + 1, perPage: this.props.pagination.per_page, showLoading: true }, this.state.filtersApplied ) }
                                onPageSizeChange={ ( data ) => this.props.getPaginatedBonuses({ page: 1, perPage: data, showLoading: true }, this.state.filtersApplied ) }
                                page={ this.props.pagination.current_page - 1 }
                                pageSize={ this.props.pagination.per_page }
                                pages={ this.props.pagination.last_page }
                                manual
                            />
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    downloading: makeSelectDownloading(),
    bonuses: makeSelectBonuses(),
    filterData: makeSelectFilterData(),
    notification: makeSelectNotification(),
    numberOfDeleteUnavailable: makeSelectNumberOfDeleteUnavailable(),
    pagination: makeSelectPagination()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        bonusesActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( View );
