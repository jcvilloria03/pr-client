export const FILTER_TYPES = {
    BONUS_TYPE: 'bonus_type',
    PAYROLL_GROUP: 'payroll_group',
    EMPLOYEE: 'employee',
    DEPARTMENT: 'department'
};
