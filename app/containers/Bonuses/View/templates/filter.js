import React from 'react';
import MultiSelect from '../../../../components/MultiSelect';
import Button from '../../../../components/Button';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import { FilterWrapper } from './styles';

import { FILTER_TYPES } from './constants';

/**
 *
 * Filter Component
 *
 */
class Filter extends React.Component {

    static propTypes = {
        filterData: React.PropTypes.shape({
            bonusTypes: React.PropTypes.arr,
            departments: React.PropTypes.array,
            payrollGroups: React.PropTypes.array
        }),
        onCancel: React.PropTypes.func,
        onApply: React.PropTypes.func
    };

    constructor( props ) {
        super( props );

        this.state = {
            account: null,
        };
    }

    componentWillMount() {
        this.setState({
            companyId: company.getLastActiveCompanyId()
        });
    }

    onApply = () => {
        const filters = [];
        this.bonusTypes.state.value && this.bonusTypes.state.value.forEach( ( bonusType ) => {
            filters.push( Object.assign( bonusType, { type: FILTER_TYPES.BONUS_TYPE }) );
        });
        this.employees.state.value && this.employees.state.value.forEach( ( employee ) => {
            filters.push( Object.assign( employee, { type: FILTER_TYPES.EMPLOYEE }) );
        });
        this.departments.state.value && this.departments.state.value.forEach( ( department ) => {
            filters.push( Object.assign( department, { type: FILTER_TYPES.DEPARTMENT }) );
        });
        this.payrollGroups.state.value && this.payrollGroups.state.value.forEach( ( payrollGroup ) => {
            filters.push( Object.assign( payrollGroup, { type: FILTER_TYPES.PAYROLL_GROUP }) );
        });
        this.props.onApply( filters );
    }

    onCancel = () => {
        this.resetFilters();
        this.props.onCancel();
    }

    getBonusTypes = () => {
        if ( !this.props.filterData.bonusTypes ) {
            return [];
        }

        const bonusTypes = this.props.filterData.bonusTypes.map( ( bonusType ) => (
            this.formatDataForMultiselect( bonusType )
        ) );

        return bonusTypes;
    }

    getEmployees = ( keyword, callback ) => {
        const { companyId } = this.state;

        Fetch( `/company/${companyId}/employees?include=payroll&mode=MINIMAL&keyword=${keyword}&page=1&per_page=10`, { method: 'GET' })
          .then( ( result ) => {
                const employees = result.data.map( ( employee ) => {
                const firstName = employee.first_name ? `${employee.first_name} ` : '';
                const middleName = employee.middle_name ? `${employee.middle_name} ` : '';
                const lastName = employee.last_name ? employee.last_name : '';

                return {
                  value: employee.id,
                  label: `${firstName}${middleName}${lastName} `,
                  disabled: false
                };
            });
              callback( null, { options: employees });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    getDepartments = () => {
        if ( !this.props.filterData.departments ) {
            return [];
        }

        const departments = this.props.filterData.departments.map( ( department ) => (
            this.formatDataForMultiselect( department )
        ) );

        return departments;
    }

    getPayrollGroups = () => {
        if ( !this.props.filterData.payrollGroups ) {
            return [];
        }

        const payrollGroups = this.props.filterData.payrollGroups.map( ( payrollGroup ) => (
            this.formatDataForMultiselect( payrollGroup )
        ) );

        return payrollGroups;
    }

    resetFilters = () => {
        this.bonusTypes.setState({ value: null });
        this.employees.setState({ value: null });
        this.departments.setState({ value: null });
        this.payrollGroups.setState({ value: null }, () => {
            this.onApply();
        });
    }

    formatDataForMultiselect = ( data ) => (
        {
            value: data.id,
            label: data.name === '13TH_MONTH_PAY' ? '13TH_MONTH_PAY' : data.name,
            disabled: false
        }
    )

    render() {
        return (
            <FilterWrapper>
                <div className="row">
                    <div className="col-xs-3">
                        <MultiSelect
                            id="bonus_types"
                            label={
                                <span>Bonus Types</span>
                            }
                            ref={ ( ref ) => { this.bonusTypes = ref; } }
                            data={ this.getBonusTypes() }
                            placeholder="All bonus types"
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            async
                            id="employees"
                            label={
                                <span>Employees</span>
                            }
                            ref={ ( ref ) => { this.employees = ref; } }
                            placeholder="All employees"
                            loadOptions={ this.getEmployees }
                            autoload={ false }
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            id="departments"
                            label={
                                <span>Departments</span>
                            }
                            ref={ ( ref ) => { this.departments = ref; } }
                            data={ this.getDepartments() }
                            placeholder="All departments"
                        />
                    </div>
                    <div className="col-xs-3">
                        <MultiSelect
                            id="payroll_groups"
                            label={
                                <span>Payroll Groups</span>
                            }
                            ref={ ( ref ) => { this.payrollGroups = ref; } }
                            data={ this.getPayrollGroups() }
                            placeholder="All payroll groups"
                        />
                    </div>
                </div>
                <div className="sl-c-filter-actions">
                    <div className="sl-c-filter-reset">
                        <Button
                            label="Reset to default filters"
                            size="large"
                            type="neutral"
                            onClick={ this.resetFilters }
                        />
                    </div>
                    <div className="sl-c-filter-buttons">
                        <Button
                            label="Cancel"
                            size="large"
                            type="neutral"
                            onClick={ this.onCancel }
                        />
                        <Button
                            label="Apply"
                            size="large"
                            type="action"
                            onClick={ this.onApply }
                        />
                    </div>
                </div>
            </FilterWrapper>
        );
    }
}

export default Filter;
