import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import fileDownload from 'js-file-download';

import {
    SET_LOADING,
    GET_PAGINATED_BONUSES,
    SET_PAGINATION,
    GET_BONUSES,
    SET_BONUSES,
    GET_COMPANY_EMPLOYEES,
    SET_FILTER_DATA,
    DELETE_BONUSES,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    IS_DELETE_AVAILABLE,
    SET_NUMBER_OF_DELETE_UNAVAILABLE,
    EXPORT_BONUSES,
    SET_DOWNLOADING
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

/**
 * Get bonuses for table
 */
export function* getBonuses( filters ) {
    try {
        yield put({
            type: SET_LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        yield call( getPaginatedBonuses, {
            payload: {
                page: 1,
                perPage: 10,
                showLoading: true
            },
            filters
        });

        const filterData = {};
        const bonusTypes = yield call( Fetch, `/company/${companyId}/other_income_types/bonus_type`, { method: 'GET' });
        const locations = yield call( Fetch, `/philippine/company/${companyId}/locations`, { method: 'GET' });
        const departments = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });
        const positions = yield call( Fetch, `/company/${companyId}/positions`, { method: 'GET' });
        const payrollGroups = yield call( Fetch, `/philippine/company/${companyId}/payroll_groups`, { method: 'GET' });

        filterData.bonusTypes = bonusTypes.data;
        filterData.locations = locations.data;
        filterData.departments = departments.data;
        filterData.positions = positions.data;
        filterData.payrollGroups = payrollGroups.data;

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * get paginated bonuses
 */
export function* getPaginatedBonuses({ payload, filters }) {
    const { page, perPage, showLoading } = payload;
    const bonusFilter = filters.filters ? filters.filters : filters;

    try {
        if ( showLoading ) {
            yield put({
                type: SET_LOADING,
                payload: true
            });
        }

        let filtersQueryParams = '';
        if ( bonusFilter !== undefined && bonusFilter.length > 0 ) {
            const filtersIds = {
                other_income_type_ids: [],
                department_ids: [],
                employee_ids: [],
                payroll_group_ids: []
            };

            bonusFilter.map( ( filter ) => {
                if ( filter.type !== 'keyword' ) {
                    const filterFieldName = filter.type === 'bonus_type' ? 'other_income_type_ids' : `${filter.type}_ids`;
                    if ( filter.disabled === false ) {
                        filtersIds[ filterFieldName ].push( filter.value );
                    }
                } else {
                    filtersQueryParams += `&filter[keyword]=${filter.value}`;
                }

                return null;
            });

            for ( const [ field, ids ] of Object.entries( filtersIds ) ) {
                if ( ids.length > 0 ) {
                    for ( const filterId of ids ) {
                        filtersQueryParams += `&filter[${field}][]=${filterId}`;
                    }
                }
            }
        }

        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_incomes/bonus_type?page=${page}&per_page=${perPage}${filtersQueryParams}`, { method: 'GET' });
        const { data, ...others } = response;

        yield put({
            type: SET_PAGINATION,
            payload: others
        });

        yield put({
            type: SET_BONUSES,
            payload: data
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        if ( showLoading ) {
            yield put({
                type: SET_LOADING,
                payload: false
            });
        }
    }
}

/**
 * Get Company Employees
 */
export function* getCompanyEmployees({ payload }) {
    try {
        const keyword = payload.keyword ? payload.keyword : '';
        const companyId = company.getLastActiveCompanyId();
        const filterData = {};
        let filterEmployeeIdsQueryString = '';
        let keywordParam = '';

        if ( keyword ) {
            keywordParam += `&keyword=${keyword}`;
        }

        if ( payload.filterEmployeeIds ) {
            const filterEmployeeIds = payload.filterEmployeeIds;
            if ( filterEmployeeIds.length > 0 ) {
                for ( const employeeId of filterEmployeeIds ) {
                    filterEmployeeIdsQueryString += `&filter[employee_ids][]=${employeeId}`;
                }
            }
        }

        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL${keywordParam}${filterEmployeeIdsQueryString}`, { method: 'GET' });

        filterData.employees = employees.data;

        yield put({
            type: SET_FILTER_DATA,
            payload: filterData
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_LOADING,
            payload: false
        });
    }
}

/**
 * Check is delete available
 */
export function* checkIsDeleteBonusesAvailable({ payload }) {
    try {
        yield put({
            type: SET_NUMBER_OF_DELETE_UNAVAILABLE,
            payload: null
        });

        const companyId = company.getLastActiveCompanyId();

        const response = yield call( Fetch, `/company/${companyId}/other_income/is_delete_available`, {
            method: 'POST',
            data: {
                ids: payload
            }
        });

        yield put({
            type: SET_NUMBER_OF_DELETE_UNAVAILABLE,
            payload: response.unavailable
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Batch delete bonuses
 */
export function* deleteBonuses({ payload }) {
    try {
        yield call( Fetch, `/company/${company.getLastActiveCompanyId()}/other_income`, {
            method: 'DELETE',
            data: {
                ids: payload
            }
        });
        yield call( getBonuses );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Export bonuses
 */
export function* exportBonuses({ payload }) {
    try {
        yield put({
            type: SET_DOWNLOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const response = yield call( Fetch, `/company/${companyId}/other_incomes/bonus_type/download`, {
            method: 'POST',
            data: {
                ids: payload,
                company_id: company.getLastActiveCompanyId()
            }
        });

        fileDownload( response, 'bonuses.csv' );
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DOWNLOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( getBonuses );
}

/**
 * Watcher for GET bonuses
 */
export function* watchForGetBonuses() {
    const watcher = yield takeEvery( GET_BONUSES, getBonuses );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for IS_DELETE_AVAILABLE
 */
export function* watchForCheckIsDeleteBonusesAvailable() {
    const watcher = yield takeEvery( IS_DELETE_AVAILABLE, checkIsDeleteBonusesAvailable );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for DELETE bonuses
 */
export function* watchForDeleteBonuses() {
    const watcher = yield takeEvery( DELETE_BONUSES, deleteBonuses );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for EXPORT bonuses
 */
export function* watchForExportBonuses() {
    const watcher = yield takeEvery( EXPORT_BONUSES, exportBonuses );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_COMPANY_EMPLOYEES
 */
export function* watchForGetCompanyEmployees() {
    const watcher = yield takeEvery( GET_COMPANY_EMPLOYEES, getCompanyEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Paginated bonuses
 */
export function* watchForPaginatedBonuses() {
    const watcher = yield takeEvery( GET_PAGINATED_BONUSES, getPaginatedBonuses );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForGetBonuses,
    watchForPaginatedBonuses,
    watchForGetCompanyEmployees,
    watchForCheckIsDeleteBonusesAvailable,
    watchForDeleteBonuses,
    watchForExportBonuses,
    watchForNotifyUser,
    watchForReinitializePage
];
