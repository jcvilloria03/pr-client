import {
    GET_BONUSES,
    GET_PAGINATED_BONUSES,
    GET_COMPANY_EMPLOYEES,
    DELETE_BONUSES,
    NOTIFICATION,
    EXPORT_BONUSES,
    IS_DELETE_AVAILABLE
} from './constants';

/**
 * Fetch list of bonuses
 */
export function getBonuses( filters ) {
    return {
        type: GET_BONUSES,
        filters
    };
}

/**
 * Fetch list of bonuses
 */
export function getPaginatedBonuses( payload, filters ) {
    return {
        type: GET_PAGINATED_BONUSES,
        filters,
        payload,
        showLoading: true
    };
}

/**
 * Get Company Employees
 */
export function getCompanyEmployees( payload ) {
    return {
        type: GET_COMPANY_EMPLOYEES,
        payload
    };
}

/**
 * Check is delete available for bonuses
 * @param {array} IDs
 */
export function checkIsDeleteAvailable( IDs ) {
    return {
        type: IS_DELETE_AVAILABLE,
        payload: IDs
    };
}

/**
 * Delete bonuses
 * @param {array} IDs
 */
export function deleteBonuses( IDs ) {
    return {
        type: DELETE_BONUSES,
        payload: IDs
    };
}

/**
 * Export bonuses
 * @param {array} IDs
 */
export function exportBonuses( IDs ) {
    return {
        type: EXPORT_BONUSES,
        payload: IDs
    };
}

/**
 * Display a notification to a user
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
