import styled from 'styled-components';

const MainWrapper = styled.div`
    padding-top: 76px;
    background: #fff;

    .nav {
        padding: 10px 20px;
        background: #f0f4f6;
        margin-bottom: 50px;
    }

    .loader {
        & > div {
            font-size: 30px;

            .anim3 {
                &:before {
                    width: 24px;
                    height: 24px;
                    border-width: 4px;
                    border-color: #444;
                }

                &:after {
                    background-color: #666;
                    width: 24px;
                    height: 6px;
                }
            }
        }
    }

    .radiogroup {
        padding: 0;
        & > span {
            padding: 0 15px;
            color: #5b5b5b;
            font-size: 14px;
            margin-bottom: 4px;
            margin-right: 1rem;
            font-weight: 400;
        }
    }

    .date {
        & > div > span {
            display: block;
        }
        .DayPickerInput {
            display: block;

            input {
                width: 100%;
            }
        }
    }

    .heading {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 0 auto 30px auto;

        h3 {
            font-weight: 600;
        }

        p {
            text-align: center;
            max-width: 800px;
        }
    }

    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;
        margin-top: 20px;

        button {
            min-width: 120px;
        }
    }

    .hide {
        display: none;
    }

    .flex-align-center {
        display: flex;
        align-items: center;
    }

    .summary-panel {
        font-size: 12px;
        display: flex;
        padding: 25px;
        background-color: rgba(229, 246, 252, 1);
        border-radius: 5px;
        margin-bottom: 70px;

        &__column {
            flex-grow: 1;
            flex-basis: 0;
            padding: 10px;

            &:not(:last-child) {
                border-right: solid 2px rgb(216, 216, 216);
                margin-right: 40px
            }

            p {
                margin: 0;
            }
        }
    }

    .release-date-header {
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 15px;
    }

    .release-date-summary {
        display: flex;
        padding: 3px;
        background-color: rgba(229, 246, 252, 1);
        border-radius: 5px;
        align-items: center;
        margin-left: 20px;

        &__amount {
            margin: 0 15px;
        }

        &__percentage {
            background-color: rgba(255, 255, 255, 1);
            border-radius: 5px;
            padding: 5px 25px;
        }
    }

    .release-date-details {
        border: 1px solid rgb(142, 142, 142);
        padding: 20px 30px;
        margin-bottom: 50px;

        &--summary {
            background-color: rgb(249, 249, 249);
        }
    }

    .input-group-addon {
        background-color: #fff !important;
        border-radius: 0;
    }

    .input-group > .form-control {
        border-right: 0;
        z-index: 0;
    }
`;

const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        display: flex;
        align-self: center;
    }
`;

export {
    MainWrapper,
    ConfirmBodyWrapperStyle
};
