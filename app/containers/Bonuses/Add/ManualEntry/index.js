import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment';

import SnackBar from '../../../../components/SnackBar';
import SalConfirm from '../../../../components/SalConfirm';
import Loader from '../../../../components/Loader';
import Button from '../../../../components/Button';
import SalSelect from '../../../../components/Select';
import Switch from '../../../../components/Switch';
import Input from '../../../../components/Input';
import DatePicker from '../../../../components/DatePicker';
import MultiSelect from '../../../../components/MultiSelect';

import {
    formatCurrency,
    stripNonDigit,
    getEmployeeFullName,
    formatCurrencyToDecimalNotation,
    checkIfAnyEmployeesBelongToMultipleGroups,
    truncate,
    formatDate
} from '../../../../utils/functions';
import { browserHistory } from '../../../../utils/BrowserHistory';
import { isAuthorized } from '../../../../utils/Authorization';
import { DATE_FORMATS } from '../../../../utils/constants';

import {
    MainWrapper,
    ConfirmBodyWrapperStyle
} from './styles';

import {
    makeSelectBonusTypes,
    makeSelectFormOptions,
    makeSelectSubmitted,
    makeSelectErrors,
    makeSelectNotification,
    makeSelectLoading
} from '../selectors';

import * as createBonusActions from '../actions';

const inputTypes = {
    input: [],
    select: [],
    datePicker: []
};

const BONUS_TYPE_FIXED_BASIS = 'FIXED';
const BONUS_TYPE_SALARY_BASED = 'SALARY_BASED';
const BONUS_TYPE_PERIODIC_FREQUENCY = 'PERIODIC';
const BONUS_TYPE_ONE_TIME_FREQUENCY = 'ONE_TIME';

const BONUS_TYPE = {
    BASIS: {
        [ BONUS_TYPE_FIXED_BASIS ]: 'Fixed',
        [ BONUS_TYPE_SALARY_BASED ]: 'Salary Based'
    },
    FREQUENCY: {
        [ BONUS_TYPE_PERIODIC_FREQUENCY ]: 'Periodic',
        [ BONUS_TYPE_ONE_TIME_FREQUENCY ]: 'One-time'
    }
};

const BASIS = [
    { label: 'Current salary', value: 'CURRENT_BASIC_SALARY' },
    { label: 'Average salary', value: 'AVERAGE_OF_BASIC_SALARY' },
    { label: 'Gross salary', value: 'GROSS_SALARY' },
    { label: 'Basic Pay', value: 'BASIC_PAY' }
];

const RELEASE_DETAILS_STATUS = [
    { label: 'Disbursed', value: 1 },
    { label: 'Not yet released', value: 0 }
];

/**
 * ManualEntry Container
 */
export class ManualEntry extends React.Component {
    static propTypes = {
        initializeData: React.PropTypes.func,
        formOptions: React.PropTypes.object,
        bonusTypes: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitForm: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        errors: React.PropTypes.object,
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        previousRoute: React.PropTypes.object
    }

    static defaultProps = {
        loading: true,
        errors: {}
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            bonusData: {
                type: null,
                amount: '',
                basis: '',
                percentage: '',
                recipients: {
                    employees: [],
                    payroll_groups: [],
                    departments: []
                },
                release_details: [],
                auto_assign: true
            },
            releaseDetails: {
                date: '',
                disburse_through_special_pay_run: false,
                prorate_based_on_tenure: false,
                coverage_from: '',
                coverage_to: '',
                status: null
            },
            editReleaseDetails: null,
            showModal: false,
            showBatchModal: false,
            errors: {},
            prepopulatedEmployee: null,
            isWarningMessageVisible: false
        };

        this.bonusType = null;
    }

    componentWillMount() {
        isAuthorized(['create.bonuses'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        this.props.initializeData();

        let prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddBonus' ) );
        prepopulatedEmployee = prepopulatedEmployee ? {
            value: prepopulatedEmployee.id,
            field: 'employees',
            label: `${prepopulatedEmployee.first_name}${prepopulatedEmployee.middle_name ? ` ${prepopulatedEmployee.middle_name} ` : ' '}${prepopulatedEmployee.last_name}`
        } : null;

        this.setState({ prepopulatedEmployee });
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.errors !== this.props.errors && this.setState({ errors: nextProps.errors }, () => {
            this.handleApiErrors();
        });

        nextProps.submitted !== this.props.submitted && this.submitButton.setState({ disabled: nextProps.submitted });
    }

    componentWillUnmount() {
        this.props.resetStore();
    }

    getRecipientsOptions() {
        const { formOptions } = this.props;

        const departments = formOptions.departments.map( ( department ) => ({
            value: department.id, label: department.name, field: 'departments'
        }) );
        const payrollGroups = formOptions.payrollGroups.map( ( payrollGroup ) => ({
            value: payrollGroup.id, label: payrollGroup.name, field: 'payroll_groups'
        }) );
        const employees = formOptions.employees.map( ( employee ) => ({
            value: employee.id, label: getEmployeeFullName( employee ), field: 'employees'
        }) );

        return [ ...departments, ...payrollGroups, ...employees ];
    }

    getTotalAmountOfAlreadyReleased = ( releases ) => (
        releases.reduce( ( sum, release ) => (
            release.status && release.id ? sum + release.amount : sum
        ), 0 )
    )

    getTotalAmountOfReleases = ( releases ) => (
        releases.reduce( ( acc, release ) => (
            acc + release.amount
        ), 0 )
    )

    getAmountPerRelease = ( index ) => (
        this.readAmountData( this.state.bonusData.release_details[ index ])
    )

    getLastAddedReleaseIndex = ( releases ) => {
        let index = -1;
        releases.forEach( ( release, i ) => {
            if ( !release.id || ( release.id && !release.status ) || ( release.id && release.status && release.edited ) ) {
                index = i;
            }
        });
        return index;
    }

    calculatePercentage = ( amountOfRelease ) => (
        ( amountOfRelease / ( ( parseFloat( stripNonDigit( this.state.bonusData.amount ) ) / 100 ) || 1 ) )
    )

    partitionReleasesByReleasedAndNew = ( releases = this.state.bonusData.release_details ) => (
        releases.reduce( ([ released, newReleases ], release ) => (
            release.id && release.status ? [[ ...released, release ], newReleases ] : [ released, [ ...newReleases, release ]]
        ), [[], []])
    )

    remapReleasesWithRecalculatedAmounts = ( releases, amount ) => {
        const [ released, newReleases ] = this.partitionReleasesByReleasedAndNew( releases ); // eslint-disable-line
        const amountOfAlreadyReleased = this.getTotalAmountOfAlreadyReleased( releases );

        return releases.map( ( release ) => {
            if ( release.id && release.status ) {
                return { ...release, amount: truncate( release.amount ) };
            }
            return { ...release, amount: truncate( ( amount - amountOfAlreadyReleased ) / ( newReleases.length || 1 ) ) };
        });
    }

    readAmountData = ( release ) => (
        release.amount ? release.amount : 0
    )

    recalculateReleaseAmounts = ( releases ) => {
        const totalAmount = Number( stripNonDigit( this.state.bonusData.amount ) );
        const recalculatedReleases = this.remapReleasesWithRecalculatedAmounts( releases, totalAmount );
        const sum = this.getTotalAmountOfReleases( recalculatedReleases );
        const remainder = totalAmount - sum;
        const lastAddedReleaseIndex = this.getLastAddedReleaseIndex( recalculatedReleases );
        if ( lastAddedReleaseIndex > -1 ) {
            const amountOfLastAddedRelease = recalculatedReleases[ lastAddedReleaseIndex ].amount;
            recalculatedReleases[ lastAddedReleaseIndex ].amount = amountOfLastAddedRelease + parseFloat( remainder.toFixed( 2 ) );
        }

        return recalculatedReleases;
    }

    handleApiErrors() {
        const keys = Object.keys( this.state.errors );
        if ( keys.length ) {
            keys.forEach( ( key ) => {
                if ( inputTypes.input.includes( key ) || inputTypes.select.includes( key ) ) {
                    this[ key ].setState({ error: true, label: this.state.errors[ key ][ 0 ] });
                } else if ( inputTypes.datePicker.includes( key ) ) {
                    this[ key ].setState({ error: true, message: this.state.errors[ key ][ 0 ] });
                }
            });
        }
    }

    updateBonusData( key, value, onSetStateFinish ) {
        if ( typeof value === 'undefined' ) {
            return;
        }
        const dataClone = Object.assign({}, this.state.bonusData );
        dataClone[ key ] = value !== '' && value !== null ? value : '';
        if ( key === 'type_id' ) {
            this.bonusType = this.props.bonusTypes.find( ( bonusType ) => bonusType.id === value );
            dataClone.release_details = [];
        }
        this.setState( ( prevState ) => ({
            bonusData: Object.assign({}, prevState.bonusData, dataClone ),
            releaseDetails: Object.assign({}, prevState.releaseDetails, { prorate_based_on_tenure: this.bonusType.basis === BONUS_TYPE_SALARY_BASED })
        }), onSetStateFinish );
    }

    updateReleaseDetailsData( key, value ) {
        if ( typeof value === 'undefined' ) {
            return;
        }
        const dataClone = Object.assign({}, this.state.releaseDetails );
        dataClone[ key ] = value !== '' && value !== null ? value : '';
        dataClone.prorate_based_on_tenure = this.bonusType.basis === BONUS_TYPE_SALARY_BASED;

        this.setState( ( prevState ) => ({ releaseDetails: Object.assign({}, prevState.releaseDetails, dataClone ) }) );
    }

    validateBonusData() {
        let valid = true;

        if ( !this.type_id._checkRequire( this.type_id.state.value ) ) {
            valid = false;
        }

        if ( !this.recipients._checkRequire( this.recipients.state.value ) ) {
            valid = false;
        }

        if ( this.bonusType.basis === BONUS_TYPE_FIXED_BASIS ) {
            if ( this.amount._validate( stripNonDigit( this.amount.state.value ) ) ) {
                valid = false;
            }
        } else if ( this.bonusType.basis === BONUS_TYPE_SALARY_BASED ) {
            if ( !this.basis._checkRequire( this.basis.state.value ) ) {
                valid = false;
            }

            if ( this.monthly_salary_percentage._validate( stripNonDigit( this.monthly_salary_percentage.state.value ) ) ) {
                valid = false;
            }
        }

        if ( this.bonusType.frequency === BONUS_TYPE_PERIODIC_FREQUENCY ) {
            if ( this.number_of_releases._validate( this.number_of_releases.state.value ) ) {
                valid = false;
            }
        }

        if ( this.bonusType.frequency === BONUS_TYPE_ONE_TIME_FREQUENCY ) {
            valid = this.validateReleaseDetails();
        }

        return valid;
    }

    validateReleaseDetails() {
        let valid = true;

        if ( this.release_date.checkRequired() ) {
            valid = false;
        }

        if ( !this.status._checkRequire( this.status.state.value ) ) {
            valid = false;
        }

        if ( this.bonusType.basis === BONUS_TYPE_SALARY_BASED ) {
            if ( this.coverage_from.checkRequired() ) {
                valid = false;
            }

            if ( this.state.releaseDetails.coverage_to
                && this.state.releaseDetails.coverage_from
                && !moment( this.state.releaseDetails.coverage_to, [ DATE_FORMATS.API, DATE_FORMATS.DISPLAY ]).isAfter( this.state.releaseDetails.coverage_from, [ DATE_FORMATS.API, DATE_FORMATS.DISPLAY ])
            ) {
                const message = 'The coverage to date must be a date after coverage from.';
                this.coverage_to.setState({ error: true, message });

                valid = false;
            } else if ( this.coverage_to.checkRequired() ) {
                valid = false;
            }
        }

        return valid;
    }

    submitForm = () => {
        if ( this.validateBonusData() ) {
            const bonusData = {
                ...this.state.bonusData,
                amount: Number( stripNonDigit( this.state.bonusData.amount ) ),
                recipients: {
                    employees: this.state.prepopulatedEmployee ? [this.state.prepopulatedEmployee.value] : this.state.bonusData.recipients.employees.map( ( employee ) => employee.value ),
                    departments: this.state.bonusData.recipients.departments.map( ( department ) => department.value ),
                    payroll_groups: this.state.bonusData.recipients.payroll_groups.map( ( payrollGroup ) => payrollGroup.value )
                }
            };

            if ( this.bonusType.frequency === BONUS_TYPE_ONE_TIME_FREQUENCY ) {
                bonusData.release_details.push({ ...this.state.releaseDetails });
            }

            this.props.submitForm({
                ...bonusData,
                previousRouteName: this.props.previousRoute.name
            });
        }
    }

    addReleaseDetails = () => {
        if ( this.validateReleaseDetails() ) {
            let releaseDetails = [ ...this.state.bonusData.release_details, { ...this.state.releaseDetails }];
            releaseDetails = this.recalculateReleaseAmounts( releaseDetails );
            this.updateBonusData( 'release_details', releaseDetails );

            this.setState({
                releaseDetails: {
                    date: '',
                    disburse_through_special_pay_run: false,
                    prorate_based_on_tenure: false,
                    coverage_from: '',
                    coverage_to: '',
                    status: null
                }
            });
        }
    }

    editReleaseDetails( index ) {
        this.setState({
            releaseDetails: { ...this.state.bonusData.release_details[ index ] },
            editReleaseDetails: index
        });
    }

    deleteReleaseDetails( index ) {
        const releaseDetails = this.state.bonusData.release_details;
        const newAmount = parseFloat( stripNonDigit( this.state.bonusData.amount ) ) - parseFloat( this.getAmountPerRelease( index ) );
        this.setState({
            bonusData: {
                ...this.state.bonusData,
                amount: formatCurrency( newAmount ),
                release_details: releaseDetails.slice( 0, index ).concat( releaseDetails.slice( index + 1 ) )
            }
        });
    }

    updateReleaseDetails = () => {
        if ( this.validateReleaseDetails() ) {
            this.updateBonusData( 'release_details', this.state.bonusData.release_details.map( ( releaseDetails, index ) => {
                if ( index === this.state.editReleaseDetails ) {
                    return { ...this.state.releaseDetails };
                }

                return releaseDetails;
            }) );

            this.setState({
                releaseDetails: {
                    date: '',
                    disburse_through_special_pay_run: false,
                    prorate_based_on_tenure: false,
                    coverage_from: '',
                    coverage_to: '',
                    status: null
                },
                editReleaseDetails: null
            });
        }
    }

    renderReleaseDetailsForm() {
        return (
            <div>
                <div className="row">
                    <div className="col-xs-4 date">
                        <DatePicker
                            label="Release date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            selectedDay={ this.state.releaseDetails.date }
                            required
                            ref={ ( ref ) => { this.release_date = ref; } }
                            onChange={ ( value ) => {
                                const selectedDay = formatDate( value, DATE_FORMATS.API );
                                if ( selectedDay === this.state.releaseDetails.date ) {
                                    return;
                                }

                                this.setState({
                                    releaseDetails: {
                                        ...this.state.releaseDetails,
                                        date: selectedDay
                                    }
                                });
                            } }
                        />
                    </div>
                    <div className="col-xs-4">
                        <span>* Disburse through Special Pay Run?</span>
                        <div style={ { marginTop: '10px' } }>
                            <Switch
                                checked={ this.state.releaseDetails.disburse_through_special_pay_run }
                                onChange={ ( value ) => { this.updateReleaseDetailsData( 'disburse_through_special_pay_run', value ); } }
                            />
                        </div>
                    </div>
                    { this.bonusType.basis === BONUS_TYPE_FIXED_BASIS && (
                        <div className="col-xs-4">
                            <SalSelect
                                id="status"
                                label="Status"
                                required
                                key="status"
                                data={ RELEASE_DETAILS_STATUS }
                                value={ this.state.releaseDetails.status }
                                placeholder="Choose an option"
                                ref={ ( ref ) => { this.status = ref; } }
                                onChange={ ({ value }) => { this.updateReleaseDetailsData( 'status', value ); } }
                            />
                        </div>
                    )}
                </div>
                {this.bonusType.basis === BONUS_TYPE_SALARY_BASED ? (
                    <div className="row">
                        <div className="col-xs-4 date">
                            <DatePicker
                                label="Coverage from"
                                dayFormat={ DATE_FORMATS.DISPLAY }
                                required
                                selectedDay={ this.state.releaseDetails.coverage_from }
                                ref={ ( ref ) => { this.coverage_from = ref; } }
                                onChange={ ( value ) => {
                                    const selectedDay = formatDate( value, DATE_FORMATS.API );
                                    if ( selectedDay === this.state.releaseDetails.coverage_from ) {
                                        return;
                                    }

                                    this.setState({
                                        releaseDetails: {
                                            ...this.state.releaseDetails,
                                            coverage_from: selectedDay
                                        }
                                    });
                                } }
                            />
                        </div>
                        <div className="col-xs-4 date">
                            <DatePicker
                                label="Coverage to"
                                dayFormat={ DATE_FORMATS.DISPLAY }
                                required
                                selectedDay={ this.state.releaseDetails.coverage_to }
                                ref={ ( ref ) => { this.coverage_to = ref; } }
                                onChange={ ( value ) => {
                                    const selectedDay = formatDate( value, DATE_FORMATS.API );
                                    if ( selectedDay === this.state.releaseDetails.coverage_to ) {
                                        return;
                                    }

                                    this.setState({
                                        releaseDetails: {
                                            ...this.state.releaseDetails,
                                            coverage_to: selectedDay
                                        }
                                    });
                                } }
                            />
                        </div>
                        <div className="col-xs-4">
                            <SalSelect
                                id="status"
                                label="Status"
                                required
                                key="status"
                                data={ RELEASE_DETAILS_STATUS }
                                value={ this.state.releaseDetails.status }
                                placeholder="Choose an option"
                                ref={ ( ref ) => { this.status = ref; } }
                                onChange={ ({ value }) => { this.updateReleaseDetailsData( 'status', value ); } }
                            />
                        </div>
                    </div>
                ) : null}
            </div>
        );
    }

    render() {
        return (
            <div>
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <MainWrapper>
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/bonuses' ); } }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showModal }
                    />
                    <SalConfirm
                        onConfirm={ () => { browserHistory.push( '/bonuses/batch-add' ); } }
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    You are about to leave the page with unsaved changes.
                                    <br /><br />
                                    Do you wish to proceed?
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        title="Warning!"
                        visible={ this.state.showBatchModal }
                    />
                    <SalConfirm
                        title="Warning"
                        body={
                            <ConfirmBodyWrapperStyle>
                                <div className="message">
                                    Some employees you chose belong to several groups. However, those employees will receive the bonus only once.
                                </div>
                            </ConfirmBodyWrapperStyle>
                        }
                        onConfirm={ () => {
                            this.setState({ isWarningMessageVisible: false });
                        } }
                        showCancel={ false }
                        buttonStyle="info"
                        confirmText="Ok"
                        visible={ this.state.isWarningMessageVisible }
                    />
                    <div className={ `loader ${this.props.loading ? '' : 'hide'}` }>
                        <Loader />
                    </div>

                    <div className={ this.props.loading ? 'hide' : '' }>
                        <Container>
                            <h2 className="sl-c-section-title">
                                <span className="sl-c-circle">1</span>
                                Choose the bonus type
                            </h2>
                            <div className="row">
                                <div className="col-xs-4">
                                    <SalSelect
                                        id="type_id"
                                        label="Bonus type"
                                        required
                                        key="type_id"
                                        data={ this.props.formOptions.bonusTypes }
                                        value={ this.state.bonusData.type_id }
                                        placeholder="Choose a bonus type"
                                        ref={ ( ref ) => { this.type_id = ref; } }
                                        onChange={ ({ value }) => { this.updateBonusData( 'type_id', value ); } }
                                    />
                                </div>
                            </div>

                            {this.bonusType ? (
                                <div>
                                    <div className="row">
                                        <div className="col-xs-12">
                                            <p>Bonus type summary</p>
                                            <div className="summary-panel">
                                                <div className="summary-panel__column">
                                                    <strong>Amount Basis</strong>
                                                    <p>{BONUS_TYPE.BASIS[ this.bonusType.basis ]}</p>
                                                </div>
                                                <div className="summary-panel__column">
                                                    <strong>Frequency</strong>
                                                    <p>{BONUS_TYPE.FREQUENCY[ this.bonusType.frequency ]}</p>
                                                </div>
                                                <div className="summary-panel__column">
                                                    <strong>Fully taxable</strong>
                                                    <p>{this.bonusType.fully_taxable ? 'Yes' : 'No'}</p>
                                                </div>
                                                <div className="summary-panel__column">
                                                    <strong>Maximum non-taxable amount</strong>
                                                    <p>{!this.bonusType.fully_taxable ? `P ${formatCurrency( this.bonusType.max_non_taxable )}` : ''}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 className="sl-c-section-title">
                                        <span className="sl-c-circle">2</span>
                                        Bonus details
                                    </h2>
                                    <div className="row" style={ { margin: '50px 0' } }>
                                        <div className="col-xs-4" style={ { marginLeft: '-15px' } }>
                                            <Switch
                                                defaultChecked
                                                disabled
                                            />
                                            <span
                                                style={ {
                                                    fontSize: '14px',
                                                    marginBottom: '4px',
                                                    marginLeft: '5px'
                                                } }
                                            >
                                                Auto-assign
                                            </span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-12">
                                            <MultiSelect
                                                id="recipients"
                                                label={ <span>Recipients</span> }
                                                placeholder="Search recipients"
                                                required
                                                disabled={ !!this.state.prepopulatedEmployee }
                                                data={ this.getRecipientsOptions() }
                                                value={ this.state.prepopulatedEmployee === null ? [
                                                    ...this.state.bonusData.recipients.employees,
                                                    ...this.state.bonusData.recipients.payroll_groups,
                                                    ...this.state.bonusData.recipients.departments
                                                ] : [
                                                    this.state.prepopulatedEmployee
                                                ] }
                                                onChange={ ( values ) => {
                                                    const newRecipientsValue = {
                                                        employees: values.filter( ( v ) => v.field === 'employees' ),
                                                        payroll_groups: values.filter( ( v ) => v.field === 'payroll_groups' ),
                                                        departments: values.filter( ( v ) => v.field === 'departments' )
                                                    };

                                                    this.updateBonusData( 'recipients', newRecipientsValue, () => {
                                                        const belongsToMultipleGroups = checkIfAnyEmployeesBelongToMultipleGroups(
                                                            {
                                                                employeesIds: newRecipientsValue.employees.map( ( employee ) => employee.value ),
                                                                departmentsIds: newRecipientsValue.departments.map( ( department ) => department.value ),
                                                                payrollGroupsIds: newRecipientsValue.payroll_groups.map( ( group ) => group.value )
                                                            },
                                                            this.props.formOptions.employees
                                                        );

                                                        if ( belongsToMultipleGroups ) {
                                                            this.setState({
                                                                isWarningMessageVisible: true
                                                            });
                                                        }
                                                    });
                                                } }
                                                ref={ ( ref ) => { this.recipients = ref; } }
                                            />
                                        </div>
                                    </div>

                                    <div style={ { marginBottom: '50px' } }>
                                        <div className="row">
                                            {this.bonusType.basis === BONUS_TYPE_FIXED_BASIS ? (
                                                <div className="col-xs-4">
                                                    <Input
                                                        id="amount"
                                                        label="Amount"
                                                        required
                                                        key="amount"
                                                        ref={ ( ref ) => { this.amount = ref; } }
                                                        value={ this.state.bonusData ? this.state.bonusData.amount : '0.00' }
                                                        minNumber={ 1 }
                                                        onChange={ ( value ) => { this.amount.setState({ value: stripNonDigit( value ) }); } }
                                                        onFocus={ () => {
                                                            this.amount.setState({ value: stripNonDigit( this.amount.state.value ) });
                                                        } }
                                                        onBlur={ ( value ) => {
                                                            this.amount.setState({ value: formatCurrency( value ) }, () => {
                                                                this.updateBonusData( 'amount', formatCurrency( value ), () => {
                                                                    const releaseDetails = this.recalculateReleaseAmounts( this.state.bonusData.release_details );
                                                                    this.updateBonusData( 'release_details', releaseDetails );
                                                                });
                                                            });
                                                        } }
                                                    />
                                                </div>
                                            ) : (
                                                <div>
                                                    <div className="col-xs-4">
                                                        <SalSelect
                                                            id="basis"
                                                            label="Amount basis"
                                                            required
                                                            key="basis"
                                                            data={ BASIS }
                                                            value={ this.state.bonusData.basis }
                                                            placeholder="Choose an option"
                                                            ref={ ( ref ) => { this.basis = ref; } }
                                                            onChange={ ({ value }) => { this.updateBonusData( 'basis', value ); } }
                                                        />
                                                    </div>
                                                    <div className="col-xs-4">
                                                        <Input
                                                            id="monthly_salary_percentage"
                                                            label="Percentage of monthly salary"
                                                            required
                                                            minNumber={ 1 }
                                                            key="monthly_salary_percentage"
                                                            value={ this.state.bonusData.percentage }
                                                            ref={ ( ref ) => { this.monthly_salary_percentage = ref; } }
                                                            onChange={ ( value ) => { this.monthly_salary_percentage.setState({ value: stripNonDigit( value ) }); } }
                                                            onFocus={ () => {
                                                                this.monthly_salary_percentage.setState({ value: stripNonDigit( this.monthly_salary_percentage.state.value ) });
                                                            } }
                                                            onBlur={ ( value ) => {
                                                                this.monthly_salary_percentage.setState({ value: formatCurrencyToDecimalNotation( value ) }, () => {
                                                                    this.updateBonusData( 'percentage', formatCurrencyToDecimalNotation( value ) );
                                                                });
                                                            } }
                                                            addon={ {
                                                                content: '%',
                                                                placement: 'right'
                                                            } }
                                                        />
                                                    </div>
                                                </div>
                                            )}
                                            {this.bonusType.frequency === BONUS_TYPE_PERIODIC_FREQUENCY ? (
                                                <div className="col-xs-4">
                                                    <Input
                                                        id="number_of_releases"
                                                        label="Number of releases"
                                                        key="number_of_releases"
                                                        disabled
                                                        minNumber={ 1 }
                                                        value={ this.state.bonusData.release_details.length.toString() }
                                                        ref={ ( ref ) => { this.number_of_releases = ref; } }
                                                    />
                                                </div>
                                            ) : null}
                                        </div>
                                    </div>

                                    {this.bonusType.frequency === BONUS_TYPE_PERIODIC_FREQUENCY && this.state.bonusData.release_details.map( ( releaseDetails, index ) => {
                                        if ( this.state.editReleaseDetails === index ) {
                                            return (
                                                <div key={ index }>
                                                    <div className="release-date-header">
                                                        <div className="flex-align-center">
                                                            <strong>Release date {index + 1}</strong>
                                                        </div>
                                                        <div>
                                                            <Button
                                                                label="Update"
                                                                type="neutral"
                                                                onClick={ this.updateReleaseDetails }
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="release-date-details">
                                                        {this.renderReleaseDetailsForm()}
                                                    </div>
                                                </div>
                                            );
                                        }

                                        return (
                                            <div key={ index }>
                                                <div className="release-date-header">
                                                    <div className="flex-align-center">
                                                        <strong>Release date {index + 1}</strong>
                                                        {this.bonusType.basis === BONUS_TYPE_FIXED_BASIS && this.state.bonusData.amount ? (
                                                            <div className="release-date-summary">
                                                                <dic className="release-date-summary__amount">
                                                                    Amount per recipient: &nbsp;<strong>P { formatCurrency( truncate( this.getAmountPerRelease( index ) ) ) }</strong>
                                                                </dic>
                                                                <div className="release-date-summary__percentage">
                                                                    <strong>{ formatCurrencyToDecimalNotation( this.calculatePercentage( this.getAmountPerRelease( index ) ).toFixed( 2 ) ) }%</strong>
                                                                </div>
                                                            </div>
                                                        ) : null}
                                                    </div>
                                                    {this.state.editReleaseDetails === null ? (
                                                        <div>
                                                            <Button
                                                                label="Edit"
                                                                type="neutral"
                                                                onClick={ () => this.editReleaseDetails( index ) }
                                                            />
                                                            <Button
                                                                label="Delete"
                                                                type="neutral"
                                                                onClick={ () => this.deleteReleaseDetails( index ) }
                                                            />
                                                        </div>
                                                    ) : null}
                                                </div>
                                                <div className="release-date-details release-date-details--summary">
                                                    <div className="row">
                                                        <div className="col-xs-4">
                                                            <div>Release date</div>
                                                            <strong>{formatDate( releaseDetails.date, DATE_FORMATS.DISPLAY )}</strong>
                                                        </div>
                                                        <div className="col-xs-4">
                                                            <div>Disburse through Special Pay Run</div>
                                                            <strong>{releaseDetails.disburse_through_special_pay_run ? 'Yes' : 'No'}</strong>
                                                        </div>
                                                        {
                                                            this.bonusType.basis === BONUS_TYPE_FIXED_BASIS ? (
                                                                <div className="col-xs-4">
                                                                    <div>Status</div>
                                                                    <strong>{RELEASE_DETAILS_STATUS.find( ( status ) => status.value === releaseDetails.status ).label}</strong>
                                                                </div>
                                                            ) : null
                                                        }
                                                    </div>
                                                    {
                                                        this.bonusType.basis === BONUS_TYPE_SALARY_BASED ? (
                                                            <div className="row">
                                                                <div className="col-xs-4">
                                                                    <div>Status</div>
                                                                    <strong>{RELEASE_DETAILS_STATUS.find( ( status ) => status.value === releaseDetails.status ).label}</strong>
                                                                </div>
                                                            </div>
                                                        ) : null
                                                    }
                                                    {this.bonusType.basis === BONUS_TYPE_SALARY_BASED ? (
                                                        <div className="row">
                                                            <div className="col-xs-4">
                                                                <div>Coverage from</div>
                                                                <strong>{formatDate( releaseDetails.coverage_from, DATE_FORMATS.DISPLAY )}</strong>
                                                            </div>
                                                            <div className="col-xs-4">
                                                                <div>Coverage to</div>
                                                                <strong>{formatDate( releaseDetails.coverage_to, DATE_FORMATS.DISPLAY )}</strong>
                                                            </div>
                                                            <div className="col-xs-4">
                                                                <div>Status</div>
                                                                <strong>{RELEASE_DETAILS_STATUS.find( ( status ) => status.value === releaseDetails.status ).label}</strong>
                                                            </div>
                                                        </div>
                                                    ) : null}
                                                </div>
                                            </div>
                                        );
                                    })}
                                    {this.state.editReleaseDetails === null ? (
                                        this.bonusType.frequency === BONUS_TYPE_PERIODIC_FREQUENCY ? (
                                            <div>
                                                <div className="release-date-header">
                                                    <div className="flex-align-center">
                                                        <strong>Release date {this.state.bonusData.release_details.length + 1}</strong>
                                                    </div>
                                                    <div>
                                                        <Button
                                                            label="Add release date"
                                                            type="neutral"
                                                            onClick={ this.addReleaseDetails }
                                                        />
                                                    </div>
                                                </div>
                                                <div className="release-date-details">
                                                    {this.renderReleaseDetailsForm()}
                                                </div>
                                            </div>
                                        ) : this.renderReleaseDetailsForm()
                                    ) : <div style={ { height: '210px' } } />}
                                </div>
                            ) : null}
                        </Container>
                        <div className="foot">
                            <Button
                                label="Cancel"
                                type="neutral"
                                size="large"
                                onClick={ () => {
                                    this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/bonuses' );
                                } }
                            />
                            <Button
                                label={ this.props.submitted ? <Loader /> : 'Submit' }
                                type="action"
                                size="large"
                                onClick={ this.submitForm }
                                ref={ ( ref ) => { this.submitButton = ref; } }
                            />
                        </div>
                    </div>
                </MainWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    bonusTypes: makeSelectBonusTypes(),
    formOptions: makeSelectFormOptions(),
    submitted: makeSelectSubmitted(),
    errors: makeSelectErrors(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        createBonusActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ManualEntry );
