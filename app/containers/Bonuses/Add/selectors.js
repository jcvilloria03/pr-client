import { createSelector } from 'reselect';

/**
 * Direct selector to the view state domain
 */
const selectAddBonusDomain = () => ( state ) => state.get( 'addBonus' );

const makeSelectBonus = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => substate.get( 'bonus' ).toJS()
);

const makeSelectLoading = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectFormOptions = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectSubmitted = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectNotification = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectBatchUploadJobId = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => substate.get( 'batchUploadJobId' )
);

const makeSelectBatchUploadStatus = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => substate.get( 'batchUploadStatus' )
);

const makeSelectBatchUploadErrors = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => substate.get( 'batchUploadErrors' )
);

const makeSelectBonusesPreview = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => substate.get( 'bonusesPreview' ).toJS()
);

const makeSelectSaving = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => substate.get( 'saving' ).toJS()
);

const makeSelectBonusTypes = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => substate.get( 'bonusTypes' ).toJS()
);

const makeSelectErrors = () => createSelector(
    selectAddBonusDomain(),
    ( substate ) => {
        let error;
        try {
            error = substate.get( 'errors' ).toJS();
        } catch ( err ) {
            error = substate.get( 'errors' );
        }

        return error;
    }
);

export {
    makeSelectBatchUploadErrors,
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBonus,
    makeSelectBonusTypes,
    makeSelectBonusesPreview,
    makeSelectErrors,
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSaving,
    makeSelectSubmitted
};
