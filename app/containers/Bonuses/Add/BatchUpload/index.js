import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';

import A from '../../../../components/A';
import Table from '../../../../components/Table';
import Button from '../../../../components/Button';
import Clipboard from '../../../../components/Clipboard';
import FileInput from '../../../../components/FileInput';
import { H4, P } from '../../../../components/Typography';

import { browserHistory } from '../../../../utils/BrowserHistory';
import { formatPaginationLabel, formatCurrency, formatDate } from '../../../../utils/functions';
import { DATE_FORMATS } from '../../../../utils/constants';

import { BASE_PATH_NAME } from '../../../../constants';

import * as actions from '../actions';

import {
    makeSelectBatchUploadJobId,
    makeSelectBatchUploadStatus,
    makeSelectBatchUploadErrors,
    makeSelectBonusesPreview,
    makeSelectSaving
} from '../selectors';

import { StyledLoader, PageWrapper } from './styles';

/**
 * Bonuses Batch Upload Component
 */
class BatchUpload extends React.Component {
    static propTypes = {
        batchUploadJobId: React.PropTypes.string,
        uploadBonuses: React.PropTypes.func,
        batchUploadStatus: React.PropTypes.string,
        batchUploadErrors: React.PropTypes.oneOfType([
            React.PropTypes.object,
            React.PropTypes.array
        ]),
        bonusesPreview: React.PropTypes.shape({
            status: React.PropTypes.string,
            data: React.PropTypes.array
        }),
        saving: React.PropTypes.shape({
            status: React.PropTypes.string,
            errors: React.PropTypes.object
        }),
        saveBonuses: React.PropTypes.func
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            files: [],
            status: null,
            errors: {},
            clipboard: '',
            submit: false,
            bonusesTableLabel: 'Showing 0-0 of 0 entries',
            errorsTableLabel: 'Showing 0-0 of 0 entries',
            bonusesPreview: {
                data: [],
                status: 'ready'
            },
            savingStatus: '',
            savingErrors: {}
        };
    }

    componentWillReceiveProps( nextProps ) {
        nextProps.batchUploadStatus !== this.props.batchUploadStatus && this.setState({
            status: nextProps.batchUploadStatus
        }, () => {
            this.validateButton && this.validateButton.setState({
                disabled: ![ 'validated', 'validation_failed' ].includes( nextProps.batchUploadStatus )
            });
        });

        nextProps.batchUploadErrors !== this.props.batchUploadErrors && this.setState({
            errors: nextProps.batchUploadErrors
        }, () => {
            let clipboard = '';
            const rows = Object.keys( nextProps.batchUploadErrors );

            if ( rows.length > 0 ) {
                rows.forEach( ( row ) => {
                    Object.values( nextProps.batchUploadErrors[ row ]).forEach( ( value ) => {
                        clipboard += `Row ${row}, ${value}\n`;
                    });
                });
            }

            this.handleErrorsTableChanges();
            this.setState({ clipboard });
        });

        nextProps.bonusesPreview.data !== this.props.bonusesPreview.data && this.setState({
            bonusesPreview: { ...this.state.bonusesPreview, data: nextProps.bonusesPreview.data }
        }, () => {
            this.handleBonusesTableChanges();
        });

        nextProps.bonusesPreview.status !== this.props.bonusesPreview.status && this.setState({
            bonusesPreview: { ...this.state.bonusesPreview, status: nextProps.bonusesPreview.status }
        });

        nextProps.saving.status !== this.props.saving.status && this.setState({
            savingStatus: nextProps.saving.status
        }, () => {
            this.submitButton && this.submitButton.setState({ disabled: nextProps.saving.status !== 'save_failed' });
        });
    }

    formatToCurrency = ( value ) => (
        value ? `P ${formatCurrency( value )}` : 'P 0.00'
    );

    handleErrorsTableChanges = () => {
        if ( !this.errorsTable ) {
            return;
        }

        this.setState({
            errorsTableLabel: formatPaginationLabel( this.errorsTable.tableComponent.state )
        });
    };

    handleBonusesTableChanges = () => {
        if ( !this.bnusesTable ) {
            return;
        }

        this.setState({
            bonusesTableLabel: formatPaginationLabel( this.bonusesTable.tableComponent.state )
        });
    };

    renderBonusesSection = () => {
        const tableColumns = [
            {
                header: 'Employee ID',
                accessor: 'employee_id',
                minWidth: 200,
                render: ({ row }) => <div>{row.employee_id}</div>
            },
            {
                header: 'Bonus Type',
                accessor: 'bonus_type',
                minWidth: 200,
                render: ({ row }) => <div>{row.bonus_type}</div>
            },
            {
                header: 'Amount (to be divided by number of releases)',
                accessor: 'amount',
                minWidth: 200,
                render: ({ row }) => <div>{row.amount}</div>
            },
            {
                header: 'Percentage of monthly salary',
                accessor: 'percentage',
                minWidth: 200,
                render: ({ row }) => <div>{row.percentage}</div>
            },
            {
                header: 'Release date',
                accessor: 'release_date',
                minWidth: 200,
                render: ({ row }) => <div>{row.release_date}</div>
            },
            {
                header: 'Disburse through Special Pay Run?',
                accessor: 'disburse_through_special_pay_run',
                minWidth: 200,
                render: ({ row }) => <div>{row.disburse_through_special_pay_run}</div>
            },
            {
                header: 'Prorate based on tenure?',
                accessor: 'prorate_based_on_tenure',
                minWidth: 200,
                render: ({ row }) => <div>{row.prorate_based_on_tenure}</div>
            },
            {
                header: 'Coverage from',
                accessor: 'coverage_from',
                minWidth: 200,
                render: ({ row }) => <div>{row.coverage_from}</div>
            },
            {
                header: 'Coverage to',
                accessor: 'coverage_to',
                minWidth: 200,
                render: ({ row }) => <div>{row.coverage_to}</div>
            },
            {
                header: 'Status',
                accessor: 'status',
                minWidth: 200,
                render: ({ row }) => <div>{row.status}</div>
            }
        ];

        const dataForDisplay = this.state.bonusesPreview.data.map( ( data ) => ({
            ...data,
            amount: data.amount && `Php ${formatCurrency( data.amount )}`,
            percentage: data.percentage && `${formatCurrency( data.percentage )} %`,
            release_date: formatDate( data.date, DATE_FORMATS.DISPLAY ),
            coverage_from: data.coverage_from && formatDate( data.coverage_from, DATE_FORMATS.DISPLAY ),
            coverage_to: data.coverage_to && formatDate( data.coverage_to, DATE_FORMATS.DISPLAY )
        }) );

        return this.state.bonusesPreview.data.length === 0
            ? null
            : (
                <div>
                    <H4>Bonuses List:</H4>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.bonusesTableLabel }
                        </span>
                    </div>
                    <Table
                        columns={ tableColumns }
                        data={ dataForDisplay }
                        pagination
                        ref={ ( ref ) => { this.bonusesTable = ref; } }
                        onDataChange={ this.handleBonusesTableChanges }
                    />
                </div>
            );
    };

    renderErrorsSection = () => {
        const errorDisplay = [];
        const errorList = this.state.errors;

        if ( Object.keys( errorList ).length ) {
            const columns = [
                {
                    header: 'Row Number',
                    accessor: 'row',
                    minWidth: 150,
                    sortable: false
                },
                {
                    header: 'Error Type',
                    accessor: 'error',
                    minWidth: 550,
                    sortable: false
                }
            ];

            const keys = Object.keys( errorList );

            keys.forEach( ( key ) => {
                errorDisplay.push({
                    row: <H4 style={ { margin: '0', textAlign: 'center' } }>Row {key}</H4>,
                    error: <ul>{Object.values( errorList[ key ]).map( ( value ) => <li key={ value } style={ { margin: '0' } } >{value}</li> )}</ul>
                });
            });

            return (
                <div className="errors">
                    <H4>Data field errors</H4>
                    <p>There seems to be an error in the file you uploaded. Review the list below, correct the errors and upload the file again.</p>
                    <div style={ { textAlign: 'right' } }>
                        <span>
                            { this.state.errorsTableLabel }
                        </span>
                        <Clipboard value={ this.state.clipboard } />
                    </div>
                    <Table
                        columns={ columns }
                        data={ errorDisplay }
                        pagination
                        ref={ ( ref ) => { this.errorsTable = ref; } }
                        onDataChange={ this.handleErrorsTableChanges }
                    />
                </div>
            );
        }

        return null;
    }

    render() {
        return (
            <PageWrapper>
                <Container>
                    <p className="sl-u-gap-top--lg sl-u-gap-bottom--lg">
                        Follow the steps below for adding employee bonuses through
                        batch upload. A template will be available for you to fill out.
                    </p>

                    <div className="steps">
                        <div className="step">
                            <div className="template">
                                <H4>Step 1:</H4>
                                <P>Download and fill-out the Employee Payroll Information Template.</P>
                                <P><A target="_blank" href={ `${BASE_PATH_NAME}/guides/bonuses/batch-upload` }>Click here to view the upload guide.</A></P>
                                <A className="sl-c-btn--wide" href="https://s3-us-west-2.amazonaws.com/frontendv3-assets-dev/guides/bonuses.csv" download>Download Template</A>
                            </div>
                        </div>
                        <div className="step">
                            <div className="upload">
                                <H4>Step 2:</H4>
                                <P>After completely filling out the template, choose and upload it here.</P>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'none' : 'block' } } >
                                    <FileInput
                                        accept=".csv"
                                        onDrop={ ( files ) => {
                                            const { acceptedFiles } = files;

                                            this.setState({
                                                files: acceptedFiles.length > 0 ? acceptedFiles[ 0 ] : null,
                                                errors: {},
                                                status: null
                                            }, () => {
                                                this.validateButton && this.validateButton.setState({ disabled: acceptedFiles.length <= 0 });
                                            });
                                        } }
                                        ref={ ( ref ) => { this.fileInput = ref; } }
                                    />
                                </div>
                                <div style={ { display: this.state.status === 'validated' ? 'block' : 'none' } }>
                                    <H4 style={ { color: '#4ABA4A', margin: '0' } }>VALIDATED <i className="fa fa-check" /></H4>
                                </div>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' || this.state.status === 'validated' ? 'none' : 'block' } } >
                                    <Button
                                        label={
                                            this.state.submit ? (
                                                <StyledLoader className="animation">
                                                    Uploading <div className="anim3"></div>
                                                </StyledLoader>
                                            ) : (
                                                'Upload'
                                            )
                                        }
                                        disabled={ this.state.files ? this.state.files.length <= 0 : true }
                                        type="neutral"
                                        ref={ ( ref ) => { this.validateButton = ref; } }
                                        onClick={ () => {
                                            this.setState({
                                                errors: {},
                                                status: null,
                                                submit: true
                                            }, () => {
                                                this.validateButton && this.validateButton.setState({ disabled: true }, () => {
                                                    this.props.uploadBonuses({ file: this.state.files });

                                                    setTimeout( () => {
                                                        this.validateButton.setState({ disabled: false });
                                                        this.setState({ submit: false });
                                                    }, 5000 );
                                                });
                                            });
                                        } }
                                    />
                                </div>
                                <div style={ { display: this.state.status === 'validating' || this.state.status === 'validation_queued' ? 'block' : 'none' } } >
                                    <StyledLoader className="animation">
                                        <H4 style={ { margin: '0' } }>{ this.state.status === 'validation_queued' ? 'QUEUED FOR VALIDATION' : 'VALIDATING' }</H4> <div className="anim3"></div>
                                    </StyledLoader>
                                </div>
                            </div>
                        </div>
                    </div>
                    { this.renderBonusesSection() }
                    { this.renderErrorsSection() }
                </Container>

                <div className="foot">
                    <Container>
                        <Button
                            label="Cancel"
                            type="neutral"
                            size="large"
                            onClick={ () => {
                                const prepopulatedEmployee = JSON.parse( localStorage.getItem( 'prepopulatedEmployeeForAddBonus' ) );
                                prepopulatedEmployee && prepopulatedEmployee.id === ''
                                ? browserHistory.push( `/employee/${prepopulatedEmployee.id}`, true )
                                : browserHistory.push( '/bonuses' );
                            } }
                        />
                        <Button
                            ref={ ( ref ) => { this.submitButton = ref; } }
                            type="action"
                            disabled={ this.state.status !== 'validated' }
                            label={
                                this.state.savingStatus === 'save_queued' || this.state.savingStatus === 'saving' ?
                                    (
                                        <StyledLoader className="animation">
                                            <span>Saving</span> <div className="anim3"></div>
                                        </StyledLoader>
                                    ) : 'Submit'
                            }
                            size="large"
                            onClick={ () => {
                                this.submitButton && this.submitButton.setState({ disabled: true });
                                this.setState({ savingStatus: 'save_queued' });
                                this.props.saveBonuses({ jobId: this.props.batchUploadJobId });
                            } }
                        />
                    </Container>
                </div>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    batchUploadJobId: makeSelectBatchUploadJobId(),
    batchUploadStatus: makeSelectBatchUploadStatus(),
    batchUploadErrors: makeSelectBatchUploadErrors(),
    bonusesPreview: makeSelectBonusesPreview(),
    saving: makeSelectSaving()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators(
        actions,
        dispatch
    )
);

export default connect( mapStateToProps, mapDispatchToProps )( BatchUpload );
