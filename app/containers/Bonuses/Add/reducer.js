import { fromJS } from 'immutable';
import {
    LOADING,
    SET_FORM_OPTIONS,
    SET_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BATCH_UPLOAD_ERRORS,
    RESET_BONUSES_PREVIEW,
    SET_BONUSES_PREVIEW_STATUS,
    SET_BONUSES_PREVIEW_DATA,
    SET_BONUS_TYPES,
    SET_SAVING_STATUS,
    SET_SAVING_ERRORS,
    SUBMITTED,
    NOTIFICATION_SAGA
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    errors: {},
    submitted: false,
    batchUploadJobId: null,
    batchUploadStatus: '',
    batchUploadErrors: {},
    bonusesPreview: {
        status: 'ready',
        data: []
    },
    saving: {
        status: '',
        errors: {}
    },
    bonus: {
        type_id: null
    },
    formOptions: {
        bonusTypes: [],
        employees: [],
        payrollGroups: []
    },
    bonusTypes: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Manual or batch assign bonuses
 *
 */
function addBonus( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_ERRORS:
            return state.set( 'errors', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_BATCH_UPLOAD_JOB_ID:
            return state.set( 'batchUploadJobId', action.payload );
        case SET_BATCH_UPLOAD_STATUS:
            return state.set( 'batchUploadStatus', action.payload );
        case SET_BATCH_UPLOAD_ERRORS:
            return state.set( 'batchUploadErrors', action.payload );
        case SET_BONUSES_PREVIEW_STATUS:
            return state.setIn([ 'bonusesPreview', 'status' ], action.payload );
        case SET_BONUSES_PREVIEW_DATA:
            return state.setIn([ 'bonusesPreview', 'data' ], fromJS( action.payload ) );
        case SET_BONUS_TYPES:
            return state.set( 'bonusTypes', fromJS( action.payload ) );
        case SET_SAVING_STATUS:
            return state.setIn([ 'saving', 'status' ], action.payload );
        case SET_SAVING_ERRORS:
            return state.setIn([ 'saving', 'errors' ], action.payload );
        case RESET_BONUSES_PREVIEW:
            return state.set( 'preview', fromJS({
                status: 'ready',
                data: []
            }) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default addBonus;
