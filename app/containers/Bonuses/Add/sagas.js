import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';
import { formatFeedbackMessage } from '../../../utils/functions';

import {
    GET_BONUSES_PREVIEW,
    INITIALIZE,
    LOADING,
    NOTIFICATION,
    NOTIFICATION_SAGA,
    SAVE_BONUSES,
    SET_BATCH_UPLOAD_ERRORS,
    SET_BATCH_UPLOAD_JOB_ID,
    SET_BATCH_UPLOAD_STATUS,
    SET_BONUSES_PREVIEW_DATA,
    SET_BONUSES_PREVIEW_STATUS,
    SET_BONUS_TYPES,
    SET_ERRORS,
    SET_FORM_OPTIONS,
    SET_SAVING_STATUS,
    SUBMITTED,
    SUBMIT_FORM,
    UPLOAD_BONUSES
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const formOptions = {};

        const bonusTypes = yield call( Fetch, `/company/${companyId}/other_income_types/bonus_type`, { method: 'GET' });
        const employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL`, { method: 'GET' });
        const payrollGroups = yield call( Fetch, `/philippine/company/${companyId}/payroll_groups`, { method: 'GET' });
        const departments = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });

        formOptions.bonusTypes = bonusTypes.data
            .filter( ( type ) => !type.deleted_at )
            .map( ( value ) => ({ label: value.name === '13TH_MONTH_PAY' ? '13th month pay' : value.name, value: value.id }) );

        formOptions.employees = employees.data.filter( ( employee ) => employee.payroll );
        formOptions.payrollGroups = payrollGroups.data;
        formOptions.departments = departments.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });
        yield put({
            type: SET_BONUS_TYPES,
            payload: bonusTypes.data
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Submit form
 * @param payload
 */
export function* submitForm({ payload }) {
    try {
        yield [
            put({ type: SUBMITTED, payload: true }),
            put({ type: SET_ERRORS, payload: {}})
        ];

        // Transform data to remove null keys
        const data = { ...payload };

        Object.keys( payload ).forEach( ( key ) => {
            if ( !payload[ key ]) {
                delete data[ key ];
            }
        });

        const companyId = company.getLastActiveCompanyId();

        yield call( Fetch, `/philippine/company/${companyId}/bonus/bulk_create`, { method: 'POST', data: [data]});
        yield call( showSuccessMessage );
        yield call( delay, 500 );

        payload.previousRouteName ? yield call( browserHistory.goBack ) : yield call( browserHistory.push, '/bonuses' );
    } catch ( error ) {
        if ( error.response && error.response.status === 406 && !error.response.data.message ) {
            yield call( setErrors, error.response.data );
        } else {
            yield call( notifyUser, error );
        }
    } finally {
        yield put({
            type: SUBMITTED,
            payload: false
        });
    }
}

/**
 * Changes store with errors from API
 * @param errors
 */
export function* setErrors( errors ) {
    yield put({
        type: SET_ERRORS,
        payload: errors
    });
}

/**
 * Display a notification to user
 */
export function* showSuccessMessage() {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage()
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: formatFeedbackMessage( false )
    });
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Uploads the CSV of bonuses to add and starts the validation process
 */
export function* uploadBonuses({ payload }) {
    try {
        yield put({
            type: SET_BATCH_UPLOAD_ERRORS,
            payload: {}
        });

        const companyId = company.getLastActiveCompanyId();

        const data = new FormData();

        data.append( 'company_id', companyId );
        data.append( 'file', payload.file );

        const upload = yield call( Fetch, `/company/${companyId}/other_income/bonus/upload`, {
            method: 'POST',
            data
        });

        if ( upload.id ) {
            yield put({
                type: SET_BATCH_UPLOAD_JOB_ID,
                payload: upload.id
            });

            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: 'validation_queued'
            });

            yield call( checkValidation, { payload: { jobId: upload.id }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Checks the status of batch upload
 */
export function* checkValidation({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const check = yield call(
            Fetch,
            `/company/${companyId}/other_income/bonus/upload/status?job_id=${payload.jobId}`,
            { method: 'GET' }
        );

        if ( check.status === 'validation_failed' ) {
            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: check.status
            });

            yield put({
                type: SET_BATCH_UPLOAD_ERRORS,
                payload: check.errors
            });
        } else if ( check.status === 'saved' ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: check.status
            });

            browserHistory.push( '/bonuses' );
        } else {
            yield put({
                type: SET_BATCH_UPLOAD_STATUS,
                payload: check.status
            });

            yield call( delay, 2000 );
            yield call( checkValidation, { payload: { jobId: payload.jobId }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Fetches preview for uploaded bonuses
 */
export function* getBonusesPreview({ payload }) {
    try {
        yield put({
            type: SET_BONUSES_PREVIEW_DATA,
            payload: []
        });

        yield put({
            type: SET_BONUSES_PREVIEW_STATUS,
            payload: 'busy'
        });

        const preview = yield call(
            Fetch,
            `/company/other_income/bonus/upload/preview?job_id=${payload.jobId}&company_id=${company.getLastActiveCompanyId()}`,
            { method: 'GET' }
        );

        yield put({
            type: SET_BONUSES_PREVIEW_DATA,
            payload: preview
        });
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_BONUSES_PREVIEW_STATUS,
            payload: 'ready'
        });
    }
}

/**
 * Saves validated bonuses
 */
export function* saveBonuses({ payload }) {
    try {
        const companyId = company.getLastActiveCompanyId();

        const upload = yield call( Fetch, `/company/${companyId}/other_income/bonus/upload/save`, {
            method: 'POST',
            data: {
                company_id: company.getLastActiveCompanyId(),
                job_id: payload.jobId
            }
        });

        if ( upload.id ) {
            yield put({
                type: SET_SAVING_STATUS,
                payload: 'save_queued'
            });

            yield call( checkValidation, { payload: {
                step: 'save',
                jobId: payload.jobId
            }});
        }
    } catch ( error ) {
        yield call( notifyUser, {
            show: true,
            title: error.response ? error.response.statusText : 'Error',
            message: error.response ? error.response.data.message : error.statusText,
            type: 'error'
        });
    }
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SUBMIT_FORM
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT_FORM, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for UPLOAD_BONUSES
 */
export function* watchForUploadBonuses() {
    const watcher = yield takeEvery( UPLOAD_BONUSES, uploadBonuses );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for GET_BONUSES_PREVIEW
 */
export function* watchForGetBonusesPreview() {
    const watcher = yield takeEvery( GET_BONUSES_PREVIEW, getBonusesPreview );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for SAVE_BONUSES
 */
export function* watchForSaveBonuses() {
    const watcher = yield takeEvery( SAVE_BONUSES, saveBonuses );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchForGetBonusesPreview,
    watchForUploadBonuses,
    watchForInitializeData,
    watchForReinitializePage,
    watchForSaveBonuses,
    watchNotify
];
