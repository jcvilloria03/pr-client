import {
    INITIALIZE,
    SUBMIT_FORM,
    UPLOAD_BONUSES,
    SAVE_BONUSES,
    NOTIFICATION
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData() {
    return {
        type: INITIALIZE
    };
}

/**
 * Display notification in page
 * @param {boolean} show
 * @param {string} title
 * @param {string} message
 * @param {string} type
 */
export function notify( show, title = '', message = '', type = 'error' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

/**
 * Submit a new bonus
 * @param {object} payload
 */
export function submitForm( payload ) {
    return {
        type: SUBMIT_FORM,
        payload
    };
}

/**
 * Upload bonuses
 * @param {object} payload
 */
export function uploadBonuses( payload ) {
    return {
        type: UPLOAD_BONUSES,
        payload
    };
}

/**
 * Save bonuses
 * @param {object} payload
 */
export function saveBonuses( payload ) {
    return {
        type: SAVE_BONUSES,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
