import { createSelector } from 'reselect';

/**
 * Direct selector to the bonuses detail state domain
 */
const selectBonusesDetailDomain = () => ( state ) => state.get( 'bonusesDetail' );

const makeSelectLoading = () => createSelector(
    selectBonusesDetailDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectBonus = () => createSelector(
    selectBonusesDetailDomain(),
    ( substate ) => substate.get( 'bonus' ).toJS()
);

const makeSelectFormOptions = () => createSelector(
    selectBonusesDetailDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectBonusesDetailDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectLoading,
    makeSelectBonus,
    makeSelectFormOptions,
    makeSelectNotification
};
