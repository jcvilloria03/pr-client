import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { formatCurrency, getEmployeeFullName, formatDate } from '../../../utils/functions';
import { PAYROLL_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';
import { subscriptionService } from '../../../utils/SubscriptionService';

import A from '../../../components/A';
import SnackBar from '../../../components/SnackBar';
import Button from '../../../components/Button';
import Switch from '../../../components/Switch';
import SubHeader from '../../../containers/SubHeader';
import { H2, H3 } from '../../../components/Typography';
import MultiSelect from '../../../components/MultiSelect';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper,
    MainWrapper,
    ValueWrapper,
    LoaderWrapper
} from './styles';

import {
    makeSelectLoading,
    makeSelectFormOptions,
    makeSelectNotification,
    makeSelectBonus
} from './selectors';

import * as editAllowanceActions from './actions';

const BONUS_TYPE_FIXED_BASIS = 'FIXED';
const BONUS_TYPE_SALARY_BASED = 'SALARY_BASED';
const BONUS_TYPE_PERIODIC_FREQUENCY = 'PERIODIC';
const BONUS_TYPE_ONE_TIME_FREQUENCY = 'ONE_TIME';

const BONUS_TYPE = {
    BASIS: {
        [ BONUS_TYPE_FIXED_BASIS ]: 'Fixed',
        [ BONUS_TYPE_SALARY_BASED ]: 'Salary Based'
    },
    FREQUENCY: {
        [ BONUS_TYPE_PERIODIC_FREQUENCY ]: 'Periodic',
        [ BONUS_TYPE_ONE_TIME_FREQUENCY ]: 'One-time'
    }
};

const BASIS = {
    CURRENT_BASIC_SALARY: 'Current salary',
    AVERAGE_OF_BASIC_SALARY: 'Average salary',
    GROSS_SALARY: 'Gross salary'
};

/**
 * Bonus Detail Component
 */
export class Detail extends React.Component {
    static propTypes = {
        loading: React.PropTypes.bool,
        bonus: React.PropTypes.object,
        formOptions: React.PropTypes.object,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        params: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['edit.bonuses'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        const { id: bonusId } = this.props.params;
        this.props.initializeData({ bonusId });
    }

    componentWillUnmount() {
        localStorage.removeItem( 'employeeIdForBonusDetail' );
        this.props.resetStore();
    }

    getRecipientsOptions = () => {
        const { formOptions } = this.props;

        const departments = formOptions.departments.map( ( department ) => ({
            value: department.id, label: department.name, field: 'departments'
        }) );
        const payrollGroups = formOptions.payrollGroups.map( ( payrollGroup ) => ({
            value: payrollGroup.id, label: payrollGroup.name, field: 'payroll_groups'
        }) );
        const employees = formOptions.employees.map( ( employee ) => ({
            value: employee.id, label: getEmployeeFullName( employee ), field: 'employees'
        }) );

        return [ ...departments, ...payrollGroups, ...employees ];
    }

    getRecipientsValues = () => {
        const { formOptions, bonus } = this.props;
        const { employees, payroll_groups, departments } = bonus.recipients;

        return [
            ...( employees ? employees.reduce( ( employeesAcc, e ) => {
                const employee = formOptions.employees.find( ( f ) => f.id === e.recipient_id );

                if ( employee ) {
                    employeesAcc.push({
                        label: getEmployeeFullName( employee ),
                        value: employee.id
                    });
                }

                return employeesAcc;
            }, []) : []),
            ...( payroll_groups ? payroll_groups.reduce( ( payrollGroupsAcc, g ) => { // eslint-disable-line camelcase
                const payrollGroup = formOptions.payrollGroups.find( ( f ) => f.id === g.recipient_id );

                if ( payrollGroup ) {
                    payrollGroupsAcc.push({
                        label: payrollGroup.name,
                        value: payrollGroup.id
                    });
                }

                return payrollGroupsAcc;
            }, []) : []),
            ...( departments ? departments.reduce( ( departmentsAcc, d ) => {
                const department = formOptions.departments.find( ( f ) => f.id === d.recipient_id );

                if ( department ) {
                    departmentsAcc.push({
                        label: department.name,
                        value: department.id
                    });
                }

                return departmentsAcc;
            }, []) : [])
        ];
    }

    renderOneTimeReleaseDetails = ( releaseDetails ) => (
        this.props.bonus.type.basis === BONUS_TYPE_FIXED_BASIS ? (
            <div style={ { marginBottom: '4rem' } }>
                <div className="row">
                    <div className="col-xs-4">
                        Amount:
                        <ValueWrapper>
                            {`Php ${formatCurrency( this.props.bonus.amount )}`}
                        </ValueWrapper>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        Release date:
                        <ValueWrapper>
                            {formatDate( releaseDetails[ 0 ].date, DATE_FORMATS.DISPLAY )}
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-4">
                        Disburse through special pay run?
                        <ValueWrapper>
                            <Switch
                                disabled
                                checked={ releaseDetails[ 0 ].disburse_through_special_pay_run }
                            />
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-4">
                        Status:
                        <ValueWrapper>
                            {releaseDetails[ 0 ].status ? 'Disbursed' : 'Not yet released'}
                        </ValueWrapper>
                    </div>
                </div>
            </div>
        ) : (
            <div style={ { marginBottom: '4rem' } }>
                <div className="row">
                    <div className="col-xs-4">
                        Amount basis:
                        <ValueWrapper>
                            {BASIS[ this.props.bonus.basis ]}
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-4">
                        Percentage of monthly salary:
                        <ValueWrapper>
                            {`${formatCurrency( this.props.bonus.percentage )}%`}
                        </ValueWrapper>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        Release date:
                        <ValueWrapper>
                            {formatDate( releaseDetails[ 0 ].date, DATE_FORMATS.DISPLAY )}
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-4">
                        Disburse through special pay run?
                        <ValueWrapper>
                            <Switch
                                disabled
                                checked={ releaseDetails[ 0 ].disburse_through_special_pay_run }
                            />
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-4">
                        Prorate based on tenure?
                        <ValueWrapper>
                            <Switch
                                disabled
                                checked={ releaseDetails[ 0 ].prorate_based_on_tenure }
                            />
                        </ValueWrapper>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        Coverage from:
                        <ValueWrapper>
                            {releaseDetails[ 0 ].coverage_from ? formatDate( releaseDetails[ 0 ].coverage_from, DATE_FORMATS.DISPLAY ) : '-'}
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-4">
                        Coverage to:
                        <ValueWrapper>
                            {releaseDetails[ 0 ].coverage_to ? formatDate( releaseDetails[ 0 ].coverage_to, DATE_FORMATS.DISPLAY ) : '-'}
                        </ValueWrapper>
                    </div>
                    <div className="col-xs-4">
                        Status:
                        <ValueWrapper>
                            {releaseDetails[ 0 ].status ? 'Disbursed' : 'Not yet released'}
                        </ValueWrapper>
                    </div>
                </div>
            </div>
        )
    )

    renderPeriodicReleaseDetails = ( releaseDetails ) => (
        <div style={ { marginBottom: '4rem' } }>
            <div className="row">
                <div className="col-xs-4">
                    Amount:
                    <ValueWrapper>
                        {`Php ${formatCurrency( this.props.bonus.amount )}`}
                    </ValueWrapper>
                </div>
                <div className="col-xs-4">
                    Number of releases:
                    <ValueWrapper>
                        {releaseDetails.length}
                    </ValueWrapper>
                </div>
            </div>
            { releaseDetails.map( ( releaseDetail, index ) => (
                <div key={ index }>
                    <div className="release-date-header">
                        <div className="flex-align-center">
                            <strong>Release date {index + 1}</strong>
                            {this.props.bonus.type.basis === BONUS_TYPE_FIXED_BASIS ? (
                                <div className="release-date-summary">
                                    <dic className="release-date-summary__amount">
                                        Amount per recipient:&nbsp;<strong>Php {formatCurrency( releaseDetail.amount )}</strong>
                                    </dic>
                                    <div className="release-date-summary__percentage">
                                        <strong>{( 100 / releaseDetails.length ).toFixed( 2 )}%</strong>
                                    </div>
                                </div>
                            ) : null}
                        </div>
                    </div>
                    <div className="release-date-details release-date-details--summary">
                        <div className="row">
                            <div className="col-xs-4">
                                <div>Release date:</div>
                                <strong>{formatDate( releaseDetail.date, DATE_FORMATS.DISPLAY )}</strong>
                            </div>
                            <div className="col-xs-4">
                                <div>Disburse through special pay run?</div>
                                <strong>{releaseDetail.disburse_through_special_pay_run ? 'Yes' : 'No'}</strong>
                            </div>
                            {this.props.bonus.type.basis === BONUS_TYPE_SALARY_BASED ? (
                                <div className="col-xs-4">
                                    <div>Prorate based on tenure?</div>
                                    <strong>{releaseDetail.prorate_based_on_tenure ? 'Yes' : 'No'}</strong>
                                </div>
                            ) : (
                                <div className="col-xs-4">
                                    <div>Status:</div>
                                    <strong>{releaseDetail.status ? 'Disbursed' : 'Not yet released'}</strong>
                                </div>
                            )}
                        </div>
                        {this.props.bonus.type.basis === BONUS_TYPE_SALARY_BASED && releaseDetail.prorate_based_on_tenure ? (
                            <div className="row">
                                <div className="col-xs-4">
                                    <div>Coverage from:</div>
                                    <strong>{releaseDetail.coverage_from ? formatDate( releaseDetail.coverage_from, DATE_FORMATS.DISPLAY ) : '-'}</strong>
                                </div>
                                <div className="col-xs-4">
                                    <div>Coverage to:</div>
                                    <strong>{releaseDetail.coverage_to ? formatDate( releaseDetail.coverage_to, DATE_FORMATS.DISPLAY ) : '-'}</strong>
                                </div>
                                <div className="col-xs-4">
                                    <div>Status:</div>
                                    <strong>{releaseDetail.status ? 'Disbursed' : 'Not yet released'}</strong>
                                </div>
                            </div>
                        ) : null}
                    </div>
                </div>
            ) ) }
        </div>
    )

    /**
     * Component Render Method
     */
    render() {
        return (
            <PageWrapper>
                <Helmet
                    title="Bonus Detail"
                    meta={ [
                        { name: 'description', content: 'Bonus Detail' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/bonuses' );
                            } }
                        >
                         &#8592; Back to { this.props.previousRoute.name ? this.props.previousRoute.name : 'Bonuses' }
                        </A>
                    </Container>
                </NavWrapper>
                { this.props.loading ? (
                    <LoaderWrapper>
                        <H2>Loading</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoaderWrapper>
                ) : (
                    <div>
                        <Container>
                            <HeadingWrapper>
                                <h3>Bonus Detail</h3>
                            </HeadingWrapper>
                        </Container>
                        <FormWrapper>
                            <MainWrapper>
                                <div>
                                    <Container>
                                        <div className="row">
                                            <div className="col-xs-6">
                                            Bonus type
                                            <ValueWrapper>
                                                {this.props.bonus.type.name}
                                            </ValueWrapper>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-12">
                                                <p>Bonus type summary</p>
                                                <div className="summary-panel">
                                                    <div className="summary-panel__column">
                                                        <strong>Amount Basis</strong>
                                                        <p>{BONUS_TYPE.BASIS[ this.props.bonus.type.basis ]}</p>
                                                    </div>
                                                    <div className="summary-panel__column">
                                                        <strong>Frequency</strong>
                                                        <p>{BONUS_TYPE.FREQUENCY[ this.props.bonus.type.frequency ]}</p>
                                                    </div>
                                                    <div className="summary-panel__column">
                                                        <strong>Fully taxable</strong>
                                                        <p>{this.props.bonus.type.fully_taxable ? 'Yes' : 'No'}</p>
                                                    </div>
                                                    <div className="summary-panel__column">
                                                        <strong>Maximum non-taxable amount</strong>
                                                        <p>{!this.props.bonus.type.fully_taxable ? `P ${formatCurrency( this.props.bonus.type.max_non_taxable )}` : '-'}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <strong>Bonus details</strong>
                                        <div className="row" style={ { paddingTop: '15px' } }>
                                            <div className="col-xs-2">
                                                Auto assign:
                                                <ValueWrapper>
                                                    <Switch
                                                        disabled
                                                        checked={ this.props.bonus.auto_assign }
                                                    />
                                                </ValueWrapper>
                                            </div>
                                            <div className="col-xs-10">
                                                Recipients:
                                                <ValueWrapper>
                                                    <MultiSelect
                                                        id="recipients"
                                                        disabled
                                                        data={ this.getRecipientsOptions() }
                                                        value={ this.getRecipientsValues() }
                                                    />
                                                </ValueWrapper>
                                            </div>
                                        </div>
                                        {
                                            this.props.bonus.type.frequency === BONUS_TYPE_ONE_TIME_FREQUENCY
                                                ? this.renderOneTimeReleaseDetails( this.props.bonus.release_details )
                                                : this.renderPeriodicReleaseDetails( this.props.bonus.release_details )
                                        }
                                    </Container>
                                    <div className="foot">
                                        <Button
                                            label="Cancel"
                                            type="neutral"
                                            size="large"
                                            onClick={ () => {
                                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/bonuses' );
                                            } }
                                        />
                                        <Button
                                            label="Edit"
                                            type="action"
                                            size="large"
                                            onClick={ () => {
                                                browserHistory.push( `/bonuses/${this.props.params.id}/edit` );
                                            } }
                                        />
                                    </div>
                                </div>
                            </MainWrapper>
                        </FormWrapper>
                    </div>
                ) }
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    bonus: makeSelectBonus(),
    formOptions: makeSelectFormOptions(),
    notification: makeSelectNotification()
});

/**
 * Combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        editAllowanceActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Detail );
