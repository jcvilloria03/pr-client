export const INITIALIZE = 'app/Bonuses/Detail/INITIALIZE';
export const LOADING = 'app/Bonuses/Detail/LOADING';

export const SET_BONUS = 'app/Bonuses/Detail/SET_BONUS';
export const SET_FORM_OPTIONS = 'app/Bonuses/Detail/SET_FORM_OPTIONS';

export const NOTIFICATION_SAGA = 'app/Bonuses/Detail/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Bonuses/Detail/NOTIFICATION';
