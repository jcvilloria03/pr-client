import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, takeLatest, delay } from 'redux-saga';

// import { browserHistory } from '../../../utils/BrowserHistory';
import { company } from '../../../utils/CompanyService';
import { Fetch } from '../../../utils/request';

import {
    INITIALIZE,
    LOADING,
    SET_BONUS,
    SET_FORM_OPTIONS,
    NOTIFICATION_SAGA,
    NOTIFICATION
} from './constants';

import { REINITIALIZE_PAGE } from '../../App/constants';
import { resetStore } from '../../App/sagas';

/**
 * Initialize data
 */
export function* initializeData({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();

        const bonus = yield call( Fetch, `/other_income/${payload.bonusId}`, { method: 'GET' });

        let employees = null;
        let filterEmployeeIdsQueryString = '';
        if ( bonus.recipients.employees ) {
            const recipients = bonus.recipients.employees;
            if ( recipients.length > 0 ) {
                for ( const recipient of recipients ) {
                    filterEmployeeIdsQueryString += `&filter[employee_ids][]=${recipient.recipient_id}`;
                }
            }

            employees = yield call( Fetch, `/company/${companyId}/employees?include=payroll&mode=MINIMAL${filterEmployeeIdsQueryString}`, { method: 'GET' });
        }

        const payrollGroups = yield call( Fetch, '/account/payroll_groups', { method: 'GET' });
        const departments = yield call( Fetch, `/company/${companyId}/departments`, { method: 'GET' });

        const formOptions = {};
        formOptions.employees = employees ? employees.data.filter( ( employee ) => employee.payroll ) : [];
        formOptions.payrollGroups = payrollGroups.data;
        formOptions.departments = departments.data;

        yield put({
            type: SET_FORM_OPTIONS,
            payload: formOptions
        });

        yield put({
            type: SET_BONUS,
            payload: bonus
        });
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializeData );
}

/**
 * Watch for INITIALIZE
 */
export function* watchForInitializeData() {
    const watcher = yield takeEvery( INITIALIZE, initializeData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for NOTIFICATION
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watch for REINITIALIZE_PAGE
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForInitializeData,
    watchForReinitializePage,
    watchNotify
];
