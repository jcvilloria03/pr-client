import { createSelector } from 'reselect';

/**
 * Direct selector to the edit bonus state domain
 */
const selectEditBonusDomain = () => ( state ) => state.get( 'editBonus' );

const makeSelectLoading = () => createSelector(
    selectEditBonusDomain(),
    ( substate ) => substate.get( 'loading' )
);

const makeSelectSubmitted = () => createSelector(
    selectEditBonusDomain(),
    ( substate ) => substate.get( 'submitted' )
);

const makeSelectFormOptions = () => createSelector(
    selectEditBonusDomain(),
    ( substate ) => substate.get( 'formOptions' ).toJS()
);

const makeSelectBonus = () => createSelector(
    selectEditBonusDomain(),
    ( substate ) => substate.get( 'bonus' ).toJS()
);

const makeSelectBonusTypes = () => createSelector(
    selectEditBonusDomain(),
    ( substate ) => substate.get( 'bonusTypes' ).toJS()
);

const makeSelectNotification = () => createSelector(
    selectEditBonusDomain(),
    ( substate ) => substate.get( 'notification' ).toJS()
);

export {
    makeSelectFormOptions,
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectBonus,
    makeSelectBonusTypes,
    makeSelectNotification
};
