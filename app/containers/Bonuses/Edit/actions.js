import {
    INITIALIZE,
    UPDATE_BONUS
} from './constants';

import { RESET_STORE } from '../../App/constants';

/**
 * Initialize data
 */
export function initializeData( payload ) {
    return {
        type: INITIALIZE,
        payload
    };
}

/**
 * Update commission
 */
export function updateBonus( payload ) {
    return {
        type: UPDATE_BONUS,
        payload
    };
}

/**
 * Reset the store to initial state
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
