import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Container } from 'reactstrap';
import moment from 'moment';
import get from 'lodash/get';

import A from '../../../components/A';
import Button from '../../../components/Button';
import SnackBar from '../../../components/SnackBar';
import SalSelect from '../../../components/Select';
import MultiSelect from '../../../components/MultiSelect';
import Input from '../../../components/Input';
import Switch from '../../../components/Switch';
import SalConfirm from '../../../components/SalConfirm';
import Loader from '../../../components/Loader';
import DatePicker from '../../../components/DatePicker';
import SubHeader from '../../../containers/SubHeader';
import { H2, H3 } from '../../../components/Typography';

import { browserHistory } from '../../../utils/BrowserHistory';
import { isAuthorized } from '../../../utils/Authorization';
import { subscriptionService } from '../../../utils/SubscriptionService';
import { PAYROLL_SUBHEADER_ITEMS, DATE_FORMATS } from '../../../utils/constants';
import {
    stripNonDigit,
    formatCurrency,
    formatCurrencyToDecimalNotation,
    getEmployeeFullName,
    checkIfAnyEmployeesBelongToMultipleGroups,
    truncate,
    formatDate
} from '../../../utils/functions';

import {
    PageWrapper,
    NavWrapper,
    HeadingWrapper,
    FormWrapper,
    MainWrapper,
    LoaderWrapper,
    ConfirmBodyWrapperStyle
} from './styles';

import {
    makeSelectLoading,
    makeSelectSubmitted,
    makeSelectFormOptions,
    makeSelectNotification,
    makeSelectBonus,
    makeSelectBonusTypes
} from './selectors';

import * as actions from './actions';

const BONUS_TYPE_FIXED_BASIS = 'FIXED';
const BONUS_TYPE_SALARY_BASED = 'SALARY_BASED';
const BONUS_TYPE_PERIODIC_FREQUENCY = 'PERIODIC';
const BONUS_TYPE_ONE_TIME_FREQUENCY = 'ONE_TIME';

const BONUS_TYPE = {
    BASIS: {
        [ BONUS_TYPE_FIXED_BASIS ]: 'Fixed',
        [ BONUS_TYPE_SALARY_BASED ]: 'Salary Based'
    },
    FREQUENCY: {
        [ BONUS_TYPE_PERIODIC_FREQUENCY ]: 'Periodic',
        [ BONUS_TYPE_ONE_TIME_FREQUENCY ]: 'One-time'
    }
};

const BASIS = [
    { label: 'Current salary', value: 'CURRENT_BASIC_SALARY' },
    { label: 'Average salary', value: 'AVERAGE_OF_BASIC_SALARY' },
    { label: 'Gross salary', value: 'GROSS_SALARY' },
    { label: 'Basic Pay', value: 'BASIC_PAY' }
];

const RELEASE_DETAILS_STATUS = [
    { label: 'Disbursed', value: 1 },
    { label: 'Not yet released', value: 0 }
];

/**
 * Edit Bonus Component
 */
class Edit extends Component {
    static propTypes = {
        loading: React.PropTypes.bool,
        submitted: React.PropTypes.bool,
        bonus: React.PropTypes.object,
        formOptions: React.PropTypes.object,
        bonusTypes: React.PropTypes.array,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        params: React.PropTypes.object,
        initializeData: React.PropTypes.func,
        updateBonus: React.PropTypes.func,
        resetStore: React.PropTypes.func,
        products: React.PropTypes.array,
        previousRoute: React.PropTypes.object
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            deletingBonusIndex: -1,
            showDeleteWarning: false,
            showModal: false,
            canUpdateBonusType: true,
            bonusForm: {
                type_id: '',
                amount: '',
                basis: '',
                auto_assign: true,
                percentage: '',
                recipients: {
                    employees: [],
                    payroll_groups: [],
                    departments: []
                },
                release_details: [
                    {
                        id: null,
                        disburse_through_special_pay_run: false,
                        date: '',
                        amount: 0
                    }
                ]
            },
            releaseDetails: {
                date: null,
                disburse_through_special_pay_run: false,
                prorate_based_on_tenure: false,
                coverage_from: null,
                coverage_to: null,
                status: null,
                amount: 0
            },
            editReleaseDetails: null,
            prepopulatedEmployeeId: '',
            isWarningMessageVisible: false
        };
    }

    componentWillMount() {
        if ( !subscriptionService.isSubscribedToPayroll( this.props.products ) ) {
            browserHistory.replace( '/unauthorized' );
        }

        isAuthorized(['edit.bonuses'], ( authorized ) => {
            !authorized && browserHistory.replace( '/unauthorized' );
        });

        const { id: bonusId } = this.props.params;
        this.props.initializeData({ bonusId });

        const prepopulatedEmployeeId = JSON.parse( localStorage.getItem( 'employeeIdForEditBonus' ) );
        prepopulatedEmployeeId && this.setState({ prepopulatedEmployeeId });
    }

    componentWillReceiveProps( nextProps ) {
        if ( Object.keys( nextProps.bonus ).length !== Object.keys( this.props.bonus ).length ) {
            this.setInitialBonusFormFromProps( nextProps );
        }
    }

    componentWillUnmount() {
        localStorage.removeItem( 'employeeIdForEditBonus' );
        this.props.resetStore();
    }

    setInitialBonusFormFromProps = ( props ) => {
        const { formOptions, bonus } = props;

        if ( Object.keys( formOptions ).length === 0 ) {
            return;
        }

        // Checks for a release detail which is
        // disbursed through special pay run
        // and has a payroll ID
        const disableEdit = bonus.release_details
            .some( ( detail ) => detail.disburse_through_special_pay_run && Boolean( detail.payroll_id ) );

        this.bonusType = this.props.bonusTypes.find( ( bonusType ) => bonusType.id === bonus.type_id ) || bonus.type;

        this.setState({
            bonusForm: {
                type_id: bonus.type_id,
                amount: `${bonus.amount}`,
                percentage: `${bonus.percentage}`,
                basis: bonus.basis,
                auto_assign: true,
                recipients: {
                    employees: bonus.recipients.employees
                        ? bonus.recipients.employees
                            .reduce( ( employees, e ) => {
                                const employee = formOptions.employees.find( ( f ) => f.id === e.recipient_id );

                                if ( employee ) {
                                    employees.push({
                                        label: getEmployeeFullName( employee ),
                                        value: employee.id,
                                        field: 'employees'
                                    });
                                }

                                return employees;
                            }, [])
                        : [],
                    payroll_groups: bonus.recipients.payroll_groups
                        ? bonus.recipients.payroll_groups
                            .reduce( ( payrollGroups, pg ) => {
                                const payrollGroup = formOptions.payrollGroups.find( ( f ) => f.id === pg.recipient_id );

                                if ( payrollGroup ) {
                                    payrollGroups.push({
                                        label: payrollGroup.name,
                                        value: payrollGroup.id,
                                        field: 'payroll_groups'
                                    });
                                }

                                return payrollGroups;
                            }, [])
                        : [],
                    departments: bonus.recipients.departments
                        ? bonus.recipients.departments
                            .reduce( ( departments, d ) => {
                                const department = formOptions.departments.find( ( f ) => f.id === d.recipient_id );

                                if ( department ) {
                                    departments.push({
                                        label: department.name,
                                        value: department.id,
                                        field: 'departments'
                                    });
                                }

                                return departments;
                            }, [])
                        : []
                },
                release_details: this.bonusType.frequency !== BONUS_TYPE_ONE_TIME_FREQUENCY
                    ? bonus.release_details.map( ( releaseDetails ) => ({ ...releaseDetails, status: releaseDetails.status ? 1 : 0 }) )
                    : []
            },
            disableEdit,
            releaseDetails: this.bonusType.frequency === BONUS_TYPE_ONE_TIME_FREQUENCY ? { ...bonus.release_details[ 0 ], status: bonus.release_details[ 0 ].status ? 1 : 0 } : this.state.releaseDetails
        }, () => {
            if ( this.bonusType.frequency !== BONUS_TYPE_ONE_TIME_FREQUENCY ) {
                const someDisbursed = this.state.bonusForm.release_details.some( ( release ) => !!release.status );
                this.setState({ canUpdateBonusType: !someDisbursed });
            } else {
                this.setState({ canUpdateBonusType: !this.state.releaseDetails.status });
            }
        });
    }

    getRecipientsOptions = () => {
        const { formOptions } = this.props;

        if ( Object.keys( formOptions ).length === 0 ) {
            return [];
        }

        const departments = formOptions.departments.map( ( department ) => ({
            value: department.id, label: department.name, field: 'departments'
        }) );
        const payrollGroups = formOptions.payrollGroups.map( ( payrollGroup ) => ({
            value: payrollGroup.id, label: payrollGroup.name, field: 'payroll_groups'
        }) );
        const employees = formOptions.employees.map( ( employee ) => ({
            value: employee.id, label: getEmployeeFullName( employee ), field: 'employees'
        }) );

        return departments.concat( payrollGroups ).concat( employees );
    }

    getTotalAmountOfAlreadyReleased = ( releases ) => (
        releases.reduce( ( sum, release ) => (
            release.status && release.id && !release.edited ? sum + release.amount : sum
        ), 0 )
    )

    getTotalAmountOfReleases = ( releases ) => (
        releases.reduce( ( acc, release ) => (
            acc + release.amount
        ), 0 )
    )

    getAmountPerRelease = ( index ) => (
        this.readAmountData( this.state.bonusForm.release_details[ index ])
    )

    getLastAddedReleaseIndex = ( releases ) => {
        let index = -1;
        releases.forEach( ( release, i ) => {
            if ( !release.id || ( release.id && !release.status ) || ( release.id && release.status && release.edited ) ) {
                index = i;
            }
        });
        return index;
    }

    partitionReleasesByReleasedAndNew = ( releases = this.state.bonusForm.release_details ) => (
        releases.reduce( ([ released, newReleases ], release ) => (
            release.id && release.status && !release.edited ? [[ ...released, release ], newReleases ] : [ released, [ ...newReleases, release ]]
        ), [[], []])
    )

    remapReleasesWithRecalculatedAmounts = ( releases, amount ) => {
        const [ released, newReleases ] = this.partitionReleasesByReleasedAndNew( releases ); // eslint-disable-line
        const amountOfAlreadyReleased = this.getTotalAmountOfAlreadyReleased( releases );
        return releases.map( ( release ) => {
            if ( release.id && release.status && !release.edited ) {
                return { ...release, amount: truncate( release.amount ) };
            }
            return { ...release, amount: truncate( ( amount - amountOfAlreadyReleased ) / ( newReleases.length || 1 ) ) };
        });
    }

    recalculateReleaseAmounts = ( releases ) => {
        const totalAmount = Number( stripNonDigit( this.state.bonusForm.amount ) );
        const recalculatedReleases = this.remapReleasesWithRecalculatedAmounts( releases, totalAmount );
        const sum = this.getTotalAmountOfReleases( recalculatedReleases );
        const remainder = totalAmount - sum;
        const lastAddedReleaseIndex = this.getLastAddedReleaseIndex( recalculatedReleases );
        if ( lastAddedReleaseIndex > -1 ) {
            const amountOfLastAddedRelease = recalculatedReleases[ lastAddedReleaseIndex ].amount;
            recalculatedReleases[ lastAddedReleaseIndex ].amount = amountOfLastAddedRelease + parseFloat( remainder.toFixed( 2 ) );
        }

        return recalculatedReleases;
    }

    calculateMinimalAmount = () => {
        let minAmount = 0;

        this.state.bonusForm.release_details.forEach( ( release ) => {
            if ( release.status && release.id && !release.edited ) {
                minAmount += release.amount;
            }
        });

        return minAmount || 1;
    }

    readAmountData = ( release ) => (
        release.amount ? release.amount : 0
    )

    calculatePercentage = ( amountOfRelease ) => (
        ( amountOfRelease / ( ( parseFloat( stripNonDigit( this.state.bonusForm.amount ) ) / 100 ) || 1 ) )
    )

    validateAmount( value ) {
        const minimalAmount = this.calculateMinimalAmount();
        const parsedValue = parseFloat( stripNonDigit( value ) );
        if ( value && ( parsedValue < minimalAmount ) ) {
            this.amount.setState({
                error: true,
                errorMessage: `Minimal required amount is ${minimalAmount}`
            });
            return false;
        }
        return true;
    }

    clearForm = ( typeId ) => {
        this.setState({
            bonusForm: {
                type_id: typeId,
                amount: '',
                basis: '',
                auto_assign: true,
                percentage: '',
                recipients: {
                    employees: this.state.prepopulatedEmployeeId ? this.state.bonusForm.recipients.employees : [],
                    payroll_groups: [],
                    departments: []
                },
                release_details: []
            },
            releaseDetails: {
                date: null,
                disburse_through_special_pay_run: false,
                prorate_based_on_tenure: false,
                coverage_from: null,
                coverage_to: null,
                status: null,
                amount: 0
            }
        });
    }

    updateBonusFormState = ( field, value, onSetStateFinish ) => {
        if ( field === 'type_id' ) {
            this.bonusType = this.props.bonusTypes.find( ( bonusType ) => bonusType.id === value );
            setTimeout( () => {
                this.clearForm( value );
            });
        }

        this.setState({
            bonusForm: {
                ...this.state.bonusForm,
                [ field ]: value
            }
        }, onSetStateFinish );
    }

    updateReleaseDetailsState( key, value ) {
        if ( typeof value === 'undefined' ) {
            return;
        }
        const dataClone = Object.assign({}, this.state.releaseDetails );
        dataClone[ key ] = value !== '' && value !== null ? value : '';
        this.setState({ releaseDetails: Object.assign( this.state.releaseDetails, dataClone ) });
    }

    toggleProrateBasedOnTenure = ( value ) => {
        this.updateReleaseDetailsState( 'prorate_based_on_tenure', value );
        if ( !value ) {
            setTimeout( () => {
                this.coverage_from.setState({ selectedDay: '' });
                this.coverage_to.setState({ selectedDay: '' });
            });
        }
    }

    addReleaseDetails = () => {
        if ( this.validateReleaseDetails() ) {
            let releaseDetails = [ ...this.state.bonusForm.release_details, { ...this.state.releaseDetails }];
            releaseDetails = this.recalculateReleaseAmounts( releaseDetails );
            this.updateBonusFormState( 'release_details', releaseDetails );
            this.state.releaseDetails.date = null;
            this.setState({
                releaseDetails: {
                    date: null,
                    disburse_through_special_pay_run: false,
                    prorate_based_on_tenure: false,
                    coverage_from: null,
                    coverage_to: null,
                    status: null
                }
            });
        }
    }

    editReleaseDetails( index ) {
        this.setState({
            releaseDetails: { ...this.state.bonusForm.release_details[ index ] },
            editReleaseDetails: index
        });
    }

    deleteReleaseDetails( index ) {
        const releaseDetails = this.state.bonusForm.release_details;
        const newAmount = parseFloat( stripNonDigit( this.state.bonusForm.amount ) ) - parseFloat( this.getAmountPerRelease( index ) );
        this.setState({
            bonusForm: {
                ...this.state.bonusForm,
                amount: formatCurrency( newAmount ),
                release_details: releaseDetails.slice( 0, index ).concat( releaseDetails.slice( index + 1 ) )
            }
        });
    }

    updateReleaseDetails = () => {
        if ( this.validateReleaseDetails() ) {
            this.updateBonusFormState( 'release_details', this.state.bonusForm.release_details.map( ( releaseDetails, index ) => {
                if ( index === this.state.editReleaseDetails ) {
                    if ( this.state.bonusForm.release_details[ index ].id ) {
                        const initialStatus = this.props.bonus.release_details[ index ].status;
                        if ( !initialStatus ) {
                            return { ...this.state.releaseDetails, edited: !!this.state.releaseDetails.status };
                        }
                    }
                    return { ...this.state.releaseDetails };
                }

                return releaseDetails;
            }) );
            this.state.releaseDetails.date = null;
            this.setState({
                releaseDetails: {
                    date: null,
                    disburse_through_special_pay_run: false,
                    prorate_based_on_tenure: false,
                    coverage_from: null,
                    coverage_to: null,
                    status: null
                },
                editReleaseDetails: null
            });
        }
    }

    validateBonusData() {
        let valid = true;

        if ( !this.type_id._checkRequire( this.type_id.state.value ) ) {
            valid = false;
        }

        if ( !this.recipients._checkRequire( this.recipients.state.value ) ) {
            valid = false;
        }

        if ( this.bonusType.basis === BONUS_TYPE_FIXED_BASIS ) {
            if ( this.amount._validate( stripNonDigit( this.amount.state.value ) ) ) {
                valid = false;
            }
            if ( !this.validateAmount( this.amount.state.value ) ) {
                valid = false;
            }
        } else if ( this.bonusType.basis === BONUS_TYPE_SALARY_BASED ) {
            if ( !this.basis._checkRequire( this.basis.state.value ) ) {
                valid = false;
            }

            if ( this.monthly_salary_percentage._validate( stripNonDigit( this.monthly_salary_percentage.state.value ) ) ) {
                valid = false;
            }
        }

        if ( this.bonusType.frequency === BONUS_TYPE_PERIODIC_FREQUENCY ) {
            if ( this.number_of_releases._validate( this.number_of_releases.state.value ) ) {
                valid = false;
            }
        }

        if ( this.bonusType.frequency === BONUS_TYPE_ONE_TIME_FREQUENCY ) {
            valid = this.validateReleaseDetails();
        }

        return valid;
    }

    validateReleaseDetails() {
        let valid = true;

        if ( this.release_date.checkRequired() ) {
            valid = false;
        }

        if ( !this.status._checkRequire( this.status.state.value ) ) {
            valid = false;
        }

        if ( this.bonusType.basis === BONUS_TYPE_SALARY_BASED && this.state.releaseDetails.prorate_based_on_tenure ) {
            if ( this.coverage_from.checkRequired() ) {
                valid = false;
            }

            if ( this.state.releaseDetails.coverage_to
                && this.state.releaseDetails.coverage_from
                && !moment( this.state.releaseDetails.coverage_to, Object.values( DATE_FORMATS ) ).isAfter( this.state.releaseDetails.coverage_from, Object.values( DATE_FORMATS ) )
            ) {
                const message = 'The coverage to date must be a date after coverage from.';
                this.coverage_to.setState({ error: true, message });

                valid = false;
            } else if ( this.coverage_to.checkRequired() ) {
                valid = false;
            }
        }

        return valid;
    }

    prepareBonusForSubmit = ( b ) => {
        const bonus = { ...b };
        if ( this.bonusType.basis !== BONUS_TYPE_SALARY_BASED ) {
            if ( this.bonusType.frequency !== BONUS_TYPE_ONE_TIME_FREQUENCY ) {
                bonus.release_details = bonus.release_details.map( ( release ) => {
                    const { coverage_from, coverage_to, prorate_based_on_tenure, ...rest } = release; // eslint-disable-line
                    return rest;
                });
            } else {
                const { coverage_from, coverage_to, prorate_based_on_tenure, ...rest } = this.state.releaseDetails; // eslint-disable-line
                bonus.release_details = [rest];
            }
        } else if ( this.bonusType.basis === BONUS_TYPE_SALARY_BASED && this.bonusType.frequency === BONUS_TYPE_ONE_TIME_FREQUENCY ) {
            bonus.release_details = [this.state.releaseDetails];
        }

        return {
            ...bonus,
            amount: Number( stripNonDigit( bonus.amount ) ),
            recipients: {
                employees: bonus.recipients.employees.map( ( x ) => x.value ),
                payroll_groups: bonus.recipients.payroll_groups.map( ( x ) => x.value ),
                departments: bonus.recipients.departments.map( ( x ) => x.value )
            }
        };
    }

    updateBonus = () => {
        if ( this.validateBonusData() ) {
            this.props.updateBonus({
                ...this.prepareBonusForSubmit( this.state.bonusForm ),
                bonusId: this.props.params.id,
                previousRouteName: this.props.previousRoute.name
            });
        }
    }

    renderReleaseDetailsForm( id ) {
        const { releaseDetails } = this.state;

        const formReleaseDetails = get( this.state.bonusForm, 'release_details', []);
        const index = formReleaseDetails.findIndex( ( detail ) => detail.id === id );

        const disburseThroughSpecialPayRun = get( formReleaseDetails, [ index, 'disburse_through_special_pay_run' ], false );
        const includedInPayroll = Boolean( get( formReleaseDetails, [ index, 'payroll_id' ]) );
        const disableEdit = disburseThroughSpecialPayRun && includedInPayroll;

        return (
            <div>
                <div className="row">
                    <div className="col-xs-4 date">
                        <DatePicker
                            label="Release date"
                            dayFormat={ DATE_FORMATS.DISPLAY }
                            selectedDay={ releaseDetails.date }
                            required
                            disabled={ disableEdit }
                            ref={ ( ref ) => { this.release_date = ref; } }
                            onChange={ ( value ) => this.setState( ( state ) => ({
                                releaseDetails: {
                                    ...state.releaseDetails,
                                    date: formatDate( value, DATE_FORMATS.API )
                                }
                            }) ) }
                        />
                    </div>
                    <div className="col-xs-4">
                        <span>* Disburse through Special Pay Run?</span>
                        <div style={ { marginTop: '10px' } }>
                            <Switch
                                checked={ releaseDetails.disburse_through_special_pay_run }
                                onChange={ ( value ) => { this.updateReleaseDetailsState( 'disburse_through_special_pay_run', value ); } }
                                disabled={ disableEdit }
                            />
                        </div>
                    </div>
                    {this.bonusType.basis !== BONUS_TYPE_SALARY_BASED && (
                        <div className="col-xs-4">
                            <SalSelect
                                id="status"
                                label="Status"
                                required
                                disabled={ disableEdit }
                                key="status"
                                data={ RELEASE_DETAILS_STATUS }
                                value={ releaseDetails.status }
                                placeholder="Choose an option"
                                ref={ ( ref ) => { this.status = ref; } }
                                onChange={ ({ value }) => { this.updateReleaseDetailsState( 'status', value ); } }
                            />
                        </div>
                    )}
                </div>
                {this.bonusType.basis === BONUS_TYPE_SALARY_BASED ? (
                    <div className="row">
                        <div className="col-xs-4 date">
                            <DatePicker
                                label="Coverage from"
                                dayFormat={ DATE_FORMATS.DISPLAY }
                                required={ Boolean( releaseDetails.prorate_based_on_tenure ) }
                                disabled={ !releaseDetails.prorate_based_on_tenure }
                                selectedDay={ releaseDetails.coverage_from }
                                ref={ ( ref ) => { this.coverage_from = ref; } }
                                onChange={ ( value ) => this.setState( ( state ) => ({
                                    releaseDetails: {
                                        ...state.releaseDetails,
                                        coverage_from: formatDate( value, DATE_FORMATS.API )
                                    }
                                }) ) }

                            />
                        </div>
                        <div className="col-xs-4 date">
                            <DatePicker
                                label="Coverage to"
                                dayFormat={ DATE_FORMATS.DISPLAY }
                                required={ Boolean( releaseDetails.prorate_based_on_tenure ) }
                                disabled={ !releaseDetails.prorate_based_on_tenure }
                                selectedDay={ releaseDetails.coverage_to }
                                ref={ ( ref ) => { this.coverage_to = ref; } }
                                onChange={ ( value ) => this.setState( ( state ) => ({
                                    releaseDetails: {
                                        ...state.releaseDetails,
                                        coverage_to: formatDate( value, DATE_FORMATS.API )
                                    }
                                }) ) }

                            />
                        </div>
                        <div className="col-xs-4">
                            <SalSelect
                                id="status"
                                label="Status"
                                required
                                disabled={ disableEdit }
                                key="status"
                                data={ RELEASE_DETAILS_STATUS }
                                value={ releaseDetails.status }
                                placeholder="Choose an option"
                                ref={ ( ref ) => { this.status = ref; } }
                                onChange={ ({ value }) => { this.updateReleaseDetailsState( 'status', value ); } }
                            />
                        </div>
                    </div>
                ) : null}
            </div>
        );
    }

    render() {
        return (
            <PageWrapper>
                <Helmet
                    title="Edit Bonus"
                    meta={ [
                        { name: 'description', content: 'Edit a Bonus' }
                    ] }
                />
                <SnackBar
                    message={ this.props.notification.message }
                    title={ this.props.notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ this.props.notification.show }
                    delay={ 5000 }
                    type={ this.props.notification.type }
                />
                <SubHeader items={ PAYROLL_SUBHEADER_ITEMS } />
                <NavWrapper>
                    <Container>
                        <A
                            href
                            onClick={ ( e ) => {
                                e.preventDefault();
                                this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/bonuses' );
                            } }
                        >
                            &#8592; Back to { this.props.previousRoute.name ? this.props.previousRoute.name : 'Bonuses' }
                        </A>
                    </Container>
                </NavWrapper>
                { this.props.loading ? (
                    <LoaderWrapper>
                        <H2>Loading</H2>
                        <br />
                        <H3>Please wait...</H3>
                    </LoaderWrapper>
                ) : (
                    <div>
                        <Container>
                            <HeadingWrapper>
                                <h3>Edit Bonus</h3>
                            </HeadingWrapper>
                        </Container>
                        <FormWrapper>
                            <MainWrapper>
                                <SalConfirm
                                    onConfirm={ () => { this.deleteReleaseDetails( this.state.deletingBonusIndex ); } }
                                    onClose={ () => { this.setState({ deletingBonusIndex: -1 }); } }
                                    body={
                                        <ConfirmBodyWrapperStyle>
                                            <div className="message">
                                                You are about to delete a disbursed release.
                                                <br /><br />
                                                Do you wish to proceed?
                                            </div>
                                        </ConfirmBodyWrapperStyle>
                                    }
                                    title="Warning!"
                                    visible={ this.state.showDeleteWarning }
                                />
                                <SalConfirm
                                    onConfirm={ () => {
                                        this.updateBonusFormState( 'type_id', this.type_id.state.value.value );
                                    } }
                                    onClose={ () => {
                                        this.type_id.setState({ value: this.state.bonusForm.type_id });
                                    } }
                                    body={
                                        <ConfirmBodyWrapperStyle>
                                            <div className="message">
                                                Part two of the bonus form will be cleared upon changing bonus type.
                                                <br /><br />
                                                Do you wish to proceed?
                                            </div>
                                        </ConfirmBodyWrapperStyle>
                                    }
                                    title="Warning!"
                                    visible={ this.state.showModal }
                                />
                                <SalConfirm
                                    title="Warning"
                                    body={
                                        <ConfirmBodyWrapperStyle>
                                            <div className="message">
                                                Some employees you chose belong to several groups. However, those employees will receive the bonus only once.
                                            </div>
                                        </ConfirmBodyWrapperStyle>
                                    }
                                    onConfirm={ () => {
                                        this.setState({ isWarningMessageVisible: false });
                                    } }
                                    showCancel={ false }
                                    buttonStyle="info"
                                    confirmText="Ok"
                                    visible={ this.state.isWarningMessageVisible }
                                />
                                <Container>
                                    <div className="row">
                                        <div className="col-xs-4">
                                            <SalSelect
                                                id="type_id"
                                                label="Bonus type"
                                                required
                                                disabled={ !this.state.canUpdateBonusType || this.state.disableEdit }
                                                key="type_id"
                                                data={ this.props.formOptions.bonusTypes }
                                                value={ this.state.bonusForm.type_id }
                                                placeholder="Choose a bonus type"
                                                ref={ ( ref ) => { this.type_id = ref; } }
                                                onChange={ () => {
                                                    this.setState({ showModal: false }, () => {
                                                        this.setState({ showModal: true });
                                                    });
                                                } }
                                            />
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-xs-12">
                                            <p>Bonus type summary</p>
                                            <div className="summary-panel">
                                                <div className="summary-panel__column">
                                                    <strong>Amount Basis</strong>
                                                    <p>{BONUS_TYPE.BASIS[ this.bonusType.basis ]}</p>
                                                </div>
                                                <div className="summary-panel__column">
                                                    <strong>Frequency</strong>
                                                    <p>{BONUS_TYPE.FREQUENCY[ this.bonusType.frequency ]}</p>
                                                </div>
                                                <div className="summary-panel__column">
                                                    <strong>Fully taxable</strong>
                                                    <p>{this.bonusType.fully_taxable ? 'Yes' : 'No'}</p>
                                                </div>
                                                <div className="summary-panel__column">
                                                    <strong>Maximum non-taxable amount</strong>
                                                    <p>{!this.bonusType.fully_taxable ? `P ${formatCurrency( this.bonusType.max_non_taxable )}` : ''}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row" style={ { margin: '50px 0' } }>
                                        <div className="col-xs-4" style={ { marginLeft: '-15px' } }>
                                            <Switch
                                                defaultChecked
                                                disabled
                                            />
                                            <span
                                                style={ {
                                                    fontSize: '14px',
                                                    marginBottom: '4px',
                                                    marginLeft: '5px'
                                                } }
                                            >
                                                Auto-assign
                                            </span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-12">
                                            <MultiSelect
                                                id="recipients"
                                                label={ <span>Recipients</span> }
                                                placeholder="Search recipients"
                                                required
                                                data={ this.getRecipientsOptions() }
                                                disabled={ !!this.state.prepopulatedEmployeeId || this.state.disableEdit }
                                                value={ [
                                                    ...this.state.bonusForm.recipients.employees,
                                                    ...this.state.bonusForm.recipients.payroll_groups,
                                                    ...this.state.bonusForm.recipients.departments
                                                ] }
                                                onChange={ ( values ) => {
                                                    const newRecipientsValue = {
                                                        employees: values.filter( ( v ) => v.field === 'employees' ),
                                                        payroll_groups: values.filter( ( v ) => v.field === 'payroll_groups' ),
                                                        departments: values.filter( ( v ) => v.field === 'departments' )
                                                    };

                                                    this.updateBonusFormState( 'recipients', newRecipientsValue, () => {
                                                        const belongsToMultipleGroups = checkIfAnyEmployeesBelongToMultipleGroups(
                                                            {
                                                                employeesIds: newRecipientsValue.employees.map( ( employee ) => employee.value ),
                                                                departmentsIds: newRecipientsValue.departments.map( ( department ) => department.value ),
                                                                payrollGroupsIds: newRecipientsValue.payroll_groups.map( ( group ) => group.value )
                                                            },
                                                            this.props.formOptions.employees
                                                        );

                                                        if ( belongsToMultipleGroups ) {
                                                            this.setState({
                                                                isWarningMessageVisible: true
                                                            });
                                                        }
                                                    });
                                                } }
                                                ref={ ( ref ) => { this.recipients = ref; } }
                                            />
                                        </div>
                                    </div>

                                    <div style={ { marginBottom: '50px' } }>
                                        <div className="row">
                                            {this.bonusType.basis === BONUS_TYPE_FIXED_BASIS ? (
                                                <div className="col-xs-4">
                                                    <Input
                                                        id="amount"
                                                        label="Amount"
                                                        required
                                                        key="amount"
                                                        value={ this.state.bonusForm.amount }
                                                        ref={ ( ref ) => { this.amount = ref; } }
                                                        onChange={ ( value ) => { this.amount.setState({ value: stripNonDigit( value ) }); } }
                                                        onFocus={ () => {
                                                            this.amount.setState({ value: stripNonDigit( this.amount.state.value ) });
                                                        } }
                                                        onBlur={ ( value ) => {
                                                            this.amount.setState({ value: formatCurrency( value ) }, () => {
                                                                this.updateBonusFormState( 'amount', formatCurrency( value ), () => {
                                                                    const releaseDetails = this.recalculateReleaseAmounts( this.state.bonusForm.release_details );
                                                                    this.updateBonusFormState( 'release_details', releaseDetails );
                                                                });
                                                                this.validateAmount( value );
                                                            });
                                                        } }
                                                    />
                                                </div>
                                            ) : (
                                                <div>
                                                    <div className="col-xs-4">
                                                        <SalSelect
                                                            id="basis"
                                                            label="Amount basis"
                                                            required
                                                            key="basis"
                                                            data={ BASIS }
                                                            value={ this.state.bonusForm.basis }
                                                            placeholder="Choose an option"
                                                            ref={ ( ref ) => { this.basis = ref; } }
                                                            onChange={ ({ value }) => { this.updateBonusFormState( 'basis', value ); } }
                                                        />
                                                    </div>
                                                    <div className="col-xs-4">
                                                        <Input
                                                            id="monthly_salary_percentage"
                                                            label="Percentage of monthly salary"
                                                            required
                                                            minNumber={ 1 }
                                                            key="monthly_salary_percentage"
                                                            value={ this.state.bonusForm.percentage }
                                                            ref={ ( ref ) => { this.monthly_salary_percentage = ref; } }
                                                            onChange={ ( value ) => { this.monthly_salary_percentage.setState({ value: stripNonDigit( value ) }); } }
                                                            onFocus={ () => {
                                                                this.monthly_salary_percentage.setState({ value: stripNonDigit( this.monthly_salary_percentage.state.value ) });
                                                            } }
                                                            onBlur={ ( value ) => {
                                                                this.monthly_salary_percentage.setState({ value: formatCurrency( value ) }, () => {
                                                                    this.updateBonusFormState( 'percentage', formatCurrency( value ) );
                                                                });
                                                            } }
                                                            addon={ {
                                                                content: '%',
                                                                placement: 'right'
                                                            } }
                                                        />
                                                    </div>
                                                </div>
                                            )}
                                            {this.bonusType.frequency === BONUS_TYPE_PERIODIC_FREQUENCY ? (
                                                <div className="col-xs-4">
                                                    <Input
                                                        id="number_of_releases"
                                                        label="Number of releases"
                                                        key="number_of_releases"
                                                        disabled
                                                        minNumber={ 1 }
                                                        value={ this.state.bonusForm.release_details.length.toString() }
                                                        ref={ ( ref ) => { this.number_of_releases = ref; } }
                                                    />
                                                </div>
                                            ) : null}
                                        </div>
                                    </div>

                                    {this.bonusType.frequency === BONUS_TYPE_PERIODIC_FREQUENCY && this.state.bonusForm.release_details.map( ( releaseDetails, index ) => {
                                        if ( this.state.editReleaseDetails === index ) {
                                            return (
                                                <div key={ index }>
                                                    <div className="release-date-header">
                                                        <div className="flex-align-center">
                                                            <strong>Release date {index + 1}</strong>
                                                        </div>
                                                        <div>
                                                            <Button
                                                                label="Update"
                                                                type="neutral"
                                                                onClick={ this.updateReleaseDetails }
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="release-date-details">
                                                        {this.renderReleaseDetailsForm( releaseDetails.id )}
                                                    </div>
                                                </div>
                                            );
                                        }

                                        return (
                                            <div key={ index }>
                                                <div className="release-date-header">
                                                    <div className="flex-align-center">
                                                        <strong>Release date {index + 1}</strong>
                                                        {this.bonusType.basis === BONUS_TYPE_FIXED_BASIS && this.state.bonusForm.amount ? (
                                                            <div className="release-date-summary">
                                                                <dic className="release-date-summary__amount">
                                                                    Amount per recipient: &nbsp;<strong>P { formatCurrency( truncate( this.getAmountPerRelease( index ) ) ) }</strong>
                                                                </dic>
                                                                <div className="release-date-summary__percentage">
                                                                    <strong>{ formatCurrencyToDecimalNotation( this.calculatePercentage( this.getAmountPerRelease( index ) ).toFixed( 2 ) ) }%</strong>
                                                                </div>
                                                            </div>
                                                        ) : null}
                                                    </div>
                                                    {this.state.editReleaseDetails === null ? (
                                                        <div>
                                                            <Button
                                                                label="Edit"
                                                                type="neutral"
                                                                disabled={ !!releaseDetails.status && !!releaseDetails.id && !releaseDetails.edited }
                                                                onClick={ () => this.editReleaseDetails( index ) }
                                                            />
                                                            <Button
                                                                label="Delete"
                                                                type="neutral"
                                                                onClick={ () => {
                                                                    if ( releaseDetails.status ) {
                                                                        this.setState({ showDeleteWarning: false, deletingBonusIndex: index }, () => {
                                                                            this.setState({ showDeleteWarning: true });
                                                                        });
                                                                    } else {
                                                                        this.deleteReleaseDetails( index );
                                                                    }
                                                                } }
                                                            />
                                                        </div>
                                                    ) : null}
                                                </div>
                                                <div className="release-date-details release-date-details--summary">
                                                    <div className="row">
                                                        <div className="col-xs-4">
                                                            <div>Release date</div>
                                                            <strong>{formatDate( releaseDetails.date, DATE_FORMATS.DISPLAY )}</strong>
                                                        </div>
                                                        <div className="col-xs-4">
                                                            <div>Disburse through Special Pay Run</div>
                                                            <strong>{releaseDetails.disburse_through_special_pay_run ? 'Yes' : 'No'}</strong>
                                                        </div>
                                                        {
                                                            this.bonusType.basis === BONUS_TYPE_FIXED_BASIS ? (
                                                                <div className="col-xs-4">
                                                                    <div>Status</div>
                                                                    <strong>{RELEASE_DETAILS_STATUS.find( ( status ) => status.value === releaseDetails.status ).label}</strong>
                                                                </div>
                                                            ) : null
                                                        }
                                                    </div>
                                                    {
                                                        this.bonusType.basis === BONUS_TYPE_SALARY_BASED && !releaseDetails.prorate_based_on_tenure ? (
                                                            <div className="row">
                                                                <div className="col-xs-4">
                                                                    <div>Status</div>
                                                                    <strong>{RELEASE_DETAILS_STATUS.find( ( status ) => status.value === releaseDetails.status ).label}</strong>
                                                                </div>
                                                            </div>
                                                        ) : null
                                                    }
                                                    {this.bonusType.basis === BONUS_TYPE_SALARY_BASED && releaseDetails.prorate_based_on_tenure ? (
                                                        <div className="row">
                                                            <div className="col-xs-4">
                                                                <div>Coverage from</div>
                                                                <strong>{formatDate( releaseDetails.coverage_from, DATE_FORMATS.DISPLAY )}</strong>
                                                            </div>
                                                            <div className="col-xs-4">
                                                                <div>Coverage to</div>
                                                                <strong>{formatDate( releaseDetails.coverage_to, DATE_FORMATS.DISPLAY )}</strong>
                                                            </div>
                                                            <div className="col-xs-4">
                                                                <div>Status</div>
                                                                <strong>{RELEASE_DETAILS_STATUS.find( ( status ) => status.value === releaseDetails.status ).label}</strong>
                                                            </div>
                                                        </div>
                                                    ) : null}
                                                </div>
                                            </div>
                                        );
                                    })}
                                    {this.state.editReleaseDetails === null ? (
                                        this.bonusType.frequency === BONUS_TYPE_PERIODIC_FREQUENCY ? (
                                            <div>
                                                <div className="release-date-header">
                                                    <div className="flex-align-center">
                                                        <strong>Release date {this.state.bonusForm.release_details.length + 1}</strong>
                                                    </div>
                                                    <div>
                                                        <Button
                                                            label="Add release date"
                                                            type="neutral"
                                                            onClick={ this.addReleaseDetails }
                                                        />
                                                    </div>
                                                </div>
                                                <div className="release-date-details">
                                                    {this.renderReleaseDetailsForm()}
                                                </div>
                                            </div>
                                        ) : this.renderReleaseDetailsForm()
                                    ) : <div style={ { height: '210px' } } />}
                                </Container>
                                <div className="foot">
                                    <Button
                                        label="Cancel"
                                        type="neutral"
                                        size="large"
                                        onClick={ () => {
                                            this.props.previousRoute.name ? browserHistory.goBack() : browserHistory.push( '/bonuses' );
                                        } }
                                    />
                                    <Button
                                        label={ this.props.submitted ? <Loader /> : 'Update' }
                                        type="action"
                                        size="large"
                                        onClick={ this.updateBonus }
                                    />
                                </div>
                            </MainWrapper>
                        </FormWrapper>
                    </div>
                ) }
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    submitted: makeSelectSubmitted(),
    formOptions: makeSelectFormOptions(),
    bonus: makeSelectBonus(),
    bonusTypes: makeSelectBonusTypes(),
    notification: makeSelectNotification()
});

const mapDispatchToProps = ( dispatch ) => (
    bindActionCreators( actions, dispatch )
);

export default connect( mapStateToProps, mapDispatchToProps )( Edit );
