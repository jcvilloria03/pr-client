import { fromJS } from 'immutable';
import {
    LOADING,
    SET_FORM_OPTIONS,
    SET_SUBMITTED,
    NOTIFICATION_SAGA,
    SET_BONUS,
    SET_BONUS_TYPES
} from './constants';

import { RESET_STORE } from '../../App/constants';

const initialState = fromJS({
    loading: true,
    submitted: false,
    formOptions: {},
    bonus: {},
    bonusTypes: [],
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    }
});

/**
 *
 * Edit bonus reducer
 *
 */
function editBonusReducer( state = initialState, action ) {
    switch ( action.type ) {
        case LOADING:
            return state.set( 'loading', action.payload );
        case SET_SUBMITTED:
            return state.set( 'submitted', action.payload );
        case SET_FORM_OPTIONS:
            return state.set( 'formOptions', fromJS( action.payload ) );
        case SET_BONUS:
            return state.set( 'bonus', fromJS( action.payload ) );
        case SET_BONUS_TYPES:
            return state.set( 'bonusTypes', fromJS( action.payload ) );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case RESET_STORE:
            return initialState;
        default:
            return state;
    }
}

export default editBonusReducer;
