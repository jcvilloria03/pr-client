export const INITIALIZE = 'app/Bonuses/Edit/INITIALIZE';
export const LOADING = 'app/Bonuses/Edit/LOADING';

export const SET_SUBMITTED = 'app/Bonuses/Edit/SET_SUBMITTED';
export const SET_FORM_OPTIONS = 'app/Bonuses/Edit/SET_FORM_OPTIONS';
export const SET_BONUS = 'app/Bonuses/Edit/SET_BONUS';
export const SET_BONUS_TYPES = 'app/Bonuses/Edit/SET_BONUS_TYPES';

export const UPDATE_BONUS = 'app/Bonuses/Edit/UPDATE_COMMISSION';

export const NOTIFICATION_SAGA = 'app/Bonuses/Edit/NOTIFICATION_SAGA';
export const NOTIFICATION = 'app/Bonuses/Edit/NOTIFICATION';
