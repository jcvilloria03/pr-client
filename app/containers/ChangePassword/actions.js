import {
    SUBMIT
} from './constants';

/**
 * Submit request for change password
 * @param {Object} payload - Contains password and confirm password
 */
export function submitRequest( payload ) {
    return {
        type: SUBMIT,
        payload
    };
}
