import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import SnackBar from '../../components/SnackBar';
import { H3, SUB } from '../../components/Typography';
import Input from '../../components/Input';
import Button from '../../components/Button';

import {
    StyledWrapper,
    StyledContent
} from './styles';
import * as actions from './actions';
import {
    makeSelectSubmitting,
    makeSelectNotification
} from './selectors';

/**
 *
 * ChangePassword
 *
 */
export class ChangePassword extends React.PureComponent {
    static propTypes = {
        notification: PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        submitRequest: PropTypes.func,
        submitting: PropTypes.bool
    }

    /**
     * Form Validation
     */
    validateFields = () => {
        let valid = true;

        const password = this.password.state;
        const confirm = this.confirm.state;

        if ( password.value && !password.error && confirm.value && !confirm.error ) {
            valid = password.value === confirm.value;
            this.confirm.setState({
                error: !valid,
                errorMessage: valid ? '' : 'Passwords do not match.'
            });
        } else {
            valid = false;
        }
        return valid;
    }

    /**
     * sends a request to change password
     */
    submitRequest() {
        const valid = this.validateFields();
        if ( valid ) {
            this.props.submitRequest({
                currentPassword: this.currentPassword.state.value,
                newPass: this.password.state.value,
                newPassConfirm: this.confirm.state.value
            });
        }
    }

    /**
     *
     * ChangePassword render method
     *
     */
    render() {
        const { notification, submitting } = this.props;

        return (
            <div>
                <StyledWrapper>
                    <Helmet
                        title="Change Password"
                        meta={ [
                            { name: 'description', content: 'Description of ChangePassword' }
                        ] }
                    />
                    <SnackBar
                        message={ notification.message }
                        title={ notification.title }
                        offset={ { top: 0 } }
                        type={ notification.type }
                        show={ notification.show }
                        delay={ 5000 }
                        ref={ ( ref ) => { this.notification = ref; } }
                    />
                    <StyledContent>
                        <H3>Change Your Password</H3>
                        <hr />
                        <Input
                            required
                            id="current"
                            type="password"
                            label="Current Password"
                            onBlur={ this.validateFields }
                            min={ 8 }
                            ref={ ( ref ) => { this.currentPassword = ref; } }
                            className="sl-u-gap-bottom"
                            disabled={ submitting }
                        />
                        <Input
                            required
                            id="new"
                            type="password"
                            label="New Password"
                            onBlur={ this.validateFields }
                            min={ 8 }
                            ref={ ( ref ) => { this.password = ref; } }
                            className="sl-u-gap-bottom"
                            disabled={ submitting }
                        />
                        <Input
                            required
                            id="confirm"
                            type="password"
                            label="Confirm Password"
                            onBlur={ this.validateFields }
                            min={ 8 }
                            ref={ ( ref ) => { this.confirm = ref; } }
                            disabled={ submitting }
                        />
                        <div style={ { textAlign: 'center', marginTop: '20px' } }>
                            <Button
                                label="SUBMIT REQUEST"
                                size="large"
                                disabled={ submitting }
                                onClick={ () => {
                                    this.password._validate( this.password.state.value );
                                    this.confirm._validate( this.confirm.state.value );
                                    this.submitRequest();
                                } }
                            />
                        </div>
                    </StyledContent>
                </StyledWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    submitting: makeSelectSubmitting(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        actions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( ChangePassword );
