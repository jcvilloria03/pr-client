/*
 *
 * Change Password constants
 *
 */
export const NAMESPACE = 'app/containers/ChangePassword/';

export const SUBMIT = `${NAMESPACE}SUBMIT`;
export const NOTIFICATION = `${NAMESPACE}NOTIFICATION`;
export const SET_NOTIFICATION = `${NAMESPACE}SET_NOTIFICATION`;
export const SET_SUBMITTING = `${NAMESPACE}SET_SUBMITTING`;
