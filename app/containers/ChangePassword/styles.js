import styled from 'styled-components';

export const StyledWrapper = styled.div`
    background-color: #f0f4f6;
    min-height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    color: #444;
    padding: 90px 20px 20px 20px;
    margin-top: -75px;
`;

export const StyledContent = styled.div`
    background-color: #FFF;
    width: 30%;
    min-width: 400px;
    max-width: 500px;
    height: 100%;
    box-shadow: 0px 0px 20px 15px rgba(204,204,204,.2);
    padding: 30px 50px 30px;
    margin-bottom: 3rem;

    a {
        color: #00A5E5 !important;
    }
`;
