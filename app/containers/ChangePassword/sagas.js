import { take, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { takeEvery, delay } from 'redux-saga';

import { browserHistory } from '../../utils/BrowserHistory';
import { auth } from '../../utils/AuthService';

import {
    SUBMIT,
    SET_NOTIFICATION,
    NOTIFICATION,
    SET_SUBMITTING
} from './constants';

/**
 * Submits form
 *
 */
export function* submitForm({ payload }) {
    const { currentPassword, newPass, newPassConfirm } = payload;
    yield put({
        type: SET_SUBMITTING,
        payload: true
    });

    try {
        const response = yield call( auth.changePassword, currentPassword, newPass, newPassConfirm );

        if ( response ) {
            yield put({
                type: SET_NOTIFICATION,
                payload: {
                    title: 'Success!',
                    message: 'You have successfully changed your password. Logging out in 5 seconds.',
                    type: 'success',
                    show: true
                }
            });

            yield call( delay, 5000 );
            localStorage.clear();
            yield call( browserHistory.replace, '/login', true );
        }
    } catch ( error ) {
        yield call( notifyUser, error );
    } finally {
        yield put({
            type: SET_SUBMITTING,
            payload: false
        });
    }
}

/**
 * Display a notification to user
 */
export function* notifyUser( error ) {
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });

    const payload = {
        show: true,
        title: error.response ? error.response.statusText : 'Error',
        message: error.response ? error.response.data.message : error.statusText,
        type: 'error'
    };

    yield put({
        type: SET_NOTIFICATION,
        payload
    });

    yield call( delay, 5000 );
    yield put({
        type: SET_NOTIFICATION,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: 'error'
        }
    });
}

/**
 * Individual exports for testing
 */
export function* watchForSubmitForm() {
    const watcher = yield takeEvery( SUBMIT, submitForm );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchNotify() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForSubmitForm,
    watchNotify
];
