import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    NOTIFICATION_SAGA,
    SET_ADD_DEDUCTIONTYPE
} from './constants';

const initialState = fromJS({
    addDeductionType: '',
    loading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    }
});

/**
 *
 * Add reducer
 *
 */
function addReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_ADD_DEDUCTIONTYPE:
            return state.set( 'addDeductionType', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        default:
            return state;
    }
}

export default addReducer;
