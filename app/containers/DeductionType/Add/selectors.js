import { createSelector } from 'reselect';

/**
 * Direct selector to the add state domain
 */
const selectAddDeductionTypeDomain = () => ( state ) => state.get( 'addDeductionType' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by Add
 */

const makeSelectAddDeductionType = () => createSelector(
  selectAddDeductionTypeDomain(),
  ( substate ) => substate.toJS()
);

const makeSelectSetAddDeductionType = () => createSelector(
  selectAddDeductionTypeDomain(),
  ( substate ) => substate.get( 'addDeductionType' )
);

const makeSelectNotification = () => createSelector(
  selectAddDeductionTypeDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectAddDeductionTypeDomain(),
  ( substate ) => substate.get( 'loading' )
);

export default makeSelectAddDeductionType;
export {
  selectAddDeductionTypeDomain,
  makeSelectSetAddDeductionType,
  makeSelectNotification,
  makeSelectLoading
};
