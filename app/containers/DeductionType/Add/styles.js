import styled from 'styled-components';

export const NavWrapper = styled.div`
    padding: 10px 20px;
    background: #f0f4f6;
    margin-bottom: 50px;
    margin-top: 76px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 9;

    .icon-arrow {
        width: 14px;
        font-size: 14px;
        display: inline-block;
        margin-right: 4px;

        > i {
            align-self: center;
        }
    }

    .back-text {
        font-size: 14px;
    }
`;

export const PageWrapper = styled.div`
    padding-top: 120px;
    .content {
        margin-top: 40px;
        .entriTitle{
            text-align:right;
            font-size:14px;
        }
        .entriTitle-main{
            .entriTitle-txt{
                p{
                    font-size:14px;
                    color:#474747;
                }
            }
            .entriTitle-input{
                div{
                    textarea{
                        border:1px solid #474747;
                        min-height:106px;
                    }
                }
            }
        }
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;

            h1 {
                font-size: 36px;
                font-weight: 700;
                line-height: 57.6px;
            }
        }

        .submit {
            & > div {
                text-align: right;
                margin-top: 50px;

                & > button {
                    min-width: 160px;
                    margin-bottom: 50px;
                }
            }
        }
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;

export const Footer = styled.div`
    text-align: right;
    padding: 10px 10vw;
    background: #f0f4f6;
    margin-top: 20px;
    position: fixed;
    bottom: 0;
    width: 100%;

    button {
        min-width: 120px;
    }
`;
