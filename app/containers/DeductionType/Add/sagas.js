import { LOCATION_CHANGE } from 'react-router-redux';
import { delay, takeEvery } from 'redux-saga';
import { take, call, put, cancel } from 'redux-saga/effects';
import { ADD_DEDUCTIONTYPE, LOADING, NOTIFICATION, NOTIFICATION_SAGA, SET_ADD_DEDUCTIONTYPE } from './constants';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';
import { browserHistory } from '../../../utils/BrowserHistory';
/**
 *
 * Add DeductionType actions
 *
 */
export function* addDeductionType({ payload }) {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        if ( payload.includes( ',' ) === true ) {
            const resArr = [];
            const split = payload.split( ',' );
            const dataArr = [];
            for ( const data of split ) {
                dataArr.push({ name: data });
            }
            for ( const i of dataArr ) {
                const res = yield call( Fetch, `/philippine/company/${companyId}/deduction_type/is_name_available`, { method: 'POST', data: i });
                resArr.push( res.available );
            }
            if ( resArr.includes( false ) ) {
                yield call( notifyUser, {
                    title: 'Error',
                    message: 'A deduction with the same name already exists',
                    show: true,
                    type: 'error'
                });
            } else {
                yield call( Fetch, `/philippine/company/${companyId}/deduction_type/bulk_create`,
                    {
                        method: 'POST',
                        data: dataArr
                    });
                yield call( notifyUser, {
                    title: 'Success',
                    message: 'Record saved successfully',
                    show: 'true',
                    type: 'success'
                });
                browserHistory.push( '/company-settings/payroll/deduction-types', true );
            }
        } else {
            const res = yield call( Fetch, `/philippine/company/${companyId}/deduction_type/is_name_available`, { method: 'POST', data: { name: payload }});

            if ( res.available === true ) {
                yield call( Fetch, `/philippine/company/${companyId}/deduction_type/bulk_create`, { method: 'POST', data: [{ name: payload }]});

                yield call( notifyUser, {
                    title: 'Success',
                    message: 'Record saved successfully',
                    show: 'true',
                    type: 'success'
                });
                yield call( browserHistory.push( '/company-settings/payroll/deduction-types', true ) );
            } else {
                yield call( notifyUser, {
                    title: 'Error',
                    message: 'Not a valid deduction type name.',
                    show: true,
                    type: 'error'
                });
            }
        }

        yield put({
            type: SET_ADD_DEDUCTIONTYPE,
            payload: true
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: true,
            type: 'error'
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } finally {
        yield put({
            type: SET_ADD_DEDUCTIONTYPE,
            payload: false
        });
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * watcher For Add DeductionType
 */
export function* watchForAddDeductionType() {
    const watcher = yield takeEvery( ADD_DEDUCTIONTYPE, addDeductionType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Notification
 */
export function* watchFroNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForAddDeductionType,
    watchFroNotification
];
