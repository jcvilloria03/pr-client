import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';
import { Container } from 'reactstrap';
import { createStructuredSelector } from 'reselect';

import Sidebar from 'components/Sidebar';
import SnackBar from 'components/SnackBar';
import A from 'components/A';
import { H1 } from 'components/Typography';
import Button from 'components/Button';
import Input from 'components/Input';
import Loader from 'components/Loader';
import Icon from 'components/Icon';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';

import makeSelectAddDeductionType, {
    makeSelectLoading,
    makeSelectNotification,
    makeSelectSetAddDeductionType
} from './selectors';

import * as allActions from './actions';

import {
    PageWrapper,
    NavWrapper,
    Footer
} from './styles';

/**
 *
 * Add Deduction Type
 *
 */
export class AddDeductionType extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
        addDeductionType: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        loading: React.PropTypes.bool
    }

    static defaultProps = {
        loading: false
    };

    constructor( props ) {
        super( props );

        this.state = {
            name: '',
            length: 0
        };
    }

    setValues( value ) {
        this.setState({
            name: value,
            length: value === '' ? 0 : value.split( ',' ).length
        });
    }

    addOnClick() {
        this.props.addDeductionType( this.state.name );
    }
    /**
     *
     * Add render method
     *
     */
    render() {
        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const { notification, loading } = this.props;
        const { name, length } = this.state;

        return (
            <div>
                <div>
                    <Helmet
                        title="Add Deduction Types"
                        meta={ [
                        { name: 'description', content: 'Description of Add Deduction Types' }
                        ] }
                    />
                    <Sidebar items={ sidebarLinks } />
                    <SnackBar
                        message={ notification.message }
                        title={ notification.title }
                        offset={ { top: 70 } }
                        ref={ ( ref ) => { this.notification = ref; } }
                        show={ notification.show }
                        delay={ 5000 }
                        type={ notification.type }
                    />
                    <NavWrapper>
                        <Container>
                            <A
                                href
                                onClick={ ( e ) => {
                                    e.preventDefault();
                                    browserHistory.push( '/company-settings/payroll/deduction-types', true );
                                } }
                            >
                                <Icon name="arrow" className="icon-arrow" /> <span className="back-text">Back to Deduction Types</span>
                            </A>
                        </Container>
                    </NavWrapper>
                    <PageWrapper>
                        <Container>
                            <div className="content">
                                <div className="heading">
                                    <H1>Add Deduction Types</H1>
                                </div>

                                <p className="entriTitle">{length} entries added</p>
                                <div className="w-100">
                                    <div className="row entriTitle-main">
                                        <div className="col-md-2 entriTitle-txt">
                                            <p>Use comma to separate values. Example:<em> Gym Membership, Mobile Plans, Car Parking.</em></p>
                                        </div>
                                        <div className="col-md-10 entriTitle-input">
                                            <Input
                                                id="name"
                                                value={ name }
                                                type="textarea"
                                                onChange={ ( value ) => this.setValues( value ) }
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Container>
                    </PageWrapper>
                </div>
                <Footer>
                    <div className="submit">
                        <div className="col-xs-12">
                            <Button
                                label="Cancel"
                                type="action"
                                size="large"
                                alt
                                onClick={ () => browserHistory.push( '/company-settings/payroll/deduction-types', true ) }
                            />
                            <Button
                                label={ loading ? <Loader /> : 'Submit' }
                                type="action"
                                size="large"
                                onClick={ () => this.addOnClick() }
                                disabled={ name === '' }
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    AddDeductionType: makeSelectAddDeductionType(),
    AddAllDeductionTypes: makeSelectSetAddDeductionType(),
    notification: makeSelectNotification(),
    loading: makeSelectLoading()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        allActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( AddDeductionType );
