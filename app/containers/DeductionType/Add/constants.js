/*
 *
 * Add constants
 *
 */
export const nameSpace = 'app/DeductionType/Add';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const ADD_DEDUCTIONTYPE = `${nameSpace}/ADD_DEDUCTIONTYPE`;
export const SET_ADD_DEDUCTIONTYPE = `${nameSpace}/SET_ADD_DEDUCTIONTYPE`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
export const LOADING = `${nameSpace}/LOADING`;
