import {
    ADD_DEDUCTIONTYPE,
    DEFAULT_ACTION,
    NOTIFICATION
} from './constants';

/**
 *
 * Add actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Add DeductionType action
 *
 */
export function addDeductionType( payload ) {
    return {
        type: ADD_DEDUCTIONTYPE,
        payload
    };
}

/**
 *
 * Notification action
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}

