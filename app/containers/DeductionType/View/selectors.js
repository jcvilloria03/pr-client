import { createSelector } from 'reselect';

/**
 * Direct selector to the deductionType state domain
 */
const selectDeductionTypeDomain = () => ( state ) => state.get( 'deductionType' );

/**
 * Other specific selectors
 */

/**
 * Default selector used by DeductionType
 */

const makeSelectDeductionType = () => createSelector(
  selectDeductionTypeDomain(),
  ( substate ) => substate.toJS()
);

const makeSelectGetDeductionTypes = () => createSelector(
  selectDeductionTypeDomain(),
  ( substate ) => substate.get( 'deductionType' ).toJS()
);

const makeSelectLoading = () => createSelector(
  selectDeductionTypeDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectedTableLoading = () => createSelector(
  selectDeductionTypeDomain(),
  ( substate ) => substate.get( 'tableLoading' )
);

const makeSelectNotification = () => createSelector(
  selectDeductionTypeDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectEditedDeductionTypes = () => createSelector(
  selectDeductionTypeDomain(),
  ( substate ) => substate.get( 'editDeductionType' )
);

export default makeSelectDeductionType;
export {
  selectDeductionTypeDomain,
  makeSelectGetDeductionTypes,
  makeSelectLoading,
  makeSelectedTableLoading,
  makeSelectNotification,
  makeSelectEditedDeductionTypes
};
