/*
 *
 * DeductionType constants
 *
 */
export const nameSpace = 'app/DeductionType/View';

export const DEFAULT_ACTION = `${nameSpace}/DEFAULT_ACTION`;
export const GET_DEDUCTIONTYPE = `${nameSpace}/GET_DEDUCTIONTYPE`;
export const SET_DEDUCTIONTYPE = `${nameSpace}/SET_DEDUCTIONTYPE`;
export const LOADING = `${nameSpace}/LOADING`;
export const TABLE_LOADING = `${nameSpace}/TABLE_LOADING`;
export const DELETE_DEDUCTIONTYPE = `${nameSpace}/DELETE_DEDUCTIONTYPE`;
export const SET_DELETE_DEDUCTIONTYPE = `${nameSpace}/SET_DELETE_DEDUCTIONTYPE`;
export const EDIT_DEDUCTIONTYPE = `${nameSpace}/EDIT_DEDUCTIONTYPE`;
export const SET_EDIT_DEDUCTIONTYPE = `${nameSpace}/SET_EDIT_DEDUCTIONTYPE`;
export const NOTIFICATION = `${nameSpace}/NOTIFICATION`;
export const NOTIFICATION_SAGA = `${nameSpace}/NOTIFICATION_SAGA`;
