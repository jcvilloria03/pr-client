import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { isEqual } from 'lodash';

import FooterTablePaginationV2 from 'components/FooterTablePagination/FooterTablePaginationV2';
import Sidebar from 'components/Sidebar';
import SalDropdown from 'components/SalDropdown';
import Button from 'components/Button';
import Icon from 'components/Icon';
import Table from 'components/Table';
import Input from 'components/Input';
import SnackBar from 'components/SnackBar';
import Modal from 'components/Modal';
import Loader from 'components/Loader';
import { getIdsOfSelectedRows } from 'components/Table/helpers';
import { H3, H5, H2, H1, P } from 'components/Typography';

import { getCompanySettingsSidebarLinks } from 'utils/sidebarHelper';
import { browserHistory } from 'utils/BrowserHistory';
import { formatDeleteLabel, formatPaginationLabel } from 'utils/functions';

import makeSelectDeductionType, {
    makeSelectGetDeductionTypes,
    makeSelectLoading,
    makeSelectedTableLoading,
    makeSelectNotification
} from './selectors';

import * as allActions from './actions';

import {
    PageWrapper,
    LoadingStyles
} from './styles';

/**
 *
 * DeductionType
 *
 */
export class DeductionType extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        listDeductionTypes: React.PropTypes.array,
        getDeductionTypes: React.PropTypes.func,
        editDeductionTypes: React.PropTypes.func,
        loading: React.PropTypes.bool,
        tableLoading: React.PropTypes.bool,
        deleteDeductionTypes: React.PropTypes.func,
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.string,
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        })
    }

    static defaultProps = {
        loading: false
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayedData: [],
            deleteLabel: '',
            show: false,
            showModel: false,
            showButtons: false,
            editedData: '',
            validatedName: 'name',
            nameExists: 'name',
            pagination: {
                total: 0,
                per_page: 10,
                current_page: 1,
                last_page: 0,
                to: 0
            }
        };
    }

    componentDidMount() {
        this.props.getDeductionTypes();
    }

    componentWillReceiveProps( nextProps ) {
        const listChanged = !isEqual( nextProps.listDeductionTypes, this.props.listDeductionTypes );

        this.setState( ( prevState ) => ({
            displayedData: nextProps.listDeductionTypes,
            label: formatPaginationLabel( this.deductionTypeTable.tableComponent.state ),
            pagination: {
                total: nextProps.listDeductionTypes.length,
                per_page: 10,
                current_page: listChanged ? 1 : prevState.pagination.current_page,
                last_page: Math.ceil( nextProps.listDeductionTypes.length / 10 ),
                to: 0
            }
        }) );
    }

    onPageSizeChange = ( pageSize ) => {
        this.setState( ( prevState ) => ({
            pagination: {
                ...prevState.pagination,
                per_page: pageSize,
                current_page: 1,
                last_page: Math.ceil( prevState.pagination.total / pageSize )
            }
        }), () => {
            Object.assign( this.deductionTypeTable.tableComponent.state, {
                page: 0,
                pageSize
            });
            this.handleTableChanges();
        });
    }

    onPageChange = ( page ) => {
        this.setState( ( prevState ) => ({
            page,
            pagination: {
                ...prevState.pagination,
                current_page: page
            }
        }), () => {
            Object.assign( this.deductionTypeTable.tableComponent.state, {
                page: page - 1
            });
            this.handleTableChanges();
        });
    }

    handleTableChanges = ( tableProps = this.deductionTypeTable.tableComponent.state ) => {
        this.setState({
            label: formatPaginationLabel( tableProps ),
            deleteLabel: formatDeleteLabel()
        });
    }

    handleSearch = ( term = '' ) => {
        let displayData = this.props.listDeductionTypes;

        if ( term ) {
            const regex = new RegExp( term, 'i' );
            displayData = displayData.filter( ( row ) => ( regex.test( row.name ) ) );
        }

        this.setState({ displayedData: displayData }, () => {
            this.handleTableChanges();
        });
    }

    deleteList = () => {
        this.DeleteModel.toggle();
        const payload = getIdsOfSelectedRows( this.deductionTypeTable );
        this.props.deleteDeductionTypes( payload );
        this.setState({ show: false, showModel: false });
    }

    showTextBox( row ) {
        return this.state.editedData.id === row.id;
    }

    editDeductionType() {
        if ( this.validateName() ) {
            this.props.editDeductionTypes({
                data: this.state.editedData,
                clone: this.props.listDeductionTypes
            });
            this.setState({ showButtons: false });
        }
    }

    validateName = () => {
        let valid = true;
        const name = this.props.listDeductionTypes.filter( ( arr ) => arr.id !== this.state.editedData.id );
        if ( this.state.editedData.name === '' ) {
            this.setState({
                validatedName: ''
            });
            valid = false;
        } else if ( name.map( ( value ) => value.name ).includes( this.state.editedData.name ) ) {
            this.setState({
                nameExists: ''
            });
            valid = false;
        }
        return valid;
    }

    /**
     *
     * DeductionType render method
     *
     */
    render() {
        const tableColumn = [
            {
                id: 'name',
                header: 'Deduction Types',
                sortable: true,
                render: ({ row }) => (
                    this.showTextBox( row ) && this.state.showButtons
                    ? <div style={ { width: '100%' } }>
                        <Input
                            className={ this.state.validatedName === '' ? 'input-rt-td-error' : this.state.nameExists === '' ? 'input-rt-td-error' : 'input-rt-td' }
                            id="name" type="text"
                            value={ this.state.editedData.name }
                            onChange={ ( eve ) => this.setState({
                                validatedName: 'name',
                                nameExists: 'name',
                                editedData: { ...this.state.editedData, name: eve }
                            }) }
                        />
                        {this.state.validatedName === '' ? <p className="field-warn">Name is required</p> : this.state.nameExists === '' ? <p className="field-warn">The name has already been taken.</p> : ''}
                    </div>
                    : <div>{row.name}</div>
                )
            },
            {
                id: 'name',
                header: '',
                sortable: false,
                width: 200,
                style: { justifyContent: 'right' },
                render: ({ row }) => (
                    this.showTextBox( row ) && this.state.showButtons
                    ? <div>
                        <Button
                            label={ <span>Cancel</span> }
                            type="neutral"
                            size="small"
                            onClick={ () => this.setState({ showButtons: false, validatedName: 'name', nameExists: 'name' }) }
                        ></Button>
                        <Button
                            label={ <span>Save</span> }
                            type="action"
                            size="small"
                            onClick={ () => this.editDeductionType() }
                        ></Button>
                    </div>
                    : <Button
                        label={ this.props.tableLoading && row.id === this.state.editedData.id ? <Loader /> : 'Edit' }
                        type="grey"
                        size="small"
                        onClick={ () => this.setState({ showButtons: true, editedData: row }) }
                    ></Button>
                )
            }
        ];

        const sidebarLinks = getCompanySettingsSidebarLinks({
            isSubscribedToPayroll: false,
            isSubscribedToTA: false
        });

        const { loading, notification } = this.props;
        const { displayedData, show, deleteLabel, label } = this.state;

        return (
            <div>
                <Helmet
                    title="Deduction Types"
                    meta={ [
                        { name: 'description', content: 'Description of Deduction Types' }
                    ] }
                />
                <Sidebar items={ sidebarLinks } />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <Modal
                    title="Confirm Your Action"
                    className="modal-md modal-commission-type"
                    ref={ ( ref ) => {
                        this.DeleteModel = ref;
                    } }
                    center
                    showClose={ false }
                    body={ (
                        <div>
                            <p>Proceed in removing the record?</p>
                        </div>
                    ) }
                    buttons={ [
                        {
                            type: 'grey',
                            label: 'No',
                            onClick: () => this.DeleteModel.toggle()
                        },
                        {
                            type: 'darkRed',
                            label: 'Yes',
                            onClick: this.deleteList
                        }
                    ] }
                />
                <PageWrapper>
                    <Container>
                        <div className="content">
                            <div className="loader" style={ { display: loading ? '' : 'none' } }>
                                <LoadingStyles>
                                    <H2>Loading Deduction Types</H2>
                                    <br />
                                    <H3>Please wait...</H3>
                                </LoadingStyles>
                            </div>
                            <div className="content" style={ { display: loading ? 'none' : '' } }>
                                <div className="heading">
                                    <H1 noBottomMargin>Deduction Types</H1>

                                    <div className="buttons-wrapper">
                                        <Button
                                            label="Add Deductions"
                                            type="neutral"
                                            onClick={ () => browserHistory.push( '/payroll/deductions/add', true ) }
                                        />
                                        <Button
                                            label="Add Deduction Types"
                                            type="action"
                                            onClick={ () => browserHistory.push( '/company-settings/payroll/deduction-types/add', true ) }
                                        />
                                    </div>
                                </div>
                                <div className="title">
                                    <div className="search-wrapper">
                                        <H5 noBottomMargin>Deduction Types List</H5>

                                        <Input
                                            id="search"
                                            className="search"
                                            onChange={ ( value ) => this.handleSearch( value.toLowerCase() ) }
                                            addon={ {
                                                content: <Icon name="search" />,
                                                placement: 'right'
                                            } }
                                            ref={ ( ref ) => { this.searchInput = ref; } }
                                        />
                                    </div>
                                    <div className="actions-wrapper">
                                        <P noBottomMargin>{show ? deleteLabel : label}</P>

                                        {show && (
                                            <span style={ { marginLeft: '14px' } }>
                                                <SalDropdown
                                                    dropdownItems={ [
                                                        {
                                                            label: 'Delete',
                                                            children: <div>Delete</div>,
                                                            onClick: () => this.DeleteModel.toggle()
                                                        }
                                                    ] }
                                                />
                                            </span>
                                        )}
                                    </div>
                                </div>

                                <Table
                                    className="table-title"
                                    data={ displayedData }
                                    columns={ tableColumn }
                                    onDataChange={ this.handleTableChanges }
                                    selectable
                                    onSelectionChange={ ({ selected }) => {
                                        const selectionLength = selected.filter( ( row ) => row ).length;
                                        this.setState({
                                            show: selectionLength > 0,
                                            deleteLabel: formatDeleteLabel( selectionLength )
                                        });
                                    } }
                                    ref={ ( ref ) => { this.deductionTypeTable = ref; } }
                                />

                                <div>
                                    <FooterTablePaginationV2
                                        page={ this.state.pagination.current_page }
                                        pageSize={ this.state.pagination.per_page }
                                        pagination={ this.state.pagination }
                                        onPageChange={ this.onPageChange }
                                        onPageSizeChange={ this.onPageSizeChange }
                                        paginationLabel={ this.state.label }
                                        fluid
                                    />
                                </div>
                            </div>
                        </div>
                    </Container>
                </PageWrapper>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    DeductionType: makeSelectDeductionType(),
    listDeductionTypes: makeSelectGetDeductionTypes(),
    loading: makeSelectLoading(),
    tableLoading: makeSelectedTableLoading(),
    notification: makeSelectNotification()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        allActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( DeductionType );
