import {
    DEFAULT_ACTION,
    DELETE_DEDUCTIONTYPE,
    EDIT_DEDUCTIONTYPE,
    GET_DEDUCTIONTYPE,
    NOTIFICATION
} from './constants';

/**
 *
 * DeductionType actions
 *
 */
export function defaultAction() {
    return {
        type: DEFAULT_ACTION
    };
}

/**
 *
 * Get DeductionTypes actions
 *
 */
export function getDeductionTypes() {
    return {
        type: GET_DEDUCTIONTYPE
    };
}

/**
 *
 * Delete DeductionTypes actions
 *
 */
export function deleteDeductionTypes( payload ) {
    return {
        type: DELETE_DEDUCTIONTYPE,
        payload
    };
}

/**
 *
 * Edit DeductionTypes actions
 *
 */
export function editDeductionTypes( payload ) {
    return {
        type: EDIT_DEDUCTIONTYPE,
        payload
    };
}

/**
 *
 * Notification action
 *
 */
export function notify( show, title = '', message = '', type = 'success' ) {
    return {
        type: NOTIFICATION,
        payload: {
            show,
            title,
            message,
            type
        }
    };
}
