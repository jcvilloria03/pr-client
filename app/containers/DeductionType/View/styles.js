import styled from 'styled-components';

export const PageWrapper = styled.div`
    .content {
        margin-top: 110px;
        .field-warn{
            color: #EB7575;
        }
        .ReactTable{
            .rt-tbody{
                .selected{
                    background-color: rgba(131, 210, 75, 0.15);
                    &:hover{
                        background-color: rgba(131, 210, 75, 0.15) !important;
                    }
                }
            }
        }
        .heading {
            display: flex;
            align-items: center;
            flex-direction: column;
            margin: 0 auto 50px auto;

            h1 {
                font-size: 36px;
                font-weight: 700;
                line-height: 57.6px;
            }

            p {
                text-align: center;
                max-width: 800px;
                font-size: 14px;
                line-height: 22.4px;
            }

            .buttons-wrapper {
                display: flex;
                flex-grow: 1;
                align-items: center;
                justify-content: center;
                margin-top: 28px;

                button {
                    margin: 0 5px;
                    border-radius: 28px;
                    min-width: 200px;
                }
            }
        }

        .title {
            display: flex;
            align-items: center;
            margin-bottom: 14px;

            .search-wrapper {
                display: flex;
                align-items: center;
                flex-direction: row;

                h5 {
                    margin: 0;
                    margin-right: 28px;
                    font-weight: 600;
                    font-size: 18px;
                    line-height: 28.8px;
                }

                .search {
                    width: 200px;
                    border: 1px solid #333;
                    border-radius: 28px;
                    margin-right: 3px;

                    input, .input-group {
                        height: 42px;
                    }

                    .input-group {
                      height: 42px;

                        input {
                            height: 42px;
                            border: none;
                        }

                        .isvg {
                            transform: translateY(-2px);
                            width:14px !important;
                        }
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }

            .actions-wrapper {
                display: flex;
                flex-grow: 1;
                align-items: center;
                justify-content: flex-end;

                .dropdown {
                    span {
                        border-radius: 2px;
                    }
                }

                span{
                    font-size: 14px;
                }

                .dropdown,
                .dropdown-item {
                    font-size: 14px;
                }

                .dropdown-menu {
                    min-width: 122px;
                }
            }
        }

        .table-title{
            position:relative;
            z-index:0;
            .ReactTable{
                .rt-table{
                    .rt-tbody{
                        .rt-tr-group{
                            .rt-tr{
                                .rt-td{
                                    .input-rt-td{
                                        width:62%;
                                        height:42px;
                                        input{
                                            border: 1px solid #474747;
                                            color: black;
                                        }
                                    }
                                    .input-rt-td-error{
                                        width:62%;
                                        height:42px;
                                        input{
                                            border: 1px solid #EB7575;
                                            color: black;
                                        }
                                    }
                                }
                            }
                        }
                    }       
                }
            }
        }
    }

    a {
        padding: 4px 10px;
        color: #00A5E5;
        text-decoration: none !important;
    }

    .hide {
        display: none;
    }

    .bullet-red {
        color: #eb7575;
    }

    .bullet-green {
        color: #9fdc74;
    }

    .filter-icon > svg {
        height: 10px;
    }

    .no-license-error {
        color: #e13232;
    }
`;
export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;
    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;
        > i {
            align-self: center;
        }
    }
    .message {
        display: flex;
        align-self: center;
    }
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;
