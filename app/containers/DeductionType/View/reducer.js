import { fromJS } from 'immutable';
import {
    DEFAULT_ACTION,
    LOADING,
    TABLE_LOADING,
    NOTIFICATION_SAGA,
    SET_DEDUCTIONTYPE,
    SET_DELETE_DEDUCTIONTYPE,
    SET_EDIT_DEDUCTIONTYPE
} from './constants';

const initialState = fromJS({
    deductionType: [],
    loading: false,
    tableLoading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'success'
    }
});

/**
 *
 * DeductionType reducer
 *
 */
function deductionTypeReducer( state = initialState, action ) {
    switch ( action.type ) {
        case DEFAULT_ACTION:
            return state;
        case SET_DEDUCTIONTYPE:
            return state.set( 'deductionType', fromJS( action.payload ) );
        case LOADING:
            return state.set( 'loading', action.payload );
        case TABLE_LOADING:
            return state.set( 'tableLoading', action.payload );
        case NOTIFICATION_SAGA:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_DELETE_DEDUCTIONTYPE:
            return state.set( action.payload );
        case SET_EDIT_DEDUCTIONTYPE:
            return state.set( action.payload );
        default:
            return state;
    }
}

export default deductionTypeReducer;
