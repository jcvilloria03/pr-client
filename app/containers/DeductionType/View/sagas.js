import { take, call, put, cancel } from 'redux-saga/effects';
import { delay, takeEvery, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';

import { DELETE_DEDUCTIONTYPE, EDIT_DEDUCTIONTYPE, GET_DEDUCTIONTYPE, LOADING, TABLE_LOADING, NOTIFICATION, NOTIFICATION_SAGA, SET_DEDUCTIONTYPE, SET_DELETE_DEDUCTIONTYPE, SET_EDIT_DEDUCTIONTYPE } from './constants';

import { Fetch } from '../../../utils/request';
import { company } from '../../../utils/CompanyService';

import { REINITIALIZE_PAGE } from '../../App/constants';

/**
 * GET Deduction Types
 */
export function* getDeductionType() {
    try {
        yield put({
            type: LOADING,
            payload: true
        });
        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/other_income_types/deduction_type?exclude_trashed=1`, { method: 'GET' });

        yield put({
            type: SET_DEDUCTIONTYPE,
            payload: res.data || []
        });
        yield put({
            type: LOADING,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: LOADING,
            payload: false
        });
    }
}

/**
 * Reinitialize Page
 */
export function* reinitializePage() {
    yield call( getDeductionType );
}

/**
 * Delete Deduction type
 */
export function* deleteDeductionType({ payload }) {
    try {
        yield put({
            type: SET_DELETE_DEDUCTIONTYPE,
            payload: true
        });

        const companyId = company.getLastActiveCompanyId();
        const res = yield call( Fetch, `/company/${companyId}/other_income_type/is_delete_available`, { method: 'POST', data: { ids: payload }});

        if ( res.available === true ) {
            const formData = new FormData();
            formData.append( '_method', 'DELETE' );
            formData.append( 'company_id', companyId );
            for ( const i of payload ) {
                formData.append( 'ids[]', i );
            }
            yield call( Fetch, `/company/${companyId}/other_income_type`, { method: 'POST', data: formData });
        }

        yield call( getDeductionType );

        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully deleted!',
            show: true,
            type: 'success'
        });

        yield put({
            type: SET_DELETE_DEDUCTIONTYPE,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: SET_DELETE_DEDUCTIONTYPE,
            payload: false
        });
    }
}

/**
 * Edit Deduction type
 */
export function* editDeductionType({ payload }) {
    try {
        yield put({
            type: SET_EDIT_DEDUCTIONTYPE,
            payload: true
        });
        yield put({
            type: TABLE_LOADING,
            payload: true
        });

        const response = yield call( Fetch, `/philippine/deduction_type/${payload.data.id}`, { method: 'PATCH', data: payload.data });

        const deductionListClone = payload.clone;
        const objIndex = deductionListClone.findIndex( ( ( type ) => type.id === response.id ) );

        deductionListClone[ objIndex ].name = response.name;

        yield put({
            type: SET_DEDUCTIONTYPE,
            payload: deductionListClone
        });

        yield call( notifyUser, {
            title: 'Success',
            message: 'Record successfully edited!',
            show: true,
            type: 'success'
        });

        yield put({
            type: SET_EDIT_DEDUCTIONTYPE,
            payload: false
        });
    } catch ( error ) {
        yield call( notifyUser, {
            title: 'Error',
            message: error.response.data.message || 'Something went wrong',
            show: true,
            type: 'error'
        });
    } finally {
        yield put({
            type: TABLE_LOADING,
            payload: false
        });
    }
}

/**
 * Notify user
 */
export function* notifyUser( payload ) {
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });

    yield put({
        type: NOTIFICATION_SAGA,
        payload
    });

    yield call( delay, 2000 );
    yield put({
        type: NOTIFICATION_SAGA,
        payload: {
            title: ' ',
            message: ' ',
            show: false,
            type: payload.type
        }
    });
}

/**
 * Individual exports for testing
 */
export function* defaultSaga() {
    // Refer to samples
}

/**
 * Watcher for GET Deduction types
 */
export function* watchForGetDeductionTypes() {
    const watcher = yield takeEvery( GET_DEDUCTIONTYPE, getDeductionType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For ReinitializePage
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For Delete Deduction types
 */
export function* watchForDeleteDeductionType() {
    const watcher = yield takeEvery( DELETE_DEDUCTIONTYPE, deleteDeductionType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher For Edit Deduction types
 */
export function* watchForEditDeductionType() {
    const watcher = yield takeEvery( EDIT_DEDUCTIONTYPE, editDeductionType );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for Notification
 */
export function* watchFroNotification() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    defaultSaga,
    watchForGetDeductionTypes,
    watchForReinitializePage,
    watchForDeleteDeductionType,
    watchFroNotification,
    watchForEditDeductionType
];
