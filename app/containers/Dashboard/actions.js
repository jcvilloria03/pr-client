import {
    INITIALIZE,
    REGENERATE,
    RESET_STORE,
    EXPORT_EMPLOYEE,
    GET_STATISTICS,
    GET_ANNOUNCEMENTS,
    GET_CALENDAR_DAY_VIEW,
    GET_CALENDAR_MONTH_VIEW,
    GET_EMPLOYEE_TABLE_DATA,
    GET_SHIFT_EMPLOYEES,
    SET_SHIFT_EMPLOYEES_PAGINATION,
    SET_SHIFT_EMPLOYEES,
    SET_LOADING,
    SET_PAGINATION,
    SET_STATISTICS,
    SET_NOTIFICATION,
    SET_ANNOUNCEMENTS,
    SET_CALENDAR_DAY_DATA,
    SET_REGENERATE_LOADING,
    SET_CALENDAR_MONTH_DATA,
    SET_EMPLOYEE_TABLE_DATA,
    SET_ANNOUNCEMENT_LOADING,
    SET_ANNOUNCEMENTS_PAGINATION,
    CREATE_ANNOUNCEMENTS
} from './constants';

/**
 * initialize page
 */
export function initializePage() {
    return {
        type: INITIALIZE
    };
}

/**
 * initialize page
 */
export function regenerate() {
    return {
        type: REGENERATE
    };
}

/**
 * export data
 */
export function exportCsv( payload ) {
    return {
        type: EXPORT_EMPLOYEE,
        payload
    };
}

/**
 * Sends request to fetch employees Statistics
 */
export function getStatistics() {
    return {
        type: GET_STATISTICS
    };
}

/**
 * Sends request to fetch calendar day view data
 */
export function getCalendarDayData( payload ) {
    return {
        type: GET_CALENDAR_DAY_VIEW,
        payload
    };
}

/**
 * Sends request to fetch calendar month view data
 */
export function getCalendarMonthData( payload ) {
    return {
        type: GET_CALENDAR_MONTH_VIEW,
        payload
    };
}

/**
 * Sends request to fetch employees table data
 * @param {Array} payload - pagination, table details
 *
 */
export function getEmployeeTableData( payload ) {
    return {
        type: GET_EMPLOYEE_TABLE_DATA,
        payload
    };
}

/**
 * display a notification in page
 */
export function getShiftEmployees( employeeIds = [],
    page = 1, pageSize = 10, orderBy = 'last_name', orderDir = 'asc', searchKeyword = '' ) {
    return {
        type: GET_SHIFT_EMPLOYEES,
        payload: {
            employeeIds,
            page,
            perPage: pageSize,
            orderBy,
            orderDir,
            searchKeyword
        }
    };
}

/**
 * Sets shift employees pagination
 * @param {Boolean} payload - pagination details
 * @returns {Object} action
 */
export function setShiftEmployeesPagination( payload ) {
    return {
        type: SET_SHIFT_EMPLOYEES_PAGINATION,
        payload
    };
}

/**
 * Sets shift employees table data
 */
export function setShiftEmployees( payload ) {
    return {
        type: SET_SHIFT_EMPLOYEES,
        payload
    };
}

/**
 * Sends request to fetch announcements
 * @param {Array} payload - pagination, table details
 *
 */
export function getAnnouncements( payload ) {
    return {
        type: GET_ANNOUNCEMENTS,
        payload
    };
}

/**
 * Sets pagination
 * @param {Boolean} payload - pagination details
 * @returns {Object} action
 */
export function setPagination( payload ) {
    return {
        type: SET_PAGINATION,
        payload
    };
}

/**
 * Sets pagination for announcements
 * @param {Boolean} payload - pagination details
 * @returns {Object} action
 */
export function setAnnouncementsPagination( payload ) {
    return {
        type: SET_ANNOUNCEMENTS_PAGINATION,
        payload
    };
}

/**
 * Sets Employees Statistics
 * @param {Array} payload - Statistics
 * @returns {Object}
 */
export function setStatistics( payload ) {
    return {
        type: SET_STATISTICS,
        payload
    };
}

/**
 * Sets Calendar day data
 * @param {Object} payload
 * @returns {Object}
 */
export function setCalendarDayData( payload ) {
    return {
        type: SET_CALENDAR_DAY_DATA,
        payload
    };
}

/**
 * Sets Calendar month data
 * @param {Object} payload - data
 * @returns {Object}
 */
export function setCalendarMonthData( payload ) {
    return {
        type: SET_CALENDAR_MONTH_DATA,
        payload
    };
}

/**
 * Sets employees table data
 */
export function setEmployeeTableData( payload ) {
    return {
        type: SET_EMPLOYEE_TABLE_DATA,
        payload
    };
}

/**
 * Create announcements
 * @param {Array} payload
 * @returns {Object}
 */
export function createAnnouncements( payload ) {
    return {
        type: CREATE_ANNOUNCEMENTS,
        payload
    };
}

/**
 * Sets announcements
 * @param {Array} payload - announcements
 * @returns {Object}
 */
export function setAnnouncements( payload ) {
    return {
        type: SET_ANNOUNCEMENTS,
        payload
    };
}

/**
 * Sets page loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object}
 */
export function setLoading( payload ) {
    return {
        type: SET_LOADING,
        payload
    };
}

/**
 * Sets Regenerate loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object}
 */
export function setRegenerateLoading( payload ) {
    return {
        type: SET_REGENERATE_LOADING,
        payload
    };
}

/**
 * Sets Announcement loading status
 * @param {Boolean} payload - Loading status
 * @returns {Object}
 */
export function setAnnouncementLoading( payload ) {
    return {
        type: SET_ANNOUNCEMENT_LOADING,
        payload
    };
}

/**
 * Sets notification
 * @param {Object} payload - Notification config
 * @returns {Object}
 */
export function setNotification( payload ) {
    return {
        type: SET_NOTIFICATION,
        payload
    };
}

/**
 *
 * Dashboard actions
 *
 */
export function resetStore() {
    return {
        type: RESET_STORE
    };
}
