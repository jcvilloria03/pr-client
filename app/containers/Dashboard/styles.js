import styled from 'styled-components';
import { Container } from 'reactstrap';

export const PageWrapper = styled.div`
.selected .fill {
    fill: #fff;
}

.selected .stroke {
    stroke: #fff;
}

.fill {
    fill: rgb(0,165,226);
}

.stroke {
    stroke: rgb(0,165,226);
}
`;

export const StyledContainer = styled( Container )`
    min-height: calc(100vh - 80px);
    background: #fff;
    padding: 80px 0 20px 0;
`;

export const LoadingStyles = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 200px;
    justify-content: center;
    padding: 140px 0;
`;
