/* eslint-disable import/no-named-as-default-member */
import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import SnackBar from 'components/SnackBar';

import StatisticsSection from './sections/statistics';
import CalendarSection from './sections/calendar';
import AnnouncementsSection from './sections/announcements';

import * as dashboardActions from './actions';

import {
    PageWrapper,
    StyledContainer
} from './styles';

import {
    makeSelectLoading,
    makeSelectPagination,
    makeSelectStatistics,
    makeSelectDayCalendar,
    makeSelectNotification,
    makeSelectMonthCalendar,
    makeSelectStatTableData,
    makeSelectAnnouncements,
    makeSelectRegenerateLoading,
    makeSelectAnnouncementLoading,
    makeSelectAnnouncementsPagination,
    makeSelectShiftEmployeesPagination,
    makeSelectShiftEmployees
} from './selectors';
/**
 *
 * Dashboard
 *
 */
export class Dashboard extends React.PureComponent {
    static propTypes = {
        initializePage: React.PropTypes.func,
        getCalendarDayData: React.PropTypes.func,
        getCalendarMonthData: React.PropTypes.func,
        getAnnouncements: React.PropTypes.func,
        createAnnouncements: React.PropTypes.func,
        regenerate: React.PropTypes.func,
        exportCsv: React.PropTypes.func,
        getEmployeeTableData: React.PropTypes.func,
        loading: React.PropTypes.bool,
        regenerateLoading: React.PropTypes.bool,
        announcementLoading: React.PropTypes.bool,
        day: React.PropTypes.shape({
            default_schedule: React.PropTypes.array,
            calendar_Data: React.PropTypes.array
        }),
        month: React.PropTypes.shape({
            holidays: React.PropTypes.array,
            events: React.PropTypes.array
        }),
        notification: React.PropTypes.shape({
            title: React.PropTypes.string,
            message: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node,
                React.PropTypes.symbol
            ]),
            show: React.PropTypes.bool,
            type: React.PropTypes.string
        }),
        announcementsPagination: React.PropTypes.shape({
            from: React.PropTypes.number,
            to: React.PropTypes.number,
            total: React.PropTypes.number,
            current_page: React.PropTypes.number,
            total_pages: React.PropTypes.number,
            per_page: React.PropTypes.number
        }),
        pagination: React.PropTypes.shape({
            item_count: React.PropTypes.number,
            page: React.PropTypes.number,
            total_pages: React.PropTypes.number,
            total_records: React.PropTypes.number
        }),
        statistics: React.PropTypes.shape({
            attendance: React.PropTypes.object,
            activeEmployees: React.PropTypes.number
        }),
        statTableData: React.PropTypes.array,
        announcements: React.PropTypes.array,
        shiftEmployeesPagination: React.PropTypes.shape({
            from: React.PropTypes.number,
            to: React.PropTypes.number,
            total: React.PropTypes.number,
            current_page: React.PropTypes.number,
            total_pages: React.PropTypes.number,
            per_page: React.PropTypes.number
        }),
        shiftEmployees: React.PropTypes.array,
        getShiftEmployees: React.PropTypes.func
    }

    constructor( props ) {
        super( props );

        this.state = {
        };
    }

    componentDidMount() {
        this.props.initializePage();
    }

    /**
     *
     * Dashboard render method
     *
     */
    render() {
        const {
          day,
          month,
          loading,
          exportCsv,
          regenerate,
          statistics,
          pagination,
          notification,
          statTableData,
          announcements,
          getAnnouncements,
          regenerateLoading,
          getCalendarDayData,
          announcementLoading,
          createAnnouncements,
          getEmployeeTableData,
          getCalendarMonthData,
          announcementsPagination,
          shiftEmployeesPagination,
          shiftEmployees,
          getShiftEmployees
        } = this.props;

        return (
            <PageWrapper>
                <Helmet
                    title="Dashboard"
                    meta={ [
                        { name: 'description', content: 'Description of Dashboard' }
                    ] }
                />
                <SnackBar
                    message={ notification.message }
                    title={ notification.title }
                    offset={ { top: 70 } }
                    ref={ ( ref ) => { this.notification = ref; } }
                    show={ notification.show }
                    delay={ 5000 }
                    type={ notification.type }
                />
                <StyledContainer>
                    <div>
                        <StatisticsSection
                            pagination={ pagination }
                            regenerate={ regenerate }
                            statistics={ statistics }
                            loading={ regenerateLoading }
                            statTableData={ statTableData }
                            exportCsv={ exportCsv }
                            fetchTable={ ( data ) => {
                                getEmployeeTableData( data );
                            } }
                        />
                        <CalendarSection
                            dashboardLoading={ loading }
                            dayData={ day }
                            monthData={ month }
                            getCalendarDayData={ ( data ) => getCalendarDayData( data ) }
                            getCalendarMonthData={ ( data ) => getCalendarMonthData( data ) }
                            shiftEmployeesPagination={ shiftEmployeesPagination }
                            shiftEmployees={ shiftEmployees }
                            getShiftEmployees={ getShiftEmployees }
                        />
                        <AnnouncementsSection
                            loading={ announcementLoading }
                            pagination={ announcementsPagination }
                            announcements={ announcements }
                            getAnnouncements={ ( data ) => { getAnnouncements( data ); } }
                            createAnnouncements={ ( data ) => { createAnnouncements( data ); } }
                        />
                    </div>
                </StyledContainer>
            </PageWrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    day: makeSelectDayCalendar(),
    loading: makeSelectLoading(),
    regenerateLoading: makeSelectRegenerateLoading(),
    announcementLoading: makeSelectAnnouncementLoading(),
    month: makeSelectMonthCalendar(),
    statistics: makeSelectStatistics(),
    pagination: makeSelectPagination(),
    notification: makeSelectNotification(),
    statTableData: makeSelectStatTableData(),
    announcements: makeSelectAnnouncements(),
    announcementsPagination: makeSelectAnnouncementsPagination(),
    shiftEmployeesPagination: makeSelectShiftEmployeesPagination(),
    shiftEmployees: makeSelectShiftEmployees()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators(
        dashboardActions,
        dispatch
    );
}

export default connect( mapStateToProps, mapDispatchToProps )( Dashboard );
