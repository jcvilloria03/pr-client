/*
 *
 * Dashboard constants
 *
 */

export const LOADING = 'app/Dashboard/LOADING';
export const INITIALIZE = 'app/Dashboard/INITIALIZE';
export const REGENERATE = 'app/Dashboard/REGENERATE';
export const RESET_STORE = 'app/Dashboard/RESET_STORE';
export const NOTIFICATION = 'app/Dashboard/NOTIFICATION';
export const EXPORT_EMPLOYEE = 'app/Dashboard/EXPORT_EMPLOYEE';

export const SET_LOADING = 'app/Dashboard/SET_LOADING';
export const SET_REGENERATE_LOADING = 'app/Dashboard/SET_REGENERATE_LOADING';
export const SET_ANNOUNCEMENT_LOADING = 'app/Dashboard/SET_ANNOUNCEMENT_LOADING';
export const SET_CALENDAR_DAY_DATA = 'app/Dashboard/SET_CALENDAR_DAY_DATA';
export const SET_CALENDAR_MONTH_DATA = 'app/Dashboard/SET_CALENDAR_MONTH_DATA';
export const SET_PAGINATION = 'app/Dashboard/SET_PAGINATION';
export const SET_STATISTICS = 'app/Dashboard/SET_STATISTICS';
export const SET_NOTIFICATION = 'app/Dashboard/SET_NOTIFICATION';
export const SET_ANNOUNCEMENTS = 'app/Dashboard/SET_ANNOUNCEMENTS';
export const SET_TABLE_PAGINATION = 'app/Employees/SET_TABLE_PAGINATION';
export const SET_EMPLOYEE_TABLE_DATA = 'app/Dashboard/SET_EMPLOYEE_TABLE_DATA';
export const SET_ANNOUNCEMENTS_PAGINATION = 'app/Dashboard/SET_ANNOUNCEMENTS_PAGINATION';
export const SET_SHIFT_EMPLOYEES_PAGINATION = 'app/Dashboard/SET_SHIFT_EMPLOYEES_PAGINATION';
export const SET_SHIFT_EMPLOYEES = 'app/Dashboard/SET_SHIFT_EMPLOYEES';

export const GET_STATISTICS = 'app/Dashboard/GET_STATISTICS';
export const GET_CALENDAR_DAY_VIEW = 'app/Dashboard/GET_CALENDAR_DAY_VIEW';
export const GET_CALENDAR_MONTH_VIEW = 'app/Dashboard/GET_CALENDAR_MONTH_VIEW';
export const GET_ANNOUNCEMENTS = 'app/Dashboard/GET_ANNOUNCEMENTS';
export const GET_EMPLOYEE_TABLE_DATA = 'app/Dashboard/GET_EMPLOYEE_TABLE_DATA';
export const GET_SHIFT_EMPLOYEES = 'app/Dashboard/GET_SHIFT_EMPLOYEES';

export const CREATE_ANNOUNCEMENTS = 'app/Dashboard/CREATE_ANNOUNCEMENTS';

export const SELECT_ALL_EMPLOYEES_OPTIONS = [
    {
        id: null,
        type: 'department',
        uid: 'all_department',
        value: 'department',
        field: 'department',
        label: 'All departments',
        disabled: false
    },
    {
        id: null,
        type: 'position',
        uid: 'all_position',
        value: 'position',
        field: 'position',
        label: 'All positions',
        disabled: false
    },
    {
        id: null,
        type: 'location',
        uid: 'all_location',
        value: 'location',
        field: 'location',
        label: 'All locations',
        disabled: false
    },
    {
        id: null,
        type: 'employee',
        uid: 'all_employee',
        value: 'employee',
        field: 'employee',
        label: 'All employee',
        disabled: false
    }
];

export const PRESENT_TABLE_COLUMNS = [
    {
        header: 'id',
        accessor: 'id',
        show: false
    },
    {
        id: 'employee_name',
        header: 'Employee Name',
        accessor: ( row ) => row.employee_name,
        minWidth: 150
    },
    {
        id: 'shift_name',
        header: 'Shift',
        accessor: ( row ) => row.shift_name,
        sortable: false
    },
    {
        id: 'clock_in',
        header: 'Clock In',
        accessor: ( row ) => row.clock_in || 'N/A',
        sortable: false
    },
    {
        id: 'clock_out',
        header: 'Clock Out',
        accessor: ( row ) => row.clock_out || 'N/A',
        sortable: false
    },
    {
        id: 'remarks',
        header: 'Remarks',
        accessor: ( row ) => row.remarks,
        sortable: false
    }
];

export const LEAVE_TABLE_COLUMNS = [
    {
        header: 'id',
        accessor: 'id',
        show: false
    },
    {
        id: 'employee_name',
        header: 'Employee Name',
        accessor: ( row ) => row.employee_name,
        minWidth: 150
    },
    {
        id: 'shift_name',
        header: 'Shift',
        accessor: ( row ) => row.shift_name,
        minWidth: 150
    },
    {
        id: 'leave_start',
        header: 'Start Leave',
        accessor: ( row ) => row.leave_start
    },
    {
        id: 'leave_end',
        accessor: ( row ) => row.leave_end,
        header: 'End Leave'
    }
];

export const LATE_TABLE_COLUMNS = [
    {
        header: 'id',
        accessor: 'id',
        show: false
    },
    {
        id: 'employee_name',
        header: 'Employee Name',
        accessor: ( row ) => row.employee_name,
        minWidth: 150
    },
    {
        id: 'shift_name',
        header: 'Shift',
        accessor: ( row ) => row.shift_name,
        minWidth: 150
    },
    {
        id: 'clock_in',
        header: 'Clock In',
        accessor: ( row ) => row.clock_in || 'N/A'
    },
    {
        id: 'remarks',
        accessor: ( row ) => row.remarks,
        header: 'Remarks'
    }
];

export const ABSENT_TABLE_COLUMNS = [
    {
        header: 'id',
        accessor: 'id',
        show: false
    },
    {
        id: 'employee_name',
        header: 'Employee Name',
        accessor: ( row ) => row.employee_name,
        minWidth: 150
    },
    {
        id: 'shift_name',
        header: 'Shift',
        accessor: ( row ) => row.shift_name,
        minWidth: 150
    },
    {
        id: 'clock_in',
        header: 'Clock In',
        accessor: ( row ) => row.clock_in || 'N/A'
    },
    {
        id: 'clock_out',
        header: 'Clock Out',
        accessor: ( row ) => row.clock_out || 'N/A'
    },
    {
        id: 'remarks',
        accessor: ( row ) => row.remarks,
        header: 'Remarks'
    }
];

