import { createSelector } from 'reselect';

/**
 * Direct selector to the dashboard state domain
 */
const selectDashboardDomain = () => ( state ) => state.get( 'dashboard' );

const makeSelectLoading = () => createSelector(
    selectDashboardDomain(),
  ( substate ) => substate.get( 'loading' )
);

const makeSelectRegenerateLoading = () => createSelector(
    selectDashboardDomain(),
  ( substate ) => substate.get( 'regenerateLoading' )
);

const makeSelectAnnouncementLoading = () => createSelector(
    selectDashboardDomain(),
  ( substate ) => substate.get( 'announcementLoading' )
);

const makeSelectNotification = () => createSelector(
    selectDashboardDomain(),
  ( substate ) => substate.get( 'notification' ).toJS()
);

const makeSelectStatistics = () => createSelector(
    selectDashboardDomain(),
  ( substate ) => substate.get( 'statistics' ).toJS()
);

const makeSelectStatTableData = () => createSelector(
    selectDashboardDomain(),
  ( substate ) => substate.get( 'statistics_table' ).toJS()
);

const makeSelectDayCalendar = () => createSelector(
    selectDashboardDomain(),
  ( substate ) => substate.get( 'day' ).toJS()
);

const makeSelectMonthCalendar = () => createSelector(
    selectDashboardDomain(),
  ( substate ) => substate.get( 'month' ).toJS()
);

const makeSelectAnnouncements = () => createSelector(
    selectDashboardDomain(),
  ( substate ) => substate.get( 'announcements' ).toJS()
);

const makeSelectPagination = () => createSelector(
    selectDashboardDomain(),
    ( substate ) => substate.get( 'pagination' ).toJS()
);

const makeSelectAnnouncementsPagination = () => createSelector(
    selectDashboardDomain(),
    ( substate ) => substate.get( 'announcementPagination' ).toJS()
);

const makeSelectShiftEmployeesPagination = () => createSelector(
  selectDashboardDomain(),
  ( substate ) => substate.get( 'shiftEmployeesPagination' ).toJS()
);

const makeSelectShiftEmployees = () => createSelector(
  selectDashboardDomain(),
  ( substate ) => substate.get( 'shiftEmployees' ).toJS()
);

export {
  makeSelectLoading,
  makeSelectPagination,
  makeSelectStatistics,
  makeSelectDayCalendar,
  makeSelectNotification,
  makeSelectMonthCalendar,
  makeSelectAnnouncements,
  makeSelectStatTableData,
  makeSelectRegenerateLoading,
  makeSelectAnnouncementLoading,
  makeSelectAnnouncementsPagination,
  makeSelectShiftEmployeesPagination,
  makeSelectShiftEmployees
};
