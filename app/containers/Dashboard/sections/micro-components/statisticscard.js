import React, { PropTypes } from 'react';
import { H3 } from 'components/Typography';

import { Wrapper } from './styles';

/**
 *
 * Statistics Card
 *
 */
function StatisticsCard({ title, value, total, onClick }) {
    return (
        <Wrapper
            onClick={ onClick }
        >
            <div className="header">
                <p>{ title }</p>
            </div>

            {( value || total ) &&
            <div className="body">
                <H3>{ value } / { total } </H3>
            </div>
            }
        </Wrapper>
    );
}

StatisticsCard.propTypes = {
    title: PropTypes.string,
    value: PropTypes.number,
    total: PropTypes.number,
    onClick: PropTypes.func
};

StatisticsCard.defaultProps = {
    value: 0,
    total: 0
};

export default StatisticsCard;

