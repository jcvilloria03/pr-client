import React, { Component } from 'react';
import moment from 'moment';

import { H3 } from 'components/Typography';

import { Fetch } from 'utils/request';

import {
    Wrapper
} from './styles';

/**
 * Dashboard Server Clock
 */
class ServerClock extends Component {
    constructor( props ) {
        super( props );

        this.state = {
            time: null,
            syncCalledAt: null
        };

        this.interval = null;
        this.syncTime = null;
    }

    componentDidMount() {
        this.syncServerTime();

        this.interval = setInterval( this.setTime, 1000 );
    }

    componentWillUnmount() {
        clearInterval( this.interval );
    }

    setTime = () => {
        if ( this.state.syncCalledAt === null ) {
            this.state.time === null ? this.syncServerTime() : this.stepTime( 1, 'second' );
        }
    }

    syncServerTime = async () => {
        this.setState({ syncCalledAt: moment() });

        const { data } = await Fetch( '/time' );
        const responseOffset = moment().diff( this.state.syncCalledAt, 'seconds' );

        this.setState({ time: moment( data.server_timestamp, 'X' ).utcOffset( data.conversion.utc_offset / 3600 ) });

        if ( !this.state.time ) {
            this.setState({ time: null });
        } else if ( responseOffset > 0 ) {
            this.stepTime( responseOffset, 'second' );
        }

        this.setState({ syncCalledAt: null });
        this.syncTime = setTimeout( this.syncServerTime, 300000 );
    }

    stepTime = ( offset, unit ) => {
        this.setState({ time: moment( this.state.time ).add( offset, unit ) });
    }

    render() {
        return (
            <Wrapper>
                <div className="header">
                    <p>Official Time</p>
                </div>

                <div className="body">
                    { this.state.time
                      ? <H3>{moment( this.state.time ).format( 'HH:mm:ss' )}</H3>
                      : <H3> -- : -- </H3>
                    }
                </div>
            </Wrapper>
        );
    }
}

export default ServerClock;
