/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { Component } from 'react';
import moment from 'moment';

import filter from 'lodash/filter';
import isEmpty from 'lodash/isEmpty';
import { debounce } from 'lodash';

import A from 'components/A';
import Icon from 'components/Icon';
import Modal from 'components/Modal';
import Input from 'components/Input';
import Table from 'components/Table';
import Loader from 'components/Loader';
import Calendar from 'components/Calendar';
import { H4 } from 'components/Typography';

import { DATE_FORMATS } from 'utils/constants';
import { browserHistory } from 'utils/BrowserHistory';
import { formatDate, formatPaginationLabel } from 'utils/functions';

import {
  CalendarWrapper,
  ModalBodyWrapper
} from './styles';
/**
 * Calendar Section
 */
class CalendarSection extends Component {
    static propTypes = {
        dayData: React.PropTypes.shape({
            default_schedule: React.PropTypes.array,
            calendar_data: React.PropTypes.array,
            events: React.PropTypes.array,
            holidays: React.PropTypes.array
        }),
        monthData: React.PropTypes.shape({
            holidays: React.PropTypes.array,
            events: React.PropTypes.array
        }),
        shiftEmployees: React.PropTypes.array,
        shiftEmployeesPagination: React.PropTypes.object,
        getCalendarDayData: React.PropTypes.func,
        getCalendarMonthData: React.PropTypes.func,
        getShiftEmployees: React.PropTypes.func
    }

    constructor( props ) {
        super( props );
        this.state = {
            loading: false,
            shiftTableLoading: false,
            shifts: [],
            holidays: [],
            birthdays: [],
            anniversaries: [],
            selectedDay: moment(),
            selectedMonth: moment().format( 'M' ),
            selectedYear: moment().format( 'YYYY' ),
            label: 'Showing 0-0 of 0 entries',
            calendarData: null,
            shiftSelected: [],
            displayedData: [],
            holidaysForCurrentDate: [],
            birthdaysForCurrentDate: [],
            anniversariesForCurrentDate: [],
            modifiers: {
                holidays: [],
                birthdays: [],
                anniversaries: []
            },
            modifiersStyles: {
                holidays: {
                    backgroundColor: '#fadcdc'
                },
                birthdays: {
                    backgroundColor: '#fce8c8'
                },
                anniversaries: {
                    backgroundColor: '#fce8c8'
                }
            },
            searchTerm: '',
            orderBy: 'first_name',
            orderDir: 'asc',
            tablePageSize: 10
        };

        this.handleSearch = this.handleSearch.bind( this );
        this.handleTableChanges = this.handleTableChanges.bind( this );
        this.onPageChange = this.onPageChange.bind( this );
        this.onPageSizeChange = this.onPageSizeChange.bind( this );
        this.onSortingChange = this.onSortingChange.bind( this );
        this.resetCalendarTableData = this.resetCalendarTableData.bind( this );
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.shiftEmployeesPagination !== nextProps.shiftEmployeesPagination ) {
            this.handleTableChanges( nextProps.shiftEmployeesPagination );
        }
        this.setCalendarDayData( nextProps.dayData.events, nextProps.dayData.holidays );
        this.setState({ loading: false, shiftTableLoading: false });

        if ( !nextProps.dashboardLoading ) {
            this.initializeData();
        }
    }

    onShiftClick =( shift ) => {
        this.setState({ shiftTableLoading: true });
        this.calendarModal.toggle();
        this.props.getShiftEmployees(
            shift.employeeIds,
            1,
            this.state.tablePageSize,
            this.state.orderBy,
            this.state.orderDir,
            this.state.searchTerm
        );
        this.setState({ shiftSelected: shift });
    }

    /**
     * On table page change handler
     */
    onPageChange( page ) {
        this.calendarTable.setState({ page });
        this.props.getShiftEmployees(
            this.state.shiftSelected.employeeIds,
            page + 1,
            this.state.tablePageSize,
            this.state.orderBy,
            this.state.orderDir,
            this.state.searchTerm
        );
    }

    /**
     * On table page size change handler
     */
    onPageSizeChange( pageSize ) {
        this.setState({ tablePageSize: pageSize });
        this.calendarTable.setState({ pageSize });

        this.props.getShiftEmployees(
            this.state.shiftSelected.employeeIds,
            1,
            pageSize,
            this.state.orderBy,
            this.state.orderDir,
            this.state.searchTerm
        );
    }

    onSortingChange( column, additive ) {
        // clone current sorting sorting
        let newSorting = JSON.parse( JSON.stringify( this.calendarTable.state.sorting, ( key, value ) => {
            if ( typeof value === 'function' ) {
                return value.toString();
            }
            return value;
        }) );

        // check if already sorted by same column
        const existingIndex = newSorting.findIndex( ( d ) => d.id === column.id );

        let isDesc = false;
        if ( existingIndex > -1 ) {
            const existing = newSorting[ existingIndex ];
            if ( existing.desc ) {
                if ( additive ) {
                    newSorting.splice( existingIndex, 1 );
                } else {
                    existing.desc = false;
                    newSorting = [existing];
                }
            } else {
                existing.desc = true;
                isDesc = true;
                if ( !additive ) {
                    newSorting = [existing];
                }
            }
        } else if ( additive ) {
            newSorting.push({
                id: column.id,
                desc: false
            });
        } else {
            newSorting = [{
                id: column.id,
                desc: false
            }];
        }

        const orderDir = isDesc ? 'desc' : 'asc';

        this.props.getShiftEmployees(
            this.state.shiftSelected.employeeIds,
            this.props.shiftEmployeesPagination.current_page,
            this.state.tablePageSize,
            column.id,
            orderDir
        );

        // set new sorting states
        this.setState({
            orderBy: column.id,
            orderDir
        });
        this.calendarTable.setState({
            sorting: newSorting
        });
        this.calendarTable.tableComponent.setState({
            sorting: newSorting
        });
    }

    setCalendarDayData = ( events, holidays ) => {
        const birthdays = this.mapEvents( events, 'birth_date', this.state.selectedYear );
        const anniversaries = this.mapEvents( events, 'date_hired', this.state.selectedYear );

        this.setState({
            birthdays,
            anniversaries,
            birthdaysForCurrentDate: [...filter( birthdays, ( birthday ) => moment( birthday.date ).isSame( this.state.selectedDay.format( 'YYYY-MM-DD' ) ) )],
            anniversariesForCurrentDate: [...filter( anniversaries, ( anniversary ) => moment( anniversary.date ).isSame( this.state.selectedDay.format( 'YYYY-MM-DD' ) ) )],
            holidaysForCurrentDate: [...filter( holidays, ( holiday ) => moment( holiday.date ).isSame( this.state.selectedDay.format( 'YYYY-MM-DD' ) ) )]
        });
    }

    initializeData() {
        const { dayData, monthData } = this.props;
        const { selectedDay, selectedYear } = this.state;

        const shiftsData = dayData.calendar_data.map( ( shift ) =>
            ({
                id: shift.id,
                scheduleId: shift.schedule_id,
                employees: shift.employees,
                scheduleName: shift.schedule_name,
                startTime: shift.start_time,
                endTime: shift.end_time,
                employeeIds: shift.employee_ids
            })
        );

        const birthdays = this.mapEvents( monthData.events, 'birth_date', selectedYear );
        const anniversaries = this.mapEvents( monthData.events, 'date_hired', selectedYear );

        this.setState({
            birthdays,
            anniversaries,
            shifts: shiftsData,
            holidays: monthData.holidays,
            modifiers: {
                holidays: monthData.holidays.map( ( day ) => new Date( day.date ) ) || [],
                birthdays: birthdays.map( ( day ) => new Date( day.date ) ),
                anniversaries: anniversaries.map( ( day ) => new Date( day.date ) )
            },
            birthdaysForCurrentDate: [...filter( birthdays, ( birthday ) => moment( birthday.date ).isSame( selectedDay.format( 'YYYY-MM-DD' ) ) )],
            holidaysForCurrentDate: [...filter( monthData.holidays, ( holiday ) => moment( holiday.date ).isSame( selectedDay.format( 'YYYY-MM-DD' ) ) )],
            anniversariesForCurrentDate: [...filter( anniversaries, ( anniversary ) => moment( anniversary.date ).isSame( selectedDay.format( 'YYYY-MM-DD' ) ) )]
        });
    }

    mapEvents( events, field, year ) {
        return events && events.filter( ( event ) => event[ field ] && moment( event[ field ]).year() <= year )
            .map( ( employee ) => {
                const date = moment( employee[ field ]).date();
                const month = moment( employee[ field ]).month();

                return {
                    employeeId: employee.id,
                    employeeFullName: `${employee.first_name} ${employee.last_name}`,
                    date: moment().year( year ).month( month ).date( date ).format( 'YYYY-MM-DD' )
                };
            });
    }

    movePrevDay = () => {
        const date = moment( this.state.selectedDay ).subtract( 1, 'd' );

        this.setState({
            selectedDay: moment( date ),
            selectedMonth: moment( date ).format( 'M' ),
            selectedYear: moment( date ).format( 'YYYY' ),
            loading: true
        });
        this.props.getCalendarDayData( date );
    }

    moveNextDay =() => {
        const date = moment( this.state.selectedDay ).add( 1, 'd' );

        this.setState({
            selectedDay: moment( date ),
            selectedMonth: moment( date ).format( 'M' ),
            selectedYear: moment( date ).format( 'YYYY' ),
            loading: true
        });
        this.props.getCalendarDayData( date );
    }

    handleTableChanges = ( paginationData = this.props.shiftEmployeesPagination ) => {
        const tableProps = {
            page: paginationData.current_page - 1,
            pageSize: paginationData.per_page,
            dataLength: paginationData.total
        };
        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    /**
     * handles and search inputs
     */
    handleSearch = debounce( ( term = '' ) => {
        this.setState({
            searchTerm: term.trim()
        });
        if ( this.state.searchTerm ) {
            this.props.getShiftEmployees(
                this.state.shiftSelected.employeeIds,
                1,
                this.state.tablePageSize,
                this.state.orderBy,
                this.state.orderDir,
                this.state.searchTerm
            );
        } else {
            this.props.getShiftEmployees(
                this.state.shiftSelected.employeeIds,
                1,
                this.state.tablePageSize,
                this.state.orderBy,
                this.state.orderDir
            );
        }
    }, 600 )

    resetCalendarTableData =() => {
        this.setState({
            searchTerm: '',
            orderBy: 'first_name',
            orderDir: 'asc',
            tablePageSize: 10
        });
    }

    render() {
        const {
          shifts,
          loading,
          modifiers,
          selectedDay,
          shiftSelected,
          modifiersStyles,
          holidaysForCurrentDate,
          birthdaysForCurrentDate,
          anniversariesForCurrentDate
        } = this.state;

        const SHIFT_TABLE_COLUMNS = [{
            header: 'id',
            accessor: 'id',
            show: false
        },
        {
            id: 'first_name',
            header: 'Employee Name',
            accessor: ( row ) => row.full_name,
            minWidth: 200
        },
        {
            id: 'status',
            header: 'Status',
            sortable: false,
            accessor: ( row ) => row.date_ended,
            render: () => (
                <div>
                  Clock in
                </div>
            )
        },
        {
            id: 'expected_hours',
            header: 'Expected Hours',
            sortable: false,
            accessor: ( row ) => row.hours_per_day,
            render: () => (
                <div>
                    {shiftSelected.startTime} - {shiftSelected.endTime}
                </div>
            )
        },
        {
            id: 'ip_address',
            header: 'IP Address',
            sortable: false,
            accessor: ( row ) => row.secondary_location_id,
            render: () => (
                <div>
                  127.0.0.1
                </div>
            )
        }];

        const hasEventsForCurrentDay = holidaysForCurrentDate.length
        || birthdaysForCurrentDate.length
        || anniversariesForCurrentDate.length;

        return (
            <CalendarWrapper>
                <Modal
                    ref={ ( ref ) => { this.calendarModal = ref; } }
                    size="lg"
                    title={ shiftSelected.scheduleName }
                    backdrop={ 'static' }
                    onClose={ this.resetCalendarTableData }
                    className="modal"
                    body={ !isEmpty( shiftSelected ) && (
                    <ModalBodyWrapper>
                        <div className="header">
                            <div className="search-wrapper">
                                <Input
                                    className="search"
                                    id="search"
                                    ref={ ( ref ) => { this.searchInput = ref; } }
                                    onChange={ () => {
                                        this.handleSearch( this.searchInput.state.value.toLowerCase() );
                                    } }
                                    addon={ {
                                        content: <Icon name="search" />,
                                        placement: 'right'
                                    } }
                                />
                            </div>
                            <span> { this.state.label } </span>
                        </div>
                        <Table
                            data={ this.props.shiftEmployees }
                            columns={ SHIFT_TABLE_COLUMNS }
                            showPageSizeOptions
                            loading={ this.state.shiftTableLoading }
                            onDataChange={ this.handleTableChanges }
                            pagination
                            ref={ ( ref ) => { this.calendarTable = ref; } }
                            onPageChange={ this.onPageChange }
                            onPageSizeChange={ this.onPageSizeChange }
                            page={ this.props.shiftEmployeesPagination.current_page - 1 || 0 }
                            pageSize={ this.state.tablePageSize || 10 }
                            sizeOptions={ [ 10, 20, 50, 100, 200 ] }
                            pages={ this.props.shiftEmployeesPagination.last_page || 1 }
                            onSortingChange={ this.onSortingChange }
                            manual
                        />
                    </ModalBodyWrapper>
                    ) }
                    buttons={ [] }
                />
                <H4 className="title"> Calendar</H4>
                <div className="body">
                    <div className="summary">
                        <div className="header">
                            <span
                                className="arrow arrow-prev"
                                onClick={ this.movePrevDay }
                            ></span>
                            <H4 className="title"> {formatDate( moment( this.state.selectedDay ), DATE_FORMATS.DISPLAY )}</H4>
                            <span
                                className="arrow arrow-next"
                                onClick={ this.moveNextDay }
                            ></span>
                        </div>
                        { loading
                        ? <Loader className="loader" />
                        : <div className="body">
                            {!isEmpty( shifts ) && (
                            <section>
                                <p><strong>{ moment( selectedDay ).format( 'dddd' ) }</strong></p>

                                <ul className="list">
                                    { shifts.map( ( shift ) => (
                                        <li key={ shift.scheduleId }>
                                            <A
                                                href
                                                className="text-underline"
                                                onClick={ ( e ) => {
                                                    e.preventDefault();
                                                    this.onShiftClick( shift );
                                                } }
                                            >
                                                { shift.employees } { shift.employees > 1 ? 'employees' : 'employee' }
                                            </A>
                                            { shift.scheduleName } ({ shift.startTime } - { shift.endTime })
                                          </li>
                                      ) )}
                                </ul>

                            </section>
                              )}

                            { Boolean( hasEventsForCurrentDay ) && (
                            <section>
                                <p><strong>Events</strong></p>

                                <ul className="list">
                                    {!isEmpty( holidaysForCurrentDate ) && holidaysForCurrentDate.map( ( holiday ) => (
                                        <li key={ holiday.id }>
                                            { holiday.name } - Non working holiday
                                          </li>
                                      ) )}

                                    {!isEmpty( birthdaysForCurrentDate ) && birthdaysForCurrentDate.map( ( birthday ) => (
                                        <li key={ birthday.employeeId }>
                                            <A
                                                href
                                                className="text-underline"
                                                onClick={ ( e ) => { e.preventDefault(); browserHistory.push( `employee/${birthday.employeeId}`, true ); } }
                                            >
                                                { birthday.employeeFullName }
                                            </A>
                                              Birthday
                                          </li>
                                      ) )}

                                    {!isEmpty( anniversariesForCurrentDate ) && anniversariesForCurrentDate.map( ( anniversary ) => (
                                        <li key={ anniversary.employeeId }>
                                            <A
                                                className="text-underline"
                                                href={ `/employee/${anniversary.employeeId}` }
                                            >
                                                { anniversary.employeeFullName }
                                            </A>
                                              Anniversary
                                          </li>
                                      ) )}
                                </ul>

                            </section>
                              )}
                        </div>
                        }
                    </div>
                    <Calendar
                        modifiers={ modifiers }
                        modifiersStyles={ modifiersStyles }
                        handleDayClick={ ( data ) => {
                            this.setState({
                                selectedDay: moment( data ),
                                selectedMonth: moment( data ).format( 'M' ),
                                selectedYear: moment( data ).format( 'YYYY' ),
                                loading: true
                            });
                            this.props.getCalendarDayData( data );
                        } }
                        handleMonthClick={ ( data ) => {
                            this.setState({
                                selectedMonth: moment( data ).format( 'M' ),
                                selectedYear: moment( data ).format( 'YYYY' ),
                                loading: true
                            });
                            this.props.getCalendarMonthData( data );
                        } }
                    />
                </div>

            </CalendarWrapper>
        );
    }
}

export default CalendarSection;
