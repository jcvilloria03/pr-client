/* eslint-disable react/no-unused-prop-types */
/* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
import React, { Component, PropTypes } from 'react';

import isEmpty from 'lodash/isEmpty';

import Icon from 'components/Icon';
import Modal from 'components/Modal';
import Input from 'components/Input';
import Table from 'components/Table';
import Loader from 'components/Loader';
import { H4 } from 'components/Typography';
import SalDropdown from 'components/SalDropdown';
import { isAnyRowSelected, getIdsOfSelectedRows } from 'components/Table/helpers';

import { formatPaginationLabel } from 'utils/functions';

import ServerClock from './micro-components/clock';
import StatisticsCard from './micro-components/statisticscard';

// WIP customTable
// import CustomTable from './micro-components/customTable';

import { StatWrapper, ModalBodyWrapper } from './styles';

import {
  PRESENT_TABLE_COLUMNS,
  LEAVE_TABLE_COLUMNS,
  LATE_TABLE_COLUMNS,
  ABSENT_TABLE_COLUMNS
} from '../constants';

/**
 * Statistics Section
 */
class StatisticsSection extends Component {
    static propTypes = {
        loading: PropTypes.bool,
        regenerate: PropTypes.func,
        fetchTable: PropTypes.func,
        exportCsv: PropTypes.func,
        statTableData: PropTypes.array,
        statistics: PropTypes.shape({
            attendance: PropTypes.object,
            activeEmployees: PropTypes.number
        }),
        pagination: React.PropTypes.shape({
            item_count: React.PropTypes.number,
            page: React.PropTypes.number,
            total_pages: React.PropTypes.number,
            total_records: React.PropTypes.number
        })
    }

    static defaultProps = {
        statistics: {},
        statTableData: []
    };

    constructor( props ) {
        super( props );
        this.state = {
            cards: [
              { title: 'Present Employees', value: 0, type: 'present', table: PRESENT_TABLE_COLUMNS, data: []},
              { title: 'Employees on Leave', value: 0, type: 'on_leave', table: LEAVE_TABLE_COLUMNS, data: []},
              { title: 'Late Employees', value: 0, type: 'tardy', table: LATE_TABLE_COLUMNS, data: []},
              { title: 'Absent Employees', value: 0, type: 'absent', table: ABSENT_TABLE_COLUMNS, data: []}
            ],
            active_employees: 0,
            label: 'Showing 0-0 of 0 entries',
            deleteLabel: '',
            selectedCard: {},
            displayedData: []
        };
    }

    componentWillMount() {
        this.modalTableBody();
    }

    /**
    * handle actions when receiving changes in props
    */
    componentWillReceiveProps( nextProps ) {
        if ( nextProps.statTableData ) {
            this.setState( ( prevState ) => ({
                render: !this.state.render,
                displayedData: nextProps.statTableData,
                selectedCard: { ...prevState.selectedCard, data: nextProps.statTableData }
            }), () => {
                this.modalTableBody();
            });
        }
    }

    handleTableChanges = ( tableProps = this.employeeTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.pagination.total_records });

        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    handleSearch = ( term = '' ) => {
        const { selectedCard } = this.state;
        const matchingRows = this.employeeTable.getFilteredData( selectedCard.data, term );

        this.setState({ displayedData: term ? matchingRows : selectedCard.data }, () => {
            this.handleTableChanges();
        });
    }

    modalTableBody() {
        const {
          selectedCard,
          displayedData
        } = this.state;

        const {
          fetchTable,
          pagination,
          exportCsv
        } = this.props;

        return (
            <ModalBodyWrapper>
                <div className="header">
                    <div className="search-wrapper">
                        <Input
                            className="search"
                            id="search"
                            ref={ ( ref ) => { this.searchInput = ref; } }
                            onChange={ () => {
                                this.handleSearch( this.searchInput.state.value.toLowerCase() );
                            } }
                            addon={ {
                                content: <Icon name="search" />,
                                placement: 'right'
                            } }
                        />
                    </div>
                    {isAnyRowSelected( this.employeeTable )
                  ? <SalDropdown
                      dropdownItems={ [{
                          label: 'Export',
                          children: <div>Export</div>,
                          onClick: () => exportCsv({
                              employees_ids: getIdsOfSelectedRows( this.employeeTable ),
                              export_type: selectedCard.type
                          })
                      }] }
                  />
                  : <span> { this.state.label } </span>
                }
                </div>
                <Table
                    pagination
                    data={ displayedData }
                    columns={ selectedCard.table || [] }
                    onDataChange={ this.handleTableChanges }
                    ref={ ( ref ) => { this.employeeTable = ref; } }
                    onPageChange={ ( data ) => {
                        fetchTable({
                            type: selectedCard.type,
                            perPage: pagination.item_count,
                            page: data + 1
                        });
                    } }
                    onPageSizeChange={ ( data ) => {
                        fetchTable({
                            type: selectedCard.type,
                            perPage: data,
                            page: 1
                        });
                    } }
                    page={ pagination.page - 1 }
                    pageSize={ pagination.item_count }
                    pages={ pagination.total_pages }
                    manual
                />
            </ModalBodyWrapper>
        );
    }

    render() {
        const {
          cards,
          selectedCard,
          displayedData
        } = this.state;

        const {
          loading,
          statistics,
          fetchTable,
          regenerate
        } = this.props;

        return (
            <StatWrapper>
                <Modal
                    ref={ ( ref ) => { this.statisticsModal = ref; } }
                    size="lg"
                    title={ selectedCard.title }
                    backdrop={ 'static' }
                    onClose={ () => {
                        this.setState({
                            displayedData: [],
                            selectedCard: []
                        });
                        this.employeeTable = null;
                    } }
                    body={
                        isEmpty( displayedData ) || isEmpty( displayedData )
                        ? <Loader />
                        : this.modalTableBody()
                     }
                    buttons={ [] }
                />
                <div className="header">
                    <H4 className="title"> Statistics</H4>
                    <button
                        className="regenerate"
                        onClick={ regenerate }
                        type="button"
                    >
                        <Icon name="regenerate" className="icon" />
                    </button>
                </div>
                { loading
                  ? <Loader className="my-5" />
                  : <div className="body">
                      {Object.values( statistics.attendance ).length > 0 && cards.map( ( card ) =>
                          <StatisticsCard
                              key={ card.title }
                              title={ card.title }
                              value={ statistics.attendance[ card.type ] }
                              total={ statistics.activeEmployees }
                              onClick={ () => {
                                  if ( !statistics.attendance[ card.type ]) {
                                      return;
                                  }
                                  this.setState({ selectedCard: card });
                                  this.statisticsModal && this.statisticsModal.toggle();
                                  fetchTable({ type: card.type, perPage: 10, page: 1 });
                              } }
                          />
                        )}
                      <ServerClock />
                  </div>
                  }
            </StatWrapper>
        );
    }
}

export default StatisticsSection;
