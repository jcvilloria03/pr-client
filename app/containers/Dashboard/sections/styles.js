import styled from 'styled-components';

const Wrapper = styled.div`
  width: 100%;
  background-color:#F6F6F6;
  margin: 2rem 0;
  border: 1px solid transparent;
  border-radius: 0.25rem;
  padding: 1rem;

  .title {
    font-weight: bold;
  }

  .header {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    height: 45px;

    .regenerate {
      height: 40px;
      width: 40px;

      &:focus, &:active:focus {
        outline: none;
        background-color: #fff;
        border-radius: 50%;
        color: #4ABA4A;
      }
    }
  }
`;

export const ConfirmBodyWrapperStyle = styled.div`
    display: flex;
    padding: 0 20px;

    .icon {
        font-size: 50px;
        color: orange;
        display: inline-flex;
        min-width: 45px;
        margin-right: 20px;

        > i {
            align-self: center;
        }
    }

    .message {
        text-align: left;
    }
`;

export const ModalWrapper = styled.section`
  .toggleWrapper {
    display: flex;
    flex-direction: row;
    align-items: center;
    line-height: 1;
    margin: 15px 0;
  }

  .download-icon > svg {
    width: 12px;
    height: 12px;
    margin: 0 6px 0 0;
  }
`;

export const StatWrapper = styled( Wrapper )`
  .body {
    display: flex;
    flex-direction: row;
  }
`;

export const CalendarWrapper = styled( Wrapper )`
  .body {
    display: flex;
    flex-direction: row;

    .summary {
      margin-right: 1rem;
      padding: 8px 1rem;
      width:55%;
      background-color:#fff;

      .header {
        align-items: center;

        .title {
          margin-bottom: 0;
        }

        .arrow {
          width: 1.5rem;
          height: 1.5rem;
          background-repeat: no-repeat;
          background-position: center;
          background-size: contain;
          cursor: pointer;
        }

        .arrow-prev {
          left: 1rem;
          background-image: url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+Cjxzdmcgd2lkdGg9IjI2cHgiIGhlaWdodD0iNTBweCIgdmlld0JveD0iMCAwIDI2IDUwIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIj4KICAgIDwhLS0gR2VuZXJhdG9yOiBTa2V0Y2ggMy4zLjIgKDEyMDQzKSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5wcmV2PC90aXRsZT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPgogICAgPGRlZnM+PC9kZWZzPgogICAgPGcgaWQ9IlBhZ2UtMSIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCIgc2tldGNoOnR5cGU9Ik1TUGFnZSI+CiAgICAgICAgPGcgaWQ9InByZXYiIHNrZXRjaDp0eXBlPSJNU0xheWVyR3JvdXAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEzLjM5MzE5MywgMjUuMDAwMDAwKSBzY2FsZSgtMSwgMSkgdHJhbnNsYXRlKC0xMy4zOTMxOTMsIC0yNS4wMDAwMDApIHRyYW5zbGF0ZSgwLjg5MzE5MywgMC4wMDAwMDApIiBmaWxsPSIjNTY1QTVDIj4KICAgICAgICAgICAgPHBhdGggZD0iTTAsNDkuMTIzNzMzMSBMMCw0NS4zNjc0MzQ1IEwyMC4xMzE4NDU5LDI0LjcyMzA2MTIgTDAsNC4yMzEzODMxNCBMMCwwLjQ3NTA4NDQ1OSBMMjUsMjQuNzIzMDYxMiBMMCw0OS4xMjM3MzMxIEwwLDQ5LjEyMzczMzEgWiIgaWQ9InJpZ2h0IiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIj48L3BhdGg+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4K");
        }

        .arrow-next {
          right: 1rem;
          background-image: url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+Cjxzdmcgd2lkdGg9IjI2cHgiIGhlaWdodD0iNTBweCIgdmlld0JveD0iMCAwIDI2IDUwIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIj4KICAgIDwhLS0gR2VuZXJhdG9yOiBTa2V0Y2ggMy4zLjIgKDEyMDQzKSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5uZXh0PC90aXRsZT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPgogICAgPGRlZnM+PC9kZWZzPgogICAgPGcgaWQ9IlBhZ2UtMSIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCIgc2tldGNoOnR5cGU9Ik1TUGFnZSI+CiAgICAgICAgPGcgaWQ9Im5leHQiIHNrZXRjaDp0eXBlPSJNU0xheWVyR3JvdXAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuOTUxNDUxLCAwLjAwMDAwMCkiIGZpbGw9IiM1NjVBNUMiPgogICAgICAgICAgICA8cGF0aCBkPSJNMCw0OS4xMjM3MzMxIEwwLDQ1LjM2NzQzNDUgTDIwLjEzMTg0NTksMjQuNzIzMDYxMiBMMCw0LjIzMTM4MzE0IEwwLDAuNDc1MDg0NDU5IEwyNSwyNC43MjMwNjEyIEwwLDQ5LjEyMzczMzEgTDAsNDkuMTIzNzMzMSBaIiBpZD0icmlnaHQiIHNrZXRjaDp0eXBlPSJNU1NoYXBlR3JvdXAiPjwvcGF0aD4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPgo=");
        }
      }

      .animation {
        margin-top: 100px;
      }

      .body {
        margin-top: 2rem;
        display: flex;
        flex-direction: column;

        section {
          margin: 1rem 0;
        }

        .list {
          list-style: none;
          padding-left: 0;

          .text-underline {
            text-decoration: underline !important;
            margin-right: 1rem;
          }
        }
      }
    }
  }
`;

export const ModalBodyWrapper = styled.div`
  .header {
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    flex-direction: row;
    margin-bottom: 10px;

    .hidden {
        display: none !important;
    }

    .search-wrapper {
        flex-grow: 1;

        .search {
            width: 300px;
            border: 1px solid #333;
            border-radius: 30px;

            input {
                border: none;
            }
        }

        p {
            display: none;
        }

        .input-group,
        .form-control {
            background-color: transparent;
        }

        .input-group-addon {
            background-color: transparent;
            border: none;
        }

        .isvg {
            display: inline-block;
            width: 1rem;
        }
    }
  }
`;

export const AnnouncementWrapper = styled( Wrapper )`
  .content {
        margin-top: 40px;
        background-color:#fff;
        padding: 1rem;

        .announcement {
          float: right;
          margin-bottom: 1rem
        }

        .header {
            width: 100%;
            display: flex;
            justify-content: space-between;
            align-items: flex-end;
            flex-direction: row;
            margin-bottom: 10px;

            .search-wrapper {
                flex-grow: 1;

                .search {
                    width: 300px;
                    border: 1px solid #333;
                    border-radius: 30px;

                    input {
                        border: none;
                    }
                }

                p {
                    display: none;
                }

                .input-group,
                .form-control {
                    background-color: transparent;
                }

                .input-group-addon {
                    background-color: transparent;
                    border: none;
                }

                .isvg {
                    display: inline-block;
                    width: 1rem;
                }
            }

            .filter-icon > svg {
                height: 10px;
            }
        }

        .announcementTable {
          .details {
            padding: 1rem;
            margin: 20px;
            background-color: #fafbfc;
            border: 1px solid #f0f4f6;

            .head {
              display: flex;
              justify-content: space-between;
            }
          }
        }
    }
`;
