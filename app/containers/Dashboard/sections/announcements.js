/* eslint-disable camelcase */
import React, { Component } from 'react';

import isEqual from 'lodash/isEqual';
import Icon from 'components/Icon';
import Table from 'components/Table';
import Modal from 'components/Modal';
import Input from 'components/Input';
import Switch from 'components/Switch';
import Button from 'components/Button';
import { H4 } from 'components/Typography';
import SalConfirm from 'components/SalConfirm';
import MultiSelect from 'components/MultiSelect';

import { Fetch } from 'utils/request';
import { auth } from 'utils/AuthService';
import { formatDate, formatPaginationLabel } from 'utils/functions';
import { DATE_FORMATS } from 'utils/constants';

import { SELECT_ALL_EMPLOYEES_OPTIONS } from '../constants';

import {
  ModalWrapper,
  AnnouncementWrapper,
  ConfirmBodyWrapperStyle
} from './styles';

/**
 * Announcement Section
 */
class AnnouncementsSection extends Component {
    static propTypes = {
        loading: React.PropTypes.bool,
        getAnnouncements: React.PropTypes.func,
        createAnnouncements: React.PropTypes.func,
        announcements: React.PropTypes.array,
        pagination: React.PropTypes.shape({
            from: React.PropTypes.number,
            to: React.PropTypes.number,
            total: React.PropTypes.number,
            current_page: React.PropTypes.number,
            total_pages: React.PropTypes.number,
            per_page: React.PropTypes.number
        })
    }

    static defaultProps = {
        announcements: []
    };

    constructor( props ) {
        super( props );

        this.state = {
            label: 'Showing 0-0 of 0 entries',
            displayed_data: [],
            show_filter: false,
            hasFiltersApplied: false,
            previousQuery: '',
            filtersApplied: [],
            inactiveUsers: [],
            subject: '',
            message: '',
            allow_response: false,
            read_receipt: false,
            showModal: false,
            profile: null
        };
    }
    componentWillMount() {
        const profile = auth.getUser();

        this.setState({ profile });
    }

    componentWillReceiveProps( nextProps ) {
        if ( !isEqual( nextProps.pagination, this.props.pagination ) ) {
            this.setState({
                pagination: nextProps.pagination,
                label: formatPaginationLabel({
                    page: nextProps.pagination.current_page - 1,
                    pageSize: nextProps.pagination.per_page,
                    dataLength: nextProps.pagination.total
                })
            });
        }
    }

    getRecipients = ( announcement ) => {
        let recipients = announcement.recipient_groups || [];

        if ( this.state.profile.account_id ) {
            recipients = recipients.filter( ( recipient ) => recipient.rel_id !== this.state.profile.account_id );
        }

        const countOfRecipients = recipients.length;
        const name = recipients[ 0 ].name;

        if ( !countOfRecipients ) return '';

        return countOfRecipients === 1 ? name : `${name} and ${countOfRecipients - 1} ${countOfRecipients === 2 ? 'other' : 'others'}`;
    }

    getInactiveEmployees = () => {
        Fetch( `/company/${this.state.profile.last_active_company_id}/inactive_employees`,
            { method: 'POST',
                data: {
                    affected_employees: this.employees.state.value
                }
            }).then( ({ data }) => {
                if ( data.length ) {
                    this.setState({
                        showModal: true,
                        inactiveUsers: data
                    });
                } else {
                    this.announcementModal.toggle();
                    this.setState({ showModal: false });

                    this.props.createAnnouncements({
                        affected_employees: this.employees.state.value,
                        company_id: this.state.profile.last_active_company_id,
                        subject: this.state.subject,
                        message: this.state.message,
                        allow_response: this.state.allow_response,
                        read_receipt: this.state.read_receipt
                    });
                }
            });
    }

    loadEmployeesList = ( keyword, callback ) => {
        Fetch( `/company/${this.state.profile.last_active_company_id}/affected_employees/search?term=${keyword}&limit=10&without_users=yes`, { method: 'GET' })
          .then( ( result ) => {
              const list = result.map( ({ id, name, type }) => ({
                  id,
                  type,
                  uid: `${type}${id}`,
                  value: id,
                  label: name,
                  field: type,
                  disabled: false
              }) );
              callback( null, { options: SELECT_ALL_EMPLOYEES_OPTIONS.concat( list ) });
          })
          .catch( ( error ) => callback( error, null ) );
    }

    resetFilters = () => {
        this.setState({
            show_filter: false,
            displayed_data: this.props.announcements,
            has_filters_applied: false
        }, () => {
            this.handleSearch();
        });
    }

    toggleFilter = () => {
        this.setState( ( state ) => ({
            show_filter: !state.show_filter
        }) );
    }

    handleTableChanges = ( tableProps = this.announcementsTable.tableComponent.state ) => {
        Object.assign( tableProps, { dataLength: this.props.pagination.total });

        this.setState({
            label: formatPaginationLabel( tableProps )
        });
    }

    handleSearch = () => {
        let searchQuery = null;

        if ( this.searchInput && this.searchInput.state.value ) {
            searchQuery = this.searchInput.state.value.toLowerCase();
        }

        this.props.getAnnouncements({
            term: searchQuery,
            page: 1,
            perPage: 10,
            role: 'recipient'
        });
    };

    render() {
        const {
          profile,
          subject,
          message,
          inactiveUsers,
          showModal,
          allow_response,
          read_receipt,
          label
          // hasFiltersApplied
        } = this.state;

        const {
          loading,
          pagination,
          announcements,
          getAnnouncements,
          createAnnouncements
        } = this.props;

        const tableColumns = [
            {
                id: 'id',
                header: 'id',
                accessor: 'id',
                show: false
            },
            {
                id: 'Sender',
                header: 'Sender',
                accessor: ( row ) => row.sender.full_name
            },
            {
                id: 'Subject',
                header: 'Subject',
                accessor: ( row ) => row.subject
            },
            {
                id: 'Date Received',
                header: 'Date Received',
                sortable: false,
                accessor: ( row ) => formatDate( row.recipient.seen_at, DATE_FORMATS.DISPLAY ) || 'N/A'
            },
            {
                id: 'Created On',
                header: 'Created On',
                sortable: false,
                accessor: ( row ) => formatDate( row.created_at, DATE_FORMATS.DISPLAY )
            }];

        return (
            <div>
                <SalConfirm
                    onConfirm={ () => {
                        this.announcementModal.toggle();
                        this.setState({ showModal: false });

                        createAnnouncements({
                            affected_employees: this.employees.state.value,
                            company_id: profile.last_active_company_id,
                            subject,
                            message,
                            allow_response,
                            read_receipt
                        });
                    } }
                    onClose={ () => this.setState({ showModal: false }) }
                    body={
                        <ConfirmBodyWrapperStyle>
                            <div className="message">
                                    Listed Below are Inactive Recipients:
                                    <br /><br />
                                {inactiveUsers.map( ( user, index ) => <div key={ index }>
                                    <span >{user.full_name}</span>
                                </div> )}

                            </div>
                        </ConfirmBodyWrapperStyle>
                        }
                    backdrop="static"
                    title="Inactive recipient!"
                    buttonStyle="primary"
                    confirmText="Send Anyway"
                    cancelText="Edit"
                    visible={ showModal }
                />
                <Modal
                    ref={ ( ref ) => {
                        this.announcementModal = ref;
                    } }
                    size="lg"
                    title="Create Announcement"
                    body={
                        <ModalWrapper>
                            <MultiSelect
                                id="announcement"
                                async
                                label={ <span>To:</span> }
                                loadOptions={ this.loadEmployeesList }
                                ref={ ( ref ) => { this.employees = ref; } }
                                placeholder="Enter location, department name, position, employee name"
                                required
                            />
                            <Input
                                className="mb-1"
                                label="Subject:"
                                id="subject"
                                name="subject"
                                onChange={ ( data ) => {
                                    this.setState({ subject: data });
                                } }
                                required
                            />
                            <Input
                                className="mb-1"
                                id="message"
                                label="Message:"
                                type="textarea"
                                max={ 260 }
                                onChange={ ( data ) => {
                                    this.setState({ message: data });
                                } }
                                required
                            />

                            <div className="toggleWrapper">
                                <Switch
                                    checked={ allow_response }
                                    onChange={ ( data ) => {
                                        this.setState({ allow_response: data });
                                    } }
                                />
                              &nbsp; Allow response from recipients
                            </div>

                            <div className="toggleWrapper">
                                <Switch
                                    checked={ read_receipt }
                                    onChange={ ( data ) => {
                                        this.setState({ read_receipt: data });
                                    } }
                                />
                              &nbsp; Show Read Receipt
                            </div>

                        </ModalWrapper>
                     }
                    buttons={ [
                        {
                            label: 'Send',
                            type: 'action',
                            onClick: () => { this.getInactiveEmployees(); }
                        },
                        {
                            label: 'Cancel',
                            type: 'grey',
                            onClick: () => { this.announcementModal.toggle(); }
                        }
                    ] }
                />
                <AnnouncementWrapper>
                    <H4 className="title"> Announcements</H4>
                    <div className="content">
                        <Button
                            className="announcement"
                            label="Create Announcement"
                            type="action"
                            onClick={ () => { this.announcementModal.toggle(); } }
                        />
                        <div className="header">
                            <div className="search-wrapper">
                                <Input
                                    className="search"
                                    id="search"
                                    ref={ ( ref ) => { this.searchInput = ref; } }
                                    onKeyPress={ ( e ) => {
                                        e.charCode === 13 && this.handleSearch();
                                    } }
                                    addon={ {
                                        content: <Icon name="search" />,
                                        placement: 'right'
                                    } }
                                />
                            </div>
                            <span>
                                { label }
                                {/* &nbsp;
                                <Button
                                    label={ <span><Icon className="filter-icon" name="filter" /> Filter</span> }
                                    type={ hasFiltersApplied ? 'primary' : 'neutral' }
                                    onClick={ this.toggleFilter }
                                /> */}
                            </span>
                        </div>
                        <Table
                            className="announcementTable"
                            data={ announcements }
                            loading={ loading }
                            columns={ tableColumns }
                            pagination
                            onDataChange={ this.handleTableChanges }
                            ref={ ( ref ) => { this.announcementsTable = ref; } }
                            SubComponent={ ( data ) => (
                                <section className="details">
                                    <div className="head">
                                        <p><strong>Subject:</strong> {data.rowValues.Subject}</p>
                                        <p><strong>Date received:</strong> {data.rowValues[ 'Date Received' ]}</p>
                                    </div>
                                    <p><strong>From:</strong> {data.rowValues[ 'Date Received' ]}</p>
                                    <p><strong>To:</strong> {this.getRecipients( data.row )}</p>
                                    <p>{data.row.message}</p>
                                </section>
                              ) }
                            onPageChange={ ( data ) => {
                                getAnnouncements({
                                    page: data + 1,
                                    perPage: pagination.per_page,
                                    role: 'recipient'
                                });
                            } }
                            onPageSizeChange={ ( data ) => {
                                getAnnouncements({
                                    page: 1,
                                    perPage: data,
                                    role: 'recipient'
                                });
                            } }
                            page={ pagination.current_page - 1 }
                            pageSize={ pagination.per_page }
                            pages={ pagination.total_pages }
                            manual
                        />
                    </div>
                </AnnouncementWrapper>
            </div>
        );
    }
}

export default AnnouncementsSection;
