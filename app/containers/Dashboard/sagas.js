/* eslint-disable camelcase */
/* eslint-disable no-inner-declarations */
import { take, call, put, cancel } from 'redux-saga/effects';
import { takeEvery, delay, takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import fileDownload from 'js-file-download';
import moment from 'moment';
import get from 'lodash/get';
import { auth } from 'utils/AuthService';

import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';

import { REINITIALIZE_PAGE } from '../App/constants';
import { resetStore } from '../App/sagas';

import {
    setLoading,
    setPagination,
    setStatistics,
    setNotification,
    setAnnouncements,
    setCalendarDayData,
    setRegenerateLoading,
    setEmployeeTableData,
    setCalendarMonthData,
    setAnnouncementLoading,
    setAnnouncementsPagination,
    setShiftEmployeesPagination,
    setShiftEmployees
} from './actions';

import {
    REGENERATE,
    INITIALIZE,
    NOTIFICATION,
    EXPORT_EMPLOYEE,
    GET_STATISTICS,
    GET_ANNOUNCEMENTS,
    CREATE_ANNOUNCEMENTS,
    GET_CALENDAR_DAY_VIEW,
    GET_CALENDAR_MONTH_VIEW,
    GET_EMPLOYEE_TABLE_DATA,
    GET_SHIFT_EMPLOYEES
} from './constants';

/**
 * initialize page
 */
export function* initializePage() {
    if ( auth.isNonAdmin() || auth.isExpired() ) {
        return;
    }

    try {
        yield put( setLoading( true ) );
        const companyId = company.getLastActiveCompanyId();
        const year = moment().year();
        let month = moment().month() + 1;
        month = month < 10 ? `0${month}` : month;

        const [
          employees,
          statistics,
          announcements,
          default_schedule,
          calendar_data,
          holidays,
          events
        ] = yield [
            call( Fetch, `/company/${companyId}/active_employees_count`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/attendance_stats`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/announcements`, { method: 'GET',
                params: {
                    per_page: 10,
                    role: 'recipient',
                    page: 1,
                    sort_by: 'created_at',
                    sort_order: 'desc'
                }
            }),
            call( Fetch, `/default_schedule?company_id=${companyId}`, { method: 'GET' }),
            call( Fetch, '/admin_dashboard/calendar_data_counts', { method: 'POST',
                data: {
                    company_id: companyId,
                    start_date: moment().format( 'YYYY-MM-DD' ),
                    end_date: moment().format( 'YYYY-MM-DD' ),
                    group: 'time_attendance'
                }
            }),
            call( Fetch, `/company/${companyId}/holidays`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/employee/events?month=${year}-${month}`, { method: 'GET' })
        ];

        yield [
            put( setStatistics({ attendance: statistics.data, activeEmployees: employees.data.count }) ),
            put( setAnnouncements( announcements.data ) ),
            put( setAnnouncementsPagination( announcements.meta.pagination ) ),
            put( setCalendarDayData({ default_schedule: default_schedule.data, calendar_data, events, holidays: holidays.data }) ),
            put( setCalendarMonthData({ holidays: holidays.data, events }) )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * get calendar day view data
 * @param {Date}   Selected Date
 *
 */
export function* getCalendarDayViewData({ payload }) {
    const company_id = company.getLastActiveCompanyId();

    const year = moment( payload ).format( 'YYYY' );
    let month = moment( payload ).format( 'M' );
    month = month < 10 ? `0${month}` : month;
    let day = moment( payload ).format( 'D' );
    day = day < 10 ? `0${day}` : day;

    try {
        const [
          default_schedule,
          calendar_data,
          holidays,
          events
        ] = yield [
            call( Fetch, `/default_schedule?company_id=${company_id}`, { method: 'GET' }),
            call( Fetch, '/admin_dashboard/calendar_data_counts',
                { method: 'POST',
                    data: {
                        company_id,
                        start_date: moment( payload ).format( 'YYYY-MM-DD' ),
                        end_date: moment( payload ).format( 'YYYY-MM-DD' ),
                        group: 'time_attendance'
                    }
                }),
            call( Fetch, `/company/${company_id}/holidays`, { method: 'GET' }),
            call( Fetch, `/company/${company_id}/employee/events?month=${year}-${month}-${day}`, { method: 'GET' })
        ];

        yield put( setCalendarDayData({ default_schedule: default_schedule.data, calendar_data, events, holidays: holidays.data }) );
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * get calendar month view data
 * @param {Date}   Year
 * @param {Date}   Month
 */
export function* getCalendarMonthViewData({ payload }) {
    const year = moment( payload ).format( 'YYYY' );
    let month = moment( payload ).format( 'M' );
    month = month < 10 ? `0${month}` : month;

    try {
        const companyId = company.getLastActiveCompanyId();

        const [
          holidays,
          events
        ] = yield [
            call( Fetch, `/company/${companyId}/holidays`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/employee/events?month=${year}-${month}`, { method: 'GET' })
        ];

        yield put( setCalendarMonthData({ holidays: holidays.data, events }) );
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * get statistics data
 */
export function* getStatisticsData() {
    try {
        yield put( setLoading( true ) );

        const companyId = company.getLastActiveCompanyId();

        const [
          employees,
          statistics
        ] = yield [
            call( Fetch, `/company/${companyId}/active_employees_count`, { method: 'GET' }),
            call( Fetch, `/company/${companyId}/attendance_stats`, { method: 'GET' })
        ];

        yield put( setStatistics({
            attendance: statistics.data,
            activeEmployees: employees.data.count
        }) );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Simple mode for employees list
 *
 */
export function* getShiftEmployees({ payload }) {
    const companyId = company.getLastActiveCompanyId();
    const { employeeIds, page, perPage,
        orderBy, orderDir, searchKeyword
    } = payload;

    try {
        const response = yield call( Fetch, `/company/${companyId}/ta_employees/id`,
            { method: 'POST',
                data: {
                    values: employeeIds,
                    response_size: 'minimal',
                    pagination: {
                        page,
                        per_page: perPage,
                        order_dir: orderDir,
                        order_by: orderBy,
                        keyword: searchKeyword.trim()
                    }
                }
            });

        if ( response.meta ) {
            const { meta } = response;
            const paginationData = {
                from: meta.pagination.current_page || 1,
                to: meta.pagination.total_pages || 1,
                total: meta.pagination.total || 1,
                current_page: meta.pagination.current_page || 1,
                last_page: meta.pagination.total_pages || 1,
                per_page: meta.pagination.count
            };

            yield put( setShiftEmployeesPagination( paginationData ) );
        } else if ( response.total ) {
            const paginationData = {
                from: response.from || 1,
                to: response.to || 1,
                total: response.total || 1,
                current_page: response.current_page || 1,
                last_page: response.last_page || 1,
                per_page: response.per_page
            };

            yield put( setShiftEmployeesPagination( paginationData ) );
        }

        yield put( setShiftEmployees( response.data || []) );
    } catch ( error ) {
        yield call( notifyUser, error );
    }
}

/**
 * Get employees for dashboard statistics modal
 * @param {String|Number}   companyId
 * @param {String}          attendanceType
 * @param {Number}          pageSize
 * @param {Number}          pageNumber
 *
 * @returns {Promise}
 */
export function* getEmployees({ payload }) {
    const { type, perPage, page } = payload;
    const companyId = company.getLastActiveCompanyId();

    try {
        const { data } = yield call( Fetch, `/company/${companyId}/dashboard/attendance?type=${type}&item_count=${perPage}&page=${page}`, { method: 'GET' });

        const { employees, ...others } = data;

        yield put( setPagination( others ) );
        yield put( setEmployeeTableData( employees ) );
    } catch ( error ) {
        yield call( notifyError, error );
    }
}

/**
 * Get Announcements
 */
export function* getAnnouncements({ payload }) {
    const { term = '', perPage, role, page } = payload;

    try {
        yield put( setAnnouncementLoading( true ) );
        const companyId = company.getLastActiveCompanyId();

        const announcements = yield call( Fetch, `/company/${companyId}/announcements`, { method: 'GET',
            params: {
                term,
                per_page: perPage,
                role,
                page,
                sort_by: 'created_at',
                sort_order: 'desc'
            }
        });

        yield [
            put( setAnnouncements( announcements.data ) ),
            put( setAnnouncementsPagination( announcements.meta.pagination ) )
        ];
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setAnnouncementLoading( false ) );
    }
}

/**
 * Create Announcements
 * @param {array} data
 */
export function* createAnnouncements({ payload }) {
    try {
        yield put( setAnnouncementLoading( true ) );
        yield call( Fetch, '/announcement', { method: 'POST', data: payload });

        yield call( getAnnouncements, { payload: {
            per_page: 10,
            role: 'recipient',
            page: 1,
            sort_by: 'created_at',
            sort_order: 'desc'
        }});

        yield call( notifyUser, {
            show: true,
            title: 'Success',
            message: 'Announcement successfully created',
            type: 'success'
        });
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setAnnouncementLoading( false ) );
    }
}

/**
 * Export Employee Data
 * @param {array} data
 */
export function* exportEmployee({ payload }) {
    const companyId = company.getLastActiveCompanyId();

    try {
        yield put( setLoading( true ) );
        const response = yield call( Fetch, `/company/${companyId}/scorecard/download`, { method: 'POST', data: payload });

        fileDownload( response, 'employee-data.csv' );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setLoading( false ) );
    }
}

/**
 * Regenerate attendance computation
 *
 * @returns {Promise}
 */
export function* regenerate() {
    try {
        yield put( setRegenerateLoading( true ) );

        const currentDay = moment().format( 'YYYY-MM-DD' );
        const companyId = company.getLastActiveCompanyId();

        const data = {
            company_id: companyId,
            filter: { employee_status: ['active']},
            employee_ids: [],
            search_terms: '',
            dates: [currentDay]
        };
        let jobID = null;

        /**
         * calculate attendance
         */
        function* calculate() {
            const response = yield call( Fetch, '/attendance/calculate/bulk', {
                method: 'POST',
                data
            });

            if ( response.data ) {
                jobID = response.data.job_id;
                yield call( fetchJobStatus, response.data.job_id );
            } else {
                yield call( calculate, data );
            }
        }

        /**
         * Get the calculation status
         */
        function* fetchJobStatus( jobId ) {
            const response = yield call( Fetch, `/attendance/job/${jobId}`, { method: 'GET' });

            if ( response.data.status === 'FINISHED' ) {
                yield call( getStatisticsData );
            } else if ( response.data.status === 'FAILED' ) {
                yield call( displayJobErrors, jobId );
            } else {
                yield call( delay, 5000 );
                yield call( fetchJobStatus, jobId );
            }

            return true;
        }

        /**
         * Get the job errors
         */
        function* displayJobErrors( jobId ) {
            const response = yield call( Fetch, `/attendance/job/${jobId}/errors`, { method: 'GET' });

            const uniqueErrors = new Set( response.data.map( ({ message }) => message ) );
            const errorMessages = Array.from( uniqueErrors );

            yield call( delay, 5000 );
            yield call( notifyError, errorMessages );
        }

        if ( !jobID ) yield call( calculate, data );
    } catch ( error ) {
        yield call( notifyError, error );
    } finally {
        yield put( setRegenerateLoading( false ) );
    }
}

/**
 * Handle error notification
 * @param {Object} error - Error object
 */
export function* notifyError( error ) {
    const payload = {
        show: true,
        title: get( error, 'response.statusText', 'Error' ),
        message: get( error, 'response.data.message', error.statusText ),
        type: 'error'
    };

    yield call( notifyUser, payload );
}

/**
 * Display a notification to user
 * @param {String} payload.title
 * @param {*} payload.message
 * @param {Boolean} payload.show
 * @param {String} payload.type - One of 'success', 'error', 'warning', 'default'
 */
export function* notifyUser( payload ) {
    const emptyNotification = {
        title: ' ',
        message: ' ',
        show: false,
        type: payload.type || 'error'
    };

    yield put( setNotification( emptyNotification ) );

    yield put( setNotification( payload ) );

    yield call( delay, 5000 );
    yield put( setNotification( emptyNotification ) );
}

/**
 * Reinitialize page
 */
export function* reinitializePage() {
    // RESET STORE TO INITIAL STATE FIRST TO MAKE SURE EVERYTHING IS FRESH
    yield call( resetStore );

    yield call( initializePage );
}

/**
 * Watcher for GET_STATISTICS
 */
export function* watchForGetStatistics() {
    const watcher = yield takeEvery( GET_STATISTICS, getStatisticsData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_CALENDAR_DAY_VIEW
 */
export function* watchForGetCalendarDayViewData() {
    const watcher = yield takeEvery( GET_CALENDAR_DAY_VIEW, getCalendarDayViewData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}
/**
 * Watcher for GET_CALENDAR_MONTH_VIEW
 */
export function* watchForGetCalendarMonthViewData() {
    const watcher = yield takeEvery( GET_CALENDAR_MONTH_VIEW, getCalendarMonthViewData );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_ANNOUNCEMENTS
 */
export function* watchForGetAnnouncements() {
    const watcher = yield takeEvery( GET_ANNOUNCEMENTS, getAnnouncements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for CREATE_ANNOUNCEMENTS
 */
export function* watchForCreateAnnouncements() {
    const watcher = yield takeEvery( CREATE_ANNOUNCEMENTS, createAnnouncements );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for GET_EMPLOYEE_TABLE_DATA
 */
export function* watchForGetEmployees() {
    const watcher = yield takeEvery( GET_EMPLOYEE_TABLE_DATA, getEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Watcher for NOTIFICATION
 */
export function* watchForNotifyUser() {
    const watcher = yield takeEvery( NOTIFICATION, notifyUser );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForReinitializePage() {
    const watcher = yield takeLatest( REINITIALIZE_PAGE, reinitializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForRegenerate() {
    const watcher = yield takeLatest( REGENERATE, regenerate );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForExport() {
    const watcher = yield takeLatest( EXPORT_EMPLOYEE, exportEmployee );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForInitializePage() {
    const watcher = yield takeLatest( INITIALIZE, initializePage );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

/**
 * Individual exports for testing
 */
export function* watchForGetShiftEmployees() {
    const watcher = yield takeEvery( GET_SHIFT_EMPLOYEES, getShiftEmployees );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

// All sagas to be loaded
export default [
    watchForExport,
    watchForRegenerate,
    watchForNotifyUser,
    watchForInitializePage,
    watchForReinitializePage,
    watchForGetEmployees,
    watchForGetStatistics,
    watchForGetCalendarDayViewData,
    watchForGetCalendarMonthViewData,
    watchForGetAnnouncements,
    watchForCreateAnnouncements,
    watchForGetShiftEmployees
];
