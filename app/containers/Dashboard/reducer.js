import { fromJS } from 'immutable';
import {
    SET_LOADING,
    SET_PAGINATION,
    SET_STATISTICS,
    SET_NOTIFICATION,
    SET_ANNOUNCEMENTS,
    SET_TABLE_PAGINATION,
    SET_CALENDAR_DAY_DATA,
    SET_REGENERATE_LOADING,
    SET_CALENDAR_MONTH_DATA,
    SET_EMPLOYEE_TABLE_DATA,
    SET_ANNOUNCEMENT_LOADING,
    SET_ANNOUNCEMENTS_PAGINATION,
    SET_SHIFT_EMPLOYEES_PAGINATION,
    SET_SHIFT_EMPLOYEES
} from './constants';

const initialState = fromJS({
    loading: false,
    regenerateLoading: false,
    announcementLoading: false,
    notification: {
        title: '',
        message: '',
        show: false,
        type: 'error'
    },
    serverTime: null,
    statistics: {
        attendance: {},
        activeEmployees: 0
    },
    announcements: [],
    day: {
        default_schedule: [],
        calendar_data: []
    },
    month: {
        holidays: [],
        events: []
    },
    statistics_table: [],
    pagination: {
        item_count: 10,
        page: 1,
        total_pages: 0,
        total_records: 0
    },
    shiftEmployeesPagination: {
        from: 1,
        to: 1,
        total: 1,
        current_page: 0,
        last_page: 1,
        per_page: 10
    },
    shiftEmployees: [],
    announcementPagination: {
        from: 1,
        to: 1,
        total: 1,
        current_page: 1,
        total_pages: 1,
        per_page: 10
    }
});

/**
 *
 * Dashboard reducer
 *
 */
function dashboardReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_LOADING:
            return state.set( 'loading', action.payload );
        case SET_REGENERATE_LOADING:
            return state.set( 'regenerateLoading', action.payload );
        case SET_ANNOUNCEMENT_LOADING:
            return state.set( 'announcementLoading', action.payload );
        case SET_NOTIFICATION:
            return state.set( 'notification', fromJS( action.payload ) );
        case SET_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_ANNOUNCEMENTS_PAGINATION:
            return state.set( 'announcementPagination', fromJS( action.payload ) );
        case SET_STATISTICS:
            return state.set( 'statistics', fromJS( action.payload ) );
        case SET_CALENDAR_DAY_DATA:
            return state.set( 'day', fromJS( action.payload ) );
        case SET_CALENDAR_MONTH_DATA:
            return state.set( 'month', fromJS( action.payload ) );
        case SET_ANNOUNCEMENTS:
            return state.set( 'announcements', fromJS( action.payload ) );
        case SET_TABLE_PAGINATION:
            return state.set( 'pagination', fromJS( action.payload ) );
        case SET_EMPLOYEE_TABLE_DATA:
            return state.set( 'statistics_table', fromJS( action.payload ) );
        case SET_SHIFT_EMPLOYEES_PAGINATION:
            return state.set( 'shiftEmployeesPagination', fromJS( action.payload ) );
        case SET_SHIFT_EMPLOYEES:
            return state.set( 'shiftEmployees', fromJS( action.payload ) );
        default:
            return state;
    }
}

export default dashboardReducer;
