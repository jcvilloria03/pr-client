import arrow from './arrow.svg';
import ban from './ban.svg';
import batchUpload from './batch-upload.svg';
import bell from './bell.svg';
import building from './building.svg';
import bullhorn from './bullhorn.svg';
import calendar from './calendar.svg';
import calendarO from './calendar-o.svg';
import caret from './caret.svg';
import caretCircle from './caret-circle.svg';
import carets from './carets.svg';
import checkCircle from './check-circle.svg';
import chevron from './chevron.svg';
import circleONotch from './circle-o-notch.svg';
import clock from './clock.svg';
import cog from './cog.svg';
import cogs from './cogs.svg';
import deviceManagement from './device-management.svg';
import dollarSign from './dollar-sign.svg';
import employees from './employees.svg';
import filesOText from './files-o-text.svg';
import filter from './filter.svg';
import fingerprint from './fingerprint.svg';
import globe from './globe.svg';
import handHoldingUSD from './hand-holding-usd.svg';
import hierarchy from './hierarchy.svg';
import home from './home.svg';
import imageSymbol from './image-symbol.svg';
import infoCircle from './info-circle.svg';
import ipAddress from './ip-address.svg';
import levels from './levels.svg';
import list from './list.svg';
import listCheck from './list-check.svg';
import lock from './lock.svg';
import lockClose from './lock-close.svg';
import lockOpen from './lock-open.svg';
import manualEntry from './manual-entry.svg';
import minusCircle from './minus-circle.svg';
import minusSquareO from './minus-square-o.svg';
import payroll from './payroll.svg';
import pencil from './pencil.svg';
import plus from './plus.svg';
import plusCircle from './plus-circle.svg';
import plusSquareO from './plus-square-o.svg';
import question from './question.svg';
import remove from './remove.svg';
import regenerate from './regenerate.svg';
import revealPw from './reveal-pw.svg';
import search from './search.svg';
import signOut from './sign-out.svg';
import sortArrows from './sort-arrows.svg';
import tag from './tag.svg';
import textBlock from './text-block.svg';
import tick from './tick.svg';
import tickCircle from './tick-circle.svg';
import time from './time.svg';
import trash from './trash.svg';
import userCircle from './user-circle.svg';
import userCircleInverted from './user-circle-inverted.svg';
import userGroup from './user-group.svg';
import users from './users.svg';
import unauthorized from './unauthorized.svg';
import warning from './warning.svg';
import worldGrid from './world-grid.svg';
import envelope from './envelope.svg';
import creditCard from './credit-card.svg';
import time2 from './time2.svg';
import circleRepo from './circle-repo.svg';
import flag from './flag-repo.svg';
import sendIcon from './sendIcon.svg';
import reply from './reply.svg';
import sort from './sort.svg';
import next from './next.svg';
import back from './back.svg';
import download from './download.svg';
import greenArrow from './green-up-arrow.svg';
import redArrow from './red-down-arrow.svg';
import handMoneyBag from './hand-money-bag.svg';
import moneyBag from './money-bag.svg';
import money from './money.svg';
import percentage from './percentage.svg';
import minusDollar from './minus-dollar.svg';
import plusCircleOutline from './plus-circle-outlined.svg';
import funnel from './funnel.svg';
import timesCircle from './times-circle.svg';
import commenting from './commenting.svg';
import dot from './dot.svg';
import spinner from './spinner.svg';

export default {
    arrow,
    ban,
    batchUpload,
    bell,
    building,
    bullhorn,
    calendar,
    calendarO,
    caret,
    caretCircle,
    carets,
    checkCircle,
    chevron,
    circleONotch,
    clock,
    cog,
    cogs,
    deviceManagement,
    dollarSign,
    employees,
    filesOText,
    filter,
    fingerprint,
    globe,
    handHoldingUSD,
    hierarchy,
    home,
    imageSymbol,
    infoCircle,
    ipAddress,
    levels,
    list,
    listCheck,
    lock,
    lockClose,
    lockOpen,
    manualEntry,
    minusCircle,
    minusSquareO,
    payroll,
    pencil,
    plus,
    plusCircle,
    plusSquareO,
    question,
    remove,
    regenerate,
    revealPw,
    search,
    signOut,
    sortArrows,
    tag,
    textBlock,
    tick,
    tickCircle,
    time,
    trash,
    unauthorized,
    userCircle,
    userCircleInverted,
    userGroup,
    users,
    warning,
    worldGrid,
    envelope,
    creditCard,
    time2,
    circleRepo,
    flag,
    sendIcon,
    reply,
    download,
    sort,
    next,
    back,
    greenArrow,
    redArrow,
    handMoneyBag,
    moneyBag,
    money,
    percentage,
    minusDollar,
    plusCircleOutline,
    funnel,
    timesCircle,
    commenting,
    dot,
    spinner
};
