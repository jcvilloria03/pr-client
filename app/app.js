/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

// Needed for redux-saga es6 generator support
import 'babel-polyfill';

// Import all the third party stuff
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyRouterMiddleware, Router, browserHistory as routerBrowserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { useScroll } from 'react-router-scroll';
import each from 'lodash/each';
import 'sanitize.css/sanitize.css';
// import axios from 'axios';
// import { onboarding } from 'utils/OnboardingService';

// Import root app
import App from 'containers/App';

// Import AuthService
import { auth } from 'utils/AuthService';
import { isTokenExpired } from 'utils/jwtHelper';

// Import SubscriptionService
import { subscriptionService } from 'utils/SubscriptionService';

// Import selector for `syncHistoryWithStore`
import { makeSelectLocationState } from 'containers/App/selectors';
import IdleTimerContainer from 'components/IdleTimer';

// Import Language Provider
import LanguageProvider from 'containers/LanguageProvider';

// Load the favicon, the manifest.json file and the .htaccess file
/* eslint-disable import/no-unresolved, import/extensions, import/no-webpack-loader-syntax */
import '!file-loader?name=[name].[ext]!./favicon.png';
import '!file-loader?name=[name].[ext]!./manifest.json';
import 'file-loader?name=[name].[ext]!./.htaccess';
/* eslint-enable import/no-unresolved, import/extensions, import/no-webpack-loader-syntax */

import configureStore from './store';

// Import i18n messages
import { translationMessages } from './i18n';

// Import CSS reset and Global Styles
import './global-styles';

// Import root routes
import createRoutes from './routes';
import { setRootRoutes, getMatchedRoutes, getUserPermissions, injectStore } from './utils/request';
import modulesMap from './modules-map';

import { UNPROTECTED_ROUTES, BASE_PATH_NAME } from './constants';

// Create redux store with history
// this uses the singleton browserHistory provided by react-router
// Optionally, this could be changed to leverage a created history
// e.g. `const browserHistory = useRouterHistory(createBrowserHistory)();`
const initialState = {};
const store = configureStore( initialState, routerBrowserHistory );
injectStore( store );

// Sync history and store, as the react-router-redux reducer
// is under the non-default key ("routing"), selectLocationState
// must be provided for resolving how to retrieve the "route" in the state
const history = syncHistoryWithStore( routerBrowserHistory, store, {
    selectLocationState: makeSelectLocationState()
});

const requireAuth = ( nextState, replace ) => {
    const profile = JSON.parse( localStorage.getItem( 'user' ) );
    if ( !auth.loggedIn() ) {
        localStorage.clear();
        if ( profile ) {
            localStorage.setItem( `login_redirect:${profile.id}`, nextState.location.pathname );
        } else {
            if ( nextState.location.pathname !== '/' ) {
                localStorage.setItem( 'login_redirect', nextState.location.pathname );
            }
            const payrollDownloadRegex = new RegExp( '/payroll/download', 'i' );
            if ( payrollDownloadRegex.test( nextState.location.pathname ) ) {
                localStorage.setItem( 'download_params', JSON.stringify( nextState.location.query ) );
            }
        }

        localStorage.clear();
    } else if ( !isTokenExpired( localStorage.getItem( 'access_token' ) ) ) {
        handleValidToken( profile, nextState, replace );
        const masterfileDownloadRegex = new RegExp( '/employees/download', 'i' );
        if ( masterfileDownloadRegex.test( nextState.location.pathname ) ) {
            localStorage.setItem( 'download_params', JSON.stringify( nextState.location.query ) );
        }
    } else {
        const userID = profile.id;
        localStorage.clear();
        localStorage.setItem( `login_redirect:${userID}`, nextState.location.pathname );
    }
};

/**
 * Route guard for checking if user have valid permssion before entering the page
 * @returns Boolean
 */
async function checkPermission() {
    const { routes: matchedRoutes } = await getMatchedRoutes();

    if ( matchedRoutes !== undefined ) {
        const isAuthorized = [];
        const { path } = matchedRoutes[ 0 ];
        const endpoints = modulesMap[ path ];
        const authzPermissions = await getUserPermissions();

        each( endpoints, ( endpoint ) => {
            each( endpoint, ( items ) => {
                each( items.modules, ( permission ) => {
                    const authorized = authzPermissions.includes( permission );

                    isAuthorized.push( authorized );
                });
            });
        });

        if ( !isAuthorized.every( Boolean ) && path !== '/change-password' ) {
            window.location.replace( `${BASE_PATH_NAME}/unauthorized` );
        }
    }
}

/**
 * Route guard for handling user with valid token
 * @param {Object} profile - Salarium profile object
 * @param {Object} nextState - Router next state
 * @param {Function} replace - Callback function for replacing router state
 */
async function handleValidToken( profile, nextState, replace ) {
    await checkPermission();

    if ( auth.isFreshAccount() ) {
        nextState.location.pathname !== '/change-password' && replace({ pathname: '/change-password' });
    } else if ( !nextState.location.pathname.match( new RegExp( '/control-panel/subscriptions', 'gi' ) ) && auth.isExpired() ) {
        if ( !nextState.location.pathname.match( new RegExp( `${BASE_PATH_NAME}/guides/subscriptions/update`, 'gi' ) ) ) {
            replace({ pathname: '/control-panel/subscriptions' });
        }
    } else if ( nextState.location.pathname.match( new RegExp( '/control-panel/subscriptions/receipts', 'gi' ) ) && subscriptionService.isTrial() ) {
        replace({ pathname: '/control-panel/subscriptions/summary' });
    } else if ( nextState.location.pathname === '/verify' ) {
        window.location.replace( '/dashboard' );
    }
}

// add onEnter for adding validation for protected pages
const addProtectedRoutes = ( routes ) => {
    const protectedRoutes = [];
    routes.forEach( ( route, index ) => {
        if ( UNPROTECTED_ROUTES.includes( route.name ) ) {
            protectedRoutes[ index ] = route;
        } else {
            protectedRoutes[ index ] = Object.assign( route, {
                onEnter: requireAuth
            });
        }
    });
    return protectedRoutes;
};

// Set up the router, wrapping all Routes in the App component
const rootRoute = {
    component: App,
    childRoutes: addProtectedRoutes( createRoutes( store ) )
};

setRootRoutes( rootRoute );

const render = ( messages ) => {
    ReactDOM.render(
        <div>
            <Provider store={ store }>
                <div>
                    <IdleTimerContainer></IdleTimerContainer>
                    <LanguageProvider messages={ messages }>
                        <Router
                            history={ history }
                            routes={ rootRoute }
                            render={
                                // Scroll to top when going to a new page, imitating default browser
                                // behaviour
                                applyRouterMiddleware( useScroll() )
                            }
                        />
                    </LanguageProvider>
                </div>
            </Provider>
        </div>,
    document.getElementById( 'app' )
  );
};

// Hot reloadable translation json files
if ( module.hot ) {
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
    module.hot.accept( './i18n', () => {
        render( translationMessages );
    });
}

// Chunked polyfill for browsers without Intl support
if ( !window.Intl ) {
    ( new Promise( ( resolve ) => {
        resolve( import( 'intl' ) );
    }) )
    .then( () => Promise.all([
        import( 'intl/locale-data/jsonp/en.js' )
    ]) )
    .then( () => render( translationMessages ) )
    .catch( ( err ) => {
        throw err;
    });
} else {
    render( translationMessages );
}

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if ( process.env.NODE_ENV === 'production' ) {
    require( 'offline-plugin/runtime' ).install(); // eslint-disable-line global-require
}
