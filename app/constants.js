/**
 * Global app constants
 */
export const BASE_PATH_NAME = '/payroll';

export const BASE_PATH_NAME_MF = '/employees';

export const LOGIN_PAGE_SNACKBAR_MESSAGE = 'login_page_snackbar_message';

export const UNPROTECTED_ROUTES = [
    'Guides',
    'Employee Guide',
    'Employee Batch Upload Guide',
    'Payroll Guide',
    'Payroll Generate Guide Index',
    'Payroll Generate Import Guide',
    'Payroll Generate Guide',
    'Unauthorized',
    'Sign Up',
    'Login',
    'Welcome',
    'Not Found',
    'Forgot Password',
    'Reset Password'
];

export const AUTHZ_PERMISSIONS = 'authz_permissions';

export const TRACKING_ID = process.env.TRACKING_ID;
export const TAG_MANAGER_ARGS = {
    gtmId: process.env.GTMID
};

export const INLINEMANUAL_API_KEY = process.env.INLINEMANUAL_API_KEY;

export const STRIPE_PUB_KEY = process.env.STRIPE_PUB_KEY;
export const GMAPS_KEY = process.env.GMAPS_KEY;

export const CARD_ELEMENT_OPTIONS = {
    iconStyle: 'solid',
    hidePostalCode: true,
    style: {
        base: {
            iconColor: '#666EE8',
            color: '#31325F',
            lineHeight: '40px',
            fontWeight: 300,
            fontSize: '15px',
            fontFamily: 'Roboto',

            '::placeholder': {
                color: '#CFD7E0'
            }
        },
        invalid: {
            color: '#e5424d',
            fontFamily: 'Roboto',
            ':focus': {
                color: '#303238'
            }
        }
    }
};

export const DATE_FORMATS = {
    DEFAULT: 'YYYY-MM-DD',
    HOLIDAYS_DISPLAY: 'MMMM DD, YYYY',
    REQUEST_END_DATE_DISPLAY: 'DD, YYYY',
    SCHEDULES_DISPLAY: 'MM/DD/YYYY',
    SHIFTS_DISPLAY: 'MMMM D, YYYY',
    DATE_TIME: 'MMMM DD, YYYY - HH:mm',
    MESSAGING: 'HH:mm MMMM DD, YYYY',
    CLOCK: 'HH:mm:ss',
    HOURS_AND_MINUTES: 'HH:mm'
};

export const MAIN_PAGE_PERMISSION = 'main_page';
