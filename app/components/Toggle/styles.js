import styled from "styled-components";

export const Wrapper = styled.div`
    * {
        box-sizing: border-box;
        position: relative;
        margin: 0px;
        padding: 0px;
        outline: none;
    }

    .rockerSwitch {
        color: #333;
        width: 100%;
        min-width: 500px;
        border-radius: 6px;
        overflow: hidden;
        margin: auto;
        background: #fff;
        border: 1px solid #f6f6f6;
        border-radius: 33px;
    }
    .rockerSwitch:after {
        content: "or";
        position: absolute;
        width: 38px;
        height: 38px;
        left: 50%;
        top: 50%;
        margin-left: -20px;
        margin-top: -20px;
        border-radius: 50%;
        background: #fff;
        color: #000;
        border: 1px solid #f6f6f6;
        font-size: 14px;
        line-height: 38px;
        text-align: center;
        vertical-align: middle;
    }
    .rockerSwitch > a {
        color: #000 !important;
        background: #fff;
        padding: 10px 50px;
        width: 50%;
        height: 100px;
        cursor: pointer;
        text-align: center;
        font-size: 28px;
        display: flex;
        align-items: center;
    }
    .rockerSwitch > a:first-child {
        float: left;
        -moz-background-clip: padding;
        -webkit-background-clip: padding;
        background-clip: padding-box;
    }
    .rockerSwitch > a:last-child {
        float: right;
        border-left: 1px solid #f6f6f6;
        -moz-background-clip: padding;
        -webkit-background-clip: padding;
        background-clip: padding-box;
    }
    .rockerSwitch > a:hover,
    .rockerSwitch > a:focus {
        background: #19bfff;
        color: #fff !important;
        text-decoration: none;

        svg {
            defs {
                
            }

            g {
                > rect {
                    fill: #fff;
                }
        
                .stroke {
                    stroke: #fff !important;
                    fill: none !important;
                }
            }
        }
    }
    .rockerSwitch > a.selected {
        background: #19bfff;
        color: #fff !important;
    }

    .rockerSwitch > a > div {
        text-align: left;
        color: inherit;
    }

    .rockerSwitch > a > div > .title {
        font-size: 16px;
        font-weight: 600;
        color: inherit;
    }

    .rockerSwitch > a > div > .sub {
        font-size: 14px;
        font-weight: 400;
        color: inherit;
    }

    .icon {
        display: flex;
        margin-right: 10px;
    }

    .icon svg {
        width: 60px;
        height: 60px;
    }
`;
