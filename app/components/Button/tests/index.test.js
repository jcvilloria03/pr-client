import React from 'react';
import { shallow, mount } from 'enzyme';
import * as router from 'react-router';
import sinon from 'sinon';
import Button from '../index';

describe( '<Button />', () => {
    it( 'should have prop key "type" and the default return value is "primary"', () => {
        const component = shallow(
            <Button />
        );
        const props = component.instance().props;
        expect( props.type ).toEqual( 'primary' );
    });

    it( 'should go the the assign url if was clicked', () => {
        const handleClick = jest.fn();
        const renderedComponent = mount(
            <Button
                alt
                onClick={ handleClick }
            />
        );
        renderedComponent.find( 'button' ).simulate( 'click' );
        expect( handleClick ).toHaveBeenCalled();
    });

    it( 'should render an alternative color if alt prop is set to true', () => {
        const renderedComponent = shallow(
            <Button alt />
        );
        const props = renderedComponent.instance().props;
        expect( props.alt ).toEqual( true );
    });

    it( 'should have a prop key "label" and the default return value is "Button"', () => {
        const component = shallow(
            <Button />
        );
        const props = component.instance().props;
        expect( props.label ).toEqual( 'Button' );
        expect( component.render().find( 'button.btn' ).text() ).toEqual( 'Button' );
    });

    it( 'should have a prop key "size" and the default return value is "default"', () => {
        const component = shallow(
            <Button />
        );
        const props = component.instance().props;
        expect( props.size ).toEqual( 'default' );
    });

    it( 'should have a prop key "disabled" and the default return value is "false"', () => {
        const component = shallow(
            <Button />
        );
        const props = component.instance().props;
        expect( props.disabled ).toEqual( false );
    });

    it( 'should have a prop key "block" and return value is "false"', () => {
        const component = shallow(
            <Button />
        );
        const props = component.instance().props;
        expect( props.block ).toEqual( false );
    });

    it( 'should have a prop key "to" and return value is empty string', () => {
        const component = shallow(
            <Button />
        );
        const props = component.instance().props;
        expect( props.to ).toEqual( '' );
    });

    it( 'should find a button tag and a class name of "btn"', () => {
        const component = shallow(
            <Button />
        );
        expect( component.render().find( 'button.btn' ).length ).toEqual( 1 );
    });

    it( 'should simulates a click event', () => {
        const handleClick = jest.fn();
        const renderedComponent = mount(
            <Button onClick={ handleClick } />
        );
        renderedComponent.find( 'button' ).simulate( 'click' );
        expect( handleClick ).toHaveBeenCalled();
    });

    it( 'should handle routing if to props is available', () => {
        const basePath = '/payroll';
        router.browserHistory = { push: () => { } };

        const mockPush = jest.fn();
        const browserHistoryPushStub = sinon.stub(
            router.browserHistory,
            'push',
            mockPush
        );

        const spy = sinon.spy( Button.prototype, 'handleRoute' );
        const renderedComponent = mount(
            <Button to="/" />
        );
        renderedComponent.instance().handleRoute();
        expect( spy.calledOnce ).toEqual( true );
        expect( browserHistoryPushStub.calledOnce ).toEqual( true );
        expect( mockPush ).toHaveBeenCalledWith( `${basePath}/` );

        renderedComponent.setProps({ to: [ '/foo', true ]});
        renderedComponent.instance().handleRoute();
        expect( browserHistoryPushStub.calledTwice ).toEqual( true );
        expect( mockPush ).toHaveBeenCalledWith( '/foo' );

        browserHistoryPushStub.restore();
    });
});
