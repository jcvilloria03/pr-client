import { css } from 'styled-components';
import { button } from './constant';

/**
 * it will return the required styling for
 * the styled Button component.
 */
export const styleProps = ( props ) => {
    const padding = button[ props.size ].padding;
    const fontSize = button[ props.size ].fontSize;
    const fontWeight = button[ props.size ].fontWeight;
    const borderRadius = button[ props.size ].borderRadius;
    const backgroundColor = props.alt ? button[ props.type ].alt.normalState.default : button[ props.type ].normalState.default;
    const fontColor = props.alt ? button[ props.type ].alt.normalState.fontColor : button[ props.type ].normalState.fontColor;
    const borderColor = props.alt ? button[ props.type ].alt.normalState.borderColor : button[ props.type ].normalState.borderColor;
    const backgroundColorHovered = props.alt ? button[ props.type ].alt.hovered.default : button[ props.type ].hovered.default;
    const fontColorHovered = props.alt ? button[ props.type ].alt.hovered.fontColor : button[ props.type ].hovered.fontColor;
    const borderColorHovered = props.alt ? button[ props.type ].alt.hovered.borderColor : button[ props.type ].hovered.borderColor;
    const disabledBackground = props.alt ? button[ props.type ].alt.disabled.default : button[ props.type ].disabled.default;
    const disabledFontColor = props.alt ? button[ props.type ].alt.disabled.fontColor : button[ props.type ].disabled.fontColor;
    const disabledBorderColor = props.alt ? button[ props.type ].alt.disabled.borderColor : button[ props.type ].disabled.borderColor;

    return css`
        background-color: ${backgroundColor};
        color: ${fontColor};
        border-color: ${borderColor};
        padding: ${padding};
        border-radius: ${borderRadius};
        font-weight: ${fontWeight};
        font-size: ${fontSize};
        line-height: 1.5;
        display: inline;
        vertical-align: middle;
        margin: 2px;
        transition: all .2s ease-in-out;

        &:hover, &:focus, &:active:focus {
            outline: none;
            background-color: ${backgroundColorHovered};
            color: ${fontColorHovered};
            border-color: ${borderColorHovered};
        }

        &:disabled, &:disabled:hover, &:disabled:focus {
            background-color: ${disabledBackground};
            color: ${disabledFontColor};
            border-color: ${disabledBorderColor};
            opacity: 1;
        }
    `;
};
