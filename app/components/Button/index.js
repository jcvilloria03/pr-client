import React from 'react';
import styled from 'styled-components';
import { Button as ReactStrapButton } from 'reactstrap';
import { browserHistory } from '../../utils/BrowserHistory';
import { styleProps } from './styles';

const StyledButton = styled( ReactStrapButton )`
    ${styleProps}
`;

/**
* Button
*/
export default class Button extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        className: React.PropTypes.string,
        type: React.PropTypes.oneOf([
            'primary', 'danger', 'neutral', 'action', 'grey', 'darkRed', 'cancel'
        ]),
        size: React.PropTypes.oneOf([
            'large', 'default', 'small', 'mini'
        ]),
        block: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        label: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.object,
            React.PropTypes.element,
            React.PropTypes.node
        ]).isRequired,
        onClick: React.PropTypes.func,
        to: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.array
        ]),
        alt: React.PropTypes.bool,
        style: React.PropTypes.object
    };
    static defaultProps = {
        type: 'primary',
        size: 'default',
        label: 'Button',
        block: false,
        disabled: false,
        to: '',
        alt: false
    };

    /**
    * constructor()
    */
    constructor( props ) {
        super( props );
        this.handleClick = this.handleClick.bind( this );
    }
    /**
    * this will only execute if the given default onClick
    * is defined and valid
    */
    onButtonClick() {
        const { onClick } = this.props;
        typeof onClick === 'function' && onClick && onClick();
    }
    /**
    * this method will check base the required props and if
    * conditions are meant it will push a new browser history.
    */
    handleRoute() {
        const { to } = this.props;
        if ( !to ) return;

        if ( Array.isArray( to ) ) {
            browserHistory.push( ...to );
        } else {
            browserHistory.push( to );
        }
    }
    /**
    * this method will execute the functions if
    * the required props is given
    */
    handleClick( e ) {
        e.preventDefault();
        this.onButtonClick();
        this.handleRoute();
    }
    /**
    * it will return a Button or a Anchor component base on the prop.type
    */
    render() {
        const { label, type, size, block, style } = this.props;
        return (
            <StyledButton
                className={ this.props.className }
                onClick={ this.handleClick }
                type={ type }
                size={ size }
                block={ block }
                disabled={ this.props.disabled }
                alt={ this.props.alt }
                style={ style }
            >
                { label }
            </StyledButton>
        );
    }
}
