export const button = {
    large: {
        padding: '14px 28px',
        fontSize: '14px',
        borderRadius: '25px'
    },
    large2: {
        padding: '.5rem 1rem',
        fontSize: '14px',
        borderRadius: '4px'
    },
    default: {
        padding: '7px 21px',
        fontSize: '14px',
        borderRadius: '20px'
    },
    small: {
        padding: '3.5px 14px 3.5px 14px',
        fontSize: '12px',
        borderRadius: '15px'
    },
    mini: {
        padding: '3px 6px 3px 6px',
        fontSize: '10px',
        borderRadius: '10px'
    },
    primary: {
        normalState: {
            fontColor: '#ffffff',
            default: '#00A5E5',
            borderColor: '#00A5E5'
        },
        hovered: {
            fontColor: '#ffffff',
            default: '#0086b7',
            borderColor: '#0086b7'
        },
        disabled: {
            default: '#7ccfef',
            fontColor: '#ecf6fa',
            borderColor: '#7ccfef'
        },
        alt: {
            normalState: {
                fontColor: '#474747',
                default: '#ffffff',
                borderColor: '#00A5E5'
            },
            hovered: {
                fontColor: '#0086b7',
                default: '#ffffff',
                borderColor: '#0086b7'
            },
            disabled: {
                default: '#ffffff',
                fontColor: '#7ccfef',
                borderColor: '#7ccfef'
            }
        }
    },
    danger: {
        normalState: {
            fontColor: '#ffffff',
            default: '#F21108',
            borderColor: '#F21108'
        },
        hovered: {
            fontColor: '#ffffff',
            default: '#c20c06',
            borderColor: '#c20c06'
        },
        disabled: {
            default: '#f58480',
            fontColor: '#fbf4f4',
            borderColor: '#f58480'
        },
        alt: {
            normalState: {
                fontColor: '#474747',
                default: '#ffffff',
                borderColor: '#F21108'
            },
            hovered: {
                fontColor: '#c20c06',
                default: '#ffffff',
                borderColor: '#c20c06'
            },
            disabled: {
                default: '#fff',
                fontColor: '#f58480',
                borderColor: '#f58480'
            }
        }
    },
    grey: {
        normalState: {
            fontColor: '#474747',
            default: 'rgb(240, 244, 246)',
            borderColor: 'rgb(173, 173, 173)'
        },
        hovered: {
            fontColor: '#474747',
            default: '#fff',
            borderColor: 'rgb(173, 173, 173)'
        },
        disabled: {
            default: 'transparent',
            fontColor: '#adadad',
            borderColor: '#e2e3e3'
        }
    },
    cancel: {
        normalState: {
            fontColor: '#ffffff',
            default: '#999999',
            borderColor: 'rgb(173, 173, 173)'
        },
        hovered: {
            fontColor: '#474747',
            default: '#999999d1',
            borderColor: 'rgb(173, 173, 173)'
        },
        disabled: {
            default: 'transparent',
            fontColor: '#979393',
            borderColor: '#e2e3e3'
        }
    },
    neutral: {
        normalState: {
            fontColor: '#474747',
            default: '#ffffff',
            borderColor: '#9fdc74'
        },
        hovered: {
            fontColor: '#474747',
            default: '#ffffff',
            borderColor: '#9fdc74'
        },
        disabled: {
            default: '#ffffff',
            fontColor: '#adadad',
            borderColor: '#e2e3e3'
        },
        alt: {
            normalState: {
                fontColor: '#474747',
                default: '#ffffff',
                borderColor: '#9fdc74'
            },
            hovered: {
                fontColor: '#474747',
                default: '#ffffff',
                borderColor: '#9fdc74'
            },
            disabled: {
                default: '#ffffff',
                fontColor: '#adadad',
                borderColor: '#e2e3e3'
            }
        }
    },
    action: {
        normalState: {
            fontColor: '#ffffff',
            default: '#83d24b',
            borderColor: '#83d24b'
        },
        hovered: {
            fontColor: '#ffffff',
            default: '#9fdc74',
            borderColor: '#9fdc74'
        },
        disabled: {
            default: '#ade188',
            fontColor: '#ecf6fa',
            borderColor: '#ade188'
        },
        alt: {
            normalState: {
                fontColor: '#474747',
                default: '#ffffff',
                borderColor: '#9fdc74'
            },
            hovered: {
                fontColor: '#9fdc74',
                default: '#ffffff',
                borderColor: '#9fdc74'
            },
            disabled: {
                default: '#ffffff',
                fontColor: '#474747',
                borderColor: '#9fdc74'
            }
        }
    },
    darkRed: {
        normalState: {
            fontColor: '#ffffff',
            default: '#EB7575',
            borderColor: '#EB7575'
        },
        hovered: {
            fontColor: '#ffffff',
            default: '#f1a2a2',
            borderColor: '#f1a2a2'
        },
        disabled: {
            default: '#7ccfef',
            fontColor: '#ecf6fa',
            borderColor: '#7ccfef'
        },
        alt: {
            normalState: {
                fontColor: '#474747',
                default: '#ffffff',
                borderColor: '#EB7575'
            },
            hovered: {
                fontColor: '#f1a2a2',
                default: '#ffffff',
                borderColor: '#f1a2a2'
            },
            disabled: {
                default: '#ffffff',
                fontColor: '#7ccfef',
                borderColor: '#7ccfef'
            }
        }
    }
};
