import styled from 'styled-components';

export const DropdownWrapper = styled.div`
    .sl-c-dropdown-border-solid {
        border: 1px solid #ccc;
        border-radius: 2px;
        padding: 0.5rem 1rem;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;

        .icon {
          font-size: 15px;
          color: white;
          display: flex;
          width: 15px;
          margin-right: 0.5rem;

          > i {
              align-self: center;
          }
        }
    }
    .sl-c-text-align-right {
        align-self: right;
    }
    .sl-c-gap-top {
        margin-top: 10px;
        min-width: 135px;
    }
`;
