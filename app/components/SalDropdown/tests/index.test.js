import { shallow, mount } from 'enzyme';
import React from 'react';
import SalDropdown from '../index';

describe( '<SalDropdown />', () => {
    const callback = jest.fn();
    const dropdownItems = [
        {
            label: 'Delete',
            children: <div>Delete</div>,
            onClick: callback
        },
        {
            label: 'Download',
            children: <div >Download</div>,
            onClick: callback
        }
    ];

    it( 'should mount the component', () => {
        const component = shallow(
            <SalDropdown dropdownItems={ dropdownItems } />
        );

        expect( component.find( 'SalDropdown' ) ).toBeTruthy();
        expect( component.instance().state.isCollapsed ).toBeFalsy();
        expect( component.instance().state.modal ).toBeFalsy();
    });

    it( 'should handle toggle', () => {
        const component = mount(
            <SalDropdown dropdownItems={ dropdownItems } />
        );

        component.instance().toggle();
        expect( component.instance().state.isCollapsed ).toBeTruthy();
    });
});
