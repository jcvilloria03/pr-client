import React from 'react';
import { Dropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap';
import Icon from '../Icon';
import {
    DropdownWrapper
} from './styles';

/**
 *
 * SalDropdown component
 *
 */
class SalDropdown extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        new: React.PropTypes.bool,
        icon: React.PropTypes.string,
        backgroundColor: React.PropTypes.string,
        textColor: React.PropTypes.string,
        label: React.PropTypes.string,
        dropdownItems: React.PropTypes.array
    }

    static defaultProps = {
        icon: '',
        label: 'More actions',
        textColor: 'white',
        backgroundColor: '#83d24b'
    };

    /**
     * Component Constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            modal: false,
            isCollapsed: false
        };

        this.toggle = this.toggle.bind( this );
    }

    /**
     * Toggles dropdown Visibility
     */
    toggle() {
        this.setState({
            isCollapsed: !this.state.isCollapsed
        });
    }

    /**
     *
     * Modal
     *
     */
    render() {
        const { label, icon } = this.props;
        return (
            <DropdownWrapper>
                <Dropdown
                    isOpen={ this.state.isCollapsed }
                    onClick={ this.toggle }
                    toggle={ this.toggle }
                >
                    <DropdownToggle
                        tag="span"
                        data-toggle="dropdown"
                        className="sl-c-dropdown-border-solid"
                        caret
                        style={ {
                            background: this.props.new ? this.props.backgroundColor : 'none',
                            color: this.props.new ? this.props.textColor : 'none'
                        } }
                    >
                        {icon !== '' && <Icon name={ icon } className="icon" />}
                        <span className="sl-c-text-align-right">{ label }</span>
                    </DropdownToggle>
                    <DropdownMenu className="sl-c-gap-top">
                        {
                            this.props.dropdownItems.map( ( item, index ) => (
                                <DropdownItem
                                    key={ index }
                                    onClick={ item.onClick }
                                    disabled={ item.disabled }
                                >
                                    {item.children}
                                </DropdownItem>
                            ) )
                        }
                    </DropdownMenu>
                </Dropdown>
            </DropdownWrapper>
        );
    }
}

export default SalDropdown;
