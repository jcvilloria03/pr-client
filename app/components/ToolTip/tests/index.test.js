import React from 'react';
import { shallow } from 'enzyme';
import ToolTip from '../index';

let component;

const content = 'I\'m the content';
const id = 'test';
const target = 'HOVER ME';

describe( '<ToolTip />', () => {
    beforeEach( () => {
        component = shallow(
            <ToolTip target={ target } id={ id }>
                { content }
            </ToolTip>
        );
    });

    it( 'should toggle tooltip', () => {
        component.instance().toggle();
        const state = component.instance().state;
        expect( state.tooltipOpen ).toEqual( true );
    });

    it( 'should mount the component', () => {
        expect( component.html() ).toBeTruthy();
    });

    it( 'should toggle tooltip on and off', () => {
        component.setState({ tooltipOpen: true });
        expect( component.state( 'tooltipOpen' ) ).toEqual( true );
    });

    it( 'should have a default state key "tooltipOpen" and default value is set to "false"', () => {
        const state = component.instance().state;
        expect( state.tooltipOpen ).toEqual( false );
    });

    it( 'should have a prop key "placement" and default value is "bottom"', () => {
        const props = component.instance().props;
        expect( props.placement ).toEqual( 'bottom' );
    });
});
