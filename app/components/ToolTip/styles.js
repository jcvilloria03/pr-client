import { css } from 'styled-components';

/**
 * { styledTarget }
 */
export function styledTarget( props ) { // eslint-disable-line no-unused-vars
    return css`
        font-family: 'Source Sans Pro', sans-serif;
        color: #00A5E5 !important;
        border-bottom: 1px dotted #8B8B8B;
        padding-bottom: 2px;
        letter-spacing: 1px;

        &:hover {
            text-decoration: none;
            color: #00A5E5 !important;
            cursor: help;
        }
    `;
}

/**
 * { styledCustomTip }
 */
export function styledCustomTip( props ) {  // eslint-disable-line no-unused-vars
    return css`
        border-radius: 0;
        font-size: 12px;
        letter-spacing: 1px;
        font-weight: 400;
        text-align: left;

        a {
            font-size: 100%;
            color: #fff;
            text-decoration: none;
            border-bottom: 1px solid #fff;
        }
    `;
}
