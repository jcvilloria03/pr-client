import React from 'react';
import { Tooltip } from 'reactstrap';
import styled from 'styled-components';
import { styledTarget, styledCustomTip } from './styles';

const Target = styled.a`
    ${styledTarget}
`;

const CustomTip = styled( Tooltip )`
    ${styledCustomTip}
`;

/**
* ToolTip
*/
class ToolTip extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        placement: React.PropTypes.string,
        id: React.PropTypes.string.isRequired,
        children: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.node,
            React.PropTypes.element
        ]).isRequired,
        target: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.element,
            React.PropTypes.node
        ]).isRequired,
        delay: React.PropTypes.object
    }

    static defaultProps = {
        placement: 'bottom'
    }

    /**
     * Constructor.
     */
    constructor( props ) {
        super( props );
        this.toggle = this.toggle.bind( this );
        this.state = {
            tooltipOpen: false
        };
    }
    /**
     * toggles the tooltip to show and hide
     */
    toggle() {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        });
    }
    /**
     * ToolTip
     */
    render() {
        return (
            <span>
                <Target
                    id={ this.props.id }
                >
                    { this.props.target }
                </Target>
                <CustomTip
                    placement={ this.props.placement }
                    isOpen={ this.state.tooltipOpen }
                    autohide={ false }
                    target={ this.props.id }
                    toggle={ this.toggle }
                    delay={ this.props.delay }
                >
                    { React.Children.toArray( this.props.children ) }
                </CustomTip>
            </span>
        );
    }
}

export default ToolTip;
