import { shallow } from 'enzyme';
import React from 'react';
import sinon from 'sinon';
import SnackBar from '../index';

describe( '<SnackBar />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <SnackBar message="Hi There!" title="title" show />
        );
        expect( component.html() ).toBeTruthy();
    });

    it( 'can be mounted as visible', () => {
        const component = shallow(
            <SnackBar message="Hi There!" title="title" show />
        );
        expect( component.state( 'showSnackBar' ) ).toEqual( true );
    });

    it( 'can have custom action buttons', () => {
        const callback = jest.fn();
        const component = shallow(
            <SnackBar message="Hi There!" title="title" action={ { label: 'test', callback } } show />
        );
        expect( component.find( '.customAction' ) ).toBeTruthy();
    });

    it( 'can be offseted its position away from top and left sides', () => {
        const component = shallow(
            <SnackBar message="Hi There!" title="title" offset={ { top: 100, left: 100 } } show />
        );
        const bound = component.instance().props.offset;
        expect( bound ).toEqual({ top: 100, left: 100 });
    });

    it( 'can be toggled to view and hide', () => {
        jest.useFakeTimers();
        const component = shallow(
            <SnackBar message="Hi There!" title="title" />
        );
        component.setState({ showSnackBar: true });
        expect( component.html() ).toBeTruthy();

        const hide = sinon.spy( SnackBar.prototype, 'hideSnackbar' );
        const component2 = shallow( <SnackBar message="Hi There!" title="title" /> );
        component2.setProps({ show: true });
        expect( setTimeout.mock.calls.length ).toBe( 1 );
        expect( setTimeout.mock.calls[ 0 ][ 1 ]).toBe( 3000 );
        jest.runAllTimers();
        expect( hide.calledOnce ).toEqual( true );
    });

    it( 'can change type of notification', () => {
        const component = shallow(
            <SnackBar message="Hi There!" title="title" show type="error" />
        );

        expect( component.instance().props.type ).toEqual( 'error' );

        const component2 = shallow(
            <SnackBar message="Hi There!" title="title" show type="warning" />
        );

        expect( component2.instance().props.type ).toEqual( 'warning' );

        const component3 = shallow(
            <SnackBar message="Hi There!" title="title" show type="success" />
        );

        expect( component3.instance().props.type ).toEqual( 'success' );
    });

    it( 'can change type of notification with alternate color', () => {
        const component = shallow(
            <SnackBar message="Hi There!" title="title" show type="error" alt />
        );

        expect( component.instance().props.type ).toEqual( 'error' );
        expect( component.instance().props.alt ).toEqual( true );

        const component2 = shallow(
            <SnackBar message="Hi There!" title="title" show type="warning" alt />
        );

        expect( component2.instance().props.type ).toEqual( 'warning' );
        expect( component2.instance().props.alt ).toEqual( true );

        const component3 = shallow(
            <SnackBar message="Hi There!" title="title" show type="success" alt />
        );

        expect( component3.instance().props.type ).toEqual( 'success' );
        expect( component3.instance().props.alt ).toEqual( true );

        const component4 = shallow(
            <SnackBar message="Hi There!" title="title" show alt />
        );

        expect( component4.instance().props.type ).toEqual( 'general' );
        expect( component4.instance().props.alt ).toEqual( true );
    });
});
