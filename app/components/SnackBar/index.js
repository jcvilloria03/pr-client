import React, { PropTypes } from 'react';
import {
    ActionWrapper,
    Wrapper,
    StyledH5,
    StyledP,
    StyledContent
} from './styles';

/**
 * SnackBar Component
 */
export default class SnackBar extends React.Component {
    static propTypes = {
        message: PropTypes.any,
        title: PropTypes.any,
        show: PropTypes.bool,
        type: PropTypes.string,
        alt: PropTypes.bool,
        action: PropTypes.shape({
            label: PropTypes.string.isRequired,
            callback: PropTypes.func
        }),
        delay: PropTypes.number,
        offset: PropTypes.shape({
            top: PropTypes.number,
            left: PropTypes.number
        }),
        isCustom: PropTypes.bool,
        children: PropTypes.node
    }

    static defaultProps = {
        actionText: 'CLOSE',
        type: 'general',
        delay: 3000,
        isCustom: false
    }

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );
        this.state = {
            showSnackBar: this.props.show
        };

        this.hideSnackbar = this.hideSnackbar.bind( this );
    }

    /**
     * Executes before new props is applied
     */
    componentWillReceiveProps( nextProps ) {
        const { showSnackBar } = this.state;
        showSnackBar !== nextProps.show && this.setState({ showSnackBar: nextProps.show }, () => {
            setTimeout( () => {
                this.hideSnackbar();
            }, this.props.delay );
        });
    }

    /**
     * Hide existing snackbar
     */
    hideSnackbar() {
        this.setState({
            showSnackBar: false
        });
    }

    /**
     * component render method
     */
    render() {
        const { message, title, isCustom, children } = this.props;
        const { showSnackBar } = this.state;

        let background;
        switch ( this.props.type ) {
            case 'success':
                background = this.props.alt ? '#e1f2f1' : '#41BA41';
                break;
            case 'error':
                background = this.props.alt ? '#fbe9e7' : '#F21108';
                break;
            case 'warning':
                background = this.props.alt ? '#fdf8e5' : '#EFAE00';
                break;
            default:
                background = '#FFF';
        }

        let color;
        if ( this.props.alt || ( !this.props.alt && this.props.type === 'general' ) ) {
            color = '#000';
        } else {
            color = '#fff';
        }

        let lastButtonStyle;
        switch ( this.props.type ) {
            case 'success':
                lastButtonStyle = this.props.alt ? { color: '#33cc67' } : { color: '#FFF' };
                break;
            case 'error':
                lastButtonStyle = this.props.alt ? { color: '#f43932' } : { color: '#FFF' };
                break;
            case 'warning':
                lastButtonStyle = this.props.alt ? { color: '#f2bc08' } : { color: '#FFF' };
                break;
            default:
                lastButtonStyle = { color: '#00a5e5' };
        }

        let actionButtonStyle;
        if ( this.props.type === 'general' ) {
            actionButtonStyle = { color: '#000' };
        } else {
            actionButtonStyle = this.props.alt ? { color: 'rgba(0,0,0,.6)' } : { color: 'rgba(255,255,255,.6)' };
        }

        let actionBorder;
        if ( this.props.type === 'general' ) {
            actionBorder = { borderLeft: '1px solid rgba(0, 0, 0, .1)' };
        } else {
            actionBorder = this.props.alt ? { borderLeft: '1px solid rgba(0,0,0,.1)' } : { borderLeft: '1px solid rgba(255, 255, 255, .3)' };
        }

        const action = this.props.action ?
            <ActionWrapper className="customAction" style={ actionBorder } ><button style={ actionButtonStyle } onClick={ this.hideSnackbar }>CLOSE</button><button style={ lastButtonStyle } onClick={ this.props.action.callback }>{ this.props.action.label }</button></ActionWrapper> : <ActionWrapper style={ actionBorder }><button style={ lastButtonStyle } onClick={ this.hideSnackbar }>CLOSE</button></ActionWrapper>;

        const topOffset = this.props.offset && this.props.offset.top ? `${this.props.offset.top}px` : '0px';
        const leftOffset = this.props.offset && this.props.offset.left ? `${this.props.offset.left}px` : '0px';
        const notifWidth = this.props.offset && this.props.offset.left ? `calc(100% - ${this.props.offset.left}px )` : '100%';

        return (
            <Wrapper style={ { top: showSnackBar ? topOffset : '-300px', background, color, left: leftOffset, width: notifWidth } }>
                <StyledContent>
                    { !isCustom ? <div>
                        <StyledH5>{ title }</StyledH5>
                        <StyledP>{ message }</StyledP>
                    </div> : <div>
                        { children }
                    </div>
                    }
                </StyledContent>
                { action }
            </Wrapper>
        );
    }
}
