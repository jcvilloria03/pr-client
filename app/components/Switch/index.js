import React from 'react';
import Toggle from 'rc-switch';
import {
    Wrapper
} from './styles';

/**
* Switch
*/
class Switch extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        new: React.PropTypes.bool,
        checked: React.PropTypes.bool,
        defaultChecked: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        onChange: React.PropTypes.func
    }

    static defaultProps = {
        defaultChecked: false,
        disabled: false,
        new: false
    }

    /**
    * Switch render method.
    */
    render() {
        const { defaultChecked, disabled, onChange, checked } = this.props;
        return (
            <Wrapper new={ this.props.new }>
                <Toggle
                    defaultChecked={ defaultChecked }
                    disabled={ disabled }
                    onChange={ onChange }
                    checked={ checked === undefined ? defaultChecked : checked }
                />
            </Wrapper>
        );
    }
}

export default Switch;
