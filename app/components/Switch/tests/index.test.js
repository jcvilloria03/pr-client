import React from 'react';
import { shallow } from 'enzyme';

import Switch from '../index';

describe( '<Switch />', () => {
    it( 'should have default prop value "className"', () => {
        const component = shallow(
            <Switch />
        );
        expect( component.find( '.rc-switch' ) ).toBeTruthy();
    });
});
