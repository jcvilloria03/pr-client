/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import { CheckboxStyle } from './styles';

/**
* CustomCheckbox
*/
class CustomCheckbox extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        label: React.PropTypes.string,
        onChange: React.PropTypes.func,
        checkValue: React.PropTypes.bool,
        disabled: React.PropTypes.bool
    };

    render() {
        return (
            <CheckboxStyle>
                <label className="checkCust">{this.props.label}
                    <input
                        type="checkbox"
                        onChange={ this.props.onChange }
                        checked={ this.props.checkValue }
                        disabled={ this.props.disabled }
                    />
                    <span className="checkmark"></span>
                </label>
            </CheckboxStyle>
        );
    }
}

export default CustomCheckbox;
