import styled from 'styled-components';

export const CheckboxStyle = styled.div`
    .checkCust {
        display: inline-block;
        position: relative;
        padding-left: 25px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 14px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        input{
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }
        .checkmark {
            position: absolute;
            top: 2px;
            left: 0;
            height: 16px;
            width: 16px;
            background-color: #fff;
            border: 1px solid #474747;
            &:after{
                content: "";
                position: absolute;
                display: none;
            }
        }
    }
    .checkCust:hover input ~ .checkmark {
        border: 1px solid #00A5E5;
    }
    .checkCust:hover input:checked ~ .checkmark {
        border: 1px solid #474747;
    }
    .checkCust input:checked ~ .checkmark:after {
        display: block;
    }
    .checkCust .checkmark:after {
        left: 5px;
        top: 0px;
        width: 5px;
        height: 13px;
        border: solid #00A5E5;
        border-width: 0 2px 2px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
    .checkCust input:disabled ~ .checkmark {
        cursor: not-allowed;
        border: 1px solid #c7c7c7;
        background-color: #f8f8f8;

        &:after {
            border-color: #00A5E5;
        }
    } 
`;
