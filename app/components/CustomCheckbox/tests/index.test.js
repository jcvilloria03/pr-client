import React from 'react';
import { shallow } from 'enzyme';

import CustomCheckbox from '../index';

describe( '<CustomCheckbox />', () => {
    it( 'Expect to have unit tests specified', () => {
        const component = shallow( <CustomCheckbox /> );
        expect( component.find( '.checkCust' ) ).toBeTruthy();
    });
});
