import styled, { css } from 'styled-components';

export const styleProps = ( props ) => css`
    display: flex;
    flex-wrap: wrap;
    pointer-events: ${props.isLoading ? 'none' : 'auto'};

    ${props.new
      ? 'flex-direction: row; justify-content: space-between; align-items: center;'
      : ''
    }

    & > * {
       width: ${props.new ? 'auto' : '100%'}
    }

    .label-wrapper {
      ${props.new
        ? 'width: 20%; margin-right: .65rem;'
        : ''
      }
      label {
        color: #5b5b5b;
        font-size: 14px;
        font-weight: 400;
        order: 1;
        margin-bottom: 4px;
      }
    }

    .select-wrapper {
      ${props.new
        ? 'flex-grow: 1;'
        : ''
      }

      .Select {
          order: 2;
  
      .Select-control {
              border-color: #95989a;
              height: 46px;
  
          .Select-input {
              height: 44px;
              & > input {
                  line-height: 28px;
                  font-size: 1rem;
                  padding: 10px 0;
              }
          }
          .Select-value, .Select-placeholder {
                  line-height: 44px;
                  max-width: calc( 100% - 34px );
              }
  
          .Select-arrow-zone {
                  padding-right: 12px;
                  padding-top: 4px;
              }
          }
  
      .Select-menu-outer {
              margin-top: 2px;
              max-height: none;
              border: none;
              box-shadow: 0px 0px 15px -4px rgba(128,128,128,1);
  
          .Select-menu {
              .Select-option {
                      padding: 12px 16px;
                  }
              }
          }
  
      &.is-open, &.is-focused, &.is-pseudo-focused {
          .Select-control {
                  border-color: #0096d0;
                  box-shadow: none;
                  border-bottom-right-radius: 4px;
                  border-bottom-left-radius: 4px;
              }
          &~label {
                  color: #149ed3;
              }
          }
  
      &.is-disabled {
          .Select-control {
                  cursor: not-allowed;
                  background: #f8f8f8;
                  border-color: #c7c7c7;
              }
          .Select-value-label {
                  color: #999 !important;
              }
          &~label {
                  color: #9da0a1;
              }
          }
  
      &.is-error {
          .Select-control {
                  border-color: #f21108;
              }
  
          &~label {
                  color: #f2130a;
              }
          }
      }
    }


    p {
        order: 3;
        color: #f21108;
        font-size: 13px;
        margin-bottom: 6px;
    }

    span.required {
        color: #eb7575;
    }
`;

export const LoadingPlaceholder = styled.span`
    display: flex;
    align-items: center;

    &&&&&& {
        margin: 0 0 0 -10px;
    }
`;
