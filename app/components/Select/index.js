import React from 'react';
import styled from 'styled-components';
import Select from 'react-select';
import { debounce } from 'lodash';

import BlueLoader from 'assets/loader.svg';

import 'react-select/dist/react-select.css';

import { LoadingPlaceholder, styleProps } from './styles';

const Wrapper = styled.div`
    ${styleProps}
`;

/**
*
* Select
*
*/
class SalSelect extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        new: React.PropTypes.bool,
        style: React.PropTypes.object,
        disabled: React.PropTypes.bool,
        id: React.PropTypes.string.isRequired,
        async: React.PropTypes.bool,
        loadOptions: React.PropTypes.func,
        required: React.PropTypes.bool,
        onChange: React.PropTypes.func,
        data: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                value: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
                    React.PropTypes.string,
                    React.PropTypes.number,
                    React.PropTypes.object
                ]),
                label: React.PropTypes.string,
                disabled: React.PropTypes.bool
            })
        ),
        label: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.element,
            React.PropTypes.symbol,
            React.PropTypes.node
        ]),
        placeholder: React.PropTypes.string,
        value: React.PropTypes.any,
        autoSize: React.PropTypes.bool,
        isLoading: React.PropTypes.bool,
        loadingPlaceholder: React.PropTypes.string,
        searchable: React.PropTypes.bool,
        clearable: React.PropTypes.bool
    };

    static defaultProps = {
        disabled: false,
        value: null,
        new: false,
        loadingPlaceholder: 'Fetching',
        clearable: false
    };

    /**
     * constructor method of the component
     */
    constructor( props ) {
        super( props );
        this.state = {
            errorMessage: 'This field is required',
            error: false,
            value: this.props.value,
            disabled: this.props.disabled
        };

        this._handleChange = this._handleChange.bind( this );
        this._checkRequire = this._checkRequire.bind( this );
    }

    /**
     * handles changes when new props are passed
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.value !== this.props.value && this.setState({ value: nextProps.value });
        nextProps.disabled !== this.props.disabled && this.setState({ disabled: nextProps.disabled });
        nextProps.required !== this.props.required && !nextProps.required && this.setState({ error: false });
    }

    _handleChange = ( value ) => {
        const passed = this._checkRequire( value ? value.value : null );
        if ( passed && this.props.onChange ) {
            this.setState({ value }, () => this.props.onChange( value ) );
        } else {
            this.setState({ value });
        }

        return true;
    }

    _checkRequire = ( value, errorMessage ) => {
        let passed;

        if ( this.props.required ) {
            if ( value === '' || value === null || value === undefined ) {
                this.setState( ( prevState ) => ({
                    error: true,
                    errorMessage: errorMessage || prevState.errorMessage
                }) );

                passed = false;
            } else {
                this.setState({ error: false });

                passed = true;
            }
        } else {
            this.setState({ error: false });

            passed = true;
        }

        return passed;
    }

    /**
     * component render method
     */
    render() {
        const {
            async = false,
             loadOptions = () => null,
             placeholder,
             isLoading,
             loadingPlaceholder,
             clearable
             } = this.props;

        const requiredIndicator = <span className="required">*</span>;

        const label = this.props.required
            ? this.props.new
                ? <span>{ this.props.label } { requiredIndicator }</span>
                : <span>{ requiredIndicator } { this.props.label }</span>
            : this.props.label;

        const labelElement = this.props.label ? <label htmlFor={ this.props.id }>{ label }</label> : '';

        const dropdownImage = <i className="fa fa-caret-down" />;

        const placeholderComponent = loadingPlaceholder && isLoading
            ? (
                <LoadingPlaceholder>
                    <img alt="Loading" src={ BlueLoader } />

                    {loadingPlaceholder}
                </LoadingPlaceholder>
        ) : placeholder;

        const SelectProps = {
            className: this.state.error ? 'is-error' : '',
            disabled: this.state.disabled,
            placeholder: placeholderComponent,
            clearable,
            name: 'form-field-name',
            value: this.state.value,
            options: this.props.data,
            onChange: this._handleChange,
            arrowRenderer: () => dropdownImage,
            autosize: this.props.autoSize,
            resetValue: { value: null, label: '' },
            style: this.props.style,
            isLoading: loadingPlaceholder
                ? false
                : isLoading,
            searchable: this.props.searchable
        };

        return (
            <Wrapper isLoading={ isLoading } new={ this.props.new }>
                <div className="label-wrapper">
                    { labelElement }
                </div>

                <div className="select-wrapper">
                    { !async
                        ? (
                            <Select
                                { ...SelectProps }
                            />
                        ) : (
                            <Select.Async
                                { ...SelectProps }
                                loadOptions={ debounce( loadOptions, 600 ) }
                            />
                        )
                    }

                    <p style={ { display: this.state.error ? 'block' : 'none' } }>
                        { this.state.errorMessage }
                    </p>
                </div>
            </Wrapper>
        );
    }
}

export default SalSelect;
