import styled from 'styled-components';

export const Wrapper = styled.div`
.search-wrapper {
    flex-grow: 1;

    .search {
        width: 300px;
        border: 1px solid #333;
        border-radius: 30px;

        input {
            border: none;
        }
    }

    p {
        display: none;
    }

    .input-group,
    .form-control {
        background-color: transparent;
    }

    .input-group-addon {
        background-color: transparent;
        border: none;
    }

    .isvg {
        display: inline-block;
        width: 1rem;
    }
}
`;
