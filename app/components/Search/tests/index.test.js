import React from 'react';
import { shallow } from 'enzyme';
import Search from '../index';

describe( '<Search />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <Search />
        );

        expect( component.html() ).toBeTruthy();
    });

    // You can also check if the 'foo' argument was passed to onParentClick
    it( 'should check is child component mounted and callback parsed', () => {
        const handleSearch = jest.fn();
        const component = shallow(
            <Search handleSearch={ handleSearch } />
        );
        const input = component.find( 'Input' );

        expect( input ).toBeTruthy();
        expect( input.props().className ).toEqual( 'search' );
        expect( input.props().id ).toEqual( 'search' );
    });
});
