import React from 'react';
import Input from '../../components/Input';
import Icon from '../../components/Icon';
import { Wrapper } from './styles';

/**
 *
 * More action dropdown component
 *
 */
class Search extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        handleSearch: React.PropTypes.func
    }

    /**
     * Component Constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            value: null
        };

        this.searchInput = null;
    }

    /**
     *
     * Modal
     *
     */
    render() {
        return (
            <Wrapper>
                <div className="search-wrapper">
                    <Input
                        className="search"
                        id="search"
                        ref={ ( ref ) => { this.searchInput = ref; } }
                        onChange={ () => {
                            this.props.handleSearch( this.searchInput.state.value.toLowerCase() );
                        } }
                        addon={ {
                            content: <Icon name="search" />,
                            placement: 'right'
                        } }
                    />
                </div>
            </Wrapper>
        );
    }
}

export default Search;
