import React from 'react';
import { shallow } from 'enzyme';

import FileInput from '../index';

describe( '<FileInput />', () => {
    const files = [{
        name: 'file1.pdf',
        size: 1111,
        type: 'application/pdf'
    }];
    const images = [{
        name: 'cats.gif',
        size: 1234,
        type: 'image/gif'
    }, {
        name: 'dogs.jpg',
        size: 2345,
        type: 'image/jpeg'
    }];
    it( 'Should mount the component', () => {
        const component = shallow(
            <FileInput accept=".txt" />
        );

        expect( component.html() ).toBeTruthy();
    });

    it( 'handle accepted file select', () => {
        const component = shallow(
            <FileInput accept=".pdf" />
        );
        component.instance()._handleDrop( files, []);
        expect( component.instance().state.files[ 0 ].name ).toEqual( files[ 0 ].name );
    });

    it( 'handle rejected file select', () => {
        const component = shallow(
            <FileInput accept=".pdf" />
        );
        component.instance()._handleDrop([], files );
        expect( component.instance().state.files.length ).toEqual( 0 );
    });

    it( 'handle cancel selection', () => {
        const component = shallow(
            <FileInput accept=".pdf" />
        );
        component.instance()._handleDrop([], []);
        expect( component.instance().state.files.length ).toEqual( 0 );
    });

    it( 'handle multiple file select', () => {
        const component = shallow(
            <FileInput accept="image/*" />
        );
        component.instance()._handleDrop( images, []);
        expect( component.instance().state.files.length ).toEqual( 2 );
    });

    it( 'handle onDrop callbacks', () => {
        const spy = jest.fn();
        const component = shallow(
            <FileInput accept=".pdf" onDrop={ spy } />
        );
        component.instance()._handleDrop( files, []);
        expect( spy ).toHaveBeenCalled();
    });
});
