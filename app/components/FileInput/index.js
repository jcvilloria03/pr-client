import React from 'react';
import Dropzone from 'react-dropzone';
import { Wrapper } from './styles';
import { SUB } from '../../components/Typography';

/**
* FileInput
*/
class FileInput extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        onDrop: React.PropTypes.func,
        multiple: React.PropTypes.bool,
        accept: React.PropTypes.string.isRequired,
        placeholder: React.PropTypes.string,
        disableClick: React.PropTypes.bool,
        className: React.PropTypes.string,
        label: React.PropTypes.string
    }

    static defaultProps = {
        multiple: false,
        disableClick: false
    }

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            files: [],
            error: false,
            label: 'Try dropping some files'
        };

        this._handleDrop = this._handleDrop.bind( this );
    }

    /**
     * this handles file selection events
     * @param acceptedFiles = array of accepted files
     * @param rejectedFiles = array of rejected files
     */
    _handleDrop( acceptedFiles, rejectedFiles ) {
        if ( rejectedFiles.length ) {
            this.setState({ label: 'Try dropping some files', error: true });
        } else {
            let message;
            if ( acceptedFiles.length ) {
                message = acceptedFiles.length > 1 ? `${acceptedFiles.length} files selected` : acceptedFiles[ 0 ].name;
            } else {
                message = this.props.placeholder || 'Try dropping some files';
            }

            this.setState({ label: message, error: false, files: acceptedFiles });
        }
        this.props.onDrop && this.props.onDrop({ acceptedFiles, rejectedFiles });
    }

    /**
     * FileInput render method.
     */
    render() {
        return (
            <Wrapper>
                <Dropzone className={ `${this.props.className && this.props.className} dropzone ${this.props.disableClick && 'disabled'}` } id="dropzone" onDrop={ this._handleDrop } multiple={ this.props.multiple } accept={ this.props.accept } disableClick={ this.props.disableClick }>
                    <div className="label">
                        <div className="content" style={ { color: this.state.files.length ? '#4e4e4e' : '#cccccc' } } >
                            { this.state.label }
                        </div>
                    </div>
                    <div className="clicker">
                        <div className="content">
                            { this.props.label || 'CHOOSE FILE' }
                        </div>
                    </div>
                </Dropzone>
                {this.state.error && <SUB style={ { color: '#F21108', paddingLeft: '2px', opacity: this.state.error ? '1' : '0' } } >Invalid File</SUB> }
            </Wrapper>
        );
    }
}

export default FileInput;
