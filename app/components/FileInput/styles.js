import styled from 'styled-components';

export const Wrapper = styled.div`
    & > div {
        width: 320px !important;
        height: 46px !important;
        border: 1px solid #ccc !important;
        border-radius: 5 !important;
        cursor: pointer;
        display: flex;
        align-items: stretch;

        & > div {
            display: flex;
            align-items: center;
            padding: 0 6px;
        }

        .label {
            flex-grow: 1;
            background: #fff;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
            padding: 0 10px;

            .content {
                width: 180px;
                overflow: hidden;
                text-overflow: ellipsis;
                word-break: keep-all;
                white-space: nowrap;
            }
        }

        .clicker {
            background: #00A5E5;
            color: #fff;
            font-weight: 600;
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;

            &:hover {
                background: #0086b7;
            }
        }
    }

    & > p {
        text-align: left;
    }

    .disabled {
        .clicker {
            cursor: not-allowed;
            background: #ccc;
            border-top-right-radius: 5px;
            &:hover {
                background: #ccc;
            }
        }
    }

    .dropzone {
        border-radius: 6px;
    }
`;
