/**
 * Testing our HeaderLink component
 */

import React from 'react';
import { shallow } from 'enzyme';

import HeaderLink from '../index';

const href = 'http://mxstbr.com/';

const renderComponent = ( props ) => shallow(
    <HeaderLink
        showTooltip
        href={ href }
        { ...props }
    >
        { props && props.title ? props.title : null }
    </HeaderLink>
);

describe( '<HeaderLink />', () => {
    it( 'should render a child <a> tag', () => {
        const renderedComponent = renderComponent();
        expect( renderedComponent.find( 'a' ) ).toHaveLength( 1 );
    });

    it( 'should render a child <a> tag with an href attribute', () => {
        const renderedComponent = renderComponent();
        expect( renderedComponent.find( 'a' ).prop( 'href' ) ).toEqual( href );
    });

    it( 'should render a child <a> tag with a target attribute', () => {
        const target = '_blank';
        const renderedComponent = renderComponent({ target });
        expect( renderedComponent.find( 'a' ).prop( 'target' ) ).toEqual( target );
    });

    it( 'should render a child <a> tag with a type attribute', () => {
        const type = 'text/html';
        const renderedComponent = renderComponent({ type });
        expect( renderedComponent.find( 'a' ).prop( 'type' ) ).toEqual( type );
    });

    it( 'should render a child <a> tag with a child text', () => {
        const title = 'I am a link';
        const renderedComponent = renderComponent({ title });
        expect( renderedComponent.find( 'a' ).children() ).toHaveLength( 1 );
    });

    it( 'should not render a child <Tooltip> component on mouseLeave', () => {
        const title = 'I am a link';
        const renderedComponent = renderComponent({ title });
        renderedComponent.simulate( 'mouseLeave' );
        expect( renderedComponent.find( 'Tooltip' ) ).toHaveLength( 0 );
    });

    it( 'should render a child <Tooltip> component on mouseEnter', () => {
        const title = 'I am a link';
        const renderedComponent = renderComponent({ title });
        renderedComponent.simulate( 'mouseEnter' );
        expect( renderedComponent.find( 'Tooltip' ) ).toHaveLength( 1 );
    });

    it( 'should render a child <Tooltip> component with a child text on mouseEnter', () => {
        const title = 'I am a link';
        const renderedComponent = renderComponent({ title });
        renderedComponent.simulate( 'mouseEnter' );
        expect( renderedComponent.find( 'Tooltip' ).children() ).toHaveLength( 1 );
    });

    it( 'should have active link', () => {
        const links = [ 'link1', 'link2' ];
        const renderedComponent = renderComponent({ links });
        expect( renderedComponent.instance().hasActiveLink( '/test-link/link1' ) ).toEqual( true );
    });
});
