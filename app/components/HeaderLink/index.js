import React, { Component, PropTypes } from 'react';
import { Wrapper, Tooltip } from './styles';

/**
 * Link to a certain page, an anchor tag to be placed in the Header
 */
class HeaderLink extends Component {
    static propTypes = {
        title: PropTypes.string,
        links: PropTypes.array,
        location: PropTypes.string,
        href: PropTypes.string,
        target: PropTypes.string,
        type: PropTypes.string,
        children: PropTypes.node,
        showTooltip: PropTypes.bool,
        onClick: PropTypes.func
    };

    static defaultProps = {
        links: [],
        showTooltip: false
    };

    constructor( props ) {
        super( props );

        this.state = {
            isTooltipVisible: false
        };
    }

    /**
     * Sets visibility of tooltip to the given value.
     */
    setIsTooltipVisible( isVisible ) {
        this.setState({
            isTooltipVisible: isVisible
        });
    }

    /**
     * Checks if one of the given links is currently active.
     */
    hasActiveLink( href = window.location.href ) {
        const { links } = this.props;

        return links.filter( ( link ) => (
            href.includes( link )
        ) ).length > 0;
    }

    render() {
        const { isTooltipVisible } = this.state;
        const {
            title,
            children,
            showTooltip,
            location,
            href,
            type,
            target,
            onClick
        } = this.props;

        return (
            <Wrapper
                onMouseEnter={ () => this.setIsTooltipVisible( true ) }
                onMouseLeave={ () => this.setIsTooltipVisible( false ) }
            >
                <a
                    href={ href }
                    type={ type }
                    target={ target }
                    tabIndex={ -1 }
                    onClick={ onClick }
                    className={ `${this.hasActiveLink( location ) ? 'active' : ''}` }
                >
                    {children}
                </a>{ showTooltip && isTooltipVisible
                    ? <Tooltip>{ title }</Tooltip>
                    : null }
            </Wrapper>
        );
    }
}

export default HeaderLink;
