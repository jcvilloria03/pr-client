import styled from 'styled-components';

const Wrapper = styled.div`
    height: 100%;
    display: inline-flex;
`;

const Tooltip = styled.div`
    position: absolute;
    top: 85px;
    z-index: 1100;
    max-width: 15rem;
    padding: .25rem 1rem;
    color: #00A5E5;
    font-size: 0.7rem;
    text-align: center;
    filter: drop-shadow(0 0 .1rem #00A5E5);
    background-color: #fff;
    border-radius: .25rem;

    &:after {
        position: absolute;
        top: -5px;
        left: 50%;
        transform: translateX(-50%);
        display: block;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 0 5px 5px 5px;
        border-color: transparent transparent #fff transparent;
        content: '';
    }
`;

Tooltip.displayName = 'Tooltip';

export { Wrapper, Tooltip };
