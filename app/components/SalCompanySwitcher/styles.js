import styled, { css } from 'styled-components';
import {
    Dropdown as BaseDropdown,
    DropdownMenu as BaseDropdownMenu,
    DropdownToggle as BaseDropdownToggle
} from 'reactstrap';

export const DropdownToggle = styled( BaseDropdownToggle )`
    display: flex;
    width: 100%;
    max-width: 306px;
    height: 100%;
    align-items: center;
    gap: 1rem;
`;

export const CompanyName = styled.span`
    max-width: 15rem;
    overflow: hidden;
    color: #fff;
    font-size: 14px;
    text-overflow: ellipsis;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
`;

export const CompanyAvatar = styled.img`
    border: 2px solid #fff;
    min-width: 40px;
    max-width: 40px;
    height: 40px;
    border-radius: 50%;
    object-fit: contain;
    object-position: center;
    background-color: #fff;
`;

export const CompanyAvatarPlaceholder = styled.div`
    border: 2px solid #fff;
    min-width: 40px;
    max-width: 40px;
    height: 40px;
    border-radius: 50%;
    background-color: #ccc;
`;

export const IconWrapper = styled.span`
    > span {
        color: #fff;

        > svg {
            width: 1rem;
            height: 1rem;
        }
    }
`;

export const Dropdown = styled( BaseDropdown )`
    padding-right: 2rem;
`;

const activeCompanyItemLinkStyles = css`
    border-left-color: #00A5E5;
    color: #0077A5;
`;

export const DropdownMenuItem = styled.div`
    border-bottom: 1px solid #e6e6e6;
    display: flex;
    gap: 0.75rem;
    padding: .5rem 1rem .5rem .5rem;
    color: #474747;
    border-left: .25rem solid transparent;
    font-weight: 400;
    align-items: center;
    transition: all .2s ease-in-out;

    ${({ active }) => active && activeCompanyItemLinkStyles}

    :hover {
        ${activeCompanyItemLinkStyles}
    }

    :last-child {
        border-bottom: none;
    }


    ${CompanyAvatar}, ${CompanyAvatarPlaceholder} {
        min-width: 28px;
        max-width: 28px;
        height: 28px;
    }
`;

export const DropdownMenu = styled( BaseDropdownMenu )`
    font-size: 14px;
    position: absolute;
    top: 58px;
    margin-top: 0;
    left: 0;
    z-index: 10;
    min-width: 10rem;
    background-color: #F0F4F6;
    border: none;
    box-shadow: 0 1px 2px 0 #ccc;
    padding: 0;
    max-height: 34rem;
    overflow-x: auto;

    :before {
        position: absolute;
        top: -.5rem;
        left: auto;
        right: 1rem;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 0 .5rem .5rem .5rem;
        border-color: transparent transparent #F0F4F6 transparent;
        content: '';
    }
`;

export const Wrapper = styled.div`
    position: relative;
    cursor: pointer;
    user-select: none;
    display: flex;
    align-items: center;

    :after {
        position: absolute;
        top: 50%;
        right: 0;
        transform: translateY(-50%);
        display: block;
        width: 0;
        height: 75%;
        border-right: 1px solid #fff;
        content: '';
    }
`;

export const ManageCompanies = styled( DropdownMenuItem )`
    justify-content: center;
`;
