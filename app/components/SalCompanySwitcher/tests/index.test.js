import React from 'react';
import { shallow } from 'enzyme';

import SalCompanySwitcher from '../../SalCompanySwitcher';
import { DropdownMenuItem } from '../../SalCompanySwitcher/styles';

describe( '<SalCompanySwitcher', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <SalCompanySwitcher
                currentCompany={ { name: 'Test Company' } }
                companies={ [] }
            />
        );
        expect( component.html() ).toBeTruthy();
    });

    it( 'should have manageCompanies callback', () => {
        const _handleOnManageCompanies = jest.fn();
        const component = shallow(
            <SalCompanySwitcher
                currentCompany={ { name: 'Test Company' } }
                companies={ [] }
                onManageCompanies={ _handleOnManageCompanies }
            />
        );
        component.find( '#manage-companies' ).simulate( 'click' );
        expect( _handleOnManageCompanies ).toHaveBeenCalled();
    });

    it( 'should have setCompany callback', () => {
        const _handleSetCompany = jest.fn();
        const component = shallow(
            <SalCompanySwitcher
                currentCompany={ { name: 'Test Company ' } }
                companies={ [
                    { name: 'Test Company' }
                ] }
                onSetCompany={ _handleSetCompany }
                onManageCompanies={ () => {} }
            />
        );
        component.find( DropdownMenuItem ).at( 0 ).simulate( 'click' );
        expect( _handleSetCompany ).toHaveBeenCalled();
    });
});
