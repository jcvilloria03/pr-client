import React from 'react';

import Icon from '../Icon';

import { COMPANY_LOGO_PLACEHOLDER } from './constants';

import {
    CompanyAvatar,
    CompanyAvatarPlaceholder,
    CompanyName,
    Dropdown,
    DropdownMenu,
    DropdownMenuItem,
    DropdownToggle,
    IconWrapper,
    ManageCompanies,
    Wrapper
} from './styles';

/**
 *
 * Company Switcher
 *
 */
export default class SalCompanySwitcher extends React.Component {
    static propTypes = {
        currentCompany: React.PropTypes.object,
        companies: React.PropTypes.array,
        onManageCompanies: React.PropTypes.func,
        onSetCompany: React.PropTypes.func
    };

    static defaultProps = {};

    state = {
        dropdownOpen: false
    }

    /**
     * Pass selected company to callback and close dropdown.
     * @param {Object} company
     */
    handleSetCompany = ( company ) => {
        this.props.onSetCompany( company );
        this.toggle();
    }

    /**
     * Trigger callback and close dropdown.
     */
    handleManageCompanies = () => {
        this.props.onManageCompanies();
        this.toggle();
    }

    /**
     * Toggle dropdown meny.
     */
    toggle = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    /**
     * SalCompanySwitcher
     */
    render() {
        const {
            currentCompany,
            companies
        } = this.props;

        return (
            <Wrapper>
                <Dropdown isOpen={ this.state.dropdownOpen } toggle={ this.toggle }>
                    <DropdownToggle
                        tag="span"
                        onClick={ this.toggle }
                        data-toggle="dropdown"
                        aria-expanded={ this.state.dropdownOpen }
                    >
                        { currentCompany.logo
                            ? (
                                <CompanyAvatar
                                    role="presentation"
                                    src={ currentCompany.logo }
                                />
                            ) : (
                                <CompanyAvatarPlaceholder />
                            )
                        }

                        <CompanyName>
                            <strong>{ currentCompany.name || 'Loading...' }</strong>
                        </CompanyName>

                        <IconWrapper>
                            <Icon name="caretCircle" />
                        </IconWrapper>
                    </DropdownToggle>

                    <DropdownMenu>
                        { companies.map( ( item, index ) => (
                            <DropdownMenuItem
                                key={ index }
                                tabIndex={ 0 }
                                onClick={ () => this.handleSetCompany( item ) }
                                active={ currentCompany.id === item.id }
                            >
                                { item.logo
                                        ? (
                                            <CompanyAvatar
                                                role="presentation"
                                                src={ item.logo ? item.logo : COMPANY_LOGO_PLACEHOLDER }
                                            />
                                        ) : (
                                            <CompanyAvatarPlaceholder />
                                        )
                                    }

                                <span>{ item.name }</span>
                            </DropdownMenuItem>
                        ) ) }

                        <ManageCompanies
                            id="manage-companies"
                            onClick={ this.handleManageCompanies }
                        >
                            Manage Companies
                        </ManageCompanies>
                    </DropdownMenu>
                </Dropdown>
            </Wrapper>
        );
    }
}
