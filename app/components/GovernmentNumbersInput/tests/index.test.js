import React from 'react';
import { shallow, mount } from 'enzyme';
import GovernmentNumbersInput from '../index';

describe( '<GovernmentNumbersInput />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <GovernmentNumbersInput id="test" label="test" />
        );
        expect( component.html() ).toBeTruthy();
    });

    it( 'should have prop key "type" and the default return value is "text"', () => {
        const component = shallow(
            <GovernmentNumbersInput id="test" />
        );
        const props = component.instance().props;
        expect( props.type ).toEqual( 'text' );
    });

    it( 'should have prop key "disabled" and the default return value is "text"', () => {
        const component = shallow(
            <GovernmentNumbersInput id="test" label="test" />
        );
        const props = component.instance().props;
        expect( props.disabled ).toEqual( false );
    });

    it( 'should handle prop changes', () => {
        const component = shallow(
            <GovernmentNumbersInput id="test" label="test" value="3" />
        );
        expect( component.instance().props.value ).toEqual( '3' );
        component.setProps({ value: '4' });
        expect( component.instance().props.value ).toEqual( '4' );
    });

    it( 'should have a rendered label and placeholder', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                label="label"
                placeholder="placeholder"
            />
        );
        const props = component.instance().props;
        expect( props.label ).toEqual( 'label' );
        expect( props.placeholder ).toEqual( 'placeholder' );
    });

    it( 'should have the default expected states', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                label="test"
            />
        );
        const state = component.instance().state;
        expect( state.error ).toEqual( false );
    });

    it( 'should handle focus', () => {
        const spy = jest.fn();

        const component = mount(
            <GovernmentNumbersInput
                id="test"
                label="test"
                onFocus={ spy }
            />
        );

        component.find( 'input' ).simulate( 'focus' );

        expect( spy ).toHaveBeenCalled();
    });

    it( 'should handle blur', () => {
        const spy = jest.fn();

        const component = mount(
            <GovernmentNumbersInput
                id="test"
                label="test"
                onBlur={ spy }
            />
        );

        component.find( 'input' ).simulate( 'blur' );

        expect( spy ).toHaveBeenCalled();
    });

    it( 'should check the minimum value required by the input', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                min={ 3 }
                label="test"
            />
        );
        expect( component.instance()._checkMin( 'ab' ) ).toEqual( false );
        expect( component.instance()._checkMin( 'abcd' ) ).toEqual( true );
    });

    it( 'should be mounted as disabled', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                label="test"
                disabled
            />
        );
        expect( component.instance().state.disabled ).toEqual( true );
    });

    it( 'should be able to toggle disabled state on and off', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                label="test"
                disabled
            />
        );
        expect( component.instance().state.disabled ).toEqual( true );
        component.setProps({ disabled: false });
        expect( component.instance().state.disabled ).toEqual( false );
        component.setProps({ disabled: true });
        expect( component.instance().state.disabled ).toEqual( true );
    });

    it( 'should check the max value required by the input', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                max={ 3 }
                label="test"
            />
        );
        expect( component.instance()._checkMax( 'ab' ) ).toEqual( true );
        expect( component.instance()._checkMax( 'abcd' ) ).toEqual( false );
    });

    it( 'should check the min number value required by the input', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                minNumber={ 50 }
                label="test"
            />
        );
        expect( component.instance()._checkMinNumber( 100 ) ).toEqual( true );
        expect( component.instance()._checkMinNumber( 0 ) ).toEqual( false );
    });

    it( 'should check the max number value required by the input', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                maxNumber={ 50 }
                label="test"
            />
        );
        expect( component.instance()._checkMaxNumber( 50 ) ).toEqual( true );
        expect( component.instance()._checkMaxNumber( 100 ) ).toEqual( false );
    });

    it( 'should throw an error if the required minimum character are not met', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <GovernmentNumbersInput
                onChange={ _handleChange }
                id="test"
                min={ 3 }
                label="test"
            />
        );
        const eventObject = {
            target: {
                value: 'ab'
            }
        };
        component.find( 'input' ).simulate( 'blur', eventObject );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'This field requires at least 3 characters',
            value: 'ab',
            disabled: false
        });
    });

    it( 'should throw an error if the required maximum character are not met', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <GovernmentNumbersInput
                onChange={ _handleChange }
                id="test"
                max={ 3 }
                label="test"
            />
        );
        const eventObject = {
            target: {
                value: 'abcd'
            }
        };
        component.find( 'input' ).simulate( 'blur', eventObject );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'This field must not exceed 3 characters',
            value: 'abcd',
            disabled: false
        });
    });

    it( 'should throw an error if the input prop is set to required and the value is empty', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <GovernmentNumbersInput
                onChange={ _handleChange }
                id="test"
                required
                label="test"
            />
        );
        const eventObject = {
            target: {
                value: ''
            }
        };
        component.find( 'input' ).simulate( 'blur', eventObject );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'This field is required',
            value: '',
            disabled: false
        });
    });

    it( 'should call _handleChange event in onChange event', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <GovernmentNumbersInput
                id="test"
                onChange={ _handleChange }
                label="test"
            />
        );
        const eventObject = {
            target: {
                value: 'My new value'
            }
        };
        component.find( 'input' ).simulate( 'change', eventObject );
        expect( _handleChange ).toHaveBeenCalled();
    });

    it( 'should handle input focus', () => {
        const component = mount(
            <GovernmentNumbersInput
                id="test"
                value="test"
                label="test"
            />
        );
        component.find( '#test' ).simulate( 'focus' );
        expect( component.instance().state.focus ).toEqual( true );
    });

    it( 'should throw an error if the input field is not a valid RDO and the prop type is set to `rdo`', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                type="rdo"
                label="test"
                value="rdo"
            />
        );

        component.instance()._validate( component.instance().state.value );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'Enter a valid RDO code',
            value: 'rdo',
            disabled: false
        });

        component.setProps({ value: '110' });
        component.instance()._validate( component.instance().state.value );
        expect( component.instance().state.error ).toEqual( false );
    });

    it( 'should throw an error if the input field is not a valid SSS and the prop type is set to `sss`', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                type="sss"
                label="test"
                value="123"
            />
        );

        component.instance()._validate( component.instance().state.value );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'This field requires 10 numeric values',
            value: '123',
            disabled: false
        });

        component.setProps({ value: '12-3456789-0' });
        component.instance()._validate( component.instance().state.value );
        expect( component.instance().state.error ).toEqual( false );
    });

    it( 'should throw an error if the input field is not a valid TIN and the prop type is set to `tin`', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                type="tin"
                label="test"
                value="123"
            />
        );

        component.instance()._validate( component.instance().state.value );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'This field requires 9 or 12 numeric values',
            value: '123',
            disabled: false
        });

        component.setProps({ value: '123-456-789' });
        component.instance()._validate( component.instance().state.value );
        expect( component.instance().state.error ).toEqual( false );

        component.setProps({ value: '123-456-789-012' });
        component.instance()._validate( component.instance().state.value );
        expect( component.instance().state.error ).toEqual( false );
    });

    it( 'should throw an error if the input field is not a valid HDMF and the prop type is set to `hdmf`', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                type="hdmf"
                label="test"
                value="123"
            />
        );

        component.instance()._validate( component.instance().state.value );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'This field requires 12 numeric values',
            value: '123',
            disabled: false
        });

        component.setProps({ value: '1234-5678-9012' });
        component.instance()._validate( component.instance().state.value );
        expect( component.instance().state.error ).toEqual( false );
    });

    it( 'should throw an error if the input field is not a valid PhilHealth and the prop type is set to `philhealth`', () => {
        const component = shallow(
            <GovernmentNumbersInput
                id="test"
                type="philhealth"
                label="test"
                value="123"
            />
        );

        component.instance()._validate( component.instance().state.value );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'This field requires 12 numeric values',
            value: '123',
            disabled: false
        });

        component.setProps({ value: '12-345678901-2' });
        component.instance()._validate( component.instance().state.value );
        expect( component.instance().state.error ).toEqual( false );
    });
});
