import React from 'react';
import Input from 'components/Input';
import {
    checkSSS,
    checkHDMF,
    checkPhilHealth,
    checkTIN,
    checkRDO
} from 'utils/functions';

/**
 *
 * GovernmentNumbersInput
 *
 */
class GovernmentNumbersInput extends Input {
    static propTypes = Object.assign({}, Input.propTypes, {
        type: React.PropTypes.oneOf([
            'text',
            'rdo',
            'sss',
            'tin',
            'hdmf',
            'philhealth'
        ])
    })

    /**
     * constructor method of the component
     */
    constructor( props ) {
        super( props );
        this.state = {
            errorMessage: '&nbsp;',
            error: false,
            value: props.value,
            focus: false,
            disabled: props.disabled
        };

        this._validate = this._validate.bind( this );
    }

    /**
     * validation for input value upon blur or change depending on input type
     *
     * @param value - value of input to validate
     * @returns {Boolean} returns error status
     */
    _validate = ( value, type = 'blur' ) => {
        let error = false;
        let errorMessage = '&nbsp;';
        let response;

        if ( value && value.trim() !== '' ) {
            switch ( this.props.type ) {
                case 'rdo':
                    if ( !checkRDO( value ) ) {
                        error = true;
                        errorMessage = 'Enter a valid RDO code';
                    }
                    break;
                case 'sss':
                    if ( !checkSSS( value ) ) {
                        error = true;
                        errorMessage = 'This field requires 10 numeric values';
                    }
                    break;
                case 'tin':
                    if ( !checkTIN( value ) ) {
                        error = true;
                        errorMessage = 'This field requires 9 or 12 numeric values';
                    }
                    break;
                case 'hdmf':
                    if ( !checkHDMF( value ) ) {
                        error = true;
                        errorMessage = 'This field requires 12 numeric values';
                    }
                    break;
                case 'philhealth':
                    if ( !checkPhilHealth( value ) ) {
                        error = true;
                        errorMessage = 'This field requires 12 numeric values';
                    }
                    break;
                default:
            }

            if ( this.props.min && !this._checkMin( value ) ) {
                error = true;
                errorMessage = `This field requires at least ${this.props.min} characters`;
            } else {
                error = error || false;
            }

            if ( this.props.max && !this._checkMax( value ) ) {
                error = true;
                errorMessage = `This field must not exceed ${this.props.max} characters`;
            } else {
                error = error || false;
            }

            response = {
                error,
                errorMessage,
                value
            };
        } else {
            response = {
                error: this.props.required || false,
                errorMessage: this.props.required ? 'This field is required' : errorMessage,
                value
            };
        }

        if ( type === 'blur' ) {
            this.setState( response, () => {
                this.props.onBlur && this.props.onBlur( value );
            });
        } else {
            response.error = false;
            this.setState({ error: response.error, errorMessage: '&nbsp;', value: response.value }, () => { this.props.onChange && this.props.onChange( value ); });
        }

        return response.error;
    }
}

export default GovernmentNumbersInput;
