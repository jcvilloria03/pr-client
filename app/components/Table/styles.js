import { css } from 'styled-components';

/**
 * Default styles for SalPagination
 */
export const styleProps = css`
    .ReactTable {
        position: relative;

        * {
            box-sizing: border-box;
        }

        .rt-table {
            display: flex;
            flex-direction: column;
            align-items: stretch;
            width: 100%;
            height: 100%;
            overflow: auto;
            border-collapse: collapse;
            background-color: #ffffff;
        }

        .rt-thead {
            display: flex;
            flex-direction: column;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            color: #ffffff;
            background-color: #00A5E5;
            font-weight: 600;

            > .rt-tr {
                padding-top: 6px;
                padding-bottom: 6px;
            }
        }

        .Select {
            font-size: 14px;

            .Select-control {
                height: 39px;

                .Select-value {
                    line-height: 39px;
                }

                .Select-input {
                    height: 37px;
                }
            }
        }
    }

    .ReactTable .rt-thead.-headerGroups {
        background: rgba(0,0,0,0.03);
        border-bottom: 1px solid rgba(0,0,0,0.05) !important;
    }

    .ReactTable .rt-thead.-filters .rt-th {
        border-right: 1px solid rgba(0,0,0,0.05)
    }

    .rt-thead.-filters input {
        background: #fff;
    }

    .ReactTable .rt-thead .rt-tr {
        text-align: center;
    }

    .ReactTable .rt-thead .rt-th,
    .ReactTable .rt-thead .rt-td {
        padding: 5px 5px;
        line-height: normal;
        position: relative;
        transition: box-shadow 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
        box-shadow: inset 0 0 0 0 transparent;
    }

    .ReactTable .rt-thead .rt-th.-sort-asc,
    .ReactTable .rt-thead .rt-td.-sort-asc {
        box-shadow: inset 0 3px 0 0 rgba(0,0,0,0.6);
    }

    .ReactTable .rt-thead .rt-th.-sort-desc,
    .ReactTable .rt-thead .rt-td.-sort-desc {
        box-shadow: inset 0 -3px 0 0 rgba(0,0,0,0.6);
    }

    .ReactTable .rt-thead .rt-th.-cursor-pointer,
    .ReactTable .rt-thead .rt-td.-cursor-pointer {
        cursor: pointer;
    }

    .ReactTable .rt-thead .rt-th:last-child,
    .ReactTable .rt-thead .rt-td:last-child {
        border-right: 0;
    }

    .ReactTable .rt-thead .rt-resizable-header {
        overflow: visible;
        text-align: left;
        display: flex;
        align-items: center;

        > div {
            display: flex;
        }
    }

    .ReactTable .rt-thead .rt-resizable-header:last-child {
        overflow: hidden;
    }

    .ReactTable .rt-thead .rt-resizable-header-content {
        overflow: hidden;
        text-overflow: ellipsis;
        padding: 12px 14px;
        display: inline-block;
        font-size: 14px;
        padding-right: 7px;
    }

    .ReactTable .rt-tbody {
        display: flex;
        flex-direction: column;
    }

    .ReactTable .rt-tbody .rt-tr.-padRow {
        display: none;
    }

    .ReactTable .rt-tbody .rt-tr-group:last-child {
        border-bottom: 0;
    }

    .ReactTable .rt-tbody .rt-td {
        border-right: none;
        align-self: stretch;
        align-items: center;
        display: flex;
        white-space: pre-wrap;

        &.select {
            > span {
                display: flex;
                align-items: center;
                justify-content: center;
            }
        }
    }

    .ReactTable .rt-tbody .rt-td:last-child {
        border-right: 0;
    }

    .ReactTable .rt-tbody .rt-pivot {
        cursor: pointer;
    }

    .ReactTable .rt-tr-group {
        display: flex;
        flex-direction: column;
        align-items: stretch;
        position: relative;

        &:nth-child(even) {
            background-color: #fafbfc;
        }
    }

    .ReactTable .rt-tr {
        display: inline-flex;

        &.selected:after {
            content: '';
            position: absolute;
            width: 4px;
            height: calc( 100% - 1px );
            background: #00A5E5;
            left: 0;
        }
    }

    .ReactTable .rt-th,
    .ReactTable .rt-td {
        flex: 1 0 0px;
        white-space: nowrap;
        text-overflow: ellipsis;
        padding: 7px 20px;
        overflow: hidden;
        transition: 0.3s ease;
        transition-property: width, min-width, padding, opacity;
    }

    .ReactTable .rt-tbody .rt-td {
        font-size: 14px;
    }

    .ReactTable .rt-th.-hidden,
    .ReactTable .rt-td.-hidden {
        width: 0 !important;
        min-width: 0 !important;
        padding: 0 !important;
        border: 0 !important;
        opacity: 0 !important;
    }

    .rt-expander-header.rt-th {
        border-right: none !important;
        width: 40px !important;
    }

    .ReactTable .rt-expander {
        display: inline-block;
        position: relative;
        margin: 0;
        color: transparent;
        margin: 0 10px;
    }

    .ReactTable .rt-expander:after {
        content: '';
        position: absolute;
        width: 0;
        height: 0;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) rotate(-90deg);
        border-left: 5.04px solid transparent;
        border-right: 5.04px solid transparent;
        border-top: 7px solid rgba(0,0,0,0.8);
        transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
        cursor: pointer;
    }

    .ReactTable .rt-expander.-open:after {
        transform: translate(-50%, -50%) rotate(0deg);
    }

    .ReactTable .rt-resizer {
        cursor: col-resize;
        z-index: 10;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .ReactTable .rt-tfoot {
        display: flex;
        flex-direction: column;
        box-shadow: 0 0px 15px 0px rgba(0,0,0,0.15);
    }

    .ReactTable .rt-tfoot .rt-td {
        border-right: 1px solid rgba(0,0,0,0.05);
    }

    .ReactTable .rt-tfoot .rt-td:last-child {
        border-right: 0;
    }

    .ReactTable.-highlight .rt-tbody .rt-tr:not(.-padRow):hover {
        background: rgba(0, 165, 229, 0.01);
    }

    .ReactTable.-highlight .rt-tbody .rt-tr:not(.-padRow) {
        // border-top: 1px solid #E5E5E5;
    }

    .ReactTable .-pagination {
        z-index: 1;
        display: flex;
        padding: 20px 0;
        justify-content: space-between;
        align-items: stretch;
        flex-wrap: wrap;
        
        .-next {
            order: 3;
        }
        
        .-previous {
            margin-left: 20px;
            order: 2;
        }

        .-center {
            order: 1;
            text-align: center;
            margin-bottom: 0;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            flex-grow: 1;
            align-items: center;
            width: calc( 100% - 280px );
            justify-content: space-between;
        }

        .-btn {
            appearance: none;
            display: block;
            height: 40px;
            border: 0;
            border-radius: 40px;
            padding: 7px 21px;
            font-size: 1em;
            color: #474747;
            background: #fff;
            border: 1px solid #9fdc74;
            transition: all 0.1s ease;
            cursor: pointer;
            outline: none;
        }

        .-btn[disabled] {
            cursor: default;
            border: 1px solid #9fdc74;
            color: #adadad;
        }

        .-btn:not([disabled]):hover {
            color: #474747;
            border: 1px solid #9fdc74;
        }
    }

    .ReactTable .-pagination .-pageInfo {
        display: inline-block;
        margin: 3px 10px;
        white-space: nowrap;
        order: 2;
    }

    .ReactTable .-pagination .-pageJump {
        display: inline-block;
        order: 1;

        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    }

    .ReactTable .-pagination .-pageJump input {
        width: 40px;
        text-align: center;
    }

    .ReactTable .rt-noData {
        display: block;
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        background: rgba(255,255,255,0.8);
        transition: all 0.3s ease;
        z-index: 1;
        pointer-events: none;
        padding: 20px;
        color: rgba(0,0,0,0.5);
    }

    .ReactTable .-loading {
        display: block;
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        background: rgba(255,255,255,0.8);
        transition: all 0.3s ease;
        z-index: 2;
        opacity: 0;
        pointer-events: none;
    }

    .ReactTable .-loading > div {
        position: absolute;
        display: block;
        text-align: center;
        width: 100%;
        top: 50%;
        left: 0;
        font-size: 15px;
        color: rgba(0,0,0,0.6);
        transform: translateY(-52%);
        transition: all 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94);
    }

    .ReactTable .-loading.-active {
        opacity: 1;
        pointer-events: all;
    }

    .ReactTable .-loading.-active > div {
        transform: translateY(50%);
    }

    .ReactTable input,
    .ReactTable select {
        appearance: none;
        background: #fff;
        padding: 5px 7px;
        font-size: inherit;
        border-radius: 3px;
        font-weight: normal;
        outline: none;
    }

    .ReactTable .checkbox {
        -webkit-appearance: none;
        background-color: #fff;
        border: 1px solid #cacece;
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
        padding: 6px;
        border-radius: 0px;
        display: inline-block;
        position: relative;
    }

    .ReactTable .checkbox:active, .checkbox:checked:active {
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
    }

    .ReactTable .checkbox:checked {
        background-color: #fafafa;
        border: 1px solid #adb8c0;
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
        color: #00A5E5;
    }

    .ReactTable .checkbox:checked:after {
        content: "\\2714";
        font-size: 12px;
        position: absolute;
        top: -2px;
        left: 1px;
        color: #00A5E5;
    }

    .ReactTable .select-wrap {
        position: relative;
        display: inline-block;
    }

    .ReactTable .select-wrap select {
        padding: 5px 15px 5px 7px;
        min-width: 100px;
    }

    .ReactTable .select-wrap:after {
        content: '';
        position: absolute;
        right: 8px;
        top: 50%;
        transform: translate(0, -50%);
        border-color: #999 transparent transparent;
        border-style: solid;
        border-width: 5px 5px 2.5px;
    }

    .rt-th > input {
        color: #333;
    }

    .hidden {
        display: none;
    }

    .-pageSizeOptions {
        select {
            border: 1px solid #ccc;
        }
    }

    .-pageJump {
        input {
            border: 1px solid #ccc;
        }
    }

    .rt-noData {
        display: none !important;
    }

    .ReactTable .rt-thead .rt-th.-cursor-pointer, .ReactTable .rt-thead .rt-td.-cursor-pointer {
        overflow: hidden;
        &:after {
            content: "\\f0dc";
            font: normal normal normal 14px/1 FontAwesome;
            font-size: 16px;
            background: #00a5e6;
        }
    }

    .ReactTable .rt-thead .rt-th.-sort-asc, .ReactTable .rt-thead .rt-td.-sort-asc {
        box-shadow: none;
        &:after {
            content: "\\f077";
            font-size: 16px;
        }
    }

    .ReactTable .rt-thead .rt-th.-sort-desc, .ReactTable .rt-thead .rt-td.-sort-desc {
        box-shadow: none;
        &:after {
            content: "\\f078";
            font-size: 16px;
        }
    }

    .ReactTable .rt-thead .rt-th.-cursor-pointer, .ReactTable .rt-thead .rt-td.-cursor-pointer {
        overflow: hidden;
    }

    .ReactTable .rt-thead .rt-th.-sort-asc, .ReactTable .rt-thead .rt-td.-sort-asc {
        box-shadow: none;
    }

    .ReactTable .rt-thead .rt-th.-sort-desc, .ReactTable .rt-thead .rt-td.-sort-desc {
        box-shadow: none;
    }


`;
