/* eslint-disable react/no-unused-prop-types */
import _ from 'lodash';
import React from 'react';
import ReactDOM from 'react-dom';
import ReactTable from 'react-table';
import styled from 'styled-components';
import { styleProps } from './styles';

const StyledDiv = styled.div`
    ${styleProps}
`;

/**
*
* Table
*
*/
class Table extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        columns: React.PropTypes.array.isRequired,
        data: React.PropTypes.arrayOf( React.PropTypes.object ).isRequired,
        onRowClick: React.PropTypes.func,
        pagination: React.PropTypes.bool,
        pageSize: React.PropTypes.number,
        sizeOptions: React.PropTypes.arrayOf( React.PropTypes.number ),
        defaultSorting: React.PropTypes.array,
        showFilters: React.PropTypes.bool,
        SubComponent: React.PropTypes.func,
        selectable: React.PropTypes.bool,
        checkboxLabel: React.PropTypes.string,
        noDataText: React.PropTypes.string,
        onDataChange: React.PropTypes.func,
        onSelectionChange: React.PropTypes.func,
        defaultSorted: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                id: React.PropTypes.string,
                desc: React.PropTypes.bool
            })
        ),
        loading: React.PropTypes.bool,
        emptyTableComponent: React.PropTypes.func,
        showPageSizeOptions: React.PropTypes.bool,
        className: React.PropTypes.string,
        onPageChange: React.PropTypes.func,
        onPageSizeChange: React.PropTypes.func,
        onSortingChange: React.PropTypes.func,
        onFilteringChange: React.PropTypes.func,
        page: React.PropTypes.number,
        pages: React.PropTypes.number,
        manual: React.PropTypes.bool,
        external: React.PropTypes.bool
    };

    static defaultProps = {
        pagination: false,
        sizeOptions: [ 10, 20, 50, 100, 200 ],
        pageSize: 10,
        selectable: false,
        checkboxLabel: '',
        noDataText: 'No rows found',
        showPageSizeOptions: true,
        className: '',
        manual: false
    };

    /**
     * component constructor
     * @param props
     */
    constructor( props ) {
        super( props );

        this.state = {
            selected: [],
            columns: [],
            pageSelected: {
                0: false
            },
            disabledSelection: false,
            sorting: [],
            selectedRows: []
        };

        this.toggleRow = this.toggleRow.bind( this );
        this.selectPage = this.selectPage.bind( this );
    }

    /**
     * this handles preparation if selectable prop is set to true
     */
    componentWillMount() {
        this.initColumns();
    }

    /**
     * handle actions when receiving changes in props
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.data.length !== this.props.data.length && this.setState({ selected: nextProps.data.map( ( item ) => item.selected || false ) });
        !this.props.selectable && nextProps.selectable && this.initColumns();

        this.renderEmptyTableContent( nextProps );
    }

    /**
     * Handle table search
     * @param {Array} data
     * @param {String} term
     * @returns {Array}
     */
    getFilteredData( data, term ) {
        if ( this.tableComponent ) {
            const matchingRows = [];
            data.forEach( ( row ) => {
                const match = this.tableComponent.state.columns.some( ( column ) => (
                    column.id &&
                    column.accessor &&
                    column.accessor( row ) &&
                    column.accessor( row ).toString().toLowerCase().indexOf( term.trim().toLowerCase() ) >= 0
                ) );

                match && matchingRows.push( row );
            });

            return matchingRows;
        }

        return data;
    }

    /**
     * Init columns.
     */
    initColumns() {
        const { columns, selectable, data } = this.props;
        const updatedColumns = [...columns];

        if ( selectable && updatedColumns[ 0 ].accessor !== 'select' ) {
            // Add a new column for checkbox as first index
            const labelId = Date.now().toString();
            updatedColumns.unshift({
                accessor: 'select',
                header: (
                    <span>
                        <input
                            className="checkbox"
                            id={ labelId }
                            onChange={ ( e ) => { this.selectPage( e, this.tableComponent.state.page + 1, this.tableComponent.state.pageSize ); } }
                            type="checkbox"
                            ref={ ( ref ) => { this.headerSelect = ref; } }
                        />
                        <label style={ { fontWeight: '600', marginLeft: '10px' } } htmlFor={ labelId }>{ this.props.checkboxLabel ? `${this.props.checkboxLabel} all` : '' } </label>
                    </span>
                ),
                hideFilter: true,
                sortable: false,
                resizable: false,
                width: this.props.checkboxLabel ? 140 : 61,
                style: { justifyContent: 'center' },
                className: 'select',
                render: ({ index, row }) => {
                    const id = ( Date.now() * index ).toString();
                    return row.noselect ? '' : (
                        <span>
                            <TableCheckbox
                                id={ id }
                                selected={ this.state.selected[ index ] }
                                toggleRow={ this.toggleRow }
                                index={ index }
                                disabled={ this.state.disabledSelection }
                                rowData={ row }
                            />
                            <label style={ { marginLeft: '10px' } } htmlFor={ id }>{ this.props.checkboxLabel } </label>
                        </span>
                    );
                }
            });
            // append checkbox data for each data entry
            const selected = data.map( () => false );
            this.setState({ selected, columns: updatedColumns });
        } else {
            this.setState({ columns: updatedColumns });
        }
    }

    /**
     * handles toggling of checkboxes
     * @param index = row to update
     */
    toggleRow( e, index, rowData ) {
        // This sets the value of the row
        const { selected } = this.state;
        selected[ index ] = e.target.checked;
        if ( e.target.checked ) {
            this.setState({ selectedRows: [ ...this.state.selectedRows, rowData ]});
        } else {
            this.setState({ selectedRows: this.state.selectedRows.filter( ( item ) => item.id !== rowData.id ) });
        }

        this.setState({ selected }, () => {
            let flag = true;
            const pageStart = ( ( this.tableComponent.state.page + 1 ) * this.tableComponent.state.pageSize ) - this.tableComponent.state.pageSize;
            const pageEnd = this.tableComponent.state.sortedData.length - pageStart > this.tableComponent.state.pageSize ? ( this.tableComponent.state.page + 1 ) * this.tableComponent.state.pageSize : this.tableComponent.state.sortedData.length;
            for ( let i = pageStart; i < pageEnd; i += 1 ) {
                for ( let x = 0; x < selected.length; x += 1 ) {
                    const row = this.tableComponent.state.sortedData[ i ];
                    if ( row && x === row.__index && !row.__original.noselect && selected[ x ] === false ) {
                        flag = false;
                    }
                }
            }

            if ( this.headerSelect.checked !== flag ) {
                this.headerSelect.checked = flag;
                const pageSelected = this.state.pageSelected;
                pageSelected[ `${this.tableComponent.state.page}` ] = flag;
                this.setState({ pageSelected });
            }
            this.props.onSelectionChange && this.props.onSelectionChange({ pageSelected: this.state.pageSelected, selected: this.state.selected, page: this.tableComponent.state.page + 1, selectedRows: this.state.selectedRows });
        });
    }

    /**
     * this handles toggles of checkboxes of visible rows
     * @param e = event
     * @param page = current table page
     * @param size = current table size
     */
    selectPage( e, page, size ) {
        const newSelected = this.state.selected;
        const newPageSelected = this.state.pageSelected;
        const sortedData = this.tableComponent.state.sortedData;
        const pageStart = ( page * size ) - size;
        const pageEnd = newSelected.length - pageStart > size ? page * size : newSelected.length;
        for ( let index = pageStart; index < pageEnd; index += 1 ) {
            const row = sortedData[ index ];
            if ( row && row.__original.noselect ) {
                newSelected[ row.__index ] = false;
            } else if ( row ) {
                newSelected[ row.__index ] = !!e.target.checked;
            }
        }
        // determines the current items in the page
        const currentItems = this.props.data.slice( pageStart, pageEnd );

        if ( e.target.checked ) {
             // concat the prev selected item and the new selected item then select only the unique items by id
            this.setState({ selectedRows: _.uniqBy([ ...currentItems, ...this.state.selectedRows ], 'id' ) });
        } else {
            // filter items that are unselected
            this.setState({ selectedRows: this.state.selectedRows.filter( ( item ) => !currentItems.find( ( f ) => item.id === f.id ) ) });
        }
        newPageSelected[ `${page - 1}` ] = !!e.target.checked;
        this.setState({ selected: newSelected, pageSelected: newPageSelected }, () => {
            this.props.onSelectionChange && this.props.onSelectionChange({ pageSelected: newPageSelected, selected: newSelected, page, selectedRows: this.state.selectedRows });
        });
    }

    /**
     * this renders empty table custom component
     */
    renderEmptyTableContent( nextProps ) {
        if ( !nextProps.emptyTableComponent ) {
            return;
        }

        const tableElement = document.querySelector( '.rt-tbody' );
        const foundElement = tableElement.querySelector( '.react-table-no-data' );

        if ( ( nextProps.data || []).length > 0 ) {
            foundElement && foundElement.remove();
        }

        if (
            ( nextProps.data || []).length === 0 &&
            !foundElement
        ) {
            const element = document.createElement( 'div' );
            element.classList.add( 'react-table-no-data' );

            tableElement.append( element );
            ReactDOM.render(
                React.createElement( this.props.emptyTableComponent ),
                element
            );
        }
    }

    /**
     *
     * Table
     *
     */
    render() {
        const {
            pagination,
            sizeOptions,
            pageSize,
            defaultSorting,
            showFilters,
            onRowClick,
            SubComponent,
            selectable,
            loading,
            showPageSizeOptions,
            className,
            onPageChange,
            onPageSizeChange,
            onSortingChange,
            onFilteringChange,
            page,
            pages,
            manual,
            external
        } = this.props;
        const { columns } = this.state;

        const manualPaginationProps = manual ?
        {
            onPageChange,
            onPageSizeChange,
            onSortingChange,
            onFilteringChange,
            page,
            pageSize,
            pages,
            manual
        } : {
            defaultPageSize: pageSize
        };

        const externalFooterPaginationProps = external ? {
            page,
            pageSize,
            pages
        } : {};

        return (
            <StyledDiv className={ className }>
                <ReactTable
                    className="-highlight"
                    data={ this.props.data }
                    columns={ columns }
                    showPagination={ pagination }
                    pageSizeOptions={ sizeOptions }
                    showPageSizeOptions={ showPageSizeOptions }
                    defaultSorting={ defaultSorting }
                    showFilters={ showFilters }
                    getTdProps={ onRowClick }
                    SubComponent={ SubComponent }
                    loading={ loading }
                    ref={ ( ref ) => { this.tableComponent = ref; } }
                    getProps={ this.getProps }
                    getTrProps={ ( state, rowInfo ) => {
                        const response = {};

                        if ( selectable && rowInfo ) {
                            response.className = this.state.selected[ rowInfo.index ] ? 'selected' : '';
                        }

                        if ( rowInfo && rowInfo.row.noselect ) {
                            return {
                                ...response,
                                style: {
                                    background: '#f2faed'
                                }
                            };
                        }

                        return response;
                    } }
                    onChange={ ( tableProps ) => {
                        this.setState({
                            sorting: tableProps.sorting
                        });

                        if ( selectable ) {
                            // This checks if header should be updated
                            const { selected } = this.state;
                            let flag = true && !!selected.length;

                            if ( flag ) {
                                const pageStart = ( ( tableProps.page + 1 ) * tableProps.pageSize ) - tableProps.pageSize;
                                const pageEnd = tableProps.sortedData.length - pageStart > tableProps.pageSize ? ( tableProps.page + 1 ) * tableProps.pageSize : tableProps.sortedData.length;
                                for ( let i = pageStart; i < pageEnd; i += 1 ) {
                                    for ( let x = 0; x < selected.length; x += 1 ) {
                                        if ( x === tableProps.sortedData[ i ].__index && selected[ x ] === false ) {
                                            flag = false;
                                            break;
                                        }
                                    }
                                }
                            }

                            if ( this.headerSelect && this.headerSelect.checked !== flag ) {
                                this.headerSelect.checked = flag;
                                const pageSelected = this.state.pageSelected;
                                pageSelected[ `${tableProps.page}` ] = flag;
                                this.setState({ pageSelected });
                            }
                        }
                        this.props.onDataChange && this.props.onDataChange( tableProps );
                    } }
                    noDataText={ this.props.noDataText }
                    defaultSorted={ this.props.defaultSorted }
                    { ...manualPaginationProps }
                    { ...externalFooterPaginationProps }
                />
            </StyledDiv>
        );
    }
}

export default Table;

/**
 * Checkbox for table
 */
function TableCheckbox( props ) {
    return (
        <input
            id={ props.id }
            className="checkbox"
            checked={ props.selected }
            type="checkbox"
            onChange={ ( e ) => { props.toggleRow( e, props.index, props.rowData ); } }
            disabled={ props.disabled }
        />
    );
}

TableCheckbox.propTypes = {
    id: React.PropTypes.string,
    selected: React.PropTypes.bool,
    toggleRow: React.PropTypes.func,
    index: React.PropTypes.number,
    disabled: React.PropTypes.bool,
    rowData: React.PropTypes.object
};
