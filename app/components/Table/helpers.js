import orderBy from 'lodash/orderBy';

/**
 * Checks if any table row is selected
 * @param {Object} table
 * @returns {Boolean}
 */
function isAnyRowSelected( table ) {
    return table && table.state.selected.some( ( rowSelected ) => rowSelected );
}

/**
 * Get sorted data for export
 * @param {Object} table
 * @returns {Array}
 */
function getSortedData( table ) {
    const selected = [];

    const sortingOptions = table.state.sorting;

    table.state.selected.forEach( ( rowSelected, index ) => {
        if ( rowSelected ) {
            selected.push( table.props.data[ index ]);
        }
    });

    return orderBy(
        selected,
        sortingOptions.map( ( o ) => o.id ),
        sortingOptions.map( ( o ) => ( o.desc ? 'desc' : 'asc' ) )
    );
}

/**
 * Get ids for delete
 * @param {Object} table
 * @returns {Array}
 */
function getIdsOfSelectedRows( table ) {
    const ids = [];

    table.state.selected.forEach( ( rowSelected, index ) => {
        if ( rowSelected ) {
            ids.push( table.props.data[ index ].id );
        }
    });

    return ids;
}

export {
    isAnyRowSelected,
    getSortedData,
    getIdsOfSelectedRows
};
