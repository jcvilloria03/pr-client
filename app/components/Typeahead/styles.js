import styled from 'styled-components';

export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;

    .rbt-input-hint {
        top: 8px !important;
    }

    label {
        color: #5b5b5b;
        font-size: 14px;
        margin-bottom: 4px;
        font-weight: 400;

        span.required {
            color: #eb7575;
        }
    }

    input {
        padding: 0 .75rem;
    }

    .rbt-input-hint-container input, .input-group {
        border-color: #95989a;
        order: 2;
        line-height: 28px;
        background-color: #FFF;
        height: 46px;

        &:focus {
            border-color: #0096d0;
            background-color: #fdfdfd;

            &~label {
                color: #149ed3;
            }

            & + .input-group-addon {
                border-color: #0096d0;
            }
        }
        .input-group-addon {
            background-color: #f5f5f5;
            border: 1px solid rgb(149, 152, 154);
            border-left: none;
        }
        &:disabled {
            border-color: #c7c7c7;
            background: #f8f8f8;
            color: #d3d3d3;

            & + .input-group-addon {
                border-color: #c7c7c7;
            }

            &~label {
                color: #9da0a1;
            }
        }
        &.error {
            border-color: #f21108;

            &~label {
                color: #f2130a;
            }

            & + .input-group-addon {
                border-color: #f21108;
            }
        }
    }

    p {
        order: 3;
        color: #f21108;
        font-size: 13px;
        padding-left: 2px;
        margin-bottom: 6px;
    }
`;
