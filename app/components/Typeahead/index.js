import React from 'react';
import { Typeahead as BootstrapTypeahead, AsyncTypeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import {
    Wrapper
} from './styles';

/**
 *
 * Input
 *
 */
class Typeahead extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        type: React.PropTypes.oneOf([
            'text',
            'password',
            'number',
            'email'
        ]),
        options: React.PropTypes.oneOfType([
            React.PropTypes.array,
            React.PropTypes.object
        ]),
        filterBy: React.PropTypes.oneOfType([
            React.PropTypes.array,
            React.PropTypes.func
        ]),
        labelKey: React.PropTypes.func,
        className: React.PropTypes.string,
        disabled: React.PropTypes.bool,
        id: React.PropTypes.string.isRequired,
        required: React.PropTypes.bool,
        onInputChange: React.PropTypes.func,
        onChange: React.PropTypes.func,
        onFocus: React.PropTypes.func,
        onBlur: React.PropTypes.func,
        label: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.element,
            React.PropTypes.node,
            React.PropTypes.symbol
        ]),
        placeholder: React.PropTypes.string,
        value: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.number
        ]),
        defaultValue: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.number
        ]),
        multiple: React.PropTypes.bool,
        async: React.PropTypes.bool,
        isLoading: React.PropTypes.bool,
        onSearch: React.PropTypes.func
    };

    static defaultProps = {
        value: '',
        type: 'text',
        disabled: false,
        filterBy: [],
        options: []
    };

    /**
     * constructor method of the component
     */
    constructor( props ) {
        super( props );
        this.state = {
            errorMessage: '&nbsp;',
            error: false,
            value: this.props.value,
            focus: false,
            disabled: props.disabled
        };

        // custom methods
        this._validate = this._validate.bind( this );
        this._handleChange = this._handleChange.bind( this );
        this._getLabel = this._getLabel.bind( this );
        this._handleFocus = this._handleFocus.bind( this );
    }

    /**
     * handles property changes
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.value !== this.props.value && this.setState({ value: nextProps.value });
        nextProps.disabled !== this.props.disabled && this.setState({ disabled: nextProps.disabled });
    }

    /**
     * onChange handler - executes validation first then the callback if there is any.
     */
    _handleChange = ( event ) => {
        if ( event.target ) {
            event.preventDefault();
            this.setState({ focus: event.type !== 'blur' });
            this._validate( event.type !== 'blur' ? event.target.value : this.state.value, event.type );
        } else {
            this.setState({ focus: true });
            this._validate( event, 'change' );
        }
    }

    /**
     * validation for input value upon blur or change depending on input type
     *
     * @param value - value of input to validate
     * @returns {Boolean} returns error status
     */
    _validate = ( value, type = 'blur' ) => {
        const error = false;
        const errorMessage = '&nbsp;';
        let response;

        if ( value !== '' && value.length ) {
            response = {
                error,
                errorMessage,
                value
            };
        } else {
            response = {
                error: this.props.required || false,
                errorMessage: this.props.required ? 'This field is required' : errorMessage,
                value
            };
        }

        if ( type === 'blur' ) {
            this.setState( response, () => {
                this.props.onBlur && this.props.onBlur( value );
            });
        } else {
            response.error = false;
            this.setState({ error: response.error, errorMessage: '&nbsp;', value: response.value ? response.value : '' }, () => {
                this.props.onChange && this.props.onChange( value );
            });
        }

        return response.error;
    }

    _handleFocus = () => {
        this.setState({ focus: true }, () => {
            this.props.onFocus && this.props.onFocus();
        });
    }

    /**
     *  it will return the required label for the current input.
     */
    _getLabel() {
        const requiredIndicator = <span className="required">*</span>;
        const label = this.props.required ? <span>{requiredIndicator} {this.props.label}</span> : this.props.label;

        return (
            label
        );
    }

    /**
     * Render method
     */
    render() {
        const { async = false } = this.props;
        const InputProps = {
            options: this.props.options,
            filterBy: this.props.filterBy,
            labelKey: this.props.labelKey,
            defaultInputValue: this.props.defaultValue,
            className: this.state.error ? 'error' : '',
            id: this.props.id,
            name: this.props.id,
            type: this.props.type,
            disabled: this.state.disabled,
            onChange: this._handleChange,
            onFocus: this._handleFocus,
            onBlur: this._handleChange,
            onInputChange: this.props.onInputChange,
            placeholder: this.props.placeholder,
            multiple: this.props.multiple
        };

        return (
            <Wrapper className={ this.props.className } >
                {
                    this.props.label ? <label htmlFor={ this.props.id } style={ this.state.error ? { color: '#f2130a' } : this.state.focus ? { color: '#149ed3' } : {} }>{ this._getLabel() }</label> : ''
                }
                {!async ?
                    <BootstrapTypeahead
                        { ...InputProps }
                    />
                    : <AsyncTypeahead
                        { ...InputProps }
                        onSearch={ this.props.onSearch }
                        isLoading={ this.props.isLoading }
                        delay={ 600 }
                    />
                }
                <p style={ { opacity: this.state.error ? '1' : '0' } }>
                    { this.state.errorMessage }
                </p>
            </Wrapper>
        );
    }
}

export default Typeahead;
