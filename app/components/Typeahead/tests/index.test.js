import React from 'react';
import { shallow } from 'enzyme';
import Typeahead from '../index';

( () => {
    let lastTime = 0;
    const vendors = [ 'ms', 'moz', 'webkit', 'o' ];
    for ( let x = 0; x < vendors.length && !window.requestAnimationFrame; x += 1 ) {
        window.requestAnimationFrame = window[ `${vendors[ x ]}RequestAnimationFrame` ];
        window.cancelAnimationFrame = window[ `${vendors[ x ]}CancelAnimationFrame` ]
                                   || window[ `${vendors[ x ]}CancelRequestAnimationFrame` ];
    }

    if ( !window.requestAnimationFrame ) {
        window.requestAnimationFrame = ( callback ) => {
            const currTime = new Date().getTime();
            const timeToCall = Math.max( 0, 16 - ( currTime - lastTime ) );
            const id = window.setTimeout( () => callback( currTime + timeToCall ), timeToCall );
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if ( !window.cancelAnimationFrame ) {
        window.cancelAnimationFrame = ( id ) => {
            clearTimeout( id );
        };
    }
})();

describe( '<Typeahead />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <Typeahead id="test" />
        );
        expect( component.html() ).toBeTruthy();
    });

    it( 'should handle prop changes', () => {
        const component = shallow(
            <Typeahead id="test" label="test" value="3" />
        );
        expect( component.instance().props.value ).toEqual( '3' );
        component.setProps({ value: '4' });
        expect( component.instance().props.value ).toEqual( '4' );
    });

    it( 'should have a rendered label and placeholder', () => {
        const component = shallow(
            <Typeahead
                id="test"
                label="label"
                placeholder="placeholder"
            />
        );
        const props = component.instance().props;
        expect( props.label ).toEqual( 'label' );
        expect( props.placeholder ).toEqual( 'placeholder' );
    });

    // it( 'should throw an error if the input prop is set to required and the value is empty', () => {
    //     const _handleChange = jest.fn();
    //     const component = mount(
    //         <Typeahead
    //             onChange={ _handleChange }
    //             id="test"
    //             required
    //             label="test"
    //         />
    //     );
    //     const eventObject = {
    //         target: {
    //             value: ''
    //         }
    //     };
    //     component.find( 'input' ).simulate( 'blur', eventObject );

    //     expect(
    //         component.instance().state
    //     ).toEqual({
    //         error: true,
    //         focus: false,
    //         errorMessage: 'This field is required',
    //         value: '',
    //         disabled: false
    //     });
    // });

    // it( 'should pass validations if input is empty and required props is not defined', () => {
    //     const _handleChange = jest.fn();
    //     const component = mount(
    //         <Typeahead
    //             onChange={ _handleChange }
    //             id="test"
    //             label="test"
    //         />
    //     );
    //     const eventObject = {
    //         target: {
    //             value: ''
    //         }
    //     };
    //     component.find( 'input' ).simulate( 'blur', eventObject );

    //     expect( component.instance().state.error ).toEqual( false );
    // });

    it( 'should be able to reach validations externally', () => {
        const component = shallow(
            <Typeahead
                id="test"
                value="test"
                label="test"
            />
        );
        component.instance()._validate( 'Test Value' );
        expect(
            component.instance().state
        ).toEqual({
            error: false,
            focus: false,
            errorMessage: '&nbsp;',
            value: 'Test Value',
            disabled: false
        });
    });
});
