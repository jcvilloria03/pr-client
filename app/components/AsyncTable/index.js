/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import ReactTable from 'react-table';
import styled from 'styled-components';

import { styleProps } from './styles';

const StyledDiv = styled.div`
    ${styleProps}
`;

/**
*
* Async Table
*
*/
class AsyncTable extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        columns: React.PropTypes.array.isRequired,
        data: React.PropTypes.arrayOf( React.PropTypes.object ).isRequired,
        onRowClick: React.PropTypes.func,
        sorted: React.PropTypes.object,
        pagination: React.PropTypes.bool,
        onPageSizeChange: React.PropTypes.func,
        onPageChange: React.PropTypes.func,
        pages: React.PropTypes.number,
        loading: React.PropTypes.bool,
        pageSize: React.PropTypes.number,
        sizeOptions: React.PropTypes.arrayOf( React.PropTypes.number ),
        defaultSorting: React.PropTypes.array,
        showFilters: React.PropTypes.bool,
        SubComponent: React.PropTypes.func,
        selectable: React.PropTypes.bool,
        noDataText: React.PropTypes.string,
        onDataChange: React.PropTypes.func,
        onSelectionChange: React.PropTypes.func,
        onSortingChange: React.PropTypes.func,
        defaultSorted: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                id: React.PropTypes.string,
                desc: React.PropTypes.bool
            }),
        ),
        showPageSizeOptions: React.PropTypes.bool,
        page: React.PropTypes.number,
        tablePage: React.PropTypes.number
    };

    static defaultProps = {
        pagination: false,
        sizeOptions: [ 10, 20, 50, 100, 200 ],
        pageSize: 10,
        pages: null,
        selectable: false,
        noDataText: 'No rows found',
        loading: false,
        showPageSizeOptions: true
    };

    /**
     * component constructor
     * @param props
     */
    constructor( props ) {
        super( props );

        this.state = {
            selected: [],
            columns: [],
            pageSelected: {
                0: false
            },
            page: 0,
            disabledSelection: false,
            sorting: [],
            expandedRows: {}
        };

        this.toggleRow = this.toggleRow.bind( this );
        this.selectPage = this.selectPage.bind( this );
    }

    /**
     * this handles preparation if selectable prop is set to true
     */
    componentWillMount() {
        this.initColumns();
    }

   /**
    * handle actions when receiving changes in props
    */
    componentWillReceiveProps( nextProps ) {
        if ( nextProps.data.length !== this.props.data.length ) {
            const pageSelected = this.state.pageSelected;
            pageSelected[ this.tableComponent.state.page ] = false;

            if ( this.headerSelect ) {
                this.headerSelect.checked = false;
            }

            this.setState({
                selected: nextProps.data.map( ( item ) => item.selected || false ),
                pageSelected
            });
        }
    }

    /**
     * Init columns.
     */
    initColumns() {
        const { columns, selectable, data } = this.props;
        if ( selectable && columns[ 0 ].accessor !== 'select' ) {
            // Add a new column for checkbox as first index
            columns.unshift({
                accessor: 'select',
                header: <input
                    className="checkbox"
                    onChange={ ( e ) => { this.selectPage( e, this.tableComponent.state.page + 1 ); } }
                    type="checkbox"
                    ref={ ( ref ) => { this.headerSelect = ref; } }
                />,
                hideFilter: true,
                sortable: false,
                resizable: false,
                width: 61,
                style: { justifyContent: 'center' },
                className: 'select',
                render: ({ index, row }) => (
                    row.noselect ? '' : ( <TableCheckbox
                        selected={ this.state.selected[ index ] }
                        toggleRow={ this.toggleRow }
                        index={ index }
                        disabled={ this.state.disabledSelection }
                    /> )
                )
            });
            // append checkbox data for each data entry
            const selected = data.map( () => false );
            this.setState({ selected, columns });
        } else {
            this.setState({ columns });
        }
    }

    /**
     * handles toggling of checkboxes
     * @param index = row to update
     */
    toggleRow( e, index ) {
        // This sets the value of the row
        const { selected } = this.state;
        selected[ index ] = e.target.checked;
        this.setState({ selected }, () => {
            let flag = true;
            const pageStart = 0;
            const pageEnd = this.tableComponent.state.pageSize;
            for ( let i = pageStart; i < pageEnd; i += 1 ) {
                for ( let x = 0; x < selected.length; x += 1 ) {
                    const row = this.tableComponent.state.sortedData[ i ];
                    if ( row && x === row.__index && !row.__original.noselect && selected[ x ] === false ) {
                        flag = false;
                    }
                }
            }

            if ( this.headerSelect.checked !== flag ) {
                this.headerSelect.checked = flag;
                const pageSelected = this.state.pageSelected;
                pageSelected[ `${this.tableComponent.state.page}` ] = flag;
                this.setState({ pageSelected });
            }

            this.props.onSelectionChange && this.props.onSelectionChange({ pageSelected: this.state.pageSelected, selected: this.state.selected, page: this.tableComponent.state.page + 1 });
        });
    }

    /**
     * this handles toggles of checkboxes of visible rows
     * @param e = event
     * @param page = current table page
     */
    selectPage( e, page ) {
        const newSelected = this.state.selected;
        const newPageSelected = this.state.pageSelected;
        const sortedData = this.tableComponent.state.sortedData;
        const pageStart = 0;
        const pageEnd = this.tableComponent.state.pageSize;
        for ( let index = pageStart; index < pageEnd; index += 1 ) {
            const row = sortedData[ index ];

            if ( row && row.__original.noselect ) {
                newSelected[ row.__index ] = false;
            } else if ( row ) {
                newSelected[ row.__index ] = !!e.target.checked;
            }
        }

        newPageSelected[ `${page - 1}` ] = !!e.target.checked;
        this.setState({ selected: newSelected, pageSelected: newPageSelected }, () => {
            this.props.onSelectionChange && this.props.onSelectionChange({ pageSelected: newPageSelected, selected: newSelected, page });
        });
    }

    /**
     *
     * Table
     *
     */
    render() {
        const {
            data,
            pagination,
            sizeOptions,
            pageSize,
            defaultSorting,
            showFilters,
            onRowClick,
            SubComponent,
            onPageSizeChange,
            selectable,
            sorted,
            pages,
            loading,
            noDataText,
            defaultSorted,
            onSortingChange,
            showPageSizeOptions,
            tablePage
        } = this.props;

        const columns = this.state.columns;
        let page = this.state.page;
        // Add feature to set manually set async table pages
        if ( tablePage >= 0 && tablePage !== undefined ) {
            page = tablePage;
        }

        return (
            <StyledDiv>
                <ReactTable
                    className="-highlight"
                    data={ data }
                    columns={ columns }
                    showPagination={ pagination }
                    onPageChange={ ( pageIndex ) => {
                        if ( this.headerSelect ) {
                            this.headerSelect.checked = false;
                        }

                        this.setState({
                            page: pageIndex,
                            pageChanged: true,
                            selected: this.props.data.map( ( item ) => item.selected || false )
                        }, () => {
                            this.props.onPageChange( pageIndex + 1 );
                        });
                    } }
                    onPageSizeChange={ onPageSizeChange }
                    pageSize={ pageSize }
                    onRowClick={ onRowClick }
                    manual
                    pages={ pages }
                    page={ page }
                    sorted={ sorted }
                    loading={ loading }
                    pageSizeOptions={ sizeOptions }
                    defaultPageSize={ pageSize }
                    defaultSorting={ defaultSorting }
                    showFilters={ showFilters }
                    getTdProps={ onRowClick }
                    SubComponent={ SubComponent }
                    ref={ ( ref ) => { this.tableComponent = ref; } }
                    expandedRows={ this.state.expandedRows }
                    getTrProps={ ( state, rowInfo ) => {
                        const response = {};
                        if ( selectable && rowInfo ) {
                            response.className = this.state.selected[ rowInfo.index ] ? 'selected' : '';
                        }
                        return {
                            ...response,
                            onClick: () => {
                                this.setState({
                                    expandedRows: {
                                        ...this.state.expandedRows,
                                        [ rowInfo.viewIndex ]: !this.state.expandedRows[ rowInfo.viewIndex ] || false
                                    }
                                });
                            }
                        };
                    } }

                    onChange={ ( tableProps ) => {
                        this.setState({
                            sorting: tableProps.sorting
                        });

                        if ( selectable ) {
                            // This checks if header should be updated
                            const { selected } = this.state;
                            let flag = true && !!selected.length;

                            if ( flag ) {
                                const pageStart = ( ( tableProps.page + 1 ) * tableProps.pageSize ) - tableProps.pageSize;
                                const pageEnd = tableProps.sortedData.length - pageStart > tableProps.pageSize ? ( tableProps.page + 1 ) * tableProps.pageSize : tableProps.sortedData.length;

                                for ( let i = pageStart; i < pageEnd; i += 1 ) {
                                    for ( let x = 0; x < selected.length; x += 1 ) {
                                        if ( x === tableProps.sortedData[ i ].__index && selected[ x ] === false ) {
                                            flag = false;
                                            break;
                                        }
                                    }
                                }
                            }

                            if ( this.headerSelect.checked !== flag ) {
                                this.headerSelect.checked = flag;
                                const pageSelected = this.state.pageSelected;
                                pageSelected[ `${tableProps.page}` ] = flag;

                                this.setState({ pageSelected });
                            }
                        }

                        this.props.onDataChange && this.props.onDataChange( tableProps );
                    } }
                    noDataText={ noDataText }
                    defaultSorted={ defaultSorted }
                    showPageSizeOptions={ showPageSizeOptions }
                    onSortingChange={ onSortingChange }
                />
            </StyledDiv>
        );
    }
}

export default AsyncTable;

/**
 * Checkbox for table
 */
function TableCheckbox( props ) {
    return (
        <input
            className="checkbox"
            checked={ props.selected }
            type="checkbox"
            onChange={ ( e ) => { props.toggleRow( e, props.index ); } }
            disabled={ props.disabled }
        />
    );
}

TableCheckbox.propTypes = {
    selected: React.PropTypes.bool,
    toggleRow: React.PropTypes.func,
    index: React.PropTypes.number,
    disabled: React.PropTypes.bool
};
