import React from 'react';
import { shallow } from 'enzyme';
import AsyncTable from '../index';

const columns = [
    {
        accessor: 'test1',
        header: 'test1',
        sortable: true
    },
    {
        accessor: 'test2',
        header: 'true'
    },
    {
        accessor: 'test3',
        header: 'test3',
        sortable: true,
        minWidth: 100
    },
    {
        accessor: 'test4',
        header: 'test4',
        sortable: true
    }
];

const data = [
    {
        test1: 'string',
        test2: 'sample',
        test3: 100,
        test4: 'value'
    }
];

describe( '<AsyncTable />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <AsyncTable
                columns={ columns }
                data={ data }
            />
        );

        expect( component.find( '.ReactTable' ) ).toBeTruthy();
    });

    it( 'should render table with row selection feature', () => {
        const component = shallow(
            <AsyncTable columns={ columns } data={ data } pagination selectable />
        );

        expect( component.find( '.ReactTable' ) ).toBeTruthy();
    });
});
