import { css } from 'styled-components';

/**
 * Default styles for Accordion
 */
export const styleProps = css`
    .react-sanfona-item {
        background: #fff;
        margin-bottom: 10px;
        box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);

        &.react-sanfona-item-disabled {
            background: #F5F5F5;

            * {
                color: #8B8B8B;
                cursor: not-allowed;
            }

            .flag {
                span, span:after {
                    border-color: #8B8B8B;
                }
            }
        }

        &.react-sanfona-item-expanded {
            .flag {
                span:after {
                    transform: rotate(135deg);
                    margin-left: 11px;
                    margin-top: 8px;
                }
            }
        }

        & > div:first-of-type {
            display: inline-flex;
            width: 100%;

            h3 {
                flex-grow: 1;
                padding: 20px;
                margin: 0;
            }
        }

        &.done {
            .flag {
                span {
                    background: #00A5E5;
                    border-color: #00A5E5;
                    &:after {
                        content: '';
                        display: inline-block;
                        margin-top: 11px;
                        margin-left: 8px;
                        width: 25px;
                        height: 15px;
                        border-top: 0.2em solid #FFF;
                        border-right: 0.2em solid #FFF;
                        transform: rotate(135deg);
                    }
                }
            }
        }
    }

    .fa {
        margin-right: 10px;
    }

    .flag {
        display: flex;
        align-items: center;
        margin-right: 10px;
        span {
            display: inline-block;
            width: 3em;
            height: 3em;
            border: 0.2em solid #00A5E5;
            border-radius: 50%;
            margin-left: 1.5em;

            &:after {
                content: '';
                display: inline-block;
                margin-top: 11px;
                margin-left: 8px;
                width: 20px;
                height: 20px;
                border-top: 0.2em solid #00A5E5;
                border-right: 0.2em solid #00A5E5;
                transform: rotate(45deg);
            }
        }
    }
`;
