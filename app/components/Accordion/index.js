import React from 'react';
import { Accordion as Accord, AccordionItem } from 'react-sanfona';
import styled from 'styled-components';
import { styleProps } from './styles';

const Wrapper = styled.div`
    ${styleProps}
`;
/**
* Accordion
*/
class Accordion extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        items: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                title: React.PropTypes.oneOfType([
                    React.PropTypes.string,
                    React.PropTypes.number,
                    React.PropTypes.element
                ]).isRequired,
                content: React.PropTypes.oneOfType([
                    React.PropTypes.string,
                    React.PropTypes.number,
                    React.PropTypes.element
                ]).isRequired,
                expanded: React.PropTypes.bool,
                onExpand: React.PropTypes.func,
                onClose: React.PropTypes.func,
                disabled: React.PropTypes.bool,
                done: React.PropTypes.bool
            })
        ).isRequired,
        allowMultiple: React.PropTypes.bool,
        activeItems: React.PropTypes.array,
        onChange: React.PropTypes.func
    };

    static defaultProps = {
        allowMultiple: false,
        activeItems: []
    };

    /**
     * component constructorr
     */
    constructor( props ) {
        super( props );
        this.getTitle = this.getTitle.bind( this );
    }

    /**
     * appends right side icons to the accordion item titles
     */
    getTitle( title ) {
        return (
            <div>
                { title }
                <div className="flag"><span></span></div>
            </div>
        );
    }

    /**
    * Accordion render method.
    */
    render() {
        const { items } = this.props;
        return (
            <Wrapper>
                <Accord
                    allowMultiple={ this.props.allowMultiple }
                    activeItems={ this.props.activeItems }
                    onChange={ this.props.onChange }
                >
                    {
                        items.map( ( item, index ) => (
                            <AccordionItem
                                title={ this.getTitle( item.title ) }
                                key={ index }
                                expanded={ item.expanded }
                                onExpand={ item.onExpand }
                                onClose={ item.onClose }
                                disabled={ item.disabled }
                                className={ item.done ? 'done' : null }
                            >
                                { item.content }
                            </AccordionItem>
                        ) )
                    }
                </Accord>
            </Wrapper>
        );
    }
}

export default Accordion;
