import React from 'react';
import { shallow } from 'enzyme';

import Accordion from '../index';

const items = [
    {
        title: 'Your Company',
        content: 'test1'
    },
    {
        title: 'Users',
        content: 'test2'
    }
];

describe( '<Accordion />', () => {
    it( 'should have default prop value "className"', () => {
        const component = shallow(
            <Accordion items={ items } />
        );
        expect( component.find( '.react-sanfona' ) ).toBeTruthy();
    });
    it( 'should tagged an item as done', () => {
        const items2 = items;
        items2[ 0 ].done = true;
        const component = shallow(
            <Accordion items={ items2 } />
        );
        expect( component.find( '.react-sanfona' ) ).toBeTruthy();
    });
    it( 'should tagged an item as disabled', () => {
        const items2 = items;
        items2[ 0 ].disabled = true;
        const component = shallow(
            <Accordion items={ items2 } />
        );
        expect( component.find( '.react-sanfona' ) ).toBeTruthy();
    });
});
