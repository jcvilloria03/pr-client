import React from 'react';
import ProgressBar from './ProgressBar';

/**
 * attaches a progress bar to the component passed
 * @param WrappedComponent = Component Passed
 * @returns {AppWithProgressBar}
 */
function withProgressBar( WrappedComponent ) {
    /**
     * AppWithProgressBar
     */
    class AppWithProgressBar extends React.Component {
        /**
         * Component Constructor
         */
        constructor( props ) {
            super( props );
            this.state = {
                progress: -1,
                loadedRoutes: props.location && [props.location.pathname]
            };
            this.updateProgress = this.updateProgress.bind( this );
        }

        /**
         * Called before component is mounted
         */
        componentWillMount() {
            // Store a reference to the listener.
            /* istanbul ignore next */
            this.unsubscribeHistory = this.props.router && this.props.router.listenBefore( ( location ) => {
                    // Do not show progress bar for already loaded routes.
                if ( this.state.loadedRoutes.indexOf( location.pathname ) === -1 ) {
                    this.updateProgress( 0 );
                }
            });
        }

        /**
         * called when new props is passed and component needs to update.
         */
        componentWillUpdate( newProps, newState ) {
            const { loadedRoutes, progress } = this.state;
            const { pathname } = newProps.location;

            // Complete progress when route changes. But prevent state update while re-rendering.
            if ( loadedRoutes.indexOf( pathname ) === -1 && progress !== -1 && newState.progress < 100 ) {
                this.updateProgress( 100 );
                this.setState({
                    loadedRoutes: loadedRoutes.concat([pathname])
                });
            }
        }

        /**
         * Called before component unmounts.
         */
        componentWillUnmount() {
            // Unset unsubscribeHistory since it won't be garbage-collected.
            this.unsubscribeHistory = undefined;
        }

        /**
         * Sets the progress of the bar.
         */
        updateProgress( progress ) {
            this.setState({ progress });
        }

        /**
         * Component render method.
         */
        render() {
            return (
                <div>
                    <ProgressBar percent={ this.state.progress } updateProgress={ this.updateProgress } />
                    <WrappedComponent { ...this.props } />
                </div>
            );
        }
    }

    AppWithProgressBar.propTypes = {
        location: React.PropTypes.object,
        router: React.PropTypes.object
    };

    return AppWithProgressBar;
}

export default withProgressBar;
