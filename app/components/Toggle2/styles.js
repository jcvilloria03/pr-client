import styled from 'styled-components';

export const Wrapper = styled.div`
    * {
        box-sizing: border-box;
        position: relative;
        margin: 0px;
        padding: 0px;
        outline: none;
    }

    .rockerSwitch {
        color: #333;
        width: 100%;
        min-width: 500px;
        max-width: 800px;
        border-radius: 6px;
        overflow: hidden;
        margin: auto;
        background: #FFF;
        border: 1px solid #8D8D8D;
        display: flex;
        align-items: center;
    }
    .rockerSwitch > a:not(:last-of-type) {
        border-right: 1px solid #ccc;
    }
    .rockerSwitch > a {
        color: #000 !important;
        background: #fff;
        padding: 10px 50px;
        height: 60px;
        cursor: pointer;
        justify-content: center;
        font-size: 28px;
        flex-grow: 1;
        display: flex;
    }

    .rockerSwitch > a > div {
        display: flex;
        align-items: center;
    }

    .rockerSwitch > a:first-child {
      border-top-left-radius: 6px;
      border-bottom-left-radius: 6px;
      -moz-background-clip: padding;
      -webkit-background-clip: padding;
      background-clip: padding-box;
    }
    .rockerSwitch > a:last-child {
      -moz-background-clip: padding;
      -webkit-background-clip: padding;
      background-clip: padding-box;
      border-top-right-radius: 6px;
      border-bottom-right-radius: 6px;
    }
    .rockerSwitch > a:hover, .rockerSwitch > a:focus {
      background: rgb(234, 249, 255);
      color: #000;
      text-decoration: none;
    }
    .rockerSwitch > a.selected {
      background: rgb(0,165,226);
      color: #fff !important;
    }

    .rockerSwitch > a > div > .title {
      font-size: 16px;
      font-weight: 600;
      color: inherit;
    }

    .rockerSwitch > a > div > div > .sub {
      font-size: 12px;
      font-weight: 400;
      color: inherit;
    }

    .icon {
      display: flex;
      margin-right: 10px;
    }

    .icon svg {
      width: 60px;
      height: 60px;
    }
`;
