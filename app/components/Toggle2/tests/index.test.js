import React from 'react';
import { shallow } from 'enzyme';

import Toggle2 from '../index';

describe( '<Toggle2 />', () => {
    it( 'Should mount the component', () => {
        const component = shallow(
            <Toggle2
                options={ [
                    { title: 'test1', value: 'test1' },
                    { title: 'test2', value: 'test2' }
                ] }
            />
        );

        expect( component.html() ).toBeTruthy();
    });

    it( 'can have a default value selected', () => {
        const component = shallow(
            <Toggle2
                options={ [
                    { title: 'test1', value: 'test1' },
                    { title: 'test2', value: 'test2' }
                ] }
                defaultSelected="test1"
            />
        );

        expect( component.find( 'a.selected' ) ).toBeTruthy();
        expect( component.instance().state.value ).toEqual( 'test1' );
    });

    it( 'changes value when a selection is clicked', () => {
        const component = shallow(
            <Toggle2
                options={ [
                    { title: 'test1', value: 'test1' },
                    { title: 'test2', value: 'test2' }
                ] }
                defaultSelected="test1"
            />
        );

        expect( component.find( 'a.selected' ) ).toBeTruthy();
        expect( component.instance().state.value ).toEqual( 'test1' );
    });

    it( 'can have a callback when changes are made', () => {
        const spy = jest.fn();
        const component = shallow(
            <Toggle2
                options={ [
                    { title: 'test1', value: 'test1' },
                    { title: 'test2', value: 'test2' }
                ] }
                defaultSelected="test1"
                onChange={ spy }
            />
        );
        component.instance()._onChange( 'test2' );
        expect( spy ).toHaveBeenCalled();
    });
});
