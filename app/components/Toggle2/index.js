import React from 'react';
import { Wrapper } from './styles';

/**
* Toggle
*/
class Toggle2 extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        options: React.PropTypes.arrayOf( React.PropTypes.shape({
            label: React.PropTypes.string.isRequired,
            value: React.PropTypes.any.isRequired
        }) ),
        defaultSelected: React.PropTypes.any,
        onChange: React.PropTypes.func
    }

    static defaultProps = {
        defaultSelected: null
    }

    /**
     * component constructor
     * @param props = component props
     */
    constructor( props ) {
        super( props );

        this.state = {
            value: props.defaultSelected
        };

        this._onChange = this._onChange.bind( this );
    }

    /**
     * changes the value of toggle onclick and triggers any callback function
     * @param value
     * @private
     */
    _onChange( value ) {
        value !== this.state.value && this.setState({ value }, () => {
            this.props.onChange && this.props.onChange( value );
        });
    }

    /**
    * Toggle render method.
    */
    render() {
        return (
            <Wrapper>
                <div className="rockerSwitch">
                    {
                        this.props.options.map( ( option, index ) => (
                            <a href key={ index } className={ this.state.value === option.value ? 'selected' : '' } onClick={ ( e ) => { e.preventDefault(); this._onChange( option.value ); } }>
                                <div className="content">
                                    <div className="title">{ option.label }</div>
                                </div>
                            </a>
                        ) )
                    }
                </div>
            </Wrapper>
        );
    }
}

export default Toggle2;
