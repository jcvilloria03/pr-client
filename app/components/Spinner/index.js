import React from 'react';
import styled from 'styled-components';

export const SpinnerWrapper = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background: rgba(0, 0, 0, 0.4);
    z-index: 9999;

    i {
        position: absolute;
        width: 60px;
        height: 60px;
        overflow: hidden;
        top: 50%;
        left: calc(50% - 32px);
        transform: translate(-50%, -50%);
        font-size: 60px;
    }
`;

export class Spinner extends React.Component {
    render() {
        return (
            <SpinnerWrapper>
                <i className="fa fa-lg fa-circle-o-notch fa-spin fa-fw" />
            </SpinnerWrapper>
        );
    }
}
