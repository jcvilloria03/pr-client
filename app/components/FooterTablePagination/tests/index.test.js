import { shallow } from 'enzyme';
import React from 'react';
import FooterTablePagination from '../index';

const pagination = {
    from: 1,
    to: 271,
    total: 2701,
    current_page: 1,
    last_page: 271,
    per_page: 10
};

const page = 1;
const tablePageSize = 10;

describe( '<FooterTablePagination />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <FooterTablePagination
                id="test"
                pagination={ pagination }
                tablePageSize={ tablePageSize }
                page={ page }
            />
        );
        expect( component.find( 'FooterTablePagination' ) ).toBeTruthy();
    });
});
