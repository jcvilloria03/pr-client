/* eslint-disable react/no-did-update-set-state */
/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import { Wrapper } from './styles';
import Select from '../Select';
import Button from '../Button';

/**
 * FooterTablePagination
 */
class FooterTablePaginationV2 extends React.Component {
    // eslint-disable-line react/prefer-stateless-functiofffn
    static propTypes = {
        fluid: React.PropTypes.bool,
        bgColor: React.PropTypes.string,
        page: React.PropTypes.number,
        pageSize: React.PropTypes.number,
        pagination: React.PropTypes.object,
        onPageChange: React.PropTypes.func,
        onPageSizeChange: React.PropTypes.func,
        paginationLabel: React.PropTypes.string,
        sizeOptions: React.PropTypes.array
    };

    defaultProps = {
        page: 0,
        bgColor: '#f0f4f6'
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            pages: [],
            lastPage: 0,
            sizeOptions: [
                { value: 10, label: '10' },
                { value: 20, label: '20' },
                { value: 50, label: '50' },
                { value: 100, label: '100' },
                { value: 200, label: '200' }
            ],
            pagination: this.props.pagination || {}
        };
    }

    /**
     * Pages
     */

    componentDidUpdate( prevProps, prevState ) {
        if ( prevProps.pagination !== this.props.pagination ) {
            this.setState({
                pagination: this.props.pagination
            });
        }

        const { pagination, sizeOptions } = this.state;

        const pages = [];
        let lastPage = 0;

        if ( prevState.pagination !== pagination ) {
            lastPage =
                pagination.last_page ||
                Math.ceil( pagination.total / pagination.pageSize );

            for ( let i = 1; i <= lastPage; i += 1 ) {
                pages.push({ value: i, label: `${i}` });
            }

            this.setState({
                pages,
                lastPage
            });
        }

        if ( prevState.sizeOptions !== sizeOptions ) {
            this.setState({
                sizeOptions: sizeOptions.map( ( option ) => ({
                    value: option,
                    label: String( option )
                }) )
            });
        }
    }

    render() {
        const { fluid, page, pageSize, onPageChange, onPageSizeChange } =
            this.props;

        const { pages, lastPage, sizeOptions } = this.state;
        return (
            <Wrapper fluid={ fluid } bgColor={ '#FFFFFF' }>
                <div className="footer-wrapper">
                    <div className="perPageWrapper">
                        <span>Display</span>
                        <Select
                            id="records_per_page"
                            label=""
                            data={ sizeOptions }
                            value={ pageSize }
                            onChange={ ( item ) => onPageSizeChange( item.value ) }
                        />
                        <span>per page</span>
                    </div>
                    <div className="pageWrapper">
                        <div className="details">
                            <span> Page </span>
                            <Select
                                id="pages"
                                label=""
                                data={ pages }
                                value={ page }
                                onChange={ ( item ) => onPageChange( item.value ) }
                            />
                            <span>of</span>
                            <span>{lastPage || 0}</span>
                        </div>
                        <div className="button-wrapper">
                            <Button
                                label="Previous"
                                type="neutral"
                                size="default"
                                onClick={ () => onPageChange( page - 1 ) }
                                disabled={ page <= 1 }
                            />
                            <Button
                                label="Next"
                                type="neutral"
                                size="default"
                                onClick={ () => onPageChange( page + 1 ) }
                                disabled={ page >= lastPage }
                            />
                        </div>
                    </div>
                </div>
            </Wrapper>
        );
    }
}

export default FooterTablePaginationV2;
