import styled from 'styled-components';

export const Wrapper = styled.div`
    background-color: ${(props) => props.bgColor || '#f0f4f6'};
    padding: ${(props) => (props.fluid ? '.5rem 0' : '.5rem 1rem')};
    width: ${(props) => (props.fluid ? '100%' : '100vw')};
    z-index: 2;

    .footer-wrapper {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        width: 100%;

        .perPageWrapper {
            display: flex;
            flex-direction: row;
            align-items: center;
            font-size: 14px;

            .Select {
                margin: 0 0.5rem;
                width: 5rem;

                .Select-menu-outer {
                    top: auto;
                    bottom: 100%;
                }
            }
        }

        .pageWrapper {
            display: flex;
            flex-direction: row;
            align-items: center;

            .details {
                display: flex;
                flex-direction: row;
                align-items: center;
                background-color: transparent;
                border: none;

                .Select {
                    margin: 0 0.5rem;
                    width: 5rem;

                    .Select-menu-outer {
                        top: auto;
                        bottom: 100%;
                    }
                }

                span:last-child {
                    margin-left: 0.5rem;
                }
            }

            button {
                cursor: pointer;
                &:focus {
                    outline: 0;
                }

                .icon {
                    font-size: 15px;
                    color: black;
                    display: inline-flex;
                    min-width: 15px;

                    > i {
                        align-self: center;
                    }
                }

                .chevron-icon-left > svg {
                    height: 10px;
                    transform: rotate(90deg);
                    margin-right: 1rem;
                }

                .chevron-icon-right > svg {
                    height: 10px;
                    transform: rotate(270deg);
                    margin-left: 1rem;
                }
            }
        }

        .button-wrapper {
            padding-left: 5px;

            button {
                margin-left: 10px;
            }
        }
    }
`;
