/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import { Container } from 'reactstrap';
import { Wrapper } from './styles';
import Select from '../Select';
import Icon from '../Icon';
// import Button from '../Button';

/**
* FooterTablePagination
*/
class FooterTablePagination extends React.Component { // eslint-disable-line react/prefer-stateless-functiofffn
    static propTypes = {
        fluid: React.PropTypes.bool,
        bgColor: React.PropTypes.string,
        page: React.PropTypes.number,
        pageSize: React.PropTypes.number,
        pagination: React.PropTypes.object,
        onPageChange: React.PropTypes.func,
        onPageSizeChange: React.PropTypes.func,
        paginationLabel: React.PropTypes.string,
        sizeOptions: React.PropTypes.array
    };

    defaultProps = {
        page: 0,
        bgColor: '#f0f4f6'
    }

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            pages: [],
            lastPage: 0,
            sizeOptions: [
              { value: 10, label: '10' },
              { value: 20, label: '20' },
              { value: 50, label: '50' },
              { value: 100, label: '100' },
              { value: 200, label: '200' }
            ]
        };
    }

    /**
     * Pages
     */

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.pagination ) {
            const pages = [];
            let lastPage = 0;

            if ( nextProps.pagination.last_page ) {
                lastPage = nextProps.pagination.last_page;
            } else {
                lastPage = Math.ceil( nextProps.pagination.total / nextProps.pageSize );
            }

            for ( let i = 1; i <= lastPage; i += 1 ) {
                pages.push({ value: i, label: `${i}` });
            }

            this.setState({
                pages,
                lastPage
            });
        }

        if ( nextProps.sizeOptions ) {
            this.setState({ sizeOptions: nextProps.sizeOptions.map( ( option ) => ({
                value: option,
                label: String( option )
            }) )
            });
        }
    }

    /**
     *
     * FooterTablePagination
     *
     */

    render() {
        const {
        fluid,
        page,
        bgColor,
        pageSize,
        onPageChange,
        onPageSizeChange,
        paginationLabel
        } = this.props;

        const {
          pages,
          lastPage,
          sizeOptions
        } = this.state;
        return (
            <Wrapper fluid={ fluid } bgColor={ bgColor }>
                <Container style={ { padding: fluid && '0' } } >
                    <div className="footer-wrapper">
                        <div className="perPageWrapper">
                            <span>Display</span>
                            <Select
                                id="records_per_page"
                                label=""
                                data={ sizeOptions }
                                value={ pageSize }
                                onChange={ ( item ) => onPageSizeChange( item.value ) }
                            />
                            <span>records per page</span>
                        </div>
                        <div className="displayPagination">
                            <span>{ paginationLabel }</span>
                        </div>
                        <div className="pageWrapper">
                            <button
                                onClick={ () => onPageChange( page - 1 ) }
                                style={ page === 1 ? { display: 'none' } : {} }
                                className="button-icon"
                                type="button"
                            >
                                <Icon className="chevron-icon-left" name="chevron" />
                            </button>
                            <div className="details">
                                <span> Page </span>
                                <Select
                                    id="pages"
                                    label=""
                                    data={ pages }
                                    value={ page }
                                    onChange={ ( item ) => onPageChange( item.value ) }
                                />
                                <span>of</span>
                                <span>{ lastPage }</span>
                            </div>
                            <button
                                onClick={ () => onPageChange( page + 1 ) }
                                style={ page === lastPage ? { display: 'none' } : {} }
                                className="button-icon"
                                type="button"
                            >
                                <Icon className="chevron-icon-right" name="chevron" />
                            </button>
                        </div>
                    </div>
                </Container>
            </Wrapper>
        );
    }
}

export default FooterTablePagination;
