/* eslint-disable require-jsdoc */

import {
    GET_DETAILS
} from './constants';

export function getDetails() {
    return {
        type: GET_DETAILS
    };
}
