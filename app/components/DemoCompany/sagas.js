/* eslint-disable require-jsdoc */
import { call, put, take, cancel } from 'redux-saga/effects';
import { takeLatest } from 'redux-saga';
import { LOCATION_CHANGE } from 'react-router-redux';
import { Fetch } from 'utils/request';
import { company } from 'utils/CompanyService';
import { auth } from 'utils/AuthService';

import {
    GET_DETAILS,
    SET_SETUP_PROGRESS
} from './constants';

export function* getDetails() {
    if ( !auth.isOwner() || !company.isDemoCompany() ) {
        return;
    }

    try {
        const lastActiveCompanyId = company.getLastActiveCompanyId();
        yield put({ type: SET_SETUP_PROGRESS, payload: { isFetching: true }});

        const details = yield call( Fetch, `/philippine/company_demo/details/${lastActiveCompanyId}`,
            {
                method: 'GET',
                authzModules: 'control_panel.companies.company_information'
            }
        );

        if ( details ) {
            yield put({ type: SET_SETUP_PROGRESS,
                payload: {
                    employeeRecords: details.employee_records,
                    attendanceComputation: details.attendance_computation,
                    payrollCalculation: details.payroll_calculation,
                    isFetching: false
                }});
        }
    } catch ( error ) {
        yield put({ type: SET_SETUP_PROGRESS, payload: { isFetching: false }});
    }
}

export function* watchForGetDetails() {
    const watcher = yield takeLatest( GET_DETAILS, getDetails );

    yield take( LOCATION_CHANGE );
    yield cancel( watcher );
}

export default [
    watchForGetDetails
];
