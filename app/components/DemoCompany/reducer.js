/* eslint-disable require-jsdoc */

import { SET_SETUP_PROGRESS } from './constants';

const initialState = {
    setupProgress: {
        employeeRecords: false,
        attendanceComputation: false,
        payrollCalculation: false,
        isFetching: true,
    }
};

function demoCompanyReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_SETUP_PROGRESS:
            return {
                ...state,
                setupProgress: {
                    ...state.setupProgress,
                    ...action.payload
                }
            };
        default:
            return state;
    }
}

export default demoCompanyReducer;
