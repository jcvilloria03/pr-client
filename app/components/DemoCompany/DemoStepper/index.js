/* eslint-disable require-jsdoc */
/* eslint-disable react/prefer-stateless-function */

import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';

import { company } from 'utils/CompanyService';

import Stepper from '../../Stepper';
import { makeSelectSetupProgress } from '../selectors';
import * as demoCompanyActions from '../actions';

const Wrapper = styled.div`
    position: relative;
    width: 100%;

    min-height: 0;
    padding: 10px;
    background: #83D24B;

    opacity: 1;
    height: 139px;

    &.is-hidden {
        opacity: 0;
        transition: .5s;
        height: 1px;
        padding: 0;
        overflow: hidden;
    }
`;

const StepperContainer = styled.div`
    margin: 0 auto;
    padding: 0;
`;

const Title = styled.p`
    font-size: 20px;
    font-weight: 300;
    color: #818a91;
    margin: 0 0 5px 0;
    color: #FFF;
    text-align: center;
`;

const StyledStepper = styled( Stepper )`
    margin: 0 auto;
`;

const steps = [
    { label: 'Employee Records' },
    { label: 'Attendance Computation' },
    { label: 'Payroll Calculation' }
];

class DemoStepper extends React.Component {
    static propTypes = {
        setupProgress: React.PropTypes.object.isRequired,
        getDetails: React.PropTypes.func.isRequired
    };

    constructor( props ) {
        super( props );

        this.state = {
            hidden: true
        };

        this.pollGetDetails = this.pollGetDetails.bind( this );
    }

    componentDidMount() {
        this.pollGetDetails();
    }

    componentDidUpdate( prevProps ) {
        if ( prevProps.setupProgress !== this.props.setupProgress ) {
            const { setupProgress } = this.props;
            const { employeeRecords, attendanceComputation, payrollCalculation, isFetching } = setupProgress;
            if ( !isFetching && ( !employeeRecords || !attendanceComputation || !payrollCalculation ) ) {
                this.setState({ hidden: false });
            } else if ( employeeRecords && attendanceComputation && payrollCalculation ) {
                setTimeout( () => {
                    this.setState({ hidden: true });
                    clearTimeout( this.getDetailsTimeoutFn );
                }, 2000 );
            }
        }
    }

    componentWillUnmount() {
        clearTimeout( this.getDetailsTimeoutFn );
    }

    getActiveStep = () => {
        const {
            employeeRecords,
            attendanceComputation,
            payrollCalculation
        } = this.props.setupProgress;
        let activeStep = 0;

        if ( employeeRecords ) {
            activeStep = 1;
        }

        if ( attendanceComputation ) {
            activeStep = 2;
        }

        if ( payrollCalculation ) {
            activeStep = 3;
        }

        return activeStep;
    };

    getDetailsTimeoutFn = () => setTimeout( this.pollGetDetails, 5000 );

    pollGetDetails() {
        const { getDetails, setupProgress } = this.props;
        const { employeeRecords, attendanceComputation, payrollCalculation } = setupProgress;
        if ( !employeeRecords || !attendanceComputation || !payrollCalculation ) {
            getDetails();
            this.getDetailsTimeoutFn();
        }
    }

    render() {
        const { hidden } = this.state;

        if ( !company.isDemoCompany() ) {
            return null;
        }

        return (
            <Wrapper className={ `demo-stepper ${hidden ? 'is-hidden' : ''}` }>
                <StepperContainer>
                    <Title>Preparing Demo Account..</Title>
                    <StyledStepper
                        steps={ steps }
                        activeStep={ this.getActiveStep() }
                        size={ 35 }
                        circleFontSize={ 14 }
                        titleFontSize={ 14 }
                        defaultColor="#FFF"
                        defaultTitleColor="#818a91"
                        completeColor="#83D24B"
                        completeTitleColor="#FFF"
                        activeColor="#FFF"
                        activeTitleColor="#FFF"
                        icon="spinner"
                        rootStyle={ { margin: '10px 0' } }
                    />
                </StepperContainer>
            </Wrapper>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    setupProgress: makeSelectSetupProgress()
});

/**
 * combine dispatchers and actions to props
 */
function mapDispatchToProps( dispatch ) {
    return bindActionCreators( demoCompanyActions, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( DemoStepper );
