import { createSelector } from 'reselect';

const selectDemoCompanyDomain = () => ( state ) => state.get( 'demoCompany' );

const makeSelectSetupProgress = () => createSelector(
    selectDemoCompanyDomain(),
    ( state ) => state.setupProgress
);

export { makeSelectSetupProgress };
