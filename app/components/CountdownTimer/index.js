import React, { Component } from 'react';

const { SESSION_TIMEOUT_COUNTDOWN_SECONDS } = process.env;

/**
* Countdown Timer
*/
export default class CountdownTimerContainer extends Component {
    static propTypes = {
        onFinish: React.PropTypes.func.isRequired
    }

    static defaultProps = {
        seconds: parseInt( SESSION_TIMEOUT_COUNTDOWN_SECONDS, 10 )
    }

    constructor( props ) {
        super( props );
        this.state = {
            seconds: parseInt( SESSION_TIMEOUT_COUNTDOWN_SECONDS, 10 )
        };
    }

    componentWillMount() {
        const countdownTimeLeft = localStorage.getItem( 'countdownTimeLeft' );
        if ( countdownTimeLeft ) {
            this.setState({ seconds: parseInt( countdownTimeLeft, 10 ) });
        }

        this.setState({ seconds: parseInt( SESSION_TIMEOUT_COUNTDOWN_SECONDS, 10 ) }, () => {
            const interval = setInterval( () => {
                this.setState({ seconds: this.state.seconds - 1 }, () => {
                    localStorage.setItem( 'countdownTimeLeft', this.state.seconds );
                    if ( this.state.seconds <= 0 ) {
                        clearInterval( interval );
                        this.props.onFinish();
                    }
                });
            }, 1000 );
        });
    }

    render() {
        return (
            <span>{ this.state.seconds }</span>
        );
    }
}
