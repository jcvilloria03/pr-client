/* eslint-disable no-tabs */
import React from 'react';
import { shallow } from 'enzyme';
import CountdownTimerContainer from '../index';

describe( '<CountdownTimerContainer />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <CountdownTimerContainer />
        );
        expect( component.html() ).toBeTruthy();
    });
});
