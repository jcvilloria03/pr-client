import React from 'react';
import { Modal as ReactModal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import styled from 'styled-components';
import Button from '../Button/index';

const StyledModalHeader = styled( ModalHeader )`
    font-size: 14px;
`;

const StyledModalBody = styled( ModalBody )`
    padding: 20px;
    margin-bottom: 10px;
    font-size: 14px;
`;

const StyledModalFooter = styled( ModalFooter )`
    padding: 0px 15px 15px;
    text-align: center;
    border-top: none;
    
`;

/**
*
* Modal
*
*/
export default class Modal extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        center: React.PropTypes.bool,
        size: React.PropTypes.string,
        title: React.PropTypes.string,
        body: React.PropTypes.oneOfType([
            React.PropTypes.element,
            React.PropTypes.node,
            React.PropTypes.string,
            React.PropTypes.number
        ]),
        footer: React.PropTypes.oneOfType([
            React.PropTypes.element,
            React.PropTypes.node,
            React.PropTypes.string,
            React.PropTypes.number
        ]),
        buttons: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                type: React.PropTypes.string.isRequired, // eslint-disable-line react/no-unused-prop-types
                label: React.PropTypes.oneOfType([
                    React.PropTypes.string,
                    React.PropTypes.object,
                    React.PropTypes.element,
                    React.PropTypes.node
                ]).isRequired, // eslint-disable-line react/no-unused-prop-types
                size: React.PropTypes.string, // eslint-disable-line react/no-unused-prop-types
                onClick: React.PropTypes.func // eslint-disable-line react/no-unused-prop-types
            })
        ).isRequired,
        onClose: React.PropTypes.func,
        showClose: React.PropTypes.bool,
        keyboard: React.PropTypes.bool,
        noFooter: React.PropTypes.bool,
        className: React.PropTypes.string,
        maxWidth: React.PropTypes.string
    }

    static defaultProps = {
        backdrop: true,
        showClose: true,
        keyboard: true,
        center: false,
        noFooter: false
    }

    /**
     * Component Constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind( this );
        this.close = this.close.bind( this );
    }

    /**
     * Toggles Modal Visibility
     */
    toggle() {
        this.setState({
            modal: !this.state.modal
        }, () => {
            if ( !this.state.modal && this.props.onClose ) this.props.onClose();
        });
    }

    /**
     * Closes modal
     */
    close() {
        this.setState({
            modal: false
        }, () => {
            this.props.onClose && this.props.onClose();
        });
    }

    /**
     *
     * Modal
     *
     */
    render() {
        const modalHeaderConfig = {};

        if ( this.props.showClose ) {
            modalHeaderConfig.toggle = this.close;
        }

        return (
            <ReactModal
                className={ `sl-c-modal ${this.props.center ? 'modal-center' : ''} ${this.props.className}` }
                isOpen={ this.state.modal }
                toggle={ this.toggle }
                style={ { maxWidth: this.props.maxWidth || '450px' } }
                size={ this.props.size }
                backdrop="static"
                keyboard={ this.props.keyboard }
            >
                <StyledModalHeader { ...modalHeaderConfig }>{ this.props.title }</StyledModalHeader>

                <StyledModalBody>
                    { this.props.body }
                </StyledModalBody>

                {!this.props.noFooter && (
                    <StyledModalFooter>
                        { this.props.footer
                                        ? this.props.footer
                                        : this.props.buttons.map( ( button, index ) => <Button key={ index } { ...button } /> )
                                    }
                    </StyledModalFooter>
                    )
                }
            </ReactModal>
        );
    }
}
