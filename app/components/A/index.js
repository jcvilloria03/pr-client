/**
 * A link to a certain page, an anchor tag
 */

import styled from 'styled-components';

const A = styled.a`
    font-weight: 400;
    color: #00A5E5;
    text-decoration: none !important;

    &:visited, &:active {
        color: #00A5E5;
    }
`;

export default A;
