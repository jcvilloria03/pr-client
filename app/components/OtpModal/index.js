import React from 'react';
import { Modal as ReactModal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Button from '../Button/index';

/**
*
* Modal
*
*/
export default class OtpModal extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        size: React.PropTypes.string,
        title: React.PropTypes.string,
        body: React.PropTypes.oneOfType([
            React.PropTypes.element,
            React.PropTypes.node,
            React.PropTypes.string,
            React.PropTypes.number
        ]),
        buttons: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                type: React.PropTypes.string.isRequired, // eslint-disable-line react/no-unused-prop-types
                label: React.PropTypes.oneOfType([
                    React.PropTypes.string,
                    React.PropTypes.object,
                    React.PropTypes.element,
                    React.PropTypes.node
                ]).isRequired, // eslint-disable-line react/no-unused-prop-types
                size: React.PropTypes.string, // eslint-disable-line react/no-unused-prop-types
                onClick: React.PropTypes.func // eslint-disable-line react/no-unused-prop-types
            })
        ).isRequired,
        onClose: React.PropTypes.func,
        backdrop: React.PropTypes.oneOfType([
            React.PropTypes.bool,
            React.PropTypes.oneOf(['static'])
        ]),
        showClose: React.PropTypes.bool,
        keyboard: React.PropTypes.bool
    }

    static defaultProps = {
        backdrop: true,
        showClose: true,
        keyboard: true
    }

    /**
     * Component Constructor
     */
    constructor( props ) {
        super( props );
        this.state = {
            modal: false
        };
        this.toggle = this.toggle.bind( this );
        this.close = this.close.bind( this );
    }

    /**
     * Toggles Modal Visibility
     */
    toggle() {
        this.setState({
            modal: !this.state.modal
        }, () => {
            !this.state.modal && this.props.onClose && this.props.onClose();
        });
    }

    /**
     * Closes modal
     */
    close() {
        this.setState({
            modal: false
        }, () => {
            this.props.onClose && this.props.onClose();
        });
    }

    /**
     *
     * Modal
     *
     */
    render() {
        const modalHeaderConfig = {};
        if ( this.props.showClose ) {
            modalHeaderConfig.toggle = this.toggle;
        }

        return (
            <ReactModal
                className="sl-c-modal"
                isOpen={ this.state.modal }
                toggle={ this.toggle }
                style={ { width: '450px' } }
                size={ this.props.size }
                backdrop={ this.props.backdrop }
                keyboard={ this.props.keyboard }
            >
                <ModalHeader { ...modalHeaderConfig }>{ this.props.title }</ModalHeader>
                <ModalBody style={ { padding: '20px 25px 0 25px' } }>
                    { this.props.body }
                </ModalBody>
                <ModalFooter style={ { textAlign: 'center', padding: '0 15px 30px 15px', borderTop: 'none' } }>
                    {
                        this.props.buttons.map( ( button, index ) => <Button key={ index } { ...button } /> )
                    }
                </ModalFooter>
            </ReactModal>
        );
    }
}
