import React from 'react';
import { shallow } from 'enzyme';
import Modal from '../index';

const modalBody = (
    <div>Test</div>
);
const buttons = [
    {
        type: 'neutral',
        size: 'large',
        label: 'Keep Request'
    },
    {
        type: 'danger',
        size: 'large',
        label: 'Reject Request'
    }
];

describe( '<Modal />', () => {
    it( 'should have default states and props', () => {
        const component = shallow(
            <Modal
                title="Reject Request"
                body={ modalBody }
                size="xs"
                buttons={ buttons }
            />
        );
        const props = component.instance().props;

        expect( component.instance().state.modal ).toEqual( false );
        expect( props.title ).toEqual( 'Reject Request' );
        expect( props.size ).toEqual( 'xs' );
        expect( props.buttons.length ).toEqual( 2 );
    });

    it( 'can toggle the current modal state and it should return true', () => {
        const _handleToggle = jest.fn();
        const component = shallow(
            <Modal
                title="Reject Request"
                body={ modalBody }
                size="xs"
                buttons={ buttons }
                toggle={ _handleToggle }
            />
        );
        component.instance().toggle();

        expect( component.instance().state.modal ).toEqual( true );
    });

    it( 'should call onClose props on modal close', () => {
        const handleToggle = jest.fn();
        const onClose = jest.fn();

        const component = shallow(
            <Modal
                title="Reject Request"
                body={ modalBody }
                size="xs"
                buttons={ buttons }
                toggle={ handleToggle }
                onClose={ onClose }
            />
        );
        component.instance().toggle();

        expect( component.instance().state.modal ).toBe( true );

        component.instance().toggle();

        expect( component.instance().state.modal ).toBe( false );
        expect( onClose ).toHaveBeenCalled();
    });
});
