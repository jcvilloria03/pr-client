import React from 'react';
import { StyledLoader } from './styles';

/**
* Loader
*/
function Loader() {
    return (
        <StyledLoader className="animation">
            Please wait <div className="anim3"></div>
        </StyledLoader>
    );
}

export default Loader;
