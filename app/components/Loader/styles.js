import styled, { keyframes } from 'styled-components';

const anim3 = keyframes`
    to {
        transform: rotate(45deg) translate(3px, 3px);
    }
`;

export const StyledLoader = styled.div`
    font-size: 12px;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;

    .anim3 {
        padding-left: 10px;
        &:before {
            display: block;
            width: 12px;
            height: 12px;
            margin-top: -1px;
            border: 2px solid currentColor;
            content: "";
            animation: ${anim3} .5s ease infinite alternate;
            border-radius: 100% 100% 0 100%;
            transform: rotate( 45deg );
        }

        &:after {
            display: block;
            width: 12px;
            height: 3px;
            margin-top: 8px;
            background-color: currentColor;
            content: "";
            border-radius: 100%;
        }
    }
`;
