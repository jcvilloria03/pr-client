import React from 'react';
import { shallow } from 'enzyme';
import Loader from '../index';

describe( '<Loader />', () => {
    it( 'should have default prop value "className"', () => {
        const component = shallow(
            <Loader />
        );
        expect( component.find( '.animation' ) ).toBeTruthy();
    });
});
