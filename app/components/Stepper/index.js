/* eslint-disable no-confusing-arrow */
import React from 'react';
import styled from 'styled-components';
import Step from './Step';

const StyledRoot = styled.div`
    width: 100%;
    min-height: 0;
    margin: 50px 0;
    padding: 0;
`;

const StyledStepper = styled.div`
    display: table;
    width: 100%;
    margin: 0 auto;

    .sl-c-step {
        position: relative;

        &:before {
            position: absolute;
            top: ${( props ) => ( props.size / 2 ) + 10}px;
            left: 0;
            right: 0;
            height: ${( props ) => props.newStyle ? 3 : 1}px;
            background-color: ${( props ) => props.newStyle && props.activeStep !== 0 ? props.activeColor : '#F0F4F6'};
            content: '';
        }

        &:first-child {
            &:before {
                left: ${( props ) => props.newStyle ? 55 : 50}%;
            }
        }

        &:last-child {
            &:before {
                right: ${( props ) => props.newStyle ? 55 : 50}%;;
            }
        }
    }

    .sl-c-badge {
        position: relative;
        z-index: 2;
        background-color: black;
        border: 1px solid #FFF !important;

        .isvg {
            svg {
                width: 1rem;
                height: 1rem;
            }
        }
    }
`;

/**
 * Stepper
 */
export default class Stepper extends React.Component {
    static defaultProps = {
        activeStep: 0,
        size: 26,
        useIndex: false,
        useNumbers: false
    }

    static propTypes = {
        activeStep: React.PropTypes.number,
        steps: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                label: React.PropTypes.string.isRequired,
                onClick: React.PropTypes.func
            })
        ),
        activeColor: React.PropTypes.string,
        completeColor: React.PropTypes.string,
        defaultColor: React.PropTypes.string,
        activeTitleColor: React.PropTypes.string,
        completeTitleColor: React.PropTypes.string,
        defaultTitleColor: React.PropTypes.string,
        circleFontColor: React.PropTypes.string,
        size: React.PropTypes.number,
        circleFontSize: React.PropTypes.number,
        titleFontSize: React.PropTypes.number,
        circleTop: React.PropTypes.number,
        titleTop: React.PropTypes.number,
        useIndex: React.PropTypes.bool,
        useNumbers: React.PropTypes.bool,
        newStyle: React.PropTypes.bool,
        icon: React.PropTypes.string,
        rootStyle: React.PropTypes.object
    }

    /**
     * Component constructor
     */
    constructor() {
        super();
        this.renderStep = this.renderStep.bind( this );
    }

    /**
     * Renders each step using the Step component
     */
    renderStep( index ) {
        const {
            activeStep,
            steps,
            activeColor,
            completeColor,
            defaultColor,
            circleFontColor,
            activeTitleColor,
            completeTitleColor,
            defaultTitleColor,
            size,
            circleFontSize,
            titleFontSize,
            circleTop,
            titleTop,
            useIndex,
            useNumbers,
            newStyle,
            icon,
        } = this.props;

        const step = steps[ index ];
        const active = index === activeStep;
        const completed = useIndex
            ? !active
            : index < activeStep;
        const first = index === 0;
        const last = index === steps.length - 1;

        return (
            <Step
                key={ index }
                width={ 100 / steps.length }
                title={ step.label }
                active={ active }
                onClick={ step.onClick }
                completed={ completed }
                first={ first }
                last={ last }
                index={ index }
                activeColor={ activeColor }
                completeColor={ completeColor }
                defaultColor={ defaultColor }
                circleFontColor={ circleFontColor }
                activeTitleColor={ activeTitleColor }
                completeTitleColor={ completeTitleColor }
                defaultTitleColor={ defaultTitleColor }
                size={ size }
                circleFontSize={ circleFontSize }
                titleFontSize={ titleFontSize }
                circleTop={ circleTop }
                titleTop={ titleTop }
                useIndex={ useIndex }
                useNumbers={ useNumbers }
                newStyle={ newStyle }
                icon={ icon }
            />
        );
    }

    /**
     * component render method
     * @returns {React Component JSX}
     */
    render() {
        const {
            steps,
            size,
            newStyle,
            activeColor,
            activeStep,
            rootStyle
        } = this.props;

        return (
            <StyledRoot className="stepper" style={ rootStyle }>
                <StyledStepper
                    size={ size }
                    newStyle={ newStyle }
                    activeColor={ activeColor }
                    activeStep={ activeStep }
                >
                    { steps.map( ( s, i ) => this.renderStep( i ) ) }
                </StyledStepper>
            </StyledRoot>
        );
    }
}
