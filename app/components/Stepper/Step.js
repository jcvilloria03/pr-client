import React from 'react';
import Icon from '../Icon';

/**
 * This renders every step from the stepper component
 */
export default class Step extends React.Component {
    static propTypes = {
        width: React.PropTypes.number,
        activeColor: React.PropTypes.string,
        completeColor: React.PropTypes.string,
        defaultColor: React.PropTypes.string,
        activeTitleColor: React.PropTypes.string,
        completeTitleColor: React.PropTypes.string,
        defaultTitleColor: React.PropTypes.string,
        size: React.PropTypes.number,
        circleFontSize: React.PropTypes.number,
        titleFontSize: React.PropTypes.number,
        circleTop: React.PropTypes.number,
        title: React.PropTypes.string,
        active: React.PropTypes.bool,
        completed: React.PropTypes.bool,
        onClick: React.PropTypes.func,
        useIndex: React.PropTypes.bool,
        useNumbers: React.PropTypes.bool,
        index: React.PropTypes.number,
        icon: React.PropTypes.string
    }

    static defaultProps = {
        activeColor: '#FFF',
        completeColor: '#00A5E5',
        defaultColor: '#CCCCCC',
        activeTitleColor: '#00A5E5',
        completeTitleColor: '#00A5E5',
        defaultTitleColor: '#c0c0c0',
        size: 26,
        circleFontSize: 14,
        titleFontSize: 14,
        circleTop: 10,
        titleTop: 2,
        icon: 'tick'
    }
    /**
     * component constructor
     */
    constructor() {
        super();
        this.getStyles = this.getStyles.bind( this );
    }

    /**
     * this calculates and sets up the styles for the component
     * @returns {{step: {width: string, display: string, position: string, paddingTop: Step.props.circleTop}, circle: {width: Step.props.size, height: Step.props.size, margin: string, backgroundColor: Step.props.defaultColor, borderRadius: string, textAlign: string, padding: number, fontSize: Step.props.circleFontSize, display: string}, activeCircle: {backgroundColor: Step.props.activeColor}, completedCircle: {backgroundColor: Step.props.completeColor}, title: {marginTop: Step.props.titleTop, fontSize: Step.props.titleFontSize, fontWeight: string, textAlign: string, color: Step.props.defaultTitleColor}, activeTitle: {color: Step.props.activeTitleColor}, completedTitle: {color: Step.props.completeTitleColor}, leftBar: {position: string, top: *, height: number, borderTopStyle: string, borderTopWidth: number, borderTopColor: Step.props.defaultColor, left: number, right: string, marginRight: number}, rightBar: {position: string, top: *, height: number, borderTopStyle: string, borderTopWidth: number, borderTopColor: Step.props.defaultColor, right: number, left: string, marginLeft: number}, completedBar: {borderTopStyle: string, borderTopWidth: number, borderTopColor: Step.props.completeColor}}}
     */
    getStyles() {
        const {
            activeColor, completeColor, defaultColor,
            activeTitleColor, completeTitleColor, defaultTitleColor,
            size, circleFontSize, titleFontSize,
            circleTop, width, useIndex, useNumbers
        } = this.props;

        return {
            step: {
                width: `${width}%`,
                display: 'table-cell',
                position: 'relative',
                textAlign: 'center',
                padding: `${circleTop}px 5px 0 5px`
            },
            circle: {
                width: size,
                height: size,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                margin: '0 auto',
                backgroundColor: '#ffffff',
                borderRadius: '50%',
                textAlign: 'center',
                padding: '1px',
                fontSize: circleFontSize,
                color: '#cccccc',
                border: `1px solid ${defaultColor}`
            },
            activeCircle: {
                backgroundColor: activeColor,
                color: useIndex || useNumbers ? '#ffffff' : completeColor,
                border: `1px solid ${useIndex ? activeColor : completeColor}`
            },
            completedCircle: {
                backgroundColor: completeColor,
                border: `1px solid ${completeColor}`,
                cursor: 'pointer',
                color: '#ffffff'
            },
            title: {
                fontSize: titleFontSize,
                fontWeight: '300',
                display: 'inline-block',
                color: defaultTitleColor
            },
            activeTitle: {
                color: activeTitleColor,
                fontWeight: '500'
            },
            completedTitle: {
                color: completeTitleColor,
                fontWeight: '500'
            },
            completedBar: {
                borderTopStyle: 'solid',
                borderTopWidth: 10,
                borderTopColor: completeColor
            }
        };
    }

    /**
     * component render method
     * @returns {React component JSX}
     */
    render() {
        const { title, active, completed, onClick, useNumbers, index, icon } = this.props;
        const styles = this.getStyles();
        const circleStyle = Object.assign(
            styles.circle,
            completed ? styles.completedCircle : {},
            active ? styles.activeCircle : {},
        );
        const titleStyle = Object.assign(
            styles.title,
            completed ? styles.completedTitle : {},
            active ? styles.activeTitle : {},
        );
        /* eslint-disable jsx-a11y/no-static-element-interactions */
        return (
            <div className="sl-c-step" style={ styles.step }>
                <div className="sl-c-badge" style={ circleStyle } onClick={ onClick }>
                    <span style={ styles.index }>
                        { useNumbers ? (
                            index + 1
                        ) : (
                            <Icon name={ completed ? 'tick' : icon } />
                        ) }
                    </span>
                </div>
                <div style={ titleStyle }>{ title }</div>
            </div>
        );
    }
}
