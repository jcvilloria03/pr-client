import React from 'react';
import { shallow } from 'enzyme';

import Stepper from '../index';

describe( '<Stepper />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <Stepper steps={ [{ label: 'Step One' }, { label: 'Step Two' }, { label: 'Step Three' }, { label: 'Step Four' }] } activeStep={ 1 } />
        );
        expect( component.html() ).toBeTruthy();
    });
});
