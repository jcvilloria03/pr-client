import React from 'react';

/**
* RadioGroup
*/
class RadioGroup extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        horizontal: React.PropTypes.bool,
        children: React.PropTypes.node,
        value: React.PropTypes.string,
        onChange: React.PropTypes.func
    }

    /**
     * component constructor
     * @param children
     * @param value
     */
    constructor( props ) {
        super( props );
        const index = this.props.children.findIndex( ( c ) => c.props.value === this.props.value );
        this.state = {
            checkedIndex: index > -1 ? index : -1,
            value: this.props.value
        };
        this.renderChild = this.renderChild.bind( this );
        this.onChange = this.onChange.bind( this );
    }

    /**
     * this sets the value when new props has been passed
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        const index = this.props.children.findIndex( ( c ) => c.props.value === nextProps.value );
        this.setState({ value: nextProps.value, checkedIndex: index > -1 ? index : -1 });
    }

    /**
     * handles change event
     * @param index = index of radio
     */
    onChange( index ) {
        const { onChange, children } = this.props;
        const child = children[ index ];
        child && index !== this.state.checkedIndex
            && this.setState({ checkedIndex: index, value: child.props.value }, () => {
                onChange && onChange( child.props.value || '' );
            });
    }

    /**
     * renders radio elements
     * @param child
     * @param index
     * @param checked
     * @returns {React Component}
     */
    renderChild( child, index, checked ) {
        const { children, horizontal } = this.props;
        return React.cloneElement( child, {
            key: index,
            horizontal,
            index,
            checked,
            last: index === children.length - 1,
            onChange: this.onChange,
            ...child.props
        });
    }

    /**
     * RadioGroup render method.
     */
    render() {
        const { checkedIndex } = this.state;
        const { horizontal, children, ...props } = this.props;
        const style = horizontal ? { display: 'inline-flex', width: '100%' } : {};
        return (
            <div style={ style } { ...props }>
                {
                    children.map( ( c, i ) => ( this.renderChild( c, i, i === checkedIndex ) ) )
                }
            </div>
        );
    }
}

export default RadioGroup;
