import React from 'react';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import RadioGroup from '../index';
import Radio from '../../Radio';

describe( '<RadioGroup />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <RadioGroup>
                <Radio value="apple">
                    Apple
                </Radio>
                <Radio value="banana">
                    Banana
                </Radio>
            </RadioGroup>
        );
        expect( component.html() ).toBeTruthy();
    });

    it( 'should be mounted as horizontal', () => {
        const component = mount(
            <RadioGroup horizontal>
                <Radio value="apple">
                    Apple
                </Radio>
                <Radio value="banana">
                    Banana
                </Radio>
            </RadioGroup>
        );

        sinon.spy( RadioGroup.prototype, 'componentWillReceiveProps' );
        component.setProps({ horizontal: false });
        expect( RadioGroup.prototype.componentWillReceiveProps.calledOnce ).toEqual( true );
        RadioGroup.prototype.componentWillReceiveProps.restore();
    });

    it( 'should be handle property changes', () => {
        sinon.spy( RadioGroup.prototype, 'componentWillReceiveProps' );

        const component = mount(
            <RadioGroup horizontal>
                <Radio value="apple" disabled>
                    Apple
                </Radio>
                <Radio value="banana">
                    Banana
                </Radio>
            </RadioGroup>
        );

        component.setProps({ horizontal: false });
        expect( RadioGroup.prototype.componentWillReceiveProps.calledOnce ).toEqual( true );
        component.setProps({ value: 'apple' });
        RadioGroup.prototype.componentWillReceiveProps.restore();
    });

    it( 'should have onchange callback', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <RadioGroup value="apple" horizontal onChange={ _handleChange }>
                <Radio value="apple">
                    Apple
                </Radio>
                <Radio id="Banana">
                    Banana
                </Radio>
            </RadioGroup>
        );

        component.find( '#Banana' ).simulate( 'click' );
        expect( _handleChange ).toHaveBeenCalled();
    });
});
