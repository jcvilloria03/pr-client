import React from 'react';
import { shallow } from 'enzyme';

import Switch2 from '../index';

describe( '<Switch2 />', () => {
    it( 'should render with default prop value', () => {
        const component = shallow(
            <Switch2 />
        );

        const { props } = component.instance();
        expect( props.defaultChecked ).toBe( false );
        expect( props.disabled ).toBe( false );
    });

    it( 'should render with given props', () => {
        const component = shallow(
            <Switch2 checked defaultChecked={ false } disabled={ false } />
        );

        const { props } = component.instance();
        expect( props.defaultChecked ).toBe( false );
        expect( props.checked ).toBe( true );
        expect( props.disabled ).toBe( false );
    });
});
