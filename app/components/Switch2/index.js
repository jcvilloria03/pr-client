import React from 'react';
import Toggle from 'rc-switch';
import {
    Wrapper
} from './styles';

/**
* Switch2
*/
class Switch2 extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        checked: React.PropTypes.bool,
        defaultChecked: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        onChange: React.PropTypes.func
    }

    static defaultProps = {
        defaultChecked: false,
        disabled: false
    }

    /**
    * Switch render method.
    */
    render() {
        const { defaultChecked, disabled, onChange, checked } = this.props;
        return (
            <Wrapper>
                <Toggle
                    defaultChecked={ defaultChecked }
                    disabled={ disabled }
                    onChange={ onChange }
                    checked={ checked === undefined ? defaultChecked : checked }
                />
            </Wrapper>
        );
    }
}

export default Switch2;
