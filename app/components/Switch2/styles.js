import styled, { keyframes } from 'styled-components';

export const Wrapper = styled.span`
    .rc-switch {
        position: relative;
        display: inline-block;
        box-sizing: border-box;
        width: 28px;
        height: 9px;
        line-height: 20px;
        vertical-align: middle;
        border-radius: 100px 100px;
        border: 1px solid #ccc;
        background-color: #ccc;
        cursor: pointer;
        transition: all .3s cubic-bezier(0.35, 0, 0.25, 1);

        &-inner {
            color:#fff;
            font-size: 12px;
            position: absolute;
            left:24px;
        }

        &:after{
            position: absolute;
            width: 18px;
            height: 18px;
            left: -2px;
            top: -5px;
            border-radius: 50% 50%;
            background-color: #474747;
            content: " ";
            cursor: pointer;
            transform: scale(1);
            transition: left .3s cubic-bezier(0.35, 0, 0.25, 1);
            animation-timing-function: cubic-bezier(0.35, 0, 0.25, 1);
            animation-duration: .3s;
            animation-name: ${rcSwitchOff};
            transition: all .3s cubic-bezier(0.35, 0, 0.25, 1);
        }

        &:focus {
            box-shadow: 0 0 0 2px tint(#2db7f5, 80%);
            outline: none;
        }

        &-checked{
            .rc-switch-inner {
                left:6px;
            }

            &:after{
                left: 10px;
                background-color: #83d24b;
            }
        }

        &-disabled{
            cursor: no-drop;
            background: #ccc;
            border-color:#ccc;

            &:after{
                background: #9e9e9e;
                animation-name: none;
                cursor: no-drop;
            }

            &:hover:after{
                transform: scale(1);
                animation-name: none;
            }
        }

        &-label {
            display: inline-block;
            line-height: 20px;
            font-size: 14px;
            padding-left: 10px;
            vertical-align: middle;
            white-space: normal;
            pointer-events: none;
            user-select: text;
        }
    }
`;

const rcSwitchOn = keyframes`
    0% {
        transform: scale(1);
    }

    50% {
        transform: scale(1.25);
    }

    100% {
        transform: scale(1.1);
    }
`;

const rcSwitchOff = keyframes`
    0% {
        transform: scale(1.1);
    }
    100% {
        transform: scale(1);
    }
`;
