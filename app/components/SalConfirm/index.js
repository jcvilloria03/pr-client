import React from 'react';
import Confirm from 'react-confirm-bootstrap';

/**
* Confirm
*/
class SalConfirm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        onConfirm: React.PropTypes.func.isRequired,
        onClose: React.PropTypes.func,
        body: React.PropTypes.oneOfType([
            React.PropTypes.element,
            React.PropTypes.symbol,
            React.PropTypes.node
        ]).isRequired,
        confirmText: React.PropTypes.string,
        cancelText: React.PropTypes.string,
        showCancel: React.PropTypes.bool,
        title: React.PropTypes.string.isRequired,
        visible: React.PropTypes.bool,
        buttonStyle: React.PropTypes.oneOf([
            'primary', 'success', 'info', 'warning', 'danger', 'link'
        ]),
        backdrop: React.PropTypes.oneOfType([
            React.PropTypes.bool,
            React.PropTypes.oneOf(['static'])
        ]),
        keyboard: React.PropTypes.bool
    }

    static defaultProps = {
        confirmText: 'Confirm',
        showCancel: true,
        visible: false,
        cancelText: 'Cancel',
        buttonStyle: 'danger',
        backdrop: true,
        keyboard: true
    }

    /**
     * component constructor
     * @param props = initial props
     */
    constructor( props ) {
        super( props );
        this.state = {
            visible: this.props.visible
        };
    }

    /**
     * checks if modal should be shown
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.visible !== this.props.visible && this.modal.setState({ isOpened: nextProps.visible });
    }

    /**
    * Confirm render method.
    */
    render() {
        return (
            <Confirm
                ref={ ( modal ) => { this.modal = modal; } }
                onConfirm={ this.props.onConfirm }
                visible={ this.props.visible }
                title={ this.props.title }
                body={ this.props.body }
                confirmText={ this.props.confirmText }
                showCancelButton={ this.props.showCancel }
                cancelText={ this.props.cancelText }
                confirmBSStyle={ this.props.buttonStyle }
                onClose={ this.props.onClose }
                backdrop={ this.props.backdrop }
                keyboard={ this.props.keyboard }
            />
        );
    }
}

export default SalConfirm;
