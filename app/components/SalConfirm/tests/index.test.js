import React from 'react';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';

import SalConfirm from '../index';

describe( '<SalConfirm />', () => {
    it( 'should mount the component', () => {
        const spy = jest.fn();
        const component = shallow(
            <SalConfirm
                onConfirm={ spy }
                body="Sample Body"
                title="Imma title"
            />
        );
        expect( component.find( 'SalConfirm' ) ).toBeTruthy();
    });

    it( 'can have a component instance as body', () => {
        const spy = jest.fn();
        const component = shallow(
            <SalConfirm
                onConfirm={ spy }
                body={ <div><i className="fa fa-check" /></div> }
                title="Imma title"
            />
        );
        expect( component.find( 'Icon' ) ).toBeTruthy();
    });

    it( 'can have different confirm button styles', () => {
        const spy = jest.fn();
        const component = shallow(
            <SalConfirm
                onConfirm={ spy }
                body="asdasd"
                title="Imma title"
                buttonStyle="primary"
            />
        );
        expect( component.find( '.btn-primary' ) ).toBeTruthy();

        component.setProps({ buttonStyle: 'success' });
        expect( component.find( '.btn-success' ) ).toBeTruthy();

        component.setProps({ buttonStyle: 'info' });
        expect( component.find( '.btn-info' ) ).toBeTruthy();

        component.setProps({ buttonStyle: 'warning' });
        expect( component.find( '.btn-warning' ) ).toBeTruthy();

        component.setProps({ buttonStyle: 'danger' });
        expect( component.find( '.btn-danger' ) ).toBeTruthy();

        component.setProps({ buttonStyle: 'link' });
        expect( component.find( '.btn-link' ) ).toBeTruthy();
    });

    it( 'can handle props changes', () => {
        sinon.spy( SalConfirm.prototype, 'componentWillReceiveProps' );
        const spy = jest.fn();
        const component = mount(
            <SalConfirm
                onConfirm={ spy }
                body="asdasd"
                title="Imma title"
                buttonStyle="primary"
            />
        );
        component.setProps({ visible: true });
        expect( SalConfirm.prototype.componentWillReceiveProps.calledOnce ).toEqual( true );
    });
});
