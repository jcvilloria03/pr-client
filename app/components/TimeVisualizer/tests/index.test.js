import React from 'react';
import { shallow } from 'enzyme';

import TimeVisualizer from '../index';

describe( '<TimeVisualizer />', () => {
    it( 'Expect to have unit tests specified', () => {
        const component = shallow( <TimeVisualizer /> );
        expect( component.find( '.sl-c-col--wrapper' ) ).toBeTruthy();
    });
});
