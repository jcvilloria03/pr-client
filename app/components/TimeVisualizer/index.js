/* eslint-disable radix */
import React from 'react';
import moment from 'moment';

import { Wrapper } from './styles';

const FULL_CIRCLE = 360;
const NUMBER_OF_HOURS_ON_CLOCK = 12;
const ROTATION_FIX = 90;

/**
* TimeVisualizer
*/
class TimeVisualizer extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        start: React.PropTypes.string,
        end: React.PropTypes.string
    };

    /**
      * Calculate rotation for start.
      * @return {Number}
      */
    calculateRotation() {
        return FULL_CIRCLE * ( this.startHours() / NUMBER_OF_HOURS_ON_CLOCK ) - ROTATION_FIX;
    }

    /**
     * Get start time hours in 12h format.
     * @return {Number}
     */
    startHours() {
        return parseInt( moment( this.props.start, 'HH:mm' ).format( 'h' ) );
    }

    /**
       * Get duration time as decimal number.
       * @return {Number}
       */
    fullDuration() {
        const start = moment( this.props.start, 'HH:mm' );
        let end = moment( this.props.end, 'HH:mm' );

        if ( start.isSameOrAfter( end ) ) {
            end = end.add( 1, 'days' );
        }

        return end.diff( start, 'hours', true );
    }

    /**
      * Get duration, revinding after 12h
      * @return {Number}
      */
    duration() {
        const fullDuration = this.fullDuration();

        return fullDuration / 12 > 1
            ? fullDuration - 12
            : fullDuration;
    }

    /**
       * Convert 24h format to 12h time
       * @param {String} timeString
       * @return {String}
       */
    convertTo12h( timeString ) {
        const time = moment( timeString, 'HH:mm' );

        if ( !time.isValid() ) {
            return '';
        }
        return time.format( 'hh:mmA' );
    }

    /**
       * Check if values are present and valid.
       * @return {Boolean}
       */
    isFilled() {
        return moment( this.props.start, 'HH:mm' ).isValid() &&
            moment( this.props.end, 'HH:mm' ).isValid();
    }
    /**
       * Calculated styles for svg element.
       * @return {Object}
       */
    svgStyles() {
        let rotation = 0;

        if ( this.isFilled() ) {
            rotation = this.calculateRotation();
        }

        return {
            transform: `rotate(${rotation}deg)`
        };
    }

    /**
       * Start time in 12h format.
       * @return {String}
       */
    startTime12h() {
        return this.convertTo12h( this.props.start );
    }

    /**
     * End time in 12h format.
     * @return {String}
     */
    endTime12h() {
        return this.convertTo12h( this.props.end );
    }
    /**
      * Calculated styles for circle.
      * @return {Object}
      */
    circleStyles() {
        let duration = 0;

        if ( this.isFilled() ) {
            duration = ( ( this.duration() / 12 ) * 100 ).toFixed( 4 );
        }
        return {
            strokeDasharray: `${duration}, 100`
        };
    }

    render() {
        return (
            <Wrapper>
                <div className="sl-c-col--wrapper">
                    <div className="sl-c-col--6">
                        <div className="sl-c-shift-duration">
                            <span>
                                Day
                            </span>

                            <div className="sl-c-circle-loader">
                                <svg viewBox="0 0 32 32" style={ this.svgStyles() }>
                                    <circle r="16" cx="16" cy="16" style={ this.circleStyles() } />
                                </svg>
                            </div>
                            <span>
                            Night
                        </span>
                        </div>
                    </div>

                    {this.startTime12h() || this.endTime12h() ?
                        <div className="sl-c-col--6 sl-u-center">
                            <span className="sl-u-text--info">This schedule will be from {this.startTime12h()} to {this.endTime12h()} </span>
                        </div>
                        : <div></div>
                    }
                </div>
            </Wrapper>
        );
    }
}

export default TimeVisualizer;
