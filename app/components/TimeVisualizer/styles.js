import styled from 'styled-components';

export const Wrapper = styled.div`
.sl-c-col--wrapper{
    width: 100%;
    display:flex;
    align-items: center;
    .sl-c-col--6{
        width: calc(50%);
        padding: 0 0.5rem 1rem 0.5rem;
        .sl-c-shift-duration {
            position: relative;
            display: flex;
            align-items: center;
            justify-content: center;
            padding: 0.5rem;
            text-align: center;
            border-radius: 0.25rem;
            font-size: .75rem;
        
            &:before,
            &:after {
                position: absolute;
                top: 0;
                bottom: 0;
                display: block;
                content: '';
            }
        
            &:before {
                right: 50%;
                left: 0;
                background-color: #F0F4F6;
                border-radius: 0.25rem 0 0 0.25rem;
            }
        
            &:after {
                left: 50%;
                right: 0;
                background-color: #A8CBD7;
                border-radius: 0 0.25rem 0.25rem 0;
            }
        
            > * {
                position: relative;
                z-index: 1;
            }
        
            > span {
                min-width: 2.5rem;
        
                &:last-child {
                    color:#ffffff;
                }
            }
            .sl-c-circle-loader {
                display: inline-flex;
                align-items: center;
                justify-content: center;
                width: 55px;
                height: 55px;
                background-color:#fff;
                border-radius:50%;
                border: 3px solid #ffffff;

                > svg {
                display: inline-block;
                width: 100%;
                height: 100%;
                border-radius: 50%;
                animation: colorload 2s ease-in-out;
                background: #fff;

                > circle {
                    fill: #ffffff;
                    stroke: #00A5E5;
                    stroke-width: 32;
                }
                }
            }
        }
        .sl-u-text--info{
            color: #00A5E5;
            font-size:14px;
        }
    }
}
`;
