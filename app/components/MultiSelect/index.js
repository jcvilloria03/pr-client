import React from 'react';
import Select from 'react-select';
import styled from 'styled-components';

import { debounce } from 'lodash';

import 'react-select/dist/react-select.css';
import { styleProps } from './styles';
import Icon from '../Icon';

const Wrapper = styled.div`
    ${styleProps}
`;

const SearchIcon = styled( Icon )`
    position: absolute;
    top: 50%;
    right: 15px;
    transform: translateY(-50%);
    cursor: pointer;
    width: 14px;
    height: 30px;
    z-index: 1;
`;

// ! https://github.com/JedWatson/react-select/issues/614

/**
* MultiSelect
*/
class MultiSelect extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        disabled: React.PropTypes.bool,
        id: React.PropTypes.string.isRequired,
        required: React.PropTypes.bool,
        onChange: React.PropTypes.func,
        loadOptions: React.PropTypes.func,
        defaultValue: React.PropTypes.arrayOf( React.PropTypes.shape({
            value: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
                React.PropTypes.string,
                React.PropTypes.number
            ]),
            label: React.PropTypes.string,
            disabled: React.PropTypes.bool
        }) ),
        async: React.PropTypes.bool,
        data: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                value: React.PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
                    React.PropTypes.string,
                    React.PropTypes.number
                ]),
                label: React.PropTypes.string,
                disabled: React.PropTypes.bool
            })
        ),
        label: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.element,
            React.PropTypes.symbol,
            React.PropTypes.node
        ]),
        placeholder: React.PropTypes.string,
        value: React.PropTypes.any,
        autoSize: React.PropTypes.bool,
        multi: React.PropTypes.bool,
        hasSearchIcon: React.PropTypes.bool,
        notSearchable: React.PropTypes.bool
    };

    static defaultProps = {
        disabled: false,
        value: null,
        multi: true,
        defaultValue: null
    };

    /**
     * constructor method of the component
     */
    constructor( props ) {
        super( props );
        this.state = {
            errorMessage: 'This field is required',
            error: false,
            value: this.props.value,
            disabled: this.props.disabled
        };

        this._handleChange = this._handleChange.bind( this );
        this._checkRequire = this._checkRequire.bind( this );
    }

    /**
     * handles changes when new props are passed
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.value !== this.props.value && this.setState({ value: nextProps.value });
        nextProps.disabled !== this.props.disabled && this.setState({ disabled: nextProps.disabled });
    }

    _handleChange = ( value ) => {
        this._checkRequire( value );
        if ( this.props.onChange ) {
            this.setState({ value }, () => this.props.onChange( value ) );
        } else {
            this.setState({ value });
        }

        return true;
    }

    _checkRequire = ( value ) => {
        let passed;
        if ( this.props.required ) {
            if ( !value || value.length === 0 ) {
                this.setState({ error: true });
                passed = false;
            } else {
                this.setState({ error: false });
                passed = true;
            }
        } else {
            passed = true;
        }
        return passed;
    }

    /**
    * MultiSelect render method.
    */
    render() {
        const { hasSearchIcon } = this.props;

        const label = this.props.required ? (
            <span>
                <span className="required">*</span>{' '}
                { this.props.label }
            </span>
        ) : this.props.label;
        const labelElement = this.props.label ? <label htmlFor={ this.props.id }>{ label }</label> : '';
        const dropdownImage = <i className="fa fa-caret-down" />;
        const { async = false, loadOptions = () => null } = this.props;
        return (
            <Wrapper className={ hasSearchIcon && 'has-icon' }>
                {hasSearchIcon && <SearchIcon name="search" className="icon-search" />}
                { !async ?
                    <Select
                        className={ this.state.error ? 'is-error' : '' }
                        disabled={ this.state.disabled }
                        placeholder={ this.props.placeholder }
                        clearable={ false }
                        name={ this.props.id }
                        value={ this.state.value }
                        options={ this.props.data }
                        onChange={ this._handleChange }
                        arrowRenderer={ () => dropdownImage }
                        autosize={ this.props.autoSize }
                        resetValue={ { value: null, label: '' } }
                        multi={ this.props.multi }
                        searchable={ !this.props.notSearchable }
                    />
                    : <Select.Async
                        multi={ this.props.multi }
                        className={ this.state.error ? 'is-error' : '' }
                        disabled={ this.state.disabled }
                        placeholder={ this.props.placeholder }
                        clearable={ false }
                        name="form-field-name"
                        value={ this.state.value }
                        onChange={ this._handleChange }
                        loadOptions={ debounce( loadOptions, 600 ) }
                        arrowRenderer={ () => dropdownImage }
                        autosize={ this.props.autoSize }
                        resetValue={ { value: null, label: '' } }
                        defaultValue={ this.props.defaultValue || [] }
                        searchable={ !this.props.notSearchable }
                    />
                }
                { labelElement }
                <p style={ { display: this.state.error ? 'block' : 'none' } }>
                    { this.state.errorMessage }
                </p>
            </Wrapper>
        );
    }
}

export default MultiSelect;
