import { shallow, mount } from 'enzyme';
import React from 'react';
import MultiSelect from '../index';

const data = [
    { value: undefined, label: 'no value' },
    { value: 1, label: 'test2' },
    { value: 2, label: 'test3' },
    { value: 3, label: 'test4' },
    { value: 4, label: 'test5' },
    { value: 5, label: 'disabled option', disabled: true }
];

describe( '<MultiSelect />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <MultiSelect
                id="test"
                data={ data }
            />
        );
        expect( component.find( 'MultiSelect' ) ).toBeTruthy();
    });

    it( 'should handle change events', () => {
        const callback = jest.fn();
        const component = mount(
            <MultiSelect
                id="test"
                data={ data }
                label="Test label"
                value={ [{ value: 2, label: 'test3' }] }
                onChange={ callback }
                required
            />
        );

        component.instance()._handleChange( data[ 3 ]);
        expect( callback ).toHaveBeenCalled();
    });

    it( 'should handle change events when value is null', () => {
        const component = mount(
            <MultiSelect
                id="test"
                data={ data }
                label="Test label"
                value={ [{ value: 2, label: 'test3' }] }
                required
            />
        );

        component.instance()._handleChange( null );
        expect( component.instance().state.value ).toEqual( null );
    });

    it( 'should throw error if required and passed with invalid value', () => {
        const component = shallow(
            <MultiSelect
                id="test"
                data={ data }
                label="Test label"
                value={ [{ value: 2, label: 'test3' }] }
                required
            />
        );

        expect( component.state().error ).toEqual( false );
        component.find( 'Select' ).simulate( 'change', [{ value: 'qweqweqe' }]);
        expect( component.state().error ).toEqual( false );
        component.find( 'Select' ).simulate( 'change', null );
        expect( component.state().error ).toEqual( true );
        component.find( 'Select' ).simulate( 'change', undefined );
        expect( component.state().error ).toEqual( true );
    });

    it( 'should contain passed options', () => {
        const component = shallow(
            <MultiSelect
                id="test"
                data={ data }
                label="Test label"
            />
        );

        expect( component.find( 'Select' ).props().options ).toEqual( data );
    });

    it( 'should handle disable changes', () => {
        const component = shallow(
            <MultiSelect
                id="test"
                data={ data }
                label="Test label"
            />
        );

        expect( component.state().disabled ).toEqual( false );

        component.setProps({ disabled: true });
        expect( component.state().disabled ).toEqual( true );
    });

    it( 'should have not errors when given with invalid data if required props is set to false', () => {
        const component = shallow(
            <MultiSelect
                id="test"
                data={ data }
                label="Test label"
            />
        );

        component.find( 'Select' ).simulate( 'change', { value: undefined });
        expect( component.state().error ).toEqual( false );
    });

    it( 'should handle prop changes', () => {
        const component = shallow(
            <MultiSelect
                id="test"
                data={ data }
                label="Test label"
            />
        );
        component.setProps({ label: 'New Label' });
        expect( component.instance().props.label ).toEqual( 'New Label' );

        component.setProps({ value: { label: 'haha', value: 'hehe' }});
        expect( component.instance().props.value ).toEqual({ label: 'haha', value: 'hehe' });
    });
});
