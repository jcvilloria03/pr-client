import { css } from 'styled-components';

export const styleProps = () => css`
    display: flex;
    flex-wrap: wrap;
    position: relative;

    &.has-icon {
        .Select-arrow-zone {
            display: none;
        }

        .Select-loading-zone {
            top: -2px;
            right: 35px;
        }
    }

    & > * {
        width: 100%;
    }

    .Select-menu-outer {
        .Select-option.is-disabled {
            cursor: default;
            font-weight: 700;
            padding: 10px 10px 0 10px !important;
            color: #666;
        }
    }

    label {
        color: #5b5b5b;
        font-size: 14px;
        font-weight: 400;
        order: 1;
        margin-bottom: 4px;
    }

    .Select {
        order: 2;

        &.Select--multi {
            .Select-control {
                .Select-multi-value-wrapper {
                    display: inline-flex;
                    align-items: center;
                    flex-wrap: wrap;

                    .Select-value {
                        display: flex;
                        flex-direction: row-reverse;
                        margin-top: 3px;
                        margin-bottom: 3px;
                        padding: 5px;
                        border-radius: 20px;
                        border: 1px solid #cccccc;
                        color: #474747;
                        background-color: #ffffff;
                        font-size: 14px;
                    }

                    .Select-value-icon {
                        border: none;
                        background-color: #f0f4f6;
                        padding: 0 7px;
                        font-weight: 700;
                        border-radius: 50%;

                        &:hover {
                            color: #FFFFFF;
                        }
                    }
                }

            }
        }

        .Select-control {
            border-color: #474747;
            height: 46px;

            .Select-input {
                height: 44px;

                & > input {
                    line-height: 28px;
                    font-size: 1rem;
                    padding: 10px 0;
                }
            }

            .Select-value, 
            .Select-placeholder {
                vertical-align: baseline;
                font-size: 14px;
            }

            .Select-value {
                display: flex;
                align-items: center;
            }

            .Select-placeholder {
                line-height: 44px;
            }

            .Select-arrow-zone {
                padding-right: 12px;
                padding-top: 4px;
            }
        }

        .Select-menu-outer {
            margin-top: 2px;
            max-height: none;
            border: none;
            box-shadow: 0px 0px 15px -4px rgba(128,128,128,1);

            .Select-menu {
                .Select-option {
                    padding: 12px 16px;
                }
            }
        }

        &.is-open, &.is-focused, &.is-pseudo-focused {
            .Select-control {
                border-color: #0096d0;
                box-shadow: none;
                border-bottom-right-radius: 4px;
                border-bottom-left-radius: 4px;
            }
            &~label {
                color: #149ed3;
            }
        }

        &.is-disabled {
            .Select-control {
                cursor: not-allowed;
                background: #f8f8f8;
            }
            .Select-value-label {
                color: #d3d3d3 !important;
            }
            &~label {
                color: #9da0a1;
            }
        }

        &.is-error {
            .Select-control {
                border-color: #f21108;
            }

            &~label {
                color: #f2130a;
            }
        }
    }

    p {
        order: 3;
        color: #f21108;
        font-size: 13px;
        margin-bottom: 6px;
    }

    span.required {
        color: #eb7575;
    }

    .multiselect__tags {
        align-items: center;
        font-size: 14px;
    }
`;
