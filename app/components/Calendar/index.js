import React from 'react';
import DayPicker, { DateUtils } from 'react-day-picker';
import { Wrapper } from './styles';

/**
* Calendar
*/
class Calendar extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        handleDayClick: React.PropTypes.func,
        handleMonthClick: React.PropTypes.func,
        modifiers: React.PropTypes.object,
        modifiersStyles: React.PropTypes.object,
        month: React.PropTypes.string
    }

    /**
     * component constructor
     * @param {object} props - initial props on component construct
     */
    constructor( props ) {
        super( props );
        this.handleDayClick = this.handleDayClick.bind( this );

        this.state = {
            selectedDays: []
        };
    }

    /**
     * calendar day click event handler
     * @param day
     * @param selected
     */
    handleDayClick( day, { selected }) {
        const { selectedDays } = this.state;
        if ( selected ) {
            const selectedIndex = selectedDays.findIndex( ( selectedDay ) =>
                DateUtils.isSameDay( selectedDay, day ),
            );
            selectedDays.splice( selectedIndex, 1 );
        } else {
            selectedDays.push( day );
        }
        this.setState({ selectedDays });
    }

    /**
    * Calendar render method.
    */
    render() {
        return (
            <Wrapper>
                <DayPicker
                    fixedWeeks
                    selectedDays={ this.state.selectedDays }
                    month={ this.props.month ? new Date( this.props.month ) : new Date() }
                    onDayClick={ this.props.handleDayClick || this.handleDayClick }
                    onMonthChange={ this.props.handleMonthClick }
                    modifiers={ this.props.modifiers }
                    modifiersStyles={ this.props.modifiersStyles }
                    showOutsideDays
                    enableOutsideDays
                />
            </Wrapper>
        );
    }
}

export default Calendar;
