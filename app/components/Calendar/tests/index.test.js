import React from 'react';
import moment from 'moment';
import { shallow, mount } from 'enzyme';

import Calendar from '../index';

describe( '<Calendar />', () => {
    it( 'Should mount the component', () => {
        const component = shallow(
            <Calendar />
        );

        expect( component.html() ).toBeTruthy();
    });

    it( 'Should select and unselect dates on calendar click', () => {
        const component = mount(
            <Calendar />
        );
        component.find( '.DayPicker-Day--today' ).simulate( 'click' );
        expect( moment( component.instance().state.selectedDays[ 0 ]).format( 'YYYY[-]MM[-]DD' ) ).toEqual( moment().format( 'YYYY[-]MM[-]DD' ) );

        component.find( '.DayPicker-Day--today' ).simulate( 'click' );
        expect( component.instance().state.selectedDays ).toEqual([]);
    });

    it( 'Should display passed month', () => {
        const today = moment().format( 'YYYY[-]MM[-]DD' );
        const component = shallow(
            <Calendar month={ today } />
        );

        expect( component.instance().props.month ).toEqual( today );
    });
});
