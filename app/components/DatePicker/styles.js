import { css } from 'styled-components';

export const styles = ( props ) => css`
        position: relative;
        span {
            color: #5b5b5b;
            font-size: 14px;
            margin-bottom: 4px;
            margin-right: 1rem;
            font-weight: 400;

            .required {
                display: inline !important;
                color: #eb7575;
                margin: 0;
            }
        }

        span.isvg {
            margin-bottom: 0;
            margin-right: 0;
            height: 24px;
        }

        input {
            padding: 0 .75rem;
        }

        input {
            border: 1px solid rgba(0,0,0,.15);
            border-color: #95989a;
            border-radius: 0;
            line-height: 28px;
            background-color: #fdfdfd;
            height: 46px;
            padding-top: 5px;

            &:focus {
                border-color: #0096d0;
                background-color: #fdfdfd;

                &~label {
                    color: #149ed3;
                }

                & + .input-group-addon {
                    border-color: #0096d0;
                }
            }
            .input-group-addon {
                background-color: #f5f5f5;
                border: 1px solid rgb(149, 152, 154);
                border-left: none;
            }
            &:disabled {
                border-color: #c7c7c7;
                background: #f8f8f8;
                color: #d3d3d3;
                cursor: not-allowed;

                & + .input-group-addon {
                    border-color: #c7c7c7;
                }

                &~label {
                    color: #9da0a1;
                }
            }
            &.error {
                border-color: #f21108;

                &~label {
                    color: #f2130a;
                }

                & + .input-group-addon {
                    border-color: #f21108;
                }
            }
        }

        .error input {
            border-color: #f21108;

            &~label {
                color: #f2130a;
            }

            & + .input-group-addon {
                border-color: #f21108;
            }
        }

        * {
            outline: none;
        }

        .DayPicker {
            background: #FFF;
            display: flex !important;
            flex-wrap: wrap;
            justify-content: center;
            position: relative;
            padding: 1rem 0;
            user-select: none;
        }

        .DayPicker-Month {
            display: block;
            border-collapse: collapse;
            border-spacing: 0;
            user-select: none;
            margin: 0 1rem;
        }

        .DayPicker-NavBar {
            position: absolute;
            left: 0;
            right: 0;
            padding: 0 .5rem;
        }

        .DayPicker-NavButton {
            position: absolute;
            width: 14px;
            height: 14px;
            background-repeat: no-repeat;
            background-position: center;
            background-size: 14px;
            cursor: pointer;
        }

        .DayPicker-NavButton--prev {
            left: 1rem;
            background-image: url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+Cjxzdmcgd2lkdGg9IjI2cHgiIGhlaWdodD0iNTBweCIgdmlld0JveD0iMCAwIDI2IDUwIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIj4KICAgIDwhLS0gR2VuZXJhdG9yOiBTa2V0Y2ggMy4zLjIgKDEyMDQzKSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5wcmV2PC90aXRsZT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPgogICAgPGRlZnM+PC9kZWZzPgogICAgPGcgaWQ9IlBhZ2UtMSIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCIgc2tldGNoOnR5cGU9Ik1TUGFnZSI+CiAgICAgICAgPGcgaWQ9InByZXYiIHNrZXRjaDp0eXBlPSJNU0xheWVyR3JvdXAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEzLjM5MzE5MywgMjUuMDAwMDAwKSBzY2FsZSgtMSwgMSkgdHJhbnNsYXRlKC0xMy4zOTMxOTMsIC0yNS4wMDAwMDApIHRyYW5zbGF0ZSgwLjg5MzE5MywgMC4wMDAwMDApIiBmaWxsPSIjNTY1QTVDIj4KICAgICAgICAgICAgPHBhdGggZD0iTTAsNDkuMTIzNzMzMSBMMCw0NS4zNjc0MzQ1IEwyMC4xMzE4NDU5LDI0LjcyMzA2MTIgTDAsNC4yMzEzODMxNCBMMCwwLjQ3NTA4NDQ1OSBMMjUsMjQuNzIzMDYxMiBMMCw0OS4xMjM3MzMxIEwwLDQ5LjEyMzczMzEgWiIgaWQ9InJpZ2h0IiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIj48L3BhdGg+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4K");
        }

        .DayPicker-NavButton--next {
            right: 0.25rem;
            background-image: url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+Cjxzdmcgd2lkdGg9IjI2cHgiIGhlaWdodD0iNTBweCIgdmlld0JveD0iMCAwIDI2IDUwIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIj4KICAgIDwhLS0gR2VuZXJhdG9yOiBTa2V0Y2ggMy4zLjIgKDEyMDQzKSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5uZXh0PC90aXRsZT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPgogICAgPGRlZnM+PC9kZWZzPgogICAgPGcgaWQ9IlBhZ2UtMSIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCIgc2tldGNoOnR5cGU9Ik1TUGFnZSI+CiAgICAgICAgPGcgaWQ9Im5leHQiIHNrZXRjaDp0eXBlPSJNU0xheWVyR3JvdXAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuOTUxNDUxLCAwLjAwMDAwMCkiIGZpbGw9IiM1NjVBNUMiPgogICAgICAgICAgICA8cGF0aCBkPSJNMCw0OS4xMjM3MzMxIEwwLDQ1LjM2NzQzNDUgTDIwLjEzMTg0NTksMjQuNzIzMDYxMiBMMCw0LjIzMTM4MzE0IEwwLDAuNDc1MDg0NDU5IEwyNSwyNC43MjMwNjEyIEwwLDQ5LjEyMzczMzEgTDAsNDkuMTIzNzMzMSBaIiBpZD0icmlnaHQiIHNrZXRjaDp0eXBlPSJNU1NoYXBlR3JvdXAiPjwvcGF0aD4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPgo=");
        }


        .DayPicker-Caption {
            display: flex;
            padding: 0 20px;
            text-align: center;
            margin-top: 9px;
            font-size: 18px !important;
            margin: 0;

            .year-picker, .month-picker {
                flex: 1;
                display: flex;

                > * {
                    width: 100%;
                    font-size: 18px;
                }
            }

            .month-picker {
                .Select-value {
                    padding-left: 30px;
                }

                .Select {
                    .Select-control {
                        .Select-arrow-zone {
                            padding-top: 0;
                            padding-right: 0;
                            padding-left: 15px;
                            display: table-cell;
                        }
                    }
                }
            }

            .year-picker {
                .Select-value {
                    padding-left: 15px;
                }

                .Select {
                    .Select-control {
                        .Select-arrow-zone {
                            display: none;
                        }
                    }
                }
            }

            .Select {
                .Select-control {
                    border: none !important;
                    box-shadow: none !important;

                    .Select-multi-value-wrapper {
                        text-align: left;
                    }
    
                    .Select-value {
                        > .Select-value-label {
                            margin-bottom: 0;
                        }

                        .Select-value-label {
                            font-size: 18px;
                        }
                    }

                    .Select-input {
                        input {
                            height: 100%;
                            width: 100%;
                            padding: 0;
                            border: none;
                        }
                    }
                }
            }
        }

        .DayPicker-Weekdays {
            display: block;
            margin-top: 0;
        }

        .DayPicker-WeekdaysRow {
            display: block;
        }

        .DayPicker-Weekday {
            display: inline-flex;
            width: 34px;
            font-size: .875em;
            justify-content: center;
            color: rgba(0,0,0,.54);
            margin: 0 4px;
            font-weight: 600;
        }

        .DayPicker-Body {
            display: block;
        }

        .DayPicker-Week {
            display: flex;
        }

        .DayPicker-Day {
            display: flex;
            cursor: pointer;
            align-items: center;
            width: 38px;
            height: 38px;
            justify-content: center;
            border-radius: 50%;
            margin: 5px 2px;
            font-size: 14px;
        }

        .DayPicker--interactionDisabled .DayPicker-Day {
            cursor: default;
        }

        /* Default modifiers */

        .DayPicker-Day--today {
            border: 1px solid #959ea9;
            color: #393939;
            font-weight: 400;

            &:hover {
                background-color: #959ea9 !important;
                color: #FFF;
            }
        }

        .DayPicker-Day--disabled {
            color: #dce0e0;
            cursor: default;
            background-color: #eff1f1;
        }

        .DayPicker-Day--outside {
            cursor: default;
            color: #dce0e0;
        }

        /* Example modifiers */

        .DayPicker-Day--sunday {
            background-color: #f7f8f8;
        }

        .DayPicker-Day--sunday:not(.DayPicker-Day--today) {
            color: #dce0e0;
        }

        .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside) {
            color: #FFF;
            background-color: #00a5e2;
            border: 1px solid #00a5e2;
        }

        .DayPickerInput-OverlayWrapper {
            z-index: 999;
        }

        .DayPickerInput-Overlay {
            bottom: ${props.showOnTop ? '50px' : 'auto'}
        }
    `;
