export const DATE_FORMATS = [ 'MM/DD/YYYY', 'YYYY-MM-DD', 'MMM-DD-YYYY', 'MMMM DD, YYYY' ];

export const BACKSPACE_KEY_CODE = 8;
