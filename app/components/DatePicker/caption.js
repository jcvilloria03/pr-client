import React, { PropTypes } from 'react';
import MultiSelect from 'components/MultiSelect';

/**
 * @param {Object} props
 */
function YearMonthForm({ date, localeUtils, onChange, fromDate, toDate }) {
    const months = localeUtils.getMonths();

    const years = [];
    for ( let i = fromDate.getFullYear(); i <= toDate.getFullYear(); i += 1 ) {
        years.push( i );
    }

    const handleMonthChange = ( option ) => {
        const newDate = new Date( date.getFullYear(), option.value );
        onChange( newDate );
    };

    const handleYearChange = ( option ) => {
        const newDate = new Date( option.value, date.getMonth() );
        onChange( newDate );
    };

    return (
        <form className="DayPicker-Caption">
            <div className="month-picker">
                <MultiSelect
                    id="month"
                    value={ date.getMonth() }
                    data={ months.map( ( month, i ) => ({
                        value: i,
                        label: String( month )
                    }) ) }
                    multi={ false }
                    onChange={ handleMonthChange }
                    placeholder=""
                    notSearchable
                />
            </div>
            <div className="year-picker">
                <MultiSelect
                    id="year"
                    value={ date.getFullYear() }
                    data={ years.map( ( year ) => ({
                        value: year,
                        label: String( year )
                    }) ) }
                    multi={ false }
                    onChange={ handleYearChange }
                    placeholder=""
                />
            </div>

        </form>
    );
}

YearMonthForm.propTypes = {
    date: PropTypes.instanceOf( Date ),
    localeUtils: PropTypes.object,
    onChange: PropTypes.func,
    fromDate: PropTypes.instanceOf( Date ),
    toDate: PropTypes.instanceOf( Date )
};

export default YearMonthForm;
