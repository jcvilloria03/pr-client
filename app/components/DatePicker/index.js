import React from 'react';
import moment from 'moment';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import Icon from '../Icon';
import YearMonthForm from './caption';
import { styles } from './styles';
import { DATE_FORMATS, BACKSPACE_KEY_CODE } from './constants';

const Wrapper = styled.div`
    ${styles}
`;

const CalendarIcon = styled(Icon)`
    position: absolute;
    top: 50%;
    right: 10px;
    transform: translateY(-50%);
    cursor: pointer;
    width: 14px;
`;

const InputContainer = styled.div`
    position: relative;
`;


class DatePicker extends React.PureComponent {
    static propTypes = {
        dayFormat: React.PropTypes.string,
        disabled: React.PropTypes.bool,
        disabledDays: React.PropTypes.oneOfType([
            React.PropTypes.shape({
                daysOfWeek: React.PropTypes.array,
            }),
            React.PropTypes.array,
        ]),
        enableOutsideDays: React.PropTypes.bool,
        placeholder: React.PropTypes.string,
        label: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.element,
            React.PropTypes.node,
            React.PropTypes.symbol,
        ]).isRequired,
        selectedDay: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.object,
        ]),
        onChange: React.PropTypes.func,
        required: React.PropTypes.bool,
        hasIcon: React.PropTypes.bool,
        disablePreviousDates: React.PropTypes.bool,
    };

    static defaultProps = {
        disabled: false,
        dayFormat: 'YYYY-MM-DD',
        selectedDay: '',
        placeholder: '',
        required: false,
        keepFocus: true,
    };

    /**
     * component constructor
     */
    constructor(props) {
        super(props);

        this.state = {
            selectedDay: this.props.selectedDay || '',
            error: false,
            message: '',
            showOnTop: false,
            selectedMonth: '',
            disabled: props.disabled,
        };

        this.handleDayChange = this.handleDayChange.bind(this);
        this.checkRequired = this.checkRequired.bind(this);
        this.determineOverlayPosition =
            this.determineOverlayPosition.bind(this);
    }

    /**
     * this displays the payroll group and period selection screen if nothing has been selected yet.
     */

    /**
     * checks if theres a need to reposition the overlay and attaches the scroll event handler.
     */
    componentDidMount() {
        this.determineOverlayPosition();
        window.addEventListener('scroll', this.determineOverlayPosition);
    }

    /**
     * handles changes when new props are passed
     */
    componentWillReceiveProps(nextProps) {
        nextProps.required !== this.props.required &&
            !nextProps.required &&
            this.setState({ error: false });
        nextProps.disabled !== this.props.disabled &&
            this.setState({ disabled: nextProps.disabled });
    }

    /**
     * handles changes to props and states
     */
    componentDidUpdate(prevProps) {
        this.determineOverlayPosition();

        if (
            prevProps.selectedDay !== this.props.selectedDay &&
            moment(this.props.selectedDay, DATE_FORMATS).isValid()
        ) {
            this.setState({ selectedDay: this.props.selectedDay });
        }
    }

    /**
     * removes the event listeners
     */
    componentWillUnmount() {
        window.removeEventListener('scroll', this.determineOverlayPosition);
    }

    /**
     * sets the position of datepicker overlay
     */
    determineOverlayPosition() {
        const componentBound =
            ReactDOM.findDOMNode(this).getBoundingClientRect(); // eslint-disable-line react/no-find-dom-node
        const pageBound = document
            .querySelector('body')
            .getBoundingClientRect();
        this.setState({
            showOnTop: componentBound.bottom + 240 > pageBound.height,
        });
    }

    /**
     * triggers when selected date(s) is changed
     */
    handleDayChange(selectedDay) {
        const currentSelectedDay = moment(this.state.selectedDay, DATE_FORMATS);
        const newSelectedDay = moment(selectedDay, DATE_FORMATS);

        const value =
            !newSelectedDay.isValid() ||
            newSelectedDay.isSame(currentSelectedDay, 'day')
                ? ''
                : newSelectedDay.format(this.props.dayFormat);
        this.setState(
            {
                selectedDay: value,
            },
            () => {
                this.props.onChange && this.props.onChange(value);
                value && this.checkRequired();
            },
        );
    }

    /**
     * handles error display.
     */
    checkRequired() {
        let error = false;
        let message = '';

        if (this.props.required) {
            if (
                this.state.selectedDay === undefined ||
                this.state.selectedDay === null ||
                this.state.selectedDay === ''
            ) {
                message = 'This field is required';
                this.setState({ error: true, message });
                return true;
            }
        }

        if (this.state.selectedDay) {
            const value = moment(this.state.selectedDay, DATE_FORMATS).format(
                'YYYY-MM-DD',
            );
            const dateRegex = new RegExp(/^\d{4}[-]\d{2}[-]\d{2}$/g);
            const isError =
                !moment(value, DATE_FORMATS).isValid() ||
                !dateRegex.test(value);
            message = isError ? 'Invalid Date' : '';
            this.setState({ error: isError, message });
            error = isError;
        } else {
            this.setState({ error, message });
            error = false;
        }

        return error;
    }

    handleYearMonthChange = (event) => {
        this.setState({ selectedMonth: moment(event) });
    };

    /**
     * DatePicker render method.
     */
    render() {
        const { selectedDay, error, message, selectedMonth } = this.state;

        const {
            dayFormat,
            label,
            placeholder,
            disabledDays,
            enableOutsideDays = true,
            required,
            hasIcon,
            disablePreviousDates,
        } = this.props;
        const formattedDay = moment(selectedDay, DATE_FORMATS).isValid()
            ? moment(selectedDay, DATE_FORMATS).format(dayFormat)
            : selectedDay;

        const fromDate =
            disabledDays && disabledDays[0] && disabledDays[0].before
                ? disabledDays[0].before
                : new Date(new Date().getFullYear() - 100, 0, 1, 0, 0);
        const toDate =
            disabledDays && disabledDays[1] && disabledDays[1].after
                ? disabledDays[1].after
                : new Date(new Date().getFullYear() + 100, 11, 31, 23, 59);

        const selectedDayDate = formattedDay
            ? new Date(moment(selectedDay, DATE_FORMATS))
            : null;
        const selectedMonthDate = moment(selectedMonth, DATE_FORMATS).isValid()
            ? new Date(moment(selectedMonth, DATE_FORMATS))
            : new Date();

        const dayPickerProps = {
            weekdaysShort: [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ],
            disabledDays: disablePreviousDates ? {
                before: new Date(),
            } : disabledDays,
            showOutsideDays: enableOutsideDays,
            enableOutsideDaysClick: true,
            month: selectedMonthDate,
            initialMonth: selectedDayDate,
            fromMonth: disablePreviousDates ? new Date() : null,
            captionElement: (
                <YearMonthForm
                    fromDate={ fromDate }
                    toDate={ toDate }
                    onChange={ this.handleYearMonthChange }
                />
            ),
        };

        if (selectedDayDate) {
            dayPickerProps.selectedDays = selectedDayDate;
        }

        return (
            <Wrapper showOnTop={ this.state.showOnTop }>
                <span
                    style={ error ? { color: '#f2130a' } : {} }
                    className="label"
                >
                    {required && <span className="required">* </span>}
                    {label}
                </span>
                <InputContainer>
                    {hasIcon && (
                        <CalendarIcon
                            name="calendar"
                            className="icon-calendar"
                        />
                    )}
                    <DayPickerInput
                        value={ formattedDay }
                        onDayChange={ this.handleDayChange }
                        format={ dayFormat }
                        placeholder={ placeholder }
                        dayPickerProps={ dayPickerProps }
                        disabled={ this.state.disabled }
                        keepFocus={ false }
                        classNames={ {
                            container: this.state.error
                                ? 'DayPickerInput error'
                                : 'DayPickerInput',
                            overlayWrapper: 'DayPickerInput-OverlayWrapper',
                            overlay: 'DayPickerInput-Overlay',
                        } }
                        inputProps={ {
                            disabled: this.state.disabled,
                            onKeyDown: (event) => {
                                event.keyCode === BACKSPACE_KEY_CODE
                                    ? this.handleDayChange('')
                                    : event.preventDefault();
                            },
                        } }
                    />
                </InputContainer>

                {message && (
                    <p
                        style={ {
                            color: '#f21108',
                            fontSize: '13px',
                            paddingLeft: '2px',
                            marginBottom: '6px',
                            opacity: error ? '1' : '0',
                        } }
                        className="error-message"
                    >
                        {message || '.'}
                    </p>
                )}
            </Wrapper>
        );
    }
}

export default DatePicker;
