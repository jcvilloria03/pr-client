import React, { PropTypes } from 'react';
import SVG from 'react-inlinesvg';

import icons from '../../assets/icons';

/**
*
* Icon
*
*/
const Icon = ({ name, style, className }) => (
    <SVG
        src={ icons[ name ] }
        className={ className }
        style={ { ...style, pointerEvents: 'none' } }
    />
);

Icon.propTypes = {
    name: PropTypes.oneOf( Object.keys( icons ) ),
    style: PropTypes.shape(),
    className: PropTypes.string
};

export default Icon;
