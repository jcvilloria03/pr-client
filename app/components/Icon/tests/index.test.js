import { shallow } from 'enzyme';
import React from 'react';
import Icon from '../index';

describe( '<Icon />', () => {
    it( 'should be able to render a fontawesome icon', () => {
        const component = shallow(
            <Icon class="fa fa-bank" />
        );
        expect( component.find( '.fa-bank' ) ).toBeTruthy();
    });
});
