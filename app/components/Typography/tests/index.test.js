import React from 'react';
import { shallow } from 'enzyme';
import { H1, H2, H3, H4, H5, H6, P, SUB } from '../index';

const content = 'the content.';

describe( 'H1', () => {
    const component = shallow(
        <H1>
            { content }
        </H1>
    );
    it( 'should render an H1 tag', () => {
        expect( component.containsMatchingElement( <h1>the content.</h1> ) ).toEqual( true );
    });
    it( 'should contain its children', () => {
        expect( component.contains([content]) ).toEqual( true );
    });
});

describe( 'H2', () => {
    const component = shallow(
        <H2>
            { content }
        </H2>
    );
    it( 'should render an H2 tag', () => {
        expect( component.containsMatchingElement( <h2>the content.</h2> ) ).toEqual( true );
    });
    it( 'should contain its children', () => {
        expect( component.contains([content]) ).toEqual( true );
    });
});

describe( 'H3', () => {
    const component = shallow(
        <H3>
            { content }
        </H3>
    );
    it( 'should render an H3 tag', () => {
        expect( component.containsMatchingElement( <h3>the content.</h3> ) ).toEqual( true );
    });
    it( 'should contain its children', () => {
        expect( component.contains([content]) ).toEqual( true );
    });
});

describe( 'H4', () => {
    const component = shallow(
        <H4>
            { content }
        </H4>
    );
    it( 'should render an H4 tag', () => {
        expect( component.containsMatchingElement( <h4>the content.</h4> ) ).toEqual( true );
    });
    it( 'should contain its children', () => {
        expect( component.contains([content]) ).toEqual( true );
    });
});

describe( 'H5', () => {
    const component = shallow(
        <H5>
            { content }
        </H5>
    );
    it( 'should render an H5 tag', () => {
        expect( component.containsMatchingElement( <h5>the content.</h5> ) ).toEqual( true );
    });
    it( 'should contain its children', () => {
        expect( component.contains([content]) ).toEqual( true );
    });
});

describe( 'H6', () => {
    const component = shallow(
        <H6>
            { content }
        </H6>
    );
    it( 'should render an H6 tag', () => {
        expect( component.containsMatchingElement( <h6>the content.</h6> ) ).toEqual( true );
    });
    it( 'should contain its children', () => {
        expect( component.contains([content]) ).toEqual( true );
    });
});

describe( 'P', () => {
    const component = shallow(
        <P>
            { content }
        </P>
    );
    it( 'should render a p tag', () => {
        expect( component.containsMatchingElement( <p>the content.</p> ) ).toEqual( true );
    });
    it( 'should contain its children', () => {
        expect( component.contains([content]) ).toEqual( true );
    });
});

describe( 'SUB', () => {
    const component = shallow(
        <SUB>
            { content }
        </SUB>
    );
    it( 'should render an p tag', () => {
        expect( component.containsMatchingElement( <p>the content.</p> ) ).toEqual( true );
    });
    it( 'should contain its children', () => {
        expect( component.contains([content]) ).toEqual( true );
    });
});
