import styled from 'styled-components';

const H1 = styled.h1`
    font-size: 48px;
    font-weight: 200;
    margin: 0;
    ${( props ) => !props.noBottomMargin && 'margin-bottom: 20px;'}
`;

const H2 = styled.h2`
    font-size: 32px;
    font-weight: 300;
    margin: 0;
    ${( props ) => !props.noBottomMargin && 'margin-bottom: 20px;'}
`;

const H3 = styled.h3`
    font-size: 24px;
    font-weight: 300;
    margin: 0;
    ${( props ) => !props.noBottomMargin && 'margin-bottom: 20px;'}
`;

const H4 = styled.h4`
    font-size: 20px;
    font-weight: 400;
    margin: 0;
    ${( props ) => !props.noBottomMargin && 'margin-bottom: 20px;'}
`;

const H5 = styled.h5`
    font-size: 18px;
    font-weight: 600;
    margin: 0;
    ${( props ) => !props.noBottomMargin && 'margin-bottom: 20px;'}
`;

const H6 = styled.h6`
    font-size: 16px;
    font-weight: 600;
    margin: 0;
    ${( props ) => !props.noBottomMargin && 'margin-bottom: 20px;'}
`;

const P = styled.p`
    font-size: 14px;
    font-weight: 400;
    margin: 0;
    ${( props ) => !props.noBottomMargin && 'margin-bottom: 40px;'}
    ${( props ) => props.italicized && 'font-style: italic;'}
`;

const SUB = styled.p`
    font-size: 12px;
    margin: 0;
    font-weight: 400;
    letter-spacing: 0px;
`;

export { H1, H2, H3, H4, H5, H6, P, SUB };
