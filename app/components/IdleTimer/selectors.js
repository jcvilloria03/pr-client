import { createSelector } from 'reselect';

const selectIdleTimerDomain = () => ( state ) => state.get( 'idleTimer' );

const makeSelectShouldResetTimer = () => createSelector(
    selectIdleTimerDomain(),
    ( state ) => state.get( 'shouldResetTimer' )
);

export { makeSelectShouldResetTimer };
