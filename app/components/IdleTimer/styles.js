import styled, { keyframes } from 'styled-components';

const MainWrapper = styled.div`
    .foot {
        text-align: right;
        padding: 10px 10vw;
        background: #f0f4f6;

        button {
            min-width: 120px;
        }
    }
`;

const ModalWrapper = styled.div`
    .modal-buttons {
        width: 100%;
        display: flex;
        flex-direction: column;
        justify-items:  space-around;
        padding-top: 30px;

        a {
            width: 100%;
        }
    }

    a {
        text-align: center;
        border: 1px solid #9fdc74;
        background-color: #ffffff;
        color: #474747;
        padding: 7px 21px;
        border-radius: 20px;
        font-size: 14px;
        line-height: 1.5;
        display: inline;
        vertical-align: middle;
        margin: 2px;
        -webkit-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
    }
`;

const anim3 = keyframes`
    to {
        transform: rotate(45deg) translate(3px, 3px);
    }
`;

const StyledLoader = styled.div`
    font-size: 16px;
    font-weight: 600;
    color: #00a5e5;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;

    .anim3 {
        padding-left: 10px;
        &:before {
            display: block;
            width: 12px;
            height: 12px;
            margin-top: -1px;
            border: 2px solid #00a5e5;
            content: "";
            animation: ${anim3} .5s ease infinite alternate;
            border-radius: 100% 100% 0 100%;
            transform: rotate( 45deg );
        }

        &:after {
            display: block;
            width: 12px;
            height: 3px;
            margin-top: 8px;
            background-color: rgba(0, 0, 0, 0.2);
            content: "";
            border-radius: 100%;
        }
    }
`;

export { MainWrapper, StyledLoader, ModalWrapper };
