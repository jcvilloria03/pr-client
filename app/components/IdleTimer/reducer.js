/* eslint-disable require-jsdoc */

import { fromJS } from 'immutable';

import { SET_SHOULD_RESET_TIMER } from './constants';

const initialState = fromJS({
    shouldResetTimer: false
});

function idleTimerReducer( state = initialState, action ) {
    switch ( action.type ) {
        case SET_SHOULD_RESET_TIMER:
            return state.set( 'shouldResetTimer', action.payload );
        default:
            return state;
    }
}

export default idleTimerReducer;
