/* eslint-disable require-jsdoc */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import IdleTimer from 'react-idle-timer';
import { createStructuredSelector } from 'reselect';
import { auth } from '../../utils/AuthService';
import SalConfirm from '../../components/SalConfirm';
import CountdownTimerContainer from '../../components/CountdownTimer';
import { ModalWrapper } from './styles';
import { makeSelectShouldResetTimer } from './selectors';
import { setShouldResetTimer } from './actions';

const { SESSION_IDLE_TIME_MS, SESSION_TIMEOUT_COUNTDOWN_SECONDS } = process.env;

/**
 * Idle Timer
 */
class IdleTimerContainer extends Component {
    constructor( props ) {
        super( props );
        this.state = {
            showCloseConfirm: false
        };
        this.idleTimer = null;
        this.isIdleModalActive = false;
        this.handleOnIdle = this.handleOnIdle.bind( this );
        this.handleLogoutNow = this.handleLogoutNow.bind( this );
        this.handleStayConnected = this.handleStayConnected.bind( this );
        this.handleOnCountdownFinish = this.handleOnCountdownFinish.bind( this );
        this.handleGetLastActiveTime = this.handleGetLastActiveTime.bind( this );
        this.handleGetElapsedTime = this.handleGetElapsedTime.bind( this );
        this.handleGetRemainingTime = this.handleGetRemainingTime.bind( this );
        this.handleIsIdle = this.handleIsIdle.bind( this );
        this.interval = null;
        window.timerComponent = this;
        window.lastActiveTime = 0;
    }

    componentDidMount() {
        const currentTime = new Date().getTime();
        const sessionIdleTime = Number( SESSION_IDLE_TIME_MS );

        const lastActiveTime = Number( localStorage.getItem( 'lastActiveTime' ) ) || currentTime + SESSION_IDLE_TIME_MS;
        const countdownTimeLeft =
        Number( localStorage.getItem( 'countdownTimeLeft' ) ) ||
        Number( SESSION_TIMEOUT_COUNTDOWN_SECONDS ) * 1000;

        if (
            currentTime - lastActiveTime >
            sessionIdleTime + countdownTimeLeft
        ) {
            this.handleLogoutNow();
        }

        this.interval = setInterval( () => {
            if ( auth.loggedIn() && !this.handleIsIdle() ) {
                localStorage.setItem( 'lastActiveTime', Date.now() );
            }

            const propagateCancel = localStorage.getItem( 'propagateCancel' );
            if ( propagateCancel === 'true' ) {
                this.isIdleModalActive = false;
                this.handleReset();
                localStorage.removeItem( 'countdownTimeLeft' );
                localStorage.removeItem( 'lastActiveTime' );
                this.setState({ showCloseConfirm: false }, () => {
                    this.isIdleModalActive = false;
                    localStorage.removeItem( 'propagateCancel' );
                });
            }
        }, 1000 );
    }

    componentDidUpdate( prevProps ) {
        if ( this.props.shouldResetTimer === true && this.props.shouldResetTimer !== prevProps.shouldResetTimer ) {
            this.handleReset();
        }
    }

    componentWillUnmount() {
        if ( this.interval ) {
            clearInterval( this.interval );
        }
    }

    handleLogoutNow() {
        this.setState({ showCloseConfirm: false }, () => {
            localStorage.removeItem( 'countdownTimeLeft' );
            localStorage.removeItem( 'lastActiveTime' );
            this.isIdleModalActive = false;
            auth.logout();
        });

        localStorage.setItem( 'propagateLogout', 'true' );
    }

    handleStayConnected( propagateEvent = false ) {
        this.setState({ showCloseConfirm: false }, () => {
            localStorage.removeItem( 'countdownTimeLeft' );
            localStorage.removeItem( 'lastActiveTime' );
            this.handleReset();
            this.isIdleModalActive = false;
        });

        localStorage.setItem( 'propagateCancel', propagateEvent.toString() );
    }

    handleOnIdle() {
        if ( auth.loggedIn() && this.isIdleModalActive === false ) {
            localStorage.setItem( 'propagateLogout', 'true' );
            localStorage.setItem( 'lastActiveTime', Date.now() );
            this.setState({ showCloseConfirm: true }, () => {
                this.isIdleModalActive = true;
            });
        }
    }

    handleOnCountdownFinish() {
        auth.logout();
        this.setState({ showCloseConfirm: false }, () => {
            this.isIdleModalActive = false;
        });

        localStorage.setItem( 'propagateLogout', 'true' );
    }

    handleReset() {
        if ( this.idleTimer ) {
            this.idleTimer.reset();
            this.props.dispatch( setShouldResetTimer( false ) );
        }
    }

    handlePause() {
        this.idleTimer.pause();
    }

    handleResume() {
        this.idleTimer.resume();
    }

    handleGetLastActiveTime() {
        return this.idleTimer.getLastActiveTime();
    }

    handleGetElapsedTime() {
        return this.idleTimer.getElapsedTime();
    }

    handleGetRemainingTime() {
        return this.idleTimer.getRemainingTime();
    }

    handleIsIdle() {
        return this.idleTimer && this.idleTimer.isIdle();
    }

    renderModalBody = () => (
        <ModalWrapper>
            <div>
                <center>
                    <p>
                        Your secure connection to Salarium is about to time-out.
                    </p>
                    <p>
                        You will be automatically logged out in{' '}
                        <CountdownTimerContainer
                            onFinish={ this.handleOnCountdownFinish }
                        />{' '}
                        seconds.
                    </p>
                </center>
            </div>
        </ModalWrapper>
    );

    render() {
        return (
            auth.loggedIn() && (
                <div>
                    <SalConfirm
                        title="Session Expiring"
                        confirmText="Stay Connected"
                        cancelText="Log Out Now"
                        body={ this.renderModalBody() }
                        onClose={ this.handleLogoutNow }
                        onConfirm={ () => this.handleStayConnected( true ) }
                        visible={ this.state.showCloseConfirm }
                        buttonStyle="success"
                        backdrop="static"
                    />
                    <IdleTimer
                        ref={ ( ref ) => {
                            this.idleTimer = ref;
                        } }
                        timeout={ Number( SESSION_IDLE_TIME_MS ) }
                        onIdle={ this.handleOnIdle }
                        debounce={ 250 }
                        crossTab={ {
                            emitOnAllTabs: true
                        } }
                    />
                </div>
            )
        );
    }
}

const mapStateToProps = createStructuredSelector({
    shouldResetTimer: makeSelectShouldResetTimer()
});

function mapDispatchToProps( dispatch ) {
    return {
        dispatch
    };
}

export default connect( mapStateToProps, mapDispatchToProps )( IdleTimerContainer );

IdleTimerContainer.propTypes = {
    shouldResetTimer: React.PropTypes.bool,
    dispatch: React.PropTypes.func
};
