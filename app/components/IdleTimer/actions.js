/* eslint-disable require-jsdoc */

import {
    SET_SHOULD_RESET_TIMER
} from './constants';

export function setShouldResetTimer( boolean ) {
    return {
        type: SET_SHOULD_RESET_TIMER,
        payload: boolean
    };
}
