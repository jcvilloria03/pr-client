import { shallow, mount } from 'enzyme';
import React from 'react';
import SalCheckbox from '../index';

describe( '<SalCheckbox />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <SalCheckbox
                id="test"
            />
        );
        expect( component.find( 'SalCheckbox' ) ).toBeTruthy();
    });

    it( 'should mount a component with label', () => {
        const component = shallow(
            <SalCheckbox id="test" >Test Label</SalCheckbox>
        );
        expect( component.find( 'SalCheckbox' ) ).toBeTruthy();
    });

    it( 'should be rendered checked', () => {
        const component = shallow(
            <SalCheckbox
                id="test"
                checked
            />
        );
        expect( component.instance().props.checked ).toEqual( true );
    });

    it( 'should be rendered as disabled', () => {
        const component = shallow(
            <SalCheckbox
                id="test"
                disabled
            />
        );
        expect( component.find( '.rc-checkbox-disabled' ) ).toBeTruthy();
    });

    it( 'should handle change events', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <SalCheckbox
                id="test"
                onChange={ _handleChange }
                required
                checked
            />
        );

        component.find( 'input' ).simulate( 'change' );
        expect( _handleChange ).toHaveBeenCalled();
    });

    it( 'should display errors', () => {
        const component = shallow(
            <SalCheckbox
                id="test"
                required
            />
        );
        component.find( '#test' ).simulate( 'change', { target: { checked: true }});
        component.find( '#test' ).simulate( 'change', { target: { checked: false }});
        expect( component.instance().state.error ).toEqual( true );
    });
});
