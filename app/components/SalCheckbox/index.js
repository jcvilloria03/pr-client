import React from 'react';
import Checkbox from 'rc-checkbox';
import 'rc-checkbox/assets/index.css';
import {
    StyledLabel
} from './styles';

/**
*
* Checkbox
*
*/
export default class SalCheckbox extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        id: React.PropTypes.string.isRequired,
        disabled: React.PropTypes.bool,
        onClick: React.PropTypes.func,
        onChange: React.PropTypes.func,
        children: React.PropTypes.any,
        checked: React.PropTypes.bool,
        required: React.PropTypes.bool
    }

    static defaultProps = {
        disabled: false,
        checked: false
    };

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            disabled: this.props.disabled,
            error: false,
            checked: this.props.checked
        };

        this._onChange = this._onChange.bind( this );
    }

    /**
     * Sets the error state true if prop required is true and checked value is false.
     */
    _onChange = ( event ) => {
        const checked = event.target.checked;
        if ( this.props.required && !checked ) {
            this.setState({ error: true });
        } else {
            this.setState({ error: false });
            this.props.onChange && this.props.onChange( checked );
        }
    }

    /**
     * render method
     */
    render() {
        return (
            <StyledLabel htmlFor={ this.props.id } className={ this.state.disabled ? 'disabled' : '' }>
                <Checkbox
                    className={ this.state.error ? 'error' : '' }
                    checked={ this.props.checked }
                    name={ this.props.id }
                    id={ this.props.id }
                    onClick={ this.props.onClick }
                    onChange={ this._onChange }
                    disabled={ this.props.disabled }
                />
                {
                    this.props.children ?
                        <span className="label">
                            { React.Children.toArray( this.props.children ) }
                        </span>
                        :
                        ''
                }
            </StyledLabel>
        );
    }
}
