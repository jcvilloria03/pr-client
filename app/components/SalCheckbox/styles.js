import styled from 'styled-components';
export const StyledLabel = styled.label`
    margin-bottom: 0;
    &.disabled {
        cursor: not-allowed;
    }
    .rc-checkbox.error, .rc-checkbox.error:hover {
        .rc-checkbox-inner {
            border-color: #f21108;
        }
    }
    .rc-checkbox-inner {
        border-color: #b0b2b4;
    }
    .rc-checkbox-checked .rc-checkbox-inner {
        border-color: #1ba0d4;
        background-color: #fff;
    }
    .rc-checkbox-checked .rc-checkbox-inner:after {
        border-color: #1ba0d4;
    }
    .rc-checkbox:hover .rc-checkbox-inner, .rc-checkbox-focused .rc-checkbox-inner {
        border-color: #1ba0d4;
    }
    .rc-checkbox-disabled .rc-checkbox-inner, .rc-checkbox-disabled:hover .rc-checkbox-inner {
        border-color: #d0d0d0;
        background-color: #f3f3f3;
    }
    .label {
        margin-left: 6px;
    }
`;
