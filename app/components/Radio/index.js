import React from 'react';
import RadioIcon from './RadioIcon';

/**
* Radio
*/
class Radio extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        iconSize: React.PropTypes.number,
        iconInnerSize: React.PropTypes.number,
        padding: React.PropTypes.number,
        rootColor: React.PropTypes.string,
        pointColor: React.PropTypes.string,
        value: React.PropTypes.string, // eslint-disable-line react/no-unused-prop-types
        index: React.PropTypes.number,
        checked: React.PropTypes.bool,
        children: React.PropTypes.node,
        horizontal: React.PropTypes.bool,
        onChange: React.PropTypes.func,
        last: React.PropTypes.bool,
        disabled: React.PropTypes.bool
    }
    /**
     * Component constructor
     */
    constructor() {
        super();
        this.getStyles = this.getStyles.bind( this );
        this.onClick = this.onClick.bind( this );
    }

    /**
     * handles click event
     */
    onClick() {
        const { onChange, index, disabled } = this.props;
        !disabled && onChange && onChange( index );
    }

    /**
     * Sets up and generate styles for component
     * @returns {{root: {borderWidth: number, borderStyle: string, borderColor: string, borderRadius: number, padding: number, flex: number, marginBottom: number, marginRight: number}, checked: {borderColor: string, color: string}}}
     */
    getStyles() {
        const { horizontal, last, padding, pointColor } = this.props;

        return {
            root: {
                padding: horizontal ? padding || 16 : 0,
                marginBottom: 0,
                marginRight: horizontal && !last ? ( padding || 16 ) / 2 : 0,
                cursor: 'pointer',
                borderColor: '#474747'
            },
            disabled: {
                opacity: '0.5',
                cursor: 'default'
            },
            checked: {
                borderColor: pointColor || '#00A5E5',
                color: pointColor || '#00A5E5'
            }
        };
    }

    /**
    * Radio render method.
    */
    render() {
        const { checked, iconSize, iconInnerSize, rootColor, pointColor, children, disabled } = this.props;
        const style = this.getStyles();
        const buttonStyle = Object.assign( style.root, checked ? style.checked : {}, disabled ? style.disabled : {});
        /* eslint-disable jsx-a11y/no-static-element-interactions */
        return (
            <div style={ buttonStyle } onClick={ this.onClick } id={ children }>
                <div style={ { display: 'inline-flex' } }>
                    <RadioIcon
                        size={ iconSize }
                        innerSize={ iconInnerSize }
                        checked={ checked }
                        rootColor={ rootColor }
                        pointColor={ pointColor }
                    />
                    <div style={ { paddingLeft: '6px', color: '#474747', display: 'flex', alignItems: 'center' } }>
                        { children }
                    </div>
                </div>
            </div>
        );
    }
}

export default Radio;
