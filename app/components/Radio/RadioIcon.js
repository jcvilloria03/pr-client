import React from 'react';

/**
 * radio Icon
 */
class RadioIcon extends React.Component {
    static propTypes = {
        size: React.PropTypes.number,
        innerSize: React.PropTypes.number,
        pointColor: React.PropTypes.string,
        checked: React.PropTypes.bool,
    }

    /**
     * component constructor
     */
    constructor(props) {
        super(props);
        this.getStyles = this.getStyles.bind(this);
    }

    /**
     * generates component styles
     * @returns {{root: {width: (RadioIcon.props.size|number), height: (RadioIcon.props.size|number), padding: number, backgroundColor: string, borderWidth: number, borderRadius: string, borderStyle: string, borderColor: (RadioIcon.props.rootColor|string)}, checked: {borderColor: (RadioIcon.props.pointColor|string)}, inner: {width: (RadioIcon.props.innerSize|number), height: (RadioIcon.props.innerSize|number), borderRadius: string, background: (RadioIcon.props.pointColor|string)}}}
     */
    getStyles() {
        const { size, innerSize, pointColor, checked } = this.props;

        return {
            root: {
                height: size || 14,
                margin: '5px 0',
                backgroundColor: '#FFF',
                borderWidth: 1,
                borderRadius: '50%',
                borderStyle: 'solid',
                borderColor: '#474747',
            },
            checked: {
                borderColor: '#474747',
            },
            inner: {
                width: innerSize || 8,
                height: innerSize || 8,
                borderRadius: '50%',
                background: checked ? pointColor || '#00A5E5' : 'transparent',
                margin: 2,
            },
        };
    }

    /**
     * component render method
     * @returns {JSX} React Component
     */
    render() {
        const { checked } = this.props;
        const style = this.getStyles();
        const iconStyle = Object.assign(style.root, checked ? style.checked : {});
        return (
            <div style={ iconStyle }>
                { <div style={ style.inner } /> }
            </div>
        );
    }
}

export default RadioIcon;
