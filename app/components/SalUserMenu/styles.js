import styled from 'styled-components';

export const Wrapper = styled.div`
    .dropdown-menu {
        position: absolute;
        top: 85%;
        left: auto;
        right: 0;
        z-index: 10;
        min-width: 11.7rem;
        font-size: 14px;
        background-color: #F0F4F6;
        border: none;
        box-shadow: 0 1px 2px 0 #cccccc;
        padding: 0;

        &:before {
            position: absolute;
            top: -.5rem;
            left: auto;
            right: 1.4rem;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 0 .5rem .5rem .5rem;
            border-color: transparent transparent #F0F4F6 transparent;
            content: '';
        }

        > span {
            &:first-child {
                a {
                    border-top-right-radius: .25rem;
                    border-top-left-radius: .25rem;
                }
            }

            &:last-child {
                a {
                    border-bottom-right-radius: .25rem;
                    border-bottom-left-radius: .25rem;
                }
            }

            &:not(:last-child) {
                a {
                    border-bottom: 1px solid #eee;
                }
            }
        }

        a {
            display: flex;
            padding: .5rem 1rem .5rem .5rem;
            color: #474747;
            border-left: .25rem solid transparent;
            font-weight: 400;
            align-items: center;

            &.active,
            &:hover {
                border-left-color: #00A5E5;
                color: #0077A5;
                border-bottom: none;
                box-shadow: none;
            }
        }

        .item-icon > svg {
            display: inline-flex;
            align-items: center;
            justify-content: center;
            width: 1rem;
            height: 1rem;
            fill: currentColor;
            vertical-align: -.2rem;
            margin-right: 1rem;
        }
    }

    .dropdown {
        padding: 1rem;
    }

    .dropdown > a {
        color: #ffffff;
        padding: 1rem;
    }

    img {
        border-radius: 50%
    }
`;
