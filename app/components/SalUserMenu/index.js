import React, { Component } from 'react';
import { Dropdown, DropdownMenu, DropdownToggle } from 'reactstrap';
import A from '../A';
import Icon from '../Icon';
import { Wrapper } from './styles';

/**
 *
 * User Menu
 *
 */
export default class SalUserMenu extends Component {
    static propTypes = {
        dropdownItems: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                id: React.PropTypes.string,
                icon: React.PropTypes.string,
                label: React.PropTypes.string,
                onClick: React.PropTypes.func
            })
        ).isRequired
    };

    state = {
        dropdownOpen: false
    }

    toggle = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    render() {
        return (
            <Wrapper>
                <Dropdown isOpen={ this.state.dropdownOpen } toggle={ this.toggle }>
                    <DropdownToggle
                        tag="span"
                        onClick={ this.toggle }
                        data-toggle="dropdown"
                        aria-expanded={ this.state.dropdownOpen }
                    >
                        <div
                            style={ {
                                height: '30px',
                                width: '30px'
                            } }
                        >
                            <Icon
                                name="userCircleInverted"
                            />
                        </div>
                    </DropdownToggle>
                    <DropdownMenu>
                        { this.props.dropdownItems.map( ( item ) => (
                            <span key={ item.id }>
                                <A onClick={ item.onClick }>
                                    <Icon name={ item.icon } className="item-icon" />
                                    { item.label }
                                </A>
                            </span>
                        ) )}
                    </DropdownMenu>
                </Dropdown>
            </Wrapper>
        );
    }
}
