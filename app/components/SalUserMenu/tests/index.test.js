import React from 'react';
import { shallow } from 'enzyme';
import SalUserMenu from '../../SalUserMenu';
import A from '../../A';

describe( '<SalUserMenu', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <SalUserMenu
                dropdownItems={ [] }
            />
        );
        expect( component.html() ).toBeTruthy();
    });

    it( 'should display dropdownItems', () => {
        const dropdownItems = [
            {
                id: 'user',
                icon: 'userCircle',
                label: 'User',
                onClick: () => {}
            }
        ];
        const component = shallow(
            <SalUserMenu
                dropdownItems={ dropdownItems }
            />
        );
        expect( component.find( A ).length ).toBe( 1 );
    });
});
