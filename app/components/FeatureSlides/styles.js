/* eslint-disable no-tabs */
import styled from 'styled-components';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

export const StyledSlider = styled( Slider )`
	margin: 0;
	background-color: #fff;
	width: -webkit-fill-available;

	.slick-dots {
		height: 0;

		li button::before {
			color: #d6d6d6;
			font-size: 20px;
			opacity: 1 !important;
		}

		li button:hover::before {
			color: #00A4E4;
		}


		li.slick-active button::before {
			color: #00A4E4 !important;
		}
	}

	.slide-container {
		text-align: center;
		display: flex;

		.slide-text-container {
			margin-top: 2em;

			.slide-title {
				color: #00a4e4;
				font-size: 46px;
				font-weight: bold;
				width: 60%;
				margin: 0 auto;
				letter-spacing: normal;
			}

			.slide-description {
				color: black;
				margin: 10px auto auto;
				font-size: 24px;
				letter-spacing: normal;
			}
		}

		.slide-image-container {
			margin-top: 100px;

			img {
				margin: auto;
				width: auto; 
				height: 25vw;
				padding: 1em;
			}
		}
	}
`;
