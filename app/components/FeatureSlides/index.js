/* eslint-disable no-tabs */
import React from 'react';
import { H2, P } from '../Typography';
import { StyledSlider } from './styles';

/**
 * FeatureSlides Container
 */
class FeatureSlides extends React.PureComponent {
    static propTypes = {
        slides: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                title: React.PropTypes.string,
                description: React.PropTypes.string,
                image: React.PropTypes.any
            })
        )
    };

    static defaultProps = {
        slides: null
    };

    render() {
        const { slides } = this.props;

        return (
            <StyledSlider
                dots
                infinite
                autoplay
                speed={ 700 }
                autoplaySpeed={ 4000 }
                slidesToShow={ 1 }
                slidesToScroll={ 1 }
                arrows={ false }
            >
                { slides.map( ( slide, index ) => (
                    <div key={ index } className="slide-container">
                        <div className="slide-text-container">
                            <H2 className="slide-title" >
                                { slide.title }
                            </H2>
                            <P className="slide-description" >
                                { slide.description }
                            </P>
                        </div>
                        <div className="slide-image-container">
                            <img src={ slide.image } alt={ slide.title } />
                        </div>
                    </div>
				) ) }
            </StyledSlider>
        );
    }
}

export default FeatureSlides;
