/* eslint-disable no-tabs */
import React from 'react';
import { shallow } from 'enzyme';
import './matchMedia.mock';
import FeatureSlides from '../index';

describe( '<FeatureSlides />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <FeatureSlides
                slides={ [] }
            />
        );
        expect( component.html() ).toBeTruthy();
    });
});
