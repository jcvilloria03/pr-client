import React from 'react';
import { shallow } from 'enzyme';
import Clipboard from '../index';

describe( '<Clipboard />', () => {
    it( 'Should mount the component', () => {
        const component = shallow(
            <Clipboard />
        );

        expect( component.html() ).toBeTruthy();
    });

    it( 'Should copy the value to clipboard', () => {
        jest.useFakeTimers();
        const component = shallow(
            <Clipboard value="test" />
        );

        component.instance().onCopy();
        expect( component.instance().state.copied ).toEqual( true );
        jest.runAllTimers();
        expect( component.instance().state.copied ).toEqual( false );
    });

    it( 'Should change the value when passed a new one', () => {
        const component = shallow(
            <Clipboard value="test" />
        );
        const value = 'Test Value';
        component.setProps({ value });
        expect( component.instance().state.value ).toEqual( value );
    });
});
