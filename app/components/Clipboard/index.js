import React from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';
import Button from '../Button';

/**
* Clipboard
*/
class Clipboard extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        value: React.PropTypes.string
    }

    /**
     * component constructor
     */
    constructor( props ) {
        super( props );
        this.state = {
            value: this.props.value || '',
            copied: false
        };

        this.onCopy = this.onCopy.bind( this );
    }

    /**
     * updates the component state based on passed props
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.value !== this.state.value && this.setState({ value: nextProps.value });
    }

    /**
     * handles button click
     */
    onCopy() {
        this.setState({ copied: true }, () => {
            setTimeout( () => {
                this.setState({ copied: false });
            }, 1000 );
        });
    }

    /**
    * Clipboard render method.
    */
    render() {
        return (
            <CopyToClipboard
                text={ this.state.value }
                onCopy={ this.onCopy }
            >
                <Button label={ <span><i className="fa fa-copy" /> { this.state.copied ? 'COPIED' : 'COPY' } </span> } type="primary" alt />
            </CopyToClipboard>
        );
    }
}

export default Clipboard;
