import React from 'react';
import { Input as BootstrapInput, InputGroup, InputGroupAddon } from 'reactstrap';
import {
    Wrapper
} from './styles';

/**
 *
 * Input
 *
 */
class Input extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        new: React.PropTypes.bool,
        type: React.PropTypes.oneOf([
            'text',
            'password',
            'number',
            'email',
            'textarea'
        ]),
        className: React.PropTypes.string,
        clientError: React.PropTypes.string,
        disabled: React.PropTypes.bool,
        id: React.PropTypes.string.isRequired,
        min: React.PropTypes.number,
        max: React.PropTypes.number,
        minNumber: React.PropTypes.number,
        maxNumber: React.PropTypes.number,
        required: React.PropTypes.oneOfType([
            React.PropTypes.bool,
            React.PropTypes.string
        ]),
        onChange: React.PropTypes.func,
        onKeyPress: React.PropTypes.func,
        onFocus: React.PropTypes.func,
        onBlur: React.PropTypes.func,
        mask: React.PropTypes.func,
        label: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.element,
            React.PropTypes.node,
            React.PropTypes.symbol
        ]),
        placeholder: React.PropTypes.string,
        value: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.number
        ]),
        valueFormat: React.PropTypes.string,
        addon: React.PropTypes.shape({
            content: React.PropTypes.oneOfType([
                React.PropTypes.string,
                React.PropTypes.number,
                React.PropTypes.element,
                React.PropTypes.node
            ]).isRequired,
            placement: React.PropTypes.oneOf([
                'left', 'right'
            ]).isRequired
        })
    };

    static defaultProps = {
        value: '',
        valueFormat: '',
        type: 'text',
        disabled: false
    };

    /**
     * constructor method of the component
     */
    constructor( props ) {
        super( props );
        this.state = {
            errorMessage: '&nbsp;',
            error: false,
            value: this.props.value,
            focus: false,
            disabled: props.disabled
        };

        // custom methods
        this._validate = this._validate.bind( this );
        this._handleChange = this._handleChange.bind( this );
        this._checkMin = this._checkMin.bind( this );
        this._checkMax = this._checkMax.bind( this );
        this._checkEmail = this._checkEmail.bind( this );
        this._checkSpaces = this._checkSpaces.bind( this );
        this._getLabel = this._getLabel.bind( this );
        this._handleFocus = this._handleFocus.bind( this );
    }

    /**
     * handles property changes
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        nextProps.value !== this.props.value && this.setState({ value: nextProps.value });
        nextProps.disabled !== this.props.disabled && this.setState({ disabled: nextProps.disabled });
        nextProps.clientError !== this.props.clientError && this.setState({ errorMessage: nextProps.clientError, error: !!nextProps.clientError });
    }

    /**
     * onChange handler - executes validation first then the callback if there is any.
     */
    _handleChange = ( event ) => {
        event.preventDefault();
        this.setState({ focus: event.type !== 'blur' });
        let value = this.props.valueFormat
            ? ( event.target.value || '' ).replace( new RegExp( this.props.valueFormat, 'gmi' ), '' )
            : event.target.value;
        if ( this.props.mask ) {
            value = this.props.mask( value );
        }
        this._validate( value, event.type );
        event.type === 'blur' && this.state.value && this.setState({ value: typeof this.state.value === 'number' ? this.state.value : this.state.value.trim() });
    }

    /**
     * validation for input value upon blur or change depending on input type
     *
     * @param value - value of input to validate
     * @returns {Boolean} returns error status
     */
    _validate = ( value, type = 'blur' ) => {
        const {
            type: inputType,
            min: inputMin,
            max: inputMax,
            minNumber: inputMinNumber,
            maxNumber: inputMaxNumber,
            required: inputRequired,
            onBlur: inputOnBlur,
            onChange: inputOnChange
        } = this.props;
        let error = false;
        let errorMessage = '&nbsp;';
        let response;

        const notEmpty = value !== null && ( typeof value === 'number' || value.trim() !== '' );
        if ( notEmpty ) {
            switch ( inputType ) {
                case 'password':
                    if ( !this._checkSpaces( value ) ) {
                        error = true;
                        errorMessage = 'This field cannot contain spaces';
                    } else {
                        error = false;
                    }
                    break;
                case 'email':
                    if ( !this._checkEmail( value ) ) {
                        error = true;
                        errorMessage = 'Enter a valid email address.';
                    } else {
                        error = false;
                    }
                    break;
                default:
                    break;
            }

            if ( inputMin && !this._checkMin( value ) ) {
                error = true;
                errorMessage = `This field requires at least ${inputMin} characters`;
            } else {
                error = error || false;
            }

            if ( inputMax && !this._checkMax( value ) ) {
                error = true;
                errorMessage = `This field must not exceed ${inputMax} characters`;
            } else {
                error = error || false;
            }

            if ( inputMinNumber !== undefined && !this._checkMinNumber( value ) ) {
                error = true;
                errorMessage = `This field's minimum value is ${inputMinNumber}`;
            } else {
                error = error || false;
            }

            if ( inputMaxNumber !== undefined && !this._checkMaxNumber( value ) ) {
                error = true;
                errorMessage = `This field's maximum value is ${inputMaxNumber}`;
            } else {
                error = error || false;
            }

            response = {
                error,
                errorMessage,
                value
            };
        } else {
            const requiredErrorMessage = inputRequired === true
                ? 'This field is required'
                : inputRequired;

            response = {
                error: Boolean( inputRequired ),
                errorMessage: inputRequired
                    ? requiredErrorMessage
                    : errorMessage,
                value
            };
        }

        if ( type === 'blur' ) {
            this.setState( response, () => {
                inputOnBlur && inputOnBlur( value );
            });
        } else {
            response.error = false;

            this.setState(
                {
                    error: response.error,
                    errorMessage: '&nbsp;',
                    value: response.value
                },
                () => { inputOnChange && inputOnChange( value ); }
            );
        }

        return response.error;
    }

    _checkMin = ( value ) => value.length >= this.props.min;

    _checkMax = ( value ) => value.length <= this.props.max;

    _checkMinNumber = ( value ) => value >= this.props.minNumber;

    _checkMaxNumber = ( value ) => value <= this.props.maxNumber;

    _checkEmail = ( value ) => {
        const emailRegex = /^(?:[\w!#$%&'*+-/=?^`{|}~]+\.)*[\w!#$%&'*+-/=?^`{|}~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])]))$/;

        return emailRegex.test( value );
    }

    _handleFocus = () => {
        this.setState({ focus: true }, () => {
            this.props.onFocus && this.props.onFocus();
        });
    }

    _checkSpaces = ( value ) => value.indexOf( ' ' ) === -1;

    /**
     *  it will return the required label for the current input.
     */
    _getLabel() {
        const requiredIndicator = <span className="required">*</span>;
        const labelWithIndicator = this.props.new
            ? <span>{this.props.label} {requiredIndicator}</span>
            : <span>{requiredIndicator} {this.props.label}</span>;

        return this.props.required
            ? labelWithIndicator
            : this.props.label;
    }

    /**
     * Render method
     */
    render() {
        const InputElement = (
            <BootstrapInput
                className={ this.state.error ? 'error' : '' }
                id={ this.props.id }
                name={ this.props.id }
                type={ this.props.type }
                disabled={ this.state.disabled }
                onKeyPress={ this.props.onKeyPress }
                onChange={ this._handleChange }
                onFocus={ this._handleFocus }
                onBlur={ this._handleChange }
                placeholder={ this.props.placeholder }
                value={ this.state.value }
            />
        );

        let InputComponent;
        if ( this.props.addon ) {
            InputComponent = this.props.addon.placement === 'left' ? (
                <InputGroup>
                    <InputGroupAddon>{ this.props.addon.content }</InputGroupAddon>
                    { InputElement }
                </InputGroup>
            ) : (
                <InputGroup>
                    { InputElement }
                    <InputGroupAddon>{ this.props.addon.content }</InputGroupAddon>
                </InputGroup>
            );
        } else {
            InputComponent = InputElement;
        }
        return (
            <Wrapper className={ this.props.className }>
                {
                    this.props.new
                        ?
                            <div style={ { width: '100%', display: 'flex', flexDirection: this.props.new ? 'row' : 'column' } }>
                                <div style={ { margin: '10px 0 0 0', width: '22%' } }>
                                    {
                                        this.props.label ? <label htmlFor={ this.props.id } style={ this.state.error ? { color: '#f2130a' } : this.state.focus ? { color: '#149ed3' } : {} }>{ this._getLabel() }</label> : ''
                                    }
                                </div>
                                <div style={ { width: '78%' } }>
                                    { InputComponent }
                                    {
                                        this.state.error
                                            ? (
                                                <p>{this.state.errorMessage}</p>
                                            )
                                            : null
                                    }
                                </div>
                            </div>
                        :
                            <div style={ { display: 'flex', width: '100%', flexDirection: this.props.new ? 'row' : 'column' } }>
                                { InputComponent }
                                {
                                    this.props.label ? <label htmlFor={ this.props.id } style={ this.state.error ? { color: '#f2130a' } : this.state.focus ? { color: '#149ed3' } : {} }>{ this._getLabel() }</label> : ''
                                }
                                {
                                    this.state.error
                                        ? (
                                            <p>{this.state.errorMessage}</p>
                                        )
                                        : null
                                }
                            </div>
                }
            </Wrapper>
        );
    }
}

export default Input;
