import React from 'react';
import { shallow, mount } from 'enzyme';
import Input from '../index';

describe( '<Input />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <Input id="test" label="test" />
        );
        expect( component.html() ).toBeTruthy();
    });

    it( 'should have prop key "type" and the default return value is "text"', () => {
        const component = shallow(
            <Input id="test" />
        );
        const props = component.instance().props;
        expect( props.type ).toEqual( 'text' );
    });

    it( 'should have prop key "disabled" and the default return value is "text"', () => {
        const component = shallow(
            <Input id="test" label="test" />
        );
        const props = component.instance().props;
        expect( props.disabled ).toEqual( false );
    });

    it( 'should handle prop changes', () => {
        const component = shallow(
            <Input id="test" label="test" value="3" />
        );
        expect( component.instance().props.value ).toEqual( '3' );
        component.setProps({ value: '4' });
        expect( component.instance().props.value ).toEqual( '4' );
    });

    it( 'should have a rendered label and placeholder', () => {
        const component = shallow(
            <Input
                id="test"
                label="label"
                placeholder="placeholder"
            />
        );
        const props = component.instance().props;
        expect( props.label ).toEqual( 'label' );
        expect( props.placeholder ).toEqual( 'placeholder' );
    });

    it( 'should have the default expected states', () => {
        const component = shallow(
            <Input
                id="test"
                label="test"
            />
        );
        const state = component.instance().state;
        expect( state.error ).toEqual( false );
    });

    it( 'should handle focus', () => {
        const spy = jest.fn();

        const component = mount(
            <Input
                id="test"
                label="test"
                onFocus={ spy }
            />
        );

        component.find( 'input' ).simulate( 'focus' );

        expect( spy ).toHaveBeenCalled();
    });

    it( 'should handle blur', () => {
        const spy = jest.fn();

        const component = mount(
            <Input
                id="test"
                label="test"
                onBlur={ spy }
            />
        );

        component.find( 'input' ).simulate( 'blur' );

        expect( spy ).toHaveBeenCalled();
    });

    it( 'should check the minimum value required by the input', () => {
        const component = shallow(
            <Input
                id="test"
                min={ 3 }
                label="test"
            />
        );
        expect( component.instance()._checkMin( 'ab' ) ).toEqual( false );
        expect( component.instance()._checkMin( 'abcd' ) ).toEqual( true );
    });

    it( 'should be mounted as disabled', () => {
        const component = shallow(
            <Input
                id="test"
                label="test"
                disabled
            />
        );
        expect( component.instance().state.disabled ).toEqual( true );
    });

    it( 'should be able to toggle disabled state on and off', () => {
        const component = shallow(
            <Input
                id="test"
                label="test"
                disabled
            />
        );
        expect( component.instance().state.disabled ).toEqual( true );
        component.setProps({ disabled: false });
        expect( component.instance().state.disabled ).toEqual( false );
        component.setProps({ disabled: true });
        expect( component.instance().state.disabled ).toEqual( true );
    });

    it( 'should check the max value required by the input', () => {
        const component = shallow(
            <Input
                id="test"
                max={ 3 }
                label="test"
            />
        );
        expect( component.instance()._checkMax( 'ab' ) ).toEqual( true );
        expect( component.instance()._checkMax( 'abcd' ) ).toEqual( false );
    });

    it( 'should check the min number value required by the input', () => {
        const component = shallow(
            <Input
                id="test"
                minNumber={ 50 }
                label="test"
            />
        );
        expect( component.instance()._checkMinNumber( 100 ) ).toEqual( true );
        expect( component.instance()._checkMinNumber( 0 ) ).toEqual( false );
    });

    it( 'should check the max number value required by the input', () => {
        const component = shallow(
            <Input
                id="test"
                maxNumber={ 50 }
                label="test"
            />
        );
        expect( component.instance()._checkMaxNumber( 50 ) ).toEqual( true );
        expect( component.instance()._checkMaxNumber( 100 ) ).toEqual( false );
    });

    it( 'should check if the input value is a email', () => {
        const component = shallow(
            <Input
                id="test"
                type="email"
                label="test"
            />
        );
        expect(
            component.instance()._checkEmail( 'foo@bar.com' )
        ).toEqual( true );
        expect(
            component.instance()._checkEmail( 'email' )
        ).toEqual( false );
    });

    it( 'should check if the input value have spaces or not', () => {
        const component = shallow(
            <Input id="test" label="test" />
        );
        expect(
            component.instance()._checkSpaces( '' )
        ).toEqual( true );
        expect(
            component.instance()._checkSpaces( ' ' )
        ).toEqual( false );
    });

    it( 'should throw an error if the input field have spaces and type is set to password', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <Input
                onChange={ _handleChange }
                id="test"
                type="password"
                label="test"
            />
        );
        const eventObject = {
            target: {
                value: 'asdawfas dawf'
            }
        };
        component.find( 'input' ).simulate( 'blur', eventObject );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'This field cannot contain spaces',
            value: 'asdawfas dawf',
            disabled: false
        });

        component.find( 'input' ).simulate( 'blur', { target: { value: '12345678' }});

        expect( component.instance().state.error ).toEqual( false );
    });

    it( 'should throw an error if the input field is not a valid email and the prop type is set to email', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <Input
                onChange={ _handleChange }
                id="test"
                type="email"
                label="test"
            />
        );
        const eventObject = {
            target: {
                value: 'email'
            }
        };
        component.find( 'input' ).simulate( 'blur', eventObject );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'Enter a valid email address.',
            value: 'email',
            disabled: false
        });

        component.find( 'input' ).simulate( 'blur', { target: { value: 'test@salarium.com' }});

        expect( component.instance().state.error ).toEqual( false );
    });

    it( 'should throw an error if the required minimum character are not met', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <Input
                onChange={ _handleChange }
                id="test"
                min={ 3 }
                label="test"
            />
        );
        const eventObject = {
            target: {
                value: 'ab'
            }
        };
        component.find( 'input' ).simulate( 'blur', eventObject );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'This field requires at least 3 characters',
            value: 'ab',
            disabled: false
        });
    });

    it( 'should throw an error if the required maximum character are not met', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <Input
                onChange={ _handleChange }
                id="test"
                max={ 3 }
                label="test"
            />
        );
        const eventObject = {
            target: {
                value: 'abcd'
            }
        };
        component.find( 'input' ).simulate( 'blur', eventObject );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'This field must not exceed 3 characters',
            value: 'abcd',
            disabled: false
        });
    });

    it( 'should throw an error if the input prop is set to required and the value is empty', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <Input
                onChange={ _handleChange }
                id="test"
                required
                label="test"
            />
        );
        const eventObject = {
            target: {
                value: ''
            }
        };
        component.find( 'input' ).simulate( 'blur', eventObject );

        expect(
            component.instance().state
        ).toEqual({
            error: true,
            focus: false,
            errorMessage: 'This field is required',
            value: '',
            disabled: false
        });
    });

    it( 'should pass validations if input is empty and required props is not defined', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <Input
                onChange={ _handleChange }
                id="test"
                label="test"
            />
        );
        const eventObject = {
            target: {
                value: ''
            }
        };
        component.find( 'input' ).simulate( 'blur', eventObject );

        expect( component.instance().state.error ).toEqual( false );
    });

    it( 'should call _handleChange event in onChange event', () => {
        const _handleChange = jest.fn();
        const component = mount(
            <Input
                id="test"
                onChange={ _handleChange }
                label="test"
            />
        );
        const eventObject = {
            target: {
                value: 'My new value'
            }
        };
        component.find( 'input' ).simulate( 'change', eventObject );
        expect( _handleChange ).toHaveBeenCalled();
    });

    it( 'should set a value by default', () => {
        const component = mount(
            <Input
                id="test"
                value="test"
                label="test"
            />
        );
        expect(
            component.find( 'input' ).node.value
        ).toEqual( 'test' );
    });

    it( 'should handle input focus', () => {
        const component = mount(
            <Input
                id="test"
                value="test"
                label="test"
            />
        );
        component.find( '#test' ).simulate( 'focus' );
        expect( component.instance().state.focus ).toEqual( true );
    });

    it( 'can contain input addon on left side', () => {
        const component = mount(
            <Input
                id="test"
                value="test"
                label="test"
                addon={ {
                    placement: 'left',
                    content: 'hehe'
                } }
            />
        );
        expect( component.find( '.input-group-addon + input' ) ).toBeTruthy();
    });

    it( 'can contain input addon on right side', () => {
        const component = shallow(
            <Input
                id="test"
                value="test"
                label="test"
                addon={ {
                    placement: 'right',
                    content: 'hehe'
                } }
            />
        );
        expect( component.find( 'input + .input-group-addon' ) ).toBeTruthy();
    });

    it( 'should be able to reach validations externally', () => {
        const component = shallow(
            <Input
                id="test"
                value="test"
                label="test"
            />
        );
        component.instance()._validate( 'Test Value' );
        expect(
            component.instance().state
        ).toEqual({
            error: false,
            focus: false,
            errorMessage: '&nbsp;',
            value: 'Test Value',
            disabled: false
        });
    });
});
