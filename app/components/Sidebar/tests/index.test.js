import React from 'react';
import { shallow } from 'enzyme';

import Sidebar from '../../Sidebar';

describe( '<Sidebar />', () => {
    it( 'should mount the component', () => {
        const component = shallow( <Sidebar items={ [] } /> );
        expect( component.html() ).toBeTruthy();
    });

    it( 'should render a child <SidebarItem /> component ', () => {
        const component = shallow(
            <Sidebar
                items={ [{ label: 'test', link: 'test', icon: 'test', sublinks: []}] }
            />
        );
        expect( component.find( 'SidebarItem' ) ).toHaveLength( 1 );
    });

    it( 'toggle collapsed state', () => {
        const component = shallow( <Sidebar items={ [] } /> );
        component.setState({
            isCollapsed: true
        });
        component.instance().toggle();
        expect( component.state( 'isCollapsed' ) ).toBeFalsy();
        component.instance().toggle();
        expect( component.state( 'isCollapsed' ) ).toBeTruthy();
    });

    it( 'should collapse and clear expanded item', () => {
        const component = shallow( <Sidebar items={ [] } /> );
        component.setState({
            isCollapsed: false,
            expandedItemId: 3
        });
        component.instance().collapse();
        expect( component.state( 'isCollapsed' ) ).toBeTruthy();
        expect( component.state( 'expandedItemId' ) ).toBeNull();
    });

    it( 'should set and unset expanded item', () => {
        const component = shallow( <Sidebar items={ [] } /> );
        component.instance().expand( 1 );
        expect( component.state( 'expandedItemId' ) ).toEqual( 1 );
        component.instance().expand( 1 );
        expect( component.state( 'expandedItemId' ) ).toBeNull();
    });
});
