import React, { PropTypes } from 'react';

import {
    Wrapper
} from './styles';

import Icon from '../Icon';

import SidebarItem from './SidebarItem';

/**
 * Sidebar Component
 */
class Sidebar extends React.Component {
    static propTypes = {
        items: PropTypes.arrayOf( React.PropTypes.shape({
            label: React.PropTypes.string.isRequired,
            link: React.PropTypes.string.isRequired,
            icon: React.PropTypes.string.isRequired,
            native: React.PropTypes.bool,
            sublinks: React.PropTypes.arrayOf( React.PropTypes.shape({
                label: React.PropTypes.string.isRequired,
                link: React.PropTypes.string.isRequired,
                native: React.PropTypes.bool.isRequired
            }) )
        }) )
    };

    /**
     * Component constructor
     */
    constructor( props ) {
        super( props );

        this.state = {
            isCollapsed: true,
            expandedItemId: null
        };
    }

    /**
     * Toggles sidebar
     */
    toggle = () => {
        const { isCollapsed } = this.state;

        this.setState({
            isCollapsed: !isCollapsed,
            expandedItemId: null
        });
    };

    /**
     * Collapses the sidebar
     */
    collapse = () => {
        this.setState({
            isCollapsed: true,
            expandedItemId: null
        });
    }

    /**
     * Expands (or collapses) item with the given id
     */
    expand = ( itemId ) => {
        const { expandedItemId } = this.state;

        this.setState({
            expandedItemId: itemId === expandedItemId ? null : itemId,
            isCollapsed: false
        });
    };

    /**
     * Component render method
     */
    render() {
        const { isCollapsed, expandedItemId } = this.state;
        const { items } = this.props;

        return (
            <Wrapper className={ isCollapsed ? 'sl-is-collapsed' : 'sl-is-expanded' }>
                <ul className="sl-c-dropdown">
                    <li className="sl-c-dropdown__controls">
                        <button className="sl-c-dropdown__icon" onClick={ this.toggle }>
                            <Icon name="carets" style={ { transform: isCollapsed ? '' : 'scaleX(-1)' } } />
                        </button>

                        <div className="sl-c-dropdown__item">
                            <button className="sl-c-dropdown__title" onClick={ this.collapse }>
                                Close
                            </button>
                        </div>
                    </li>

                    { items.map( ( item, index ) => (
                        !item.hide && <SidebarItem
                            key={ index }
                            item={ { ...item, id: index } }
                            isExpanded={ expandedItemId === index }
                            expand={ this.expand }
                        />
                    ) ) }
                </ul>
            </Wrapper>
        );
    }
}

export default Sidebar;
