import styled from 'styled-components';

const Wrapper = styled.div`
    position: fixed;
    top: 4.7rem;
    bottom: 0;
    left: 0;
    z-index: 10;
    width: 22rem;
    overflow-y: auto;
    color: #ffffff;
    font-size: 14px;
    background-color: #02688f;
    transition: all .2s ease-in-out;

    &:before {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 5.7rem;
        border-right: 1px solid #0077a5;
        content: '';
    }

    &.sl-is-collapsed {
        width: 5.7rem;
        overflow: hidden;
    }

    a,
    button,
    .sl-c-dropdown__icon,
    .sl-c-dropdown__title {
        color: #ffffff;
        border-radius: 0;

        &:hover {
            background-color: #0077A5;
            cursor: pointer;
            text-decoration: none;
        }
    }

    button {
      width: 100%;
      text-align: left;
    }

    .sl-c-dropdown__title {
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 1.1rem 1rem;

        .isvg {
            width: .875rem;
            height: .875rem;
        }
    }

    .sl-c-dropdown__item {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        flex-grow: 1;

        > ul {
            margin: 0;
            padding: 0;
            list-style-type: none;

            > li {
                &.sl-is-active {
                    background-color: #00A5E5;

                    > a {
                        color: #ffffff;
                    }
                }

                > a, button {
                    display: block;
                    padding: 1rem 1rem 1rem 3rem;
                    color: #cccccc;
                }
            }
        }
    }

    .sl-c-dropdown {
        position: relative;
        width: 22rem;
        padding-left: 0;

        &:before {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 5.7rem;
            border-right: 1px solid #0077a5;
            content: '';
        }

        > li {
            display: flex;
            align-items: flex-start;
            justify-content: space-between;

            &.sl-is-active {
                .sl-c-dropdown__icon,
                .sl-c-dropdown__title {
                    background-color: #00A5E5;
                }

                .sl-c-dropdown__icon {
                    &:before {
                        position: absolute;
                        top: 1rem;
                        right: .5rem;
                        width: .5rem;
                        height: .5rem;
                        border-radius: 50%;
                        background-color: #9fdc74;
                        content: '';
                    }
                }
            }
        }
    }

    &.sl-is-collapsed {
        .sl-c-dropdown__controls {
            .sl-c-dropdown__icon {
                .sl-u-rotate--180 {
                    transform: rotate(0);
                }
            }
        }
    }

    .sl-c-dropdown__icon,
    .sl-c-dropdown__title {
        min-height: 3rem;
        background-color: transparent;
        border: none;
        color: #ffffff;
    }

    .sl-c-dropdown__icon {
        position: relative;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: .88rem 1rem;
        width: 5.7rem;

        &:after {
            position: absolute;
            top: -1px;
            left: 50%;
            transform: translateX(-50%);
            width: 3rem;
            border-bottom: 1px solid #0077a5;
            content: '';
        }
    }

    .sl-c-dropdown__controls {
        .sl-c-dropdown__icon,
        .sl-c-dropdown__title {
            min-height: 2.5rem;
            padding-top: 0.66rem;
            padding-bottom: 0.66rem;
        }

        .sl-c-dropdown__icon {
            &:after {
                display: none;
            }

            .isvg {
                width: .875rem;
                height: .875rem;
            }
        }

        .sl-c-dropdown__title {
            justify-content: flex-end;
        }
    }

    .isvg {
        display: inline-flex;
        align-items: center;
        justify-content: center;
        width: 1.75rem;
        height: 1.75rem;

        > svg {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 1.75rem;
            height: 1.75rem;
        }
    }
`;

export {
    Wrapper
};
