import React from 'react';
import { shallow } from 'enzyme';

import SidebarItem from '../../SidebarItem';

describe( '<SidebarItem />', () => {
    it( 'should mount the component', () => {
        const component = shallow(
            <SidebarItem
                item={ {
                    id: 1,
                    label: 'test',
                    link: '/test',
                    icon: 'arrow',
                    sublinks: []
                } }
                expand={ () => {} }
            />
        );
        expect( component.html() ).toBeTruthy();
    });

    it( 'should call method passed as prop on button click', () => {
        const mockFunction = jest.fn();
        const component = shallow(
            <SidebarItem
                item={ {
                    id: 1,
                    label: 'test',
                    link: '/test',
                    icon: 'arrow',
                    sublinks: [{
                        label: 'test',
                        link: '/test'
                    }]
                } }
                expand={ mockFunction }
            />
        );
        component.find( 'button' ).at( 1 ).simulate( 'click' );
        expect( mockFunction ).toHaveBeenCalled();
    });
});
