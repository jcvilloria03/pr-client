import React from 'react';
import { browserHistory } from 'utils/BrowserHistory';

import Icon from '../../Icon';

/**
 * SidebarItem component
 */
class SidebarItem extends React.PureComponent {
    static propTypes = {
        item: React.PropTypes.shape({
            id: React.PropTypes.number.isRequired,
            label: React.PropTypes.string.isRequired,
            link: React.PropTypes.string.isRequired,
            icon: React.PropTypes.string.isRequired,
            native: React.PropTypes.bool,
            sublinks: React.PropTypes.arrayOf( React.PropTypes.shape({
                label: React.PropTypes.string.isRequired,
                link: React.PropTypes.string.isRequired,
                native: React.PropTypes.bool.isRequired
            }) )
        }).isRequired,
        isExpanded: React.PropTypes.bool,
        expand: React.PropTypes.func.isRequired
    };

    /**
     * Checks if the given link is currently active
     */
    isActiveLink = ( link ) => (
        window.location.pathname.includes( link )
    );

    /**
     * Component render method
     */
    render() {
        const { item, isExpanded, expand } = this.props;

        return (
            <li className={ this.isActiveLink( item.link ) ? 'sl-is-active' : '' }>
                { item.sublinks.length
                    ? (
                        <button className="sl-c-dropdown__icon" onClick={ () => expand( item.id ) }>
                            <Icon name={ item.icon } />
                        </button>
                    )
                    : item.native
                      ? ( <button className="sl-c-dropdown__icon" onClick={ () => browserHistory.push( item.link, true ) }> <Icon name={ item.icon } /> </button> )
                      : ( <a className="sl-c-dropdown__icon" href={ item.link }><Icon name={ item.icon } /></a> )
                  }

                <div className="sl-c-dropdown__item">
                    { item.sublinks.length
                        ? (
                            <button className="sl-c-dropdown__title" onClick={ () => expand( item.id ) }>
                                <span>
                                    { item.label }
                                </span>
                                <Icon name="chevron" style={ { transform: isExpanded ? 'scaleY(-1)' : '' } }></Icon>
                            </button>
                        )
                        : item.native
                          ? ( <button className="sl-c-dropdown__title" onClick={ () => browserHistory.push( item.link, true ) }> <span> { item.label } </span> </button> )
                          : ( <a className="sl-c-dropdown__title" href={ item.link }><span> { item.label } </span></a> )
                    }

                    { item.sublinks.length !== 0 && isExpanded
                        ? (
                            <ul>
                                { item.sublinks.filter( ( sublink ) => !sublink.hide ).map( ( sublink, index ) => (
                                    <li key={ index } className={ this.isActiveLink( sublink.link ) ? 'sl-is-active' : '' }>
                                        { sublink.native
                                            ? ( <button onClick={ () => browserHistory.push( sublink.link, true ) }> { sublink.label } </button> )
                                            : ( <a href={ sublink.link }>{ sublink.label }</a> )
                                        }
                                    </li>
                                ) ) }
                            </ul>
                        ) : null
                    }
                </div>
            </li>
        );
    }
}

export default SidebarItem;
